#ifndef I2C_STRESS_TEST_H
#define I2C_STRESS_TEST_H

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/input.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <sys/select.h>
#include <sys/time.h>
#include <cstdlib>
#include <sstream>

#include <gtest/gtest.h>

class I2cStressTest: public ::testing::Test
{
protected:
	void SetUp()
	{
	}

	void TearDown()
	{
	}
};

class I2cTestDevice
{
public:
	I2cTestDevice(const std::string& dev_name, const std::string& dev_path);
	virtual ~I2cTestDevice();

	int get_fd(){return fd;}
	bool open();
	std::string get_name(){return name;}
	void reset_samples_count(){samples_count = 0;}

	int get_samples_count(){return samples_count;}
	int read_samples();

	virtual void store_poll_rate(){}
	virtual void set_poll_rate_ms(int poll_rate){}
	virtual void restore_poll_rate(){}

	virtual void enable(){}
	virtual void restore_enabled(){}

private:
	void restore_poll_rate_ms();

	int fd;
	std::string name;
	std::string path;
	int samples_count;
};

class GyroTestDevice : public I2cTestDevice
{
public:
	GyroTestDevice();
	virtual void store_poll_rate();
	virtual void set_poll_rate_ms(int poll_rate);
	virtual void restore_poll_rate();

	virtual void enable();
	virtual void restore_enabled();
};

class AccTestDevice : public I2cTestDevice
{
public:
	AccTestDevice();
	virtual void store_poll_rate();
	virtual void set_poll_rate_ms(int poll_rate);
	virtual void restore_poll_rate();
};

class PressureSensorTestDevice : public I2cTestDevice
{
public:
	PressureSensorTestDevice();
	virtual void store_poll_rate();
	virtual void set_poll_rate_ms(int poll_rate);
	virtual void restore_poll_rate();

	virtual void enable();
	virtual void restore_enabled();
};

class I2cTest
{
public:
	I2cTest();
	~I2cTest();

	void add_device(I2cTestDevice* device);

	bool run_test(int duration_seconds, int poll_rate_ms);

	std::string get_failure_description(){return test_failure_description;}

	void set_success_margin(int margin_percent){success_margin_percent = margin_percent;}

private:

	void store_poll_rates();
	void set_poll_rates(int poll_rate_ms);
	void restore_poll_rates();
	bool determine_success();
	void enable_devices();
	void restore_devices_enabled();

	int reset_devices_and_get_max_fd();
	void print_results();

	int test_duration_seconds;
	int test_poll_rate_ms;

	std::vector<I2cTestDevice*> devices;
	std::string test_failure_description;
	int success_margin_percent;
};


I2cTestDevice::I2cTestDevice(const std::string &dev_name, const std::string& dev_path)
	: fd(-1)
	, name(dev_name)
	, path(dev_path)
	, samples_count(0)
{

}

I2cTestDevice::~I2cTestDevice()
{
	if (fd > -1)
		close(fd);
}

bool I2cTestDevice::open()
{
	fd = ::open(path.c_str(), O_RDONLY);
	return fd >= 0;
}

int I2cTestDevice::read_samples()
{
	if (fd < 0)
		return 0;

	int bytes_read = 0;
	struct input_event events[64];
	int samples = 0;

	bytes_read = read(fd, events, sizeof(events));

	if (bytes_read < (int) sizeof(struct input_event)) {
	    printf("%s: Expected %d bytes, got %d\n", name.c_str() , (int) sizeof(struct input_event), bytes_read);
	    return 0;
	}

	for (int i = 0; i < bytes_read / sizeof(struct input_event); i++) {
	    unsigned int type, code;

	    type = events[i].type;
	    code = events[i].code;

	    if (type == EV_SYN)
	    {
		samples++;
	    }
	}

	samples_count+=samples;
	return samples;
}



GyroTestDevice::GyroTestDevice()
	:I2cTestDevice("Gyro", "/dev/input/event5")
{
}

void GyroTestDevice::store_poll_rate()
{
	system("cat /tmp/sys_platform-gyro/device/delay > /dev/shm/gyro_pr");
}

void GyroTestDevice::set_poll_rate_ms(int poll_rate)
{
	char set_polling_rate[64];
	sprintf(set_polling_rate, "echo %d > /tmp/sys_platform-gyro/device/delay", poll_rate);
	system(set_polling_rate);
}

void GyroTestDevice::restore_poll_rate()
{
	system("cat /dev/shm/gyro_pr > /tmp/sys_platform-gyro/device/delay; rm /dev/shm/gyro_pr");
}

void GyroTestDevice::enable()
{
	// Save enabled status so we can restore it
	system("cat /tmp/sys_platform-gyro/device/enable > /dev/shm/gyro_en");
	system("echo 1 > /tmp/sys_platform-gyro/device/enable");
}

void GyroTestDevice::restore_enabled()
{
	system("cat /dev/shm/gyro_en > /tmp/sys_platform-gyro/device/enable; rm /dev/shm/gyro_en");
}

AccTestDevice::AccTestDevice()
	: I2cTestDevice("Accelerometer", "/dev/input/event4")
{
}

void AccTestDevice::store_poll_rate()
{
	system("cat /tmp/sys_platform-acc/device/pollrate_ms > /dev/shm/acc_pr");
}

void AccTestDevice::set_poll_rate_ms(int poll_rate)
{
	char set_polling_rate[64];
	sprintf(set_polling_rate, "echo %d > /tmp/sys_platform-acc/device/pollrate_ms", poll_rate);
	system(set_polling_rate);
}

void AccTestDevice::restore_poll_rate()
{
	system("cat /dev/shm/acc_pr > /tmp/sys_platform-acc/device/pollrate_ms; rm /dev/shm/acc_pr");
}


PressureSensorTestDevice::PressureSensorTestDevice()
	: I2cTestDevice("Pressure sensor", "/dev/input/event6")
{

}

void PressureSensorTestDevice::store_poll_rate()
{
	system("cat /tmp/sys_platform-presure/delay > /dev/shm/press_pr");
}

void PressureSensorTestDevice::set_poll_rate_ms(int poll_rate)
{
	char set_polling_rate[64];
	sprintf(set_polling_rate, "echo %d > /tmp/sys_platform-presure/delay", poll_rate);
	system(set_polling_rate);
}

void PressureSensorTestDevice::restore_poll_rate()
{
	system("cat /dev/shm/press_pr > /tmp/sys_platform-presure/delay; rm /dev/shm/press_pr");
}

void PressureSensorTestDevice::enable()
{
	// Save enabled status so we can restore it
	system("cat /tmp/sys_platform-presure/enable > /dev/shm/pressure_en");
	system("echo 1 > /tmp/sys_platform-presure/enable");
}

void PressureSensorTestDevice::restore_enabled()
{
	system("cat /dev/shm/pressure_en > /tmp/sys_platform-presure/enable; rm /dev/shm/pressure_en");
}


I2cTest::I2cTest()
	: test_duration_seconds(3)
	, test_poll_rate_ms(200)
	, success_margin_percent(20)
{

}

I2cTest::~I2cTest()
{
	for (int i = 0; i < devices.size(); ++i)
		delete devices[i];
}

void I2cTest::add_device(I2cTestDevice *device)
{
	if (device){
		if (!device->open())
			printf("Could not open %s device", device->get_name().c_str());
		devices.push_back(device);
	}
}

bool I2cTest::run_test(int seconds, int poll_rate_ms)
{
	if(seconds<=0 || poll_rate_ms<=0 || poll_rate_ms >=1000)
	{
		test_failure_description = "Invalid test input.\n";
		return false;
	}

	fd_set devices_to_read;
	FD_ZERO(&devices_to_read);
	int max_fd_val = 0;
	bool stop = false;

	struct timeval poll_time;
	poll_time.tv_sec = seconds;
	poll_time.tv_usec = 0;

	test_duration_seconds = seconds;
	test_poll_rate_ms = poll_rate_ms;

	printf("\nRunning test for %d seconds, with %dms poll rates\n", seconds, poll_rate_ms);

	store_poll_rates();
	set_poll_rates(poll_rate_ms);
	enable_devices();

	max_fd_val = reset_devices_and_get_max_fd();

	while (!stop)
	{
		// Add all open devices to the set (has to be done every time)
		for(int i = 0; i < devices.size(); ++i){
			if (devices[i]->get_fd() > -1)
				FD_SET(devices[i]->get_fd(), &devices_to_read); // Add device to set

		}

		// Block until any device ready for reading
		stop = !select(max_fd_val+1, &devices_to_read, 0, 0, &poll_time);

		// If select returns 0, no device is ready so break.
		if (stop)
		    break;

		// For every device check if it has data ready and read the samples
		for(int i = 0; i < devices.size(); ++i){
			if (FD_ISSET(devices[i]->get_fd(), &devices_to_read)){
			    devices[i]->read_samples();
			}
		}
	}

	print_results();

	restore_poll_rates();
	restore_devices_enabled();

	return determine_success();
}

void I2cTest::store_poll_rates()
{
	for(int i = 0; i < devices.size(); ++i)
		devices[i]->store_poll_rate();
}

void I2cTest::set_poll_rates(int poll_rate_ms)
{
	for(int i = 0; i < devices.size(); ++i)
		devices[i]->set_poll_rate_ms(poll_rate_ms);
}

void I2cTest::restore_poll_rates()
{
	for(int i = 0; i < devices.size(); ++i)
		devices[i]->restore_poll_rate();
}

void I2cTest::enable_devices()
{
	for(int i = 0; i < devices.size(); ++i)
		devices[i]->enable();
}

void I2cTest::restore_devices_enabled()
{
	for(int i = 0; i < devices.size(); ++i)
		devices[i]->restore_enabled();
}

bool I2cTest::determine_success()
{
	bool success = true; // Let's be optimistic

	test_failure_description.clear();
	std::stringstream message_stream;

	for(int i = 0; i < devices.size(); ++i){

		I2cTestDevice* device = devices[i];
		int target_samples_count = (test_duration_seconds* 1000) / test_poll_rate_ms;
		int measured_samples_count = device->get_samples_count();

		// Allow significant margin (our measurement is not really precise, it just gives us an idea)
		if (abs(target_samples_count - measured_samples_count) > target_samples_count/(100/success_margin_percent))
		{
			message_stream  << device->get_name() << " failed at poll rate " <<test_poll_rate_ms
					<<"ms. Expected " << target_samples_count << " samples ("
					<<1000/test_poll_rate_ms << " samples/sec), "
					<< "received "<< device->get_samples_count()<< " samples ("
					<< device->get_samples_count()/test_duration_seconds
					<< " samples/sec)" << std::endl;
			success = false;
		}
	}

	test_failure_description = message_stream.str();
	return success;
}

int I2cTest::reset_devices_and_get_max_fd()
{
	int max_fd_val = 0;

	// Loop all devices, reset their samples count and figure out max fd value
	for(int i = 0; i < devices.size(); ++i){
		if (devices[i]->get_fd() > -1){
			devices[i]->reset_samples_count();

			if (devices[i]->get_fd() > max_fd_val) // Find max fd value (needed for select() call)
				max_fd_val = devices[i]->get_fd();
		}
	}

	return max_fd_val;
}

void I2cTest::print_results()
{
	// Print results:
	for(int i = 0; i < devices.size(); ++i){
		printf("%s samples: %d(%d/sec)\n",
			devices[i]->get_name().c_str(),
			devices[i]->get_samples_count(),
			devices[i]->get_samples_count()/test_duration_seconds);
	}
}

TEST_F(I2cStressTest, test_gyro_acc_pressure_sending_samples)
{
	I2cTest test;

	// Test takes ownership and will delete test objects.
	test.add_device(new GyroTestDevice);
	test.add_device(new AccTestDevice);
	test.add_device(new PressureSensorTestDevice);

	// Test for one second in 10 samples/sec
	int test_duration_seconds = 1;
	int polling_rate = 100; // ms

	test.set_success_margin(50); // Sample count margin is 50% - just check if are getting any samples

	// Run test
	bool success = test.run_test(test_duration_seconds, polling_rate);

	// Handle if test was not successful
	if (!success) {
		EXPECT_TRUE(false) << test.get_failure_description();
	}
}

TEST_F(I2cStressTest, DISABLED_test_gyro_acc_pressure_sample_rates)
{
	I2cTest test;

	// Test takes ownership and will delete test objects.
	test.add_device(new GyroTestDevice);
	test.add_device(new AccTestDevice);
	test.add_device(new PressureSensorTestDevice);

	int test_duration_seconds = 3;
	int polling_rate_intervals[] = {200, 100, 50, 30, 20, 10, 5}; // In ms

	int min_expected_poll_rate = 10; // Right now, anything below will fail

	// Loop polling intervals
	for (int i = 0; i < sizeof(polling_rate_intervals)/sizeof(polling_rate_intervals[0]); ++i){

		// Run test
		bool success = test.run_test(test_duration_seconds, polling_rate_intervals[i]);

		// Handle if test was not successful
		if (!success && polling_rate_intervals[i] >= min_expected_poll_rate) {
			EXPECT_TRUE(false) << test.get_failure_description();
		}
	}
}

TEST_F(I2cStressTest, DISABLED_stress_test_gyro_acc_pressure_sampling)
{
	I2cTest test;

	// Test takes ownership and will delete test objects.
	test.add_device(new GyroTestDevice);
	test.add_device(new AccTestDevice);
	test.add_device(new PressureSensorTestDevice);

	int test_duration_seconds = 60;
	int polling_rate = 10;

	// Run test, a full minute in 100samples/sec
	bool success = test.run_test(test_duration_seconds, polling_rate);

	// Handle if test was not successful
	if (!success) {
		EXPECT_TRUE(false) << test.get_failure_description();
	}
}


#endif // I2C_STRESS_TEST_H
