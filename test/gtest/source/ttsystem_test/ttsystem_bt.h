//
// file: tsystem_bt.cpp
//
// Test tool for libttsystem Bluetooth using GTEST
//

#ifndef TTSYSTEMBT_TEST_H
#define TTSYSTEMBT_TEST_H

#include <gtest/gtest.h>
#include "ttsystem.h"

#include<cstdlib>
#include<sstream>
#include "../tt_utils.h"


class TTSYSTEMBTTest: public ::testing::Test
{
protected:
	bool btinitialstate;

	void SetUp()
	{
		btinitialstate = TTUtils::command("systemctl status bluetopia | grep Active:", "running");
		printd("Initial BT service is %d\n", btinitialstate);
		ttsystem_stop_bt();
	}

	void TearDown()
	{
		if (btinitialstate == 0) {
			ttsystem_stop_bt();
			printd("Restoring BT service to STOPPED:\n");
		} else {
			ttsystem_start_bt();
			printd("Restoring BT service to RUNNING:\n");
		}
	}


};


// Test ttsystem_start_bt(). Starts the bluetooth system, there are no parameters.
// Returns: 0 on success, -errno on failure
TEST_F(TTSYSTEMBTTest, BT_start_stop)
{
	bool btstatus;

	btstatus = TTUtils::command("systemctl status bluetopia | grep Active:", "running");
	EXPECT_FALSE(btstatus) << "Expected Bluetopia service to NOT be running";

	EXPECT_EQ(0, ttsystem_start_bt()) << "BlueTooth start failed - first run";

	btstatus = TTUtils::command("systemctl status bluetopia | grep Active:", "running");
	EXPECT_TRUE(btstatus) << "Expected Bluetopia service to be running";
}

// Test ttsystem_start_bt(). Starts the bluetooth system, there are no parameters.
// Returns: 0 on success, -errno on failure
TEST_F(TTSYSTEMBTTest, BT_start_twice)
{
	bool btstatus;

	btstatus = TTUtils::command("systemctl status bluetopia | grep Active:", "running");
	EXPECT_FALSE(btstatus) << "Expected Bluetopia service to NOT be running";

	// start when already started, returns success
	EXPECT_EQ(0, ttsystem_start_bt()) << "BlueTooth start failed - first run of two";
	btstatus = TTUtils::command("systemctl status bluetopia | grep Active:", "running");
	EXPECT_TRUE(btstatus) << "Expected Bluetopia service to be running";

	// start a second time, when already running, returns success
	EXPECT_EQ(0, ttsystem_start_bt()) << "BlueTooth start failed - start when already started";
	btstatus = TTUtils::command("systemctl status bluetopia | grep Active:", "running");
	EXPECT_TRUE(btstatus) << "Expected Bluetopia service to be running";
}

#endif // TTSYSTEMBT_TEST_H

