#ifndef TTV_SLEEP_TEST_H
#define TTV_SLEEP_TEST_H

#include <gtest/gtest.h>
#include <ttv.h>
class TTVSleepTest : public ::testing::Test
{
protected:
	void SetUp()
	{
		runBasicSetup();
	}

	void TearDown()
	{
	}

	void runBasicSetup();
	void runSleepCycle(const int sleeping_period_sec);
};

TEST_F(TTVSleepTest, sleepTest)
{
	const int sleeping_period_sec = 10;

	runSleepCycle(sleeping_period_sec);
}

TEST_F(TTVSleepTest, sleepTestLoop)
{
	/* The number of loops */
	const int sleeping_loops = 20;
	const int sleeping_period_sec = 5;

	printf(">> Starting loop test. Loops: %d Period: %ds\n", sleeping_loops, sleeping_period_sec);
	for(int i=0; i<sleeping_loops; i++) {
		runSleepCycle( sleeping_period_sec);
		// Sleep for a half second, just in case.
		usleep(500000);
	}
}

void TTVSleepTest::runBasicSetup()
{
	struct timespec tp_presync, tp_postsync, tp_diff;
	/* Flushing the filesystem can take several seconds due to bad sectors on the SD card.
	 * Going to sleep does this internally, but we are timing it.
	 */

	clock_gettime(CLOCK_MONOTONIC, &tp_presync);
	sync();
	clock_gettime(CLOCK_MONOTONIC, &tp_postsync);
	tp_diff.tv_sec = tp_postsync.tv_sec - tp_presync.tv_sec;
	tp_diff.tv_nsec = tp_postsync.tv_nsec - tp_presync.tv_nsec;
	if (tp_diff.tv_nsec < 0) {
		tp_diff.tv_sec--;
		tp_diff.tv_nsec += 1000000000;
	}
	//printf(">>>>>>>>>>>> Sync() took %d.%ld Seconds\n", (int)tp_diff.tv_sec, tp_diff.tv_nsec);
}

void TTVSleepTest::runSleepCycle(const int sleeping_period_sec)
{
	struct timeval tv_sleep, tv_wakeup, tv_diff;
	struct timespec tp_sleep, tp_wakeup, tp_diff;

	/* Bottom boundary difference */
	const int sleeping_period = sleeping_period_sec*1000;
	const int sleeping_margin_sec = 2;

	/* Get time before ttv_sleep */
	gettimeofday(&tv_sleep, NULL);
	clock_gettime(CLOCK_BOOTTIME, &tp_sleep);


	/* ttv_sleep for 'sleeping_period_sec' seconds. Expect return code 0..TTV_SLEEP_MORE */
	EXPECT_LE(ttv_sleep(sleeping_period), TTV_SLEEP_MORE);

	/* Get time after ttv_sleep */
	gettimeofday(&tv_wakeup, NULL);
	clock_gettime(CLOCK_BOOTTIME, &tp_wakeup);

	timersub(&tv_wakeup, &tv_sleep, &tv_diff);
	tp_diff.tv_sec = tp_wakeup.tv_sec - tp_sleep.tv_sec;
	tp_diff.tv_nsec = tp_wakeup.tv_nsec - tp_sleep.tv_nsec;
	if (tp_diff.tv_nsec < 0) {
		tp_diff.tv_sec--;
		tp_diff.tv_nsec += 1000000000;
	}

	float f_tp_diff = tp_diff.tv_sec + (float)tp_diff.tv_nsec/(float)1000000000;
	float f_tv_diff = tv_diff.tv_sec + (float)tv_diff.tv_usec/(float)1000000;
	// Uncomment the following two lines for more details regarding timings.
	//printf(">>>>>>>>>>>> ttv_sleep() took: tp_diff = %f\n", f_tp_diff);
	//printf(">>>>>>>>>>>> ttv_sleep() took: tv_diff = %f\n", f_tv_diff);

	int sleeping_bottom_sec = sleeping_period_sec - sleeping_margin_sec;
	EXPECT_TRUE(f_tp_diff >= sleeping_bottom_sec) << ">> Failure: sleeping period TP (" << f_tp_diff << ") below bottom level (" << sleeping_bottom_sec << ")";
	EXPECT_TRUE(f_tp_diff <= sleeping_period_sec) << ">> Failure: sleeping period TP (" << f_tp_diff << ") over top level (" << sleeping_period_sec << ")";

	EXPECT_TRUE(f_tv_diff >= sleeping_bottom_sec) << ">> Failure: sleeping period TV (" << f_tv_diff << ") below bottom level (" << sleeping_bottom_sec << ")";
	EXPECT_TRUE(f_tv_diff <= sleeping_period_sec) << ">> Failure: sleeping period TV (" << f_tv_diff << ") over top level (" << sleeping_period_sec << ")";
}

#endif //TTV_SLEEP_TEST_H
