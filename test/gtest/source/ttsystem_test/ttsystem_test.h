//
// file: tsystem_test.cpp
//
// Test tool for libttsystem using GTEST
//

#ifndef TTSYSTEM_TEST_H
#define TTSYSTEM_TEST_H

#include <gtest/gtest.h>
#include "ttsystem.h"
#include "../tt_utils.h"

#include<cstdlib>
#include<sstream>

class TTSYSTEMTest: public ::testing::Test
{
protected:
	void SetUp()
	{

	}

	void TearDown()
	{
		/* Disable external mic */
		ttsystem_extmic_enable(false);
	}

	bool is_gps_sleepable;
	void gps_rtc_test();

};

TEST_F(TTSYSTEMTest, test_ttsystem_extmic_enable)
{

	FILE *fp;
	char mic_bias_voltage[3];

	// Disable external mic
	ttsystem_extmic_enable(false);

	fp = popen("amixer cget name='Mic Bias 2 Voltage' | tail -1 | grep -o [0-9]", "r");
	fgets(mic_bias_voltage, sizeof(mic_bias_voltage)-1, fp);
	pclose(fp);

	EXPECT_STREQ("0", mic_bias_voltage);

	// Enable external mic
	ttsystem_extmic_enable(true);

	fp = popen("amixer cget name='Mic Bias 2 Voltage' | tail -1 | grep -o [0-9]", "r");
	fgets(mic_bias_voltage, sizeof(mic_bias_voltage)-1, fp);
	pclose(fp);

	EXPECT_STREQ("2", mic_bias_voltage);


}

TEST_F(TTSYSTEMTest, test_ttsystem_gps_put_to_sleep)
{

	is_gps_sleepable = false;
	FILE *fp = NULL;
	int res = -1;
	const char *echo_sleep = "echo 1 > /sys/class/tty/ttyGSD5xp0/device/sleep";
	const char *echo_sleep_not = "echo 0 > /sys/class/tty/ttyGSD5xp0/device/sleep";
	const char *echo_awake = "cat /sys/class/tty/ttyGSD5xp0/device/awake";

	//Go to sleep
	system(echo_sleep);
	//Check if asleep
	fp = popen(echo_awake, "r");
	if(!fp) {
		ASSERT_TRUE(false) << "Unable to open awake pipe";
	}
	fscanf(fp, "%d", &res);

	EXPECT_TRUE(res == 0);

	if(res == 0) {
		is_gps_sleepable = true;
	}
	pclose(fp);

	//Wake up
	sleep(2);

	system(echo_sleep_not);
	//Check if awake
	fp = popen(echo_awake, "r");
	if(!fp) {
		ASSERT_TRUE(false) << "Unable to open awake pipe";
	}
	fscanf(fp, "%d", &res);

	EXPECT_TRUE(res == 1);

	pclose(fp);

}

TEST_F(TTSYSTEMTest, test_ttsystem_set_rtc)
{
	// Setting RTC while awake test

	gps_rtc_test();

}


TEST_F(TTSYSTEMTest, test_ttsystem_set_rtc_while_asleep)
{

	// Setting RTC while asleep test
	if (is_gps_sleepable) {
		FILE *fp = NULL;
		int res = -1;
		const char *echo_sleep = "echo 1 > /sys/class/tty/ttyGSD5xp0/device/sleep";
		const char *echo_sleep_not = "echo 0 > /sys/class/tty/ttyGSD5xp0/device/sleep";
		const char *echo_awake = "cat /sys/class/tty/ttyGSD5xp0/device/awake";

		//Go to sleep
		system(echo_sleep);
		//Check if asleep
		fp = popen(echo_awake, "r");
		if(!fp) {
			ASSERT_TRUE(false) << "Unable to open awake pipe";
		}
		fscanf(fp, "%d", &res);
		if (fp) {
			pclose(fp);
		}
		if (res == 0) {
			gps_rtc_test();
		}
		//Settle down
		sleep(2);
		//Wake up
		system(echo_sleep_not);
		//Check if awake
		fp = popen(echo_awake, "r");
		if(!fp) {
			ASSERT_TRUE(false) << "Unable to open awake pipe";
		}
		fscanf(fp, "%d", &res);

		EXPECT_TRUE(res == 1);

		pclose(fp);
	}
}

TEST_F(TTSYSTEMTest, test_ttsystem_gps_sleep_vs_system_sleep)
{

	//The idea behind this test is to verify that gps will not be woken up once the system is.
	if (is_gps_sleepable) {
		FILE *fp = NULL;
		int res = -1, res_wakeup = 0;
		const char *echo_sleep = "echo 1 > /sys/class/tty/ttyGSD5xp0/device/sleep";
		const char *echo_sleep_not = "echo 0 > /sys/class/tty/ttyGSD5xp0/device/sleep";
		const char *echo_awake = "cat /sys/class/tty/ttyGSD5xp0/device/awake";

		//Go to sleep
		system(echo_sleep);
		//Check if asleep
		fp = popen(echo_awake, "r");
		if(!fp) {
			ASSERT_TRUE(false) << "Unable to open awake pipe";
		}
		fscanf(fp, "%d", &res);
		pclose(fp);
		if (res == 0) {
			res_wakeup = ttsystem_sleep(3000);
		}
		//Settle down
		sleep(1);

		//Check if awake
		fp = popen(echo_awake, "r");
		if(!fp) {
			ASSERT_TRUE(false) << "Unable to open awake pipe";
		}
		fscanf(fp, "%d", &res);

		EXPECT_TRUE(res == 0);

		pclose(fp);
	}


}

void TTSYSTEMTest::gps_rtc_test()
{

	const int buff_size = 32;
	const int time_increment = 30;

	time_t timer, timer_new;
	tm *tm_timer;
	char buff[buff_size] = {0};

	time(&timer);
	tm_timer = gmtime(&timer);
	tm_timer->tm_year += 1900;
	tm_timer->tm_mon += 1;
	//%Y-%m-%d %H:%M:%S
	sprintf(buff, "%04d-%02d-%02d %02d:%02d:%02d", tm_timer->tm_year, tm_timer->tm_mon, tm_timer->tm_mday, tm_timer->tm_hour, tm_timer->tm_min, tm_timer->tm_sec );
	printf("%-16s: %s\n", "Current time: ", buff);

	timer += time_increment;
	tm_timer = gmtime(&timer);
	tm_timer->tm_year += 1900;
	tm_timer->tm_mon += 1;
	memset(buff, 0, buff_size);
	sprintf(buff, "%04d-%02d-%02d %02d:%02d:%02d", tm_timer->tm_year, tm_timer->tm_mon, tm_timer->tm_mday, tm_timer->tm_hour, tm_timer->tm_min, tm_timer->tm_sec );
	printf("%-16s: %s\n", "Time to set: ", buff);

	//Revert timer for later comparison
	timer -= time_increment;
	ttsystem_set_rtc(const_cast<const char*> (buff));
	//Wait to settle down
	sleep(2);

	time(&timer_new);
	tm_timer = gmtime(&timer_new);
	tm_timer->tm_year += 1900;
	tm_timer->tm_mon += 1;
	memset(buff, 0, buff_size);
	sprintf(buff, "%04d-%02d-%02d %02d:%02d:%02d", tm_timer->tm_year, tm_timer->tm_mon, tm_timer->tm_mday, tm_timer->tm_hour, tm_timer->tm_min, tm_timer->tm_sec );
	printf("%-16s: %s\n", "Time newly set: ", buff);
	//Expected
	EXPECT_TRUE((timer_new-timer) >= time_increment);
	//Something can go wrong and the gsd date can be set to some unusual value.
	//10s is about right margin.
	EXPECT_TRUE((timer_new-timer) <= time_increment+10);
}

// Test ttv_get_device_serial()
TEST_F(TTSYSTEMTest, test_ttv_get_device_serial)
{
	int buffsize = 256;
	char buff[buffsize];		// eg "NC4504A00060"
	EXPECT_EQ(0, ttv_get_device_serial(buff, buffsize)) << "Device serial failed";
	printf("ttv_get_device_serial() returned >%s<\n", buff);

	EXPECT_EQ(12, strlen(buff))    << "Length of the device serial number not as expected";

	EXPECT_EQ('N', buff[0])        << "First character should be 'N'";
	EXPECT_EQ('C', buff[1])        << "Second character should be 'C'";
	EXPECT_TRUE(isdigit(buff[2]))  << "char 3 should be a digit";
	EXPECT_TRUE(isdigit(buff[3]))  << "char 4 should be a digit";
	EXPECT_TRUE(isdigit(buff[4]))  << "char 5 should be a digit";
	EXPECT_TRUE(isdigit(buff[5]))  << "char 6 should be a digit";
	EXPECT_EQ('A', buff[6])        << "Seventh character should be 'A'";
	EXPECT_TRUE(isdigit(buff[7]))  << "char 8 should be a digit";
	EXPECT_TRUE(isdigit(buff[8]))  << "char 9 should be a digit";
	EXPECT_TRUE(isdigit(buff[9]))  << "char 10 should be a digit";
	EXPECT_TRUE(isdigit(buff[10])) << "char 11 should be a digit";
	EXPECT_TRUE(isdigit(buff[11])) << "char 12 should be a digit";
}

// Test void ttsystem_beep_enabled(void)
TEST_F(TTSYSTEMTest, beep_enabled)
{
	ttsystem_beep_enabled(false);
	ttsystem_beep_enabled(true);
	ttsystem_beep_enabled(false);
}

// Test bool ttsystem_is_sdcard_present(void)
// Returns: True if SD card is present
TEST_F(TTSYSTEMTest, is_sdcard_present)
{
	EXPECT_TRUE(ttsystem_is_sdcard_present()) << "Expected SD card to be present";
}

// Test int ttsystem_get_sdcard_status(void)
// Returns: 0 SD card available, 1 SD card not present, 2 SD card failed to mount, 3 SD card readonly
TEST_F(TTSYSTEMTest, get_sdcard_status)
{
	EXPECT_EQ(0, ttsystem_get_sdcard_status()) << "Expected SD card to be available";
}

// Test int ttsystem_check_sdcard(bool repair)
// repair:   true = enable the automatic error repairing
//           false = no repair, only check for errors
// Returns:
// 0      on success (no filesystem errors found or repair successfully completed)
// -errno on system failure (device not present or mount/unmount failure)
// 1      no valid FAT or exFAT filesystem found
// 2      error invoking external fsck tool by the API
// 3      filesystem errors found
// 4      the repair has not been completed successfully. Device might still contain errors
TEST_F(TTSYSTEMTest, check_sdcard)
{
	int status;
	int tries = 0;
	int const sameretries = 4;
	int const maxtries = 4;

	// Verify that calling "ttsystem_check_sdcard(false)" multiple times does not change the result
	status = ttsystem_check_sdcard(false);
	for (tries = 0; tries < sameretries; tries++) {
		printf("Test %d: SD Card status is %d\n", tries, status);
		EXPECT_EQ(status, ttsystem_check_sdcard(false)) << "Multiple calls to check_sdcard(false) should return the same result (%d)", tries;
	}
#if 0	// [LBP-2159] do not run ttsystem_check_sdcard(true) on a live filesystem
	do {
		status = ttsystem_check_sdcard(false);
		if (status != 0) {
			printf("SD Card status is %d: Attempting repair\n", status);
			status = ttsystem_check_sdcard(true);
			tries++;
		}
		printf("SD Card status is %d, tries = %d\n", status, tries);
	} while (status != 0 and tries < maxtries);
	EXPECT_EQ(0, status) << "Expected SD card to not have errors";
#endif
}


// Test int ttsystem_gps_is_awake(int* is_awake)
//   is_awake : if the value returned is 0, the GSD is asleep. If set to 1, it is awake.
// Returns:
//   0              on success
//   -1 or -errno   on failure
TEST_F(TTSYSTEMTest, gps_awake)
{
	int awake, originalstate;

	ttsystem_gps_is_awake(&originalstate);
	printf("gps initial status is %d\n", originalstate);

	EXPECT_EQ(0, ttsystem_gps_put_to_sleep(true)) << "GPS should go to sleep";
	sleep(1);
	EXPECT_EQ(0, ttsystem_gps_is_awake(&awake));
	printf("gps awake status is %d\n", awake);
	EXPECT_EQ(0, awake);

	EXPECT_EQ(0, ttsystem_gps_put_to_sleep(false)) << "GPS should wake up";
	sleep(1);
	EXPECT_EQ(0, ttsystem_gps_is_awake(&awake));
	printf("gps awake status is %d\n", awake);
	EXPECT_EQ(1, awake);

	ttsystem_gps_put_to_sleep(originalstate == 0);		// 1 == awake, true => go to sleep
	sleep(1);
	EXPECT_EQ(0, ttsystem_gps_is_awake(&awake));
	printf("gps restore status to %d\n", awake);
}

TEST_F(TTSYSTEMTest, DISABLED_test_ttsystem_app_started)
{
	bool btstatus;

	btstatus = TTUtils::command("systemctl status gpsd.service | grep Active:", "running");
	EXPECT_FALSE(btstatus) << "Expected gpsd.service to be NOT running";

	ttsystem_app_started();
	btstatus = TTUtils::command("systemctl status gpsd.service | grep Active:", "running");
	EXPECT_TRUE(btstatus) << "Expected gpsd.service service to be running";
}

TEST_F(TTSYSTEMTest, test_ttsystem_factory_reset_counter)
{
	EXPECT_LT(0, ttsystem_factory_reset_counter()) << "ttsystem_factory_reset_counter returned invalid value.";
}

TEST_F(TTSYSTEMTest, test_ttsystem_get_sgee_info)
{
	ttsystem_sgee_navsys_t navsys = TTSYSTEM_SGEE_GPS;
	int32_t status;
	int32_t age;
	uint32_t pred_int;
	EXPECT_EQ(0, ttsystem_get_sgee_info(navsys, &status, &age, &pred_int)) << "ttsystem_get_sgee_info call failed.";
	EXPECT_EQ(0, status) << "Quick fix data should not be avaiable";
	//TODO: check for appropriate parameters value, if needed.
}


#endif // TTSYSTEM_TEST_H


