# Makefile for gtest
#
# Copyright (C) 2015 TomTom BV <http://www.tomtom.com/>

CROSS_COMPILE=arm-arago-linux-gnueabi-
CC=$(CROSS_COMPILE)gcc
LD=$(CROSS_COMPILE)ld
AR=$(CROSS_COMPILE)ar
STRIP= $(CROSS_COMPILE)strip

TOPDIR:=../../..
SYSTEMD_INSTALL_DIR = $(TOPDIR)/source/ext_tools/systemd/systemd-216_arm-arago-linux-gnueabi
SYSTEMD_DEV_DIR = $(SYSTEMD_INSTALL_DIR)-dev
GPSD_DEV_DIR = $(TOPDIR)/source/ext_tools/gpsd
CFLAGS= -I./ -I$(SYSTEMD_DEV_DIR)/usr/include/ -I$(GPSD_DEV_DIR)/

# This needs to be set because we are out of the main source dir so this variable is wrong
BUILD_TOOL_DIR=$(TOPDIR)/source/ti_tools/linux_devkit


ifeq ($(TT_LB_POWERMODE_OBSOLETE), YES)
CFLAGS += -DLONGBEACH_POWERMODE_OBSOLETE
endif

# gtest
GTEST_INCLUDES=-I$(TOPDIR)/source/ext_tools/gtest/gtest-1.7.0_arm-arago-linux-gnueabi/include
GTEST_LDFLAGS=-L$(TOPDIR)/source/ext_tools/gtest/gtest-1.7.0_arm-arago-linux-gnueabi -lgtest_main -lgtest -lpthread -lrt -lblkid

# libttsystem
LIBTTSYSTEM_INCLUDES=-I$(TOPDIR)/source/tt_tools/libttsystem
LIBTTSYSTEM_LDFLAGS=-L$(TOPDIR)/source/tt_tools/libttsystem -lttsystem -L$(TOPDIR)/source/ext_tools/libfdt -lfdt -L$(SYSTEMD_INSTALL_DIR)/usr/lib -lsystemd
TTSYSTEM_FLAGS=$(LIBTTSYSTEM_INCLUDES) $(LIBTTSYSTEM_LDFLAGS)

# libttv
MCFW_ROOT	:= $(TOPDIR)/source/ipnc_rdk/ipnc_mcfw/mcfw
TTDEVICE	:= -L$(TOPDIR)/source/tt_tools/deviceinfo -lttvdevice
TTSYSTEM	:= -L$(TOPDIR)/source/tt_tools/libttsystem -lttsystem
MCFW_LIBS	:= $(addprefix $(TOPDIR)/source/ipnc_rdk/ipnc_mcfw/build-PLATFORM_LIB/lib/, ipnc_rdk_mcfw_api.a ipnc_rdk_link_api.a ipnc_rdk_osa.a)
SYSLINK		:= $(TOPDIR)/source/ti_tools/syslink_2_21_02_10/packages/ti/syslink/lib/syslink.a_release
GLBCE		:= -L$(TOPDIR)/source/ipnc_rdk/ipnc_mcfw/mcfw/src_linux/links/alg/glbce/lib -lGlbceSupport_ti
TTV_FLAGS	:= -I$(MCFW_ROOT)/interfaces $(TTDEVICE) $(TTSYSTEM) $< $(MCFW_LIBS) $(SYSLINK) $(GLBCE)

# libttw
TTW_LDFLAGS=-L$(TOPDIR)/source/tt_tools/wifi_direct_api -lrt -lttw -lpthread
TTW_INCLUDES=-I$(TOPDIR)/source/tt_tools/wifi_direct_api -I$(TOPDIR)/source/tt_tools/wifi_direct_api/wpa_ctrl
TTW_FLAGS=$(TTW_INCLUDES) $(TTW_LDFLAGS)

GTEST_INCLUDE_FILES = $(wildcard ttsystem_test/*.h) $(wildcard ttw_test/*.h) $(wildcard ttv_test/*.h)

# gtest: ttsystem_test/ttsystem_test.h ttw_test/ttw_test.h
gtest:  $(GTEST_INCLUDE_FILES) gtest.cpp
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" $(CROSS_COMPILE)g++ gtest.cpp $(GTEST_INCLUDES) $(TTSYSTEM_FLAGS) $(TTW_FLAGS) $(TTV_FLAGS) $(GTEST_LDFLAGS) -o $@
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" $(STRIP) $@

### Overall targets:

build: gtest

# housecleaning
clean:
	$(RM) gtest

.PHONY: clean build

# EOF
