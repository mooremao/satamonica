#ifndef TTV_GET_FEATURE_TEST_H
#define TTV_GET_FEATURE_TEST_H

#include <gtest/gtest.h>

class TTVSetGetFeatureTest : public ::testing::Test
{
protected:
	void SetUp()
	{
		ttv_mode(TTV_MODE_NONE);
	}

	void TearDown()
	{
		ttv_mode(TTV_MODE_NONE);
	}
};

TEST_F(TTVSetGetFeatureTest, setFeatureModeNone)
{
	int val = 0;

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_SATURATION, val)) << "Successfully set TTV_IMG_SATURATION even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_BRIGHTNESS, val)) << "Successfully set TTV_IMG_BRIGHTNESS even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_CONTRAST, val)) << "Successfully set TTV_IMG_CONTRAST even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_HUE, val)) << "Successfully set TTV_IMG_HUE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_SHARPNESS, val)) << "Successfully set TTV_IMG_SHARPNESS even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AE, val)) << "Successfully set TTV_IMG_AE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AWB, val)) << "Successfully set TTV_IMG_AWB even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_WB_MODE, val)) << "Successfully set TTV_IMG_WB_MODE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AE_METERING, val)) << "Successfully set TTV_IMG_AE_METERING even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_EV_COMPENSATION, val)) << "Successfully set TTV_IMG_EV_COMPENSATION even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AE_TIME, val)) << "Successfully set TTV_IMG_AE_TIME even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_set_feature(TTV_FEATURE_MAX, val)) << "Successfully set TTV_FEATURE_MAX even the TTV_MODE_NONE is set";
}

TEST_F(TTVSetGetFeatureTest, getFeatureModeNone)
{
	int val = 0;

	EXPECT_FALSE(ttv_get_feature(TTV_IMG_SATURATION, &val)) << "Successfully get TTV_IMG_SATURATION even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_BRIGHTNESS, &val)) << "Successfully get TTV_IMG_BRIGHTNESS even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_CONTRAST, &val)) << "Successfully get TTV_IMG_CONTRAST even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_HUE, &val)) << "Successfully get TTV_IMG_HUE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_SHARPNESS, &val)) << "Successfully get TTV_IMG_SHARPNESS even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AE, &val)) << "Successfully get TTV_IMG_AE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AWB, &val)) << "Successfully get TTV_IMG_AWB even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Successfully get TTV_IMG_WB_MODE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Successfully get TTV_IMG_AE_METERING even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, &val)) << "Successfully get TTV_IMG_EV_COMPENSATION even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AE_TIME, &val)) << "Successfully get TTV_IMG_AE_TIME even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_FEATURE_MAX, &val)) << "Successfully get TTV_FEATURE_MAX even the TTV_MODE_NONE is set";
}

TEST_F(TTVSetGetFeatureTest, getFeatureValueNull)
{
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_SATURATION, NULL)) << "Successfully get TTV_IMG_SATURATION even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_BRIGHTNESS, NULL)) << "Successfully get TTV_IMG_BRIGHTNESS even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_CONTRAST, NULL)) << "Successfully get TTV_IMG_CONTRAST even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_HUE, NULL)) << "Successfully get TTV_IMG_HUE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_SHARPNESS, NULL)) << "Successfully get TTV_IMG_SHARPNESS even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AE, NULL)) << "Successfully get TTV_IMG_AE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AWB, NULL)) << "Successfully get TTV_IMG_AWB even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_WB_MODE, NULL)) << "Successfully get TTV_IMG_WB_MODE even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AE_METERING, NULL)) << "Successfully get TTV_IMG_AE_METERING even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, NULL)) << "Successfully get TTV_IMG_EV_COMPENSATION even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_IMG_AE_TIME, NULL)) << "Successfully get TTV_IMG_AE_TIME even the TTV_MODE_NONE is set";
	EXPECT_FALSE(ttv_get_feature(TTV_FEATURE_MAX, NULL)) << "Successfully get TTV_FEATURE_MAX even the TTV_MODE_NONE is set";
}

TEST_F(TTVSetGetFeatureTest, getFeatureDefaultValues)
{
	int val = 0;

	ASSERT_EQ(1U, ttv_mode(TTV_MODE_VIDEO));

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SATURATION, &val)) << "Failed to get TTV_IMG_SATURATION";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_SATURATION is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_BRIGHTNESS, &val)) << "Failed to get TTV_IMG_BRIGHTNESS";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_BRIGHTNESS is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_CONTRAST, &val)) << "Failed to get TTV_IMG_CONTRAST";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_CONTRAST is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_HUE, &val)) << "Failed to get TTV_IMG_HUE";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_HUE is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SHARPNESS, &val)) << "Failed to get TTV_IMG_SHARPNESS";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_SHARPNESS is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE, &val)) << "Failed to get TTV_IMG_AE";
	EXPECT_EQ(1, val) << "Default value for TTV_IMG_AE is " << val << " and it should be 1";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AWB, &val)) << "Failed to get TTV_IMG_AWB";
	EXPECT_EQ(1, val) << "Default value for TTV_IMG_AWB is " << val << " and it should be 1";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_AUTO, val) << "Default value for TTV_IMG_WB_MODE is " << val << " and it should be " << TTV_WB_AUTO;

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Failed to get TTV_IMG_AE_METERING";
	EXPECT_EQ(TTV_AE_CENTER, val) << "Default value for TTV_IMG_AE_METERING is " << val << " and it should be " << TTV_AE_CENTER;

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, &val)) << "Failed to get TTV_IMG_EV_COMPENSATION";
	EXPECT_EQ(0, val) << "Default value for TTV_IMG_EV_COMPENSATION is " << val << " and it should be 0";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_TIME, &val)) << "Failed to get TTV_IMG_AE_TIME";
	EXPECT_EQ(100, val) << "Default value for TTV_IMG_AE_TIME is " << val << " and it should be 100";
}

TEST_F(TTVSetGetFeatureTest, setFeatureInvalidFeature)
{
	int val = 0;

	ASSERT_EQ(1U, ttv_mode(TTV_MODE_VIDEO));

	// Try to set invalid feature ...
	EXPECT_FALSE(ttv_get_feature(TTV_FEATURE_MAX, &val)) << "Successfully set TTV_FEATURE_MAX";

	// ... and after that attempt fail, we should still have default values for all features
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SATURATION, &val)) << "Failed to get TTV_IMG_SATURATION";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_SATURATION is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_BRIGHTNESS, &val)) << "Failed to get TTV_IMG_BRIGHTNESS";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_BRIGHTNESS is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_CONTRAST, &val)) << "Failed to get TTV_IMG_CONTRAST";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_CONTRAST is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_HUE, &val)) << "Failed to get TTV_IMG_HUE";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_HUE is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SHARPNESS, &val)) << "Failed to get TTV_IMG_SHARPNESS";
	EXPECT_EQ(128, val) << "Default value for TTV_IMG_SHARPNESS is " << val << " and it should be 128";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE, &val)) << "Failed to get TTV_IMG_AE";
	EXPECT_EQ(1, val) << "Default value for TTV_IMG_AE is " << val << " and it should be 1";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AWB, &val)) << "Failed to get TTV_IMG_AWB";
	EXPECT_EQ(1, val) << "Default value for TTV_IMG_AWB is " << val << " and it should be 1";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_AUTO, val) << "Default value for TTV_IMG_WB_MODE is " << val << " and it should be " << TTV_WB_AUTO;

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Failed to get TTV_IMG_AE_METERING";
	EXPECT_EQ(TTV_AE_CENTER, val) << "Default value for TTV_IMG_AE_METERING is " << val << " and it should be " << TTV_AE_CENTER;

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, &val)) << "Failed to get TTV_IMG_EV_COMPENSATION";
	EXPECT_EQ(0, val) << "Default value for TTV_IMG_EV_COMPENSATION is " << val << " and it should be 0";

	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_TIME, &val)) << "Failed to get TTV_IMG_AE_TIME";
	EXPECT_EQ(100, val) << "Default value for TTV_IMG_AE_TIME is " << val << " and it should be 100";
}

TEST_F(TTVSetGetFeatureTest, setFeatureInvalidValues)
{
	int val = 0;

	ASSERT_EQ(1U, ttv_mode(TTV_MODE_VIDEO));

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_SATURATION, -1)) << "Successfully set TTV_IMG_SATURATION to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SATURATION, &val)) << "Failed to get TTV_IMG_SATURATION";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_SATURATION is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_SATURATION, 256)) << "Successfully set TTV_IMG_SATURATION to 256";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SATURATION, &val)) << "Failed to get TTV_IMG_SATURATION";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_SATURATION is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_BRIGHTNESS, -1)) << "Successfully set TTV_IMG_BRIGHTNESS to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_BRIGHTNESS, &val)) << "Failed to get TTV_IMG_BRIGHTNESS";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_BRIGHTNESS is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_BRIGHTNESS, 256)) << "Successfully set TTV_IMG_BRIGHTNESS to 256";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_BRIGHTNESS, &val)) << "Failed to get TTV_IMG_BRIGHTNESS";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_BRIGHTNESS is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_CONTRAST, -1)) << "Successfully set TTV_IMG_CONTRAST to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_CONTRAST, &val)) << "Failed to get TTV_IMG_CONTRAST";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_CONTRAST is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_CONTRAST, 256)) << "Successfully set TTV_IMG_CONTRAST to 256";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_CONTRAST, &val)) << "Failed to get TTV_IMG_CONTRAST";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_BRIGHTNESS is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_HUE, -1)) << "Successfully set TTV_IMG_HUE to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_HUE, &val)) << "Failed to get TTV_IMG_HUE";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_HUE is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_HUE, 256)) << "Successfully set TTV_IMG_HUE to 256";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_HUE, &val)) << "Failed to get TTV_IMG_HUE";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_HUE is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_SHARPNESS, -1)) << "Successfully set TTV_IMG_SHARPNESS to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SHARPNESS, &val)) << "Failed to get TTV_IMG_SHARPNESS";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_SHARPNESS is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_SHARPNESS, 256)) << "Successfully set TTV_IMG_SHARPNESS to 256";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SHARPNESS, &val)) << "Failed to get TTV_IMG_SHARPNESS";
	EXPECT_EQ(128, val) << "Current value for TTV_IMG_SHARPNESS is " << val << " and it should be 128";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_WB_MODE, -1)) << "Successfully set TTV_IMG_WB_MODE to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_AUTO, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_AUTO";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_MAX)) << "Successfully set TTV_IMG_WB_MODE to TTV_WB_MAX";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_AUTO, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_AUTO";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AE_METERING, -1)) << "Successfully set TTV_IMG_AE_METERING to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Failed to get TTV_IMG_AE_METERING";
	EXPECT_EQ(TTV_AE_CENTER, val) << "Current value for TTV_IMG_AE_METERING is " << val << " and it should be TTV_AE_CENTER";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AE_METERING, TTV_AE_MAX)) << "Successfully set TTV_IMG_AE_METERING to TTV_AE_MAX";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Failed to get TTV_IMG_AE_METERING";
	EXPECT_EQ(TTV_AE_CENTER, val) << "Current value for TTV_IMG_AE_METERING is " << val << " and it should be TTV_AE_CENTER";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_EV_COMPENSATION, -201)) << "Successfully set TTV_IMG_EV_COMPENSATION to -201";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, &val)) << "Failed to get TTV_IMG_EV_COMPENSATION";
	EXPECT_EQ(0, val) << "Current value for TTV_IMG_EV_COMPENSATION is " << val << " and it should be 0";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_EV_COMPENSATION, 201)) << "Successfully set TTV_IMG_EV_COMPENSATION to 201";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, &val)) << "Failed to get TTV_IMG_EV_COMPENSATION";
	EXPECT_EQ(0, val) << "Current value for TTV_IMG_EV_COMPENSATION is " << val << " and it should be 0";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AE_TIME, -1)) << "Successfully set TTV_IMG_AE_TIME to -1";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_TIME, &val)) << "Failed to get TTV_IMG_AE_TIME";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_AE_TIME is " << val << " and it should be 100";

	EXPECT_FALSE(ttv_set_feature(TTV_IMG_AE_TIME, 8001)) << "Successfully set TTV_IMG_AE_TIME to 8001";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_TIME, &val)) << "Failed to get TTV_IMG_AE_TIME";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_AE_TIME is " << val << " and it should be 100";
}

TEST_F(TTVSetGetFeatureTest, setgetFeatureCustomValues)
{
	int val = 0;

	ASSERT_EQ(1U, ttv_mode(TTV_MODE_VIDEO));

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_SATURATION, 100)) << "Failed to set TTV_IMG_SATURATION to 100";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SATURATION, &val)) << "Failed to get TTV_IMG_SATURATION";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_SATURATION is " << val << " and it should be 100";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_BRIGHTNESS, 100)) << "Failed to set TTV_IMG_BRIGHTNESS to 100";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_BRIGHTNESS, &val)) << "Failed to get TTV_IMG_BRIGHTNESS";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_BRIGHTNESS is " << val << " and it should be 100";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_CONTRAST, 100)) << "Failed to set TTV_IMG_CONTRAST to 100";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_CONTRAST, &val)) << "Failed to get TTV_IMG_CONTRAST";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_CONTRAST is " << val << " and it should be 100";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_HUE, 100)) << "Failed to set TTV_IMG_HUE to 100";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_HUE, &val)) << "Failed to get TTV_IMG_HUE";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_HUE is " << val << " and it should be 100";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_SHARPNESS, 100)) << "Failed to set TTV_IMG_SHARPNESS to 100";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_SHARPNESS, &val)) << "Failed to get TTV_IMG_SHARPNESS";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_SHARPNESS is " << val << " and it should be 100";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_AE, 0)) << "Failed to set TTV_IMG_AE to 0";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE, &val)) << "Failed to get TTV_IMG_AE";
	EXPECT_EQ(0, val) << "Current value for TTV_IMG_AE is " << val << " and it should be 0";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_AWB, 0)) << "Failed to set TTV_IMG_AWB to 0";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AWB, &val)) << "Failed to get TTV_IMG_AWB";
	EXPECT_EQ(0, val) << "Current value for TTV_IMG_AWB is " << val << " and it should be 0";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_7500K)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_7500K";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_7500K, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_7500K";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_5000K)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_5000K";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_5000K, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_5000K";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_4000K)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_4000K";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_4000K, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_4000K";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_3000K)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_3000K";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_3000K, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_3000K";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_2000K)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_2000K";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_2000K, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_2000K";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_RED_FILTER)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_RED_FILTER";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_RED_FILTER, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_RED_FILTER";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_MAGENTA_FILTER)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_MAGENTA_FILTER";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_MAGENTA_FILTER, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_MAGENTA_FILTER";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_WB_MODE, TTV_WB_AUTO)) << "Failed to set TTV_IMG_WB_MODE to TTV_WB_AUTO";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_WB_MODE, &val)) << "Failed to get TTV_IMG_WB_MODE";
	EXPECT_EQ(TTV_WB_AUTO, val) << "Current value for TTV_IMG_WB_MODE is " << val << " and it should be TTV_WB_AUTO";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_AE_METERING, TTV_AE_NORMAL)) << "Failed to set TTV_IMG_AE_METERING to TTV_AE_NORMAL";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Failed to get TTV_IMG_AE_METERING";
	EXPECT_EQ(TTV_AE_NORMAL, val) << "Current value for TTV_IMG_AE_METERING is " << val << " and it should be TTV_AE_NORMAL";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_AE_METERING, TTV_AE_WIDE)) << "Failed to set TTV_IMG_AE_METERING to TTV_AE_WIDE";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Failed to get TTV_IMG_AE_METERING";
	EXPECT_EQ(TTV_AE_WIDE, val) << "Current value for TTV_IMG_AE_METERING is " << val << " and it should be TTV_AE_WIDE";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_AE_METERING, TTV_AE_CENTER)) << "Failed to set TTV_IMG_AE_METERING to TTV_AE_CENTER";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_METERING, &val)) << "Failed to get TTV_IMG_AE_METERING";
	EXPECT_EQ(TTV_AE_CENTER, val) << "Current value for TTV_IMG_AE_METERING is " << val << " and it should be TTV_AE_CENTER";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_EV_COMPENSATION, 100)) << "Failed to set TTV_IMG_EV_COMPENSATION to 100";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, &val)) << "Failed to get TTV_IMG_EV_COMPENSATION";
	EXPECT_EQ(100, val) << "Current value for TTV_IMG_EV_COMPENSATION is " << val << " and it should be 100";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_EV_COMPENSATION, -100)) << "Failed to set TTV_IMG_EV_COMPENSATION to -100";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_EV_COMPENSATION, &val)) << "Failed to get TTV_IMG_EV_COMPENSATION";
	EXPECT_EQ(-100, val) << "Current value for TTV_IMG_EV_COMPENSATION is " << val << " and it should be -100";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_AE_TIME, 0)) << "Failed to set TTV_IMG_AE_TIME to 0";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_TIME, &val)) << "Failed to get TTV_IMG_AE_TIME";
	EXPECT_EQ(0, val) << "Current value for TTV_IMG_AE_TIME is " << val << " and it should be 0";

	EXPECT_TRUE(ttv_set_feature(TTV_IMG_AE_TIME, 8000)) << "Failed to set TTV_IMG_AE_TIME to 8000";
	EXPECT_TRUE(ttv_get_feature(TTV_IMG_AE_TIME, &val)) << "Failed to get TTV_IMG_AE_TIME";
	EXPECT_EQ(8000, val) << "Current value for TTV_IMG_AE_TIME is " << val << " and it should be 8000";
}

#endif //TTV_GET_FEATURE_TEST_H
