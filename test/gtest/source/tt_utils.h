/*
 * tt_utils.h
 *
 *  Created on: Oct 1, 2015
 *      Author: goran
 */

#ifndef TT_UTILS_H_
#define TT_UTILS_H_

#include <gtest/gtest.h>
#include "ttsystem.h"

#include<cstdlib>
#include<sstream>


// set DEBUG to true to enable debug printing
#define DEBUG false

#if DEBUG
#define printd(...) do { printf(__VA_ARGS__); } while (0)
#else
#define printd(format, args...)
#endif

class TTUtils{
public:
	static bool command(const char* command, const char* expect) {
		FILE* fp;
		char buffer[200];		// shell command stdout buffer
		char *s;

		fp = popen(command, "r");
			s = fgets(buffer, sizeof(buffer)-1, fp);
			pclose(fp);

		if (s) {
			// remove trailing crlf from string
			while ((strlen(s) > 0) && (s[strlen(s)-1] == '\r') || (s[strlen(s)-1] == '\n')) {
				s[strlen(s)-1] = 0;
			}
			printd("Command returned '%s'\n", s);
			if (strstr(s, expect) != NULL) {
				printd("'%s' found in '%s'\n", expect, s);
				return true;
			} else {
				printd("'%s' NOT found in '%s'\n", expect, s);
			}
		} else {
			printd("s is NULL");
		}
		return false;
	}
};



#endif /* TT_UTILS_H_ */
