import sys
import os
import ConfigParser
import getopt
import tarfile
import iptools
import traceback


sys.path.insert(0, "../longbeach_utf/deployment")
from deploy_lib import sendToRemote, fetchFromRemote, runRemoteCommand, runSystemCommand, deviceUp,\
    waitNTPd, RemoteException, rebootAndWait

sys.path.insert(0, "../longbeach_utf/clients")
from utils import reset_failed_camera, reset_reboot_counter
import recoveryMethods


def sendTestFile(deploymentDir, testFilePath, devAddress, devTestRootDir, gTestsFile):

    if not os.path.isfile(testFilePath):
        print("[ERROR] '{0}' does not exist. Did it fail to compile?\n".format(testFilePath))
        sys.exit()

    maxRetries = 2
    for retry in range(1, maxRetries+1):
        print("[INFO] Sending GTest file '{0}'".format(testFilePath))
        try:
            sendToRemote(devAddress, testFilePath, devTestRootDir)
            break
        except RemoteException as e:
            print("[INFO] Attempting repair of Read-Only fileystem {0} of {1}".format(retry, maxRetries))
            os.chdir("../longbeach_utf/deployment/")
            recoveryMethods.remountPartition(devAddress)
            os.chdir(deploymentDir)
            if retry==maxRetries:
                print("[ERROR] Max retries {0} reached. Aborting tests".format(maxRetries))
                sys.exit()     # Read-only file system?

    print("Making '{0}gtests/{1}' executable on the camera".format(devTestRootDir, gTestsFile))
    runRemoteCommand("cd {0} && rm -rf ./gtests && mkdir gtests && mv {1} gtests/ && chmod o+x gtests/{1}".format(devTestRootDir, gTestsFile), devAddress)


def runGTestCases(gTestFile, testsFilter, devAddress, devTestRootDir, devLogsDir):
    runRemoteCommand("rm -rf {0}; mkdir -p {0}/{1}".format(devLogsDir, gTestFile), devAddress)
    print("[INFO] Stopping the camera-app before running Google Test Framework")

    reset_failed_camera(devAddress)
    reset_reboot_counter(devAddress)
    runRemoteCommand("systemctl stop camera-app", devAddress)

    runRemoteCommand("mkdir -p {0}/{1}".format(devLogsDir, gTestFile), devAddress)
    cmd = "{0}/gtests/{1} --gtest_filter='{3}' --gtest_output=xml:{2}/{1}/{1}.xml 2> {2}/{1}/{1}.stderr.log | tee {2}/{1}/{1}.stdout.log"\
       .format(devTestRootDir, gTestFile, devLogsDir, testsFilter)

    try:
        runRemoteCommand(cmd, devAddress)

        print("[INFO] Returned from GTF")
        print("[INFO] Starting the camera-app after running Google Test Framework")

        runRemoteCommand("systemctl reset-failed camera-app.service", devAddress)
        runRemoteCommand("systemctl start camera-app", devAddress)
    except RemoteException as ex:
        err = "".join(ex.args)
        print("'{0}' exception occurred: >{1}<".format(type(ex).__name__, err))
        print traceback.print_stack()


def retrieveLogs(gTestFile, destDir, devAddress, devLogsDir):
    try:
        destDir = "{0}/{1}".format(destDir, gTestFile)
        devLogsDir = "{0}/{1}".format(devLogsDir, gTestFile)

        runSystemCommand("mkdir -p {0}; rm -rf {0}/*".format(destDir))

        print("Pull logs from '{0} to '{1}'".format(devLogsDir, destDir))
        fetchFromRemote(devAddress, "{0}/*".format(devLogsDir), destDir)
    except RemoteException as ex:
        err = "".join(ex.args).strip()
        print("'{0}' exception occurred at retrieveLogs(): >{1}<".format(type(ex).__name__, err))


def parseTestsFilter(filterFileName):

    if filterFileName is None:
        return "*"

    filterFile = open(filterFileName, "r")
    filterFileLines = filterFile.readlines()

    filterString = ""

    filterOutList = list()

    for line in filterFileLines :
        if line.strip().startswith("#") or not line.strip() :
            continue
        if line.strip().startswith("-") :
            filterOutList.append(line.strip("-").rstrip())
            continue
        filterString += ":" + line.strip()

    if len(filterOutList) > 0 :
        filterString += ":-"
        for stringOut in filterOutList :
            filterString += stringOut + ":"
        filterString = filterString.rstrip(':')

    filterFile.close()

    return filterString


def main():

    configFile = "GTests.cfg"
    cameraIP = ""                   # default camera ip address not set

    opts, args = getopt.getopt(sys.argv[1:], "hgc:t:n:i:", ["config=", "test=", "camera-ip="])
    gtest = False
    testCount = 1
    testGroupFile = None
    for opt, arg in opts:
        if opt == "-h":
            print "USAGE: python runGTests.py [-c <config>] [-t <test_group_file>]  [-n <count>] [--camera-ip <camera_ip_address>]"
            sys.exit()
        elif opt in ("-c", "--config"):
            configFile = arg
        elif opt in ("-t", "--test"):
            testGroupFile = arg
        elif opt in ("-n"):
            testCount = int(arg)
        elif opt in ("-i", "--camera-ip"):
            cameraIP = arg

    config = ConfigParser.ConfigParser()
    config.read(configFile)

    deploymentDir = os.path.dirname(os.path.realpath(__file__)) + "/"
    baseDir = "{0}/../../../".format(deploymentDir)
    devTestRootDir = config.get("CONFIG_DEVICE", "test_rootdir")
    devLogsDir = config.get("CONFIG_DEVICE", "logs_dir")

    if cameraIP == "":          # not defined by command line parameter ?
        devAddress = config.get("CONFIG_DEVICE", "address")
        locAddress = config.get("CONFIG_LOCAL", "address")
    else:
        devAddress = cameraIP
        locAddress = iptools.ipv4.long2ip(iptools.ipv4.ip2long(cameraIP)+1)

    localLogsDir = "{0}/{1}".format(deploymentDir, config.get("CONFIG_LOCAL", "logs_dir"))

    if not deviceUp(locAddress):
        print("[ERROR] Skipping everything! The device is not up")
        sys.exit()

    sendTestFile(deploymentDir, "source/gtest", devAddress, devTestRootDir, "gtest")

    print("[INFO] Rebooting the device to ensure tests have a consistent state")
    rebooted = rebootAndWait(120, devAddress, locAddress)
    if not rebooted:
        print("[ERROR] Skipping tests, the device is not up")
        sys.exit()

    runRemoteCommand("date -us '$(date -u +%Y.%m.%d-%T)'", devAddress)

    os.chdir(deploymentDir)
    waitNTPd(80, devAddress)

    testsFilter = parseTestsFilter(testGroupFile)

    for testRun in range(1, testCount+1):
        print("\nTest run {0} of {1}:".format(testRun, testCount))
        runGTestCases("gtest", testsFilter, devAddress, devTestRootDir, devLogsDir)

    retrieveLogs("gtest", localLogsDir, devAddress, devLogsDir)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt as e:
        print("Control-C from user: Aborting all tests")
