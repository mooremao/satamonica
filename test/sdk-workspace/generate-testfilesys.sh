#!/bin/bash
set -x
set -e

WORKSPACE_ROOT_DIR=$1

#When LBP_SDK is not set, we asume we need workspace paths instead.
if [ -z "$LBP_SDK" ]
then
    MTD_DIR=$WORKSPACE_ROOT_DIR/source/ipnc_rdk/target/mtd-utils
    LBP_SDK=$WORKSPACE_ROOT_DIR/source/ipnc_rdk/target/longbeach_sdk
    export MTD_DIR LBP_SDK
fi

cp -a $LBP_SDK/Makefile.workspace Makefile

#Prevent removing demoapp from SDK
export SKIP_CLEANUP_PLATFORM=YES

rm -rf target
make copyimages
make extractssystemfs
make installpy
make ubifs

