#! /usr/bin/env python
#
# Test whether device licenses are correct and valid
#
# Copyright (C) 2013 TomTom
#

# For longbeach, this expects 2 files in the current directory:
# release_open_sources.xml.gz and rootfs_chksum.md5 (retrieved from ).

import os
import glob
import gzip
# import lxml
import lxml.etree
# from lxml import etree

import xmlrunner
import types
import zipfile
import tarfile
import logging
import subprocess
import sys
import optparse

import fileinput
from fnmatch import fnmatch

import unittest

class LicensesTest(unittest.TestCase):
    """
    Verify that the device has the proper licenses for installed modules
    """
    # Constants
    RootfsImgFile = "data/rootfs.img"
    PlatformZipFile = "data/platform-*.zip"
    AndroidTgzFile = "data/androidfs-*.tgz"

    # In Jenkins, the files are extracted by different script, put in the cwd:
    LicenseXMLFile = "release_open_sources.xml.gz"

    NoticeGZipFile = "META/release_open_sources.xml.gz"
    LicenseAdditional = "license-additional"
    LicenseWhitelist  = "license-whitelist"

    rootfsEntries = None
    xmlEntries = None
    whitelistedFiles = []
    additionalFiles = []
    baseDir = None

    licenseAdditional = None
    licenseWhitelist = None

    @classmethod
    def getBaseDir(cls):
        if cls.baseDir == None:
            # figure out the source TOP
            base = None
            if os.getenv("ANDROID_BUILD_TOP"):
                file =  os.getenv("ANDROID_BUILD_TOP") + "/development/testrunner"
                if os.path.exists(file):
                    base = os.getenv("ANDROID_BUILD_TOP")
            else:
                file = "../../../../development/testrunner"
                if os.path.exists(file):
                    base = "../../../../"
                else:
                    file = "../../../../../development/testrunner"
                    if os.path.exists(file):
                        base = "../../../../../"
                    else:
                        # longbeach
                        base = "./"
            cls.baseDir = base
        return cls.baseDir

    @classmethod
    def processFileList(cls, content):
        if content == None:
            return []

        list = []
        for l in content.splitlines():
            l = l.strip();
            if len(l) == 0:
                continue
            # skip comments
            if l[0] != '#':
                list.append(l)
        return list

    @classmethod
    def readFileList(cls, filename):

        if cls.getBaseDir() == None:
            logging.error("Could not determine base directory.")
            return []

        fileToRead = "%s%s" % (cls.getBaseDir(), filename)
        if not glob.glob(fileToRead):
            logging.info("No file found at %s", fileToRead)
            return []

        list = cls.processFileList(open(fileToRead).read())
        fileinput.close()

        return list

    # Override this and DO NOT CALL SUPER.setupClass
    # then we can use -i device for our own purposes
    @classmethod
    def readAdditional(cls):
        if cls.licenseWhitelist != None or cls.licenseAdditional != None:
            # if one comes from the tgz/zip then the other as well
            cls.whitelistedFiles = cls.processFileList(cls.licenseWhitelist)
            cls.additionalFiles = cls.processFileList(cls.licenseAdditional)
        else:
            # The device is used as subdirectory in device/tomtom
            whiteListFile = "device/tomtom/%s/%s" % (opts.device, cls.LicenseWhitelist)
            additionalFile = "device/tomtom/%s/%s" % (opts.device, cls.LicenseAdditional)
            cls.whitelistedFiles = cls.readFileList(whiteListFile)
            cls.additionalFiles = cls.readFileList(additionalFile)

        for l in cls.whitelistedFiles:
            logging.info("whitelist %s" % l)

        for l in cls.additionalFiles:
            if '*' in l or '?' in l:
                logging.error("Wildcards not supported in additional file list.")
            logging.info("additional %s" % l)

    @classmethod
    def isWhitelisted(cls, filename):
        for w in cls.whitelistedFiles:
            if fnmatch(filename, w):
                return True
            base = os.path.basename(filename)
            if fnmatch(base, w):
                return True

        return False

    @classmethod
    def getFiles(cls, image, root):
        """ Get files from the supplied image recursively """
        # memoization: the parameter @root is disregarded in futher calls!
        if cls.rootfsEntries != None:
            return cls.rootfsEntries

        if not glob.glob(image):
            raise RuntimeError, "Cannot execute licenses test: no rootfs image found!"

        cls.rootfsEntries = cls.getFilesRecursive(image, root)
        logging.info("Read %d entries from rootfs" % len(cls.rootfsEntries))
        return cls.rootfsEntries

    @classmethod
    def getFilesRecursive(cls, image, root, parent='/'):
        """return the list of files in dirtree rooted at @parent"""

        # Initialize
        files = []

        # Retrieve file list for specified parent directory in image
        command = "e2ls -la -d %s %s" % (image, parent)
        output, error = subprocess.Popen(command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        if error:
            logging.error("Cannot retrieve files from %s for directory %s : %s" % (image, parent, error))
            return None

        # Iterate through the list recursively
        for entry in output.splitlines():
            filename, filetype = LicensesTest.getFileInfo(entry)

            # Look for real files (type=10) and directories (type=4), skip '.' and '..' paths
            if (filetype=="4") and (filename!=".") and (filename!=".."):
                # Found directory, add directory files recursively to list
                files.extend( LicensesTest.getFilesRecursive(image, root, os.path.join(parent, filename)) )
            elif (filetype=="10"):
                # Found file, add it to list
                files.append( root + os.path.join(parent, filename) )

        return files

    @staticmethod
    def getFileInfo(entry):
        """ Get the file name and type from e2ls result entry """
        # Check entry
        if (not entry) or (entry=="") or (entry.startswith(">")):
            return None, None

        # The info retrieved from the image by means of e2ls holds per line the file name (index=7) and the file attributes (index=1)
        info = entry.split()

        filename = info[7]
        filetype = info[1][:-4]

        return filename, filetype

    # populates  cls slots: noticefile licenseWhitelist licenseAdditional
    @classmethod
    def readNoticeFromZip(cls, zipFile):
        # Get the platform zip file
        platformzip = zipfile.ZipFile(glob.glob(zipFile)[0], 'r')

        # Get the notice file from the platform zip file
        for item in platformzip.infolist():
            if cls.NoticeGZipFile in item.filename:
                # Extract notice file folder to current working directory
                noticegzip = platformzip.extract(item)
                cls.noticefile = gzip.GzipFile(noticegzip, 'r').read()

            if item.filename.endswith(cls.LicenseAdditional):
                logging.info("Reading additional files from zip")
                cls.licenseAdditional = platformzip.read(item)

            if item.filename.endswith(cls.LicenseWhitelist):
                logging.info("Reading whitelist from zip")
                cls.licenseWhitelist = platformzip.read(item)

        if None != platformzip:
            platformzip.close()

# Strasbourg-only
    @classmethod
    def readNoticeFromTgz(cls, tgzFile):
        tgzfile = tarfile.open(name = glob.glob(tgzFile)[0])

        for item in tgzfile.getnames():
            if item.endswith(cls.NoticeGZipFile):
                logging.info("Reading notices from tgz")
                noticegzip = tgzfile.extractfile(item)
                cls.noticefile = gzip.GzipFile("", 'r', 1, noticegzip).read()
            if item.endswith(cls.LicenseAdditional):
                logging.info("Reading additional files from tgz")
                cls.licenseAdditional = tgzfile.extractfile(item).read()
            if item.endswith(cls.LicenseWhitelist):
                logging.info("Reading whitelist from tgz")
                cls.licenseWhitelist = tgzfile.extractfile(item).read()

        if None != tgzfile:
            tgzfile.close()

    # Memoization.
    # Returns the hash of filenames->xml node.
    @classmethod
    def readXmlFile(cls):
        if cls.xmlEntries != None:
            return

        cls.noticefile = None
        # cannot use information from the device class here, so we'll just hardcode it
        if opts.device == "am335x" or opts.device == "italia":
            if glob.glob(cls.PlatformZipFile):
                cls.readNoticeFromZip(cls.PlatformZipFile)
            else:
                raise RuntimeError, "Cannot find platform.zip"
        elif opts.device == "strasbourg":
            if glob.glob(cls.AndroidTgzFile):
                cls.readNoticeFromTgz(cls.AndroidTgzFile)
            else:
                raise RuntimeError, "Cannot find androidfs.tgz"
        elif opts.device == "longbeach":
            if glob.glob(cls.LicenseXMLFile):
                cls.noticefile = gzip.GzipFile(cls.LicenseXMLFile, 'r').read()
            else:
                raise RuntimeError, "Cannot find "+ cls.LicenseXMLFile
        else:
            raise RuntimeError, "Cannot execute licenses test: device %s not recognized" % opts.device

        if cls.noticefile != None:
            entries = {}
            for e in lxml.etree.fromstring(cls.noticefile, parser=lxml.etree.XMLParser(huge_tree=True)).getiterator("licenseInfo"):
                path = e.find("path").text
                entries[path] = e

            cls.xmlEntries = entries
            logging.info("Read %d entries from notices.xml" % len(cls.xmlEntries))
        else:
            raise RuntimeError, "No notice file found"


    @classmethod
    def getFilesFromMd5HashFile(cls, filename, prefix):
        """ Get files from the supplied md5 hash list"""
        files = []
        with open(filename) as f:
            for line in f:
                files.append(prefix + line[36:].rstrip('\n'))

        logging.info("Found %d filenames" % len(files))
        return files;


    @classmethod
    def getFilesList(cls):
        """(memoization) """
        if opts.device == "am335x" or opts.device == "italia" or opts.device == "strasbourg":
            return LicensesTest.getFiles( LicensesTest.RootfsImgFile, "/system");
        elif opts.device == "longbeach":
            # this prefix helps avoid filename conflicts, if more "namespaces"
            # (i.e. filenames) are included. The XML file is then assumed to be
            # post-processed by the App.
            if cls.rootfsEntries != None:
                return cls.rootfsEntries
            else:
                cls.rootfsEntries = cls.getFilesFromMd5HashFile("rootfs_chksum.md5", "ipnc_rdk/target/filesys/")
                cls.rootfsEntries.append("/uImage");
                cls.rootfsEntries.append("/u-boot");
                return cls.rootfsEntries
        else:
            raise RuntimeError, "Cannot execute licenses test: device %s not recognized" % opts.device

    @classmethod
    def setUpClass(cls):
        # mmc: with unittest.TestCase this does not work as it did with test.TestBase in NAV4
        # super(LicensesTest, cls).setUpClass('licenses')
        LicensesTest.readXmlFile

    def setUp(self):
        pass

    def noticeRequired(self, entry):
        type = entry.find("license/type").text
        version = entry.find("license/version").text

        if type == "APACHE":
            if version == "2.0":
                return True
        elif type == "BSD-STYLE":
            if version == "1.0":
                return True
        elif type == "GPL":
            if version == "2.0":
                return True
        elif type == "ICU":
            if version == "1.0":
                return True
        elif type == "LGPL":
            if version == "2.1":
                return True
        # not reported in the legal document, but has been validated by legal
        # see ESTRELLA-3683
        elif type == "MFI":
            if version == "1":
                return True
        elif type == "MIT":
            if version == "1.0":
                return True
        elif type == "MPL":
            if version == "1.1":
                return True
        elif type == "PROPRIETARY":
            return False
        elif type == "PUBLIC_DOMAIN":
            return False

        raise RuntimeError, "Unhandled license type " + type + " and version " + version

    def test0001_summary(self):
        """ Check that the system and kernel modules are recorded in the notice file
            The execution of this test is a pre-condition for the execution of the individual file license tests
        """

        # Initialize
        passed = 0
        failed = 0
        whitelisted = 0

        # Make sure that the notice file was retrieved
        self.assertTrue(self.noticefile!=None, "Could not get the notice file from %s" % self.NoticeGZipFile)

        # Verify the files in the rootfs image
        for filename in self.getFilesList():

            if self.isWhitelisted(filename):
                logging.info("Whitelisted: %s" % filename)
                whitelisted+=1
            else:
                # Verify notice
                if filename in self.noticefile:
                    passed+=1
                else:
                    logging.warning("License missing for:  %s" % filename)
                    failed+=1

        # Generate summary
        total = passed + failed + whitelisted
        logging.info("License verification: total=%d, passed=%d, failed=%d, whitelisted=%d" % (total, passed, failed, whitelisted))

        self.assertEqual(failed, 0, "Found %d files that have not been mentioned in licenses' notice" % failed)

    def test0002_licenseFile(self):
        """ Check that the notice file entries have valid license type and version """

        # Initialize
        failed = 0

        self.assertTrue(self.noticefile!=None, "Could not get the notice file from %s" % self.NoticeGZipFile)

        # Check licenses
        for path, entry in self.xmlEntries.iteritems():

            # skip whitelisted files if they are not in the file
            # HOWEVER if they *are* in the file then they have to be complete
            if entry.find("license/type") == None and self.isWhitelisted(path):
                continue

            self.assertIsNotNone(entry.find("license/type"))
            licenseType = entry.find("license/type").text

            self.assertIsNotNone(entry.find("license/version"))
            licenseVersion = entry.find("license/version").text

            licenseWarning = "License type and version missing" if (not licenseType) and (not licenseVersion) else (
                             "License type missing"             if (not licenseType)                          else (
                             "License version missing"          if (not licenseVersion)                       else (
                             None )))
            if licenseWarning:
                logging.warning("%s for %s" % (licenseWarning, path))
                failed+=1

        self.assertEqual(failed, 0, "Found %d files that have license type and/or version missing in licenses' notice" % failed)


    def assertLicenseFileEntry(self, pathToTest):
        """ Check that the notice file entries have valid license type and version """

        entry = self.xmlEntries[pathToTest]

        # this really means programmer error, but better make sure
        self.assertIsNotNone(entry, "Could not find entry for " + pathToTest)

        if entry.find("license/type") == None and self.isWhitelisted(pathToTest):
            return

        self.assertIsNotNone(entry.find("license/type"))
        licenseType = entry.find("license/type").text.strip()
        self.assertNotEqual(licenseType, "", "License type for %s is empty" % (pathToTest))

        self.assertIsNotNone(entry.find("license/version"))
        licenseVersion = entry.find("license/version").text.strip()
        self.assertNotEqual(licenseVersion, "", "License version for %s is empty" % (pathToTest))

        notice = entry.find("license/notice").text.strip()
        required = self.noticeRequired(entry)
        # if it is required then there has to be text
        # if it is not required then there may be text
        if required:
            self.assertNotEquals(notice, "", "Required notice (%s %s) for %s is empty" % (licenseType, licenseVersion, pathToTest))

    def assertNotice(self, filename):
        """ Check that there is a license info in the xml file for filename """
        if self.isWhitelisted(filename):
            return

        self.assertIn(filename, self.xmlEntries, "%s not mentioned in notices file" % filename)



    def assertFileEntry(self, filename):
        """ Check that there is a file in the rootfs for an xml entry """

        # static libraries are not in the rootfs image
        # this cannot be handled with whitelisting
        if not filename.endswith(".a"):
            if (self.additionalFiles != None) and (filename in self.additionalFiles):
                # and test again:
                self.assertTrue(filename in self.additionalFiles,  "xml entry %s not in rootfs image" % (filename))
            else:
                self.assertTrue(filename in self.getFilesList(),  "xml entry %s not in rootfs image" % (filename))

    def tearDown(self):
        pass



##### Test main #####

if __name__ == '__main__':

    # Configure logging
    logroot = logging.getLogger()
    logroot.setLevel(logging.INFO)
    logroot.addHandler(logging.StreamHandler(sys.stdout))

    LicensesTest.options = optparse.OptionParser()
    LicensesTest.options.add_option("-i", "--device-name", dest="device", action="store", default=None, help="Specify target device type")
    LicensesTest.options.add_option("-x", "--xml", dest="xml", action="store_true", default=False, help="Generate junit XML report")
    logging.info("opts....")
    (opts, args) = LicensesTest.options.parse_args()
    logging.info("....end opts")
    # hardwired:
#    opts.device="longbeach"
#    LicensesTest.options = opts

    LicensesTest.readXmlFile()
    LicensesTest.readAdditional()

    testidx=1000

    # Get the tests for the files
    for filename in LicensesTest.getFilesList():
        # Get test file
        testfile = filename.replace("/", "_")
        # Get test method
        testidx+=1
        testmethod = "test%04d_%s" % (testidx, testfile)
        # Add test method
        def testexec(filename):
            return lambda self: self.assertNotice(filename)
        # Here we generate a method.... a test.
        setattr(LicensesTest, testmethod, testexec(filename))

    # pretend all the additional files are also in the rootfs
    for filename in LicensesTest.additionalFiles:
        # Get test file
        testfile = filename.replace("/", "_")
        # Get test method
        testidx+=1
        testmethod = "test%04d_%s" % (testidx, testfile)
        # Add test method
        def testexec(filename):
            return lambda self: self.assertNotice(filename)
        setattr(LicensesTest, testmethod, testexec(filename))

    if testidx > 2990:
        raise Exception, "More files than expected. Please fix the test"

    # valid license type and version
    testidx = 3000
    for path in LicensesTest.xmlEntries.iterkeys():
        testidx += 1
        testpath = path.replace("/", "_")
        testmethod = "test%04d_%s" % (testidx, testpath)
        def testexec(path):
            return lambda self: self.assertLicenseFileEntry(path)
        setattr(LicensesTest, testmethod, testexec(path))

    # test: do we have for each xml item an existing file?
    testidx = 5000
    for path in LicensesTest.xmlEntries.iterkeys():
        testidx += 1
        testpath = path.replace("/", "_")
        testmethod = "test%04d_%s" % (testidx, testpath)
        def testexec(path):
            return lambda self: self.assertFileEntry(path)
        setattr(LicensesTest, testmethod, testexec(path))


    # 1.  text only:
    # unittest.main()

    # 2. produce a file with junit xml.
    unittest.main(testRunner=xmlrunner.XMLTestRunner, argv=[sys.argv[0]])
    1
