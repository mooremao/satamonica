import os
import re
import sys
import time
import subprocess

from unittest import TestCase
from servers.ssh_host import SSHHost
from clients import ButLoggingConfiguration, utils
from common import ConfigBUT
from deployment.deploy_lib import runRemoteCommandAndGetOutput, AddBootTimeLog
from datetime import datetime

class ServerButTestCase(TestCase):
    """
    Base class for all server BUT test cases
    """

    VERDICT = False

    # Get the camera IP address from environment variable. If CAMERA_IP variable not set, use default
    devAddress = os.getenv("CAMERA_IP", "169.254.255.1")

    # The local IP address is equal to the camera's IP incremented by 1
    _ipTokenized = devAddress.split(".")
    locAddress = "{0}.{1}.{2}.{3}".format(_ipTokenized[0], _ipTokenized[1], _ipTokenized[2], str(int(_ipTokenized[3])+1))

    USER = "root"
    TEST_NAME = "Undefined"

    def setUp(self):
        print("ServerButTestCase setUp: '{0}'".format(self.name()))
        super(ServerButTestCase, self).setUp()
        self.tcConfiguration = ConfigBUT.ConfigBUT(self.name())

        self.logMan = ButLoggingConfiguration.LoggingConfig(self.tcConfiguration, self.tcConfiguration.serverlogsDir)
        self.logMan.configure()

        self.remoteDevice = SSHHost(self.devAddress, user=self.USER, password="")

        msg = "ServerButTestCase Starting: '{0}'".format(self.name())
        self.logger.info(msg)
        self.remoteDevice.run("logger \"{0}\"".format(msg))

        utils.reset_failed_camera(self.devAddress)
        utils.reset_reboot_counter(self.devAddress)

        self.VERDICT = False

    def tearDown(self):
        utils.reset_reboot_counter(self.devAddress)

        messages = {None: "VERDICT is None", True: "PASSED", False: "FAILED"}
        self.logger.info("Test case '{0}': {1}\n".format(self.name(), messages[self.VERDICT]))

        msg = "ServerButTestCase Ending: '{0}'".format(self.name())
        self.remoteDevice.run("logger \"{0}\"".format(msg))
        self.logger.info(msg)

        self.logMan.deconfigure()
        self.remoteDevice.close()

        super(ServerButTestCase, self).tearDown()

    def name(self):
        return self.TEST_NAME

    @property
    def logger(self):
        return self.logMan.getLogger


    def rebootDutAndWait(self):
        self.remoteDevice.run("shutdown -r now")
        return self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=True)

    def waitForDutAfterReboot(self):
        return self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=True)

    def waitForDutAfterFactoryReset(self):
        return self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=False)

    '''
    Return the status of the usb0 interface.
    Check that ifconfig has the ip address. (Could check 'ifconfig usb0')
    '''
    def deviceUp(self, locAddress, quiet=False):
        ifstatus = os.popen("ifconfig").read()
        if re.search("inet addr:" + locAddress, ifstatus):  # got ip address for the device?
            return True
        if not quiet:
            print("ifconfig returned\n{1}\nCamera at '{0}' is not plugged in or not powered on.\n".format(locAddress, ifstatus))
        return False


    def printksleep(self, text, delay):
        sys.stdout.write(text)
        sys.stdout.flush()
        if delay != 0:
            time.sleep(delay)

    '''
    instruct the device to reboot
    wait for the usb0 device to come up after reboot. Timeout after 'timeout' in seconds (A timeout of at least 10 seconds is useful)
    return
        status True = Device is up
        return False = Device failed after timeout
    While loops are preferable to sleeps (As sleeps will be either too large or too small on different builds)
    '''
    def waitReboot(self, timeout, devAddress, locAddress, logBootTime=True):

        print("Waiting for '{0}' + '{1}' to go down and come up again ...".format(locAddress, devAddress))
        count = 0
        while self.deviceUp(locAddress, quiet=True):  # wait while interface is up
            self.printksleep('-', 1)
            count += 1
            if count >= timeout:
                print("Timeout count reached [#1]")
                return False

        while not self.deviceUp(locAddress, quiet=True):  # wait while interface is down
            self.printksleep('_', 1)
            count += 1
            if count >= timeout:
                print("Timeout count reached [#2]")
                return False

        # allow time for sshd / dropbear to start, otherwise get "connection refused" while sshd starts
        self.printksleep('-', 1)

        for retries in range(0, 10):
            ret = subprocess.Popen(['ssh -T -o ConnectTimeout=1 root@{0} true'.format(devAddress)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            (done, fail) = ret.communicate()
            fail = fail.strip()
            if fail == "":  # successfully connected?
                print("\nSuccessfully connected to sshd on the Device\n")
                AddBootTimeLog(self, logBootTime)
                return True

            self.printksleep("ssh attempt, will retry [" + fail + "]\n", 1)

        print("Timeout connecting to Device")
        return False

    '''
    Check the date-time on device (at devAddress) and wait until the clock is set correctly
     using the PC date-time as a reference.
     A timeout in seconds is required. ntpd may take a while to set the clock.
     An optional flag 'ntptest' if True will verify that the ntp daemon is running correctly
     Return False if the timeout is reached or ntptest= True and ntpd is not running.
    '''
    def waitNTPd(self, timeout, devAddress):

        count = 0
        while True:
            device_timenow, result = runRemoteCommandAndGetOutput("date -u +'%Y.%m.%d-%H:%M:%S'", devAddress)  # device time in UTC
            remote_timenow = datetime.strptime(device_timenow.rstrip(), "%Y.%m.%d-%H:%M:%S")

            local_timenow = datetime.utcnow()  # pc time in UTC
            print("PC time is: {0}, Device time is: {1}".format(local_timenow, device_timenow))

            count += 1
            if count == timeout:  # failsafe - don't wait forever (this loop takes about 1 second per iteration) ntpd can take 4 mins 20 seconds to update the clock
                print("Reached timeout. Give up waiting for ntpd to correct the clock")
                return False

            if abs((local_timenow - remote_timenow).total_seconds()) <= 3600:  # allow for timezone differences
                print("Device time is correct enough")
                return True

        return True

    def get_boot_reason(self):
        cmdline = self.remoteDevice.run("cat /proc/cmdline").stdout.strip()
        bootReason = re.search("boot_reason=([a-zA-Z0-9_]+)", cmdline).group(1)
        return bootReason

    # flush to "/var/log/all/current"
    # return True if flushed successfully, False otherwise
    def flushLog(self):
        flushed_file = "/run/metalog.flushed"                  # log flushed state
        self.remoteDevice.run("rm -f {0}".format(flushed_file))

        # flush the log file by sending signal USR1
        cmd = "systemctl kill --signal=USR1 metalog.service"   # switch to synchronous mode ...
        self.remoteDevice.run(cmd)

        timeout = 5
        status = False
        while timeout > 0:
            waitforflushed = self.remoteDevice.run("ls -l {0}".format(flushed_file), ignore_status=True).stdout.strip()
            if flushed_file in waitforflushed:
                status = True
                break                   # avoid sleeping
            time.sleep(1)
            timeout -= 1

        # set back to async logging by sending signal USR2
        cmd = "systemctl kill --signal=USR2 metalog.service"   # ... switch back to asynchronous mode.
        self.remoteDevice.run(cmd)

        return status
