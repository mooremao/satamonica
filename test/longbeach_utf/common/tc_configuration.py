'''
Created on Nov 14, 2014

@author: segura
'''
import ConfigParser

class TCConfig(object):
    '''
    This class is representing the BUT configuration for a concrete test suite
    '''
    def __init__(self, tcName):
        '''
        Constructor
        '''
        self.config = ConfigParser.ConfigParser()
        self.config.read("../config_files/" + tcName + ".cfg")

    def logFileName(self):
        return self.config.get('TESTCASE', "log_file_name")

    def get(self, param):
        return self.config.get('TESTCASE', param)
