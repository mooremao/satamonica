"""
Created on Dec 12, 2014

@author: segura
"""
from clients import ButLoggingConfiguration
from clients import utils
from common import ConfigBUT
from unittest import TestCase
import os.path
import time

class ButTestCase(TestCase):
    """
    Base class for all BUT test cases
    """

    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    RESET_REBOOT_COUNTER = "echo 0 > /sys/bus/platform/drivers/reboot-counter/reboot-counter/value"

    VERDICT = False
    TEST_NAME = "Undefined"
    SERVER_DIR = None

    def setUp(self):
        print("ButTestCase setUp: '{0}'".format(self.name()))
        super(ButTestCase, self).setUp()
        self.tcConfiguration = ConfigBUT.ConfigBUT(self.name())
        self.logMan = ButLoggingConfiguration.LoggingConfig(self.tcConfiguration, self.SERVER_DIR)
        self.logMan.configure()

        msg = "ButTestCase Starting: '{0}'".format(self.name())
        self.logger.info(msg)
        utils.system("logger \"{0}\"".format(msg))

        # Prevent reboots (into recovery)
        utils.system(self.RESET_FAILED_CMD, ignore_status=True)
        utils.system(self.RESET_REBOOT_COUNTER, ignore_status=True)

        self.VERDICT = False

    def name(self):
        return self.TEST_NAME

    def tearDown(self):
        # Prevent reboots (into recovery)
        utils.system(self.RESET_FAILED_CMD, ignore_status=True)

        messages = {None: "VERDICT is None", True: "PASSED", False: "FAILED"}
        self.logger.info("Test case '{0}': {1}\n".format(self.name(), messages[self.VERDICT]))

        msg = "ButTestCase Ending: '{0}'".format(self.name())
        utils.system("logger \"{0}\"".format(msg))
        self.logger.info(msg)

        self.logMan.deconfigure()
        super(ButTestCase, self).tearDown()

    @property
    def logger(self):
        return self.logMan.getLogger

    def flushLog(self):
        flushed_file = "/run/metalog.flushed"                  # log flushed state
        utils.system("rm -f {0}".format(flushed_file))

        # flush the log file by sending signal USR1
        cmd = "systemctl kill --signal=USR1 metalog.service"   # switch to synchronous mode ...
        utils.system(cmd)

        timeout = 5
        status = False
        while timeout > 0:
            if os.path.exists(flushed_file):                   # log flushed file recreated
                self.logger.info("'{0}' recreated".format(flushed_file))
                status = True
                break               # avoid sleeping
            time.sleep(1)
            timeout -= 1

        # set back to async logging by sending signal USR2
        cmd = "systemctl kill --signal=USR2 metalog.service"   # ... switch back to asynchronous mode.
        utils.system(cmd)

        return status
