'''
Created on Nov 14, 2014

@author: segura
'''
import ConfigParser

class BUTBasicConfig(object):
    '''
    This class is representing the BUT configuration for a concrete test suite
    '''
    def __init__(self):
        '''
        Constructor
        '''
        self.configFile = '../config_files/but.cfg'
        self.config = ConfigParser.ConfigParser()
        self.config.read(self.configFile)    
    @property
    def address(self):
        return self.config.get('DEVICE','address')
    @property
    def rootDir(self):
        return self.config.get('DEVICE','test_rootdir')
    @property
    def logsDir(self):
        return self.config.get('DEVICE','logs_dir')
    @property
    def tmpDir(self):
        return self.config.get('DEVICE','tmp_dir')
    @property
    def user(self):
        return self.config.get('DEVICE','user')
    @property
    def password(self):
        return self.config.get('DEVICE','password')
    @property
    def serverAddress(self):
        return self.config.get('SERVER','server_address')
    @property
    def serverRootDir(self):
        return self.config.get('SERVER','test_server_rootdir')
    @property
    def logLevel(self):
        return self.config.get('TC_GLOBAL_CONFIG','loglevel')
