'''
Created on Mar 16, 2015

@author: astout
'''
from common.ServerButTestCase import ServerButTestCase

RESET_COUNT_FILE = "/mnt/userdata/ft-rst-count"

class Test(ServerButTestCase):

    TEST_NAME = "check_factory_restart"
    TEST_DESC = "Checking factory restart as expected"

    def testFactoryRestart(self):
        subTestName = "testFactoryRestart"
        self.logger.info("Starting '{0}'".format(subTestName))

        # if a factory restart has never occurred then this file does not exist (ignore status and get blank)
        resetcountBefore = self.remoteDevice.run("cat {0}".format(RESET_COUNT_FILE), ignore_status=True).stdout.strip()
        if resetcountBefore == "":      # file not existing counts as zero
            resetcountBefore = 0
        self.logger.info("'{0}' factory_reset counter BEFORE = {1}".format(RESET_COUNT_FILE, resetcountBefore))

        self.remoteDevice.run("touch /mnt/userdata/factory-restart.txt")        # file should be deleted
        self.remoteDevice.run("factory_reset -l")

        self.logger.info("factory_reset wait for reboot ...")
        rebooted = self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=False)
        errMsg = "Device did not reboot"
        self.assertTrue(rebooted, errMsg)

        status = self.remoteDevice.run("ls -l /mnt/userdata/").stdout
        errMsg = "Factory restart should have removed files from /mnt/userdata/. Got '{0}'".format(status)
        self.assertTrue("factory-restart.txt" not in status, errMsg)

        resetcountAfter = self.remoteDevice.run("cat {0}".format(RESET_COUNT_FILE)).stdout.strip()
        self.logger.info("'{0}' factory_reset counter AFTER = {1}".format(RESET_COUNT_FILE, resetcountAfter))

        errMsg = "Reset counter not incremented (Before {0}+1 != After {1})".format(resetcountBefore, resetcountAfter)
        self.assertEqual(int(resetcountBefore) + 1, int(resetcountAfter), errMsg)     # Note the +1
        self.logger.info("factory_reset counter is incremented correctly. {0} became {1}".format(resetcountBefore, resetcountAfter))


        status = self.remoteDevice.run("systemctl status firstboot.service").stdout
        errMsg = "expected 'Firstboot done' in status: Got '{0}'".format(status)
        self.assertTrue("firstboot.service - Firstboot done" in status, errMsg)


        self.logger.info("Ending '{0}'".format(subTestName))
        self.VERDICT = True


    def testFactoryRestartZero(self):
        subTestName = "testFactoryRestartZero"
        self.logger.info("Starting '{0}'".format(subTestName))

        # delete the factory restart count file and verify that it is recreated
        resetcountBefore = self.remoteDevice.run("cat {0}".format(RESET_COUNT_FILE), ignore_status=True).stdout.strip()
        if resetcountBefore == "":      # file not existing counts as zero
            resetcountBefore = 0
        self.logger.info("'{0}' factory_reset counter BEFORE = {1}".format(RESET_COUNT_FILE, resetcountBefore))

        self.remoteDevice.run("rm -f {0}".format(RESET_COUNT_FILE), ignore_status=True)
        resetcount = self.remoteDevice.run("cat {0}".format(RESET_COUNT_FILE), ignore_status=True).stdout.strip()
        errMsg = "'{0}' was not removed".format(RESET_COUNT_FILE)
        self.assertEqual(resetcount, "", errMsg)


        self.remoteDevice.run("factory_reset -l")

        self.logger.info("Deleted ft-rst-count: factory_reset wait for reboot ...")
        rebooted = self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=False)
        errMsg = "Device did not reboot"
        self.assertTrue(rebooted, errMsg)

        resetcountAfter = self.remoteDevice.run("cat {0}".format(RESET_COUNT_FILE)).stdout.strip()
        self.logger.info("'{0}' factory_reset counter AFTER = {1}".format(RESET_COUNT_FILE, resetcountAfter))

        errMsg = "Reset counter not incremented (Before {0}+1 != After {1})".format(resetcountBefore, resetcountAfter)
        self.assertEqual(1, int(resetcountAfter), errMsg)       # file should have been created and contain 1
        self.logger.info("factory_reset counter was correctly created with '1' in it.")

        self.logger.info("Restoring reset counter in '{0}' to {1}".format(RESET_COUNT_FILE, resetcountBefore))
        self.remoteDevice.run("echo {0} > {1}".format(resetcountBefore, RESET_COUNT_FILE), ignore_status=True)

        # assert that boot_reason="warm"
        bootReason = self.get_boot_reason()
        self.logger.info("boot_reason='{0}'".format(bootReason))

        errMsg = "Expected boot_reason to be 'warm'. Got: boot_reason='{0}'".format(bootReason)
        self.assertEqual(bootReason, "warm", errMsg)


        status = self.remoteDevice.run("systemctl status firstboot.service").stdout
        errMsg = "expected 'Firstboot done' in status: Got '{0}'".format(status)
        self.assertTrue("firstboot.service - Firstboot done" in status, errMsg)


        self.logger.info("Ending '{0}'".format(subTestName))
        self.VERDICT = True
