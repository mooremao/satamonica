'''
Created March 23, 2015 from template_server.py
@author: Andy Stout
'''
import re

from common.ServerButTestCase import ServerButTestCase


# if this file exists, logging is enabled, if it is deleted logging is disabled
LOGGING_DISABLED_FILE = "/mnt/userdata/sd-logging-disabled"

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    # This text MUST match the filename, excluding the .py.
    # Note that '-' (minus) is an illegal character for a python module
    TEST_NAME = "check_sd_card"
    TEST_DESC = "Test mounting and unmounting the SD card"
    mountExpected = "/dev/mmcblk0"


    # store the status of the logging file, so that it can be restored by the tearDown method
    def setUp(self):
        super(Test, self).setUp()
        logging_file_ls = self.remoteDevice.run("ls -l {0}".format(LOGGING_DISABLED_FILE), ignore_status=True).stdout.strip()
        self.logging_status = (LOGGING_DISABLED_FILE in logging_file_ls)


    def tearDown(self):
        # ensure the SD card is mounted, regardless
        self.remoteDevice.run(("systemctl start mmc"), ignore_status=True)

        mountStatus = self.remoteDevice.run(("mount | grep mmc"), ignore_status=True).stdout.strip()
        if self.mountExpected in mountStatus:
            self.logger.info("SD card is correctly re-mounted by the tearDown method")
        else:
            self.logger.info("tearDown mount. Expecting mmc to be mounted.   mount reports: '{0}'".format(mountStatus))
            self.logger.info("tearDown mount: Expected '{0}'. Got '{1}'".format(self.mountExpected, mountStatus))

        # restore the logging disabled file back to setUp() state
        if self.logging_status:
            self.remoteDevice.run(("touch {0}".format(LOGGING_DISABLED_FILE)), ignore_status=True)
        else:
            self.remoteDevice.run(("rm -f {0}".format(LOGGING_DISABLED_FILE)), ignore_status=True)

        super(Test, self).tearDown()


    '''
    mount the sd card (mount-when-already-mounted test)
    assert(sd card is mounted)

    turn off logging (which frees the SD card)
    unmount the sd card
    assert(sd card is unmounted)

    The teardown mounts the sd card regardless of any assert failures in this test
    '''

    def testSDCardMounting(self):
        """
        test mounting, unmounting and remounting the SD card
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))

        devicepwd = self.remoteDevice.run("pwd", ignore_status=True).stdout.strip()
        self.logger.info("cwd on device = {0}".format(devicepwd))

        # mount and assert it is mounted
        self.remoteDevice.run("systemctl start mmc", ignore_status=True).stdout.strip()

        mountStatus = self.remoteDevice.run("mount | grep mmc", ignore_status=True).stdout.strip()
        if self.mountExpected in mountStatus:
            self.logger.info("mount: SD card is correctly mounted")
        else:
            self.logger.info("mount: Expecting mmc to be mounted.   mount reports: '{0}'".format(mountStatus))
            mmcstatus = self.remoteDevice.run("systemctl -l status mmc.service", ignore_status=True).stdout.strip()
            self.logger.info("mmc service status=[{0}]\n\n".format(mmcstatus))

        errMsg = "mount: Expected '{0}'. Got '{1}'".format(self.mountExpected, mountStatus)
        self.assertIn(self.mountExpected, mountStatus, errMsg)           # assert SD card mounted successfully

        mountgrep = re.search("/dev/mmcblk.+ on /mnt/mmc type (vfat|texfat|exfat) \((r[ow])", mountStatus)
        if mountgrep is None:
            print("Bad mount string: '{0}' <<< Will fail".format(mountStatus))

        fstype = mountgrep.group(1)
        rwmode = mountgrep.group(2)

        self.logger.info("mounted filesystem is '{0}'".format(fstype))
        self.logger.info("Expecting mode to be RW. Got '{0}'".format(rwmode))
        self.assertEqual(rwmode, "rw")

        # unmount and assert it is unmounted - SD card will not unmount if the card is in use
        self.remoteDevice.run("systemctl stop mmc", ignore_status=True).stdout.strip()

        # TODO: add a test which tries to unmount the SD card when it is in use
        mountStatus = self.remoteDevice.run("mount | grep mmc", ignore_status=True).stdout.strip()
        if mountStatus == "":
            self.logger.info("unmount: SD card was unmounted.")
        else:
            self.logger.info("unmount: Expecting mmc unmount to succeed. mount reports: '{0}'".format(mountStatus))
            mmcstatus = self.remoteDevice.run("systemctl -l status mmc.service", ignore_status=True).stdout.strip()
            self.logger.info("mmc service status=[{0}]\n\n".format(mmcstatus))


        errMsg = "unmount: Expected mmc unmount to succeed. Got '{0}'".format(mountStatus)
        self.assertEqual("", mountStatus, errMsg)                 # assert card is unmounted

        # NOTE that the tearDown will remount the SD card regardless of any errors in the test
        self.logger.info("mount - mount - unmount: PASSED")

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True


    def testSDCardSpeed(self):
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))

        self.remoteDevice.run("cat /sys/kernel/debug/mmc1/ios", ignore_status=True).stdout.strip()

        sdclock = self.remoteDevice.run("cat /sys/kernel/debug/mmc1/clock", ignore_status=True).stdout.strip()
        self.logger.info("SD card is clocked at '{0}'".format(sdclock))

        if "38400000" in sdclock:
            self.logger.info("SD card speed is NORMAL")
        elif "50000000" in sdclock:
            self.logger.info("SD card speed is HIGH")
        else:
            msg = "SD card speed is unexpected. Got '{0}'".format(sdclock)
            self.logger.info(msg)
            self.assertTrue(False, msg)

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
