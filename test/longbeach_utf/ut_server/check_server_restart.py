'''
Created on Nov 12, 2014

@author: segura
'''
from common.ServerButTestCase import ServerButTestCase
import os.path
from subprocess import Popen, PIPE

# Each entry has format: [<testname>, <search string>, <expected string>]
test_list = [
                ["ARM", "ARM clk:", "970MHz"],
                ["DDR", "DDR clk:", "529MHz"],
                ["L3", "L3 clk:", "220MHz"],
                ["IVA", "IVA clk:", "390MHz"],
                ["ISS", "ISS clk:", "390MHz"],
                ["DSP", "DSP Default", "OFF"],
                ["DSS", "DSS Default", "OFF"]
            ]

class Test(ServerButTestCase):

    TEST_NAME = "check_server_restart"


    def testRestart(self):
        subTestName = "testRestart"
        self.logger.info("Starting '{0}'".format(subTestName))

        self.logger.info("The remote host is %s", self.remoteDevice.run("uname -a"))
        self.logger.info("The remote host is %s", self.remoteDevice.run("ls"))

        self.logger.info("Rebooting the system ...")
        rebooted = self.rebootDutAndWait()
        self.assertTrue(rebooted, "Device did not come back after reboot")

        status_camera = self.remoteDevice.run("systemctl is-active camera-app.service").stdout
        self.logger.info("The status of the camera after restarting is " + status_camera)

        errMsg = "Camera application is not started. Expected 'active'. Got '{0}'".format(status_camera)
        self.assertIn("active", status_camera, errMsg)

        bootReason = self.get_boot_reason()
        self.logger.info("boot_reason='{0}'".format(bootReason))

        errMsg = "Expected boot_reason to be 'warm'. Got: boot_reason='{0}'".format(bootReason)
        self.assertEqual(bootReason, "warm", errMsg)


        self.logger.info("Ending '{0}'".format(subTestName))
        self.VERDICT = True


    def testCPUCurrentSpeed(self):
        subTestName = "testCPUCurrentSpeed"
        self.logger.info("Starting '{0}'".format(subTestName))

        currentCPUSpeed = self.remoteDevice.run("cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq").stdout.strip()
        errMsg = "Expecting current CPU speed to be between 110MHz and 430MHz. Got: '{0}'".format(currentCPUSpeed)
        self.logger.info(errMsg)
        self.assertTrue("110000" <= currentCPUSpeed <= "428998", errMsg)

        self.logger.info("Ending '{0}'".format(subTestName))
        self.VERDICT = True


    """
    The bootspeed "ARM clk: 970MHz" does not appear in the dmesg, or any of the device log files
    However, it is available from the serial data captured into serialdata.log file.
    """
    def checkDeviceMHz(self, testName, searchString, expectedString):
        subTestName = "testDevice{0}-MHz".format(testName)
        self.logger.info("Starting '{0}'".format(subTestName))

        serialdataLogFile = "../../../serialdata.log"

        bootSpeed = ""
        if os.path.isfile(serialdataLogFile):
            cmd = ["grep", searchString, serialdataLogFile]
            bootSpeed = Popen(cmd, stdout=PIPE).communicate()[0].strip()
        else:
            errMsg = "serialdata.log file not found."
            self.assertTrue(False, errMsg)

        errMsg = "Did not find ('{0}') in serialdata.log. Expecting at least one.".format(searchString)
        self.assertTrue(bootSpeed != "", errMsg)

        bootList = bootSpeed.split("\n")
        count = 0
        found = 0
        for bootLine in bootList:
            bootLine = bootLine.strip()
            self.logger.info(">{0}<".format(bootLine))
            if expectedString in bootLine:
                found += 1
            else:
                self.logger.info("Found unexpected {0} speed. Expected '{1}'. Got: '{2}'".format(searchString, expectedString, bootLine))
            count += 1

        errMsg = "Device has been rebooted {0} times. {1} times @ {2} {3}".format(count, found, searchString, expectedString)
        self.logger.info(errMsg)
        self.assertTrue(found >= 1, errMsg)         # at least one. This allows for serial data corruption

        self.logger.info("Ending '{0}'".format(subTestName))
        self.VERDICT = True


'''
Create dynamically named test cases
'''
def _add_test(testName, searchString, expectedString):
    def test_method(self):
        self.checkDeviceMHz(testName, searchString, expectedString)
    setattr(Test, 'test_'+testName, test_method)
    test_method.__name__ = 'test_'+testName

for (testName, searchString, expectedString) in test_list:
    _add_test(testName, searchString, expectedString)
