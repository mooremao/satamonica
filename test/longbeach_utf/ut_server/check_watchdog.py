'''
Created on March 25, 2015

@author: astout
'''
from common.ServerButTestCase import ServerButTestCase
import re
import time

class Test(ServerButTestCase):
    TEST_NAME = "check_watchdog"


    def testWatchdog(self):
        subTestName = "testWatchdog"
        self.logger.info("Starting '{0}'".format(subTestName))

        for tries in range(0, 2):
            cameraApp = self.remoteDevice.run("ps w | grep run_camera_app.sh").stdout
            cameraPID = re.search("\s+(\d+)\s+root\s+(\d+)", cameraApp).group(1)
            self.logger.info("Loop: {0}: The camera PID is {1}".format(tries, cameraPID))

            killcameraApp = self.remoteDevice.run("kill -9 {0}".format(cameraPID), ignore_status=True).stdout
            time.sleep(5)                           # allow time for camera app to die and be restarted
            if not self.deviceUp(self.locAddress):  # watchdog rebooted ?
                break


        self.logger.info("Expecting the watchdog to reboot the device ...")
        rebooted = self.waitReboot(60, self.devAddress, self.locAddress)

        errMsg = "Device did not reboot by watchdog"
        self.assertTrue(rebooted, errMsg)
        status_camera = self.remoteDevice.run("systemctl is-active camera-app.service").stdout
        self.logger.info("The status of the camera after restarting is " + status_camera)

        errMsg = "Camera application is not started. Expected 'active'. Got '{0}'".format(status_camera)
        self.assertIn("active", status_camera, errMsg)

        bootReason = self.get_boot_reason()
        self.logger.info("boot_reason='{0}'".format(bootReason))

        errMsg = "Expected boot_reason to be 'warm'. Got: boot_reason='{0}'".format(bootReason)
        self.assertEqual(bootReason, "warm", errMsg)


        self.logger.info("Ending '{0}'".format(subTestName))
        self.VERDICT = True



