'''
Created on Feb 25, 2015

@author: vinit
'''
import sys
import os
import time
from clients import utils
from common.ServerButTestCase import ServerButTestCase
from clients import videotool_main_app
RECORD_TIME=10
class Test(ServerButTestCase):

    TEST_NAME = "cinematic_server"
    TEST_DESC = "Testing the cinematic mode recording"

    def setUp(self):
        super(Test, self).setUp()

        """
        Prepare the device for the testcase by restarting application and removing old files.
        """
        self.remoteDevice.run("rm -f /mnt/mmc/DCIM/100TTCAM/*.mp4")
        self.logger.info('Preparing test case ' + self.name())
        self.remoteDevice.run("systemctl stop camera-app")
        self.remoteDevice.run("systemctl start camera-app")

    def tearDown(self):
        """"
        Moving recorded file in cinamatic folder
        """
        self.remoteDevice.run("mkdir -p /mnt/mmc/DCIM/100TTCAM/cinematic")
        self.remoteDevice.run("mv /mnt/mmc/DCIM/100TTCAM/*.MP4 /mnt/mmc/DCIM/100TTCAM/cinematic")
        super(Test, self).tearDown()

    def testCinematic(self):
        """
        This test case is going to record video in cinematic mode
        """
        self.logger.info('Starting ' + self.TEST_NAME)
        self.logger.info(self.TEST_DESC)
        videotool_main_app.cinematic(RECORD_TIME)
        test_status=self.remoteDevice.run("for file in /mnt/mmc/DCIM/100TTCAM/*.MP4; do if [[ ! -s $file ]]; then echo \"FAIL\"; fi; done").stdout
        self.assertNotEqual(test_status=="FAIL", "Failed since some files are empty(zero size)")
