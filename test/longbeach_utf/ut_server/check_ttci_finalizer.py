'''
Created on 4 Feb 2015

@author: jensenj
'''
from common.ServerButTestCase import ServerButTestCase
import os.path

class Test(ServerButTestCase):

    TEST_NAME = "check_ttci_finalizer"

    def setUp(self):
        super(Test, self).setUp()

    def tearDown(self):
        self.remoteDevice.run("rm -rf /mnt/mmc/fdt.backup.bin /mnt/userdata/fdt.bin")
        super(Test, self).tearDown()

    def getDeviceSerial(self):
        result = self.remoteDevice.run("deviceinfo_example device-id || true").stdout
        return result.rstrip("\n")

    def runTTCIFinalizerAndWaitForReboot(self, fdtFile):
        # Send fdtFile if not None or remove it if it exists
        if fdtFile is not None:
            self.remoteDevice.send_file(fdtFile, "/mnt/userdata/fdt.bin")
        else:
            self.remoteDevice.run("rm -rf /mnt/userdata/fdt.bin")

        # Start ttci-finalizer, this should program the fdt and reboot the device
        self.remoteDevice.run("systemctl start ttci-finalizer")
        rebooted = self.waitForDutAfterReboot()
        self.assertTrue(rebooted, "Device did not come back after reboot")


    def verifyFDTFileIsRemoved(self):
        self.remoteDevice.run("test ! -f /mnt/userdata/fdt.bin")

    def testTTCIFinalizer(self):
        # Retrieve serial
        serial = self.getDeviceSerial()
        self.logger.info("Current serial: {0}".format(serial))

        # Create backup of FDT if is does not exist yet
        fdtBackupFile = "{0}.fdt.backup.bin".format(serial)
        if not os.path.isfile(fdtBackupFile):
            self.remoteDevice.run("cat /dev/fdtexport > /mnt/mmc/fdt.backup.bin")
            self.remoteDevice.get_file("/mnt/mmc/fdt.backup.bin", fdtBackupFile)

        # Send new fdt.bin and run finalizer.
        self.runTTCIFinalizerAndWaitForReboot("../test_data/fdt.bin")

        # Verify fdt.bin is removed
        self.verifyFDTFileIsRemoved()

        # Verify the serial has been changed.
        newSerial = self.getDeviceSerial()
        self.logger.info("New serial: {0}".format(newSerial))
        self.assertEqual("NC6494A66666", newSerial, "New serial does not equal to NC6494A66666")

        # Restore original FDT.
        self.runTTCIFinalizerAndWaitForReboot(fdtBackupFile)

        # Verify fdt.bin is removed
        self.verifyFDTFileIsRemoved()

        # Verify the serial has been restored
        newSerial = self.getDeviceSerial()
        self.logger.info("New serial: {0}".format(newSerial))
        self.assertEqual(newSerial, serial, "New serial does not equal to {0}".format(serial))

        # Run TTCI finalizer without fdt.bin
        self.runTTCIFinalizerAndWaitForReboot(None)

        # Verify the serial is still valid.
        newSerial = self.getDeviceSerial()
        self.logger.info("New serial: {0}".format(newSerial))
        self.assertEqual(newSerial, serial, "New serial does not equal to {0}".format(serial))

        self.VERDICT = True
