"""
Created on Dec 2, 2014

@author: Jeroen Jensen
"""

from common.ServerButTestCase import ServerButTestCase


class Test(ServerButTestCase):

    TEST_NAME = "check_camera_app_sigsegv"
    TEST_DESC = "Checking whether SIGSEGV signal results in reboot of the device"

    EXPECT_REBOOT = False

    def testSigSEGV(self):
        """
        This test case is going to check whatever SIGSEGV signal results in restart of the device
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))
        self.logger.info(self.TEST_DESC)

        running_since = self.remoteDevice.run("systemctl show camera-app.service | grep ExecMainStartTimestamp", ignore_status=True)
        self.logger.info(running_since)

        self.remoteDevice.run("uptime")
        self.flushLog()
        self.remoteDevice.run("systemctl kill --signal=SIGSEGV camera-app.service", ignore_status=True)

        status = self.waitReboot(90, self.devAddress, self.locAddress, logBootTime=True)
        self.logger.info("\nDevice rebooted: {0}".format(status))

        cameraAppServiceStatus = ""
        if not status:      # device did not reboot
            self.logger.info("Device did not reboot after SIGSEGV camera-app.service")

            self.remoteDevice.run("uptime")
            cameraAppServiceStatus = self.remoteDevice.run("systemctl status camera-app.service", ignore_status=True)

        if self.EXPECT_REBOOT:
            errMsg = "check_camera_app_sigsegv: Expected device to reboot. Rebooted? {0}\n" \
                     "Camera-app service status is '{1}'".format(status, cameraAppServiceStatus)
        else:
            errMsg = "check_camera_app_sigsegv: NOT Expected device to reboot. Rebooted? {0}\n" \
                     "Camera-app service status is '{1}'".format(status, cameraAppServiceStatus)
        self.logger.info(errMsg)
        self.assertEqual(status, self.EXPECT_REBOOT, errMsg)

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
