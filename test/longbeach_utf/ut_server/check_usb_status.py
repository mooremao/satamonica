'''
Created on Nov 12, 2014

@author: segura
'''
import sys

from clients import utils
from common.ServerButTestCase import ServerButTestCase

class Test(ServerButTestCase):

    TEST_NAME = "check_usb_status"
    TEST_DESC = "Testing the stability of the usb connection. We iterate 10 times waiting for having always ssh connectivity via usb (Was 200 without error)"
    FAILURE = "No route to host"

    TEST_COUNT = 10
    def testUSB(self):
        """
        This test case is going to check the usb connection of a LongBeach device
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))
        self.logger.info(self.TEST_DESC)
        ssh_connection = ""
        for iteration in range(1, self.TEST_COUNT+1):
            try:
                self.logger.info("[Test {0} of {1}]...".format(iteration, self.TEST_COUNT))
                ssh_connection = utils.system_output('ssh {0}@{1} ls'.format(self.USER, self.devAddress), retain_output=True)
                self.logger.info(ssh_connection)
                self.logger.info(" iteration number: {0} ssh connection output: '{1}'".format(iteration, ssh_connection))

            except Exception as e:
                self.assertFalse (self.FAILURE in ssh_connection, "Failed path output does not exist.")
                self.assertTrue(False, sys.exc_info())

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
