'''
Created on Nov 12, 2014

@author: segura
'''
import re
import time
from clients import utils

from common.ServerButTestCase import ServerButTestCase


class Test(ServerButTestCase):

    TEST_NAME = "check_device_status_after_suspend_resume"
    SLEEPING_TIME = 5
    SUSPEND_CMD = "echo mem > /sys/power/state"
    WAKEUP_TIME = "echo {0} > /sys/class/power_supply/tomtom_mcu/device/wakeup_timeout".format(SLEEPING_TIME)

    running_ref = ['bluetopia',
                   'camera-app',
                   'dropbear',
                   'gpsd',
                   'metalog',
                   'monitor-temp',
                   'ntpd',
                   'omap-serial-getty',
                   'remote-debug-client',
                   'systemd-journald',
                   'systemd-udevd'
                   ]

    def setUp(self):
        super(Test, self).setUp()

    def tearDown(self):
        super(Test, self).tearDown()

    def checkActiveServices(self):
        running_list = []

        services = self.remoteDevice.run("systemctl --all --type service --no-legend list-unit-files --no-pager").stdout
        services = services.split('\n')
        services.sort()  # in-place sort. Makes all the other lists sorted
        #print(">> Handle these services: ",services)
        for line in services:
            if line == "":
                continue
            #print "> ["+line+"]"

            service, type = line.split()
            #print ">> ",service, type

            name, sep, suffix = service.rpartition('.')
            print name
            status = self.remoteDevice.run("systemctl status {0}".format(name), ignore_status=True).stdout
            if re.search("Active: active \(running\) since", status):  # currently running
                running_list.append(name)

        self.logger.info("Running processes List: {0}".format(running_list))
        #utils.compare_lists(self, running_list, running_ref, None, "Running")
        return running_list


    def testSuspendResume(self):
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))

        self.logger.info("Evaluating active services BEFORE suspend")
        before_list = self.checkActiveServices()


        # Set waking up time
        self.logger.info("Setting the device wake time: {0}".format(self.WAKEUP_TIME))
        status = self.remoteDevice.run(self.WAKEUP_TIME)
        self.assertTrue(status.exit_status==0, "Error setting waking up time")

        # Suspend device
        self.logger.info("Suspending device: {0}".format(self.SUSPEND_CMD))
        status = self.remoteDevice.run(self.SUSPEND_CMD)
        self.assertTrue(status.exit_status==0, "Error executing Suspend command")

        # Sleep to allow services to start
        time.sleep(self.SLEEPING_TIME)


        # Check services after resume
        self.logger.info("Evaluating active services AFTER suspend")
        after_list = self.checkActiveServices()

        strErr = "Expecting the before and after lists to be the same"
        utils.compare_lists(self, before_list, after_list, None, strErr)

        strErr = "Expecting both lists to match the reference list"
        utils.compare_lists(self, before_list, self.running_ref, None, strErr)

        self.logger.info("Running process lists are the same before and after suspend")
        self.VERDICT = True
