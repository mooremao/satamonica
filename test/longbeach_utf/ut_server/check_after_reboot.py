'''
Created April 10, 2015 from template_server

@author: Andy Stout
'''
import sys
import unittest

from clients import utils
from common.ServerButTestCase import ServerButTestCase

# See https://docs.python.org/2/library/unittest.html for more information
# Note that /ut/ and /ut_server/ cannot contain the same file names.
class Test(ServerButTestCase):

    TEST_NAME = "check_after_reboot"
    TEST_DESC = "Check things immediately after a reboot"

    GYRO_STATUS = "cat /tmp/sys_platform-gyro/device/enable"
    GYRO_EVTEST_READ = "evtest /dev/input/event5 & sleep 2; killall evtest"

    def testGyroAfterReboot(self):
        """
        reboot the device, then read the gyro status with evtest
        [LBP-1684] if "SYN_REPORT" is not seen, then the polling is not working
        """

        self.logger.info("Starting '{0}'".format(self.TEST_NAME))

        self.logger.info("Reboot and wait ...")
        rebooted = self.rebootDutAndWait()
        self.assertTrue(rebooted, "Device did not come back after reboot")


        gyrostatus = self.remoteDevice.run(self.GYRO_STATUS).stdout.strip()
        self.logger.info("Gyro status '{0}' after reboot is '{1}'".format(self.GYRO_STATUS, gyrostatus))

        gyrotext = self.remoteDevice.run(self.GYRO_EVTEST_READ).stdout.strip()
        self.logger.info("Gyro evtest returned '{0}'".format(gyrotext))

        errMsg = "gyrostatus is '{0}'".format(gyrostatus)
        self.assertEqual(gyrostatus, "1", errMsg)       # expecting enabled immediately after reboot

        errMsg = "Expected (REL_RX). Got {0}".format(gyrotext)
        self.assertIn("(REL_RX)", gyrotext, errMsg)

        errMsg = "Expected (REL_RY). Got {0}".format(gyrotext)
        self.assertIn("(REL_RY)", gyrotext, errMsg)

        errMsg = "Expected (REL_RZ). Got {0}".format(gyrotext)
        self.assertIn("(REL_RZ)", gyrotext, errMsg)

        errMsg = "Default evtest gyro behaviour has changed. Gyro is enabled, but polling not working. Got {0}".format(gyrotext)
        self.assertIn("SYN_REPORT", gyrotext, errMsg)

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
