'''
Created Mar 2, 2015 from template_server.py

@author: Andy Stout
'''
import sys
import re
import time

from clients import utils
from common.ServerButTestCase import ServerButTestCase
from deployment.deploy_lib import runRemoteCommandAndGetOutput, runSystemCommandAndGetOutput, sendToRemote

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    # This text MUST match the filename, excluding the .py.
    # Note that '-' (minus) is an illegal character for a python module
    TEST_NAME = "do_sdcard_upgrade"
    TEST_DESC = "Upgrade from image on sd card"

    '''
    Define separate tests within the suite.
    Tests must be named 'test<something>' but the order of test execution is not defined and may even be randomised.
    Each test has the setUp run before and tearDown run after.
    Do not use try / except. The ButTest / unitTest harness catches those.
    '''

    def testSDCardUpgrade(self, result=None):
        """
        This test case is going put an image on the SD card, then reboot.
        Then verify that the update has worked.
        Falling through all the asserts and setting VERDICT = True marks the test as a pass
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)

        (ssh_result, stderr) = runRemoteCommandAndGetOutput("uname -a", self.devAddress)
        ssh_result = ssh_result.strip()
        self.logger.info("Current build version 'uname -a' says: [{0}]".format(ssh_result))

        (lbp_version_result, stderr) = runRemoteCommandAndGetOutput("get-lbp-version -l", self.devAddress)
        lbp_version_result = lbp_version_result.strip()
        self.logger.info("Current build version 'get-lbp-version' says: [{0}]".format(lbp_version_result))

        (imagefile_name, stderr) = runSystemCommandAndGetOutput("ls ../../../longbeach_*.tar.gz", self.devAddress)
        imagefile_name = imagefile_name.strip()
        self.logger.info("Imagefile available is: [{0}]".format(imagefile_name))

        image_id1 = re.search(r"_release_([0-9a-g]+).tar.gz", imagefile_name).group(1)
        print(">>>> reference 1: [{0}]".format(image_id1))



        # replace the release_{8alphas}.tar.gz with {201503031}

        current_date = time.strftime("%Y%m%d1")     # YYYYMMDDV where V is the version number
        self.logger.info("new filename should be adjusted to use '{0}'".format(current_date))
        target_name = "/mnt/mmc/longbeach_update_release_{0}.tar.gz".format(current_date)
        self.logger.info("new filename adjusted to: '{0}'".format(target_name))

        print("About to scp {0} root@{1}:{2}".format(imagefile_name, self.devAddress, target_name))
        # copy the same build to the device. on reboot it should upgrade to this.
        sendToRemote(self.devAddress, imagefile_name, target_name)

        rebooted = self.rebootDutAndWait()
        self.assertTrue(rebooted, "Device did not come back after reboot")

        (ssh_result, stderr) = runRemoteCommandAndGetOutput("uname -a", self.devAddress)
        ssh_result = ssh_result.strip()
        self.logger.info("Current build version 'uname -a' says: [{0}]".format(ssh_result))

        (lbp_version_result, stderr) = runRemoteCommandAndGetOutput("get-lbp-version -l", self.devAddress)
        lbp_version_result = lbp_version_result.strip()
        self.logger.info("Current build version 'get-lbp-version' says: [{0}]".format(lbp_version_result))
        print(">>>> reference 2: [{0}]".format(lbp_version_result))

        strErr = "Expected version numbers to match. Got '{0}' and '{1}'".format(image_id1, lbp_version_result)
        self.logger(strErr)
        self.assertEqual(image_id1, lbp_version_result, strErr)

        self.logger.info("Ending '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
