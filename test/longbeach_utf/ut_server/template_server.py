'''
Created Jan 22, 2015 from measure_soc_temperature.py

@author: Andy Stout
'''
import sys

from clients import utils
from common.ServerButTestCase import ServerButTestCase

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    # This text MUST match the filename, excluding the .py.
    # Note that '-' (minus) is an illegal character for a python module
    TEST_NAME = "template_server"
    TEST_DESC = "Template test"

    MAX_TEMP = 95.0
    MIN_TEMP = 10.0

    # if you don't want any local setUp or tearDown, these are optional
    # Note that setUp is run before each sub-test and tearDown after each sub-test.
    def setUp(self):
        super(Test, self).setUp()
        self.logger.info('local setup goes here - after parent setup')

    def tearDown(self):
        self.logger.info('local teardown goes here - before parent teardown')
        super(Test, self).tearDown()

    '''
    Define separate tests within the suite.
    Tests must be named 'test<something>' but the order of test execution is not defined and may even be randomised.
    Each test has the setUp run before and tearDown run after.
    Do not use try / except. The ButTest / unitTest harness catches those.
    '''

    def testMeasure(self, result=None):
        """
        This test case is going to check the temperature of a LongBeach device
        Falling through all the asserts and setting VERDICT = True marks the test as a pass
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Measuring the Fuel Gauge Temperature')

        ssh_result = utils.system_output('ssh {0}@{1} echo tmp103'.format(self.USER, self.devAddress), retain_output=True)
        self.assertTrue(ssh_result == "tmp103", "SOC Temperature device name expected to be tmp103. Got {0}".format(ssh_result))

        ssh_result = utils.system_output('ssh {0}@{1} echo 56.78'.format(self.USER, self.devAddress), retain_output=True)
        testValue = float(ssh_result)
        strError = "Failed since the SOC temperature is '{0}'C (min {1}, max {2})".format(ssh_result, self.MIN_TEMP, self.MAX_TEMP)
        self.assertTrue(self.MIN_TEMP <= testValue <= self.MAX_TEMP, strError)

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True

    '''
    Define more tests.
    Use the assert family of macros
    '''
    def testA(self, result=None):
        self.logger.info('Test something else A')

        self.VERDICT = True
        if not self.VERDICT:
            self.fail("testA fails")

    def testZ(self, result=None):
        self.logger.info('Test something else Z')

        self.VERDICT = True
