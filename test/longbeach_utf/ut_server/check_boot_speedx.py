'''
Created on Feb 3, 2015

@author: Komal Padia
'''

from clients import utils
from common.ServerButTestCase import ServerButTestCase
from clients import error
import re

class Test(ServerButTestCase):

    TEST_NAME = "check_boot_speedx"
    TEST_DESC = "Testing boot speed Excluding an first boot occasions"

    '''
    Input a number in microseconds, convert to a human readable quantity string
    '''
    def uStohuman(self, number):
        factor = ["uS", "mS", "S"]
        factoroffset = 0

        number = (float)(number)
        while number >= 1000.0 and factoroffset < len(factor)-1:
            number /= 1000.0
            factoroffset += 1

        return "{0:.3f} {1}".format(number, factor[factoroffset])


    def testBootSpeedX(self):
        """
        This test case is going to check the boot speed
        If this boot is a 'first boot' then the time is too large and can be ignored.
        A device test cannot request a reboot and continue
        """
        self.logger.info(self.TEST_DESC)

        BOOT1MAX = 4.5
        BOOT2MAX = 3.5

        for reboots in range(2):
            try:
                bootinfo = self.remoteDevice.run("boottime").stdout.strip()
            except error.AutoservRunError as e:
                exit_code_re = re.search("Exit status: (\d+)\n", "{0}".format(e))
                exit_code = exit_code_re.group(1)
                self.logger.error("ERROR IN RUNNING BOOTTIME. Exit code: {0}".format(exit_code))

                errMsg = "Aborting test. Probably SD card trouble"
                self.assertTrue(False, errMsg)

            boottime = re.findall(r"[-+]?\d+", bootinfo)

            self.logger.info("Bootinfo returned:\n" + bootinfo)

            if "WARNING:" not in bootinfo:      # not a first boot, with longer timing?
                break

            self.logger.info("First boot times are longer. Reboot and check again")
            rebooted = self.rebootDutAndWait()
            self.assertTrue(rebooted, "Device did not come back after reboot")

        boot1 = int(boottime[0])
        boot2 = int(boottime[1])

        self.assertTrue(boot1 > 0 and boot2 > 0, "Negative boot times are unexpected. Got: boot1='{0}' boot2='{1}'".format(boot1, boot2))

        with open("{0}/{1}/bootspeed.csv".format(self.tcConfiguration.serverlogsDir, self.TEST_NAME), "wb") as csvfile:
            csvfile.write("Boot1, Boot2\n")
            csvfile.write("{0}, {1}\n".format(boot1, boot2))
        csvfile.close()

        boot1msg = self.uStohuman(boot1)
        boot2msg = self.uStohuman(boot2)
        self.logger.info("bootime values in human form: {0} and {1}. (Expecting under {2} Seconds and {3} Seconds respectively)".format(boot1msg, boot2msg, BOOT1MAX, BOOT2MAX))

        ERROR_MSG = "Expecting under {0} S and {1} S respectively. Got: {2} and {3}".format(BOOT1MAX, BOOT2MAX, boot1msg, boot2msg)
        self.assertTrue(boot1 <= int(BOOT1MAX*1e6) and boot2 <= int(BOOT2MAX*1e6), ERROR_MSG)

        self.logger.info("End of test " + self.TEST_DESC)

        self.VERDICT = True
