'''
Created March 23, 2015 from template_server.py

@author: Andy Stout
'''
import sys

from common.ServerButTestCase import ServerButTestCase

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    # This text MUST match the filename, excluding the .py.
    # Note that '-' (minus) is an illegal character for a python module
    TEST_NAME = "check_bluetooth"
    TEST_DESC = "Bluetooth tests"


    '''
    Define separate tests within the suite.
    Tests must be named 'test<something>' but the order of test execution is not defined and may even be randomised.
    Each test has the setUp run before and tearDown run after.
    Do not use try / except. The ButTest / unitTest harness catches those.
    '''

    def testSS1BTPM(self, result=None):
        """
        test whether the SS1BTPM process is running
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))

        ss1btpmStatus = self.remoteDevice.run(("ps w | grep SS1BTPM"), ignore_status=True).stdout.strip()
        errMsg = "Bluetopia process not running. Got '{0}'".format(ss1btpmStatus)
        self.assertIn("/usr/sbin/SS1BTPM", ss1btpmStatus, errMsg)

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
