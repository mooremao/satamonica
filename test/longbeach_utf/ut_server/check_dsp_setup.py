'''
Created on Feb 26, 2014

@author: astout
'''
from common.ServerButTestCase import ServerButTestCase
from time import sleep

# When the DSP is re-enabled in the project, change this to True and the tests should pass again.
DSP_ENABLED = False

# The expected texts are each preceded by timestamps, so check as separate lines. (Order is not checked)
EXPECTED_TEXT1 = "ADC3101: programming mini dsp"
EXPECTED_TEXT =     [
                    "asoc: tlv320adc3101 <-> davinci-mcasp.0 mapping ok",
                    "ADC3101 Audio Codec Rev A",
                    "Failed to add route",
                    ]

NOT_EXPECTED_TEXT = [
                    "ADC3101: not writing",
                    ]

'''
/var/log/all/current contains the entries since the last boot.
'''
class Test(ServerButTestCase):
    TEST_NAME = "check_dsp_setup"
    TEST_DESC = "Testing DSP has been setup correctly"

    # Assert that the DSP messages are as expected in the log - some strings are expected to NOT be present
    def testDSPGoodText(self):
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))
        self.logger.info(self.TEST_DESC)

        self.logger.info("Reboot and wait ...")
        rebooted = self.rebootDutAndWait()
        self.assertTrue(rebooted, "Device did not come back after reboot")

        sleep(1)
        dspStatus = self.remoteDevice.run("cat /sys/devices/platform/omap/omap_i2c.3/i2c-3/3-0018/load_dsp").stdout.strip()
        if dspStatus == "0":
            self.logger.info("DSP is DISABLED")
        elif dspStatus == "1":
            self.logger.info("DSP is ENABLED")
        else:
            self.logger.info("DSP status should be [0,1] Got '{0}'".format(dspStatus))

        dspActual = (dspStatus == "1")
        msg = "DSP state is not as expected. Test says DSP_ENABLED='{0}' Device is '{1}'".format(DSP_ENABLED, dspActual)
        self.assertEqual(dspActual, DSP_ENABLED, msg)               # DSP status matches expected

        self.flushLog()     # ensure the log file on disk is up to date
        self.remoteDevice.run("ls -l /var/log/all", ignore_status=True)         # useful to see a list of log files

        # get lines before (-B) and lines after (-A) match
        dsp_text = self.remoteDevice.run("grep -B 0 -A 5 ADC3101 /var/log/all/current", ignore_status=True).stdout.strip()
        if dsp_text == "":                  # not found anything? log file may have been rotated
            files = self.remoteDevice.run("ls -rt /var/log/all/log-*", ignore_status=True).stdout.strip()
            files = files.split("\n")       # want the latest rotated file
            dsp_text = self.remoteDevice.run("grep -B 0 -A 5 ADC3101 {0}".format(files[-1]), ignore_status=True).stdout.strip()

        self.logger.info("DSP A3101:'{0}'\n".format(dsp_text))

        if DSP_ENABLED:
            self.assertIn(EXPECTED_TEXT1, dsp_text)
        else:
            self.assertNotIn(EXPECTED_TEXT1, dsp_text)

        for each in EXPECTED_TEXT:
            if each not in dsp_text:
                self.logger.info("DSP string '{0}' was expected to be found".format(each))
            self.assertIn(each, dsp_text)

        for each in NOT_EXPECTED_TEXT:
            if each in dsp_text:
                self.logger.info("DSP string '{0}' was expected to NOT be found".format(each))
            self.assertNotIn(each, dsp_text)

        self.logger.info("DSP strings are present (or not present) as expected")

        self.logger.info("Ending '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
