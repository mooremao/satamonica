"""
Created March 20, 2015 from template.py

@author: Andy Stout
"""
import os
from subprocess import Popen, PIPE

from common.ServerButTestCase import ServerButTestCase


# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    TEST_NAME = "check_logs"
    TEST_DESC = "Verify that various strings are NOT present in the logs. This should be the last program of the suite."

    # These strings will be searched for with case insensitive mode
    fatalStrings = [
        "kernel panic",
        "fatal",
        "fifo full",
        "fifo is full",
        "i2c error",
        # "swap.target failed to load",
        # "slices.target failed to load",
        "check sd card",
        "hibernate failed. Resetting",
        "Internal error: Oops:",
        "lsm303e_acc 4-0019: hw init error",
        "lsm303e_acc: Driver Init failed",
        "Failed to start",
        "BUG! This might cause LBP-1779.",
        "BUG! You may have tackled LBP-1779.",
        "Failed to get D-Bus connection",
    ]

    def localRun(self, cmd):
        return Popen(cmd, stdout=PIPE).communicate()[0].strip()

    def assertFileSize(self, fileToCheck, minimumSize):
        fileSize = os.path.getsize(fileToCheck)
        msg = "'{0}' has a size of {1} bytes. (Minimum required is {2} bytes):\n".format(fileToCheck, fileSize, minimumSize)
        self.logger.info(msg)
        self.assertTrue(fileSize > minimumSize, msg)

    def testSerialLog(self, result=None):
        """
        check that none of the error strings are found in the local serialdata.log file
        """
        subTestName = "testSerialLog"
        self.logger.info("Starting '{0}'".format(subTestName))

        checkserialfile = "../../../serialdata.log"

        errMsg = "'{0}' file not found.".format(checkserialfile)
        self.assertTrue(os.path.isfile(checkserialfile), errMsg)

        # Check that there is more than just the Jenkins "Start of Test" and "End of Test" messages in the serial log
        self.assertFileSize(checkserialfile, 130)

        for string in self.fatalStrings:
            grepResult = self.localRun(["grep", "-i", "-B10", "-A10", string, checkserialfile])
            errMsg = "'{0}' should not have been found in '{1}' but was. ('\n{2}\n')".format(string, checkserialfile, grepResult)
            self.assertEqual(grepResult, "", errMsg)

        self.logger.info("Stopping '{0}'".format(subTestName))
        self.VERDICT = True


    def testCurrentLog(self, result=None):
        """
        check that none of the error strings are found in the device /var/log/all/current
        """
        subTestName = "testCurrentLog"
        self.logger.info("Starting '{0}'".format(subTestName))

        checklogfile = "/var/log/all/current"

        for string in self.fatalStrings:
            grepResult = self.remoteDevice.run("grep -i -B10 -A10 '{0}' {1}".format(string, checklogfile), ignore_status=True).stdout.strip()
            errMsg = "'{0}' should not have been found in '{1}' but was. ('\n{2}\n')".format(string, checklogfile, grepResult)
            self.assertEqual(grepResult, "", errMsg)

        self.logger.info("Stopping '{0}'".format(subTestName))
        self.VERDICT = True
