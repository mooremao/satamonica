'''
Created on Sep 22, 2015

@author: mrmak
'''
from common.ServerButTestCase import ServerButTestCase
from os import *
from subprocess import *
from deployment.deploy_lib import *

class Test(ServerButTestCase):

    TEST_NAME = "check_uboot_and_recovery_update"

    """
    In this test, we will try to update the camera with the same version of firmware
    that is already present on the camera. This way, we expect to see the following strings:
    "Skipping uboot image update (update version matches current version)"
    """

    def testUbootProcedure(self):
        subTestName = "testUbootProcedure"

        serialdataLogFile = '../../../serialdata.log'
        ubootdataLogFile  = '../../../uboot.log'

        self.logger.info("Starting '{0}'".format(subTestName))
        self.logger.info("The remote host is %s", self.remoteDevice.run("uname -a"))
        self.logger.info("About to send an update to the camera that is identical to the firmware already on the camera...")

        sendToRemote(self.devAddress, "../../../longbeach_update__PLATFORM_LIB_release*.tar.gz", "/mnt/mmc/longbeach_update__PLATFORM_LIB_000000000.tar.gz")
        runRemoteCommand('enter_recovery', self.devAddress)
        waitReboot(90, self.devAddress, self.locAddress)

        searchStringUboot    = "Skipping uboot image update"
        searchStringRecovery = "Skipping recovery image update"

        ubootUpdated    = ""
        recoveryUpdated = ""
        if os.path.isfile(serialdataLogFile):
            cmd = "tail -n 1000 {0} > {1}".format(serialdataLogFile, ubootdataLogFile)
            runSystemCommand(cmd)

            cmd = ["grep", searchStringUboot, ubootdataLogFile]
            ubootUpdated = Popen(cmd, stdout=PIPE).communicate()[0].strip()

            cmd = ["grep", searchStringRecovery, ubootdataLogFile]
            recoveryUpdated = Popen(cmd, stdout=PIPE).communicate()[0].strip()
        else:
           errMsg = "serialdata.log file not found."
           self.assertTrue(False, errMsg)

        errMsg = "Did not find ('{0}') in serialdata.log. Expected to see it in the logs.".format(searchStringUboot)
        self.assertTrue(ubootUpdated != "", errMsg)

        errMsg = "Did not find ('{0}') in serialdata.log. Expected to see it in the logs.".format(searchStringRecovery)
        self.assertTrue(recoveryUpdated != "", errMsg)

        self.logger.info("Uboot and recovery image update skipping - OK")

# ========================================

        self.logger.info("About to send an update to the camera that is different from the firmware already on the camera...")
        sendToRemote(self.devAddress, "../test_data/longbeach_update__PLATFORM_LIB_release*.tar.gz", "/mnt/mmc/longbeach_update__PLATFORM_LIB_000000000.tar.gz")
        runRemoteCommand('enter_recovery', self.devAddress)
        waitReboot(90, self.devAddress, self.locAddress)

        searchStringUboot    = "The uboot image in this update is different"
        ubootUpdatedRecovery = "The recovery image in this update is different"
        if os.path.isfile(serialdataLogFile):
            cmd = "tail -n 1000 {0} > {1}".format(serialdataLogFile, ubootdataLogFile)
            runSystemCommand(cmd)

            cmd = ["grep", searchStringUboot, ubootdataLogFile]
            ubootUpdated2 = Popen(cmd, stdout=PIPE).communicate()[0].strip()

            cmd = ["grep", searchStringRecovery, ubootdataLogFile]
            recoveryUpdated2 = Popen(cmd, stdout=PIPE).communicate()[0].strip()
        else:
           errMsg = "serialdata.log file not found."
           self.assertTrue(False, errMsg)

        errMsg = "Did not find ('{0}') in serialdata.log. Expected to see it in the logs.".format(searchStringUboot)
        self.assertTrue(ubootUpdated != "", errMsg)

        errMsg = "Did not find ('{0}') in serialdata.log. Expected to see it in the logs.".format(searchStringRecovery)
        self.assertTrue(recoveryUpdated != "", errMsg)

        self.logger.info("Uboot and recovery image update test - OK")

# ========================================

        self.logger.info("About to revert firmware to the one that was on the camera before the test...")
        sendToRemote(self.devAddress, "../../../longbeach_update__PLATFORM_LIB_release*.tar.gz", "/mnt/mmc/longbeach_update__PLATFORM_LIB_000000000.tar.gz")
        runRemoteCommand('enter_recovery', self.devAddress)
        waitReboot(90, self.devAddress, self.locAddress)

        self.logger.info("Ending '{0}'".format(subTestName))

        self.VERDICT = True