"""
Created Feb 2, 2015 from template.py

@author: Andy Stout
"""
import os
import subprocess
import datetime
import re

from common.ServerButTestCase import ServerButTestCase
from clients import utils

# Set this True to test both directions of scp copying (which doubles the test execution time)
# If this is False, files will be copied from the PC to the device
SCPUpandDown = False

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    # This text MUST match the filename, excluding the .py
    TEST_NAME = "check_usb_stability"
    TEST_DESC = "Stress the usb connection by copying a large file and verify the checksum"

    DEVICE_PATH_PREFIX = "/mnt/mmc/"              # SD card is r/w

    SD_CARD_NAME = "/mnt/mmc"
    RW_FILE = "BigFile.DELETEME"

    ONEGB = 1024 * 1024 * 1024     # 1GB

    def runRemoteCommandAndGetOutput(self, command, devAddress):
        self.logger.info("Executing @[{0}]: [{1}]".format(devAddress, command))
        ssh_connection = utils.system_output("ssh root@{0} {1}".format(devAddress, command), retain_output=True)
        return ssh_connection


    '''
    Delete all files of the format "BigFile.DELETEME[0-9]" before and after each test
    '''
    def deleteTempFiles(self):
        self.logger.info("Deleting ... takes time")
        self.delete_remote_file(self.DEVICE_PATH_PREFIX + self.RW_FILE + "[0-9]")
        self.delete_remote_file(self.DEVICE_PATH_PREFIX + self.RW_FILE + "[0-9][0-9]")
        self.delete_remote_file(self.DEVICE_PATH_PREFIX + self.RW_FILE + "[0-9][0-9][0-9]")

        self.delete_local_file(self.RW_FILE + "[0-9]")
        self.logger.info("Deleting done")

    def setUp(self):
        self.SERVER_DIR = "../testServerLogs"
        super(Test, self).setUp()
        self.deleteTempFiles()

    def tearDown(self):
        self.deleteTempFiles()
        super(Test, self).tearDown()


    '''
    Return the free space on the remote SD card
    '''
    def remote_freespace(self, path):
        df = self.runRemoteCommandAndGetOutput("df {0}".format(path), self.devAddress)
        df = df.split('\n')
        # ignore the header returned from df
        filesystem, size, used, available, percent, mountpoint = df[1].split()
        return int(available) * 1024  # size is in 1k blocks


    '''
    Create '../clients/<filename>' of the specified size.
    The fast way to do this is open, seek to the end, write one byte and close
    '''
    def create_empty_file(self, filename, sizeinbytes):
        self.logger.info("Creating: '{0}' size {1}".format(filename, sizeinbytes))
        with open(filename, "wb") as out:
            out.seek(sizeinbytes - 1)
            out.write('\0')
        out.close()

    '''
    Silently remove file
    '''
    def delete_local_file(self, filename):
        self.logger.info("Deleting: '{0}/{1}'".format(os.getcwd(), filename))
        try:
            os.remove(filename)
        except OSError:
            pass


    def delete_remote_file(self, filename):
        result = self.runRemoteCommandAndGetOutput("rm -f " + filename, self.devAddress)  # -f never fails
        return result == ""                         # Return Bool, should always return True


    def get_local_md5Sum(self, filename):
        md5 = subprocess.Popen(["md5sum", filename], stdout=subprocess.PIPE)
        result = md5.communicate()[0].split()
        return result[0]


    def get_remote_md5Sum(self, filename):
        md5 = self.runRemoteCommandAndGetOutput("md5sum " + filename, self.devAddress)
        md5 = md5.split('\n')
        result = md5[0].split()
        return result[0]


    # return a nice formatted string. Cope with tdiff being zero
    def speed(self, start_time, end_time, filesize):
        tdiff = abs(int((end_time - start_time).total_seconds()))       # cope with start and end being reversed

        factor = ["B/s", "kB/s", "MB/s", "GB/s", "TB/s"]
        factoroffset = 0

        if tdiff == 0:
            return "{0} bytes probably NOT copied in 0 seconds, due to previous error".format(filesize)
        else:
            speed = float(filesize/tdiff)

            while speed >= 1024.0 and factoroffset < len(factor) - 1:
                speed /= 1024.0
                factoroffset += 1

        return "{0} bytes copied in {1} seconds = {2:.2f} {3}".format(filesize, tdiff, speed, factor[factoroffset])


    '''
    Create a local file and return the md5
    '''
    def createlocalfile(self, filename, filesize):

        self.create_empty_file(filename, filesize)    # create large file locally
        local_md5sum = self.get_local_md5Sum(filename)
        self.logger.info("md5sum of local file '{0}' is [{1}]".format(filename, local_md5sum))
        return local_md5sum


    def scp(self, srcfilename, dstfilename, filesize, local_md5sum):

        self.logger.info("push {0} bytes '{1}' over usb".format(filesize, dstfilename))
        tstart = datetime.datetime.now()

        cmd = "scp {0} root@{1}:/{2}".format(srcfilename, self.devAddress, self.DEVICE_PATH_PREFIX + dstfilename)
        ret = subprocess.Popen([cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (scp_stdout, scp_stderr) = ret.communicate()

        print(">>>>>>>>>>>>> scp returns: [{0}] + [{1}]".format(scp_stdout, scp_stderr))
        if scp_stderr != "":
            return False

        tend = datetime.datetime.now()
        self.logger.info(self.speed(tstart, tend, filesize))

        remote_md5sum = self.get_remote_md5Sum(self.DEVICE_PATH_PREFIX + dstfilename)
        self.logger.info("md5sum of remote file '{0}' is [{1}]".format(dstfilename, remote_md5sum))

        strErr = "md5 of remote file '{0}' is not correct (Got [{1}], expected [{2}]".format(dstfilename, remote_md5sum, local_md5sum)
        self.assertEqual(local_md5sum, remote_md5sum, strErr)

        if SCPUpandDown:
            # pull the file back over scp and verify that the md5 is correct.
            # (Yes, this is inefficient, but the purpose is to test scp in both directions)
            self.logger.info("pull '{0}' over usb".format(dstfilename))
            tstart = datetime.datetime.now()
            cmd = "scp root@{0}:/{1} {2}".format(self.devAddress, self.DEVICE_PATH_PREFIX + srcfilename, dstfilename)
            ret = subprocess.Popen([cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            (scp_stdout, scp_stderr) = ret.communicate()
            if scp_stderr != "":
                return False

        tend = datetime.datetime.now()

        self.logger.info(self.speed(tstart, tend, filesize))

        local2_md5sum = self.get_local_md5Sum(srcfilename)
        strErr = "md5sum of local file '{0}' is not correct (Got [{1}], expected [{2}]".format(srcfilename, local2_md5sum, local_md5sum)
        self.assertEqual(local2_md5sum, local_md5sum, strErr)

        return True

    '''
    Notes:
    Even if there is enough free space, each file may not be larger than 4GB (vfat limit)
    Remember that the setup deletes all the DELETEME files between each subtest
    The tests may be run in any order, so each needs to be stand-alone
    The purpose here is to stress test the usb connection, not the filesystem, which is why it is slow
    '''

    '''
    [LBP-1195] Copy 1x1GB file onto the SD card. We expect this test to not fill the SD Card.
    '''
    def testusbscp1x1GB(self):

        local_name = "testusbscp1GB"
        self.logger.info("Starting '{0}'".format(local_name))

        # Even if there is enough free space, each file may not be larger than 4GB (vfat limit)

        freespace = self.remote_freespace(self.SD_CARD_NAME)
        self.logger.info("Freespace on the device SD Card: {0} bytes".format(freespace))

        strErr = "Need more than 1GB of free space on the SD card"
        self.assertLess(self.ONEGB, freespace, strErr)

        filename = "{0}{1}".format(self.RW_FILE, "0")
        filesize = self.ONEGB
        local_md5sum = self.createlocalfile(filename, filesize)
        scpResult = self.scp(filename, filename, filesize, local_md5sum)
        self.assertTrue(scpResult, "scp failed")
        self.delete_local_file(filename)


        self.logger.info("Stopping '{0}'".format(local_name))
        self.VERDICT = True


    '''
    Copy 'n' 1GB files onto the SD card. We expect this test to fill the SD Card to within 1GB of full
    '''
    def testusbscpnx1GB(self):

        local_name = "testusbscpnx1GB"
        self.logger.info("Starting '{0}'".format(local_name))

        freespace = self.remote_freespace(self.SD_CARD_NAME)
        self.logger.info("Freespace on the device SD Card: {0} bytes".format(freespace))

        strErr = "Need more than 1GB of free space on the SD card"
        self.assertLess(self.ONEGB, freespace, strErr)

        filename0 = self.RW_FILE + "0"
        filesize = self.ONEGB
        local_md5sum = self.createlocalfile(filename0, filesize)
        count = int(freespace / filesize)         # how many GB files to create to almost fill the SD Card. Round down
        self.logger.info("copying {0} x 1GB files to almost fill the SD card".format(count))

        for filecounter in range(0, count):
            filename = "{0}{1}".format(self.RW_FILE, filecounter)
            self.logger.info("send {0} of {1}".format(filename, count))
            scpResult = self.scp(filename0, filename, filesize, local_md5sum)
            self.assertTrue(scpResult, "scp failed")

        self.delete_local_file(filename0)


        self.logger.info("Stopping '{0}'".format(local_name))
        self.VERDICT = True

    '''
    Fill the SD card. Expect this to fail
    '''
    def testFillSDCard(self):

        local_name = "testFillSDCard"
        self.logger.info("Starting '{0}'".format(local_name))

        freespace = self.remote_freespace(self.SD_CARD_NAME)
        self.logger.info("Freespace on the device SD Card: {0} bytes".format(freespace))

        strErr = "Need more than 1GB of free space on the SD card"
        self.assertLess(self.ONEGB, freespace, strErr)

        filename0 = self.RW_FILE + "0"
        filesize = self.ONEGB
        local_md5sum = self.createlocalfile(filename0, filesize)
        count = int(freespace / filesize)    # how many GB files to create to almost fill the SD Card. Round down
        self.logger.info("copying {0} x 1GB files to over fill the SD card".format(count))

        for filecounter in range(0, count):
            filename = "{0}{1}".format(self.RW_FILE, filecounter)
            self.logger.info("send {0}".format(filename))
            scpResult = self.scp(filename0, filename, filesize, local_md5sum)
            self.assertTrue(scpResult, "scp failed")

        # sending one more file will fill the filesystem
        filename = "{0}{1}".format(self.RW_FILE, count)
        scpResult = self.scp(filename0, filename, filesize, local_md5sum)
        self.assertFalse(scpResult, "scp was expected to fail")

        self.delete_local_file(filename0)


        self.logger.info("Stopping '{0}'".format(local_name))
        self.VERDICT = True            # if reach here, the test has failed its purpose !


    '''
    Create a 4GB -1 byte file on the device. 4GB is the vfat filesize limit and this should pass on all filesystem types
    '''
    def testvfat4GBminus1filesize(self):

        local_name = "testvfat4GB-1filesize"
        self.logger.info("Starting '{0}'".format(local_name))

        freespace = self.remote_freespace(self.SD_CARD_NAME)
        self.logger.info("Freespace on the device SD Card: {0} bytes".format(freespace))

        strErr = "Need more than 4GB of free space on the SD card"
        self.assertLess(self.ONEGB*4, freespace, strErr)

        filename = "{0}{1}".format(self.RW_FILE, "0")
        filesize = self.ONEGB*4-1              # -1 byte, should be ok
        local_md5sum = self.createlocalfile(filename, filesize)
        scpResult = self.scp(filename, filename, filesize, local_md5sum)
        self.assertTrue(scpResult, "scp failed")

        self.delete_local_file(filename)


        self.logger.info("Stopping '{0}'".format(local_name))
        self.VERDICT = True

    '''
    Create a 4GB file on the device. 4GB is the vfat filesize limit and this should fail on some filesystem types
    '''
    def testvfat4GBfilesize(self):

        local_name = "testvfat4GBfilesize"
        self.logger.info("Starting '{0}'".format(local_name))

        freespace = self.remote_freespace(self.SD_CARD_NAME)
        self.logger.info("Freespace on the device SD Card: {0} bytes".format(freespace))

        strErr = "Need more than 4GB of free space on the SD card"
        self.assertLess(self.ONEGB*4, freespace, strErr)

        filename = "{0}{1}".format(self.RW_FILE, "0")
        filesize = self.ONEGB*4               # exactly 4GB, should fail
        local_md5sum = self.createlocalfile(filename, filesize)

        mountStatus = self.remoteDevice.run("mount | grep mmc", ignore_status=True).stdout.strip()
        mountgrep = re.search("/dev/mmcblk.+ on /mnt/mmc type (.*) \((r[ow])", mountStatus)
        mountfs = mountgrep.group(1)
        mountrw = mountgrep.group(2)

        self.assertTrue(mountrw == "rw", "SD card needs to be rw")

        self.logger.info("Does filesystem type '{0}' support 4GB files?".format(mountfs))
        scpResult = self.scp(filename, filename, filesize, local_md5sum)
        if "vfat" == mountfs:           # vfat does not support 4GB files
            self.assertFalse(scpResult, "Filesystem type '{0}' does support 4GB files, and was not expected to".format(mountfs))
            self.logger.info("Filesystem '{0}' correctly does NOT support 4GB files".format(mountfs))
        elif "texfat" == mountfs:       # texfat does support 4GB files
            self.assertTrue(scpResult, "Filesystem type '{0}' does NOT support 4GB files, and was expected to".format(mountfs))
            self.logger.info("Filesystem '{0}' correctly does support 4GB files".format(mountfs))
        else:
            self.assertTrue(False, "Found unexpected SD card filesystem format '{0}'".format(mountfs))

        self.delete_local_file(filename)


        self.logger.info("Stopping '{0}'".format(local_name))
        self.VERDICT = True            # if reach here, the test has failed its purpose !


    '''
    Create a deep directory structure. We were expecting this to fail, but happily it passes
    '''
    def testvfatdeepdirectory(self):

        local_name = "testvfatdeepdirectory"
        self.logger.info("Starting '{0}'".format(local_name))

        pathname = self.DEVICE_PATH_PREFIX + "DELETEME"                 # r/w filesystem on SD card
        toplevelpathname = pathname
        for d in range(ord('a'), ord('z')+1):
            pathname += "/" + chr(d)
        self.logger.info("1. Make directory of " + pathname)
        self.runRemoteCommandAndGetOutput("mkdir -p " + pathname, self.devAddress)

        # gently explore where the failure boundary is
        for d in range(ord('A'), ord('Z')+1):
            pathname += "/" + chr(d)
            self.logger.info("2. Make directory of "+pathname)
            self.runRemoteCommandAndGetOutput("mkdir -p " + pathname, self.devAddress)

        self.runRemoteCommandAndGetOutput("rm -rf " + toplevelpathname, self.devAddress)


        self.logger.info("Stopping '{0}'".format(local_name))
        self.VERDICT = True


    '''
    [LBP-1196] Copy 1x3GB file onto the SD card while the cpu has a high load
    '''
    def testusbscp3GBHighCPUload(self):

        local_name = "testusbscp3GBHighCPULoad"
        self.logger.info("Starting '{0}'".format(local_name))

        # Even if there is enough free space, each file may not be larger than 4GB (vfat limit)
        testfilesize = self.ONEGB*3

        freespace = self.remote_freespace(self.SD_CARD_NAME)
        self.logger.info("Freespace on the device SD Card: {0} bytes".format(freespace))

        strErr = "Need more than 3GB of free space on the SD card"
        self.assertLess(testfilesize, freespace, strErr)

        filename = "{0}{1}".format(self.RW_FILE, "0")
        local_md5sum = self.createlocalfile(filename, testfilesize)

        # time to transfer 3GB at 2MB/s = 24mins
        # Set cpu to 100% for half of that time. (I benchmarked with count=1M)
        # Note that /dev/random is a blocking read if there is insufficient entropy in the system.
        self.runRemoteCommandAndGetOutput("dd if=/dev/urandom of=/dev/null count=1M &", self.devAddress)

        scpResult = self.scp(filename, filename, testfilesize, local_md5sum)
        self.assertTrue(scpResult, "scp failed")
        self.delete_local_file(filename)


        self.logger.info("Stopping '{0}'".format(local_name))
        self.VERDICT = True
