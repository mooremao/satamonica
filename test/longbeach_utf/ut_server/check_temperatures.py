'''
Created March 17, 2015

@author: Andy Stout
'''
import sys

from clients import utils
from common.ServerButTestCase import ServerButTestCase


# Each entry has format: [<short name>, <device path>, <enable path>, <minimum expected>, <maximum expected>]
test_list = [
                ["SensorTemp", "/sys/bus/i2c/devices/4-0068/temp", "/sys/bus/i2c/devices/4-0068/enable", 20, 81],
                ["FuelGaugeTemp", "/sys/bus/i2c/devices/3-0055/power_supply/bq27421-0/temp", "", 200, 900],
                ["PressureTemp", "/sys/bus/i2c/devices/4-0077/input/input6/temperature", "/sys/bus/i2c/devices/4-0077/input/input6/enable", 2000, 9000],
                ["SOCTemp", "/sys/bus/i2c/devices/3-0072/temp1_input", "", 20000, 87000],

                ["GyroNice", "/dev/sensors/devices/gyro/temp", "/dev/sensors/devices/gyro/enable", 20, 87],
                ["FGNice", "/dev/sensors/devices/fg/temp", "", 200, 900],
                ["SOCNice", "/dev/sensors/devices/soc/temp1_input", "", 20000, 87000]
            ]


# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    # This text MUST match the filename, excluding the .py.
    # Note that '-' (minus) is an illegal character for a python module
    TEST_NAME = "check_temperatures"
    TEST_DESC = "Check all the temperature sensors"



    '''
    This test is run on each of the tests in the array above
    '''
    def checkDeviceTemperature(self, shortname, devicePath, enablePath, minExpected, maxExpected):
        self.logger.info("Starting 'test{0}'".format(shortname))

        if enablePath != "":
            enabledStatus = self.remoteDevice.run("cat {0}".format(enablePath)).stdout.strip()
            self.logger.info("Enabling device at '{0}' (Was at {1})".format(enablePath, enabledStatus))
            self.remoteDevice.run("echo 1 > {0}".format(enablePath))

        dTemp = self.remoteDevice.run("cat {0}".format(devicePath), ignore_status=True).stdout.strip()
        self.logger.info("'cat {0}' Returned: '{1}'".format(devicePath, dTemp))
        self.assertTrue(dTemp != "", "cat {0} is not enabled.".format(devicePath))

        if enablePath != "":
            self.logger.info("Restoring state of device at '{0}' to {1}".format(enablePath, enabledStatus))
            self.remoteDevice.run("echo {0} > {1}".format(enabledStatus, enablePath))

        # do the assert after restoring the enabled status, so that it gets done even on fail
        errMsg = "{0}: FAILED. Got '{1}' Expected range: [{2}, {3}]".format(shortname, dTemp, minExpected, maxExpected)
        self.assertTrue(int(minExpected) <= int(dTemp) <= int(maxExpected), errMsg)


        self.logger.info("Ending 'test{0}'".format(shortname))
        self.VERDICT = True



'''
Create dynamically named test cases
'''
def _add_test(shortname, devicePath, enablePath, min, max):
    def test_method(self):
        self.checkDeviceTemperature(shortname, devicePath, enablePath, min, max)
    setattr(Test, 'test'+shortname, test_method)
    test_method.__name__ = 'test'+shortname

for (shortname, devicePath, enablePath, min, max) in test_list:
    _add_test(shortname, devicePath, enablePath, min, max)
