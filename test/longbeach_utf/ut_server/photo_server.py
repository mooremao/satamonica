'''
Created on Feb 25, 2015

@author: vinit
'''
import sys
import os
import time
from clients import utils
from common.ServerButTestCase import ServerButTestCase
from clients import videotool_main_app
CONTINUTIME=60
class Test(ServerButTestCase):

    TEST_NAME = "photo_server"
    TEST_DESC = "Testing the Still image capture"

    def setUp(self):
        super(Test, self).setUp()

        """
        Prepare the device for the testcase by restarting application and removing old files.
        """
        self.remoteDevice.run("rm -f /mnt/mmc/DCIM/100TTCAM/*.JPG")
        self.logger.info('Preparing test case ' + self.name())
        self.remoteDevice.run("systemctl stop camera-app")
        self.remoteDevice.run("systemctl start camera-app")

    def tearDown(self):
        """"
        Moving captured file in photo folder
        """
        self.remoteDevice.run("mkdir -p /mnt/mmc/DCIM/100TTCAM/photo")
        self.remoteDevice.run("mv /mnt/mmc/DCIM/100TTCAM/*.JPG /mnt/mmc/DCIM/100TTCAM/photo")
        super(Test, self).tearDown()

    def testPhoto(self):
        """
        This test case is going to capture still images
        """
        self.logger.info('Starting ' + self.TEST_NAME)
        self.logger.info(self.TEST_DESC)
        videotool_main_app.photo(CONTINUTIME)
        test_status=self.remoteDevice.run("for file in /mnt/mmc/DCIM/100TTCAM/*.JPG; do if [[ ! -s $file ]]; then echo \"FAIL\"; fi; done").stdout
        self.assertNotEqual(test_status=="FAIL", "Failed since some files are empty(zero size)")
