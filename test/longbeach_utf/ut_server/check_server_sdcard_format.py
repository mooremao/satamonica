'''
Created on Dec 22, 2014

@author: Komal Padia
'''
import re

from common.ServerButTestCase import ServerButTestCase
from clients.remote import scp_to_remote

class Test(ServerButTestCase):

    TEST_NAME = "check_server_sdcard_format"
    TEST_DESC = "This test case tests SD card formatting on LongBeach device"

    def format_sdcard(self):
        """
        Format SD Card
        """
        # ignore_status because sdformat gives a warning
        # "Warning: block count mismatch" which is a known error
        # This warning does not indicate sdformat error
        format_out = self.remoteDevice.run("PATH=$PATH:/usr/sbin/ /mnt/userdata/sdformat", ignore_status=True).stdout
        self.assertIn('Formatting SD Card completed', format_out, "SD Card formatting failed")

    def check_sdcard(self):
        """
        Check SD Card is mounted
        """
        mount_out = self.remoteDevice.run("mount").stdout
        self.assertIn('mmc', mount_out, "SD Card not mounted")

        """
        Check single partition
        """
        self.assertNotIn('mmcblk0p', mount_out, "More than one partition after formatting")

        """
        Check file system type
        """
        fdisk_out = self.remoteDevice.run("fdisk -l /dev/mmcblk0 | grep Disk").stdout
        numbers = re.findall(r'\d+', fdisk_out)
        size = int(numbers[-1]) / 1024
        if size < (32 * 1024 * 1024):
            self.assertIn('vfat', mount_out, "Wrong file system type")
        else:
            self.assertIn('exfat', mount_out, "Wrong file system type")

        """
        Check SD card free space
        """
        df_out = self.remoteDevice.run("df /mnt/mmc/ | grep mmcblk0").stdout
        numbers = re.findall(r'\d+', df_out)
        freespace = int(numbers[-2])
        self.assertAlmostEqual(size, freespace, msg="Free space mismatch", delta=(1 * 1024 * 1024))

        """
        Check file read write
        """
        dd_out = self.remoteDevice.run("dd if=/dev/zero of=/mnt/mmc/file1 bs=1k count=1k").stderr
        self.assertNotIn('can\'t open', dd_out, "File write failed")
        cat_out = self.remoteDevice.run("cat /mnt/mmc/file1 > /mnt/mmc/file2").stderr
        self.assertNotIn('can\'t open', cat_out, "File read failed")

        """
        Run fsck
        """
        # TODO: fsck for vfat and exfat is not supported on the device

    def setUp(self):
        super(Test, self).setUp()

        scp_to_remote(host=self.devAddress, port="22", username=self.USER, password="",
            local_path="../../../source/tt_tools/examples/ttsystem_sdformat_example",
            remote_path="/mnt/userdata/sdformat")

    def tearDown(self):
        self.remoteDevice.run("rm /mnt/userdata/sdformat")

        super(Test, self).tearDown()

    def testFormatSdcard(self):
        """
        This test case is going to check SD card formatting on a LongBeach device
        """
        self.logger.info('Starting ' + self.TEST_NAME)
        self.logger.info(self.TEST_DESC)

        self.format_sdcard()
        rebooted = self.rebootDutAndWait()
        self.assertTrue(rebooted, "Device did not come back after reboot")

        self.check_sdcard()
        self.VERDICT = True
