"""
Created March 19, 2015 from template.py

@author: Andy Stout
"""
import re
import time
import subprocess
from clients import utils
from common.ServerButTestCase import ServerButTestCase
from clients.remote import scp_to_remote
import unittest

# GPS location of the possible test devices. Add or edit these as required.
AMSTERDAM = {"lat": "52.37642", "lon": "4.90824"}
BANGALORE = {"lat": "12.96923", "lon": "77.64905"}
BELGRADE = {"lat": "44.80428", "lon": "20.42883"}

TOLERANCE = {"lat": "0.01", "lon": "0.01"}

# Change this to match the test location
CURRENT_LOCATION = BELGRADE

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ServerButTestCase):

    TEST_NAME = "check_gps"
    TEST_DESC = "Verify GPS is available and as expected"

    # wait for maximum of 'timeout' seconds for the gps daemon to start
    def waitForGPSd(self, timeout):
        gpsdstate = self.remoteDevice.run("systemctl status gpsd", ignore_status=True).stdout
        if "active (running)" not in gpsdstate:
            self.logger.info("gpsd was not running. Restarting gpsd")
            self.remoteDevice.run("systemctl restart gpsd")
            time.sleep(1)

        count = 0
        while count < timeout:
            psgrep = self.remoteDevice.run("ps w | grep gpsd").stdout
            if "/usr/sbin/gpsd" in psgrep:
                self.logger.info("gpsd is running")
                break
            count += 1
            self.logger.info("Waiting for gpsd to start [Timeout {0} of {1}]".format(count, timeout))
            time.sleep(1)
        else:
            self.logger.info("Timeout reached {0} seconds. gpsd not running".format(timeout))
            return False

        # wait for the gpsd to process the tmp files
        count = 1
        while count < 60:
            files = self.remoteDevice.run("ls -l /tmp/sgee_info_*", ignore_status=True).stdout.strip()
            if ("sgee_info_gps" in files) and ("sgee_info_glo" in files):
                self.logger.info("Both files present, continuing test")
                break

            self.logger.info("[Loop {0}] Waiting for gpsd to create the tmp files".format(count))
            time.sleep(1)       # wait until both these files exist
            count += 1
        else:
            self.assertTrue(False, "Timeout waiting for temp files to be created")
            return False

        self.gpsvsutc()         # wait for date to not be "2015-01-01"
        time.sleep(1)
        return True


    @unittest.expectedFailure
    def testGPS(self):
        """
        check that GPS is available and as expected
        """
        SUBTEST_NAME = "check_gps"
        self.logger.info("Starting '{0}'".format(SUBTEST_NAME))

        self.waitForGPSd(60)
        MAXTESTS = 4
        errMsg = "lat and lon never found"
        for count in range(1, (MAXTESTS+1)):
            # get one gps block. Unpack the values so that we can allow for tolerance later if necessary.
            gpstext = self.remoteDevice.run("gpxlogger & sleep 3; killall gpxlogger | awk '/<trkpt/,/trkpt>/' | head -10").stdout
            self.logger.info("[Test {0} of {1}]: GPS packet...\n{2}".format(count, MAXTESTS, gpstext))

            if (("lat=" in gpstext)
                and ("lon=" in gpstext)
               ):

                # <trkpt lat="52.376420" lon="4.908241">
                latlong = re.search(r'<trkpt lat="([+-]?[0-9]+.[0-9]+)" lon="([+-]?[0-9]+.[0-9]+)">', gpstext)
                lat = latlong.group(1)
                lon = latlong.group(2)

                self.logger.info("GPS: lat='{0}': lon='{1}'".format(lat, lon))

                difflat = abs(float(lat) - float(CURRENT_LOCATION["lat"]))
                difflon = abs(float(lon) - float(CURRENT_LOCATION["lon"]))

                errMsg = "GPS values not as expected ({0}, {1}): Got: {2}, {3} (Diff: {4:.4f}, {5:.4f}".\
                    format(CURRENT_LOCATION["lat"], CURRENT_LOCATION["lon"], lat, lon, difflat, difflon)

                self.logger.info("GPS difference from reference location. (Diff: {0:.4f}, {1:.4f})".format(difflat, difflon))

                if (difflat <= float(TOLERANCE["lat"])
                and difflon <= float(TOLERANCE["lon"])):

                    self.logger.info("GPS location is within tolerance. (Diff: {0:.4f}, {1:.4f})".format(difflat, difflon))
                    self.VERDICT = True
                    break

        msg = "Did not find GPS coordinates which match the reference location in {0} tests.\n{1}".format(MAXTESTS, errMsg)
        self.assertTrue(count < MAXTESTS, msg)
        self.logger.info("Stopping '{0}'".format(SUBTEST_NAME))


    def getStatusAge(self):
        gpsageText = self.remoteDevice.run("/mnt/userdata/quickfix/ttsystem_sgee_example").stdout.strip()

        gpsText = re.search(r"Navsys GPS - status:([0-9]+) age:(-?[0-9]+)", gpsageText)
        gpsStatus = gpsText.group(1)
        gpsAge = gpsText.group(2)

        gloText = re.search(r"Navsys GLONASS - status:([0-9]+) age:(-?[0-9]+)", gpsageText)
        gloStatus = gloText.group(1)
        gloAge = gloText.group(2)

        return int(gpsStatus), int(gpsAge), int(gloStatus), int(gloAge)


    def testGPSAge(self):
        """
        scp ../../../source/tt_tools/examples/ttsystem_sgee_example root@<camera_ip_address>:/mnt/userdata/ttsystem_sgee_example

        # get the latest gps and glasnost tables from Tomtom. Ensure overwriting the same file.
        wget http://gpsquickfix.services.tomtom.com/fitness/sifglo.f2p3enc.ee
        wget http://gpsquickfix.services.tomtom.com/fitness/sifgps.f2p3enc.ee

        # put the latest tables on the device
        scp sifglo.f2p3enc.ee root@<camera_ip_address>:/mnt/userdata/quickfix/sifglo.f2pxenc.ee
        scp sifgps.f2p3enc.ee root@<camera_ip_address>:/mnt/userdata/quickfix/sifgps.f2pxenc.ee
        """
        SUBTEST_NAME = "testGPSAge"
        self.logger.info("Starting '{0}'".format(SUBTEST_NAME))

        self.remoteDevice.run("factory_reset -l")
        self.logger.info("factory_reset wait for reboot ...")
        rebooted = self.waitReboot(60, self.devAddress, self.locAddress)
        errMsg = "Device did not reboot"
        self.assertTrue(rebooted, errMsg)

        self.waitForGPSd(60)

        ret = subprocess.Popen(["wget http://gpsquickfix.services.tomtom.com/fitness/sifglo.f2p3enc.ee -O sifglo.f2p3enc.ee"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (stdOut, stdErr) = ret.communicate()
        self.logger.info("Fetched latest Glasnost tables: '{0}' '{1}'".format(stdOut, stdErr))

        ret = subprocess.Popen(["wget http://gpsquickfix.services.tomtom.com/fitness/sifgps.f2p3enc.ee -O sifgps.f2p3enc.ee"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (stdOut, stdErr) = ret.communicate()
        self.logger.info("Fetched latest GPS tables: '{0}' '{1}'".format(stdOut, stdErr))

        scp_to_remote(host=self.devAddress, port="22", username=self.USER, password="",
            local_path="../../../source/tt_tools/examples/ttsystem_sgee_example",
            remote_path="/mnt/userdata/quickfix")

        self.remoteDevice.run("chmod +x /mnt/userdata/quickfix/ttsystem_sgee_example")

        (gpsStatus, gpsAge, gloStatus, gloAge) = self.getStatusAge()
        errMsg = "GPS status is expected to start at 0. Got {0}".format(gpsStatus)
        self.assertTrue(gpsStatus == 0, errMsg)

        errMsg = "GLONASS status is expected to start at 0. Got {0}".format(gloStatus)
        self.assertTrue(gloStatus == 0, errMsg)

        # copy the latest gps and glasnost tables to the device and rename
        # the service polls the files every 20 seconds and the status changes to "1" when it sees them
        # the status becomes "3" when it has finished processing - and then the age is valid.
        scp_to_remote(host=self.devAddress, port="22", username=self.USER, password="",
            local_path="sifglo.f2p3enc.ee",
            remote_path="/mnt/userdata/quickfix/sifglo.f2pxenc.ee")

        scp_to_remote(host=self.devAddress, port="22", username=self.USER, password="",
            local_path="sifgps.f2p3enc.ee",
            remote_path="/mnt/userdata/quickfix/sifgps.f2pxenc.ee")

        count = 0
        timeout = 600           # timeout in seconds
        while count < timeout:
            self.logger.info("\n[Test {0}: Waiting for gpsd to process tables]".format(count))
            (gpsStatus, gpsAge, gloStatus, gloAge) = self.getStatusAge()
            self.logger.info("GPS status:{0}, age:{1}".format(gpsStatus, gpsAge))
            self.logger.info("GLONASS status:{0}, age:{1}".format(gloStatus, gloAge))

            time.sleep(1)               # need this delay before reading age again at 1 of 10.

            # stop when both gps status and glosnass status contain "3" or "4"
            # "3" means the ephemeris data is applied and the fix is obtained
            # "4" means the ephemeris data is applied, but the device has no gps signal, so no time is being set
            if (gpsStatus == 3 and gloStatus == 3) or (gpsStatus == 4 and gloStatus == 4):
                self.logger.info("check_gps_age: Files have been processed after {0} Seconds".format(count))
                break

            count += 1

        if count == timeout:
            self.logger.info("check_gps_age: Count has reached the timeout limit {0} Seconds".format(count))
            self.remoteDevice.run("ls -l /mnt/userdata/quickfix")
            self.remoteDevice.run("dmesg | grep -e 'malformed' -e 'discarded'")     # SPI errors on the GPS module?

        errMsg = "GPS status is expected to be 3 or 4. Got {0}".format(gpsStatus)
        self.assertTrue(gpsStatus == 3 or gpsStatus == 4, errMsg)

        errMsg = "GLONASS status is expected to be 3 or 4. Got {0}".format(gpsStatus)
        self.assertTrue(gloStatus == 3 or gloStatus == 4, errMsg)

        if gpsStatus == 3 and gloStatus == 3:
            gpsShadow = gpsAge
            gloRefAge = gloAge
            TestAgeCount = 10
            failed = False
            for count in range(1, TestAgeCount+1):
                self.logger.info("\n[Test {0} of {1}: Verifying that the age only increases]".format(count, TestAgeCount))
                (gpsStatus, gpsAge, gloStatus, gloAge) = self.getStatusAge()
                self.logger.info("GPS status:{0}, age:{1}".format(gpsStatus, gpsAge))
                self.logger.info("GLONASS status:{0}, age:{1}".format(gloStatus, gloAge))

                # if the latest file age is not >= previous file age, then fail. (It may be possible that the age
                # increments by 1,1,0,2 instead of 1 second per second, due to server reading delays)
                if gpsAge < gpsShadow:         # equal is not a failure
                    failed = True
                if gloAge < gloRefAge:
                    failed = True

                if failed:
                    dump = self.remoteDevice.run("cat /tmp/sgee_info_g*").stdout.strip()
                    self.logger.info("Fail: gps tmp files contain {0}".format(dump))

                gpsShadow = gpsAge
                gloRefAge = gloAge

                time.sleep(1)

            if failed:
                errMsg = "GPS / GLONASS age is expected to increase '{0}' '{1}'".format(gpsAge, gpsShadow)
                self.assertTrue(failed, errMsg)
            else:
                self.logger.info("Age values always increased")

        elif gpsStatus == 4 and gloStatus == 4:
            errMsg = "GPS age is expected to be -1"
            self.assertTrue(gpsAge == -1, errMsg)

        else:
            self.logger.info("Got unexpected status: {0} {1}".format(gpsStatus, gloStatus))

        self.logger.info("Stopping '{0}'".format(SUBTEST_NAME))
        self.VERDICT = True


    def gpsvsutc(self):

        timeout = 60
        while timeout > 0:
            # read the device time and the pc time as close together as possible
            devicetime = self.remoteDevice.run("date -u -I'seconds'").stdout.strip()
            pctime = utils.system_output("date -u -I'seconds'", ignore_status=True)

            self.logger.info("Time on the device is: '{0}'".format(devicetime))
            self.logger.info("Time on the PC is    : '{0}'".format(pctime))

            if not (devicetime.startswith("2015-01-01")):
                break
            self.logger.info("Waiting for date to be set: ntpd gets time from gpsd")
            time.sleep(1)
            timeout -= 1

        tPC = time.strptime(pctime, "%Y-%m-%dT%H:%M:%S+0000")
        pcTimestamp = time.mktime(tPC)

        tDevice = time.strptime(devicetime, "%Y-%m-%dT%H:%M:%SUTC")
        deviceTimestamp = time.mktime(tDevice)

        self.logger.info(">>> deviceTimestamp {0}".format(deviceTimestamp))
        self.logger.info(">>> PCTimestamp     {0}".format(pcTimestamp))

        timeStampDiff = abs(deviceTimestamp - pcTimestamp)
        self.logger.info("Time Difference = {0} - {1} = {2}".format(pcTimestamp, deviceTimestamp, timeStampDiff))
        return timeStampDiff


    @unittest.expectedFailure
    def testGPSTimeAfterReset(self):

        SUBTEST_NAME = "testGPSTimeAfterReset"
        self.logger.info("Starting '{0}'".format(SUBTEST_NAME))

        # Ensure the pc time is accurate
        ntpdiff = utils.system_output("ntpsweep --host nl.pool.ntp.org", ignore_status=True)
        self.logger.info("PC clock differs from ntp servers by:\n{0}".format(ntpdiff))

        tdiff = 9999
        REBOOT_COUNT = 5
        LOOP_COUNT = 5
        VALID_DIFF = 5

        for retries in range(REBOOT_COUNT):
            if retries == 0:
                self.remoteDevice.run("factory_reset -l")
                self.logger.info("factory_reset wait for reboot ...")
                self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=False)
            else:
                self.remoteDevice.run("shutdown -r now")
                self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=True)

            self.logger.info("[Loop: {0} of {1}]:".format(retries+1, REBOOT_COUNT))

            # [LBP-1790] see if the date from gps is 16 seconds off from UTC due to leap-seconds
            for count in range(LOOP_COUNT):
                self.logger.info("[Try: {0} of {1}]:".format(count+1, LOOP_COUNT))

                tdiff = self.gpsvsutc()
                if tdiff <= VALID_DIFF:
                    break
                time.sleep(1)
            else:
                self.logger.info("FAIL: Timeout: Device time remained different to PC time")

            if tdiff <= VALID_DIFF:
                self.logger.info("PASS: Device time is within {0} seconds of PC time".format(VALID_DIFF))
            elif tdiff <= 30:   # if gpsd is broken, we might get 17
                devicetime = self.remoteDevice.run("date -u -I'seconds'").stdout.strip()
                msg = "FAIL: Device time is not adjusting for LEAP Seconds correctly. Device time is {0}. Got {1} seconds difference".format(devicetime, tdiff)

                dump = self.remoteDevice.run("cat /tmp/sgee_info_*").stdout.strip()
                self.logger.info("sgee files contain:\n{0}\n".format(dump))

                self.logger.info(msg)
                self.assertTrue(False, msg)
            else:
                devicetime = self.remoteDevice.run("date -u -I'seconds'").stdout.strip()
                msg = "FAIL: Device time difference is unexpected. Device time is {0}. Got {1} seconds difference".format(devicetime, tdiff)
                self.logger.info(msg)
                self.assertTrue(False, msg)

        self.logger.info("Stopping '{0}'".format(SUBTEST_NAME))
        self.VERDICT = True


    # get the gps internal date/time. This vaue is used by ntpd to set the system date/time
    def gpsInternalTime(self):
        internaltime = None
        while internaltime is None:
            gpstime = self.remoteDevice.run("(sleep 1 && killall gpspipe) | gpspipe -w | grep -m 1 TPV", ignore_status=True).stdout.strip()
            # print(">>> gpstime: '{0}'".format(gpstime))
            internaltime = re.search(r'"time":"(.*)"', gpstime)

        time = internaltime.group(1)
        if time.endswith('Z'):
            time = time[:-5]        # remove the ".000Z"
        return time

    # test for LBP-2084
    @unittest.expectedFailure
    def testGPSInternalClock(self):

        VALID_DIFF = 5

        SUBTEST_NAME = "testGPSInternalClock"
        self.logger.info("Starting '{0}'".format(SUBTEST_NAME))

        self.remoteDevice.run("factory_reset -l")
        self.logger.info("factory_reset wait for reboot ...")
        self.waitReboot(60, self.devAddress, self.locAddress, logBootTime=False)

        failFlag = False
        gpsRefTime = 0
        for x in range(120):
            gpsTime = self.gpsInternalTime()
            print(">>>> gpstime is '{0}'".format(gpsTime))

            pctime = utils.system_output("date -u -I'seconds'", ignore_status=True)

            tPC = time.strptime(pctime, "%Y-%m-%dT%H:%M:%S+0000")
            pcTimestamp = time.mktime(tPC)

            tDevice = time.strptime(gpsTime, "%Y-%m-%dT%H:%M:%S")
            gpsTimestamp = time.mktime(tDevice)

            self.logger.info(">>> gpsTimestamp    {0}".format(gpsTimestamp))
            self.logger.info(">>> PCTimestamp     {0}".format(pcTimestamp))

            tDiff = abs(gpsTimestamp - pcTimestamp)
            self.logger.info("Time Difference = {0} - {1} = {2}".format(pcTimestamp, gpsTimestamp, tDiff))

            if tDiff <= VALID_DIFF:
                self.logger.info("PASS: Device time is within {0} seconds of PC time".format(VALID_DIFF))
                break

            if abs(tDiff - gpsRefTime) > VALID_DIFF:
                if gpsRefTime != 0:
                    self.logger.info(">>>> BANG: Time changed, but not the whole date ! '{0}'\n\n".format(gpsTime))
                    failFlag = True
                gpsRefTime = tDiff

        else:
            msg = "Timeout reached: GPS time did not match PC time within the timeout period"
            self.logger.info(msg)
            self.assertTrue(False, msg)

        msg = "GPS internal clock made an intermediate step"
        self.assertFalse(failFlag, msg)

        self.logger.info("Stopping '{0}'".format(SUBTEST_NAME))
        self.VERDICT = True
