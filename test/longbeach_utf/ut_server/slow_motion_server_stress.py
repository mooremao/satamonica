'''
Created on Feb 25, 2015

@author: vinit
'''
import sys
import os
import time
from clients import utils
from common.ServerButTestCase import ServerButTestCase
from clients import videotool_main_app

RECORD_TIME=1
class Test(ServerButTestCase):

    TEST_NAME = "slow_motion_server_stress"
    TEST_DESC = "Testing the slowmotion capture"
    TEST_COUNT = 3      # was 6000

    def setUp(self):
        super(Test, self).setUp()

        """
        Prepare the device for the testcase by restarting application and removing old files.
        """
        self.remoteDevice.run("rm -f /mnt/mmc/DCIM/100TTCAM/*.MP4")
        self.logger.info("Preparing test case '{0}'".format(self.name()))
        self.remoteDevice.run("systemctl stop camera-app")
        self.remoteDevice.run("systemctl start camera-app")


    def tearDown(self):
        """"
        Moving recorded file in slowmotion folder
        """
        self.remoteDevice.run("mkdir -p /mnt/mmc/DCIM/100TTCAM/slowmotion")
        self.remoteDevice.run("mv /mnt/mmc/DCIM/100TTCAM/*.MP4 /mnt/mmc/DCIM/100TTCAM/slowmotion")
        super(Test, self).tearDown()


    def testVideo(self):
        """
        This test case is going to capture slowmotion video
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))
        self.logger.info(self.TEST_DESC)
        videotool_main_app.move_to_ready()
        videotool_main_app.navigate_to_slowmotion_view()
        videotool_main_app.LE()
        videotool_main_app.UP()
        videotool_main_app.RI()
        videotool_main_app.DW()
        videotool_main_app.DW()
        videotool_main_app.UP()

        for i in range(1, self.TEST_COUNT+1):
            self.logger.info("[Test {0} of {1}]".format(i, self.TEST_COUNT))
            videotool_main_app.REC()
            time.sleep(RECORD_TIME)
            videotool_main_app.STOP()
        test_status=self.remoteDevice.run("for file in /mnt/mmc/DCIM/100TTCAM/*.MP4; do if [[ ! -s $file ]]; then echo \"FAIL\"; fi; done").stdout
        self.assertNotEqual(test_status=="FAIL", "Failed since some files are empty (zero size)")

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
