'''
Created on Dec 10, 2014

@author: nila
'''
from common.ServerButTestCase import ServerButTestCase

class Test(ServerButTestCase):
    TEST_NAME = "check_server_cross_opp"
    TEST_DESC = "Testing LBP-481 cross OPP"

    GET_VHDVICP_CMD = "cat /sys/class/regulator/regulator.1/microvolts"
    GET_VCORE_CMD = "cat /sys/class/regulator/regulator.2/microvolts"
    CROSS_OPP_FILE = "/mnt/mmc/crossopp"
    SWITCH_CROSS_OPP = "touch " + CROSS_OPP_FILE
    SWITCH_SINGLE_OPP = "rm -rf " + CROSS_OPP_FILE
    RESTART_SERVICE = "systemctl restart check-crossopp.service"
    """ Single OPP """
    VCORE_SINGLE_OPP = 1320000
    """ Cross OPP """
    VCORE_CROSS_OPP = 1200000
    VHDVICP_CROSS_OPP = 1320000

    def switch_single_opp(self):
        """
        Switch to single opp by creating a specific file on the SD card and reboot the device
        """
        self.logger.info('Switching to single opp for test case ' + self.name())

        """ Switch to single OPP mode """
        self.remoteDevice.run(self.SWITCH_SINGLE_OPP).stdout
        rebooted = self.rebootDutAndWait()
        self.assertTrue(rebooted, "Device did not come back after reboot")

    def switch_cross_opp(self):
        """
        Switch to cross opp by removing a specific file on the SD card and reboot the device
        """
        self.logger.info('Switching to cross opp for test case ' + self.name())

        """ Switch to cross OPP mode """
        self.remoteDevice.run(self.SWITCH_CROSS_OPP).stdout
        rebooted = self.rebootDutAndWait()
        self.assertTrue(rebooted, "Device did not come back after reboot")

    def setUp(self):
        super(Test, self).setUp()

        """
        Prepare the device for the testcase by switching it into single opp mode (default)
        """
        self.logger.info('Preparing test case ' + self.name())

        """ Make sure we are in single OPP mode """
        self.switch_single_opp()

    def tearDown(self):
        """
        Restore device initial status
        Make sure we are in single OPP mode
        """
        self.switch_single_opp()
        super(Test, self).tearDown()

    def testSwitchCrossOpp(self):

        self.logger.info('Starting ' + self.TEST_NAME)
        self.logger.info(self.TEST_DESC)

        """
        Check OPP is single OPP
        """
        single_vcore = self.remoteDevice.run(self.GET_VCORE_CMD).stdout
        single_vhdvicp = self.remoteDevice.run(self.GET_VHDVICP_CMD).stdout

        self.logger.info('SINGLE OPP VCORE is   ' + single_vcore)
        self.logger.info('SINGLE OPP VHDVICP is ' + single_vhdvicp)

        self.assertEqual(int(single_vcore), self.VCORE_SINGLE_OPP, "Failed since vcore for single opp is incorrect")
        self.assertEqual(int(single_vhdvicp), self.VCORE_SINGLE_OPP, "Failed since vhdicp for single opp is incorrect")

        """
        Switch to cross OPP
        """
        self.switch_cross_opp()

        """
        Check OPP is cross OPP
        """
        cross_vcore = self.remoteDevice.run(self.GET_VCORE_CMD).stdout
        cross_vhdvicp = self.remoteDevice.run(self.GET_VHDVICP_CMD).stdout

        self.logger.info('CROSS OPP VCORE is   ' + cross_vcore)
        self.logger.info('CROSS OPP VHDVICP is ' + cross_vhdvicp)

        self.assertEqual(int(cross_vcore), self.VCORE_CROSS_OPP, "Failed since vcore for cross opp is incorrect")
        self.assertEqual(int(cross_vhdvicp), self.VHDVICP_CROSS_OPP, "Failed since vhdicp for cross opp is incorrect")
        self.VERDICT = True
