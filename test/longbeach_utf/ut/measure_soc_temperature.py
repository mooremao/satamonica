'''
Created on Oct 2, 2014

@author: segura
'''

from common import ButTestCase
from clients import utils

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ButTestCase.ButTestCase):

    TEST_NAME = "measure_soc_temperature"
    TEST_DESC = "Testing Temperature range"

    MAX_TEMP = 95.0
    MIN_TEMP = 10.0

    # if you don't want any local setUp or tearDown, these are optional
    # Note that setUp is run before each sub-test and tearDown after each sub-test.
    '''
    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        self.logger.info('local setup goes here - after parent setup')

    def tearDown(self):
        self.logger.info('local teardown goes here - before parent teardown')
        ButTestCase.ButTestCase.tearDown(self)
    '''

    def testMeasure(self, result=None):
        """
        This test case is going to check the temperature of a LongBeach device
        Falling through all the asserts and setting VERDICT = True marks the test as a pass
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Measuring the Fuel Gauge Temperature')

        soc_name = utils.system_output('cat /sys/bus/i2c/devices/3-0072/name', retain_output=True)
        self.assertTrue(soc_name=="tmp103", "SOC Temperature device name expected to be tmp103. Got {0}".format(soc_name))

        soc_temperature = utils.system_output('cat /sys/bus/i2c/devices/3-0072/temp1_input', retain_output=True)
        self.assertTrue(soc_temperature != None, "Failed path output does not exist.")
        self.logger.info('Output of SOC Temperature sensor is ' + soc_temperature)
        self.logger.info('SOC Temperature Measured is {0}C'.format((int(soc_temperature) / 1000)))

        # SOC Temperature is represented in 1/1000 (mC)
        soc_temperature_formatted = float(soc_temperature) / 1000
        strError = "Failed since the SOC temperature is '{0}'C (min {1}, max {2})".format(soc_temperature_formatted, self.MIN_TEMP, self.MAX_TEMP)
        self.assertTrue((soc_temperature_formatted <= self.MAX_TEMP) and (soc_temperature_formatted >= self.MIN_TEMP), strError)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

    '''
    Define more tests. Anything of the format 'test<something>' will be executed in alphabetical order.
    Each test has the setUp run before and tearDown run after.
    Do not use try / except. The unitTest harness catches those.
    '''
    '''
    def testA(self, result=None):
        self.logger.info('Test something else A')

        #self.VERDICT = True
        if self.VERDICT == False:
            self.fail("testA fails")

    def testZ(self, result=None):
        self.logger.info('Test something else Z')

        self.VERDICT = True
    '''
