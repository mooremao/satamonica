'''
Created on Nov 6, 2014

@author: Alberto Segura
'''
from common import ButTestCase
from clients import utils
import commands
import os

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_sdcard_status"
    SD_CARD_STATUS_COMMAND = "check_sdcard.sh"
    LOG_FILE = "check_sdcard_status"
    TEST_DESC = "Testing SD Card Status"           

    def testSDCard(self, result=None):
        """
        This test case is going to check the correct functioning of the backlight on the LongBeach device

        self.setlooger
        logging.basicConfig(level=logging.INFO)
        output_dir = "/mnt/mmc/test/output/"
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")

        # 0=card present, 1=card not present, 2=card failed to mount, 3=card is readonly
        status = commands.getstatusoutput(self.SD_CARD_STATUS_COMMAND)
        self.logger.info('The status from the command is %d', os.WEXITSTATUS(status[0]))
        self.logger.info('The text output by the command is %s', status[1])

        """
        Check the result
        """
        statusOutput = os.WEXITSTATUS(status[0])
        strError = "value of 0 is a pass. 1..3 are errors. Got {0}".format(statusOutput)
        if (statusOutput==1):
            strError =  "Failed since sdcard is not present"
        elif (statusOutput==2):
            strError =  "Failed since sdcard failed to mount"
        elif (statusOutput == 3):
            strError =  "Failed since sdcard is readonly"

        self.assertEqual(statusOutput, 0, strError)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
