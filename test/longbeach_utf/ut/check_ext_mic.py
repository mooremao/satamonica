'''
Created Jan 27, 2015 from template.py

@author: Andy Stout
'''
from common import ButTestCase
from clients import utils
import re

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ButTestCase.ButTestCase):

    # This text MUST match the filename, excluding the .py
    TEST_NAME = "check_ext_mic"
    TEST_DESC = "Check that the external mic can be turned on and off"

    # amixer name for the external mic
    MIC_DEVICENAME = "Mic Bias 2 Voltage"


    def tearDown(self):
        # after each test, ensure the mic is turned off, to save power
        state = self.set_ext_mic_state("off")

        ButTestCase.ButTestCase.tearDown(self)

    '''
    Setting the state also returns the new state
    '''
    def set_ext_mic_state(self, newstate):
        mic_state = utils.system_output("amixer sset '" + self.MIC_DEVICENAME + "' " + newstate + " | grep Item0:", retain_output=True)
        state = re.match("  Item0: '(.*)'", mic_state)  # state is the word after Item0 in single quotes
        if state == None:
            state = ""
        else:
            state = state.group(1)
        return state


    def testExtMicOff(self, result=None):
        """
        Test mic can be turned off
        """
        self.logger.info("Starting 'testExtMicOff'")

        state = self.set_ext_mic_state("off")
        self.assertEqual(state, "off")

        self.logger.info("Starting 'testExtMicOff'")
        self.VERDICT = True


    def testExtMic2V(self, result=None):
        """
        Test mic can be set to 2V
        """
        self.logger.info("Starting 'testExtMic2V'")

        state = self.set_ext_mic_state("2V")
        self.assertEqual(state, "2V")

        self.logger.info("Stopping 'testExtMic2V'")
        self.VERDICT = True


    def testExtMic2V5(self, result=None):
        """
        Test mic can be set to 2.5V
        """
        self.logger.info("Starting 'testExtMic2.5V'")

        state = self.set_ext_mic_state("2.5V")
        self.assertEqual(state, "2.5V")

        self.logger.info("Stopping 'testExtMic2.5V'")
        self.VERDICT = True


    def testExtMicAVDD(self, result=None):
        """
        Test mic can be set to AVDD
        """
        self.logger.info("Starting 'testExtMicAVDD'")

        state = self.set_ext_mic_state("AVDD")
        self.assertEqual(state, "AVDD")

        self.logger.info("Stopping 'testExtMicAVDD'")
        self.VERDICT = True
