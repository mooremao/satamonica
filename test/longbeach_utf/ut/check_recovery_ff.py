'''
Created on Nov 13, 2014

@author: Jeroen Jensen
'''
from common import ButTestCase
from clients import utils

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_recovery_ff"
    TEST_DESC = "Checking for recovery flip flop is cleared during boot"

    GET_RECOVERY_FF = "cat /sys/devices/platform/flipflop.0/value"
    SET_RECOVERY_FF_CMD = "echo 1 > /sys/devices/platform/flipflop.0/value"
    SET_RECOVERY_UPDATE_FF_CMD = "echo 2 > /sys/devices/platform/flipflop.0/value"
    CLEAR_RECOVERY_UPDATE_CMD = "echo 0 > /sys/devices/platform/flipflop.0/value"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)

    def tearDown(self):
        """
        Restore device initial status
        """
        utils.system(self.CLEAR_RECOVERY_UPDATE_CMD, ignore_status=False)
        ButTestCase.ButTestCase.tearDown(self)

    def testRecoveryFF(self, result=None):
        """
        Testing recovery flip flop is cleared during boot
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)

        """
        Verify output of cat /sys/devices/platform/flipflop.0/value
        """
        sysfs_value = utils.system_output(self.GET_RECOVERY_FF, ignore_status=False, retain_output=True)
        self.assertEqual(sysfs_value, "0", "Flip flop not cleared during boot")

        utils.system(self.SET_RECOVERY_FF_CMD, ignore_status=False)

        sysfs_value = utils.system_output(self.GET_RECOVERY_FF, ignore_status=False, retain_output=True)
        self.assertEqual(sysfs_value, "1", "Recovery flip flop not set to recovery state")

        utils.system(self.SET_RECOVERY_UPDATE_FF_CMD, ignore_status=False)

        sysfs_value = utils.system_output(self.GET_RECOVERY_FF, ignore_status=False, retain_output=True)
        self.assertEqual(sysfs_value, "2", "Recovery flip flop not set to update state")

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
