'''
Created on Oct 23, 2014

@author: Niels Langendorff
'''

from common import ButTestCase
from clients import utils
import re


class Test(ButTestCase.ButTestCase):

    CMD = "fdtquery -I /dev/fdtexport /features/device-serial"
    BAD_CMD = "fdtquery -I /dev/fdtexport /features/device-seria"  # invalid command
    LOG_FILE = "check_fdt_serial.log"
    TEST_DESC = "LBP-388 - Testing FDT serial"
    TEST_NAME = "check_fdt_serial"
    VERDICT = False


    def testFdtSerial(self, result=None):
        """
        check the fdt serial number from fdtquery
        """
        self.logger.info("Starting 'testFdtSerial'")

        # define the correct serial number format, 12 digits in total. Note the 'A' in the middle.
        prog = re.compile("N[A-C][0-9]{4}A[0-9]{5}")

        # verify that fdtquery with an invalid input returns empty string
        fdt_serial = utils.system_output(self.BAD_CMD, retain_output=True, ignore_status=True)
        errMsg = "fdtserial was expected to return an empty string on invalid request. Got '{0}'".format(fdt_serial)
        self.assertTrue(fdt_serial == "", errMsg)

        fdt_serial = utils.system_output(self.CMD, retain_output=True, ignore_status=True)
        self.logger.info("fdtquery returned '{0}'".format(fdt_serial))

        errMsg =  "serial number from fdtquery is not correct. Got '{0}'".format(fdt_serial)
        self.assertTrue(prog.match(fdt_serial), errMsg)

        self.logger.info("Stopping 'testFdtSerial'")
        self.VERDICT = True


    def testdeviceinfo(self, result=None):
        """
        check the fdt serial number from deviceinfo_example
        """
        self.logger.info("Starting 'testdeviceinfo_example'")

        # define the correct serial number format, 12 digits in total. Note the 'A' in the middle.
        prog = re.compile("N[A-C][0-9]{4}A[0-9]{5}")

        deviceinfoSerial = utils.system_output("deviceinfo_example device-id", retain_output=True, ignore_status=True)
        self.logger.info("deviceinfo_example returned '{0}'".format(deviceinfoSerial))

        errMsg = "serial number from deviceinfo_example is not correct. Got: '{0}'".format(deviceinfoSerial)
        self.assertTrue(prog.match(deviceinfoSerial), errMsg)

        self.logger.info("Stopping 'testdeviceinfo-example'")
        self.VERDICT = True


    def testSerialsMatch(self):
        """
        check that fdt serial number matches deviceinfo_example serial number
        """
        self.logger.info("Starting 'testSerialsMatch'")

        deviceinfoSerial = utils.system_output("deviceinfo_example device-id", retain_output=True, ignore_status=True)
        self.logger.info("deviceinfo_example returned '{0}'".format(deviceinfoSerial))

        fdt_serial = utils.system_output(self.CMD, retain_output=True, ignore_status=True)
        self.logger.info("fdtquery returned '{0}'".format(fdt_serial))

        self.assertEqual(fdt_serial, deviceinfoSerial)

        self.logger.info("Stopping 'testSerialsMatch'")
        self.VERDICT = True
