'''
Created on Nov 18, 2014

@author: Jeroen Jensen
'''
from common import ButTestCase
from clients import utils

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_firstboot_done"
    TEST_DESC = "Checking first boot handling is done"

    DIFF_LAST_BOOT_VERSION_FILE_CMD = "diff /mnt/userdata/lbp-lastboot-version /etc/lbp-version"


    def testFirstbootDone(self):
        """
        Testing first boot handling is done
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")

        """
        Diff /mnt/userdata/lbp-lastboot-version against /etc/lbp-version
        First boot copies /etc/lbp-version to /mnt/userdata/lbp-lastboot-version so the files
        should be the same. If not, first boot handling is not performed.
        
        diff command exit code is zero when both files are the same
        """
        utils.system(self.DIFF_LAST_BOOT_VERSION_FILE_CMD, ignore_status=False)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
