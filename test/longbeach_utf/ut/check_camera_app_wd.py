'''
Created on Dec 2, 2014

@author: Jeroen Jensen
'''

from common import ButTestCase
from clients import utils
import time

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_camera_app_wd"
    TEST_DESC = "Checking whether SW watchdog functionality of camera-app service works"

    SEND_SIGUSR_TO_CAMERA_APP_CMD = "systemctl kill --signal=SIGUSR1 camera-app.service"
    CAMERA_APP_STARTED_SINCE_CMD = "systemctl show camera-app.service | grep ExecMainStartTimestamp"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    CAMERA_APP_START_CMD = "systemctl start camera-app.service"
    LOG_FILE = "check_camera_app_wd.log"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Setup initial device status
        """
        utils.system(self.RESET_FAILED_CMD, ignore_status=True)

    def tearDown(self):
        """
        Restore device initial status
        """
        utils.system(self.RESET_FAILED_CMD, ignore_status=True)
        utils.system(self.CAMERA_APP_START_CMD, ignore_status=True)
        ButTestCase.ButTestCase.tearDown(self)

    def testWatchdog(self):
        """
        This test case is going to check whatever SW watchdog functionality of camera-app services works
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info(self.TEST_DESC)

        start_since_before = utils.system_output(self.CAMERA_APP_STARTED_SINCE_CMD, retain_output=True, ignore_status=True)

        utils.system(self.SEND_SIGUSR_TO_CAMERA_APP_CMD, ignore_status=True)

        timeout = 25 # Watchdog trigger is 10 seconds. So 25 seconds should be enough.
        while timeout > 0:
            time.sleep(1)
            timeout -= 1
            start_since_now = utils.system_output(self.CAMERA_APP_STARTED_SINCE_CMD, retain_output=True, ignore_status=True)
            if start_since_now != start_since_before:
                break

        self.assertNotEqual(start_since_before, start_since_now, "camera-app.service not restarted due to SIGUSR1 signal")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
