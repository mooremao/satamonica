'''
This test script is for checking if camera can record all sensor video modes successfully.Currently the video recording has to be done manually and in sequential order.

Created on Nov 12,2014
@author:Raghuraman
'''


from common import ButTestCase
from clients import utils
from clients import videotool
import clients.video_parse as p
import os
import sys
import time
import platform
import uinput
RECORD_TIME=1  #Recording time.
IN_BITRATE=100 #-1,60%,80%,100%,120%... -1 for all

class Test(ButTestCase.ButTestCase):
    TEST_DIR = "/mnt/mmc/"
    DURATION = 20
    TEST_NAME = "check_video_modes"
    TEST_DESC = "Testing all the different video modes"
    mypath ='/mnt/mmc/Generated_streams/'
    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Inserting uinput driver
        """
        self.device=videotool.start()
        self.logger.info('uinput device is created')
        self.fp = open("../config_files/check_video_modes.txt", "r+")

    def tearDown(self):
        self.fp.close()
        self.logger.info('demo app restart and uinput driver removed')
        videotool.stop(self.device)
        self.logger.info('uinput device deleted')
        ButTestCase.ButTestCase.tearDown(self)

    def testVideo(self, result=None):
        self.logger.info('Starting ' + self.TEST_NAME)
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Checking video modes')

        # List of filenames are stored in sequential order ie TTCamVideo01.h264 always corresponds to 1920*1080*60_Wide mode and TTCamvideo23.h264 always corresponds to WVGA_150fps_Wide
        #Calling fucntion to record video in all sensor mode.
        videotool.config_record(self,self.device,RECORD_TIME,IN_BITRATE)
        for line in self.fp:  # Call the analysis functions on each video file
            filename = line.strip()
            self.logger.info("Testing "+filename)
            self.assertTrue(os.path.getsize(self.TEST_DIR + filename) > 4096, "'{0}' File is corrupt / not large enough".format(self.TEST_DIR + filename))
            framerate, width, height, framecount, bitrate, profile = p.parse(filename, self.DURATION)
            self.logger.info("parsed: Got: {0}, {1}, {2}, {3}, {4}, {5}".format(framerate, width, height, framecount, bitrate, profile))
            check_resolution_framerate_count(self, filename, height, width, framerate, framecount, bitrate, self.DURATION)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True


def check_resolution_framerate_count(self, filename, height, width, framerate, framecount, bitrate, duration=20):
    # STEP 1: GET THE REFERENCE VALUES
    reference = [[1,1920, 1080, 60,10, "W"], [2, 1920, 1080, 60,10, "N"], [3, 1920, 1080, 50,10, "W"], [4, 1920, 1080, 50,10, "N"], [5, 1920, 1080, 30,10, "W"], [6, 1920, 1080, 30,10, "N"], [7, 1920, 1080, 25,10, "W"], [8, 1920, 1080, 25,10, "N"], [9, 3840, 2160, 15,10, "W"], [10, 3840, 2160, 12.5,10, "W"], [11, 2704, 1524, 30,10, "W"], [12, 2704, 1524, 25,10, "W"], [13, 2704, 1524, 25,10, "N"], [14, 1280, 720, 120,10, "W"], [15, 1280, 720, 120,10, "N"], [16, 1280, 720, 100,10, "W"], [17, 1280, 720, 100,10, "N"], [18, 1280, 720, 60,10, "W"], [19, 1280, 720, 60,10, "N"], [20, 1280, 720, 50,10, "W"], [21, 1280, 720, 50,10, "N"], [22, 848, 480, 180,10, "W"], [23, 848, 480, 150,10, "W"]]  # References for each sensormodes
    name = filename.split(".")
    number = name[0].split("o")
    fno = int(number[1])

    ref_wd = int(reference[fno - 1][1])
    ref_ht = int(reference[fno - 1][2])
    ref_fr = float(reference[fno - 1][3])
    ref_br = float(reference[fno - 1][4])
    FOV = reference[fno - 1][5]
    # ref_fc=ref_fr * duration #Will be used for framecount comparison

    # STEP 2:COMPARE THE OUTPUT VALUES TO REFERENCES
    self.assertEqual(int(width)==int(ref_wd), "%dx%d@%d@%dmbps_%s Test Failed: Width doesn't match. Got %d Expected %d" % (ref_wd, ref_ht, ref_fr, ref_br, FOV, width, ref_wd))
    self.assertEqual(int(height)==int(ref_ht), "%dx%d@%d@%dmbps_%s Test Failed: Height doesn't match. Got %d Expected %d" % (ref_wd, ref_ht, ref_fr, ref_br, FOV, height, ref_ht))
    self.assertEqual(int(framerate)==int(ref_fr), "%dx%d@%d@%dmbps_%s Test Failed: Framerate doesn't match. Got %d Expected %d" % (ref_wd, ref_ht, ref_fr, ref_br, FOV, framerate, ref_fr))
