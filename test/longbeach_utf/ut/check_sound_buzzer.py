'''
Created on Nov 18, 2014

@author: Jay Albertoe
'''
from common import ButTestCase
from clients import utils

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_sound_buzzer"
    TEST_DESC = "Testing sounding the buzzer"

    BZ_FREQUENCY = 2700
    BZ_ON = "echo %d > /sys/devices/platform/pwm-beeper/tune" % BZ_FREQUENCY
    BZ_OFF = "echo 0 > /sys/devices/platform/pwm-beeper/tune"
    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)

        """
        Setup initial device status
        """
        utils.system(self.STOP_APP, ignore_status=True)

    def tearDown(self):
        """
        Restore device initial status
        """
        utils.system(self.RESET_FAILED_CMD, ignore_status=True)
        utils.system(self.START_APP, ignore_status=True)
        ButTestCase.ButTestCase.tearDown(self)

    def testBuzzer(self):
        """
        This test case is going to check the correct functioning of the sound-buzzer on the LongBeach device
        This only checks if the sysfs entry is valid and writing to the sysfs entry works. There is no
        other way to verify that the buzzer is actual working. Even reading the sysfs entry is not allowed.
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")

        """
        Turn off buzzer
        Turn on buzzer
        """
        utils.system(self.BZ_OFF, ignore_status=False)
        utils.system(self.BZ_ON, ignore_status=False)
        utils.system(self.BZ_OFF, ignore_status=False)
        utils.system(self.BZ_ON, ignore_status=False)
        utils.system(self.BZ_OFF, ignore_status=False)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
