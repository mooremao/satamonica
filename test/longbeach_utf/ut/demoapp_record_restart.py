from clients import utils
from clients import videotool
from common import ButTestCase
import os
import sys
import time

RECORD_TIME=10
LOOP=3

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "demoapp_record_restart"
    TEST_DESC = "Record after each demo app restart."

    mypath ='/mnt/mmc/Generated_streams/'


    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Inserting uinput driver
        """
        self.device=videotool.start()
        if not os.path.isdir(self.mypath):
            os.makedirs(self.mypath)
        self.logger.info('uinput device is created')


    def tearDown(self):
        self.logger.info('demo app restart and uinput driver removed')
        videotool.stop(self.device)
        self.logger.info('uinput device deleted')
        ButTestCase.ButTestCase.tearDown(self)


    def testrecord(self, result=None):
        """
        Record after each demo app restart. ("reset-failed camera-app" happens in the parent setUp and tearDown)
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Recording')

        Result="PASS"
        for i in range(1, LOOP+1):
            self.logger.info("testrecord: Loop [{0} of {1}]".format(i, LOOP))

            INPUT_SETTING="1920_1080_60_Wide_APP_RESTART"+str(i)
            videotool.record_m(self.device, INPUT_SETTING, "10", 0, 0, RECORD_TIME, "Normal_load")
            time.sleep(5)        # wait for file write to complete

            filename="TTCamVideo01_1920_1080_60_Wide_APP_RESTART"+str(i)
            num_list=[f for f in os.listdir(self.mypath) if f.endswith('.h264') and f.startswith(filename)]
            if (os.path.isfile(self.mypath + num_list[0])):
                statinfo = os.stat(self.mypath + num_list[0])
                self.logger.info("'{0}' is a file of {1} bytes".format(num_list[0], statinfo.st_size))
                if (statinfo.st_size == 0):
                    Result="FAIL"

            else:
                self.logger.info("'{0}' is not a file".format(num_list[0]))
                Result="FAIL"

        self.assertTrue(Result == "PASS", "Recording Fail:File size zero")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
