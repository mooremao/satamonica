"""
Created on Dec 3, 2014

@author: Komal Padia
"""

from common import ButTestCase
from clients import utils
import time

# sync is in python v3.3 and later
import ctypes
libc = ctypes.CDLL("libc.so.6")
def sync():
    libc.sync()


MIN_SLEEP_RANGE = 5
MAX_SLEEP_RANGE = 17

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_deepsleep"
    TEST_DESC = "Testing Deep Sleep mode"

    DEEPSLEEP_COMMAND = "echo mem > /sys/power/state"
    DEEPSLEEP_MSG = "\"PM: late suspend of devices complete\""
    WAKEUP_MSG = "\"PM: resume of devices complete\""
    DMSG_GREP_COMMAND = "dmesg | grep "

    LOG_FILE = "check_deepsleep.log"

    # The time spent asleep may be less, but must never be longer than the requested amount
    def deepSleep(self, timeToSleep):
        self.logger.info("{0} Sleeping for {1} seconds".format(self.TEST_DESC, timeToSleep))
        """
        Clear dmesg
        Set up wake up timer
        sync the filesystem
        Enter deep sleep
        On wake up, check dmesg for deep sleep logs
        """
        utils.system_output("dmesg -c", retain_output=False)                    # empty the log

        SET_WAKEUP_TIMER_COMMAND = "echo {0} > /sys/class/power_supply/tomtom_mcu/device/wakeup_timeout".format(timeToSleep)
        utils.system(SET_WAKEUP_TIMER_COMMAND, ignore_status=False)

        # sync the filesystem, so that we are not timing the non-determinstic filesystem sync within sleep()
        sync()

        tStart = time.time()
        utils.system(self.DEEPSLEEP_COMMAND, ignore_status=False)
        tEnd = time.time()

        tDiff = tEnd - tStart
        self.logger.info("deepspeep({0}): tStart = {1}, tEnd = {2}, tDiff = {3}".format(timeToSleep, tStart, tEnd, tDiff))

        deepsleep = utils.system_output(self.DMSG_GREP_COMMAND + self.DEEPSLEEP_MSG, retain_output=True)  # grep the log
        wakeup = utils.system_output(self.DMSG_GREP_COMMAND + self.WAKEUP_MSG, retain_output=True)

        self.logger.info("Deep Sleep state: {0}".format(deepsleep))
        self.logger.info("Wake up state:    {0}".format(wakeup))

        """
        Check the result
        """
        minSleepAllowed = timeToSleep - 2.0
        maxSleepAllowed = timeToSleep + 0.2     # allow some slack for the 'echo' and python overhead

        msg = "Time spent in deepsleep({3}) needs to be in range ({0:.2f} to {1:.2f}). Got {2:.3f}".format(minSleepAllowed, maxSleepAllowed, tDiff, timeToSleep)
        self.assertTrue(minSleepAllowed <= tDiff <= maxSleepAllowed, msg)

        strError = "Failed since we did not find deep sleep and wake up logs"
        self.assertTrue((deepsleep != "" and wakeup != ""), strError)

        self.VERDICT = True

'''
Create dynamically named test cases
'''
def _add_test(sleepTime):
    def test_method(self):
        self.deepSleep(sleepTime)

    testName = "test_{0:02d}".format(sleepTime)
    setattr(Test, testName, test_method)
    test_method.__name__ = testName

for delay in range(MIN_SLEEP_RANGE, MAX_SLEEP_RANGE+1):
    _add_test(delay)
