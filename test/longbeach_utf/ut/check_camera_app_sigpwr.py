'''
Created on Oct 31, 2014

@author: Jeroen Jensen
'''
from common import ButTestCase
from clients import utils
import time

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_camera_app_sigpwr"
    TEST_DESC = "Checking SIGPWR handing by camera-app.service (demoapp)"

    SEND_SIGGPWR_CMD = "systemctl kill --signal=SIGPWR camera-app.service"
    STOP_CAMERA_APP_CMD = "systemctl stop camera-app.service"
    START_CAMERA_APP_CMD = "systemctl start camera-app.service"
    GET_STATUS_CMD = "systemctl --no-pager -l status camera-app.service"
    CHECK_FOR = "Battery critical low! Not shutting down; systemd will trigger poweroff"
    LOG_FILE = "check_camera_app_sigpwr.log"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)

    def tearDown(self):
        """
        Restore camera-app.services initial status
        """
        utils.system(self.START_CAMERA_APP_CMD, ignore_status=True)
        ButTestCase.ButTestCase.tearDown(self)

    # Return how many times <string> occurs in the log file
    def countStringinLog(self, string):
        lines = utils.system_output("grep '{0}' /var/log/all/current".format(string), ignore_status=True).splitlines()
        count = len(lines)          # may be zero
        return count

    def testSigpwr(self, result=None):
        """
        check the camera-app accepts SIGPWR signal
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info(self.TEST_DESC)

        self.flushLog()     # ensure the log file on disk is up to date

        # count how many times in the log file before and after sending SIGPWR
        beforeCount = self.countStringinLog(self.CHECK_FOR)
        self.logger.info("String found {0} times BEFORE SIGPWR".format(beforeCount))

        utils.system(self.SEND_SIGGPWR_CMD, ignore_status=True)
        utils.system(self.STOP_CAMERA_APP_CMD, ignore_status=True)

        self.flushLog()     # ensure the log file on disk is up to date

        afterCount = self.countStringinLog(self.CHECK_FOR)
        self.logger.info("String found {0} times AFTER SIGPWR".format(afterCount))

        errMsg = "Camera-app service not stopped due to SIGPWR signal. Got '{0}' before, and '{1}' after\n\n".format(beforeCount, afterCount)
        self.assertTrue(afterCount == beforeCount +1,errMsg)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
