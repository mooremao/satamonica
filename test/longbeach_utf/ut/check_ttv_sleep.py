'''
Created May 19, 2015 from check_gps.py

@author: Andy Stout
'''
import time
from datetime import datetime
from common import ButTestCase
from clients import utils

# sync is in python v3.3 and later
import ctypes
libc = ctypes.CDLL("libc.so.6")
def sync():
    libc.sync()


# See https://docs.python.org/2/library/unittest.html for more information
class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_ttv_sleep"
    TEST_DESC = "Verify ttv_sleep works at different CPU speeds"
    SLEEP_TIME = 5000                   # time in milliseconds
    MIN_SLEEP = SLEEP_TIME - 2000       # minimum valid expected time (3.1S was found experimentally)
    MAX_SLEEP = SLEEP_TIME              # maximum valid expected time (fail if more than this value)


    def readCPUSpeed(self):
        speed = utils.system_output("cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq", ignore_status=True)
        self.logger.info("CPU speed is '{0}'".format(speed))
        return speed


    def writeCPUSpeed(self, speed):
        self.logger.info("Setting CPU speed to '{0}'".format(speed))
        utils.system_output("echo '{0}' > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed".format(speed), ignore_status=True)


    def readCPUGovernor(self):
        gov = utils.system_output("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", ignore_status=True)
        self.logger.info("CPU governor is '{0}'".format(gov))
        return gov


    def writeCPUGovernor(self, gov):
        self.logger.info("Setting CPU governor to '{0}'".format(gov))
        utils.system_output("echo '{0}' > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor".format(gov), ignore_status=True)


    def runttv_sleep(self, speed):
        self.writeCPUSpeed(speed)
        readbackspeed = self.readCPUSpeed()
        if readbackspeed != speed:
            self.logger.info("CPU speed not same as written. Set to '{0}', got '{1}'".format(speed, readbackspeed))

        # sync the filesystem, so that we are not timing the filesystem writes
        self.logger.info("sync filesystem: ")
        sync()
        self.logger.info("sync filesystem: Done")

        startTime = datetime.now()
        status = utils.system_output("/mnt/userdata/ttv_sleep_example {0}".format(self.SLEEP_TIME),
                                     retain_output=False, ignore_status=True, verbose=False)
        endTime = datetime.now()
        diffTime = endTime - startTime

        self.logger.info("ttv_sleep_example returned '{0}' Time taken = {1}".format(status, diffTime))
        millidiff = diffTime.total_seconds()*1000    # want milliseconds
        status = False
        # verify that the time taken is between n and n-1 seconds
        if millidiff > self.MAX_SLEEP:
            self.logger.info("FAIL: CPU at '{0}'. ttv_sleep({1}) delay too long. Got {2} (Above {3})".format(speed, self.SLEEP_TIME, millidiff, self.MAX_SLEEP))
        elif millidiff < self.MIN_SLEEP:
            self.logger.info("FAIL: CPU at '{0}'. ttv_sleep({1}) delay too short. Got {2} (Below {3})".format(speed, self.SLEEP_TIME, millidiff, self.MIN_SLEEP))
        else:
            self.logger.info("PASS: ttv_sleep({0}) delay about right".format(self.SLEEP_TIME))
            status = True
        return status, millidiff


    def setUp(self):
        super(Test, self).setUp()

        self.originalCPUGovernor = self.readCPUGovernor()
        self.logger.info("Old CPU governor = '{0}'".format(self.originalCPUGovernor))
        self.originalCPUSpeed = self.readCPUSpeed()
        self.logger.info("Old CPU speed = '{0}'".format(self.originalCPUSpeed))
        self.writeCPUGovernor("userspace")          # can't change cpu speed without this


    def tearDown(self):
        self.logger.info("Restore CPU speed to original '{0}'".format(self.originalCPUSpeed))
        self.writeCPUSpeed(self.originalCPUSpeed)
        self.logger.info("Restore CPU governor to original '{0}'".format(self.originalCPUGovernor))
        self.writeCPUGovernor(self.originalCPUGovernor)
        super(Test, self).tearDown()


    def testttvsleep(self):
        """
        check that ttv_sleep works at default speed
        """
        SUBTEST_NAME = "check_ttv_sleepDefault"
        self.logger.info("Starting '{0}'".format(SUBTEST_NAME))

        failList = []
        # cpu speeds are in kHz
        cpuSpeedList = [   "0",     # below min speed
                      "100000",     # 100 MHz (minimum)
                      "200000",
                      "300000",
                      "370000",
                      "400000",
                      "500000",
                      "600000",
                      "700000",
                      "800000",
                      "900000",
                      "970000",     # 970 MHz
                      "999999",
                     "1000000",     # 1 GHz
                     ]

        for sleepVariation in [5, 0]:           # test with these pauses between calls to deepsleep()
            for speed in cpuSpeedList:
                (status, mdiff) = self.runttv_sleep(speed)
                if not status:
                    failList.append([speed, mdiff])
                self.logger.info("Delay between calls to deepsleep() {0}S".format(sleepVariation))
                time.sleep(sleepVariation)        # TESTING: put a gap between calls to deepsleep, otherwise not tired!

            # VERDICT defaults to False in the parent setup
            msg = "sleep({0}) ".format(self.SLEEP_TIME)
            if len(failList) == 0:
                self.VERDICT = True
                msg += "Passed at all CPU speeds"
            elif len(failList) == 1:
                msg += "Failed at the following CPU [speed, time] '{0}'. With delay between deepsleep() of {1}S".format(failList, sleepVariation)
                self.VERDICT = False
            else:
                msg += "Failed at the following CPU speeds [speed, time] '{0}'. With delay between deepsleep() of {1}S".format(failList, sleepVariation)
                self.VERDICT = False

            self.logger.info(msg)

        self.assertTrue(self.VERDICT, msg)
        self.logger.info("Stopping '{0}'".format(SUBTEST_NAME))
