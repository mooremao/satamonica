from clients import utils
from clients import videotool
from common import ButTestCase
import os
import sys
import time
import platform
import uinput

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "still_test"
    TEST_DESC = "still capture testing"
    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    mypath ='/mnt/mmc/Generated_streams/'

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Inserting uinput driver
        """
        self.device=videotool.start()
        self.logger.info('uinput device is created')

    def tearDown(self):
        self.logger.info('demo app restart and uinput driver removed')
        videotool.stop(self.device)
        self.logger.info('uinput device deleted')
        ButTestCase.ButTestCase.tearDown(self)

    def testrecord(self, result=None):
        """
        This test case is going to capture still image in demo app
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Recording')
        Result=videotool.still_image(self,self.device)
        self.assertTrue(Result=="PASS","Still Capture fail:File size zero.")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
