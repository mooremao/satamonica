'''
Created on Oct 2, 2014

@author: segura
'''

from clients import utils
from common import ButTestCase

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "measure_temperature_sensor"
    TEST_DESC = "Testing Temperature Sensors"

    MAX_TEMP = "20"
    MIN_TEMP = "10"

    def testAmbientTemperatureSensorMeasure(self, result=None):
        """
        This test case is going to check the temperature of a LongBeach device
        """
        self.logger.info('Starting ' + self.TEST_NAME)
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Measuring the Ambient  Temperature')

        tmp_name = utils.system_output('cat /sys/bus/i2c/devices/4-0071/name', retain_output=True)
        self.assertTrue(tmp_name=="tmp103", "Temperature device name expected to be tmp103. Got {0}".format(tmp_name))

        ambient_temperature = utils.system_output('cat /sys/bus/i2c/devices/4-0071/temp1_input', retain_output=True)
        self.assertTrue (ambient_temperature != None, "Failed path output does not exist.")

        self.logger.info('Output of Ambient Temperature sensor is ' + ambient_temperature)
        strTemp = 'Ambient Temperature Measure (Celsius degrees) is ' + str(int(ambient_temperature) / 1000)
        self.logger.info(strTemp)
        # SOC Temperature is represent in 1/1000 (mC).
        ambient_temperature_formatted = float(ambient_temperature) / 1000
        strError = "Failed since the Ambient temperature is out of the range. Ambient temperature sensor is measuring '%s' Centigrades" % str(ambient_temperature_formatted)
        self.assertTrue((ambient_temperature_formatted < self.MAX_TEMP) and (ambient_temperature_formatted > self.MIN_TEMP), strError)

        self.VERDICT = True
