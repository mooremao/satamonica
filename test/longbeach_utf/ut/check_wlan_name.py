'''
Created on Nov 25, 2014

@author: Jeroen Jensen
'''

from common import ButTestCase
from clients import utils
import time

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_wlan_name"
    TEST_DESC = "Checking name of wlan can be configured"

    CUSTOM_WLAN_NAME = "check_wlan_name_test"
    DEFAULT_WLAN_NAME = "BANDIT-"
    DEFAULT_P2P_WLAN_NAME = "BANDIT-"

    WLAN_ON_AP_DEFAULT_CMD = "wlan_on ap"
    WLAN_ON_AP_CUSTOM_CMD = "wlan_on ap -n %s" % CUSTOM_WLAN_NAME
    AP_GET_INFO = "/usr/sbin/iw dev wlan0 info"
    AP_DEFAULT_CHECK_FOR = "ssid %s" % DEFAULT_WLAN_NAME
    AP_CUSTOM_CHECK_FOR = "ssid %s" % CUSTOM_WLAN_NAME

    WLAN_ON_P2P_DEFAULT_CMD = "wlan_on p2p"
    WLAN_ON_P2P_CUSTOM_CMD = "wlan_on p2p -n %s" % CUSTOM_WLAN_NAME
    P2P_GET_INFO = "cat /mnt/userdata/wifi/p2p_supplicant_lb.conf"
    P2P_DEFAULT_CHECK_FOR = "device_name=%s" % DEFAULT_P2P_WLAN_NAME
    P2P_CUSTOM_CHECK_FOR = "device_name=%s" % CUSTOM_WLAN_NAME

    WLAN_OFF_CMD = "wlan_off"

    WLAN_CLEAN_CONFIG_CMD = "rm -rf /mnt/userdata/wifi"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Reset the configuration
        """
        utils.system(self.WLAN_CLEAN_CONFIG_CMD)

    def tearDown(self):
        """
        Restore device initial status
        """
        utils.system(self.WLAN_OFF_CMD)
        """
        Reset the configuration
        """
        utils.system(self.WLAN_CLEAN_CONFIG_CMD)
        ButTestCase.ButTestCase.tearDown(self)


    """
    The tests case checks if the name of the wlan interface can be configured (ssid / device name)
    """
    def testDefaultWlanAPName(self):
        """
        Test default wlan AP name
        """
        utils.system(self.WLAN_ON_AP_DEFAULT_CMD)

        time.sleep(1)  # Give it time to configure itself

        wlaninfo = utils.system_output(self.AP_GET_INFO, retain_output=True)

        self.assertIn(self.AP_DEFAULT_CHECK_FOR, wlaninfo, "Failed to find correct default SSID in wlan info!")
        self.VERDICT = True


    def testCustomWlanAPName(self):
        """
        Test custom ssid in AP mode
        """
        utils.system(self.WLAN_ON_AP_CUSTOM_CMD)

        time.sleep(1)  # Give it time to configure itself

        wlaninfo = utils.system_output(self.AP_GET_INFO, retain_output=True)

        self.assertIn(self.AP_CUSTOM_CHECK_FOR, wlaninfo, "Failed to find correct custom SSID in wlan info!")
        self.VERDICT = True


    def testDefaultWlanP2PName(self):
        """
        Test default device name in P2P mode.
        """
        utils.system(self.WLAN_ON_P2P_DEFAULT_CMD)

        wlaninfo = utils.system_output(self.P2P_GET_INFO, retain_output=True)

        self.assertIn(self.P2P_DEFAULT_CHECK_FOR, wlaninfo, "Failed to find correct default device name in wlan info! Got ["+wlaninfo+"] Expected ["+self.P2P_DEFAULT_CHECK_FOR+"]")
        self.VERDICT = True


    def testCustomWlanP2PName(self):
        """
        Test custom device name in P2P mode.
        """
        utils.system(self.WLAN_ON_P2P_CUSTOM_CMD)

        wlaninfo = utils.system_output(self.P2P_GET_INFO, retain_output=True)

        self.assertIn(self.P2P_CUSTOM_CHECK_FOR, wlaninfo, "Failed to find correct custom device name in wlan info! Got ["+wlaninfo+"] Expected ["+self.P2P_CUSTOM_CHECK_FOR+"]")
        self.VERDICT = True
