from clients import utils
from clients import videotool
from common import ButTestCase
import os
import sys
import time
import platform
import uinput
RECORD_TIME=20  #Recording time.
INPUT_SETTING_NUMBER=0 #0 to 22
TIME_INTERVAL_IN_HOUR=12 #Device under recording test time.

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "stress_heavyload"
    TEST_DESC = "stress heavyload recording test"

    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    mypath ='/mnt/mmc/Generated_streams/'

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Inserting uinput driver
        """
        self.device=videotool.start()
        self.logger.info('uinput device is created')

    def tearDown(self):
        self.logger.info('demo app restart and uinput driver removed')
        videotool.stop(self.device)
        self.logger.info('uinput device deleted')
        ButTestCase.ButTestCase.tearDown(self)

    def testrecord(self, result=None):
        """
        This test case is going to record under heavy cpu load in all possible sensor mode in demo app
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Recording')
        output_dir = "/mnt/mmc/tests/output/"
        Result=videotool.record_stress_heavyload(self,self.device,RECORD_TIME,INPUT_SETTING_NUMBER,TIME_INTERVAL_IN_HOUR)
        self.assertTrue(Result=="PASS","Recording fail:File size zero.")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
