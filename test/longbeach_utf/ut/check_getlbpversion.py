"""
Created on Nov 7, 2014

@author: Jeroen Jensen
"""
from common import ButTestCase
from clients import utils

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_getlbpversion"
    TEST_DESC = "Checking get-lbp-version utility"

    GET_LBP_VERSION_CMD = "get-lbp-version; echo $?"
    GET_LBP_VERSION_LONG_CMD = "get-lbp-version -l; echo $?"

    """
    In a 'master' build:
        get-lbp-version -l
        g1514be4
        get-lbp-version
        1514be4

    In a 'stable' / named build:
        get-lbp-version -l
        lb_komal_cpudvfs1_201507010
        get-lbp-version
        201507010
    """

    def testGetlbpversionShort(self):
        """
        Testing get-lbp-version script. This script is different as it reports the git revision or (part of the) the git tag name.
        """
        self.logger.info("Starting 'testGetlbpversionShort'")

        """
        Verify status exit code of get-lbp-version command. Should be 0 (date format) or 1 (git rev).
        """
        status = utils.system_output(self.GET_LBP_VERSION_CMD, retain_output=True)
        if "\n" in status:
            (lbp_version, retcode) = status.split("\n")
        else:
            self.logger.info("get-lbp-version returned '{0}'".format(status))
            lbp_version = "0"
            retcode = status.strip()        # probably "2"
        retcode = int(retcode)

        errMsg = "Unexpected return code from 'get-lbp-version': Expect 0 or 1. Got '{0}'".format(retcode)
        self.assertTrue(retcode == 0 or retcode == 1, errMsg)

        self.logger.info("lbp-version returned '{0}' with return code {1}".format(lbp_version, retcode))

        lbp_version_length = len(lbp_version)

        """
        Version length should be  9 for <yyyymmddv> format (taggend version)
                                  7 for git revisions (non tagged versions)
                                 13 for dirty git revisions 7 + "-dirty" (build with modified or untracked changes)
        """
        if retcode == 0:
            self.assertEqual(lbp_version_length, 9, "Invalid version length for date format. Got {0}".format(lbp_version_length))
        elif retcode == 1:
            self.assertIn(lbp_version_length, [7, 13], "Invalid version length for git revision format. Got {0}".format(lbp_version_length))
        else:
            self.assertTrue(False, "Unexpected return code. Got '{0}', with '{1}' length ({2})".format(retcode, lbp_version, lbp_version_length))
        self.logger.info("length of '{0}' ({1}) is correct for return code {2}".format(lbp_version, lbp_version_length, retcode))


        self.logger.info("Ending 'testGetlbpversionShort'")
        self.VERDICT = True


    def testGetlbpversionLong(self):
        """
        Test "get-lbp-version -l" return code
        """
        self.logger.info("Starting 'testGetlbpversionLong'")

        """
        Verify status exit code of get-lbp-version -1 command. Should be 0 (date format) or 1 (git rev).
        """
        status = utils.system_output(self.GET_LBP_VERSION_LONG_CMD, retain_output=True)
        (lbp_version, retcode) = status.split("\n")
        retcode = int(retcode)

        errMsg = "Unexpected return code from 'get-lbp-version -l': Expect 0. Got '{0}'".format(retcode)
        self.assertEqual(retcode, 0, errMsg)

        self.logger.info("lbp-version -l returned '{0}' with return code {1}".format(lbp_version, retcode))


        self.logger.info("Ending 'testGetlbpversionLong'")
        self.VERDICT = True
