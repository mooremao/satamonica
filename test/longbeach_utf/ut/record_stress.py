from clients import utils
from clients import videotool
from common import ButTestCase
import os
import sys
import time
import platform
import uinput
Record_time=15  #Recording time.
input_setting_number=0 #0 to 22
time_interval_in_hour=12 #Device under recording test time.

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "record_stress"
    TEST_DESC = "Stress recording test"

    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    mypath ='/mnt/mmc/Generated_streams/'

    def testrecord(self, result=None):
        """
        This test case is going to stress record in all possible sensor mode in demo app
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Recording')
        output_dir = "/mnt/mmc/tests/output/"
        events = (
            uinput.KEY_UP,
            uinput.KEY_DOWN,
            uinput.KEY_LEFT,
            uinput.KEY_RIGHT,
            uinput.KEY_RECORD,
            uinput.KEY_STOP,
            )
        # Stop camera app
        utils.system(self.STOP_APP, ignore_status=True)
        status="active"
        while(status!="inactive"):
            os.system("systemctl is-active camera-app>/mnt/mmc/curr_status.txt")
            f=open('/mnt/mmc/curr_status.txt','r')
            status=f.read()
            status=status.rstrip()
        # Insert uinput driver
        os.system("modprobe uinput")
        # Create uinput device
        device = uinput.Device(events)
        # Start camera app
        utils.system(self.START_APP, ignore_status=True)
        status="inactive"
        while(status!="active"):
            os.system("systemctl is-active camera-app>/mnt/mmc/curr_status.txt")
            f=open('/mnt/mmc/curr_status.txt','r')
            status=f.read()
            status=status.rstrip()
        Result=videotool.record_stress_config(self,device,Record_time,input_setting_number,time_interval_in_hour)
        self.assertTrue(Result,"PASS","Recording fail:File size zero.")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
