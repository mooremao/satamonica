'''
This script is for starting and stoping demo application multiple times
Created on Dec 12,2014
@author:Vinit
'''
from clients import utils
from common import ButTestCase
import os
import sys
import time
import platform
import uinput
class Test(ButTestCase.ButTestCase):

    TEST_NAME = "Start_stop"
    TEST_DESC = "Start stop demo app"
    Itrr=2 #Number of start/stop
    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"

    def teststartstop(self, result=None):
        """
        This test case is going to start stop demo app multiple times.
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Start_stop')
        Num_time=self.Itrr
        result="PASS"
        while(Num_time>0):
            status="active"
            # Stop camera app
            utils.system(self.STOP_APP, ignore_status=True)
            time.sleep(4)
            os.system("systemctl is-active camera-app>/mnt/mmc/curr_status.txt")
            f=open('/mnt/mmc/curr_status.txt','r')
            status=f.read()
            status=status.rstrip()
            if(status=="active"):
                result="FAIL"
                break
            status="inactive"
            # Start camera app
            utils.system(self.START_APP, ignore_status=True)
            time.sleep(4)
            os.system("systemctl is-active camera-app>/mnt/mmc/curr_status.txt")
            f=open('/mnt/mmc/curr_status.txt','r')
            status=f.read()
            status=status.rstrip()
            if(status=="inactive"):
                result="FAIL"
                break
            self.logger.info("Start/Stop Count: %d \n"%(self.Itrr-Num_time))
            Num_time-=1
        self.assertTrue(Result,"PASS","App restart failed after %d iteration"%(self.Itrr-Num_time))
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

if __name__ == "__main__":
        # import sys;sys.argv = ['', 'Test.testName']
        unittest.main()
