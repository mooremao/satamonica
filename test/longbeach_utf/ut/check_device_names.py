'''
Created Jan 23, 2015 from template.py

@author: Andy Stout
'''
from common import ButTestCase
from clients import utils

'''
List of possible devices with a name field: find /sys/devices/platform/ -name name
List of name results: cd /sys/devices/platform/; find . -name name -print -exec cat {} \;
'''
namelist = {
    "omap/omap_i2c.3/i2c-3": "OMAP I2C adapter",
    "omap/omap_i2c.3/i2c-3/3-0018": "tlv320adc3101-codec",
    "omap/omap_i2c.3/i2c-3/3-0034": "tca8418_keypad",
    "omap/omap_i2c.3/i2c-3/3-0034/input/input2": "tca8418_keypad",
    "omap/omap_i2c.3/i2c-3/3-0044": "tomtom_mcu",
    "omap/omap_i2c.3/i2c-3/3-0055": "bq27421",
    "omap/omap_i2c.3/i2c-3/3-0060": "tps62363",
    "omap/omap_i2c.3/i2c-3/3-0060/regulator/regulator.1": "iva",
    "omap/omap_i2c.3/i2c-3/3-0072": "tmp103",
    "omap/omap_i2c.3/i2c-3/i2c-dev/i2c-3": "OMAP I2C adapter",
    "omap/omap_i2c.4/i2c-4": "OMAP I2C adapter",
    "omap/omap_i2c.4/i2c-4/4-0019": "lsm303e_acc",
    "omap/omap_i2c.4/i2c-4/4-0019/input/input4": "lsm303e_acc",
    "omap/omap_i2c.4/i2c-4/4-0060": "tps62363",
    "omap/omap_i2c.4/i2c-4/4-0060/regulator/regulator.2": "mpu",
    "omap/omap_i2c.4/i2c-4/4-0068": "mpu3050_gyro",
    "omap/omap_i2c.4/i2c-4/4-0068/input/input5": "mpu3050",
    "omap/omap_i2c.4/i2c-4/4-0077": "bmp280",
    "omap/omap_i2c.4/i2c-4/4-0077/input/input6": "bmp280",
    "omap/omap_i2c.4/i2c-4/i2c-dev/i2c-4": "OMAP I2C adapter",
    "mmci-omap-hs.0/mmc_host/mmc0/mmc0:0001/mmc0:0001:2/wl18xx/ieee80211/phy0": "phy0",
    "mmci-omap-hs.0/mmc_host/mmc0/mmc0:0001/mmc0:0001:2/wl18xx/ieee80211/phy0/rfkill0": "phy0",
    "gpio-keys.0/input/input0": "gpio-keys",
    "gpio-keys.1/input/input1": "gpio-keys",
    "pwm-beeper/input/input3": "pwm-beeper",
    "omap2_mcspi.3/spi_master/spi3/spi3.0/graphics/fb0": "LS013B7DH01",
    "omap_rtc/rtc/rtc0": "omap_rtc"
}

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ButTestCase.ButTestCase):

    # This text MUST match the filename, excluding the .py
    TEST_NAME = "check_device_names"
    TEST_DESC = "Verify that each of the device names is as expected"

    NAME_PREFIX = "cat /sys/devices/platform/"
    NAME_SUFFIX = "/name"

    '''
    This test is run on each of the strings in the table above
    '''
    def checkDeviceName(self, pname, expect):
        dName = utils.system_output(self.NAME_PREFIX + pname + self.NAME_SUFFIX, retain_output=True, ignore_status=True)
        self.logger.info(self.NAME_PREFIX + pname + self.NAME_SUFFIX + " Returned: '{0}'".format(dName))
        self.assertEqual(dName, expect, "Failed. Got '{0}' Expected '{1}'".format(dName, expect))
        self.VERDICT = True

'''
Create dynamically named test cases
'''
def _add_test(pname, expect):
    def test_method(self):
        self.checkDeviceName(pname, expect)
    setattr(Test, 'test_'+expect, test_method)
    test_method.__name__ = 'test_'+expect

for fname, expected in namelist.iteritems():
    _add_test(fname, expected)
