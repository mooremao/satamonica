'''
Created on 11 Dec 2014

@author: Andy Stout
'''
"""
assert                    cat /tmp/sys_platform-acc/device/enable_device (returns 1 if enabled)
verify the device name    cat /tmp/sys_platform-acc/device/name (expect lsm303e_acc)
disable the accelerometer echo 0 > /tmp/sys_platform-acc/device/enable_device
assert                    cat /tmp/sys_platform-acc/device/enable_device (returns 0 if disabled)
"""

from common import ButTestCase
from clients import utils
import re
from datetime import datetime


class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_acc"

    ACC_STATUS = "cat /tmp/sys_platform-acc/device/enable_device"
    # In '(ABS_X),' (the comma is important), need at least one of x/y/z (sometimes get Y,Z,Y,Z
    # request more lines than necessary, to allow time for status to be requested in parallel
    # using 'time', this complex command takes a while to setup and about 5 seconds to run
    ACC_DATA = "evtest /dev/input/event4 | grep '(ABS_[X|Y|Z]),' | head -10"

    ACC_NAME = "cat /tmp/sys_platform-acc/device/name"
    VERDICT = False

    LOG_FILE = "check_acc.log"
    TEST_DESC = "Testing Accelerometer lsm303c Functionality"

    def testAcc(self):
        """
        This test case is going to check the accelerometer
        """
        SUBTEST_NAME = "check_acc"
        self.logger.info("Starting '" + SUBTEST_NAME + "'")
        self.logger.info("Writing results to %s", self.tcConfiguration.logsDir)

        self.logger.info("Reading the accelerometer lsm303e_acc name")
        acc_name = utils.system_output(self.ACC_NAME, retain_output=True)
        self.logger.info("The accelerator device name field contains: '" + acc_name + "'")
        self.assertTrue(acc_name == "lsm303e_acc", "Accelerator name is not correct")

        acc_status = utils.system_output(self.ACC_STATUS, retain_output=True)
        self.assertEqual(acc_status, "0", "Accelerometer is expected to be OFF")

        # read the enable_device status while evtest is running
        # the sleep is necessary, as the simpler cat command can finish before the evtest starts
        acc_data, acc_status = utils.system_output_parallel([self.ACC_DATA, "sleep 1; " + self.ACC_STATUS], retain_output=True, ignore_status=True)

        acc_status = acc_status.rstrip()  # strip any trailing newline

        self.logger.info("Read accelerometer data as:\n" + acc_data)
        self.assertEqual(acc_status, "1", "Accelerometer is expected to be ON")  # evtest turns the device on when it runs

        acc_status = utils.system_output(self.ACC_STATUS, retain_output=True)
        self.assertEqual(acc_status, "0", "Accelerometer is expected to be OFF")  # evtest turns the device off when it exits

        acc_x = re.search(r'\(ABS_X\), value (-?[0-9]+)', acc_data).group(1)
        acc_y = re.search(r'\(ABS_Y\), value (-?[0-9]+)', acc_data).group(1)
        acc_z = re.search(r'\(ABS_Z\), value (-?[0-9]+)', acc_data).group(1)
        # we cannot determine whether the x/y/z values are correct as the device may be at an angle

        self.logger.info("Acc Registers: ACC_X=" + acc_x + ", ACC_Y=" + acc_y + ", ACC_Z=" + acc_z)

        acc_ix = (int)(acc_x); acc_iy = (int)(acc_y); acc_iz = (int)(acc_z)

        UP = 700
        DOWN = -UP
        mesg = "Orientation is: "
        if (acc_iz > UP):
            mesg += "Screen Up"
        elif (acc_iz < DOWN):
            mesg += "Screen Down"
        elif (acc_ix > UP):
            mesg += "Right side Up"
        elif (acc_ix < DOWN):
            mesg += "Left side Up"
        elif (acc_iy > UP):
            mesg += "Nose Up"
        elif (acc_iy < DOWN):
            mesg += "Nose Down"
        else:
            mesg += "Uncertain"
        self.logger.info(mesg)

        acc_absx = abs(acc_ix); acc_absy = abs(acc_iy); acc_absz = abs(acc_iz)
        total = acc_absx + acc_absy + acc_absz
        mesg = ""
        if (total > 1700):
            mesg += "Accelerometer values seem too large"
        elif (total < 100):
            mesg += "Accelerometer values seem too small"
        elif (total < 10):
            mesg += "Is device in free-fall?"
        else:
            mesg += "Accelerometer values seem about right"
        self.logger.info(mesg)

        self.logger.info("Stopping '" + SUBTEST_NAME + "'")
        self.VERDICT = True


    def assertTimeIncreases(self, latestTime):
        self.logger.info("Grab batch of monotonic times:")
        acc_data = utils.system_output("evtest --monotonic /dev/input/event4 | head -50", retain_output=True)

        lines = acc_data.split("\n")
        for each in lines:
            if "Event: time" in each:
                lineTime = float(re.search(r'Event: time ([0-9]+.[0-9]+),', each).group(1))
                self.logger.info("Assert time: {0} > {1}".format(lineTime, latestTime))
                self.assertTrue(lineTime > latestTime)
                latestTime = lineTime

        return latestTime


    def testAccMonotonic(self):
        SUBTEST_NAME = "check_accMonotonic"
        self.logger.info("Starting '" + SUBTEST_NAME + "'")

        latestTime = self.assertTimeIncreases(0.0)

        self.logger.info("Set clock back in time to verify that the monotonic clock only goes forwards")
        utctimenow = datetime.now()
        self.logger.info("Time is currently: {0}".format(utctimenow.strftime("%Y/%m/%d %H:%M:%S")))
        self.logger.info("Setting clock backwards by [0-59] seconds: {0}".format(utctimenow.strftime("%Y/%m/%d %H:%M:00")))
        utils.system_output("date -s '{0}'".format(utctimenow.strftime("%Y-%m-%d %H:%M:00")))

        self.assertTimeIncreases(latestTime)

        self.logger.info("Stopping '" + SUBTEST_NAME + "'")
        self.VERDICT = True
