'''
Created on 10 Dec 2014

@author: Andy Stout
'''
"""
enable the gyro       echo 1 > /tmp/sys_platform-gyro/device/enable
assert                cat /tmp/sys_platform-gyro/device/enable (returns 1 if enabled)
read the registers    cat /tmp/sys_platform-gyro/device/registers
disable the gyro      echo 0 > /tmp/sys_platform-gyro/device/enable
assert                cat /tmp/sys_platform-gyro/device/enable (returns 0 if disabled)
"""

from common import ButTestCase
from clients import utils

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_gyro"

    GYRO_ON = "echo 1 > /tmp/sys_platform-gyro/device/enable"
    GYRO_OFF = "echo 0 > /tmp/sys_platform-gyro/device/enable"
    GYRO_STATUS = "cat /tmp/sys_platform-gyro/device/enable"
    GYRO_CAT_READ = "cat /tmp/sys_platform-gyro/device/registers"
    GYRO_EVTEST_READ = "evtest /dev/input/event5 & sleep 2; killall evtest"

    VERDICT = False

    LOG_FILE = "check_gyro.log"
    TEST_DESC = "Testing Gyroscope mpu3050 Functionality"

    def setUp(self):

        ButTestCase.ButTestCase.setUp(self)
        # turn on the GYRO
        utils.system(self.GYRO_ON)


    def tearDown(self):

        # turn off the GYRO
        utils.system(self.GYRO_OFF)
        ButTestCase.ButTestCase.tearDown(self)


    def testGyroCat(self):
        """
        check gyroscope functionality using "cat"
        """
        TEST_NAME = "testGyroCat"
        self.logger.info("Starting '{0}'".format(TEST_NAME))
        self.logger.info("Reading the gyro mpu3050 registers by 'cat'")

        gyro_status = utils.system_output(self.GYRO_STATUS, retain_output=True)
        self.assertEqual(int(gyro_status), 1, "Gyro is not ON")
        gyro_registers = utils.system_output(self.GYRO_CAT_READ, retain_output=True)
        self.logger.info("The gyro registers contain:\n" + gyro_registers)
        self.assertIn("CHIP_ID", gyro_registers, "Failed to find CHIP_ID")
        self.assertIn("PWR_MGMT", gyro_registers, "Failed to find PWR_MGMT")
        self.logger.info("Ending '{0}'".format(TEST_NAME))

        self.VERDICT = True


    # [LBP-1684] gyro is enabled but doesn't work, immediately after boot
    # This test here checks the normal use. A server-side test needs to check after reboot
    def testGyroEvtest(self):
        """
        check gyroscope functionality using "evtest"
        """
        TEST_NAME = "testGyroEvtest"
        self.logger.info("Starting '{0}'".format(TEST_NAME))
        self.logger.info("Reading the gyro mpu3050 registers by 'evtest'")

        gyro_status = utils.system_output(self.GYRO_STATUS, retain_output=True)
        self.assertEqual(int(gyro_status), 1, "Gyro is not ON")
        gyro_registers = utils.system_output(self.GYRO_EVTEST_READ, retain_output=True)
        self.logger.info("The gyro evtest returned:\n" + gyro_registers)
        self.assertIn("SYN_REPORT", gyro_registers, "Failed to read gyro registers with Evtest.")
        self.logger.info("Ending '{0}'".format(TEST_NAME))

        self.VERDICT = True
