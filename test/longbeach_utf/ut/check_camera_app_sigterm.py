'''
Created on Dec 2, 2014

@author: Jeroen Jensen
'''

from common import ButTestCase
from clients import utils
import time

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_camera_app_sigterm"
    TEST_DESC = "Checking whether SIGTERM signal results in restart of camera-app service"

    SEND_SIGTERM_TO_CAMERA_APP_CMD = "systemctl kill --signal=SIGTERM camera-app.service"
    CAMERA_APP_STARTED_SINCE_CMD = "systemctl status camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    CAMERA_APP_START_CMD = "systemctl start camera-app.service"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Setup initial device status
        """

    def tearDown(self):
        """
        Restore device initial status
        """
        utils.system(self.RESET_FAILED_CMD, ignore_status=True)
        utils.system(self.CAMERA_APP_START_CMD, ignore_status=True)
        ButTestCase.ButTestCase.tearDown(self)


    def testSigterm(self):
        """
        This test case is going to check whatever SIGTERM signal results in restart of camera-app service
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info(self.TEST_DESC)

        utils.system(self.SEND_SIGTERM_TO_CAMERA_APP_CMD, ignore_status=True)

        time.sleep(2) # +2 seconds slack.

        status = utils.system_output(self.CAMERA_APP_STARTED_SINCE_CMD, retain_output=True, ignore_status=True)

        self.assertIn("(code=exited, status=0/SUCCESS)", status);
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
