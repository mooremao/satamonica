'''
Created Feb 24, 2015 from template.py

@author: Romeo Cane
'''
from common import ButTestCase
from clients import utils

# See https://docs.python.org/2/library/unittest.html for more information
class Test(ButTestCase.ButTestCase):

    # This text MUST match the filename, excluding the .py
    TEST_NAME = "measure_pressure"
    TEST_DESC = "Pressure sensor test"

    MAX_PRESSURE = 105.0
    MIN_PRESSURE = 95.0

    '''
    Define separate tests within the suite. Tests must be named 'test<something>' but the order of test execution is not defined and may even be randomised.
    Each test has the setUp run before and tearDown run after.
    Do not use try / except. The ButTest / unitTest harness catches those.
    '''

    def testMeasure(self, result=None):
        """
        This test case is going to check the pressure of the atmosphere
        Falling through all the asserts and setting VERDICT = True marks the test as a pass
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Measuring the Pressure')

        utils.system_output('echo 1 > /tmp/sys_platform-presure/enable', retain_output=False)

        soc_name = utils.system_output('cat /tmp/sys_platform-presure/name', retain_output=True)
        self.assertTrue(soc_name=="bmp280", "Pressure device name expected to be bmp280. Got {0}".format(soc_name))

        pressure = utils.system_output('cat /tmp/sys_platform-presure/pressure', retain_output=True)
        self.assertTrue(pressure != None, "Failed path output does not exist.")
        self.logger.info('Output of Pressure sensor is ' + pressure)
        self.logger.info('Pressure Measured is {0}kPa'.format((int(pressure) / 1000)))

        utils.system_output('echo 0 > /tmp/sys_platform-presure/enable', retain_output=False)

        pressure_formatted = float(pressure) / 1000
        strError = "Failed since the pressure is '{0}'kPa (min {1}, max {2})".format(pressure_formatted, self.MIN_PRESSURE, self.MAX_PRESSURE)
        self.assertTrue(self.MIN_PRESSURE <= pressure_formatted <= self.MAX_PRESSURE, strError)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

