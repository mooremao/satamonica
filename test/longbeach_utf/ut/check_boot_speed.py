'''
Created on Feb 3, 2015

@author: Komal Padia
'''

from common import ButTestCase
from clients import utils
from clients import error
import re

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_boot_speed"
    TEST_DESC = "Testing boot speed"

    BOOTTIME = "boottime"

    '''
    Input a number in microseconds, convert to a human readable quantity string
    '''
    def uStohuman(self, number):
        factor = ["uS", "mS", "S"]
        factoroffset = 0

        number = (float)(number)
        while number >= 1000.0 and factoroffset < len(factor)-1:
            number /= 1000.0
            factoroffset += 1

        return "{0:.3f} {1}".format(number, factor[factoroffset])

    def testBootSpeed(self):
        """
        This test case is going to check the boot speed
        """
        self.logger.info(self.TEST_DESC)

        try:
            bootinfo = utils.system_output(self.BOOTTIME, retain_output=True, ignore_status=False)
        except error.CmdError as e:
            exit_code_re = re.search("Exit status: (\d+)\n", "{0}".format(e))
            exit_code = exit_code_re.group(1)
            self.logger.error("ERROR IN RUNNING BOOTTIME. Exit code: {0}".format(exit_code))

            errMsg = "Aborting test. Probably SD card trouble"
            self.assertTrue(False, errMsg)

        boottime = re.findall(r"[-+]?\d+", bootinfo)

        self.logger.info("Bootinfo returned:\n" + bootinfo)

        boot1 = int(boottime[0])
        boot2 = int(boottime[1])

        self.assertTrue(boot1 > 0 and boot2 > 0, "Error getting boot time. Possibly SD card error or the video app not started. Got: boot1='{0}' boot2='{1}'".format(boot1, boot2))

        with open(self.tcConfiguration.logsDir + "/" + self.TEST_NAME + "/" + "bootspeed.csv", "wb") as csvfile:
            csvfile.write("Boot1, Boot2\n")
            csvfile.write("{0}, {1}\n".format(boot1, boot2))
        csvfile.close()

        boot1msg = self.uStohuman(boot1)
        boot2msg = self.uStohuman(boot2)
        self.logger.info("bootime values in human form: {0} and {1}. (Expecting under 4.5 Seconds and 3.5 Seconds respectively)".format(boot1msg, boot2msg))

        ERROR_MSG = "Expecting under 4.5 S and 3.5 S respectively. Got: {0} and {1}".format(boot1msg, boot2msg)
        self.assertTrue(boot1 <= 4500000 and boot2 <= 3500000, ERROR_MSG)

        self.logger.info("End of test " + self.TEST_DESC)

        self.VERDICT = True
