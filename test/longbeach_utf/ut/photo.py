from clients import utils
from clients import videotool_main_app
from common import ButTestCase
import os
import sys
import time
import platform
import uinput
Continutime=60
############
#PHOTO     #
############
class Test(ButTestCase.ButTestCase):

    TEST_NAME = "photo"
    TEST_DESC = "video app photo capture"

    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"

    def photo(self, result=None):
        """
        This test case is going to capture photo in all possible sensor mode in video app
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Recording')
        output_dir = "/mnt/mmc/tests/output/"
        events = (
            uinput.KEY_UP,
            uinput.KEY_DOWN,
            uinput.KEY_LEFT,
            uinput.KEY_RIGHT,
            uinput.KEY_RECORD,
            uinput.KEY_STOP,
            )
        # Stop camera app
        utils.system(self.STOP_APP, ignore_status=True)
        status="active"
        while(status!="inactive"):
            os.system("systemctl is-active camera-app>/mnt/mmc/curr_status.txt")
            f=open('/mnt/mmc/curr_status.txt','r')
            status=f.read()
            status=status.rstrip()
        # Insert uinput driver
        os.system("modprobe uinput")
        # Create uinput device
        device = uinput.Device(events)
        # Start camera app
        utils.system(self.START_APP, ignore_status=True)
        status="inactive"
        while(status!="active"):
            os.system("systemctl is-active camera-app>/mnt/mmc/curr_status.txt")
            f=open('/mnt/mmc/curr_status.txt','r')
            status=f.read()
            status=status.rstrip()
        Result=videotool_main_app.photo(self,device,Continutime)
        self.assertTrue(Result,"PASS","File size zero")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
