'''
Created on Nov 17, 2014

@author: Raghuraman
'''

from common import ButTestCase
from clients import utils

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_device_battery_capacity"

    BATTERY_PATH = "/sys/class/power_supply/bq27421-0/"
    FAKE_BATTERY_PATH = "/sys/devices/platform/omap/omap_i2c.3/i2c-3/3-0055/"


    def setUp(self):
        super(Test, self).setUp()
        self.logger.info('local setup goes here - after parent setup')


    def tearDown(self):
        self.setFake("fake_capacity", -1)               # disable fake_capacity
        self.setFake("fake_capacity_level", -1)         # disable fake_capacity_level

        super(Test, self).tearDown()


    def getString(self, filename):
        return utils.system_output("cat {0}".format(filename), retain_output=True)


    def setString(self, filename, level):
        return utils.system_output("echo {0} > {1}".format(level, filename), retain_output=True)


    def expectString(self, filename, expected):
        output = self.getString(self.BATTERY_PATH + filename)
        strError = "Battery test: Expected: '{0}'. Got '{1}'".format(expected, output)
        self.logger.info(strError)
        self.assertTrue(output == expected, strError)


    def expectRange(self, filename, rangeMin, rangeMax):
        output = self.getString(self.BATTERY_PATH + filename)
        strError = "Battery test: Expected between: '{0}' and '{1}'. Got '{2}'".format(rangeMin, rangeMax, output)
        self.logger.info(strError)
        self.assertTrue(float(rangeMin) <= float(output) <= float(rangeMax), strError)


    def expectInList(self, filename, list):
        output = self.getString(self.BATTERY_PATH + filename)
        strError = "Battery test: Expected '{0}' in {1}".format(output, list)
        self.logger.info(strError)
        self.assertTrue(output in list, strError)


    def getFake(self, filename):
        return self.getString(self.FAKE_BATTERY_PATH + filename)


    def setFake(self, filename, level):
        return self.setString(self.FAKE_BATTERY_PATH + filename, level)


    def testBatteryCapacity(self):
        """
        check the battery capacity is between 0% and 100%
        Fake all levels, and assert that real_capacity does not change
        """
        MIN_LEVEL = 0
        MAX_LEVEL = 100

        self.SUBTEST_NAME = "testBatteryCapacity"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectRange("capacity", MIN_LEVEL, MAX_LEVEL)              # range check 0% to 100%

        OriginalRealCapacity = self.getFake("real_capacity")
        self.logger.info("Battery real capacity is {0}".format(OriginalRealCapacity))
        self.expectString("capacity", OriginalRealCapacity)             # should be the same

        oldBatteryCapacity = self.getFake("fake_capacity")              # should be -1 for disabled
        self.logger.info("Original battery fake_capacity = {0}".format(oldBatteryCapacity))

        for level in range(0, 100, 3):
            self.logger.info("Setting FAKE battery capacity: {0}".format(level))
            self.setFake("fake_capacity", level)

            self.expectRange("capacity", MIN_LEVEL, MAX_LEVEL)          # range check 0% to 100%

            currentRealCapacity = self.getFake("real_capacity")
            self.expectString("capacity", str(level))                   # capacity should match fake level
            self.assertEqual(currentRealCapacity, OriginalRealCapacity) # capacity should be unchanged

        self.logger.info("Restoring fake_capacity to {0}".format(oldBatteryCapacity))
        self.setFake("fake_capacity", oldBatteryCapacity)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True



    def testBatteryCapacityLevel(self):
        """
        check the battery capacity status is one of the strings in the list
        """
        CAPACITY_LEVEL_LIST = ["Critical", "Low", "Normal", "High", "Full"]

        self.SUBTEST_NAME = "testBatteryCapacityLevel"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        oldBatteryCapacity = self.getFake("fake_capacity_level")                # should be -1 for disabled
        self.logger.info("Original battery fake_capacity_level = {0}".format(oldBatteryCapacity))

        for level in range(2, 6):   # 1 is critical and causes the device to shutdown!
            self.logger.info("Setting FAKE battery capacity: {0}".format(level))
            self.setFake("fake_capacity_level", level)

            self.expectString("capacity_level", CAPACITY_LEVEL_LIST[level-1])   # expect exact element

        self.logger.info("Restoring fake_capacity_level to {0}".format(oldBatteryCapacity))
        self.setFake("fake_capacity_level", oldBatteryCapacity)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryChargeNow(self):
        """
        check the battery charge is within range in uAh
        """
        MIN_CHARGE = 0
        MAX_CHARGE = 4500000

        self.SUBTEST_NAME = "testBatteryChargeNow"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectRange("charge_now", MIN_CHARGE, MAX_CHARGE)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryCurrentNow(self):
        """
        check the battery current is within range in uA
        """
        MIN_CURRENT = 0
        MAX_CURRENT = 1990320

        self.SUBTEST_NAME = "testBatteryCurrentNow"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectRange("current_now", MIN_CURRENT, MAX_CURRENT)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryEnergyNow(self):
        """
        check the battery energy is within range
        """
        MIN_ENERGY = 0
        MAX_ENERGY = 1990321

        self.SUBTEST_NAME = "testBatteryEnergyNow"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectRange("energy_now", MIN_ENERGY, MAX_ENERGY)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryPresent(self):
        """
        check the battery is present
        """
        self.SUBTEST_NAME = "testBatteryPresent"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectString("present", "1")

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryStatus(self):
        """
        check the battery status string
        """
        STATUS_LIST = [
            "Full",
            "Charging",
            "Discharging",
            "Not charging",
        ]

        self.SUBTEST_NAME = "testBatteryStatus"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectInList("status", STATUS_LIST)            # expect one from the list

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryTechnology(self):
        """
        check the battery technology string
        """
        self.SUBTEST_NAME = "testBatteryTechnology"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectString("technology", "Li-ion")

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryTemp(self):
        """
        check the battery temperature
        """
        MIN_TEMP = 200
        MAX_TEMP = 500

        self.SUBTEST_NAME = "testBatteryTemp"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectRange("temp", MIN_TEMP, MAX_TEMP)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryType(self):
        """
        check the battery type
        """
        self.SUBTEST_NAME = "testBatteryType"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectString("type", "Battery")

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryType2(self):
        """
        check the battery type from a different file. The 3000mAH type was never commissioned
        """
        self.SUBTEST_NAME = "testBatteryType2"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        output = self.getString("/sys/bus/i2c/devices/3-0044/battery_type")
        expected = "2000mAH"
        strError = "Battery type: Expected: '{0}'. Got '{1}'".format(expected, output)
        self.logger.info(strError)
        self.assertTrue(output == expected, strError)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testBatteryVoltageNow(self):
        """
        check the battery voltage is within range
        """
        MIN_VOLTAGE = 2900000
        MAX_VOLTAGE = 4300000

        self.SUBTEST_NAME = "testBatteryVoltageNow"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        self.expectRange("voltage_now", MIN_VOLTAGE, MAX_VOLTAGE)

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True


    def testPlotBatteryCSV(self):
        self.SUBTEST_NAME = "testPlotBatteryCSV"
        self.logger.info("Starting '" + self.SUBTEST_NAME + "'")

        voltage = self.getString(self.BATTERY_PATH + "voltage_now")
        charge = self.getString(self.BATTERY_PATH + "charge_now")

        with open(self.tcConfiguration.logsDir + "/" + self.TEST_NAME + "/" + "battery.csv", "wb") as csvfile:
            csvfile.write("Voltage, Charge\n")
            csvfile.write("{0}, {1}\n".format(voltage, charge))

        self.logger.info("Stopping '" + self.SUBTEST_NAME + "'")
        self.VERDICT = True
