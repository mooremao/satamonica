"""
Created on Feb 24, 2015

@author: Komal Padia
"""

from common import ButTestCase
from clients import utils


class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_cpu_freq"
    TEST_DESC = "Testing cpu frequencies for various governors"

    MIN_FREQ = 110000
    MAX_FREQ = 428998

    AVAILABLE_GOVERNORS = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"
    GOVERNOR = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
    CPUFREQ = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)

    def tearDown(self):
        ButTestCase.ButTestCase.tearDown(self)

    def testCpuFreqs(self):
        """
        This test case is going to check CPU frequencies for all governors
        """
        self.logger.info(self.TEST_DESC)

        # print kernel version and available frequency governors
        utils.system_output("uname -r", retain_output=True)
        utils.system_output("cat " + self.AVAILABLE_GOVERNORS, retain_output=True)

        gov = utils.system_output("cat " + self.GOVERNOR, retain_output=True)
        self.assertEqual(gov, "lb_governor", "Failed. Got '{0}' Expected lb_governor".format(gov))

        freq = utils.system_output("cat " + self.CPUFREQ, retain_output=True)
        # use same message for logging and error case
        msg = "Set '{0}': Expected CPU frequency between '{1}' and '{2}', got '{3}'".format(gov, self.MIN_FREQ, self.MAX_FREQ, freq)

        self.logger.info(msg)
        self.assertTrue(self.MIN_FREQ <= int(freq) <= self.MAX_FREQ, msg)

        self.logger.info("End of test " + self.TEST_DESC)

        self.VERDICT = True
