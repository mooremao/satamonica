'''
Created on Nov 6, 2014

@author: Alberto Segura
'''

from common import ButTestCase
from clients import utils
import time

"""
Test that the wlan can be turned on and off multiple times.
Test that wlan_on can be called multiple times in a row (ditto wlan_off)
"""
class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_wifi_on"
    TEST_DESC = "Testing switching on Wifi"

    DEFAULT_WLAN_NAME = "BANDIT-"
    WLAN_OFF_CMD = "wlan_off"
    WLAN_ON_CMD = "wlan_on"
    WLAN_GET_INFO = "/usr/sbin/iw dev wlan0 info"
    WLAN_CHECK_ON = "ssid %s" % DEFAULT_WLAN_NAME

    IFCONFIG_INFO = "ifconfig wlan0"
    IFCONFIG_CHECK_ON = "UP BROADCAST"
    IFCONFIG_CHECK_OFF = "BROADCAST"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        self.logger.info("Switching on Wifi")
        utils.system(self.WLAN_ON_CMD)

    def tearDown(self):
        """
        Restore device initial status
        """
        self.logger.info("Switching off Wifi")
        utils.system(self.WLAN_OFF_CMD)
        ButTestCase.ButTestCase.tearDown(self)

    """
    for <testCount> times:
        wlan_on()
        allow time for wlan to get online (it is not deterministic)
        assert(wlan is on)

        sleep()
        assert(wlan is on after sleeping)

        wlan_off()
        allow time for wlan to go off (faster to go off than connect)
        assert(wlan is off)
    """
    def testWifiOn(self):
        """
        test the wifi_on and wifi_off scripts
        """
        self.logger.info(self.TEST_DESC)

        testCount = 3
        wlan_on_sleeptime = 10                 # wlan_on takes a while (it is not deterministic, having to hunt)
        wlan_off_sleeptime = 1                 # wlan_off is faster

        SET_WAKEUP_TIMER_COMMAND = "echo 8 > /sys/class/power_supply/tomtom_mcu/device/wakeup_timeout"
        DEEPSLEEP_COMMAND = "echo mem > /sys/power/state"
        DMSG_GREP_COMMAND = "dmesg | grep 'PM: late suspend of devices complete'"


        for count in range(testCount):
            utils.system(self.WLAN_ON_CMD)
            time.sleep(wlan_on_sleeptime)

            # assert wlan is on, before sleeping
            ifconfiginfo = utils.system_output(self.IFCONFIG_INFO, retain_output=True)
            self.assertIn(self.IFCONFIG_CHECK_ON, ifconfiginfo, "Count {0}: (Before sleep) Wlan not on. ifconfig Got {1}".format(count, ifconfiginfo))

            wlaninfo = utils.system_output(self.WLAN_GET_INFO, retain_output=True)
            self.assertIn(self.WLAN_CHECK_ON, wlaninfo, "Count {0}: (Before sleep) Wlan not on. iw Got {1}".format(count, wlaninfo))

            # check that suspend / resume works while wlan is on (from check_deepsleep.py)
            utils.system(SET_WAKEUP_TIMER_COMMAND)
            utils.system(DEEPSLEEP_COMMAND)
            deepsleep = utils.system_output(DMSG_GREP_COMMAND, retain_output=True)
            errMsg =  "Failed as we did not find deep sleep in the log. Got '{0}'".format(deepsleep)
            self.assertTrue(deepsleep != "", errMsg)


            # assert wlan is on, after sleeping
            ifconfiginfo = utils.system_output(self.IFCONFIG_INFO, retain_output=True)
            self.assertIn(self.IFCONFIG_CHECK_ON, ifconfiginfo, "Count {0}: (After sleep) Wlan not on. ifconfig Got {1}".format(count, ifconfiginfo))

            wlaninfo = utils.system_output(self.WLAN_GET_INFO, retain_output=True)
            self.assertIn(self.WLAN_CHECK_ON, wlaninfo, "Count {0}: (After sleep) Wlan not on. iw Got {1}".format(count, wlaninfo))


            utils.system(self.WLAN_OFF_CMD)
            time.sleep(wlan_off_sleeptime)

            ifconfiginfo = utils.system_output(self.IFCONFIG_INFO, retain_output=True)
            self.assertIn(self.IFCONFIG_CHECK_OFF, ifconfiginfo, "Count {0}: Wlan not off. ifconfig Got {1}".format(count, ifconfiginfo))

            wlaninfo = utils.system_output(self.WLAN_GET_INFO, retain_output=True)
            self.assertNotIn(self.WLAN_CHECK_ON, wlaninfo, "Count {0}: Wlan not off. Got {1}".format(count, wlaninfo))


        self.logger.info("Wlan On / Off {0} times. Pass".format(testCount))

        self.logger.info("End of test " + self.TEST_DESC)
        self.VERDICT = True


    def testhw_pg_ver(self):
        """
        test  cat /sys/bus/platform/devices/wl18xx/hw_pg_ver
        """
        self.logger.info(self.TEST_DESC)

        hwpgverExpected = "PG2.2"

        hwpgver = utils.system_output("cat /sys/bus/platform/devices/wl18xx/hw_pg_ver", retain_output=True)
        msg = "hw_pg_ver has changed. Expected '{0}' Got '{1}'".format(hwpgverExpected, hwpgver)
        self.assertIn(hwpgverExpected, hwpgver, msg)


        self.logger.info("End of test " + self.TEST_DESC)
        self.VERDICT = True
