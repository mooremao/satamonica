'''
Created on 16 Dec 2014

@author: Andy Stout
'''
"""
e-magnetomoeter lm303e_mag test

evtest /dev/input/event5 produces a stream of X/Y/Z data
evtest enables the device and disables it on exit

enable the compass        echo 1 > /tmp/sys_platform-mag/device/enable_device
assert                    cat /tmp/sys_platform-mag/device/enable_device (returns 1 if enabled)
verify the device name    cat /tmp/sys_platform-mag/device/name (expect lsm303e_acc)
disable the compass       echo 0 > /tmp/sys_platform-mag/device/enable_device
assert                    cat /tmp/sys_platform-mag/device/enable_device (returns 0 if disabled)
"""

from common import ButTestCase
from clients import utils
import re
import sys


class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_mag"

    MAG_ON = "echo 1 > /tmp/sys_platform-mag/device/enable_device"
    MAG_OFF = "echo 0 > /tmp/sys_platform-mag/device/enable_device"
    MAG_STATUS = "cat /tmp/sys_platform-mag/device/enable_device"
    # In '(ABS_X),' (the comma is important), need at least one of x/y/z (sometimes get Y,Z,Y,Z
    # request more lines than necessary, to allow time for status to be requested in parallel
    MAG_DATA = "evtest /dev/input/event5 | grep '(ABS_[X|Y|Z]),' | head -20"

    MAG_NAME = "cat /tmp/sys_platform-mag/device/name"
    VERDICT = False

    # log directory is /mnt/mmmc/tests/output
    LOG_FILE = "check_mag.log"
    TEST_DESC = "Testing Magnetometer lsm303e Functionality"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)

        '''
        ensure enable_device is OFF
        use evtest to read data, check that evtest has turned on the device during use
        assert that evtest has set enable_device back to OFF
        '''
        utils.system(self.MAG_OFF)

    def tearDown(self):
        ButTestCase.ButTestCase.tearDown(self)

    def testMag(self, result=None):
        """
        This test case is going to check the magnetometer
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)

        self.logger.info("Reading the magnetometer lsm303e_mag name")
        mag_name = utils.system_output(self.MAG_NAME, retain_output=True)
        self.logger.info("The magnetometer device name field contains: '" + mag_name + "'")
        self.assertTrue(mag_name == "lsm303e_mag", "Magnetometer name is not correct")

        mag_status = utils.system_output(self.MAG_STATUS, retain_output=True)
        self.assertEqual(int(mag_status), 0, "Magnetometer is expected to be OFF")

        # read the enable_device status while evtest is running
        mag_data, mag_status = utils.system_output_parallel([self.MAG_DATA, "sleep 1;" + self.MAG_STATUS], retain_output=True, ignore_status=True)

        mag_status = mag_status.rstrip()  # strip any trailing newline

        self.logger.info("magnetometer status during evtest is " + mag_status)
        self.logger.info("Read magnetometer data as:\n" + mag_data)
        self.assertEqual(mag_status, "1", "Magnetometer is expected to be ON during evtest")  # evtest turns the device on when it runs

        mag_status = utils.system_output(self.MAG_STATUS, retain_output=True)
        self.assertEqual(mag_status, "0", "Magnetometer is expected to be OFF after evtest")  # evtest turns the device off when it exits

        mag_x = re.search(r'\(ABS_X\), value (-?[0-9]+)', mag_data).group(1)
        mag_y = re.search(r'\(ABS_Y\), value (-?[0-9]+)', mag_data).group(1)
        mag_z = re.search(r'\(ABS_Z\), value (-?[0-9]+)', mag_data).group(1)
        # we cannot determine whether the x/y/z values are correct as the device may be pointing in any directino

        self.logger.info("Mag Registers: MAG_X=" + mag_x + ", MAG_Y=" + mag_y + ", MAG_Z=" + mag_z)

        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True
