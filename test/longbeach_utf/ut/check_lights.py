'''
Created on Oct 31, 2014

@author: Niels Langendorff
'''

from common import ButTestCase
from clients import utils

class Test(ButTestCase.ButTestCase):

    TEST_NAME = "check_lights"
    TEST_DESC = "Testing the backlight and LED"

    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Setup initial device status
        """
        utils.system(self.STOP_APP, ignore_status=True)

    def tearDown(self):
        '''
        Restore device initial status
        '''
        utils.system(self.RESET_FAILED_CMD, ignore_status=True)
        utils.system(self.START_APP, ignore_status=True)
        ButTestCase.ButTestCase.tearDown(self)

    def testBacklight(self):
        """
        This test case is going to check the correct functioning of the backlight on the LongBeach device
        """
        TEST_NAME = "check_backlight"
        self.logger.info("Starting '" + self.TEST_NAME + "'")

        """
        Turn off backlight
        Retrieve backlight status
        Turn on backlight
        Retrieve backlight status
        """
        BL_PATH = "/sys/class/leds/Backlight/brightness"

        bl_previous_status = utils.system_output("cat {0}".format(BL_PATH), retain_output=True)
        self.logger.info("BACKLIGHT is currently set to {0}".format(bl_previous_status))

        self.logger.info("BACKLIGHT HALF")
        utils.system("echo 128 > {0}".format(BL_PATH))
        bl_status_half = utils.system_output("cat {0}".format(BL_PATH), retain_output=True)

        self.logger.info("BACKLIGHT OFF")
        utils.system("echo 0 > {0}".format(BL_PATH))
        bl_status_off = utils.system_output("cat {0}".format(BL_PATH), retain_output=True)

        self.logger.info("BACKLIGHT ON")
        utils.system("echo 255 > {0}".format(BL_PATH))
        bl_status_on = utils.system_output("cat {0}".format(BL_PATH), retain_output=True)
        utils.system("echo 0 > {0}".format(BL_PATH))

        # put the state back to how it was
        self.logger.info("Restoring BACKLIGHT state to {0}".format(bl_previous_status))
        utils.system("echo {0} > {1}".format(bl_previous_status, BL_PATH))

        """
        Check the result
        """
        self.assertEqual(int(bl_status_off), 0, "Backlight is not off. Got {0}".format(bl_status_off))
        self.assertEqual(int(bl_status_half), 128, "Backlight is not half. Got {0}".format(bl_status_half))
        self.assertEqual(int(bl_status_on), 255, "Backlight is not on. Got {0}".format(bl_status_on))

        self.logger.info("Stopping '" + self.TEST_NAME + "' Backlight")
        self.VERDICT = True


    def testLED1(self):
        """
        This test case is going to check the correct functioning of LED1 on the LongBeach device
        """
        TEST_NAME = "check_led"
        self.logger.info("Starting '" + self.TEST_NAME + "'")

        """
        Turn off LED1
        Retrieve LED1 status
        Turn on LED1
        Retrieve LED1 status
        """
        LED_PATH = "/sys/class/leds/LED1/brightness"

        led_previous_status = utils.system_output("cat {0}".format(LED_PATH), retain_output=True)
        self.logger.info("LED is currently set to {0}".format(led_previous_status))

        self.logger.info("LED HALF")
        utils.system("echo 128 > {0}".format(LED_PATH))
        led_status_half = utils.system_output("cat {0}".format(LED_PATH), retain_output=True)

        self.logger.info("LED OFF")
        utils.system("echo 0 > {0}".format(LED_PATH))
        led_status_off = utils.system_output("cat {0}".format(LED_PATH), retain_output=True)

        self.logger.info("LED ON")
        utils.system("echo 255 > {0}".format(LED_PATH))
        led_status_on = utils.system_output("cat {0}".format(LED_PATH), retain_output=True)

        # put the state back to how it was
        self.logger.info("Restoring LED state to {0}".format(led_previous_status))
        utils.system("echo {0} > {1}".format(led_previous_status, LED_PATH))

        """
        Check the result
        """
        self.assertEqual(int(led_status_off), 0, "LED is not off. Got {0}".format(led_status_off))
        self.assertEqual(int(led_status_half), 128, "LED is not half. Got {0}".format(led_status_half))
        self.assertEqual(int(led_status_on), 255, "LED is not on. Got {0}".format(led_status_on))

        self.logger.info("Stopping '" + self.TEST_NAME + "' LED1")
        self.VERDICT = True
