from clients import utils
from clients import videotool
from common import ButTestCase
import os
import sys
import time
import platform
import uinput
import os.path
import csv
import shutil
RECORD_TIME=1
TRANSCODE_TIME=RECORD_TIME*2
#############
#Transcoding#
#############
class Test(ButTestCase.ButTestCase):

    TEST_NAME = "autotest_transcode"
    TEST_DESC = "Transcoding test"

    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    mypath ='/mnt/mmc/Generated_streams/'

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Inserting uinput driver
        """
        self.device=videotool.start()
        self.logger.info('uinput device is created')

    def tearDown(self):
        self.logger.info('demo app restart and uinput driver removed')
        videotool.stop(self.device)
        self.logger.info('uinput device deleted')
        ButTestCase.ButTestCase.tearDown(self)

    def testtranscode(self, result=None):
        """
        This test case is going to transcode all recorded video
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Transcoding')
        Result=videotool.transcode(self,self.device,RECORD_TIME,TRANSCODE_TIME)
        self.assertTrue(Result=="PASS","Transcoding fail:File size zero.")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

if __name__ == "__main__":
        # import sys;sys.argv = ['', 'Test.testName']
        unittest.main()
