from clients import utils
from clients import videotool
from common import ButTestCase
import os
import sys
import time
import platform
import uinput
RECORD_TIME=1  #Recording time.
IN_BITRATE=100 #-1,60%,80%,100%,120%... -1 for all
############
#Recording #
############
class Test(ButTestCase.ButTestCase):

    TEST_NAME = "autotest_record"
    TEST_DESC = "Recording test"

    STOP_APP = "systemctl stop camera-app.service"
    START_APP = "systemctl start camera-app.service"
    RESET_FAILED_CMD = "systemctl reset-failed camera-app.service"
    mypath ='/mnt/mmc/Generated_streams/'

    def setUp(self):
        ButTestCase.ButTestCase.setUp(self)
        """
        Inserting uinput driver
        """
        self.device=videotool.start()
        self.logger.info('uinput device is created')

    def tearDown(self):
        self.logger.info('demo app restart and uinput driver removed')
        videotool.stop(self.device)
        self.logger.info('uinput device deleted')
        ButTestCase.ButTestCase.tearDown(self)

    def testrecord(self, result=None):
        """
        This test case is going to record in all possible sensor mode in demo app
        """
        self.logger.info("Starting '" + self.TEST_NAME + "'")
        self.logger.info('Writing results to %s', self.tcConfiguration.logsDir)
        self.logger.info('Recording')
        Result=videotool.config_record(self,self.device,RECORD_TIME,IN_BITRATE)
        self.assertTrue(Result=="PASS","Recording Fail:File size zero")
        self.logger.info("Stopping '" + self.TEST_NAME + "'")
        self.VERDICT = True

if __name__ == "__main__":
        # import sys;sys.argv = ['', 'Test.testName']
        unittest.main()
