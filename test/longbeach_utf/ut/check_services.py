'''
Created on Jan 14, 2015

@author: Andy Stout
'''

import glob
import os
import re

from clients import utils
from common import ButTestCase


class Test(ButTestCase.ButTestCase):
    TEST_NAME = "check_services"
    TEST_DESC = "Test services against white and black lists"

    def testCheckServices(self, result=None):
        """
        for each service in systemctl --all
            switch systemctl status
              case running
                 check if in running list
              case oneshot
                 check if in oneshot list
              default
                 check if in not running list
            end switch
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))

        '''
        These are reference lists which are compared with services in each state
        '''
        running_ref = ['bluetopia', 'camera-app', 'gpsd', 'metalog', 'monitor-temp', 'ntpd', 'omap-serial-getty', 'remote-debug-client', 'systemd-journald',
                        'systemd-udevd', 'dropbear']

        exited_ref = ['check-battery', 'check-crossopp', 'check-temp', 'dspload', 'exfatmod', 'freespace', 'gsdclock', 'gsdpatcher', 'ipnc-init',
                       'logdata', 'log-identification', 'mmc',
                       'mmc-dev',
                       'osa_kermod', 'rtcclocksignal', 'syslink',
                       'systemd-udev-trigger', 'update-fuelgauge', 'usbalias',
                       'userdata', 'vpss-m3-fw', 'wifi-setup']

        dead_ref = ['check-firstboot', 'check-recovery', 'check-test', 'check-ttci',
                    'check-watchdog-boot',
                    'emergency', 'gadget',
                    'gsd-factory-reset', 'initrd-cleanup', 'initrd-parse-etc',
                    'initrd-switch-root', 'initrd-udevadm-cleanup-db', 'monitor-usb',
                    'rescue',
                    'show-battery',
                    'sigpwr', 'sigtemp', 'systemd-fsck-root', 'systemd-halt',
                    'systemd-hibernate', 'systemd-hybrid-sleep', 'systemd-initctl',
                    'systemd-journal-flush', 'systemd-kexec', 'systemd-poweroff',
                    'systemd-reboot', 'systemd-suspend', 'systemd-udev-settle', 'test',
                    'ttci-finalizer', 'ttci-poweroff', 'ttci', 'uart-menu', 'userdata-mmc',
                    'wifi-ap', 'wifi-dhcp', 'wifi-p2p-reconf', 'wifi-p2p', 'wifi'
                    ]

        # Services ignored as state might change during execution of test case.
        ignore = ['firstboot',
                  'reset-reboot-counter']

        other_ref = ['systemd-fsck@', 'user@']     # cross check that the test is working correctly

        running_list = []
        exited_list = []
        dead_list = []
        other_list = []

        # Has the system been up long enough for the services to stabilise?
        uptime = utils.system_output("uptime", ignore_status=True)
        self.logger.info("Uptime is '{0}'".format(uptime))

        services = utils.system_output("systemctl --all --type service --no-legend list-unit-files --no-pager")
        services = services.split('\n')
        services.sort()  # in-place sort. Makes all the other lists sorted
        for line in services:
            # print "> "+line
            service = line.split()[0]
            name, sep, suffix = service.rpartition('.')
            # print name
            status = utils.system_output("systemctl status " + name, ignore_status=True)
            # print("state = "+status)
            if re.search("Active: active \(running\) since", status):  # currently running
                running_list.append(name)
            elif re.search("Active: active \(exited\) since", status):  # not running right now
                exited_list.append(name)
            elif re.search("Active: inactive \(dead\)", status):  # never ran
                dead_list.append(name)
            else:
                other_list.append(name)

        '''
        Copy these back to get new reference lists
        '''

        utils.compare_lists(self, running_list, running_ref, ignore, "Running")
        utils.compare_lists(self, exited_list, exited_ref, ignore, "Exited")
        utils.compare_lists(self, dead_list, dead_ref, ignore, "Dead")
        utils.compare_lists(self, other_list, other_ref, None, "Other")
        self.logger.info("All lists match their reference lists")

        self.logger.info("End of '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
