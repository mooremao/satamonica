'''
Created on Oct 2, 2014

@author: segura
'''
import sys
import unittest

from clients import utils
from common import ButTestCase

class Test(ButTestCase.ButTestCase):

    version = 2
    TEST_NAME = "measure_fuel_gauge_temperature"
    TEST_DESC = "Testing Temperature Sensors"

    MAX_TEMP = 90.0
    MIN_TEMP = 10.0
    TEMP_SCALING = 10.0

    def testMeasure(self, result=None):
        """
        This test case is going to check the temperature of a LongBeach device
        """
        self.logger.info("Starting '{0}'".format(self.TEST_NAME))
        self.logger.info("Writing results to {0}".format(self.tcConfiguration.logsDir))
        self.logger.info("Measuring the Fuel Gauge Temperature")

        fuel_gauge_temperature = utils.system_output("cat /sys/class/power_supply/bq27421-0/temp", retain_output=True)
        self.assertTrue (fuel_gauge_temperature != None, "Failed path output does not exist.")
        self.logger.info("Output Fuel Gauge Temperature Sensor is {0}".format(fuel_gauge_temperature))

        strTemp = "Fuel Gauge Temperature Measure is {0}C".format(float(fuel_gauge_temperature) / self.TEMP_SCALING)
        self.logger.info(strTemp)
        "Fuel Gauge Temperature is represented in 1/10 (C). We guess that in the room the temperature can vary between 10 - 20C"
        strError = "Error - Output is None. Sensor does not provide any valid temperature."
        self.assertTrue(fuel_gauge_temperature, strError)

        fuel_gauge_temperature_formatted = float(fuel_gauge_temperature) / self.TEMP_SCALING
        strError = "Failed since the fuel gauge temperature is out of range [{0}, {1}]. Got '{2}'".format(self.MIN_TEMP, self.MAX_TEMP, fuel_gauge_temperature_formatted)
        self.assertTrue(self.MIN_TEMP < fuel_gauge_temperature_formatted < self.MAX_TEMP, strError)

        self.logger.info("Stopping '{0}'".format(self.TEST_NAME))
        self.VERDICT = True
