#!/usr/bin/env python
import os
import sys
import getopt
import ConfigParser
from setup_environment import reflashLB, buildSystemFSWithTestsBinaries

def main():
    opts, args = getopt.getopt(sys.argv[1:], "hfc:", ["flash", "config"])
    reflash = False
    configFile = 'LB_BUT_FW.cfg'
    for opt, arg in opts:
        if opt == '-h':
            print 'USAGE: python gerritBasicTest.py [-c <config_file>] [-f]'
            sys.exit()
        elif opt in ("-c", "--config"):
            configFile = arg
        elif opt in ("-f", "--flash"):
            reflash = True

    config = ConfigParser.ConfigParser()
    config.read(configFile)
    
    workingDir = os.path.dirname(os.path.realpath(__file__))+'/'
    sdkWorkspaceDir = workingDir + config.get('LOCAL', 'sdk_workspace_dir')
    workspaceRootDir = workingDir + config.get('LOCAL', 'workspace_root_dir')
    fastbootDir = '{0}/{1}'.format(workingDir, config.get('LOCAL', 'fastboot_dir'))
    devAddress = config.get('DEVICE','address')

    if reflash:
        try:
            buildSystemFSWithTestsBinaries(sdkWorkspaceDir, workspaceRootDir)
            reflashLB(devAddress, fastbootDir, sdkWorkspaceDir)
        except Exception as e:
            print ('[ERROR] An error ocurred while flashing the image: {0}'.format(e))
            sys.exit(1)

    if reflash:
        print ('[INFO] Build OK and image correctly flashed.')
    else:
        print ('[INFO] Build OK.')
    sys.exit(0)

if __name__ == "__main__":
    main()

