import os
import sys
import shutil
import subprocess
import getopt
import ConfigParser
import iptools
from deploy_lib import *

def copyDirContent(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(src).st_mtime - os.stat(dst).st_mtime > 1:
                shutil.copy2(s, d)


def cleanLogsDir(dirsToClean):
    for directory in dirsToClean:
        if os.path.exists(directory):
            shutil.rmtree(directory)
        os.makedirs(directory)


def run_device_tests(configFile, cameraIP='169.254.255.1'):
    print('\n[INFO] RUNNING TESTS ON DEVICE: {0} \n'.format(cameraIP))
    runSystemCommand('python -u run_tests_device.py -c {0} -i {1}'.format(configFile, cameraIP))

def run_server_tests(configFile, serverLogsDir, generalLogsDir, cameraIP='169.254.255.1'):
    print('\n[INFO] RUNNING TESTS ON THE SERVER \n')
    runSystemCommand('python -u run_tests_server.py -c {0} -i {1}'.format(configFile, cameraIP))
    print("Retrieve logs from device ({0}): {1} to server: {2}".format(cameraIP, serverLogsDir, generalLogsDir))
    copyDirContent(serverLogsDir, generalLogsDir)
    print("Done\n")


def main():

    onlyServer = False
    onlyDevice = False
    gtest = False
    configFile = "LB_BUT_FW.cfg"
    cameraIP = "not_set"

    opts, args = getopt.getopt(sys.argv[1:], "hsdgc:i:", ["config=", "server", "device", "camera-ip="])
    for opt, arg in opts:
        if opt == '-h':
            print 'USAGE: python -u run_tests.py [-c <config>] [--server] [--device] [--camera-ip <camera_ip_address>]'
            sys.exit()
        elif opt in ("-s", "--server"):
            onlyServer = True
        elif opt in ("-d", "--device"):
            onlyDevice = True
        elif opt in ("-c", "--config"):
            configFile = arg
        elif opt in ("-i", "--camera-ip"):
            cameraIP = arg

    config = ConfigParser.ConfigParser()
    config.read(configFile)

    deploymentDir = os.path.dirname(os.path.realpath(__file__))+'/'
    serverLogsDir = '{0}/../output/'.format(deploymentDir)
    generalLogsDir = '{0}/{1}'.format(deploymentDir, config.get('LOCAL', 'logs_dir'))

    if cameraIP == "not_set":
        locAddress = config.get('LOCAL', 'address')
    else:
        locAddress = iptools.ipv4.long2ip(iptools.ipv4.ip2long(cameraIP)+1)

    cleanLogsDir([serverLogsDir, generalLogsDir])

    if not deviceUp(locAddress):      # error message is printed inside deviceUp
        print ('[ERROR] Skipping tests, the device is not up')
        sys.exit()

    if onlyServer:
        run_server_tests(configFile, serverLogsDir, generalLogsDir, cameraIP)
    elif onlyDevice:
        run_device_tests(configFile, cameraIP)
    else:
        run_device_tests(configFile, cameraIP)
        run_server_tests(configFile, serverLogsDir, generalLogsDir, cameraIP)

if __name__ == "__main__":
    main()



