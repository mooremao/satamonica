#!/usr/bin/env python

from datetime import datetime
import os
import re
import subprocess
import sys
import time
import traceback

def runSystemCommand(command, ignore=[]):
    print "Executing @localhost: {0}".format(command)

    ret = subprocess.call([command], shell=True)

    if ret != 0:
        raise RuntimeError("Command: \"{0}\" failed with exit code: {1} @localhost".format(command, ret))

def runSystemCommandAndGetOutput(command, ignore=[]):
    print "Executing @localhost: {0}".format(command)

    ret = subprocess.Popen([command], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (done, fail) = ret.communicate()
    sys.stdout.write(done)
    sys.stderr.write(fail)
    if done == '' and fail != '':
        ignoreError = False
        for msg in ignore:
            if msg in fail:
                print ('[WARNING] The following error message was ignored: {0}'.format(fail))
                ignoreError = True
        if not ignoreError:
            print ('\n[ERROR] Exception raised when running system command: {0}\n'.format(fail))
            raise RuntimeError (fail)
    return [done, fail]

def runRemoteCommand(command, devAddress, usr='root', passwd=None, ignore=[]):
    print "Executing @{0}: {1}".format(devAddress, command)

    if not passwd:
        ret = subprocess.call(["ssh -T -o TCPKeepAlive=yes -o ConnectTimeout=30 -o ServerAliveInterval=30 -o ServerAliveCountMax=2 {0}@{1} '{2}'".format(usr, devAddress, command)], shell=True)
    else:
        ret = subprocess.call(["sshpass -p {0} ssh -T -o TCPKeepAlive=yes -o ConnectTimeout=30 -o ServerAliveInterval=30 -o ServerAliveCountMax=2 {1}@{2} '{3}'".format(passwd, usr, devAddress, command)], shell=True)

    if ret != 0:
        raise RemoteException("Command: '{0}' failed with exit code: {1} @{2}".format(command, ret, devAddress))

def runRemoteCommandAndGetOutput(command, devAddress, usr='root', passwd=None, ignore=[]):
    print "Executing @{0}: {1}".format(devAddress, command)

    if not passwd:
        ret = subprocess.Popen(["ssh -T -o ConnectTimeout=10 -o ServerAliveInterval=30 -o ServerAliveCountMax=2 {0}@{1} '{2}'".format(usr, devAddress, command)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (done, fail) = ret.communicate()
        sys.stdout.write(done)
        sys.stderr.write(fail)
        try:
            checkException([done, fail], ignore)
        except RemoteException, e:
            print ('\n[ERROR] Exception raised when running command on remote host: {0}\n'.format(''.join(e)))
            raise
    else:
        ret = subprocess.Popen(["sshpass -p {0} ssh -T -o ConnectTimeout=10 {1}@{2} '{3}'".format(passwd, usr, devAddress, command)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (done, fail) = ret.communicate()
        sys.stdout.write(done)
        sys.stderr.write(fail)
        try:
            checkException([done, fail], ignore)
        except RemoteException, e:
            print ('\n[ERROR] Exception raised when running command on remote host: {0}\n'.format(''.join(e)))
            raise

    return [done, fail]

def runRemoteCommandWithStdout(command, devAddress, usr='root', passwd=None, ignore=False):
    print "Executing @{0}: {1}".format(devAddress, command)
    retString = None
    retCode = 0
    try:
        if not passwd:
            retString = subprocess.check_output(["ssh -T -o ConnectTimeout=10 -o ServerAliveInterval=30 -o ServerAliveCountMax=2 {0}@{1} '{2}'".format(usr, devAddress, command)], shell=True)
        else:
            retString = subprocess.check_output(["sshpass -p {0} ssh -T -o ConnectTimeout=10 -o ServerAliveInterval=30 -o ServerAliveCountMax=2 {1}@{2} '{3}'".format(passwd, usr, devAddress, command)], shell=True)
    except subprocess.CalledProcessError, e:
        retString = e.output
        retCode = e.returncode
        if not ignore:
            print "\n>>> CalledProcessError Exception: (Command {0}) Got the following output\n'{1}' with return code {2} <<<".format(e.cmd, retString, retCode)
            traceback.print_exc()
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise
    return (retCode, retString)

def runSystemCommandWithStdout(command, ignore=[]):
    retString = None
    retCode = 0
    try:
        retString = subprocess.check_output([command], shell=True)
    except subprocess.CalledProcessError, e:
        retString = e.output
        retCode = e.returncode
        print "CalledProcessError Exception (Command {0}) got the following output '{1}' with return code {2}".format(e.cmd, retString, retCode)
        traceback.print_exc()
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise
    return (retCode, retString)

def sendToRemote(devAddress, src, dst, usr='root', passwd=None):
    if not passwd:
        ret = subprocess.Popen(['scp {0} {1}@{2}:/{3}'.format(src, usr, devAddress, dst)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (done, fail) = ret.communicate()
        sys.stdout.write(done)
        sys.stderr.write(fail)
        try:
            checkException([done, fail])
        except RemoteException, e:
            print ('\n[ERROR] Exception raised when sending files to remote host: {0}\n'.format(''.join(e)))
            raise
    else:
        ret = subprocess.Popen(['sshpass -p {0} scp {1} {2}@{3}:/{4}'.format(passwd, src, usr, devAddress, dst)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (done, fail) = ret.communicate()
        sys.stdout.write(done)
        sys.stderr.write(fail)
        try:
            checkException([done, fail])
        except RemoteException, e:
            print ('\n[ERROR] Exception raised when sending files to remote host: {0}\n'.format(''.join(e)))
            raise
    return [done, fail]


def fetchFromRemote(devAddress, src, dst, usr='root', passwd=None):
    if not passwd:
        ret = subprocess.Popen(['scp {0}@{1}:/{2} {3}'.format(usr, devAddress, src, dst)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (done, fail) = ret.communicate()
        sys.stdout.write(done)
        sys.stderr.write(fail)
        try:
            checkException([done, fail])
        except RemoteException, e:
            print ('\n[WARNING] Exception raised when fetching files from remote host: {0}\n'.format(''.join(e)))
            raise
    else:
        ret = subprocess.Popen(['sshpass -p {0} scp {1}@{2}:/{3} {4}'.format(passwd, usr, devAddress, src, dst)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (done, fail) = ret.communicate()
        sys.stdout.write(done)
        sys.stderr.write(fail)
        try:
            checkException([done, fail])
        except RemoteException, e:
            print ('\n[WARNING] Exception raised when fetching files from remote host: {0}\n'.format(''.join(e)))
            raise
    return [done, fail]


def checkException(commOut, ignore=[]):
    if commOut[0] == '' and commOut[1] != '':
        ignoreError = False
        for msg in ignore:
            if msg in commOut[1]:
                print ("[WARNING] The following error message was ignored: '{0}'".format(commOut[1]))
                ignoreError = True
        if not ignoreError:
            print ("\n[ERROR] Exception raised: '{0}'\n".format(commOut[1]))
            raise RemoteException(''.join(commOut[1]))


class RemoteException(Exception):
    def __init__(self, arg):
        self.args = arg


'''
Return the status of the usb0 interface.
Check that ifconfig has the ip address. (Could check 'ifconfig usb0')
'''
def deviceUp(locAddress, quiet=False):
    (retCode, ifstatus) = runSystemCommandWithStdout("ifconfig")
    if re.search("inet addr:" + locAddress, ifstatus):  # got ip address for the device?
        return True
    if not quiet:
        print("ifconfig returned\n{1}\nCamera at '{0}' is not plugged in or not powered on.\n".format(locAddress, ifstatus))
    return False

'''
Return the status of the device.
Check that the device is responding to ping
'''
def deviceAlive(devAddress, quiet=False):
    (retCode, pingstatus) = runSystemCommandWithStdout("ping -W1 -c1 {0}".format(devAddress))
    if re.search("1 packets transmitted, 1 received, 0% packet loss, time 0ms", pingstatus):
        return True
    if not quiet:
        print("deviceAlive: ping returned '{0}'".format(pingstatus))
    return False

def printksleep(text, delay):
    sys.stdout.write(text)
    sys.stdout.flush()
    if delay != 0:
        time.sleep(delay)

'''
instruct the device to reboot
wait for the usb0 device to come up after reboot. Timeout after 'timeout' in seconds (A timeout of at least 10 seconds is useful)
return
    status True = Device is up
    return False = Device failed after timeout
While loops are preferable to sleeps (As sleeps will be either too large or too small on different builds)
'''
def rebootAndWait(timeout, devAddress, locAddress):

    if not deviceUp(locAddress):
        return False

    runRemoteCommand("shutdown -r now", devAddress)
    return waitReboot(timeout, devAddress, locAddress)

def waitReboot(timeout, devAddress, locAddress):

    print("Waiting for '{0}' + '{1}' to go down and come up again ...".format(locAddress, devAddress))
    count = 0
    while deviceUp(locAddress, quiet=True):  # wait while interface is up
        printksleep('-', 1)
        count += 1
        if count >= timeout:
            print("Timeout count reached")
            return False

    while not deviceUp(locAddress, quiet=True):  # wait while interface is down
        printksleep('_', 1)
        count += 1
        if count >= timeout:
            print("Timeout count reached")
            return False

    # allow time for sshd / dropbear to start, otherwise get "connection refused" while sshd starts
    printksleep('-', 1)

    for retries in range(0, 10):
        ret = subprocess.Popen(["ssh -T -o ConnectTimeout=1 root@{0} true".format(devAddress)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (done, fail) = ret.communicate()
        fail = fail.strip()
        if fail == "":  # successfully connected?
            print("\nSuccessfully connected to sshd on the Device\n")
            return True

        printksleep("ssh attempt, will retry [" + fail + "]\n", 1)

    print("Timeout connecting to Device")
    return False



'''
Check the date-time on device (at devAddress) and wait until the clock is set correctly
 using the PC date-time as a reference.
 A timeout in seconds is required. ntpd may take a while to set the clock.
 An optional flag 'ntptest' if True will verify that the ntp daemon is running correctly
 Return False if the timeout is reached or ntptest= True and ntpd is not running.
'''
def waitNTPd(timeout, devAddress):

    count = 0
    while True:
        device_timenow, result = runRemoteCommandAndGetOutput("date -u +'%Y.%m.%d-%H:%M:%S'", devAddress)  # device time in UTC
        remote_timenow = datetime.strptime(device_timenow.rstrip(), "%Y.%m.%d-%H:%M:%S")

        local_timenow = datetime.utcnow()  # pc time in UTC
        print("PC time is: {0}, Device time is: {1}".format(local_timenow, device_timenow))

        count += 1
        if count == timeout:  # failsafe - don't wait forever (this loop takes about 1 second per iteration) ntpd can take 4 mins 20 seconds to update the clock
            print("Reached timeout. Give up waiting for ntpd to correct the clock")
            print("Setting the device time from the PC")
            runSystemCommand('ssh root@{0} "date -us $(date -u +%Y.%m.%d-%T)"'.format(devAddress))
            return False

        if abs((local_timenow - remote_timenow).total_seconds()) <= 3600:  # allow for timezone differences
            print("Device time is correct enough")
            return True

    return True

# boottimes are accumulated here
bootLogFile = "../testServerLogs/boottimelog.txt"
bootCSVFile = "../testServerLogs/bootspeed.csv"

# Called from 'run_tests_server', before the tests are run
def openBootTimeLog():
    print("Reset boottime log '{0}/{1}'".format(os.getcwd(), bootLogFile))
    os.open(bootLogFile, os.O_CREAT | os.O_TRUNC)


# During the tests, after every boot, log the boottime
# Allow flag to not log boottime after factory_reset or entering recovery
def AddBootTimeLog(self, logBootTime):
    if logBootTime:
        (bootinfo, bootinfoStderr) = runRemoteCommandAndGetOutput("boottime", self.devAddress, ignore=["ERROR:"])
        bootinfo = bootinfo.strip()
        bootinfoStderr = bootinfoStderr.strip()

        if "ERROR:" in bootinfoStderr:
            print("Not logging invalid boottime '{0}'".format(bootinfoStderr))
        elif bootinfo != "":
            boottime = re.findall(r"[-+]?\d+", bootinfo)
            boot1 = int(boottime[0])
            boot2 = int(boottime[1])
            self.logger.info("BootTime boottime returns: '{0}', '{1}' Appending to {2}".format(boot1, boot2, bootLogFile))

            if boot1 > 0 and boot2 > 0:
                with open(bootLogFile, "a", 1) as bootfile:     # 1 = line_buffered
                    bootfile.write("{0}, {1}\n".format(boot1, boot2))
            else:
                print("ERROR: Not logging negative boot times")
        else:
            self.logger.info("boottime returned: '{0}' '{1}'".format(bootinfo, bootinfoStderr))


# Called from 'run_tests_server' after the tests have run, calculate the average boottime
def closeBootTimeLog():
    print("Close boottime log '{0}/{1}' - calculate average of the entries".format(os.getcwd(), bootLogFile))
    boot1Average = 0
    boot2Average = 0
    try:
        boot1Total = boot2Total= 0
        boot1Min = boot2Min = 9999999           # will go down
        boot1Max = boot2Max = 0                 # will go up

        lines = open(bootLogFile).read().strip().split('\n')
        count = len(lines)
        if len(lines) > 1:
            for line in lines:
                if "," in line:                     # don't use blank line
                    (boot1, boot2) = line.split(",")
                    boot1 = int(boot1.strip())
                    boot2 = int(boot2.strip())
                    print("boot1={0}, boot2={1}".format(boot1, boot2))
                    boot1Total += boot1
                    boot2Total += boot2

                    boot1Min = min(boot1, boot1Min)
                    boot1Max = max(boot1, boot1Max)
                    boot2Min = min(boot2, boot2Min)
                    boot2Max = max(boot2, boot2Max)

            boot1Average = float(boot1Total) / count
            boot2Average = float(boot2Total) / count
            header = "Boot1, Boot2, minBoot1, maxBoot1, minBoot2, maxBoot2"
            print(header)
            print("{0:.1f}, {1:.1f}, {2}, {3}, {4}, {5}\n".format(boot1Average, boot2Average, boot1Min, boot1Max, boot2Min, boot2Max))
            with open(bootCSVFile, "wb") as csvfile:
                csvfile.write(header+"\n")
                csvfile.write("{0:.1f}, {1:.1f}, {2}, {3}, {4}, {5}\n".format(boot1Average, boot2Average, boot1Min, boot1Max, boot2Min, boot2Max))

        else:
            print("'{0}' was empty!".format(bootLogFile))

    except IOError as e:        # file may not exist
        print("'{0}/{1}' does not exist: Average boot times are undefined".format(os.getcwd(), bootLogFile))
