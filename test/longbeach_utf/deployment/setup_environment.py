#!/usr/bin/env python

import ConfigParser
import getopt
import os
import sys
import time
import iptools

from deploy_lib import runSystemCommand, runRemoteCommand, \
    sendToRemote, fetchFromRemote, runRemoteCommandAndGetOutput, deviceUp, rebootAndWait, waitNTPd, waitReboot


def buildSystemFSWithTestsBinaries(sdkWorkspaceDir, workspaceRootDir):
    os.chdir(sdkWorkspaceDir)
    try:
        runSystemCommand('./generate-testfilesys.sh {0}'.format(workspaceRootDir))
    except RuntimeError as e:
        print("{0}\nNeed to run 'make sys genrelease'".format(e))
        sys.exit()

def reflashLB(devAddress, fastbootDir, sdkWorkspaceDir, locAddress):
    runRemoteCommand('enter_fastboot', devAddress, ignore=['WARNING: could not determine runlevel'])

    # Sleep while the system is rebooting
    print('[INFO] Waiting for the device to reboot in fastboot_mode')
    time.sleep(15)
    os.chdir(fastbootDir)
    print("Adding python and flashing image at {0}/target/tftp".format(sdkWorkspaceDir))
    runSystemCommand('./fastboot.sh --path {0}/target/tftp/'.format(sdkWorkspaceDir))
    print('[INFO] Device flashed. Waiting for device to boot')
    # Sleep while the system is rebooting
    status = waitReboot(60, devAddress, locAddress)
    if status:
        status = runRemoteCommand('uname -a', devAddress)
        # Set the remote clock from the PC clock, so that ntpd does not get a big jump later
        runRemoteCommand("date -us '$(date -u +%Y.%m.%d-%T)'", devAddress)
    return status


def installSDKCameraApp(appServerUser, appServer, appServerPass, appSourceDir, appName, appDevDestDir, devAddress, localAddress):
    print('[INFO] Retrieving {0} from {1}'.format(appName, appServer))
    remoteAppName = appName.replace('*', '\*')
    fetchFromRemote(appServer, '{0}/{1}'.format(appSourceDir, remoteAppName), './', usr=appServerUser, passwd=appServerPass)
    print('[INFO] Sending {0} to the device'.format(appName))
    sendToRemote(devAddress, './{0}'.format(appName), appDevDestDir)
    runSystemCommand('rm -f ./{0}'.format(appName))
    print('[INFO] Rebooting the device to finish the Camera Application installation. This process can take up to 2 minutes')
    rebootAndWait(120, devAddress, localAddress)



def checkAppRunning(devAddress):
    appRunningProcess = '/home/root/tom'
    ret = runRemoteCommandAndGetOutput('"ps | grep {0}"'.format(appRunningProcess), devAddress)
    appRunning = False
    psOutputDone = ret[0].split('\n')
    psOutputFail = ret[1].split('\n')
    fullOutput = psOutputDone + psOutputFail
    for line in fullOutput:
        if appRunningProcess in line and not 'sh -c ps' in line and not 'grep' in line:
            appRunning = True
    return appRunning


def removeApp(devAddress, localAddress):
    appRunningProcess = '/home/root/tom'
    appBin = '/opt/ipnc/app/src/tom'
    ret = runRemoteCommandAndGetOutput('"ps | grep {0}"'.format(appRunningProcess), devAddress)
    psOutputDone = ret[0].split('\n')
    psOutputFail = ret[1].split('\n')
    fullOutput = psOutputDone + psOutputFail
    appKilled = False
    for line in fullOutput:
        if appRunningProcess in line and not 'sh -c ps' in line and not 'grep' in line:
            pid = line.strip().split(' ')[0]
            print ('[INFO] Killing Camera Application (PID {0})'.format(pid))
            runRemoteCommand('"kill -9 {0}; rm {1};"'.format(pid, appBin), devAddress)
            appKilled = True
    if appKilled:
        print ('[INFO] Application successfully uninstalled. Waiting for the device to reboot')
        rebootAndWait(20, devAddress, localAddress)
    else:
        print ('[INFO] The Camera Application could not be found')


def main():

    flash = False
    appInstall = False
    appRemove = False
    configFile = "LB_BUT_FW.cfg"
    cameraIP = ""

    opts, args = getopt.getopt(sys.argv[1:], "hfarc:i:", ["flash", "config=", "appInstall", "removeApp", "camera-ip="])
    for opt, arg in opts:
        if opt == '-h':
            print 'USAGE: python setup_environment.py [-c <config_file>] [-f] [-a] [-r] [-i <camera_ip>]'
            sys.exit()
        elif opt in ("-c", "--config"):
            configFile = arg
        elif opt in ("-f", "--flash"):
            flash = True
        elif opt in ("-a", "--appInstall"):
            appInstall = True
        elif opt in ("-r", "--removeApp"):
            appRemove = True
        elif opt in ("-i", "--camera-ip"):
            cameraIP = arg

    config = ConfigParser.ConfigParser()
    config.read(configFile)

    workingDir = os.path.dirname(os.path.realpath(__file__)) + '/'
    sdkWorkspaceDir = workingDir + config.get('LOCAL', 'sdk_workspace_dir')
    workspaceRootDir = workingDir + config.get('LOCAL', 'workspace_root_dir')
    fastbootDir = workingDir + config.get('LOCAL', 'fastboot_dir')

    if cameraIP == "":
        devAddress = config.get('DEVICE', 'address')
        locAddress = config.get('LOCAL', 'address')
    else:
        devAddress = cameraIP
        locAddress = iptools.ipv4.long2ip(iptools.ipv4.ip2long(cameraIP)+1)

    appServer = config.get('CAMERA_APP', 'app_server')
    appServerUser = config.get('CAMERA_APP', 'app_server_user')
    appServerPass = config.get('CAMERA_APP', 'app_server_pass')
    appSourceDir = config.get('CAMERA_APP', 'app_source_dir')
    appDevDestDir = config.get('CAMERA_APP', 'app_dest_dir')
    appName = config.get('CAMERA_APP', 'app_name')

    if not deviceUp(locAddress):  # error message is printed inside deviceUp
        print ('[ERROR] The device is not up')
        return 1

    if flash:
        buildSystemFSWithTestsBinaries(sdkWorkspaceDir, workspaceRootDir)
        os.chdir(workingDir)
        reflashLB(devAddress, fastbootDir, sdkWorkspaceDir, locAddress)

    os.chdir(workingDir)
    if appInstall:
        installSDKCameraApp(appServerUser, appServer, appServerPass, appSourceDir, appName, appDevDestDir, devAddress, locAddress)
    if appRemove:
        removeApp(devAddress, locAddress)

if __name__ == "__main__":
    main()
