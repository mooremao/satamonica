import sys
import ConfigParser
import getopt
import os
import tarfile
import iptools
from random import shuffle

from deploy_lib import *
import SutStatus
import traceback

def sendTestFiles(testFilesPath, devAddress, devTestRootDir, devUtfFile):

    # return True to exclude file from tar
    def tar_exclude_filter(filename):
        if ("/testLogs/" in filename
            or "/testServerLogs/" in filename
            or "BigFile.DELETEME" in filename
            or filename.endswith(".pyc")
        ):
            #print("++ {0}".format(filename))
            return True

        #print("-- {0}".format(filename))
        return False

    print("[INFO] Sending Test files - creating the tar file")
    try:
        os.chdir('{0}/test'.format(testFilesPath))
        tar = tarfile.open(devUtfFile, "w")
        tar.add("./longbeach_utf", exclude=tar_exclude_filter)
    except OSError as detail:
        print("SendTestFiles Exception: ", detail.strerror, detail.filename)
    finally:
        tar.close()

    print("[INFO] sending '{0}' to device '{1}'".format(devUtfFile, devTestRootDir))
    sendToRemote(devAddress, devUtfFile, devTestRootDir)
    runRemoteCommand("cd {0} && rm -rf longbeach_utf && tar xvf {1}".format(devTestRootDir, devUtfFile), devAddress)


def prepareTestSuite(devAddress, devLogsDir, devLogsDirDebug):
    runRemoteCommand("rm -rf {0}; mkdir -p {0}".format(devLogsDir), devAddress)
    runRemoteCommand("rm -rf {0}; mkdir -p {0}".format(devLogsDirDebug), devAddress)

    print("Setting the device time from the PC")
    runRemoteCommand("date -us '$(date -u +%Y.%m.%d-%T)'", devAddress)

    # push the example binaries used by the tests. Add more here as required.
    print("Sending the example files, used by the tests")
    for each in ["ttv_sleep_example"]:
        sendToRemote(devAddress, "../source/tt_tools/examples/{0}".format(each), "/mnt/userdata/")
        runRemoteCommand("chmod +x /mnt/userdata/{0}".format(each), devAddress)


def runTestSuite(fileName, devAddress, devTestRootDir, devLogsDir, localAddress, deviceLog, runRecoveryScenarios, recoveryLogsDir, devUtfFile, baseDir, deploymentDir, localLogsDir, randomize):
    testFile = ConfigParser.ConfigParser()
    testFile.read(fileName)

    testList = testFile.options('GUEST_TESTS')
    if randomize:
        shuffle(testList)
    print("Test sequence will be: {0}".format(testList))
    for test in testList:
        if testFile.get('GUEST_TESTS', test) == 'enabled':
            #Check device is alive
            if (deviceUp(localAddress) and SutStatus.isDeviceReady(devAddress, localAddress)):
                status = runTestCase(test, devAddress, devTestRootDir, devLogsDir, deviceLog)
                if (status):
                    print "Test case '{0}' execution failed" .format(test)
                #Build destination local path where to store the test case
                #We try to recover the logs even if the test case wasn't executed successfully
                testCaseLocalDir = localLogsDir + "/" + test
                testCaseRemoteDir = devLogsDir + "/" + test
                retrieveLogs(testCaseLocalDir, devAddress, testCaseRemoteDir)
            else:
                print "Test case '{0}' skipped due to an erroneous state of the device".format(test)
        else:
            print("\n[INFO] Test case '{0}' is disabled".format(test))


def runTestCase(testCase, devAddress, devTestRootDir, devLogsDir, deviceLog):
    try:
        (status, command) = runRemoteCommandWithStdout("cd {0}/longbeach_utf/clients && python -u ut_runner.py {1} {2} {3} {4}".
            format(devTestRootDir, testCase, devLogsDir, deviceLog, devAddress), devAddress)
        print ("Test Case '{0}' return status '{1}'".format(testCase, status))
    except Exception as e:
        print("\n[ERROR] Failed to run testcase: '{0}': '{1}' Status execution {2}".format(testCase, traceback.format_exception(*sys.exc_info()), status))
        status = e.message
    return status


def retrieveLogs(destDir, devAddress, devLogsDir):
    print("Retrieve logs from device: '{0}' to server: '{1}'".format(devLogsDir, destDir))
    try:
        runRemoteCommand("cd {0} && tar cvf but_logs.tar ./*".format(devLogsDir), devAddress)
        runSystemCommand("mkdir -p {0}".format(destDir))
        fetchFromRemote(devAddress, "{0}/but_logs.tar".format(devLogsDir), destDir)
        runRemoteCommand("cd {0} && rm -r but_logs.tar".format(devLogsDir), devAddress)
        runSystemCommand("tar xvf {0}/but_logs.tar -C {0}".format(destDir))
    except Exception as e:
        print("\n[WARNING] Unable to retrieve the logs from '{0}': '{1}'".format(devLogsDir, sys.exc_info()[0]))


def main():

    configFile = 'LB_BUT_FW.cfg'
    cameraIP = ""                   # default camera ip address not set

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc:t:n:i:", ["config=", "test=", "camera-ip="])
    except getopt.GetoptError as e:
        print("Option error: {0}".format(e))
        sys.exit()

    gtest = False
    testName = None
    testCount = 1
    for opt, arg in opts:
        if opt == '-h':
            print 'USAGE: python run_tests_device.py [-c <config>] [-t <testname>] [-n <count>] [--camera-ip <camera_ip_address>]'
            sys.exit()
        elif opt in ("-c", "--config"):
            configFile = arg
        elif opt in ("-t", "--test"):
            testName = arg
        elif opt in ("-n"):
            testCount = int(arg)
        elif opt in ("-i", "--camera-ip"):
            cameraIP = arg

    if len(args) != 0:
        print("Unhandled arguments remain: Did you mean to use -t ?: {0}".format(args))
        sys.exit()

    config = ConfigParser.ConfigParser()
    status = config.read(configFile)
    if status == []:
        print("[ERROR] configFile '{0}' Not found".format(configFile))
        sys.exit()

    deploymentDir = os.path.dirname(os.path.realpath(__file__)) + '/'
    baseDir = '{0}/../../../'.format(deploymentDir)
    devTestRootDir = config.get('DEVICE', 'test_rootdir')
    devLogsDir = config.get('DEVICE', 'logs_dir')
    devLogsDirDebug = config.get('DEVICE', 'logs_dir_debug')
    localLogsDir = '{0}/{1}'.format(deploymentDir, config.get('DEVICE', 'logs_remote_client_dir'))

    if cameraIP == "":
        devAddress = config.get('DEVICE', 'address')
        locAddress = config.get('LOCAL', 'address')
    else:
        devAddress = cameraIP
        locAddress = iptools.ipv4.long2ip(iptools.ipv4.ip2long(cameraIP)+1)

    runRecoveryScenarios = config.getboolean('DEVICE', 'recovery_scenarios')

    devUtfFile = config.get('DEVICE', 'utf_file')
    deviceLog = config.getboolean('DEVICE', 'deviceLog')
    testFile = config.get('LOCAL', 'tests_file')
    randomize = config.getboolean('LOCAL', 'randomize')

    if not deviceUp(locAddress):      # error message is printed inside deviceUp
        print ('[ERROR] Skipping tests, the device is not up')
        sys.exit()

    if not deviceAlive(devAddress):
        print("[ERROR] Skipping tests, the device is not alive")
        sys.exit()


    print("Taking tests list from '{0}'".format(testFile))
    recoveryLogsDir = config.get('LOCAL', 'recovery_logs_dir')
    #Evaluate if device is ready for running test scenarios
    ready = SutStatus.isDeviceReady(devAddress, locAddress)
    if not ready and runRecoveryScenarios:
        ready = SutStatus.runRecoveryProcedure(devAddress, locAddress, recoveryLogsDir)
    if ready:
        sendTestFiles(baseDir, devAddress, devTestRootDir, devUtfFile)
        prepareTestSuite(devAddress, devLogsDir, devLogsDirDebug)
        for testRun in range(1, testCount+1):
            os.chdir(deploymentDir)
            print("Test run {0} of {1}: Current directory='{2}', Randomized test order={3}".format(testRun, testCount, os.getcwd(), randomize))

            if testName is None:
                runTestSuite(testFile, devAddress, devTestRootDir, devLogsDir, locAddress, deviceLog, runRecoveryScenarios, recoveryLogsDir, devUtfFile, baseDir, deploymentDir, localLogsDir, randomize)
            else:
                runTestCase(testName, devAddress, devTestRootDir, devLogsDir, deviceLog)
                retrieveLogs(localLogsDir, devAddress, devLogsDir)
                print("\nShowing xml results file for '{0}'\n".format(testName))
                try:
                    runSystemCommand("awk '/<error/,/error>/' {0}/{1}/*.xml".format(localLogsDir, testName))
                    runSystemCommand("grep 'failures=' {0}{1}/*.xml".format(localLogsDir, testName))
                except:
                    print("Exception occurred: Needs investigation")    # ignore any errors
                print("\n")

    else:
        print('[ERROR] Device is not available to run any test case. Please review if the image was properly installed and the SD card is properly mounted')

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt as e:
        print("Control-C from user: Aborting all tests")
