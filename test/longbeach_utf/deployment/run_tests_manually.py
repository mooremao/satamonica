import os
from subprocess import Popen, PIPE, STDOUT
import termios, fcntl, sys, os
import getpass
import re
from deploy_lib import *
import ConfigParser
import datetime


# class for communicating with a device via ssh
class ssh():
    def __init__(self):
        print("Constructor for ssh object")
        configFile = "LB_BUT_FW.cfg"
        config = ConfigParser.ConfigParser()
        config.read(configFile)

        self.locAddress = config.get('LOCAL', 'address')
        self.devAddress = config.get('DEVICE', 'address')


    def __del__(self):
        print("Destructor for ssh object")


    def reboot(self):
        self.cmd("shutdown -r now")


    def cmd(self, cmdtext):
        while not deviceUp(self.locAddress, quiet=True):
            print("Device is asleep or not connected - Please wake it up")
            time.sleep(1)

        try:
            while True:
                p = Popen("ssh root@{0} '{1}'".format(self.devAddress, cmdtext), stdout=PIPE, stderr=PIPE, shell=True, preexec_fn=os.setsid)
                (stdOut, stdErr) = p.communicate()
                stdOut = stdOut.strip()
                stdErr = stdErr.strip()

                if "Connection refused" not in stdErr:
                    break

                print("ssh: Got stdout: '{0}' stderr: '{1}' - will retry".format(stdOut, stdErr))
                time.sleep(1)

        except KeyboardInterrupt as e:
            stdOut = ""
            stdErr = "<FONT color=RED>User pressed [Ctrl-C] FAIL</FONT>"

        print("ssh({0}) Returned: '{1}' '{2}'".format(cmdtext, stdOut, stdErr))
        # return stdOut, stdErr, or "[Blank]", whichever is not blank
        if stdOut == "":
            stdOut = stdErr
            if stdErr == "":
                stdOut = "[Blank]"
        return stdOut


    def pushFile(self, srcFile, dstFile):
        while not deviceUp(self.locAddress, quiet=True):
            print("Device is asleep or not connected - Please wake it up")
            time.sleep(1)

        try:
            p = Popen("scp {1} root@{0}:{2}".format(self.devAddress, srcFile, dstFile), stdout=PIPE, stderr=PIPE, shell=True, preexec_fn=os.setsid)
            (stdOut, stdErr) = p.communicate()
            stdOut = stdOut.strip()
            stdErr = stdErr.strip()

        except KeyboardInterrupt as e:
            stdOut = ""
            stdErr = "<FONT color=RED>User pressed [Ctrl-C] FAIL</FONT>"

        print("scp {0}, {1} Returned: '{2}' '{3}'".format(srcFile, dstFile, stdOut, stdErr))
        # return stdOut, stdErr, or "[Blank]", whichever is not blank
        if stdOut == "":
            stdOut = stdErr
            if stdErr == "":
                stdOut = "[Blank]"
        return stdOut

# class for communicating with a device via adb
class adb():
    def cmd(self, cmdtext):
        pass


    def reboot(self):
        pass


    def pushFile(self, srcFile, dstFile):
        pass


# create report with html
class html:

    def __init__(self, report_filename, title, deviceConnectionMethod):
        self.reportFilename = report_filename
        self.f = open(report_filename, "w")
        self.f.write("<!DOCTYPE html>\n<html>\n<body>\n")
        self.f.write("<title>{0}</title>".format(title))

        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.f.write("Manual Testing started at {0}".format(now))

        self.device = deviceConnectionMethod

        self.closed = False

    def __del__(self):
        print("Destructor for html object")
        if not self.closed:
            print("html object was not closed, tidying")
            self.close()
        else:
            print("html object was closed already")


    def close(self):
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.f.write("Manual Testing ended at {0}".format(now))

        self.f.write("\n</html>\n</body>\n")
        print("End of manual test - Well done")
        print("html report is '{0}/{1}'\n".format(os.getcwd(), self.reportFilename))
        self.closed = True
        self.f.close()



    def heading1(self, text):
        self.f.write("<H1>{0}</H1>".format(text))


    # Start a new test table
    def startTestTable(self, testTitle):
        print(testTitle)
        self.f.write("<B>{0}</B>".format(testTitle))
        self.f.write("\n<TABLE border=1 width=100%>\n")


    def writeHTMLTableRow(self, cell1, cell2, cell3, cell4):
        if cell2 == "":
            cell2 = "[Blank]"
        if cell3 == "":
            cell3 = "[Blank]"
        self.f.write("<TR>\n"
                     "<TD>{0}</TD>"
                     "<TD>{1}</TD>"
                     "<TD><PRE>{2}</PRE></TD>"
                     "<TD>{3}</TD>\n"
                     "</TR>\n".format(cell1, cell2, cell3, cell4))


    # Start a new line within the test table
    def startSubTest(self, testTitle):
        print(testTitle)
        self.f.write("<TR>\n"
                     "<TH>{0}</TH>"
                     "<TH>Command</TH>"
                     "<TH>Result</TH>"
                     "<TH>P/F/S</TH></TR>".format(testTitle))


    # Automatic test - expected result puts PASS or FAIL in the Result box
    def testExpectedString(self, qtext, cmd, expected):
        print(qtext)

        result = self.device.cmd(cmd)

        if expected in result:
            passfail = "<FONT color=GREEN>PASS: '{0}' found</FONT>".format(expected)
        else:
            passfail = "<FONT color=RED>FAIL: '{0}' NOT found</FONT>".format(expected)

        self.writeHTMLTableRow(qtext, cmd, result, passfail)
        return result


    # Automatic test - expected result puts PASS or FAIL in the Result box
    # pass two strings. the conversions to numeric for the comparison are done here
    def testExpectedNumericRange(self, qtext, cmd, expectedLow, expectedHigh):
        print(qtext)

        result = self.device.cmd(cmd)
        if int(expectedLow) <= int(result) <= int(expectedHigh):
            passfail = "<FONT color=GREEN>PASS: '{0}' in range({1}, {2})</FONT>".format(result, expectedLow, expectedHigh)
        else:
            passfail = "<FONT color=RED>FAIL: '{0}' NOT in range ({1}, {2})</FONT>".format(result, expectedLow, expectedHigh)

        self.writeHTMLTableRow(qtext, cmd, result, passfail)
        return result


    # Information only test - Just print the text, the command and the result - leave the Result box empty
    def testI(self, text1, cmd):
        print(cmd)
        result = self.device.cmd(cmd)

        self.writeHTMLTableRow(text1, cmd, result, "Information")
        return result


    # Manual test - ask if pass / failed and put PASS or FAIL in the Result box
    # If the cmd is blank, don't run anything on the device
    # The (optional) command is run, then the question text is emitted, and the answer is logged
    def testM(self, qtext, cmd=""):
        c = "A"
        while c in "aA" or c == "":        # Again or newline stripped to an empty string
            try:
                if cmd != "":
                    result = self.device.cmd(cmd)
                print("{0}: (Y)es, (N)o, (P)ass, (F)ail, (S)kip, (A)gain (Q)uit".format(qtext))
                c = sys.stdin.readline().strip()
                print("stdin.read returned '{0}'".format(c))

            except KeyboardInterrupt as e:
                c = "[Ctrl-C]"

        if c in "pPyY":
            passfail = "<FONT color=GREEN>PASS</FONT>"
        elif c in "fFnN":
            passfail = "<FONT color=RED>FAIL</FONT>"
        elif c in "sS":
            passfail = "<FONT color=RED>SKIPPED</FONT>"
        elif c in "qQ":
            passfail = "<FONT color=RED>Quit all subsequent tests</FONT>"
            print("Quitting all subsequent tests - html report is not valid")
            sys.exit()
        else:
            passfail = "<FONT color=RED>User pressed '{0}' FAIL</FONT>".format(c)
            print("Got other char '{0}' (length {1})".format(c, len(c)))

        self.writeHTMLTableRow(qtext, cmd, "User Verified", passfail)
        return c


    def getUserInput(self, text):
        print(text)
        self.f.write("Waited for user: Prompt: '{0}'\n<P>\n".format(text))
        try:
            c = sys.stdin.readline().strip()
        except KeyboardInterrupt as e:
            c = "[Ctrl-C]"
        return c


    def endTestTable(self):
        self.f.write("</TABLE>\n<P>")


    def wait(self, msg):
        c = self.getUserInput("\nWaiting:\n{0}: Press enter to continue".format(msg))
        print("stdin.read returned '{0}'".format(c))


    def getComments(self):
        comments = self.getUserInput("If you have any comments, enter them here.")
        if comments == "":
            comments = "Nothing entered"
        self.f.write("\n<P>\n<B>Tester Comments:</B>\n<PRE>{0}</PRE>\n<P>\n".format(comments))


"""
Open the html test reporter and run the tests
"""
reportName = datetime.datetime.now().strftime('manualTestReport_%Y-%m-%d.html')

# communicating with the device is via ssh
d = ssh()

# reporting object is html. Needs to know how to communicate with the device
r = html(reportName, "Manual test Report", d)

print("Rebooting so that we start with a known state")
d.reboot()                  # reboot here so that it happens in parallel with user input

print("Starting manual tests of the device.")

buildname = r.getUserInput("If this build has a specific name, enter it now, or just press enter if not")
if buildname == "":
    buildname = "today's build"

buildname = buildname.lower()

testername = r.getUserInput("Enter your name or initials")

r.heading1("Manual test of {0}: by {1}".format(buildname, testername))

print("Starting tests now")

r.startTestTable("Get strings from the device")
r.startSubTest("Test get-lbpversion")
unameText = r.testExpectedString("Get current version", "uname -a", buildname)
search = re.search("-lbp_(\w*)_(\w*)", unameText)
if search != None:
    ubuildName = search.group(1)
    ubuildDate = search.group(2)
else:
    ubuildName = ""
    ubuildDate = ""


r.testI("Time on the device", "date")
r.testExpectedString("short lbp", "get-lbp-version; echo $?", ubuildDate)
r.testExpectedString("long lbp", "get-lbp-version -l; echo $?", buildname + "_" + ubuildDate)
r.testExpectedString("Is the Demo App running?", "ps w | grep demo", "./run_demoapp.sh")
r.testExpectedString("Or is the Camera App running?", "ps w | grep tom", "/home/root/tom")
r.testI("What is mounted?", "mount")
r.testI("Is there free space on the devices?", "df -h")

r.testI("SD Card: failsafe_clk_en", "cat /sys/kernel/debug/mmc1/failsafe_clk_en")
r.testI("SD Card: Clock speed", "cat /sys/kernel/debug/mmc1/clock")
r.testI("SD Card: req_timeout_en", "cat /sys/kernel/debug/mmc1/req_timeout_en")

sdCardMountState = r.testExpectedString("Is the SD card read-write? (0=ReadWrite, 1=not present, 2=failed to mount, 3=SD card is ReadOnly)", "check_sdcard.sh; echo $?", "0")
r.endTestTable()
if sdCardMountState != "0":
    sdStates = {None:"None", "0":"is OK", "1":"is not present", "2":"failed to mount", "3":"is ReadOnly"}
    print("SD card {0}: Fatal: Aborting further tests".format(sdStates[sdCardMountState]))
    sys.exit()

d.pushFile("../../../source/tt_tools/examples/ttsystem_rtc_example", "/mnt/userdata/")
d.cmd("chmod +x /mnt/userdata/ttsystem_rtc_example")


r.startTestTable("Read the various sensors on the device")
r.startSubTest("Temperature sensors")
r.testExpectedString("Ambient Temperature", "cat /sys/bus/i2c/devices/4-0071/temp1_input", "No such file or directory")
r.testExpectedNumericRange("SOC Temperature", "cat /sys/bus/i2c/devices/3-0072/temp1_input", "20000", "49000")
r.testExpectedNumericRange("Fuel Gauge Temperature", "cat /sys/class/power_supply/bq27421-0/temp", "200", "450")

r.startSubTest("Pressure sensor BMP280")
r.testI("enable the pressure sensor", "echo 1 > /tmp/sys_platform-presure/enable")   # Yes, "pressure" is not correct
r.testExpectedNumericRange("Grab pressure data", "cat /tmp/sys_platform-presure/pressure", "90000", "105000")
r.endTestTable()

r.startTestTable("Button tests")
r.startSubTest("manual button tests")
r.testExpectedString("Press and hold the UP button", "evtest /dev/input/event0 | head -30", "(KEY_UP)")
r.testExpectedString("Press and hold the DOWN button", "evtest /dev/input/event0 | head -30", "(KEY_DOWN)")
r.testExpectedString("Press and hold the RIGHT button", "evtest /dev/input/event0 | head -30", "(KEY_RIGHT)")
r.testExpectedString("Press and hold the LEFT button", "evtest /dev/input/event0 | head -30", "(KEY_LEFT)")
r.testExpectedString("Press and hold the RECORD button", "evtest /dev/input/event0 | head -30", "(KEY_RECORD)")
r.testExpectedString("Press and hold the STOP button", "evtest /dev/input/event0 | head -30", "(KEY_STOP)")

r.testExpectedString("Press and hold the batt-stick button", "evtest /dev/input/event1 | head -30", "(KEY_BATTERY)")
r.endTestTable()


r.startTestTable("Test the LEDs")
r.startSubTest("Watch the LEDs")
r.testM("Are the LEDs on ?", "echo 255 > /sys/class/leds/LED1/brightness")
r.testM("Are the LEDs off ?", "echo 0 > /sys/class/leds/LED1/brightness")

r.startSubTest("Watch the Backlight")
r.testM("Is the backlight on (maybe only briefly) ?", "echo 255 > /sys/class/leds/Backlight/brightness")
r.testM("Is the backlight off ?", "echo 0 > /sys/class/leds/Backlight/brightness")
r.endTestTable()

r.startTestTable("Test the buzzer")
r.startSubTest("Listen for the buzzer")
r.testM("Did it beep ?", "cd /sys/devices/platform/pwm-beeper; echo 440 > tune; sleep 1; echo 0 > tune")
r.testM("Did it beep twice ?", "cd /sys/devices/platform/pwm-beeper; echo 262 > tune; sleep 1; echo 0 > tune; sleep 1; echo 440 > tune; sleep 1; echo 0 > tune")
r.endTestTable()

r.startTestTable("Accelerometer_lsm303c_acc - Does not need to be enabled")
r.testExpectedString("Accelerometer status", "cat /tmp/sys_platform-acc/device/enable_device", "0")
r.testExpectedString("Grab some Acc data", "evtest /dev/input/event4 | head -35", "(ABS_Z)")
r.endTestTable()

r.startTestTable("Gyroscope_mpu3050 - You may need to press Control-C")
r.testI("Enable the gyro sensor", "echo 1 > /tmp/sys_platform-gyro/device/enable")
r.testExpectedString("Grab some gyro data", "evtest /dev/input/event5 | head -21", "(REL_RZ)")
r.testI("Disable the gyro sensor", "echo 0 > /tmp/sys_platform-gyro/device/enable")
r.testI("Re-enable the gyro sensor", "echo 1 > /tmp/sys_platform-gyro/device/enable")
r.testExpectedString("Grab some gyro data - Should work this time", "evtest /dev/input/event5 | head -21", "(REL_RZ)")
r.testI("Powering off", "shutdown -h now")
r.endTestTable()

r.wait("Device should now be OFF. Short-press (too short to turn on). Then long-press to turn ON the device")
r.startTestTable("Gyroscope_mpu3050 - After short-press power on")
r.testI("Enable the gyro sensor", "echo 1 > /tmp/sys_platform-gyro/device/enable")
r.testExpectedString("After short-press power on failure. Grab gyro data", "evtest /dev/input/event5 | head -21", "(REL_RZ)")
r.endTestTable()

r.startTestTable("Remote Controller Test")
r.wait("Connect the remote control: On the camera Menu: connect -> remote -> searching. Press and hold the top button on the remote until 'connected' on the camera")
r.testM("Did the remote control connect ok ?")
r.testM("Are you able to take a photo / video using the remote control ?")
r.endTestTable()

r.startTestTable("Heart Rate monitor Test")
r.wait("Connect the heart rate monitor: On the camera menu: connect -> sensors -> heart. Touch the two shiny pads to your skin")
r.testM("Did the heart rate monitor connect ok ?")
r.testM("Enter the heart rate value displayed")
r.endTestTable()


r.startTestTable("Viewfinder Test")
r.wait("Connect with the viewfinder app")
r.testM("Can you see live video with the viewfinder app (or the browser 192.168.1.101) ?")
r.endTestTable()

r.startTestTable("Record Video Test")
r.testM("Record a few seconds of video")
r.testM("Record a few still image snapshots")
r.testI("Powering off", "shutdown -h now")
r.endTestTable()

r.startTestTable("Device should be OFF. Remove and look at the batt stick LEDs")
r.testM("Are the batt-stick LEDs ok ?")
r.testM("Is the SD card readable on the PC ?")
r.testM("Are the videos and still images there that you just took ?")
r.testM("Is the device charging in the USB socket ?")
r.testM("Is the device charging by USB cable ?")
r.endTestTable()

r.wait("Eject the SD card from the PC. Restore the batt-stick and turn ON the device")

r.startTestTable("Verify we have the same build at the end of the test")
r.startSubTest("Test uname -a")
r.testExpectedString("Get current version", "uname -a", buildname)
r.endTestTable()

r.getComments()

# close the html report file
r.close()
