import os
import getopt
import ConfigParser
import iptools
from deploy_lib import *
from random import shuffle

def prepareTestSuite(deploymentDir):
    os.chdir(deploymentDir)
    os.environ['PYTHONPATH'] = '{0}/..:{1}'.format(deploymentDir, os.getenv('PYTHONPATH'))

def runTestSuite(fileName, deploymentDir, serverLogsDir, deviceLog, randomize, cameraIP):
    testFile = ConfigParser.ConfigParser()
    testFile.read(fileName)
    testList = testFile.options('SERVER_TESTS')
    if randomize:
        shuffle(testList)
    print("Test sequence will be: {0}".format(testList))
    for test in testList:
        if testFile.get('SERVER_TESTS', test) == 'enabled':
            runTestCase(test, deploymentDir, serverLogsDir, deviceLog, cameraIP)
        else:
            print('\n[INFO] Testing module "{0}" is disabled'.format(test))


def runTestCase(testCase, deploymentDir, serverLogsDir, deviceLog, cameraIP):
    cwd = os.getcwd()                   # remember the old current working directory ..
    os.chdir('{0}/../clients'.format(deploymentDir))
    try:
        runSystemCommand('python ut_runner.py {0} {1} {2} {3}'.format(testCase, serverLogsDir, deviceLog, cameraIP))
    except Exception as e:
        print ('\n[ERROR] Failed to run testcase: {0}: {1}'.format(testCase, sys.exc_info()[0]))
    os.chdir(cwd)                       # .. and restore


def main():

    configFile = "LB_BUT_FW.cfg"
    cameraIP = ""                   # default camera ip address not set

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc:t:n:i:", ["config=", "test=", "camera-ip="])
    except getopt.GetoptError as e:
        print("Option error: {0}".format(e))
        sys.exit()

    testName = None
    testCount = 1

    for opt, arg in opts:
        if opt == '-h':
            print 'USAGE: python run_tests_server.py [-c <config>] [-t <testname>] [-n <count>] [--camera-ip <camera_ip_address>]'
            sys.exit()
        elif opt in ("-c", "--config"):
            configFile = arg
        elif opt in ("-t", "--test"):
            testName = arg
        elif opt in ("-n"):
            testCount = int(arg)
        elif opt in ("-i", "--camera-ip"):
            cameraIP = arg

    if len(args) != 0:
        print("Unhandled arguments remain: Did you mean to use -t ?: {0}".format(args))
        sys.exit()

    config = ConfigParser.ConfigParser()
    status = config.read(configFile)
    if status == []:
        print("[ERROR] configFile '{0}' Not found".format(configFile))
        sys.exit()

    deploymentDir = os.path.dirname(os.path.realpath(__file__)) + '/'
    testFile = config.get('LOCAL', 'tests_file')
    randomize = config.getboolean('LOCAL', 'randomize')
    serverLogsDir = config.get('LOCAL', 'logs_dir')
    deviceLog = config.getboolean('LOCAL', 'deviceLog')
    if cameraIP == "":
        devAddress = config.get('DEVICE', 'address')
        locAddress = config.get('LOCAL', 'address')
    else:
        devAddress = cameraIP
        locAddress = iptools.ipv4.long2ip(iptools.ipv4.ip2long(cameraIP)+1)

    if not deviceUp(locAddress):      # error message is printed inside deviceUp
        print("[ERROR] Skipping tests, the device is not up")
        sys.exit()

    if not deviceAlive(devAddress):
        print("[ERROR] Skipping tests, the device is not alive")
        sys.exit()

    print("Taking tests list from '{0}'".format(testFile))
    runSystemCommand("rm -rf {0}; mkdir -p {0}".format(serverLogsDir))

    prepareTestSuite(deploymentDir)
    openBootTimeLog()

    for testRun in range(1, testCount+1):
        print("Test run {0} of {1}: Current directory='{2}', Randomized test order={3}".format(testRun, testCount, os.getcwd(), randomize))

        if testName is None:
            runTestSuite(testFile, deploymentDir, serverLogsDir, deviceLog, randomize, devAddress)
        else:
            runTestCase(testName, deploymentDir, serverLogsDir, deviceLog, devAddress)
            print("\nShowing xml results file for '{0}'\n".format(testName))
            try:
                runSystemCommand("awk '/<error/,/error>/' {0}{1}/*.xml".format(serverLogsDir, testName))
                runSystemCommand("grep 'failures=' {0}{1}/*.xml".format(serverLogsDir, testName))
            except:
                print("Exception occurred: Needs investigation")    # ignore any errors
            print("\n")

    closeBootTimeLog()

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt as e:
        print("Control-C from user: Aborting all tests")
