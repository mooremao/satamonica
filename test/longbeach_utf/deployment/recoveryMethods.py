from deploy_lib import runRemoteCommandWithStdout
from deploy_lib import runSystemCommandWithStdout
from deploy_lib import sendToRemote
from deploy_lib import fetchFromRemote
import time
import sys
import traceback

def retrieveLogs(devAddress, devLogsDir):
    print("retrieveLogs: Recovering logs from device to server: '{0}'".format(devLogsDir))
    try:
        sendToRemote(devAddress, "../recovery_tools/ttsystem_dump_log_example", "/tmp/ttsystem_dump_log_example")
        print("Dump logs:")
        runRemoteCommandWithStdout("/tmp/ttsystem_dump_log_example /tmp/", devAddress)

        dest = "{0}/recoveryScenarioLogs/{1}".format(devLogsDir, time.strftime("%Y%m%d%H%M%S"))
        runSystemCommandWithStdout("mkdir -p {0}".format(dest))
        fetchFromRemote(devAddress, "/tmp/*.log", dest)
        runRemoteCommandWithStdout("rm -rf /tmp/*.log /tmp/ttsystem_dump_log_example", devAddress)
    except Exception:
        print("\n[WARNING] Unable to retrieve the logs from '{0}': '{1}'".format(devLogsDir, traceback.format_exception(*sys.exc_info())))

def remountPartition(deviceAdress):
    UMOUNT_SDCARD = "systemctl stop mmc"
    CLEANTMP = "rm -rf /tmp/*"
    MAKE_EXEC = "chmod 777 /tmp/ttsystem_sdformat_example"
    RUNTT = "/tmp/ttsystem_sdformat_example"
    MOUNTSDCARD= "mount_sdcard"

    print("Repairing SD Card:")
    try:
        runRemoteCommandWithStdout(CLEANTMP, deviceAdress)

        (code, cameraUmountInfo) = runSystemCommandWithStdout("pwd", deviceAdress)
        print("The current path is {0}".format(cameraUmountInfo))

        sendToRemote(deviceAdress, "../recovery_tools/ttsystem_sdformat_example", "/tmp")

        code = runRemoteCommandWithStdout(UMOUNT_SDCARD, deviceAdress)[0]
        if code == 0:
            print("Formatting SD card:")
            code = runRemoteCommandWithStdout(MAKE_EXEC, deviceAdress)[0]
        if code == 0:
            code = runRemoteCommandWithStdout(RUNTT, deviceAdress)[0]
        if code == 0:
            code = runRemoteCommandWithStdout(MOUNTSDCARD, deviceAdress)[0]
    except Exception:
        print ("Exception raised for the device. Camera failed mounting/unmounting sdcard\n. Exception: {0}", traceback.format_exception(*sys.exc_info()))
        return False
    return code == 0
