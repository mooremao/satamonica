'''
Created on Feb 23, 2015

@author: segura
'''
import sys
import re
import deploy_lib
import recoveryMethods
from time import sleep
import traceback
import subprocess


def isDeviceReady(deviceAddress, localAddress):
    # Check that the device is Up and not rebooting:
    print("isDeviceReady - Starting device status evaluation function...")
    isCamActive = isPartitionAv = False
    if isDeviceStable(deviceAddress, localAddress):
        # This sleep time is needed to detect whether the device is rebooting between tests.
        sleep(1)
        print("isDeviceReady - Checking if Camera Service is active")
        isCamActive = isDeviceStable(deviceAddress, localAddress) and isCameraServiceActive(deviceAddress)
        sleep(1)
        # Check that the partition is r/w
        print("isDeviceReady - Checking if data partition is read-write")
        isPartitionAv = isDeviceStable(deviceAddress, localAddress) and isPartitionReadWrite(deviceAddress)
        sleep(1)
    return isCamActive and isPartitionAv


def runRecoveryProcedure(devAddress, localAddress, recoveryLogsDir):
    print("runRecoveryProcedure - Data partition not available ... Starting recovery mechanism")
    recoveryMethods.retrieveLogs(devAddress, recoveryLogsDir)
    sleep(1)
    isPartitionAv = False
    if isDeviceStable(devAddress, localAddress) and recoveryMethods.remountPartition(devAddress):
        sleep(1)
        # Retry to check the status of the partition
        print("runRecoveryProcedure - Is the SD card R/W after recovery?")
        isPartitionAv = isDeviceStable(devAddress, localAddress) and isPartitionReadWrite(devAddress)
    return isPartitionAv


def isDeviceStable(deviceAddress, localAddress):
    status = deploy_lib.deviceUp(localAddress)
    print("isDeviceStable - Status '{0}'".format(status))
    if not status:
        # We guess the device is rebooting... wait 20 seconds for reboot and to be kind of stable
        print("isDeviceStable - The device is not accessible via ssh... wait for 20 sec in case it is rebooting")
        sleep(20)
        if not deploy_lib.deviceUp(localAddress):
            print("isDeviceStable - The device is not accessible, Suspending test case execution")
            return False
    print("isDeviceStable - The device is up")
    return True


def isCameraServiceActive(deviceAddress):
    EXPECTED_OUTPUT = "Active: active (running)"
    EVALUATE_STATUS = "systemctl --no-pager -l status camera-app.service"
    try:
        print("isCameraServiceActive - Executing '{0}'".format(EVALUATE_STATUS))
        (cameraServiceCode, cameraServiceInfo) = deploy_lib.runRemoteCommandWithStdout(EVALUATE_STATUS, deviceAddress)
    except Exception as e:
        print("Exception raised for the device. Camera is not ready for running a test case\n.Exception: '{0}'", traceback.format_exception(*sys.exc_info()))
        return False
    print("isCameraServiceActive - The camera is status is: '{0}'".format(cameraServiceInfo[:180]))
    return EXPECTED_OUTPUT in cameraServiceInfo


# Return True if the SD card is read-write, otherwise return False
def isPartitionReadWrite(deviceAddress):
    EVALUATE_PARTITION = "mount | grep /mnt/mmc"
    ret = False
    try:
        print("isPartitionAvailable - Executing '{0}'".format(EVALUATE_PARTITION))
        (mountCmdServiceCode, mountCmdOutput) = deploy_lib.runRemoteCommandWithStdout(EVALUATE_PARTITION, deviceAddress, ignore=True)
        print mountCmdOutput
        if "(rw," in mountCmdOutput:  # good
            print("SD card is read-write. OK")
            return True

        elif "(ro," in mountCmdOutput:
            print("SD card is read-only... Needs to be repaired")

        else:
            print("SD card is not mounted")

    except Exception as e:
        print("Camera is not ready to run a test case\n. Exception: '{0}'".format(traceback.format_exception(*sys.exc_info())))

    return ret
