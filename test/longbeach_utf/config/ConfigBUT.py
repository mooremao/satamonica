'''
Created on Nov 14, 2014

@author: segura
'''
import ConfigParser

from config import but_basic_configuration
from config import tc_configuration


class ConfigBUT(object):
    '''
    This class is representing the BUT configuration for a concrete test suite 
    '''
    def __init__(self, name):
        '''
        Constructor
        '''
        self.name = name
        self.tcConfig = tc_configuration.TCConfig(name)
        self.configFile = '../config_files/but.cfg'
        self.butConfig = ConfigParser.ConfigParser()
        self.butConfig.read(self.configFile)


    @property
    def address(self):
        return self.butConfig.get('DEVICE', 'address')

    @property
    def rootDir(self):
        return self.butConfig.get('DEVICE', 'test_rootdir')

    @property
    def logsDir(self):
        return self.butConfig.get('DEVICE', 'logs_dir')

    @property
    def tmpDir(self):
        return self.butConfig.get('DEVICE', 'tmp_dir')

    @property
    def user(self):
        return self.butConfig.get('DEVICE', 'user')

    @property
    def password(self):
        return self.butConfig.get('DEVICE', 'password')

    @property
    def serverAddress(self):
        return self.butConfig.get('SERVER', 'server_address')

    @property
    def serverRootDir(self):
        return self.butConfig.get('SERVER', 'test_server_rootdir')

    @property
    def logLevel(self):
        return self.butConfig.get('TC_GLOBAL_CONFIG', 'loglevel')
