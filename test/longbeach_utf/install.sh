#!/bin/bash
# Call this script with the user name on the jenkins slave machine
# sh install <userName>

echo "Compress the framework"

cd ..
tar cvf lb_utf.tar longbeach_utf/

echo "Moving the tests to jenkins slave"
scp lb_utf.tar $1@jenkins-hesfitness-02:/tmp
 
exit 0
