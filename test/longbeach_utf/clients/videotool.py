from clients import utils
from common import ButTestCase
from subprocess import Popen, PIPE
import logging
import string
import os
import sys
import time
import platform
import uinput
import os.path
import csv
import shutil
import random
import glob

Result="PASS"
def config_record(self,device,Record_time,inbitrate):
    global Result
    os.system("rm -f  -r /mnt/mmc/Generated_streams")
    fh= csv.writer(open("/mnt/mmc/Result.csv","wb"))
    fh.writerow(["File name","Width","Height","Frame rate","Start time","End time","Theoritical frame count","Actual frame count","Error"])
    input_setting=["1920_1080_60_Wide","1920_1080_60_Normal","1920_1080_50_Wide","1920_1080_50_Normal","1920_1080_30_Wide","1920_1080_30_Normal","1920_1080_25_Wide","1920_1080_25_Normal","3840_2160_15_Wide","3840_2160_12.5_Wide","2704_1524_30_Wide","2704_1524_25_Wide","2704_1524_25_Normal","1280_720_120_Wide","1280_720_120_Normal","1280_720_100_Wide","1280_720_100_Normal","1280_720_60_Wide","1280_720_60_Normal","1280_720_50_Wide","1280_720_50_Normal","848_480_180_Wide","848_480_150_Wide"]
    time.sleep(5)
    bitrate=["60","80","100","120","200","250"]
    if not os.path.isdir(self.mypath):
        os.makedirs(self.mypath)
    R2(device)
    if(inbitrate==-1):
        for b in range(6):
            for _ in range(7):
                D2(device)
            R2(device)
            if(b!=0):
                D2(device)
            L2(device)
            for _ in range(7):
                U2(device)
            for _ in range(10):
                D2(device)
            set_sensor_orientation(device,0)
            L2(device)
            R2(device)
            for _ in range(23):
                U2(device)
            for i in range(23):
                record_m(device,input_setting[i],bitrate[b],i,fh,Record_time,"Normal_load")
                D2(device)
            L2(device)
    else:
        pos=0
        while(int(bitrate[pos])!=inbitrate):
            print ("bitrate:%d")%int(bitrate[pos])
            pos+=1
        for _ in range(7):
                D2(device)
        R2(device)
        for _ in range(pos):
            D2(device)
        L2(device)
        for _ in range(7):
            U2(device)
        for i in range(23):
            record_m(device,input_setting[i],bitrate[pos],i,fh,Record_time,"Normal_load")
            D2(device)
        L2(device)
        time.sleep(5)
    return Result

def record_stress_config(self,device,Record_time,input_setting_number,time_interval_in_hour):
    global Result
    fh= csv.writer(open("/mnt/mmc/Result.csv","wb"))
    fh.writerow(["File name","Width","Height","Frame rate","Start time","End time","Theoritical frame count","Actual frame count","Error"])
    input_setting=["1920_1080_60_Wide_Stress","1920_1080_60_Normal_Stress","1920_1080_50_Wide_Stress","1920_1080_50_Normal_Stress","1920_1080_30_Wide_Stress","1920_1080_30_Normal_Stress","1920_1080_25_Wide_Stress","1920_1080_25_Normal_Stress","3840_2160_15_Wide_Stress","3840_2160_12.5_Wide_Stress","2704_1524_30_Wide_Stress","2704_1524_25_Wide_Stress","2704_1524_25_Normal_Stress","1280_720_120_Wide_Stress","1280_720_120_Normal_Stress","1280_720_100_Wide_Stress","1280_720_100_Normal_Stress","1280_720_60_Wide_Stress","1280_720_60_Normal_Stress","1280_720_50_Wide_Stress","1280_720_50_Normal_Stress","848_480_180_Wide_Stress","848_480_150_Wide_Stress"]
    time.sleep(5)
    if not os.path.isdir(self.mypath):
        os.makedirs(self.mypath)
    R2(device)
    for _ in range(10):
        D2(device)
    set_sensor_orientation(device,0)
    L2(device)
    R2(device)
    for _ in range (input_setting_number):
        D2(device)
    itrrnum=0
    timecurr=time.time()
    timeend=timecurr+time_interval_in_hour*60*60
    while(timecurr<timeend):
        L2(device)
        D2(device)
        set_sensor_orientation(device,itrrnum)
        set_ISP_values(device,itrrnum)
        L2(device)
        R2(device)
        device.emit_click(uinput.KEY_RECORD)
        time.sleep(Record_time)
        device.emit_click(uinput.KEY_STOP)
        num_list=[f for f in os.listdir("/mnt/mmc/") if f.endswith('.h264') and "-trans" not in f ]
        file_name=num_list[0].split(".h264")
        source_file="/mnt/mmc/"+str(file_name[0])+".h264"
        dest_file="/mnt/mmc/Generated_streams/"+str(file_name[0])+"_"+str(itrrnum)+"_recorded.h264"
        os.rename(source_file,dest_file)
        if(itrrnum%2==0):
            D2(device)
        else:
            U2(device)
        os.system("rm -f /mnt/mmc/*.h264")
        os.system("rm -f /mnt/mmc/*.avi")
        os.system("rm -f /mnt/mmc/*.info")
        itrrnum+=1
        timecurr=time.time()
    time.sleep(5)
    return Result

def record_stress_heavyload(self,device,Record_time,input_setting_number,time_interval_in_hour):
    global Result
    fh= csv.writer(open("/mnt/mmc/Result.csv","wb"))
    fh.writerow(["File name","Width","Height","Frame rate","Start time","End time","Theoritical frame count","Actual frame count","Error"])
    input_setting=["1920_1080_60_Wide_Stress_HL","1920_1080_60_Normal_Stress_HL","1920_1080_50_Wide_Stress_HL","1920_1080_50_Normal_Stress_HL","1920_1080_30_Wide_Stress_HL","1920_1080_30_Normal_Stress_HL","1920_1080_25_Wide_Stress_HL","1920_1080_25_Normal_Stress_HL","3840_2160_15_Wide_Stress_HL","3840_2160_12.5_Wide_Stress_HL","2704_1524_30_Wide_Stress_HL","2704_1524_25_Wide_Stress_HL","2704_1524_25_Normal_Stress_HL","1280_720_120_Wide_Stress_HL","1280_720_120_Normal_Stress_HL","1280_720_100_Wide_Stress_HL","1280_720_100_Normal_Stress_HL","1280_720_60_Wide_Stress_HL","1280_720_60_Normal_Stress_HL","1280_720_50_Wide_Stress_HL","1280_720_50_Normal_Stress_HL","848_480_180_Wide_Stress_HL","848_480_150_Wide_Stress_HL"]
    time.sleep(5)
    if not os.path.isdir(self.mypath):
        os.makedirs(self.mypath)
    R2(device)
    R2(device)
    for _ in range (input_setting_number):
        D2(device)
    itrrnum=0
    timecurr=time.time()
    time_interval_in_hour=time_interval_in_hour*60*60
    print "\n\ntime interval in hours %d\n\n"%time_interval_in_hour
    timeend=timecurr+time_interval_in_hour
    while(timecurr<timeend):
        record_m(device,input_setting[input_setting_number],"default",itrrnum,fh,Record_time,"Heavy_load")
        os.system("rm -f  /mnt/mmc/TTCamVideo*.avi")
        itrrnum+=1
        timecurr=time.time()
    time.sleep(5)
    return Result

def record_m(device,input_setting,bitrate,i,fh,Record_time,load):
    global Result
    R2(device)
    device.emit_click(uinput.KEY_RECORD)
    if(load=="Normal_load"):
        time.sleep(Record_time)
    else:
        heavyload(device,Record_time)
    device.emit_click(uinput.KEY_STOP)
    time.sleep(1)
    os.system("sync")
    num_list=[f for f in os.listdir("/mnt/mmc/") if f.endswith('.h264') and "-trans" not in f ]
    file_name=num_list[i].split(".h264")
    source_file="/mnt/mmc/"+str(file_name[0])+".h264"
    dest_file="/mnt/mmc/Generated_streams/"+str(file_name[0])+"_"+str(input_setting)+"_"+str(bitrate)+"_"+str(Record_time)+"_recorded.h264"
    source_file_avi="/mnt/mmc/"+str(file_name[0])+".avi"
    dest_file_avi="/mnt/mmc/Generated_streams/"+str(file_name[0])+"_"+str(input_setting)+"_"+str(bitrate)+"_"+str(Record_time)+"_recorded.avi"
    if(os.path.isfile(source_file)):
        statinfo = os.stat(source_file)
        if(statinfo.st_size>0):
            shutil.copy2(source_file,dest_file)
            shutil.copy2(source_file_avi,dest_file_avi)
        else:
            Result="FAIL"
    else:
        Result="FAIL"
    source_file_info="/mnt/mmc/"+str(file_name[0])+"_pri.info"
    dest_file_info="/mnt/mmc/Generated_streams/"+str(file_name[0])+"_"+str(input_setting)+"_"+str(bitrate)+"_"+str(Record_time)+"_recorded.info"
    source_file_info_sec="/mnt/mmc/"+str(file_name[0])+"_sec.info"
    dest_file_info_sec="/mnt/mmc/Generated_streams/"+str(file_name[0])+"_"+str(input_setting)+"_"+str(bitrate)+"_"+str(Record_time)+"_recorded_sec.info"
    if(os.path.isfile(source_file_info)):
        statinfo = os.stat(source_file_info)
        if(statinfo.st_size>0):
            shutil.copy2(source_file_info,dest_file_info)
            shutil.copy2(source_file_info_sec,dest_file_info_sec)

def heavyload(device,Record_time):
    L2(device)
    ##########switcihng on wifi
    os.system("ifconfig wlan0 up")
    time.sleep(10)
    ###########Increasing load by starting multiple data transfer
    if(Record_time>5):
        timecurr=time.time()
        timeend=timecurr+Record_time
        while(timecurr<timeend):
            os.system("for i in 1 2 3 4 5 6 7 8 9 10; do while : ; do : ; done &  done|dd if=/dev/zero of=/mnt/mmc/abc bs=1M count=512 &")
            #Increasing load by switching
            L2(device)
            R2(device)
            os.system("rm -f abc")
            timecurr=time.time()
            print (timeend-timecurr)
    else:
        time.sleep(Record_time)
    device.emit_click(uinput.KEY_STOP)
    time.sleep(1)

def transcode(self,device,Record_time,Transcode_time):
    global Result
    c= csv.writer(open('/mnt/mmc/Zero_file_size_list.csv','wb'))
    c.writerow(["Filename"])
    ctype=["H264HP","H264MP","MJPEG"]
    time.sleep(5)
    R2(device)
    #Navigation to Transcoding menu will change it menu position changes
    for _ in range(8):
         D2(device)
    if not os.path.isdir(self.mypath):
        print "Recorded file in desired format not found Run Recording script first"
    num_list=[f for f in sorted(os.listdir("/mnt/mmc/Generated_streams/")) if f.endswith('_recorded.h264')]
    #Moving video to be transcoded.
    for i in range(len(num_list)):
        ftt_source_file="/mnt/mmc/Generated_streams/"+num_list[i]
        filename=num_list[i].split("_")
        ftt_dest_file="/mnt/mmc/"+filename[0]+".h264"
        shutil.copy2(ftt_source_file,ftt_dest_file)
        filename_info=num_list[i].split(".h264")
        ftt_source_file_info="/mnt/mmc/Generated_streams/"+filename_info[0]+".info"
        ftt_dest_file_info="/mnt/mmc/"+filename[0]+"_pri.info"
        shutil.copy2(ftt_source_file_info,ftt_dest_file_info)
    for i in range(len(num_list)):
        print "At Input file window"
        filename=num_list[i].split("_")
        name=num_list[i].split("_recorded.h264")
        if(i==0):
            R2(device)
        R2(device)
        U2(device)
        U2(device)
        print filename[0]
        #codec type menu(H264HP,H264MP,MJPEG)
        for j in range(len(ctype)):
            print "at codec type"
            if(ctype[j]=="H264HP"):
                R2(device)
                print "Inside H264HP"
                H264transcode(filename[0],ctype[j],name[0],c,self,device,Record_time,Transcode_time)
                print Result
                if(Result=="FAIL"):
                    return Result
                D2(device)
            elif(ctype[j]=="H264MP"):
                print "H264MP not supported"
                D2(device)
            elif(ctype[j]=="MJPEG"):
                print "Inside MJPEG"
                R2(device)
                time.sleep(Transcode_time)
                info_file="/mnt/mmc/Generated_streams/"+str(name[0])+"_recorded.info"
                with open(info_file) as f:
                    frame_count = sum(1 for _ in f) - 3
                image_counter = len(glob.glob1("/mnt/mmc/","*.jpg"))
                logging.info("frame count:%d"%(frame_count))
                logging.info("image_counter:%d"%(image_counter))
                if(image_counter!=frame_count):
                    Result="FAIL"
                    return
                for f in os.listdir("/mnt/mmc/"):
                    if f.endswith('.jpg'):
                        source = "/mnt/mmc/"+str(f)
                        dest = "/mnt/mmc/Generated_streams/"+str(name[0])+"_"+str(f)
                        os.rename(source,dest)
                time.sleep(6)
                L2(device)
        print "Back to input"
        L2(device)
        D2(device)
    return Result
#Inside Codec type
def H264transcode(file_name,ctype,name,log_file,self,device,Record_time,Transcode_time):
    global Result
    print "Ctype cycle started"
    res=["1080","720","WVGA","2.7K"]
    if(ctype=="H264MP"):
        print "in H264MP moving up"
        profile=77
    else:
        profile=100
    U2(device)
    U2(device)
    U2(device)
    for k in range(len(res)):
    #Inside Resolution menu
        R2(device)
        if(res[k]=="1080"):
            frnum=["60","50","30","25","15"]
        elif(res[k]=="720"):
            frnum=["120","100","60","50","30","25","15"]
        elif(res[k]=="WVGA"):
            frnum=["30"]
        else:
            frnum=["60","30","25","15"]
        if res[k]!="WVGA":
            for l in range(len(frnum)):
            #Inside Frame rate menu
                if(l==0):
                    for m in range(len(frnum)):
                        U2(device)
                name_rec=name.split("_")
                iframerate=name_rec[3]
                R2(device)
                if ((float(iframerate) >= float(frnum[l]))or(float(frnum[l])%float(iframerate)==0)):
                    time.sleep(Transcode_time)
                else:
                    time.sleep(1)
                print "rec frame rate: %d" % float(iframerate)
                print "trans frame rate: %s" % str(frnum[l])
                if ((float(iframerate) >= float(frnum[l]))or(float(frnum[l])%float(iframerate)==0)):
                    print "writing file in Generated_streams"
                    source_file="/mnt/mmc/"+str(file_name)+"-trans.h264"
                    print "source: %s"%source_file
                    dest_file="/mnt/mmc/Generated_streams/"+str(name)+"_"+str(profile)+"_"+str(res[k])+"_"+str(frnum[l])+"_trans.h264"
                    print "dest file : %s"%dest_file
                    if(os.path.exists(source_file)):
                        statinfo = os.stat(source_file)
                        print "Transcoded file size : %f" %float(statinfo.st_size)
                        shutil.move(source_file,dest_file)
                        time.sleep(Transcode_time)
                        logging.info("Transcoded video:%s"%(dest_file))
                    else:
                        print "Transcoded file size : 0"
                        Result="FAIL"
                        log_file.writerow([dest_file])
                        return
                else:
                    source_file="/mnt/mmc/"+str(file_name)+"-trans.h264"
                    if(os.path.exists(source_file)):
                        Result="FAIL"
                        return
                L2(device)
                D2(device)
        elif res[k]=="WVGA":
            print "WVGA Start"
            name_rec=name.split("_")
            iframerate=name_rec[3]
            R2(device)
            if ((float(iframerate) >= 30)or(30%float(iframerate)==0)):
                time.sleep(Transcode_time)
            else:
                time.sleep(1)
            if ((float(iframerate) >= 30)or(30%float(iframerate)==0)):
                source_file="/mnt/mmc/"+str(file_name)+"-trans.h264"
                dest_file="/mnt/mmc/Generated_streams/"+str(name)+"_"+str(profile)+"_"+"WVGA"+"_"+"30"+"_trans.h264"
                if(os.path.exists(source_file)):
                    statinfo = os.stat(source_file)
                    print "Transcoded file size : %f" %float(statinfo.st_size)
                    shutil.move(source_file,dest_file)
                    time.sleep(Transcode_time)
                    logging.info("Transcoded video:%s"%(dest_file))
                else:
                    print "Transcoded file size : 0"
                    Result="FAIL"
                    log_file.writerow([dest_file])
                    return
            else:
                    source_file="/mnt/mmc/"+str(file_name)+"-trans.h264"
                    if(os.path.exists(source_file)):
                        Result="FAIL"
                        return
            L2(device)
            D2(device)
            print "WVGA End"
        if res[k]!="WVGA":
            L2(device)
            D2(device)
    L2(device)
    print "Ctype cycle End"

def still_image(self,device):
    global Result
    time.sleep(5)
    R2(device)
    for _ in range(10):
        D2(device)
    set_sensor_orientation(device,0)

    for _ in range(9):
        U2(device)
    R2(device)
    for mode in range(5):
        if mode<2:
            device.emit_click(uinput.KEY_RECORD)
            time.sleep(2)
            D2(device)
        else:
            device.emit_click(uinput.KEY_RECORD)
            time.sleep(20)
            D2(device)
    num_list=[f for f in sorted(os.listdir("/mnt/mmc/")) if f.endswith('.jpg')]
    for i in range(len(num_list)):
        if(os.path.isfile(num_list[i])):
            fileinfo = os.stat(num_list[i])
            Result="PASS"
            if(fileinfo.st_size==0):
                Result="FAIL"
                time.sleep(5)
    return Result

def continuous_test(self,device):
    global Result
    time.sleep(5)
    R2(device)
    D2(device)
    D2(device)
    D2(device)
    R2(device)
    #Capture for 8MP
    conti_capture(device,1)
    conti_capture(device,5)
    conti_capture(device,10)
    conti_capture(device,15)
    conti_capture(device,30)
    conti_capture(device,60)
    #Capture for 16MP
    conti_capture(device,1)
    conti_capture(device,5)
    conti_capture(device,10)
    conti_capture(device,15)
    conti_capture(device,30)
    conti_capture(device,60)
    num_list=[f for f in os.listdir("/mnt/mmc/") if f.endswith('.jpg')]
    for i in range(len(num_list)):
        if(os.path.isfile(num_list[i])):
            fileinfo = os.stat(num_list[i])
            if(fileinfo.st_size == 0):
                Result="FAIL"
                time.sleep(5)
    return Result

def conti_capture(device,capture_time):
    R2(device)
    time_to_wait = capture_time*2+2
    time.sleep(time_to_wait)
    L2(device)
    D2(device)

def time_lapse_test(self,device):
    global Result
    time.sleep(5)
    R2(device)
    D2(device)
    D2(device)
    R2(device)
    #Capture for 1080p
    timelapse_capture(device,1)
    timelapse_capture(device,5)
    timelapse_capture(device,10)
    timelapse_capture(device,15)
    timelapse_capture(device,30)
    timelapse_capture(device,60)
    #Capture for 4K
    timelapse_capture(device,1)
    timelapse_capture(device,5)
    timelapse_capture(device,10)
    timelapse_capture(device,15)
    timelapse_capture(device,30)
    timelapse_capture(device,60)
    #For 0.5sec intervla
    REC(device)
    time.sleep(2)
    STOP(device)
    D2(device)
    REC(device)
    time.sleep(2)
    STOP(device)
    num_list=[f for f in os.listdir("/mnt/mmc/") if f.endswith('.h264')]
    for i in range(len(num_list)):
        if(os.path.isfile(num_list[i])):
            fileinfo = os.stat(num_list[i])
            if(fileinfo.st_size == 0):
                Result="FAIL"
                time.sleep(5)
    return Result

def timelapse_capture(device,capture_time):
    R2(device)
    time_to_wait = capture_time*2+2
    time.sleep(time_to_wait)
    L2(device)
    D2(device)

def start():
    events = (
        uinput.KEY_UP,
        uinput.KEY_DOWN,
        uinput.KEY_LEFT,
        uinput.KEY_RIGHT,
        uinput.KEY_RECORD,
        uinput.KEY_STOP,
        )
    os.system("rm -f  /mnt/mmc/TTCamVideo*.h264")
    os.system("rm -f  /mnt/mmc/TTCamVideo*.avi")
    os.system("rm -f  /mnt/mmc/TTCamVideo*.info")
    for f in os.listdir("/mnt/mmc/"):
        if f.endswith('.jpg'):
            source = "/mnt/mmc/"+str(f)
            dest = "/mnt/mmc/Generated_streams/"+str(f)
            os.rename(source,dest)
    os.system("rm -f  /mnt/mmc/still_*.info")
    # Stop camera app
    os.system("systemctl reset-failed camera-app.service")
    os.system("systemctl stop camera-app")
    status="active"
    while(status!="inactive"):
        curr_status = Popen(["systemctl", "is-active", "camera-app"], stdout=PIPE)
        (status,err) = curr_status.communicate()
        status=status.rstrip()
    # Insert uinput driver
    os.system("modprobe uinput")
    # Create uinput device
    device = uinput.Device(events)
    # Start camera app
    os.system("systemctl start camera-app")
    status="inactive"
    while(status!="active"):
        curr_status = Popen(["systemctl", "is-active", "camera-app"], stdout=PIPE)
        (status,err) = curr_status.communicate()
        status=status.rstrip()
    return device

def stop(device):
    # Stop camera app
    os.system("systemctl reset-failed camera-app.service")
    os.system("systemctl stop camera-app")
    status="active"
    while(status!="inactive"):
        curr_status = Popen(["systemctl", "is-active", "camera-app"], stdout=PIPE)
        (status,err) = curr_status.communicate()
        status=status.rstrip()
    # Delete uinput device
    del device
    time.sleep(2)
    # Start camera app
    os.system("systemctl start camera-app")
    status="inactive"
    while(status!="active"):
        curr_status = Popen(["systemctl", "is-active", "camera-app"], stdout=PIPE)
        (status,err) = curr_status.communicate()
        status=status.rstrip()

def sanity_record(self,device,Record_time,inbitrate):
    global Result
    fh= csv.writer(open("/mnt/mmc/Result.csv","wb"))
    fh.writerow(["File name","Width","Height","Frame rate","Start time","End time","Theoritical frame count","Actual frame count","Error"])
    input_setting=["1920_1080_60_Wide","1920_1080_60_Normal","1920_1080_50_Wide","1920_1080_50_Normal","1920_1080_30_Wide","1920_1080_30_Normal","1920_1080_25_Wide","1920_1080_25_Normal","3840_2160_15_Wide","3840_2160_12.5_Wide","2704_1524_30_Wide","2704_1524_25_Wide","2704_1524_25_Normal","1280_720_120_Wide","1280_720_120_Normal","1280_720_100_Wide","1280_720_100_Normal","1280_720_60_Wide","1280_720_60_Normal","1280_720_50_Wide","1280_720_50_Normal","848_480_180_Wide","848_480_150_Wide"]
    time.sleep(5)
    bitrate=["60","80","100","120"]
    if not os.path.isdir(self.mypath):
        os.makedirs(self.mypath)
    R2(device)
    pos=0
    while(int(bitrate[pos])!=inbitrate):
        print ("bitrate:%d")%int(bitrate[pos])
        pos+=1
    for _ in range(7):
        D2(device)
    R2(device)
    for _ in range(pos):
        D2(device)
    L2(device)
    for _ in range(7):
        U2(device)
    R2(device)
    for i in range(23):
        if(i==0 or i==4 or i==8 or i==10 or i==13 or i==17 or i==21):
            record_m(device,input_setting[i],bitrate[pos],i,fh,Record_time,"Normal_load")
        D2(device)
    L2(device)
    return Result

def set_sensor_orientation(device,orientation):
    R2(device)
    if((orientation%2)==0):
        U2(device)
    else:
        D2(device)
    L2(device)

def set_ISP_values(device,seed_value):
    D2(device)
    R2(device)
    random.seed(seed_value)
    #Brightness
    select_brightness(device)
    #Contrast
    select_contrast(device)
    #Saturation
    select_saturation(device)
    #Hue
    select_hue(device)
    #Sharpness
    select_sharpness(device)
    #White Balance
    select_white_balance(device)
    #AE Metering
    select_AE_metering(device)
    #EV Compensat
    select_EV_compensat(device)
    #Auto Exposure
    select_auto_exposure(device)
    #Auto White Balance
    select_auto_white_balance(device)
    #Moving back to original menu.
    for _ in range(9):
        U2(device)
    L2(device)
    U2(device)

def select_brightness(device):
    select_random_option(device,6,1)

def select_contrast(device):
    select_random_option(device,6,1)

def select_saturation(device):
    select_random_option(device,6,1)

def select_hue(device):
    select_random_option(device,6,1)

def select_sharpness(device):
    select_random_option(device,6,1)

def select_white_balance(device):
    select_random_option(device,5,1)

def select_AE_metering(device):
    select_random_option(device,2,1)

def select_EV_compensat(device):
    select_random_option(device,4,1)

def select_auto_exposure(device):
    select_random_option(device,1,1)

def select_auto_white_balance(device):
    select_random_option(device,1,1)

def select_random_option(device,option,direction):
    R2(device)
    ORN=random.randint(0,option)
    UD=random.randint(0,direction)
    for _ in range(ORN):
        if(UD==0):
            U2(device)
        else:
            D2(device)
    #Moving back to ISP menu
    L2(device)
    #Moving to next item in ISP menu
    D2(device)

def U2(device):
    device.emit_click(uinput.KEY_UP)
    time.sleep(2)

def D2(device):
    device.emit_click(uinput.KEY_DOWN)
    time.sleep(2)

def R2(device):
    device.emit_click(uinput.KEY_RIGHT)
    time.sleep(2)

def L2(device):
    device.emit_click(uinput.KEY_LEFT)
    time.sleep(2)

def REC(device):
    device.emit_click(uinput.KEY_RECORD)

def STOP(device):
    device.emit_click(uinput.KEY_STOP)
    time.sleep(2)
