import os
import logging_config
import settings


class ClientLoggingConfig(logging_config.LoggingConfig):

    def add_file_handlers_for_all_levels(self, log_dir, log_name):
      
        self._add_file_handlers_for_all_levels(log_dir, log_name)

    def configure_logging(self, log_name, results_dir=None, verbose=False):
        super(ClientLoggingConfig, self).configure_logging(
            use_console=True,
            verbose=verbose)

        if results_dir:
            log_dir = os.path.join(results_dir, 'debug')
            if not os.path.exists(log_dir):
                os.mkdir(log_dir)
            self.add_file_handlers_for_all_levels(log_dir, log_name)
