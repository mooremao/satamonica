'''
Created on Dec 17, 2014

@author: segura
'''
import os
import signal
import sys
import re
import threading, time
from subprocess import Popen, PIPE
import utils
import ConfigParser
from threading import Lock

class  ReadLogThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def stdoutandlog(self, text):
        sys.stdout.write(text)
        self.deviceLogFile.write(text)

    def __init__(self, output_path, tcName, serverRun, devAddress, locAddress):
        self.currentTestName = tcName

        self.device_ip = devAddress
        self.server_ip = locAddress

        super(ReadLogThread, self).__init__()
        self._stop = threading.Event()
        self.serverRun = serverRun
        self.deviceLogFile = file(output_path + tcName + '_device_' + utils.getCurrentTimeFormated() + '.log', 'wb')
        self.stdoutandlog("[{0}] Starting device logging for '{1}'\n".format(utils.getCurrentTimeFormated(), self.currentTestName))
        self.process = None
        self.lock = Lock()

    def stop(self):
        self.lock.acquire()
        try:
            cmd = "systemctl kill --signal=USR1 metalog.service"   # switch to synchronous mode
            if self.serverRun:
                p2 = Popen("ssh -T {0}@{1} {2}".format("root", self.device_ip, cmd),
                        stdout=PIPE, shell=True, preexec_fn=os.setsid)
            else:
                p2 = Popen(cmd,
                        stdout=PIPE, shell=True, preexec_fn=os.setsid)
            status = p2.communicate()[0]

            self._stop.set()
            # having the sleep _after_ stop.set() allows the sigusr2 message to be logged at the end of the
            # current log rather than at the start of the next one.
            time.sleep(1)                                    # wait for metalog.service to flush
            if self.process is not None:
                os.killpg(self.process.pid, signal.SIGTERM)  # Send the signal to all the process groups
                self.process = None                          # avoid double kill

        finally:
            cmd = "systemctl kill --signal=USR2 metalog.service"   # switch to asynchronous mode
            if self.serverRun:
                p2 = Popen("ssh -T {0}@{1} {2}".format("root", self.device_ip, cmd),
                        stdout=PIPE, shell=True, preexec_fn=os.setsid)
            else:
                p2 = Popen(cmd,
                        stdout=PIPE, shell=True, preexec_fn=os.setsid)
            status = p2.communicate()[0]

            self.lock.release()

    def stopped(self):
        return self._stop.isSet()

    def startDeviceLog(self, ipAddress):
        if self.serverRun:
            p = Popen("ssh -T {0}@{1} tail -f /var/log/all/current".format("root", ipAddress),
                    stdout=PIPE, shell=True, preexec_fn=os.setsid)
        else:
            p = Popen("tail -f /var/log/all/current",
                    stdout=PIPE, shell=True, preexec_fn=os.setsid)
        return p

    '''
    Return the status of the usb0 interface.
    Check that ifconfig has the ip address. (Could check 'ifconfig usb0')
    '''
    def deviceUp(self, ipAddress):
        cmd = ["ifconfig"]
        ifstatus = Popen(cmd, stdout=PIPE).communicate()[0]
        return re.search("inet addr:" + ipAddress, ifstatus)  # got ip address for the device?


    def sshdUp(self, ipAddress):
        cmd = ["ssh", "-q", "{0}@{1}".format('root', ipAddress), "echo UP"]
        sshdstatus = Popen(cmd, stdout=PIPE).communicate()[0]
        return sshdstatus != ""


    def run(self):
        try:
            self.lock.acquire()
            try:
                p = self.startDeviceLog(self.device_ip)
                self.process = p
                if self.stopped():
                    return
            finally:
                self.lock.release()

            while not self.stopped():
                line = p.stdout.readline()                 # this is a blocking read
                if line:            # not EOF
                    sys.stdout.write("device log: {0}".format(line))
                    self.deviceLogFile.write(line)

                else:
                    if not self.stopped():
                        self.stdoutandlog("[{0}] device log stopped:\n".format(utils.getCurrentTimeFormated()))
                        self.deviceLogFile.flush()

                        time.sleep(1)                      # device is going down
                        while not self.deviceUp(self.server_ip) and not self.stopped():
                            time.sleep(1)                  # wait for ifconfig to say device is alive
                        while not self.sshdUp(self.device_ip) and not self.stopped():
                            time.sleep(1)                  # wait for sshd to start

                        if self.stopped():
                            break

                        self.lock.acquire()
                        try:
                            p = self.startDeviceLog(self.device_ip) # reconnect
                            self.process = p
                            if self.stopped():
                                break
                        finally:
                            self.lock.release()

                        self.stdoutandlog("[{0}] device log restarted:\n".format(utils.getCurrentTimeFormated()))
                    else:
                        print("STOPPED")

        except (OSError, IOError) as e:                             # catch 'broken pipe' and close the file
            self.stdoutandlog(" device log exception: '{0}'\n".format(e))

        finally:
            self.stdoutandlog("[{0}] Ending device logging for '{1}'\n".format(utils.getCurrentTimeFormated(), self.currentTestName))
            self.deviceLogFile.close()
        return
