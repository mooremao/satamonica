from clients import utils
from common.ServerButTestCase import ServerButTestCase
import logging
import string
import os
import sys
import time
import platform
import os.path
import csv
import shutil

def photo(Continutime):
    #Capture in normal mode
    move_to_ready()
    navigate_to_photo()
    UP()
    UP()
    RI()
    RI()
    REC()
    time.sleep(4)
    #Capture in 1s BURST mode
    navigate_to_photo()
    select_burst()
    RI()
    UP()
    RI()
    REC()
    time.sleep(10)
    navigate_to_photo()
    RI()
    RI()
    DW()
    REC()
    time.sleep(10)
    #Capture in 2s BURST mode
    navigate_to_photo()
    select_burst()
    DW()
    RI()
    UP()
    RI()
    REC()
    time.sleep(10)
    navigate_to_photo()
    RI()
    RI()
    UP()
    REC()
    time.sleep(10)

def navigate_to_photo():
    #################################
    #Navigating to Photo menu       #
    #################################
    LE()
    for i in range(5):
        UP()
    #################################
    #Going inside Photo menu        #
    #################################
    RI()
    DW()
    RI()
    time.sleep(2)

def select_burst():
    UP()
    UP()
    DW()
    RI()
    time.sleep(2)

def video(Record_time):
    move_to_ready()
    ####Recording for wide view
    for n in range(4):
        navigate_to_view()
        #Wide view selection
        UP()
        navigate_to_size()
        for j in range(n):
            DW()
        REC()
        time.sleep(Record_time)
        STOP()
    ####Recording for normal view
    for n in range(4):
        navigate_to_view()
        #Wide view selection
        DW()
        navigate_to_size()
        for j in range(n):
            DW()
        REC()
        time.sleep(Record_time)
        STOP()

def navigate_to_view():
    #################################
    #Navigating to video menu       #
    #################################
    LE()
    for i in range(5):
        UP()
    DW()
    #################################
    #Going inside video menu        #
    #################################
    RI()
    DW()
    RI()
    #################################
    #Selection of view              #
    #################################
    DW()
    RI()
    time.sleep(2)

def navigate_to_size():
    LE()
    UP()
    #################################
    #Selection of size              #
    #################################
    RI()
    #################################
    #Making cursor move to top      #
    #################################
    for i in range(3):
        UP()
    time.sleep(2)

def timelapse(Record_time):
    move_to_ready()
    #Capture in time lapse mode
    for k in range(2):
        for j in range(6):
            navigate_to_timelapse()
            UP()
            if(j==0):
                RI()
                if (k==0):
                    UP()
                elif (k==1):
                    DW()
                LE()
            DW()
            RI()
            UP()
            REC()
            time.sleep(Record_time)
            STOP()

def navigate_to_timelapse():
    #################################
    #Navigating to Photo menu       #
    #################################
    LE()
    for i in range(5):
        UP()
    DW()
    DW()
    DW()
    #################################
    #Going inside Time lapse menu   #
    #################################
    RI()
    DW()
    RI()
    time.sleep(2)

def slowmotion(Record_time):
    move_to_ready()
    ####Recording for wide view
    for n in range(3):
        navigate_to_slowmotion_view()
        #Wide view selection
        UP()
        LE()
        UP()
        RI()
        UP()
        UP()
        UP()
        for j in range(n):
            DW()
        REC()
        time.sleep(Record_time)
        STOP()
    ####Recording for normal view
    for n in range(3):
        navigate_to_slowmotion_view()
        #Wide view selection
        DW()
        LE()
        UP()
        RI()
        UP()
        UP()
        UP()
        for j in range(n):
            DW()
        REC()
        time.sleep(Record_time)
        STOP()

def navigate_to_slowmotion_view():
    #################################
    #Navigating to slow motion menu #
    #################################
    LE()
    for i in range(5):
        UP()
    DW()
    DW()
    #################################
    #Going insideslow motion menu   #
    #################################
    RI()
    DW()
    RI()
    #################################
    #Selection of view              #
    #################################
    DW()
    RI()

def cinematic(Record_time):
    move_to_ready()
    ####Recording for cinematic 4k
    navigate_to_cinematic()
    RI()
    UP()
    REC()
    time.sleep(Record_time)
    STOP()
    ####Recording for cinematic 2.7K
    navigate_to_cinematic()
    RI()
    DW()
    REC()
    time.sleep(Record_time)
    STOP()

def navigate_to_cinematic():
    #################################
    #Navigating to video menu       #
    #################################
    LE()
    for i in range(5):
        DW()
    #################################
    #Going inside video menu        #
    #################################
    RI()
    DW()
    RI()

def move_to_ready():
    time.sleep(5)
    """
    Moving to ready state
    """
    for i in range(7):
        RI()
    time.sleep(2)
    LE()
    LE()
    LE()

def DW():
    os.system("./ttdb inject keydown press")
    os.system("./ttdb inject keydown release")
    time.sleep(1)

def UP():
    os.system("./ttdb inject keyup press")
    os.system("./ttdb inject keyup release")
    time.sleep(1)

def LE():
    os.system("./ttdb inject keyleft press")
    os.system("./ttdb inject keyleft release")
    time.sleep(1)

def RI():
    os.system("./ttdb inject keyright press")
    os.system("./ttdb inject keyright release")
    time.sleep(1)

def REC():
    os.system("./ttdb inject keystart press")
    time.sleep(1)
    os.system("./ttdb inject keystart release")
    time.sleep(4)

def STOP():
    os.system("./ttdb inject keystop press")
    time.sleep(2)
    os.system("./ttdb inject keystop press")
    time.sleep(5)
