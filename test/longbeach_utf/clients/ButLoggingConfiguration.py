'''
Created on Nov 18, 2014

@author: segura
'''
import logging
import sys
import time

import colored_logger
import utils


class LoggingConfig:
    '''
    This class is the standard logging configuration for the BUT framework.
    In this configuration, the framework will print out all the information for both console and file 
    '''
    def __init__(self, config, server_dir=None):
        '''
        Constructor
        '''
        self.config = config
        if server_dir is None:
            self.LOGS_DIR = self.config.logsDir
        else:
            self.LOGS_DIR = server_dir
        self.LOGS_LEVEL = self.config.logLevel
        # self.LOG_FILE_NAME = self.config.tcConfig.logFileName()

    def configure(self):
        print ("LoggingConfig - configure")
        self.root = logging.getLogger()
        # colored_logger.install(level=logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.root_handler = colored_logger.ColoredStreamHandler(level=logging.DEBUG, show_name=False, show_severity=False, show_timestamps=False,
                 show_hostname=True,)
        self.root_handler.setLevel(logging.DEBUG)
        self.root_handler.setFormatter(formatter)
        self.root.setLevel(logging.DEBUG)
        self.root.addHandler(self.root_handler)

        self.ch2 = logging.FileHandler(self.LOGS_DIR + "/" + self.config.name + "/" + self.config.name + "_" + utils.getCurrentTimeFormated() + ".log")
        self.ch2.setLevel(logging.INFO)
        self.ch2.setFormatter(formatter)
        self.root.addHandler(self.ch2)

    def deconfigure(self):
        print ("LoggingConfig - deconfigure")
        self.root.removeHandler(self.root_handler)
        self.root_handler.close()

        self.root.removeHandler(self.ch2)
        self.ch2.close()

    @property
    def getLogger(self):
        return self.root
