import os

def parse(filename,Duration):
                #This Function analyses a h264 video file and returns Height,Width,Framerate,Framecount,Bitrate and Profile details
                data=[]
                bitrate=8
                print str(filename)
                try:
                        f=os.popen("/opt/ipnc/videoparams -i /mnt/mmc/%s"%filename)
                except IOError:
                        logging.error("Video file not Found")
                        result.addError(self,sys.exc_info())
                        #raise error.TestFail( "File %s doesn't Exist"%filename)
                for line in f:
                        if "LBP:" in line:
                                data.append(line[4:])
                #print data
                fr=float(data[1][13:])
                wd=int(data[2][9:])
                ht=int(data[3][9:])
                pr=int(data[0][11:])
                try:
                        frc=os.popen("/opt/ipnc/Framecount /mnt/mmc/%s -o /dev/null" %filename)
                except IOError:
                        print("File not Found")
                        #raise error.TestFail( "File %s doesn't Exist"%filename)
                for line in frc:
                        if "LBP:" in line:
                                data.append(line[4:])
                fc =int(data[4][13:])
                if fc !=0:
                        bitrate=calc_bitrate(filename,fc,fr,data)
                else:
                        strError="%s test failed"%filename
                        print(strError)
                        #raise error.TestFail(strError)
                return fr,wd,ht,fc,bitrate,pr

def calc_bitrate(filename,fcnt,fr,data):
        #CALCULATE BITRATE
        path="/mnt/mmc/"
        fs=os.path.getsize(path+filename)
        br=float("%.2f"%((fs*8*fr)/(fcnt*1024*1024)))#calculate the bitrate using file size ,framerate and frame count
        data.append("BITRATE =%s"%br)
        return br
