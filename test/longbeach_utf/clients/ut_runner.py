import os
import sys
import unittest
import junitxml
import readlogthread
import utils


def find(file, path):
    result = []
    # print('find '+file+' in '+path)
    for root, dirs, files in os.walk(path):
        for name in files:
            # print 'Name found ' + name
            if file == name:
                # print("Match found '{0}/{1}'".format(root, name))
                result.append(os.path.join(root, name))
    # print ('find('+file+') - result is ', result)
    return result


def startReadDeviceLogThread(output_path, tcName, serverMode, devAddress, locAddress):
    Th = readlogthread.ReadLogThread(output_path, tcName, serverMode, devAddress, locAddress)
    Th.start()
    return Th

def validName(tcName):
    if '-' in tcName:
        sys.stderr.write(" [ERROR] '{0}' is not a valid python module name. Use underscore\n".format(tcName))
        return False
    return True

def runLBTest(tcName, logsDir, getDeviceLog, devAddress, locAddress):

    if not validName(tcName):
        sys.stderr.write(" [ERROR] Skipping this test '{0}'\n".format(tcName))
        return                       # module has invalid name

    folderName = find(tcName + ".py", "..")
    if folderName == []:  # dont read folderName[0] if got an empty array
        #logger.error("Module: '{0}' not found\n".format(tcName))
        sys.stderr.write(" [ERROR] Module: '{0}.py' not found - Skipping this test\n".format(tcName))
        return                       # module not found

    elif len(folderName) > 1:
        sys.stderr.write(" [ERROR] Found duplicate Modules: {0} Skipping this test\n".format(folderName))
        return                       # found more than one module

    else:
        loader = unittest.TestLoader().discover('..', tcName + '.py')     # this duplicates 'find()'
        #print("TestLoader.discover returned: {0} containing {1} test cases".format(loader, loader.countTestCases()))
        if loader.countTestCases() == 0:
            sys.stderr.write(" [ERROR] Module: '{0}' has zero tests\n".format(tcName))
            return                   # module has zero tests

        serverMode = False
        output_path = logsDir

        print("Running {0}".format(folderName))
        if "ut_server" in folderName[0]:        # Only expecting one test to be found
            serverMode = True

        # Create folder directory
        output_path = output_path + tcName
        if not (os.path.isdir(output_path)):
            os.makedirs(output_path)
        output_path = output_path + "/"
        # Create file logs inside of the test case folder
        fp = file(output_path + tcName + '_' + utils.getCurrentTimeFormated() + '.xml', 'wb')
        # Start reading device log

        try:
            if getDeviceLog:
                readLogTh = startReadDeviceLogThread(output_path, tcName, serverMode, devAddress, locAddress)
            else:
                print("device logging is off / false")
            # Set up the Junit summary log file
            result = junitxml.JUnitXmlResult(fp)
            # Don't do that if you want to see some results
            # result.buffer = True
            result.startTestRun()
            # Redundant result._setupStdout()
            alltests = unittest.TestSuite(loader)
            alltests.run(result)

            result.stopTestRun()
        except KeyboardInterrupt as e:
            print("Quitting due to user Control-C")
        finally:
            if getDeviceLog:
                readLogTh.stop()
    return                           # success


if __name__ == "__main__":

    test = sys.argv[1]
    logsDir = sys.argv[2]
    deviceLog = sys.argv[3]
    cameraIP = sys.argv[4]

    devAddress = cameraIP

    # This is a hack, but it will work for all camera addresses up to 255. Since we only use a small subset of the IP range, this is reasonably safe.
    IPTokenized = devAddress.split(".")
    locAddress = IPTokenized[0]+"."+IPTokenized[1]+"."+IPTokenized[2]+"."+str(int(IPTokenized[3])+1)

    print("Running '{0}' with logging to '{1}' and deviceLog to '{2}' Camera @ {3}, server @ {4}".format(test, logsDir, deviceLog, devAddress, locAddress))
    runLBTest(test, logsDir, deviceLog, devAddress, locAddress)
