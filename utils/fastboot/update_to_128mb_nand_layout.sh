#!/bin/bash

# Script for updating LB WS2 device from old partition layout to the new 128MB nand partition layout.
# The script will first check whether fastboot is functional and all image files are present.
# Next, it will update u-boot and reboot into the new u-boot.
# Once we enter the new u-boot the script flashes all other parts of the device.

set -e

export FASTBOOT=fastboot
export USB_VENDOR_ID=0x1390
export PRODUCT_OUT=./

usage ()
{
	echo "Usage: $0 --path <pathname>";
	exit 1;
}

if [ "$1" = "--path" ] || [ "$1" = "-p" ] ; then
	export PRODUCT_OUT=$2
fi


# =============================================================================
# pre-run
# =============================================================================

# Verify fastboot program is available
# Verify user permission to run fastboot
# Verify fastboot detects a device, otherwise exit
if [ ${FASTBOOT} ]; then
	FASTBOOT="$FASTBOOT -i $USB_VENDOR_ID"
	fastboot_status=`${FASTBOOT} devices 2>&1`
	if [ `echo $fastboot_status | grep -wc "no permissions"` -gt 0 ]; then
		cat <<-EOF >&2
		-------------------------------------------
		 Fastboot does not have permission to access the usb device.
		 Please create a fastboot udev rule, e.g:

		 % cat /etc/udev/rules.d/70-android-tools-fastboot.rules
		        ACTION=="add|change", SUBSYSTEM=="usb", \\
		        ATTRS{idVendor}=="$USB_VENDOR_ID", ATTRS{idProduct}=="d001", \\
		        TAG+="uaccess"
		-------------------------------------------
		EOF
		exit 1
	elif [ "X$fastboot_status" = "X" ]; then
		echo "No device detected. Please ensure that" \
			 "fastboot is running on the target device"
                exit -1;
	else
		device=`echo $fastboot_status | awk '{print$1}'`
		echo -e "\nFastboot - device detected: $device\n"
	fi
else
	echo "Error: fastboot is not available at ${FASTBOOT}"
        exit -1;
fi


# Create the filenames

bootimg="${PRODUCT_OUT}/uImage"
ubootmin="${PRODUCT_OUT}/u-boot.min.nand"
systemimg="${PRODUCT_OUT}/ubifs_longbeach.bin"
recoveryimg="${PRODUCT_OUT}/tt_recovery.img"
fdtimg="${PRODUCT_OUT}/dm385lb.dtb"

if [ ! -e "$ubootmin" ] ; then
  echo "Missing ${ubootmin}"
  exit -1;
fi

if [ ! -e "${bootimg}" ] ; then
  echo "Missing ${bootimg}"
  exit -1;
fi

if [ ! -e "${systemimg}" ] ; then
  echo "Missing ${systemimg}"
  exit -1;
fi

if [ ! -e "${recoveryimg}" ] ; then
  echo "Missing ${recoveryimg}"
  exit -1;
fi

# First update uboot

echo "Erasing environment"
${FASTBOOT} erase environment

echo "Erasing environment"
${FASTBOOT} erase kernel

echo "Erasing environment"
${FASTBOOT} erase recovery

echo "Flashing u-boot-min"
${FASTBOOT} flash uboot ${ubootmin}

echo "Reboot"
${FASTBOOT} reboot

sleep 10

echo "Flashing kernel"
${FASTBOOT} flash kernel ${bootimg}

echo "Flashing filesystem"
${FASTBOOT} flash system ${systemimg}

echo "Flashing filesystem"
${FASTBOOT} flash recovery ${recoveryimg}

echo "Flashing FDT"
${FASTBOOT} flash fdt ${fdtimg}

echo "Erasing userdata"
${FASTBOOT} erase userdata

echo "Erasing environment"
${FASTBOOT} erase environment

#reboot now
${FASTBOOT} reboot

echo ""
echo "Update to 128MB nand partion layout succeed!"
echo ""

