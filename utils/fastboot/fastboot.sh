#!/bin/bash

export FASTBOOT=fastboot
export USB_VENDOR_ID=0x1390
export PRODUCT_OUT=./
export PARTITION="all"
export UBOOT=0
export KERNEL=0
export SYSTEM=0
export RECOVERY=0
export FDT=0

usage ()
{
	echo "Usage: %fastboot.sh --flash <partition> --path <pathname>";
	exit 1;
}

#no args case
if [ "$1" = "--help" ] ; then
	usage
fi

#path specification
if [ "$3" = "--path" ] || [ "$3" = "-p" ] ; then
	export PRODUCT_OUT=$4
fi

if [ "$1" = "--path" ] || [ "$1" = "-p" ] ; then
	export PRODUCT_OUT=$2
fi

if [ "$1" = "--flash" ] || [ "$1" = "-f" ] ; then
	export PARTITION=$2
fi
if [ "$PARTITION" == "uboot" ]; then
	export UBOOT=1
fi

if [ "$PARTITION" == "kernel" ]; then
	export KERNEL=1
fi

if [ "$PARTITION" = "system" ]; then
	export SYSTEM=1
fi

if [ "$PARTITION" == "recovery" ]; then
	export RECOVERY=1
fi

if [ "$PARTITION" == "fdt" ]; then
	export FDT=1
fi

if [ "$PARTITION" == "all" ]; then
	export UBOOT=1
	export KERNEL=1
	export SYSTEM=1
fi

# =============================================================================
# pre-run
# =============================================================================

# Verify fastboot program is available
# Verify user permission to run fastboot
# Verify fastboot detects a device, otherwise exit
if [ ${FASTBOOT} ]; then
	FASTBOOT="$FASTBOOT -i $USB_VENDOR_ID"
	fastboot_status=`${FASTBOOT} devices 2>&1`
	if [ `echo $fastboot_status | grep -wc "no permissions"` -gt 0 ]; then
		cat <<-EOF >&2
		-------------------------------------------
		 Fastboot does not have permission to access the usb device.
		 Please create a fastboot udev rule, e.g:

		 % cat /etc/udev/rules.d/70-android-tools-fastboot.rules
		        ACTION=="add|change", SUBSYSTEM=="usb", \\
		        ATTRS{idVendor}=="$USB_VENDOR_ID", ATTRS{idProduct}=="d001", \\
		        TAG+="uaccess"
		-------------------------------------------
		EOF
		exit 1
	elif [ "X$fastboot_status" = "X" ]; then
		echo "No device detected. Please ensure that" \
			 "fastboot is running on the target device"
                exit -1;
	else
		device=`echo $fastboot_status | awk '{print$1}'`
		echo -e "\nFastboot - device detected: $device\n"
	fi
else
	echo "Error: fastboot is not available at ${FASTBOOT}"
        exit -1;
fi


# Create the filename

bootimg="${PRODUCT_OUT}/uImage"
ubootmin="${PRODUCT_OUT}/u-boot.min.nand"
systemimg="${PRODUCT_OUT}/ubifs_longbeach.bin"
recoveryimg="${PRODUCT_OUT}/tt_recovery.img"
fdtimg="${PRODUCT_OUT}/dm385lb.dtb"


# Verify that all the files required for the fastboot flash
# process are available

if [[ $UBOOT == 1 ]] ; then
if [ ! -e "$ubootmin" ] ; then
  echo "Missing ${ubootmin}"
  exit -1;
fi
echo "Flashing u-boot-min"
${FASTBOOT} flash uboot ${ubootmin}
fi

if [[ $KERNEL == 1 ]] ; then
if [ ! -e "${bootimg}" ] ; then
  echo "Missing ${bootimg}"
  exit -1;
fi
echo "Flashing kernel"
${FASTBOOT} flash kernel ${bootimg}
fi

if [[ $SYSTEM == 1 ]] ; then
if [ ! -e "${systemimg}" ] ; then
  echo "Missing ${systemimg}"
  exit -1;
fi
echo "Flashing filesystem"
${FASTBOOT} flash system ${systemimg}
fi

if [[ $RECOVERY == 1 ]] ; then
if [ ! -e "${recoveryimg}" ] ; then
  echo "Missing ${recoveryimg}"
  exit -1;
fi
echo "Flashing filesystem"
${FASTBOOT} flash recovery ${recoveryimg}
fi

if [[ $FDT == 1 ]] ; then
if [ ! -e "$fdtimg" ] ; then
  echo "Missing ${fdtimg}"
  exit -1;
fi
echo "Flashing FDT"
${FASTBOOT} flash fdt ${fdtimg}
fi

#reboot now
${FASTBOOT} reboot

