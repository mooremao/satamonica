/*!
 * Copyright (C) TomTom International B.V., 2014
 * All rights reserved.
 *
 * \file vidbench.cc
 * \brief This is a benchmark to simulate a real-time video source
 */

#include <algorithm>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <tr1/memory>

#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

#undef USE_FSTREAM

/*!
 * \mainpage vidbench: Real Time video source storage benchmark
 *
 * \section Introduction
 *
 * This program is used to determine what video bitrates a system can record, by
 * simulating real-time video source and checking whether and how often buffer
 * overruns occur. No video encoder is used and it is therefore possible to
 * isolate the storage subsystem from encoder performance.
 *
 * \section Impl Implementation details
 *
 * A periodic timer is initialized which generates a new "frame" of video, which
 * merely consists of an amount of pre-generated random data, at the specified
 * framerate, in (soft) real-time. These frames are appended to a FIFO queue
 * which has a configurable maximum byte size (\em not maximum number of
 * frames), whose output is consumed by a dedicated writer thread and written
 * via the standard C++ iostream library to a file on disk. Thus, the
 * performance of the entire storage subsystem as a whole is tested including
 * the stream buffer, kernel page cache, the underlying file system and storage
 * medium.
 *
 * If the storage system is unable to sustain the configured video bitrate,
 * eventually there will be back pressure on the various levels of the storage
 * subsystem - i.e. if the bitrate is too high, the storage device's cache will
 * fill up, causing pressure on the kernel's page cache, causing writes to the
 * output file to block. The FIFO will fill, and ultimately overflow. The
 * benchmark will report that such an event has occurred and drop frames until
 * the FIFO has been cleared enough to write a new frame.
 *
 * To support storing video without frame dropping, the storage subsystem must
 * provide sufficient throughput to be able to write the file at the required
 * bitrate. If it does not have this, the FIFO will at some point overflow, and
 * continue to overflow, as the storage subsystem cannot cope with the volume of
 * data being written.
 *
 * It's possible, however, that the storage medium has sufficient throughput,
 * but hiccups or lags due to inherent hardware limitations, or other system
 * activity, or contention for system resources (e.g. memory / bus bandwidth /
 * IO priority inversions etc.). So to write real-time video to a file, there
 * are also latency requirements placed on the storage subsystem.
 *
 * The FIFO size, which is configurable, determines the size of storage hiccups
 * that can be tolerated without dropping frames. A large FIFO bound means that
 * video frames will be queued during a storage hiccup and once the subsystem
 * has recovered, the FIFO should be drained faster than it is being filled by
 * the writer thread. A large enough hiccup (or small enough FIFO size) will
 * result in dropped frames.
 * The benchmark reports both FIFO occupancy (during the run and at the end of
 * the benchmark) and dropped frames.
 *
 */

static const int MB = 1048576;

/*!
 * \brief Group-of-pictures
 *
 * This models a set of encoded video frames with a key frame per group. The
 * first frame of each GOP will be large (like an I-frame) and the rest will be
 * small (like P-frames). The peak factor determines the ratio of I to P frame
 * size.
 */
class Gop
{
public:
	/*!
	 * \brief generate frame sizes for a new GOP
	 * \param[in] bits bit budget for GOP
	 * \param[in] frames number of frames in GOP
	 * \param[in] peakfactor ratio of I to P frame size
	 */
	Gop(unsigned int bits, int frames, double peakfactor)
	{
		unsigned int isize = bits / (1 + (frames - 1) / peakfactor);
		unsigned int psize = isize / peakfactor;

		m_framesize.push_back(isize);
		m_framesize.resize(frames, psize);
	}

	/*!
	 * \brief Get vector of frame sizes in GOP
	 * \return vector of frame sizes
	 */
	const std::vector<unsigned int> GetFrameSizes()
	{
		return m_framesize;
	}

private:
	std::vector<unsigned int> m_framesize;
};

/*!
 * \brief A fake frame of encoded video data
 *
 * This models a single frame of encoded video data as a set of random bytes
 */
class Frame
{
public:
	// Default amount of random data to generate, used for fake encoded video data
	static const size_t DEFAULT_DATA_SIZE = 262144; // 256KB

public:
	/*!
	 * \brief Create a new fake frame of encoded video data
	 * \param[in] bits number of bits in the frame
	 */
	Frame(unsigned int bits):
		m_bits(bits)
	{
		// Generate more bytes if frame size is larger than previous amount of random data
		if (s_RandomData.size() < bits / 8) {
			std::cout << "Reading random data: " << bits / 8 << " bytes" << std::endl;
			s_RandomData = InitRandomData(bits / 8);
		}
	}

	/*!
	 * \brief Get buffer of random data
	 * \return std::pair of buffer bytes and length
	 */
	std::pair<const char*, size_t> GetBuf() const
	{
		// No data is actually copied
		return std::make_pair(&s_RandomData[0], m_bits / 8);
	}

private:
	/*!
	 * \brief Initialize the random data used to generate the frames
	 * \param[in] sz number of bytes to generate
	 */
	static std::vector<char> InitRandomData(size_t sz)
	{
		std::ifstream urandom("/dev/urandom", std::ios_base::binary);

		std::vector<char> result(sz);
		urandom.read(&result[0], sz);

		urandom.close();

		return result;
	}

private:
	unsigned int m_bits;

private:
	static std::vector<char> s_RandomData;
};

std::vector<char> Frame::s_RandomData = InitRandomData(DEFAULT_DATA_SIZE);

//! Pointer to fake video frame.
typedef std::tr1::shared_ptr<Frame> FramePtr;

/*!
 * \brief Maintain various statistics related to the video data being written
 *
 * This keeps various statistics over the object's lifetime and also globally
 * \note The bitrate is calculated from what is actually written, not how the
 *       video source has been configured.
 */
class WriterStats
{
public:
	/*!
	 * \brief Create period of time to maintain statistics
	 * \param[in] expires_ms Approx number of milliseconds over which
	 *                       statistics will be sampled.
	 */
	WriterStats(int expires_ms=1000):
		m_expires_ms(expires_ms),
		m_bits(0),
		m_fifo_high(0),
		m_ms(0)
	{
		// Set an event timer to be triggered at end of sampling period
		struct sigevent se;
		memset(&se, 0, sizeof(sigevent));
		se.sigev_notify	= SIGEV_NONE;
		int r = timer_create(CLOCK_MONOTONIC, &se, &m_timer);
		if (r == -1) {
			throw std::runtime_error(std::string("Could not create timer: ") + strerror(errno));
		}

		// Prime the timer and log the start time
		struct itimerspec itspec;
		memset(&itspec, 0, sizeof(itimerspec));
		itspec.it_value.tv_sec = m_expires_ms / 1000;
		itspec.it_value.tv_nsec = (m_expires_ms % 1000) * 1000000;
		timer_settime(m_timer, 0, &itspec, NULL);
		clock_gettime(CLOCK_MONOTONIC, &m_start_tspec);
	}

	~WriterStats()
	{
		timer_delete(m_timer);
		s_total_bits += m_bits;

		// Calculate stats based on lifetime of object, *not* specified sampling period
		timespec end_tspec;
		clock_gettime(CLOCK_MONOTONIC, &end_tspec);
		int elapsed_ms = (end_tspec.tv_sec - m_start_tspec.tv_sec) * 1000 + ((end_tspec.tv_nsec - m_start_tspec.tv_nsec) / 1000000);
		s_total_ms += elapsed_ms;
	}

	/*!
	 * \brief Update statistics based on frame data
	 * \param[in] fp Pointer to frame
	 */
	void AddDatapoint(FramePtr fp)
	{
		m_bits += fp->GetBuf().second * 8;
	}

	/*!
	 * \brief Update FIFO size statistics
	 * \param[in] bytes current byte size of FIFO
	 * \param[in] ms current number of frames in FIFO * frame period in ms
	 */
	void UpdateFifoSize(size_t bytes, int ms)
	{
		if (bytes > m_fifo_high) {
			m_fifo_high = bytes;
			m_ms = ms;
		}

		if (bytes > s_fifo_highest) {
			s_fifo_highest = bytes;
			s_ms = ms;
		}
	}

	/*!
	 * \brief Calculate the bit rate since sampling began
	 * \return Bit rate in bits / second
	 */
	double GetBitrate() const
	{
		timespec cur_tspec;
		clock_gettime(CLOCK_MONOTONIC, &cur_tspec);
		int elapsed_ms = (cur_tspec.tv_sec - m_start_tspec.tv_sec) * 1000 + ((cur_tspec.tv_nsec - m_start_tspec.tv_nsec) / 1000000);

		if (IsExpired()) {
			return m_bits / (elapsed_ms / 1000.0);
		} else {
			return 0.0;
		}
	}

	/*!
	 * \brief Get FIFO high water mark since sampling began
	 * \return Size of FIFO in bytes
	 */
	size_t GetFifoSize() const
	{
		return m_fifo_high;
	}

	/*!
	 * \brief Get FIFO high water mark since sampling began in ms
	 * \return Size of FIFO in ms
	 */
	int GetFifoMs() const
	{
		return m_ms;
	}

	/*!
	 * \brief Has the sampling timer expired
	 */
	bool IsExpired() const
	{
		struct itimerspec itspec;
		timer_gettime(m_timer, &itspec);
		if (itspec.it_value.tv_sec == 0 && itspec.it_value.tv_nsec == 0) {
			return true;
		}

		return false;
	}

	/*!
	 * \brief Get the average bitrate for over the lifetime of the program
	 * \return Bit rate calculated from all WriterStats instances since
	 *         program began, in bits / second
	 *
	 * The bitrate as measured by the writer thread (can be larger than the
	 * configured bitrate if FIFO is drained faster than real-time)
	 */
	static double GetAvgBitrate()
	{
		if (s_total_ms == 0) {
			return 0.0;
		}

		return s_total_bits / (s_total_ms / 1000.0);
	}

	/*!
	 * \brief Get total number of bits written to file since program began
	 * \return Total number of bits logged by all WriterStats instances
	 */
	static long long GetTotalBits()
	{
		return s_total_bits;
	}

	/*!
	 * \brief Log a FIFO overrun event
	 */
	static void NotifyOverrun()
	{
		s_overruncnt++;
	}

	/*!
	 * \brief Get total number of FIFO overruns since program began
	 */
	static int GetOverruns()
	{
		return s_overruncnt;
	}

	/*!
	 * \brief Get number of dropped frames since program began
	 */
	static int GetDroppedFrames()
	{
		return s_dropped_frames;
	}

	/*!
	 * \brief Log a dropped frame event
	 */
	static void NotifyDroppedFrame()
	{
		s_dropped_frames++;
	}

	/*!
	 * \brief Get FIFO high water mark since program began
	 */
	static size_t GetFifoHighest()
	{
		return s_fifo_highest;
	}

	/*!
	 * \brief Get FIFO high water mark since program began in ms
	 */
	static size_t GetFifoHighestMs()
	{
		return s_ms;
	}

private:
	timer_t m_timer;
	int m_expires_ms;
	long m_bits;
	size_t m_fifo_high;
	timespec m_start_tspec;
	int m_ms;

private:
	static long long s_total_bits;
	static long s_total_ms;
	static int s_overruncnt;
	static int s_dropped_frames;
	static size_t s_fifo_highest;
	static int s_ms;
};

long long WriterStats::s_total_bits = 0;
long WriterStats::s_total_ms = 0;
int WriterStats::s_overruncnt = 0;
int WriterStats::s_dropped_frames = 0;
size_t WriterStats::s_fifo_highest = 0;
int WriterStats::s_ms = 0;

/*!
 * \brief Model a video stream
 *
 * A video stream consisting of consecutive groups of pictures, each of which
 * contains frames of fake encoded video data
 */
class VStream
{
public:
	/*!
	 * \brief Create a new video stream
	 * \param[in] bitrate Target bitrate of the fake video data
	 * \param[in] fps Number of frames to generate per second
	 * \param[in] goplen Number of frames per GOP
	 * \param[in] peakfactor Ratio of I-frame size to P-frame size in GOP
	 */
	VStream(int bitrate, int fps, int goplen, double peakfactor):
		m_goplen(goplen),
		m_peakfactor(peakfactor),
		m_bits((long long) goplen * bitrate / fps)
	{
	}

	/*!
	 * \brief Generate the next fake video frame
	 * \return New frame of video data.
	 */
	const FramePtr GetNextFrame()
	{
		if (m_framesize.empty()) {
			// Generate a new GOP if all frames from the previous
                        // GOP have been generated, and add frame sizes to queue
			std::vector<unsigned int> framesizes = Gop(m_bits, m_goplen, m_peakfactor).GetFrameSizes();
			std::copy(framesizes.begin(), framesizes.end(), std::back_inserter(m_framesize));
		}

		// Pop the next frame size from the GOP
		FramePtr result = FramePtr(new Frame(m_framesize.front()));
		m_framesize.pop_front();

		return result;
	}

private:
	int m_goplen;
	int m_peakfactor;
	int m_bits;
	std::deque<unsigned int> m_framesize; //! Queued frame sizes from GOP
};

/*!
 * \brief Real-time video frame generator with FIFO of generated frames
 *
 * A periodic timer is started and one frame is generated, in its own dedicated
 * thread, once per interval
 */
class MediaSource
{
public:
	/*!
	 * \brief Create a new real-time video frame generator
	 * \param[in] bitrate target bitrate for the video stream. The bitrate
	 *            of generated video should never exceed this, but the speed
	 *            at which it is written to disk may, as the FIFO can be
	 *            drained faster than real-time
	 * \param[in] fps Number of frames to generate each second
	 * \param[in] goplen number of frames per GOP
	 * \param[in] peakfactor Ratio of I to P frame size
	 * \param[in] fifo_max Maximum size of FIFO in bytes
	 */
	MediaSource(int bitrate, int fps, int goplen, double peakfactor, int fifo_max):
		m_vstream(bitrate, fps, goplen, peakfactor),
		m_fps(fps),
		m_fifo_bytes(0),
		m_dropped_frames(0),
		m_fifo_max(fifo_max),
		m_frames(0)
	{
		// Data pushed into FIFO by a different thread than the writer
		pthread_mutex_init(&m_lock, NULL);
		pthread_cond_init(&m_newframecond, NULL);

		// Setup periodic timer to spawn new thread when timer expires
		struct sigevent se;
		memset(&se, 0, sizeof(sigevent));
		se.sigev_notify = SIGEV_THREAD;
		se.sigev_notify_function = do_run;
		se.sigev_value.sival_ptr = this;
		int r = timer_create(CLOCK_MONOTONIC, &se, &m_timer);
		if (r == -1) {
			throw std::runtime_error(std::string("Could not create timer: ") + strerror(errno));
		}

		// Prime the periodic timer
		struct itimerspec itspec;
		memset(&itspec, 0, sizeof(itimerspec));
		itspec.it_value.tv_sec = itspec.it_interval.tv_sec = 1 / fps;
		itspec.it_value.tv_nsec = itspec.it_interval.tv_nsec = (1000000000 / fps) % 1000000000;
		timer_settime(m_timer, 0, &itspec, NULL);
	}

	~MediaSource()
	{
		pthread_cond_destroy(&m_newframecond);
		pthread_mutex_destroy(&m_lock);

		timer_delete(m_timer);
	}

	/*!
	 * \brief Get the next frame from the FIFO to write out
	 */
	FramePtr GetFrame()
	{
		pthread_mutex_lock(&m_lock);

		// Wait for new data if the FIFO is empty
		while (m_fifo.empty()) {
			pthread_cond_wait(&m_newframecond, &m_lock);
		}

		FramePtr result = m_fifo.front();
		m_fifo.pop_front();
		m_fifo_bytes -= result->GetBuf().second;
		m_frames--;

		pthread_mutex_unlock(&m_lock);

		return result;
	}

	/*!
	 * \brief Get the current size of the FIFO
	 * \return Size of queued data in the FIFO in bytes
	 */
	int GetFifoSize()
	{
		pthread_mutex_lock(&m_lock);
		int result = m_fifo_bytes;
		pthread_mutex_unlock(&m_lock);

		return result;
	}

	/*!
	 * \brief Return size of FIFO in milliseconds
	 */
	int GetFifoMs()
	{
		pthread_mutex_lock(&m_lock);
		int result = m_frames * 1000 / m_fps;
		pthread_mutex_unlock(&m_lock);

		return result;
	}

	/*!
	 * \brief Notify the media source that data has been read out of the FIFO
	 */
	void notify()
	{
	}

private:
	/*!
	 * \brief Generate a new frame when timer expires, and push it onto the FIFO
	 */
	void Run()
	{
		pthread_mutex_lock(&m_lock);

		if (m_fifo_bytes + m_vstream.GetNextFrame()->GetBuf().second >= m_fifo_max) {
			// FIFO is full, drop frames until there's enough space

			if (m_dropped_frames == 0) {
				std::cout << "Deadline missed, pausing video source" << std::endl;
				WriterStats::NotifyOverrun();
			}
			m_dropped_frames++;
			WriterStats::NotifyDroppedFrame();
		} else {
			// FIFO has enough free space to append a new frame

			if (m_dropped_frames > 0) {
				std::cout << "Restarting video source after " << m_dropped_frames << " dropped frames" << std::endl;
			}
			m_dropped_frames = 0;
			m_fifo.push_back(m_vstream.GetNextFrame());
			m_fifo_bytes += m_fifo.back()->GetBuf().second;
			m_frames++;
			pthread_cond_broadcast(&m_newframecond);
		}

		pthread_mutex_unlock(&m_lock);
	}

	// C to C++ callback adapter
	static void do_run(union sigval sigev_value)
	{
		MediaSource *ms = static_cast<MediaSource*>(sigev_value.sival_ptr);
		ms->Run();
	}

private:
	pthread_t m_thread;
	VStream m_vstream;
	int m_fps;
	pthread_mutex_t m_lock;
	pthread_cond_t m_newframecond;
	std::deque<FramePtr> m_fifo;
	int m_fifo_bytes;
	timer_t m_timer;
	int m_dropped_frames;
	int m_fifo_max;
	int m_frames;
};

/*!
 * \brief Handler to stop program gracefully and print output on ctrl-C
 */
class InterruptHandler
{
public:
	//! Set handler for ctrl-C
	InterruptHandler()
	{
		struct sigaction sa;
		memset(&sa, 0, sizeof(struct sigaction));
		sa.sa_handler = interrupt;
		sigaction(SIGINT, &sa, &m_oldact);
	}

	~InterruptHandler()
	{
		sigaction(SIGINT, &m_oldact, NULL);
	}

	static bool IsInterrupted()
	{
		return s_Interrupted;
	}

private:
	static void interrupt(int sig)
	{
		s_Interrupted = true;
	}

private:
	struct sigaction m_oldact;

private:
	static bool s_Interrupted;
};

bool InterruptHandler::s_Interrupted = false;

//! Print stats from a WriterStats instance
std::ostream& operator<<(std::ostream& os, const WriterStats& ws)
{
	os << (long) ws.GetBitrate() << " bits / second | fifo high == " << (double) ws.GetFifoSize() / MB  << " MB (" << ws.GetFifoMs() << " ms)";
	return os;
}

/*!
 * \brief Top-level of application
 *
 * Set up a real-time video generator + FIFO (media source) and consume its output
 */
class VidBench
{
private:
	static const size_t IOBUFFER_SIZE = 32678;

public:
	VidBench(const char* outfilename, int bitrate, int framerate, int goplen, double peakfactor, int fifo_max):
#ifdef USE_FSTREAM
		m_outfile(outfilename, std::ofstream::binary),
#endif
		m_framerate(framerate),
		m_bitrate(bitrate),
		m_goplen(goplen),
		m_peakfactor(peakfactor),
		m_fifo_max(fifo_max)
	{
		std::cout << "Bitrate is " << m_bitrate << " bits / second" << std::endl;
		std::cout << "Framerate is " << m_framerate << " fps" << std::endl;
		std::cout << "Fifo limit is " << m_fifo_max << std::endl;

#ifndef USE_FSTREAM
		m_outfile = fopen(outfilename, "w");
		if (m_outfile == 0) {
			throw std::runtime_error(std::string("Could not open file \"") + outfilename + "\": " + strerror(errno));
		}

		int r = setvbuf(m_outfile, NULL, _IOFBF, IOBUFFER_SIZE);
		if (r != 0) {
			throw std::runtime_error("Could not set I/O buffer size");
		}
#endif
	}

	~VidBench()
	{
		std::cout << std::endl;
		std::cout << "Wrote " << (double) WriterStats::GetTotalBits() / 8 / MB << " MB" << std::endl;
		std::cout << "Average bitrate: " << (int) WriterStats::GetAvgBitrate() << " bits / second" << std::endl;
		std::cout << "Number of FIFO overruns: " << WriterStats::GetOverruns() << std::endl;
		std::cout << "Number dropped frames: " << WriterStats::GetDroppedFrames() << std::endl;
		std::cout << "FIFO high water: " << (double) WriterStats::GetFifoHighest() / MB << " MB (" << WriterStats::GetFifoHighestMs() << " ms)" << std::endl;
#ifdef USE_FSTREAM
		m_outfile.close();
#else
		fclose(m_outfile);
#endif
	}

	//! Write the output of the FIFO to disk as quickly as we can using the
	//! C++ standard iostream library. Interrupted by ctrl-C
	void Run()
	{
		InterruptHandler ih;
		WriterStats* ws = new WriterStats();

		MediaSource ms(m_bitrate, m_framerate, m_goplen, m_peakfactor, m_fifo_max);

		// Do until ctrl-C is received from the term
		while (!ih.IsInterrupted()) {
			// Get next frame from FIFO
			FramePtr fp = ms.GetFrame();

			// Write data to storage using std ofstream
#ifdef USE_FSTREAM
			m_outfile.write(fp->GetBuf().first, fp->GetBuf().second);
#else
			int r = fwrite(fp->GetBuf().first, fp->GetBuf().second, 1, m_outfile);
			if (r != 1) {
				throw std::runtime_error("Could not write to output file");
			}
#endif

			// Notify FIFO that frame has been consumed
			ms.notify();

			// Update and print stats
			ws->AddDatapoint(fp);
			ws->UpdateFifoSize(ms.GetFifoSize(), ms.GetFifoMs());
			if (ws->IsExpired()) {
				std::cout << "Stats: " << *ws << std::endl;
				delete ws;
				ws = new WriterStats();
			}
		}

		delete ws;
	}

private:
#ifdef USE_FSTREAM
	std::ofstream m_outfile;
#else
	FILE *m_outfile;
#endif
	int m_framerate;
	int m_bitrate;
	int m_goplen;
	double m_peakfactor;
	int m_fifo_max;
};

int main(int argc, char *argv[])
{
	static const int DEFAULT_BITRATE = 2000000; // 2Mbit
	static const int DEFAULT_FRAMERATE = 60;
	static const int DEFAULT_GOPLEN = DEFAULT_FRAMERATE; // 1s
	static const int DEFAULT_PEAKFACTOR = 8.0; // Simulated I-frames are 8x larger than P-frames
	static const int DEFAULT_FIFO_SIZE = 3;

	if (argc < 2) {
		std::cerr << "Usage: vidbench <outfile> [<bitrate>] [<fifo limit MB>]" << std::endl;
		std::cerr << "Default bitrate is " << DEFAULT_BITRATE << std::endl;
		std::cerr << "Default fifo limit is " << DEFAULT_FIFO_SIZE << std::endl;
		return 1;
	}

	const char *outfilename = argv[1];

	int bitrate = DEFAULT_BITRATE;
	if (argc > 2) {
		bitrate = atoi(argv[2]);
	}

	int fifosize = DEFAULT_FIFO_SIZE;
	if (argc > 3) {
		fifosize = atoi(argv[3]);
	}

	VidBench App(outfilename, bitrate, DEFAULT_FRAMERATE, DEFAULT_GOPLEN, DEFAULT_PEAKFACTOR, fifosize * MB);
	App.Run();

	return 0;
}
