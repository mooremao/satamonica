#include <stdio.h>
#include <stdlib.h>
#include "ttsystem.h"

int main(int argc, char** argv)
{
	int ret;

	if (argc < 2) {
		printf("Usage: %s <date>\n\t<date> in 'YYYY-MM-DD hh:mm:ss' format (example: '2015-05-07 12:30:00')\n", argv[0]);
		return EXIT_FAILURE;
	}

	printf("Setting GSD RTC time...\n");

	ret = ttsystem_set_rtc(argv[1]);
	if(ret < 0) {
		printf("Error setting RTC time: %d\n", ret);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
