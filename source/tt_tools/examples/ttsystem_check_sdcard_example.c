#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "ttsystem.h"

int main(int argc, char *argv[])
{
	int ret;
	bool repair = false;

	if (argc >= 2) {
		if(strcmp("repair", argv[1]) == 0) {
			repair = true;
		}
	}

	printf("Checking SD card (repair=%d)...\n\n", (int) repair);
	ret = ttsystem_check_sdcard(repair);

	printf("ttsystem_check_sdcard return value: %d\n", ret);

	if (ret < 0)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}
