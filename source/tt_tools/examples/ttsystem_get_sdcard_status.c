#include <stdio.h>
#include <stdlib.h>
#include "ttsystem.h"

int main(void)
{
	int ret;

	printf("Checking SD Card ...\n");
	ret = ttsystem_get_sdcard_status();
	switch (ret) {
	case 0:
		printf("SD card available and ready for use\n");
		break;
	case 1:
		printf("SD card not present\n");
		break;
	case 2:
		printf("SD card failed to mount\n");
		break;
	case 3:
		printf("SD card readonly\n");
		break;
	default:
		printf("Unknown error\n");
		break;
	}

	return 0;
}
