#include <stdio.h>
#include <stdlib.h>
#include "ttsystem.h"


int main(int argc, char *argv[])
{
	int ret;

	if (argc != 2) {
		printf("Usage: %s <command>\n\tValid commands are: reboot, update, recovery, poweroff, factory\n", argv[0]);
		return -1;
	}

	if (strcmp("reboot", argv[1]) == 0) {
		ret = ttsystem_reboot(TTSYSTEM_REBOOT_NORMAL);
	} else if (strcmp("update", argv[1]) == 0) {
		ret = ttsystem_reboot(TTSYSTEM_REBOOT_UPDATE);
	} else if (strcmp("recovery", argv[1]) == 0) {
		ret = ttsystem_reboot(TTSYSTEM_REBOOT_RECOVERY);
	} else if (strcmp("poweroff", argv[1]) == 0) {
		ret = ttsystem_poweroff();
	} else if (strcmp("factory", argv[1]) == 0) {
		ret = ttsystem_factory_reset();
	} else {
		printf("Invalid command\n");
		ret = -1;
	}

	return ret;
}
