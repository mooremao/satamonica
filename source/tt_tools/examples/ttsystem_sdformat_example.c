#include <stdio.h>
#include <stdlib.h>
#include "ttsystem.h"

int main()
{
	int ret;

	printf("Formatting SD Card ...\n\n");
	ret = ttsystem_format_sdcard();
	if(ret < 0) {
		printf("Formatting SD Card failed: %s\n", strerror(-ret));
		return EXIT_FAILURE;
	}
	else
		printf("Formatting SD Card completed.\n");

	return EXIT_SUCCESS;
}
