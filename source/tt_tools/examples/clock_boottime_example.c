#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <ttv.h> /* defines ttv_sleep and CLOCK_BOOTTIME */

void print_time(struct timespec *tp)
{
	time_t t = tp->tv_sec;
	struct tm tm = *localtime(&t);

	printf("\t%02d:%02d:%02d\n", tm.tm_hour, tm.tm_min, tm.tm_sec);
}

void print_all_clocks()
{
	struct timespec tp;
	int ret;

	/* CLOCK_REALTIME represents time-of-day time. It is affected by changes
	 * in system time
	 */
	ret = clock_gettime(CLOCK_REALTIME, &tp);
	if (ret < 0)
		printf("clock_gettime failed: %s", strerror(errno));
	printf("CLOCK_REALTIME time is");
	print_time(&tp);

	/* CLOCK_MONOTONIC represents monotonic time since some specified starting
	 * point. It is unaffected by change in system time. It does not include
	 * time spent in suspend / deepsleep
	 */
	ret = clock_gettime(CLOCK_MONOTONIC, &tp);
	if (ret < 0)
		printf("clock_gettime failed: %s", strerror(errno));
	printf("CLOCK_MONOTONIC time is");
	print_time(&tp);

	/* CLOCK_BOOTTIME represents monotonic time since some specified starting
	 * point including time spent in suspend / deepsleep
	 */
	ret = clock_gettime(CLOCK_BOOTTIME, &tp);
	if (ret < 0)
		printf("clock_gettime failed: %s", strerror(errno));
	printf("CLOCK_BOOTTIME time is");
	print_time(&tp);
}

int main()
{
	time_t new_time;

	printf("** This program demonstrates the use of clock_gettime **\n\n");

	print_all_clocks();

	/* Change system time */
	new_time = 1420070400; /* 1 Jan 2015 00:00:00 UTC */
	stime(&new_time);
	printf("\n\nAfter changing system time ...\n");
	print_all_clocks();

	/* Goto deep sleep and return */
	ttv_sleep(60 * 1000);
	printf("\n\nAfter waking up from deep sleep of 1 minute ...\n");
	print_all_clocks();

	return EXIT_SUCCESS;
}
