#include <stdio.h>
#include <stdlib.h>
#include "ttsystem.h"

int main()
{
	int ret1, ret2;
	int32_t age, status;
	uint32_t pred_int;

	printf("Retrieving SGEE quickfix status...\n");

	ret1 = ttsystem_get_sgee_info(TTSYSTEM_SGEE_GPS, &status, &age, &pred_int);
	if(ret1 < 0) {
		printf("Error getting SGEE data for navsys GPS\n");
	} else {
		printf("Navsys GPS - status:%d age:%d pi:%u\n", status, age, pred_int);
	}

	ret2 = ttsystem_get_sgee_info(TTSYSTEM_SGEE_GLO, &status, &age, &pred_int);
	if(ret2 < 0) {
		printf("Error getting SGEE data for navsys GLONASS\n");
	} else {
		printf("Navsys GLONASS - status:%d age:%d pi:%u\n", status, age, pred_int);
	}

	if (ret1 || ret2)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}
