#include <stdio.h>
#include <stdlib.h>
#include <ttv.h>
#include <ttsystem.h>

int main(int argc, char *argv[])
{
	ttv_wakeup_t ret;
	int timeout = 2000;

	if (argc >= 2) {
		timeout = atoi(argv[1]);
	}

	if (argc >= 3) {
		while (ttsystem_bt_sleep_allowed() != 0);
	}

	printf("ttv_sleep(%d)\n", timeout);
	ret = ttv_sleep(timeout);

	printf("ttv_sleep(%d) result: ", timeout);

	switch (ret) {
	case TTV_WAKE_OTHER:
		printf("TTV_WAKE_OTHER");
		break;
	case TTV_WAKE_BUTTON:
		printf("TTV_WAKE_BUTTON");
		break;
	case TTV_WAKE_TIMER:
		printf("TTV_WAKE_TIMER");
		break;
	case TTV_WAKE_BLE:
		printf("TTV_WAKE_BLE");
		break;
	case TTV_SLEEP_MORE:
		printf("TTV_SLEEP_MORE");
		break;
	case TTV_WAKE_EXT_MIC:
		printf("TTV_WAKE_EXT_MIC");
		break;
	case TTV_WAKE_POWER_PRN:
		printf("TTV_WAKE_POWER_PRN");
		break;
	case TTV_WAKE_BATT_TEMP:
		printf("TTV_WAKE_BATT_TEMP");
		break;
	case TTV_WAKE_SOC_BATT_LOW:
		printf("TTV_WAKE_SOC_BATT_LOW");
		break;
	case TTV_WAKE_SOC_TURN_OFF:
		printf("TTV_WAKE_SOC_TURN_OFF");
		break;
	case TTV_SLEEP_ERROR_INVALID_TIMEOUT:
		printf("TTV_SLEEP_ERROR_INVALID_TIMEOUT");
		break;
	case TTV_SLEEP_ERROR_INTERNAL:
		printf("TTV_SLEEP_ERROR_INTERNAL");
		break;
	case TTV_SLEEP_ERROR_BLE_STATE:
		printf("TTV_SLEEP_ERROR_BLE_STATE");
		break;
	case TTV_SLEEP_ERROR_TIMEOUT_TOO_SHORT:
		printf("TTV_SLEEP_ERROR_TIMEOUT_TOO_SHORT");
		break;
	case TTV_SLEEP_ERROR_WOKEN_UP:
		printf("TTV_SLEEP_ERROR_WOKEN_UP");
		break;
	}

	printf("\n");

	return EXIT_SUCCESS;
}
