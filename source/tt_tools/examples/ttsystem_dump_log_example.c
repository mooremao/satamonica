#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "ttsystem.h"

int main(int argc, char *argv[])
{
	int ret;
	const char *dest_dir;
	char file_path[PATH_MAX] = "";

	if (argc <= 1) {
		printf("Missing destination argument.\n");
		return EXIT_FAILURE;
	}

	dest_dir = argv[1];

	if(argc >= 3 && strcmp("NULL", argv[2]) == 0) {
		printf("ttsystem_dump_log(\"%s\", NULL)...\n\n", dest_dir);

		ret = ttsystem_dump_log(dest_dir, NULL);

		printf("ttsystem_dump_log return value: %d\n", ret);
	} else {
		printf("ttsystem_dump_log(\"%s\", file_path)...\n\n", dest_dir);

		ret = ttsystem_dump_log(dest_dir, file_path);

		printf("ttsystem_dump_log return value: %d\n", ret);
		printf("file_path: %s\n", file_path);
	}

	if (ret != 0)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}

