/*
 * \file Show battery charging status
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <time.h>
#include <linux/input.h>
#include "ttsystem.h"

#define LONG_PRESS_TIME_MSEC 1000
#define DEEP_SLEEP_TIME_MSEC 60000
#define REPLUGGING_TIMEOUT 5 /* time during which charger may be reconnected, in seconds */
#define REPLUGGING_RESOLUTION 10 /* charger checked REPLUGGING_RESOLUTION times per second */
#define REPLUGGING_SLEEP 1000000UL / REPLUGGING_RESOLUTION

/*Eliminating udev services from boot to decrease boot time,
 *we use /dev/input/event0 instead of /dev/input/by-path/platform-gpio-keys-event
 *as udev makes the symlink*/
#define GPIO_KEYS_EVENT "/dev/input/event0"

#define TOMTOM_MCU_ONLINE "/sys/class/power_supply/tomtom_mcu/online"
#define BATTERY_CACHE "/sys/class/power_supply/bq27421-0/device/invalidate_cache"
#define BATTERY_STATUS "/sys/class/power_supply/bq27421-0/status"
#define BATTERY_CAPACITY "/sys/class/power_supply/bq27421-0/capacity_level"
#define BATTERY_VOLTAGE "/sys/class/power_supply/bq27421-0/voltage_avg"

#define BUFSIZE 20

#define CLEAR_DISPLAY "fb-display clear"
#define SHOW_LOGO "fb-display logo"
#define SHOW_CHARGING "fb-display /usr/share/assets/screens/usbpoweron-charging.bmp"
#define SHOW_FULL "fb-display /usr/share/assets/screens/usbpoweron-full.bmp"
#define SHOW_TOO_LOW_TO_BOOT "fb-display /usr/share/assets/screens/usbpoweron-toolowtoboot.bmp"
#define SHOW_TOOHOT_ALERT "/usr/bin/cam-critical note_high_temp small user"

#define POWEROFF "systemctl --no-block poweroff"
#define REBOOT "systemctl reboot"
#define ACKNOWLEDGE_BOOT "acknowledge_boot"

#define BOOT_VOLTAGE 3300000 /* microvolt */

static int keyfd;
static int mcufd;
static int batcachefd;
static int batstatusfd;
static int batcapfd;
static int batvoltfd;

static int accept_discharge = 1;

static pthread_t power_button_thread;
static pthread_t battery_status_thread;
static int power_button_check_in_progress = 0;

enum exit_reason_t {
	EXIT_REASON_NONE,
	EXIT_REASON_POWEROFF,
	EXIT_REASON_REBOOT,
};

static enum exit_reason_t exit_reason;

static void * check_power_button_press(void *);
static void * check_battery_and_sleep(void *);

static void init(void)
{
	exit_reason = EXIT_REASON_NONE;

	/* Enter low power state */
	ttsystem_set_powermode(TTSYSTEM_POWERMODE_DEFAULT);

	/* Acknowledge boot */
	system(ACKNOWLEDGE_BOOT);

	/* Open file descriptors */

	keyfd = open(GPIO_KEYS_EVENT, O_RDONLY | O_NONBLOCK | O_CLOEXEC);
	if (keyfd < 0) {
		fprintf(stderr, "battery-charging: cannot open %s\n", GPIO_KEYS_EVENT);
		exit(1);
	}

	mcufd = open(TOMTOM_MCU_ONLINE, O_RDONLY | O_CLOEXEC);
	if (mcufd < 0) {
		fprintf(stderr, "battery-charging: cannot open %s\n", TOMTOM_MCU_ONLINE);
		goto err_mcufd;
	}

	batcachefd = open(BATTERY_CACHE, O_WRONLY | O_CLOEXEC);
	if (batcachefd < 0) {
		fprintf(stderr, "battery-charging: cannot open %s\n", BATTERY_CACHE);
		goto err_batcachefd;
	}

	batstatusfd = open(BATTERY_STATUS, O_RDONLY | O_CLOEXEC);
	if (batstatusfd < 0) {
		fprintf(stderr, "battery-charging: cannot open %s\n", BATTERY_STATUS);
		goto err_batstatusfd;
	}

	batcapfd = open(BATTERY_CAPACITY, O_RDONLY | O_CLOEXEC);
	if (batcapfd < 0) {
		fprintf(stderr, "battery-charging: cannot open %s\n", BATTERY_CAPACITY);
		goto err_batcapfd;
	}

	batvoltfd = open(BATTERY_VOLTAGE, O_RDONLY | O_CLOEXEC);
	if (batvoltfd < 0) {
		fprintf(stderr, "battery-charging: cannot open %s\n", BATTERY_VOLTAGE);
		goto err_batvoltfd;
	}

	if (pthread_create(&power_button_thread, NULL, check_power_button_press, NULL) < 0) {
		fprintf(stderr, "battery-charging: cannot create power button press thread\n");
		goto err_thread;
	}

	if (pthread_create(&battery_status_thread, NULL, check_battery_and_sleep, NULL) < 0) {
		fprintf(stderr, "battery-charging: cannot create check battery thread\n");
		goto err_thread;
	}

	return;

err_thread:
	close(batvoltfd);
err_batvoltfd:
	close(batcapfd);
err_batcapfd:
	close(batstatusfd);
err_batstatusfd:
	close(batcachefd);
err_batcachefd:
	close(mcufd);
err_mcufd:
	close(keyfd);
	exit(1);
}

static void deinit(void)
{
	close(keyfd);
	close(mcufd);
	close(batcachefd);
	close(batstatusfd);
	close(batcapfd);
	close(batvoltfd);
}

static void poweroff(void)
{
	/* Clear display and power off */
	fprintf(stderr, "battery-charging: Powering off\n");
	exit_reason = EXIT_REASON_POWEROFF;
	system(CLEAR_DISPLAY);
	system(POWEROFF);
}

static void reboot(void)
{
	/* Show TomTom logo and reboot */
	fprintf(stderr, "battery-charging: Rebooting\n");
	exit_reason = EXIT_REASON_REBOOT;
	system(SHOW_LOGO);
	system(REBOOT);
}

static int invalidate_battery_cache(void) {
	int n;

	n = write(batcachefd, "1\n", sizeof("1\n"));
	if (n < 0) {
		fprintf(stderr, "battery-charging: cannot write %s\n", BATTERY_CACHE);
	}

	return n;
}

static void * check_power_button_press(void * arg)
{
	struct input_event keyev;
	struct timeval keypress_time, diff;
	unsigned long diff_ms;
	unsigned char buf[BUFSIZE];
	unsigned char voltbuf[BUFSIZE];
	unsigned long voltage;

	/* Key press time is initially zero */
	timerclear(&keypress_time);

	while (exit_reason == EXIT_REASON_NONE) {
		if(read(keyfd, &keyev, sizeof(struct input_event)) == sizeof(struct input_event)
				&& keyev.type == EV_KEY && keyev.code == KEY_RECORD) {

			if (keyev.value != 0) {
				/* Power button pressed */
				if (!timerisset(&keypress_time)) {
					/* First key press event */
					power_button_check_in_progress = 1;
					keypress_time = keyev.time;
				}

				timersub(&keyev.time, &keypress_time, &diff);
				diff_ms = diff.tv_sec * 1000 + (diff.tv_usec / 1000);
				/* If power button pressed for 1 second, reboot */
				if (diff_ms >= LONG_PRESS_TIME_MSEC) {
					if (invalidate_battery_cache() < 0) {
						timerclear(&keypress_time);
						power_button_check_in_progress = 0;
						continue;
					}

					lseek(batcapfd, 0, SEEK_SET);
					if (read(batcapfd, buf, BUFSIZE) < 0) {
						fprintf(stderr, "battery-charging: cannot read %s\n", BATTERY_CAPACITY);
						timerclear(&keypress_time);
						power_button_check_in_progress = 0;
						continue;
					}

					lseek(batvoltfd, 0, SEEK_SET);
					if (read(batvoltfd, voltbuf, BUFSIZE) < 0) {
						fprintf(stderr, "battery-charging: cannot read %s\n", BATTERY_VOLTAGE);
						timerclear(&keypress_time);
						power_button_check_in_progress = 0;
						continue;
					}
					voltage = strtoul(voltbuf, NULL, 10);

					if (!strncmp(buf, "Critical", strlen("Critical")) ||
							voltage < BOOT_VOLTAGE) {
						/* If battery capacity is 'Critical' show too low to boot */
						system(SHOW_TOO_LOW_TO_BOOT);
						sleep(1);
						timerclear(&keypress_time);
						power_button_check_in_progress = 0;
					}
					else {
						reboot();
					}
				}
			}
			else {
				/* Power button released */
				timerclear(&keypress_time);
				power_button_check_in_progress = 0;
			}
		}
	}

	return NULL;
}

static void check_charging_status(void)
{
	unsigned char buf[BUFSIZE];
	unsigned int replugTimeout = REPLUGGING_TIMEOUT * REPLUGGING_RESOLUTION;
	/*power off flag in case of timeout*/
	int pwroff = 0;
	int n;

	while (replugTimeout--) {

		lseek(mcufd, 0, SEEK_SET);
		if (read(mcufd, buf, BUFSIZE) < 0) {
			fprintf(stderr, "battery-charging: cannot read '%s', powering off.\n", TOMTOM_MCU_ONLINE);
			pwroff = 1;
			system(CLEAR_DISPLAY);
			break;
		} else if (strtoul(buf, NULL, 10)) { /*if charging cable is connected - mcu online*/
			accept_discharge = 1;
			pwroff = 0;
			break;
		}
		if (!pwroff) {
			fprintf(stderr, "battery-charging: charger disconnected, powering off in %d secs.\n", replugTimeout / REPLUGGING_RESOLUTION);
			pwroff = 1;
			system(CLEAR_DISPLAY);
		}
		usleep(REPLUGGING_SLEEP);
	}

	if (pwroff) {
		fprintf(stderr, "battery-charging: powering off!\n");
		poweroff();
		return;
	}

	if (invalidate_battery_cache() < 0) {
		/* Cannot show battery charging status, so power off */
		poweroff();
		return;
	}

	lseek(batstatusfd, 0, SEEK_SET);
	n = read(batstatusfd, buf, BUFSIZE);
	if (n < 0) {
		fprintf(stderr, "battery-charging: cannot read %s\n", BATTERY_STATUS);
		/* Cannot show battery charging status, so power off */
		poweroff();
		return;
	}

	if (strncmp(buf, "Charging", strlen("Charging")) == 0) {
		/* If battery status is 'Charging' show charging icon */
		system(SHOW_CHARGING);
		accept_discharge = 0;
	}
	else if (strncmp(buf, "Full", strlen("Full")) == 0) {
		/* If battery status is 'Full' show full icon */
		system(SHOW_FULL);
		accept_discharge = 0;
	}
	else if (accept_discharge && strncmp(buf, "Discharging", strlen("Discharging")) == 0) {
		/* For now, show charging icon. We might need to give the FG some time to detect charging */
		system(SHOW_CHARGING);
	} else {
		/* Battery status is Unknown, Discharging, or Not Charging : power off */
		fprintf(stderr, "battery-charging: charger status is %s\n", buf);
		poweroff();
	}

	return;
}

static void * check_battery_and_sleep(void * arg)
{
	int wakeup_reason;

	while (exit_reason == EXIT_REASON_NONE) {
		ttsystem_prepare_sleep();

		/* Update battery charging status */
		check_charging_status();

		if (exit_reason == EXIT_REASON_NONE && !power_button_check_in_progress)
		{
			wakeup_reason = ttsystem_sleep(DEEP_SLEEP_TIME_MSEC);
			if (wakeup_reason == TTSYSTEM_WAKE_POWER_PRN)
				check_charging_status();
			else if (wakeup_reason == TTSYSTEM_SLEEP_MORE || wakeup_reason == TTSYSTEM_WAKE_TIMER)
				accept_discharge = 0;
			else if (wakeup_reason == TTSYSTEM_WAKE_BATT_TEMP) {
				system(SHOW_TOOHOT_ALERT);
				poweroff();
			}
		}
	}

	return NULL;
}

int main(void)
{
	init();

	pthread_join(power_button_thread, NULL);
	pthread_join(battery_status_thread, NULL);

	switch (exit_reason) {
	case EXIT_REASON_POWEROFF:
	case EXIT_REASON_REBOOT:
		deinit();
		break;
	default:
		fprintf(stderr, "battery-charging: WARNING! unknown exit reason %d\n", exit_reason);
		deinit();
		break;
	}
}
