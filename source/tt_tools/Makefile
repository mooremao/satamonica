# This makefile only contains the TT tools

SIRFGPSTEST_DIR := sirfgps_test
GSDPATCHER_DIR := gsdpatcher
FDTQUERY_DIR := fdtquery
FBDISPLAY_DIR := fb-display
BATTERY_LOCK := battery_lock
BATTERY_CHARGING := battery-charging
GAUGE_TOOL := gaugetool
TTW_DIR := wifi_direct_api
TEST_DIR := deviceinfo
LIBTTSYSTEM_DIR := libttsystem
TEMPORARY_TRIGGER_WD_DIR := temporary_trigger_wd
ASSETS_DIR := assets
EXAMPLES_DIR := examples
MINIDSP_DIR := minidsp
PTSCAN_DIR := ptscan

BUILD_TARGET := arm-arago-linux-gnueabi

#Following entries might be set already by calling ipnc_rdk makefiles. So we only set them if not already set.
#We have them here so we can build ext tools here without invoking them from ipnc_rdk.
BUILD_TOOL_DIR ?= $(shell pwd)/../ti_tools/linux_devkit
TARGET_FS_DIR ?= $(shell pwd)/../ipnc_rdk/target/filesys
ASSETS_INSTALL_DIR := $(TARGET_FS_DIR)/usr/share/assets/screens

### gsdpatcher targets:

gsdpatcherclean:
	make -C $(GSDPATCHER_DIR) clean

gsdpatcherbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(GSDPATCHER_DIR)

gsdpatcherinstall:
	mkdir -p $(TARGET_FS_DIR)/opt/ipnc/gps
	cp -a $(GSDPATCHER_DIR)/install/* $(TARGET_FS_DIR)/usr/bin/

### sirfgpstest targets:

sirfgsptestclean:
	make -C $(SIRFGPSTEST_DIR) clean

sirfgsptestbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(SIRFGPSTEST_DIR)

sirfgsptestinstall:
	mkdir -p $(TARGET_FS_DIR)/opt/ipnc/gps
	cp -a $(SIRFGPSTEST_DIR)/install/* $(TARGET_FS_DIR)/opt/ipnc/gps/

### fdtquery targets:

fdtqueryclean:
	make -C $(FDTQUERY_DIR) clean

fdtquerybuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(FDTQUERY_DIR)

fdtqueryinstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(FDTQUERY_DIR)/fdtquery $(TARGET_FS_DIR)/usr/bin/

### deviceinfo targets:

deviceinfoclean:
	make -C $(TEST_DIR) clean

deviceinfobuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(TEST_DIR)

deviceinfoinstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(TEST_DIR)/deviceinfo_example $(TARGET_FS_DIR)/usr/bin/


### fb-display targets:

fbdisplayclean:
	make -C $(FBDISPLAY_DIR) clean

fbdisplaybuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(FBDISPLAY_DIR)

fbdisplayinstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(FBDISPLAY_DIR)/fb-display $(TARGET_FS_DIR)/usr/bin/

### battery_lock targets:

batlockclean:
	make -C $(BATTERY_LOCK) clean

batlockbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(BATTERY_LOCK)

batlockinstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(BATTERY_LOCK)/batlock $(TARGET_FS_DIR)/usr/bin/

### battery-charging targets:
batchargingclean:
	make -C $(BATTERY_CHARGING) clean

batchargingbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(BATTERY_CHARGING)

batcharginginstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(BATTERY_CHARGING)/battery-charging $(TARGET_FS_DIR)/usr/bin/

### gauge_tool targets:
gaugetoolclean:
	make -C $(GAUGE_TOOL) clean

gaugetoolbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(GAUGE_TOOL)

gaugetoolinstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(GAUGE_TOOL)/gaugetool $(TARGET_FS_DIR)/usr/bin/

### wifi_direct_api targets:
ttwclean:
	make -C $(TTW_DIR) clean

ttwbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(TTW_DIR)

ttwinstall:
	mkdir -p $(TARGET_FS_DIR)/sbin/
	cp -a $(TTW_DIR)/ttw_cli $(TARGET_FS_DIR)/usr/sbin/

### libttsystem
.PHONY: libttsystemclean
libttsystemclean:
	make -C $(LIBTTSYSTEM_DIR) clean

.PHONY: libttsystembuild
libttsystembuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(LIBTTSYSTEM_DIR)

### temporary_trigger_wd
.PHONY: temporarytriggerwdclean
temporarytriggerwdclean:
	make -C $(TEMPORARY_TRIGGER_WD_DIR) clean

.PHONY: temporarytriggerwdbuild
temporarytriggerwdbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(TEMPORARY_TRIGGER_WD_DIR)

.PHONY: temporarytriggerwdinstall
temporarytriggerwdinstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(TEMPORARY_TRIGGER_WD_DIR)/temporary_trigger_wd $(TARGET_FS_DIR)/usr/bin/

### assets targets:
assetsclean:
	make -C $(ASSETS_DIR) clean

assetsbuild:
	make -C $(ASSETS_DIR)

# Normal file system only need critical_battery and usb_poweron screen.
assetsinstall:
	mkdir -p $(ASSETS_INSTALL_DIR)
	cp -a $(ASSETS_DIR)/build/screens/note_critical_battery.bmp $(ASSETS_INSTALL_DIR)
	cp -a $(ASSETS_DIR)/build/screens/note_high_temp.bmp $(ASSETS_INSTALL_DIR)
	cp -a $(ASSETS_DIR)/build/screens/usbpoweron* $(ASSETS_INSTALL_DIR)

### examples targets:
examplesclean:
	make -C $(EXAMPLES_DIR) clean

examplesbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(EXAMPLES_DIR)

### minidsp targets:

minidspclean:
	make -C $(MINIDSP_DIR) clean

minidspbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(MINIDSP_DIR)

minidspinstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(MINIDSP_DIR)/minidsp $(TARGET_FS_DIR)/usr/bin/

### ptscan targets:

ptscanclean:
	make -C $(PTSCAN_DIR) clean

ptscanbuild:
	PATH="$(BUILD_TOOL_DIR)/bin/:${PATH}" make -C $(PTSCAN_DIR)

ptscaninstall:
	mkdir -p $(TARGET_FS_DIR)/usr/bin/
	cp -a $(PTSCAN_DIR)/ptscan $(TARGET_FS_DIR)/usr/bin/

.PHONY: sirfgsptestclean sirfgsptestbuild fdtquerybuild fbdisplaybuild gaugetoolbuild ttwbuild deviceinfobuild \
	minidspbuild ptscanbuild

### Overall targets:

clean: sirfgsptestclean gsdpatcherclean fdtqueryclean fbdisplayclean gaugetoolclean ttwclean deviceinfoclean \
	libttsystemclean temporarytriggerwdclean assetsclean examplesclean minidspclean batchargingclean \
	ptscanclean

build: sirfgsptestbuild gsdpatcherbuild fdtquerybuild fbdisplaybuild gaugetoolbuild ttwbuild deviceinfobuild \
	libttsystembuild temporarytriggerwdbuild assetsbuild examplesbuild minidspbuild batchargingbuild \
	ptscanbuild

install: sirfgsptestinstall gsdpatcherinstall fdtqueryinstall fbdisplayinstall gaugetoolinstall ttwinstall deviceinfoinstall \
        temporarytriggerwdinstall assetsinstall batcharginginstall ptscaninstall

.PHONY: clean build install

