/*
 * fdquery - Query properties from device tree binary blob
 * Copyright (c) 2009 TomTom BV <http://www.tomtom.com/>
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>

#include <libfdt.h>


#define TYPE_STR	1
#define TYPE_INT	2
#define TYPE_HEX	3

/* Print integer values as int's or hex? */
#define INT_PRINT_TYPE_INT 1
#define INT_PRINT_TYPE_HEX 2

const char DEFAULT_INFILE[] =  "/lib/udev/devices/fdtexport";
const char *infile = DEFAULT_INFILE;

static void usage(const char *progname)
{
	fprintf(stderr,
		"Usage: %s [ -i | -h | -n ] [ -I <dtbfile> ] "
		"/<path>/<to>/<node>/<property> [<defvalue>]\n"
		"Read a property from a device-tree binary dtb "
		"(default to '%s')\n"
		" -n : query the existence of the node\n"
		" -I <dtbfile> : Input file to read\n"
		" -i : Data to be read as integer\n"
		" -x : Print int values as hex (this implies -i)\n"
		" -h : Data to be read as hexbuf\n"
		"      default format is string\n"
		"Note: node path should start with a '/'\n",
			progname, DEFAULT_INFILE);
}

static void dump_zero_buffer(const void *buf)
{
	const unsigned char *q = (const unsigned char *)buf;
	while (*q) {
		fprintf(stdout, "%02x ", *q);
		q++;
	}
	fprintf(stdout, "\n");
}


void output(const void *prop, int type, int int_print_mode, const char *defvalue)
{
	if (prop) {
		/* Print it to stdout */
		switch(type) {
		case TYPE_STR:
			fprintf(stdout, "%s\n", (char*)prop);
			break;
		case TYPE_INT:
			if (int_print_mode ==  INT_PRINT_TYPE_INT){
				fprintf(stdout, "%lu\n", (unsigned long)fdt32_to_cpu(
					*((unsigned long int*)prop)));
			} else {
				fprintf(stdout, "%08lx\n", (unsigned long)fdt32_to_cpu(
					*((unsigned long int*)prop)));
			}
			break;
		case TYPE_HEX:
			dump_zero_buffer(prop);
			break;
		}
	} else {
		if (defvalue)
			fprintf(stdout, "%s\n", defvalue);
	}
}

const char * split_name(char *nname)
{
	char *pname = NULL;
	char *p     = NULL; 

	p = &(nname[strlen(nname) - 1]);

	while (*p != '/' && p > nname) {
		p--;
	}

	if (p == nname && *p != '/') 
		return NULL;

	*p = '\0';
	pname = p + 1;

	/* Special case for root node path '/'. This is caused by the non-
	 * orthogonality of specifying nodes by libfdt: root is '/' and
	 * non-root is something like '/node' w/o '/' at the end. */
	if (p == nname) {
		pname = "/";
	}

	return pname;
}


int get_node_data(const unsigned char *fdtbuf, char *nname, const void **prop)
{
	int retval = 0;
	const char *pname = split_name(nname);

	/* Get the node */
	int offset = fdt_path_offset(fdtbuf, nname);
		
	if (offset < 0 ) {
		retval = 1;
	} else {
		/* Get the property */
		*prop = fdt_getprop(fdtbuf, offset, pname, NULL);
	}

	return retval;
}


int get_node_existence(const unsigned char *fdtbuf, char *nname)
{
	int found = 1; 

	/* Get node offset */
	int offset = fdt_path_offset(fdtbuf, nname);
	
	if (offset < 0 ) {
		const char *pname = split_name(nname);

		/* Get the node */
		int offset = fdt_path_offset(fdtbuf, nname);
		
		if (offset >= 0) {
			/* Get the property */
			if (fdt_get_property(fdtbuf, offset, pname, NULL))
				found = 0; /* found */
		}
	} else {
		found = 0; /* found */
	}
	
	return found;
}

int main(int argc, char *argv[])
{
	unsigned char *fdtbuf = MAP_FAILED;
	int index, type = TYPE_STR;
	int int_print_mode = INT_PRINT_TYPE_INT;
	off_t size;	
	int retval = 0;
	int ask_node_existence = 0;
	char *nname, *defvalue= NULL;
  struct stat fdtstat;
	
	/* Parse command-line arguments */
	index = 1;
	while (index < argc) {
		if (strcmp(argv[index], "-n") == 0) {
			ask_node_existence = 1; 
		} else if (strcmp(argv[index], "-i") == 0) {
			type = TYPE_INT;
		} else if (strcmp(argv[index], "-h") == 0) {
			type = TYPE_HEX;
		} else if (strcmp(argv[index], "-x") == 0) {
			type = TYPE_INT; /* imply -i */
			int_print_mode = INT_PRINT_TYPE_HEX;
		} else if (strcmp(argv[index], "-I") == 0 && index + 1 < argc) {
			index++;
			infile = argv[index];
		} else {
			break;
		}
		index++;
	}
	if (index >= argc) { /* We start counting at 1 so we are
				really counting arguments */
		usage(argv[0]);
		retval = 1;
		goto out;
	}

	/* Split node/property argument in two */
	nname = argv[index];
	index++;

	if (nname[0] != '/') {
		fprintf(stderr, "Invalid property/node name: %s\n", nname);
		usage(argv[0]);
		retval = 1;
		goto out;
	}

	/* If we still have argument they must be the default value */
	if (index <  argc){
		defvalue = argv[index];
		index++;
	} 
	
	/* Load binary in memory */
	int fd = open(infile, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Can't open input '%s': %s\n", infile,
				strerror(errno));
		if (defvalue)
			fprintf(stdout, "%s\n", defvalue);
		retval = 1;
		goto out;
	}

  if (fstat(fd, &fdtstat) < 0) {
    fprintf(stderr, "Failed to read fdt: Can't fstat '%s': %s\n", infile, strerror(errno));
    retval = 1;
    goto outclose;
  }

  size = lseek(fd, 0, SEEK_END);
	if (size < 0) {
		fprintf(stderr, "Can't lseek '%s'\n", infile);
		if (defvalue)
			fprintf(stdout, "%s\n", defvalue);
		retval = 1;
		goto outclose;
	}

	fdtbuf = (unsigned char *)(mmap(0, (size_t)size, PROT_READ,
			MAP_SHARED, fd, 0));
	if (MAP_FAILED == fdtbuf) {
		fprintf(stderr, "Can't mmap '%s'\n", infile);
		if (defvalue)
			fprintf(stdout, "%s\n", defvalue);
		retval = 1;
		goto outclose;
	}

	if (ask_node_existence) {
		retval = get_node_existence(fdtbuf, nname);

		fprintf(stdout, "%d\n", retval);
	} else {
		const void * prop = NULL;

		retval = get_node_data(fdtbuf, nname, &prop);

		output(prop, type, int_print_mode, defvalue);
	}

outclose:
	if (MAP_FAILED != fdtbuf)
		munmap(fdtbuf, size);

	close(fd);
out:
	return retval;
}

