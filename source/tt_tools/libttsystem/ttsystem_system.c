/*
 * \file Implements reboot, poweroff, factory reset, enter recovery...
 */

#include "ttsystem.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define REBOOT_PARAM_PATH "/run/systemd/reboot-param"


int ttsystem_reboot(ttsystem_reboot_mode_t type)
{
	const char *param = NULL;
	int ret = 0;
	int paramFd = -1;

	switch (type) {
	case TTSYSTEM_REBOOT_NORMAL:
		break;
	case TTSYSTEM_REBOOT_UPDATE:
		param = "update\n";
		break;
	case TTSYSTEM_REBOOT_RECOVERY:
		param = "recovery\n";
		break;
	case TTSYSTEM_REBOOT_USBPOWERON:
		param = "usbpoweron\n";
		break;
	default:
		perror("Unrecognized reboot mode!");
		return -1;
	}

	if (param != NULL) {
		paramFd = open(REBOOT_PARAM_PATH, O_CREAT | O_CLOEXEC | O_WRONLY, S_IRUSR | S_IWUSR);
		if (paramFd < 0) {
			perror("Failed to open/create reboot-param");
			return -1;
		}
		if (write(paramFd, param, strlen(param)) < 0) {
			perror("Failed to write reboot param");
			close(paramFd);
			return -1;
		}
		close(paramFd);
	}

	ret = system("/bin/systemctl --no-block reboot");

	return WEXITSTATUS(ret);
}


int ttsystem_poweroff(void)
{
	int ret = 0;

	/* If USB charger is connected, reboot with reason 'usbpoweron' */
	if (ttsystem_get_charger_connected())
		ret = ttsystem_reboot(TTSYSTEM_REBOOT_USBPOWERON);
	else
		ret = system("/bin/systemctl --no-block poweroff");

	return WEXITSTATUS(ret);
}


int ttsystem_factory_reset(void)
{
	int ret = 0;
	ret = system("factory_reset");
	return WEXITSTATUS(ret);
}
