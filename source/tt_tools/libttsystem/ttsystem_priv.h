/**
 * \file ttsystem_priv.h
 * \brief Long Beach API for system specific functions.
 */

#ifndef TTSYSTEM_PRIV_H_
#define TTSYSTEM_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#define GSD_SLEEP_LOCK_PATH "/dev/shm/gsd_sleep_lock"

#ifdef LONGBEACH_POWERMODE_OBSOLETE

#include "ttsystem.h"

// ttsystem platform power modes
typedef enum {
	TTSYSTEM_PRIV_POWERMODE_VIEWFINDER = TTSYSTEM_POWERMODE_MAX + 1,
	TTSYSTEM_PRIV_POWERMODE_LOWVIDEORATE,
	TTSYSTEM_PRIV_POWERMODE_HIGHVIDEORATE,
#ifdef LONGBEACH_FREQUENCY_DEBUG
	TTSYSTEM_PRIV_POWERMODE_CURRENT,
#endif
	TTSYSTEM_PRIV_POWERMODE_MAX,
} ttsystem_priv_powermode_t;

/**
 * @brief Activate video power mode
 *
 * This function can be called by the ttv video library to set platform power mode
 * during video recording.
 *
 * Power mode vs Video mode guide:
 *
 * \par TTV_POWERMODE_VIEWFINDER:
 * Viewfinder
 *
 * \par TTV_POWERMODE_LOWVIDEORATE:
 *  Recording at 1080p30, 1080p25, 720p60, 720p50, WVGA180, WVGA150.
 *  Transcoding where the maximum resolution/framerate (input and output) is one of the above
 *
 * \par TTV_POWERMODE_HIGHVIDEORATE:
 * Still pictures
 * \n Recording at 1080p60, 1080p50, 4K15, 4K12.5, 2.7K30, 2.7K25, 720p120, 720p100
 * \n Transcoding where the maximum resolution/framerate (input and output) is one of the above
 * \n Application wants to do something CPU-intensive, like going through the sensor data stream of an MP4 file
 *    and doing some statistics on it
 *
 * @param[in] powermode powermode of type ttsystem_priv_powermode_t
 *
 * @return true on success, false on failure
 */
bool ttsystem_activate_video_power(ttsystem_priv_powermode_t powermode);

/**
 * @brief Deactivate video power mode
 *
 * This function can be called by the ttv video library when video recording is stopped
 * to deactivate platform power mode set by ttsystem_activate_video_power()
 *
 * @return true on success, false on failure
 */
bool ttsystem_deactivate_video_power();

#endif /* LONGBEACH_POWERMODE_OBSOLETE */

#ifdef __cplusplus
}
#endif

#endif /* TTSYSTEM_PRIV_H_ */
