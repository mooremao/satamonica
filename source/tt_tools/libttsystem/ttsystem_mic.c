/*
 * \file Implements ttsystem_extmic enable/disable functions
 */

#include <stdio.h>
#include <stdbool.h>

void ttsystem_extmic_enable(bool enable)
{
	if (enable)
		system("/opt/ipnc/set_audio_path.sh ext");
	else
		system("/opt/ipnc/set_audio_path.sh int");
}
