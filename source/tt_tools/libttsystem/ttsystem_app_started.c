/*
 * \file Implements ttsystem_app_started function.
 */

#include "ttsystem.h"
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>

void ttsystem_app_started() {
#ifdef LONGBEACH_POWERMODE_OBSOLETE
	ttsystem_set_powermode(TTSYSTEM_POWERMODE_ACTIVE);
#else
	ttsystem_set_powermode(TTSYSTEM_POWERMODE_DEFAULT);
#endif
	sd_notify(0, "READY=1");
}

