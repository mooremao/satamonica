/*
 * \file Implements ttsystem_power functions
 *
 * Note 1: In viewfinder power mode the A8 CPU speed can be reduced below the speed needed in active mode.
 * 	   So viewfinder power mode will only be activate when application requires passive mode.
 */

#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include "ttsystem.h"
#include "ttsystem_priv.h"

#ifdef LONGBEACH_POWERMODE_OBSOLETE

typedef struct {
	bool video_power_active;
	ttsystem_priv_powermode_t last_requested_power_mode;
	ttsystem_priv_powermode_t last_requested_video_power_mode;
	ttsystem_priv_powermode_t last_set_power_mode;
	pthread_mutex_t mutex;
} ttsystem_power_internal_state_t;

static ttsystem_power_internal_state_t ttsystem_power_state = {
	.video_power_active = false,
	.last_requested_power_mode = TTSYSTEM_PRIV_POWERMODE_MAX,
	.last_requested_video_power_mode = TTSYSTEM_PRIV_POWERMODE_MAX,
	.last_set_power_mode = TTSYSTEM_PRIV_POWERMODE_MAX,
	.mutex = PTHREAD_MUTEX_INITIALIZER,
};

static bool ttsystem_internal_set_power_mode(ttsystem_priv_powermode_t powermode)
{
	int fd;
	int retval;

	if (powermode == ttsystem_power_state.last_set_power_mode)
		return true;

	fd = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", O_RDWR | O_CLOEXEC);
	if (fd < 0) {
		printf("ttsystem: cannot set power mode - file open failed : %s\n", strerror(errno));
		return false;
	}

	switch (powermode) {
		case TTSYSTEM_PRIV_POWERMODE_VIEWFINDER:
			retval = write(fd, "lb_high1", sizeof("lb_high1"));
			break;
		case TTSYSTEM_PRIV_POWERMODE_LOWVIDEORATE:
			retval = write(fd, "lb_high2", sizeof("lb_high2"));
			break;
		case TTSYSTEM_PRIV_POWERMODE_HIGHVIDEORATE:
			retval = write(fd, "lb_high3", sizeof("lb_high3"));
			break;
		case TTSYSTEM_POWERMODE_ACTIVE:
			retval = write(fd, "lb_active", sizeof("lb_active"));
			break;
		case TTSYSTEM_POWERMODE_PASSIVE:
			retval = write(fd, "lb_passive", sizeof("lb_passive"));
			break;
		default:
			printf("ttsystem: cannot set power mode - invalid power mode\n");
			close(fd);
			return false;
	}

	close(fd);
	if (retval < 0) {
		printf("ttsystem: cannot set power mode - file write failed : %s\n", strerror(errno));
		return false;
	} else
		ttsystem_power_state.last_set_power_mode = powermode;

	return true;
}

bool ttsystem_activate_video_power(ttsystem_priv_powermode_t powermode)
{
	bool ret = true;

	if (powermode < TTSYSTEM_PRIV_POWERMODE_VIEWFINDER)
		return false;

	pthread_mutex_lock(&ttsystem_power_state.mutex);

	ttsystem_power_state.video_power_active = true;
	ttsystem_power_state.last_requested_video_power_mode = powermode;

	/* Do not activate viewfinder power mode when application requires active mode. See note 1 for details. */
	if (powermode == TTSYSTEM_PRIV_POWERMODE_VIEWFINDER && ttsystem_power_state.last_requested_power_mode == TTSYSTEM_POWERMODE_ACTIVE)
		ret = ttsystem_internal_set_power_mode(TTSYSTEM_POWERMODE_ACTIVE);
	else
		ret = ttsystem_internal_set_power_mode(powermode);

	pthread_mutex_unlock(&ttsystem_power_state.mutex);

	return ret;
}

bool ttsystem_deactivate_video_power()
{
	bool ret;

	pthread_mutex_lock(&ttsystem_power_state.mutex);

	ret = ttsystem_internal_set_power_mode(ttsystem_power_state.last_requested_power_mode);
	ttsystem_power_state.video_power_active = false;

	pthread_mutex_unlock(&ttsystem_power_state.mutex);

	return ret;
}

bool ttsystem_set_powermode(ttsystem_powermode_t powermode)
{
	bool ret = true;

	if (powermode >= TTSYSTEM_POWERMODE_MAX)
		return false;

	pthread_mutex_lock(&ttsystem_power_state.mutex);

	ttsystem_power_state.last_requested_power_mode = powermode;

	/**
	 * If viewfinder is active and application request passive mode change power mode to viewfinder
	 * See note 1 for details.
	 **/
	if(ttsystem_power_state.video_power_active &&
			powermode == TTSYSTEM_POWERMODE_PASSIVE && ttsystem_power_state.last_requested_video_power_mode == TTSYSTEM_PRIV_POWERMODE_VIEWFINDER)
		ret = ttsystem_internal_set_power_mode(TTSYSTEM_PRIV_POWERMODE_VIEWFINDER);

	/**
	 * Otherwise only set power mode when not recording or when application requests active mode
	 * while viewfinder power mode is currently active. See note 1 for details.
	 */
	else if (!ttsystem_power_state.video_power_active ||
			(powermode == TTSYSTEM_POWERMODE_ACTIVE && ttsystem_power_state.last_set_power_mode == TTSYSTEM_PRIV_POWERMODE_VIEWFINDER))
		ret = ttsystem_internal_set_power_mode(powermode);

	pthread_mutex_unlock(&ttsystem_power_state.mutex);

	return ret;
}

#else

bool ttsystem_set_powermode(ttsystem_powermode_t powermode)
{
	int fd;

	if (powermode >= TTSYSTEM_POWERMODE_MAX)
		return false;

	fd = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", O_RDWR | O_CLOEXEC);
	if (fd < 0) {
		printf("ttsystem: cannot set power mode - file open failed : %s\n", strerror(errno));
		return false;
	}

	if (write(fd, "lb_governor", sizeof("lb_governor")) < sizeof("lb_governor")) {
		printf("ttsystem: cannot set power mode - write to file failed : %s\n", strerror(errno));
		close(fd);
		return false;
	}

	close(fd);
	return true;
}

#endif

bool ttsystem_get_charger_connected(void)
{
	unsigned char buf[10];
	unsigned int mcu_online = 0;
	int fd;

	fd = open("/sys/class/power_supply/tomtom_mcu/online", O_RDONLY | O_CLOEXEC);
	if (fd < 0) {
		printf("ttsystem_get_charger_connected: file open failed %s\n", strerror(errno));
		return false;
	}

	if (read(fd, buf, 10) < 0) {
		printf("ttsystem_get_charger_connected: file read failed %s\n", strerror(errno));
		close(fd);
		return false;
	}

	errno = 0;
	mcu_online = strtoul(buf, NULL, 10);
	if (errno) {
		printf("ttsystem_get_charger_connected: error in reading mcu_online property %s\n", strerror(errno));
		close(fd);
		return false;
	}

	close(fd);
	return mcu_online ? true : false;
}
