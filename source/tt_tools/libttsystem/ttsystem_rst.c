/*
 * \file Implements ttsystem_rst functions
 */

#include <stdio.h>
#include <fcntl.h>

#define RESET_COUNT_FILE "/mnt/userdata/ft-rst-count"

/**
 * @brief Get the number of factory resets
 *
 * The number represents how many factory resets have been done on the device
 *
 * @param none
 *
 * @return
 *   <> 0 the number of factory resets occurred\n
 *   = 0 if a device has never been factory-reset before or if the number cannot be determined
 */
int ttsystem_factory_reset_counter(void)
{
	FILE* fp;
	int cnt, res;

	fp = fopen (RESET_COUNT_FILE, "r");

	/** if the file doesn't exists yet or cannot be accessed, consider the counter to be 0 */
	if (fp == NULL)
		return 0;

	/** get the counter value */
	res = fscanf(fp, "%10d", &cnt);

	/** in case of failed parsing, consider the counter to be 0 */
	if ((res == EOF) || (res == 0)) {
		fprintf(stderr, "warning: cannot determine the number of factory resets, set it to 0\n");
		cnt = 0;
	}

	fclose(fp);

	return cnt;
}
