/**
 * \file ttsystem.h
 * \brief Long Beach API for system specific functions.
 */

#ifndef TTSYSTEM_H_
#define TTSYSTEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

/** ioctl to set the clockid for timestamp of the events from input devices (CLOCK_MONOTONIC or CLOCK_REALTIME) */
#ifndef EVIOCSCLOCKID
#define EVIOCSCLOCKID _IOW('E', 0xa0, int)
#endif

// Wakeup from deepsleep takes ~370 ms. Using a maximum value of 500ms.
#define MAX_SYSTEM_WAKEUP_TIME_IN_MS 500

typedef enum {
	TTSYSTEM_WAKE_OTHER = 0,               // < Wakeup for unknown reason.
	TTSYSTEM_WAKE_BUTTON,                  // < Wakeup from button.
	TTSYSTEM_WAKE_TIMER,	                  // < Wakeup from timer; successfully slept for requested duration.
	TTSYSTEM_WAKE_BLE,                     // < Wakeup from BLE (e.g. remote control button press).
	TTSYSTEM_SLEEP_MORE,                   // < Woke up before requested timeout; please do a software sleep for the remainder.
	TTSYSTEM_WAKE_EXT_MIC,                 // < External mic detection triggered wakeup (please ignore and go back to sleep).
	TTSYSTEM_WAKE_POWER_PRN,               // < Power present change triggered wakeup (insertion/removal of charging cable).
	TTSYSTEM_WAKE_BATT_TEMP,               // < Battery temperature triggered wakeup (could be a reason for not charging).
	TTSYSTEM_WAKE_SOC_BATT_LOW,            // < Battery capacity becoming low.
	TTSYSTEM_WAKE_SOC_TURN_OFF,            // < FG indicates a critical low battery. But FG might be wrong, so check capacity level separately.
	TTSYSTEM_SLEEP_ERROR_INVALID_TIMEOUT,  // < Invalid timeout argument given. Valid values are -1 to approx. 65,000,000, but please don't use -1.
	TTSYSTEM_SLEEP_ERROR_INTERNAL,         // < Failed to enter deep sleep for some internal reason. Wait a second and try again.
	TTSYSTEM_SLEEP_ERROR_BLE_STATE, 	  // < BLE stack not currently able to go to sleep. Wait a sec and retry.
	TTSYSTEM_SLEEP_ERROR_GSD_STATE,        // < Failed to obtain gsd sleep lock (gsd patching in progress?)
	TTSYSTEM_SLEEP_ERROR_TIMEOUT_TOO_SHORT, // < Timeout is too short. Lowest useful timeout value is about 1500ms; if you need shorter, do a software sleep.
	TTSYSTEM_SLEEP_ERROR_WOKEN_UP         		// < Failed to enter deep sleep because someone woke the system up in the meantime.

} ttsystem_wakeup_t;

/**
 *  @func	ttsystem_prepare_sleep
 *
 *  @desc	Prepare for sleep
 *		\n If a wake up event occurs between call to this function and ttsystem_sleep
 *		\n the system will not enter deep sleep
 *
 *  @return
 *		0 on success, -1 on error
 */
bool ttsystem_prepare_sleep(void);

/**
 *  @func	ttsystem_sleep
 *
 *  @desc	Software or hardware for sleep 'timeout' milliseconds
 *
 *  @param[in]	timeout timeout in milliseconds
 * 		\n timeout = -1: power off. This feature is supported but deprecated.
 * 		\n timeout =  0: sleep indefinitely or until button or bluetooth wakeup
 * 		\n timeout >  0: sleep for timeout seconds or until button or bluetooth wakeup
 * 		\n timeout <  0 but not -1: return
 *
 *  @return
 * 		\n TTSYSTEM_WAKE_TIMER:  Device was woken up by timer after 'timeout' milliseconds
 * 		\n TTSYSTEM_WAKE_BUTTON: Device was woken up by button press
 * 		\n TTSYSTEM_WAKE_BLE:    Device was woken up by bluetooth
 * 		\n TTSYSTEM_WAKE_OTHER:  Device was woken up by another source
 *		\n TTSYSTEM_SLEEP_MORE:  Function returns before 'timeout' milliseconds have elapsed
 *                    		    and device was not woken up by any external source.
 */
ttsystem_wakeup_t ttsystem_sleep(int timeout);

/**
 * \brief Application started. Continue start with other services.
 *
 * Application should call this function once the application is ready to record.
 * This to improve boot performance.
 * This call will be used by the platform to continue starting systemd services that
 * can be started later, such as services for GPSD/BT/WiFi/logging.
 *
 * It is important to call this function otherwise not all system service will be started at all.
 *
 * Please ensure that his function is called before first call to syslog()
 * Better to use no syslog() at all and just log to stdout/stderr and start application by systemd.
 * But if the application is still using syslog(), just call this function directly at start of main().
 * Please note, calling it directly from main will not improve boottime. But will at least start
 * all services as it did before.
 */
void ttsystem_app_started(void);

/**
 * \brief Triggers the SW watchdog.
 *
 * This function should be called by the application to indicate that the
 * application is not frozen and working as it should. Each time this
 * function is called the SW watchdog timer will be cleared. If the SW watchdog
 * is not cleared for 10 seconds, the watchdog will assume the application is
 * not working as is should.
 * To stay away from the 10 seconds deadline, a good practice is too call this
 * function each 2 seconds.
 *
 * Do not call this function in a way such as:
 *
 * \code{.c}
 * while(true) { sleep(2); ttsystem_trigger_watchdog(); }
 * \endcode
 *
 * and handle all application functionality in other threads. This will prevent
 * any SW watchdog timeouts to occur, but disables the purpose of the SW
 * watchdog.
 *
 * In case of a single threaded application call this function
 * as part of the main loop. Please consider not to call this function too many
 * times to reduces unneeded overhead. So when this main loop is at least each
 * second or faster, call this function only when it is not called for more
 * than 2 seconds.
 * In case of a multithreaded application, introduce a watchdog thread that stops
 * calling this function when one of the others threads of the application stops
 * reporting in. *
 */
void ttsystem_trigger_watchdog(void);

/**
 *  @func	ttv_get_device_serial
 *
 *  @desc	Function to get the unique serial id of a camera device
 *
 *  @param	serial character array in which the serial id of the device is filled
 *
 *  @param	strsize size of the character array being passed
 *
 *  @return	0 on success
 *		\n error number on failure
 *
 */
int ttv_get_device_serial(char *serial, int strsize);

/**
 * \brief Format SD Card
 *
 * This function can be called by the application to format the SD card.
 * \n If the SD card capacity is less than or equal to 32 GB, format as FAT32
 * \n If the SD card capacity is greater than 32 GB, format as exFAT
 * \n All file descriptors pointing to /mnt/mmc should be closed before calling this function
 *
 * \return     0 on success
 *             \n -errno on failure
 */
int ttsystem_format_sdcard(void);

/**
 * \brief Start Bluetooth service
 *
 * This function can be called by the application to connect to bluetooth.
 * ttsystem_start_bt starts the bluetopia service and initializes BT API.
 *
 * \return     0 on success
 *             \n -errno on failure
 */
int ttsystem_start_bt(void);

/**
 * \brief Stop Bluetooth service
 *
 * This function can be called by the application to disconnect bluetooth by stopping the bluetopia service.
 */
void ttsystem_stop_bt(void);

/**
 * @brief Get the number of factory resets
 *
 * The number represents how many factory resets have been done on the device
 *
 * @param none
 *
 * @return
 *   <> 0 the number of factory resets occurred\n
 *   = 0 if a device has never been factory-reset before or if the number cannot be determined
 */
int ttsystem_factory_reset_counter(void);

/**
 * @brief Enable or disable external mic
 *
 * This function can be called by the application to enable or disable the external mic
 *
 * @param enable true to enable external mic, false to disable external mic
 */
void ttsystem_extmic_enable(bool enable);

/**
 * @brief Inform platform that beeper has been enabled or disabled by user
 *
 * This function should be called by the application whenever user changes the beep camera setting,
 * so platform can try to respect user preference when showing alerts.
 * Note that this API call does not affect the /sys/devices/platform/pwm-beeper/tune sysfs entry.
 *
 * @param enabled true if beeper has been enabled, false if beeper has been disabled
 */
void ttsystem_beep_enabled(bool enabled);

// ttsystem platform power modes
typedef enum {
#ifdef LONGBEACH_POWERMODE_OBSOLETE
	TTSYSTEM_POWERMODE_PASSIVE,
	TTSYSTEM_POWERMODE_ACTIVE,
#endif
	TTSYSTEM_POWERMODE_DEFAULT,
	TTSYSTEM_POWERMODE_MAX,
} ttsystem_powermode_t;

/**
 * @brief Check if USB charger is connected
 *
 * This function can be called by the application to check if USB charger is connected
 *
 * @return
 *   true if USB charger is connected\n
 *   false if USB charger is not connected
 */
bool ttsystem_get_charger_connected(void);

/**
 * @brief Set platform power mode
 *
 * This function can be called by the application to set platform power mode
 *
 * @param[in] powermode Power mode to be set\n
 *                      Permitted power modes are TTSYSTEM_POWERMODE_PASSIVE, TTSYSTEM_POWERMODE_ACTIVE
 *
 * @return
 *   true on success\n
 *   false on failure
 */
bool ttsystem_set_powermode(ttsystem_powermode_t powermode);

/**
 * \brief Check SD Card for errors and optionally repairs them
 *
 * This function can be called by the application to check the SD card for errors and try to repair them.
 * \n The SD card gets automatically un-mounted before starting the check and re-mounted afterwards
 *
 * @param[in] repair Enable the automatic error repairing\n
 *                                    true = repair enabled\n
 *                                    false = repair disabled
 *
 * \return     0 on success (no filesystem errors found or repair completed)
 *             \n -errno on system failure (device not present or mount/unmount failure)
 *             \n 1 no valid FAT or exFAT filesystem found
 *             \n 2 error invoking fsck tool
 *             \n 3 device has errors
 *             \n 4 the fsck has not been completed successfully. Device might still contain errors
 *
 */
int ttsystem_check_sdcard(bool repair);

/**
 * \brief Check presence of SD card
 *
 * This function can be called by the application to check if SD card is present.
 *
 * \return     true if SD card is present\n
 *             false otherwise
 *
 */
bool ttsystem_is_sdcard_present(void);

/**
 * \brief Get SD card status
 *
 * This function can be called by the application to detect the status of the SDCard (present, read/write...)
 *
 * \return     0 SD card available\n
 *             1 SD card not present\n
 *             2 SD card failed to mount\n
 *             3 SD card readonly
 *
 */
int ttsystem_get_sdcard_status(void);


/**
 * \brief Check if the BT stack allows the device to sleep
 *
 * This function can be called by the application to check if the status
 * of the Bluetooth stack allows the device to enter deep sleep mode
 *
 * \return     0 sleep is not allowed
 *             1 sleep is allowed
 *             \n -errno on failure
 */
int ttsystem_bt_sleep_allowed(void);

/**
 * \brief GSD SGEE Navigation system
 */
typedef enum {
	TTSYSTEM_SGEE_GPS,	/**< GPS navigation system */
	TTSYSTEM_SGEE_GLO,	/**< GLONASS navigation system */
} ttsystem_sgee_navsys_t;

/**
 * \brief Get information about the GSD quickfix data
 *
 * This function can be called by the application to check the status of the quickfix data in the
 * GSD module and, if present, retrieve its age and prediction interval
 * The output parameters (status, age, pred_int) are valid only when the function returns 0
 *
 * NOTE: in order to obtain a valid age, the RTC of the GSD module should contain a valid time provided by a fix
 * or by calling ttsystem_set_rtc(). The status code for lack of RTC info (status=4) should be checked immediately after downloading the
 * quickfix data, since at next reboot this condition will be indicated with status=0 as if the download never occurred.
 * In both cases, the presece of a valid RTC data will make the age information immediately visible (status=3) without
 * the need of re-download the quickfix data
 *
 * @param[in] navsys : navigation system to use #ttsystem_sgee_navsys_t
 * @param[out] *status  status of the quickfix data:
 *                                             0 = quickfix data not available
 *                                             1 = quickfix download in progress
 *                                             2 = quickfix download failed
 *                                             3 = quickfix download complete
 *                                             4 = quickfix download complete but no valid RTC data in GSD
 * @param[out] *age : age of the quickfix data in seconds (only if status = 3)
 * @param[out] *pred_int : prediction interval of the quickfix data in seconds (only if *status = 3)
 *
 * \return     0 on success
 *             \n <> 0 on failure
 *
 */
int ttsystem_get_sgee_info(ttsystem_sgee_navsys_t navsys, int32_t *status, int32_t *age, uint32_t *pred_int);


/**
 * \brief Reboot types
 */
typedef enum {
	TTSYSTEM_REBOOT_NORMAL,
	TTSYSTEM_REBOOT_UPDATE,
	TTSYSTEM_REBOOT_RECOVERY,
	TTSYSTEM_REBOOT_USBPOWERON,
} ttsystem_reboot_mode_t;

/**
 * \brief Reboot system into desired mode
 *
 * This function can be called by the application to reboot system
 *
 * @param[in] type Reboot type\n
 *                        TTSYSTEM_REBOOT_NORMAL = normal reboot\n
 *                        TTSYSTEM_REBOOT_UPDATE = enter into recovery mode for instaling an update\n
 *                        TTSYSTEM_REBOOT_RECOVERY = enter into recovery mode
 *                        TTSYSTEM_REBOOT_USBPOWERON = show charging icon
 *
 * \return    0 on success
 *
 */
int ttsystem_reboot(ttsystem_reboot_mode_t type);

/**
 * \brief Initiate power off
 *
 * This function can be called by the application to shutdown camera
 *
 * \return    0 on success
 */
int ttsystem_poweroff(void);

/**
 * \brief Initiate factory reset
 *
 * This function can be called by the application to reset the device to factory settings.
 * It will erase the user partition, reset the gps chip and reboot the device. The camera app
 * should close all file descriptors pointing to files stored at the user data partition before executing this function.
 *
 * \return    0 on success
 */
int ttsystem_factory_reset(void);


/** CLOCK_BOOTTIME was added in Linux 2.6.39 and backported to Longbeach project.
 *  If the glibc definition is missing we define it here to be able to access the kernel function.
 *  Use clock_gettime(CLOCK_BOOTTIME,..) to get monotonic time including time spent in suspend.
 */
#ifndef CLOCK_BOOTTIME
#define CLOCK_BOOTTIME 7
#endif

/**
 * \brief Creates a log dump and provides generated file path
 *
 * The log dump is a file called ttb_<NNN>.log in <dest_dir> containing an tar.gz archive with
 * all log files. When no files in <dest_dir> match ttb_<nnn>.log the <NNN> part will be 001.
 * When one or more files match ttb_<nnn>.log pattern. The <NNN> part will be the highest <nnn>
 * value increased by 1. For example, when ttb_001.log and ttb_003.log exists
 * the new generated file will be called ttb_004.log.
 *
 * The generated file name will copied to <r_file_path> when <r_file_path> is not NULL.
 *
 * Caller is responsible to remove the file afterwards, if desired.
 *
 * \param[in] dest_dir Destination location for the log file. The location should be writable.
 * \param[out] r_file_path When not NULL, should point to a buffer of at least PATH_MAX bytes long.\n
 *                         PATH_MAX is defined in limits.h. When not NULL, the full filename\n
 *                         path is copied to this location when a file has been generated\n
 *                         successfully.
 * \return Returns 0 when the log dump is successfully generated. Or a negative error code
 * \retval 0 OK. Dump file generated. Full file name path is copied to <r_file_path> when not NULL
 * \retval -1 Internal error
 * \retval -2 Creating file failed. For example, destination is not writable or out of space.
 */
int ttsystem_dump_log(const char *dest_dir, char *r_file_path);

/**
 * \brief Set the GSD RTC time
 *
 * This function can be called by the application to set the RTC time/date of the GSD module.
 * The on-chip storage of the time might take few seconds after the function returns, therefore a proper
 * delay must be inserted if a reboot/shutdown is performed afterwards
 *
 * NOTE: the operation causes any existing GPS fix and RTC time to be lost, thus it's recommended to use it only when
 *       a GPS fix has never been obtained before and the system time is still invalid (1st Jan 2015)
 *
 * SGEE NOTE: Setting the RTC affects the age calculation of SGEE quickfix data, therefore it's recommended to
 *            re-read the age every time a new value is stored into the RTC.
 *            Please also avoid calling this API when a quickfix file is being downloaded into the GSD module.
 *
 * @param[in] time : string containing the UTC time in the format "YYYY-MM-DD hh:mm:ss" (example: "2015-05-07 12:30:00")
 *
 * \return     0 on success
 *             \n -1 or -errno on failure
 *
 */
int ttsystem_set_rtc(const char *time);

/**
 * \brief Puts the GSD to sleep, or wakes it up.
 *
 * @param[in] sleep : If set to "true", the GSD will be put to sleep/hibernate if it is awake. If set to "false", it will be woken up.
 *
 * \return     0 on success
 *             \n -1 or -errno on failure
 */
int ttsystem_gps_put_to_sleep(bool sleep);

/**
 * \brief Gets GSD awake state.
 *
 * @param[out] is_awake : If 0, the GSD is asleep, if 1, it is awake.
 *
 * \return     0 on success
 *             \n -1 or -errno on failure
 */
int ttsystem_gps_is_awake(int* is_awake);

#ifdef __cplusplus
}
#endif

#endif /* TTSYSTEM_H_ */
