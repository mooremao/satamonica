/*
 * \file Implements ttsystem_trigger_watchdog function.
 */

#include <systemd/sd-daemon.h>

#include "ttsystem.h"

void ttsystem_trigger_watchdog() {
	sd_notify(0, "WATCHDOG=1");
}

