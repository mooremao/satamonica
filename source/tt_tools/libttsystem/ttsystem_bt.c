/*
 * \file Implements ttsystem_bt start/stop functions
 */

#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/stat.h>

#define BT_SLEEP_NOT_ALLOWED_FILENAME "/tmp/BT_Sleep_Not_Allowed"

int ttsystem_start_bt(void)
{
	int s, i, len;
	struct sockaddr_un sock;

	system("systemctl start bluetopia");

	/* Try to connect the backend IPC socket to check if the startup has completed */
	s = socket(AF_LOCAL, SOCK_SEQPACKET, 0);
	if (s == -1) {
		fprintf(stderr, "Error creating BT IPC socket: %s\n", strerror(errno));
		return -errno;
	}
	sock.sun_family = AF_LOCAL;
	strcpy(sock.sun_path, "/tmp/SS1BTPMS");
	len = strlen(sock.sun_path) + sizeof(sock.sun_family);
	i = 0;
	while (1) {
		if (connect(s, (struct sockaddr *)&sock, len) == -1) {
			fprintf(stderr, "Error connecting BT IPC socket: %s", strerror(errno));
			if (i < 20) {
				/* If the connection fails, the backend might not be ready yet, so try again */
				fprintf(stderr, " - retrying....\n");
				usleep (200000);
			}
			else {
				/* no more retries */
				fprintf(stderr, " - giving up!\n");
				break;
			}
		}
		else {
			/* Connection successful */
			close(s);
			return 0;
		}
		i++;
	}
	close(s);
	return -errno;
}

void ttsystem_stop_bt(void)
{
	system("systemctl stop bluetopia");
}

int ttsystem_bt_sleep_allowed(void)
{
	int ret;
	struct stat st;

	/* check the presence of the file created by the BT stack */
	ret = stat(BT_SLEEP_NOT_ALLOWED_FILENAME, &st);
	if (ret != 0) {
		if (errno == ENOENT) {
			ret = 1;
		}
		else {
			fprintf(stderr, "Error checking file existance: %s - %s\n", BT_SLEEP_NOT_ALLOWED_FILENAME, strerror(errno));
			ret = -errno;
		}
	}

	return ret;
}
