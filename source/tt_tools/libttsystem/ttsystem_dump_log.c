/*
 * \file Implements ttsystem_dump_log functions
 */
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "ttsystem.h"

int ttsystem_dump_log(const char *dest_dir, char *r_file_path)
{
	int status;
	FILE *process;
	char *s;
	char file_path[PATH_MAX];
	char cmd[PATH_MAX];

	snprintf(cmd, PATH_MAX, "logdump \"%s\"", dest_dir);

	process = popen(cmd, "re");
	if (process == NULL) {
		fprintf(stderr, "Failed to execute cmd '%s': %s\n", cmd, strerror(errno));
		return -1;
	}

	s = fgets(file_path, PATH_MAX, process);

	status = pclose(process);
	if (status < 0 || !WIFEXITED(status)) {
		fprintf(stderr, "Failed to wait for execute '%s' to terminate: %s\n", cmd, strerror(errno));
		return -1;
	}
	status = WEXITSTATUS(status);
	if (status != 0) {
		fprintf(stderr, "Executing '%s' failed. Exit code: %d\n", cmd, status);
		return -2;
	}

	if (s == NULL) {
		fprintf(stderr, "Failed to read filename\n");
		return -1;
	}

	if (r_file_path != NULL) {
		strncpy(r_file_path, file_path, PATH_MAX);
		r_file_path[PATH_MAX - 1] = '\0';
	}

	return 0;
}
