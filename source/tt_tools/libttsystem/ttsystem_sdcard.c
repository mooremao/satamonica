/*
 * \file Implements ttsystem_sdcard functions
 */

#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <blkid/blkid.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include "ttsystem.h"

#define GB (1024 * 1024 * 1024)

#define SDCARD "/dev/mmcblk0"
#define SDCARD_PARTITION1 "/dev/mmcblk0p1"
#define SDCARD_MOUNT_POINT "/mnt/mmc"

#define MKDOSFS "mkdosfs "
#define MKEXFATFS "mkexfat --sda-whole --sda-relax "

#define FATLABEL "fatlabel"
#define EXFATLABEL "exfatlabel"
#define FSCK_FAT "fsck.fat"
#define FSCK_EXFAT "exfatck"

#define DEFAULT_LABEL "TT_CAM"

#define MAX_LABEL_LEN 11

static int _ttsystem_unmount_sdcard(void)
{
	int retval = 0;

	retval = system("umount_sdcard");
	if (retval < 0)
		fprintf(stderr, "Can't unmount '%s': %s\n",
				SDCARD_MOUNT_POINT, strerror(errno));

	return retval;
}

static int _ttsystem_mount_sdcard(void)
{
	int retval = 0;

	retval = system("mount_sdcard");
	if(retval < 0)
		fprintf(stderr, "Can't mount sdcard: %s\n", strerror(errno));

	return retval;
}

static int _ttsystem_get_sdcard_size(uint64_t *p_size)
{
	int retval = 0;
	int fd;

	fd = open(SDCARD, O_RDONLY | O_CLOEXEC);
	if (fd < 0) {
		fprintf(stderr, "Can't open '%s': %s\n",
				SDCARD, strerror(errno));
		retval = -errno;
		return retval;
	}

	if (ioctl(fd, BLKGETSIZE64, p_size) < 0) {
		fprintf(stderr, "Can't get SD Card size: %s\n",
				strerror(errno));
		retval = -errno;
	}
	else
		printf("Found SD card of capacity %.2f GB\n", *p_size / (double)GB);

	close(fd);
	return retval;
}

static int _ttsystem_probe_fat(const char *dev_name)
{
	char cmd[50];
	int retval;

	/* Probe for FAT filesystem */
	snprintf(cmd, 50, "%s %s", FATLABEL, dev_name);
	cmd[49] = '\0';
	retval = system(cmd);

	return WEXITSTATUS(retval);
}

static int _ttsystem_probe_exfat(const char *dev_name)
{
	char cmd[50];
	int retval;

	/* Probe for exFAT filesystem */
	snprintf(cmd, 50, "%s -p %s", FSCK_EXFAT, dev_name);
	cmd[49] = '\0';
	retval = system(cmd);

	return WEXITSTATUS(retval);
}

static int _ttsystem_check_fat(const char *dev_name, bool repair)
{
	char cmd[50];
	int retval;

	/* Perform the check with optional repair */
	if (repair)
		snprintf(cmd, 50, "%s -V -a %s", FSCK_FAT, dev_name);
	else
		snprintf(cmd, 50, "%s -n %s", FSCK_FAT, dev_name);

	cmd[49] = '\0';
	retval = system(cmd);
	if (retval < 0) {
		fprintf(stderr, "Cannot execute FAT fsck on %s\n", dev_name);
		retval = 2;
	}
	else if (WEXITSTATUS(retval) != 0) {
		/* status = 1 means errors found, but is also returned when they've been successfully repaired */
		if (WEXITSTATUS(retval) == 1)
			retval = (repair) ? 0 : 3;
		else {
			fprintf(stderr, "FAT fsck returned with error code %d\n", WEXITSTATUS(retval));
			retval = 4;
		}
	}

	return retval;
}

static int _ttsystem_check_exfat(const char *dev_name, bool repair)
{
	char cmd[50];
	int retval;

	/* Perform the check with optional repair */
	if (repair)
		snprintf(cmd, 50, "%s -r %s", FSCK_EXFAT, dev_name);
	else
		snprintf(cmd, 50, "%s %s", FSCK_EXFAT, dev_name);
	cmd[49] = '\0';
	retval = system(cmd);

	if (retval < 0) {
		fprintf(stderr, "Cannot execute exFAT fsck on %s\n", dev_name);
		retval = 2;
	}
	else if (WEXITSTATUS(retval) != 0) {
		/* if check-only and status=3 -> errors found */
		if (!repair && (WEXITSTATUS(retval) == 3))
			retval = 3;
		else {
			fprintf(stderr, "exFAT fsck returned with error code %d\n", WEXITSTATUS(retval));
			retval = 4;
		}
	}

	return retval;
}

static void _ttsystem_process_fs_label(const char *dev_name, char *label)
{
	blkid_probe pr = NULL;
	int ret;
	const char *data = NULL;
	char *fs_filter[] = {"vfat", "exfat", NULL};
	size_t len;

	/* Open the block device for probing */
	pr = blkid_new_probe_from_filename(dev_name);
	if (!pr) {
		fprintf(stderr, "unable to probe device '%s'\n", dev_name);
		ret = -1;
		goto out;
	}

	/* Enable probing of superblock */
	ret = blkid_probe_enable_superblocks(pr, 1);
	if (ret != 0) {
		fprintf(stderr, "error enabling superblock probing for device '%s': %d\n", dev_name, ret);
		goto out;
	}

	/* Set filter for supported filesystems */
	ret = blkid_probe_filter_superblocks_type(pr, BLKID_FLTR_ONLYIN, fs_filter);
	if (ret != 0) {
		fprintf(stderr, "error setting superblock probe filter for device '%s': %d\n", dev_name, ret);
		goto out;
	}

	/* Probe the device data */
	ret = blkid_do_safeprobe(pr);
	if (ret != 0) {
		fprintf(stderr, "error probing device '%s': %d\n", dev_name, ret);
		goto out;
	}

	/* Retrieve the label */
	ret = blkid_probe_lookup_value(pr, "LABEL", &data, &len);

out:
	/* If something went wrong or label is empty or too long -> use the default one */
	if ((ret != 0) || !data || (len > MAX_LABEL_LEN + 1))
		strncpy(label, DEFAULT_LABEL, MAX_LABEL_LEN + 1);
	else
		strncpy(label, data, MAX_LABEL_LEN + 1);

	/* Free the data structure */
	if (pr)
		blkid_free_probe(pr);
}

int ttsystem_check_sdcard(bool repair)
{
	struct stat stat_buf;
	char dev_name[20];
	int retval;

	/* If exists, use the 1st partition as device to check */
	if (stat(SDCARD_PARTITION1, &stat_buf) == 0) {
		strncpy(dev_name, SDCARD_PARTITION1, sizeof(dev_name));
	}
	/* Otherwise use the whole device */
	else if (stat(SDCARD, &stat_buf) == 0) {
		strncpy(dev_name, SDCARD, sizeof(dev_name));
	}
	else {
		perror("Cannot open SDCARD device");
		retval = -errno;
		goto out;
	}

	/* Unmount SD Card */
	if (_ttsystem_unmount_sdcard() < 0) {
		retval = -errno;
		goto out;
	}

	/* Check if the filesystem is FAT and performs the check/repair */
	if (_ttsystem_probe_fat(dev_name) == 0) {
		retval = _ttsystem_check_fat(dev_name, repair);
	}
	/* Check if the filesystem is exFAT and performs the check/repair */
	else if (_ttsystem_probe_exfat(dev_name) == 0) {
		retval = _ttsystem_check_exfat(dev_name, repair);
	}
	else {
		/* Fallthrough in case no valid FS has been found */
		fprintf(stderr, "Cannot find a FAT or exFAT filesystem on %s\n", dev_name);
		retval = 1;
	}

	/* Mount SD Card */
	if(_ttsystem_mount_sdcard() < 0)
		retval = -errno;
out:
	return retval;
}

int ttsystem_format_sdcard(void)
{
	int retval = 0;
	uint64_t size;
	unsigned char cmd[100];
	struct stat stat_buf;
	char label[MAX_LABEL_LEN+1];

	/* Unmount SD Card */
	if (_ttsystem_unmount_sdcard() < 0) {
		retval = -errno;
		goto out;
	}

	/* Get SD Card size */
	if (_ttsystem_get_sdcard_size(&size) < 0) {
		retval = -errno;
		goto mountout;
	}

	/* Determine the label to assign */
	if (stat(SDCARD_PARTITION1, &stat_buf) == 0)
		_ttsystem_process_fs_label(SDCARD_PARTITION1, label);
	else if (stat(SDCARD, &stat_buf) == 0)
		_ttsystem_process_fs_label(SDCARD, label);

	/* Format SD Card */
	if (size <= ((uint64_t)33 * GB))
		snprintf(cmd, 100, "%s -n '%s' %s", MKDOSFS, label, SDCARD);
	else
		snprintf(cmd, 100, "%s -l '%s' %s", MKEXFATFS, label, SDCARD);

	cmd[99] = '\0';
	retval = system(cmd);

	if (retval < 0) {
		fprintf(stderr, "SD Card formatting failed: %s",
				strerror(errno));
		retval = -errno;
	}

mountout:
	/* Mount SD Card */
	if(_ttsystem_mount_sdcard() < 0)
		retval = -errno;
out:
	return retval;
}

bool ttsystem_is_sdcard_present(void)
{
	DIR *blockDir = NULL;
	struct dirent entry;
	struct dirent *result = NULL;
	bool ret = false;

	if ((blockDir = opendir("/sys/block/")) == NULL) {
		perror("Can't open /sys/block/");
		return false;
	}

	while (readdir_r(blockDir, &entry, &result) == 0 && result != NULL) {
		if (strncmp(entry.d_name, "mmcblk", sizeof("mmcblk") - 1) == 0) {
			ret = true;
			break;
		}
	}
	closedir(blockDir);

	return ret;
}

int ttsystem_get_sdcard_status(void)
{
	int ret = 0;
	FILE *mounts = NULL;
	char line[256];

	char *savePtr = NULL;
	char *dev = NULL;
	char *mountPoint = NULL;
	char *options = NULL;

	if (!ttsystem_is_sdcard_present()) {
		return 1;
	}

	mounts = fopen("/proc/mounts", "r");
	if (mounts == NULL) {
		perror("Failed to open /proc/mounts");
		return -1;
	}

	ret = 2;
	while (fgets(line, sizeof(line), mounts) != NULL) {
		dev = strtok_r(line, " \t\n", &savePtr);
		if (dev == NULL || strncmp(dev, SDCARD, sizeof(SDCARD) - 1) != 0) continue;

		mountPoint = strtok_r(NULL, " \t\n", &savePtr);
		if (mountPoint == NULL || strcmp(mountPoint, SDCARD_MOUNT_POINT) != 0) continue;

		if (strtok_r(NULL, " \t\n", &savePtr) == NULL) continue; /* skip file system type */

		options = strtok_r(NULL, " \t\n", &savePtr);
		if (options != NULL) {
			if (strstr(options, "rw") != NULL) {
				ret = 0;
			} else {
				ret = 3;
			}
		}
		break;
	}

	fclose(mounts);

	return ret;
}
