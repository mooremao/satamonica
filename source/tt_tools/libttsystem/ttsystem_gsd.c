/*
 * \file Implements ttsystem_gsd query functions
 */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include "gpsd_sgee.h"
#include "gsd_rtc_defs.h"
#include "ttsystem.h"
#include "ttsystem_priv.h"

#define GSD_SLEEP_PATH "/sys/class/tty/ttyGSD5xp0/device/sleep"
#define GSD_AWAKE_PATH "/sys/class/tty/ttyGSD5xp0/device/awake"

int ttsystem_gps_put_to_sleep(bool sleep);
static int ttsystem_check_and_restart();

int ttsystem_set_rtc(const char *time)
{
	struct tm tm_in;
	int ret, tmp;
	FILE *fp;

	/* Validate and convert the input string */
	if (!strptime(time, "%Y-%m-%d %H:%M:%S", &tm_in)) {
		fprintf(stderr, "Invalid time string format: '%s'\n", time);
		ret = -1;
		goto err;
	};

	/* Convert into seconds from epoch */
	tmp = (int)mktime(&tm_in);
	if ((tmp == -1) || (tmp < DEFAULT_RTC_DATE)) {
		fprintf(stderr, "Invalid time: '%s' - %d\n", time, tmp);
		ret = -1;
		goto err;
	}

	/* Open the RTC pipe */
	fp = fopen(GSD_RTC_PIPE, "r+");
	if (fp == NULL) {
		fprintf(stderr, "Error opening GPS pipe %s: %s\n", GSD_RTC_PIPE, strerror(errno));
		ret = -errno;
		goto err;
	}

	/* Send the value */
	ret = fprintf(fp, "%d", tmp);
	if (ret < 0) {
		fprintf(stderr, "Error writing to GPS pipe: %s\n", strerror(errno));
		ret = -errno;
	}

	fclose(fp);


	if (ttsystem_check_and_restart() != 0) {
		fprintf(stderr, "Error while restarting gsd for system clk sync.: %s\n", strerror(errno));
	}

err:
	return (ret >= 0) ? 0 : ret;
}

int ttsystem_get_sgee_info(ttsystem_sgee_navsys_t navsys, int32_t *status, int32_t *age, uint32_t *pred_int)
{
	FILE *fp;
	int ret;
	const char *buf;
	sgee_status_t tmp_state;
	int32_t tmp_age, tmp_ts;
	uint32_t tmp_pi;
	struct timespec tp;

	/* Sanity check of the parameters */
	if (((navsys != TTSYSTEM_SGEE_GPS) && (navsys != TTSYSTEM_SGEE_GLO)) || !status || !age || !pred_int) {
		fprintf(stderr, "Invalid parameters: navsys=%d status=%p age=%p pred_int=%p\n", navsys, status, age, pred_int);
		ret = -1;
		goto err;
	}

	/* Set the input file to use */
	buf = (navsys == TTSYSTEM_SGEE_GPS) ? SGEE_INFO_FILE_GPS : SGEE_INFO_FILE_GLO;

	/* Open the file */
	fp = fopen(buf,"rb");
	if (fp == NULL) {
		fprintf(stderr, "Error opening file %s: %s\n", buf, strerror(errno));
		ret = -1;
		goto err;
	}

	/* Parse the data */
	/* Length formatting should help avoid overflow. */
	ret = fscanf(fp, "state=%1d;age=%10d;ts=%10d;pi=%10u\n", &tmp_state, &tmp_age, &tmp_ts, &tmp_pi);
	if (ret != 4) {
		fprintf(stderr, "Error parsing file %s: %d - %s\n", buf, ret, strerror(errno));
		ret = -1;
		goto err_close;
	}

	/* Check the SGEE status */
	switch(tmp_state) {
		case SGEE_NOQF:
			*status = 0;
			*age = -1;
			*pred_int = 0;
			ret = 0;
			break;
		case SGEE_DOWNLOAD:
			*status = 1;
			*age = -1;
			*pred_int = 0;
			ret = 0;
			break;
		case SGEE_ERR:
			*status = 2;
			*age = -1;
			*pred_int = 0;
			ret = 0;
			break;
		case SGEE_DONE:
			if (tmp_age == -1) {
				/* Age shouldn't be invalid if status is DONE */
				fprintf(stderr, "Invalid age: %d %s", tmp_age, strerror(errno));
				ret = -1;
				goto err_close;
		 	} else {
				/* Age valid -> calculate the age based on the current system boot time */
				if (clock_gettime(CLOCK_BOOTTIME, &tp) < 0) {
					fprintf(stderr, "Error getting system time: %s", strerror(errno));
					ret = -1;
					goto err_close;
				}
				*status = 3;
				*age = tmp_age + (int32_t)tp.tv_sec - tmp_ts;
				*pred_int = tmp_pi;
			}
			ret = 0;
			break;
		case SGEE_NORTC:
			*status = 4;
			*age = -1;
			*pred_int = 0;
			ret = 0;
			break;

		/* Unknown state */
		default:
			fprintf(stderr, "Invalid SGEE state: %d\n", tmp_state);
			ret = -1;
			break;
	}

err_close:
	fclose(fp);
err:
	return ret;
}

int ttsystem_gps_put_to_sleep(bool sleep)
{
	int ret = 0, tmp = 0;
	FILE* fp = NULL;
	int gsd_sleep_lock = -1;

	/* If going to sleep, obtain the lock */
	if (sleep) {
		/* Open the lock file */
		gsd_sleep_lock = open(GSD_SLEEP_LOCK_PATH, O_CREAT | O_RDWR | O_CLOEXEC);
		if (gsd_sleep_lock < 0) {
			fprintf(stderr, "Error opening GPS sleep lock %s: %s\n", GSD_SLEEP_LOCK_PATH, strerror(errno));
			ret = -errno;
			goto err;
		}

		/* Lock the gps sleep lock */
		if (flock(gsd_sleep_lock, LOCK_EX | LOCK_NB) != 0) {
			fprintf(stderr, "GSD chip is not in sleep-safe status\n");
			ret = -errno;
			goto err;
		}
	}

	/* Open the VFS entry*/
	fp = fopen(GSD_SLEEP_PATH, "w");
	if (fp == NULL) {
		fprintf(stderr, "Error opening GPS sleep path %s: %s\n", GSD_SLEEP_PATH, strerror(errno));
		ret = -errno;
		goto unlock;
	}

	if (sleep) {
		tmp = 1;
	}
	/* Send the value */
	ret = fprintf(fp, "%d", tmp);
	if (ret < 0) {
		fprintf(stderr, "Error writing to GPS pipe: %s\n", strerror(errno));
		ret = -errno;
	}

	fclose(fp);
unlock:
	if (sleep && flock(gsd_sleep_lock, LOCK_UN) != 0)
		fprintf(stderr, "Error unlocking gsd sleep lock: s\n", strerror(errno));
err:
	if (sleep)
		close(gsd_sleep_lock);

	return (ret >= 0) ? 0 : ret;
}

/*The system time can't be synchronized with the GPS' if it is a sleep. Wake it up and put it to sleep again if needed. */
static int ttsystem_check_and_restart()
{
	int ret = 0, val = 0;
	/* Open the VFS awake entry*/
	ret = ttsystem_gps_is_awake(&val);
	if (ret != 0) {
		goto err;
	}
	/* If the gsd was asleep, wake it up */
	if (val == 0) {
		ret = ttsystem_gps_put_to_sleep(false);
		if (ret != 0) {
			fprintf(stderr, "Error while attempting to wake up for RTC sync %s\n", strerror(errno));
			goto err;
		}
		/*Give it some time to sync.*/
		sleep(2);
		ret = ttsystem_gps_put_to_sleep(true);
		if (ret != 0) {
			fprintf(stderr, "Error while attempting to go back to sleep after RTC sync %s\n", strerror(errno));
		}
	}

err:
	return (ret >= 0) ? 0 : ret;
}

int ttsystem_gps_is_awake(int* is_awake){
	int ret = 0;
	FILE* fp = NULL;
	/* Open the VFS awake entry*/
	fp = fopen(GSD_AWAKE_PATH, "r");
	if (fp == NULL) {
		fprintf(stderr, "Error opening GPS awake path %s: %s\n", GSD_AWAKE_PATH, strerror(errno));
		ret = -errno;
		goto err;
	}

	fscanf(fp, "%d", is_awake);

	fclose(fp);

err:
	return (ret >= 0) ? 0 : ret;
}


