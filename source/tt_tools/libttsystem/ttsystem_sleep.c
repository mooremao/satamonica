/*
 * \file Implements ttsystem_sleep function
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <ttsystem.h>
#include "ttsystem_priv.h"

#define MILLISEC_PER_SEC 1000
#define MICROSEC_PER_SEC (1000 * 1000)
#define NANOSEC_PER_SEC (1000 * 1000 * 1000)
#define NANOSEC_PER_MILLISEC (1000 * 1000)

#define BT_SLEEP_REQ_FILENAME "/tmp/BT_Sleep_Req"

static int fd_wakeuptimeout;
static int fd_wakeupsource;
static int fd_wakeupcount;
static int fd_irqsource;
static int fd_sleep;
static int fd_gsd_sleep_lock;

static bool prepare_sleep_done;
static int wakeup_count;

static int _ttsystem_sleep_init(void)
{
	if(ttsystem_prepare_sleep() < 0)
		return -1;

	/* Init for ttsystem_sleep */
	fd_wakeupcount = open("/sys/power/wakeup_count", O_RDWR | O_CLOEXEC);
	if(fd_wakeupcount < 0) {
		fprintf(stderr, "ttsystem_prepare_sleep: could not open wakeup_count sysfs file, errno = %d\n", errno);
		return -1;
	}

	fd_wakeuptimeout = open("/sys/class/power_supply/tomtom_mcu/device/wakeup_timeout", O_RDWR | O_CLOEXEC);
	if(fd_wakeuptimeout < 0) {
		fprintf(stderr, "ttsystem_sleep: could not open wakeup_timeout sysfs file, errno = %d\n", errno);
		return -1;
	}

	fd_wakeupsource = open("/sys/class/power_supply/tomtom_mcu/device/wakeup_source", O_RDONLY | O_CLOEXEC);
	if(fd_wakeupsource < 0) {
		fprintf(stderr, "ttsystem_sleep: could not open wakeup_source sysfs file, errno = %d\n", errno);
		return -1;
	}

	fd_irqsource = open("/sys/bus/i2c/drivers/tca8418_keypad/3-0034/irq_source", O_RDONLY | O_CLOEXEC);
	if(fd_irqsource < 0) {
		fprintf(stderr, "ttsystem_sleep: could not open irq_source sysfs file, errno = %d\n", errno);
		return -1;
	}

	fd_sleep = open("/sys/power/state", O_WRONLY | O_CLOEXEC);
	if(fd_sleep < 0) {
		fprintf(stderr, "ttsystem_sleep: could not open deepsleep sysfs file, errno = %d\n", errno);
		return -1;
	}

	fd_gsd_sleep_lock = open(GSD_SLEEP_LOCK_PATH, O_CREAT | O_RDWR | O_CLOEXEC);
	if(fd_gsd_sleep_lock < 0) {
		fprintf(stderr, "ttsystem_sleep: could not gsd sleep lock file, errno = %d\n", errno);
		return -1;
	}

	/*
	 * Ensure rtcclocksignal service is started (although it is probably already started, but a race condition can
	 * occure when entering deep sleep directy after starting the camera-app, as rtcclocksignal.service
	 * is not enforced to be started before camera-app.service starts).
	 *
	 * RTC clock signal needs to enabled since the SoC RTC needs to be operational during deep sleep.
	 */
	system("systemctl start rtcclocksignal.service");

	return 0;
}

static void _ttsystem_sleep_deinit(void)
{
	prepare_sleep_done = 0;
	wakeup_count = 0;
	close(fd_wakeupcount);
	close(fd_wakeuptimeout);
	close(fd_wakeupsource);
	close(fd_irqsource);
	close(fd_sleep);
	close(fd_gsd_sleep_lock);
}

/***************************************************************************************
 * ttsystem_prepare_sleep:
 *	Prepare for sleep. If a wake up event occurs between call
 *	to ttsystem_prepare_sleep and ttsystem_sleep, the system will not sleep
 ***************************************************************************************/

bool ttsystem_prepare_sleep(void)
{
	int fd;
	char buf[12];

	if(prepare_sleep_done)
		return 0;

	fd = open("/sys/power/wakeup_count", O_RDWR | O_CLOEXEC);
	if(fd < 0) {
		fprintf(stderr, "ttsystem_prepare_sleep: could not open wakeup_count sysfs file, errno = %d\n", errno);
	}

	/* Read from and write back to /sys/power/wakeup_count */
	lseek(fd, 0, SEEK_SET);
	if (read(fd, buf, 10) < 0) {
		/* Read should not fail unless the file /sys/power/wakeup_count
		   does not exist or we do not have permission to read it.
		   If read fails, we assume that the kernel does not support
		   /sys/power/wakeup_count and we go ahead and deepsleep
		   without worrying about losing wakeup events */
		fprintf(stderr, "ttsystem_sleep: could not read wakeup count\n");
		goto err_rd;
	}
	wakeup_count = strtoul(buf, NULL, 10);

	prepare_sleep_done = 1;

err_rd:
	close(fd);
	return 0;
}

/***************************************************************************************
 * ttsystem_sleep:
 *	Software or hardware for sleep 'timeout' milliseconds.
 *
 *	timeout = -1: power off
 *	timeout =  0: sleep indefinitely or until button or bluetooth wakeup
 *	timeout >  0: sleep for timeout seconds or until button or bluetooth wakeup
 *	timeout <  0 but not -1: return
 *
 *	Returns source of wakeup
 *
 * Dependencies:
 * - LBP-784: Wakeup timer on MCU should have required granularity
 * - LBP-699: System time should be set correctly after wakeup
 ***************************************************************************************/
ttsystem_wakeup_t ttsystem_sleep(int timeout)
{
	const int buff_size = 16;
	char buf[buff_size];
	size_t size;
	int ret;

	struct timespec tp_sleep, tp_wakeup, tp_diff, tp_init;
	int wakeup_timeout_in_sec; // default wake up timeout is zero

	ttsystem_wakeup_t rv;

	FILE *fp;

	clock_gettime(CLOCK_BOOTTIME, &tp_sleep);

	/* A timeout value of -1 means power off */
	if (timeout == -1) {
		system("systemctl poweroff");
		return TTSYSTEM_WAKE_OTHER;
	}
	else if (timeout < 0) {
		/* We do not handle negative timeout values other than -1 */
		fprintf(stderr, "ttsystem_sleep: unsupported timeout value\n");
		return TTSYSTEM_SLEEP_ERROR_INVALID_TIMEOUT;
	}

	ret = _ttsystem_sleep_init();
	if (ret < 0) {
		fprintf(stderr, "ttsystem_sleep: init failed\n");
		rv = TTSYSTEM_SLEEP_ERROR_INTERNAL;
		goto err_do_deinit;
	}

	/* Calculate wakeup timeout */

	/* A timeout value of zero means no wakeup timer needs to be set */
	if (timeout > 0) {

		 /* We measure the time taken for init and subtract that from timeout */
		clock_gettime(CLOCK_BOOTTIME, &tp_init);
		tp_diff.tv_sec = tp_init.tv_sec - tp_sleep.tv_sec;
		tp_diff.tv_nsec = tp_init.tv_nsec - tp_sleep.tv_nsec;
		if (tp_diff.tv_nsec < 0) {
			tp_diff.tv_sec--;
			tp_diff.tv_nsec += NANOSEC_PER_SEC;
		}

		int wakeup_timeout_in_msec = timeout -
			((tp_diff.tv_sec * MILLISEC_PER_SEC) + tp_diff.tv_nsec / NANOSEC_PER_MILLISEC);

		/* Wakeup Timeout = timeout - (time required for system wakeup) */
		wakeup_timeout_in_sec = (wakeup_timeout_in_msec - MAX_SYSTEM_WAKEUP_TIME_IN_MS) / MILLISEC_PER_SEC;

		/* The caller set a timeout value that is less than the minimum time we can deepsleep */
		if (wakeup_timeout_in_sec <= 0)
		{
			rv = TTSYSTEM_SLEEP_ERROR_TIMEOUT_TOO_SHORT;
			goto err_do_deinit;
		}
	} else {
		wakeup_timeout_in_sec = 0;
	}

	/* Set up wakeup timer */
	size = snprintf(buf, 12, "%d\n", wakeup_timeout_in_sec);
	ret = write(fd_wakeuptimeout, buf, size);
	if (ret < 0) {
		/* Should not happen */
		fprintf(stderr, "ttsystem_sleep: could not set wakeup timeout\n");
		rv = TTSYSTEM_SLEEP_ERROR_INTERNAL;
		goto err_do_deinit;
	}


	/* Create the semaphore file for the BT stack */
	fp = fopen(BT_SLEEP_REQ_FILENAME,"w");
	if (fp == NULL) {
		fprintf(stderr, "ttsystem_sleep: error creating semaphore file for BT stack\n");
		rv = TTSYSTEM_SLEEP_ERROR_INTERNAL;
		goto err_do_deinit;
	}
	fclose(fp);

	/* Check the presence of the file created by the BT stack */
	if (ttsystem_bt_sleep_allowed() != 1) {
		fprintf(stderr, "ttsystem_sleep: BT stack not in sleep-safe status, not entering deep sleep\n");
		rv = TTSYSTEM_SLEEP_ERROR_BLE_STATE;
		goto err_bt_semapore_locked;
	}

	/* Lock the gps sleep lock */
	if (flock(fd_gsd_sleep_lock, LOCK_EX | LOCK_NB) != 0) {
		fprintf(stderr, "ttsystem_sleep: GSD chip is not in sleep-safe status, not entering deep sleep\n");
		rv = TTSYSTEM_SLEEP_ERROR_GSD_STATE;
		goto err_bt_semapore_locked;
	}


	if (prepare_sleep_done) {
		size = snprintf(buf, 12, "%d\n", wakeup_count);
		if (write(fd_wakeupcount, buf, size) < 0) {
			/* If read on /sys/power/wakeup_count succeeds
			   and write fails, it means a wakeup event has occured
			   and we should not enter deep sleep */
			fprintf(stderr, "ttsystem_sleep: write wakeup count failed, wakeup event fired already.\n");
			rv = TTSYSTEM_SLEEP_ERROR_WOKEN_UP;
			goto err_gsd_sleep_locked;
		}
	}

	/* Enter deep sleep */
	ret = write(fd_sleep, "mem\n", sizeof("mem\n"));

	/* Do post wake up activity */
	/* We are awake after deepsleep now */

	if (ret < 0) { /* Entering deep sleep failed */
		if (errno == ETIME) {
			/*
			 * Timeout expired before entering deep sleep.
			 * Continue normal processing but skip processing keypad and mcu wakeup sources
			 */
			rv = TTSYSTEM_WAKE_TIMER;
		} else if (errno == EAGAIN) {
			/*
			 * Wake up event occured
			 */
			rv = TTSYSTEM_SLEEP_ERROR_WOKEN_UP;
		} else {
			fprintf(stderr, "ttsystem_sleep: could not enter deep sleep, errno = %d\n", errno);
			rv = TTSYSTEM_SLEEP_ERROR_INTERNAL;
			goto err_gsd_sleep_locked;
		}
	} else { /* Normal wake up */
		/* Read wakeup interrupt source from keypad */
		lseek(fd_irqsource, 0, SEEK_SET);
		if (read(fd_irqsource, buf, 10) <= 0) {
			fprintf(stderr, "ttsystem_sleep: could not read keypad irq source\n");
			rv = TTSYSTEM_WAKE_OTHER;
			goto err_gsd_sleep_locked;
		}

		/* Determine wakeup reason */
		if (!strncmp(buf, "mcu", strlen("mcu"))) {
			/* Read wakeup interrupt source from mcu */
			lseek(fd_wakeupsource, 0, SEEK_SET);
			if (read(fd_wakeupsource, buf, buff_size) <= 0) {
				fprintf(stderr, "ttsystem_sleep: could not read mcu wakeup source\n");
				rv = TTSYSTEM_WAKE_OTHER;
				goto err_gsd_sleep_locked;
			}

			if (!strncmp(buf, "button", strlen("button")))
				rv = TTSYSTEM_WAKE_BUTTON;
			else if (!strncmp(buf, "timer", strlen("timer")))
				rv = TTSYSTEM_WAKE_TIMER;
			else if (!strncmp(buf, "power_prn", strlen("power_prn")))
				rv = TTSYSTEM_WAKE_POWER_PRN;
			else if (!strncmp(buf, "soc_turn_off", strlen("soc_turn_off")))
				rv = TTSYSTEM_WAKE_SOC_TURN_OFF;
			else if (!strncmp(buf, "batt_low", strlen("batt_low")))
				rv = TTSYSTEM_WAKE_SOC_BATT_LOW;
			else if (!strncmp(buf, "batt_temp", strlen("batt_temp")))
				rv = TTSYSTEM_WAKE_BATT_TEMP;
			else
				rv = TTSYSTEM_WAKE_OTHER;
		} else if (!strncmp(buf, "ble", strlen("ble"))) {
			rv = TTSYSTEM_WAKE_BLE;
		} else if (!strncmp(buf, "button", strlen("button"))) {
			rv = TTSYSTEM_WAKE_BUTTON;
		} else {
			rv = TTSYSTEM_WAKE_OTHER;
		}
	}

	/* Did we wake up early? Should we software sleep some more? */
	if (rv == TTSYSTEM_WAKE_TIMER) {
		clock_gettime(CLOCK_BOOTTIME, &tp_wakeup);
		tp_diff.tv_sec = tp_wakeup.tv_sec - tp_sleep.tv_sec;
		tp_diff.tv_nsec = tp_wakeup.tv_nsec - tp_sleep.tv_nsec;
		if (tp_diff.tv_nsec < 0) {
			tp_diff.tv_sec--;
			tp_diff.tv_nsec += NANOSEC_PER_SEC;
		}

		/* We slept for less than the required duration */
		if (timeout > (tp_diff.tv_sec * MILLISEC_PER_SEC + tp_diff.tv_nsec / NANOSEC_PER_MILLISEC))
			rv = TTSYSTEM_SLEEP_MORE;
	}

err_gsd_sleep_locked:
	if (flock(fd_gsd_sleep_lock, LOCK_UN) != 0)
		fprintf(stderr, "ttsystem_sleep: error unlocking gsd sleep lock\n");

err_bt_semapore_locked:
	if (remove(BT_SLEEP_REQ_FILENAME) != 0)
		fprintf(stderr, "ttsystem_sleep: error removing semaphore file for BT stack\n");

err_do_deinit:
	_ttsystem_sleep_deinit();
	return rv;
}

