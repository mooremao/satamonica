/*
 * \file Implements ttsystem_beep enable/disable functions
 */

#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MUTE_FILE "/mnt/userdata/mute"

void ttsystem_beep_enabled(bool enabled)
{
	if (enabled)
		unlink(MUTE_FILE);
	else {
		int fd = creat(MUTE_FILE, S_IRUSR);
		if (fd >= 0) close(fd);
	}
}
