#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <ttsystem.h>

static volatile int exitFlag = 0;

static void sig_handler(int signum)
{
	fprintf(stderr, "Got TERM signal. Exiting...\n");
	exitFlag = 1;
}

int main(int argc, char **argv)
{
	struct sigaction sigAct;
	sigset_t sigSet;

	memset(&sigAct, 0, sizeof(sigAct));
	sigAct.sa_handler = sig_handler;
	if (sigaction(SIGTERM, &sigAct, NULL)) {
		fprintf(stderr, "Failed to register signal handler: %s\n", strerror(errno));
		return -1;
	}

	sigemptyset(&sigSet);
	sigaddset(&sigSet, SIGTERM);
	sigprocmask(SIG_UNBLOCK, &sigSet, NULL);

	ttsystem_app_started();

	while (!exitFlag) {
		ttsystem_trigger_watchdog();
		sleep(2);
	}

	return 0;
}
