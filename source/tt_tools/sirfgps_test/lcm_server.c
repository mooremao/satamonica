/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <signal.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <mqueue.h>
#include <memory.h>
#include "lcm_msg.h"

#define DELAY_SEC 0.5
//#define FONT_SIZE_16X8
#define FONT_SIZE_16X16

#ifdef FONT_SIZE_16X16
#define FONT_SIZE_X 16
#define FONT_SIZE_Y 16
#include "font16x16_basic.h"
#endif



#ifdef FONT_SIZE_16X8
#define FONT_SIZE_X 16
#define FONT_SIZE_Y 8
#include "font16x8_basic.h"
#endif

#define LCM_WIDTH				(framebuffer.var_scr_info.xres)// 144
#define LCM_HEIGTH			(framebuffer.var_scr_info.yres) // 	168
#define NUM_LINES (LCM_HEIGTH/FONT_SIZE_Y)
#define LCM_PIXELS_PER_BYTE		(8/framebuffer.var_scr_info.bits_per_pixel)
#define LCM_BYTES_PER_LINE		(LCM_WIDTH / LCM_PIXELS_PER_BYTE)




#define FB_DEV_NODE "/dev/fb0"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

struct fb_priv
{
	int fd;
};
struct framebuffer
{
	uint8_t *mem;

	struct fb_fix_screeninfo fix_scr_info;
	struct fb_var_screeninfo var_scr_info;

	struct fb_priv *priv;
};

static struct fb_priv fb_priv = {
	.fd	= -1,
};

static struct framebuffer framebuffer = {
	.priv	= &fb_priv,
	.mem	= NULL,
};




static void pabort(const char *s)
{
	perror(s);
	abort();
}
static int aQuit=0,start_to_terminate = 0;
static int fb_fd;

static int lcd_on ;


typedef unsigned char uint8_t;
static int active_line = 0;



static void drawChar(char ch, uint8_t cursor_pos_x, uint8_t cursor_pos_y )
{
	 uint8_t y ;
	 
	 int dst_byte_address,src_byte_address;
	 
	for(y=0;y<FONT_SIZE_Y;y++)
	{
		dst_byte_address = (cursor_pos_y*FONT_SIZE_X+y)*LCM_BYTES_PER_LINE+cursor_pos_x*FONT_SIZE_X/8;
		src_byte_address = ch*(FONT_SIZE_X/8)*FONT_SIZE_Y+(FONT_SIZE_X/8)*y;
		memcpy(&framebuffer.mem[dst_byte_address], &font[src_byte_address],FONT_SIZE_X/8);
 }
}

static void drawString(char *str, uint8_t cursor_pos_x, uint8_t cursor_pos_y )
{
	int i;
	
	for(i=0;i<strlen(str);i++)
		drawChar(str[i],cursor_pos_x+i,cursor_pos_y);
}

static void clearStr(uint8_t cursor_pos_x, uint8_t cursor_pos_y, uint8_t num_char )
{
  uint8_t y ;
	 
	int dst_byte_address;
	 
	for(y=0;y<FONT_SIZE_Y;y++)
	{
		dst_byte_address = (cursor_pos_y*FONT_SIZE_X+y)*LCM_BYTES_PER_LINE+cursor_pos_x*FONT_SIZE_X/8;
		memset(&framebuffer.mem[dst_byte_address], 0x00,num_char*FONT_SIZE_X/8);
	}

}

static void clearAll(void )
{
	memset(&framebuffer.mem[0], 0x00, framebuffer.fix_scr_info.smem_len);
}


struct framebuffer *framebuffer_open(void)
{
	printf("framebuffer_open\n");
	framebuffer.priv->fd = open(FB_DEV_NODE, O_RDWR);
	if (framebuffer.priv->fd == -1) {
		printf("Unable to open %s", FB_DEV_NODE);
		return NULL;
	}

	if (ioctl(framebuffer.priv->fd, FBIOGET_FSCREENINFO, &framebuffer.fix_scr_info)) {
		printf("unable to get fixed screen info");
		goto err_scr_info;
	}

	if (ioctl(framebuffer.priv->fd, FBIOGET_VSCREENINFO, &framebuffer.var_scr_info)) {
		printf("unable to get variable screen info");
		goto err_scr_info;
	}

	framebuffer.mem = mmap(NULL, framebuffer.fix_scr_info.smem_len, PROT_READ | PROT_WRITE, MAP_SHARED, framebuffer.priv->fd, 0);
	if (framebuffer.mem == NULL) {
		printf("Unable to mmap %s", FB_DEV_NODE);
		goto err_mmap;
	}

	if (ioctl(framebuffer.priv->fd, FBIOBLANK, FB_BLANK_UNBLANK)) {
		printf("Unable to turn on the LCD");
		goto err_unblank;
	}

 // clear all frame buffer
  memset(&framebuffer.mem[0], 0x00, framebuffer.fix_scr_info.smem_len);
	return &framebuffer;

err_mmap:
err_scr_info:
err_unblank:
	close(framebuffer.priv->fd);
	framebuffer.priv->fd = -1;

	return NULL;
}

void framebuffer_close(struct framebuffer *fb)
{
	if (fb->priv->fd != -1) {
		munmap(fb->mem, fb->fix_scr_info.smem_len);
		close(fb->priv->fd);

		fb->mem = NULL;
		fb->priv->fd = -1;
	}
}

static void SignalHandler(int signo)
{
	printf("Received signal %s\n", strsignal(signo));
	switch (signo)
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
				 aQuit = 1;
				 break;
		default:
				 break;
	} // end switch
}

static void processMsg( t_LCM_DispayMsg *msg)
{
	switch(msg->msg_id)
	{
		case MSG_ID_LCM_DISPLAY_DRAW:
			drawString(msg->msg_data.msg_data_draw.text, msg->msg_data.msg_data_draw.cursor_pos_x, msg->msg_data.msg_data_draw.cursor_pos_y);
			fsync(framebuffer.priv->fd);			
			break;
		case MSG_ID_LCM_DISPLAY_CLEAR:
			clearStr(msg->msg_data.msg_data_clear.cursor_pos_x, msg->msg_data.msg_data_clear.cursor_pos_y, msg->msg_data.msg_data_clear.num_chars);
			fsync(framebuffer.priv->fd);			
			break;
		case MSG_ID_LCM_DISPLAY_CLEAR_ALL:
			clearAll();
			fsync(framebuffer.priv->fd);			
			break;
		case MSG_ID_LCM_DISPLAY_EXIT:
			start_to_terminate = 1;
			break;					
		default:
			printf("unknown display msg\n");
		}
}

int main(int argc, char *argv[])
{
	int ret = 0;
	struct framebuffer *fb;
	struct sigaction act;
	t_LCM_DispayMsg *msg_p;
	unsigned char buffer[MSG_SIZE];
  mqd_t mq;
  struct mq_attr attr;
  ssize_t bytes_read;

  /* initialize the queue attributes */
  attr.mq_flags = 0;
  attr.mq_maxmsg = 10;
  attr.mq_msgsize = MSG_SIZE;
  attr.mq_curmsgs = 0;
	
/* create the message queue */
   mq = mq_open(QUEUE_NAME, O_CREAT | O_RDONLY, 0644, &attr);
   CHECK((mqd_t)-1 != mq);
   	 
	 fb = framebuffer_open();
	sigemptyset(&act.sa_mask); 
	act.sa_flags   = 0;
	act.sa_handler = SignalHandler;
	sigaction(SIGTERM, &act, NULL); 
	sigaction(SIGINT, &act, NULL);
	lcd_on = 0;
        	
	while(!aQuit)
	{
		bytes_read = mq_receive(mq, &buffer[0], MSG_SIZE, NULL);
    CHECK(bytes_read >= 0);
		msg_p = (t_LCM_DispayMsg *) &buffer[0];
		
  	if(bytes_read > 0)
  	{
			processMsg(msg_p);
		}
		if(start_to_terminate)
		{
			if(bytes_read == 0)
			{
				aQuit = 1;
			}
		}

  }
	framebuffer_close(fb);
	
    /* cleanup */
    CHECK((mqd_t)-1 != mq_close(mq));
    CHECK((mqd_t)-1 != mq_unlink(QUEUE_NAME));


	return ret;
}