
#include <stdlib.h>
#include "gps_state.h"
static eGPS_STATE mg_gps_state;

void gps_set_state( eGPS_STATE state)
{
	mg_gps_state = state;
}

eGPS_STATE gps_get_state(void)
{
	return mg_gps_state;
}