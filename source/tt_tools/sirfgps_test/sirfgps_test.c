/*
 * CSR GSDX testing utility 
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <signal.h>
#include <pthread.h>
#include <execinfo.h>
#include "errno.h"
#include "osp.h"
#include "tty_data.h"
#include "test_result.h"
#include "gps_state.h"

extern uint16_t hibernate_cmd_send;	
int aQuit=0;

static int pipe_cmd_fd =-1;
static int testmode4 = 0, test_period = 10, svid=1, testmode4_num_period = 2, chamber_mode=0, open_sky_mode = 0 , hibernate_test = 0, fix_only_mode = 0, has_display=1;
static int coldstart = 0;
static int wake_at_init = 0;

// In first 5.5.21 patch the RTC in hibernate mode is default on.
static int hibernate_rtc_out = 1;

static int is_awake() {
	int fd;
	int len;
	char value[2];

	fd = open("/sys/class/tty/ttyGSD5xp0/device/awake", O_RDONLY);
	if (fd < 0) {
		printf("Failed to open awake sysfs entry\n");
		return 0;
	}

	len = read(fd, value, sizeof(value));
	if (len < 0) {
		printf("Failed to read awake sysfs entry\n");
		close(fd);
		return 0;
	}

	close(fd);
	return value[0] == '1';
}

static void cold_reset_without_tty_open() {
	printf("cold_reset_without_tty_open()\n");

	system("echo 1 > /sys/class/tty/ttyGSD5xp0/device/reset");
	gps_set_state( GPS_STATE_INIT );

	system("gsdpatcher /dev/ttyGSD5xp0");

	printf("cold_reset_without_tty_open() finished\n");
}

static void cold_reset_with_tty_open() {
	printf("cold_reset_with_tty_open()\n");
	//Close tty
	tty_data_deinit();
	tty_data_join_parse_thread();

	cold_reset_without_tty_open();

	//Reopen tty
	tty_data_init();
	tty_data_start_parse_thread();

	printf("cold_reset_with_tty_open() finished\n");
}

static int ask_version_with_retry(void)
{
		int patch_detect_count = 0;
		int rom_patch_det;
		
		while(patch_detect_count<=6)
		{
			rom_patch_det = rom_patch_detected();
			if(rom_patch_det != -1)
			{
				break;
			}
			else
			{ // not yet got version
				ask_version();
				sleep(1);
				patch_detect_count++;
			}			
		}
		return rom_patch_det;	
}

static int wait_for_mid_18_1(void)
{
	uint8_t count;
	
	count = 0;
 	while(count <=6)
 	{
 		if(get_mid_18_1_detected())
 			break;
 		count++;
 		usleep(500000);
 	}
 	if(count == 7 )
 	{
 		printf("%s: cannot detect mid_18_1\n",__FUNCTION__);
 		return -1;
 	}
 	else
 	{
 		return 0;
 	}
}

static void hibernate_rtc_option(int rtc_on)
{
	
  tcxo_io_config(rtc_on);
  clear_mid_18_1_detected();
  hot_start();
  if(wait_for_mid_18_1()!=0)
  {
  	hot_start();
  	sleep(1);
 	}
	printf("read_wakeup_pin wake %d\n",is_awake());
	hibernate();
}

static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "cold",  1, 0, 'c' },
			{ "tm",  1, 0, 't' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "cs:p:n:m:g:o:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'o':
			hibernate_rtc_out = atoi(optarg);
			if(hibernate_rtc_out)
				hibernate_rtc_out = 1;
			else
				hibernate_rtc_out = 0;
			break;
	
		case 'c':
			coldstart = 1;
			break;
		case 'm':
			if(strstr(optarg,"t4")!=NULL)
			{
				coldstart = 1;
				testmode4=1;						
			}
			else if(strstr(optarg,"open")!=NULL)
			{
				coldstart = 1;
				open_sky_mode=1;						
			}		
			else if(strstr(optarg,"cham")!=NULL)
			{
				coldstart = 1;
				chamber_mode=1;						
			}
			else if(strstr(optarg,"hib")!=NULL)
			{
				printf("hibernate test \n");
				hibernate_test = 1;				
			}
			else if(strstr(optarg,"fixonly")!=NULL)
			{
				fix_only_mode=1;						
			}					
			break;
			
		case 'g':
			has_display = atoi(optarg);
			break;
									
		case 's':
			svid=atoi(optarg);
			break;
		case 'p':
			test_period=atoi(optarg);
			break;
		case 'n':
			testmode4_num_period=atoi(optarg);
			break;	
		default:
		//	print_usage(argv[0]);
		  printf("unknown option\n");
		  exit(1);
			break;
		}
	}
}

static void err_handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:size %zu\n", sig,size);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}


static void SignalHandler(int signo)
{
	printf("Received signal %s\n", strsignal(signo));
	switch (signo)
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
				 aQuit = 1;
				 break;
		case SIGABRT:
		case SIGSEGV:
					err_handler(signo);
				 break;
		default:
				 break;
	} // end switch
}




static void read_cmd_pipe(void)
{
	char cmd;
	if(read( pipe_cmd_fd, &cmd,sizeof(cmd))>0)
	{
		if(cmd=='h')
		{
			hibernate_rtc_option(hibernate_rtc_out);
		}
		else if(cmd=='v')
		{
			ask_version();
		}
		else if(cmd=='w')
		{
			warm_start();
		}
		else if(cmd=='r')
		{
			hot_start();
		}		
		else if(cmd=='c')
		{
			cold_start();
		}
		else if(cmd=='g')
		{
			read_tcxo_io_config();
		}
		else if (cmd == 'o')
		{
			hibernate_rtc_out = hibernate_rtc_out?0:1;
			printf("hibernate_rtc_out %d\n",hibernate_rtc_out);
		}
#ifdef USE_SPI_LOG		
		else if (cmd == 'l')
		{
			int log_en = spi_get_log_en();
			
			log_en = log_en?0:1;
			spi_enable_log(log_en);
		}		
#endif		
			
	}
}

static void one_time_task(void)
{
	reset_ttff();

	if(testmode4)
	{
		test_mode4(svid,test_period);
	}
	else if(hibernate_test)
	{
		hibernate_rtc_option(hibernate_rtc_out);
	}

}

static void test_result_init(void)
{
	if( has_display == 0)
		return;
		
	if(testmode4)
	{
		t_TestResult_TestMode4_init(testmode4_num_period);		
	}
	else if(chamber_mode)
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_Chamber);
	}
	else if(open_sky_mode)
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_OpenSky);
	}
	else if(fix_only_mode)
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_FixOnly);
	}			
	else if(hibernate_test)
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_HibernateOnly);
	}			
	else
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_Endless);
	}		
		
}

static void gps_try_state_change( eGPS_STATE current_state )
{

	switch(current_state)
	{
		case GPS_STATE_INIT:
			one_time_task();
			gps_set_state( GPS_STATE_RUN );
			break;

			case GPS_STATE_RUN:
				if(aQuit)
					gps_set_state( GPS_STATE_TERMINATE );
					
				read_cmd_pipe();
				
				if(hibernate_cmd_send)
				{
					gps_set_state( GPS_STATE_HIBERNATING );
				}
				break;
			case GPS_STATE_HIBERNATING:
				if(is_awake()==0)
				{
					printf("hibernate ok\n");
					gps_set_state( GPS_STATE_TERMINATE );
					aQuit =1;
				}				
				break;
			default:
				break;
		}
		
}

int main(int argc, char *argv[])
{
	eGPS_STATE current_state ;
	
	printf("GPS_TEST DATE %s TIME %s\n",__DATE__,__TIME__);
	parse_opts(argc, argv);


	struct sigaction act; 
	sigemptyset(&act.sa_mask); 
	act.sa_flags   = 0;
	act.sa_handler = SignalHandler;
	sigaction(SIGTERM, &act, NULL); 
	sigaction(SIGINT, &act, NULL);
	
	sigaction(SIGABRT, &act, NULL);
	sigaction(SIGSEGV, &act, NULL);

	if(coldstart)
	{
		cold_reset_without_tty_open();
	}
  
	tty_data_init();

	test_result_init();

	clear_mid_18_1_detected();

	system("mknod /tmp/gps_cmd p");
	
	osp_data_init();
	
  printf("create cmd pipe\n");
  if (pipe_cmd_fd < 0)
  {
        pipe_cmd_fd = open("/tmp/gps_cmd", O_RDONLY | O_NDELAY | O_NONBLOCK);

        if (pipe_cmd_fd < 0) {
            printf("PIPE: read open cmd device fail (%d)\n", pipe_cmd_fd);
            return -1;
        }
  }    
  
  if(coldstart!=1)
  {
    printf("warm/hot start\n");
  	gps_set_state( GPS_STATE_INIT );
  }
  
	tty_data_start_parse_thread();
  
  while( (current_state = gps_get_state()) != GPS_STATE_TERMINATE)
  { 
		gps_try_state_change(current_state);
		sleep(0.5);					
  }
  
	if(pipe_cmd_fd>0)
		close(pipe_cmd_fd);

	t_TestResult_deinit();
	
	unlink("/tmp/gps_cmd");
	tty_data_deinit();
	// sync log finally
	sync(); 
	return 0;
}

