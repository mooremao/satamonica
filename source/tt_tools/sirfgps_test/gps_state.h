#ifndef __GPS_STATE_H__
#define __GPS_STATE_H__

typedef enum {
	GPS_STATE_INIT,
	GPS_STATE_RUN,
	GPS_STATE_HIBERNATING,
	GPS_STATE_TERMINATE
} eGPS_STATE;

eGPS_STATE gps_get_state(void);
void gps_set_state( eGPS_STATE state);
#endif //__GPS_STATE_H__
