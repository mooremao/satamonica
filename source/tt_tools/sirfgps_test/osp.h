#ifndef __OSP_H_
#define __OSP_H_
typedef unsigned char uint8_t;
typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
/* Align on 1 byte boundary */
#pragma pack(1)
/* GSD4e type definition */
typedef enum _osp_header_t
{
	OSP_HEADER_NOTFOUND,
	OSP_HEADER_PARTIAL,
	OSP_HEADER_FULL	
} osp_header_t;

typedef struct Msg2 {                 ///< Measure Navigation Data Out
  uint8_t MsgId;
  int32_t X_Position;         // (m)
  int32_t Y_Position;         // (m)
  int32_t Z_Position;         // (m)
  int16_t X_Velocity;         // (m/s*8)
  int16_t Y_Velocity;         // (m/s*8)
  int16_t Z_Velocity;         // (m/s*8)
  uint8_t mode_1;
  uint8_t HDOP;
  uint8_t mode_2;
  uint16_t GPS_Week;
  uint32_t GPS_TOW;
  uint8_t SVs_in_Fix;
  uint8_t prn[12];            // Channel PRNs
}Msg2;

typedef struct _Msg7 {                 ///< Clock Status Data
  uint8_t  MsgId;
  uint16_t Extended_GPS_Week;
  uint32_t GPS_TOW;            // (sec*100)
  uint8_t  SVs;
  uint32_t Clock_Drift;        // (Hz)
  uint32_t Clock_Bias;         // (ns)
  uint32_t Estimated_GPS_Time; // (ms)
}Msg7;

typedef struct _Msg30 {                ///< Navigation Library SV State Data
  uint8_t  MsgId;
  uint8_t  SatelliteId;
  uint8_t  GPSTime[8];          // (double)
  uint8_t  PositionX[8];        // (double)
  uint8_t  PositionY[8];        // (double)
  uint8_t  PositionZ[8];        // (double)
  uint8_t  VelocityX[8];        // (double)
  uint8_t  VelocityY[8];        // (double)
  uint8_t  VelocityZ[8];        // (double)
  uint8_t  ClockBias[8];        // (double)
  uint8_t  ClockDrift[4];       // (single)
  uint8_t  EphemerisFlag;
  uint8_t  Reserved1[4];
  uint8_t  Reserved2[4];
  uint8_t  IonosphericDelay[4]; // (single)
}Msg30;

typedef struct _Msg41 {                ///< Geodetic Navigation Source
  uint8_t  MsgId;
  uint16_t Nav_Valid;
  uint16_t Nav_Type;
  uint16_t Extended_Week_Number;
  uint32_t TOW;				 // (sec*1000)
  uint16_t UTC_Year;
  uint8_t  UTC_Month;
  uint8_t  UTC_Day;
  uint8_t  UTC_Hour;
  uint8_t  UTC_Minute;
  uint16_t UTC_Milliseconds;
  uint32_t Satellite_ID_List;
  int32_t  Latitude;							// (deg*10^7)
  int32_t  Longitude;                           // (deg*10^7)
  int32_t  Altitude_Ellipsoid;                  // (m*100)
  int32_t  Altitude_AltitudeMSL;                // (m*100)
  uint8_t  Map_Datum;
  uint16_t Speed_Over_Ground;                   // (m/s*100)
  uint16_t Course_Over_Ground;                  // (deg*100)
  uint16_t Magnetic_Variation;
  int16_t  Climb_Rate;                          // (m/s*100)
  int16_t  Heading_Rate;                        // (deg/s*100)
  uint32_t Estimated_Horizontal_Position_Error; // (m*100)
  uint32_t Estimated_Vertical_Position_Error;   // (m*100)
  uint32_t Estimated_Time_Error;                // (sec*100)
  uint16_t Estimated_Horizontal_Velocity_Error; // (m/sec*100)
  int32_t  Clock_Bias;
  uint32_t Clock_Bias_Error;
  int32_t  Clock_Drift;
  uint32_t Clock_Drift_Error;
  uint32_t Distance;                            // (m)
  uint16_t Distance_Error;                      // (m)
  uint16_t Heading_Error;                       // (deg*100)
  uint8_t  SVs_in_Fix;
  uint8_t  HDOP;
  uint8_t  Additional_Mode_Info;
}Msg41;

typedef struct _Msg4_sat {
  uint8_t SVid;
  uint8_t Azimuth;            // (deg*2/3)
  uint8_t Elevation;          // (deg*2)
  uint16_t State;
  uint8_t CN0[10];            // (db-Hz)
}Msg4_sat;
typedef struct _Msg4 {                 ///< Measured Tracker Data Out
  uint8_t  MsgId;
  uint16_t GPS_Week;
  uint32_t GPS_TOW;             // (sec*100)
  uint8_t Chans;
  Msg4_sat sat[12];
}Msg4;

typedef struct _Msg46 {
  uint8_t  MsgId;
  uint16_t SVid;
  uint16_t Period;
  uint16_t BitSyncTime;
  uint16_t BitCount;
  uint16_t PoorStatus;
  uint16_t GoodStatus;
  uint16_t ParityErrorCount;
  uint16_t LostVCOCount;
  uint16_t FrameSyncTime;
  int16_t  CN0Mean;
  int16_t  CN0Sigma;
  int16_t  ClockDriftChange;
  int32_t  ClockDrift;
  int16_t  Bad1kHzBitCount;
  int32_t  AbsI20ms;
  int32_t  AbsQ20ms;
  uint32_t PhaseLockIndicator;
  uint16_t RTCFrequency;
  uint16_t ECLKRatio;
  uint8_t  TimerSynchInput_AGC;
  uint8_t  Reserved[3];
}Msg46;  

typedef struct __tMsg67_1_tag
{
	uint8_t  MsgId;
	uint8_t  SId;	
   uint32_t solution_validity;
   uint32_t solution_info;
   uint16_t gps_week;
   uint32_t tow;
   uint32_t tow_sub_ms;
   int16_t  time_bias;
   uint8_t  time_accuracy;
   uint8_t  time_source;
   uint16_t utc_year;
   uint8_t  utc_month;
   uint8_t  utc_day;
   uint8_t  utc_hour;
   uint8_t  utc_min;
   uint16_t utc_sec;
   uint8_t  utc_offset;
   uint8_t  datum;
   uint8_t clk_bias[8];
   uint32_t clk_bias_error;
   int32_t  clk_offset;
   uint32_t clk_offset_error;
   int32_t  lat;
   int32_t  lon;
   int32_t  alt_ellips;
   int32_t  alt_msl;
   uint16_t sog;
   uint16_t cog;
   int16_t  climb_rate;
   int16_t  heading_rate;
   uint32_t distance_travel;
   uint16_t heading_error;
   uint16_t distance_travel_error;
   uint32_t ehpe;
   uint32_t evpe;
   uint16_t ehve;
   uint8_t  gdop;
   uint8_t  pdop;
   uint8_t  hdop;
   uint8_t  vdop;
   uint8_t  tdop;
   uint8_t  num_svs_in_sol;
   uint32_t sv_list_1;          /* GPS */
   uint32_t sv_list_2;          /* SBAS */
   uint32_t sv_list_3;          /* Glonass */
   uint32_t sv_list_4;          /* QZSS */
   uint32_t reserved_2;
   uint32_t additional_info;

}  Msg67_1;  


typedef struct tSIRF_MSG_SSB_GNSS_SAT_DATA_PER_SV_tag
{
      uint16_t sat_info;
      uint16_t azimuth;
      uint16_t elevation;
      uint16_t avg_cno;
      uint32_t status;
} tSIRF_MSG_SSB_GNSS_SAT_DATA_PER_SV;

#define SIRF_GNSS_SAT_DATA_NUM_OF_SATS 15

typedef struct tMsg67_16_tag
{
	uint8_t  MsgId;
	uint8_t  SId;
   uint16_t gps_week;
   uint32_t tow;
   uint32_t tow_sub_ms;
   int16_t  time_bias;
   uint8_t  time_accuracy;
   uint8_t  time_source;
   uint8_t  msg_info;
   uint8_t  num_of_sats;
   tSIRF_MSG_SSB_GNSS_SAT_DATA_PER_SV sat[SIRF_GNSS_SAT_DATA_NUM_OF_SATS];

}  Msg67_16;

/* Back to the default */
#pragma pack()

/* Type definition */
typedef enum _ack
{
	ACK_NONE,
	ACK_GOOD,
	ACK_FAIL,
	ACK_FINISH
}ack_t;

typedef struct _ee_ack
{
	uint8_t ack_mid;
	uint8_t ack_sid;
	uint8_t ack_value;
	uint8_t ack_reason;
} ee_ack_t;


typedef struct _timestamp_t
{
	uint16_t UTC_year;
	uint8_t	 UTC_month;
	uint8_t	 UTC_day;
	uint8_t	 UTC_hour;
	uint8_t	 UTC_minute;
	uint16_t UTC_second;
} timestamp_t;

typedef struct _position_t
{
	int32_t	latitude;
	int32_t	longitude;
	uint16_t velocity;
	uint16_t head;
	int32_t altitude;
	uint16_t nav;
	uint8_t hdop;
	uint32_t ehpe;
	uint32_t evpe;
	timestamp_t	GPS_timestamp;
} position_t;

typedef struct _sat_t
{
	uint8_t	ee_age;		// Age of the extended ephemeris. Bit 0-4: SGEE age, Bit 5-7: CGEE age
	uint8_t svid[12];		// Satellite ID
	uint8_t azimuth[12];	// Azimuth
	uint8_t elevation[12];	// Elevation
	uint8_t cn0[12];		// Signal strength
	int8_t sv_status[12];	//Source of ephemeris: -1: undefined, 0:BE, 1:SGEE, 2:CGEE
} sat_t;

#define SIRF_GNSS_SAT_DATA_TOTAL_NUM_OF_SATS 30
typedef struct _gnss_sat_t
{
	uint8_t num_of_sats;
	uint8_t svid[SIRF_GNSS_SAT_DATA_TOTAL_NUM_OF_SATS];		// Satellite ID
	uint8_t azimuth[SIRF_GNSS_SAT_DATA_TOTAL_NUM_OF_SATS];	// Azimuth
	uint8_t elevation[SIRF_GNSS_SAT_DATA_TOTAL_NUM_OF_SATS];	// Elevation
	uint8_t cn0[SIRF_GNSS_SAT_DATA_TOTAL_NUM_OF_SATS];		// Signal strength
	int8_t sv_status[SIRF_GNSS_SAT_DATA_TOTAL_NUM_OF_SATS];	//Source of ephemeris: -1: undefined, 0:BE, 1:SGEE, 2:CGEE
} gnss_sat_t;


typedef enum _patch_response_t
{
	IDLE,
	GOT_INFO,
	REQUEST_PROMPT,
	ACK_PATCH_FAILED,
	ACK_PATCH_OK,
	ACK_EXIT_FAILED,
	ACK_EXIT_OK
} patch_response_t;



/* External functions */
osp_header_t header_detected(uint8_t *data, uint32_t length, uint32_t *index);
uint8_t idle_bytes_detected(uint8_t *data, uint32_t length);
uint8_t checksum_osp(uint8_t *data);
void decode_osp4 (uint8_t *osp4, sat_t *sat_info);
void decode_osp6(uint8_t *osp6, uint16_t *crc);
void decode_osp41 (uint8_t *osp41, position_t *position);
void decode_osp56 (uint8_t *osp56, ee_ack_t *ack);

void lock_spi_buffer (void);
void unlock_spi_buffer (void);
int8_t send_osp (uint8_t *data, uint32_t len);
int8_t disable_output (void);
int8_t enable_output (void);
int8_t enable_navi_debug (void);
int8_t enable_mid_output (uint8_t mid);

void decode_osp30 (uint8_t *osp30, sat_t *sat_info);
int8_t ask_version (void);
/* Initialize the SPI Mutex */
void spi_mutex_init (void);

/* For test mode 4 */
int8_t test_mode4 (uint8_t svn, uint8_t time);

int process_osp (uint8_t *osp_msg);
int8_t tcxo_io_config (int rtc_on);
int8_t io_config (void);
int8_t pool_config (void);
int8_t cold_start (void);
int8_t warm_start (void);

patch_response_t get_response (void);
void clear_response (void);
int8_t hibernate (void);
int8_t factory_reset (void);
int8_t eclm_start (void);
void enable_csr_debug (void);
void disable_csr_debug (void);
uint8_t get_csr_debug (void);
void reset_ttff (void);
void osp_data_init(void);
void kalimba_workaround (void);
int rom_patch_detected(void);
void clear_rom_patch_detected(void);
#endif //__OSP_H_
