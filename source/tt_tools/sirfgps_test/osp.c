#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <memory.h>
#include <fcntl.h>

//#include "api.h"
#include "osp.h"
#include "tty_data.h"
#include "test_result.h"
#include "gps_state.h"
/* Pre-processor definitions */

/* Type definitions */
#define GPS_LOG "/tmp/gps.log"

/* Static data definitions */
sat_t sat_info;
gnss_sat_t gnss_sat_info;
patch_response_t	response;
#define rtos_dly_wait(x) usleep(1000*x)
/* Static data declarations */
extern int k32_available;
extern int update_k32;
uint8_t csr_debug = 0;
static uint16_t ttff, time_count = 0;
uint16_t hibernate_cmd_send = 0;

static int8_t rom_version_detected = -1;
static uint8_t num_sat_count_mid_67_sid_16;
static uint8_t mid_18_1_detected;         
extern int aQuit;              
/* Static function definitions */
/* function to convert data */
static uint16_t convert_uint16(uint16_t v)
{
	return ((v & 0xff) << 8) | (v >> 8);
}

static uint32_t convert_uint32 (uint32_t v)
{
  return 0
    | ((v & 0x000000ff) << 24)
    | ((v & 0x0000ff00) <<  8)
    | ((v & 0x00ff0000) >>  8)
    |   v               >> 24;
}

static int16_t convert_int16 (int16_t v)
{
  return 0
    | ((v & 0xff) << 8)
    | ((v >> 8) & 0xff);
}

static int32_t convert_int32 (int32_t v)
{
  return 0
    | ((v & 0x000000ff) << 24)
    | ((v & 0x0000ff00) <<  8)
    | ((v & 0x00ff0000) >>  8)
    | ((v               >> 24) & 0xff);
}

/* External function definitions */
osp_header_t header_detected(uint8_t *data, uint32_t length, uint32_t *index)
{
	uint32_t i;
	uint8_t byte1;
	uint8_t byte2;

	for (i = 0; i < length-1; i ++)
	{
		byte1 = data[i];
		byte2 = data[i+1];
		if ((byte1 == 0xA0) && (byte2 == 0xA2))
		{
			*index = i;
			return OSP_HEADER_FULL;
		}
	}
	
	if(byte2 == 0xA0)
	{
		// Need to request more data
		return OSP_HEADER_PARTIAL;
	}
		
	return OSP_HEADER_NOTFOUND;	
}
uint8_t idle_bytes_detected(uint8_t *data, uint32_t length)
{
	uint32_t i;

	for (i = 0; i < length; i ++)
	{
		uint8_t byte1 = data[i];
		uint8_t byte2 = data[i+1];
		if ((byte1 == 0xA7) && (byte2 == 0xB4))
			return 1;
	}
	/* Return 0 if no header detected */
	return 0;	
}



uint8_t checksum_osp(uint8_t *data)
{
	uint16_t checksum, index, msg_checksum;
	uint32_t length;
	length = (data[2] << 8) | data[3];
	msg_checksum = (data[length+4] << 8) | data[length+5];
	checksum = 0;
	index = 4;
	while(index < length+4)
	{
		checksum = checksum + (uint16_t) data[index];
		checksum = checksum & 0x7FFF;
		index++;
	}
	if (checksum == msg_checksum)
		return 1;
	else
		return 0;
}


void decode_osp4 (uint8_t *osp4, sat_t *sat_info)
{
	uint8_t svid[12], azimuth[12], elevation[12], cn0[12];
	uint8_t i, k;
	/* Get data from MID 4 */
	Msg4 *msg4 = (Msg4 *)&osp4[4];
	
	for (i=0; i < 12; i ++)
	{
		uint16_t cn0_temp = 0;
		svid[i] = msg4->sat[i].SVid;
		azimuth[i] = msg4->sat[i].Azimuth;
		elevation[i] = msg4->sat[i].Elevation;
		for (k=0; k < 10; k ++)
		{
			cn0_temp += (uint16_t) (msg4->sat[i].CN0[k]);
		}
		cn0[i] = (uint8_t) (cn0_temp/10);
	}
	/* Assign data to sat_info */
	/* Set sattellite with zero cn0 tobe undefined status */
	for (i=0; i < 12; i ++)
	{
		sat_info->svid[i] = svid[i];
		sat_info->azimuth[i] = azimuth[i];
		sat_info->elevation[i] = elevation[i];
		sat_info->cn0[i] = cn0[i];
		if (cn0[i] == 0) sat_info->sv_status[i] = -1;
	}	
//	printf("MID 4 cn0[0] %d\n",cn0[0]);
}

//ret: decode finish all SV
static uint8_t decode_osp67_16 (uint8_t *osp4, gnss_sat_t *sat_info)
{
	uint8_t svid[SIRF_GNSS_SAT_DATA_NUM_OF_SATS], azimuth[SIRF_GNSS_SAT_DATA_NUM_OF_SATS], elevation[SIRF_GNSS_SAT_DATA_NUM_OF_SATS], cn0[SIRF_GNSS_SAT_DATA_NUM_OF_SATS];
	uint16_t nav_system;
	uint8_t i;
	uint8_t ret = 0;

	/* Get data from MID 4 */
	Msg67_16 *msg67_16 = (Msg67_16 *)&osp4[4];
	sat_info->num_of_sats = msg67_16->num_of_sats;
	if( (msg67_16->msg_info&0xf)==1 )
		num_sat_count_mid_67_sid_16 = 0;
	
	if( (msg67_16->msg_info&0xf) == ((msg67_16->msg_info&0xf0)>>4) )
	{
		//decode finish all SV
		ret = 1;
	}
/*	for (i=4;i<24;i++)
		printf("osp[%d]=0x%x ", i, osp4[i]);
	printf("\n");
	*/
//	printf("sat_info->num_of_sats %d\n",sat_info->num_of_sats);
	for (i=0; i < SIRF_GNSS_SAT_DATA_NUM_OF_SATS; i ++)
	{
		nav_system = ( (convert_uint16(msg67_16->sat[i].sat_info))>>13)&0x7;
		
		svid[i] = convert_uint16(msg67_16->sat[i].sat_info)&0xff;
		cn0[i] = (float)((convert_uint16(msg67_16->sat[i].avg_cno))/10);
	//	printf("svid[i] %d cn0[i] %d\n",svid[i],cn0[i]);
	//	printf("nav_system %d msg67_16->sat[i].sat_info 0x%x svid[i] %d \n",nav_system,msg67_16->sat[i].sat_info,svid[i]);
		if(nav_system == 2 ) //GLONESS
		{
			svid[i]+=64;
		}
		
		azimuth[i] = convert_uint16(msg67_16->sat[i].azimuth);
		elevation[i] = convert_uint16(msg67_16->sat[i].elevation);
		
	}
	/* Assign data to sat_info */
	/* Set sattellite with zero cn0 tobe undefined status */
	for (i=0; i < SIRF_GNSS_SAT_DATA_NUM_OF_SATS; i ++)
	{
		if(cn0[i] >0 && svid[i]!=64) // GLO SVID 0 is not ok. Means GLO SVID is not decoded yet.
		{
			sat_info->svid[num_sat_count_mid_67_sid_16] = svid[i];
			sat_info->azimuth[num_sat_count_mid_67_sid_16] = azimuth[i];
			sat_info->elevation[num_sat_count_mid_67_sid_16] = elevation[i];
			sat_info->cn0[num_sat_count_mid_67_sid_16] = cn0[i];
			if(num_sat_count_mid_67_sid_16<SIRF_GNSS_SAT_DATA_TOTAL_NUM_OF_SATS )
				num_sat_count_mid_67_sid_16 ++;
			else
				printf("WARNING:num_sat_count_mid_67_sid_16 %d\n",num_sat_count_mid_67_sid_16);
	  }
		// if (cn0[i] == 0) sat_info->sv_status[num_sat_count] = -1;	
	}
	return ret;
}

static void decode_osp67_1 (uint8_t *osp4, position_t *position)
{
	uint16_t UTC_year16, UTC_second16;
	uint8_t UTC_month8, UTC_day8, UTC_hour8, UTC_minute8;
	uint32_t solution_info;
		
	/* Get data from MID 4 */
	Msg67_1 *msg67 = (Msg67_1 *)&osp4[4];
	
	UTC_year16 = convert_uint16(msg67->utc_year);
	UTC_month8 = msg67->utc_month;
	UTC_day8 = msg67->utc_day;
	UTC_hour8 = msg67->utc_hour;
	UTC_minute8 = msg67->utc_min;
	UTC_second16 = convert_uint16(msg67->utc_sec);
		
	solution_info = convert_uint32(msg67->solution_info);
	if( (solution_info &0x7)!=0)
	{
		position->latitude = convert_int32(msg67->lat);
		position->longitude = convert_int32(msg67->lon);
	}
	else
	{
		position->latitude = 0;
		position->longitude = 0;
	}	
	
	position->GPS_timestamp.UTC_year = UTC_year16;
	position->GPS_timestamp.UTC_month = UTC_month8;
	position->GPS_timestamp.UTC_day = UTC_day8;
	position->GPS_timestamp.UTC_hour = UTC_hour8;
	position->GPS_timestamp.UTC_minute = UTC_minute8;
	position->GPS_timestamp.UTC_second = UTC_second16;	
}

/* MSG ID 6 is different with the others, it is encoded by ascii character */
/* We expect to get Custormer Version ID rather than Sirf Version ID */
void decode_osp6(uint8_t *osp6, uint16_t *crc)
{
	uint8_t sirf_version[81];
	uint8_t sirf_length;
	sirf_length = osp6[5];
	
	memset(sirf_version, 0x00, sizeof(sirf_version));
	/* Copy SiRF version into string */
	memcpy(sirf_version, &osp6[6], sirf_length);
	sirf_version[sirf_length] = '\0';
	
	
	printf("\nVersion = %s - length = %d\n", sirf_version, sirf_length);
	if(strstr(sirf_version, "5.5.2-")!=0)
		rom_version_detected = 1;
	else
		rom_version_detected = 0;
}

int rom_patch_detected(void)
{
	return rom_version_detected;
}

void clear_rom_patch_detected(void)
{
	rom_version_detected = -1;
}


void decode_osp30 (uint8_t *osp30, sat_t *sat_info)
{
	uint8_t i;
	Msg30 *msg30 = (Msg30 *)&osp30[4];
	uint8_t svid = msg30->SatelliteId;
	uint8_t ephemeris = msg30->EphemerisFlag;
	uint8_t ee_flag = ephemeris & 0xF0;
	uint8_t ee_age = ephemeris & 0x0F;
	
	/* Update to satellite info */
	for (i = 0; i < 12; i ++)
	{
		if (sat_info->svid[i] == svid)
		{
			switch (ee_flag)
			{
			case 0x00: 	// BE
				sat_info->sv_status[i] = 0;
				break;
				
			case 0x10:	// SGEE from 1 to 15 days
				sat_info->sv_status[i] = 1;
				sat_info->ee_age = (sat_info->ee_age & 0xE0) | ee_age;
				break;
				
			case 0x20:	// CGEE from 1 to 3 days
				sat_info->sv_status[i] = 2;
				sat_info->ee_age = (sat_info->ee_age & 0x1F) | (ee_age << 5);
				break;
				
			case 0x50:	// SGEE from 16 to 31 days
				sat_info->sv_status[i] = 1;
				ee_age += 16;
				sat_info->ee_age = (sat_info->ee_age & 0xE0) | ee_age;
				break;
				
			default:	// undefined
				sat_info->sv_status[i] = -1;
				break;				
			}			
		}
	}
	//printf ("ephemeris state %d = %2x\n", svid, ephemeris);
}

void decode_osp41 (uint8_t *osp41, position_t *position)
{	
	int32_t lat32, lon32, alt32;
	uint32_t ehpe32, evpe32;
	uint16_t sog16, cog16, nav16, UTC_year16, UTC_second16;
	uint8_t hdop8, UTC_month8, UTC_day8, UTC_hour8, UTC_minute8;
	/* Get data from MID 41 */
	Msg41 *msg41 = (Msg41 *)&osp41[4];
	
	UTC_year16 = convert_uint16(msg41->UTC_Year);
	UTC_month8 = msg41->UTC_Month;
	UTC_day8 = msg41->UTC_Day;
	UTC_hour8 = msg41->UTC_Hour;
	UTC_minute8 = msg41->UTC_Minute;
	UTC_second16 = convert_uint16(msg41->UTC_Milliseconds);

	lat32 = convert_int32(msg41->Latitude);
	lon32 = convert_int32(msg41->Longitude);
	alt32 = convert_int32(msg41->Altitude_Ellipsoid);
	ehpe32 = convert_uint32(msg41->Estimated_Horizontal_Position_Error);
	evpe32 = convert_uint32(msg41->Estimated_Vertical_Position_Error);
	sog16 = convert_uint16(msg41->Speed_Over_Ground);
	cog16 = convert_uint16(msg41->Course_Over_Ground);
	nav16 = convert_uint16(msg41->Nav_Type);
	hdop8 = msg41->HDOP;
	
	/* Assign to position */
	position->GPS_timestamp.UTC_year = UTC_year16;
	position->GPS_timestamp.UTC_month = UTC_month8;
	position->GPS_timestamp.UTC_day = UTC_day8;
	position->GPS_timestamp.UTC_hour = UTC_hour8;
	position->GPS_timestamp.UTC_minute = UTC_minute8;
	position->GPS_timestamp.UTC_second = UTC_second16;

	position->latitude = lat32;
	position->longitude = lon32;
	position->velocity = sog16;
	position->head = cog16;
	position->altitude = alt32;
	position->nav = nav16;
	position->hdop = hdop8;
	position->ehpe = ehpe32;
	position->evpe = evpe32;
	//printf("lat=%d, nav=%d\n",lat32, nav16);
}

static void decode_osp46 (uint8_t *osp46, test_mode4_t *test4)
{	
	int16_t cn0_mean;
	int16_t cn0_sigma;
	int16_t clock_drift_change;
	int32_t clock_drift;
	int32_t phase_lock;

	/* Get data from MID 46 */
	Msg46 *msg46 = (Msg46 *)&osp46[4];
	
	cn0_mean = convert_int16(msg46->CN0Mean);
	cn0_sigma = convert_int16(msg46->CN0Sigma);
	clock_drift_change = convert_int16(msg46->ClockDriftChange);
	clock_drift = convert_int32(msg46->ClockDrift);
	phase_lock = convert_int32(msg46->PhaseLockIndicator);
	
	/* With scaling factor */
	test4->cn0_mean = (float) cn0_mean/10.0f;
	test4->cn0_sigma = (float) cn0_sigma/10.0f;
	test4->clock_drift_change = (float) clock_drift_change/10.0f;
	test4->clock_drift = (float) clock_drift/10.0f;
	test4->phase_lock = (float) phase_lock * 0.001f;
	test4->bit_sync_time =  convert_uint16(msg46->BitSyncTime);
	test4->AbsI20ms =  convert_int32(msg46->AbsI20ms);
	test4->AbsQ20ms =  convert_int32(msg46->AbsQ20ms);
	test4->RTCFrequency = convert_uint16(msg46->RTCFrequency);
		
}

void decode_osp56 (uint8_t *osp56, ee_ack_t *ack)
{
	ack->ack_mid 	= 	osp56[6];
	ack->ack_sid 	= 	osp56[7];
	ack->ack_value 	= 	osp56[8];
	ack->ack_reason 	= 	osp56[9];
	return;
}

static uint16_t gen_checksum_osp(uint8_t *data, uint32_t length)
{
	uint16_t checksum, index;
	checksum = 0;
	index = 0;
	while(index < length)
	{
		checksum = checksum + (uint16_t) data[index];
		checksum = checksum & 0x7FFF;
		index++;
	}
	return checksum; 
}

/* Initialize the SPI Mutex */


int8_t send_osp (uint8_t *data, uint32_t len)
{	
	uint16_t checksum = 0;
	static uint8_t buffer[1024]; 	// maximum that could send through OSP


	
	buffer[0] = 0xA0;
	buffer[1] = 0xA2;
	buffer[2] = (len & 0xFF00) >> 8;
	buffer[3] = len & 0xFF;

	memcpy(&buffer[4], data, len);
	checksum = gen_checksum_osp(data, len);
	buffer[len+4] = (checksum & 0xFF00) >> 8;
	buffer[len+5] = checksum & 0xFF;
	buffer[len+6] = 0xB0;
	buffer[len+7] = 0xB3;
	
	/* Add 4 idle bytes end of the message */
	buffer[len+8] = 0xA7; 
	buffer[len+9] = 0xB4; 
	buffer[len+10] = 0xA7; 
	buffer[len+11] = 0xB4; 
	/* Get Mutex before sending */
	//if (rtos_mut_wait(spi_mutex, 500) != OS_R_TMO)
	{	
		/* Send buffer with idle bytes */
		tty_data_send(buffer, len+12);

		/* If the idle bytes are not included in transmit() */
		/* function, a delay need to be added */
		rtos_dly_wait (20);
		
		//rtos_mut_release(spi_mutex);
		return 0;
	}
	
}

/* Disable all output */
int8_t disable_output (void)
{
	uint8_t data[] = {0xA6, 0x02, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00};
	int8_t status = send_osp (data, 8);
	
	if (status != -1)	printf ("OUTPUT DISABLED\n");
	
	return status;
}

/* Enable all output */
int8_t enable_output (void)
{
	uint8_t data[] = {0xA6, 0x02, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00};
	int8_t status = send_osp (data, 8);
	
	if (status != -1)	printf ("OUTPUT ENABLED\n");
	
	return status;
}
/* Enable navigation debug messages: MID 7, 28, 29, 30, 31 */
int8_t enable_navi_debug (void)
{
	uint8_t data[] = {0xA6, 0x05, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00};
	int8_t status = send_osp (data, 8);
	
	if (status != -1)	printf ("NAVIGATION DEBUG MSG ENABLED\n");
	
	return status;
}

int8_t enable_mid_output (uint8_t mid)
{
	uint8_t data[] = {0xA6, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00};
	int8_t status;
	
	data[2] = mid;

	status = send_osp (data, 8);
	
	if (status != -1)	printf ("OUTPUT %d ENABLED\n", mid);
	
	return status;
}


int8_t disable_mid_output (uint8_t mid)
{
	uint8_t data[] = {0xA6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	int8_t status;
	
	data[2] = mid;

	status = send_osp (data, 8);
	
	if (status != -1)	printf ("OUTPUT %d DISABLED\n", mid);
	
	return status;
}

int8_t ask_version (void)
{
	uint8_t version[] = {0x84, 0x00};

	int8_t status = send_osp (version, 2);
	
	if (status != -1) printf ("ASK VERSION\n");
	
	return status; 
}

/* Enable test mode 4, maxzimum is 255 seconds*/
int8_t test_mode4 (uint8_t svn, uint8_t time)
{
	uint8_t data[] = {
					0x96,			// mid 150
					0x1E, 0x54,		// Test mode 4
					0x00, 0x0B,		// SV number
					0x00, 0x14,		// Time for data aquisition
					0x00};
	int8_t status;
	
	data[4] = svn;
	data[6] = time;

	status = send_osp (data, sizeof (data));
	
	if (status != -1)	printf ("\nTEST MODE 4 enabled on SVN = %d, period = %d seconds\n",svn, time);
	
	return status;
}

patch_response_t get_response (void)
{
	return response;
}
void clear_response (void)
{
	response = IDLE;
}

static void print_to_tmp_APTS(char *msg)
{
	FILE * fp = fopen(GPS_LOG,"a+");
	
	if( fp!=NULL )
	{
		fprintf(fp,msg);
		fclose(fp);
	}
}

/* process_osp() becomes external function */
int process_osp (uint8_t *osp_msg)
{
	uint8_t checksumed;
	uint32_t i;
	position_t	position;	
	uint8_t mid;
	uint8_t sid; 
	uint16_t osp_length;
	char msg[512];
	
	checksumed = checksum_osp(osp_msg);
	mid = osp_msg[4];
	sid = osp_msg[5];
	
	osp_length = (osp_msg[2] << 8) | osp_msg[3];
	
	if (csr_debug)
	{
		//for (i=0; i < osp_length+8; i ++)	printf ("%c", osp_msg[i]);
		for (i=0; i < osp_length+8; i ++)	printf ("%02X ", osp_msg[i]);
		printf ("\n");
	}
	
	if(checksumed)	
	{
		if (!csr_debug)
		{
			//if (mid==255 || mid==225 || mid==68 || mid==64 || mid==30 || mid==28) printf("MID %d:\t", mid);	
			//else printf("\tMID %d ok\n", mid);	
		}

		switch (mid)
		{
			case 67:
				{
					switch(sid)
					{
						case 1:
							decode_osp67_1(osp_msg, &position);
							time_count ++;
							if (position.latitude == 0)			
							{
								ttff ++;
								//printf ("Linking for %d seconds\n", time_count);
								sprintf(msg,"Linking for %u seconds\n",time_count);
								printf(msg);
								print_to_tmp_APTS(msg);
							}
							else if(gps_get_state() == GPS_STATE_RUN)
							{
								sprintf (msg,"pos = (%d,%d), TTFF %u\n",position.latitude, position.longitude, ttff);
								printf(msg);
								print_to_tmp_APTS(msg);
								//send_msg_to_UI(msg,strlen(msg)+1);
								sprintf (msg,"date %d/%d/%d time %d:%d:%4.2f \n"
			   								, position.GPS_timestamp.UTC_year
			   									,position.GPS_timestamp.UTC_month
			  								 	,position.GPS_timestamp.UTC_day
			  								 ,position.GPS_timestamp.UTC_hour
			  								 ,position.GPS_timestamp.UTC_minute
			  								 ,(float) (position.GPS_timestamp.UTC_second)/1000);		
								printf(msg);
								print_to_tmp_APTS(msg);			  								 												
							}
							if(gps_get_state() == GPS_STATE_RUN)
							{
								t_TestResult_MultiCH_UpdateFixInfo(time_count,position.latitude == 0?0:1,ttff,position.latitude,position.longitude);
								aQuit = t_TestResult_MultiCH_TestCanExit();
							}
#if 1							
							sprintf (msg,"SAT_INFO: ");
							printf(msg);
							print_to_tmp_APTS(msg);								
							for (i=0; i < num_sat_count_mid_67_sid_16; i ++){
								 	sprintf (msg,"(%d,%d) ", gnss_sat_info.svid[i], gnss_sat_info.cn0[i]);
								 	printf(msg);
									print_to_tmp_APTS(msg);
								}
								sprintf (msg,"\n");
								printf(msg);
							  print_to_tmp_APTS(msg);								
#endif				
											
							break;
						case 16:
							if(gps_get_state() == GPS_STATE_RUN && decode_osp67_16(osp_msg, &gnss_sat_info))
							{
									t_TestResult_MultiCH_CleartCurrentSVStatus();
									for (i=0; i < num_sat_count_mid_67_sid_16; i ++)										
										t_TestResult_MultiCH_SVStatus(gnss_sat_info.svid[i],gnss_sat_info.cn0[i]);
							}
							// printf("gnss_sat_info.num_of_sats %d\n",gnss_sat_info.num_of_sats);
							break;
						default:
							break;
					}						
				}
				break;
	    case 18:
	    	{
	    		printf ("mid 18 %d\n",sid);
	    		if(sid == 1)
	    			mid_18_1_detected = 1;
	    	 break;
	      }
			case 9:
				break;
			
			case 4:
			{			
				decode_osp4 (osp_msg, &sat_info);		
				break;
			}	
			case 6:
			{
				uint16_t crc = 0;
				decode_osp6 (osp_msg, &crc);
				break;
			}
			
			case 30:
			{				
				decode_osp30 (osp_msg, &sat_info);	
				break;
			}	
			case 41:
			{		
#if 0						
				uint32_t i;
				
				/* Increase TTFF */								
				
				decode_osp41(osp_msg, &position);	
			  	printf ("date %d/%d/%d time %d:%d:%4.2f \n"
			   	, position.GPS_timestamp.UTC_year
			   	,position.GPS_timestamp.UTC_month
			   	,position.GPS_timestamp.UTC_day
			  	 ,position.GPS_timestamp.UTC_hour
			  	 ,position.GPS_timestamp.UTC_minute
			  	 ,(float) (position.GPS_timestamp.UTC_second)/1000);				
				if (position.latitude == 0)			
				{
					ttff ++;
					printf ("Linking for %d seconds\n", ttff);
					sprintf(msg,"%d sec",ttff);
				}
				else
				{
					printf ("pos = (%d,%d), TTFF %d\n",position.latitude, position.longitude, ttff);
					sprintf(msg,"%d sec",ttff);		
				}
				printf ("SAT_INFO: ");
				for (i=0; i < 12; i ++) 	printf ("(%d,%d,%d) ", sat_info.svid[i], sat_info.sv_status[i], sat_info.cn0[i]);
				printf ("\n");
#endif										  
				break;
			}
			case 46:
			{
				test_mode4_t test4;
				decode_osp46 (osp_msg, &test4);
			
				if(gps_get_state() == GPS_STATE_RUN)
				{
					t_TestResult_TestMode4_UpdateResult(&test4);
					aQuit = t_TestResult_TestMode4_TestCanExit();
				}				
				break;
			}
			
			case 56:
				{
					//uint8_t i;
					if (!csr_debug)
					{
						//for (i=0; i < osp_length+8; i ++)	printf ("%02x ", osp_msg[i]);
						//printf ("\n");
					}
					//travis handle_eclm (osp_msg, osp_length, 0);
				}
				break;
				
			case 75:
				{
					if (!csr_debug)
					{
						uint8_t i;
						for (i=0; i < osp_length+8; i ++)	printf ("%02x ", osp_msg[i]);
						printf ("\n");
					}
				}
				break;
				
			case 64:
				break;
			case 93:
				break;
			case 178:
			{	
				/* Decode SID 10 to get current configuration */
				if (sid == 10)
				{
					if (!csr_debug)
					{
						uint8_t i;
						printf ("New TCXO freq: %2x %2x %2x %2x\n", osp_msg[6], osp_msg[7], osp_msg[8], osp_msg[9]);
						for (i=0; i < osp_length; i ++)	printf ("%02x ", osp_msg[i]);
						printf ("\n");
					}
				}
			}
			case 225:
				{
					//uint8_t i;
					//for (i=6; i < osp_length + 4; i ++) printf ("%c", osp_msg[i] ^ 0xff);
					//printf ("\n");
				}
				break;
			
			case 255:
				{
					//uint8_t i;					
					//for (i=4; i < osp_length + 4; i ++) printf ("%c", osp_msg[i]);
					//printf ("\n");
				}
				break;

			default:
				break;
		}	
	}
	else 	
	{
		printf("\tMID %d - Failed\n",mid);
#ifdef USE_SPI_LOG		
		//spi_enable_log(1);
#endif		
	}	
	
	if(checksumed)
		return 0;
	else
		return 1;
}

int8_t read_tcxo_io_config (void)
{
	uint8_t buf[] = {
          0xb2, 0x09,               // MID 178, SubID 9
         
        };
	
	int8_t status = send_osp (buf, sizeof(buf)); 
	
	if (status != -1)	printf("READ TCXO_IO CONFIGURATION SENT\n");	
	
	return status;
}


int8_t tcxo_io_config (int rtc_on)
{
	uint8_t buf[] = {
          0xb2, 0x46,               // MID 178, SubID 70
          //0x00, 0xf9, 0xc5, 0x68,   // TXCO ref clk freq 16.369
					0x01, 0x8C, 0xBA, 0x80,		// TCXO ref clk frq 26MHz
          0x03, 0xff,               // ref start up delay
          0x00, 0x00, 0x0b, 0xb8,   // ref initial uncertainty, def as 3000
          0x00, 0x01, 0x77, 0xfa,   // ref initial offset
          0x01,                     // use external lna i/o pin
          0x01,                     // configuration enable
		  
          0x00, 0x00,               // GPIO0 config
          0x00, 0x00,               // GPIO1 config	 
					0x01, 0x43,               // GPIO2 config (Time Mark)
          0x00, 0x00,               // GPIO3 config	(Data ready)
          0x00, 0x00,               // GPIO4 config
   //       0x00, 0xf3,               // GPIO5 config (LNA ENABLE)		
   0x01, 0x41,               // GPIO5 config (LNA ENABLE)	  
          0x00, 0x00,               // GPIO6 config (SPI_CLK)
          0x00, 0x00,               // GPIO7 config (SPI_CS)
		//			0x00, 0xf1,								// GPIO8 config - tested in EVK 0x00, 0x31
			    0x00, 0x03,								// GPIO8 config - tested in EVK 0x00, 0x31
          0x00, 0x00,               // RX config	(SPI_DI)
          0x00, 0x00,               // TX config	(SPI_DO)
					0x00, 0x00,		// New config
					0x00, 0x00, 	// New config
					0x00, 0x00, 	// New config
		  
          0x00,                     // UART wake up max preamble
          0x00,                     // UART idle byte wake up delay
          0x00, 0x01, 0xc2, 0x00,   // UART baud rate (115200)
          0x00,                     // UART flow control --> default is disable
          0x00, 0x62,               // i2c master address
          0x00, 0x60,               // i2c slave address
          0x01,                     // i2c rate
          0x00,                     // i2c mode --> default is multi master mode
          0x01, 0xf4,               // i2c max length
					0x2A,					// New: power control on/off
					0x00,					// New: reserved
					0x00, 				// New: reserved
					0x02, 				// New: voltage control
					0x00, 				// New: power control
					0x00, 0x00,		// New: waiting control
					0x00,					// New: temperator sensor control
					0x00,					// New: reserved
					0x00, 0x06,		// New: pin functions, set to gpio5 to GPS_ON
         
        };
	
	if(rtc_on)
		buf[35] = 0x1B;
		
	int8_t status = send_osp (buf, sizeof(buf)); 
	
	if (status != -1)	printf("TCXO_IO CONFIGURATION rtc_on: %d SENT\n",rtc_on);	
	
	return status;
}

int8_t io_config (void)
{
	uint8_t buf[] = {
          0xb2, 0x02,               // MID 178, SubID 2
          0x00, 0xf9, 0xc5, 0x68,   // TXCO ref clk freq
          0x03, 0xff,               // ref start up delay
          0x00, 0x00, 0x0b, 0xb8,   // ref initial uncertainty
          0x00, 0x01, 0x77, 0xfa,   // ref initial offset
          0x01,                     // use external lna i/o pin
          0x01,                     // configuration enable
		  
          0x00, 0x00,               // GPIO0 config
          0x00, 0x00,               // GPIO1 config	  
					0x00, 0x7d,               // GPIO2 config (TM enabled)		  
          0x00, 0x3e,               // GPIO3 config	(Data ready)
          0x00, 0x00,               // GPIO4 config
          0x00, 0x3f,               // GPIO5 config (LNA ENABLE)
					//0x00, 0x3c,								// TIME MARK
					//0x07, 0xfd,               // GPIO5 config (MSG WAITING)
		  
          0x00, 0x7d,               // GPIO6 config (SPI_CLK)
          0x00, 0x7d,               // GPIO7 config (SPI_CS)
          0x00, 0x7d,               // RX config	(SPI_DI)
          0x00, 0x3d,               // TX config	(SPI_DO)
          0x0f, 0xfd,               // GPIO8 config ROM 2.2 only
		  
          0x00,                     // UART wake up max preamble
          0x00,                     // UART idle byte wake up delay
          0x00, 0x01, 0xc2, 0x00,   // UART baud rate (115200)
          0x00,                     // UART flow control
          0x00, 0x62,               // i2c master address
          0x00, 0x60,               // i2c slave address
          0x01,                     // i2c rate
          0x01,                     // i2c mode
          0x01, 0xf4,               // i2c max length
          0x00,                     // power control
          0x00,                     // power supply
        };
	
	int8_t status = send_osp (buf, sizeof(buf)); 
	
	if (status != -1)	printf("IO CONFIG SENT\n");	

	return status;
}

int8_t pool_config (void)
{
	uint8_t buf [] = {0xB2, 0x09};
	int8_t status = send_osp (buf, sizeof(buf)); 
	return status;
}

/* Cold start */
int8_t cold_start (void)
{
	uint8_t buf[] = {0x80,0x00,0x00,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 \
					,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x0C ,0x04};
	int8_t status = send_osp (buf, sizeof(buf));
	
	if (status != -1)	printf("COLD START SENT\n");	
	reset_ttff();
	return status; 
}

/* Warm start only, no initialized info */
int8_t warm_start (void)
{
	uint8_t buf[25];
	int8_t status;
	
	buf[0] = 0x80;
	
	buf[23] = 12;
	buf[24] = 0x12;

	status = send_osp (buf, sizeof(buf));

	if (status != -1)	printf("WARM START NO INIT\n");	
	
	reset_ttff();
	return status; 
}

/* Hot start only, no initialized info */
int8_t hot_start (void)
{
	uint8_t buf[25];
	int8_t status;
	
	memset(buf, 0x00, sizeof(buf));
	buf[0] = 0x80;
	
	buf[23] = 12;
	buf[24] = 0x00;

	status = send_osp (buf, sizeof(buf));
	if (status != -1)	printf("HOT START NO INIT\n");	
	
	reset_ttff();
	
	return status; 
}

/* factory reset GSD4e */
int8_t factory_reset (void)
{
	uint8_t buf[] = {0x80,0x00,0x00,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 \
					,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x0C ,0x38};
	int8_t status = send_osp (buf, sizeof(buf));
	
	if (status != -1)	printf("FACTORY RESET SENT\n");	
	
	return status; 
}

/* Hibernate GSD4e, extremely low power consumption */
int8_t hibernate (void)
{
	//uint8_t buffer[] = {0xCD, 0x10, 0x00};
	uint8_t buffer[] = {0xCD, 0x10};
	int8_t status = 0; 
	
	do
	{
		status = send_osp (buffer, sizeof (buffer)); 
	
		if (status != -1)	printf("HIBERNATE COMMAND SENT\n");	
		else printf ("HIBERNATE COMMAND ERROR !!!\n");
		
		//rtos_dly_wait (200);
	}
	/* Confirm power state to make sure it is really hibernated */
	//while (Gps_Powered());
	while (0);
	
	hibernate_cmd_send = 1;
	return status;
}


void enable_csr_debug (void)
{
	csr_debug = 1;
}
void disable_csr_debug (void)
{
	csr_debug = 0;
}
uint8_t get_csr_debug (void)
{
	return csr_debug;
}
void reset_ttff (void)
{
	ttff = 0;
	time_count = 0;
}

void osp_data_init(void)
{
	memset(&sat_info, 0x00, sizeof(sat_info));
	memset(&gnss_sat_info, 0x00, sizeof(gnss_sat_info));
	unlink(GPS_LOG);
}

uint8_t get_mid_18_1_detected(void)
{
	return mid_18_1_detected;
}
void clear_mid_18_1_detected(void)
{
	 mid_18_1_detected = 0;
}
