#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "lcm_api.h"

static int use_fb = 0;

int lcm_client_open(void)
{
	int ret;
	
	ret =  lcm_api_open_fb();
	
	if(ret < 0)
		use_fb = 0;
	else
		use_fb = 1;
		
	return ret;
}


int lcm_client_clear_all(void)
{
	if(!use_fb)
		return;
		
	(void ) lcm_api_clear_all();
}

int lcm_client_draw_text(char * text, unsigned char cursor_pos_x, unsigned char cursor_pos_y)
{
	if(!use_fb)
		return;
			
 (void) lcm_api_draw_text(text, cursor_pos_x, cursor_pos_y );
 return 0;
}


int lcm_client_clear_text(unsigned char cursor_pos_x, unsigned char cursor_pos_y, unsigned char num_chars)
{
	if(!use_fb)
		return;
			
	(void) lcm_api_clear_text( cursor_pos_x, cursor_pos_y, num_chars);
	
	return 0;
}

void lcm_client_sync_fb(void)
{
	if(!use_fb)
		return;
			
	lcm_api_sync_fb();
}

void lcm_client_close(void)
{
	if(!use_fb)
		return;
		
	lcm_api_close_fb();
}
