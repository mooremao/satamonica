#ifndef TTY_DATA_H_
#define TTY_DATA_H_

int tty_data_init(void);
void tty_data_deinit(void);
void tty_data_send(uint8_t *buffer, uint32_t size);
void tty_data_start_parse_thread(void);
void tty_data_join_parse_thread(void);

#endif /* TTY_DATA_H_ */
