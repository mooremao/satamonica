#ifndef __LCM_API_H_
#define __LCM_API_H_

int lcm_api_open_fb(void);
void lcm_api_close_fb(void);
void lcm_api_clear_all(void);
void lcm_api_draw_text(char *str, uint8_t cursor_pos_x, uint8_t cursor_pos_y );
void lcm_api_clear_text(uint8_t cursor_pos_x, uint8_t cursor_pos_y, uint8_t num_char );
void lcm_api_sync_fb(void);
#endif //__LCM_API_H_