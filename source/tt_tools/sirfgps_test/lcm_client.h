#ifndef __LCM_CLIENT_H_
#define __LCM_CLIENT_H_

int lcm_client_init(void);

int lcm_client_clear_all(void);

void lcm_client_close(void);

int lcm_client_clear_text(unsigned char cursor_pos_x, unsigned char cursor_pos_y, unsigned char num_chars);

int lcm_client_draw_text(char * text, unsigned char cursor_pos_x, unsigned char cursor_pos_y);

int lcm_client_terminate_server(void);

#endif //__LCM_CLIENT_H_