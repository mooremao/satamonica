#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include "tty_data.h"
#include "osp.h"

pthread_t parse_pthread_handle;

int tty_fd = -1;
extern int aQuit;

static int readData(unsigned char* pData, int DataLen) {
	int bRetCode = 0;
	int nReadOut = 0;
	int read_len;

	fd_set rfds;
	struct timeval tv;
	int retval;
	int i;

	while (nReadOut < DataLen && aQuit == 0 && tty_fd != -1) {
		FD_ZERO(&rfds);
		FD_SET(tty_fd, &rfds);
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		retval = select(tty_fd + 1, &rfds, NULL, NULL, &tv);
		if (retval == -1) {
			if(errno != EBADF) {
				printf("[%s] select() error!!\r\n", __FUNCTION__);
			}
			bRetCode = 0;
		} else {
			if (retval && FD_ISSET(tty_fd, &rfds)) {
				read_len = read(tty_fd, &pData[nReadOut],
						DataLen - nReadOut);
				if (read_len > 0) {
					//printf("read_len %d\n",read_len);
					nReadOut += read_len;

				}

			} else {
				printf("[%s] select() 5sec timeout!!\r\n", __FUNCTION__);
				bRetCode = 0;
				usleep(100000);
			}
		}
	}

	if (0) {
		printf("nReadOut=%d", nReadOut);

		// print out data
		for (i = 0; i < nReadOut; i++) {
			if (!(i % 16))
				puts("");
			printf("%.2X ", pData[i]);
		}
		puts("");

	}
	return bRetCode;
}

static int tty_data_drian(int nBytes) {
	uint8_t tmp[1024];
	int n;

	while(nBytes > 0) {
		n = nBytes > sizeof(tmp) ? sizeof(tmp) : nBytes;
		readData(tmp, n);
		nBytes -= n;
	}

	return 0;
}

static void* tty_parse_pthread(void* params) {
	uint8_t buffer[14];
	static uint8_t msg_buffer[1024 + 8];
	uint8_t buffer_length = sizeof(buffer) - 5;
	uint32_t header_index = 0;
	uint8_t adjust_buffer[5];
	int payload_size, msg_buffer_size;
	osp_header_t header_type = OSP_HEADER_NOTFOUND;

	while (!aQuit && tty_fd != -1) {

		//usleep(10000);
		buffer_length = sizeof(buffer) - 5;

		/* Sleep sometimes in order to allow the other task to do something */
		//Timer_Delay(100);

		/* Poll the SPI's buffer to see any valid OSP messages */
		switch (header_type) {
		case OSP_HEADER_PARTIAL:
			readData(&buffer[1], buffer_length - 1);
			break;

		case OSP_HEADER_NOTFOUND:
		case OSP_HEADER_FULL:
		default:
			readData(buffer, buffer_length);
			break;
		}

		// readData(buffer, buffer_length);
		header_type = header_detected(buffer, buffer_length, &header_index);

		switch (header_type) {
		case OSP_HEADER_NOTFOUND:
			//printf("OSP_HEADER_NOTFOUND\n");
			// No header found wait and then try again
			// if(get_patch_update_done() == 0)
			//	usleep(10000);
			break;

		case OSP_HEADER_PARTIAL:
			//printf("OSP_HEADER_PARTIAL\n");
			// Found A0 as last byte in buffer, so need to request more data to check if a real message
			buffer[0] = buffer[buffer_length - 1];
			break;

		case OSP_HEADER_FULL:
			//printf("OSP_HEADER_FULL\n");
			/* The SPI buffer is preserve for readding task */
			//lock_spi_buffer ();
			//gps_printf("\n\t\t0xA0A2 detected\n");
			/* The last byte of the payload size is missing, ask for the last byte */
			if (header_index == 6) {
				//printf("header_index == 6\n");
				readData(adjust_buffer, 1);
				memcpy(&buffer[buffer_length], adjust_buffer, 1);
				buffer_length = buffer_length + 1;
			}

			/* The payload length is missing, ask for the two bytes payload size */
			if (header_index == 7) {
				//	printf("header_index == 7\n");
				readData(adjust_buffer, 2);
				memcpy(&buffer[buffer_length], adjust_buffer, 2);
				buffer_length = buffer_length + 2;
			}

			/* Get message payload size */
			payload_size = (buffer[header_index + 2] << 8)
					| buffer[header_index + 3];
			/* Real message size Start Sequence + Payload Length + Payload + Checksum + End Sequence */
			msg_buffer_size = payload_size + 8;

			if (msg_buffer_size > sizeof(msg_buffer)) {
				printf("WARNING:header_type:%d,msg_buffer_size:%d,header_index:%u. incomplete packet, ignoring\n",header_type,msg_buffer_size,header_index);
				// trying to igore bytes
				tty_data_drian(payload_size + 4);
				continue;
			} else {
				//printf("msg_buffer_size > buffer_length %d\n",msg_buffer_size);
				memcpy(msg_buffer, &buffer[header_index],
						buffer_length - header_index);
				readData(&msg_buffer[buffer_length - header_index],
						msg_buffer_size - (buffer_length - header_index));
			}

			process_osp(msg_buffer);
			break;
		}

	}
	pthread_exit(0);
}

int tty_data_init(void) {
	int fd;
	fd = open("/dev/ttyGSD5xp0", O_RDWR);
	if (fd < 0) {
		perror("can't open device");
		abort();
	}

	tty_fd = fd;

	return 0;
}

void tty_data_deinit(void)
{
	if(tty_fd > 0) {
		close(tty_fd);
		tty_fd = -1;
	}
}

void tty_data_start_parse_thread(void)
{
	if(pthread_create( &parse_pthread_handle, NULL, tty_parse_pthread, NULL))
	{
		printf("Create tty_read_pthread failed\n");
		exit(-1);
	}
}

void tty_data_join_parse_thread(void)
{
	pthread_join(parse_pthread_handle, NULL);
}

void tty_data_send(uint8_t *buffer, uint32_t size) {
	write(tty_fd, buffer, size);
}
