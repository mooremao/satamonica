#ifndef  __CRC_H_
#define  __CRC_H_

uint16_t gps_update_crc16(uint8_t *pData, int32_t len, uint16_t pre_crc);
uint16_t gps_calculate_crc16(uint8_t *pData, int32_t len);

uint32_t gps_update_crc32(uint8_t *pData, int32_t len, uint32_t pre_crc);
uint32_t gps_calculate_crc32(uint8_t *pData, int32_t len);
uint32_t gps_finalize_crc32 (uint32_t pre_crc);

#endif /* __CRC_H_*/
