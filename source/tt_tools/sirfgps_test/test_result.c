#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include "test_result.h"
#include "lcm_client.h"

#define NON_CONFIG_FILE_PASS_CN 35


#define MAX_ALLOWED_CHAMBER_TESTTIME_SEC 120

#define CHAMBER_START_AVG_SEC (multich_result_obj.TTFF+1)
#define OPENSKY_START_AVG_SEC (multich_result_obj.TTFF+120)
#define OPENSKY_END_SEC (multich_result_obj.TTFF+240)

#define MIN_COUNT_PER_SV 10
#define MAX_FIX_TIME 60
#define MAX_TEST_TIME_FIXONLY 120


#define GNSS_CH_LOG_FILE "/opt/ipnc/test/gpschamber.log"
#define TESTMODE4_LOG_FILE "/opt/ipnc/test/testmode4.log"
#define OPENSKY_LOG_FILE "/opt/ipnc/test/gpstest.log"
#define TTFF_LOG_FILE "/opt/ipnc/test/ttff.log"

#define END_SOUND_SCRIPT "/mnt/test/buzzer_play.sh"

static t_TestResult_MultiCH multich_result_obj;
static t_TestResult_TestMode4 testmode4_result_obj;
static int GPSBetterCN = 49,GPSGoodCN=46,GLOBetterCN=50,GLOGoodCN=47,ReasonableCN=40;
static int MinGNSSSatID=65;
static int t_TestResult_MultiCH_CheckTestCanFinish();

void t_TestResult_MultiCH_init(eMultiChTestMode mode)
{
	printf("%s:mode %d\n",__FUNCTION__,mode);
	
	memset(&multich_result_obj, 0x00, sizeof(multich_result_obj));
	multich_result_obj.MinCountPerSV = 10;
	multich_result_obj.mode = mode;
	
		
	system("mkdir -p /opt/ipnc/test/");
	
	if (lcm_client_open() < 0)
	{
		printf("can't init lcm queue");
		//exit(1);
	}
	if(multich_result_obj.mode == eMultiChTestMode_Chamber )
	{
		unlink(GNSS_CH_LOG_FILE);
		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("Chamber",0,1);
		lcm_client_sync_fb();
  }		
	else if(multich_result_obj.mode == eMultiChTestMode_OpenSky )
	{
		unlink(OPENSKY_LOG_FILE);	
		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("Opensky",0,1);
		lcm_client_sync_fb();
	}
  else if(multich_result_obj.mode == eMultiChTestMode_FixOnly )
	{
		unlink(TTFF_LOG_FILE);
		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("FixOnly",0,1);
		lcm_client_sync_fb();		
  }	
  else if(multich_result_obj.mode == eMultiChTestMode_HibernateOnly )
	{
		lcm_client_clear_all();	
		lcm_client_draw_text("GNSS",0,0);
		lcm_client_draw_text("Hiber",0,1);
		lcm_client_draw_text("nating",0,2);
		lcm_client_draw_text("...",0,3);
		lcm_client_sync_fb();		
  }	  
  else if(multich_result_obj.mode == eMultiChTestMode_Endless )
	{
		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("EndLess",0,1);
		lcm_client_sync_fb();		
  }	  
}


void t_TestResult_deinit()
{	
	
	lcm_client_close();
		
}

void t_TestResult_MultiCH_CleartCurrentSVStatus()
{
		int j;
			
		for(j=0;j<MaxSatellitesID;j++)
    {
    				multich_result_obj.SNR_current[j] = 0;
    }		
}

void t_TestResult_MultiCH_SVStatus(int satnr, int snr)
{
	if( satnr >= MaxSatellitesID )
		return;

	//printf("%s:satnr %d, snr %d\n",__FUNCTION__,satnr,snr);
	multich_result_obj.SNR_current[satnr] = snr;

	if(multich_result_obj.start_average)
	{
		int j;
		multich_result_obj.SNR_total[satnr]+=snr;
		multich_result_obj.SNR_count[satnr]++;

		for(j=0;j<MaxSatellitesID;j++)
		{
			if(multich_result_obj.SNR_count[j]!=0)
				multich_result_obj.SNR_average[j] = multich_result_obj.SNR_total[j]/multich_result_obj.SNR_count[j];
		}
	}

}

static void t_TestResult_PlayEndSound()
{
	char cmd[256];
	
	sprintf(cmd,"%s %d %d &",END_SOUND_SCRIPT,2,185185);
	system(cmd);
}

static void t_TestResult_MultiCH_DisplayResult() 
{
	lcm_client_clear_all();	
	lcm_client_draw_text("Test",0,0);
	if(multich_result_obj.bResult)
		lcm_client_draw_text("Pass",0,1);	
	else
		lcm_client_draw_text("FAIL",0,1);	
	
	lcm_client_sync_fb();
	
	t_TestResult_PlayEndSound();
}

int t_TestResult_MultiCH_TestCanExit()
{
	return multich_result_obj.TestCanExit;
}

static void t_TestResult_MultiCH_SetStartAvg()
{
		if(!multich_result_obj.IsFix)
			return;
			
		if(multich_result_obj.mode == eMultiChTestMode_Chamber )
		{
			if(multich_result_obj.time_count >= CHAMBER_START_AVG_SEC )
    		 multich_result_obj.start_average = 1;
    }
		if(multich_result_obj.mode == eMultiChTestMode_OpenSky )
		{
			if(multich_result_obj.time_count >= OPENSKY_START_AVG_SEC )
    		 multich_result_obj.start_average = 1;
    }        	 	
}

static void t_TestResult_MultiCH_GetSimpleSVStat(int * MaxCN, int *numGPS, int *numGLO)
{
		int i;
			
		*MaxCN = 0; *numGPS = 0;*numGLO = 0;
		
		for(i=0;i<MaxSatellitesID;i++)
    {
    	if(multich_result_obj.SNR_current[i] > 0)
    	{
    		if(i>=MinGNSSSatID)
    		{
    			(*numGLO)++;
    		}
    		else
    		{
    			(*numGPS)++;
    		}
    		if(multich_result_obj.SNR_current[i]>*MaxCN)
    			*MaxCN = multich_result_obj.SNR_current[i];
    	}
    }		
}

void t_TestResult_MultiCH_UpdateFixInfo(int time_count,int isFix, int TTFF, int latitude, int longitude)
{
	char lcd_text[10];
	
	int MaxCN,numGPS,numGLO;
	
	if(multich_result_obj.mode == eMultiChTestMode_HibernateOnly )
		return;
		
	multich_result_obj.IsFix = isFix;
	multich_result_obj.time_count = time_count;
	
	lcm_client_clear_all();
	
	snprintf(lcd_text, sizeof(lcd_text),"%ds",time_count);
	lcm_client_draw_text(lcd_text, 0, 0);
			
	if(isFix)
	{
				
		snprintf(lcd_text, sizeof(lcd_text),"TTFF:%ds",TTFF);
		lcm_client_draw_text(lcd_text, 0, 1);
		
		if(latitude>0)
			snprintf(lcd_text, sizeof(lcd_text),"%4.2fN",(float)latitude/10000000);
		else
			snprintf(lcd_text, sizeof(lcd_text),"%4.2fS",(float)latitude*(-1)/10000000);
			
		lcm_client_draw_text(lcd_text, 0, 2);

		if(longitude>0)
			snprintf(lcd_text, sizeof(lcd_text),"%4.2fE",(float)longitude/10000000);
		else
			snprintf(lcd_text, sizeof(lcd_text),"%4.2fW",(float)longitude*(-1)/10000000);
		lcm_client_draw_text(lcd_text, 0, 3);		
				
  }
  
  t_TestResult_MultiCH_GetSimpleSVStat(&MaxCN,&numGPS,&numGLO);
  
  snprintf(lcd_text, sizeof(lcd_text),"MaxCN:%d",MaxCN);
  lcm_client_draw_text(lcd_text, 0, 4);
  snprintf(lcd_text, sizeof(lcd_text),"NrGPS:%d",numGPS);
  lcm_client_draw_text(lcd_text, 0, 5);  
  snprintf(lcd_text, sizeof(lcd_text),"NrGLO:%d",numGLO);
  lcm_client_draw_text(lcd_text, 0, 6);    
  
	//printf("%s:time_count %d, isFix %d, TTFF %d, latitude %d, longitude %d \n",__FUNCTION__,time_count,isFix,TTFF,latitude,longitude);
	
	if(multich_result_obj.IsFix && multich_result_obj.IsFirstFix == 0 )
	{
		multich_result_obj.IsFirstFix = 1;
	  multich_result_obj.TTFF = TTFF;
	}
	if(multich_result_obj.IsFix)
	{
			t_TestResult_MultiCH_SetStartAvg();
    	multich_result_obj.latitude = latitude;
    	multich_result_obj.longitude = longitude;
	}
	
	if(t_TestResult_MultiCH_CheckTestCanFinish())
	{
		multich_result_obj.TestFinished = 1;
		multich_result_obj.TestCanExit = 1;
	}
	if(multich_result_obj.TestFinished)
	{		
		t_TestResult_MultiCH_Log2File();
		multich_result_obj.TestCanExit = 1;
		t_TestResult_MultiCH_DisplayResult();		
	}	
	
	lcm_client_sync_fb();

}

static int t_TestResult_MultiCH_CheckTestCanFinishOpenSky()
{
	  int elapsed_sec = multich_result_obj.time_count;
	  
		if(!multich_result_obj.IsFix && elapsed_sec>=MAX_FIX_TIME)
			return 1;
		if(elapsed_sec>=OPENSKY_END_SEC)
			return 1;
			
		return 0;
}

static int t_TestResult_MultiCH_CheckTestCanFinishFixOnly()
{
		int elapsed_sec = multich_result_obj.time_count;
		
	  if( multich_result_obj.IsFix)
	  	return 1;
	  else if(elapsed_sec >= MAX_TEST_TIME_FIXONLY )
	  	return 1;
	  return 0;
}

static int t_TestResult_MultiCH_CheckTestCanFinish()
{
	if(multich_result_obj.mode == eMultiChTestMode_Chamber)
		return t_TestResult_MultiCH_CheckTestCanFinishByPrn7OrPrn8AndPrn74();
	if(multich_result_obj.mode == eMultiChTestMode_OpenSky)
		return t_TestResult_MultiCH_CheckTestCanFinishOpenSky();
	if(multich_result_obj.mode == eMultiChTestMode_FixOnly)
		return t_TestResult_MultiCH_CheckTestCanFinishFixOnly();
	if(multich_result_obj.mode == eMultiChTestMode_Endless)
		return 0;
				
	return 0;
		
}

int t_TestResult_MultiCH_CheckTestCanFinishByPrn7OrPrn8AndPrn74()
{
	int elapsed_sec = multich_result_obj.time_count;
	
	if(elapsed_sec>=MAX_ALLOWED_CHAMBER_TESTTIME_SEC)
		return 1;
		
	if(multich_result_obj.IsFix)
	{							
			if( ( (multich_result_obj.SNR_average[7]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[7]>=multich_result_obj.MinCountPerSV)
				|| (multich_result_obj.SNR_average[8]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[8]>=multich_result_obj.MinCountPerSV) )
				&& (multich_result_obj.SNR_average[74]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[74]>=multich_result_obj.MinCountPerSV)
				)
				return 1;			
			else
				return 0;
		
	}
	else
	{ // not fix yet
		if(elapsed_sec>MAX_FIX_TIME)
			return 1; //means already failed		
	}		
	return 0;	
}	

int t_TestResult_MultiCH_CheckResultbyPrn7OrPrn8AndPrn74(void)
{	  
	  int result;
	  
		if( ( (multich_result_obj.SNR_average[7]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[7]>=multich_result_obj.MinCountPerSV)
				|| (multich_result_obj.SNR_average[8]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[8]>=multich_result_obj.MinCountPerSV) )
				&& (multich_result_obj.SNR_average[74]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[74]>=multich_result_obj.MinCountPerSV)
				)
				result =  1;
		else
				result =  0;	
		return result;		
}

static int t_TestResult_MultiCH_CheckOpenSkyResult(void)
{
	int i;
  int nr_gps_better= 0, nr_glo_better= 0;
  int nr_gps_good= 0, nr_glo_good= 0;
  int nr_reasonable= 0;
  int Result;
      	
	for( i= 0; i< MaxSatellitesID; i++)
	{
		if( multich_result_obj.SNR_count[i]> 20)
		{
         if( i >= MinGNSSSatID)
         {
         		if( multich_result_obj.SNR_average[i]>= GLOBetterCN) nr_glo_better++;
            if( multich_result_obj.SNR_average[i]>= GLOGoodCN) nr_glo_good++;
         }
         else
       	 {
            if( multich_result_obj.SNR_average[i]>= GPSBetterCN) nr_gps_better++;
            if( multich_result_obj.SNR_average[i]>= GPSGoodCN) nr_gps_good++;
         }
         if( multich_result_obj.SNR_average[i]>= ReasonableCN) nr_reasonable++;			
		}
		
	}
    if( nr_gps_good >= 4 && nr_gps_better >= 1 &&
        nr_glo_good >= 3 && nr_glo_better >= 1)
    {
        Result= 1;
    }
    else
    {
        Result= 0;
    }
    return Result;	
}

int t_TestResult_MultiCH_Chamber_Log2File()
{
	int i;
	FILE * pFile;
	float lat, lon;
	char strNSind[2],strEWind[2];

	multich_result_obj.bResult = t_TestResult_MultiCH_CheckResultbyPrn7OrPrn8AndPrn74();
	
	pFile = fopen (GNSS_CH_LOG_FILE,"w");
	if(pFile == NULL )
	{
		printf(" log file open error\n");
		return -1;
	}	
	fprintf(pFile,"[General]\r\n");
	
	lat = (float)(multich_result_obj.latitude)/10000000;
	lon = (float)(multich_result_obj.longitude)/10000000;
	
	if(lat > 0)
	{
		strcpy(strNSind,"N");
	}
	else
	{
		lat = lat*(-1);
		strcpy(strNSind,"S");
	}
	
	if(lon > 0)
	{
		strcpy(strEWind,"E");
	}
	else
	{
		lon = lon*(-1);
		strcpy(strEWind,"W");
	}
		
	fprintf(pFile,"Latitude=%f%s\r\n",lat,strNSind);
	fprintf(pFile,"Longitude=%f%s\r\n",lon,strEWind);
	
	for(i=1;i<=32;i++)
	{
		if(i<=9)
			fprintf(pFile,"PRN0%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
		else
			fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	for(i=65;i<=96;i++)
	{
		fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	fprintf(pFile,"Result=%d\r\n",multich_result_obj.bResult);
	fprintf(pFile,"TTFF=%d\r\n",multich_result_obj.TTFF);
	fclose(pFile);


	
	return 0;
}


int t_TestResult_MultiCH_OpenSky_Log2File()
{
	int i;
	FILE * pFile;
	float lat, lon;
	char strNSind[2],strEWind[2];

	multich_result_obj.bResult = t_TestResult_MultiCH_CheckOpenSkyResult();
	
	pFile = fopen (OPENSKY_LOG_FILE,"w");
	if(pFile == NULL )
	{
		printf(" log file open error\n");
		return -1;
	}	
	fprintf(pFile,"[General]\r\n");
	
	lat = (float)(multich_result_obj.latitude)/10000000;
	lon = (float)(multich_result_obj.longitude)/10000000;
	
	if(lat > 0)
	{
		strcpy(strNSind,"N");
	}
	else
	{
		lat = lat*(-1);
		strcpy(strNSind,"S");
	}
	
	if(lon > 0)
	{
		strcpy(strEWind,"E");
	}
	else
	{
		lon = lon*(-1);
		strcpy(strEWind,"W");
	}
		
	fprintf(pFile,"Latitude=%f%s\r\n",lat,strNSind);
	fprintf(pFile,"Longitude=%f%s\r\n",lon,strEWind);
	
	for(i=1;i<=32;i++)
	{
		if(i<=9)
			fprintf(pFile,"PRN0%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
		else
			fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	for(i=65;i<=96;i++)
	{
		fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	fprintf(pFile,"Result=%d\r\n",multich_result_obj.bResult);
	fprintf(pFile,"TTFF=%d\r\n",multich_result_obj.TTFF);
	fclose(pFile);

	return 0;
}

static int t_TestResult_FixOnly_Log2File()
{
	int i;
	FILE * pFile;
	float lat, lon;
	char strNSind[2],strEWind[2];

	multich_result_obj.bResult = multich_result_obj.IsFix;
	
	pFile = fopen (TTFF_LOG_FILE,"w");
	if(pFile == NULL )
	{
		printf(" log file open error\n");
		return -1;
	}	
	fprintf(pFile,"[General]\r\n");
	
	lat = (float)(multich_result_obj.latitude)/10000000;
	lon = (float)(multich_result_obj.longitude)/10000000;
	
	if(lat > 0)
	{
		strcpy(strNSind,"N");
	}
	else
	{
		lat = lat*(-1);
		strcpy(strNSind,"S");
	}
	
	if(lon > 0)
	{
		strcpy(strEWind,"E");
	}
	else
	{
		lon = lon*(-1);
		strcpy(strEWind,"W");
	}
		
	fprintf(pFile,"Latitude=%f%s\r\n",lat,strNSind);
	fprintf(pFile,"Longitude=%f%s\r\n",lon,strEWind);
	
	for(i=1;i<=32;i++)
	{
		if(i<=9)
			fprintf(pFile,"PRN0%d=%d\r\n",i,multich_result_obj.SNR_current[i]);
		else
			fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_current[i]);
	}
	for(i=65;i<=96;i++)
	{
		fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_current[i]);
	}
	fprintf(pFile,"Result=%d\r\n",multich_result_obj.bResult);
	fprintf(pFile,"TTFF=%d\r\n",multich_result_obj.TTFF);
	fclose(pFile);

	return 0;
}

int t_TestResult_MultiCH_Log2File()
{
	if(multich_result_obj.mode == eMultiChTestMode_Chamber)
		t_TestResult_MultiCH_Chamber_Log2File();
	if(multich_result_obj.mode == eMultiChTestMode_OpenSky)
		t_TestResult_MultiCH_OpenSky_Log2File();
	if(multich_result_obj.mode == eMultiChTestMode_FixOnly)
		t_TestResult_FixOnly_Log2File();			
}

void t_TestResult_TestMode4_init(int testmode4_num_period)
{
	
	unlink(TESTMODE4_LOG_FILE);
	system("mkdir -p /opt/ipnc/test/");
	memset(&testmode4_result_obj, 0x00, sizeof(testmode4_result_obj));
	testmode4_result_obj.max_count = testmode4_num_period;
	
	if (lcm_client_open() < 0)
	{
		printf("can't init lcm queue");
		exit(1);
	}
	
	lcm_client_clear_all();
	lcm_client_draw_text("Init",0,0);
	lcm_client_draw_text("TestMod4",0,1);
	lcm_api_sync_fb();
}

static t_TestResult_TestMode4_DisplayResult() 
{
	lcm_client_clear_all();
	lcm_client_draw_text("Test",0,0);
	lcm_client_draw_text("Done",0,1);	
}


int t_TestResult_TestMode4_Write2Log(char * string)
{
	FILE * pFile;
	
	pFile = fopen (TESTMODE4_LOG_FILE,"a");
	if(pFile == NULL )
	{
		printf(" log file open error\n");
		return -1;
	}
	fprintf(pFile,string);
	fclose(pFile);
	
	testmode4_result_obj.time_count++;
	
}



int t_TestResult_TestMode4_UpdateResult(test_mode4_t *test4)
{
	char string[256];
	
	sprintf (string,"Mode4: bit_sync_time= %d, Cn0 mean = %f, sigma = %f, drift_change = %f, drift = %f, AbsI20ms = %d, AbsQ20ms = %d, phase indicator = %f, RTC_Freq = %d\n",
							test4->bit_sync_time,test4->cn0_mean, test4->cn0_sigma, test4->clock_drift_change, test4->clock_drift,test4->AbsI20ms, test4->AbsQ20ms, test4->phase_lock, test4->RTCFrequency);
	
		
	printf("%s:string %s\n",__FUNCTION__,string);
		
	t_TestResult_TestMode4_Write2Log(string);
			
	testmode4_result_obj.time_count++;
	
}

int t_TestResult_TestMode4_TestCanExit()
{
	if( testmode4_result_obj.time_count >= testmode4_result_obj.max_count )
	{
		t_TestResult_TestMode4_DisplayResult();
		lcm_api_sync_fb();
		return 1;
	}
		
	return 0;
}
