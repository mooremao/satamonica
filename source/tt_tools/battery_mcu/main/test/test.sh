#!/bin/sh

# gpio config for relays
DPM2=1
DPM1=2
LOAD=5
VBUS1=6
VBUS2=7

# i2c config for battery stick
BUS=1
BS=0x44
BS_VER=0
BS_STAT=1
BS_ILIM=2
BS_ULIM=3

# i2c config for fuel gauge
FG=0x55
FG_VOLTAGE=0x02
FG_CURRENT=0x02

MAX_VOLTAGE=`printf "%d" 0xb7`
MIN_VOLTAGE=`printf "%d" 0xa0`

dpm1_on()
{
	gpio write $DPM1 0
}

dpm1_off()
{
	gpio write $DPM1 1
}

dpm2_on()
{
	gpio write $DPM2 0
}

dpm2_off()
{
	gpio write $DPM2 1
}

vbus1_on()
{
	gpio write $VBUS1 0
}

vbus1_off()
{
	gpio write $VBUS1 1
}

vbus2_on()
{
	gpio write $VBUS2 0
}

vbus2_off()
{
	gpio write $VBUS2 1
}

load_on()
{
	gpio write $LOAD 0
}

load_off()
{
	gpio write $LOAD 1
}

init_gpio()
{
	gpio mode $DPM1 out # d+/-
	gpio mode $DPM2 out # d+/-
	gpio mode $LOAD out # lload
	gpio mode $VBUS1 out # vbus
	gpio mode $VBUS2 out # vbus
	load_off
	vbus1_on
	vbus2_off
}


get_voltage()
{
	v=`i2cget -y $BUS $FG $FG_VOLTAGE`
	printf "%d" $v
}

check_ver()
{
	echo -n "\tTEST VERSION:"
	val=`i2cget -y $BUS $BS $BS_VER`

	if [ $val = '0x0d' ]
	then
		ret="PASS"
	else
		ret="FAIL $val"
	fi
	echo "$ret"
}

check_status()
{
	echo -n "\tTEST STATUS $1: "
	val=`i2cget -y $BUS $BS $BS_STATUS`

	if [ $val = $1 ]
	then
		ret="PASS"
	else
		ret="FAIL $val"
	fi
	echo "$ret"
}

check_ilim()
{
	echo -n "\tTEST ILIM $1: "
	val=`i2cget -y $BUS $BS $BS_ILIM`

	if [ $val = $1 ]
	then
		ret="PASS"
	else
		ret="FAIL $val"
	fi
	echo "$ret"
}

check_ulim()
{
	echo -n "\tTEST ULIM $1: "
	val=`i2cget -y $BUS $BS $BS_ULIM`

	if [ $val = $1 ]
	then
		ret="PASS"
	else
		ret="FAIL $val"
	fi
	echo "$ret"
}

func_test()
{
	init_gpio

	echo "VBUS1 on, DPM1 off, LOAD off"

	vbus1_on
	vbus2_off
	load_off
	dpm1_off
	dpm2_off

	sleep 8

	check_ver
	check_status "0x0c"
	check_ilim "0x10"
	check_ulim "0x05"

	echo "VBUS2 on, DPM2 off, LOAD off"
	vbus2_on
	vbus1_off
	load_off
	dpm1_off
	dpm2_off

	sleep 8

	check_ver
	check_status "0x04"
	check_ilim "0x10"
	check_ulim "0x01"

	echo "VBUS1 on, DPM1 off, LOAD off"
	vbus1_on
	vbus2_off
	load_off
	dpm1_off
	dpm2_off
	sleep 8

	check_ver
	check_status "0x0c"
	check_ilim "0x10"
	check_ulim "0x05"

	echo "VBUS1 on, DPM1 off, LOAD on"
	vbus1_on
	vbus2_off
	load_on
	dpm1_off
	dpm2_off
	sleep 8

	check_ver
	check_status "0x0c"
	check_ilim "0x10"
	check_ulim "0x05"

	echo "VBUS2 on, DPM2 off, LOAD on"
	load_off
	vbus2_on
	vbus1_off
	dpm1_off
	dpm2_off
	sleep 8
	load_on

	check_ver
	check_status "0x04"
	check_ilim "0x10"
	check_ulim "0x01"

	echo "VBUS1 on, DPM1 on, LOAD off"

	vbus1_on
	vbus2_off
	load_off
	dpm1_on
	dpm2_off

	sleep 8

	check_ver
	check_status "0x0c"
	check_ilim "0x10"
	check_ulim "0x05"

	echo "VBUS2 on, DPM2 on, LOAD on"
	load_off
	vbus2_on
	vbus1_off
	dpm1_off
	dpm2_on
	sleep 8
	load_on

	check_ver
	check_status "0x04"
	check_ilim "0x10"
	check_ulim "0x01"


}

charge_test_vbus1()
{
	echo "CHARGE VBUS1 TEST"
	load_off
	vbus1_on
	dpm1_on
	vbus1_off
	touch loop
	
	while [ -f loop ]
	do
		sleep 5
		check_ver
		check_status "0x04"
		v=`get_voltage`
		if [ $v -gt $MAX_VOLTAGE ]
		then
			rm -f loop
		fi
	done
	echo "CHARGE TEST PASS"
}

charge_test_vbus2()
{
	echo "CHARGE VBUS2 TEST"
	load_off
	vbus2_on
	dpm2_on
	vbus1_off
	touch loop
	
	while [ -f loop ]
	do
		sleep 5
		check_ver
		check_status "0x04"
		v=`get_voltage`
		if [ $v -gt $MAX_VOLTAGE ]
		then
			rm -f loop
		fi
	done
	echo -n "CHARGE TEST PASS"
}

drain_test()
{
	echo "DRAIN TEST"
	load_on
	vbus2_off
	vbus1_off
	touch loop
	
	while [ -f loop ]
	do
		sleep 5
		check_ver
		check_status "0x00"
		v=`get_voltage`
		if [ $v -lt $MIN_VOLTAGE ]
		then
			rm -f loop
		fi
	done
	echo -n "DRAIN TEST PASS"
}


while :
do
	func_test
	charge_test_vbus1
	drain_test
	charge_test_vbus2
	drain_test
done | tee testlog.txt

