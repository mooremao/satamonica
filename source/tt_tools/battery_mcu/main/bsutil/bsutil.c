/* 
 * Description:
 *     BatteryStick Interface Utility
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include "bs.h"


/************************************************************************/
/* PRIVATE VARIABLES & DEFINITIONS                                      */
/************************************************************************/

/* SW Version */
#define SW_VER                     "1.2"

/* Default I2C Bus number */
#define I2C_BUS_DEFAULT            3




/************************************************************************/
/* PRIVATE FUNCTIONS                                                    */
/************************************************************************/

static void print_header()
{
    printf("================================\n");
    printf("BS Utility, ver. %s\n", SW_VER);
    printf("gerard.marullparetas@tomtom.com\n");
    printf("[%s, %s]\n", __DATE__, __TIME__);
    printf("================================\n\n");
}


static void print_usage()
{
    /* Note: Battery program command is hidden! */
    printf("Available commands:\n\n");
    printf("\ti: Retrieve BS information\n");
    printf("\tw [time, (s)]: Set wakeup interrupt\n");
    printf("\tt [mode]: Enable test mode\n");
    printf("\t\tAvailable modes: 0 (None), 1 (USB2), 2 (USB3), 3 (USB2&3)\n");
    printf("\tb [i2c bus]: I2C Bus (default: %d)\n", I2C_BUS_DEFAULT);
    printf("\tp [batt type]: Battery type\n");
    printf("\t\tAvailable types: 0 (2000mAh), 1 (3000mAh)\n");
    printf("\th: Display this information\n\n");
}



/************************************************************************/
/* PUBLIC FUNCTIONS                                                     */
/************************************************************************/

int main (int argc, char **argv)
{
    int dev;
    bs_info_t info;

    int c, i2c_bus = I2C_BUS_DEFAULT, total_ops = 0, curr_op = 1;
    bool i_flag = false, w_flag = false, p_flag = false, t_flag = false;
    unsigned char wakeup_time, batt_type, test_mode, pw;
    
    print_header();

    if (argc == 1)
    {
        print_usage();
        return 1;
    }

    opterr = 0;

    while ((c = getopt (argc, argv, "iw:p:t:b:")) != -1)
    {    
        switch (c)
        {
            case 'i':
                i_flag = true;
                total_ops++;
                break;
            case 'w':
                w_flag = true;
                wakeup_time = atoi(optarg);
                total_ops++;
                break;
            case 'p':
                p_flag = true;
                batt_type = atoi(optarg);
                total_ops++;
                break;
            case 't':
                t_flag = true;
                test_mode = atoi(optarg);
                total_ops++;
                break;
            case 'b':
                i2c_bus = atoi(optarg);
                total_ops++;
                break;
            case '?':
            default:
                print_usage();
                return 1;
        }
    }

    /* CONNECT */
    if ((dev = bs_connect(i2c_bus)) < 0)
    {
        fprintf(stderr, "Error connecting to BS\n");
        return 1;
    }

    /* (i) BS Information */
    if (i_flag)
    {
        printf("[OP %d/%d] - BS Information\n", curr_op++, total_ops);

        if (bs_get_info(dev, &info))
        {
            fprintf(stderr, "Error getting BS information\n");
            return 1;
        }
        
        /* FW Version */
        printf(" * FW Version: %d\n", info.fw_ver);
        /* Status */
        printf(" * Status:\n");
        printf("   - Ext. mic present: %s\n", 
            (info.status & REG_STATUS_EXTMIC) ? "Yes" : "No");
        printf("   - Battery forced discharging active: %s\n", 
            (info.status & REG_STATUS_BATT_FORCED_DISCHARGE) ? "Yes" : "No");
        printf("   - Battery charging: %s\n", 
            (info.status & REG_STATUS_BATT_CHARGING) ? "Yes" : "No");
        printf("   - USB Bridge powered: %s\n", 
            (info.status & REG_STATUS_BATT_UBRIDGE_POWERED) ? "Yes" : "No"); 
        /* Current limits (ILIM & ULIM) */
        printf(" * Charging current limits:\n");
        printf("   - ILIM:");
        switch(info.ilim)
        {
            case REG_ILIM_500MA:
                printf("500 mA\n");
                break;
            case REG_ILIM_900MA:
                printf("900 mA\n");
                break;
            case REG_ILIM_1200MA:
                printf("1200 mA\n");
                break;
            default:
                printf("Invalid\n");
        }
        printf("   - ULIM:");
        switch(info.ulim)
        {
            case REG_ULIM_500MA:
                printf("500 mA\n");
                break;
            case REG_ULIM_1500MA:
                printf("1500 mA\n");
                break;
            default:
                printf("Invalid\n");
        }
        /* Battery type */
        printf(" * Battery type: ");
        switch(info.batt_type)
        {
            case REG_BATT_TYPE_2000MAH:
                printf("NCR18500 - 2000 mAh\n");
                break;
            case REG_BATT_TYPE_3000MAH:
                printf("ICR18650 - 3000 mAh\n");
                break;
            default:
                printf("Invalid\n");
        }
        /* Test mode */
        printf(" * Test mode: %d\n", info.test_mode);
        /* Failure registers */
        printf(" * FG Failures: %d\n", info.fg_failures);
        printf(" * WDT Resets: %d\n", info.wdt_resets);
    }

    /* (w) Wakeup */
    if (w_flag)
    {
        printf("[OP %d/%d] - Setting wakeup...", curr_op++, total_ops);
        
        if(bs_set_wakeup(dev, wakeup_time))
        {
            fprintf(stderr, "Error setting wakeup time\n");
            return 1;
        }
        
        printf("DONE\n");
    }

    /* (p) Program battery type */
     if (p_flag)
    {
        printf("Enter password: ");
        scanf("%1hhu", &pw);

        printf("[OP %d/%d] - Programming battery type...", curr_op++, total_ops);

        if(bs_program_batt_type(dev, batt_type, pw))
        {
            fprintf(stderr, "Error programming battery type\n");
            return 1;
        }

        printf("DONE\n");
    }

    /* (t) Set test mode */
     if (t_flag)
    {
        printf("[OP %d/%d] - Setting test mode...", curr_op++, total_ops);

        if(bs_set_test_mode(dev, test_mode))
        {
            fprintf(stderr, "Error setting test mode\n");
            return 1;
        }

        printf("DONE\n");
    }  

    /* DISCONNECT */
    bs_disconnect(dev);

    printf("\nAll operations completed successfully\n");

    return 0;
}
