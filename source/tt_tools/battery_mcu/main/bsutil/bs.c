/* 
 * Description:
 *     BatteryStick Interface Library
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include "bs.h"

/************************************************************************/
/* PRIVATE VARIABLES & DEFINITIONS                                      */
/************************************************************************/

/* BS I2C Address */
#define BS_I2C_ADDR                      0x44

/* BS Interface Registers */
#define REG_VERSION                      0x00
#define REG_STATUS                       0x01
#define REG_ILIM                         0x02
#define REG_ULIM                         0x03
#define REG_FAIL_FG                      0x04
#define REG_FAIL_WDTR                    0x05
#define REG_WAKEUP_L                     0x06
#define REG_WAKEUP_H                     0x07
#define REG_PASSWORD                     0x08
#define REG_BATT_TYPE                    0x09
#define REG_TEST_MODE                    0x0A

/* Extended registers password */
#define REG_PASSWORD_PW                  0x48




/************************************************************************/
/* PRIVATE FUNCTIONS                                                    */
/************************************************************************/

/* Enable Extended Registers Access
 *
 * Parameters:
 *    dev: BS file descriptor
 *    password: unlock password
 *
 * Returns:
 *    0 on success, 1 otherwise
 */
static int bs_lock_access(int dev, unsigned char password)
{
    unsigned char buf[2];

    buf[0] = REG_PASSWORD;
    buf[1] = password;
    if (write(dev, (void*)buf, 2) != 2)
        return 1;

    return 0;
}




/************************************************************************/
/* PUBLIC FUNCTIONS                                                     */
/************************************************************************/

/* Connect to BS
 *
 * Parameters:
 *     i2c_bus: I2C bus number where BS is connected to
 *
 * Returns:
 *     A valid file descriptor if connection is successful, -1 otherwise
 */
int bs_connect(int i2c_bus)
{
    int file;
    char path[25];

    snprintf(path, sizeof(path), "/dev/i2c-%d", i2c_bus);

    if ((file = open(path, O_RDWR)) < 0) 
        return -1;

    if (ioctl(file, I2C_SLAVE, BS_I2C_ADDR) < 0) {
        close(file);
        return -1;
    }

    return file;
}


/* Close BS connection
 *
 * Parameters:
 *    dev: BS file descriptor
 */
void bs_disconnect(int dev)
{
    close(dev);
}


/* Get BS information
 *
 * Parameters:
 *    dev: BS file descriptor
 *    *info: Where BS info will be stored
 *
 * Returns:
 *    0 if info could be retrieved, 1 otherwise
 */
int bs_get_info(int dev, bs_info_t *info)
{
    unsigned char reg;

    /* FW Version */
    reg = REG_VERSION;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->fw_ver, 1) != 1)
        return 1;
    
    /* Status */
    reg = REG_STATUS;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->status, 1) != 1)
        return 1;

    /* Current (ILIM) */
    reg = REG_ILIM;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->ilim, 1) != 1)
        return 1;

    /* Current (ULIM) */
    reg = REG_ULIM;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->ulim, 1) != 1)
        return 1;

    /* Battery type */
    reg = REG_BATT_TYPE;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->batt_type, 1) != 1)
        return 1;

    /* Test mode */
    reg = REG_TEST_MODE;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->test_mode, 1) != 1)
        return 1;

    /* FG Failures */
    reg = REG_FAIL_FG;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->fg_failures, 1) != 1)
        return 1;

    /* WDT Resets */
    reg = REG_FAIL_WDTR;
    if (write(dev, (void*)&reg, 1) != 1)
        return 1;
    if (read(dev, (void*)&info->wdt_resets, 1) != 1)
        return 1;
    return 0;
}


/* Set device wakeup (SW1 pulse)
 *
 * Note:
 *    Wakeup resolution is 8s, will be adjusted (lower bound)
 *
 * Parameters:
 *    dev: BS file descriptor
 *    wakeup: Wakeup time (s)
 *
 * Returns:
 *    0 if wakeup was set, 1 otherwise
 */
int bs_set_wakeup(int dev, unsigned short int wakeup)
{
    unsigned char buf[3];

    wakeup /= REG_WAKEUP_INTERVAL;
    
    buf[0] = REG_WAKEUP_L;
    buf[1] = wakeup;
    buf[2] = wakeup >> 8;
    if (write(dev, (void*)buf, 3) != 3)
        return 1;

    return 0;
}


/* Program battery type
 *
 * Parameters:
 *    dev: BS file descriptor
 *    type: battery type
 *
 * Returns:
 *    0 on success, 1 otherwise
 */
int bs_program_batt_type(int dev, unsigned char type, unsigned char password)
{
    unsigned char buf[2];
    
    buf[0] = REG_BATT_TYPE;
    
    switch (type)
    {
        case REG_BATT_TYPE_2000MAH:
        case REG_BATT_TYPE_3000MAH:
            buf[1] = type;
            break;
        default:
            return 1;
    }

    /* Enable lock access */
    if (bs_lock_access(dev, password))
        return 1;

    if (write(dev, (void*)buf, 2) != 2)
        return 1;

    /* Disable lock access */
     if (bs_lock_access(dev, password))
        return 1;   

    return 0;
}


/* Set test mode
 *
 * Parameters:
 *    dev: BS file descriptor
 *    mode: Test mode (bit field)
 *
 * Returns:
 *    0 on success, 1 otherwise
 */
int bs_set_test_mode(int dev, unsigned char mode)
{
    unsigned char buf[2];
    
    buf[0] = REG_TEST_MODE;
    buf[1] = mode & REG_TEST_MODE_MASK;

    /* Enable lock access */
    if (bs_lock_access(dev, REG_PASSWORD_PW))
        return 1;

    if (write(dev, (void*)buf, 2) != 2)
        return 1;

    /* Disable lock access */
     if (bs_lock_access(dev, 0))
        return 1;   

    return 0;
}

