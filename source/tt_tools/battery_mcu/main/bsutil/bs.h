/* 
 * Description:
 *     BatteryStick Interface Library
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */



#ifndef BS_H_
#define BS_H_

/************************************************************************/
/* PUBLIC TYPES & DEFINITIONS                                           */
/************************************************************************/

/* REG_WAKEUP */
#define REG_WAKEUP_INTERVAL              8

/* REG_STATUS Fields */
#define REG_STATUS_EXTMIC                (1 << 0)
#define REG_STATUS_BATT_FORCED_DISCHARGE (1 << 1)
#define REG_STATUS_BATT_CHARGING         (1 << 2)
#define REG_STATUS_BATT_UBRIDGE_POWERED  (1 << 3)

/* REG_ILIM Currents */
#define REG_ILIM_500MA                   0x00
#define REG_ILIM_900MA                   0x10
#define REG_ILIM_1200MA                  0x30

/* REG_ULIM Currents */
#define REG_ULIM_500MA                   0x01
#define REG_ULIM_1500MA                  0x80

/* REG_BATT_TYPE / REGE_PROG_BATT_TYPE */
#define REG_BATT_TYPE_2000MAH            0x00
#define REG_BATT_TYPE_3000MAH            0x01

/* REG_TEST_MODE / REGE_SET_TEST_MODE */
#define REG_TEST_MODE_USB2               (1 << 0)
#define REG_TEST_MODE_USB3               (1 << 1)
#define REG_TEST_MODE_MASK               (REG_TEST_MODE_USB2 | REG_TEST_MODE_USB3)

/* BS Info */
typedef struct _bs_info_t
{
    unsigned char fw_ver;
    unsigned char status;
    unsigned char ilim;
    unsigned char ulim;
    unsigned char batt_type;
    unsigned char test_mode;
    unsigned char fg_failures;
    unsigned char wdt_resets;
} bs_info_t;


/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

int bs_connect(int i2c_bus);
void bs_disconnect(int dev);
int bs_get_info(int dev, bs_info_t *info);
int bs_set_wakeup(int dev, unsigned short int wakeup);
int bs_program_batt_type(int dev, unsigned char type, unsigned char password);
int bs_set_test_mode(int dev, unsigned char mode);

#endif
