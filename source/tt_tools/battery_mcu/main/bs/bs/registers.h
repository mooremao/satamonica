/*
 * Description:
 *     TWI Registers
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */
#ifndef REGISTERS_H_
#define REGISTERS_H_

/************************************************************************/
/* PUBLIC DEFINITIONS                                                   */
/************************************************************************/

/* Access: R */
#define REG_VERSION                     0x00
#define REG_STATUS                      0x01
#define REG_ILIM                        0x02
#define REG_ULIM                        0x03
#define REG_FAIL_FG                     0x04
#define REG_FAIL_WDTR                   0x05
/* Access: R/W */
#define REG_WAKEUP_L                    0x06
#define REG_WAKEUP_H                    0x07
#define REG_PASSWORD                    0x08
/* Access: R/(W + Password) */
#define REG_BATT_TYPE                   0x09
#define REG_TEST_MODE                   0x0A

/* Access: R */
#define REG_STATUS2                     0x0B
#define REG_INTERRUPT                   0x0C
#define REG_FAIL_TWI                    0x0D

/* Access: R/(W + Password) */
#define REG_FACTORY                     0x0E
#define REG_USB_POWERON                 0x0F

/* BS Interface Registers */
#define NOOF_REGISTERS                  0x10

/* REG_WAKEUP */
#define REG_WAKEUP_INTERVAL             8

/* REG_STATUS Fields */
#define REG_STATUS_EXTMIC                   (1 << 0)
#define REG_STATUS_BATT_FORCED_DISCHARGE    (1 << 1)
#define REG_STATUS_POWER_PRESENT            (1 << 2)
#define REG_STATUS_USB_BRIDGE_POWERED       (1 << 3)
#define REG_STATUS_SOC1_THRESHOLD           (1 << 4)
#define REG_STATUS_SOCF_THRESHOLD           (1 << 5)
#define REG_STATUS_USB2_POWER               (1 << 6)
#define REG_STATUS_USB3_POWER               (1 << 7)

/* REG_ILIM Currents */
#define REG_ILIM_500MA                  0x00
#define REG_ILIM_900MA                  0x10
#define REG_ILIM_1200MA                 0x30

/* REG_ULIM Currents */
#define REG_ULIM_500MA                  0x01
#define REG_ULIM_1500MA                 0x80

/* REG_BATT_TYPE / REGE_PROG_BATT_TYPE - Battery types */
#define REG_BATT_TYPE_2000MAH           0x00
#define REG_BATT_TYPE_3000MAH           0x01

#define REG_BATT_TYPE_INVALID           0xFF

/* REG_TEST_MODE / REGE_SET_TEST_MODE - Test modes */
#define REG_TEST_MODE_USB2              (1 << 0)
#define REG_TEST_MODE_USB3              (1 << 1)
#define REG_TEST_MODE_BATT_DISCHARGE	(1 << 7)
#define REG_TEST_MODE_MASK              (REG_TEST_MODE_USB2 | REG_TEST_MODE_USB3 | REG_TEST_MODE_BATT_DISCHARGE)

/* REG_FACTORY */
#define REG_FACTORY_MODE                0xFF

/* REG_USB_POWERON */
#define REG_USB_POWERON_DISABLE         0x00
#define REG_USB_POWERON_ENABLE          0x01

/* REG_STATUS2 Fields */
#define REG_STATUS2_BATT_TEMP_OK        (1 << 3)

/* REG_INTERRUPT Fields */
#define REG_INT_EXTMIC_CHANGE           (1 << 0)
#define REG_INT_WAKEUP_TIMER_TIMEOUT    (1 << 1)
#define REG_INT_POWER_PRESENT_CHANGE    (1 << 2)
#define REG_INT_BATT_TEMP_CHANGE        (1 << 3)
#define REG_INT_FG_SOC1_CHANGE          (1 << 4)
#define REG_INT_FG_SOCF_CHANGE          (1 << 5)
#define REG_INT_USB_POWERON             (1 << 6)
#define REG_INT_SW_PRESSED              (1 << 7)

#define INTERRUPT_STATUS_MASK           (REG_INT_FG_SOCF_CHANGE | REG_INT_FG_SOC1_CHANGE | REG_INT_POWER_PRESENT_CHANGE | REG_INT_EXTMIC_CHANGE)
#define INTERRUPT_STATUS2_MASK          (REG_INT_BATT_TEMP_CHANGE)

/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

void registers_init(void);
void registers_update(void);
uint8_t registers_get(uint8_t reg);
void registers_set(uint8_t reg, uint8_t value);

#endif
