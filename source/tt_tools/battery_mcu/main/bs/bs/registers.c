/*
 * Description:
 *     TWI Registers
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */
#include <avr/eeprom.h>
#include <platform/io.h>
#include <board/pins.h>
#include <platform/events.h>
#include <components/charger.h>
#include "config.h"
#include "registers.h"
#include <components/fg.h>

/* TWI Slave interface */
const uint8_t twi_sl_noofregs = NOOF_REGISTERS;
const uint8_t twi_sl_rdonly = REG_FAIL_WDTR;
const uint8_t twi_sl_lock = REG_PASSWORD;
const uint8_t twi_sl_pw = BS_PASSWORD;
const uint8_t twi_sl_status2 = REG_STATUS2;
const uint8_t twi_sl_interrupt = REG_INTERRUPT;
const uint8_t twi_sl_factory = REG_FACTORY;

uint8_t volatile twi_sl_regs[NOOF_REGISTERS];

static uint8_t prev_reg_status , prev_reg_status2;

static uint8_t batt_type;
static uint8_t last_test_mode;
static uint8_t factory_flag;
static uint8_t usb_poweron_flag;

extern fg_info_t batt_info;

/************************************************************************/
/* PUBLIC FUNCTIONS                                                     */
/************************************************************************/

/* Initialize BS TWI registers */
void registers_init()
{
	/* FW Version */
	twi_sl_regs[REG_VERSION] = FW_VER;
	/* Wakeup */
	twi_sl_regs[REG_WAKEUP_L] = 0;
	twi_sl_regs[REG_WAKEUP_H] = 0;
	/* Failures */
	twi_sl_regs[REG_FAIL_FG] = eeprom_read_byte((uint8_t*)FAILURE_FG_EE_ADDR);
	twi_sl_regs[REG_FAIL_WDTR] = eeprom_read_byte((uint8_t*)FAILURE_WDT_EE_ADDR);
	/* Password */
	twi_sl_regs[REG_PASSWORD] = 0;

	/* Battery type */
	twi_sl_regs[REG_BATT_TYPE] = REG_BATT_TYPE_INVALID;
	/* to force a recheck during registers_update */
	batt_type = REG_BATT_TYPE_INVALID -1;
	/* Test mode */
	twi_sl_regs[REG_TEST_MODE] = 0;
	last_test_mode = 0;
	/* Factory flag */
	twi_sl_regs[REG_FACTORY] = eeprom_read_byte((uint8_t*)CFG_FACTORY_ADDR);
	factory_flag = twi_sl_regs[REG_FACTORY];
	/* USB power on flag */
	twi_sl_regs[REG_USB_POWERON] = eeprom_read_byte((uint8_t*)CFG_USB_POWER_ON_ADDR);
	usb_poweron_flag = twi_sl_regs[REG_USB_POWERON];

	twi_sl_regs[REG_STATUS] = 0;							// assume nothing connected, nothing active, no problems
	twi_sl_regs[REG_STATUS2] = REG_STATUS2_BATT_TEMP_OK;

	prev_reg_status = twi_sl_regs[REG_STATUS];
	prev_reg_status2 = twi_sl_regs[REG_STATUS2];

	twi_sl_regs[REG_INTERRUPT] = 0;

	twi_sl_regs[REG_FAIL_TWI] = eeprom_read_byte((uint8_t*)FAILURE_TWI_EE_ADDR);

	registers_update();
}

/* Update BS TWI registers */
void registers_update()
{
	uint8_t  current_reg_status, current_reg_status2 , temp;		//used so that any changes to the volatile registers during testing are ignored until next loop

	/* REG_STATUS */
	twi_sl_regs[REG_STATUS] =
		/* Bit 1: Forced discharge active */
		((pin_is_high(BATT_DISCHARGE_PIN, BATT_DISCHARGE)) ? REG_STATUS_BATT_FORCED_DISCHARGE : 0) |
		/* Bit 2: Charger external power present */
		((pin_is_low(PGn_PIN, PGn)) ? REG_STATUS_POWER_PRESENT : 0) |
		/* Bit 3: USB Bridge powered */
		((pin_is_high(THREEV3_ON_PIN, THREEV3_ON)) ? REG_STATUS_USB_BRIDGE_POWERED : 0) |

		/*Bit 4 : FG Battery Low Threshold*/
		((batt_info.flags & FG_FLAG_SOC1) ? REG_STATUS_SOC1_THRESHOLD : 0 ) |

		/*Bit 5 : FG Battery Low Threshold*/
		((batt_info.flags & FG_FLAG_SOCF) ? REG_STATUS_SOCF_THRESHOLD : 0 ) |

		/* Bit 7: USB3 Power present */
		((pin_is_high(USB3_DET_5V_PIN, USB3_DET_5V)) ? REG_STATUS_USB3_POWER : 0);

	if (pin_is_low(BATT_LOCKED_DET_MCU_PIN,BATT_LOCKED_DET_MCU)) {											//if BattStick inserted into camera, then do check

		twi_sl_regs[REG_STATUS] |=
			/* Bit 0: EXTMIC Present */
			((pin_is_high(EXTMIC_DET_PIN, EXTMIC_DET)) ? REG_STATUS_EXTMIC : 0) |

//USB2 power present is valid if PG signal asserted and BATT_LOCK_DET_MCU indicates it in the product => can't connect USB3
// , therefore only 1 external source possible : USB2!!

			/* Bit 6: USB2 Power present */
			((pin_is_low(PGn_PIN, PGn)) ? REG_STATUS_USB2_POWER : 0);
	}


	/* REG_ILIM */
	twi_sl_regs[REG_ILIM] = charger_get_ilim();

	/* REG_ULIM */
	twi_sl_regs[REG_ULIM] = charger_get_ulim();

	/* REG_BATT_TYPE */
	if (twi_sl_regs[REG_BATT_TYPE] != batt_type)
	{
		temp = eeprom_read_byte((uint8_t*)BATT_TYPE_EE_ADDR);
		if (temp >= BATT_TYPE_AVAIL_CFGS){
			if (twi_sl_regs[REG_BATT_TYPE] < BATT_TYPE_AVAIL_CFGS)
			{
				eeprom_write_byte((uint8_t*)BATT_TYPE_EE_ADDR, twi_sl_regs[REG_BATT_TYPE]);
				events_set(EVENT_UPDATE_FG);
			}
		}

		/* Battery type */
		batt_type = eeprom_read_byte((uint8_t*)BATT_TYPE_EE_ADDR);

		if (batt_type >= BATT_TYPE_AVAIL_CFGS){
			twi_sl_regs[REG_BATT_TYPE] = REG_BATT_TYPE_INVALID;
		} else {
			twi_sl_regs[REG_BATT_TYPE] = batt_type;
		}
	}

	/* REG_TEST_MODE */
	if (twi_sl_regs[REG_TEST_MODE] != last_test_mode)
	{
		last_test_mode = twi_sl_regs[REG_TEST_MODE];
		events_set(EVENT_RECONFIGURE_CHRG);
	}

	/* REG_FACTORY */
	if (twi_sl_regs[REG_FACTORY] != factory_flag) {
		factory_flag = twi_sl_regs[REG_FACTORY];
		eeprom_write_byte((uint8_t *)CFG_FACTORY_ADDR, factory_flag);
	}

	/* REG_USB_POWERON */
	if (twi_sl_regs[REG_USB_POWERON] != usb_poweron_flag) {
		usb_poweron_flag = twi_sl_regs[REG_USB_POWERON];
		eeprom_write_byte((uint8_t *)CFG_USB_POWER_ON_ADDR, usb_poweron_flag);
	}

	/* REG_STATUS2 */
	twi_sl_regs[REG_STATUS2] = ( (batt_info.flags & (FG_FLAG_OVERTEMP|FG_FLAG_UNDERTEMP)) ? 0 : REG_STATUS2_BATT_TEMP_OK);

	/* REG_INTERRUPT */
	current_reg_status = twi_sl_regs[REG_STATUS];						//take a copy and work from that
	current_reg_status2 = twi_sl_regs[REG_STATUS2];

	prev_reg_status ^= current_reg_status ;			// find differences . don't damage current_reg_status
	prev_reg_status &= INTERRUPT_STATUS_MASK;

	prev_reg_status2 ^= current_reg_status2 ;		// find differences . don't damage current_reg_status2
	prev_reg_status2 &= INTERRUPT_STATUS2_MASK;

	twi_sl_regs[REG_INTERRUPT] |= (prev_reg_status | prev_reg_status2);

	prev_reg_status = current_reg_status;			//here we update with the current copy rather than volatile original register, incase it changed during testing
	prev_reg_status2 = current_reg_status2;
}

/* Get register content
 *
 * Arguments:
 *     reg: register
 *
 * Returns:
 *    register content
 */
uint8_t registers_get(uint8_t reg)
{
//	if (reg < NOOF_REGISTERS)			no test for out-of-bounds to save code space. invalid number will be returned, but no corruption of memory
		return twi_sl_regs[reg];
}

/* Set register content
 *
 * Arguments:
 *     reg: register
 *     value: value to be written
 */
void registers_set(uint8_t reg, uint8_t value)
{
	if (reg < NOOF_REGISTERS)
		twi_sl_regs[reg] = value;
}
