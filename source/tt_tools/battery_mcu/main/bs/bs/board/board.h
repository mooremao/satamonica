/* 
 * Description:
 *     Board specific code
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef BOARD_H_
#define BOARD_H_

/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

void board_init(void);


#endif