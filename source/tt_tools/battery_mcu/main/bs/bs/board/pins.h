/*
 * Description:
 *     Board pins
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *	   Hadyn van der Berg (hadyn.vanderberg@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef PINS_H_
#define PINS_H_

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include "config.h"

/* GPOUT: Fuel Charge Interrupt */
#define GPOUT_DDR                        DDRA
#define GPOUT_PORT                       PORTA
#define GPOUT_PIN                        PINA
#define GPOUT                            _BV(PINA0)

/* SDCARD_DET: */
#define SDCARD_DET_DDR                   DDRA
#define SDCARD_DET_PORT                  PORTA
#define SDCARD_DET_PIN                   PINA
#define SDCARD_DET                       _BV(PINA1)

/* SW1: ON Push-button */
#define SW1_DDR                          DDRA
#define SW1_PORT                         PORTA
#define SW1_PIN                          PINA
#define SW1                              _BV(PINA2)

/* EXTMIC_DET: External mic present */
#define EXTMIC_DET_DDR                  DDRA
#define EXTMIC_DET_PORT                 PORTA
#define EXTMIC_DET_PIN                  PINA
#define EXTMIC_DET                      _BV(PINA3)



/* IUSBx: BQ24166 USB Current Limit Control */
#define IUSB_SET_DDR                     DDRB
#define IUSB_SET_PORT                    PORTB
#define IUSB_SET_PIN                     PINB
#define IUSB_SET1                        _BV(PINB0)
#define IUSB_SET2                        _BV(PINB7)
#define IUSB_SET3                        _BV(PINB2)
#define IUSB_SET_MASK                    (IUSB_SET1 | IUSB_SET2 | IUSB_SET3)

/* ILIMx: IN Current Limit Control */
#define CHARG_ILIM_DDR                   DDRB
#define CHARG_ILIM_PORT                  PORTB
#define CHARG_ILIM_PIN                   PINB
#define CHARG_ILIM                       _BV(PINB4)
#define CHARG_ILIM2                      _BV(PINB5)
#if	BattStickPCB == BS_BOARD_WS2
#define CHARG_ILIM_MASK                  (CHARG_ILIM | CHARG_ILIM2)
#elif BattStickPCB == BS_BOARD_PR1
#define CHARG_ILIM_MASK                  (CHARG_ILIM)
#endif

/* BATT_DISCHARGE: High-current discharge of cells */
#define BATT_DISCHARGE_DDR               DDRB
#define BATT_DISCHARGE_PORT              PORTB
#define BATT_DISCHARGE_PIN               PINB
#define BATT_DISCHARGE                   _BV(PINB1)

/* HOST_INT: */
#define HOST_INT_DDR                     DDRB
#define HOST_INT_PORT                    PORTB
#define HOST_INT_PIN                     PINB
#define HOST_INT                         _BV(PINB6)


/* USB2_F1: (H: Charger connected, L: Normal USB) */
#define USB2_F1_DDR                      DDRC
#define USB2_F1_PORT                     PORTC
#define USB2_F1_PIN                      PINC
#define USB2_F1	                         _BV(PINC0)

/* THREEV3_ON: Control of the 3V regulator */
#define THREEV3_ON_DDR                   DDRC
#define THREEV3_ON_PORT                  PORTC
#define THREEV3_ON_PIN                   PINC
#define THREEV3_ON                       _BV(PINC1)

/* USB3_F1: (H: Charger connected, L: Normal USB) */
#define USB3_F1_DDR                      DDRC
#define USB3_F1_PORT                     PORTC
#define USB3_F1_PIN                      PINC
#define USB3_F1	                         _BV(PINC2)

/* nCHRG_EN: Charger enable */
#define nCHRG_EN_DDR                     DDRC
#define nCHRG_EN_PORT                    PORTC
#define nCHRG_EN_PIN                     PINC
#define nCHRG_EN                         _BV(PINC3)

#if	BattStickPCB == BS_BOARD_WS2
/* FORCE_VDPM: Disable charger's IN input */
#define FORCE_VDPM_DDR                   DDRC
#define FORCE_VDPM_PORT                  PORTC
#define FORCE_VDPM_PIN                   PINC
#define FORCE_VDPM		                 _BV(PINC7)
#elif BattStickPCB == BS_BOARD_PR1

#define MCU_SD_CDn_DDR                   DDRC
#define MCU_SD_CDn_PORT                  PORTC
#define MCU_SD_CDn_PIN                   PINC
#define MCU_SD_CDn		                 _BV(PINC7)

#else
#error	"no board definition included"
#endif




/* LEDx: Battery status indicators */
#define LED_DDR                          DDRD
#define LED_PORT                         PORTD
#define LED_PIN                          PIND
#define LED1	                         _BV(PIND0)
#define LED2	                         _BV(PIND1)
#define LED3	                         _BV(PIND2)
#define LED4	                         _BV(PIND3)
#define LED_MASK                         (LED1 | LED2 | LED3 | LED4)

/* USB_DET_5V: 5V Input from USB3 */
#define USB3_DET_5V_DDR                   DDRD
#define USB3_DET_5V_PORT                  PORTD
#define USB3_DET_5V_PIN                   PIND
#define USB3_DET_5V	                     _BV(PIND4)

/* PGn: 5V Input valid */
#define PGn_DDR                          DDRD
#define PGn_PORT                         PORTD
#define PGn_PIN                          PIND
#define PGn 	                         _BV(PIND5)

/* USB3_ACTIVE: USB3 connected */
#define USB3_ACTIVE_DDR                  DDRD
#define USB3_ACTIVE_PORT                 PORTD
#define USB3_ACTIVE_PIN                  PIND
#define USB3_ACTIVE	                     _BV(PIND6)

/* BATT_LOCKED_DET_MCU: Indicates plugged into main body */
#define BATT_LOCKED_DET_MCU_DDR          DDRD
#define BATT_LOCKED_DET_MCU_PORT         PORTD
#define BATT_LOCKED_DET_MCU_PIN          PIND
#define BATT_LOCKED_DET_MCU	             _BV(PIND7)

/* UART: Software UART */
#define UART_DDR                        DDRB
#define UART_PORT                       PORTB
#define UART_PIN                        PINB
#define UART_DATA                       _BV(PINB5)

/* Unused Pins */
#define UNUSED_PINB3                     _BV(PINB3)

#endif
