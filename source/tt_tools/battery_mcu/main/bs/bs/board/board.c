/*
 * Description:
 *     Board specific code
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <platform/io.h>
#include "board.h"
#include "pins.h"
#include "config.h"

/* Board initialization function */
void board_init()
{
	/* Pin-Change Interrupts */
	PCMSK3 = GPOUT | SDCARD_DET | SW1 | EXTMIC_DET;									// PORTA
	PCMSK2 = USB3_DET_5V | PGn | USB3_ACTIVE | BATT_LOCKED_DET_MCU;			// PORTD
	PCMSK1 = USB2_F1 | THREEV3_ON | USB3_F1;								// PORTC
	PCMSK0 |= 0;															// PORTB

	PCICR = _BV(PCIE3) | _BV(PCIE2) | _BV(PCIE1);

	/* GPIO
	 * Notes:
	 *     - Unused pins MUST be initialized to Input-Pullup! (Some are HW controlled, e.g. TWI)
	 *     - Cannot switch from Tri-State (DDR,PORT=00) to Output-High (DDR,PORT=11)!
	 *     - Cannot switch from Input-Pullup (DDR,PORT=01) to Output-Low (DDR,PORT=10)!
	 */

#if	BattStickPCB == BS_BOARD_WS2
	// PORTD
	PORTD = HI(LED4) | HI(LED3)| HI(LED2) | HI(LED1);
	DDRD = OUT(LED4) | OUT(LED3) | OUT(LED2) | OUT(LED1) | INP(USB3_DET_5V) | INP(PGn) | INP(USB3_ACTIVE) | INP(BATT_LOCKED_DET_MCU);
	PORTD |= PUDI(USB3_DET_5V) | PUEN(PGn) | PUEN(USB3_ACTIVE) | PUEN(BATT_LOCKED_DET_MCU);

#pragma message  "Compiled for WS2 boards"
#elif BattStickPCB == BS_BOARD_PR1
	// PORTD
	PORTD = HI(LED4) | HI(LED3)| HI(LED2) | HI(LED1);
	DDRD = OUT(LED4) | OUT(LED3) | OUT(LED2) | OUT(LED1) | INP(USB3_DET_5V) | INP(PGn) | INP(USB3_ACTIVE) | INP(BATT_LOCKED_DET_MCU);
	PORTD |= PUDI(USB3_DET_5V) | PUEN(PGn) | PUDI(USB3_ACTIVE) | PUDI(BATT_LOCKED_DET_MCU);

#pragma message  "Compiled for PR1 boards"

#else
#error	"no board definition included"
#endif


	// PORTB - Note: HOST_INT pin floating, driven to GND when active.
#if	BattStickPCB == BS_BOARD_WS2
	PORTB = LO(IUSB_SET3) | LO(IUSB_SET2) | HI(IUSB_SET1) | LO(CHARG_ILIM2) | LO(CHARG_ILIM) | LO(BATT_DISCHARGE);
	DDRB = OUT(IUSB_SET3) | OUT(IUSB_SET2) | OUT(IUSB_SET1) | OUT(CHARG_ILIM2) | OUT(CHARG_ILIM) | OUT(BATT_DISCHARGE) | INP(HOST_INT) | INP(UNUSED_PINB3);
	PORTB |= PUDI(HOST_INT) | PUEN(UNUSED_PINB3);

#elif BattStickPCB == BS_BOARD_PR1
#if	ENABLE_SW_UART
	PORTB = LO(IUSB_SET3) | LO(IUSB_SET2) | HI(IUSB_SET1) | HI(UART_DATA) | LO(CHARG_ILIM) | LO(BATT_DISCHARGE);
	DDRB = OUT(IUSB_SET3) | OUT(IUSB_SET2) | OUT(IUSB_SET1) | OUT(UART_DATA) | OUT(CHARG_ILIM) | OUT(BATT_DISCHARGE) | INP(HOST_INT) | INP(UNUSED_PINB3);
#else
	PORTB = LO(IUSB_SET3) | LO(IUSB_SET2) | HI(IUSB_SET1) | LO(UART_DATA) | LO(CHARG_ILIM) | LO(BATT_DISCHARGE);
	DDRB = OUT(IUSB_SET3) | OUT(IUSB_SET2) | OUT(IUSB_SET1) | OUT(UART_DATA) | OUT(CHARG_ILIM) | OUT(BATT_DISCHARGE) | INP(HOST_INT) | INP(UNUSED_PINB3);
#endif
	PORTB |= PUDI(HOST_INT) | PUEN(UNUSED_PINB3);
#endif

	// PORTA
	DDRA = INP(GPOUT) | INP(SDCARD_DET) | INP(SW1) | INP(EXTMIC_DET);
	PORTA |= PUDI(GPOUT) | PUDI(SDCARD_DET) | PUEN (SW1) | PUDI(EXTMIC_DET);


#if	BattStickPCB == BS_BOARD_WS2
	// PORTC
	PORTC = LO(THREEV3_ON) | LO(nCHRG_EN) | LO(FORCE_VDPM);
	DDRC = INP(USB2_F1) | OUT(THREEV3_ON) | INP(USB3_F1) | OUT(nCHRG_EN) | OUT(FORCE_VDPM);
	PORTC |= PUEN(USB2_F1) | PUEN(USB3_F1);

#elif BattStickPCB == BS_BOARD_PR1
	// PORTC
	PORTC = LO(THREEV3_ON) | LO(nCHRG_EN) ;
	DDRC = INP(USB2_F1) | OUT(THREEV3_ON) | INP(USB3_F1) | OUT(nCHRG_EN) | INP(MCU_SD_CDn);
	PORTC |= PUDI(USB2_F1) | PUDI(USB3_F1) | PUDI(MCU_SD_CDn);
#else
#error	"no board definition included"
#endif


}
