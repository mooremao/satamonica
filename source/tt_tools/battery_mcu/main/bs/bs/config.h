/*
 * Description:
 *     General Configuration
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Doug Steel (doug.steel@tomtom.com)
 *     Hadyn van der Berg (hadyn.vanderberg@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */
#ifndef CONFIG_H_
#define CONFIG_H_

#include <avr/io.h>
#include <platform/wdt.h>

/* Board revision */
#define BS_BOARD_WS2			1
#define BS_BOARD_PR1			2

#define BattStickPCB			BS_BOARD_PR1

/* Firmware version */
#define FW_VER				24


/* CPU Clock Speed */
#define F_CPU				4000000UL

/* TWI Clock Speed */
#define TWI_CLK				100000UL

/* Watchdog period */
#define WDT_DEFAULT_PERIOD		WDT_PER_8S

/* FG Update period
 * Note:
 *     WDTP = WDT_DEFAULT_PERIOD, so needs to be value/WDT_DEFAULT_PERIOD! */
#ifdef DEBUG
#define FG_UPDATE_PERIOD		8
#pragma message "change FG_UPDATE_PERIOD to 8 before RELEASE"

#else
#define FG_UPDATE_PERIOD		8
// BUG- debugging seems to pick up this value
#endif

/* Failure registers in EEPROM */
#define FAILURE_WDT_EE_ADDR		0x10
#define FAILURE_FG_EE_ADDR		0x20
#define FAILURE_TWI_EE_ADDR		0x22

/* Configuration registers in EEPROM */
#define CFG_FACTORY_ADDR		0x30
#define CFG_USB_POWER_ON_ADDR		0x32

/* TWI Slave parameters (Address and locked registers password) */
#define BS_TWI_ADDR			0x44
#define BS_PASSWORD			0x48
/* Releases both SDA and SCL if the I2C bus is held active for 2 seconds */
#define TWI_TIMEOUT			2

/* Soft Reset */
/* Long press time = 3 * 8 = 24 seconds */
#define SOFT_RESET_TIME			2

/* BATT_LOCKED_DET_MCU debounce */
#define BATT_UNLOCK_TIME		8

/* Wakeup (SW1) pulse width (ms) */
#define WAKEUP_PULSE_WIDTH		50

/* Battery failure retries and delays (ms) */
#define FG_FAIL_RETRIES			3
#define FG_FAIL_RETRY_DELAY		10

/* Battery types */
#define BATT_TYPE_EE_ADDR		0

#define BATT_TYPE_AVAIL_CFGS		2
#define BATT_TYPE_2000MAH		0
#define BATT_TYPE_3000MAH		1
#define BATT_TYPE_DEFAULT		BATT_TYPE_2000MAH

/* Capacity thresholds (in %) and display settings */
#define CAP_CRIT			5
#define CAP_LOW				20
#define CAP_MED				50
#define CAP_HIGH			80
#define CAP_FULL			100

#define CAP_DISP_DURATION		5000
#define CAP_DISP_DELAY			100

/* Failure blinks */
#define FAILURE_BLINKS			5
#define FAILURE_BLINKS_DURATION		1000

/* All LED blinks */
#define ALL_LEDS_BLINKS			1
#define ALL_LEDS_BLINKS_DURATION	2000

/* Display walk delay (ms) */
#define DISP_WALK_DELAY			200

/* System operation limits (impact forced discharge!) */

// High-Temperature threshold (0.1deg Celsius)
#define SYSTEM_HIGH_TEMP		600

// High-Capacity threshold (mV)
#define SYSTEM_HIGH_CAP			4000

#define ENABLE_SW_UART			0

#endif
