/* 
 * Description:
 *     LED "display" driver
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef DISP_H_
#define DISP_H_

/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

void disp_off(void);
void disp_blink(uint8_t blinks, uint16_t duration);
void disp_walk(void);
void disp_show_capacity(uint8_t capacity);



#endif