/*
 * Description:
 *     TI BQ27421 Fuel Gauge Driver
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *		Hadyn van der Berg (hadyn.vanderberg@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <avr/io.h>
#include <string.h>
#include <stdbool.h>
#include <platform/twi.h>
#include "config.h"
#include <util/delay.h>
#include "fg.h"

/************************************************************************/
/* PRIVATE DEFINITIONS                                                  */
/************************************************************************/

/* BQ27421 TWI Address */
#define FG_TWI_ADDR				0xAA

/* Commands (only used commands/fields are present!) */
#define CMD_CONTROL				0x00
// Control sub-commands
#define SUBCMD_CONTROL_CONTROL_STATUS		0x0000
#define SUBCMD_CONTROL_BAT_INSERT		0x000C
#define SUBCMD_CONTROL_SET_CFGUPDATE		0x0013
#define SUBCMD_CONTROL_SOFT_RESET		0x0042
#define SUBCMD_CONTROL_SEALED			0x0020
#define UNSEAL_KEY				0x8000

#define CMD_TEMPERATURE				0x02
#define CMD_VOLTAGE				0x04

#define CMD_FLAGS				0x06


#define CMD_SOC					0x1C
//#define CMD_SOH				0x20
#define CMD_AVERAGE_CURRENT			0x10

/* Extended Data Commands */
#define EDCMD_OPCONFIG				0x3A
#define EDCMD_DESIGNCAPACITY			0x3C
#define EDCMD_DATA_CLASS			0x3E
#define EDCMD_DATA_BLOCK			0x3F
#define EDCMD_BLOCK_DATA_BASE			0x40
#define EDCMD_BLOCK_DATA_MAX			0x5F
#define EDCMD_BLOCK_DATA_CHECKSUM		0x60
#define EDCMD_BLOCK_DATA_CONTROL		0x61

// Data Block size
#define DATA_BLOCK_SIZE				32


/*** Data Memory ***/
/** Subclasses **/

/* REGISTERS */
#define DM_SUBCLASS_REGISTERS			64
// Registers offsets
#define DM_OFF_REGISTERS_OPCONFIG		0
// Registers sizes
#define DM_SIZE_REGISTERS_OPCONFIG		2

/* STATE */
#define DM_SUBCLASS_STATE			82
// State offsets
#define DM_OFF_STATE_DESIGN_CAPACITY		10
#define DM_OFF_STATE_DESIGN_ENERGY		12
#define DM_OFF_STATE_TERM_VOLTAGE		16
#define DM_OFF_STATE_TAPER_RATE			27
// State sizes
#define DM_SIZE_STATE_DESIGN_CAPACITY		2
#define DM_SIZE_STATE_DESIGN_ENERGY		2
#define DM_SIZE_STATE_TERM_VOLTAGE		2
#define DM_SIZE_STATE_TAPER_RATE		2

#define FG__ACCESS_TIME				1	/* 1 ms */

/************************************************************************/
/* PRIVATE FUNCTIONS                                                    */
/************************************************************************/

/* Read N bytes from BQ27421
 *
 * Parameters:
 *     cmd: Command
 *     buf: Buffer where data will be stored
 *     buf_size: Buffer size
 *
 * Returns:
 *     true if data could be read, false otherwise
 */
static bool fg_read(uint8_t cmd, uint8_t *buf, uint8_t buf_size)
{
	return twi_master_transfer(FG_TWI_ADDR, &cmd, sizeof(uint8_t), (uint8_t*)buf, buf_size);
}

/* Write 1 or 2 byte/s to BQ27421
 *
 * Notes:
 *     A maximum of 2 bytes are written all over the code, hence this implementation
 *
 * Parameters:
 *     cmd: Command
 *     data: Command data
 *     data_size: Command data size (1 or 2)
 *
 * Returns:
 *     true if transfer is successful
 *     false if there is a transfer error or data_size is invalid
 */
static bool fg_write(uint8_t cmd, const uint16_t data, uint8_t data_size)
{
	uint8_t buf[3];

	if (data_size > 3)
		return false;

	/* CMD (1-byte) + Data */
	buf[0] = cmd;
	buf[1] = data;
	buf[2] = data >> 8;

	return twi_master_transfer(FG_TWI_ADDR, buf, data_size + 1, NULL, 0);
}

/* Program Data Memory
 *
 * Notes:
 *     Memory needs to be UNSEALED prior to program data memory!
 *     Checksum could be calculated using replacement method as shown in (3.1)
 *
 * Parameters:
 *     subclass: Memory subclass
 *     offset: Memory offset from EDCMD_BLOCK_DATA_BASE (0x40)
 *     data: Memory data buffer
 *     data_size: Memory data size (max. DATA_BLOCK_SIZE)
 *
 * Returns:
 *     true if data memory is successfully programmed
 *     false if data memory cannot be programmed or data_size is invalid
 */
static bool fg_program_data_memory(uint8_t subclass, uint8_t offset, uint8_t *data, uint8_t data_size)
{
	uint8_t i, temp, csum = 0;

	/* Setup command */
	if(!fg_write(EDCMD_BLOCK_DATA_CONTROL, 0x00, 1))
		return false;

	/* Data Class */
	if(!fg_write(EDCMD_DATA_CLASS, subclass, sizeof(subclass)))
		return false;

	/* Block Offset */
	if(!fg_write(EDCMD_DATA_BLOCK, (offset > DATA_BLOCK_SIZE) ? 1 : 0, 1))
		return false;

	offset %= DATA_BLOCK_SIZE;

	/* Write Data Block (1-byte each time, if not fails!) */
	for (i = 0; i < data_size; i++)
	{
		_delay_ms(FG__ACCESS_TIME);
		if (!fg_write(EDCMD_BLOCK_DATA_BASE + offset + i, *(data + i), 1))
			return false;
	}

	/* Checksum */
	for (i = EDCMD_BLOCK_DATA_BASE; i <= EDCMD_BLOCK_DATA_MAX; i++)
	{
		if (!fg_read(i, &temp, sizeof(temp)))
			return false;

		csum += temp;
	}

	csum = 255 - csum;

	if(!fg_write(EDCMD_BLOCK_DATA_CHECKSUM, csum, sizeof(csum)))
		return false;

	return true;
}

/************************************************************************/
/* PUBLIC FUNCTIONS                                                     */
/************************************************************************/

/* Initialize BQ27421 FG
 *
 * Parameters:
 *     *cfg: FG configuration
 *
 * Returns:
 *     true if success, false if TWI comm. error
 */
bool fg_init(const fg_config_t *cfg)
{
	uint16_t data;

	/* Unseal data memory access */
	if(!(fg_write(CMD_CONTROL, UNSEAL_KEY, 2) &&
			fg_write(CMD_CONTROL, UNSEAL_KEY, 2)))
		return false;

	/* Request update mode */
	do
	{
		if(!fg_write(CMD_CONTROL, SUBCMD_CONTROL_SET_CFGUPDATE, 2))
			return false;

		if(!fg_read(CMD_FLAGS, (uint8_t*)&data, sizeof(data)))
			return false;

	} while (!(data & FG_FLAG_CFGUPMODE));

	/* Program (1): Design capacity */
	if(!fg_program_data_memory(DM_SUBCLASS_STATE, DM_OFF_STATE_DESIGN_CAPACITY,
			(uint8_t*)&cfg->capacity, DM_SIZE_STATE_DESIGN_CAPACITY))
		return false;

	/* Program (2): Design energy */
	if(!fg_program_data_memory(DM_SUBCLASS_STATE, DM_OFF_STATE_DESIGN_ENERGY,
			(uint8_t*)&cfg->energy, DM_SIZE_STATE_DESIGN_ENERGY))
		return false;

	/* Program (3): Terminate voltage */
	if(!fg_program_data_memory(DM_SUBCLASS_STATE, DM_OFF_STATE_TERM_VOLTAGE,
			(uint8_t*)&cfg->term_voltage, DM_SIZE_STATE_TERM_VOLTAGE))
		return false;

	/* Program (4) Taper rate */
	if(!fg_program_data_memory(DM_SUBCLASS_STATE, DM_OFF_STATE_TAPER_RATE,
			(uint8_t*)&cfg->taper_rate, DM_SIZE_STATE_TAPER_RATE))
		return false;

	/* Program (5): OpConfig values */
	if(!fg_program_data_memory(DM_SUBCLASS_REGISTERS, DM_OFF_REGISTERS_OPCONFIG,
			(uint8_t*)&cfg->opconfig, DM_SIZE_REGISTERS_OPCONFIG))
		return false;

	/* Perform Soft-Reset and wait until out of update mode */
	if (!fg_write(CMD_CONTROL, SUBCMD_CONTROL_SOFT_RESET, 2))
		return false;

	do
	{
		if (!fg_read(CMD_FLAGS, (uint8_t*)&data, sizeof(data)))
			return false;
	} while (data & FG_FLAG_CFGUPMODE);

	/* Seal data memory access */
	if (!fg_write(CMD_CONTROL, SUBCMD_CONTROL_SEALED, 2))
		return false;

	/* Battery insert */
	if (!fg_write(CMD_CONTROL, SUBCMD_CONTROL_BAT_INSERT, 2))
		return false;

	return true;
}

/* Read BQ27421 FG information (voltage, temp., SOC and Avg. current)
 *
 * Parameters:
 *     info: where FG information will be stored
 *
 * Returns:
 *     true if information could be retrieved, false otherwise
 */
bool fg_get_info(fg_info_t *info)
{
	/* Voltage */
	if(!fg_read(CMD_VOLTAGE, (uint8_t*)&info->voltage, sizeof(info->voltage)))
		return false;

	/* Temperature (+ convert from 0.1K to 0.1deg) */
	if(!fg_read(CMD_TEMPERATURE, (uint8_t*)&info->temperature, sizeof(info->temperature)))
		return false;

	info->temperature -= 2732;
	
	
#if 0			//27-aug-14 hadyn used for debugging FG
	// internal temp
	if(!fg_read(0x1E , (uint8_t*)&info->int_temperature, sizeof(info->int_temperature)))
		return false;

//	info->int_temperature -= 2732;
	// State Of health
	if(!fg_read(0x20, (uint8_t*)&info->soh, sizeof(info->soh)))
		return false;

	//average_power
	if(!fg_read(0x18, (uint8_t*)&info->avg_power, sizeof(info->avg_power)))
		return false;

	//standby_current
	if(!fg_read(0x12, (uint8_t*)&info->standby_current, sizeof(info->standby_current)))
		return false;

	//opconfig
	if(!fg_read(0x3A, (uint8_t*)&info->opconfig, sizeof(info->opconfig)))
		return false;
		
	// control status
	if(!fg_read(SUBCMD_CONTROL_CONTROL_STATUS, (uint8_t*)&info->control_status, sizeof(info->control_status)))
		return false;	
#endif				

	/* State Of Charge */
	if(!fg_read(CMD_SOC, (uint8_t*)&info->soc, sizeof(info->soc)))
		return false;

	/* Flags */
	if(!fg_read(CMD_FLAGS, (uint8_t*)&info->flags, sizeof(info->flags)))
		return false;

	/* Average current */
	if(!fg_read(CMD_AVERAGE_CURRENT, (uint8_t*)&info->avg_current, sizeof(info->avg_current)))
		return false;

#if 0  //27-aug-14 hadyn used for debugging FG
	if ((info->flags & FG_FLAG_BAT_DET) == 0){
		/* Battery insert */
		if (!fg_write(CMD_CONTROL, SUBCMD_CONTROL_BAT_INSERT, 2)) //SUBCMD_CONTROL_BAT_INSERT
		{
			return false;
		}
	}
#endif

	return true;
}
