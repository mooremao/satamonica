/*
 * Description:
 *     TI BQ24166 Charger Driver
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <avr/io.h>
#include <stdbool.h>
#include <platform/io.h>
#include <board/pins.h>
#include "charger.h"

/* Activate or deactivate charger
 *
 * Parameters:
 *     active: true to activate, false to deactivate
 */
void charger_set_active(bool active)
{
	if (active)
		pins_set_low(nCHRG_EN_PORT, nCHRG_EN);
	else
		pins_set_high(nCHRG_EN_PORT, nCHRG_EN);
}

/* Set BQ24166 USB Current Limit
 *
 * Parameters:
 *    ulim: USB current limit
 */
void charger_set_ulim(charger_ulim_t ulim)
{
	pins_set_low(IUSB_SET_PORT, IUSB_SET_MASK);
	pins_set_high(IUSB_SET_PORT, ulim);
}


/* Get BQ24166 USB Current Limit
 *
 * Returns:
 *    USB current limit
 */
charger_ulim_t charger_get_ulim()
{
	return (charger_ulim_t)(IUSB_SET_PIN & IUSB_SET_MASK);
}


/* Set BQ24166 IN Current Limit
 *
 * Parameters:
 *     ilim: IN Current limit
 */
void charger_set_ilim(charger_ilim_t ilim)
{
	pins_set_low(CHARG_ILIM_PORT, CHARG_ILIM_MASK);
	pins_set_high(CHARG_ILIM_PORT, ilim);
}


/* Get BQ24166 IN Current Limit
 *
 * Returns:
 *     ilim: IN Current limit
 */
charger_ilim_t charger_get_ilim()
{
	return (charger_ilim_t)(CHARG_ILIM_PIN & CHARG_ILIM_MASK);
}

