/*
 * Description:
 *     TI BQ27421 Fuel Gauge Driver
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *		Hadyn van der Berg (hadyn.vanderberg@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#ifndef FG_H_
#define FG_H_

/************************************************************************/
/* PUBLIC TYPES & DEFINITIONS                                           */
/************************************************************************/

/* Battery information */
typedef struct _fg_info_t
{
	/* Voltage (in mV) */
	uint16_t voltage;
	/* Temperature (in 0.1deg) */
	int16_t temperature;
//	int16_t int_temperature;			27-aug-14 hadyn used for debugging FG
	/* State Of Charge - SOC (%) */
	uint16_t soc;
//	uint16_t soh;						27-aug-14 hadyn used for debugging FG
	/* Average current (mA) (Note: signed current!) */
	int16_t avg_current;
	uint16_t flags;

//	int16_t standby_current;			27-aug-14 hadyn used for debugging FG
//	int16_t avg_power;			//in mW		27-aug-14 hadyn used for debugging FG
//	uint16_t opconfig;					27-aug-14 hadyn used for debugging FG
//	uint16_t control_status;			27-aug-14 hadyn used for debugging FG
} fg_info_t;


/* Battery configuration options */
typedef struct _fg_config_t
{
	/* Design Capacity (mAh) */
	uint16_t capacity;
	/* Energy (mWh) */
	uint16_t energy;
	/* Terminate voltage (mV) */
	uint16_t term_voltage;
	/* Taper rate (0.1Hr rate) */
	uint16_t taper_rate;
	/* OpConfig */
	uint16_t opconfig;
	//uint16_t opconfig1;			27-aug-14 hadyn used for debugging FG
	
} fg_config_t;

/* OpConfig Register fields */
#define FG_OPCONFIG_FILL_MASK                      ((1 << 3) | (1 << 6) | (1 << 7) | (1 << 8) | (1 << 10))
#define FG_OPCONFIG_TEMPS                          (1 << 0)
#define FG_OPCONFIG_BATLOWEN                       (1 << 2)
#define FG_OPCONFIG_RMFCC                          (1 << 4)
#define FG_OPCONFIG_SLEEP                          (1 << 5)
#define FG_OPCONFIG_GPIOPOL                        (1 << 11)
#define FG_OPCONFIG_BI_PU_EN                       (1 << 12)
#define FG_OPCONFIG_BIE                            (1 << 13)

/*Flag register fields */
#define FG_FLAG_OVERTEMP			(1<<15)
#define FG_FLAG_UNDERTEMP			(1<<14)
#define FG_FLAG_CFGUPMODE			(1 << 4)
#define FG_FLAG_BAT_DET				(1<<3)
#define FG_FLAG_SOC1				(1<<2)
#define FG_FLAG_SOCF				(1<<1)


/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

bool fg_init(const fg_config_t *cfg);
bool fg_get_info(fg_info_t *info);

#endif