/*
 * Description:
 *     TI BQ24166 Charger Driver
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Doug Steel (doug.steel@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef CHARGER_H_
#define CHARGER_H_

#include "config.h"
#include <stdbool.h>
#include <platform/io.h>
#include <board/pins.h>

/************************************************************************/
/* PUBLIC TYPES                                                         */
/************************************************************************/

#if	BattStickPCB == BS_BOARD_WS2
/* Charger USB input Current Limits - connects to 4pin USB2 external custom connector  */
typedef enum _charger_ulimit_t
{
//	ULIM_100MA =  LO(IUSB_SET3) | LO(IUSB_SET2) | LO(IUSB_SET1),
	ULIM_500MA =  LO(IUSB_SET3) | LO(IUSB_SET2) | HI(IUSB_SET1),
	ULIM_1500MA = LO(IUSB_SET3) | HI(IUSB_SET2) | LO(IUSB_SET1),
//	ULIM_HZ_1 =   LO(IUSB_SET3) | HI(IUSB_SET2) | HI(IUSB_SET1),
//	ULIM_150MA =  HI(IUSB_SET3) | LO(IUSB_SET2) | LO(IUSB_SET1),
	ULIM_900MA =  HI(IUSB_SET3) | LO(IUSB_SET2) | HI(IUSB_SET1),
//	ULIM_800MA =  HI(IUSB_SET3) | HI(IUSB_SET2) | LO(IUSB_SET1),
//	ULIM_HZ_2 =   HI(IUSB_SET3) | HI(IUSB_SET2) | HI(IUSB_SET1)
} charger_ulim_t;

/* Charger AC IN Input current (ILIM) limits - connects to USB3 connector */
typedef enum _charger_ilim_t
{
	ILIM_500MA =  LO(CHARG_ILIM) | LO(CHARG_ILIM2),
	ILIM_800MA =  LO(CHARG_ILIM) | HI(CHARG_ILIM2),
	ILIM_900MA =  HI(CHARG_ILIM) | LO(CHARG_ILIM2),
	ILIM_1200MA = HI(CHARG_ILIM) | HI(CHARG_ILIM2)
} charger_ilim_t;

#elif BattStickPCB == BS_BOARD_PR1
/* Charger USB input Current Limits  - connects to USB3 connector */
typedef enum _charger_ulimit_t
{
	ULIM_500MA =  LO(IUSB_SET3) | LO(IUSB_SET2) | HI(IUSB_SET1),
	ULIM_900MA =  HI(IUSB_SET3) | LO(IUSB_SET2) | HI(IUSB_SET1),
	ULIM_1500MA = LO(IUSB_SET3) | HI(IUSB_SET2) | LO(IUSB_SET1),

} charger_ulim_t;

/* Charger AC IN Input current (ILIM) limits - connects to 4pin USB2 external custom connector */
typedef enum _charger_ilim_t
{
	ILIM_500MA =  LO(CHARG_ILIM),
	ILIM_1500MA = HI(CHARG_ILIM),
} charger_ilim_t;
#else
#error	"no board definition included"
#endif

/* Charging indicator */
#define charger_is_charging()   pin_is_low(PGn_PIN, PGn)

/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

void charger_set_active(bool active);
void charger_set_ulim(charger_ulim_t ulim);
charger_ulim_t charger_get_ulim(void);
void charger_set_ilim(charger_ilim_t ilim);
charger_ilim_t charger_get_ilim(void);

//void configure_charger(void);			TODO:  26-aug-2014 this function is currently in main.C (having been moved from the main loop into a seperate function . should be moved to charger.C / .H

#endif