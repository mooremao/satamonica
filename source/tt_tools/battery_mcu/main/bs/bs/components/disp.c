/* 
 * Description:
 *     LED "display" driver
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <stdbool.h>
#include <avr/io.h>
#include <platform/io.h>
#include <board/pins.h>
#include "config.h"
#include "registers.h"
#include <util/delay.h>

/************************************************************************/
/* PRIVATE VARIABLES                                                    */
/************************************************************************/
static bool last_state = true;

/************************************************************************/
/* PUBLIC FUNCTIONS                                                     */
/************************************************************************/

/* Turn OFF display (and reset last state flag) */
void disp_off()
{
	last_state = true;

	pins_set_high(LED_PORT, LED1 | LED2 | LED3 | LED4);	
}

/* Blink display LEDs
 *
 * Notes:
 *     Delay is achieved in steps of 1ms as it requires a constant value
 *
 * Parameters:
 *     blinks: number of blinks
 *     duration: duration of each blink (ms)
 */
void disp_blink(uint8_t blinks, uint16_t duration)
{
	uint8_t i;
	uint16_t j;

	pins_set_high(LED_PORT, LED1 | LED2 | LED3 | LED4);

	for (i = 0; i < (2 * blinks); i++)
	{
		pins_toggle(LED_PORT, LED_MASK);

		for (j = 0; j < (duration / 2); j++)
			_delay_ms(1);
	}
}

/* Display walk */
void disp_walk()
{
	disp_off();

	pins_set_low(LED_PORT, LED1);
	_delay_ms(DISP_WALK_DELAY);
	pins_set_high(LED_PORT, LED1);

	pins_set_low(LED_PORT, LED2);
	_delay_ms(DISP_WALK_DELAY);
	pins_set_high(LED_PORT, LED2);

	pins_set_low(LED_PORT, LED3);
	_delay_ms(DISP_WALK_DELAY);
	pins_set_high(LED_PORT, LED3);

	pins_set_low(LED_PORT, LED4);
	_delay_ms(DISP_WALK_DELAY);
	pins_set_high(LED_PORT, LED4);
}

/* Display battery capacity
 *
 * Parameters:
 *     capacity: battery capacity (0...100%)
 */
void disp_show_capacity(uint8_t capacity)
{
	bool charging;
	uint8_t last_led;

	charging = pin_is_low(PGn_PIN, PGn);

	/* All LEDs OFF */
	pins_set_high(LED_PORT, LED1 | LED2 | LED3 | LED4);

	/* Blinking all LEDs */
	if (registers_get(REG_FACTORY) == REG_FACTORY_MODE) {
		if (!charging)
		{
			disp_blink(ALL_LEDS_BLINKS, ALL_LEDS_BLINKS_DURATION);
		}
	}

	/* Low */
	if (charging || (capacity > CAP_CRIT))
	{
		last_led = LED1;
		pins_set_low(LED_PORT, LED1);
	}

	/* Medium */
	if (capacity > CAP_LOW)
	{
		if (!charging)
			_delay_ms(CAP_DISP_DELAY);

		last_led = LED2;
		pins_set_low(LED_PORT, LED2);

		/* High */
		if (capacity > CAP_MED)
		{
			if (!charging)
				_delay_ms(CAP_DISP_DELAY);

			last_led = LED3;
			pins_set_low(LED_PORT, LED3);

			/* Full */
			if (capacity > CAP_HIGH)
			{
				if (!charging)
					_delay_ms(CAP_DISP_DELAY);

				last_led = LED4;
				pins_set_low(LED_PORT, LED4);
			}
		}
	}

	if (!charging)
	{
		_delay_ms(CAP_DISP_DURATION);
		pins_set_high(LED_PORT, LED1 | LED2 | LED3 | LED4);
	}
	/* TODO: We should improve when to stop flashing last LED */
	else if (capacity < CAP_FULL)
	{
		if (last_state)
		{
			last_state = false;
			pins_set_high(LED_PORT, last_led);
		}
		else
		{
			last_state = true;
		}
	}
}
