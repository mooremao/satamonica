/* 
 * Description:
 *     System specific code
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef SYS_H_
#define SYS_H_

/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

void sys_init(void);
void sys_reset(void);
void sys_crash(void);

#endif