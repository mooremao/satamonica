/* 
 * Description:
 *     TWI driver for AVR (Master and Slave).
 *
 * Notes:
 *     Slave ased on AVR 311 App Note.
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef TWI_H_
#define TWI_H_


/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

void twi_init(uint32_t clk, uint8_t addr);
void twi_slave_release(void);
bool twi_slave_active(void);
bool twi_master_transfer(uint8_t sla, uint8_t *tx, uint32_t tx_size, uint8_t *rx, uint32_t rx_size);

#endif
