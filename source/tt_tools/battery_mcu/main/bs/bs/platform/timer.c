/*
 * Description:
 *     Timer1 driver
 *
 * Author/s:
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <avr/io.h>
#include "timer.h"

/* Init TIMER1 */
void timer_init_timer1(void)
{
	/* CTC mode */
	TCCR1B |= (1<<WGM12);
	TCNT1 = 0x0;
	TIMER_SET_TIME1_ONE_SECOND();
	TIFR1 = (1<<OCF1A);
	TIMSK1 |= (1<<OCIE1A);
}
