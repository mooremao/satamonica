/* 
 * Description:
 *     Watchdog driver
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#ifndef WDT_H_
#define WDT_H_

#include <avr/io.h>
#include <avr/sfr_defs.h>

/************************************************************************/
/* PUBLIC DEFINITIONS & TYPES                                           */
/************************************************************************/

/* WDT Period */
typedef enum _wdt_period_t
{
	WDT_PER_16MS =  0,
	WDT_PER_32MS =  _BV(WDP0),
	WDT_PER_64MS =  _BV(WDP1),
	WDT_PER_125MS = _BV(WDP0) | _BV(WDP1),
	WDT_PER_250MS = _BV(WDP2),
	WDT_PER_500MS = _BV(WDP2) | _BV(WDP0),
	WDT_PER_1S =    _BV(WDP2) | _BV(WDP1),
	WDT_PER_2S =    _BV(WDP2) | _BV(WDP1) | _BV(WDP0),
	WDT_PER_4S =    _BV(WDP3),
	WDT_PER_8S =    _BV(WDP3) | _BV(WDP0),	
} wdt_period_t;

/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/

void wdt_enable(wdt_period_t period);
void wdt_disable(void);
#define wdt_reset() __asm__ __volatile__ ("wdr")

#endif