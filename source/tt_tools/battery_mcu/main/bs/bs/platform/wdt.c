/* 
 * Description:
 *     Watchdog driver
 *
 * IMPORTANT NOTE:
 *     Optimization level is fixed to O1, as higher optimization levels appear to
 *     make this driver to NOT work.
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include "wdt.h"

/* Configure WDT in interrupt mode 
 *
 * Parameters:
 *     period: WDT period
 *     mode: WDT mode (INT+RST or RST)
 */
void __attribute__((optimize("O1"))) wdt_enable(wdt_period_t period)
{	
	wdt_reset();
	MCUSR &= ~_BV(WDRF);
	
	WDTCSR |= _BV(WDE) | _BV(WDCE);
	WDTCSR = _BV(WDE) | _BV(WDIE) | period;
}

/* Disable WDT */
void __attribute__((optimize("O1"))) wdt_disable()
{
	uint8_t sreg;
	
	sreg = SREG;
	
	cli();
	wdt_reset();

	MCUSR &= ~_BV(WDRF);

	WDTCSR |= _BV(WDCE) | _BV(WDE);
	WDTCSR = 0x00;
	
	if (sreg & _BV(SREG_I))
		sei();
}
