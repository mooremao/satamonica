/*
 * Description:
 *     Timer1 driver
 *
 * Author/s:
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#ifndef TIMER_H_
#define TIMER_H_

#define TIME1_PRESCALER 64

#if (TIME1_PRESCALER == 64)
	#define ONE_SECOND		62500					/* 4000000 / 64, clkI/O = 4M */
	#define TIMER_START_TIMER1()	(TCCR1B |= ((1<<CS10) | (1<<CS11)))	/* clkI/O/64 (From prescaler) */
	#define TIMER_STOP_TIMER1()	(TCCR1B &= ~((1<<CS10) | (1<<CS11)))
#elif (TIME1_PRESCALER == 1024)
	#define ONE_SECOND		3906					/* 4000000 / 1024, clkI/O = 4M */
	#define TIMER_START_TIMER1()	(TCCR1B |= ((1<<CS10) | (1<<CS12)))	/* clkI/O/1024 (From prescaler) */
	#define TIMER_STOP_TIMER1()	(TCCR1B &= ~((1<<CS10) | (1<<CS12)))
#endif

#define TIMER_TERMINATE_TIME1()		TIMSK1 &= ~(1<<OCIE1A);
#define TIMER_SET_TIME1_ONE_SECOND()	(OCR1A = ONE_SECOND)

void timer_init_timer1(void);

#endif /* TIMER_H_ */
