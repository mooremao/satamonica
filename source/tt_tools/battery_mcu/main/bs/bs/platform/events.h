/* 
 * Description:
 *     Events
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef EVENTS_H_
#define EVENTS_H_

#include <stdbool.h>

/************************************************************************/
/* PUBLIC TYPES                                                         */
/************************************************************************/

/* Available events */
typedef enum _event_t
{
	EVENT_WDT		= (1 << 0),
	EVENT_UPDATE_DISPLAY	= (1 << 1),
	EVENT_KEY_PRESS		= (1 << 2),
	EVENT_RECONFIGURE_CHRG	= (1 << 3),
	EVENT_UPDATE_REG	= (1 << 4),
	EVENT_READ_FG		= (1 << 5),
	EVENT_UPDATE_FG		= (1 << 6),
	EVENT_WAKEUP		= (1 << 7),
}event_t;

/************************************************************************/
/* PUBLIC FUNCTIONS (PROTOTYPES)                                        */
/************************************************************************/
bool events_check(event_t events);
bool events_available(void);
void events_set(event_t events);

#endif
