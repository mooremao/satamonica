/* 
 * Description:
 *     System specific code
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include "config.h"

/* Initialize system */
void sys_init()
{	
	/* Disable interrupts */
	cli();

	/* Clock pre-scaler = 2, (4 MHz, using internal 8 MHz RC OSC) */
	CLKPR = _BV(CLKPCE);
	CLKPR = _BV(CLKPS0);
	
	/* Log WDT Resets */
	if (MCUSR & _BV(WDRF))
	{
		eeprom_write_byte((uint8_t*)FAILURE_WDT_EE_ADDR,
			eeprom_read_byte((uint8_t*)FAILURE_WDT_EE_ADDR) + 1);
	}
	
	/* Watchdog */
	wdt_enable(WDT_DEFAULT_PERIOD);
}

