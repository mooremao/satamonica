/* 
 * Description:
 *     I/O Utilities
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */


#ifndef IO_H_
#define IO_H_


/************************************************************************/
/* PUBLIC DEFINITIONS                                                   */
/************************************************************************/

/* NOTE: Pin number must be already shifted! i.e. (1 << pin) */

/* Pin configuration utilities (to make code more readable) */
/* Input/Output */
#define INP(pin)                                  0
#define OUT(pin)                                  (pin)

/* High/Low */
#define HI(pin)                                   (pin)
#define LO(pin)                                   0

/* Pull-up Enable/Disable */
#define PUEN(pin)                                 (pin)
#define PUDI(pin)                                 0


/* Pin CHECK functions */
#define pin_is_input(ddr, pin_n)                  !((ddr) & (pin_n))
#define pin_is_output(ddr, pin_n)                 ((ddr) & (pin_n))
#define pin_is_low(pin, pin_n)                    !((pin) & (pin_n))
#define pin_is_high(pin, pin_n)                   ((pin) & (pin_n))

/* Pin SET functions */
#define pins_set_input(ddr, pin_n)                (ddr) &= ~(pin_n)
#define pins_set_output(ddr, pin_n)               (ddr) |= (pin_n)
#define pins_set_low(port, pin_n)                 (port) &= ~(pin_n)
#define pins_set_high(port, pin_n)                (port) |= (pin_n)
#define pins_toggle(port, pin_n)                  (port) ^= (pin_n)

#endif