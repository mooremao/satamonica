/*
 * Description:
 *     Events
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <stdbool.h>
#include <avr/io.h>
#include <util/atomic.h>
#include "events.h"

/************************************************************************/
/* PRIVATE VARIABLES                                                    */
/************************************************************************/

static volatile uint8_t __events = 0;




/************************************************************************/
/* PUBLIC FUNCTIONS                                                     */
/************************************************************************/

/* Check if event/s is/are set (and clears it/them)
 *
 * Arguments:
 *     events: event/s to be checked
 *
 * Returns:
 *     true if the event/s is/are set, false otherwise
 */
bool events_check(event_t events)
{
	bool set;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)		//just wrap the whole block			ATOMIC_RESTORESTATE takes an extra 2 bytes of flash
	{
		set = __events & events;

		if (set)
		{
//		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
			{
				__events &= ~events;
			}
		}
	}

	return set;
}


/* Check if any events are available
 *
 * Returns:
 *     true if any event is available, false otherwise
 */
bool events_available()
{
	return (__events != 0);
}


/* Set event/s
 *
 * Arguments:
 *     events: event/s to be set
 */
void events_set(event_t events)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		__events |= events;
	}
}
