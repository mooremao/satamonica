/*
 * Description:
 *     TWI driver for AVR (Master and Slave).
 *
 * Notes:
 *     Slave based on AVR 311 App Note.
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Hadyn van der Berg (hadyn.vanderberg@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <board/pins.h>
#include <util/twi.h>
#include "config.h"
#include <util/delay.h>
#include <platform/events.h>
#include <platform/io.h>

/************************************************************************/
/* PRIVATE DEFINITIONS & VARIABLES                                      */
/************************************************************************/

/* TWI Slave interface */
extern const uint8_t twi_sl_noofregs;
extern const uint8_t twi_sl_lock;
extern const uint8_t twi_sl_rdonly;
extern const uint8_t twi_sl_pw;
extern const uint8_t twi_sl_status2;
extern const uint8_t twi_sl_interrupt;
extern const uint8_t twi_sl_factory;

extern  uint8_t volatile twi_sl_regs[];

static bool twi_addr_set = false;
static uint8_t twi_reg = 0;

/* Slave active flag */
static volatile bool __twi_slave_active;
/* Written flag */
static volatile bool written = false;
/* Dummy variable */
static uint8_t dummy;

/* TWI Busy */
#define twi_busy()	(!(TWCR & _BV(TWINT)))



/************************************************************************/
/* PRIVATE FUNCTIONS                                                    */
/************************************************************************/

/* TWI ISR handler */
ISR(TWI_vect)
{
	switch (TW_STATUS)
	{
		/*** SLAVE RECEIVE ***/

		/* SLA+W received, ACK returned */
		case TW_SR_SLA_ACK:
			twi_addr_set = false;
			TWCR = _BV(TWEN) | _BV(TWIE)| _BV(TWINT) | _BV(TWEA);
			__twi_slave_active = true;
			break;

		/* Previously addressed (SLA+W), ACK returned */
		case TW_SR_DATA_ACK:
			if (!twi_addr_set)
			{
				twi_reg = TWDR;
				twi_addr_set = true;
			}
			else if ((twi_reg >= twi_sl_status2 && twi_reg < twi_sl_factory) ||
			((twi_reg > twi_sl_lock) && (twi_sl_regs[twi_sl_lock] != twi_sl_pw)) ||
			(twi_reg <= twi_sl_rdonly))
			{
				dummy = TWDR;
			}
			else
			{
				/* Clear password */
				twi_sl_regs[twi_sl_lock] = 0;
				twi_sl_regs[twi_reg++] = TWDR;
				written = true;
			}

			TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);

			break;

		/* Previously addressed (SLA+W), NACK returned */
		case TW_SR_DATA_NACK:
		/* STOP (or re-START) received while still addressed as Slave */
		case TW_SR_STOP:
			TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);

			if (written)
			{
				written = false;
				events_set(EVENT_UPDATE_REG);
			}

			__twi_slave_active = false;
			break;

		/*** SLAVE TRANSMIT ***/

		/* SLA+R Received, ACK returned */
		case TW_ST_SLA_ACK:
			__twi_slave_active = true;

		/* Data byte transmitted; ACK received */
		case TW_ST_DATA_ACK:
			if (twi_reg < twi_sl_noofregs)
			{
				TWDR = twi_sl_regs[twi_reg++];

				if(twi_reg == (twi_sl_interrupt+1))
				{
					twi_sl_regs[twi_sl_interrupt] = 0;		// if this register is transmitted, then clear it out.
					/* set HOST_INT to high to make sure edge trigger works well: from high to low. */
					pins_set_low(HOST_INT_PORT, HOST_INT);
					pins_set_input(HOST_INT_DDR, HOST_INT);
				}
			}
			else
			{
				TWDR = 0xFF;
			}

			TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);
			__twi_slave_active = true;
			break;

		/* Data byte transmitted; NACK received (last) */
		case TW_ST_DATA_NACK:
			TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);
			__twi_slave_active = false;
			break;

		/* Last data byte in TWDR has been transmitted, ACK received */
		case TW_ST_LAST_DATA:
			TWCR = _BV(TWINT) | _BV(TWSTO);
			TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);
			__twi_slave_active = false;
			break;

		/*** GENERAL CASES ***/

		/* Bus error (Illegal START or STOP condition) */
		case TW_BUS_ERROR:
			TWCR = _BV(TWINT) | _BV(TWSTO);

		default:
			TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);
			__twi_slave_active = false;
	}
}




/***********************************/
/* PUBLIC FUNCTIONS                */
/***********************************/

/* Initialize TWI
 *
 * Notes:
 *     In master mode TWBR >= 10!
 *     Relations can be found at Attiny48/88 datasheet, section 15.6.2
 *
 * Parameters:
 *     clk: TWI Speed
 *     addr: TWI Slave address
 *     sg_handler: Function called when SLA+R/W is acknowledged.
 *     sr_handler: Function called when data is received.
 *     st_handler: Function called to grab data to be transmitted.
 */
void twi_init(uint32_t clk, uint8_t addr)
{
	/* Configure baud rate generator */
	if (clk > F_CPU/36)
		TWBR = (F_CPU / clk - 16) / 2;
	else
		TWBR = 10;

	/* Configure Slave */
	TWAR = addr << 1;
	TWAMR = 0;

	/* Enable TWI interface */
	TWCR = _BV(TWINT) | _BV(TWSTO);			// release the bus, according to the datasheet
	TWCR = 0;
	TWCR = _BV(TWINT) | _BV(TWSTO);			// release the bus
	TWCR = 0;
	TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);
}

void twi_slave_release(void)
{
	/* Enable TWI interface */
	TWCR = _BV(TWINT) | _BV(TWSTO);			// release the bus, according to the datasheet
	TWCR = 0;
	TWCR = _BV(TWINT) | _BV(TWSTO);			// release the bus
	TWCR = 0;
	TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);

	__twi_slave_active = false;
}

/* Check if TWI slave is active
 *
 * Returns:
 *     true if active, false otherwise.
 */
bool twi_slave_active()
{
	return __twi_slave_active;
}

/* TWI Transfer (Master mode)
 *
 * Notes:
 *     - Supports WR->RD, WR and RD
 *     - In case of any failure, line will be released.
 *     - TWI Slave will be activated back
 *
 * Parameters:
 *    sla: Slave address
 *    *tx: TX buffer
 *    tx_size: Size of the TX buffer
 *    *rx: RX buffer
 *    rx_size: Size of the RX buffer
 *
 * Returns:
 *     true if transmission was successful, false otherwise
 */
bool twi_master_transfer(uint8_t sla, uint8_t *tx, uint32_t tx_size, uint8_t *rx, uint32_t rx_size)
{
	uint8_t status;

	/* Sanity check */
	if((!tx && tx_size) || (tx && !tx_size) || (!rx && rx_size) || (rx && !rx_size))
		return false;

	/* Wait while TWI is busy and fix master mode when free */
	cli();
	while(__twi_slave_active)
	{
		sei();
		while(__twi_slave_active);
		cli();
	}

	sei();

	/*** TRANSMISSION ***/

begin:
	/* START */
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
	while (twi_busy());

	switch ((status = TW_STATUS))
	{
		case TW_REP_START:
		case TW_START:
			break;
		case TW_MT_ARB_LOST:
			goto begin;
		default:
			return false;
	}

	/* TX */
	if (tx)
	{
		/* SLA+W */
		TWDR = sla | TW_WRITE;
		TWCR = _BV(TWINT) | _BV(TWEN);
		while (twi_busy());

		switch ((status = TW_STATUS))
		{
			case TW_MT_SLA_ACK:
				break;
			case TW_MT_SLA_NACK:
				goto quit;
			case TW_MT_ARB_LOST:
				goto begin;
			default:
				goto quit;
		}

		/* Send data */
		while (tx_size)
		{
			tx_size--;

			TWDR = *tx++;
			TWCR = _BV(TWINT) | _BV(TWEN);
			while (twi_busy());

			switch ((status = TW_STATUS))
			{
				case TW_MT_DATA_ACK:
					break;
				case TW_MT_DATA_NACK:
					goto quit;
				case TW_MT_ARB_LOST:
					goto begin;
				default:
					goto quit;
			}
		}
	}


	/* RX */
	if (rx)
	{
		/* Send re-START */
		if (tx)
		{
			TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
			while (twi_busy());

			switch ((status = TW_STATUS))
			{
				case TW_START:
				case TW_REP_START:
					break;
				case TW_MT_ARB_LOST:
					goto begin;
				default:
					goto quit;
			}
		}

		/* SLA+R */
		TWDR = sla | TW_READ;
		TWCR = _BV(TWINT) | _BV(TWEN);
		while (twi_busy());

		switch ((status = TW_STATUS))
		{
			case TW_MR_SLA_ACK:
				break;
			case TW_MR_SLA_NACK:
				goto quit;
			case TW_MR_ARB_LOST:
				goto begin;
			default:
				goto quit;
		}

		/* Receive bytes */
		while (rx_size)
		{
			rx_size--;

			if (rx_size)
				TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA);
			else
				TWCR = _BV(TWINT) | _BV(TWEN);

			while (twi_busy());

			switch ((status = TW_STATUS))
			{
				case TW_MR_DATA_NACK:
					rx_size = 0;
				case TW_MR_DATA_ACK:
					*rx = TWDR;
					rx++;
					if (status == TW_MR_DATA_NACK)
						goto quit;
					break;
				default:
					goto quit;
			}
		}
	}

quit:
	/* STOP */
	TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);

	/* Re-enable TWI. TODO: Delay is required but has been decided randomly! */
	_delay_us(50);

	TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);

	return (tx_size || rx_size) ? false : true;
}
