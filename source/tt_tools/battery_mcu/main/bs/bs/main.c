/*
 * Description:
 *     BatteryStick Project
 *
 * Author/s:
 *     Gerard Marull-Paretas (gerard.marullparetas@tomtom.com)
 *     Doug Steel (doug.steel@tomtom.com)
 *     Jut Jiang (Jut.Jiang@tomtom.com)
 *
 * Copyright (c) 2014 TomTom B.V.
 */
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <avr/fuse.h>
#include "config.h"
#include <util/delay.h>
#include <util/atomic.h>
#include <platform/sys.h>
#include <platform/io.h>
#include <platform/twi.h>
#include <platform/wdt.h>
#include <platform/events.h>
#include <platform/timer.h>
#include <components/charger.h>
#include <components/fg.h>
#include <components/disp.h>
#include <board/board.h>
#include <board/pins.h>
#include <registers.h>

/************************************************************************/
/* PRIVATE VARIABLES & DEFINITIONS                                      */
/************************************************************************/

/* FUSES */
#ifndef DEBUG
#pragma message "Production fuses enabled"
FUSES =
{
	.low = FUSE_CKDIV8 & FUSE_SUT0 & FUSE_CKSEL0,
	.high = FUSE_SPIEN,
	.extended = EFUSE_DEFAULT
};
#else
#pragma message "NO FUSE DEFINITON! - debug mode"
#endif

#define SWAP_FG_BLOCKDATA_BYTES(w)     ((((w) & 0xFF)<<8) | ((w)>>8))
// need to swap the bytes in a word for the BLOCKDATA since FG treats it "other-way-around" . strange, but true.

/* Available battery configurations */
fg_config_t batt_cfgs[BATT_TYPE_AVAIL_CFGS] = {
	/* NCR 18500 */
	{
		.capacity = SWAP_FG_BLOCKDATA_BYTES(2000),
		.energy = SWAP_FG_BLOCKDATA_BYTES(7400),
		.term_voltage = SWAP_FG_BLOCKDATA_BYTES(2700),
		.taper_rate = SWAP_FG_BLOCKDATA_BYTES(147),
		.opconfig = SWAP_FG_BLOCKDATA_BYTES(FG_OPCONFIG_FILL_MASK | FG_OPCONFIG_BATLOWEN | FG_OPCONFIG_RMFCC | FG_OPCONFIG_SLEEP | FG_OPCONFIG_BI_PU_EN),
	},
	/* ICR 18650 */
	{
		.capacity = SWAP_FG_BLOCKDATA_BYTES(3000),
		.energy = SWAP_FG_BLOCKDATA_BYTES(11100),
		.term_voltage = SWAP_FG_BLOCKDATA_BYTES(2700),
		.taper_rate = SWAP_FG_BLOCKDATA_BYTES(225),
		.opconfig = SWAP_FG_BLOCKDATA_BYTES(FG_OPCONFIG_FILL_MASK | FG_OPCONFIG_BATLOWEN | FG_OPCONFIG_RMFCC | FG_OPCONFIG_SLEEP | FG_OPCONFIG_BI_PU_EN),
	}
};

/* When charging is active, WDTP is set to 1s to update LEDs
 * Normal operation is set by WDT_DEFAULT_PERIOD, so a counter
 * is used to keep normal operations at WDT_DEFAULT_PERIOD.
 * Adjust it according to configuration needs */

#define WDT_COUNTER 8
static volatile uint8_t wdt_counter = 0;
static volatile uint8_t twi_timeout = TWI_TIMEOUT;
static volatile bool wakeup_active = false;

/* SW1 Status flag */
static volatile uint8_t sw1_count = 0;
static volatile bool sw1_pressed = false;

/* BAT_LOCK */
static volatile uint8_t unlocked_count = 0;
static volatile bool last_batt_unlocked = false;
static volatile bool batt_unlocked = false;

fg_info_t batt_info;

void update_eeprom_count(uint8_t *ee_addr);
void twi_slave_active_check(void);
void sw_pressed_check(void);
void batt_unlocked_check(void);

#define soft_reset()            \
do                              \
{                               \
    wdt_enable(WDT_PER_16MS);   \
    for(;;)                     \
    {                           \
    }                           \
} while(0)

/************************************************************************/
/* INTERRUPT ROUTINES                                                   */
/************************************************************************/

/* Watchdog */
ISR(WDT_vect)
{
	if (charger_is_charging() || twi_slave_active())
	{
		twi_slave_active_check();
		events_set(EVENT_UPDATE_DISPLAY);

		wdt_counter++;

		if (wdt_counter >= WDT_COUNTER)
			wdt_counter = 0;
		else
			return;
	}

	sw_pressed_check();
	events_set(EVENT_WDT);
}

/* PCINT1 Interrupt (USB2_F1, THREEV3_ON, USB3_F1) */
ISR(PCINT1_vect)
{
	events_set(EVENT_RECONFIGURE_CHRG);
}

/* PCINT2 Interrupt (USB_DET_5V, PGn, USB3_ACTIVE, BATT_LOCKED_DET_MCU) */
ISR(PCINT2_vect)
{
	batt_unlocked = pin_is_high(BATT_LOCKED_DET_MCU_PIN, BATT_LOCKED_DET_MCU);
	if (!batt_unlocked)
		unlocked_count = 0;

	if (batt_unlocked && !last_batt_unlocked) {
		registers_set(REG_USB_POWERON , REG_USB_POWERON_ENABLE);
	}
	last_batt_unlocked = batt_unlocked;

	events_set(EVENT_RECONFIGURE_CHRG);
}

/* PCINT3 Interrupt (EXTMIC, SW1 and SDCARD_DET - Last unused) */
ISR(PCINT3_vect)
{
	sw1_pressed = pin_is_low(SW1_PIN, SW1);
	if (sw1_pressed) {
		events_set(EVENT_KEY_PRESS);
		sw1_count = 1;
	} else {
		sw1_count = 0;
	}

	if (pin_is_low(GPOUT_PIN, GPOUT)) {
		/* The [SOC1] flag is set */
		batt_info.flags |= FG_FLAG_SOC1;
	} else {
		/* The [SOC1] flag is clear */
		batt_info.flags &= (uint16_t)~(FG_FLAG_SOC1);
	}

	events_set(EVENT_UPDATE_REG);				//use this to trigger a register update for EXTMIC change detect
}

/* TIMER1 Interrupt (Wakeup timer) */
ISR(TIMER1_COMPA_vect)
{
	uint16_t wakeup_counter;

	/* Duplicate process from WDT interrupt when TIMER1 enabled */
	twi_slave_active_check();
	if (charger_is_charging()) {
		events_set(EVENT_UPDATE_DISPLAY);
	}
	wdt_counter++;
	if (wdt_counter >= WDT_COUNTER) {
		wdt_counter = 0;
		events_set(EVENT_WDT);
	}

	wakeup_counter = registers_get(REG_WAKEUP_L) | (registers_get(REG_WAKEUP_H) << 8);
	if (wakeup_counter == 0) {
		TIMER_STOP_TIMER1();
		TIMER_TERMINATE_TIME1();
		return;
	}

	wakeup_counter--;
	registers_set(REG_WAKEUP_L, (wakeup_counter & 0xFF));
	registers_set(REG_WAKEUP_H, (wakeup_counter >> 8));

	if (wakeup_counter == 0) {
		TIMER_STOP_TIMER1();
		TIMER_TERMINATE_TIME1();
		events_set(EVENT_WAKEUP);
	} else {
		batt_unlocked_check();
	}
}

void configure_charger(void)
{
	if(charger_is_charging())
	{
#if	BattStickPCB == BS_BOARD_WS2
		/* USB 2 */
		if (registers_get(REG_TEST_MODE) & REG_TEST_MODE_USB2)
		{
			charger_set_ulim(ULIM_1500MA);
		}
		else if (pin_is_low(USB2_F1_PIN, USB2_F1))
		{
			charger_set_ulim(ULIM_500MA);
		}
		else
		{
			charger_set_ulim(ULIM_1500MA);
		}

		/* USB 3 */
		if (registers_get(REG_TEST_MODE) & REG_TEST_MODE_USB3)
		{
			charger_set_ilim(ILIM_1200MA);
		}
		else
		{
			if (pin_is_low(USB3_DET_5V_PIN, USB3_DET_5V))
			{
				charger_set_ilim(ILIM_500MA);
			}
			else
			{
				if (pin_is_high(USB3_F1_PIN, USB3_F1))
				{
					charger_set_ilim(ILIM_1200MA);
				}
				else
				{
					charger_set_ilim(ILIM_500MA);

					// TODO Hadyn 28-Aug-14  . Since WS2 isn't really supported, this wasn't implemented. But check the correct current limits are being set, since a bug was found in PR1 code for USB3
					/*
					TODO: Review logic (apparently doesn't set 500mA!)

					if (pin_is_high(USB3_ACTIVE_PIN, USB3_ACTIVE))
						charger_set_ilim(ILIM_900MA);
					else
						charger_set_ilim(ILIM_500MA);
					*/
				}
			}
		}

#elif BattStickPCB == BS_BOARD_PR1
		/* USB 2 */
		if (registers_get(REG_TEST_MODE) & REG_TEST_MODE_USB2)
		{
			charger_set_ilim(ILIM_1500MA);
		}
		else if (pin_is_low(USB2_F1_PIN, USB2_F1))
		{
			charger_set_ilim(ILIM_500MA);
		}
		else
		{
			charger_set_ilim(ILIM_1500MA);
		}

		/* USB 3 */
		if (registers_get(REG_TEST_MODE) & REG_TEST_MODE_USB3)
		{
			charger_set_ulim(ULIM_1500MA);
		}
		else
		{
			if (pin_is_low(USB3_DET_5V_PIN, USB3_DET_5V))
			{
				charger_set_ulim(ULIM_500MA);
			}
			else			// USB3_DET_5V  must be high then
 			{
				if (pin_is_high(USB3_F1_PIN, USB3_F1))
				{
					charger_set_ulim(ULIM_1500MA);
				}
				else if (pin_is_low(USB3_ACTIVE_PIN, USB3_ACTIVE))
				{
					charger_set_ulim(ULIM_900MA);	// LOW USB3_ACTIVE means USB3 detected
				}
				else
				{
					charger_set_ulim(ULIM_500MA);
				}
			}
		}
#else
#error	"no board definition included"
#endif

	} else 	{
		/* Fall back to safe limits  , disable test mode for USB2 and USB3 */
		charger_set_ilim(ILIM_500MA);
		charger_set_ulim(ULIM_500MA);

		registers_set(REG_TEST_MODE, (registers_get(REG_TEST_MODE)
				& ~(REG_TEST_MODE_USB2 | REG_TEST_MODE_USB3 | REG_TEST_MODE_BATT_DISCHARGE)) );
	}
}

/************************************************************************/
/* LOCAL FUNCTIONS                                                      */
/************************************************************************/
void update_eeprom_count(uint8_t *ee_addr)
{
	uint8_t count = 0;

	count = (eeprom_read_byte(ee_addr) + 1);
	eeprom_write_byte(ee_addr, count);
	if (ee_addr == (uint8_t *)FAILURE_FG_EE_ADDR)
		registers_set(REG_FAIL_FG , count);
	else if (ee_addr == (uint8_t *)FAILURE_TWI_EE_ADDR)
		registers_set(REG_FAIL_TWI , count);
}

/* Check TWI slave active */
void twi_slave_active_check(void)
{
	if (twi_slave_active()) {
		twi_timeout--;
		if (twi_timeout == 0) {
			twi_slave_release();
			twi_timeout = TWI_TIMEOUT;
			update_eeprom_count((uint8_t*)FAILURE_TWI_EE_ADDR);
		}
	} else {
		twi_timeout = TWI_TIMEOUT;
	}
}

/* Check long press SW1 */
void sw_pressed_check(void)
{
	if (sw1_count != 0) {
		if (pin_is_high(USB3_DET_5V_PIN, USB3_DET_5V)) {
			if (sw1_count > SOFT_RESET_TIME) {
				soft_reset();
			} else {
				sw1_count++;
			}
		} else {
			sw1_count = 0;
		}
	}
}

/* Check BATT_LOCKED_DET_MCU unlocked */
void batt_unlocked_check(void)
{
	if (batt_unlocked) {
		if (unlocked_count > BATT_UNLOCK_TIME) {
			wakeup_active = false;
			registers_set(REG_WAKEUP_L, 0x0);
			registers_set(REG_WAKEUP_H, 0x0);
		} else {
			unlocked_count++;
		}
	}
}

/************************************************************************/
/* MAIN                                                                 */
/************************************************************************/

int main(void)
{
	uint8_t retry, fg_update_counter = FG_UPDATE_PERIOD;
	uint8_t batt_type;
	uint16_t wakeup_counter;
	wdt_period_t wdt_period = WDT_DEFAULT_PERIOD;
	bool fg_initialized = false, fg_valid = false;

	/* System and board initializations */
	sys_init();
	board_init();

	/* Initialize registers and TWI */
	registers_init();
	twi_init(TWI_CLK, BS_TWI_ADDR);

	sei();

	/* Force events on first execution */
	events_set(EVENT_WDT | EVENT_RECONFIGURE_CHRG | EVENT_UPDATE_FG | EVENT_READ_FG);

	/* Show alive signal */
	disp_walk();

	/*** MAIN LOOP ***/
	while (1)
	{
		/** EVENTS **/
		while (events_available())
		{
			/* UPDATE_REG - Update registers */
			if (events_check(EVENT_UPDATE_REG))
			{
				registers_update();

				wakeup_counter = registers_get(REG_WAKEUP_L) | (registers_get(REG_WAKEUP_H) << 8);
				/* Clear internal watchdog counter if wakeup has been activated */
				if ((wakeup_counter != 0) && !wakeup_active)
				{
					wakeup_active = true;
					timer_init_timer1();
					TIMER_START_TIMER1();
				}

				if ((wakeup_counter == 0) && wakeup_active)
					wakeup_active = false;
			}

			if (events_check(EVENT_UPDATE_FG))
			{
				/* Initialize and configure FG */
				retry = FG_FAIL_RETRIES;

				batt_type = registers_get(REG_BATT_TYPE);
				while (retry && (batt_type != REG_BATT_TYPE_INVALID))
				{
					if(fg_init(&batt_cfgs[batt_type]))
						break;

					_delay_ms(FG_FAIL_RETRY_DELAY);

					retry--;
				}

				if (!retry)
				{
					/* LOG FG Failures */
					update_eeprom_count((uint8_t*)FAILURE_FG_EE_ADDR);

					fg_initialized = false;
				}
				else
				{
					fg_initialized = true;
				}
			}

			if (events_check(EVENT_READ_FG))
			{
				if (fg_initialized)
				{
					retry = FG_FAIL_RETRIES;
					while (retry)
					{
						if(fg_get_info(&batt_info))
							break;

						_delay_ms(FG_FAIL_RETRY_DELAY);

						retry--;
					}

					if (!retry)
					{
						/* LOG FG Failures */
						update_eeprom_count((uint8_t*)FAILURE_FG_EE_ADDR);

						fg_valid = false;
					}
					else
					{
						fg_valid = true;
						events_set(EVENT_UPDATE_REG);
					}
				}//if (fg_initialized)
			}

			/* Wakeup - Wakeup event */
			if (events_check(EVENT_WAKEUP))
			{
				registers_set(REG_INTERRUPT , (registers_get(REG_INTERRUPT) | REG_INT_WAKEUP_TIMER_TIMEOUT));
				wakeup_active = false;
			}

			/* WDT - Watchdog event */
			if (events_check(EVENT_WDT))
			{
				/* Read battery status (counter used to update less frequently!) */
				if (fg_update_counter++ >= FG_UPDATE_PERIOD)
				{
					events_set(EVENT_READ_FG);
					fg_update_counter = 0;
				}// if fg_update_counter++ >
			}

			/* UPDATE_DISPLAY: Refresh display with current SOC */
			if (events_check(EVENT_UPDATE_DISPLAY))
				disp_show_capacity(batt_info.soc);

			/* KEY_PRESS: SW1 key press */
			if (events_check(EVENT_KEY_PRESS))
			{
				registers_set(REG_INTERRUPT , (registers_get(REG_INTERRUPT) | REG_INT_SW_PRESSED) );

				if (sw1_pressed)
				{
					if (!charger_is_charging())
					{
						if (fg_valid)
						{
							disp_show_capacity(batt_info.soc);
						}
						else
						{
							disp_blink(FAILURE_BLINKS, FAILURE_BLINKS_DURATION);
						}
					}
					sw1_pressed = false;
				}
			}

			/* RECONFIGURE_CHRG: Reconfigure charger pins */
			if (events_check(EVENT_RECONFIGURE_CHRG))
			{
				configure_charger();

				if(charger_is_charging())
				{
					/* USB power on */
					if (registers_get(REG_USB_POWERON) != REG_USB_POWERON_DISABLE) {
						if (pin_is_low(BATT_LOCKED_DET_MCU_PIN, BATT_LOCKED_DET_MCU)) {
							if (! (registers_get(REG_STATUS) & REG_STATUS_POWER_PRESENT)) {
								/* Disable interrupts */
								cli();
								pins_set_low(SW1_PORT, SW1);
								pins_set_output(SW1_DDR, SW1);
								_delay_ms(1);
								pins_set_input(SW1_DDR, SW1);
								pins_set_high(SW1_PORT, SW1);
								sei();
								registers_set(REG_INTERRUPT , (registers_get(REG_INTERRUPT) | REG_INT_USB_POWERON));
							}
						}
					}

					/* Set WDTP to 1s (to blink LED) and disable SW1 interrupt */

					// TODO:  check here why only 1 LED blinks , and SW1 button interrupt is disabled?!
					//PCICR &= ~_BV(PCIE3);					1-sept-14 HAdyn    don't disable PORTA interrupts!

					wdt_period = WDT_PER_1S;
					wdt_counter = 0;
					wdt_enable(wdt_period);
				}
				else   /* display off, WDT to default period */
				{
					disp_off();

					//PCICR |= _BV(PCIE3);					// re-enable SW1 button interrupt			1-sept-14 HAdyn stopped disabling them
					wdt_period = WDT_DEFAULT_PERIOD;
				}

				/* USB3 Data Transfer */
				if (!registers_get(REG_TEST_MODE) && pin_is_high(USB3_DET_5V_PIN, USB3_DET_5V))
//  28-Aug-14  IF 5V is present, we need USB card reader chip to be powered to provide the correct USB D+ D- pull-resistors. Thus  ignore the D+D- detection status && pin_is_low(USB3_F1_PIN, USB3_F1))
					pins_set_high(THREEV3_ON_PORT, THREEV3_ON);
				else
					pins_set_low(THREEV3_ON_PORT, THREEV3_ON);

				/* Update registers */
				registers_update();
			} /* if (events_check(EVENT_RECONFIGURE_CHRG)) */
		}   /* while (events_available()) */

		/* Battery High Temperature Discharge */
		if (registers_get(REG_TEST_MODE) & REG_TEST_MODE_BATT_DISCHARGE)
		{
			/* disable charging */
			pins_set_high(nCHRG_EN_PORT, nCHRG_EN);
			/* enable discharge */
			pins_set_high(BATT_DISCHARGE_PORT, BATT_DISCHARGE);
		}
		else if ( (fg_valid) && (batt_info.voltage >= SYSTEM_HIGH_CAP) && (batt_info.temperature >= SYSTEM_HIGH_TEMP))
		{
			/* disable charging */
			pins_set_high(nCHRG_EN_PORT, nCHRG_EN);
			/* enable discharge */
			pins_set_high(BATT_DISCHARGE_PORT, BATT_DISCHARGE);
			/* force a re-read of fuel gauge every wakeup time */
			events_set(EVENT_READ_FG);
		}
		else
		{
			/* enable charging */
			pins_set_low(nCHRG_EN_PORT, nCHRG_EN);
			/* disable battery discharge */
			pins_set_low(BATT_DISCHARGE_PORT, BATT_DISCHARGE);
		}

		if (registers_get(REG_INTERRUPT) != 0)
		{
			/* make pin output low */
			pins_set_low(HOST_INT_PORT, HOST_INT);
			/* make HOST_INT output */
			pins_set_output(HOST_INT_DDR, HOST_INT);
		}
		else
		{
			/* no pullup */
			pins_set_low(HOST_INT_PORT, HOST_INT);
			/* make HOST_INT input */
			pins_set_input(HOST_INT_DDR, HOST_INT);
		}

		/* Sleep */
		if (twi_slave_active() || wakeup_active)
			set_sleep_mode(SLEEP_MODE_IDLE);
		else
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);

		/* TWI Slave active (keep waiting IDLE until finished) */
		do
		{
			if (twi_slave_active())
				wdt_enable(WDT_PER_1S);
			else
				wdt_enable(wdt_period);

			sleep_mode();
			set_sleep_mode(SLEEP_MODE_IDLE);
		} while (twi_slave_active());
	}
}
