/**
 * @file ttw_api.c
 *
 * @author R.Cane
 *
 * @brief TTW WiFi direct API
 *
 * @see http://www.wi-fi.org/discover-wi-fi/wi-fi-direct
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "wpa_ctrl.h"
#include "ttw_api.h"
#include "ttw_utils.h"

/**
 * Connects to the backend and provides an handle for the session
 */
ttw_p2p_handle_t* ttw_init()
{
	int ret;
	ttw_p2p_handle_t *p2p_handle = NULL;

	dbg_printf("ttw_init(): enter\n");
	p2p_handle = malloc(sizeof(ttw_p2p_handle_t));

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_init(): error allocating p2p handle\n");
		return NULL;
	}

	/** Initialize the handle structure */
	memset(p2p_handle, 0x0, sizeof(ttw_p2p_handle_t));
	inet_aton(TTW_DHCP_SERVER_DEFAULT_IP, &p2p_handle->dhcp_srv_ip);
	inet_aton(TTW_DHCP_SERVER_DEFAULT_MASK, &p2p_handle->dhcp_srv_mask);

	/** Open the connection to wpa_supplicant for the main interface */
	p2p_handle->main_ctrl_if = wpa_ctrl_open (WPA_CTRL_IF_PATH "/" WPA_CTRL_IF_NAME);
	if (p2p_handle->main_ctrl_if == NULL) {
		dbg_printf_err("ttw_init(): fail to open %s\n", WPA_CTRL_IF_PATH "/" WPA_CTRL_IF_NAME);
		free (p2p_handle);
		return NULL;
	}

	/** Open the connection to wpa_supplicant for the monitor interface */
	p2p_handle->main_mon_if = wpa_ctrl_open (WPA_CTRL_IF_PATH "/" WPA_CTRL_IF_NAME);
	if (p2p_handle->main_mon_if == NULL) {
		dbg_printf_err("ttw_init(): fail to open %s\n", WPA_CTRL_IF_PATH "/" WPA_CTRL_IF_NAME);
		free (p2p_handle);
		return NULL;
	}

	/** Attach the monitor interface to receive events from wpa_supplicant */
	ret = wpa_ctrl_attach(p2p_handle->main_mon_if);
	if (ret == -2) {
		dbg_printf_err("ttw_init(): timeout attaching to %s\n", WPA_CTRL_IF_PATH "/" WPA_CTRL_IF_NAME);
		free (p2p_handle);
		return NULL;
	} else if (ret < 0) {
		dbg_printf_err("ttw_init(): fail to attach monitor interface to %s\n", WPA_CTRL_IF_PATH "/" WPA_CTRL_IF_NAME);
		free (p2p_handle);
		return NULL;
	}

	dbg_printf("ttw_init(): exit\n");
	return p2p_handle;
}

/**
 * Registers a callback function for asyncronous events
 * The callback will be called from inside ttw_process_events()
 */
void ttw_register_event_cb(ttw_p2p_handle_t *p2p_handle, void (*event_cb)(ttw_p2p_event_t event, void *data))
{
	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_register_event_cb(): p2p_handle is NULL\n");
		return;
	}
	p2p_handle->event_cb = event_cb;
}

/**
 * Start the discovery process of new P2P devices
 */
int ttw_start_discovery(ttw_p2p_handle_t *p2p_handle)
{
	int ret;
	char buf[100];
	size_t len;

	dbg_printf("ttw_start_discovery(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_start_discovery(): p2p_handle is NULL\n");
		return -1;
	}

	/** Send the command P2P_FIND to the backend */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, "P2P_FIND",strlen("P2P_FIND"), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_start_discovery(): timeout during P2P_FIND\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_start_discovery(): fail to send P2P_FIND\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_start_discovery(): after P2P_FIND, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_start_discovery(): command P2P_FIND failed\n");
		return -1;
	}

	dbg_printf("ttw_start_discovery(): exit\n");
	return 0;
}

/**
 * Stop the P2P discovery process
 */
int ttw_stop_discovery(ttw_p2p_handle_t *p2p_handle)
{
	int ret;
	char buf[100];
	size_t len;

	dbg_printf("ttw_stop_discovery(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_stop_discovery(): p2p_handle is NULL\n");
		return -1;
	}

	/** Send the command P2P_STOP_FIND to the backend */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, "P2P_STOP_FIND",strlen("P2P_STOP_FIND"), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_stop_discovery(): timeout during P2P_STOP_FIND\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("tttw_stop_discovery(): fail to send P2P_FIND\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_stop_discovery(): after P2P_STOP_FIND, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_stop_discovery(): command P2P_STOP_FIND failed\n");
		return -1;
	}

	dbg_printf("ttw_stop_discovery(): exit\n");
	return 0;
}

/**
 * Start the autonomous creation of a group
 */
int ttw_create_group(ttw_p2p_handle_t *p2p_handle, ttw_group_persistence_t persistence)
{
	int ret;
	char buf[100];
	char cmd[100];
	size_t len;

	dbg_printf("ttw_create_group(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_create_group(): p2p_handle is NULL\n");
		return -1;
	}

	switch (persistence) {

		case TTW_GROUP_NOT_PERSISTENT:
			snprintf(cmd, 100, "P2P_GROUP_ADD");
			break;

		case TTW_GROUP_PERSISTENT:
			snprintf(cmd, 100, "P2P_GROUP_ADD persistent=0");
			break;

		default:
			dbg_printf_err("ttw_create_group(): invalid persistence type\n");
			return -1;
	}

	dbg_printf("ttw_create_group(): sending:\"%s\"\n", cmd);

	/** Send the command P2P_GROUP_ADD to the backend */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd,strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_create_group(): timeout during P2P_GROUP_ADD\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_create_group(): fail to send P2P_GROUP_ADD\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_create_group(): after P2P_GROUP_ADD, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_create group(): command P2P_GROUP_ADD failed\n");
		return -1;
	}

	dbg_printf("ttw_create_group(): exit\n");
	return 0;
}

/**
 * Start the connection to a P2P device
 */
int ttw_connect(ttw_p2p_handle_t *p2p_handle, const char *p2p_address, int go_intent, ttw_wps_req_t wps_req, char *wps_pin, int join)
{
	int ret;
	char buf[100];
	char cmd[100];
	char go_mode[20];
	size_t len;

	dbg_printf("ttw_connect(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_connect(): p2p_handle is NULL\n");
		return -1;
	}

	/** in case of group owner, send an invitation */
	if (p2p_handle->is_group_owner) {
		snprintf(cmd, 100, "P2P_INVITE group=%s peer=%s go_dev_addr=%s", p2p_handle->group_name, p2p_address, p2p_handle->group_addr);
	} else {
		/** check of the group negotiation should be skipped */
		if (join)
			strncpy(go_mode, "join provdisc", 20);
		else
			snprintf(go_mode, 20, "go_intent=%d", go_intent);

		/** Prepare the command depending on the WPS request chosen */
		switch (wps_req) {

			case TTW_WPS_PBC: /**< Press-button method */
				snprintf(cmd, 100, "P2P_CONNECT %s pbc %s", p2p_address, go_mode);
				break;

			case TTW_WPS_RANDOM_PIN: /**< PIN method with generated code */
				snprintf(cmd, 100, "P2P_CONNECT %s pin %s", p2p_address, go_mode);
				break;

			case TTW_WPS_PEER_PIN: /**< PIN method wth provided code*/
			case TTW_WPS_STATIC_PIN: /**< PIN method wth provided code*/
				/** check if the pin provided is valid */
				snprintf(cmd, 100, "WPS_CHECK_PIN %s", wps_pin);

				/** Send the PIN check command to the backend */
				len = sizeof(buf) - 1;
				ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
				if (ret == -2) {
					dbg_printf_err("ttw_connect(): timeout during WPS_CHECK_PIN\n");
					return -1;
				} else if (ret < 0) {
					dbg_printf_err("ttw_connect(): fail to send WPS_CHECK_PIN\n");
					return -1;
				}
				/** Check the result */
				if (memcmp(buf, "FAIL", 4) == 0) {
					dbg_printf_err("ttw_connect(): invalid PIN code\n");
					return -1;
				}

				/** PIN is valid, create the connect command */
				if (wps_req == TTW_WPS_PEER_PIN)
					snprintf(cmd, 100, "P2P_CONNECT %s %s %s", p2p_address, wps_pin, go_mode);
				else
					snprintf(cmd, 100, "P2P_CONNECT %s %s display %s", p2p_address, wps_pin, go_mode);
				break;

			default:
				dbg_printf_err("ttw_connect(): invalid WPS method\n");
				return -1;

		}
	}
	dbg_printf("ttw_connect(): sending:\"%s\"\n", cmd);

	/** Send the command to the backend */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_connect(): timeout during P2P connect\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_connect(): fail to send P2P connect\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_connect(): after P2P_CONNECT, reply msg: %s ", buf);
	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_connect(): command P2P_CONNECT failed\n");
		return -1;
	}

	/** retrieve the generated PIN */
	if (wps_req == TTW_WPS_RANDOM_PIN) {
		strncpy(wps_pin, buf, P2P_PIN_LEN - 1);
		wps_pin[P2P_PIN_LEN-1] = '\0';
	}

	dbg_printf("ttw_connect(): exit\n");
	return 0;
}

/**
 * Disconnect from a group
 */
int ttw_leave_group(ttw_p2p_handle_t *p2p_handle, const char *group_name)
{
	int ret;
	char buf[100];
	char cmd[100];
	size_t len;

	dbg_printf("ttw_leave_group(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_leave_group(): p2p_handle is NULL\n");
		return -1;
	}

	/** Prepare the command for the backend */
	snprintf(cmd, 100, "P2P_GROUP_REMOVE %s", group_name);

	dbg_printf("ttw_leave_group(): sending:\"%s\"\n", cmd);

	/** Send the command */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_leave_group(): timeout during P2P_GROUP_REMOVE\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_leave_group(): fail to send P2P_GROUP_REMOVE\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_leave_group(): after P2P_GROUP_REMOVE, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_leave_group(): command P2P_GROUP_REMOVE failed\n");
		return -1;
	}

	dbg_printf("ttw_leave_group(): exit\n");
	return 0;
}

/**
 * Close the P2P context
 */
void ttw_close(ttw_p2p_handle_t *p2p_handle)
{
	int ret;

	dbg_printf("ttw_close(): enter\n");

	if (p2p_handle->group_name[0] != '\0')
		ttw_leave_group(p2p_handle, p2p_handle->group_name);

	/** detach to the main monitor interface */
	ret = wpa_ctrl_detach(p2p_handle->main_mon_if);
	if (ret == -2) {
		dbg_printf_err("ttw_close(): timeout detaching main monitor interface\n");
		return;
	} else if (ret < 0) {
		dbg_printf_err("ttw_close(): fail to detach main monitor interface\n");
		return;
	}

	/** close the main monitor interface */
	wpa_ctrl_close(p2p_handle->main_mon_if);

	/** close the main control interface */
	wpa_ctrl_close(p2p_handle->main_ctrl_if);

	free(p2p_handle);
	dbg_printf("ttw_close(): exit\n");
}

/**
 * Check for pending events on the backend
 */
int ttw_pending_events(ttw_p2p_handle_t *p2p_handle)
{
	int fd_main, fd_grp, max_fd, ret;
	fd_set read_set;

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_pending_events(): p2p_handle is NULL\n");
		return -1;
	}

	FD_ZERO(&read_set);
	fd_grp = -1;

	/** Get the file descriptor of the main monitor interface */
	fd_main = wpa_ctrl_get_fd(p2p_handle->main_mon_if);
	FD_SET(fd_main, &read_set);

	if (p2p_handle->is_group_owner) {
		/** Get the file descriptor of the group monitor interface and add it to the read set */
		fd_grp = wpa_ctrl_get_fd(p2p_handle->grp_mon_if);
		FD_SET(fd_grp, &read_set);
	}

	max_fd = (fd_main > fd_grp) ? fd_main : fd_grp;

	/** Blocking wait */
	ret = select(max_fd + 1, &read_set, NULL, NULL, NULL);

	if (ret < 0) {
		if (errno == EINTR)
			return 1;
		else {
			dbg_printf_err("ttw_pending_events(): error getting events %d\n", ret);
			return -1;
		}
	}

	/** There is at lest one event in the FDs */
	return 0;
}


/**
 * Process any pending events on the backend and invoke the event callback if necessary
 */
int ttw_process_events(ttw_p2p_handle_t *p2p_handle)
{
	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_process_events(): p2p_handle is NULL\n");
		return -1;
	}

	/** Loop on events on the main monitor interface */
	while (wpa_ctrl_pending(p2p_handle->main_mon_if) > 0) {
		char buf[4096];
		size_t len = sizeof(buf) - 1;
		if (wpa_ctrl_recv(p2p_handle->main_mon_if, buf, &len) == 0) {
			buf[len] = '\0';
			dbg_printf("ttw_process_events(): received event on main monitor interface: %s\n", buf);
			ttw_parse_event(p2p_handle, buf, 0);
		} else {
			dbg_printf_err("ttw_process_events(): fail to read pending events from main monitor interface\n");
			break;
		}
	}

	/** Check for errors */
	if (wpa_ctrl_pending(p2p_handle->main_mon_if) < 0) {
		dbg_printf_err("ttw_process_events(): error checking pending events from main monitor interface\n");
		return -1;
	}

	/** Loop on events on the group monitor interface (only if the device is a group owner) */
	if (p2p_handle->is_group_owner) {
		while (wpa_ctrl_pending(p2p_handle->grp_mon_if) > 0) {
			char buf[4096];
			size_t len = sizeof(buf) - 1;
			if (wpa_ctrl_recv(p2p_handle->grp_mon_if, buf, &len) == 0) {
				buf[len] = '\0';
				dbg_printf("ttw_process_events(): received event on main group interface: %s\n", buf);
				ttw_parse_event(p2p_handle, buf, 1);
			} else {
				dbg_printf_err("ttw_process_events(): fail to read pending events from main group interface\n");
				break;
			}
		}

		/** Check for errors */
		if (wpa_ctrl_pending(p2p_handle->grp_mon_if) < 0) {
			dbg_printf_err("ttw_process_events(): error checking pending events from main group interface\n");
			return -1;
		}
	}
	return 0;
}

/**
 * Perform a legacy WPS operation on the network
 */
int ttw_wps_cmd(ttw_p2p_handle_t *p2p_handle, ttw_wps_req_t wps_req, const char *peer_address, char *wps_pin, int pin_timeout)
{
	int ret;
	char buf[100];
	char cmd[100];
	size_t len;

	dbg_printf("ttw_wps_cmd(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_wps_cmd(): p2p_handle is NULL\n");
		return -1;
	}

	/** Only the group owner can send legacy WPS requests */
	if (p2p_handle->is_group_owner == 0) {
		dbg_printf_err("ttw_wps_cmd(): local device is not group owner\n");
		return -1;
	}

	/** Parse the WPS operation requested */
	switch (wps_req) {

		case TTW_WPS_PBC: /**< Press-button method */
			/** If the peer is not provided, the pairing is valid for any station */
			if (peer_address == NULL)
				snprintf(cmd, 100, "WPS_PBC");
			else
				snprintf(cmd, 100, "WPS_PBC %s", peer_address);
			break;

		case TTW_WPS_RANDOM_PIN: /**< PIN method with locally generated random code */
			if (peer_address == NULL)
				snprintf(cmd, 100, "WPS_PIN any");
			else
				snprintf(cmd, 100, "WPS_PIN %s", peer_address);
			break;

		case TTW_WPS_STATIC_PIN: /**< PIN method with static code */
		case TTW_WPS_PEER_PIN: /**< PIN method with provided code*/
		case TTW_WPS_SET_AP_PIN: /**< Set access point PIN */
			/** check if the pin provided is valid */
			snprintf(cmd, 100, "WPS_CHECK_PIN %s", wps_pin);

			/** Send the PIN check command to the backend */
			len = sizeof(buf) - 1;
			ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
			if (ret == -2) {
				dbg_printf_err("ttw_wps_cmd(): timeout during WPS_CHECK_PIN\n");
				return -1;
			} else if (ret < 0) {
				dbg_printf_err("ttw_wps_cmd(): fail to send WPS_CHECK_PIN\n");
				return -1;
			}
			/** Check the result */
			if (memcmp(buf, "FAIL", 4) == 0) {
				dbg_printf_err("ttw_wps_cmd(): invalid PIN code\n");
				return -1;
			}

			if (wps_req == TTW_WPS_SET_AP_PIN) {
				/** Set access point pin */
				snprintf(cmd, 100, "WPS_AP_PIN set %s %d", wps_pin, pin_timeout);
			} else {
				/** Enter the provided pin
				  * If the peer is not provided, the pairing is valid for any station */
				if (peer_address == NULL)
					snprintf(cmd, 100, "WPS_PIN any %s %d", wps_pin, pin_timeout);
				else
					snprintf(cmd, 100, "WPS_PIN %s %s %d", peer_address, wps_pin, pin_timeout);
			}
			break;

		case TTW_WPS_CANCEL: /**< Cancel pending WPS requests */
			snprintf(cmd, 100, "WPS_CANCEL");
			break;

		default: /**< Invalid WPS request */
			dbg_printf_err("ttw_wps_cmd(): invalid WPS request\n");
			return -1;
	}

	dbg_printf("ttw_wps_cmd(): sending:\"%s\"\n", cmd);

	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->grp_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_wps_cmd(): timeout during sending WPS request\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_wps_cmd(): fail to send WPS request\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_wps_cmd(): after WPS request, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_wps_cmd(): WPS command failed\n");
		return -1;
	}

	/** retrieve the generated PIN */
	if (wps_req == TTW_WPS_RANDOM_PIN) {
		strncpy(wps_pin, buf, P2P_PIN_LEN - 1);
		wps_pin[P2P_PIN_LEN-1] = '\0';
	}

	dbg_printf("ttw_wps_cmd(): exit\n");

	return 0;
}

/**
 * Set the SSID of the persistent group
 */
int ttw_set_persistent_ssid(ttw_p2p_handle_t *p2p_handle, const char *ssid)
{
	int ret;
	char buf[100];
	char cmd[100];
	size_t len;

	dbg_printf("ttw_set_persistent_ssid(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_set_persistent_ssid(): p2p_handle is NULL\n");
		return -1;
	}

	/** Prepare the command for the backend */
	snprintf(cmd, 100, "SET_NETWORK 0 ssid \"%s\"", ssid);

	dbg_printf("ttw_set_persistent_ssid(): sending:\"%s\"\n", cmd);

	/** Send the command */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_set_persistent_ssid(): timeout during SET_NETWORK\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_set_persistent_ssid(): fail to send SET_NETWORK\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_set_persistent_ssid(): after SET_NETWORK, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_set_persistent_ssid(): command SET_NETWORK failed\n");
		return -1;
	}

	/** save the configuration */
	snprintf(cmd, 100, "SAVE_CONFIG");

	/** Send the command to the backend */
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_set_persistent_ssid(): timeout during SAVE_CONFIG\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_set_persistent_ssid(): fail to send SAVE_CONFIG\n");
		return -1;
	}

	dbg_printf("ttw_set_persistent_ssid(): after SAVE_CONFIG, reply msg: %s ", buf);

	/** Check the result */
	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_set_persistent_ssid(): command SAVE_CONFIG failed\n");
		return -1;
	}

	dbg_printf("ttw_set_persistent_ssid(): exit\n");
	return 0;
}

/**
 * Set the WPA passphrase of the persistent group
 */
int ttw_set_persistent_pass(ttw_p2p_handle_t *p2p_handle, const char *passphrase)
{
	int ret;
	char buf[100];
	char cmd[100];
	size_t len;

	dbg_printf("ttw_set_persistent_pass(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_set_persistent_pass(): p2p_handle is NULL\n");
		return -1;
	}

	/** Prepare the command for the backend */
	snprintf(cmd, 100, "SET_NETWORK 0 psk \"%s\"", passphrase);

	dbg_printf("ttw_set_persistent_pass(): sending:\"%s\"\n", cmd);

	/** Send the command */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_set_persistent_pass(): timeout during SET_NETWORK\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_set_persistent_pass(): fail to send SET_NETWORK\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_set_persistent_pass(): after SET_NETWORK, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_set_persistent_pass(): command SET_NETWORK failed\n");
		return -1;
	}

	/** save the configuration */
	snprintf(cmd, 100, "SAVE_CONFIG");

	/** Send the command to the backend */
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_set_persistent_pass(): timeout during SAVE_CONFIG\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_set_persistent_pass(): fail to send SAVE_CONFIG\n");
		return -1;
	}

	dbg_printf("ttw_set_persistent_pass(): after SAVE_CONFIG, reply msg: %s ", buf);

	/** Check the result */
	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_set_persistent_pass(): command SAVE_CONFIG failed\n");
		return -1;
	}

	dbg_printf("ttw_set_persistent_pass(): exit\n");
	return 0;
}

/**
 * Get the WPA passphrase of the current active group
 */
int ttw_get_current_pass(ttw_p2p_handle_t *p2p_handle, char *passphrase)
{
	int ret;
	char buf[100];
	char cmd[100];
	size_t len;

	dbg_printf("ttw_get_current_pass(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_get_current_pass(): p2p_handle is NULL\n");
		return -1;
	}

	if (!p2p_handle->is_group_owner) {
		dbg_printf_err("ttw_get_current_pass(): device is not group owner!\n");
		return -1;
	}

	/** Prepare the command for the backend */
	snprintf(cmd, 100, "P2P_GET_PASSPHRASE");

	dbg_printf("ttw_get_current_pass(): sending:\"%s\"\n", cmd);

	/** Send the command */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->grp_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_get_current_pass(): timeout during P2P_GET_PASSPHRASE\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_get_current_pass(): fail to send P2P_GET_PASSPHRASE\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_get_current_pass(): after P2P_GET_PASSPHRASE, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_get_current_pass(): command P2P_GET_PASSPHRASE failed\n");
		return -1;
	}

	/** retrieve the passphrase */
	strncpy(passphrase, buf, P2P_PASSPHRASE_LEN - 1);
	passphrase[P2P_PASSPHRASE_LEN-1] = '\0';

	dbg_printf("ttw_get_current_pass(): exit\n");
	return 0;
}

/**
 * Get the SSID of the persistent group
 */
int ttw_get_persistent_ssid(ttw_p2p_handle_t *p2p_handle, char *ssid)
{
	int ret;
	char buf[100];
	char cmd[100];
	size_t len;

	dbg_printf("ttw_get_persistent_ssid(): enter\n");

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_get_persistent_ssid(): p2p_handle is NULL\n");
		return -1;
	}

	if (!p2p_handle->is_group_owner) {
		dbg_printf_err("ttw_get_persistent_ssid(): device is not group owner!\n");
		return -1;
	}

	/** Prepare the command for the backend */
	snprintf(cmd, 100, "GET_NETWORK 0 ssid");

	dbg_printf("ttw_get_persistent_ssid(): sending:\"%s\"\n", cmd);

	/** Send the command */
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(p2p_handle->main_ctrl_if, cmd, strlen(cmd), buf, &len, NULL);
	if (ret == -2) {
		dbg_printf_err("ttw_get_persistent_ssid(): timeout during GET_NETWORK\n");
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_get_persistent_ssid(): fail to send GET_NETWORK\n");
		return -1;
	}

	buf[len] = '\0';

	dbg_printf("ttw_get_persistent_ssid(): after GET_NETWORK, reply msg: %s ", buf);

	if (memcmp(buf, "FAIL", 4) == 0) {
		dbg_printf_err("ttw_get_persistent_ssid(): command GET_NETWORK failed\n");
		return -1;
	}

	/** retrieve the ssid and remove the double quotes */
	strncpy(ssid, buf+1, P2P_SSID_LEN - 1);
	ssid[P2P_SSID_LEN-1] = '\0';
	len = strlen(ssid);
	ssid[len-1] = '\0';

	dbg_printf("ttw_get_persistent_ssid(): exit\n");
	return 0;
}

int ttw_set_group_net(ttw_p2p_handle_t *p2p_handle, const char *ip_address, const char *netmask)
{
	struct in_addr ip, mask;
	uint32_t pool_start, pool_end;

	if (p2p_handle == NULL) {
		dbg_printf_err("ttw_set_group_net(): p2p_handle is NULL\n");
		return -1;
	}

	dbg_printf("ttw_set_group_net(): setting IP to \"%s\"\n", ip_address);
	if (inet_aton(ip_address, &ip) == 0) {
		dbg_printf_err("ttw_set_group_net(): invalid IP address\n");
		return -1;
	}

	dbg_printf("ttw_set_group_net(): setting netmask to \"%s\"\n", netmask);
	if (inet_aton(netmask, &mask) == 0) {
		dbg_printf_err("ttw_set_group_net(): invalid netmask\n");
		return -1;
	}

	/** Check if the IP address is outside the expected DHCP pool */
	pool_start = (ntohl(ip.s_addr) & ntohl(mask.s_addr)) + TTW_DHCP_POOL_START;
	pool_end = (ntohl(ip.s_addr) & ntohl(mask.s_addr)) + TTW_DHCP_POOL_END;
	if ((ntohl(ip.s_addr) >= pool_start) && (ntohl(ip.s_addr) <= pool_end)) {
		dbg_printf_err("ttw_set_group_net(): IP address inside the DHCP pool range\n");
		return -1;
	}

	memcpy(&p2p_handle->dhcp_srv_ip, &ip, sizeof(struct in_addr));
	memcpy(&p2p_handle->dhcp_srv_mask, &mask, sizeof(struct in_addr));

	return 0;
}

