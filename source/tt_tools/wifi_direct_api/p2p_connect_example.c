/**
 * @file p2p_connect_example.c
 *
 * @author R.Cane
 *
 * @brief Test for WiFi direct (P2P) connections using TTW API
 *
 * This program does the following:
 *	1) Initialize a WiFi direct context
 *	2) Scan for available WiFi direct peers
 *	3) Try to connect to the first peer available using the default PIN 12345670
 *	4) Report any group and connection events
 *	5) On SIGUSR1, close the WiFi direct context
 */
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>

#include "ttw_api.h"

ttw_p2p_handle_t *p2p_handle = NULL;
ttw_p2p_device_t peer_info;
ttw_p2p_group_t group_info;
int peer_found=0;
void event_handler(ttw_p2p_event_t event, void *data);
void sig_handler(int num);

int main (int argc, char **argv)
{
	int res;
	struct sigaction action;
	char pin[P2P_PIN_LEN];

	if (argc < 2) {
		printf("\nUsage: %s <pin> <go_intent>\n\t\t<pin> : PIN code to be used for pairing\n\t\t<go_intent> : group owner intention value (0-15)\n", argv[0]);
		exit(0);
	}

	strncpy(pin, argv[1], P2P_PIN_LEN - 1);
	pin[P2P_PIN_LEN-1] = '\0';

	/** Install SIGUSR1 handler */
	memset(&action, 0, sizeof(struct sigaction));
	action.sa_handler = sig_handler;
	sigaction(SIGUSR1, &action, NULL);

	/** Initialize the framework */
	p2p_handle = ttw_init();
	if (p2p_handle == NULL) {
		printf("Error initializing P2P api\n");
		return -1;
	}

	printf("\nTTW P2P context initialized\n");

	/** Register the callback to handle async events */
	ttw_register_event_cb(p2p_handle, event_handler);

	/** Start discovery of P2P devices */
	res = ttw_start_discovery(p2p_handle);
	if (res < 0) {
		printf("Error starting discovery\n");
		ttw_close(p2p_handle);
		return -1;
	}

	printf("\nP2P discovery started....\n");

	/** Loop on backend events until a device is found */
	while (!peer_found) {
		res = ttw_pending_events(p2p_handle);
		if (res < 0) {
			printf("Error in event polling\n");
			continue;
		}
		else
			ttw_process_events(p2p_handle);
	}

	printf("\nP2P peer found, press ENTER to connect\n");
	getchar();
	printf("Connecting with PIN \"%s\" and go_intent=%d....\n", pin, atoi(argv[2]));

	/** Start the connection process */
	res = ttw_connect(p2p_handle, peer_info.p2p_addr, atoi(argv[2]), TTW_WPS_STATIC_PIN, pin, peer_info.is_group_owner);
	if (res < 0) {
		printf("Error connecting peer\n");
		ttw_close(p2p_handle);
		return -1;
	}

	printf("\nP2P connection request sent\n");

	/** Infinite loop on backend events */
	while (1) {
		res = ttw_pending_events(p2p_handle);
		if (res < 0) {
			printf("Error in event polling\n");
			continue;
		}
		else
			ttw_process_events(p2p_handle);
	}
}

void sig_handler(int num)
{
	int res;

	/** Terminate connection */
	res = ttw_leave_group(p2p_handle, group_info.name);
	if (res < 0) {
		printf("Error leaving group\n");
	}
}

void event_handler(ttw_p2p_event_t event, void *data)
{
	switch(event) {

		case TTW_P2P_EVENT_PEER_FOUND:
			printf("\nReceived event TTW_P2P_EVENT_PEER_FOUND:\n");
			printf("\tpeer address: %s\n", ((ttw_p2p_device_t *)data)->p2p_addr);
			printf("\tpeer name: \"%s\"\n", ((ttw_p2p_device_t *)data)->p2p_name);
			printf("\tdevice type: %s\n", ((ttw_p2p_device_t *)data)->dev_type);
			printf("\tis_group_owner: %s\n", (((ttw_p2p_device_t *)data)->is_group_owner) ? "Yes" : "No");
			printf("\n");
			peer_found = 1;
			/** Save the data of the peer */
			memcpy(&peer_info, (ttw_p2p_device_t *) data, sizeof(ttw_p2p_device_t));
			break;

		case TTW_P2P_EVENT_CONNECT_REPORT:
			printf("\nReceived event TTW_P2P_EVENT_CONNECT_REPORT:\n");
			printf("\tconnection is_legacy: %s\n", (((ttw_p2p_conn_info_t *)data)->is_legacy) ? "Yes" : "No");
			printf("\tconnection status: %s\n", (((ttw_p2p_conn_info_t *)data)->status == TTW_CONNECTION_ESTABLISHED) ? "ESTABLISHED" : "CLOSED");
			printf("\tconnection origin: %s\n", (((ttw_p2p_conn_info_t *)data)->origin == TTW_ORIGIN_PEER) ? "PEER" : "LOCAL");
			printf("\tconnection client_hw_addr: %s\n", ((ttw_p2p_conn_info_t *)data)->client_hw_addr);
			printf("\tconnection client_ip_addr: %s\n", ((ttw_p2p_conn_info_t *)data)->client_ip_addr);

			break;

		case TTW_P2P_EVENT_GROUP_REPORT:
			printf("\nReceived event TTW_P2P_EVENT_GROUP_REPORT:\n");
			printf("\tgroup name: %s\n", ((ttw_p2p_group_t *)data)->name);
			printf("\tgroup SSID: %s\n", ((ttw_p2p_group_t *)data)->ssid);
			printf("\tgroup passphrase: %s\n", ((ttw_p2p_group_t *)data)->passphrase);
			printf("\tgroup owner_addr: %s\n", ((ttw_p2p_group_t *)data)->owner_addr);
			printf("\tgroup owner_ipaddr: %s\n", ((ttw_p2p_group_t *)data)->owner_ipaddr);
			printf("\tgroup is_owner: %s\n", (((ttw_p2p_group_t *)data)->is_group_owner) ? "Yes" : "No");
			printf("\tgroup status: %s\n", (((ttw_p2p_group_t *)data)->status == TTW_GROUP_FORMED) ? "FORMED" : "REMOVED");

			if (((ttw_p2p_group_t *)data)->status == TTW_GROUP_FORMED) {
				/* Save group info to terminate the connection later */
				memcpy(&group_info, ((ttw_p2p_group_t *)data), sizeof(ttw_p2p_group_t));
			} else {
				ttw_close(p2p_handle);
				exit(0);
			}
		default:
			break;
	}
}
