/**
 * @file p2p_group_example.c
 *
 * @author R.Cane
 *
 * @brief Test for WiFi direct (P2P) group owner functionalities using TTW API
 *
 * This program does the following:
 *	1) Initialize a WiFi direct context
 *	2) Create an autonomous P2P group and wait for events
 *	3) Reply to any connection attempt
 *	4) On SIGUSR1, remove the group and close the WiFi direct context
 */

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include "ttw_api.h"


ttw_p2p_group_t group_info;
ttw_p2p_handle_t *p2p_handle = NULL;

void event_handler(ttw_p2p_event_t event, void *data);
void sig_handler(int num);

int main (int argc, char **argv)
{

	int res;
	struct sigaction action;

	/** Install SIGSUSR1 handler */
	memset(&action, 0, sizeof(struct sigaction));
	action.sa_handler = sig_handler;
	sigaction(SIGUSR1, &action, NULL);

	/** Initialize the framework */
	p2p_handle = ttw_init();
	if (p2p_handle == NULL) {
		printf("Error initializing P2P api\n");
	return -1;
	}

	printf("\nTTW P2P context initialized\n");

	/** Register the callback to handle async events */
	ttw_register_event_cb(p2p_handle, event_handler);

	printf("\nCreating P2P group....\n");

	/** Create an autonomous group */
	res = ttw_create_group(p2p_handle, TTW_GROUP_NOT_PERSISTENT);
	if (res < 0) {
		printf("Error creating group\n");
		ttw_close(p2p_handle);
		return -1;
	}

	printf("\nGroup creation request sent, waiting for events....\n");

	/** Infinite loop on backend events */
	while (1) {
		res = ttw_pending_events(p2p_handle);
		if (res < 0) {
			printf("Error in event polling\n");
			continue;
		}
		else
			ttw_process_events(p2p_handle);
	}
}

void event_handler(ttw_p2p_event_t event, void *data)
{
	static int wps_pending = 0;

	switch(event) {
		case TTW_P2P_EVENT_GROUP_REPORT:
			printf("\nReceived TTW_P2P_EVENT_GROUP_REPORT:\n");
			printf("\tgroup name: %s\n", ((ttw_p2p_group_t *)data)->name);
			printf("\tgroup SSID: %s\n", ((ttw_p2p_group_t *)data)->ssid);
			printf("\tgroup passphrase: %s\n", ((ttw_p2p_group_t *)data)->passphrase);
			printf("\tgroup owner_addr: %s\n", ((ttw_p2p_group_t *)data)->owner_addr);
			printf("\tgroup owner_ipaddr: %s\n", ((ttw_p2p_group_t *)data)->owner_ipaddr);
			printf("\tgroup is_owner: %s\n", (((ttw_p2p_group_t *)data)->is_group_owner) ? "Yes" : "No");
			printf("\tgroup status: %s\n", (((ttw_p2p_group_t *)data)->status == TTW_GROUP_FORMED) ? "FORMED" : "REMOVED");

			if (((ttw_p2p_group_t *)data)->status == TTW_GROUP_FORMED) {
				/** Save group info to terminate the connection later */
				memcpy(&group_info, ((ttw_p2p_group_t *)data), sizeof(ttw_p2p_group_t));
			} else {
				ttw_close(p2p_handle);
				exit(0);
			}
			printf("\n");
			break;

		case TTW_P2P_EVENT_CONNECT_REPORT:
			printf("\nReceived TTW_P2P_EVENT_CONNECT_REPORT:\n");
			printf("\tconnection is_legacy: %s\n", (((ttw_p2p_conn_info_t *)data)->is_legacy) ? "Yes" : "No");
			printf("\tconnection status: %s\n", (((ttw_p2p_conn_info_t *)data)->status == TTW_CONNECTION_ESTABLISHED) ? "ESTABLISHED" : "CLOSED");
			printf("\tconnection origin: %s\n", (((ttw_p2p_conn_info_t *)data)->origin == TTW_ORIGIN_PEER) ? "PEER" : "LOCAL");
			printf("\tconnection client_hw_addr: %s\n", ((ttw_p2p_conn_info_t *)data)->client_hw_addr);
			printf("\tconnection client_ip_addr: %s\n", ((ttw_p2p_conn_info_t *)data)->client_ip_addr);
			printf("\n");
			break;

		case TTW_P2P_EVENT_WPS_REPORT:
			if (!wps_pending && ((ttw_wps_info_t *)data)->wps_event == TTW_WPS_CREATE_PIN_REQ) {
				char pin[P2P_PIN_LEN];
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: CREATE PIN REQUEST\n");
				printf("\tWPS peer: %s\n", ((ttw_wps_info_t *)data)->peer_address);
				printf("\n");
				ttw_wps_cmd(p2p_handle, TTW_WPS_RANDOM_PIN, ((ttw_wps_info_t *)data)->peer_address, pin, 0);
				printf("\nPairing started, enter the PIN \"%s\" on the peer....\n", pin);
				wps_pending = 1;
			}
			if (!wps_pending && ((ttw_wps_info_t *)data)->wps_event == TTW_WPS_ENTER_PIN_REQ) {
				char pin[P2P_PIN_LEN];
				char ch;
				int gc_rv;
				int i = 0;
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: ENTER PIN REQUEST\n");
				printf("\tWPS peer: %s\n", ((ttw_wps_info_t *)data)->peer_address);
				printf("Enter the PIN from the peer to start pairing: ");
				while((ch = gc_rv = getchar()) != '\n' && gc_rv != EOF && i < P2P_PIN_LEN)
					pin[i++] = ch;
				pin[i] = '\0';
				if (ttw_wps_cmd(p2p_handle, TTW_WPS_PEER_PIN, ((ttw_wps_info_t *)data)->peer_address, pin, 10) == 0) {
					printf("Pairing started....\n");
					wps_pending = 1;
				} else {
					printf("Error starting pairing\n");
					wps_pending = 0;
				}
				printf("\n");
			}
			if (!wps_pending && ((ttw_wps_info_t *)data)->wps_event == TTW_WPS_PBC_REQ) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: PBC REQUEST\n");
				printf("\tWPS peer: %s\n", ((ttw_wps_info_t *)data)->peer_address);
				printf("\nStarting pairing with PBC method.....\n");
				printf("\n");
				ttw_wps_cmd(p2p_handle, TTW_WPS_PBC, NULL, NULL, 0);
				wps_pending = 1;
			}
			if (((ttw_wps_info_t *)data)->wps_event == TTW_WPS_FAIL) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: request failed\n");
				printf("\n");
				ttw_wps_cmd(p2p_handle, TTW_WPS_CANCEL, NULL, NULL, 0);
				wps_pending = 0;
			}
			if (((ttw_wps_info_t *)data)->wps_event == TTW_WPS_SUCCESS) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: request completed successfully\n");
				printf("\n");
				wps_pending = 0;
			}
			break;

		default:
			break;
	}
}

void sig_handler(int num)
{
	int res;

	/** Terminate connection */
	res = ttw_leave_group(p2p_handle, group_info.name);
	if (res < 0) {
		printf("Error leaving group\n");
	}
}

