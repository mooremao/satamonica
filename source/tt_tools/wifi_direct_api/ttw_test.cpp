//
// file: ttw_test.cpp
//
// Test tool for TTW API using GTEST
//

#include <gtest/gtest.h>
#include <ttw_api.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

class TTWTest: public ::testing::Test
{
protected:
	ttw_p2p_handle_t *p2p_handle;
	void SetUp()
	{
		// Enable the backend
		system("wlan_on p2p");
	}

	void TearDown()
	{
		// Shutdown the backend
		system("wlan_off");
	}
};

// Discovery test:
// 1. Initialize the TTW API
// 2. Start a P2P device discovery
// 3. Stop the discovery
// 4. Close the context
TEST_F(TTWTest, test_discovery)
{
	p2p_handle = ttw_init();
	ASSERT_TRUE(p2p_handle != NULL);
	EXPECT_EQ(0, ttw_start_discovery(p2p_handle));
	sleep(3);
	EXPECT_EQ(0, ttw_stop_discovery(p2p_handle));
	ttw_close(p2p_handle);
}

// Non-persistent group creation test:
// 1. Initialize the TTW API
// 2. Create a non-persistent autonomous group
// 3. Remove the group
// 4. Close the context
TEST_F(TTWTest, test_group)
{
	p2p_handle = ttw_init();
	ASSERT_TRUE(p2p_handle != NULL);
	EXPECT_EQ(0, ttw_create_group(p2p_handle, TTW_GROUP_NOT_PERSISTENT));
	sleep(3);
	EXPECT_EQ(0, ttw_leave_group(p2p_handle, "p2p-p2p0-0"));
	ttw_close(p2p_handle);
} 

// Persistent group creation test:
// 1.  Initialize the TTW API
// 2.  Create a persistent autonomous group
// 3.  Check the IP address of the group to be the default one
// 4.  Remove the group
// 5.  Close the context
// 6.  Re-initialize the TTW API
// 7.  Recall the persisteng group
// 8.  Set new subnet, SSID and passphrase for the group
// 9.  Recall the persisteng group
// 10. Check the IP, SSID, and pass to be the new ones
// 11. Remove the group
// 12. Close the context
TEST_F(TTWTest, test_persistent_group)
{
	// create a new context
	p2p_handle = ttw_init();
	ASSERT_TRUE(p2p_handle != NULL);

	// create the persistent group
	EXPECT_EQ(0, ttw_create_group(p2p_handle, TTW_GROUP_PERSISTENT));
	sleep(3);

	// process the backlog of events
	ttw_process_events(p2p_handle);

	// check the IP address of the interface to be the default one
	struct ifreq ifr;
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	ASSERT_FALSE(fd < 0);
	memset(&ifr, 0, sizeof(struct ifreq));
	strcpy(ifr.ifr_name, "p2p-p2p0-0");
	ifr.ifr_addr.sa_family = AF_INET;
	int res = ioctl(fd, SIOCGIFADDR, &ifr);
	ASSERT_FALSE(res < 0);
	struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
	EXPECT_STREQ("192.168.1.101", inet_ntoa(ipaddr->sin_addr));

	// remove the group
	EXPECT_EQ(0, ttw_leave_group(p2p_handle, "p2p-p2p0-0"));

	// process the backlog of events
	ttw_process_events(p2p_handle);

	// close the context
	ttw_close(p2p_handle);

	// create a new context
	p2p_handle = ttw_init();
	ASSERT_TRUE(p2p_handle != NULL);

	// change the group subnet
	EXPECT_EQ(0, ttw_set_group_net(p2p_handle, "192.168.9.222", "255.255.255.0"));

	// change the group ssid
	EXPECT_EQ(0, ttw_set_persistent_ssid(p2p_handle, "GTEST_SSID"));

	// change the group passphrase
	EXPECT_EQ(0, ttw_set_persistent_pass(p2p_handle, "gTestPassphrase123"));

	// re-create the persistent group
	EXPECT_EQ(0, ttw_create_group(p2p_handle, TTW_GROUP_PERSISTENT));
	sleep(3);

	// process the backlog of events
	ttw_process_events(p2p_handle);

	// check the IP address of the interface to be the new one
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ASSERT_FALSE(fd < 0);
	memset(&ifr, 0, sizeof(struct ifreq));
	strcpy(ifr.ifr_name, "p2p-p2p0-1");
	ifr.ifr_addr.sa_family = AF_INET;
	res = ioctl(fd, SIOCGIFADDR, &ifr);
	ASSERT_FALSE(res < 0);
	ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
	EXPECT_STREQ("192.168.9.222", inet_ntoa(ipaddr->sin_addr));

	// check the group SSID to be the new one
	char tmp_ssid[P2P_SSID_LEN];
	EXPECT_EQ(0, ttw_get_persistent_ssid(p2p_handle, tmp_ssid));
	EXPECT_STREQ("GTEST_SSID", tmp_ssid);

	// check the group passphrase to be the new one
	char tmp_pass[P2P_PASSPHRASE_LEN];
	EXPECT_EQ(0, ttw_get_current_pass(p2p_handle, tmp_pass));
	EXPECT_STREQ("gTestPassphrase123", tmp_pass);

	// process the backlog of events
	ttw_process_events(p2p_handle);

	// remove the group
	EXPECT_EQ(0, ttw_leave_group(p2p_handle, "p2p-p2p0-1"));

	// process the backlog of events
	ttw_process_events(p2p_handle);

	// close the context
	ttw_close(p2p_handle);
}

