/**
 * @file ttw_api.h
 *
 * @author R.Cane
 *
 * @brief TTW WiFi direct API
 *
 * @see http://www.wi-fi.org/discover-wi-fi/wi-fi-direct
 */

#ifndef TTW_API_H_
#define TTW_API_H_

#if defined (__cplusplus)
extern "C" {
#endif

#define P2P_IFNAME_LEN		20
#define P2P_GROUPNAME_LEN	20
#define P2P_ADDR_LEN		18
#define P2P_DEVNAME_LEN		33	
#define P2P_DEVTYPE_LEN		14	
#define P2P_SSID_LEN		33
#define P2P_PASSPHRASE_LEN	64
#define P2P_IPADDR_LEN		16
#define P2P_PIN_LEN		9

/**
 * @brief WiFi connection status
 */
typedef enum {
	TTW_CONNECTION_ESTABLISHED = 1,	/**< Connection has been established */
	TTW_CONNECTION_CLOSED,		/**< Connection has been terminated */
} ttw_conn_status_t;

/**
 * @brief P2P group status
 */
typedef enum {
	TTW_GROUP_FORMED = 1,	/**< Group has been formed */
	TTW_GROUP_REMOVED,	/**< Group has been removed */
} ttw_group_status_t;

/**
 * @brief P2P group persistance type
 */
typedef enum {
	TTW_GROUP_NOT_PERSISTENT = 1,	/**< Group is not persistent: the credentials are generated and not stored */
	TTW_GROUP_PERSISTENT,		/**< Group is persistent: the credentials are retrieved from the backend configuration */
} ttw_group_persistence_t;

/**
 * @brief WPS event
 */
typedef enum {
	TTW_WPS_SUCCESS = 1,	/**< WPS pairing succeeded */
	TTW_WPS_FAIL,		/**< WPS pairing failed */
	TTW_WPS_ENTER_PIN_REQ,	/**< WPS authentication request with peer PIN */
	TTW_WPS_PBC_REQ,	/**< WPS authentication request wth push-button method */
	TTW_WPS_CREATE_PIN_REQ,	/**< WPS authentication request with local PIN */
} ttw_wps_evt_t;

/**
 * @brief WPS request
 */
typedef enum {
	TTW_WPS_PBC = 1,	/**< Push-button action */
	TTW_WPS_PEER_PIN,	/**< PIN action with code from peer*/
	TTW_WPS_RANDOM_PIN,	/**< PIN action with locally generated code */
	TTW_WPS_STATIC_PIN,	/**< PIN action with static provided code */
	TTW_WPS_SET_AP_PIN,	/**< Set access-point PIN for the local device (only for group owner) */
	TTW_WPS_CANCEL,		/**< Abort any pending WPS operation (only for group owner) */
} ttw_wps_req_t;

/**
 * @brief Origin of the connection
 */
typedef enum {
	TTW_ORIGIN_PEER = 1,	/**< The connection has been created from the remote peer */
	TTW_ORIGIN_LOCAL,	/**< The connection has been created by the current device */
} ttw_conn_origin_t;

/**
 * @brief Asyncronous events
 */
typedef enum {
	TTW_P2P_EVENT_PEER_FOUND = 1,	/**< New P2P peer found during discovery */
	TTW_P2P_EVENT_CONNECT_REPORT,	/**< A connection has been created or closed */
	TTW_P2P_EVENT_WPS_REPORT,	/**< WPS event occurred */
	TTW_P2P_EVENT_GROUP_REPORT,	/**< A group has been created or removed */
} ttw_p2p_event_t;

/**
 * @brief Handle for the P2P session
 */
struct ttw_p2p_handle_s;
typedef struct ttw_p2p_handle_s ttw_p2p_handle_t;

/**
 * @brief Description of a P2P device
 */
typedef struct ttw_p2p_device_s {
	char p2p_addr[P2P_ADDR_LEN];		/**< P2P (MAC) address of the device */
	char p2p_name[P2P_DEVNAME_LEN];		/**< P2P device name */
	char dev_type[P2P_DEVTYPE_LEN];		/**< Primary device type */
	int is_group_owner;			/**< 1 = the device is a group owner, 0 = it's a client */
} ttw_p2p_device_t;

/**
 * @brief Description of a P2P group
 */
typedef struct ttw_p2p_group_s {
	char name[P2P_GROUPNAME_LEN];		/**< Group name */
	char ssid[P2P_SSID_LEN];		/**< SSID of the group */
	char passphrase[P2P_PASSPHRASE_LEN];	/**< Passphrase of the group (only available if the device is group owner) */
	char owner_addr[P2P_ADDR_LEN];		/**< P2P (MAC) address of the group owner */
	char owner_ipaddr[P2P_IPADDR_LEN];	/**< IP address of the group owner */
	int is_group_owner;			/**< 1 = the current device owns the group, 0 = the current device is a client of the group */
	ttw_group_status_t status;		/**< Group status #ttw_group_status_t */
} ttw_p2p_group_t;

/**
 * @brief Description of a WPS event
 */
typedef struct ttw_wps_info_s {
	ttw_wps_evt_t wps_event;		/**< Event type #ttw_wps_evt_t */
	char peer_address[P2P_ADDR_LEN];	/**< MAC address of the destination device (if present) */
} ttw_wps_info_t;

/**
 * @brief Connection info
 */
typedef struct ttw_p2p_conn_info_s {
	int is_legacy;				/**< 1 = the connected peer is legacy WiFi device, 0 = the peer is WiFi Direct */
	ttw_conn_status_t status;		/**< Connection status #ttw_conn_status_t */
	char client_hw_addr[P2P_ADDR_LEN];	/**< P2P (MAC) address of the client */
	char client_ip_addr[P2P_IPADDR_LEN];	/**< IP address of the client assigned by the group owner */
	ttw_conn_origin_t origin;		/**< Origin of the connection #ttw_conn_origin_t */
} ttw_p2p_conn_info_t;


/**
 * @brief Initialize the P2P context
 *
 * Connects to the backend and provides an handle for the session.
 *
 * @return ttw_p2p_handle_t* : pointer to a P2P context handle (NULL = creation failed)
 */
ttw_p2p_handle_t *ttw_init();

/**
 * @brief Register event callback
 *
 * Registers a callback function for asyncronous events.
 * The callback will be called from inside ttw_process_events()
 *
 * @param ttw_p2p_handle_t* : pointer to a valid P2P context handle
 * @param void (*event_cb)(ttw_p2p_event_t event, void* data) : pointer to a callback function
 *
 * @note
 * <pre>
 *  New P2P device found during discovery:
 *      event: TTW_P2P_EVENT_PEER_FOUND
 *      *data: pointer to a #ttw_p2p_device_t structure with info on the discovered device
 *  P2P group created/deleted:
 *      event: TTW_P2P_EVENT_GROUP_REPORT
 *      *data: pointer to a #ttw_p2p_group_t structure with info on the group
 *  Connection process completed:
 *      event: TTW_P2P_EVENT_CONNECT_REPORT
 *      *data: pointer to a #ttw_p2p_conn_info_t structure with the connection information
 *  WPS event occurred:
 *      event: TTW_P2P_EVENT_WPS_REPORT
 *      *data: pointer to a #ttw_wps_info_t structure with the details of the WPS event
 * </pre>
 * @note
 *  All *data pointers become invalid outside the callback context
 */
void ttw_register_event_cb(ttw_p2p_handle_t *p2p_handle, void (*event_cb)(ttw_p2p_event_t event, void *data));

/**
 * @brief Start the discovery process of new P2P devices
 *
 * Discovered devices are reported with the callback event TTW_P2P_EVENT_PEER_FOUND.
 * The discovery process is automatically stopped when a P2P connection request is performed.
 *
 * @param ttw_p2p_handle_t*  : pointer to a valid P2P context handle
 *
 * @return
 *   < 0 if the discovery has been started successfully\n
 *   = 0 if the discovery startup has failed
 */
int ttw_start_discovery(ttw_p2p_handle_t *p2p_handle);

/**
 * @brief Stop the discovery process of P2P devices
 *
 * @param ttw_p2p_handle_t *p2p_handle  : pointer to a valid P2P context handle
 *
 * @return
 *   < 0 if the discovery has been stopped successfully\n
 *   = 0 if the discovery stop has failed
 */
int ttw_stop_discovery(ttw_p2p_handle_t *p2p_handle);

/**
 * @brief Start the autonomous creation of a group
 *
 * The device will become the owner of the group and the network will become visible to legacy WiFi devices.
 * The data of the created group is reported with the callback event TTW_P2P_EVENT_GROUP_REPORT.
 *
 * Depending on the persistance parameter, the group credentials (SSID/passphrase) can be generated or recalled
 *
 * @param ttw_p2p_handle_t *p2p_handle : pointer to a valid P2P context handle
 * @param ttw_group_persistence_t persistence : type or persistence required for the group #ttw_group_persistence_t
 *
 * @return
 *   < 0 if group creation has failed\n
 *   = 0 if group creation succeded
 */
int ttw_create_group(ttw_p2p_handle_t *p2p_handle, ttw_group_persistence_t persistence);

/**
 * @brief Start the connection to a P2P device with optional group negotiation. Invite the peer if the local device is Group Owner
 *
 * The creation of the group (if any) is notified via the callback event TTW_P2P_EVENT_GROUP_REPORT.
 * The completion of the connection is notified via the callback event TTW_P2P_EVENT_CONNECT_REPORT.
 * If the peer is already group owner, the group negotiation should be skipped by setting the "join" parameter.
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 * @param *p2p_address : P2P (MAC) of the destination device
 * @param go_intent : intention to become group onwer (0 = min, 15 = max)
 * @param wps_req : request type #ttw_wps_req_t
 * @param *wps_pin : (optional) string with PIN for authentication. Used to return the PIN when generated locally
 * @param join : 1 = join an existing group (skip group owner negotiation), 0 = otherwise
 *
 * @return
 *   < 0 if the connection process has failed\n
 *   = 0 if the connection process has started
 */
int ttw_connect(ttw_p2p_handle_t *p2p_handle, const char *p2p_address, int go_intent, ttw_wps_req_t wps_req, char *pin, int join);

/**
 * @brief Start a WPS operation
 *
 * Perform a legacy WPS operation on the network.
 *
 * note: only valid for group owners, clients should use ttw_connect()
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 * @param wps_req : request type #ttw_wps_req_t
 * @param *peer_address : (optional) MAC address of the destination device (NULL = any device)
 * @param *wps_pin : (optional) string with PIN for authentication. Used to return the PIN when generated locally
 * @param pin_timeout : (optional) PIN validity in seconds (0 = no timeout)
 *
 * @return
 *   < 0 if the process has failed\n
 *   = 0 if the request has been successfully sent
 */
int ttw_wps_cmd(ttw_p2p_handle_t *p2p_handle, ttw_wps_req_t wps_req, const char *peer_address, char *wps_pin, int pin_timeout);

/**
 * @brief Disconnect from a group
 *
 * If the device is the owner, the entire group is removed.
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 * @param *group_name : name of the group to leave
 *
 * @return
 *   < 0 if the disconnection has failed\n
 *   = 0 if the disconnection succeded
 */
int ttw_leave_group(ttw_p2p_handle_t *p2p_handle, const char *group_name);

/**
 * @brief Close the P2P context
 *
 * Remove any existing connection.
 * If the device is the owner, the entire group is removed.
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 *
 */
void ttw_close(ttw_p2p_handle_t *p2p_handle);

/**
 * @brief Check for pending events on the backend
 *
 * The call blocks untils an event is found, a signal is received or an error occurred.
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 *
 * @return
 *   < 0 error\n
 *   = 0 pending events found\n
 *   = 1 call interrupted by signal\n
 */
int ttw_pending_events(ttw_p2p_handle_t *p2p_handle);

/**
 * @brief Process any pending event on the backend and invoke the event callback if necessary
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 *
 * @return
 *   < 0 error\n
 *   = 0 success
 */
int ttw_process_events(ttw_p2p_handle_t *p2p_handle);

/**
 * @brief Change the SSID of the persistent group
 *
 * The new value is stored and becomes visible when the persistent group is re-created
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 * @param *ssid : string containing the new SSID (max 33 chars)
 *
 * @return
 *   < 0 if the operation has failed\n
 *   = 0 if the operation succeded
 */
int ttw_set_persistent_ssid(ttw_p2p_handle_t *p2p_handle, const char *ssid);

/**
 * @brief Change the passphrase of the persistent group
 *
 * The new value is stored and becomes visible when the persistent group is re-created
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 * @param *ssid : string containing the new passphrase (8 - 64 chars)
 *
 * @return
 *   < 0 if the operation has failed\n
 *   = 0 if the operation succeded
 */
int ttw_set_persistent_pass(ttw_p2p_handle_t *p2p_handle, const char *passphrase);

/**
 * @brief Get the passphrase of the current active group (only if the device is group owner)
 *
 * The passphrase is copied in the provided buffer, which should have enogh space to carry at least P2P_PASSPHRASE_LEN characters
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 * @param *passphrase : pointer to a buffer where the retreived passphrase should be copied
 *
 * @return
 *   < 0 if the operation has failed\n
 *   = 0 if the operation succeded
 */
int ttw_get_current_pass(ttw_p2p_handle_t *p2p_handle, char *passphrase);

/**
 * @brief Get the SSID of the persistent group
 *
 * The SSID is copied in the provided buffer, which should have enogh space to carry at least P2P_SSID_LEN characters
 *
 * @param *p2p_handle : pointer to a valid P2P context handle
 * @param *ssid : pointer to a buffer where the retreived SSID should be copied
 *
 * @return
 *   < 0 if the operation has failed\n
 *   = 0 if the operation succeded
 */
int ttw_get_persistent_ssid(ttw_p2p_handle_t *p2p_handle, char *ssid);

/**
 * @brief Set the network parameters (IP address / netmask) of the group owner
 *
 * The parameters become effective next time a group is created
 * The new settings are persistent only within a P2P context, therefore are lost after a call to ttw_close()
 *
 * @param *ip_address : string cointaining the IP address of the group owner
 * @param *netmask : string containing the netmask of the network
 *
 * @return
 *   < 0 if the operation has failed\n
 *   = 0 if the operation succeded
 */
int ttw_set_group_net(ttw_p2p_handle_t *p2p_handle, const char *ip_address, const char *netmask);

#if defined (__cplusplus)
}
#endif

#endif // TTW_API_H_
