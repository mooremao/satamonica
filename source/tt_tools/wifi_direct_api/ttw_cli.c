/**
 * @file ttw_cli.c
 *
 * @author R.Cane
 *
 * @brief Test tool for WiFi direct (P2P) using TTW API
 *
 */
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include "ttw_api.h"

/** global variables */
pthread_t event_thread;
ttw_p2p_handle_t *p2p_handle = NULL;
int device_is_owner;
int event_thread_kill;

/** static functions */
static void show_cmd_list(void);
static void help_handler(int argc, char **argv);
static void async_event_handler(ttw_p2p_event_t event, void *data);
static void init_handler(int argc, char **argv);
static void close_handler(int argc, char **argv);
static void quit_handler(int argc, char **argv);
static void start_discovery_handler(int argc, char **argv);
static void stop_discovery_handler(int argc, char **argv);
static void create_group_handler(int argc, char **argv);
static void leave_group_handler(int argc, char **argv);
static void wps_pbc_handler(int argc, char **argv);
static void wps_pin_handler(int argc, char **argv);
static void wps_rnd_pin_handler(int argc, char **argv);
static void wps_ap_pin_handler(int argc, char **argv);
static void wps_cancel_handler(int argc, char **argv);
static void connect_pbc_handler(int argc, char **argv);
static void connect_peer_pin_handler(int argc, char **argv);
static void connect_static_pin_handler(int argc, char **argv);
static void connect_rnd_pin_handler(int argc, char **argv);
static void invite_handler(int argc, char **argv);
static void set_ssid_handler(int argc, char **argv);
static void set_passphrase_handler(int argc, char **argv);
static void get_passphrase_handler(int argc, char **argv);
static void get_ssid_handler(int argc, char **argv);
static void set_grp_net_handler(int argc, char **argv);

/** global list of available commands */
struct  {
	char *cmd_name;
	int cmd_args;
	void (*cmd_handler) (int argc, char **argv);
	char *cmd_desc;
	char *cmd_help;
} cmd_list[] = {
	{"help", 1, help_handler, "Show command help", "help <cmd>"},
	{"init", 0, init_handler, "Initialize the TTW P2P context", "init (no arguments)"},
	{"quit", 0, quit_handler, "Exit the tool (close the TTW context if open)", "quit (no arguments)"},
	{"close", 0, close_handler, "Close the TTW P2P context", "close (no arguments)"},
	{"start_discovery", 0, start_discovery_handler, "Start P2P devices discovery", "start_discovery (no arguments)"},
	{"stop_discovery", 0, stop_discovery_handler, "Stop P2P devices discovery", "stop_discovery (no arguments)"},
	{"create_group", 1, create_group_handler, "Start autonomous P2P group", "create_group <persistence>\n" \
							"\t\t<persistence>:\n" \
							"\t\t\t0: not persistent\n" \
							"\t\t\t1: restore a persistent group"},
	{"leave_group", 1, leave_group_handler, "Leave a P2P group", "leave_group <name>\n" \
							"\t\t<name>: name of the group to leave"},
	{"wps_pbc", 1, wps_pbc_handler, "Start a WPS press-button pairing", "wps_pbc <peer>\n" \
							"\t\t<peer>: MAC address of the destination (\"any\" for any destination)"},
	{"wps_pin", 3, wps_pin_handler, "Start a WPS pairing with provided PIN code", "wps_pin <peer> <pin> <timeout>\n" \
							"\t\t<peer>: MAC address of the destination (\"any\" for any destination)\n" \
							"\t\t<pin>: PIN code to use\n" \
							"\t\t<timeout>: PIN validity in seconds (0 = no timeout)"},
	{"wps_rnd_pin", 1, wps_rnd_pin_handler, "Start a WPS pairing with generated PIN code", "wps_rnd_pin <peer>\n" \
							"\t\t<peer>: MAC address of the destination (\"any\" for any destination)"},
	{"wps_ap_pin", 2, wps_ap_pin_handler, "Set the \"access point\" PIN", "wps_ap_pin <pin> <timeout>\n" \
							"\t\t<pin>: PIN code to use\n" \
							"\t\t<timeout>: PIN validity in seconds (0 = no timeout)"},
	{"wps_cancel", 0, wps_cancel_handler, "Stop any pending WPS operation", "wps_cancel (no arguments)"},
	{"invite", 1, invite_handler, "Invite a P2P peer to connect", "invite <peer>\n" \
							"\t\t<peer>: MAC address of the peer to invite"},
	{"connect_pbc", 2, connect_pbc_handler, "Start a P2P connection with \"press-button\" authentication method", "connect_pbc <peer> <go_intent>\n" \
							"\t\t<peer>: MAC address of the destination\n" \
							"\t\t<go_intent>:\n\t\t\t0 - 15: intention level for group-owner negotiation (0 = min, 15 = max)\n" \
							"\t\t\t\"join\": connect to an existing group owner"},
	{"connect_peer_pin", 3, connect_peer_pin_handler, "Start a P2P connection with PIN code from the peer", "connect_peer_pin <peer> <pin> <go_intent>\n" \
							"\t\t<peer>: MAC address of the destination\n" \
							"\t\t<pin>: PIN code to use\n" \
							"\t\t<go_intent>:\n\t\t\t0 - 15: intention level for group-owner negotiation (0 = min, 15 = max)"},
	{"connect_static_pin", 3, connect_static_pin_handler, "Start a P2P connection with provided static PIN code", "connect_static_pin <peer> <pin> <go_intent>\n" \
							"\t\t<peer>: MAC address of the destination\n" \
							"\t\t<pin>: PIN code to use\n" \
							"\t\t<go_intent>:\n\t\t\t0 - 15: intention level for group-owner negotiation (0 = min, 15 = max)\n" \
							"\t\t\t\"join\": connect to an existing group owner"},
	{"connect_rnd_pin", 2, connect_rnd_pin_handler, "Start a P2P connection with generated random PIN code", "connect_rnd_pin <peer> <go_intent>\n" \
							"\t\t<peer>: MAC address of the destination\n" \
							"\t\t<go_intent>:\n\t\t\t0 - 15: intention level for group-owner negotiation (0 = min, 15 = max)\n" \
							"\t\t\t\"join\": connect to an existing group owner"},
	{"set_ssid", 1, set_ssid_handler, "Set the persistent group SSID", "set_ssid <SSID>\n" \
							"\t\t<SSID>: new SSID of the group"},
	{"set_passphrase", 1, set_passphrase_handler, "Set the persistent group passphrase", "set_passphrase <passphrase>\n" \
							"\t\t<passphrase>: new passphrase of the group"},
	{"get_passphrase", 0, get_passphrase_handler, "Get the current group passphrase", "get_passphrase (no arguments)"},
	{"get_ssid", 0, get_ssid_handler, "Get the persistent group SSID", "get_ssid (no arguments)"},
	{"set_grp_net", 2, set_grp_net_handler, "Set the IP address and netmask of the group", "set_grp_net <ip> <netmask>\n" \
							"\t\t<ip>: IP address\n" \
							"\t\t<netmask>: net mask"},
};

/** handler for SIGUSR1 */
static void sigusr1_handler(int num){
	/** set the flag to terminate the thread */
	event_thread_kill = 1;
}

/** listening loop on backend events */
static void *event_loop(void *arg)
{
	int res;
	struct sigaction action;

	/** Install SIGSUSR1 handler to terminate the loop */
	memset(&action, 0, sizeof(struct sigaction));
	action.sa_handler = sigusr1_handler;
	sigaction(SIGUSR1, &action, NULL);

	while (!event_thread_kill) {
		res = ttw_pending_events(p2p_handle);
		if (res < 0) {
			fprintf(stderr, "Error in event polling\n");
			continue;
		}
		else
			if (res == 0)
				ttw_process_events(p2p_handle);
	}
}

/** handler for "help" command */
static void help_handler(int argc, char **argv)
{
	int i;
	for (i=0; i < sizeof(cmd_list)/sizeof(cmd_list[0]);i++) {
		if (strcmp(cmd_list[i].cmd_name, argv[1]) == 0)
			printf("%s\n\tUsage: %s\n", cmd_list[i].cmd_desc, cmd_list[i].cmd_help);

	}
}

/** handler for "invite" command */
static void invite_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	if (!device_is_owner) {
		fprintf(stderr, "Device is not group owner, use \"connect_xxx\"!\n");
		return;
	}

	ret = ttw_connect(p2p_handle, argv[1], 0, 0, NULL, 0);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("P2P invitation sent\n");
}

/** handler for "connect_static_pin" command */
static void connect_static_pin_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	if (device_is_owner) {
		fprintf(stderr, "Device is group owner, use \"invite\"!\n");
		return;
	}

	if (strcmp(argv[3], "join") == 0)
		ret = ttw_connect(p2p_handle, argv[1], 0, TTW_WPS_STATIC_PIN, argv[2], 1);
	else {
		int go_intent;
		sscanf(argv[3], "%2d", &go_intent);

		if ((go_intent < 0) || (go_intent > 15)) {
			fprintf(stderr, "Invalid go_intent mode (0 - 15)!\n");
			return;
		}
		ret = ttw_connect(p2p_handle, argv[1], go_intent, TTW_WPS_STATIC_PIN, argv[2], 0);
	}
	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("P2P connection request sent\n");
}

/** handler for "connect_peer_pin" command */
static void connect_peer_pin_handler(int argc, char **argv)
{
	int ret;
	int go_intent;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	if (device_is_owner) {
		fprintf(stderr, "Device is group owner, use \"invite\"!\n");
		return;
	}

	sscanf(argv[3], "%2d", &go_intent);

	if ((go_intent < 0) || (go_intent > 15)) {
		fprintf(stderr, "Invalid go_intent mode (0 - 15)!\n");
		return;
	}
	ret = ttw_connect(p2p_handle, argv[1], go_intent, TTW_WPS_PEER_PIN, argv[2], 0);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("P2P connection request sent\n");
}

/** handler for "connect_rnd_pin" command */
static void connect_rnd_pin_handler(int argc, char **argv)
{
	int ret;
	char pin[P2P_PIN_LEN];

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	if (device_is_owner) {
		fprintf(stderr, "Device is group owner, use \"invite\"!\n");
		return;
	}

	pin[0] = '\0';

	if (strcmp(argv[2], "join") == 0)
		ret = ttw_connect(p2p_handle, argv[1], 0, TTW_WPS_RANDOM_PIN, pin, 1);
	else {
		int go_intent;
		sscanf(argv[2], "%2d", &go_intent);

		if ((go_intent < 0) || (go_intent > 15)) {
			fprintf(stderr, "Invalid go_intent mode (0 - 15)!\n");
			return;
		}
		ret = ttw_connect(p2p_handle, argv[1], go_intent, TTW_WPS_RANDOM_PIN, pin, 0);
	}
	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("P2P connection request sent with generated PIN:%s\n", pin);
}

/** handler for "connect_pbc" command */
static void connect_pbc_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	if (device_is_owner) {
		fprintf(stderr, "Device is group owner, use \"invite\"!\n");
		return;
	}

	if (strcmp(argv[2], "join") == 0)
		ret = ttw_connect(p2p_handle, argv[1], 0, TTW_WPS_PBC, NULL, 1);
	else {
		int go_intent;
		sscanf(argv[2], "%2d", &go_intent);

		if ((go_intent < 0) || (go_intent > 15)) {
			fprintf(stderr, "Invalid go_intent mode (0 - 15)!\n");
			return;
		}
		ret = ttw_connect(p2p_handle, argv[1], go_intent, TTW_WPS_PBC, NULL, 0);
	}
	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("P2P connection request sent\n");
}

/** handler for "leave_group" command */
static void leave_group_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_leave_group(p2p_handle, argv[1]);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("Group left successfully\n");
}

/** handler for "set_ssid" command */
static void set_ssid_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_set_persistent_ssid(p2p_handle, argv[1]);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("SSID set successfully\n");
}

/** handler for "set_passphrase" command */
static void set_passphrase_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_set_persistent_pass(p2p_handle, argv[1]);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("Passphrase set successfully\n");
}

/** handler for "get_passphrase" command */
static void get_passphrase_handler(int argc, char **argv)
{
	int ret;
	char passphrase[P2P_PASSPHRASE_LEN];

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_get_current_pass(p2p_handle, passphrase);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}

	printf("Passphrase: \"%s\"\n", passphrase);
}

/** handler for "get_ssid" command */
static void get_ssid_handler(int argc, char **argv)
{
	int ret;
	char ssid[P2P_SSID_LEN];

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_get_persistent_ssid(p2p_handle, ssid);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}

	printf("SSID: \"%s\"\n", ssid);
}

/** handler for "set_grp_net" command */
static void set_grp_net_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_set_group_net(p2p_handle, argv[1], argv[2]);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("Network parameters set successfully\n");
}

/** handler for "wps_cancel" command */
static void wps_cancel_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_wps_cmd(p2p_handle, TTW_WPS_CANCEL, NULL, NULL, 0);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("\nWPS operation cancelled\n");
}

/** handler for "wps_ap_pin" command */
static void wps_ap_pin_handler(int argc, char **argv)
{
	int ret, timeout;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	sscanf(argv[2], "%4d", &timeout);

	ret = ttw_wps_cmd(p2p_handle, TTW_WPS_SET_AP_PIN, NULL, argv[1], timeout);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("Access point PIN set successfully\n");
}

/** handler for "wps_pin" command */
static void wps_pin_handler(int argc, char **argv)
{
	int ret, timeout;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	sscanf(argv[2], "%4d", &timeout);

	if (strcmp(argv[1], "any") == 0)
		ret = ttw_wps_cmd(p2p_handle, TTW_WPS_PEER_PIN, NULL, argv[2], timeout);
	else
		ret = ttw_wps_cmd(p2p_handle, TTW_WPS_PEER_PIN, argv[1], argv[2], timeout);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("WPS PIN pairing started....\n");
}

/** handler for "wps_rdn_pin" command */
static void wps_rnd_pin_handler(int argc, char **argv)
{
	int ret;
	char pin[P2P_PIN_LEN];

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	pin[0] = '\0';

	if (strcmp(argv[1], "any") == 0)
		ret = ttw_wps_cmd(p2p_handle, TTW_WPS_RANDOM_PIN, NULL, pin, 0);
	else
		ret = ttw_wps_cmd(p2p_handle, TTW_WPS_RANDOM_PIN, argv[1], pin, 0);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("WPS PIN pairing started with PIN \"%s\"....\n", pin);
}

/** handler for "wps_pbc" command */
static void wps_pbc_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	if (strcmp(argv[1], "any") == 0)
		ret = ttw_wps_cmd(p2p_handle, TTW_WPS_PBC, NULL, NULL, 0);
	else
		ret = ttw_wps_cmd(p2p_handle, TTW_WPS_PBC, argv[1], NULL, 0);

	if (ret < 0) {
		fprintf(stderr, "command failed!\n");
		return;
	}
	printf("WPS PBC pairing started....\n");
}

/** handler for "init" command */
static void init_handler(int argc, char **argv)
{
	if (p2p_handle != NULL) {
		fprintf(stderr, "P2P context already initialized!\n");
		return;
	}

	p2p_handle = ttw_init();
	device_is_owner = 0;

	if (p2p_handle) {
		/** create the event thread */
		event_thread_kill = 0;
		if(pthread_create(&event_thread, NULL, event_loop, NULL)) {
			fprintf(stderr, "Error creating the event thread\n");
			ttw_close(p2p_handle);
			p2p_handle = NULL;
			return;
		}

		/** Register the callback to handle async events */
		ttw_register_event_cb(p2p_handle, async_event_handler);

		printf("P2P context initialized\n");

	} else
		fprintf(stderr, "P2P context initialization failed!\n");
}

/** handler for "quit" command */
static void quit_handler(int argc, char **argv)
{
	if (p2p_handle != NULL) {
		pthread_kill(event_thread, SIGUSR1);
		pthread_join(event_thread, NULL);
		ttw_close(p2p_handle);
		printf("P2P context closed\n");
	}

	exit(0);
}

/** handler for "close" command */
static void close_handler(int argc, char **argv)
{
	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	pthread_kill(event_thread, SIGUSR1);
	pthread_join(event_thread, NULL);
	ttw_close(p2p_handle);
	p2p_handle = NULL;

	printf("P2P context closed\n");
}

/** handler for "start_discovery" command */
static void start_discovery_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_start_discovery(p2p_handle);
	if (ret < 0) {
		fprintf(stderr, "Failed to start P2P discovery!\n");
		return;
	}
	printf("P2P discovery started\n");
}

/** handler for "stop_discovery" command */
static void stop_discovery_handler(int argc, char **argv)
{
	int ret;

	if (p2p_handle == NULL) {
		fprintf(stderr, "P2P context not initialized!\n");
		return;
	}

	ret = ttw_stop_discovery(p2p_handle);
	if (ret < 0) {
		fprintf(stderr, "Failed to stop P2P discovery!\n");
		return;
	}
	printf("P2P discovery stopped\n");
}

/** handler for "create_group" command */
static void create_group_handler(int argc, char **argv)
{
	int res;
	int pers;

	sscanf(argv[1], "%1d", &pers);

	/* Create an autonomous group */
	switch (pers) {
		case 0:
			res = ttw_create_group(p2p_handle, TTW_GROUP_NOT_PERSISTENT);
			break;
		case 1:
			res = ttw_create_group(p2p_handle, TTW_GROUP_PERSISTENT);
			break;
		default:
			fprintf(stderr, "Invalid persistence value (0-1)!\n");
			return;
	}

	if (res < 0)
		fprintf(stderr, "Error creating group\n");

	return;
}

/** show the list of available commands */
static void show_cmd_list()
{
	int i;
	printf ("\nAvailable commands:\n");
	for (i=0; i < sizeof(cmd_list)/sizeof(cmd_list[0]);i++) {
		printf("\t%s : %s\n", cmd_list[i].cmd_name, cmd_list[i].cmd_desc);
	}
}

/** process command */
static void process_cmd (int argc, char **argv)
{
	int i;

	if ((strcmp(argv[0], "help") == 0) && (argc==1)) {
		show_cmd_list();
		return;
	}

	for (i=0; i < sizeof(cmd_list)/sizeof(cmd_list[0]);i++) {
		if (strcmp(argv[0],  cmd_list[i].cmd_name) == 0) {
			printf("\n");
			if ((argc-1) != cmd_list[i].cmd_args) {
				printf("Usage: %s\n", cmd_list[i].cmd_help);
				return;
			} else
				return  cmd_list[i].cmd_handler(argc, argv);
		}
	}

	printf ("Invalid command, type 'help <cmd>' for a specific command or just 'help' for a complete list\n");
}

/** handler for backend events */
static void async_event_handler(ttw_p2p_event_t event, void *data)
{
	switch(event) {
		case TTW_P2P_EVENT_PEER_FOUND:
			printf("\nReceived TTW_P2P_EVENT_PEER_FOUND:\n");
			printf("\tpeer address: %s\n", ((ttw_p2p_device_t *)data)->p2p_addr);
			printf("\tpeer name: \"%s\"\n", ((ttw_p2p_device_t *)data)->p2p_name);
			printf("\tdevice type: %s\n", ((ttw_p2p_device_t *)data)->dev_type);
			printf("\tis_group_owner: %s\n", (((ttw_p2p_device_t *)data)->is_group_owner) ? "Yes" : "No");
			printf("\n");
			break;

		case TTW_P2P_EVENT_GROUP_REPORT:
			printf("\nReceived TTW_P2P_EVENT_GROUP_REPORT:\n");
			printf("\tgroup name: %s\n", ((ttw_p2p_group_t *)data)->name);
			printf("\tgroup SSID: %s\n", ((ttw_p2p_group_t *)data)->ssid);
			printf("\tgroup passphrase: %s\n", ((ttw_p2p_group_t *)data)->passphrase);
			printf("\tgroup owner_addr: %s\n", ((ttw_p2p_group_t *)data)->owner_addr);
			printf("\tgroup owner_ipaddr: %s\n", ((ttw_p2p_group_t *)data)->owner_ipaddr);
			printf("\tgroup is_owner: %s\n", (((ttw_p2p_group_t *)data)->is_group_owner) ? "Yes" : "No");
			printf("\tgroup status: %s\n", (((ttw_p2p_group_t *)data)->status == TTW_GROUP_FORMED) ? "FORMED" : "REMOVED");
			printf("\n");
			device_is_owner = ((ttw_p2p_group_t *)data)->is_group_owner && !(((ttw_p2p_group_t *)data)->status == TTW_GROUP_REMOVED);
			break;

		case TTW_P2P_EVENT_CONNECT_REPORT:
			printf("\nReceived TTW_P2P_EVENT_CONNECT_REPORT:\n");
			printf("\tconnection is_legacy: %s\n", (((ttw_p2p_conn_info_t *)data)->is_legacy) ? "Yes" : "No");
			printf("\tconnection status: %s\n", (((ttw_p2p_conn_info_t *)data)->status == TTW_CONNECTION_ESTABLISHED) ? "ESTABLISHED" : "CLOSED");
			printf("\tconnection origin: %s\n", (((ttw_p2p_conn_info_t *)data)->origin == TTW_ORIGIN_PEER) ? "PEER" : "LOCAL");
			printf("\tconnection client_hw_addr: %s\n", ((ttw_p2p_conn_info_t *)data)->client_hw_addr);
			printf("\tconnection client_ip_addr: %s\n", ((ttw_p2p_conn_info_t *)data)->client_ip_addr);
			printf("\n");
			break;

		case TTW_P2P_EVENT_WPS_REPORT:
			if (((ttw_wps_info_t *)data)->wps_event == TTW_WPS_CREATE_PIN_REQ) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: CREATE PIN REQUEST\n");
				printf("\tWPS peer: %s\n", ((ttw_wps_info_t *)data)->peer_address);
				printf("\n");
			}
			if (((ttw_wps_info_t *)data)->wps_event == TTW_WPS_ENTER_PIN_REQ) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: ENTER PIN REQUEST\n");
				printf("\tWPS peer: %s\n", ((ttw_wps_info_t *)data)->peer_address);
				printf("\n");
			}
			if (((ttw_wps_info_t *)data)->wps_event == TTW_WPS_PBC_REQ) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: PRESS-BUTTON REQUEST\n");
				printf("\tWPS peer: %s\n", ((ttw_wps_info_t *)data)->peer_address);
				printf("\n");
			}
			if (((ttw_wps_info_t *)data)->wps_event == TTW_WPS_FAIL) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: request failed\n");
				printf("\n");
			}
			if (((ttw_wps_info_t *)data)->wps_event == TTW_WPS_SUCCESS) {
				printf("\nReceived TTW_P2P_EVENT_WPS_REPORT:\n");
				printf("\tWPS event: request completed successfully\n");
				printf("\n");
			}
			break;

		default:
			break;
	}
}

/**
 * Main function
 */
int main (int argc, char **argv)
{
	char cmdbuf[256];
	char *cmd_argv[10];
	char *cmd, *pos;
	int cmd_argc;
	int cli_quit= 0;


	printf("\n");
	printf("******************************\n");
	printf("*      TTW API test tool     *\n");
	printf("******************************\n");
	printf("\n");
	printf ("type 'help <cmd>' for a specific command or just 'help' for a complete list\n");
	do {
		printf("> ");
		cmd = fgets(cmdbuf, sizeof(cmdbuf), stdin);
		if (cmd == NULL)
			break;
		if (*cmd == '\n')
			continue;
		pos = cmd;
		while (*pos != '\0') {
			if (*pos == '\n') {
				*pos = '\0';
				break;
			}
			pos++;
		}
		cmd_argc = 0;
		pos = cmd;
		for (;;) {
			while (*pos == ' ')
				pos++;
			if (*pos == '\0')
				break;
			cmd_argv[cmd_argc] = pos;
			cmd_argc++;
			if (cmd_argc == 5)
				break;
			if (*pos == '"') {
				char *pos2 = strrchr(pos, '"');
				if (pos2)
					pos = pos2 + 1;
			}
			while (*pos != '\0' && *pos != ' ')
				pos++;
			if (*pos == ' ')
				*pos++ = '\0';
		}

		if (argc)
			process_cmd(cmd_argc, cmd_argv);
		printf("\n");

	} while (!cli_quit);

	return 0;
}
