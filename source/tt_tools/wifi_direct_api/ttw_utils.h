/**
 * @file ttw_utils.h
 *
 * @author R.Cane
 *
 * @brief Support functions for TTW WiFi direct API
 *
 */

/** Configuration macros */
#define WPA_CTRL_IF_PATH "/run/wpa_supplicant"
#define WPA_CTRL_IF_NAME "p2p0"
#define TTW_DHCP_SERVER_LEASE_FILE "/tmp/udhcpd.leases"
#define TTW_DHCP_SERVER_CONFIG "/tmp/udhcpd.conf"
#define TTW_DHCP_SERVER_DEFAULT_IP "192.168.1.101"
#define TTW_DHCP_SERVER_DEFAULT_MASK "255.255.255.0"
#define TTW_DHCP_POOL_START 20
#define TTW_DHCP_POOL_END 100

/** Debug macros */
#define dbg_printf(...) do {} while (0)
//#define dbg_printf(...) do { printf(__VA_ARGS__); } while (0)
#define dbg_printf_err(...) do { printf(__VA_ARGS__); } while (0)

/** Main handle for the framework */
struct ttw_p2p_handle_s {
	struct wpa_ctrl *main_ctrl_if;
	struct wpa_ctrl *main_mon_if;
	struct wpa_ctrl *grp_ctrl_if;
	struct wpa_ctrl *grp_mon_if;
	int is_group_owner;
	char group_name[P2P_GROUPNAME_LEN];
	char group_addr[P2P_ADDR_LEN];
	char peer_if_addr[P2P_ADDR_LEN];
	struct in_addr dhcp_srv_ip;
	struct in_addr dhcp_srv_mask;
	void (*event_cb)(ttw_p2p_event_t event, void *data);
};

/** Event parsing */
void ttw_parse_event(ttw_p2p_handle_t *p2p_handle, char *buf, int group_if);






