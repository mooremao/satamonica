/**
 * @file ttw_utils.c
 *
 * @author R.Cane
 *
 * @brief Support functions for TTW WiFi direct API
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <regex.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>

#include "wpa_ctrl.h"
#include "ttw_api.h"
#include "ttw_utils.h"

#define MIN(a,b) ((a) < (b) ? a : b)

/** DHCP functions */
static void ttw_start_dhcp_server(ttw_p2p_handle_t *p2p_handle, const char *ifname);
static void ttw_start_dhcp_client(const char *ifname);
static void ttw_stop_dhcp_client(void);
static void ttw_stop_dhcp_server(void);

/** Network utils */
static void ttw_get_dhcp_lease(const char *mac_addr, char *ip);
static void ttw_ifname2data(const char *ifname, char *ip, char *mac);
static void ttw_mac2ip_arp(const char *mac_addr, char *ip);
static int ttw_attach_grp_if(ttw_p2p_handle_t *p2p_handle, const char *if_name);

/** Event parsing */
static int ttw_parse_p2p_device_found(char *buf, ttw_p2p_device_t *device_info);
static int ttw_parse_p2p_group_started(char *buf, ttw_p2p_group_t *group_info);
static int ttw_parse_sta_connected(char *buf, ttw_p2p_conn_info_t *conn_info);
static int ttw_parse_sta_disconnected(char *buf, ttw_p2p_conn_info_t *conn_info);
static int ttw_parse_p2p_group_removed(char *buf, ttw_p2p_group_t *group_info);
static int ttw_regexp_match(const char *buf, const char *regexp_string, int *start_off, int *finish_off);
static int ttw_parse_p2p_go_neg_success(char *buf, char *peer_if);
static int ttw_parse_p2p_go_neg_req(char *buf, ttw_wps_info_t *wps_info);
static int ttw_parse_wps_prov_disc(char *buf, ttw_wps_info_t *wps_info);


/**
 * Parser for wpa_supplicant messages
 *
 * format:
 *   <xx> <message>
 */
#define TTW_EVENT_REGEXP "^<[[:digit:]]+>(\\w+)"
void ttw_parse_event(ttw_p2p_handle_t *p2p_handle, char *buf, int group_if)
{
	int res;
	int start_off, finish_off;

	/** extract the event string */
	res = ttw_regexp_match(buf, TTW_EVENT_REGEXP, &start_off, &finish_off);

	if (res < 0) {
		dbg_printf_err("ttw_parse_event(): invalid event format\n");
		return;
	}

	/** event: P2P_EVENT_GO_NEG_SUCCESS */
	if (strncmp(buf + start_off, P2P_EVENT_GO_NEG_SUCCESS, strlen(P2P_EVENT_GO_NEG_SUCCESS)) == 0) {

		dbg_printf("ttw_parse_event(): found event P2P_EVENT_GO_NEG_SUCCESS\n");

		if (ttw_parse_p2p_go_neg_success(buf + start_off, p2p_handle->peer_if_addr) < 0) {
			dbg_printf_err("ttw_parse_event(): error parsing event P2P_EVENT_GO_NEG_SUCCESS\n");
			return;
		}
		return;
	}

	/** event: P2P_EVENT_DEVICE_FOUND */
	if (strncmp(buf + start_off, P2P_EVENT_DEVICE_FOUND, strlen(P2P_EVENT_DEVICE_FOUND)) == 0) {
		ttw_p2p_device_t peer_data;
		memset(&peer_data, 0, sizeof(peer_data));

		dbg_printf("ttw_parse_event(): found event P2P_EVENT_DEVICE_FOUND\n");
		if (ttw_parse_p2p_device_found(buf + start_off, &peer_data) < 0) {
			dbg_printf_err("ttw_parse_event(): error parsing event P2P_EVENT_DEVICE_FOUND\n");
			return;
		}
		if(p2p_handle->event_cb)
			p2p_handle->event_cb(TTW_P2P_EVENT_PEER_FOUND, &peer_data);
		return;
	}

	/** event: P2P_EVENT_GROUP_STARTED */
	if (strncmp(buf + start_off, P2P_EVENT_GROUP_STARTED, strlen(P2P_EVENT_GROUP_STARTED)) == 0) {
		ttw_p2p_group_t group_data;
		memset(&group_data, 0, sizeof(group_data));

		dbg_printf("ttw_parse_event(): found event P2P_EVENT_GROUP_STARTED\n");

		if (ttw_parse_p2p_group_started(buf + start_off, &group_data) < 0) {
			dbg_printf_err("ttw_parse_event(): error parsing event P2P_EVENT_GROUP_STARTED\n");
			return;
		}
		group_data.status = TTW_GROUP_FORMED;
		strncpy(p2p_handle->group_name, group_data.name, P2P_GROUPNAME_LEN);
		if (group_data.is_group_owner) {
			char local_mac[P2P_ADDR_LEN];

			/** the local device is the owner of the group */
			p2p_handle->is_group_owner = 1;

			/** attach the group interface to the API to get events from it */
			ttw_attach_grp_if(p2p_handle, group_data.name);

			/** start the DHCP server */
			ttw_start_dhcp_server(p2p_handle, group_data.name);

			/** get IP and MAC address of the local group interface */
			ttw_ifname2data(group_data.name, group_data.owner_ipaddr, local_mac);

			/** set the group owner address */
			strncpy(p2p_handle->group_addr, local_mac, P2P_ADDR_LEN);

			/** notify the API user of the created group */
			if(p2p_handle->event_cb)
				p2p_handle->event_cb(TTW_P2P_EVENT_GROUP_REPORT, &group_data);
		} else {
			int i;
			ttw_p2p_conn_info_t conn_info;
			memset(&conn_info, 0, sizeof(conn_info));

			/** the local device is a client of the group */
			p2p_handle->is_group_owner = 0;

			/** start the DHCP client */
			ttw_start_dhcp_client(group_data.name);

			/** as P2P client, the connection started from the local device */
			conn_info.status = TTW_CONNECTION_ESTABLISHED;
			conn_info.is_legacy = 0;
			conn_info.origin = TTW_ORIGIN_LOCAL;

			/** get IP and MAC address of the local group interface */
			ttw_ifname2data(group_data.name, conn_info.client_ip_addr, conn_info.client_hw_addr);

			/** try to get the IP address of the owner (i.e. the DHCP server) */
			for (i=0; i < 3; i++) {
				dbg_printf("ttw_parse_event(): try to get IP for the server %s\n", p2p_handle->peer_if_addr);

				/** query the ARP cache for the address */
				ttw_mac2ip_arp(p2p_handle->peer_if_addr, group_data.owner_ipaddr);

				/** if found, stops */
				if (group_data.owner_ipaddr[0] != '\0')
					break;

				dbg_printf("ttw_parse_event(): no IP found for the server %s, retry...\n", p2p_handle->peer_if_addr);
			}

			/** notify the API user of the new group and the new connection */
			if(p2p_handle->event_cb) {
				p2p_handle->event_cb(TTW_P2P_EVENT_GROUP_REPORT, &group_data);
				p2p_handle->event_cb(TTW_P2P_EVENT_CONNECT_REPORT, &conn_info);
			}
		}
		return;
	}

	/** event: P2P_EVENT_GROUP_REMOVED */
	if (strncmp(buf + start_off, P2P_EVENT_GROUP_REMOVED, strlen(P2P_EVENT_GROUP_REMOVED)) == 0) {
		ttw_p2p_group_t group_data;
		memset(&group_data, 0, sizeof(group_data));

		dbg_printf("ttw_parse_event(): found event P2P_EVENT_GROUP_REMOVED\n");

		if (ttw_parse_p2p_group_removed(buf + start_off, &group_data) < 0) {
			dbg_printf("ttw_parse_event(): error parsing event P2P_EVENT_GROUP_REMOVED\n");
			return;
		}
		group_data.status = TTW_GROUP_REMOVED;
		group_data.ssid[0] = '\0';
		group_data.passphrase[0] = '\0';
		group_data.owner_addr[0] = '\0';
		group_data.owner_ipaddr[0] = '\0';
		p2p_handle->group_name[0] = '\0';
		p2p_handle->group_addr[0] = '\0';
		p2p_handle->is_group_owner = 0;
		if (group_data.is_group_owner) {
			/** the device is no longer a group owner -> stop the DHCP server */
			ttw_stop_dhcp_server();

			/** notify the API user of the group removal */
			if(p2p_handle->event_cb)
				p2p_handle->event_cb(TTW_P2P_EVENT_GROUP_REPORT, &group_data);
		} else {
			ttw_p2p_conn_info_t conn_info;
			memset(&conn_info, 0, sizeof(conn_info));

			conn_info.status = TTW_CONNECTION_CLOSED;
			conn_info.is_legacy = 0;
			conn_info.client_ip_addr[0] = '\0';
			conn_info.client_hw_addr[0] = '\0';
			conn_info.origin = TTW_ORIGIN_LOCAL;

			/** the device is no longer connected to a group -> stop the DHCP client */
			ttw_stop_dhcp_client();

			/** notify the API user of the group removal and the disconnection */
			if(p2p_handle->event_cb) {
				p2p_handle->event_cb(TTW_P2P_EVENT_GROUP_REPORT, &group_data);
				p2p_handle->event_cb(TTW_P2P_EVENT_CONNECT_REPORT, &conn_info);
			}
		}
		return;
	}

	/** event: AP_STA_CONNECTED */
	if (strncmp(buf + start_off, AP_STA_CONNECTED, strlen(AP_STA_CONNECTED)) == 0) {
		int i;
		ttw_p2p_conn_info_t conn_info;
		memset(&conn_info, 0, sizeof(conn_info));

		dbg_printf("ttw_parse_event(): found event AP_STA_CONNECTED\n");
		if (group_if) {
			/** the same event is received both on the control and the group interface, so ignore one of them */
			dbg_printf("ttw_parse_event(): group interface -> ignore event\n");
			return;
		}

		if (ttw_parse_sta_connected(buf + start_off, &conn_info) < 0) {
			dbg_printf_err("ttw_parse_event(): error parsing event AP_STA_CONNECTED\n");
			return;
		}
		conn_info.status = TTW_CONNECTION_ESTABLISHED;
		conn_info.origin = TTW_ORIGIN_PEER;

		/* Try to determine the IP address of the connected peer (assigned by the local DHCP server) */
		for (i=0; i < 3; i++) {
			dbg_printf("ttw_parse_event(): try to get IP for the client %s\n", conn_info.client_hw_addr);
			/** wait for the DHCP server to assign the IP */
			sleep (6);
			/** check the DHCP leases of the server for the client address */
			ttw_get_dhcp_lease(conn_info.client_hw_addr, conn_info.client_ip_addr);

			/** if found, break */
			if (conn_info.client_ip_addr[0] != '\0')
				break;

			dbg_printf("ttw_parse_event(): no IP found for the client %s, retry...\n", conn_info.client_hw_addr);
		}

		dbg_printf("ttw_parse_event(): IP: %s\n", conn_info.client_ip_addr);

		/** notify the API user of the new connection */
		if(p2p_handle->event_cb)
			p2p_handle->event_cb(TTW_P2P_EVENT_CONNECT_REPORT, &conn_info);

		return;
	}

	/** event: AP_STA_DISCONNECTED */
	if (strncmp(buf + start_off, AP_STA_DISCONNECTED, strlen(AP_STA_DISCONNECTED)) == 0) {
		ttw_p2p_conn_info_t conn_info;
		memset(&conn_info, 0, sizeof(conn_info));

		dbg_printf("ttw_parse_event(): found event AP_STA_DISCONNECTED\n");

		if (group_if) {
			/** the same event is received both on the control and the group interface, so ignore one of them */
			dbg_printf("ttw_parse_event(): group interface -> ignore event\n");
			return;
		}

		if (ttw_parse_sta_disconnected(buf + start_off, &conn_info) < 0) {
			dbg_printf("ttw_parse_event(): error parsing event AP_STA_DISCONNECTED\n");
			return;
		}
		conn_info.status = TTW_CONNECTION_CLOSED;
		conn_info.origin = TTW_ORIGIN_PEER;

		conn_info.client_ip_addr[0] = '\0';

		/** notify the API user of the disconnection */
		if(p2p_handle->event_cb)
			p2p_handle->event_cb(TTW_P2P_EVENT_CONNECT_REPORT, &conn_info);

		return;
	}

	/**
	 * WPS events:
	 *    P2P_EVENT_PROV_DISC_SHOW_PIN
	 *    P2P_EVENT_PROV_DISC_ENTER_PIN
	 *    P2P_EVENT_PROV_DISC_PBC_REQ
	 */
	if ((strncmp(buf + start_off, P2P_EVENT_PROV_DISC_SHOW_PIN, strlen(P2P_EVENT_PROV_DISC_SHOW_PIN)) == 0) ||
		(strncmp(buf + start_off, P2P_EVENT_PROV_DISC_ENTER_PIN, strlen(P2P_EVENT_PROV_DISC_ENTER_PIN)) == 0) ||
		(strncmp(buf + start_off, P2P_EVENT_PROV_DISC_PBC_REQ, strlen(P2P_EVENT_PROV_DISC_PBC_REQ)) == 0)) {

		ttw_wps_info_t wps_info;
		memset(&wps_info, 0, sizeof(wps_info));

		dbg_printf("ttw_parse_event(): found event WPS provisioning req\n");

		if (ttw_parse_wps_prov_disc(buf + start_off, &wps_info) < 0) {
			dbg_printf_err("ttw_parse_event(): error parsing WPS event\n");
			return;
		}

		/** notify the API user of the event */
		if(p2p_handle->event_cb)
			p2p_handle->event_cb(TTW_P2P_EVENT_WPS_REPORT, &wps_info);

		return;
	}

	/** event: P2P-GO-NEG-REQUEST */
	if (strncmp(buf + start_off, P2P_EVENT_GO_NEG_REQUEST, strlen(P2P_EVENT_GO_NEG_REQUEST)) == 0) {
		ttw_wps_info_t wps_info;
		memset(&wps_info, 0, sizeof(wps_info));

		dbg_printf("ttw_parse_event(): found event P2P GO NEG\n");

		if (ttw_parse_p2p_go_neg_req(buf + start_off, &wps_info) < 0) {
			dbg_printf_err("ttw_parse_event(): error parsing P2P group owner negotiation request\n");
			return;
		}

		/** a group owner cannot start a new group negotiation */
		if (p2p_handle->is_group_owner) {
			dbg_printf_err("ttw_parse_event(): device is group owner -> ignoring negotiation request received from %s\n", wps_info.peer_address);
			return;
		}

		/** notify the API user of the event */
		if(p2p_handle->event_cb)
			p2p_handle->event_cb(TTW_P2P_EVENT_WPS_REPORT, &wps_info);

		return;
	}

	/** event: WPS_EVENT_SUCCESS */
	if (strncmp(buf + start_off, WPS_EVENT_SUCCESS, strlen(WPS_EVENT_SUCCESS)) == 0) {
		ttw_wps_info_t wps_info;
		memset(&wps_info, 0, sizeof(wps_info));

		dbg_printf("ttw_parse_event(): found event WPS_SUCCESS\n");
	}

	/** event: WPS_EVENT_SUCCESS */
	if (strncmp(buf + start_off, WPS_EVENT_SUCCESS, strlen(WPS_EVENT_SUCCESS)) == 0) {
		ttw_wps_info_t wps_info;
		memset(&wps_info, 0, sizeof(wps_info));

		dbg_printf("ttw_parse_event(): found event WPS_SUCCESS\n");

		wps_info.peer_address[0] = '\0';
		wps_info.wps_event = TTW_WPS_SUCCESS;

		/** notify the API user of the event */
		if(p2p_handle->event_cb)
			p2p_handle->event_cb(TTW_P2P_EVENT_WPS_REPORT, &wps_info);

		return;
	}

	/** event: WPS_EVENT_FAIL */
	if (strncmp(buf + start_off, WPS_EVENT_FAIL, strlen(WPS_EVENT_FAIL)) == 0) {
		ttw_wps_info_t wps_info;
		memset(&wps_info, 0, sizeof(wps_info));

		dbg_printf("ttw_parse_event(): found event WPS_FAIL\n");

		if (strstr(buf + start_off, "config_error=0") != NULL) {
			/** in "access point" PIN mode the authentication can still succeed after a WPS_FAIL event with config_error=0 -> ignore the event */
			dbg_printf("ttw_parse_event(): WPS_FAIL with config_error=0 -> ignore event\n");
			return;
		}

		wps_info.peer_address[0] = '\0';
		wps_info.wps_event = TTW_WPS_FAIL;

		/** notify the API user of the event */
		if(p2p_handle->event_cb)
			p2p_handle->event_cb(TTW_P2P_EVENT_WPS_REPORT, &wps_info);
		return;
	}

}

/**
 * Parser for WPS provisioning discovery
 *
 * examples:
 *   P2P-PROV-DISC-SHOW-PIN 02:40:61:c2:f3:b7 12345670 p2p_dev_addr=02:40:61:c2:f3:b7 pri_dev_type=1-0050F204-1 name='Test' config_methods=0x188 dev_capab=0x21 group_capab=0x0
 *   P2P-PROV-DISC-ENTER-PIN 02:40:61:c2:f3:b7 p2p_dev_addr=02:40:61:c2:f3:b7 pri_dev_type=1-0050F204-1 name='Test' config_methods=0x188 dev_capab=0x21 group_capab=0x0
 *   P2P-PROV-DISC-PBC-REQ 02:40:61:c2:f3:b7 p2p_dev_addr=02:40:61:c2:f3:b7 pri_dev_type=1-0050F204-1 name='Test' config_methods=0x188 dev_capab=0x21 group_capab=0x0
 */
#define TTW_P2P_PROV_DISC_SHOW_PIN_REGEXP "P2P-PROV-DISC-SHOW-PIN [0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2} [0-9]{8} p2p_dev_addr=([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}) "
#define TTW_P2P_PROV_DISC_ENTER_PIN_REGEXP "P2P-PROV-DISC-ENTER-PIN [0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2} p2p_dev_addr=([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}) "
#define TTW_P2P_PROV_DISC_PBC_REQ_REGEXP "P2P-PROV-DISC-PBC-REQ [0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2} p2p_dev_addr=([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}) "
static int ttw_parse_wps_prov_disc(char *buf, ttw_wps_info_t *wps_info)
{
	int res, start_off, finish_off;

	/** Parse SHOW_PIN event */
	res = ttw_regexp_match(buf, TTW_P2P_PROV_DISC_SHOW_PIN_REGEXP, &start_off, &finish_off);
	if (res == 0) {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (wps_info->peer_address, buf + start_off, len);
		wps_info->peer_address[len] = '\0';
		wps_info->wps_event = TTW_WPS_CREATE_PIN_REQ;
		return 0;
	}

	/** Parse P2P ENTER_PIN event */
	res = ttw_regexp_match(buf, TTW_P2P_PROV_DISC_ENTER_PIN_REGEXP, &start_off, &finish_off);
	if (res == 0) {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (wps_info->peer_address, buf + start_off, len);
		wps_info->peer_address[len] = '\0';
		wps_info->wps_event = TTW_WPS_ENTER_PIN_REQ;
		return 0;
	}

	/** Parse PBC (push-button) event */
	res = ttw_regexp_match(buf, TTW_P2P_PROV_DISC_PBC_REQ_REGEXP, &start_off, &finish_off);
	if (res == 0) {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (wps_info->peer_address, buf + start_off, len);
		wps_info->peer_address[len] = '\0';
		wps_info->wps_event = TTW_WPS_PBC_REQ;
		return 0;
	}

	return -1;
}

/**
 * Parser for P2P grow owner negotiation request
 *
 * examples:
 *   P2P-GO-NEG-REQUEST 52:01:bb:40:d7:04 dev_passwd_id=1
 *   P2P-GO-NEG-REQUEST 52:01:bb:40:d7:04 dev_passwd_id=5
 */
#define TTW_P2P_GO_NEG_REQ_REGEXP_1 "P2P-GO-NEG-REQUEST ([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}) "
#define TTW_P2P_GO_NEG_REQ_REGEXP_2 "dev_passwd_id=([[:digit:]])"
static int ttw_parse_p2p_go_neg_req(char *buf, ttw_wps_info_t *wps_info)
{
	int res, start_off, finish_off;

	/** Parse device address */
	res = ttw_regexp_match(buf, TTW_P2P_GO_NEG_REQ_REGEXP_1, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_go_neg_req(): no device address info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (wps_info->peer_address, buf + start_off, len);
		wps_info->peer_address[len] = '\0';
	}

	/** Parse WPS method */
	res = ttw_regexp_match(buf, TTW_P2P_GO_NEG_REQ_REGEXP_2, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_go_neg_req(): no WPS method info\n");
		return -1;
	} else {
		int wps_mode = 0;
		sscanf (buf + start_off, "%2x", &wps_mode);
		switch(wps_mode) {
			case 1:
				wps_info->wps_event = TTW_WPS_CREATE_PIN_REQ;
				break;
			case 4:
				wps_info->wps_event = TTW_WPS_PBC_REQ;
				break;
			case 5:
				wps_info->wps_event = TTW_WPS_ENTER_PIN_REQ;
				break;
			default:
				dbg_printf_err("ttw_parse_p2p_go_neg_req(): invalid WPS method info\n");
				return -1;
		}
	}

	return 0;
}

/**
 * Parser for P2P-GROUP-STARTED event
 *
 * examples:
 *   P2P-GROUP-STARTED p2p-wlan0-0 GO ssid="DIRECT-Gu" freq=2437 passphrase="wO9jhKAu" go_dev_addr=c4:04:15:7a:a1:36
 *   P2P-GROUP-STARTED p2p-wlan0-0 client ssid="DIRECT-jk" freq=2462 psk=022a134e668343de54d966cbaf3fff76c60202fd03b55d92b7cb658aa03404ff go_dev_addr=52:01:bb:40:d7:04
 */
#define TTW_P2P_GROUP_START_NAME_REGEXP "P2P-GROUP-STARTED\\s+(\\S+)\\s+"
#define TTW_P2P_GROUP_START_MODE_REGEXP "P2P-GROUP-STARTED\\s+\\S+\\s+(client|GO)"
#define TTW_P2P_GROUP_SSID_REGEXP "ssid=\"([^\"\\\\]*(\\\\.[^\"\\\\]*)*)\""
#define TTW_P2P_GROUP_PASSPHRASE_REGEXP "passphrase=\"([^\"\\\\]*(\\\\.[^\"\\\\]*)*)\""
#define TTW_P2P_GROUP_OWNER_ADDR_REGEXP "go_dev_addr=([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2})"
static int ttw_parse_p2p_group_started(char *buf, ttw_p2p_group_t *group_info)
{
	int res, start_off, finish_off;

	/** Parse group name */
	res = ttw_regexp_match(buf, TTW_P2P_GROUP_START_NAME_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_group_started(): no group name info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_GROUPNAME_LEN-1);
		strncpy (group_info->name, buf + start_off, P2P_GROUPNAME_LEN);
		group_info->name[len] = '\0';
	}

	/** Parse group mode */
	res = ttw_regexp_match(buf, TTW_P2P_GROUP_START_MODE_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_group_started(): no group mode info\n");
		return -1;
	} else {
		if (strncmp(buf + start_off, "GO", strlen("GO")) == 0) {
			/** The device is group owner */
			group_info->is_group_owner = 1;
		} else {
			/** The device is group client */
			group_info->is_group_owner = 0;
		}
	}

	/** Parse group SSID */
	res = ttw_regexp_match(buf, TTW_P2P_GROUP_SSID_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_group_started(): no group SSID info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_SSID_LEN-1);
		strncpy (group_info->ssid, buf + start_off, len);
		group_info->ssid[len] = '\0';
	}

	/** Parse group passphrase (only if GO) */
	if (group_info->is_group_owner) {
		res = ttw_regexp_match(buf, TTW_P2P_GROUP_PASSPHRASE_REGEXP, &start_off, &finish_off);
		if (res < 0) {
			dbg_printf_err("ttw_parse_p2p_group_started(): no group passphrase\n");
			return -1;
		} else {
			int len = MIN(finish_off-start_off, P2P_PASSPHRASE_LEN-1);
			strncpy(group_info->passphrase, buf + start_off, P2P_PASSPHRASE_LEN);
			group_info->passphrase[len] = '\0';
		}
	} else {
		group_info->passphrase[0] = '\0';
	}

	/** Parse group owner address */
	res = ttw_regexp_match(buf, TTW_P2P_GROUP_OWNER_ADDR_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_group_started(): no group owner address info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (group_info->owner_addr, buf + start_off, P2P_ADDR_LEN);
		group_info->owner_addr[len] = '\0';
	}

	return 0;
}

/**
 * Parser for P2P-DEVICE-FOUND event
 *
 * example:
 *   P2P-DEVICE-FOUND 52:01:bb:40:d7:04 p2p_dev_addr=52:01:bb:40:d7:04 pri_dev_type=10-0050F204-5 name='Android_8061' config_methods=0x188 dev_capab=0x25 group_capab=0x0
 */
#define TTW_P2P_DEV_ADDR_REGEXP "p2p_dev_addr=([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2})"
#define TTW_P2P_DEV_TYPE_REGEXP "pri_dev_type=([0-9A-Fa-f]{1,2}-[0-9A-Fa-f]{8}-[0-9A-Fa-f]{1})"
#define TTW_P2P_DEV_NAME_REGEXP "name=\'([^\']*)\'"
#define TTW_P2P_DEV_GROUP_CAP_REGEXP "group_capab=0x([0-9A-Fa-f]{1,2})"
static int ttw_parse_p2p_device_found(char *buf, ttw_p2p_device_t *device_info)
{
	int res, start_off, finish_off;

	/** Parse device address */
	res = ttw_regexp_match(buf, TTW_P2P_DEV_ADDR_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_device_found(): no device address info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (device_info->p2p_addr, buf + start_off, len);
		device_info->p2p_addr[len] = '\0';
	}

	/** Parse device name */
	res = ttw_regexp_match(buf, TTW_P2P_DEV_NAME_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_device_found(): no device name info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_DEVNAME_LEN-1);
		strncpy (device_info->p2p_name, buf + start_off, len);
		device_info->p2p_name[len] = '\0';
	}

	/** Parse device type */
	res = ttw_regexp_match(buf, TTW_P2P_DEV_TYPE_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_device_found(): no device type info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_DEVTYPE_LEN-1);
		strncpy (device_info->dev_type, buf + start_off, len);
		device_info->dev_type[len] = '\0';
	}

	/** Parse group capabilities */
	res = ttw_regexp_match(buf, TTW_P2P_DEV_GROUP_CAP_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_device_found(): no device group capabilities info\n");
		return -1;
	} else {
		int group_cap = 0;
		sscanf (buf + start_off, "%2x", &group_cap);
		/** The group owner flag is the 1st bit of the capabilities field */
		device_info->is_group_owner = group_cap & 0x1;
	}
	return 0;
}

/**
 * Parser for P2P-GO-NEG-SUCCESS event
 *
 * example:
 *    P2P-GO-NEG-SUCCESS role=client freq=2462 ht40=1 peer_dev=52:01:bb:40:d7:04 peer_iface=52:01:bb:40:57:04 wps_method=Display
 */
#define TTW_P2P_PEER_IF_ADDR_REGEXP "peer_iface=([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2})"
static int ttw_parse_p2p_go_neg_success(char *buf, char *peer_if)
{
	int res, start_off, finish_off;

	/* Parse peer interface address */
	res = ttw_regexp_match(buf, TTW_P2P_PEER_IF_ADDR_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_go_neg_success(): no peer interface address info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (peer_if, buf + start_off, len);
		peer_if[len] = '\0';
	}
	return 0;
}

/**
 * Parser for AP-STA-CONNECTED event
 *
 * examples;
 *    AP-STA-CONNECTED 50:01:bb:40:d7:04
 *    AP-STA-CONNECTED 50:01:bb:40:d7:04 p2p_dev_addr=50:01:bb:40:d7:04
 */
#define TTW_STA_CONNECTED_ADDR_REGEXP "AP-STA-CONNECTED\\s+([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2})"
static int ttw_parse_sta_connected(char *buf, ttw_p2p_conn_info_t *conn_info)
{
	int res, start_off, finish_off;

	/** Parse device address */
	res = ttw_regexp_match(buf, TTW_STA_CONNECTED_ADDR_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_sta_connected(): no device address info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (conn_info->client_hw_addr, buf + start_off, len);
		conn_info->client_hw_addr[len] = '\0';
	}

	/** Check for P2P info (if present) */
	if (strstr(buf, "p2p_dev_addr") == NULL)
		conn_info->is_legacy = 1;
	else
		conn_info->is_legacy = 0;

	return 0;
}

/**
 * Parser for AP-STA-DISCONNECTED event
 *
 * examples;
 *    AP-STA-DISCONNECTED 50:01:bb:40:d7:04
 *    AP-STA-DISCONNECTED 50:01:bb:40:d7:04 p2p_dev_addr=50:01:bb:40:d7:04
 */
#define TTW_STA_DISCONNECTED_ADDR_REGEXP "AP-STA-DISCONNECTED\\s+([0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2})"
static int ttw_parse_sta_disconnected(char *buf, ttw_p2p_conn_info_t *conn_info)
{
	int res, start_off, finish_off;

	/** Parse device address */
	res = ttw_regexp_match(buf, TTW_STA_DISCONNECTED_ADDR_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_sta_disconnected(): no device address info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_ADDR_LEN-1);
		strncpy (conn_info->client_hw_addr, buf + start_off, len);
		conn_info->client_hw_addr[len] = '\0';
	}

	/* Check for P2P info (if present) */
	if (strstr(buf, "p2p_dev_addr") == NULL)
		conn_info->is_legacy = 1;
	else
		conn_info->is_legacy = 0;

	return 0;
}

/**
 * Parser for P2P-GROUP-REMOVED event
 *
 * examples:
 *    P2P-GROUP-REMOVED p2p-wlan0-1 GO reason=REQUESTED
 *    P2P-GROUP-REMOVED p2p-wlan0-2 client reason=IDLE
 */
#define TTW_P2P_GROUP_REM_NAME_REGEXP "P2P-GROUP-REMOVED\\s+(\\S+)\\s+"
#define TTW_P2P_GROUP_REM_MODE_REGEXP "P2P-GROUP-REMOVED\\s+\\S+\\s+(client|GO)"
static int ttw_parse_p2p_group_removed(char *buf, ttw_p2p_group_t *group_info)
{
	int res, start_off, finish_off;

	/** Parse group name */
	res = ttw_regexp_match(buf, TTW_P2P_GROUP_REM_NAME_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_group_removed(): no group name info\n");
		return -1;
	} else {
		int len = MIN(finish_off-start_off, P2P_GROUPNAME_LEN-1);
		strncpy (group_info->name, buf + start_off, P2P_GROUPNAME_LEN);
		group_info->name[len] = '\0';
	}

	/** Parse group mode (GO/client) */
	res = ttw_regexp_match(buf, TTW_P2P_GROUP_REM_MODE_REGEXP, &start_off, &finish_off);
	if (res < 0) {
		dbg_printf_err("ttw_parse_p2p_group_removed(): no group mode info\n");
		return -1;
	} else {
		if (strncmp(buf + start_off, "GO", strlen("GO")) == 0)
			group_info->is_group_owner = 1; /**< The device is a group owner */
		else
			group_info->is_group_owner = 0; /**< The device is a client */
	}

	return 0;
}

/**
 * Start DHCP server
 */
static void ttw_start_dhcp_server(ttw_p2p_handle_t *p2p_handle, const char *ifname)
{
	int fd, res;
	char cmd_buf[100];
	struct ifreq ifr;
	struct sockaddr_in sin;
	uint32_t pool_addr;
	struct in_addr tmp_addr;

	/** stop any previous running instance */
	res = system ("systemctl stop wifi-dhcp");
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_server(): error stopping DHCP server\n");
		return;
	}

	/** create a socket for the network operations */
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
		dbg_printf_err("ttw_start_dhcp_server(): error creating socket\n");
		return;
	}

	/** set the IP address of the interface */
	dbg_printf("ttw_start_dhcp_server(): setting IP to \"%s\"\n", inet_ntoa(p2p_handle->dhcp_srv_ip));
	memset(&ifr, 0, sizeof(struct ifreq));
	memset(&sin, 0, sizeof(struct sockaddr_in));
	memcpy(&sin.sin_addr, &p2p_handle->dhcp_srv_ip, sizeof(struct in_addr));
	memcpy(&ifr.ifr_addr, &sin, sizeof(struct sockaddr_in));
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, ifname, IFNAMSIZ-1);
	res = ioctl(fd, SIOCSIFADDR, &ifr);
	if (res < 0) {
		dbg_printf_err("ttw_start_dhcp_server(): error ioctl for setting IP address on network interface %s\n", ifname);
		close(fd);
		return;
	}

	/** set the netmask address of the interface */
	dbg_printf("ttw_start_dhcp_server(): setting netmask to \"%s\"\n", inet_ntoa(p2p_handle->dhcp_srv_mask));
	memset(&ifr, 0, sizeof(struct ifreq));
	memset(&sin, 0, sizeof(struct sockaddr_in));
	memcpy(&sin.sin_addr, &p2p_handle->dhcp_srv_mask, sizeof(struct in_addr));
	memcpy(&ifr.ifr_addr, &sin, sizeof(struct sockaddr_in));
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, ifname, IFNAMSIZ-1);
	res = ioctl(fd, SIOCSIFNETMASK, &ifr);
	if (res < 0) {
		dbg_printf_err("ttw_start_dhcp_server(): error ioctl for setting netmask on network interface %s\n", ifname);
		close(fd);
		return;
	}

	/** get the interface flags */
	res = ioctl(fd, SIOCGIFFLAGS, &ifr);
	if (res < 0) {
		dbg_printf_err("ttw_start_dhcp_server(): error ioctl for reading flags on network interface %s\n", ifname);
		close(fd);
		return;
	}

	/** set the interface UP */
	ifr.ifr_flags |= IFF_UP;
	res = ioctl(fd, SIOCSIFFLAGS, &ifr);
	if (res < 0) {
		dbg_printf_err("ttw_start_dhcp_server(): error ioctl for setting flags on network interface %s\n", ifname);
		close(fd);
		return;
	}

	/** insert the interface name in the server configuration */
	snprintf(cmd_buf, 100, "sed -i 's/\\(^\\binterface\\b\\).*/\\1 %s/' %s", ifname, TTW_DHCP_SERVER_CONFIG);
	dbg_printf("ttw_start_dhcp_server(): executing \"%s\"\n", cmd_buf);
	res = system(cmd_buf);
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_server(): error editing DHCP server config\n");
		close(fd);
		return;
	}

	/** insert the lease file name in the server configuration */
	snprintf(cmd_buf, 100, "echo \"\nlease_file %s\n\" >> %s", TTW_DHCP_SERVER_LEASE_FILE, TTW_DHCP_SERVER_CONFIG);
	dbg_printf("ttw_start_dhcp_server(): executing \"%s\"\n", cmd_buf);
	res = system(cmd_buf);
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_server(): error editing DHCP server config\n");
		close(fd);
		return;
	}

	/** insert the subnet mask in the server configuration */
	snprintf(cmd_buf, 100, "sed -i 's/\\(^\\boption\\tsubnet\\t\\).*/\\1%s/' %s", inet_ntoa(p2p_handle->dhcp_srv_mask), TTW_DHCP_SERVER_CONFIG);
	dbg_printf("ttw_start_dhcp_server(): executing \"%s\"\n", cmd_buf);
	res = system(cmd_buf);
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_server(): error editing DHCP server config\n");
		close(fd);
		return;
	}

	/** calculate the start of the DHCP pool as subnet base + TTW_DHCP_POOL_START and update it in the server configuration */
	pool_addr = (ntohl(p2p_handle->dhcp_srv_ip.s_addr) & ntohl(p2p_handle->dhcp_srv_mask.s_addr)) + TTW_DHCP_POOL_START;
	tmp_addr.s_addr = htonl(pool_addr);
	dbg_printf("ttw_start_dhcp_server(): DHCP IP pool start \"%s\"\n", inet_ntoa(tmp_addr));
	snprintf(cmd_buf, 100, "sed -i 's/\\(^\\bstart\\s\\+\\).*/\\1%s/' %s",  inet_ntoa(tmp_addr), TTW_DHCP_SERVER_CONFIG);
	dbg_printf("ttw_start_dhcp_server(): executing \"%s\"\n", cmd_buf);
	res = system(cmd_buf);
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_server(): error editing DHCP server config\n");
		close(fd);
		return;
	}

	/** calculate the end of the DHCP pool as subnet base + TTW_DHCP_POOL_END and update it in the server configuration */
	pool_addr = (ntohl(p2p_handle->dhcp_srv_ip.s_addr) & ntohl(p2p_handle->dhcp_srv_mask.s_addr)) + TTW_DHCP_POOL_END;
	tmp_addr.s_addr = htonl(pool_addr);
	dbg_printf("ttw_start_dhcp_server(): DHCP IP pool end \"%s\"\n", inet_ntoa(tmp_addr));
	snprintf(cmd_buf, 100, "sed -i 's/\\(^\\bend\\s\\+\\).*/\\1%s/' %s",  inet_ntoa(tmp_addr), TTW_DHCP_SERVER_CONFIG);
	dbg_printf("ttw_start_dhcp_server(): executing \"%s\"\n", cmd_buf);
	res = system(cmd_buf);
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_server(): error editing DHCP server config\n");
		close(fd);
		return;
	}

	/** start the DHCP server */
	res = system("systemctl start wifi-dhcp");
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_server(): error starting DHCP server\n");
		close(fd);
		return;
	}

	/** close the socket */
	close(fd);
}

/**
 * Stop DHCP server
 */
static void ttw_stop_dhcp_server(void)
{
	int res;

	res = system ("systemctl stop wifi-dhcp");
	if (res == -1) {
		dbg_printf_err("ttw_stop_dhcp_server(): error stopping DHCP server\n");
		return;
	}
}

/**
 * Start DHCP client
 */
static void ttw_start_dhcp_client(const char *ifname)
{
	int res;
	char cmd_buf[100];

	/** stop any previous running instance */
	res = system("killall -q udhcpc");
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_client(): error stopping DHCP client\n");
		return;
	}

	/** insert the interface name as command line argument */
	snprintf(cmd_buf, 100, "udhcpc -n -i %s >/dev/null 2>&1", ifname);
	dbg_printf("ttw_start_dhcp_client(): executing \"%s\"\n", cmd_buf);

	/** start the DHCP client */
	res = system(cmd_buf);
	if (res == -1) {
		dbg_printf_err("ttw_start_dhcp_client(): error starting DHCP client\n");
		return;
	}
}

/**
 * Stop DHCP client
 */
static void ttw_stop_dhcp_client(void)
{
	int res;

	res = system("killall -q udhcpc");
	if (res == -1) {
		dbg_printf_err("ttw_stop_dhcp_client(): error stopping DHCP client\n");
		return;
	}
}

/**
 * Generic match function for regular expressions
 *
 * @param char *buf : pointer to the buffer containing the input text
 * @param char *regexp_string : regular expression string to be used
 * @param int *start_off : offset of the beginning of the matched string
 * @param int *finish_off : offset of the end of the matched string
 *
 */
static int ttw_regexp_match(const char *buf, const char *regexp_string, int *start_off, int *finish_off)
{
	regex_t regex;
	regmatch_t match_arr[2];
	int res;

	/** compile the regular expression */
	res = regcomp(&regex, regexp_string, REG_EXTENDED|REG_NEWLINE);
	if (res != 0) {
		return -1;
	}

	/** execute the match search */
	res = regexec(&regex, buf, 2, match_arr, 0);
	if (res || (match_arr[1].rm_so == -1)) {
		return -1;
	}

	/** get the offset boundaries of the matched string */
	*start_off = match_arr[1].rm_so;
	*finish_off = match_arr[1].rm_eo;

	/** free the regex instance */
	regfree (&regex);
	return 0;
}

/**
 * Attach the group interface to the P2P framework to get events from wpa_supplicant
 */
static int ttw_attach_grp_if(ttw_p2p_handle_t *p2p_handle, const char *if_name)
{
	char sock_name[1000];
	int ret;

	/** get the path of the Unix socket of the interface */
	snprintf(sock_name, sizeof(sock_name), "%s/%s", WPA_CTRL_IF_PATH, if_name);

	/** open the main group interface */
	p2p_handle->grp_ctrl_if = wpa_ctrl_open(sock_name);
	if (p2p_handle->grp_ctrl_if == NULL) {
		dbg_printf_err("ttw_attach_grp_if(): fail to open %s\n", sock_name);
		return -1;
	}

	/** open the monitor group interface */
	p2p_handle->grp_mon_if = wpa_ctrl_open(sock_name);
	if (p2p_handle->grp_mon_if == NULL) {
		dbg_printf_err("ttw_attach_grp_if(): fail to open %s\n", sock_name);
		return -1;
	}

	/** attach to the monitor interface to get events from it */
	ret = wpa_ctrl_attach(p2p_handle->grp_mon_if);
	if (ret == -2) {
		dbg_printf_err("ttw_attach_grp_if(): timeout attaching to %s\n", sock_name);
		return -1;
	} else if (ret < 0) {
		dbg_printf_err("ttw_attach_grp_if(): fail to attach monitor interface to %s\n", sock_name);
		return -1;
	}
	return 0;
}

/**
 * Get the IP address from the MAC using the ARP cache
 *
 * @param char *mac_addr : MAC address of the interface
 * @param char *ip : IP address of the interface
 */
static void ttw_mac2ip_arp(const char *mac_addr, char *ip)
{
	FILE *fp;
	char cmd[100];
	size_t len;

	if (mac_addr[0] == '\0') {
		dbg_printf_err("ttw_mac2ip_arp(): empty MAC address\n");
		ip[0] = '\0';
		return;
	}
	/** grep the MAC address on the ARP cache and extracts the IP*/
	snprintf(cmd, 100, "grep %s /proc/net/arp | cut -f1 -d' '", mac_addr);
	ip[0] = '\0';

	fp = popen(cmd, "r");
	if (fp == NULL)
		return;

	if (fgets(ip, P2P_IPADDR_LEN, fp) == NULL) {
		ip[0] = '\0';
		pclose(fp);
		return;
	}

	/** remove the newline char from the IP address string */
	len = strlen(ip) - 1;
	if (ip[len] == '\n')
		ip[len] = '\0';

	pclose(fp);
}

/**
 * Get the assigned IP address from MAC using the DHCP lease file
 *
 * @param char *mac_addr : MAC address of the interface
 * @param char *ip : IP address of the interface
 */
static void ttw_get_dhcp_lease(const char *mac_addr, char *ip)
{
	FILE *fp;
	int res;
	char cmd[100];
	size_t len;

	ip[0] = '\0';

	/** send a SIGUSR1 to the DHCP server to force the dump of the leases into a file */
	res = system("killall -q -SIGUSR1 udhcpd");
	if (res == -1) {
		dbg_printf_err("ttw_get_dhcp_lease(): error sending signal to DHCP server\n");
		return;
	}

	/** wait a while for the lease file to be created and/or updated */
	sleep(2);

	/** dump the leases and parse the output */
	snprintf(cmd, 100, "dumpleases -f %s | grep %s | cut -f2 -d' '", TTW_DHCP_SERVER_LEASE_FILE, mac_addr);
	fp = popen(cmd, "r");
	if (fp == NULL) {
		dbg_printf_err("ttw_get_dhcp_lease(): error getting the lease\n");
		return;
	}

	if (fgets(ip, P2P_IPADDR_LEN, fp) == NULL) {
		dbg_printf_err("ttw_get_dhcp_lease(): error parsing the lease\n");
		pclose(fp);
		return;
	}

	/** remove the newline char from the IP address string */
	len = strlen(ip) - 1;
	if (ip[len] == '\n')
		ip[len] = '\0';

	pclose(fp);
}

/**
 * Get IP and MAC address for an interface
 *
 * @param char *ifname : name of the interface
 * @param char *ip : IP address of the interface
 * @param char *mac : MAC address of the interface
 */
static void ttw_ifname2data(const char *ifname, char *ip, char *mac)
{
	int fd, res;
	unsigned char *mac_data;
	char *ip_data;
	struct ifreq ifr;

	/** create a socket for the network operations */
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
		dbg_printf_err("ttw_ifname2data(): error creating socket\n");
		ip[0] = '\0';
		return;
	}

	memset(&ifr, 0, sizeof(struct ifreq));

	/** set the interface name */
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, ifname, IFNAMSIZ-1);

	/** get the MAC address */
	res = ioctl(fd, SIOCGIFHWADDR, &ifr);

	if (res < 0) {
		dbg_printf_err("ttw_ifname2data(): error ioctl for MAC\n");
		mac[0] = '\0';
	 	close(fd);
		return;
	}

	/** convert the MAC address in string format */
	mac_data = (unsigned char *) ifr.ifr_hwaddr.sa_data;
	snprintf(mac, P2P_ADDR_LEN, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n" , mac_data[0], mac_data[1], mac_data[2], mac_data[3], mac_data[4], mac_data[5]);

	/** get the IP address */
	res = ioctl(fd, SIOCGIFADDR, &ifr);
	if (res < 0) {
		dbg_printf_err("ttw_ifname2data(): error ioctl for IP\n");
		ip[0] = '\0';
	 	close(fd);
		return;
	}

	/** convert the IP address in string format */
	ip_data = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
	strncpy(ip, ip_data, P2P_IPADDR_LEN);

	/** close the socket */
	close(fd);
}

