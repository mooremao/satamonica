/**
 * ptscan - Force the scan of the partition table of a block device
 * Copyright (c) 2015 TomTom BV <http://www.tomtom.com/>
 */

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <errno.h>

int main(int argc, char *argv[])
{
	int fd, ret;

	if (argc < 2) {
		printf("\nUsage:\n\t%s <block device>\n\n", argv[0]);
		return 1;
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Error opening: %s - %s\n", argv[1], strerror(errno));
		return -errno;
	}

	ret = ioctl(fd, BLKRRPART, NULL);
	if (ret) {
		fprintf(stderr, "Error ioctl: %s\n", strerror(errno));
		ret = -errno;
	}

	close(fd);
	return ret;
}
