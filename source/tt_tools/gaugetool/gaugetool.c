#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "linux/i2c-dev.h"
#include "i2cbusses.h"
#include "gaugetool.h"

/*
    Defines
*/
#define MCU_I2C_BUS		3
#define MCU_I2C_ADDRESS		0x44

#define GAUGE_I2C_BUS		3
#define GAUGE_I2C_ADDRESS	0x55

#define MAX_INIT_RETRY		10

#define GAUGE_ACCESS_TIME	1000		/* 1 ms */
#define GAUGE_UPDATE_CFG_TIME	500000		/* 500 ms */

#define MCU_NAME		"ATMEL MCU"
#define FG_NAME			"BQ27421"

#define SWAP_FG_BLOCKDATA_BYTES(w)	((((w) & 0xFF)<<8) | ((w)>>8))

#define MAX_CHECK_STATE_RETRIES 4

int gauge_check_battery_type(void)
{
	int mcu_file, type = BATT_TYPE_INVALID;
	char mcu_filename[20];

	mcu_file = open_i2c_dev(MCU_I2C_BUS, mcu_filename, sizeof(mcu_filename), 1);
	if (mcu_file < 0 || set_slave_addr(mcu_file, MCU_I2C_ADDRESS, 1)) {
		printf("%s busy\n", MCU_NAME);
	} else {
		type = i2c_smbus_read_byte_data(mcu_file, REG_BATTERY_TYPE);
		close(mcu_file);
	}

	return type;
}

/**
 * Checks if FG has completed it initialization. Unsealing does not work meanwhile.
 */
int gauge_check_state(int file) {
	int ret;
	int value;

	ret = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_STATUS);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}

	/* Check status */
	value = i2c_smbus_read_word_data(file, REG_CONTROL);
	if (value < 0) {
		printf("[%s (%d)] i2c_smbus_read_word_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}

	if ((value & CNTL_STATUS_INITCOMP)) {
		return 0;
	} else {
		printf("[%s (%d)] FG is still initializing\n", __FUNCTION__, __LINE__);
		return 1;
	}
}

int gauge_switch_mode(int file, int mode)
{
	int ret;
	int value;

	if (mode == MODE_UNSEALED) {
		ret = i2c_smbus_write_word_data(file, REG_CONTROL, UNSEALED_KEY);
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		ret = i2c_smbus_write_word_data(file, REG_CONTROL, UNSEALED_KEY);
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		ret = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_STATUS);
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		/* Check status */
		value = i2c_smbus_read_word_data(file, REG_CONTROL);
		if (value < 0) {
			printf("[%s (%d)] i2c_smbus_read_word_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		if ((value & CNTL_STATUS_SS) == 0) {
			return 0;
		} else {
			printf("[%s (%d)] unsealing failed. Status: %04x\n", __FUNCTION__, __LINE__, value);
			return -1;
		}
	} else {
		ret = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_SEALED);
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		/* Check status */
		ret = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_STATUS);
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		value = i2c_smbus_read_word_data(file, REG_CONTROL);
		if (value < 0) {
			printf("[%s (%d)] i2c_smbus_read_word_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		if (value & CNTL_STATUS_SS) {
			return 0;
		} else {
			printf("[%s (%d)] sealing failed. Status: %04x\n", __FUNCTION__, __LINE__, value);
			return -1;
		}
	}

	return 0;
}

int gauge_check_version(int file)
{
	int ret;
	int checksum;

	/* Setup command */
	ret = i2c_smbus_write_byte_data(file, REG_BD_CONTROL, 0x00);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed: %d\n", __FUNCTION__, __LINE__, ret);
		return -1;
	}

	/* Access STATE subclass */
	ret = i2c_smbus_write_word_data(file, REG_DATA_CLASS, SUBCLASS_ID_108);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_word_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}
	usleep(GAUGE_ACCESS_TIME);

	checksum = i2c_smbus_read_word_data(file, REG_BD_CHECKSUM);
	if (checksum == DEFAULT_SUBCLASS_108_SUM) {
		printf("Programming ...\n");
		return 0;
	} else if (checksum == VERSION_01_SUBCLASS_108_SUM || checksum == VERSION_02_SUBCLASS_108_SUM) {
		printf("Updating ...\n");
		return 0;
	} else if (checksum != VERSION_03_SUBCLASS_108_SUM) {
		printf("Unknow version ...\n");
		return 0;
	} else {
		return 1;
	}
}

int gauge_check_bq27421(int file, int force)
{
	int result, value;

	/* Check device type and FW version */
	result = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_DEVICE_TYPE);
	if (result < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed: %d\n", __FUNCTION__, __LINE__, result);
		return -1;
	}

	value = i2c_smbus_read_word_data(file, REG_CONTROL);
	if (value < 0) {
		printf("[%s (%d)] i2c_smbus_read_word_data failed: %d\n", __FUNCTION__, __LINE__, result);
		return -1;
	}

	if (value == BQ27421_DEVICE_TYPE) {
		result = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_FW_VERSION);
		if (result < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed: %d\n", __FUNCTION__, __LINE__, result);
			return -1;
		}

		value = i2c_smbus_read_word_data(file, REG_CONTROL);
		if (value < 0) {
			printf("[%s (%d)] i2c_smbus_read_word_data failed: %d\n", __FUNCTION__, __LINE__, result);
			return -1;
		}

		if (value < BQ27421_FW_VERSION)
			return ERROR_UNKNOWN_GAUGE;
	}

	if (gauge_switch_mode(file, MODE_UNSEALED) != 0) {
		printf("%s: Unsealing failed. Failed to check version.\n", __FUNCTION__);
		return -1;
	}

	if (force != 0)
		return 0;

	/* Version check */
	result = gauge_check_version(file);
	if (result < 0)
		return -1;

	if (result == 1) {
		result = gauge_switch_mode(file, MODE_SEALED);
		if(result < 0) {
			printf("%s: Sealing failed. Ignored.\n", __FUNCTION__);
		}
		return ERROR_PROGRAMMED;
	}

	return 0;
}

int gauge_enter_cfgupdate(int file)
{
	int ret, value;
	int retry = 0;

	/* Enter Config update */
	do {
		ret = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_SET_CFGUPDATE);
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}

		value = i2c_smbus_read_byte_data(file, REG_FLAGS);
		if (value < 0) {
			printf("[%s (%d)] i2c_smbus_read_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}
		retry++;

		usleep(GAUGE_ACCESS_TIME);
	} while (!(value & CFGUPMODE) && (retry < MAX_INIT_RETRY));

	if (value & CFGUPMODE)
		return 0;
	else
		return 1;
}

int gauge_exit_cfgupdate(int file, int reset)
{
	int ret;

	if (reset != 0) {
		ret = i2c_smbus_write_word_data(file, REG_CONTROL, CNTL_SOFT_RESET);
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}
		usleep(GAUGE_ACCESS_TIME);

		do {
			usleep(GAUGE_UPDATE_CFG_TIME);
			ret = i2c_smbus_read_byte_data(file, REG_FLAGS);
		} while (ret < 0 || (ret & CFGUPMODE));
	}

	ret = gauge_switch_mode(file, MODE_SEALED);
	if (ret < 0) {
		printf("%s: Failed to seal. Ignored.\n", __FUNCTION__);
	}

	return 0;
}

int gauge_check_default_opconfig(int file)
{
	int ret, value;

	if (gauge_switch_mode(file, MODE_UNSEALED) != 0) {
		printf("%s: Failed to unseal. Failed to check opconfig\n", __FUNCTION__);
		goto err;
	}

	/* Setup command */
	ret = i2c_smbus_write_byte_data(file, REG_BD_CONTROL, 0x00);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		goto err;
	}

	/* Data Class */
	ret = i2c_smbus_write_byte_data(file, REG_DATA_CLASS, SUBCLASS_ID_REGISTERS);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		goto err;
	}

	/* Block Offset */
	ret = i2c_smbus_write_byte_data(file, REG_DATA_BLOCK, OFFSET_OPCONFIG);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		goto err;
	}

	usleep(GAUGE_ACCESS_TIME);
	value = i2c_smbus_read_word_data(file, REG_BLOCK_DATA);
	if (value < 0) {
		printf("[%s (%d)] i2c_smbus_read_word_data failed\n", __FUNCTION__, __LINE__);
		goto err;
	}

	if (value == SWAP_FG_BLOCKDATA_BYTES(DEFAULT_OPCONFIG))
		return 0;
	else
		return 1;

err:
	ret = gauge_switch_mode(file, MODE_SEALED);
	if (ret < 0) {
		printf("%s: Failed to seal. Ignored.\n", __FUNCTION__);
	}
	return -1;
}

int gauge_program_data_memory(int file, unsigned char subclass, unsigned offset, unsigned char *data, unsigned char data_size)
{
	int ret, offset_val;
	unsigned char i, csum, temp;

	/* Setup command */
	ret = i2c_smbus_write_byte_data(file, REG_BD_CONTROL, 0x00);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}

	/* Data Class */
	ret = i2c_smbus_write_byte_data(file, REG_DATA_CLASS, subclass);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}

	/* Block Offset */
	offset_val = (offset > DATA_BLOCK_SIZE) ? 1 : 0, 1;
	ret = i2c_smbus_write_byte_data(file, REG_DATA_BLOCK, offset_val);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}

	offset %= DATA_BLOCK_SIZE;
	/* Write Data Block (1-byte each time, if not fails!) */
	for (i = 0; i < data_size; i++)
	{
		usleep(GAUGE_ACCESS_TIME);
		ret = i2c_smbus_write_byte_data(file, (REG_BLOCK_DATA  + offset + i), *(data + i));
		if (ret < 0) {
			printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
			return -1;
		}
	}

	/* Checksum */
	csum = 0;
	for (i = REG_BLOCK_DATA; i <= REG_BLOCK_DATA_MAX; i++)
	{
		temp = i2c_smbus_read_byte_data(file, i);
		csum += temp;
	}

	csum = 255 - csum;
	ret = i2c_smbus_write_byte_data(file, REG_BD_CHECKSUM, csum);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}

	return 0;
}

int gauge_cfg_update(int file, struct gauge_cfg_info *cfg)
{
	int ret;
	int value;

	/* Access the subclass */
	ret = i2c_smbus_write_byte_data(file, REG_DATA_CLASS, cfg->subclass_id);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}

	ret = i2c_smbus_write_byte_data(file, REG_DATA_BLOCK, cfg->block_id);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}
	usleep(GAUGE_ACCESS_TIME);

	/* Write block data */
	ret = i2c_smbus_write_i2c_block_data(file, REG_BLOCK_DATA, sizeof(cfg->data), cfg->data);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_i2c_block_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}
	usleep(GAUGE_ACCESS_TIME);

	/* Write checksum */
	ret = i2c_smbus_write_byte_data(file, REG_BD_CHECKSUM, cfg->checksum);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}
	usleep(GAUGE_ACCESS_TIME);

	/* Check checksum */
	ret = i2c_smbus_write_byte_data(file, REG_DATA_CLASS, cfg->subclass_id);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}
	ret = i2c_smbus_write_byte_data(file, REG_DATA_BLOCK, cfg->block_id);
	if (ret < 0) {
		printf("[%s (%d)] i2c_smbus_write_byte_data failed\n", __FUNCTION__, __LINE__);
		return -1;
	}
	usleep(GAUGE_ACCESS_TIME);

	value = i2c_smbus_read_byte_data(file, REG_BD_CHECKSUM);
	if (value == cfg->checksum)
		return 0;
	else
		return 1;
}


static int gauge_update_opconfig(int file)
{
	int result;
	unsigned short opcfg = SWAP_FG_BLOCKDATA_BYTES(INIT_OPCONFIG);

	result = gauge_enter_cfgupdate(file);
	if (result != 0) {
		printf("%s device busy!\n", FG_NAME);
		gauge_switch_mode(file, MODE_SEALED);
		return -1;
	}

	result = gauge_program_data_memory(file, SUBCLASS_ID_REGISTERS, OFFSET_OPCONFIG, (unsigned char *)&opcfg, sizeof(opcfg));

	if (result == 0)
		gauge_exit_cfgupdate(file, 1);
	else
		gauge_exit_cfgupdate(file, 0);

	return result;
}

static void usage(char *self)
{
	printf("usage: %s\n", self);
	printf("Arguments:\n");
	printf("\t-f        force update FG\n");
	printf("\t-v        display version number\n");
	printf("\t-h        give this help\n");
}

int main(int argc, char *argv[])
{
	int result, i2cbus, address, file;
	int battery_type = BATT_TYPE_INVALID;
	int version = 0, force_write = 0, help = 0;
	char filename[20];
	int retries = 0;

	int c = 0;

	while ((c = getopt (argc, argv, "vVfh")) != -1)
	{
		switch (c)
		{
			case 'v':
			case 'V':
				version = 1;
				break;
			case 'f':
				force_write = 1;
				break;
			case 'h':
				help = 1;
				break;
			default:
				usage(argv[0]);
				exit(1);
		}
	}

	if (version) {
		fprintf(stderr, "gaugetool %s\n", VERSION_STRING);
		exit(0);
	}

	if (help) {
		usage(argv[0]);
		exit(0);
	}

	printf("Version: gaugetool %s\n", VERSION_STRING);

	/* Read battery type from MCU */
	battery_type = gauge_check_battery_type();
	if (battery_type == BATT_TYPE_INVALID) {
		printf("Invalid battery type! Config as default 2000mAH\n");
		battery_type = BATT_TYPE_2000MAH;
	}

	i2cbus = GAUGE_I2C_BUS;
	address = GAUGE_I2C_ADDRESS;
	file = open_i2c_dev(i2cbus, filename, sizeof(filename), 1);
	if (file < 0 || set_slave_addr(file, address, 1)) {
		printf("%s not found!\n", FG_NAME);
		exit(EXIT_FAILURE);
	}

	/* Check FG state. It might not be read for us yet. */
	do {
		if(gauge_check_state(file) == 0)
			break;

		printf("Gauge check failed. Retrying after 1 seconds\n");
		sleep(1);
		retries++;
	} while(retries < MAX_CHECK_STATE_RETRIES);

	if(retries == MAX_CHECK_STATE_RETRIES) {
		printf("Giving up\n");
		exit(EXIT_FAILURE);
	}

	result = gauge_check_bq27421(file, force_write);
	if (result < 0) {
		printf("Gauge check failed. Failed to check version\n");
		close(file);
		exit(EXIT_SUCCESS);
	}

	if (result == 0) {
		result = gauge_enter_cfgupdate(file);
		if (result != 0) {
			printf("%s device busy!\n", FG_NAME);
			close(file);
			exit(EXIT_FAILURE);
		}

		if (battery_type == BATT_TYPE_2000MAH) {
			/* 2000MAH */
			printf("Programming Battery 2000MAH.\n");
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_DISCHARGE);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: DISCHARGE filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_REGISTERS);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: REGISTERS filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_STATE_0);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: STATE 0 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_STATE_1);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: STATE 1 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_83_0);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: 83 0 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_83_1);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: 83 1 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_84_0);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: 84 0 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_84_1);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: 84 1 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_85_0);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: 85 0 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_CC_CAL);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: CC Cal 0 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_108_0);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: 108 0 filed\n");
				goto config_err;
			}
			result = gauge_cfg_update(file, (struct gauge_cfg_info *)BATT_2000MAH_SUBID_R_RAM);
			if (result != 0) {
				printf("ERROR: Update Subclass ID: R_RAM 0 filed\n");
				goto config_err;
			}
			gauge_exit_cfgupdate(file, 1);
		} else if (battery_type == BATT_TYPE_3000MAH) {
			/* 3000MAH */
			printf("Battery 3000MAH: Not supported yet.\n");
			gauge_exit_cfgupdate(file, 0);
		} else {
			printf("Unknow battery type.\n");
			gauge_exit_cfgupdate(file, 0);
		}
	} else if (result == ERROR_UNKNOWN_GAUGE) {
		printf("Unknow gauge.\n");
	} else {
		/* Check OpConfig, program if needed */
		result = gauge_check_default_opconfig(file);
		if (result < 0) {
			printf("Failed to check OpConfig file\n");
		} else if (result == 0) {
			result = gauge_update_opconfig(file);
			if (result == 0)
				printf("Updated OpConfig file\n");
			else
				printf("Failed to update OpConfig\n");
		} else {
			printf("Gauge is up to date\n");
		}
	}

	close(file);
	exit(EXIT_SUCCESS);

config_err:
	close(file);
	exit(EXIT_EAGAIN);
}
