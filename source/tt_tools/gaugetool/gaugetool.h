#ifndef __GAUGETOOL_H__
#define __GAUGETOOL_H__

#define VERSION_STRING		"0.4"

/* ERROR */
#define ERROR_UNKNOWN_GAUGE	0x01
#define ERROR_PROGRAMMED	0x02

/* EXIT */
#define EXIT_EAGAIN		0x02

/* ATMEL MCU */
#define REG_BATTERY_TYPE	0x09
#define BATT_TYPE_2000MAH	0x00
#define BATT_TYPE_3000MAH	0x01
#define BATT_TYPE_INVALID	0xFF

/* Register address */
#define REG_CONTROL		0x00
#define REG_FLAGS		0x06
#define REG_DATA_CLASS		0x3E
#define REG_DATA_BLOCK		0x3F
#define REG_BLOCK_DATA		0x40
#define REG_BLOCK_DATA_MAX	0x5F
#define REG_BD_CHECKSUM		0x60
#define REG_BD_CONTROL		0x61

/* CNTL DATA */
#define CNTL_STATUS		0x0000
#define CNTL_DEVICE_TYPE	0x0001
#define CNTL_FW_VERSION		0x0002
#define CNTL_SET_CFGUPDATE	0x0013
#define CNTL_SEALED		0x0020
#define CNTL_RESET		0x0041
#define CNTL_SOFT_RESET		0x0042

#define UNSEALED_KEY		0x8000

/* Flags */
#define CFGUPMODE		(1 << 4)

#define CNTL_STATUS_INITCOMP    (1 << 7)
#define CNTL_STATUS_SS          (1 << 13)

/* Subclass ID */
#define SUBCLASS_ID_DISCHARGE	0x0031
#define SUBCLASS_ID_REGISTERS	0x0040
#define SUBCLASS_ID_STATE	0x0052
#define SUBCLASS_ID_R_RAM	0x0059
#define SUBCLASS_ID_108		0x006C

/* OpConfig */
#define FG_OPCONFIG_FILL_MASK	((1 << 3) | (1 << 6) | (1 << 7) | (1 << 8) | (1 << 10))
#define FG_OPCONFIG_TEMPS	(1 << 0)
#define FG_OPCONFIG_BATLOWEN	(1 << 2)
#define FG_OPCONFIG_RMFCC	(1 << 4)
#define FG_OPCONFIG_SLEEP	(1 << 5)
#define FG_OPCONFIG_GPIOPOL	(1 << 11)
#define FG_OPCONFIG_BI_PU_EN	(1 << 12)
#define FG_OPCONFIG_BIE		(1 << 13)

#define OFFSET_OPCONFIG		0
#define DEFAULT_OPCONFIG	0x25F8
#define INIT_OPCONFIG		(FG_OPCONFIG_FILL_MASK | FG_OPCONFIG_BATLOWEN | FG_OPCONFIG_RMFCC | FG_OPCONFIG_SLEEP | FG_OPCONFIG_BI_PU_EN)

/* R_RAM */
#define OFFSET_R_RAM0		0
#define OFFSET_R_RAM1		2

/* STATE */
#define OFFSET_DESIGN_CAPACITY	10

/* BQ27421 */
#define BQ27421_DEVICE_TYPE	0x0421
#define BQ27421_FW_VERSION	0x0108

/* SEALED / UNSEALED */
#define MODE_SEALED		0
#define MODE_UNSEALED		1

#define DATA_BLOCK_SIZE		32

/* Version Check */
#define DEFAULT_SUBCLASS_108_SUM	0x3F
#define VERSION_01_SUBCLASS_108_SUM	0x7D
#define VERSION_02_SUBCLASS_108_SUM	0xC7
#define VERSION_03_SUBCLASS_108_SUM	0x98

/* 2000MAH */
/* Subclass ID: 49 (0x31)   Discharge
 * Block ID: 0
 *      SOC1 Set Threshold      (5)
 *      SOC1 Clear Threshold    (11)
 *      SOCF Set Threshold      (10)
 *      SOCF Clear Threshold    (15)
 */
char BATT_2000MAH_SUBID_DISCHARGE[35]={0x31, 0x00, 0x05, 0x0B, 0x0A, 0x0F, 0x32, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA4};

/* Subclass ID: 64 (0x40)   Registers
 * Block ID: 0
 *      OpConfig                (0x25fc)
 *      OpConfigB               (0x0f)
 */
char BATT_2000MAH_SUBID_REGISTERS[35]={0x40, 0x00, 0x25, 0xFC, 0x0F, 0x10, 0x00, 0x14, 0x04, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9E};

/* Subclass ID: 82 (0x52)   State
 * Block ID: 0
 *      Qmax Cell 0             (16220)
 *      Update Status           (0)
 *      Reserve Cap-mAh         (0)
 *      Load Select/Mode        (129)
 *      Q Invalid Max V         (3662)
 *      Q Invalid Min V         (3662)
 *      Design Capacity         (2000)
 *      Design Energy           (7400)
 *      Default Design Cap      (5872)
 *      Terminate Voltage       (2700)
 *      SOCI Delta              (1)
 *      Taper Rate              (147)
 *      Taper Voltage           (4100)
 */
char BATT_2000MAH_SUBID_STATE_0[35]={0x52, 0x00, 0x3F, 0x5C, 0x00, 0x00, 0x00, 0x81, 0x0E, 0x4E, 0x0E, 0x4E, 0x07, 0xD0, 0x1C, 0xE8, 0x16, 0xF0, \
			0x0A, 0x8C, 0x00, 0xC8, 0x00, 0x32, 0x00, 0x02, 0x01, 0x40, 0x01, 0x00, 0x93, 0x10, 0x04, 0x00, 0xCF};

/* Subclass ID: 82 (0x52)   State
 * Block ID: 1
 *      Sleep Current           (10)
 *      V at Chg Term           (4200)
 *      Avg I Last Run          (-25)
 *      Avg P Last Run          (-26)
 *      Delta Voltage           (3)
 */
char BATT_2000MAH_SUBID_STATE_1[35]={0x52, 0x01, 0x0A, 0x10, 0x68, 0xFF, 0xE7, 0xFF, 0xE6, 0x00, 0x03, 0x02, 0xBC, 0x00, 0x00, 0x00, 0x00, 0x00, \
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF1};

/* Subclass ID: 83 (0x53)
 * Block ID: 0
 */
char BATT_2000MAH_SUBID_83_0[35]={0x53, 0x00, 0x20, 0x56, 0x10, 0x5B, 0xD5, 0xDA, 0xEB, 0xE9, 0xE4, 0xE4, 0xE2, 0xE5, 0xE9, 0xE8, 0xEE, 0xEA, \
			0xEC, 0xE8, 0xEA, 0xE2, 0xDE, 0xE0, 0xDF, 0xEE, 0xEF, 0xF2, 0xF1, 0xF3, 0xF3, 0xF3, 0xF3, 0xF5, 0xA5};

/* Subclass ID: 83 (0x53)
 * Block ID: 1
 */
char BATT_2000MAH_SUBID_83_1[35]={0x53, 0x01, 0xF3, 0xE6, 0xE7, 0xD4, 0xDA, 0xDA, 0xED, 0xF2, 0xF7, 0xED, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, \
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x74};

/* Subclass ID: 84 (0x54)
 * Block ID: 0
 */
char BATT_2000MAH_SUBID_84_0[35]={0x54, 0x00, 0xFE, 0x0B, 0x05, 0x0C, 0x05, 0x04, 0x03, 0xFB, 0x05, 0xFC, 0x01, 0xFB, 0x05, 0xFC, 0x01, 0x00, \
			0x04, 0x03, 0x04, 0x08, 0x06, 0xF9, 0x01, 0xFE, 0xFD, 0x03, 0x00, 0xFF, 0xFF, 0xFC, 0xEF, 0xED, 0xF8};

/* Subclass ID: 84 (0x54)
 * Block ID: 1
 */
char BATT_2000MAH_SUBID_84_1[35]={0x54, 0x01, 0xF6, 0xF9, 0x15, 0xFF, 0x15, 0x05, 0x04, 0xE3, 0x9E, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDC};

/* Subclass ID: 85 (0x55)
 * Block ID: 0
 */
char BATT_2000MAH_SUBID_85_0[35]={0x55, 0x00, 0xFF, 0xB7, 0x02, 0xF7, 0xFD, 0xF0, 0x1D, 0xFB, 0x02, 0xF7, 0x03, 0xFF, 0xFA, 0xEF, 0x10, 0x04, \
			0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14};

/* Subclass ID: 105 (0x69)   CC Cal
 * Block ID: 0
 *      CC Offset               (65535)
 *      CC Cal Temp             (2991)
 *      CC Gain                 (2134565192.000000)
 *      CC Delta                (2471795205.000000)
 */
char BATT_2000MAH_SUBID_CC_CAL[35]={0x69, 0x00, 0xFF, 0xFF, 0x0B, 0xAF, 0x7F, 0x3A, 0xE1, 0x48, 0x93, 0x54, 0x9A, 0x05, 0x00, 0x00, 0x00, 0x00, \
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDF};

/* Subclass ID: 108 (0x6c)
 * Block ID: 0
 */
char BATT_2000MAH_SUBID_108_0[35]={0x6c, 0x00, 0xFF, 0x45, 0x03, 0x03, 0x01, 0x01, 0xFF, 0xFE, 0xFD, 0xF0, 0xFA, 0xF6, 0xF0, 0xDD, 0xE2, 0xF2, \
			0xA0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x98};

/* Subclass ID: 89 (0x59)   R_a RAM
 * Block ID: 0
 *      R_a0 0                  (20)
 *      R_a0 1                  (22)
 *      R_a0 2                  (25)
 *      R_a0 3                  (31)
 *      R_a0 4                  (28)
 *      R_a0 5                  (16)
 *      R_a0 6                  (18)
 *      R_a0 7                  (17)
 *      R_a0 8                  (16)
 *      R_a0 9                  (16)
 *      R_a0 10                 (20)
 *      R_a0 11                 (22)
 *      R_a0 12                 (30)
 *      R_a0 13                 (43)
 *      R_a0 14                 (87)
 */
char BATT_2000MAH_SUBID_R_RAM[35]={0x59, 0x00, 0x00, 0x14, 0x00, 0x16, 0x00, 0x19, 0x00, 0x1F, 0x00, 0x1C, 0x00, 0x10, 0x00, 0x12, 0x00, 0x11, \
			0x00, 0x10, 0x00, 0x10, 0x00, 0x14, 0x00, 0x16, 0x00, 0x1E, 0x00, 0x2B, 0x00, 0x57, 0x00, 0x00, 0x64};

struct gauge_cfg_info {
	char subclass_id;
	char block_id;
	char data[32];
	char checksum;
};

#endif /* __GAUGETOOL_H__ */

