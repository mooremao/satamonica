#!/bin/sh
#################################################################
# TI BQ27421 .gm.fs convert script Version 0.2                  #
#                                                               #
# This script convert the gm.fs file to header file.            #
# Use it to generate header file for new battery type           #
#################################################################

# Init value
FILENAME=""
count=0
flag=0
addr=0
block=0
temp=0

if [ "$1" != "" ] && [ "$2" != "" ]; then
    FILENAME=$1
    BATT_DC=$2
else
    echo "Useage: $0 <gm.fs filename> <Design Capacity>"
    echo "Ex: $0 TomTom_Longbeach_bq27421G1A_v108_chemID_285_golden_20140905.gm.fs 2000"
    exit 1
fi

if [ ! -f $FILENAME ]; then
    echo "Please input correct filename"
    exit 1
fi

HEADER="${FILENAME}.h"
if [ -f $HEADER ]; then
    rm -rf $HEADER
fi

# Definition
TITLE_02_0="BATT_${BATT_DC}MAH_SUBID_SAFETY"
TITLE_36_0="BATT_${BATT_DC}MAH_SUBID_CHARGE_TERMINATION"
TITLE_48_0="BATT_${BATT_DC}MAH_SUBID_DATA"
TITLE_49_0="BATT_${BATT_DC}MAH_SUBID_DISCHARGE"
TITLE_64_0="BATT_${BATT_DC}MAH_SUBID_REGISTERS"
TITLE_68_0="BATT_${BATT_DC}MAH_SUBID_POWER"
TITLE_80_0="BATT_${BATT_DC}MAH_SUBID_IT_CFG_0"
TITLE_80_1="BATT_${BATT_DC}MAH_SUBID_IT_CFG_1"
TITLE_80_2="BATT_${BATT_DC}MAH_SUBID_IT_CFG_2"
TITLE_81_0="BATT_${BATT_DC}MAH_SUBID_CURRENT_THRESHOLDS"
TITLE_82_0="BATT_${BATT_DC}MAH_SUBID_STATE_0"
TITLE_82_1="BATT_${BATT_DC}MAH_SUBID_STATE_1"
TITLE_89_0="BATT_${BATT_DC}MAH_SUBID_R_RAM"
TITLE_83_0="BATT_${BATT_DC}MAH_SUBID_83_0"
TITLE_83_1="BATT_${BATT_DC}MAH_SUBID_83_1"
TITLE_84_0="BATT_${BATT_DC}MAH_SUBID_84_0"
TITLE_84_1="BATT_${BATT_DC}MAH_SUBID_84_1"
TITLE_85_0="BATT_${BATT_DC}MAH_SUBID_85_0"
TITLE_104_0="BATT_${BATT_DC}MAH_SUBID_104_DATA"
TITLE_105_0="BATT_${BATT_DC}MAH_SUBID_CC_CAL"
TITLE_107_0="BATT_${BATT_DC}MAH_SUBID_CURRENT"
TITLE_108_0="BATT_${BATT_DC}MAH_SUBID_108_0"
TITLE_112_0="BATT_${BATT_DC}MAH_SUBID_CODES"
TITLE_EXTRA="BATT_${BATT_DC}MAH_EXTRA"

generate_header_file ()
{
    if [ $1 -eq 20 ]; then
        TITLE=$TITLE_02_0
    elif [ $1 -eq 360 ]; then
        TITLE=$TITLE_36_0
    elif [ $1 -eq 490 ]; then
        TITLE=$TITLE_49_0
    elif [ $1 -eq 640 ]; then
        TITLE=$TITLE_64_0
    elif [ $1 -eq 680 ]; then
        TITLE=$TITLE_68_0
    elif [ $1 -eq 820 ]; then
        TITLE=$TITLE_82_0
    elif [ $1 -eq 800 ]; then
        TITLE=$TITLE_80_0
    elif [ $1 -eq 801 ]; then
        TITLE=$TITLE_80_1
    elif [ $1 -eq 802 ]; then
        TITLE=$TITLE_80_2
    elif [ $1 -eq 810 ]; then
        TITLE=$TITLE_81_0
    elif [ $1 -eq 821 ]; then
        TITLE=$TITLE_82_1
    elif [ $1 -eq 830 ]; then
        TITLE=$TITLE_83_0
    elif [ $1 -eq 831 ]; then
        TITLE=$TITLE_83_1
    elif [ $1 -eq 840 ]; then
        TITLE=$TITLE_84_0
    elif [ $1 -eq 841 ]; then
        TITLE=$TITLE_84_1
    elif [ $1 -eq 850 ]; then
        TITLE=$TITLE_85_0
    elif [ $1 -eq 890 ]; then
        TITLE=$TITLE_89_0
    elif [ $1 -eq 1040 ]; then
        TITLE=$TITLE_104_0
    elif [ $1 -eq 1050 ]; then
        TITLE=$TITLE_105_0
    elif [ $1 -eq 1070 ]; then
        TITLE=$TITLE_107_0
    elif [ $1 -eq 1080 ]; then
        TITLE=$TITLE_108_0
    elif [ $1 -eq 1120 ]; then
        TITLE=$TITLE_112_0
    else
        TITLE=$TITLE_EXTRA
    fi

    echo -n "char $TITLE[35]={0x`printf %02x $addr`, 0x`printf %02x $block``echo $LINE | sed 's/^.*AA 40//g' | sed -n 's/ /, 0x/gp'`" >> $HEADER
}

cat $FILENAME | while read LINE
do
    set -- junk $LINE
    shift

    if [ $flag -ne 0 ]; then
        addr=$(($flag / 10))
        block=$(($flag % 10))
    fi
    if [ $flag -eq 20 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Safety" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Over Temp               (%d)\n" "0x$4$5" | tee -a $HEADER
            printf " *      Under Temp              (%d)\n" "0x$6$7" | tee -a $HEADER
            printf " *      Temp Hys                (%d)\n" "0x$8" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 360 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Charge Termination" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      TCA Set                 (%d)\n" "0x$7" | tee -a $HEADER
            printf " *      TCA Clear               (%d)\n" "0x$8" | tee -a $HEADER
            VAL=`printf %d 0x$9`
            if [ $VAL -gt 127 ]; then
                VAL=$(($VAL - 256))
            fi
            printf " *      FC Set                  (%d)\n" $VAL | tee -a $HEADER
            printf " *      FC Clear                (%d)\n" "0x$10" | tee -a $HEADER
            printf " *      DODatEOC Delta T        (%d)\n" "0x$11$12" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 480 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Data" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            VAL=`printf %d 0x$6`
            if [ $VAL -gt 127 ]; then
                VAL=$(($VAL - 256))
            fi
            printf " *      Initial Standby         (%d)\n" $VAL | tee -a $HEADER
            VAL=`printf %d 0x$7$8`
            if [ $VAL -gt 32767 ]; then
                VAL=$(($VAL - 65536))
            fi
            printf " *      Initial MaxLoad         (%d)\n" $VAL | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 490 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Discharge" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      SOC1 Set Threshold      (%d)\n" "0x$4" | tee -a $HEADER
            printf " *      SOC1 Clear Threshold    (%d)\n" "0x$5" | tee -a $HEADER
            printf " *      SOCF Set Threshold      (%d)\n" "0x$6" | tee -a $HEADER
            printf " *      SOCF Clear Threshold    (%d)\n" "0x$7" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 640 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Registers" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      OpConfig                (0x%04x)\n" "0x$4$5" | tee -a $HEADER
            printf " *      OpConfigB               (0x%02x)\n" "0x$6" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 680 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Power" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Hibernate I             (%d)\n" "0x$11$12" | tee -a $HEADER
            printf " *      Hibernate V             (%d)\n" "0x$13$14" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 800 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   IT Cfg" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      FH Setting 0            (%d)\n" "0x$15$16" | tee -a $HEADER
            printf " *      Ra Filter               (%d)\n" "0x$26$27" | tee -a $HEADER
            printf " *      FH Setting 1            (%d)\n" "0x$28$29" | tee -a $HEADER
            printf " *      FH Setting 2            (%d)\n" "0x$30$31" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 801 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   IT Cfg" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      FH Setting 3                (%d)\n" "0x$5" | tee -a $HEADER
            printf " *      Fast Qmax Start DOD         (%d)\n" "0x$7" | tee -a $HEADER
            printf " *      Fast Qmax End DOD           (%d)\n" "0x$8" | tee -a $HEADER
            printf " *      Fast Qmax Start Volt Delta  (%d)\n" "0x$9$10" | tee -a $HEADER
            printf " *      Fast Qmax Current Threshold (%d)\n" "0x$11$12" | tee -a $HEADER
            printf " *      Fast Qmax Min Points        (%d)\n" "0x$13" | tee -a $HEADER
            printf " *      Max Qmax Change             (%d)\n" "0x$17" | tee -a $HEADER
            printf " *      Qmax Max Delta              (%d)\n" "0x$18" | tee -a $HEADER
            printf " *      Max Default Qmax            (%d)\n" "0x$19" | tee -a $HEADER
            printf " *      Qmax Filter                 (%d)\n" "0x$20" | tee -a $HEADER
            printf " *      ResRelax Time               (%d)\n" "0x$22$23" | tee -a $HEADER
            printf " *      User Rate-mA                (%d)\n" "0x$24$25" | tee -a $HEADER
            printf " *      User Rate-mW                (%d)\n" "0x$26$27" | tee -a $HEADER
            printf " *      Max Sim Rate                (%d)\n" "0x$33" | tee -a $HEADER
            printf " *      Min Sim Rate                (%d)\n" "0x$34" | tee -a $HEADER
            temp=$35
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 802 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   IT Cfg" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Ra Max Delta            (%d)\n" "0x$temp$4" | tee -a $HEADER
            printf " *      Min Delta Voltage       (%d)\n" "0x$12$13" | tee -a $HEADER
            printf " *      Max Delta Voltage       (%d)\n" "0x$14$15" | tee -a $HEADER
            printf " *      DeltaV Max dV           (%d)\n" "0x$16$17" | tee -a $HEADER
            printf " *      TermV Valid t           (%d)\n" "0x$18" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 810 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Current Thresholds" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Dsg Current Threshold   (%d)\n" "0x$4$5" | tee -a $HEADER
            printf " *      Chg Current Threshold   (%d)\n" "0x$6$7" | tee -a $HEADER
            printf " *      Quit Current            (%d)\n" "0x$8$9" | tee -a $HEADER
            printf " *      Dsg Relax Time          (%d)\n" "0x$10$11" | tee -a $HEADER
            printf " *      Chg Relax Time          (%d)\n" "0x$12" | tee -a $HEADER
            printf " *      Quit Relax Time         (%d)\n" "0x$13" | tee -a $HEADER
            printf " *      Max IR Correct          (%d)\n" "0x$16$17" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 820 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   State" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Qmax Cell 0             (%d)\n" "0x$4$5" | tee -a $HEADER
            printf " *      Update Status           (%d)\n" "0x$6$7" | tee -a $HEADER
            printf " *      Reserve Cap-mAh         (%d)\n" "0x$8" | tee -a $HEADER
            printf " *      Load Select/Mode        (%d)\n" "0x$9" | tee -a $HEADER
            printf " *      Q Invalid Max V         (%d)\n" "0x$10$11" | tee -a $HEADER
            printf " *      Q Invalid Min V         (%d)\n" "0x$12$13" | tee -a $HEADER
            printf " *      Design Capacity         (%d)\n" "0x$14$15" | tee -a $HEADER
            printf " *      Design Energy           (%d)\n" "0x$16$17" | tee -a $HEADER
            printf " *      Default Design Cap      (%d)\n" "0x$18$19" | tee -a $HEADER
            printf " *      Terminate Voltage       (%d)\n" "0x$20$21" | tee -a $HEADER
            printf " *      SOCI Delta              (%d)\n" "0x$30" | tee -a $HEADER
            printf " *      Taper Rate              (%d)\n" "0x$31$32" | tee -a $HEADER
            printf " *      Taper Voltage           (%d)\n" "0x$33$34" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 821 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   State" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Sleep Current           (%d)\n" "0x$4" | tee -a $HEADER
            printf " *      V at Chg Term           (%d)\n" "0x$5$6" | tee -a $HEADER
            VAL=`printf %d 0x$7$8`
            if [ $VAL -gt 32767 ]; then
                VAL=$(($VAL - 65536))
            fi
            printf " *      Avg I Last Run          (%d)\n" $VAL | tee -a $HEADER
            VAL=`printf %d 0x$9$10`
            if [ $VAL -gt 32767 ]; then
                VAL=$(($VAL - 65536))
            fi
            printf " *      Avg P Last Run          (%d)\n" $VAL | tee -a $HEADER
            VAL=`printf %d 0x$11$12`
            if [ $VAL -gt 32767 ]; then
                VAL=$(($VAL - 65536))
            fi
            printf " *      Delta Voltage           (%d)\n" $VAL | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 890 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   R_a RAM" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      R_a0 0                  (%d)\n" "0x$4$5" | tee -a $HEADER
            printf " *      R_a0 1                  (%d)\n" "0x$6$7" | tee -a $HEADER
            printf " *      R_a0 2                  (%d)\n" "0x$8$9" | tee -a $HEADER
            printf " *      R_a0 3                  (%d)\n" "0x$10$11" | tee -a $HEADER
            printf " *      R_a0 4                  (%d)\n" "0x$12$13" | tee -a $HEADER
            printf " *      R_a0 5                  (%d)\n" "0x$14$15" | tee -a $HEADER
            printf " *      R_a0 6                  (%d)\n" "0x$16$17" | tee -a $HEADER
            printf " *      R_a0 7                  (%d)\n" "0x$18$19" | tee -a $HEADER
            printf " *      R_a0 8                  (%d)\n" "0x$20$21" | tee -a $HEADER
            printf " *      R_a0 9                  (%d)\n" "0x$22$23" | tee -a $HEADER
            printf " *      R_a0 10                 (%d)\n" "0x$24$25" | tee -a $HEADER
            printf " *      R_a0 11                 (%d)\n" "0x$26$27" | tee -a $HEADER
            printf " *      R_a0 12                 (%d)\n" "0x$28$29" | tee -a $HEADER
            printf " *      R_a0 13                 (%d)\n" "0x$30$31" | tee -a $HEADER
            printf " *      R_a0 14                 (%d)\n" "0x$32$33" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 1040 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Data" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Board Offset            (%d)\n" "0x$4" | tee -a $HEADER
            printf " *      Int Temp Offset         (%d)\n" "0x$5" | tee -a $HEADER
            printf " *      Pack V Offset           (%d)\n" "0x$6" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 1050 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   CC Cal" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      CC Offset               (%d)\n" "0x$4$5" | tee -a $HEADER
            printf " *      CC Cal Temp             (%d)\n" "0x$6$7" | tee -a $HEADER
            printf " *      CC Gain                 (%f)\n" "0x$8$9$10$11" | tee -a $HEADER
            printf " *      CC Delta                (%f)\n" "0x$12$13$14$15" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 1070 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Current" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Deadband                (%d)\n" "0x$5" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -eq 1120 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)   Codes" | tee -a $HEADER
            echo " * Block ID: $block" | tee -a $HEADER
            printf " *      Sealed to Unsealed      (0x%x)\n" "0x$5$6$7$8" | tee -a $HEADER
            echo " */" | tee -a $HEADER
            generate_header_file $flag
        fi
        flag=0
    elif [ $flag -ne 0 ]; then
        if [ "$1" = "W:" ]; then
            echo "/* Subclass ID: $addr (0x`printf %02x $addr`)" >> $HEADER
            echo " * Block ID: $block" >> $HEADER
            echo " */" >> $HEADER
            generate_header_file $flag
        fi
        flag=0
    else
        if [ "$1" = "W:" ] && [ "$3" = "3E" ]; then
            case $4 in
               02)
                   flag=20
                   ;;
               24)
                   flag=360
                   ;;
               30)
                   flag=480
                   ;;
               31)
                   flag=490
                   ;;
               40)
                   flag=640
                   ;;
               44)
                   flag=680
                   ;;
               50)
                   if [ $5 -eq 00 ]; then
                       flag=800
                   elif [ $5 -eq 01 ]; then
                       flag=801
                   elif [ $5 -eq 02 ]; then
                       flag=802
                   fi
                   ;;
               51)
                   flag=810
                   ;;
               52)
                   if [ $5 -eq 00 ]; then
                       flag=820
                   elif [ $5 -eq 01 ]; then
                       flag=821
                   fi
                   ;;
               53)
                   if [ $5 -eq 00 ]; then
                       flag=830
                   elif [ $5 -eq 01 ]; then
                       flag=831
                   fi
                   ;;
               54)
                   if [ $5 -eq 00 ]; then
                       flag=840
                   elif [ $5 -eq 01 ]; then
                       flag=841
                   fi
                   ;;
               55)
                   flag=850
                   ;;
               59)
                   flag=890
                   ;;
               68)
                   flag=1040
                   ;;
               69)
                   flag=1050
                   ;;
               6B)
                   flag=1070
                   ;;
               6C)
                   flag=1080
                   ;;
               70)
                   flag=1120
                   ;;
           esac
        elif [ "$1" = "W:" ] && [ "$3" = "60" ]; then
            echo ", 0x$4};\n" >> $HEADER
        fi
    fi
done
