/**
 * \file
 *
 * \brief SAM I2C Slave Bootloader
 *
 * Copyright (C) 2013-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

/**
 * \mainpage SAM I2C Slave Bootloader
 * See \ref appdoc_main "here" for project documentation.
 * \copydetails appdoc_preface
 *
 *
 * \page appdoc_preface Features
 * \li Application for self programming
 * \li Uses I2C Slave interface
 * \li I2C Master sends the data to be programmed over I2C bus
 * \li Resets the device after programming and starts executing application
 */

/**
 * \page appdoc_main SAM I2C Slave Bootloader
 *
 * Overview:
 * - \ref appdoc_sam0_i2c_slave_bootloader_features
 * - \ref appdoc_sam0_i2c_slave_bootloader_intro
 * - \ref appdoc_sam0_i2c_slave_bootloader_mem_org
 * - \ref appdoc_sam0_i2c_slave_bootloader_prereq
 * - \ref appdoc_sam0_i2c_slave_bootloader_hw
 * - \ref appdoc_sam0_i2c_slave_bootloader_process
 *    - \ref appdoc_sam0_i2c_slave_bootloader_process_boot_check
 *    - \ref appdoc_sam0_i2c_slave_bootloader_process_init
 *    - \ref appdoc_sam0_i2c_slave_bootloader_boot_protocol
 *    - \ref appdoc_sam0_i2c_slave_bootloader_start_app
 * - \ref appdoc_sam0_i2c_slave_bootloader_compinfo
 * - \ref appdoc_sam0_i2c_slave_bootloader_contactinfo
 *
 * \section appdoc_sam0_i2c_slave_bootloader_features Features
 * \li Application for self programming
 * \li Uses I2C Slave interface
 * \li I2C Master sends the data to be programmed over I2C bus
 * \li Resets the device after programming and starts executing application
 *
 * \section appdoc_sam0_i2c_slave_bootloader_intro Introduction
 * As many electronic designs evolve rapidly there is a growing need for being
 * able to update products, which have already been shipped or sold.
 * Microcontrollers that support boot loader facilitates updating the
 * application flash section without the need of an external programmer, are of
 * great use in situations where the application has to be updated on the field.
 * The boot loader may use various interfaces like SPI, UART, TWI, Ethernet etc.
 *
 * This application implements a I2C Slave bootloader for SAM devices.
 *
 * This application has been tested on following boards:
 * - SAM D20/D21/R21/D11/L22 Xplained Pro
 *
 * \section appdoc_sam0_i2c_slave_bootloader_mem_org Program Memory Organization
 * This bootloader implementation consumes around 8000 bytes (approximately),
 * which is 32 rows of Program Memory space starting from 0x00000000. BOOTPROT
 * fuses on the device can be set to protect first 32 rows of the program
 * memory which are allocated for the BOOT section. So, the end user application
 * should be generated with starting address as 0x00002000.
 *
 * \section appdoc_sam0_i2c_slave_bootloader_prereq Prerequisites
 * There are no prerequisites for this implementation
 *
 * \section appdoc_sam0_i2c_slave_bootloader_hw Hardware Setup
 * SAM device in SAM Xplained Pro kit is used as the I2C Slave.
 * I2C master should be connected to PIN11 (PA08 - SDA) and PIN12 (PA09 - SCL)
 * on External header 1 (EXT1) of SAM D20/D21 Xplained Pro.
 * I2C master should be connected to PIN11 (PA16 - SDA) and PIN12 (PA17 - SCL)
 * on External header 1 (EXT1) of SAM R21 Xplained Pro.
 * I2C master should be connected to PIN11 (PA22 - SDA) and PIN12 (PA23 - SCL)
 * on External header 1 (EXT1) of SAM D10/D11 Xplained Pro.
 * I2C master should be connected to PIN11 (PB30 - SDA) and PIN12 (PB31 - SCL)
 * on External header 1 (EXT1) of SAM L22 Xplained Pro.
 * SW0 will be configured as BOOT_LOAD_PIN and LED0 will be used to
 * display the bootloader status. LED0 will be ON when the device is in
 * bootloader mode.
 *
 * \section appdoc_sam0_i2c_slave_bootloader_process Bootloader Process
 *
 * \subsection appdoc_sam0_i2c_slave_bootloader_process_boot_check Boot Check
 * The bootloader is located at the start of the program memory and is
 * executed at each reset/power-on sequence. Initially check the
 * status of a user configurable BOOT_LOAD_PIN.
 * - If the pin is pulled low continue execution in bootloader mode.
 * - Else read the first location of application section (0x00002000) which
 *   contains the stack pointer address and check whether it is 0xFFFFFFFF.
 *    - If yes, application section is empty and wait indefinitely there.
 *    - If not, jump to the application section and start execution from there.
 * \note Configuring the BOOT_LOAD_PIN and disabling watchdog in this boot mode
 * check routine are made with direct peripheral register access to enable quick
 * decision on application or bootloader mode.
 *
 * \subsection appdoc_sam0_i2c_slave_bootloader_process_init Initialization
 * Initialize the following
 *   - Board
 *   - System clock
 *   - I2C Slave module
 *   - NVM module
 *
 * \subsection appdoc_sam0_i2c_slave_bootloader_boot_protocol Boot Protocol
 *   - I2C Master first sends 4 bytes of data which contains the length of
 *     the data to be programmed
 *   - Read a block from I2C Master of size NVMCTRL_PAGE_SIZE
 *   - Program the data to Program memory starting APP_START_ADDRESS
 *   - Send an acknowledgement byte 's' to I2C Master to indicate it has
 *     received the data and finished programming
 *   - Repeat till entire length of data has been programmed to the device
 *
 * \subsection appdoc_sam0_i2c_slave_bootloader_start_app Start Application
 * Once the programming is completed, enable Watchdog Timer with a timeout
 * period of 256 clock cycles and wait in a loop for Watchdog to reset
 * the device.
 *
 * \section appdoc_sam0_i2c_slave_bootloader_compinfo Compilation Info
 * This software was written for the GNU GCC and IAR for ARM.
 * Other compilers may or may not work.
 *
 * \section appdoc_sam0_i2c_slave_bootloader_contactinfo Contact Information
 * For further information, visit
 * <a href="http://www.atmel.com">http://www.atmel.com</a>.
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <string.h> /* memset */
#include <asf.h>
#include "bootupdater.h"

/* Function prototypes */
static void program_memory(uint32_t address, uint8_t *buffer, uint16_t len);

/**
 * \brief Function for programming data to Flash
 *
 * This function will check whether the data is greater than Flash page size.
 * If it is greater, it splits and writes pagewise.
 *
 * \param address address of the Flash page to be programmed
 * \param buffer  pointer to the buffer containing data to be programmed
 * \param len     length of the data to be programmed to Flash
 */
static void program_memory(uint32_t address, uint8_t *buffer, uint16_t len)
{
	/* Check if length is greater than Flash page size */
	if (len > NVMCTRL_PAGE_SIZE) {
		uint32_t offset = 0;

		while (len > NVMCTRL_PAGE_SIZE) {
			/* Check if it is first page of a row */
			if ((address & 0xFF) == 0) {
				/* Erase row */
				nvm_erase_row(address);
			}
			/* Write one page data to flash */
			nvm_write_buffer(address, buffer + offset, NVMCTRL_PAGE_SIZE);
			/* Increment the address to be programmed */
			address += NVMCTRL_PAGE_SIZE;
			/* Increment the offset of the buffer containing data */
			offset += NVMCTRL_PAGE_SIZE;
			/* Decrement the length */
			len -= NVMCTRL_PAGE_SIZE;
		}

		/* Check if there is data remaining to be programmed*/
		if (len > 0) {
			/* Write the data to flash */
			nvm_write_buffer(address, buffer + offset, len);
		}
	} else {
		/* Check if it is first page of a row) */
		if ((address & 0xFF) == 0) {
			/* Erase row */
			nvm_erase_row(address);
		}
		/* Write the data to flash */
		nvm_write_buffer(address, buffer, len);
	}
}

extern char _binary___samd10_bootloader_bin_start;
extern char _binary___samd10_bootloader_bin_size;

/**
 * \brief Main application
 */
int main(void)
{
	uint8_t buff[NVMCTRL_PAGE_SIZE];
	uint32_t bootloader;
	uint16_t bootloaderlength;
	struct nvm_config config;

	bootloader = &_binary___samd10_bootloader_bin_start;
	bootloaderlength = &_binary___samd10_bootloader_bin_size;

	/* Initialize system */
	system_init();

	/* Get NVM default configuration and load the same */
	nvm_get_config_defaults(&config);
	config.manual_page_write = false;
	nvm_set_config(&config);

	memset(buff, 0xFF, sizeof(buff));
//	nvm_execute_command( NVM_COMMAND_UNLOCK_REGION, 0, 0); 
//	nvm_execute_command( NVM_COMMAND_UNLOCK_REGION, 0x1000, 0); // 4K regions on SAMD21E16xx
	program_memory(0x0, (uint8_t *)bootloader, (uint16_t)bootloaderlength);
	program_memory(0x2000, (uint8_t *)&buff[0], NVMCTRL_PAGE_SIZE); // erase a piece of ourselves so the bootloader doesn't try to reboot us.

	while (1) {
		/* Inf loop */
	}

}
