/*
**  CRC.H - header file for CRC and checksum functions
*/

#ifndef CRC__H
#define CRC__H

#include <stdlib.h>           /* For size_t                           */
#include <stdint.h>           /* For uint8_t, uint16_t, uint32_t      */

#define UPDC32(octet,crc) (crc_32_tab[((crc)\
     ^ ((uint8_t)octet)) & 0xff] ^ ((crc) >> 8))

uint32_t updateCRC32(unsigned char ch, uint32_t crc);
uint32_t crc32buf(uint8_t *buf, size_t len);

#endif /* CRC__H */