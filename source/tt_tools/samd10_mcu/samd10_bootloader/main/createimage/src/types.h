// TomTom N.V.
// Project: Athens
// Author: J. Taverne
// File: types.h

#ifndef __TYPES_H__
#define __TYPES_H__



#if   defined ( __CC_ARM )
#define __ASM            __asm                                      /*!< asm keyword for ARM Compiler          */
#define __INLINE         __inline                                   /*!< inline keyword for ARM Compiler       */
#define __PACKED         __packed                                   /*!< packed keyword for ARM Compiler       */
#define __TASK        	 __task                                  	/*!< task keyword for ARM Compiler       */
#define __DECLARE_PACKED(__declaration__) __PACKED __declaration__

#elif defined ( __ICCARM__ )
#define __ASM           __asm                                       /*!< asm keyword for IAR Compiler          */
#define __INLINE        inline                                      /*!< inline keyword for IAR Compiler. Only available in High optimization mode! */

#elif defined ( __GNUC__ )
#define __ASM            __asm                                      /*!< asm keyword for GNU Compiler          */
#define __INLINE         inline                                     /*!< inline keyword for GNU Compiler       */
#define __PACKED         				                            /*!< packed keyword for GNU Compiler     TODO NEED TO CHECK WHAT THIS SHOULD BE FOR GNU  */
#define __TASK        	 		                                  	/*!< task keyword for GNU Compiler       */
#define __DECLARE_PACKED(__declaration__) __declaration__ __attribute__ ((__packed__))

#elif defined ( __TASKING__ )
#define __ASM            __asm                                      /*!< asm keyword for TASKING Compiler      */
#define __INLINE         inline                                     /*!< inline keyword for TASKING Compiler   */

#endif




#include <stdint.h>
	typedef int		bool_t;
	typedef unsigned int uint_t;
	typedef signed int int_t;

#ifndef __bool_true_false_are_defined
#define false	(0)
#define true	(1)
#endif

#ifndef NULL
#define NULL	(0)
#endif

#endif
