/**
 * Description:
 *  Image generator
 * 
 * Project: Tools (createimage)
 *
 * Author/s:
 *		Gareth Lonng (previous work by Robert Scott and J. Taverne)
 *
 * Filename: createimage.c
 *
 * Notes: This file, its mechanism and memory mapping is described in further
 * detail on the wiki here:
 * https://confluence.tomtomgroup.com/display/FIT/Bootloader+and+New+Memory+Map+Information?src=contextnavpagetreemode
 *
 * Copyright (c)2014 TomTom N.V.
 */

#define CREATEIMAGE_VERSION "2.01"

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <ctype.h>

#include <stdlib.h>
#include <string.h>

#include "aes.h"
#include "md5.h"
#include "types.h"
#include "dirent.h"

#include "crc.h"

//#define USE_LZO_COMPRESSION

/* compression using LZ4 */

#include "lz4.h"
#include "lz4hc.h"
#include "lz4frame.h"
#include "lz4frame_static.h"

#ifdef USE_LZO_COMPRESSION
/* compression using LZO */
#include <lzo/lzoconf.h>
#include <lzo/lzo1x.h>
#endif


//#define GENERATE_BOOT_UNLOCK_CODE

/****************************************************/
/* PRIVATE DEFINITIONS, TYPES & VARIABLES			*/
/****************************************************/

#define MD5_SIZE			16
#define SIZE_ADDRESS		0xcc					// The size of the image gets stamped at an offset of 0xCC
#define END_ADDRESS			(0x5f9fff - MD5_SIZE)	// Note some reserved memory the image can not use
#define BUFFER_SIZE			(4096 * 1024)			// was size of flash on the SAM7 - 2MBytes. Some files we add can be larger, however
#define FLASH_START_ADDRESS	0x400000

/* The most significant byte is a version (currently 0) of mismatch and can be incremented
 * as required if mismatch happens again in the future */
#define BUILD_CONFIRMATION_KEY_UNCOMPRESSED				(0x00DEC1DE)
#define BUILD_CONFIRMATION_KEY_COMPRESSED				(0x01DEC1DE)
#define CHUNK_TYPE_APP									0x00000001UL
#define CHUNK_TYPE_RAW_FILE								0x00000002UL
#define CHUNK_TYPE_RAW_FILE_APPEND						0x00000003UL
#define CHUNK_TYPE_RAW_FILE_COMPRESSED_LZO				0x00000004UL
#define CHUNK_TYPE_RAW_FILE_COMPRESSED_LZO_APPEND		0x00000005UL
#define CHUNK_TYPE_MEMPOOL_SIZE_SET						0x00000006UL
#define CHUNK_TYPE_RAW_FILE_COMPRESSED_LZ4HC			0x00000007UL
#define CHUNK_TYPE_RAW_FILE_COMPRESSED_LZ4HC_APPEND		0x00000008UL
#define CHUNK_TYPE_APP_COMPRESSED_LZ4HC					0x00000009UL
#define CHUNK_TYPE_BOOTLOADER_COMPRESSED_LZ4HC			0x0000000AUL

#ifdef USE_LZO_COMPRESSION
lzo_voidp wrkmem;
#endif

#define DEFAULT_MAX_UNCOMPRESSED_DATA_SIZE 0x4000//0x20000 // 128K

static uint32_t max_uncompressed_data_size = DEFAULT_MAX_UNCOMPRESSED_DATA_SIZE;

#define COMPRESSION_TYPE_NONE	0
#define COMPRESSION_TYPE_LZO	1
#define COMPRESSION_TYPE_LZ4HC	2

static char compression_type = COMPRESSION_TYPE_LZ4HC;
#define MAX_FNAME 512
static char	path[MAX_FNAME];
static char	drive[MAX_FNAME];
static char	dir[MAX_FNAME];
static char	fname[MAX_FNAME];
static char	ext[MAX_FNAME];
static bool_t debug = false;
static bool_t unload_image = false;
static bool_t exclude_updatefile = false;

static uint8_t	compressbuffer[0x800000]; // to save us from having to reallocate this possibly hundreds of times
static uint8_t	input_buffer[BUFFER_SIZE];
static uint8_t	output_buffer[BUFFER_SIZE];
static uint32_t	min_address = 0xffffffff;
static uint32_t	max_address = 0x00000000;
static uint32_t	file_size	= 0;

static uint32_t original_data_total = 0;

static uint32_t ascii2bin(FILE *p_input_file, uint32_t length);
static bool_t ReadHex(char *p_filename);
/*******************************************************************/

/**
 * ascii2bin
 *
 * Notes:
 *
 * Parameters:
 *	FILE *p_input_file:
 *	uint32_t length:
 */
static uint32_t ascii2bin(FILE *p_input_file, uint32_t length)
{
	uint8_t	character = 0;
	uint32_t data = 0;
	uint32_t i = 0;

	length *= 2;

	for (i = 0; i < length; i++)
	{
		data <<= 4;
		fread(&character, 1, 1, p_input_file);
		if (character <= '9')
		{
			data |= (character - '0');
		}
		else
		{
			data |= (10 + character - 'A');
		}
	}

	return data;
}
/*******************************************************************/

/**
 * ReadHex
 *
 * Notes: Prepares header from image in the file.
 *		Assistance with the file format here: http://www.keil.com/support/docs/1584/
 *
 * Parameters:
 *	char *filename
 */
static bool_t ReadHex(char *p_filename)
{
	uint32_t length	= 0;
	uint32_t address = 0;
	uint32_t type = 0;
	uint32_t offset = 0;
	uint8_t  character = 0;
	bool_t	 is_end_of_file = false;
	FILE	 *p_input_file = NULL;

	/* Open input file */
	if ((p_input_file = fopen(p_filename,"rb")) == NULL)
	{
		fprintf(stderr,"Unable to open file \"%s\"\n", p_filename);
		return false;
	}

	/* Read the hexfile into memory */
	while (!is_end_of_file)
	{
		fread(&character, 1, 1, p_input_file);

		if (':' == character)
		{
			length	= ascii2bin(p_input_file, 1);
			address = ascii2bin(p_input_file, 2) + offset;
			type 	= ascii2bin(p_input_file, 1);

			switch (type)
			{
				case 0:
					/* Read data */
					while (length--)
					{
						if ((address >= FLASH_START_ADDRESS) && (address <= END_ADDRESS))
						{
							input_buffer[address - FLASH_START_ADDRESS] = (unsigned char)ascii2bin(p_input_file,1);
							if (address < min_address)
							{
								min_address = address;
							}
							else if (address > max_address)
							{
								max_address = address;
							}
							address++;
						}
						else
						{
							/* Close input file */
							fprintf(stderr, "Address in hexfile out of boundaries!\n");
							fclose(p_input_file);
							return false;
						}
					}
					break;

				case 1:
					/* End of file detected */
					is_end_of_file = true;
					break;

				case 4:
					/* New offset */
					offset = ascii2bin(p_input_file, 2) << 16;
					break;

				default:
					/* Read and ignore unsupported types */
					ascii2bin(p_input_file, length);
					break;
			}

			/* Read and ignore checksum */
			ascii2bin(p_input_file, 1);
		}
		else
		{
			// FIX - we get here at the end of an otherwise valid hexfile
			//fclose(p_input_file);	// Close input file
			//fprintf(stderr,"Bad hex file \"%s\"\n", p_filename);
			//return false;
		}
	}

	fclose(p_input_file);	// Close input file

	return true;			// File read OK
}
/*******************************************************************/

void splitpath (char *path, char *drive, char *dir, char *fname,
		 char *ext)
{
	char *dot, *slash;
    if (*path && *(path + 1) == ':')
    {
	*drive = toupper (*path);
	path += 2;
    }
    else
	*drive = 0;

    slash = strrchr (path, '/');
    if (!slash)
	slash = strrchr (path, '/');
    dot = strrchr (path, '.');
    if (dot && slash && dot < slash)
	dot = NULL;

    if (!slash)
    {
	strcpy (dir, "");
	strcpy (fname, path);
        if (dot)
        {
	    *(fname + (dot - path)) = 0;
	    strcpy (ext, dot + 1);
        }
	else
	    strcpy (ext, "");
    }
    else
    {
	strcpy (dir, path);
	if (slash - path == 0)
	    *(dir + 1) = 0;
	else
	    *(dir + (slash - path)) = 0;
	strcpy (fname, slash + 1);
        if (dot)
	{
	    *(fname + (dot - slash) - 1) = 0;
    	    strcpy (ext, dot + 1);
	}
	else
	    strcpy (ext, "");
    }
}

#define PAGE_SIZE 16384
uint32_t application_write(char *appname, FILE *p_output_file, bool_t compressed, bool_t is_bootloader)
{
	uint32_t i = 0;
	md5_state_t pms	= {0};
	aes_context ctx	= {0};
	uint8_t decrypted_digest[MD5_SIZE] = {0};
	uint8_t encrypted_digest[MD5_SIZE] = {0};
	const uint8_t key[] =		{0xB8,0x28,0xD3,0x53,0xE3,0x83,0xBA,0xAD,0x9D,0xF3,0x1B,0x50,0xB1,0x43,0xEA,0x3D};
	const uint8_t appcbckey[] =	{0x12,0xb7,0xb3,0x27,0x12,0x2f,0xc4,0x3a,0xf1,0xcb,0x32,0xa8,0xf2,0x8f,0x69,0x83};
	const uint8_t blcbckey[]		= {0xA9,0xE3,0x2C,0x62,0x6A,0x35,0x47,0xC6,0xC2,0xD1,0x54,0x2F,0xEF,0x92,0xC8,0x08};

	uint32_t header[4]	= {1,2,3,4};		// RDS TODO dummy data for now
	uint32_t file_size = 0;

	//if (unload_image)
	//	compressed = false; // don't bother compressing the unload image

	/* Set a known keyword in the header to enforce that this is a matched build
	 * with a jump point to 0x414000 */


	header[0] = compressed ? BUILD_CONFIRMATION_KEY_COMPRESSED : BUILD_CONFIRMATION_KEY_UNCOMPRESSED;

	aes_set_key(key, 16, &ctx);		// Prepare the AES context
	aes_encrypt((unsigned char*)header, (unsigned char*)header, &ctx);

	if (!unload_image)
	{
		/* Slurp the hex file from the build */
		if (!ReadHex(appname))
		{
			fprintf(stderr,"Failure, binary not produced \n");
			return 0;		// Serious fail, nothing we can do programmatically 
		}

		fprintf(stderr, "Min. address %08X, Max. address %08X\n", min_address, max_address);

		file_size = max_address - min_address + 1;	// Calculate file size
		file_size = ((file_size + 15) / 16) * 16;	// Align to 16 bytes boundary

		original_data_total += file_size;
		/* Shift image so first address is first byte in image */
		memcpy(input_buffer, &input_buffer[min_address - FLASH_START_ADDRESS], file_size);
		memcpy(&input_buffer[SIZE_ADDRESS], &file_size, 4);	// Stamp the size at offset cc
	}
	else
	{
		/* we're just writing a segment of 0xFFFFs */
		fprintf(stderr, "Writing an 'unload' image\n");
		file_size = 0x800;
		original_data_total += 0x800;
		
		memcpy(&input_buffer[SIZE_ADDRESS], &file_size, 4);	// Stamp the size at offset cc
	}


	/* Calculate MD5 over input_buffer */
	md5_init(&pms);
	md5_append(&pms, input_buffer, file_size);
	md5_finish(&pms, decrypted_digest);

	memcpy(&input_buffer[file_size], &decrypted_digest, MD5_SIZE); // Put MD5 in input_buffer after the image
	file_size += MD5_SIZE;							// Increase file_size with the MD5
	file_size = ((file_size + 511) / 512) * 512;	// Align to 512 byte boundary

	if (!compressed)
	{
		/* Encrypt the input_buffer */
		for (i = 0; i < file_size; i += 16)
		{
			aes_encrypt(&input_buffer[i], &output_buffer[i], &ctx);
		}
	}
	else
	{
		uint32_t input_processed = file_size;
		uint32_t transfer;
		uint32_t input_position = 0;
		uint32_t output_position = 0;
		uint32_t compressed_length;
		uint8_t IV[16] = {0};
		uint8_t compressbuffer[2][PAGE_SIZE];
		uint8_t buffer_number = 0;
		LZ4_streamHC_t lz4Stream;
		LZ4_streamHC_t lz4Stream_copy;

		aes_set_key(is_bootloader ? blcbckey : appcbckey, 16, &ctx);		// Prepare the AES context
		LZ4_resetStreamHC(&lz4Stream, 16);

		while (input_processed)
		{
			transfer = input_processed > PAGE_SIZE ? PAGE_SIZE : input_processed;
			/* you need to double buffer as the compression library detects contiguous blocks when compressing, and assumes you'll be doing that when decompressing */
			memcpy(&compressbuffer[buffer_number][0], &input_buffer[input_position], PAGE_SIZE);
			lz4Stream_copy = lz4Stream;
			compressed_length = LZ4_compressHC_continue(&lz4Stream, (const char *)&compressbuffer[buffer_number][0], (char *)&output_buffer[output_position + 4], PAGE_SIZE);		
			if (debug)
			{
				fprintf(stderr, "Page size %08X compressed to %08X (%d%% of original size)\n", PAGE_SIZE, compressed_length, (compressed_length*100)/PAGE_SIZE);
			}
			if (compressed_length >= (PAGE_SIZE-1))
			{
				if (debug)
				{
					printf("Compressed page %08X is larger than %08X\n", compressed_length, PAGE_SIZE);
				}
				compressed_length = PAGE_SIZE;
				memcpy((char *)&output_buffer[output_position + 4], (const char *)&compressbuffer[buffer_number][0], PAGE_SIZE);
				lz4Stream = lz4Stream_copy;
			}
			else
			{
				buffer_number ^=1;
			}
			*(uint32_t *)&output_buffer[output_position] = compressed_length; // write this as offset to the next compressed chunk - the decompressor will know to align past to read the next chunk
			compressed_length = (((compressed_length)+(0x0f))&~(0x0f)); // align to 128 bit boundary to place the next chunk
			aes_cbc_encrypt( &output_buffer[output_position+4], &output_buffer[output_position+4], compressed_length/16, &IV[0], &ctx );
			output_position += (compressed_length + 4);
			input_processed -= transfer;
			input_position += transfer;
			
		}
		*(uint32_t *)&output_buffer[output_position] = 0; // the end
		output_position += 4;
		fprintf(stderr, "Original application size %X, Compressed application size %X (%d%% of original size)\n", file_size, output_position, (output_position*100)/file_size);
		file_size = output_position;
	}

	/* Calculate MD5 over encrypted image (for AppVerifyImage) */
	md5_init(&pms);
	md5_append(&pms, output_buffer, file_size);
	md5_finish(&pms, encrypted_digest);

	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header
	fwrite(encrypted_digest, MD5_SIZE, 1, p_output_file);	// Write the MD5 of the encrypted image
	fwrite(output_buffer, file_size, 1, p_output_file);		// Write the encrypted image
	return file_size+sizeof(header)+MD5_SIZE; // return size of data written including the application header and MD5 (will be wrapped by another header for SFF)
}

/* write the application wrapped for use within a single file format */
uint32_t chunk_application_write(char *appname, FILE *p_output_file)
{
	uint32_t header[2]	= {1,2};
	uint32_t length;
	uint32_t current;
	
	current = ftell(p_output_file); // determine current file position
	header[0] = CHUNK_TYPE_APP;
	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header

	length = application_write(appname, p_output_file, false, false);
	if (length)
	{
		fseek(p_output_file, current, SEEK_SET); // move back to the header
		header[1] = length; // application chunk length
		fwrite(header, sizeof(header), 1, p_output_file);		// Write the header
		fseek(p_output_file, 0, SEEK_END); // move back to the end of the file
		return length + sizeof(header); // length of application data written + the chunk header
	}

	return 0; // there was an error
	
}

/* write the application wrapped for use within a single file format */
uint32_t chunk_application_write_compress_lz4hc(char *appname, FILE *p_output_file)
{
	uint32_t header[2]	= {1,2};
	uint32_t length;
	uint32_t current;
	
	current = ftell(p_output_file); // determine current file position
	header[0] = CHUNK_TYPE_APP_COMPRESSED_LZ4HC;
	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header

	length = application_write(appname, p_output_file, true, false);
	if (length)
	{
		fseek(p_output_file, current, SEEK_SET); // move back to the header
		header[1] = length; // application chunk length
		fwrite(header, sizeof(header), 1, p_output_file);		// Write the header
		fseek(p_output_file, 0, SEEK_END); // move back to the end of the file
		return length + sizeof(header); // length of application data written + the chunk header
	}

	return 0; // there was an error
	
}

/* write the application wrapped for use within a single file format */
uint32_t chunk_bootloader_write_compress_lz4hc(char *appname, FILE *p_output_file)
{
	uint32_t header[2]	= {1,2};
	uint32_t length;
	uint32_t current;
	
	current = ftell(p_output_file); // determine current file position
	header[0] = CHUNK_TYPE_BOOTLOADER_COMPRESSED_LZ4HC;
	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header

	length = application_write(appname, p_output_file, true, true);
	if (length)
	{
		fseek(p_output_file, current, SEEK_SET); // move back to the header
		header[1] = length; // application chunk length
		fwrite(header, sizeof(header), 1, p_output_file);		// Write the header
		fseek(p_output_file, 0, SEEK_END); // move back to the end of the file
		return length + sizeof(header); // length of application data written + the chunk header
	}

	return 0; // there was an error
	
}

uint32_t chunk_raw_file_write(char *filename, FILE *p_output_file, bool_t partial, uint32_t filestart, uint32_t fileend)
{
	uint32_t i = 0;
	uint32_t length = 0;
	FILE *p_input_file;
	uint32_t header[3]	= {1,2,3};

	/* read the file into the big buffer that we have */

	if ((p_input_file = fopen(filename,"rb")) == NULL)
	{
		fprintf(stderr,"Unable to open file \"%s\"\n", filename);
		return 0;
	}

	/* read the input file */
	length = fread(input_buffer, 1, BUFFER_SIZE, p_input_file); 
	
	if (partial)
	{
		length = fileend - filestart;
	}
	else
	{
		filestart = 0;
		fileend = length;
	}

	/* populate the header with file information */

	/* we append if filestart is nonzero - if filestart is 0, we're either an entire file, or the first chunk of several */

	header[0] = filestart ? CHUNK_TYPE_RAW_FILE_APPEND : CHUNK_TYPE_RAW_FILE;
	header[1] = length + 4; // file length + filename field
	i = strlen(filename);
	while (i && toupper(filename[i]) != 'X') // quick means of extracting hex value from leafname
		i--;
	header[2] = strtoul(&filename[i+1], NULL, 16);  // filename read into 32 bit value
	if (debug)
	{
		if (filestart)
			fprintf(stderr,"Appended file 0x%08X from filename %s, bytes %X to %X\n", header[2], filename, filestart, fileend);
		else
			fprintf(stderr,"Added file 0x%08X from filename %s, bytes %X to %X\n", header[2], filename, filestart, fileend);
	}
	
	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header
	fwrite(input_buffer + filestart, length, 1, p_output_file);		// Write the file
	fclose(p_input_file);
	return length + sizeof(header);
}


#ifdef USE_LZO_COMPRESSION
uint32_t chunk_raw_file_write_compress_lzo(char *filename, FILE *p_output_file, bool_t partial, uint32_t filestart, uint32_t fileend)
{
	uint32_t i = 0;
	lzo_uint length = 0;
	FILE *p_input_file;
	uint32_t header[4]	= {1,2,3,4};
	int32_t result;

	lzo_bytep in;
    lzo_bytep out;
    
    lzo_uint in_len;
    lzo_uint out_len;
    lzo_uint new_len;

	/* read the file into the big buffer that we have */

	if ((p_input_file = fopen(filename,"rb")) == NULL)
	{
		fprintf(stderr,"Unable to open file \"%s\"\n", filename);
		return 0;
	}
	/* read the input file, we may re-read this but this can be fixed in future */

	length = fread(input_buffer, 1, BUFFER_SIZE, p_input_file);

	if (partial)
	{
		length = fileend - filestart;
	}
	else
	{
		filestart = 0;
		fileend = length;
	}
	/* check if uncompressed length is too big to decompress in one go */
	if (length > max_uncompressed_data_size)
	{
		uint32_t transfer = length;
		uint32_t fileposition = 0;
		uint32_t running_total = 0;

		fprintf(stderr, "File %s too big to decompress in-situ (%X bytes), compressing split chunks:\n", filename, length);
		while (length)
		{
			transfer = length > max_uncompressed_data_size ? max_uncompressed_data_size : length;
			//fprintf(stderr, "Append\n");
			running_total += chunk_raw_file_write_compress_lzo(filename, p_output_file, true, fileposition, fileposition+transfer);
			length -= transfer;
			fileposition += transfer;
		}
		return running_total;
	}
	/* check if it's valid to compress this data, if not, we'll store uncompressed */
	
    result = lzo1x_999_compress(input_buffer+filestart, length, compressbuffer, &out_len, wrkmem);
    if (result == LZO_E_OK)
        printf("compressed %lu (%X) bytes into %lu (%X) bytes\n",
               (unsigned long) length, (unsigned long) length, (unsigned long) out_len, (unsigned long) out_len);
    else
    {
        /* this should NEVER happen */
        printf("internal error - compression failed: %d\n", result);
        return 2;
    }
    /* check for an incompressible block */
    if (out_len >= length)
    {
        fprintf(stderr, "File %s contains incompressible data\n", filename);
        return chunk_raw_file_write(filename, p_output_file, partial, filestart, fileend);
    }

	/* optimise to speed up decompression */
	 result = lzo1x_optimize(compressbuffer,out_len,input_buffer+filestart,&length,NULL);
	
	/* populate the header with file information */

	/* we append if filestart is nonzero - if filestart is 0, we're either an entire file, or the first chunk of several */

	header[0] = filestart ? CHUNK_TYPE_RAW_FILE_COMPRESSED_LZO_APPEND : CHUNK_TYPE_RAW_FILE_COMPRESSED_LZO;
	header[1] = out_len + 8; // compressed length + original length field + filename field
	header[2] = length; // original file length
	i = strlen(filename);
	while (i && toupper(filename[i]) != 'X') // quick means of extracting hex value from leafname
		i--;
	header[3] = strtoul(&filename[i+1], NULL, 16);  // filename read into 32 bit value
	if (debug)
	{
		if (filestart)
			fprintf(stderr,"Appended file 0x%08X from filename %s, compressed bytes %X\n", header[3], filename, out_len);
		else
			fprintf(stderr,"Added file 0x%08X from filename %s, compressed bytes %X\n", header[3], filename, out_len);
	}

	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header

	fwrite(compressbuffer, out_len, 1, p_output_file);		// Write the file
	fclose(p_input_file);
	return out_len + sizeof(header);
}
#endif

uint32_t chunk_raw_file_write_compress_lz4hc(char *filename, FILE *p_output_file, bool_t partial, uint32_t filestart, uint32_t fileend)
{
	uint32_t i = 0;
	uint32_t length = 0;
	FILE *p_input_file;
	uint32_t header[4]	= {1,2,3,4};
    
    uint32_t out_len;

	static LZ4_streamHC_t lz4Stream_body_copy;
	static LZ4_streamHC_t lz4Stream_body; // needs to be retained when recursing
    LZ4_streamHC_t* lz4Stream = &lz4Stream_body;

    char inpBuf[2][0x20000]; // holds uncompressed data which will not be bigger than this
    static int  inpBufIndex = 0;
    
	if (!partial)
	{
		LZ4_resetStreamHC(lz4Stream, 16); // only reset for a new file, not for a partial continuation
		inpBufIndex = 0;
	}

	/* read the file into the big buffer that we have */

	if ((p_input_file = fopen(filename,"rb")) == NULL)
	{
		fprintf(stderr,"Unable to open file \"%s\"\n", filename);
		return 0;
	}
	/* read the input file, we may re-read this but this can be fixed in future */

	length = fread(input_buffer, 1, BUFFER_SIZE, p_input_file);

	if (partial)
	{
		length = fileend - filestart;
	}
	else
	{
		filestart = 0;
		fileend = length;
	}
	/* check if uncompressed length is too big to decompress in one go */
	if (length > max_uncompressed_data_size)
	{
		uint32_t transfer = length;
		uint32_t fileposition = 0;
		uint32_t running_total = 0;
		if (debug)
		{
			fprintf(stderr, "File %s too big to decompress in-situ (%X bytes), compressing split chunks:\n", filename, length);
		}
		while (length)
		{
			transfer = length > max_uncompressed_data_size ? max_uncompressed_data_size : length;
			//fprintf(stderr, "Append\n");
			running_total += chunk_raw_file_write_compress_lz4hc(filename, p_output_file, true, fileposition, fileposition+transfer);
			length -= transfer;
			fileposition += transfer;
		}
		return running_total;
	}
	/* check if it's valid to compress this data, if not, we'll store uncompressed */
	lz4Stream_body_copy = lz4Stream_body; // copy the state, we only want to maintain the running state of compressed blocks we keep	
	memcpy(&inpBuf[inpBufIndex][0], input_buffer+filestart, length); // copy the uncompressed data into the double buffer
	out_len = LZ4_compressHC_continue(lz4Stream, (const char *)&inpBuf[inpBufIndex][0], (char *)compressbuffer, length);
    
	if(out_len <= 0)
	{
		printf("internal error - compression failed: %d\n", out_len);
        return 2;
	}

    /* check for an incompressible block */
    if (out_len >= length)
    {
        //fprintf(stderr, "File %s contains incompressible data\n", filename);
		/* throw away the state change/return to the old state for this block, as we're not keeping it */
		lz4Stream_body = lz4Stream_body_copy;
        return chunk_raw_file_write(filename, p_output_file, partial, filestart, fileend);
    }
	else
	{
		// we keep the state, and flip the double buffer
		inpBufIndex ^= 1;
	}
	
	/* populate the header with file information */

	/* we append if filestart is nonzero - if filestart is 0, we're either an entire file, or the first chunk of several */

	header[0] = filestart ? CHUNK_TYPE_RAW_FILE_COMPRESSED_LZ4HC_APPEND : CHUNK_TYPE_RAW_FILE_COMPRESSED_LZ4HC;
	header[1] = out_len + 8; // compressed length + original length field + filename field
	header[2] = length; // original file length
	i = strlen(filename);
	while (i && toupper(filename[i]) != 'X') // quick means of extracting hex value from leafname
		i--;
	header[3] = strtoul(&filename[i+1], NULL, 16);  // filename read into 32 bit value
	if (debug)
	{
		if (filestart)
			fprintf(stderr,"Appended file 0x%08X from filename %s, compressed bytes %X\n", header[3], filename, out_len);
		else
			fprintf(stderr,"Added file 0x%08X from filename %s, compressed bytes %X\n", header[3], filename, out_len);
	}

	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header

	fwrite(compressbuffer, out_len, 1, p_output_file);		// Write the file
	fclose(p_input_file);
	return out_len + sizeof(header);
}

/**
 * main
 *
 * Notes:
 *
 * Parameters:
 *
 */

#define NUM_CHUNKS_OFFSET 2
#define FILE_SIZE_OFFSET 3

void RunningTotal(char *filename) // simple running total, file will be checked by the functions which actually use the files
{
	FILE *p_input_file;

	if ((p_input_file = fopen(filename,"rb")) != NULL)
	{
		fseek (p_input_file, 0, SEEK_END);
		original_data_total += ftell(p_input_file);
		fclose(p_input_file);
	}
}


int main(int argc, char **argv)
{
	char syntax[]="Usage: %s -sff [{-application | -compressapplication} <file>] [-file <file>] <file> ... [{-directory | -compressdirectory} <directory>] -outputfile <filename> [-compressiontype <type>] [-unloadimage] [-excludeupdateimage] [-debug] [-samd21image <inputfile> <outputimage>] [-samd10image <inputfile> <outputimage>]\n\n-samd21image can also be used without any other features\n\n-samd10image can also be used without any other features\n\nDeprecated usage: %s <infile.hex> <outfile.bin> <product ID>\n\nEmail gareth.long@tomtom.com for more information\n";
	FILE *p_input_file = NULL;
	FILE *p_output_file	= NULL;

	bool_t found = false; // optional parameters found flags
	bool_t mandatoryparams = true; // specifies that the default flow of input files/output image is needed. Not needed in cases like -samd21image only
	
	uint32_t i = 0;
	uint32_t single_file = 0;
	uint32_t first_file = 0; // position in command line of first command/filename
	uint32_t last_file = 0;  // position in command line of last command/filename
	uint32_t output_file = 0; // position in command line of output filename
	uint32_t current_file = 0; // current command/filename being processed

	uint32_t chunks = 0;
	uint32_t file_size = 0;

	uint32_t header[4]	=	{
								('T' << 24 ) | ('T' << 16) | ('S' << 8) | ('F' << 0), // header
								0, // version
								0x55AA55AA, // number of chunks (write back later)
								0x77777777 // file size (write back later)
							};

	

    if (argc < 0 && argv == NULL)   /* avoid warning about unused args */
        return 0;

	fprintf(stderr, "CreateImage version "CREATEIMAGE_VERSION" built "__DATE__" "__TIME__"\n", argv[0]);

#ifdef USE_LZO_COMPRESSION
	if (lzo_init() != LZO_E_OK)
    {
        fprintf(stderr, "internal error - lzo_init() failed !!!\n");
        return EXIT_FAILURE;
    }
#endif



	/* We want to compress the data block at 'in' with length 'IN_LEN' to
	 * the block at 'out'. Because the input block may be incompressible,
	 * we must provide a little more output space in case that compression
	 * is not possible.
	 */
#ifdef USE_LZO_COMPRESSION
    wrkmem = (lzo_voidp) malloc(LZO1X_999_MEM_COMPRESS);
    if (wrkmem == NULL)
    {
        printf("LZO out of memory\n");
        return EXIT_FAILURE;
    }
#endif
	memset(input_buffer, 0xFF, sizeof(input_buffer));

	/* recursively check the optional end parameters first, before working with the mandatory ones */
	do
	{
		found = false; // we will keep looping for optional parameters, in any order
		if (argc > 0 && stricmp(argv[argc-1],"-debug") == 0)
		{	
			debug = true;
			argc -= 1; /* stop the rest of the processing looking at these arguments */
			found = true;
		}

		if (argc > 0 && stricmp(argv[argc-1],"-unloadimage") == 0)
		{	
			unload_image = true;
			argc -= 1; /* stop the rest of the processing looking at these arguments */
			found = true;
		}

		if (argc > 2 && stricmp(argv[argc-3],"-samd21image") == 0)
		{
			uint32_t header[4]	= {('T' << 24 ) | ('T' << 16) | ('C' << 8) | ('0' << 0), 1, 0, 0 };
			
			if ((p_input_file = fopen(argv[argc-2],"rb")) == NULL)
			{
				fprintf(stderr,"Unable to open SAMD21 raw binary file \"%s\"\n", argv[argc-2]);
				return -1;
			}

			if ((p_output_file = fopen(argv[argc-1],"wb")) == NULL)
			{
				fprintf(stderr,"Unable to create SAMD21 image file \"%s\"\n",argv[argc-1]);
				return -1;
			}
			/* read in the SAMD21 file - we have an image buffer way big enough */

			header[2] = fread(input_buffer, 1, BUFFER_SIZE, p_input_file); 
			header[3] = crc32buf(input_buffer, header[2]);

			/* write the header with checksum */
			fwrite(header, sizeof(header), 1, p_output_file);
			/* write the rest of the image */
			fwrite(input_buffer, header[2], 1, p_output_file);
			fclose(p_input_file);
			fclose(p_output_file);

			fprintf(stderr, "SAMD21 image generation successful, file size = %08X, CRC = %08X\n", header[2], header[3]);

			argc -= 3; /* stop the rest of the processing looking at these arguments */
			mandatoryparams = false; // we had another purpose
			found = true;
		}

		if (argc > 2 && stricmp(argv[argc-3],"-samd10image") == 0)
		{
			uint32_t header[4]	= {('T' << 24 ) | ('T' << 16) | ('C' << 8) | ('0' << 0), 1, 0, 0 };
			
			if ((p_input_file = fopen(argv[argc-2],"rb")) == NULL)
			{
				fprintf(stderr,"Unable to open SAMD10 raw binary file \"%s\"\n", argv[argc-2]);
				return -1;
			}

			if ((p_output_file = fopen(argv[argc-1],"wb")) == NULL)
			{
				fprintf(stderr,"Unable to create SAMD10 image file \"%s\"\n",argv[argc-1]);
				return -1;
			}
			/* read in the SAMD21 file - we have an image buffer way big enough */

			header[2] = fread(input_buffer, 1, BUFFER_SIZE, p_input_file); 
			header[3] = crc32buf(input_buffer, header[2]);

			/* write the header with checksum */
			fwrite(header, sizeof(header), 1, p_output_file);
			/* write the rest of the image */
			fwrite(input_buffer, header[2], 1, p_output_file);
			fclose(p_input_file);
			fclose(p_output_file);

			fprintf(stderr, "SAMD10 image generation successful, file size = %08X, CRC = %08X\n", header[2], header[3]);

			argc -= 3; /* stop the rest of the processing looking at these arguments */
			mandatoryparams = false; // we had another purpose
			found = true;
		}

		if (argc > 1 && stricmp(argv[argc-2],"-compressiontype") == 0)
		{	
			if (stricmp(argv[argc-1],"LZ4HC") == 0)
				compression_type = COMPRESSION_TYPE_LZ4HC;
#ifdef USE_LZO_COMPRESSION
			else if (stricmp(argv[argc-1],"LZO") == 0)
				compression_type = COMPRESSION_TYPE_LZO;
#endif
			else fprintf(stderr, "Unknown compression type %s - using default\n", argv[argc-1]);

			argc -= 2; /* stop the rest of the processing looking at these arguments */
			found = true;
		}

		if (argc > 0 && stricmp(argv[argc-1],"-excludeupdatefile") == 0)
		{	
			exclude_updatefile = true;
			argc -= 1; /* stop the rest of the processing looking at these arguments */
			found = true;
		}

#ifdef GENERATE_BOOT_UNLOCK_CODE

		/* ... gone ... */

#endif

		if (argc > 0 && stricmp(argv[argc-1],"-extra1") == 0)
		{		
			argc -= 1; /* stop the rest of the processing looking at these arguments */
			found = true;
		}

	}
	while (found); // recurse while we still find optional parameters
	
	/* set up the flash buffers for the image generation */
	memset(input_buffer, 0xFF, BUFFER_SIZE);
	memset(output_buffer, 0xFF, BUFFER_SIZE);

	if (argc > 1 && stricmp(argv[1], "-sff") == 0)  /* single file format */
	{
		/* now check for the mandatory command for single file generation, IE -outputfile <filename> */
		if (stricmp(argv[argc-2],"-outputfile") == 0)
		{		
			last_file = argc - 3;
			output_file = argc - 1;
			argc -= 2; /* stop the rest of the processing looking at these arguments */
		}
		else
		{
			fprintf(stderr, syntax, argv[0], argv[0]);
			return -1;
		}

		single_file = 1;
		first_file = 2;
	}
	else
	{
		/* old style - check arguments */
		if (argc != 4 && mandatoryparams)
		{
			fprintf(stderr, syntax, argv[0], argv[0]);
			return -1;
		}

		if (!mandatoryparams)
		{
			return 0; // we can exit cleanly, we fulfilled a purpose
		}
		single_file = 0;
		first_file = 1;
		last_file = 1; // not used in this case
		output_file = 2;
	}

		/* Create output file */
	if ((p_output_file = fopen(argv[output_file],"wb")) == NULL)
	{
		fprintf(stderr,"Unable to create file \"%s\"\n",argv[output_file]);
		return -1;
	}

	if (!single_file)
	{
		/* old style, just write the application file to a raw image and leave */
		application_write(argv[first_file], p_output_file, false, false);
		fclose(p_output_file);									// Close, we're finished
		fprintf(stderr, "Old-style image generation succeeded\n");
	}

	/* new style from here on in */

	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header
	file_size = sizeof(header);
	current_file = first_file;

	while (current_file <= last_file)
	{
		uint32_t chunklength;
		/* first check for commands, functions will write the chunk type and length, and return the total bytes written */
		if (stricmp(argv[current_file], "-application") == 0)
		{
			chunklength =  chunk_application_write(argv[current_file+1], p_output_file);
			file_size += chunklength;
			if (chunklength == 0)
				return EXIT_FAILURE;
			current_file += 2;
			chunks += 1;
		}
		else if (stricmp(argv[current_file], "-compressapplication") == 0)
		{
			chunklength =  chunk_application_write_compress_lz4hc(argv[current_file+1], p_output_file);
			file_size += chunklength;
			if (chunklength == 0)
				return EXIT_FAILURE;
			current_file += 2;
			chunks += 1;
		}
		else if (stricmp(argv[current_file], "-compressbootloader") == 0)
		{
			chunklength =  chunk_bootloader_write_compress_lz4hc(argv[current_file+1], p_output_file);
			file_size += chunklength;
			if (chunklength == 0)
				return EXIT_FAILURE;
			current_file += 2;
			chunks += 1;
		}
		else if (stricmp(argv[current_file], "-file") == 0) // we are a specific filename, add as a raw file chunk
		{
			RunningTotal(argv[current_file+1]);
			chunklength = chunk_raw_file_write(argv[current_file+1], p_output_file, 0, 0, 0);
			file_size += chunklength;
			if (chunklength == 0)
				return EXIT_FAILURE;
			current_file += 2;
			chunks += 1;
		}
		else if (stricmp(argv[current_file], "-compressfile") == 0) // we are a specific filename, add as a raw file chunk
		{
			RunningTotal(argv[current_file+1]);
#ifdef USE_LZO_COMPRESSION
			if (compression_type == COMPRESSION_TYPE_LZO)
				chunklength = chunk_raw_file_write_compress_lzo(argv[current_file+1], p_output_file, 0, 0, 0);
			else
#endif
				chunklength =  chunk_raw_file_write_compress_lz4hc(argv[current_file+1], p_output_file, 0, 0, 0);
			file_size += chunklength;
			if (chunklength == 0)
				return EXIT_FAILURE;
			current_file += 2;
			chunks += 1;
		}
		else if (stricmp(argv[current_file], "-directory") == 0) // we are a directory, let's add every file
		{
			DIR *dir;
			struct dirent *ent;
			if ((dir = opendir(argv[current_file+1])) != NULL)
			{		
				while ((ent = readdir(dir)) != NULL)
				{
					if (!(exclude_updatefile && stricmp(ent->d_name, "0x000000F0") == 0)) // disallow if exclode_updatefile set and file is an update file
					{
						if (ent->d_name[0] != '.' ) // don't wan't '.' and '..' directory entries
						{
							strcpy(path, argv[current_file+1]);
							strcat(path, "\\");
							strcat(path, ent->d_name);
							if (debug)
								fprintf(stderr,"Add \"%s\" from directory\n", path);
							RunningTotal(path);
							file_size += chunk_raw_file_write(path, p_output_file, 0, 0, 0);
							chunks += 1;
						}
					}
				}
				closedir (dir);
			}
			else
			{
				/* could not open directory */
				fprintf(stderr,"Unable to open directory \"%s\"\n",argv[current_file+1]);
				return EXIT_FAILURE;
			}
			current_file += 2;
		}
		else if (stricmp(argv[current_file], "-compressdirectory") == 0) // we are a directory, let's add every file
		{
			DIR *dir;
			struct dirent *ent;
			if ((dir = opendir(argv[current_file+1])) != NULL)
			{		
				while ((ent = readdir(dir)) != NULL)
				{
					if (!(exclude_updatefile && stricmp(ent->d_name, "0x000000F0") == 0)) // disallow if exclode_updatefile set and file is an update file
					{
						if (ent->d_name[0] != '.' ) // don't wan't '.' and '..' directory entries
						{
							strcpy(path, argv[current_file+1]);
							strcat(path, "\\");
							strcat(path, ent->d_name);
							if (debug)
								fprintf(stderr,"Compress and add \"%s\" from directory\n", path);
							RunningTotal(path);
#ifdef USE_LZO_COMPRESSION
							if (compression_type == COMPRESSION_TYPE_LZO)
								file_size += chunk_raw_file_write_compress_lzo(path, p_output_file, 0, 0, 0);
							else
#endif
								file_size += chunk_raw_file_write_compress_lz4hc(path, p_output_file, 0, 0, 0);
							chunks += 1;
						}
					}
				}
				closedir (dir);
			}
			else
			{
				/* could not open directory */
				fprintf(stderr,"Unable to open directory \"%s\"\n",argv[current_file+1]);
				return EXIT_FAILURE;
			}
			current_file += 2;
		}
		else // we must be a direct file name, so add it as a raw file chunk 
		{
			file_size += chunk_raw_file_write(argv[current_file+1], p_output_file, 0, 0, 0);
			RunningTotal(argv[current_file+1]);
			current_file ++;
			chunks += 1;
		}

	}



	/* finish up the output file */

	header[NUM_CHUNKS_OFFSET] = chunks;
	header[FILE_SIZE_OFFSET] = file_size;
	fseek(p_output_file, 0, SEEK_SET);
	fwrite(header, sizeof(header), 1, p_output_file);		// Write the header again - with number of chunks, file size filled in

	fclose(p_output_file);									// Close, we're finished

	fprintf(stderr, "SFF image generation succeeded\n");
	if (chunks > 1)
		fprintf(stderr,"SFF contains %d chunks, file size = %08X\n", chunks, file_size);
	else
		fprintf(stderr,"SFF contains %d chunk, file size = %08X\n", chunks, file_size);
	fprintf(stderr, "%d input bytes processed, %d bytes produced (%d%% of original size)\n", original_data_total, file_size, (file_size*100)/original_data_total);
	//getchar();
	return 0;
}
/*******************************************************************/
