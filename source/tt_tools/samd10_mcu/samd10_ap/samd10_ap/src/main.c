/**
 * \file
 *
 * \brief Getting Started Application.
 *
 * Copyright (c) 2014-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>

uint16_t adc_read_voltage(const enum adc_positive_input adc_input);

void i2c_read_request_callback(
		struct i2c_slave_module *const module);
void i2c_write_request_callback(
		struct i2c_slave_module *const module);
void configure_i2c_slave(void);
void configure_i2c_slave_callbacks(void);

void configure_adc(void);

static struct i2c_slave_packet packet;

#define DATA_LENGTH 10

static uint8_t read_buffer [DATA_LENGTH];

/* Address of the I2C lave */
#define SLAVE_ADDRESS	0x12

/* ADC */
#define ADC_INT1V	1000
#define ADC_RATIO	43	/* R801: 3.3M_1%, R804: 1M_1% */
#define ADC_NOFP	10	/* Don't use float point to save code size */
#define ADC_MAX		0x0FFF

/* Init device instance */
struct i2c_slave_module i2c_slave_instance;
struct adc_module adc_instance;

uint16_t adc_read_voltage(const enum adc_positive_input adc_input)
{
	uint16_t result;

	adc_set_positive_input(&adc_instance, adc_input);
	adc_start_conversion(&adc_instance);

	do {
		/* Wait for conversion to be done and read out result */
	} while (adc_read(&adc_instance, &result) == STATUS_BUSY);

	return (result * ADC_INT1V / ADC_MAX);
}

void i2c_read_request_callback(
		struct i2c_slave_module *const module)
{
	uint16_t value[5];

	/* Work-around: Toggle interrupt pin */
	port_pin_toggle_output_level(PIN_PA14);
	port_pin_toggle_output_level(PIN_PA15);

	/* Work-around: Avoid PA03 ADC issue. */
	value[0] = adc_read_voltage(ADC_POSITIVE_INPUT_PIN1);
	value[0] = adc_read_voltage(ADC_POSITIVE_INPUT_PIN1);
	value[1] = (adc_read_voltage(ADC_POSITIVE_INPUT_PIN2) * ADC_RATIO / ADC_NOFP);
	value[3] = adc_read_voltage(ADC_POSITIVE_INPUT_PIN9);
	if (value[2] == ADC_MAX)
	{
		value[2] = adc_read_voltage(ADC_POSITIVE_INPUT_PIN3);
	}
	else
	{
		port_pin_set_output_level(PIN_PA07, true);
		value[2] = adc_read_voltage(ADC_POSITIVE_INPUT_PIN3);
		port_pin_set_output_level(PIN_PA07, false);
	}

	value[4] = 0x0;
	if (port_group_get_input_level(&(PORT->Group[PIN_PA11/ 32]), (1U << (PIN_PA11 & 0x1F))))
		value[4] |= 0x1;
	if (port_group_get_input_level(&(PORT->Group[PIN_PA16/ 32]), (1U << (PIN_PA16 & 0x1F))))
		value[4] |= (0x1 << 1);
	if (port_group_get_input_level(&(PORT->Group[PIN_PA17/ 32]), (1U << (PIN_PA17 & 0x1F))))
		value[4] |= (0x1 << 2);
	if (port_group_get_input_level(&(PORT->Group[PIN_PA24/ 32]), (1U << (PIN_PA24 & 0x1F))))
		value[4] |= (0x1 << 3);
	if (port_group_get_input_level(&(PORT->Group[PIN_PA30/ 32]), (1U << (PIN_PA30 & 0x1F))))
		value[4] |= (0x1 << 4);
	if (port_group_get_input_level(&(PORT->Group[PIN_PA31/ 32]), (1U << (PIN_PA31 & 0x1F))))
		value[4] |= (0x1 << 5);

	/* Init i2c packet */
	packet.data_length = 10;
	packet.data        = (uint8_t *)&value[0];

	/* Write buffer to master */
	i2c_slave_write_packet_job(module, &packet);
}

void i2c_write_request_callback(
		struct i2c_slave_module *const module)
{
	/* Init i2c packet */
	packet.data_length = 1;
	packet.data        = read_buffer;

	/* Read buffer from master */
	if (i2c_slave_read_packet_job(module, &packet) != STATUS_OK) {
	}
}

void configure_i2c_slave(void)
{
	/* Initialize config structure and module instance */
	struct i2c_slave_config config_i2c_slave;
	i2c_slave_get_config_defaults(&config_i2c_slave);
	/* Change address and address_mode */
	config_i2c_slave.address      = SLAVE_ADDRESS;
	config_i2c_slave.address_mode = I2C_SLAVE_ADDRESS_MODE_MASK;
	/* Initialize and enable device with config */
	i2c_slave_init(&i2c_slave_instance, CONF_I2C_SLAVE_MODULE, &config_i2c_slave);

	i2c_slave_enable(&i2c_slave_instance);
}

void configure_i2c_slave_callbacks(void)
{
	/* Register and enable callback functions */
	i2c_slave_register_callback(&i2c_slave_instance, i2c_read_request_callback,
			I2C_SLAVE_CALLBACK_READ_COMPLETE);
	i2c_slave_enable_callback(&i2c_slave_instance,
			I2C_SLAVE_CALLBACK_READ_COMPLETE);

	i2c_slave_register_callback(&i2c_slave_instance, i2c_write_request_callback,
			I2C_SLAVE_CALLBACK_WRITE_REQUEST);
	i2c_slave_enable_callback(&i2c_slave_instance,
			I2C_SLAVE_CALLBACK_WRITE_REQUEST);
}

void configure_adc(void)
{
	struct adc_config config_adc;

	adc_get_config_defaults(&config_adc);

	config_adc.reference = ADC_REFERENCE_INT1V;
	config_adc.positive_input = ADC_POSITIVE_INPUT_PIN1;

#if (SAMC21)
	adc_init(&adc_instance, ADC1, &config_adc);
#else
	adc_init(&adc_instance, ADC, &config_adc);
#endif

	adc_enable(&adc_instance);
}

int main(void)
{
	struct port_config pin_conf;

	system_init();

	/* Configure device and enable */
	configure_i2c_slave();
	configure_i2c_slave_callbacks();

	configure_adc();

	while (true) {
		/* Infinite loop while waiting for I2C master interaction */
	}
}
