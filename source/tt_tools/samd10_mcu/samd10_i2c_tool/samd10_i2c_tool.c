/*

*/

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "linux/i2c-dev.h"
#include "i2cbusses.h"
#include "samd10_i2c_tool.h"

static void usage(char *self)
{
    printf("usage: %s [ap | bl] <i2c_bus> <address> <filename>\n", self);
    exit(-1);
}

int main(int argc, char *argv[])
{
    int i2cbus = SAMD10_DEFAULT_I2C_BUS, i2caddr = SAMD10_DEFAULT_I2C_ADDRESS;
    int ret, ap_mode = 0;
    int file, ack;
    int remaining_len, row_buf_size = SAMD10_PAGE_SIZE;
    char image[64];
    char filename[20];
    unsigned short rec_val[4];
    unsigned int header[4], checksum = 0;
    unsigned char buf[SAMD10_PAGE_SIZE];
    long image_length = 0;
    FILE *fd;

    if(argc < 4)
    {
        usage(argv[0]);
        exit(1);
    } else {
        if (strcmp(argv[1], "ap") == 0)
        {
            ap_mode = 1;
            i2cbus = (int)atoi(argv[2]);
            i2caddr = strtol(argv[3], NULL, 16);
        }
        else
        {
            if(argc < 5)
            {
                usage(argv[0]);
                exit(1);
            }
            i2cbus = (int)atoi(argv[2]);
            i2caddr = strtol(argv[3], NULL, 16);
            strcpy(image,argv[4]);
        }
    }

    /* Init I2C */
    file = open_i2c_dev(i2cbus, filename, sizeof(filename), 0);
    if (file < 0) {
        printf( "Open I2C failure\n" );
        exit(1);
    }
    set_slave_addr(file, i2caddr, 1);

    if (ap_mode)
    {
        i2c_smbus_read_i2c_block_data(file, 0x0, 8, (unsigned char *)&rec_val[0]);

        printf("PA03: %d mV\n", rec_val[0]);
        printf("PA04: %d mV\n", rec_val[1]);
        printf("PA05: %d mV\n", rec_val[2]);
        printf("PORT: ");
        if (rec_val[3] & 0x01)
            printf("PA11: high, ");
        else
            printf("PA11: low , ");
        if (rec_val[3] & (0x01 << 1))
            printf("PA16: high, ");
        else
            printf("PA16: low , ");
        if (rec_val[3] & (0x01 << 2))
            printf("PA17: high");
        else
            printf("PA17: low");
        if (rec_val[3] & (0x01 << 3))
            printf("\n      PA24: high, ");
        else
            printf("\n      PA24: low , ");
        if (rec_val[3] & (0x01 << 4))
            printf("PA30: high, ");
        else
            printf("PA30: low , ");
        if (rec_val[3] & (0x01 << 5))
            printf("PA31: high\n");
        else
            printf("PA31: low\n");
    }
    else
    {
        /* Get image */
        fd = fopen( image,"rb" );
        if( NULL == fd ){
            printf( "Open image failure\n" );
            exit(1);
        } else {
            fseek(fd, 0, SEEK_END);
            image_length = ftell(fd);
            fseek(fd, 0L, SEEK_SET);
        }

        /* Check header */
        if (!fread(header, SAMD10_IMAGE_HEADER_LENGTH, 1, fd)) {
            fclose(fd);
            printf( "Invalid image header\n" );
            exit(1);
        }

        /*  check header tag and image size field */
        if (SAMD10_HEADER_TAG != header[0] || (image_length - SAMD10_IMAGE_HEADER_LENGTH) != header[2])
        {
            fclose(fd);
            printf( "Invalid image header\n" );
            exit(1);
        }
        checksum = header[3];
        image_length -= SAMD10_IMAGE_HEADER_LENGTH;
    
        buf[0] = (unsigned char)(image_length>>24);
        buf[1] = (unsigned char)(image_length>>16);
        buf[2] = (unsigned char)(image_length>>8);
        buf[3] = (unsigned char)(image_length);
        ret = i2c_smbus_write_i2c_block_data(file, buf[0], 3,  (unsigned char*)&buf[1]);
        if (ret < 0) {
            printf("Write i2c failed: file length\n");
            exit(1);
        }

        buf[0] = (unsigned char)(checksum>>24);
        buf[1] = (unsigned char)(checksum>>16);
        buf[2] = (unsigned char)(checksum>>8);
        buf[3] = (unsigned char)(checksum);
        ret = i2c_smbus_write_i2c_block_data(file, buf[0], 3,  (unsigned char*)&buf[1]);
        if (ret < 0) {
            printf("Write i2c failed: md5sum\n");
            exit(1);
        }
        ack = i2c_smbus_read_byte(file);
        if (ack == SAMD10_I2C_RETURN_NO_UPDATE) {
            printf("No update needed\n");
            fclose(fd);
            exit(0);
        }

        /* DATA */
        remaining_len = image_length;
        while ((remaining_len > 0) && (ack != SAMD10_I2C_RETURN_ERROR)) {
            if (row_buf_size > remaining_len)
                row_buf_size = remaining_len;

            ret = fread(buf, row_buf_size, 1, fd);
            write(file, buf, row_buf_size);
            usleep(SAMD10_I2C_DELAY);
            ack = i2c_smbus_read_byte(file);

            remaining_len -= row_buf_size;
        }

        fclose(fd);
        if (ack == SAMD10_I2C_RETURN_SUCCESS) {
            printf("SAMD10 update Success\n");
            exit(0);
        }
        else {
            printf("SAMD10 update FAIL\n");
            exit(1);
        }
    }
}
