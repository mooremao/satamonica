/* 
 * Description:
 *
 * Author/s:
 *
 * Filename: samd10_bl_tool.h
 *
 * Dependencies:
 *
 * Copyright (c) 2015 TomTom N.V.
 */
 
#ifndef _SAMD10_BL_TOOL_H
#define _SAMD10_BL_TOOL_H

/* I2C */
#define SAMD10_DEFAULT_I2C_BUS			4
#define SAMD10_DEFAULT_I2C_ADDRESS		0x15
#define SAMD10_I2C_DELAY			1000	/* 1 ms */

/* SAMD10 image update */
#define SAMD10_PAGE_SIZE			64
#define SAMD10_HEADER_TAG 			(('T' << 24 ) | ('T' << 16) | ('C' << 8) | (	'0' << 0))
#define SAMD10_IMAGE_HEADER_LENGTH 		16

/* Return code */
#define SAMD10_I2C_RETURN_ACK			's'
#define SAMD10_I2C_RETURN_NO_UPDATE		'n'
#define SAMD10_I2C_RETURN_ERROR			'e'
#define SAMD10_I2C_RETURN_SUCCESS		'g'

#endif
