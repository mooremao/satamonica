#!/bin/sh
TTCI_PATH="/mnt/ttci/bin/"
SAMD10_TOOL="samd10_i2c_tool"
IMAGE="samd10_ap.img"

I2C0="1"
I2C1="2"
I2C2="3"
I2C3="4"

I2C_SAMD10_APP_ID="0x12"
I2C_SAMD10_BL_ID="0x15"

if [ "$1" != "" ]; then
    IMAGE=$1
fi

# Init
echo 108 > /sys/class/gpio/export
echo 81 > /sys/class/gpio/export 
echo out > /sys/class/gpio/gpio81/direction
echo out > /sys/class/gpio/gpio108/direction

# Enter Bootloader Mode
echo 0 > /sys/class/gpio/gpio81/value
echo 1 > /sys/class/gpio/gpio108/value
sleep 0.5
echo 0 > /sys/class/gpio/gpio108/value

${TTCI_PATH}${SAMD10_TOOL} bl ${I2C2} ${I2C_SAMD10_BL_ID} $IMAGE
