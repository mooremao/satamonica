#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>

#include <libfdt.h>
#include "ttv.h"

#define INFILE	"/dev/fdtexport"

struct file_info {
	int fd;
	off_t size;
};

static int fdtopen(struct file_info *fi, unsigned char **buf)
{
	int retval = 0;

	fi->fd = open(INFILE, O_RDONLY | O_CLOEXEC);
	if (fi->fd < 0) {
		fprintf(stderr, "Can't open input '%s':%s\n",
				INFILE, strerror(errno));
		retval = -errno;
		goto out;
	}

	fi->size = lseek(fi->fd, 0, SEEK_END);
	if (fi->size < 0) {
		fprintf(stderr, "Can't lseek '%s': %s\n", INFILE,
					strerror(errno));
		retval = -errno;
		goto outclose;
	}

	*buf = (unsigned char *)(mmap(0, (size_t)fi->size, PROT_READ,
			MAP_SHARED, fi->fd, 0));
	if (MAP_FAILED == *buf) {
		fprintf(stderr, "Can't mmap '%s': %s\n", INFILE,
					strerror(errno));
		retval = -errno;
		goto outclose;
	}
	return retval;

outclose:
	close(fi->fd);
out:
	return retval;
}

static void fdtclose(struct file_info *fi, unsigned char **buf)
{
	if (MAP_FAILED != *buf) {
		if (munmap(*buf, fi->size) < 0)
			perror("Can't unmap file\n");
	}
	if(fi->fd)
		close(fi->fd);
}

int ttv_get_device_serial(char *serial, int strsize)
{
	struct file_info fdt_fi;
	unsigned char *fdtbuf = MAP_FAILED;
	int offset, len, retval;

	/* Load binary in memory */
	retval = fdtopen(&fdt_fi, &fdtbuf);
	if (retval) {
		fprintf(stderr, "Error (%d) while opening the fdt file\n", retval);
		return retval;
	}

	offset = fdt_path_offset(fdtbuf, "/features/");
	if (offset < 0) {
		fprintf(stderr, "Could not find offset while opening the fdt file\n");
		retval = -EINVAL;
	} else {
		/* Get the property */
		const void *prop = NULL;
		prop = fdt_getprop(fdtbuf, offset, "device-serial", &len);

		if (len > strsize) {
			fprintf(stderr, "Input size (%d) is less than the property size (%d)\n", strsize, len);
			retval = -EMSGSIZE;
		} else if (prop)
			strncpy(serial, (char *)prop, len);
	}

	fdtclose(&fdt_fi, &fdtbuf);
	return retval;
}
