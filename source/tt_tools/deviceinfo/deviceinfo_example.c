#include <stdio.h>
#include <stdlib.h>
#include "ttv.h"

#define MAX_LEN 128
void main(int argc, char **argv)
{
	char device[MAX_LEN];

	if (argc < 2) {
		printf("usage: deviceinfo_example <feature>\n");
		return;
	}

	if (!strcmp(argv[1], "device-id")) {
		if (!ttv_get_device_serial(device, MAX_LEN))
			printf("%s\n", device);
	} else
		printf("Nothing to find\n");
}
