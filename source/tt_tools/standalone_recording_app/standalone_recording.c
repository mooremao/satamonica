// Standalone application which saves only first frame
// to view the frames ;> convert all the .h264 file to .jpg using the command below
// ls video_* | xargs -I % ffmpeg -i % %.jpg

#define MAX_VIDEOS (1000)

#include <stdio.h>
#include <unistd.h>
#include "ttv.h"

int main()
{
    int fileCount = 0;
    char filename[200];
    ttv_frame_t frame;


    ttv_mode(TTV_MODE_VIDEO);

    ttv_configure(TTV_RES_720_120_WIDE,0);

    while(fileCount < MAX_VIDEOS)
    {

        sprintf(filename, "/mnt/mmc/video_%03d.h264",fileCount++);
        FILE *fp = fopen(filename, "wb");

        printf("STANDALONEAPP :: recoring video %s\n",filename);

        ttv_start(TTV_STREAM_PRI | TTV_STREAM_MJPG);
        /* sleep for 1000 ms */
        usleep(1000000);
        ttv_stop(TTV_STREAM_PRI | TTV_STREAM_MJPG);

        while(ttv_get_frame(&frame))
        {
            /* if not primary get the next frame */
            if(frame.channel != TTV_CHANNEL_H264)
            {
                ttv_release_frame(frame.address);
                continue;
            }

            else if(fp)
            {
                fwrite(frame.address, frame.size, 1, fp);
                fclose(fp);
                fp = NULL;
            }

            if (frame.status & TTV_STATUS_EOS)
                printf("STANDALONEAPP :: EOS\n");

            ttv_release_frame(frame.address);
        }
    }

    ttv_mode(TTV_MODE_NONE);
    return 0;
}
