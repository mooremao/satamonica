/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#define _GNU_SOURCE         /* For GNU verion of basename() */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "tty_data.h"
#include "gsddefs.h"

int tty_fd = -1;
int tty_restarting_fd = -1;

extern int debug_bitmask;

int tty_data_init(const char* pDevice)
{
	int fd;
	int restarting_fd;
	int retry_count = 0;
	char buffer[128];

	tty_data_deinit(); /* in case tty is already opened */
	while (1) {
		fd = open(pDevice, O_RDWR);
		if (fd < 0)
		{
			perror("can't open device");
			/* The open can fail because the tty is already in use (EBUSY) or still closing (EIO) */
			if ((errno == EBUSY  || errno == EIO) && retry_count < 4) {
				fprintf(stderr, "device busy or closing, retry...\n");
				usleep(100);
				retry_count++;
				continue;
			}
			return fd;
		}
		break;
	}

	snprintf(buffer, sizeof(buffer), "/sys/class/tty/%s/device/restarting", basename(pDevice));
	restarting_fd = open(buffer, O_WRONLY);
	if (restarting_fd < 0) {
		perror("Failed to open restarting sysfs entry");
		close(fd);
		return -1;
	}

	tty_fd = fd;
	tty_restarting_fd = restarting_fd;

	tty_device_restarting(0); /* Ensure default is correct */

	return 0;
}

void tty_data_deinit(void)
{
	if(tty_restarting_fd >= 0) {
		close(tty_restarting_fd);
		tty_restarting_fd = -1;
	}

	if(tty_fd >= 0)
	{
		close(tty_fd);
		tty_fd = -1;
	}
}

int tty_data_read(uint8_t *pResponse, uint32_t nBytes, struct timeval *pTimout)
{
	int nReadOut = 0;
	int read_len;

	fd_set rfds;
	int retval;

	while (nReadOut < nBytes && tty_fd != -1)
	{
		FD_ZERO(&rfds);
		FD_SET(tty_fd, &rfds);
		retval = select(tty_fd + 1, &rfds, NULL, NULL, pTimout);
		if (retval == -1)
		{
			return retval;
		}
		else
		{
			if (retval && FD_ISSET(tty_fd, &rfds))
			{
				read_len = read(tty_fd, pResponse + nReadOut,
						nBytes - nReadOut);
				if(read_len < 0)
				{
					return read_len;
				}
				else
				{
					nReadOut += read_len;
				}
			}
			else
			{
				return GSDERROR__OSP_TIMEOUT;
			}
		}
	}

	return nReadOut;
}

int tty_data_drain(int nBytes, struct timeval *pTimout)
{
	uint8_t tmp[1024];
	int n;
	int i;

	if (debug_bitmask & GSDDEBUG__TTY_DRAIN) {
		printf("tty_data_drain nBytes=%d\n", nBytes);
	}

	while (nBytes > 0) {
		n = nBytes > sizeof(tmp) ? sizeof(tmp) : nBytes;
		tty_data_read(tmp, n, pTimout);

		if (debug_bitmask & GSDDEBUG__TTY_DRAIN) {
			// print out data
			for (i = 0; i < n; i++) {
				printf("%.2X ", tmp[i]);
				if (((i + 1) % 16) == 0)
					printf("\n");
			}
		}

		nBytes -= n;
	}

	if (debug_bitmask & GSDDEBUG__TTY_DRAIN) {
		printf("\n");
	}

	return 0;
}

int tty_data_send(const uint8_t *pBuffer, uint32_t nBytes)
{
	uint32_t written = 0;
	int rv;
	int i;

	while (written < nBytes) {
		rv = write(tty_fd, pBuffer+written, nBytes-written);
		if (rv < 0) {
			return rv;
		}

		written += rv;
	}

	if (debug_bitmask & GSDDEBUG__TTY_WRITE) {
		printf("tty_data_send: nBytes=%d:\n", nBytes);

		for (i = 0; i < nBytes; i++) {
			printf("%.2X ", pBuffer[i]);
			if (((i + 1) % 16) == 0)
				printf("\n");
		}
		printf("\n");
	}

	return 0;
}

int tty_device_restarting(int value)
{
	char buf[4];
	int n;

	n = snprintf(buf, sizeof(buf), "%1d\n", value);

	if (write(tty_restarting_fd, buf, n) == -1) {
		perror("Failed to write restarting sysfs entry");
		return -1;
	}

	return 0;
}

int tty_get_autohibernate(const char* pDevice)
{
	char buf1[128];
	char buf2[2];
	int fd;
	int len;
	int val;

	snprintf(buf1, sizeof(buf1), "/sys/class/tty/%s/device/autohibernate", basename(pDevice));
	fd = open(buf1, O_RDONLY);
	if (fd < 0) {
		perror("Failed to open autohibernate sysfs entry");
		return -1;
	}
	len = read(fd, buf2, 1);
	if(len == -1) {
		perror("Failed to read SPI autohibernate sysfs entry");
		close(fd);
		return -1;
	}
	close(fd);
	buf2[1] = '\0';
	val = atoi(buf2);

	return val;
}

int tty_set_autohibernate(const char* pDevice, int val)
{
	char buf1[128];
	char buf2[2];
	int fd;
	int len;

	snprintf(buf1, sizeof(buf1), "/sys/class/tty/%s/device/autohibernate", basename(pDevice));
	fd = open(buf1, O_WRONLY);
	if (fd < 0) {
		perror("Failed to open autohibernate sysfs entry");
		return -1;
	}
	snprintf(buf2, sizeof(buf2), "%d", val);
	len = write(fd, buf2, 1);
	if(len == -1) {
		perror("Failed to write SPI autohibernate sysfs entry");
		close(fd);
		return -1;
	}
	close(fd);

	return 0;
}

int tty_reset(const char* pDevice)
{
	char buf1[128];
	int fd;
	int len;
	char val;

	snprintf(buf1, sizeof(buf1), "/sys/class/tty/%s/device/reset", basename(pDevice));
	fd = open(buf1, O_WRONLY);
	if (fd < 0) {
		perror("Failed to open reset sysfs entry");
		return -1;
	}
	val = '1';
	len = write(fd, &val, 1);
	if(len == -1) {
		perror("Failed to write SPI reset sysfs entry");
		close(fd);
		return -1;
	}
	close(fd);

	return 0;
}

