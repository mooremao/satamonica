/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#ifndef OSP_TRANSPORT_H_
#define OSP_TRANSPORT_H_

#include <stdint.h>
#include <sys/types.h>

/**
 * \brief Retrieves a single OSP message of GSD
 *
 * Only the payload is copied to pResponse. So not the header and footer of the complete
 * OSP message.
 *
 * \param[out] pResponse Storage location for the response. Should not be NULL.
 * \param[in] maxResponseLen Size of the storage location (pResponse)
 * \param[out] pReponseLen Set to the length of the response. May be NULL.
 * \param[in] pTimeout Timeout before giving up receiving a OSP message. NULL means no timeout.
 *
 * \return 0 when ok or a negative error code
 */
int osp_transport_receive(uint8_t *pResponse, uint32_t maxResponseLen, uint32_t *pReponseLen,
		struct timeval *pTimeout);

/**
 * \brief Receives OSP messages until receiving an OSP message with the given prefix
 *
 * Only the payload is copied to pResponse. So not the header and footer of the complete
 * OSP message.
 *
 * \param[in] pResponsePrefix The give response prefix
 * \param[in] responsePrefixLen The length of the response prefix
 * \param[in] timeout Timeout before giving up receiving a OSP message. 0 means no timeout.
 * \param[out] pResponse Storage location for the response. Should not be NULL.
 * \param[in] maxResponseLen Size of the storage location (pResponse)
 * \param[out] pReponseLen Set to the length of the response
 *
 * \return 0 when ok or a negative error code
 */
int osp_transport_receive_prefix(const uint8_t* pResponsePrefix, uint32_t responsePrefixLen,
		int timeout, uint8_t* pResponse, uint32_t maxResponseLen, uint32_t *pReponseLen);

/**
 * \brief Send the given data to the GSD.
 *
 * This function adds the header and footer before sending it.
 *
 * \param[in] pData The payload data
 * \param[in] len The length of the data
 *
 * \return 0 when ok or a negative error code
 */
int osp_transport_send(const uint8_t *pData, uint32_t len);

/**
 * \brief Send and waits for a given response message.
 *
 * Only the payload is copied to pResponse. So not the header and footer of the complete
 * OSP message.
 *
 * This function is doing the same as:
 *
 * osp_transport_send(...);
 * osp_transport_receive_prefix(...);
 *
 * This function allows us to abstract the way we implement reading from the device, since when
 * reading is done in a separate thread, we will need something as:
 *
 * osp_transport_clear(...); //To prevent considering already received data add receive_prefix()
 * osp_transport_send(...);
 * osp_transport_receive_prefix(...);
 *
 * So to prevent the caller needs to know whether it needs to some clear(), we provide a function
 * to hide the internals for doing send-and-waiting-for-a-response.
 *
 * \param[in] pSendData The payload data
 * \param[in] sendLen The length of the data
 * \param[in] pResponsePrefix The give response prefix
 * \param[in] responsePrefixLen The length of the response prefix
 * \param[in] timeout Timeout before giving up receiving a OSP message. 0 means no timeout.
 * \param[out] pResponse Storage location for the response. Should not be NULL.
 * \param[in] maxResponseLen Size of the storage location (pResponse)
 * \param[out] pReponseLen Set to the length of the response
 *
 * \return 0 when ok or a negative error code
 */
int osp_transport_send_receive(const uint8_t *pSendData, uint32_t sendLen,
		const uint8_t* pResponsePrefix, uint32_t responsePrefixLen,
		int timeout, uint8_t* pResponse, uint32_t maxResponseLen, uint32_t *pReponseLen);

#endif /* OSP_TRANSPORT_H_ */
