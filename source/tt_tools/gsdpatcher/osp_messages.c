/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 * OSP command / response messages needed for patching the GSD
 *
 * MID and SID (first two bytes of all most all OSP message) are in decimal notation because
 * the OSP specification uses decimal notation for MID and SID as well.
 *
 */
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <signal.h>

#include "osp_messages.h"
#include "osp_transport.h"
#include "tty_data.h"
#include "gsddefs.h"

//Timeouts in seconds
#define TIMEOUT__DEFAULT                2
#define TIMEOUT__POLL_SOFTWARE_VERSION  1
#define TIMEOUT__WAIT_FOR_OK_TO_SEND   10

typedef struct  __attribute__((__packed__))
{
  uint8_t  msgId;
  uint8_t  subId;
  uint16_t chipId;
  uint16_t siliconId;
  uint16_t romVersionCode;
  uint16_t patchRevisionCode;
} msg178_144_t;

int osp_message_poll_clock_status(msg7_t *pData)
{
	int status;
	uint8_t poll_clock_status[] = {144, 0};
	uint8_t responsePrefix[] = { 7 };
	uint32_t reponseLen;

	status = osp_transport_send_receive(poll_clock_status, sizeof(poll_clock_status),
			responsePrefix, sizeof(responsePrefix), TIMEOUT__DEFAULT,
			(uint8_t *)pData, sizeof(msg7_t), &reponseLen);

	if(status != 0)
	{
		return status;
	}

	if(reponseLen != sizeof(msg7_t))
	{
		return GSDERROR__INVALID_RESPONSE_LENGTH;
	}
	return 0;
}


int osp_message_poll_software_version(char* pSoftwareVersion, size_t pSoftwareVersionLength,
		char* pCustomerSoftwareVersion, size_t pCustomerSoftwareVersionLength, int retries)
{
	int status;
	uint8_t poll_software_version[] = {132, 0};
	uint8_t responsePrefix[] = { 6 };
	uint8_t response[1+1+1+80+80];
	uint32_t reponseLen;

	uint8_t versionLength;
	uint8_t customerVersionLength;

	if(pSoftwareVersion != NULL &&
			pSoftwareVersionLength < (OSP_MAX_VERSION_STR_LENGTH + 1)) //+1 for \0
	{
		return -EINVAL;
	}

	if(pCustomerSoftwareVersion != NULL &&
			pCustomerSoftwareVersionLength < (OSP_MAX_VERSION_STR_LENGTH + 1)) //+1 for \0
	{
		return -EINVAL;
	}

	do
	{
		status = osp_transport_send_receive(poll_software_version, sizeof(poll_software_version),
				responsePrefix, sizeof(responsePrefix), TIMEOUT__POLL_SOFTWARE_VERSION,
				response, sizeof(response), &reponseLen);
	}
	while(status == GSDERROR__OSP_TIMEOUT && retries--);

	if(status != 0)
	{
		return status;
	}

	if(reponseLen < 4)
	{
		return GSDERROR__INVALID_RESPONSE_LENGTH;
	}

	versionLength = response[1];
	customerVersionLength = response[2];

	if(versionLength > OSP_MAX_VERSION_STR_LENGTH)
	{
		return GSDERROR__INVALID_VERSION_LENGTH;
	}

	if(customerVersionLength > OSP_MAX_VERSION_STR_LENGTH)
	{
		return GSDERROR__INVALID_CUSTOMER_VERSION_LENGTH;
	}

	if(pSoftwareVersion != NULL)
	{
		strncpy(pSoftwareVersion, response+3, versionLength);
		pSoftwareVersion[pSoftwareVersionLength - 1] = '\0';
	}

	if(pCustomerSoftwareVersion != NULL)
	{
		strncpy(pCustomerSoftwareVersion, response+3+versionLength, customerVersionLength);
		pCustomerSoftwareVersion[pCustomerSoftwareVersionLength - 1] = '\0';
	}

	return 0;
}

/* The workaround to correct the Kalimba's register */
int osp_message_kalimba_workaround()
{
	int status;
	uint8_t buf1[] = {178, 3, 0x01, 0x04, 0xFF, 0xFF, 0x82, 0x00, 0x00, 0x00, 0x00, 0x00};
	uint8_t buf2[] = {178, 3, 0x01, 0x04, 0xFF, 0xFF, 0xF6, 0x64, 0xC0, 0x00, 0x80, 0x00};
	uint8_t buf3[] = {178, 3, 0x01, 0x04, 0xFF, 0xFF, 0xF6, 0x6C, 0x00, 0x00, 0x00, 0x63};

	status = osp_transport_send(buf1, sizeof(buf1));
	if(status != 0)
	{
		fprintf(stderr, "Failed to send kalimba workaround 1\n");
		return status;
	}

	status = osp_transport_send(buf2, sizeof(buf2));
	if(status != 0)
	{
		fprintf(stderr, "Failed to send kalimba workaround 2\n");
		return status;
	}

	status = osp_transport_send(buf3, sizeof(buf3));
	if(status != 0)
	{
		fprintf(stderr, "Failed to send kalimba workaround 3\n");
		return status;
	}

	return 0;
}

/* Set patch storage to be HOST */
int osp_message_set_patch_storage()
{
	uint8_t buffer[] = {178, 20, 0x00};

	return osp_transport_send(buffer, sizeof(buffer));
}

/* poll rom version */
int osp_message_poll_patch_revision(patch_info_t *pResult)
{
	int status;
	uint8_t poll_patch_revision[] = {178, 40};
	uint8_t responsePrefix[] = { 178, 144 };
	msg178_144_t response;
	uint32_t reponseLen;

	status = osp_transport_send_receive(poll_patch_revision, sizeof(poll_patch_revision),
			responsePrefix, sizeof(responsePrefix), TIMEOUT__DEFAULT,
			(uint8_t*) &response, sizeof(response), &reponseLen);
	if(status)
	{
		return status;
	}

	if(reponseLen != sizeof(response))
	{
		return GSDERROR__INVALID_RESPONSE_LENGTH;
	}

	pResult->chip_id = ntohs(response.chipId);
	pResult->silicon_id = ntohs(response.siliconId);
	pResult->rom_version = ntohs(response.romVersionCode);
	pResult->patch_revision = ntohs(response.patchRevisionCode);

	return 0;
}

int osp_message_patch_memory_load(const uint8_t *pData, uint32_t len, uint32_t seq,
		const patch_file_info_t* pPatchFileData, bool update_k32, uint32_t *pCRC)
{
	int status;
	int offset = 0;
	uint8_t full_data[1016];
	uint8_t responsePrefix[] = { 178, 145 };
	uint8_t response[6];
	uint32_t responseLen;

	full_data[0] = 178;
	full_data[1] = 34;

	full_data[2] = (seq & 0xFF00) >> 8;
	full_data[3] =  seq & 0xFF;

	if (update_k32)
	{
		full_data[2] |= 0x80;
	}

	if(seq == 1)
	{
		uint16_t rom_version = pPatchFileData->romVersionCode;
		uint16_t patch_rivision = pPatchFileData->patchRevisionCode;
		uint32_t base_address = pPatchFileData->patchDataBaseAddress;
		uint32_t ram_offset = pPatchFileData->patchRAMStartOffset;
		uint32_t data_length = pPatchFileData->patchDataLength;

		full_data[4] = 'P';
		full_data[5] = 'M';
		full_data[6] = (rom_version & 0xFF00) >> 8;
		full_data[7] = rom_version & 0xFF;
		full_data[8] = (patch_rivision & 0xFF00) >> 8;
		full_data[9] =  patch_rivision & 0xFF;

		if (!update_k32)
		{
			full_data[10] = (base_address & 0xFF000000) >> 24;
			full_data[11] = (base_address & 0xFF0000) >> 16;
			full_data[12] = (base_address & 0xFF00) >> 8;
			full_data[13] = base_address & 0xFF;
			full_data[14] = ((data_length+4) & 0xFF00) >> 8;
			full_data[15] = (data_length+4) & 0xFF;
			full_data[16] = ((ram_offset+4) & 0xFF00) >> 8;
			full_data[17] = (ram_offset+4) & 0xFF;
			offset = 18;
		}
		else
		{
			full_data[10] = ((data_length+4) & 0xFF00) >> 8;
			full_data[11] = (data_length+4) & 0xFF;
			offset = 12;
		}
	}
	else
	{
		offset = 4;
	}

	memcpy(full_data + offset, pData, len);
	len += offset;

	if(pCRC != NULL)
	{
		full_data[len++] = ((*pCRC) & 0xFF000000) >> 24;
		full_data[len++] = ((*pCRC) & 0xFF0000) >> 16;
		full_data[len++] = ((*pCRC) & 0xFF00) >> 8;
		full_data[len++] =  (*pCRC) & 0xFF;
	}

	status = osp_transport_send_receive(full_data, len,
			responsePrefix, sizeof(responsePrefix), TIMEOUT__DEFAULT,
			response, sizeof(response), &responseLen);
	if(status)
	{
		return status;
	}

	if(responseLen != sizeof(response))
	{
		return GSDERROR__INVALID_RESPONSE_LENGTH;
	}

	//Check sequence and host sub message id
	if(response[2] == full_data[2] && response[3] == full_data[3] && response[4] != full_data[1])
	{
		return GSDERROR__INVALID_RESPONSE_DATA;
	}

	if(response[5] != 0x03)
	{
		return GSDERROR__INVALID_RESPONSE_STATUS;
	}

	return 0;
}

int osp_message_patch_exit()
{
	int status;
	uint8_t buffer[] = {178, 38};
	uint8_t responsePrefix[] = { 178, 145 };
	uint8_t response[6];
	uint32_t responseLen;

	status = osp_transport_send_receive(buffer, sizeof(buffer),
			responsePrefix, sizeof(responsePrefix), TIMEOUT__DEFAULT,
			response, sizeof(response), &responseLen);
	if(status)
	{
		return status;
	}

	if(responseLen != sizeof(response))
	{
		return GSDERROR__INVALID_RESPONSE_LENGTH;
	}

	//Check sequence and host sub message id
	if(response[4] != buffer[1])
	{
		return GSDERROR__INVALID_RESPONSE_DATA;
	}

	if(response[5] != 0x03)
	{
		return GSDERROR__INVALID_RESPONSE_STATUS;
	}

	return 0;
}

int wait_for_ok_to_send()
{
	uint8_t responsePrefix[] = { 18, 1 };
	uint8_t response[2];
	uint32_t responseLen;

	return osp_transport_receive_prefix(responsePrefix, sizeof(responsePrefix),
			TIMEOUT__WAIT_FOR_OK_TO_SEND,
			response, sizeof(response), &responseLen);
}

int osp_message_factory_reset()
{
	const uint8_t fact_reset_cmd[] = {
			 128, 0x00, 0x00, 0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00,
			0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x0C ,0x08
	};
	uint8_t responsePrefix[] = { 18, 1 };
	uint8_t response[2];
	uint32_t responseLen;
	int status;
	sigset_t mask;
	sigset_t orig;

	/* Block SIGTERM and SIGINT signals. Aborting reset might result in GSD hibernate failures. */
	sigemptyset(&mask);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGINT);
	sigprocmask(SIG_BLOCK, &mask, &orig);

	tty_device_restarting(1);

	status = osp_transport_send_receive(fact_reset_cmd, sizeof(fact_reset_cmd), responsePrefix, sizeof(responsePrefix),
			TIMEOUT__WAIT_FOR_OK_TO_SEND,
			response, sizeof(response), &responseLen);

	tty_device_restarting(0);

	/* Restore signal mask. Restart done */
	sigprocmask(SIG_SETMASK, &orig, NULL);

	return status;
}

int osp_message_host_restart()
{
	const uint8_t hostRestartMessage[] = {
			 128, 0x00, 0x00, 0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00,
			0x00, 0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x0C ,0x00
	};
	uint8_t responsePrefix[] = { 18, 1 };
	uint8_t response[2];
	uint32_t responseLen;
	int status;
	sigset_t mask;
	sigset_t orig;

	/* Block SIGTERM and SIGINT signals. Aborting reset might result in GSD hibernate failures. */
	sigemptyset(&mask);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGINT);
	sigprocmask(SIG_BLOCK, &mask, &orig);

	tty_device_restarting(1);

	status = osp_transport_send_receive(hostRestartMessage, sizeof(hostRestartMessage), responsePrefix, sizeof(responsePrefix),
			TIMEOUT__WAIT_FOR_OK_TO_SEND,
			response, sizeof(response), &responseLen);

	tty_device_restarting(0);

	/* Restore signal mask. Restart done */
	sigprocmask(SIG_SETMASK, &orig, NULL);

	return status;
}

int osp_message_get_tracker_configuration(uint8_t* pTrackerConfiguration,
		size_t maxTrackerConfigurationLength, size_t* pTrackConfigurationLength)
{
	int status;
	const uint8_t trackerConfigurationPoll[] = { 178, 9 };
	uint8_t responsePrefix[] = { 178, 10 };
	uint8_t response[80];
	uint32_t responseLen;

	status = osp_transport_send_receive(trackerConfigurationPoll, sizeof(trackerConfigurationPoll), responsePrefix, sizeof(responsePrefix),
			TIMEOUT__DEFAULT,
			response, sizeof(response), &responseLen);

	if (status)
		return status;

	if (responseLen < 2) {
		return GSDERROR__INVALID_RESPONSE_LENGTH;
	}

	if (pTrackConfigurationLength != NULL)
		*pTrackConfigurationLength = (responseLen - 2);

	if ((*pTrackConfigurationLength) > maxTrackerConfigurationLength)
		*pTrackConfigurationLength = maxTrackerConfigurationLength;

	memcpy(pTrackerConfiguration, response + 2, *pTrackConfigurationLength);

	return 0;
}

int osp_message_set_tracker_configuration(const uint8_t* pTrackerConfiguration,
		size_t trackerConfigurationLength)
{
	uint8_t trackerConfigurationMessage[80] = { 178, 70 };
	uint8_t responsePrefix[] = { 178, 7, 0 };
	uint8_t response[80];
	uint32_t responseLen;

	trackerConfigurationMessage[0] = 178;
	trackerConfigurationMessage[1] = 70;

	if (trackerConfigurationLength > (sizeof(trackerConfigurationMessage) - 2))
		return GSDERROR__TRACKER_CONFIG_TOO_LONG;

	memcpy(trackerConfigurationMessage + 2, pTrackerConfiguration, trackerConfigurationLength);

	return osp_transport_send_receive(trackerConfigurationMessage, trackerConfigurationLength + 2, responsePrefix, sizeof(responsePrefix),
			TIMEOUT__DEFAULT,
			response, sizeof(response), &responseLen);
}
