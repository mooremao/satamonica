/*
 * Copyright 2014 TomTom B.V.
 *
 * Contains all gsdpatcher defines:
 * -error codes
 * -debug bits
 */

#ifndef GSDDEFS_H_
#define GSDDEFS_H_

/**
 * Error codes:
 */
#define GSDERROR__INVALID_PATCH_OFFSET            -1001
#define GSDERROR__INVALID_PATCH_FILE              -1002
#define GSDERROR__INVALID_PATCH_DATA_LENGTH       -1003
#define GSDERROR__INVALID_RAM_START_OFFSET        -1004

#define GSDERROR__RAM_OFFSET_TOO_LARGE            -1100

#define GSDERROR__INVALID_RESPONSE_LENGTH         -1200
#define GSDERROR__INVALID_VERSION_LENGTH          -1201
#define GSDERROR__INVALID_CUSTOMER_VERSION_LENGTH -1202
#define GSDERROR__INVALID_RESPONSE_DATA           -1203
#define GSDERROR__INVALID_RESPONSE_STATUS         -1204

#define GSDERROR__OSP_PAYLOAD_TOO_LONG            -1300

#define GSDERROR__OSP_TIMEOUT                     -1400

#define GSDERROR__TRACKER_CONFIG_TOO_LONG         -1500

/**
 * Debug bits
 */
#define GSDDEBUG__TTY_WRITE    (1 << 0)
#define GSDDEBUG__TTY_READ     (1 << 1) //Not implemented
#define GSDDEBUG__TTY_DRAIN    (1 << 2)
#define GSDDEBUG__OSP_RECEIVED (1 << 3)
#define GSDDEBUG__PATCHING     (1 << 4)
#define GSDDEBUG__CONFIGURE    (1 << 5)
#define GSDDEBUG__ALL          ( \
					GSDDEBUG__TTY_WRITE | GSDDEBUG__TTY_READ | \
					GSDDEBUG__TTY_DRAIN | GSDDEBUG__OSP_RECEIVED | \
					GSDDEBUG__PATCHING | GSDDEBUG__CONFIGURE)

#endif /* GSDDEFS_H_ */
