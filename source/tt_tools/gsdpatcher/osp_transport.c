/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "osp_transport.h"

#include "gsddefs.h"
#include "tty_data.h"

typedef enum _osp_header_t
{
	OSP_HEADER_NOTFOUND,
	OSP_HEADER_PARTIAL,
	OSP_HEADER_START_SEQUENCE_FOUND
} osp_header_t;

static uint8_t receive_buffer[2048];
static uint32_t received_bytes = 0;

extern int debug_bitmask;

static uint16_t osp_transport_gen_checksum(const uint8_t *pData, uint32_t length)
{
	uint16_t checksum, index;
	checksum = 0;
	index = 0;
	while(index < length)
	{
		checksum = checksum + (uint16_t) pData[index];
		checksum = checksum & 0x7FFF;
		index++;
	}
	return checksum;
}

static osp_header_t osp_transport_header_detected(const uint8_t *data, uint32_t length,
		uint32_t *index)
{
	uint32_t i;
	uint8_t byte1;
	uint8_t byte2;

	for (i = 0; i < length-1; i ++)
	{
		byte1 = data[i];
		byte2 = data[i+1];
		if ((byte1 == 0xA0) && (byte2 == 0xA2))
		{
			*index = i;
			return OSP_HEADER_START_SEQUENCE_FOUND;
		}
	}

	if(byte2 == 0xA0)
	{
		// Need to request more data
		return OSP_HEADER_PARTIAL;
	}

	return OSP_HEADER_NOTFOUND;
}

static int osp_transport_receive_header(struct timeval *pTimeout, unsigned int* pPayloadLength) {
	int rv;
	uint8_t header[4];
	uint32_t alreadyRead = 0;
	uint32_t index;
	osp_header_t state = OSP_HEADER_NOTFOUND;

	do {
		rv = tty_data_read(header+alreadyRead, 4-alreadyRead, pTimeout);
		if(rv != 4-alreadyRead) {
			return rv;
		}

		state = osp_transport_header_detected(header, 4, &index);
		switch(state) {
		case OSP_HEADER_NOTFOUND:
			alreadyRead = 0;
			continue;

		case OSP_HEADER_PARTIAL:
			header[0] = header[3];
			alreadyRead = 1;
			continue;

		case OSP_HEADER_START_SEQUENCE_FOUND:
			if(index != 0) {
				memmove(header, header+index, 4-index);
				alreadyRead = 4-index;
				continue;
			}

			*pPayloadLength = (header[2] << 8) | header[3];

			return 0;
		}

	} while(1);
}

int osp_transport_receive(uint8_t *pResponse, uint32_t maxResponseLen, uint32_t *pReponseLen,
		struct timeval *pTimeout) {
	int rv;
	unsigned int payloadLength = 0;
	uint16_t msg_checksum;
	uint8_t footer[4];

	do {
		/* Receive header */
		rv = osp_transport_receive_header(pTimeout, &payloadLength);
		if(rv) {
			return rv;
		}

		/* Check whether it will in our response. If not: drain payload + footer */
		if(payloadLength > maxResponseLen) {
			tty_data_drain(payloadLength + 4, pTimeout);
			return GSDERROR__OSP_PAYLOAD_TOO_LONG;
		}

		/* Read payload */
		rv = tty_data_read(pResponse, payloadLength, pTimeout);
		if(rv != payloadLength) {
			return rv;
		}

		/* Read footer */
		rv = tty_data_read(footer, 4, pTimeout);
		if(rv != 4) {
			return rv;
		}

		/* Check footer */
		if(footer[2] != 0xB0 || footer[3] != 0xB3) {
			fprintf(stderr, "Checking footer failed\n");
			continue;
		}

		/* Check checksum */
		msg_checksum = (footer[0] << 8) | footer[1];
		if(msg_checksum != osp_transport_gen_checksum(pResponse, payloadLength))
		{
			fprintf(stderr, "Message checking does not match\n");
			continue;
		}

		if(pReponseLen != NULL) {
			*pReponseLen = payloadLength;
		}

		return 0;
	} while(1);
}

int osp_transport_receive_prefix(const uint8_t* pResponsePrefix, uint32_t responsePrefixLen,
		int timeout, uint8_t* pResponse, uint32_t maxResponseLen, uint32_t *pReponseLen)
{
	int rv;
	uint32_t i;

	struct timeval tv;

	tv.tv_sec = timeout;
	tv.tv_usec = 0;

	while (1) {
		rv = osp_transport_receive(pResponse, maxResponseLen, pReponseLen,
				timeout != 0 ? &tv : NULL);
		if(rv == GSDERROR__OSP_PAYLOAD_TOO_LONG)
		{
			continue;
		}
		else if(rv)
		{
			return rv;
		}

		if (debug_bitmask & GSDDEBUG__OSP_RECEIVED) {
			printf("Received OSP message with id: %d\n", pResponse[0]);

			for (i = 0; i < *pReponseLen; i++) {
				printf("%.2X ", pResponse[i]);
				if (((i + 1) % 16) == 0)
					printf("\n");
			}
			printf("\n");
		}

		if (responsePrefixLen > *pReponseLen) {
			continue;
		}

		if (memcmp(pResponse, pResponsePrefix, responsePrefixLen) == 0) {
			return 0;
		}
	}
}

int osp_transport_send(const uint8_t *pData, uint32_t len) {
	uint16_t checksum = 0;
	static uint8_t buffer[1024]; 	// maximum that could send through OSP

	if(len > 1024 - 8) {
		return -1;
	}

	buffer[0] = 0xA0;
	buffer[1] = 0xA2;
	buffer[2] = (len & 0xFF00) >> 8;
	buffer[3] = len & 0xFF;

	memcpy(&buffer[4], pData, len);
	checksum = osp_transport_gen_checksum(pData, len);
	buffer[len+4] = (checksum & 0xFF00) >> 8;
	buffer[len+5] = checksum & 0xFF;
	buffer[len+6] = 0xB0;
	buffer[len+7] = 0xB3;

	return tty_data_send(buffer, len+8);
}

int osp_transport_send_receive(const uint8_t *pSendData, uint32_t sendLen,
		const uint8_t* pResponsePrefix, uint32_t responsePrefixLen, int timeout,
		uint8_t* pResponse, uint32_t maxResponseLen, uint32_t *pReponseLen) {
	int rv;

	rv = osp_transport_send(pSendData, sendLen);
	if(rv < 0) {
		return rv;
	}

	return osp_transport_receive_prefix(pResponsePrefix, responsePrefixLen, timeout, pResponse,
			maxResponseLen, pReponseLen);
}

