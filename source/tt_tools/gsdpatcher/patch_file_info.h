/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#ifndef PATCH_FILE_INFO_H_
#define PATCH_FILE_INFO_H_

#include <stdint.h>
#include <stdbool.h>

#define PATCH_FILE_HEADERINFO_SIZE          (16)

typedef struct
{
   uint16_t chipId;
   uint16_t siliconVersion;
   uint16_t romVersionCode;
   uint16_t patchRevisionCode;
   uint32_t patchDataBaseAddress;   /* Don't know if this is necessary */
   uint16_t patchDataLength;       /* Size of patch data in PD2 */
   uint16_t patchRAMStartOffset;   /* Start offset of patch ram code in PatchData array */
} patch_file_info_t;

/**
 * \brief Retrieves patch file info from the patch data
 *
 * \param[out] pPFInfo Storage location of the patch file info.
 * \param[in] offset Offset of the patch file data (should be non-zero for k32 patch)
 * \param[in] update_k32 True when to retrieve patch info for k32.
 *
 * \return 0 when ok or a negative error code
 */
int patch_file_info_get_info(patch_file_info_t *pPFInfo, uint16_t offset, bool update_k32);

/**
 * \brief Retrieves overlay data from patch data.
 *
 * \param[out] pResult Storage location of the retrieved overlay data.
 * \param[in] maxResultLength The maximum number of bytes the caller is able to retrieve
 * \param[in] offset Offset of the patch file data (should be non-zero for k32 patch)
 * \param[in] pPFInfo The patch file info
 *
 * \return number of bytes retrieved or a negative error code
 */
int patch_file_info_read_overlay (uint8_t *pResult, size_t maxResultLength, uint16_t offset,
		const patch_file_info_t *pPFInfo);

/**
 * \brief Retrieves non overlay data from patch data.
 *
 * \param[out] pResult Storage location of the retrieved overlay data.
 * \param[in] maxResultLength The maximum number of bytes the caller is able to retrieve
 * \param[in] offset Offset of the patch file data (should be non-zero for k32 patch)
 * \param[in] pPFInfo The patch file info
 * \param[in] non_overlay_offset Number of of bytes earlier retrieved.
 *
 * \return number of bytes retrieved or a negative error code
 */
int patch_file_info_read_non_overlay (uint8_t *pResult, size_t maxResultLength, uint16_t offset,
		const patch_file_info_t *pPFInfo, uint32_t non_overlay_offset);

#endif /* PATCH_FILE_INFO_H_ */
