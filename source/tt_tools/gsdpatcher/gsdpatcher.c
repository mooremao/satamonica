/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <getopt.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>

#include "gsddefs.h"
#include "patch_file_info.h"
#include "tty_data.h"
#include "osp_transport.h"
#include "osp_messages.h"
#include "crc.h"

#define ROM_SW_VERSION "5xp__5.5.2-R32+5xpt_5.5.2-R32"
#define MAX_RETRIES 4

#define GSD_SLEEP_LOCK_PATH "/dev/shm/gsd_sleep_lock"

int debug_bitmask = 0;

static void usage(const char* argv0) {
	printf("Usage:\n\n");
	printf("%s [-d <debug_bitmask>] <tty-device>\n\n", argv0);
	printf("debug_bitmask: Each bit defines type of debug messages.\n");
	printf("               %d enables all debug messages\n\n", GSDDEBUG__ALL);
	printf("tty-device   : The tty device to use. Example /dev/ttyGSD5xp0\n\n");
}

static int load_patch(patch_file_info_t* patch_file_data, uint16_t offset, bool update_k32)
{
	int rv;
	uint32_t crc;
	uint8_t patch_data[OSP_MAX_SUBSEQUENT_PATCH_MEMORY_LOAD_SIZE];
	size_t patch_ram_offset;
	size_t read_num;
	uint32_t non_over_offset = 0;
	uint32_t seq;
	int last;

	if(debug_bitmask & GSDDEBUG__PATCHING)
	{
		printf("\tBase address: %08x\n", patch_file_data->patchDataBaseAddress);
		printf("\tData length:  %8x\n", patch_file_data->patchDataLength);
		printf("\tRAM offset:   %8x\n", patch_file_data->patchRAMStartOffset);
	}

	//Read overlay
	rv = patch_file_info_read_overlay(patch_data, sizeof(patch_data), offset, patch_file_data);
	if(rv < 0)
	{
		fprintf(stderr, "Failed to read overlay. error: %d\n", rv);
		return rv;
	}
	patch_ram_offset = rv;

	//Calculate CRC of overlay
	if(!update_k32)
	{
		crc = gps_calculate_crc32(patch_data, patch_ram_offset);
	}
	else
	{
		crc = gps_calculate_crc32(patch_data+2, patch_ram_offset-2);
	}

	//Send overlay
	rv = osp_message_patch_memory_load(patch_data, patch_ram_offset, 1, patch_file_data, update_k32, &crc);
	if(rv)
	{
		fprintf(stderr, "Patch memory load of overlay failed. error: %d\n", rv);
		return rv;
	}

	printf(".");
	fflush(stdout);

	//Send non overlays
	seq = 2;
	while(1)
	{
		//Read it
		read_num = patch_file_info_read_non_overlay(patch_data, sizeof(patch_data), offset, patch_file_data, non_over_offset);
		non_over_offset += read_num;
		last = patch_file_data->patchDataLength - non_over_offset - patch_ram_offset == 0;

		//Calculate CRC of overlay
		if (seq == 2)
		{
			if (!update_k32)
			{
				crc = gps_update_crc32(patch_data + 6, read_num-6, (uint32_t) (-1));
			}
			else
			{
				crc = gps_update_crc32(patch_data + 2, read_num-2, (uint32_t) (-1));
			}
		}
		else
		{
			crc = gps_update_crc32(patch_data, read_num, crc);
		}

		if(last)
		{
			crc = gps_finalize_crc32 (crc);
		}

		//Send non overlay
		rv = osp_message_patch_memory_load(patch_data, read_num, seq, patch_file_data, update_k32,
				last ? &crc : NULL);
		if(rv)
		{
			fprintf(stderr, "Patch memory load of non overlay (%d) failed. error: %d\n", seq, rv);
			return rv;
		}

		printf(".");
		fflush(stdout);

		if(last) {
			return 0;
		}

		seq++;
	}
}

static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "debug",  1, 0, 'd' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "d:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'd':
			debug_bitmask = atoi(optarg);
			break;

		default:
			printf("unknown option\n");
			usage(argv[0]);
			exit(1);
			break;
		}
	}
}

static int awake_device(const char* dev_name)
{
	char path_buffer[128]; // sysfs entry path
	int sleep_value = 0; // value to write to sleep sysfs entry
	int awake_value = 0;
	FILE* fp = 0;

	snprintf(path_buffer, sizeof(path_buffer), "/sys/class/tty/%s/device/sleep", basename(dev_name));

	fp = fopen(path_buffer, "w");
	if (fp == 0) {
		perror("Can't open sleep sysfs entry");
		return -errno;
	}

	if (fprintf(fp, "%d\n", sleep_value) < 0) {
		perror("Failed to write to sleep sysfs entry");
		fclose(fp);
		return -errno;
	}
	fclose(fp);

	// Now make sure device is awake
	snprintf(path_buffer, sizeof(path_buffer), "/sys/class/tty/%s/device/awake", basename(dev_name));
	fp = fopen(path_buffer, "r");
	if (fp == 0) {
		perror("Can't open awake sysfs entry");
		return -errno;
	}

	fscanf(fp, "%d", &awake_value);
	fclose(fp);

	if (!awake_value) {
		fprintf(stderr, "Failed to awake gsd device\n");
		return -1;
	}

	return 0;
}

static int set_sleep_locked(int locked, const char* dev_path)
{
	static int fd = -1;
	int flags = locked ? LOCK_EX : LOCK_UN;
	int rv = 0;

	if (fd < 0) {
		fd = open(GSD_SLEEP_LOCK_PATH, O_CREAT | O_RDWR);
		if (fd < 0) {
			fprintf(stderr,"Failed to open %s: %s\n", GSD_SLEEP_LOCK_PATH, strerror(errno));
			return -errno;
		}
	}

	if (flock(fd, flags) != 0) {
		fprintf(stderr,"Failed to %s file %s : %s\n", locked?"lock":"unlock", GSD_SLEEP_LOCK_PATH, strerror(errno));
		rv = -errno;
	}

	// Ensure device is awake
	if (locked && rv == 0)
		rv = awake_device(dev_path);

	// If unlocking, close the file
	if (!locked) {
		close(fd);
		fd = -1;
	}

	return rv;
}

static int gsd_reset(const char* pDevice)
{
	int tmp_val;
	int ret = 0;

	/* Save the autohibernate status */
	tmp_val = tty_get_autohibernate(pDevice);
	if(tmp_val == -1) {
		ret = tmp_val;
		goto err;
	}

	/* Close the TTY */
	if(tty_set_autohibernate(pDevice, 2) == -1) {
		ret = -1;
		goto err;
	}
	tty_data_deinit();

	/* Reset the GSD module */
	if(tty_reset(pDevice) == -1) {
		ret = -1;
		goto err;
	}

	/* Restore the autohibernate status */
	if(tty_set_autohibernate(pDevice, tmp_val) == -1) {
		ret = -1;
	}
err:
	return ret;
}

int main(int argc, char *argv[])
{
	int rv;
	uint16_t offset;
	char softwareVersion[81];
	char customerSoftwareVersion[81];
	char expectedVersion[81];
	patch_file_info_t patch_file_data;
	sigset_t mask;
	int retries;

	if(argc < 2) {
		usage(argv[0]);
		return 1;
	}

	parse_opts(argc - 1, argv);

	rv = patch_file_info_get_info(&patch_file_data, 0, false);
	if(rv)
	{
		fprintf(stderr, "Reading patch file info failed. error: %d\n", rv);
		return 1;
	}

	fprintf(stderr, "GSD patcher for chipid=%x, silicon=%x, rom=%x, revision=%d\n", patch_file_data.chipId,
			patch_file_data.siliconVersion, patch_file_data.romVersionCode,
			patch_file_data.patchRevisionCode);

	retries = 0;
	while (retries < MAX_RETRIES) {
		rv = tty_data_init(argv[argc-1]);
		if(rv)
		{
			fprintf(stderr, "Failed to initialize tty: \"%s\". error: %d\n", argv[argc-1], rv);
			return 2;
		}

		//Wait for a message. We don't care what it is. When opening the tty resulted in powering on the gsd, we will
		//need to wait for ACK to send message.
		if(osp_transport_receive(NULL, 0, NULL, NULL) != GSDERROR__OSP_PAYLOAD_TOO_LONG)
		{
			fprintf(stderr, "Failed to wait for OSP message. error: %d\n", rv);
			return 3;
		}

		rv = osp_message_poll_software_version(softwareVersion, sizeof(softwareVersion),
				customerSoftwareVersion, sizeof(customerSoftwareVersion), 2);
		if(rv == 0) {
			break;
		}
		else {
			fprintf(stderr, "Failed to poll software version. error: %d\n", rv);

			if (gsd_reset(argv[argc-1]) == -1) {
				return 4;
			}
			retries++;
		}
	}

	if (retries == MAX_RETRIES) {
		fprintf(stderr, "Failed to poll software version, no more retries\n");
		return 4;
	}

	fprintf(stderr, "GSD software version: \"%s %s\"\n", softwareVersion, customerSoftwareVersion);

	snprintf(expectedVersion, sizeof(expectedVersion), "5.5.%d", patch_file_data.patchRevisionCode);
	if(strstr(softwareVersion, expectedVersion) != NULL)
	{
		fprintf(stderr, "GSD already up to date!\n");
		return 0;
	}

	fprintf(stderr, "Patch update needed.\n");

	/* Block SIGTERM and SIGINT signals. Aborting patch might result in GSD hibernate failures. */
	sigemptyset(&mask);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGINT);
	sigprocmask(SIG_BLOCK, &mask, NULL);

	/* Block chip suspend feature */
	if (set_sleep_locked(1, argv[argc-1]) != 0) {
		rv = 16;
		goto err_unblock_signals;
	}

	/* factory reset GSD chip before applying new patch */
	fprintf(stderr, "Performing GSD factory reset\n");
	retries = 0;
	while (retries < MAX_RETRIES) {
		rv = osp_message_factory_reset();
		if (rv == 0) {
			rv = osp_message_poll_software_version(softwareVersion, sizeof(softwareVersion), NULL, 0, 2);
			if (rv == 0 && strcmp(softwareVersion, ROM_SW_VERSION) == 0) {
				break;
			}
		}
		fprintf(stderr, "Failed to factory reset device. Retrying...\n");
		retries++;
	}

	if (retries == MAX_RETRIES) {
		fprintf(stderr, "Failed to factory reset device, no more retries\n");
		rv = 15;
		goto err_unblock_signals;
	}

	rv = osp_message_kalimba_workaround();
	if(rv)
	{
		fprintf(stderr, "Kalimba workaround failed. error: %d\n", rv);
		rv = 5;
		goto err_unblock_signals;
	}

	rv = osp_message_set_patch_storage();
	if(rv)
	{
		fprintf(stderr, "Setting patch storage failed. error: %d\n", rv);
		rv = 6;
		goto err_unblock_signals;
	}

	patch_info_t patch_info;
	rv = osp_message_poll_patch_revision(&patch_info);
	if(rv)
	{
		fprintf(stderr, "Failed to retrieve patch revision. error: %d\n", rv);
		rv = 8;
		goto err_unblock_signals;
	}

	if(debug_bitmask & GSDDEBUG__PATCHING)
	{
		printf("Current patch info:\n");
		printf("\tchipid = %x\n", patch_info.chip_id);
		printf("\tsil id = %x\n", patch_info.silicon_id);
		printf("\trom version = %x\n", patch_info.rom_version);
		printf("\t_patch revision = %d\n",patch_info.patch_revision);
	}

	rv = load_patch(&patch_file_data, 0, false);
	if(rv)
	{
		rv = 9;
		goto err_unblock_signals;
	}

	offset = PATCH_FILE_HEADERINFO_SIZE + 4 + patch_file_data.patchDataLength;

	rv = patch_file_info_get_info(&patch_file_data, offset, true);
	if(rv)
	{
		fprintf(stderr, "Reading patch file info failed. error: %d\n", rv);
		rv = 110;
		goto err_unblock_signals;
	}

	if(debug_bitmask & GSDDEBUG__PATCHING)
	{
		printf("chipid=%x, silicon=%x, rom=%x, revision=%d\n", patch_file_data.chipId,
				patch_file_data.siliconVersion, patch_file_data.romVersionCode,
				patch_file_data.patchRevisionCode);
	}

	rv = load_patch(&patch_file_data, offset, true);
	if(rv)
	{
		rv = 10;
		goto err_unblock_signals;
	}

	tty_device_restarting(1); /* patch exit will result in a device restart */

	rv = osp_message_patch_exit();
	if(rv)
	{
		fprintf(stderr, "Failed to retrieve patch upload acknowledgment. error: %d\n", rv);
		rv = 11;
		goto err_unblock_signals;
	}

	printf("\nGSD Patched. Checking version again:\n");

	rv = wait_for_ok_to_send();
	if (rv) {
		if (rv != GSDERROR__OSP_TIMEOUT) {
			fprintf(stderr, "Failed to receive OK-to-send. error: %d\n", rv);
			rv = 12;
			goto err_unblock_signals;
		} else {
			fprintf(stderr, "Timeout while waiting for OK-to-send. Justing checking version anyway.\n");
		}
	}

	tty_device_restarting(0); /* device is restarted */

	/* Unblock the suspend feature */
	set_sleep_locked(0, argv[argc-1]);

	/* Patching done. Unblock the signals */
	sigprocmask(SIG_UNBLOCK, &mask, NULL);

	rv = osp_message_poll_software_version(softwareVersion, sizeof(softwareVersion),
				customerSoftwareVersion, sizeof(customerSoftwareVersion), 10);
	if (rv) {
		fprintf(stderr, "Failed to poll software version. error: %d\n", rv);
		return 13;
	}

	printf("GSD software version: \"%s %s\"\n", softwareVersion, customerSoftwareVersion);

	if (strstr(softwareVersion, expectedVersion) != NULL) {
		printf("GSD successfully updated!\n");
		return 0;
	} else {
		return 14;
	}

err_unblock_signals:
	set_sleep_locked(0, argv[argc-1]);
	sigprocmask(SIG_UNBLOCK, &mask, NULL);
	return rv;
}
