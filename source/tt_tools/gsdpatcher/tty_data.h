/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#ifndef TTY_DATA_H_
#define TTY_DATA_H_

#include <sys/time.h>
#include <sys/types.h>
#include <stdint.h>

/**
 * \brief initializes the tty
 *
 * \param[in] pDevice The device name to open
 *
 * \return 0 when ok or a negative error code
 */
int tty_data_init(const char* pDevice);

/**
 * \brief De-initializes the tty
 */
void tty_data_deinit();

/**
 * \brief Reads bytes from the tty
 *
 * \param[out] pResponse Storages location of the read bytes. Should not be NULL
 * \param[in] nBytes Number of bytes the read.
 * \param[in] pTimout Timeout before giving up or NULL when for no timeout.
 *
 * \return 0 when ok or a negative error code
 */
int tty_data_read(uint8_t *pResponse, uint32_t nBytes, struct timeval *pTimout);

/**
 * \brief Drains the given number of bytes from the tty.
 *
 * \param[in] nBytes Number of bytes to drain.
 * \param[in] pTimout Timeout before giving up or NULL when for no timeout.
 *
 * \return 0 when ok or a negative error code
 */
int tty_data_drain(int nBytes, struct timeval *pTimout);

/**
 * \brief Sends the given data to the tty
 *
 * \param[in] pBuffer The data to send
 * \param[in] nBytes Number of bytes to send.
 *
 * \return 0 when ok or a negative error code
 */
int tty_data_send(const uint8_t *pBuffer, uint32_t nBytes);

/**
 * \brief Sets the given value to the restarting sysfs entry of the device.
 *
 * The device driver needs to be aware of device restarts. As communication with
 * the device might get affected by the restart. Therefore, this function needs
 * to be called before (value=1) and after (value=0) restarting the device.
 *
 * \param[in] value. 0 = not restarting. 1 = restarting.
 *
 * \return 0 when ok or a negative error code
 */
int tty_device_restarting(int value);

/**
 * \brief Retrieve the autohibernate status of a given device
 *
 * \param[in] pDevice The device name to query
 *
 * \return autohibernate status value or -1 in case of error
 */
int tty_get_autohibernate(const char* pDevice);

/**
 * \brief Set the autohibernate status of a given device
 *
 * \param[in] pDevice The device name to query
 * \param[in] val The autohibernate status value to set
 *
 * \return 0 when ok or -1 in case of errors
 */
int tty_set_autohibernate(const char* pDevice, int val);

/**
 * \brief Reset the given TTY device
 *
 * \param[in] pDevice The device name to reset
 *
 * \return 0 when ok or -1 in case of errors
 */
int tty_reset(const char* pDevice);

#endif /* TTY_DATA_H_ */
