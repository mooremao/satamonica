/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <getopt.h>
#include <stdlib.h>

#include "gsddefs.h"
#include "tty_data.h"
#include "osp_transport.h"
#include "osp_messages.h"
#include "crc.h"

#define MAC_RETRY_COUNT 5
#define ROM_SW_VERSION "5xp__5.5.2-R32+5xpt_5.5.2-R32"

int debug_bitmask = 0;

static void usage(const char* argv0) {
	printf("Usage:\n\n");
	printf("%s [-d <debug_bitmask>] <tty-device>\n\n", argv0);
	printf("debug_bitmask: Each bit defines type of debug messages.\n");
	printf("               %d enables all debug messages\n\n", GSDDEBUG__ALL);
	printf("tty-device   : The tty device to use. Example /dev/ttyGSD5xp0\n\n");
}

static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "debug",  1, 0, 'd' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "d:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'd':
			debug_bitmask = atoi(optarg);
			break;

		default:
			printf("unknown option\n");
			usage(argv[0]);
			exit(1);
			break;
		}
	}
}

int main(int argc, char *argv[])
{
	int rv;
	int retry_count = 0;
	char softwareVersion[81];

	if(argc < 2) {
		usage(argv[0]);
		return 1;
	}

	parse_opts(argc - 1, argv);

	rv = tty_data_init(argv[argc-1]);
	if(rv)
	{
		fprintf(stderr, "Failed to initialize tty: \"%s\". error: %d\n", argv[argc-1], rv);
		rv = 2;
		goto finish;
	}

	//Wait for a message. We don't care what it is. When opening the tty resulted in powering on the gsd, we will
	//need to wait for ACK to send message.
	if(osp_transport_receive(NULL, 0, NULL, NULL) != GSDERROR__OSP_PAYLOAD_TOO_LONG)
	{
		fprintf(stderr, "Failed to wait for OSP message. error: %d\n", rv);
		rv = 3;
		goto finish;
	}

	while( retry_count < MAC_RETRY_COUNT )
	{
		rv = osp_message_factory_reset();
		if(rv && retry_count == (MAC_RETRY_COUNT-1) )
		{
			fprintf(stderr, "Failed to receive OK to send. error: %d\n", rv);
		}
		else
		{
			rv = osp_message_poll_software_version(softwareVersion, sizeof(softwareVersion), NULL, 0, 2);
			if( rv == 0 )
			{
				if( strcmp( softwareVersion, ROM_SW_VERSION ) == 0 )
					break;
				else
					fprintf(stderr, "Wrong SW version %s, retrying..\n", softwareVersion);
			}
			else if( debug_bitmask )
			{
				fprintf(stderr, "Failed to poll software version. error: %d\n", rv);
			}
		}
		retry_count++;
		sleep(1);
	}

	if( debug_bitmask || retry_count > 0 )
		printf("retry_count %d\n",retry_count);

finish:
	tty_data_deinit();
	return rv;
}
