/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <getopt.h>
#include <stdlib.h>

#include "gsddefs.h"
#include "tty_data.h"
#include "osp_transport.h"
#include "osp_messages.h"
#include "crc.h"

int debug_bitmask = 0;
static int hibernate_rtc = -1;

#define TRACKER_CONFIG_OFFSET__GPIO_8_OPT 33

#define TRACKER_CONFIG_GPIO_8_OPT__NO_HIBERNATE_RTC 0x03
#define TRACKER_CONFIG_GPIO_8_OPT__HIBERNATE_RTC    0x1B

static void usage(const char* argv0)
{
	printf("Usage:\n\n");
	printf("%s [-d <debug_bitmask>] [-r <hibernate-rtc>] <tty-device>\n\n", argv0);
	printf("debug_bitmask: Each bit defines type of debug messages.\n");
	printf("               %d enables all debug messages\n\n", GSDDEBUG__ALL);
	printf("hiberate-rtc : Set to 1 to enable RTC clock in hibernate mode.\n");
	printf("               Set to 0 to disable RTC clock in hibernate mode.\n");
	printf("tty-device   : The tty device to use. Example /dev/ttyGSD5xp0\n\n");
}

static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "debug",  1, 0, 'd' },
			{ "rtc", 1, 0, 'r' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "d:r:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'd':
			debug_bitmask = atoi(optarg);
			break;

		case 'r':
			hibernate_rtc = atoi(optarg);
			break;

		default:
			printf("unknown option\n");
			usage(argv[0]);
			exit(1);
			break;
		}
	}
}

static void debug_print_config(const char *message, uint8_t *trackerConfiguration, size_t trackConfigurationLength)
{
	size_t i;

	if ((debug_bitmask & GSDDEBUG__CONFIGURE) == 0)
		return;

	printf("%s:\n", message);

	for (i = 0; i < trackConfigurationLength; i++) {
		printf("%.2X ", trackerConfiguration[i]);
		if (((i + 1) % 16) == 0)
			printf("\n");
	}
	printf("\n");
}

/**
 * \brief Configures the gsd with the given options and issues hot restart (when needed).
 *
 * When all configuration options (currently one) equal to -1, no actions are done.
 * When requested configuration is already active, the setting the configuration and the hot restart
 * are skipped.
 *
 * \param[in] hibernate_rtc Set to 0/1 to disable/enable the RTC clock signal in hibernate mode. Ignored when set to -1.
 * \return 0 when ok or a negative error code. Returns 0 also when nothing is to be configured or when configuration
 *  is already up to date.
 */
static int gsd_configure(int hibernate_rtc)
{
	int rv;
	uint8_t trackerConfiguration[80];
	size_t trackConfigurationLength;
	uint8_t *gpio8opt;

	if(hibernate_rtc == -1) {
		printf("Nothing to configure\n");
		return 0;
	}

	/* Get current configuration */
	rv = osp_message_get_tracker_configuration(trackerConfiguration, sizeof(trackerConfiguration), &trackConfigurationLength);
	if (rv != 0) {
		fprintf(stderr, "Failed to retrieve GSD configuration (1). rv: %d", rv);
		return rv;
	}
	debug_print_config("Current GSD configuration", trackerConfiguration, trackConfigurationLength);

	/* Check whether current configuration is already as requested */
	gpio8opt = trackerConfiguration + TRACKER_CONFIG_OFFSET__GPIO_8_OPT;

	if ((hibernate_rtc == 0 && (*gpio8opt) == TRACKER_CONFIG_GPIO_8_OPT__NO_HIBERNATE_RTC) ||
			(hibernate_rtc == 1 && (*gpio8opt) == TRACKER_CONFIG_GPIO_8_OPT__HIBERNATE_RTC)) {
		printf("GSD configuration already up to date (hibernate-rtc: %d).\n", hibernate_rtc);
		return 0;
	}

	/* Update configuration */
	(*gpio8opt) = (hibernate_rtc == 1 ? TRACKER_CONFIG_GPIO_8_OPT__HIBERNATE_RTC : TRACKER_CONFIG_GPIO_8_OPT__NO_HIBERNATE_RTC);
	debug_print_config("Updated tracker configuration", trackerConfiguration, trackConfigurationLength);

	/* And send it to GSD */
	rv = osp_message_set_tracker_configuration(trackerConfiguration, trackConfigurationLength);
	if (rv != 0) {
		fprintf(stderr, "Failed to set GSD configuration. rv: %d", rv);
		return rv;
	}

	/* Issue host restart and wait for OK-to-send */
	rv = osp_message_host_restart();
	if (rv != 0) {
		if (rv != GSDERROR__OSP_TIMEOUT) {
			fprintf(stderr, "Failed to host restart GSD (1). rv: %d", rv);
			return rv;
		}

		/* Sometimes we do not receive OK-to-send. Lets retry the hot restart */
		printf("Retrying host restart.\n");
		rv = osp_message_host_restart();
		if (rv != 0) {
			fprintf(stderr, "Failed to host restart GSD (2). rv: %d", rv);
			return rv;
		}
	}

	printf("GSD configuration updated (hibernate-rtc: %d).\n", hibernate_rtc);

	return 0;
}

int main(int argc, char *argv[])
{
	int rv;
	struct timeval tv;

	if (argc < 2) {
		usage(argv[0]);
		return 1;
	}

	parse_opts(argc - 1, argv);

	rv = tty_data_init(argv[argc-1]);
	if (rv) {
		fprintf(stderr, "Failed to initialize tty: \"%s\". error: %d\n", argv[argc-1], rv);
		rv = 2;
		goto finish;
	}

	/* set the timeout for the rx */
	tv.tv_sec = 10;
	tv.tv_usec = 0;

	/*
	 * Wait for a message. We don't care what it is. When opening the tty resulted in powering on the gsd, we will
	 * need to wait for ACK to send message.
	 */
	if (osp_transport_receive(NULL, 0, NULL, &tv) != GSDERROR__OSP_PAYLOAD_TOO_LONG)
	{
		rv = 3;
		fprintf(stderr, "Failed to wait for OSP message. error: %d\n", rv);
		goto finish;
	}

	/* Configure the GSD */
	if (gsd_configure(hibernate_rtc) != 0)
	{
		rv = 4;
		goto finish;
	}

finish:
	tty_data_deinit();
	return rv;
}
