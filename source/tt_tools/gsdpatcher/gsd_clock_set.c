/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <getopt.h>
#include <stdlib.h>
#include <time.h>

#include "gsddefs.h"
#include "tty_data.h"
#include "osp_transport.h"
#include "osp_messages.h"
#include "crc.h"
#include "gsd_rtc_defs.h"

int debug_bitmask = 0;

static void usage(const char* argv0) {
	printf("Usage:\n\n");
	printf("%s [-d <debug_bitmask>] <tty-device>\n\n", argv0);
	printf("debug_bitmask: Each bit defines type of debug messages.\n");
	printf("               %d enables all debug messages\n\n", GSDDEBUG__ALL);
	printf("tty-device   : The tty device to use. Example /dev/ttyGSD5xp0\n\n");
}

static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "debug",  1, 0, 'd' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "d:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'd':
			debug_bitmask = atoi(optarg);
			break;

		default:
			printf("unknown option\n");
			usage(argv[0]);
			exit(1);
			break;
		}
	}
}

int main(int argc, char *argv[])
{
	int rv;
	time_t gps_time;
	msg7_t time_data;
	struct timeval tv;

	if (argc < 2) {
		usage(argv[0]);
		return 1;
	}

	parse_opts(argc - 1, argv);

	rv = tty_data_init(argv[argc-1]);
	if (rv) {
		fprintf(stderr, "Failed to initialize tty: \"%s\". error: %d\n", argv[argc-1], rv);
		rv = 2;
		goto finish;
	}

	// set the timeout for the rx
	tv.tv_sec = 10;
	tv.tv_usec = 0;

	//Wait for a message. We don't care what it is. When opening the tty resulted in powering on the gsd, we will
	//need to wait for ACK to send message.
	if (osp_transport_receive(NULL, 0, NULL, &tv) != GSDERROR__OSP_PAYLOAD_TOO_LONG)
	{
		rv = 3;
		fprintf(stderr, "Failed to wait for OSP message. error: %d\n", rv);
		goto finish;
	}

	rv = osp_message_poll_clock_status(&time_data);
	if (rv == 0) {
		struct tm *gtime;

		gps_time = GPS_EPOCH + (ntohs(time_data.week) * (7*60*60*24) + ((double)ntohl(time_data.tow)*1e-2)) - DELTA_UTC;
		gtime = gmtime(&gps_time);
		if (gtime) {
			if (debug_bitmask) {
				printf ("GPS Clock status: seconds from epoch = %u\n", gps_time);
				printf ("GPS Clock status: week = 0x%04x\n", ntohs(time_data.week));
				printf ("GPS Clock status: tow = 0x%08x\n", ntohl(time_data.tow));
				printf ("GPS Clock status: time = %2d:%02d\n", gtime->tm_hour, gtime->tm_min);
				printf ("GPS Clock status: date = %2d/%2d/%4d\n", gtime->tm_mday, gtime->tm_mon, gtime->tm_year + 1900);
			}

			/* If the GPS time is < 2014 then it's invalid, so use the dafult value */
			if ((gtime->tm_year + 1900) < 2014)
				gps_time = DEFAULT_RTC_DATE;

			/* Set the system time */
			stime(&gps_time);
		} else {
			rv = 4;
			fprintf(stderr, "Failed to parse time info. error: %d\n", rv);
		}
	} else {
		fprintf(stderr, "Failed to poll clock status. error: %d\n", rv);
		rv = 5;
	}

finish:
	tty_data_deinit();
	return rv;
}
