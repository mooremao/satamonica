/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 * Code based on sirfgps_test and Watch code.
 */
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>

#include "patch_file_info.h"
#include "gsddefs.h"
#include "osp_messages.h"

extern unsigned char patchfile_bin[];
extern unsigned int patchfile_bin_len;

int patch_file_info_get_info(patch_file_info_t *pPFInfo, uint16_t offset, bool update_k32)
{
	const uint8_t *p = NULL;
	uint32_t cur = 0;
	uint32_t seg, seglen;

	/* Check patch file size. For now, it should at least contain a header */
	if(offset + PATCH_FILE_HEADERINFO_SIZE >= patchfile_bin_len)
	{
		return GSDERROR__INVALID_PATCH_OFFSET;
	}

	p = patchfile_bin + offset;

	/* Check to make sure patch file has proper start header */
	if ((p[0] != 'P') || (p[1] != 'D'))
	{
		return GSDERROR__INVALID_PATCH_FILE;
	}
	p += 2;

	pPFInfo->chipId = ntohs(*((uint16_t*) p));
	p += 2;
	pPFInfo->siliconVersion = ntohs(*((uint16_t*) p));
	p += 2;
	pPFInfo->romVersionCode = ntohs(*((uint16_t*) p));
	p += 2;
	pPFInfo->patchRevisionCode = ntohs(*((uint16_t*) p));
	p += 2;
	pPFInfo->patchDataBaseAddress = ntohl(*((uint32_t*) p));
	p += 4;
	pPFInfo->patchDataLength = ntohs(*((uint16_t*) p));
	p += 2;

	/* Check length: */
	if(offset + PATCH_FILE_HEADERINFO_SIZE + pPFInfo->patchDataLength >= patchfile_bin_len)
	{
		return GSDERROR__INVALID_PATCH_DATA_LENGTH;
	}

	/* Find the patch ram offset */
	p = patchfile_bin + offset + PATCH_FILE_HEADERINFO_SIZE;
	if(!update_k32)
	{
		while (cur < pPFInfo->patchDataLength)
		{
			seg = ntohl(*((uint32_t*) p));
			p += 4;
			if (seg < 0x200000)
			{
				p++;
				seglen = *p;
				p++;
			}
			else
			{
				pPFInfo->patchRAMStartOffset = cur;
				break;
			}

			p += seglen;
			cur += seglen + 6;
		}
	}
	else
	{
		while (cur < pPFInfo->patchDataLength)
		{
			seglen = ntohs(*((uint16_t*) p));
			p += 2;
			if (cur==0)
			{
			/* Do nothing here */
			}
			else
			{
				pPFInfo->patchRAMStartOffset = cur;
				break;
			}
			p += seglen;
			cur += seglen + 2;
		}
	}

	if(pPFInfo->patchRAMStartOffset == 0) {
		return GSDERROR__INVALID_RAM_START_OFFSET;
	}

	return 0;
}

int patch_file_info_read_overlay (uint8_t *pResult, size_t maxResultLength, uint16_t offset,
		const patch_file_info_t *pPFInfo)
{
	uint16_t patch_ram_offset;
	uint32_t overlay_crc;

	patch_ram_offset = pPFInfo->patchRAMStartOffset;

	if(patch_ram_offset > maxResultLength)
	{
		return GSDERROR__RAM_OFFSET_TOO_LARGE;
	}

	memcpy(pResult, patchfile_bin + PATCH_FILE_HEADERINFO_SIZE + offset, patch_ram_offset);

	return (int) patch_ram_offset;
}

int patch_file_info_read_non_overlay (uint8_t *pResult, size_t maxResultLength, uint16_t offset,
		const patch_file_info_t *pPFInfo, uint32_t non_overlay_offset)
{
	uint32_t read_num;
	uint32_t patch_length, patch_ram_offset;

	patch_length = pPFInfo->patchDataLength;
	patch_ram_offset =pPFInfo->patchRAMStartOffset;
	read_num = patch_length - non_overlay_offset - patch_ram_offset;
	if(read_num > OSP_MAX_SUBSEQUENT_PATCH_MEMORY_LOAD_SIZE)
	{
		read_num = OSP_MAX_SUBSEQUENT_PATCH_MEMORY_LOAD_SIZE;
	}

	if(read_num > maxResultLength) {
		read_num = maxResultLength;
	}

	memcpy(pResult, patchfile_bin + PATCH_FILE_HEADERINFO_SIZE + offset + patch_ram_offset +
			non_overlay_offset, read_num);

	return read_num;
}
