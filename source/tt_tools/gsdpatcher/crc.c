/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "crc.h"

/* Pre-processor definitions */
#define P_16        0xA001
#define P_32        0xEDB88320L
#define PRE_CRC16	 0xFFFF
#define PRE_CRC32 	 0xFFFFFFFF

/* Static variable declaration */
static uint8_t crc_tab16_init = 0;
static uint8_t crc_tab32_init = 0;
static uint16_t crc_tab16[256];
static uint32_t crc_tab32[256];

/* Static function definitions */
/* Create the crc table for CRC16 */
static void init_crc16_tab()
{
    uint16_t i, j;
    uint16_t crc, c;

    for (i=0; i<256; i++) 
	{
        crc = 0;
        c   = (uint16_t) i;
        for (j=0; j<8; j++) 
		{
            if ( (crc ^ c) & 0x0001 )
			{
            	crc = ( crc >> 1 ) ^ P_16;
			}
            else
            {
            	crc = crc >> 1;
            }

            c = c >> 1;
        }
        crc_tab16[i] = crc;
    }
	
    crc_tab16_init = 1;

}

static void init_crc32_tab()
{
    uint32_t i, j;
    uint32_t crc;

    for (i=0; i<256; i++) 
	{
        crc = (uint32_t) i;

        for (j=0; j<8; j++) 
		{
            if ( crc & 0x00000001L )
			{
            	crc = ( crc >> 1 ) ^ P_32;
			}
            else
			{
            	crc = crc >> 1;
			}
        }

        crc_tab32[i] = crc;
    }

    crc_tab32_init = 1;

}

static uint16_t crc_16( unsigned short crc, char c ) 
{
    uint16_t tmp, short_c;

    short_c = 0x00ff & (uint16_t) c;

    if (!crc_tab16_init)
    {
    	init_crc16_tab();
    }

    tmp =  crc       ^ short_c;
    crc = (crc >> 8) ^ crc_tab16[ tmp & 0xff ];

    return crc;

}

static uint32_t crc_32( unsigned long crc, char c )
{
    uint32_t tmp, long_c;

    long_c = 0x000000ff & (uint32_t) c;

    if ( ! crc_tab32_init )
	{
    	init_crc32_tab();
	}

    tmp = crc ^ long_c;
    crc = (crc >> 8) ^ crc_tab32[ tmp & 0xff ];

    return crc;

}

uint16_t gps_update_crc16(uint8_t *pData, int32_t len, uint16_t pre_crc)
{	
	/* NOTE: set pre_crc = PRE_CRC16 for the first time call this function*/
	uint16_t crc = pre_crc;
	int32_t i = 0;
	while(i < len)
	{
		crc = crc_16(crc, pData[i] );
		i++;
	}
	return crc;
}

uint16_t gps_calculate_crc16(uint8_t *pData, int32_t len)
{	
	uint16_t crc = PRE_CRC16;
	int32_t i = 0;
	while(i < len)
	{
		crc = crc_16(crc, pData[i] );
		i++;
	}
	return crc;
}

uint32_t gps_update_crc32(uint8_t *pData, int32_t len, uint32_t pre_crc)
{	
	uint32_t crc = pre_crc;
	int32_t i = 0;
	while(i < len)
	{
		crc = crc_32(crc, pData[i] );
		i++;
	}
	return crc;
}

uint32_t gps_finalize_crc32 (uint32_t pre_crc)
{
	return (pre_crc ^ PRE_CRC32);
}

uint32_t gps_calculate_crc32(uint8_t *pData, int32_t len)
{	
	uint32_t crc = PRE_CRC32;
	int32_t i = 0;
	while(i < len)
	{
		crc = crc_32(crc, pData[i] );
		i++;
	}
	return (crc ^ PRE_CRC32);
}
