/**
 * Copyright 2014 TomTom B.V. All rights reserved.
 *
 */
#ifndef OSP_MESSAGES_H_
#define OSP_MESSAGES_H_

#include <stdint.h>
#include <stdbool.h>

#include "patch_file_info.h"

#define OSP_MAX_INITIAL_PATCH_MEMORY_LOAD_SIZE (998 - 4) //Minus CRC
#define OSP_MAX_SUBSEQUENT_PATCH_MEMORY_LOAD_SIZE (1012 - 4) //Minus CRC

#define OSP_MAX_VERSION_STR_LENGTH 80

typedef struct
{
	uint16_t chip_id;
	uint16_t silicon_id;
	uint16_t rom_version;
	uint16_t patch_revision;
} patch_info_t;

typedef struct  __attribute__((__packed__))
{
  uint8_t  msgId;
  uint16_t week;
  uint32_t tow;
  uint8_t  svs;
  uint32_t drift;
  uint32_t bias;
  uint32_t time;
} msg7_t;

/**
 * \brief Request the clock data from the GPS device
 *
 * \param[in][out] pData Storage location for the received data
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_poll_clock_status(msg7_t *pData);

/**
 * \brief Request the software version and waits for the response
 *
 * \note: GSD does not response to this message when requesting the software version directly
 *        after patching and waiting for ok-to-send. So therefore we allow retries.
 *
 * \param[out] pSoftwareVersion Storage location for the received software version
 * \param[in] pSoftwareVersionLength Size of the received software version storage location
 * \param[out] pCustomerSoftwareVersion Storage location for the received customer software version
 * \param[in] pCustomerSoftwareVersionLength Size of the received customer software version storage location
 * \param[in] Number of retries before giving up.
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_poll_software_version(char* pSoftwareVersion, size_t pSoftwareVersionLength,
		char* pCustomerSoftwareVersion, size_t pCustomerSoftwareVersionLength, int retries);

/**
 * \brief Performs the kalimba workaround.
 *
 * \note: Before patching we need to apply a kalimba workaround.
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_kalimba_workaround();

/**
 * \brief Sets the patch storages location to internal RAM instead of (not connected) EEPROM.
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_set_patch_storage();

/**
 * \brief Polls the patch revision. Also consider as start condition for patching.
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_poll_patch_revision(patch_info_t *pResult);

/**
 * \brief Sends a part of the patch to the GSD
 *
 * \param[in] pData The part of the data
 * \param[in] len The length of the data
 * \param[in] seq Sequence number starting from 1.
 * \param[in] pPatchFileData The patch file data info
 * \param[in] update_k32 Should be true for updating kalimba/k32
 * \param[in] pCRC When not NULL, the CRC to add to the message.
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_patch_memory_load(const uint8_t *pData, uint32_t len, uint32_t seq,
		const patch_file_info_t* pPatchFileData, bool update_k32, uint32_t *pCRC);

/**
 * \brief Sends patch exit message to GSD
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_patch_exit();

/**
 * \brief Waits for 'ok-to-send' message from GSD.
 *
 * \return 0 when ok or a negative error code
 */
int wait_for_ok_to_send();

/**
 * \brief send factory_reset cmd and wait for 'ok-to-send' message from GSD.
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_factory_reset();

/*
 * \biref send host restart cmd and wait for 'ok-to-send'.
 *
 * Needed after updating tracker configuration.
 *
 * \return 0 when ok or a negative error code
 */
int osp_message_host_restart();

/**
 * \brief Requests the tracker configuration from the GSD and copies the track configuration to
 * the given pTrackerConfiguration for maximum pMaxTrackerConfiguration bytes.
 * The actual number of bytes copied is set to pTrackConfigurationLength when not NULL.
 *
 * \param[out] pTrackerConfiguration The location to copy the retrieved configuration to.
 * \param[in] maxTrackerConfigurationLength The maximum bytes to copy allowed.
 * \param[out] Set to the number of bytes copied.
 * \return 0 when ok or a negative error code
 */
int osp_message_get_tracker_configuration(uint8_t* pTrackerConfiguration,
		size_t maxTrackerConfigurationLength, size_t* pTrackConfigurationLength);

/**
 * \brief Sets the tracker configuration.
 *
 * \param[int] pTrackerConfiguration The tracker configuration to send
 * \param[in] trackerConfigurationLength The length of the configuration in bytes
 * \return 0 when ok or a negative error code
 */
int osp_message_set_tracker_configuration(const uint8_t* pTrackerConfiguration,
		size_t trackerConfigurationLength);

#endif /* OSP_MESSAGES_H_ */
