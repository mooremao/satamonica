/*
 * Copyright (C) 2015 TomTom BV <http://www.tomtom.com/>
 * Author: Niels Langendorff <niels.langendorff@tomtom.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define BUF_SIZE	256

/*
 * This code parses a pps cfg file and writes it to the minidsp codec
 *
 * Part of (ASCII) IIC format we are interested in looks like this
 * w 30 00 00
 * > 01
 *
 * layout of data is
 * w [30] [DATA0] [DATA1]
 * and
 * > [DATAn]
 * > [DATAn+1]
 *
 * The parser converts (for example the above) data (ASCII) into binary data
 *
 * 0x00 0x00 0x01
 *
 * comments in the pps cfg file are starting with #
 * #			# reg[0][18] = 0x84                 ; reg(0)(0x12 => 18)
 *
 * reads are starting with
 * r [30]
 * and are not used
 */

int main(int argc, char ** argv)
{
	FILE * pFileIn;
	FILE * pFileOut;
	int ret = 0;
	unsigned int addr = 0;
	unsigned int data = 0;
	char buf[BUF_SIZE];

	if (argc != 3) {
		printf ("Usage %s [in] [out]\n", argv[0]);
		printf ("Example:\n\n");
		printf ("%s minidsp.cfg /dev/minidsp\n", argv[0]);

		goto error;
	}

	pFileIn = fopen(argv[1], "r");
	if (pFileIn == NULL) {
		ret = errno;
		snprintf(buf, BUF_SIZE, "Error opening %s for reading", argv[1]);
		perror(buf);
		goto error_in;
	}

	pFileOut = fopen(argv[2], "r+");
	if (pFileOut  == NULL) {
		ret = errno;
		snprintf(buf, BUF_SIZE, "Error opening %s for writing", argv[2]);
		perror(buf);
		goto error_out;
	}

	while (fgets(buf, BUF_SIZE, pFileIn) != NULL) {
		int cnt;

		switch (buf[0]) {
			case 'w':
				cnt = sscanf(buf, "w 30 %02x %02x", &addr, &data);
				if (cnt == 2) {
					buf[0] = (unsigned char)addr;
					buf[1] = (unsigned char)data;

					if (fwrite(buf, 1, 2, pFileOut) != 2) {
						ret = errno;
						snprintf(buf, BUF_SIZE, "Error writing to %s", argv[2]);
						perror(buf);
						goto error_out_write;
					}
					fflush(pFileOut);
				}
				break;
			case '>':
				cnt = sscanf(buf, "> %02x", &data);
				if (cnt == 1) {
					addr++;
					buf[0] = (unsigned char)addr;
					buf[1] = (unsigned char)data;
					if (fwrite(buf, 1, 2, pFileOut) != 2) {
						ret = errno;
						snprintf(buf, BUF_SIZE, "Error writing to %s", argv[2]);
						perror(buf);
						goto error_out_write;
					}
					fflush(pFileOut);
				}
				break;
			case '#':
			case 'r':
			default:
				break;
		}
	}

error_out_write:
	fclose(pFileOut);
error_out:
	fclose(pFileIn);
error_in:
error:
	return ret;
}
