#include <stdio.h>
#include <fcntl.h>
#include <linux/input.h>

main() {

FILE *fd;
char *batlock = "/dev/input/by-path/platform-batlock-keys-event";
struct input_event keyev;

	if ((fd = fopen(batlock, "r")) == NULL)
	{
		perror("Error opening platform-batlock-keys-event");
	}
	else
	{
		while (fread(&keyev, sizeof(struct input_event), 1, fd) > 0)
		{
			printf("the key is %d and state %d \n", keyev.code, keyev.value);
			if ((keyev.code == 236) && (keyev.value == 1))
				printf("Battery stick removed, take action!!\n");
			sleep(1);
		}
		fclose(fd);
	}
}
