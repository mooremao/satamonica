# Copies scripts and binaries required for ubifs stress test to the file system
# do a make ubifs and flash file system after running this script

case $1 in
	"largefile")
		cp ubifs_stress_single_large_file.sh ../../ipnc_rdk/target/filesys/ubifs_stress_test.sh
		cp stress.service ../../ipnc_rdk/target/filesys/etc/systemd/system/
		cp run_stress_test.sh ../../ipnc_rdk/target/filesys/opt/ipnc/
		cd ../../ipnc_rdk/target/filesys/etc/systemd/system/default.target.wants
		ln -s ../stress.service stress.service
		cd ../../../../../../
		make ubifs
		;;

	"filesys")
		cp ubifs_stress_filesystem_extract.sh ../../ipnc_rdk/target/filesys/ubifs_stress_test.sh
		cp metalog.conf ../../ipnc_rdk/target/filesys/etc/
		cp logdata.service ../../ipnc_rdk/target/filesys/etc/systemd/system/
		cp stress.service ../../ipnc_rdk/target/filesys/etc/systemd/system/
		cp run_stress_test.sh ../../ipnc_rdk/target/filesys/opt/ipnc/
		cd ../../ipnc_rdk/target/filesys/etc/systemd/system/default.target.wants
		ln -s ../stress.service stress.service
		cd ../../../../../../
		make ubifs
		;;

	"uninstall")
		rm ../../ipnc_rdk/target/filesys/ubifs_stress_test.sh
		rm ../../ipnc_rdk/target/filesys/etc/systemd/system/stress.service
		rm ../../ipnc_rdk/target/filesys/etc/systemd/system/default.target.wants/stress.service
		rm ../../ipnc_rdk/target/filesys/opt/ipnc/run_stress_test.sh
		cd ../../ext_tools/
		make metaloginstall
		make systemdinstall
		cd ../ipnc_rdk/
		make ubifs
		;;

	*)
		echo ""
		echo "Usage : " $0 "<option>"
		echo "option : largefile | filesys"
		echo ""
		echo "Note: If using filesys option, copy a tar file testfilesys.tar.gz on the SD card"
		echo "This file will be extracted as a part of the test script"
		echo ""
		;;
esac
