# UBIFS_stress_single_large_file.sh
# This script is useful to evaluate robustness of UBIFS against continuous reboots and large file writings procedures
# It can be described with the following steps
# 1.	Check number of iteration (content of /mnt/mmc/loop.count)
# 2.	Increase by 1 the content of /mnt/mmc/loop.count (it has 0 in 1st iteration)
# 3.	If at 1st iteration, create a file of 80MB (/mnt/reserved/m-tst.bin.1)
# 4.		Calculate md5sum and save it in a file (/mnt/mmc/md5sum.1)
# 5.		Sync and cache invalidation
# 6.		Reboot
# 7.	else
# 8.		Check existence of files /mnt/reserved/m-tst.bin.<n.iteration-1> and </mnt/mmc/md5sum.<n.iteration-1>
# 9.		Recalculate md5sum of file /mnt/reserved/m-tst.bin.<n.iteration-1> and comparison with the content of file </mnt/mmc/md5sum.<n.iteration-1>
# 10.		If comparison is OK,  create a new file of 80MB (/mnt/reserved/m-tst.bin.<n.iteration>) and its </mnt/mmc/md5sum.<n.iteration>
# 11.		Reboot
# If any error occurs at steps 8, 9 and 10 system halts.

# Some defines
LOOP_COUNT_FILE=/mnt/mmc/loop.count
BLOCK_SIZE=2K
COUNT=40K
LOG_FILE=/mnt/mmc/ubifs_stress_single_large_file.log
VERSION=1.0.0

if [ ! -f $LOOP_COUNT_FILE ]; then
	# 1st run
	rm -f /mnt/reserved/m-tst.bin*
	echo 0 > $LOOP_COUNT_FILE
	> $LOG_FILE
	echo "[$0] - VERSION = $VERSION" | tee -a $LOG_FILE
fi

LOOP_COUNT=$(cat $LOOP_COUNT_FILE)
LOOP_COUNT=$(($LOOP_COUNT+1))
OUT_FILE=/mnt/reserved/m-tst.bin.$LOOP_COUNT
MD5SUM_FILE=/mnt/mmc/md5sum.$LOOP_COUNT

# Check whether this script runs for the first time or not
if [ _"$LOOP_COUNT" != _"1" ]; then
	# We have to check the existence of previous test file and its md5sum
	if [ ! -f /mnt/reserved/m-tst.bin.$(($LOOP_COUNT-1)) ]; then
		echo "$LOOP_COUNT. After a reboot /mnt/reserved/m-tst.bin.$(($LOOP_COUNT-1)) is not here anymore! Alert!" | tee -a $LOG_FILE
		echo "$LOOP_COUNT. Halting system" | tee -a $LOG_FILE
		poweroff &
		exit
	else
		echo "$LOOP_COUNT. After a reboot /mnt/reserved/m-tst.bin.$(($LOOP_COUNT-1)) is still here" | tee -a $LOG_FILE
		if [ ! -f /mnt/mmc/md5sum.$(($LOOP_COUNT-1)) ]; then
			echo "$LOOP_COUNT. After a reboot /mnt/mmc/md5sum.$(($LOOP_COUNT-1)) is not here anymore! Alert!" | tee -a $LOG_FILE
			echo "$LOOP_COUNT. Halting system" | tee -a $LOG_FILE
			poweroff &
			exit
		else
			echo "$LOOP_COUNT. After a reboot /mnt/mmc/md5sum.$(($LOOP_COUNT-1)) is still here" | tee -a $LOG_FILE
			md5sum -c /mnt/mmc/md5sum.$(($LOOP_COUNT-1))
			if [ $? != 0 ] ; then
				echo "$LOOP_COUNT. md5sum now calculated is different from the one of the previous boot. Alert!" | tee -a $LOG_FILE
				echo "$LOOP_COUNT. Halting system" | tee -a $LOG_FILE
				poweroff &
				exit
			else
				echo "$LOOP_COUNT. md5sum now calculated is the same as the previous boot. Let's go on" | tee -a $LOG_FILE
				rm -f /mnt/reserved/m-tst.bin.$(($LOOP_COUNT-1)) /mnt/mmc/md5sum.$(($LOOP_COUNT-1))

			fi
		fi
	fi
fi

# New filenames
OUT_FILE="/mnt/reserved/m-tst.bin".$LOOP_COUNT
MD5SUM_FILE="/mnt/mmc/md5sum".$LOOP_COUNT

# Creating test file with random content
echo "$LOOP_COUNT. Creating test file with random content..." | tee -a $LOG_FILE
dd if=/dev/urandom of=$OUT_FILE bs=$BLOCK_SIZE count=$COUNT
sleep 2

# Syncing
echo "$LOOP_COUNT. Syncing..." | tee -a $LOG_FILE
sync

# Invalidating caches
echo "$LOOP_COUNT. Invalidating caches..." | tee -a $LOG_FILE
echo 3 > /proc/sys/vm/drop_caches

# Calculating md5sum and writing it to file
echo "$LOOP_COUNT. Calculating md5sum..." | tee -a $LOG_FILE
MD5SUM=$(md5sum $OUT_FILE)
MD5SUM1=${MD5SUM}
echo "$LOOP_COUNT. MD5SUM($OUT_FILE)=$MD5SUM1" | tee -a $LOG_FILE

md5sum $OUT_FILE > $MD5SUM_FILE

echo $LOOP_COUNT > $LOOP_COUNT_FILE

# Syncing
echo "$(($LOOP_COUNT)). Syncing..." | tee -a $LOG_FILE
sync

# Rebooting
echo -e "$(($LOOP_COUNT)). Rebooting...\n" | tee -a $LOG_FILE
reboot
