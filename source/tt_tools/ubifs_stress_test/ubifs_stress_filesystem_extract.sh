# UBIFS_stress_filesystem_extract.sh
# This tool is useful to evaluate robustness of UBIFS against continuous reboots and writing large number of small files
# It can be described with the following steps
# 1.	Check number of iteration (content of /mnt/mmc/loop.count)
# 2.	Increase by 1 the content of /mnt/mmc/loop.count (it has 0 in 1st iteration)
# 3.	If /mnt/reserved/testfilesys folder exists, verify md5 checksum of each file in folder
#	If mismatch or md5 checksum is missing, halt
# 4.	Remove /mnt/reserved/testfilesys
# 5.	Create /mnt/reserved/testfilesys. Extract /mnt/mmc/testfilesys.tar.gz to /mnt/reserved/testfilesys
# 6.	For each file in /mnt/reserved/testfilesys write md5 checksum
# 7.	Sync and Reboot

LOOP_COUNT_FILE=/mnt/mmc/loop.count
LOG_FILE=/mnt/mmc/ubifs_stress_filesystem_extract.log
VERSION=1.0.0

if [ ! -f $LOOP_COUNT_FILE ]; then
	# 1st run
	rm -f /mnt/reserved/testfilesys
	echo 0 > $LOOP_COUNT_FILE
	> $LOG_FILE
	echo "[$0] - VERSION = $VERSION" | tee -a $LOG_FILE
fi

LOOP_COUNT=$(cat $LOOP_COUNT_FILE)
LOOP_COUNT=$(($LOOP_COUNT+1))

# Check whether this script runs for the first time or not
if [ _"$LOOP_COUNT" != _"1" ]; then
	/opt/ipnc/smart_mount UBIFS /dev/mtd8 /mnt/reserved
	# We have to check the existence of previous test file and its md5sum
	if [ ! -d /mnt/reserved/testfilesys ]; then
		echo "$LOOP_COUNT. After a reboot /mnt/reserved/testfilesys is not here anymore! Alert!" | tee -a $LOG_FILE
		echo "$LOOP_COUNT. Halting system" | tee -a $LOG_FILE
		halt &
		exit
	else
		echo "$LOOP_COUNT. After a reboot /mnt/reserved/testfilesys is still here" | tee -a $LOG_FILE
		cd /mnt/reserved/testfilesys
		for f in `find .`; do
			substring="md5sum"
			if [ -d $f ]; then
				echo ""
			elif [ -L $f ]; then
				echo ""
			elif [ -f $f ]; then
				case "$f" in
					*md5sum*)
						#echo "Skipping md5sum file" $fi ;;
						;;
					*)

						if [ ! -f $f".md5sum" ] ; then
							echo "$LOOP_COUNT. After a reboot" $f".md5sum is not here anymore! Alert!" | tee -a $LOG_FILE
							echo "$LOOP_COUNT. Halting system" | tee -a $LOG_FILE
							halt &
							exit
						fi

						md5sum -c $f".md5sum" | tee -a $LOG_FILE
						if [ $? != 0 ] ; then
							echo "$LOOP_COUNT. $f calculated md5sum is different from previous boot" | tee -a $LOG_FILE
							echo "$LOOP_COUNT. Halting system" | tee -a $LOG_FILE
							halt &
							exit
						fi
						;;
				esac
			fi
		done
	fi
fi

# Remove old filesystem and untar again
cd /
logdump /mnt/mmc
sync
echo 3 > /proc/sys/vm/drop_caches
sync
umount /mnt/reserved/
ubidetach /dev/ubi_ctrl -p /dev/mtd8
ubiformat /dev/mtd8 -s 2048 -O 2048
ubiattach /dev/ubi_ctrl -p /dev/mtd8 -O 2048
ubimkvol /dev/ubi2 -N ubifs_volume -m
mount -t ubifs ubi2:ubifs_volume /mnt/reserved

cd /mnt/reserved

tar -xvf /mnt/mmc/testfilesys.tar.gz | tee -a $LOG_FILE
cd /mnt/reserved/testfilesys
for f in `find . -type f`; do
	if [ -d $f ]; then
		echo $f : "Directory"
	elif [ -L $f ]; then
		echo $f : "Link : Skipping"
	elif [ -f $f ]; then
		md5sum $f > $f.md5sum
	fi
done

# Syncing
echo "$LOOP_COUNT. Syncing..." | tee -a $LOG_FILE
sync

# Invalidating caches
echo "$LOOP_COUNT. Invalidating caches..." | tee -a $LOG_FILE
echo 3 > /proc/sys/vm/drop_caches

echo $LOOP_COUNT > $LOOP_COUNT_FILE

# Syncing
echo "$(($LOOP_COUNT)). Syncing..." | tee -a $LOG_FILE
sync

# Rebooting
echo -e "$(($LOOP_COUNT)). Rebooting...\n" | tee -a $LOG_FILE
reboot
