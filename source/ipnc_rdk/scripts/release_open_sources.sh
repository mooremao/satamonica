#!/bin/bash -ue
# ------------------------------------------------------------------------------
#
# Script    : release.sh
#
# Desription: Creates a tarball of all external sources with open-source license
#
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Constants
# ------------------------------------------------------------------------------
THIS=`basename $0`
TERMINAL_WIDTH=`tput cols`
NORMAL=`printf "\033[0;0;0m"`   # normal text
DIMMED=`printf "\033[2;37m"`    # dimmed text
MODULE_LICENSE_PREFIX="MODULE_LICENSE_"
BUILD_DIR="ipnc_rdk"
BUILD_OUTPUT_DIR="$BUILD_DIR/target"
RELEASE_OUTPUT_DIR="$BUILD_OUTPUT_DIR/release"
PRODUCT_OUTPUT_DIR="$BUILD_OUTPUT_DIR/filesys"
MODULE_INFO_FILE="$BUILD_OUTPUT_DIR/module-info.txt"
CREATE_MODULE_SCRIPT="$BUILD_DIR/scripts/createModuleInfo.py"
PARAM_MODULE_INSTALL="INSTALLED"
PARAM_MODULE_NAME="NAME"
PARAM_MODULE_PATH="PATH"
let NUM_COLS=4

# ------------------------------------------------------------------------------
# Variables
# ------------------------------------------------------------------------------
CREATE_OUTPUT_FILE=0
CREATE_WARNING_FILE=0
CREATE_XML_OUT_FILE=0
SHOW_SKIPPED_MODULES=0
SHOW_LICENSE_SUMMARY=0
RUN_IN_TEST_MODE=0
SHOWN_LICENSES=""
MODULES_TO_RELEASE=`mktemp`
XML_OUTFILE=""
LICENSE_TYPES=""
TARBALL_FILE=""
OUTPUT_FILE=""
WARNING_FILE=""
MODULE_LICENSE=""
MODULE_INSTALL=""
MODULE_NAME=""
MODULE_PATH=""
LEN_COL_1=0
LEN_COL_2=0
LEN_COL_3=0
LEN_COL_4=0
LINENO=0
INCLUDED=0
EXCLUDED=0
TOTAL=0
ERROR=0
WARNING=0
EXCLUDE_PATTERNS=""
INCLUDE_PATTERNS=""

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------
usage()
{
    cat << EOF
    Usage: $THIS <OPTIONS>...

    This script creates a tarball of all external sources with given license type(s).

    OPTIONS:
       -h               Show this help and exit
       -t               Test (do not create a tarball, just show what would be done)
       -i <pattern>     Include modules matching given pattern (e.g., external)
       -e <pattern>     Exclude modules matching given pattern (e.g., tomtom)
       -l <license>     Process only modules with given license (e.g., GNU)
       -w               Create warning-file for modules without license-files
       -o               Write output also to file (included/excluded modules). Output will be
                        written to $(echo $THIS | cut -d\. -f1).out
       -s               Show skipped (not included) modules
       -b               Show summary of licenses
       -x               Create License Info XML file. Output will be written to $(echo $THIS | cut -d\. -f1).xml

    EXAMPLE:
        $THIS  -i external -e tomtom -l GNU -l BSD -d "tag1,tag2"

EOF
}

# Returns the minimum of given values
# $1: Value 1
# $2: Value 2
function min {
    [ $1 -lt $2 ] && echo $1 || echo $2
}

# Returns the maximum of given values
# $1: Value 1
# $2: Value 2
function max {
    [ $1 -gt $2 ] && echo $1 || echo $2
}

# Prints a line on the terminal with given length
# $1: Character to use for printing
# $2: Length of the line to print
function printLine {
    local -i WIDTH=$2-3
    printf "  "  # leave 2 spaces before
    printf "$1%.s" $(eval echo {1..$WIDTH})
    printf " \n" # leave 1 space after
}

# Prints a row on the terminal with given values
# $1: Prefix
# $2: Field 1
# $3: Field 2
# $4: Field 3
# $5: Field 4
function printTableRow {
    printf "$1 %-${LEN_COL_1}s | %-${LEN_COL_2}s | %-${LEN_COL_3}s | %s\n" \
           "`trimRight $LEN_COL_1 $2`" \
           "`trimRight $LEN_COL_2 $3`" \
           "`trimRight $LEN_COL_3 $4`" \
           "`trimLeft  $LEN_COL_4 $5`"
}

# Prints a row to output-file with given values
# $1: Prefix
# $2: Field 1
# $3: Field 2
# $4: Field 3
# $5: Field 4
function printFileRow {
    if [ -n "$OUTPUT_FILE" ]; then
        echo "+ NAME=\"$2\"" \
               "LICENSE=\"$3\"" \
               "PATH=\"$4\"" \
               "INSTALLED=\"$5\"" >> $OUTPUT_FILE
    fi
}

# Trims given string ($2) at given length ($1)
# $1: Start length
# $2: End length
# $*: String to trim
function trim {
    local -i START=$1; local -i END=$2; shift 2
    echo $* | cut -c $START-$END
}

# Returns width for a column with given min & max
# $1: Maximum total width (of the table)
# $2: Number of columns (in the table)
# $3: Desired percentage for column
# $4: Minimum width (for field)
function getColWidth {
    # NOTE: 3 is used for spaces/separators between fields
    #       and the spaces left before and after each line
    local -i WIDTH="(($1-($2*3)-3)*$3)/100"
    max $WIDTH $4
}

# Trims given string ($2) at given length ($1)
# $1: Maximum length
# $2: String to trim
function trimRight {
    local -i MAXLEN=$1; shift; local STRING=$*

    # Check whether we need to trim the string
    if [ ${#STRING} -gt $MAXLEN ]; then
        let MAXLEN=MAXLEN-3
        STRING=`trim 1 $MAXLEN $STRING`
        echo "${STRING}..."
    else
        echo $STRING
    fi
}

# Trims given string ($2) at given length ($1)
# $1: Maximum length
# $2: String to trim
function trimLeft {
    local -i MAXLEN=$1; shift; local STRING=$*

    # Check whether we need to trim the string
    if [ ${#STRING} -gt $MAXLEN ]; then
        local -i START=${#STRING}-$MAXLEN+1
        let MAXLEN=MAXLEN-3
        STRING=`trim $START ${#STRING} $STRING`
        echo "...${STRING}"
    else
        echo $STRING
    fi
}

# Checks given word ($1) in given list ($2+)
# $1: Word to check
# $2: String containing list of words
function isOneOf {
    local STRING="$1"; shift;
    echo $* | grep -qw "$STRING"
}

# Checks given words ($2+) in given string ($1)
# $1: String to check
# $2: String containing list of words
function hasOneOf {
    local STRING="$1"; shift;
    for WORD in $*; do
        if isOneOf $WORD "$STRING"; then
            return 0
        fi
    done
    return 1
}

# Checks whether dir $2 is sub-dir of $1
# $1: Dir to check (whether it's parent-dir of $2)
# $2: Dir to check (whether it's a sub-dir of $1)
function isSubdirOf {
    if [ "${2:0:${#1}}" == "$1" ]; then
        return 0  # $2 is a sub-dir of $1
    else
        return 1  # $2 is NOT a sub-dir of $1
    fi
}

# Checks whether any of given dirs ($2+) is sub-dir of $1
# $1: Dir to check (whether it's parent-dir of $2+)
# $*: Dirs to check (whether any of them are a sub-dir of $1)
function hasSubdirOf {
    local DIR; local PARENT=$1; shift

    for DIR in $*; do
        if isSubdirOf $PARENT $DIR; then
            return 0 # at least one dir is a sub-dir of $1
        fi
    done

    return 1 # none of given dirs are a sub-dir of $1
}

# Returns value of given parameter
# NOTE: For syntax see: module-info.txt
# $1: Parameter name
# $*: String in format: <name>="<value>" <name>="<value>" ...
function getParamVal {
    local NAME=$1; shift

    # NOTE: The 1st check with grep is to ensure that the parameter is there,
    #       cause otherwise "sed" will return the whole string that's given.
    if echo $* | grep -q "$NAME[[:space:]]*=[[:space:]]*\"[^\"]*\""; then
        echo $* | sed "s/.*$NAME[[:space:]]*=[[:space:]]*\"\([^\"]*\)\".*/\1/"
    else
        return 1 # parameter not found
    fi
}

# Removes duplicate values/words in given string
# $*: String with space-separated values
function removeDuplicates {
    echo `echo $* | sed "s/[[:space:]]/\n/g" | sort -u`
}

# Map MODULE_LICENSE_* to the license strings from the XSD
# enumeration values. The values mapped below are based on the current
# MODULE_LICENSE_* types in the TomTom ics-dev android source tree.
function fixupLicenseName {
    local name=$1; shift

    case ${name} in
        AFL_AND_GPL) echo 'GPL' ;;
        APACHE2)     echo 'APACHE' ;;
        APL)         echo 'APPLE_PUBLIC_SOURCE' ;;
        BSD_LIKE|BSD|BSD_OR_LGPL)
                     echo 'BSD-STYLE'
                     ;;
        GPL|GPL2)    echo 'GPL' ;;
        LGPL|LGPL2|LGPL2.1)  echo 'LGPL' ;;
        CPL|ICU|MIT|PROPRIETARY)
                     echo "${name}"
                     ;;
        MPL|MPL1.1)  echo 'MPL' ;;
        *)
                     # ILLEGAL TYPES: PUBLIC_DOMAIN, W3C, LGPL_AND_GPL,
                     # HP, BSD_AND_GPL, EPL
                     # These are illegal because they are not authorized
                     # in "TomTom Open Source Software Policy, version April 2012
                     echo "WARNING: license type ${name} is not allowed! Module: ${MODULE_PATH} !!" >&2
                     if [ $CREATE_WARNING_FILE -ne 0 ]; then
                         echo "WARNING: license type ${name} is not allowed! Module: ${MODULE_PATH} !!" >> ${WARNING_FILE}
                     fi

                     echo 'UNAUTHORIZED'
                     #let ERROR=$ERROR+1
                     ;;
    esac
}

# Map MODULE_LICENSE_* to a license version string.
# The values mapped below are based on the current MODULE_LICENSE_*
# types in the TomTom ics-dev android source tree.
function getLicenseVersion() {
    local name=$1; shift

    case ${name} in
        APACHE2|GPL2)    echo '2.0' ;;
	LGPL2)		 echo '2.1' ;;# TODO: check this!! ????
	LGPL2.1)	 echo '2.1' ;;
	MPL1.1)		 echo '1.1' ;;

	GPL)		 echo '2.0' ;; # "2.0 or later"?
	LGPL)		 echo '2.1' ;;
	# default version is 1.0
	*)		 echo '1.0' ;;
    esac
}

# $1: MOdule path (e.g., "external/speex")
function findModuleLicense {
    local MODULE="$1"; local RETVAL

    if { RETVAL=`find $MODULE -maxdepth 1 -name "$MODULE_LICENSE_PREFIX*" -printf "%f\n"`; } then
        # Ensure that there is a module-license file
        if [ -n "$RETVAL" ]; then
            echo "$RETVAL"
            return
        else
            # search in parent directories as well, like ABS does.
            # Walk upwards, but not to the top.  a/b/c -> only to a/b.
            MODULE+="/"
            while [ -n "${MODULE#*/}" ]; do
                MODULE="${MODULE%/*/}/"
                if { RETVAL=`find $MODULE -maxdepth 1 -name "$MODULE_LICENSE_PREFIX*" -printf "%f\n"`; } then
                    if [ -n "$RETVAL" ]; then
                        echo "$RETVAL"
                        return 0
                    fi
                fi
            done
        fi
    fi
}

# $1: MOdule path (e.g., "external/speex")
function getModuleLicense {
    local MODULE="$1"; local RETVAL

    if { RETVAL=`findModuleLicense $MODULE`; } then
        # Ensure that there is a module-license file
        if [ -n "$RETVAL" ]; then
            # Ensure that there is only 1 module-license file
            if [ `echo $RETVAL | wc -w` -eq 1 ]; then
                # Strip off the prefix from the module-license file-name
                if { RETVAL=`echo $RETVAL | sed "s/$MODULE_LICENSE_PREFIX//"`; } then
                    echo $RETVAL # success
                else
                    echo "ERROR: Failed to get module-license type (`echo $RETVAL`)"
                    return 1
                fi
            else
                echo "ERROR: Multiple module-license files under: $MODULE"
                return 2
            fi
        fi
    else
        echo "ERROR: Failed to search for module-license file under: $MODULE"
        return 3
    fi
}

function writeXMLLicenseEntry() {
    local license=$1; shift
    local notice=$1;shift

    if [ -n "${license}" ]; then
        local ltype="$(fixupLicenseName ${license})"
        local version="$(getLicenseVersion ${license})"

        printf '        <license>\n'
        printf '            <type>%s</type>\n' ${ltype}
        printf '            <version>%s</version>\n' ${version}
        printf '            <notice><![CDATA['

        # Note: we'll assume that the notice files are encoded in ISO-8859-1
        if [ -r "${notice}" ] && [ $(stat -c%s "${notice}") -gt 0 ]; then
            cat ${notice} | tr '\f' '\n'| iconv -f LATIN1 -t UTF-8
        else
            if [ "${ltype}" != "PROPRIETARY" ]; then
                local warnmsg="WARNING: no notice file, or empty notice file found at ${notice} !!"
                echo $warnmsg  >&2
                if [ $CREATE_WARNING_FILE -ne 0 ]; then
                    echo $warnmsg >> ${WARNING_FILE}
                fi
                printf 'EMPTY'
                let WARNING=$WARNING+1
            fi
        fi
        printf ']]></notice>\n'
        printf '        </license>\n'
    else
        printf '        <license />\n'
    fi
}

function writeXMLEntry() {
    local srcpath=$1; shift
    local license=$1; shift
    local notice=$1;shift
    local path=$1; shift

    # Note: block output is redirected
    {
        printf '    <licenseInfo>\n'
        printf '        <srcpath>%s</srcpath>\n' ${srcpath}
	printf '        <path>%s</path>\n' ${path}
        writeXMLLicenseEntry "${license}" "${notice}"
        printf '    </licenseInfo>\n'
    } >> ${XML_OUTFILE}
}

function writeXMLPrologue() {
    # Note: block output is redirected, output file is truncated if it already exists
    {
        # Note: we'll assume that the notice files are encoded in ISO-8859-1
        printf '<?xml version="1.0" encoding="UTF-8"?>'
        printf '<openSourceLicenseInfo>\n'
        printf '    <componentName>platform</componentName>\n'
        printf '    <signedOffBy>%s</signedOffBy>\n' ${USER}
        # Note: the following time format should match the xsd:dateTime format!
        printf '    <signedOffDate>%s</signedOffDate>\n' $(date +%FT%T%:z)
    } > ${XML_OUTFILE}
}

function writeXMLEpilogue() {
    # Note: block output is redirected
    {
        printf '</openSourceLicenseInfo>\n\n'
    } >> ${XML_OUTFILE}
}

function processModule {
    local MODULE_NAME=$1
    local MODULE_PATH=$2
    local MODULE_INSTALL=$3

    local -i SKIP_MODULE=$4

    # Process only if module has a license
    if { MODULE_LICENSE=`getModuleLicense $MODULE_PATH`; } then
        # Skip those modules that are not installed/released
        if [ $SKIP_MODULE -eq 0 ]; then
            if [ -z "$MODULE_LICENSE" ]; then
                echo "WARNING: No license-file found for module: $MODULE_NAME ($MODULE_PATH)" >&2
                if [ $CREATE_WARNING_FILE -ne 0 ]; then
                    echo "WARNING: No license-file found for module: $MODULE_NAME ($MODULE_PATH)" >> ${WARNING_FILE}
                fi
                let SKIP_MODULE=1
            fi
        fi
    else
        let ERROR=$ERROR+1
        let SKIP_MODULE=1
    fi

    # Skip if module matches exclude-pattern
    if hasOneOf "$MODULE_PATH" $EXCLUDE_PATTERNS; then
        let SKIP_MODULE=1
    fi

    # Process if module matches include-pattern
    if [ -n "$INCLUDE_PATTERNS" ]; then
        if ! hasOneOf "$MODULE_PATH" $INCLUDE_PATTERNS; then
            let SKIP_MODULE=1
        fi
    fi

    # Check license-type (if provided)
    if [ -n "$LICENSE_TYPES" ]; then
        if ! isOneOf "$MODULE_LICENSE" $LICENSE_TYPES; then
            let SKIP_MODULE=1
        fi
    fi

    if [ $SHOW_LICENSE_SUMMARY -eq 1 ]; then
        if [ $SKIP_MODULE -eq 0 ] && ! echo "$SHOWN_LICENSES" | grep -q "$MODULE_LICENSE"; then
            echo "$MODULE_LICENSE"
            SHOWN_LICENSES+="$MODULE_LICENSE"
        fi
    else
        # Add module to the list of modules to be released
        if [ $SKIP_MODULE -eq 0 ]; then
            # Count included modules
            let INCLUDED=$INCLUDED+1

            # Print to terminal
            printTableRow "+" "$MODULE_NAME" "$MODULE_LICENSE" "$MODULE_PATH" "$MODULE_INSTALL"
            printFileRow "+" "$MODULE_NAME" "$MODULE_LICENSE" "$MODULE_PATH" "$MODULE_INSTALL"
            case "$MODULE_LICENSE" in
                LGPL*|GPL* )
                    echo $MODULE_PATH >> $MODULES_TO_RELEASE
                    ;;
                *)
            esac

        else
            # Count skipped modules
            let EXCLUDED=$EXCLUDED+1

            if [ $SHOW_SKIPPED_MODULES -ne 0 ]; then
                # Print to terminal
                printf "$DIMMED"
                printTableRow " " "$MODULE_NAME" "$MODULE_LICENSE" "$MODULE_PATH" "$MODULE_INSTALL"
                printFileRow " " "$MODULE_NAME" "$MODULE_LICENSE" "$MODULE_PATH" "$MODULE_INSTALL"
                printf "$NORMAL"
            fi
        fi
    fi

    # Next line assumes that NOTICE file is found at module root path!
    if [ ${CREATE_XML_OUT_FILE} -ne 0 -a $SKIP_MODULE -eq 0 ]; then
        writeXMLEntry "${MODULE_NAME}" "${MODULE_LICENSE}" "${MODULE_PATH}/NOTICE" "${MODULE_INSTALL}"
    fi

    # Count number of entries processed
    let TOTAL=$TOTAL+1
}

function createModuleInfo() {
	if [ -f $MODULE_INFO_FILE ]; then
		rm $MODULE_INFO_FILE
	fi
	find . -type f -name NOTICE -exec $CREATE_MODULE_SCRIPT {} \; >> $MODULE_INFO_FILE
}

# ------------------------------------------------------------------------------
# Command-line parameters
# ------------------------------------------------------------------------------
if [ $# -gt 0 ]; then
    while getopts “hm:i:e:l:d:p:xowbst” OPTION; do
        case $OPTION in
            h) usage; exit;;
            i) INCLUDE_PATTERNS="$INCLUDE_PATTERNS $OPTARG";;
            e) EXCLUDE_PATTERNS="$EXCLUDE_PATTERNS $OPTARG";;
            l) LICENSE_TYPES="$LICENSE_TYPES $OPTARG";;
            o) let CREATE_OUTPUT_FILE=1;;
            w) let CREATE_WARNING_FILE=1;;
            s) let SHOW_SKIPPED_MODULES=1;;
            t) let RUN_IN_TEST_MODE=1;;
            b) let SHOW_LICENSE_SUMMARY=1;;
            x) let CREATE_XML_OUT_FILE=1;;
            *) usage; exit 1;;
        esac
    done
fi

# Determine product output directory
if [ ! -d "$PRODUCT_OUTPUT_DIR" ]; then
    echo "ERROR: Cannot find/access: $PRODUCT_OUTPUT_DIR"
    exit 1
fi

# Check whether an output-file is to be created
if [ $CREATE_OUTPUT_FILE -ne 0 ]; then
    OUTPUT_FILE="`echo $THIS | cut -d\. -f1`.out"

    if [ -f "$OUTPUT_FILE" ]; then
        echo "WARNING: Output file already exists: $OUTPUT_FILE"
    fi
fi

# Check whether a warning-file is to be created
if [ $CREATE_WARNING_FILE -ne 0 ]; then
    WARNING_FILE="`echo $THIS | cut -d\. -f1`.warn"

    if [ -f "$WARNING_FILE" ]; then
        echo "WARNING: Warning file already exists: $WARNING_FILE"
    fi
fi

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------
TARBALL_FILE="$RELEASE_OUTPUT_DIR/$(echo $THIS | cut -d\. -f1).tar"
XML_OUTFILE="$RELEASE_OUTPUT_DIR/$(echo $THIS | cut -d\. -f1).xml"

# Calculate table column widths
let LEN_COL_1=`getColWidth $TERMINAL_WIDTH $NUM_COLS 20 10`
let LEN_COL_2=`getColWidth $TERMINAL_WIDTH $NUM_COLS 15 5`
let LEN_COL_3=`getColWidth $TERMINAL_WIDTH $NUM_COLS 25 10`
let LEN_COL_4=`getColWidth $TERMINAL_WIDTH $NUM_COLS 40 10`

if [ $SHOW_LICENSE_SUMMARY -eq 0 ]; then
    # Print header for the table
    echo "Processing: $MODULE_INFO_FILE"
    printLine "=" $TERMINAL_WIDTH
    printTableRow " " "MODULE" "LICENSE" "PATH" "INSTALL"
    printLine "=" $TERMINAL_WIDTH
fi

createModuleInfo

if [ ${CREATE_XML_OUT_FILE} -ne 0 ]; then
    writeXMLPrologue
fi

# Process module-info.txt (generated during build)
while read MODULE_INFO; do
    let LINENO=$LINENO+1; SKIP_MODULE=0

    # Check 1st whether the module is actually installed
    if { MODULE_INSTALL=`getParamVal $PARAM_MODULE_INSTALL $MODULE_INFO`; } then
        if [ -z "$MODULE_INSTALL" -o ! -e "$MODULE_INSTALL" ]; then
            let SKIP_MODULE=1 # module is NOT installed
        fi
    else
        echo "ERROR: Failed to get $PARAM_MODULE_INSTALL parameter (on line $LINENO in: $MODULE_INFO_FILE)"
        let ERROR=$ERROR+1
        let SKIP_MODULE=1
    fi

    # Check whether this is installed on the product (rather than on the host)
    # NOTE: MODULE_INSTALL might have multiple values, so we check here whether
    #       at least 1 of the directories listed in the INSTALLED field is a
    #       sub-directory of the product-output directory (out/target/product/..)
    if ! hasSubdirOf $PRODUCT_OUTPUT_DIR $MODULE_INSTALL; then
        let SKIP_MODULE=1
    fi

    # Get module path
    if { MODULE_PATH=`getParamVal $PARAM_MODULE_PATH $MODULE_INFO`; } then
        # Eliminate duplicate values
        if { ! MODULE_PATH=`removeDuplicates $MODULE_PATH`; } then
            echo "ERROR: Failed while processing $PARAM_MODULE_PATH parameter (on line $LINENO in: $MODULE_INFO_FILE)"
            let ERROR=$ERROR+1
            let SKIP_MODULE=1
        fi
    else
        echo "ERROR: Failed to get $PARAM_MODULE_PATH parameter (on line $LINENO in: $MODULE_INFO_FILE)"
        let ERROR=$ERROR+1
        let SKIP_MODULE=1
    fi

    # Get module name
    if { ! MODULE_NAME=`getParamVal $PARAM_MODULE_NAME $MODULE_INFO`; } then
        echo "ERROR: Failed to get $PARAM_MODULE_NAME parameter (on line $LINENO in: $MODULE_INFO_FILE)"
        let ERROR=$ERROR+1
        let SKIP_MODULE=1
    fi
    processModule $MODULE_NAME "$MODULE_PATH" "$MODULE_INSTALL" $SKIP_MODULE

done < $MODULE_INFO_FILE

processModule kernel ./ti_tools/ipnc_psp_arago/kernel /uImage 0
processModule u-boot ./ti_tools/ipnc_psp_arago/u-boot /u-boot 0

if [ $SHOW_LICENSE_SUMMARY -eq 0 ]; then
    printLine "=" $TERMINAL_WIDTH
fi

if [ ${CREATE_XML_OUT_FILE} -ne 0 ]; then
    writeXMLEpilogue
    # Gzip the file, it probably contains a lot of duplicate NOTICE files.
    gzip -f ${XML_OUTFILE}
fi

if [ $SHOW_LICENSE_SUMMARY -eq 0 ]; then
    # Create tar-ball (eiliminate duplicates)
    if [ $RUN_IN_TEST_MODE -eq 0 ]; then
        echo "Creating tarball: $TARBALL_FILE"
	rm -fv $TARBALL_FILE
        for MODULE_PATH in `cat $MODULES_TO_RELEASE | sort | uniq`; do
            echo "+ $MODULE_PATH"
            git ls-files $MODULE_PATH/ |tar rf $TARBALL_FILE  -T -
        done
    fi

    # Compress tarball
    if [ $RUN_IN_TEST_MODE -eq 0 -a -f "$TARBALL_FILE" ]; then
        echo "Compressing package: $TARBALL_FILE"
        gzip -f $TARBALL_FILE
    fi
fi

# ------------------------------------------------------------------------------
# CLEAN-UP
# ------------------------------------------------------------------------------
if [ -f $MODULES_TO_RELEASE ]; then
    rm -f $MODULES_TO_RELEASE
fi

# ------------------------------------------------------------------------------
# EXIT
# ------------------------------------------------------------------------------
if [ $RUN_IN_TEST_MODE -eq 0 ]; then
    echo "Included: $INCLUDED, Excluded: $EXCLUDED, Total: $TOTAL, Errors: $ERROR, Warnings: $WARNING Created: $TARBALL_FILE.gz"
else
    echo "Included: $INCLUDED, Excluded: $EXCLUDED, Total: $TOTAL, Errors: $ERROR, Warnings: $WARNING"
fi

exit $ERROR
