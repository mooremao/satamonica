#! /usr/bin/env python
import sys
import string
import os
import glob

'''
	In the absence of the android build system this script
	takes the place of generating the module-info.txt file,
	which will then be used by the release_open_sources.sh script.

	Each time it is invoked it will print to stdout the
	module-info.txt lines for the input NOTICE file. Each
	notice file should be accompanied by a MODULE_LICENSE_*
	file. In nav4 builds this file was empty, but on longbeach
	there is no consistent way to get a pointer to the installed
	file(s), so the MODULE_LICENSE_* files will be used for this.

	The format of the MODULE_LICENSE_* files is simple, every
	line contains the location of 1 installed file and each
	MODULE_LICENSE_* can have as many lines as necessary.
	The root dir for each line is
		longbeach_ws2/source/
	So an example file might look like this:

ipnc_rdk/target/filesys/opt/ipnc/dhcpcd/dhcpcd

	If the module is not built, not installed in the filesys,
	or is proprietary then the MODULE_LICENSE_* file can be empty.

	NOTE:
	Any source code that should not be released to the public
	should have an empty MODULE_LICENSE_* file if it has a NOTICE file.
'''

def usage():
	print("One parameters is required:")
	print("  1) Location of a NOTICE file")

def getInstallFiles(noticeDir):
	installFiles = []
	files = glob.glob(noticeDir + "/MODULE_LICENSE_*")
	if (len(files) == 1):
		with open(files[0]) as f:
			installFiles = [line.strip() for line in f]
		# remove any empty lines
		installFiles = filter(None, installFiles)
		installFiles = filter(lambda k: not k.startswith('# '), installFiles)
		if (len(installFiles) == 0):
			# If there are no install dirs in the MODULE_LICENSE_ file then
			# just use the root source dir as the install dir.
			# This case should only hit for files that are not installed in
			# the target filesys, or have licenses that are ok.
			if (noticeDir.startswith("./") == True):
				noticeDir = noticeDir[2:]
			installFiles.append(noticeDir)
	else:
		sys.stderr.write("Warning: MODULE_LICENSE_* problems at " + noticeDir + "\n")
	return installFiles

def outputModuleInfo(installFile, noticeDir):
	name = os.path.basename(installFile)
	print("NAME=\"" + name + "\" PATH=\"" +  noticeDir + "\"" + " INSTALLED=\"" + installFile + "\"")

def outputNoticeInfo(notice):
	noticeDir = os.path.dirname(notice)
	installFiles = getInstallFiles(noticeDir)
	for installFile in installFiles:
		outputModuleInfo(installFile, noticeDir)

def main(argv):
	if ((len(argv) != 1) or ('-h' in argv) or ('--help' in argv)):
		usage()
	else:
		outputNoticeInfo(argv[0])

if __name__ == "__main__":
	main(sys.argv[1:])
