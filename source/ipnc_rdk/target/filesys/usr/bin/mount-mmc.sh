#!/bin/sh

#
# Mounts MMC
#
SYSTEM_DIR="/mnt/mmc/MISC/.ttsystem"

#
# If there's any filesystem corruption, we want to detect it as early as
# possible. So we try writing a file in the SD-card root and in the
# photos/videos directory to see if that triggers a write error and a
# remount-as-readonly.
# We are aware that this is not guaranteed to find all errors, but the
# app team believes it will find the majority of them.
#
# In case of error writing to a file or directory, there is no need to do
# anything in the script - the filesystem will be remounted read-only
#
do_dummy_write() {
  local mmc_root="/mnt/mmc"
  local dcim_dir="/mnt/mmc/DCIM"
  local test_file=".ttfsck.tmp"
  local test_dir="$dcim_dir/.TTFSCK"
  local video_dir=`ls -1vd $dcim_dir/*TTCAM | tail -n 1`

# Check whether the test file in SD card root already exists
# and if so, remove it first
  if [ -f "$mmc_root/$test_file" ]
  then
    echo "Found old test file in $mmc_root/$test_file... removing it."
    rm $mmc_root/$test_file
  fi
# Afterwards, check if a test file can be created in SD card root
  date > $mmc_root/$test_file
  if [ $? -eq 0 ]
  then
    echo "Successfully created test file $mmc_root/$test_file -- Deleting it..."
    rm $mmc_root/$test_file
  else
    echo "Failed to create test file $mmc_root/$test_file! Filesystem will be remounted read-only."
  fi

# Check whether the test directory exists, and if so, remove it first
  if [ -d "$test_dir" ]
  then
    echo "Found old test directory in $test_dir/ on SD card... removing it."
    rm -r $test_dir
  fi
# Afterwards, check if a test dir can be created in DCIM folder
# on the SD card and if a file can be written in that folder.
  if [ -d "$dcim_dir" ]
  then
    mkdir $test_dir
    if [ $? -eq 0 ]
    then
      echo "Successfully created test directory $test_dir/ -- will try to create a test file there."
      date > $test_dir/$test_file
      if [ $? -eq 0 ]
      then
	echo "Test file successfully created in $test_dir/$test_file -- Deleting it..."
	rm $test_dir/$test_file
      else
	echo "Failed to create test file $test_dir/$test_file! Filesystem will be remounted read-only."
      fi
      echo "Removing test directory $test_dir..."
      rm -r $test_dir
    else
      echo "Failed to create directory $test_dir! Filesystem will be remounted read-only."
    fi
  else
    echo "Directory $dcim_dir/ not found. This is normal on freshly formatted SD cards."
  fi

# Finally, check if a file can be written in the current video directory
  if [ "$video_dir" != "" ]
  then
    date > $video_dir/$test_file
    if [ $? -eq 0 ]
    then
      echo "Test file successfully created in $video_dir/$test_file -- Deleting it..."
      rm $video_dir/$test_file
    else
      echo "Failed to create test file $video_dir/$test_file! Filesystem will be remounted read-only."
    fi
  else
    echo "Video/Photo directory not found. This is normal on a freshly formatted SD card."
  fi
}

#
# Create a hidden dir if does not exists.
#
do_create_hidden_dir_if_not_exists() {
    if [ ! -d ${SYSTEM_DIR} ]
    then
        # Only make it hidden when creating directory succeed
        mkdir -p ${SYSTEM_DIR} && fatattr +h ${SYSTEM_DIR}
    fi
}

try_mount_and_exit() {
    local fstype=$1
    local device=$2
    local option=rw

    if [ $fstype = vfat ]
    then
        fsck.fat -b $device
        if [ $? = 0 ]
        then
            option=rw
        else
            option=ro
        fi
    fi

    mount -t $fstype -o $option $device /mnt/mmc
    if [ $? = 0 ]
    then
        echo "Mounted $device as $fstype"
        if [ $fstype = vfat ]
        then
          do_dummy_write
        fi
        do_create_hidden_dir_if_not_exists

        # Disable the request timeout in case of positive mount
        if [ -f /sys/kernel/debug/mmc1/req_timeout_en ]
        then
            echo 0 > /sys/kernel/debug/mmc1/req_timeout_en
        fi

        exit 0
    fi
}

try_mounts_and_exit() {
    # We only support FAT, FAT16, FAT32 and exFAT, see LBP-1122.

    # Try to mount /dev/mmcblk0p1 before /dev/mmcblk0.
    # If the SD card was not partitioned before formatting,
    # /dev/mmcblk0p1 will not exist and /dev/mmcblk0 will be mounted.
    # If SD card was partitioned, /dev/mmcblk0p1 will be mounted.
    try_mount_and_exit   vfat /dev/mmcblk0p1
    try_mount_and_exit texfat /dev/mmcblk0p1
    try_mount_and_exit   vfat /dev/mmcblk0
    try_mount_and_exit texfat /dev/mmcblk0
}

# Make sure the failsafe SD clock is disabled for the 1st try
if [ -f /sys/kernel/debug/mmc1/failsafe_clk_en ]
then
    echo 0 > /sys/kernel/debug/mmc1/failsafe_clk_en
fi

# Make sure the request timeout is enabled for the 1st try
if [ -f /sys/kernel/debug/mmc1/req_timeout_en ]
then
    echo 1 > /sys/kernel/debug/mmc1/req_timeout_en
fi

# Perform the 1st mount try
try_mounts_and_exit

# LBP-1776: some cards might not support the full SD clock rate, causing the mount to fail
# If this happens, try again using a failsafe rate defined in the kernel configuration
FAILSAFE_EN=`cat /sys/kernel/debug/mmc1/failsafe_clk_en` || exit 1
if [ $FAILSAFE_EN -eq 0 ] && [ -b /dev/mmcblk0 ]
then
    # Enable the failsafe clock rate
    echo 1 > /sys/kernel/debug/mmc1/failsafe_clk_en
    # Rescan the mmc bus
    echo 1 > /sys/class/mmc_host/mmc1/rescan
    # Rescan the partitions
    ptscan /dev/mmcblk0 || exit 1
    # Try the mount again
    try_mounts_and_exit
fi

# Make sure the request timeout is disabled before continue
if [ -f /sys/kernel/debug/mmc1/req_timeout_en ]
then
    echo 0 > /sys/kernel/debug/mmc1/req_timeout_en
fi

# A double mount failure will leave the failsafe clock enabled to
# allow the SD format API to work on both good and bad cards.
# When this script is called again after formatting, the
# first mount attempt will be done again at full clock speed.
echo "Failed to mount sd card."
exit 1
