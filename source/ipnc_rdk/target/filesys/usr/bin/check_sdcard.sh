
#!/bin/sh

#check for sdcard presence and writability
retval=0

CARD=$(mount | grep "mnt/mmc")

if [ -z "$CARD" ] ; then
	MMCBLK=$(ls /sys/block | grep "mmcblk")
	if [ -z "$MMCBLK" ] ; then
		retval=1		#sdcard not present
	else
		retval=2		#sdcard failed to mount
	fi
else
	RW=$(mount | grep "mnt/mmc" | grep -E "\brw\b")
	if [ -z "$RW" ] ; then
		retval=3		#sdcard readonly
	else
		retval=0		#sdcard available
	fi
fi
exit $retval
