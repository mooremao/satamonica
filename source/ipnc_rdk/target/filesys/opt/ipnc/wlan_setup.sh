#!/bin/sh

DEFAULT_MAC="00:0c:0c:a0:06:4c"

# Read MAC address from FDT, return default when not accessible
FDT_MAC=$(fdtquery -I /dev/fdtexport /options/wifi/wifi-mac-address || echo "$DEFAULT_MAC")

WaitWLanReady() {
	#wait for wlan0 ready
	count=0
	while [ $count -lt 20 ] && [ ! -e /sys/class/net/wlan0 ] ; do
		sleep 0.2
		count=$(($count+1))
	done

	# wait wlan up
	count=0
	ifconfig wlan0 | grep UP > /dev/null
	down_status=$?
	while [ $count -lt 20 ] && [ $down_status -ne 0 ] ; do
		sleep 0.2
		ifconfig wlan0 | grep UP > /dev/null
		down_status=$?
		count=$(($count+1))
	done
}

modprobe cfg80211

# Copy the factory NVS file to the tmpfs
mkdir -p /tmp/firmware/ti-connectivity
cp /lib/firmware/ti-connectivity/wl1271-nvs.bin.ti /tmp/firmware/ti-connectivity/wl1271-nvs.bin

# Set the new MAC address
calibrator set nvs_mac /tmp/firmware/ti-connectivity/wl1271-nvs.bin $FDT_MAC

modprobe wlcore_sdio

# check wlan with ifconfig wlan0, do rescan when needed
ifconfig wlan0 | grep HWaddr > /dev/null
card_not_exist=$?
if [ $card_not_exist -ne 0 ]; then
	# allow the driver to control the power of the wlan card
	echo 1 > /sys/devices/platform/mmci-omap-hs.0/wlan_power_ctl
	# rescan to detect card
	echo 1 > /sys/devices/platform/mmci-omap-hs.0/mmc_host/mmc0/rescan
	WaitWLanReady
fi
