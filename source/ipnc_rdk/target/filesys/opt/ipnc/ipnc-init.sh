#!/bin/sh
#
# Left overs from init.sh script.
#

# Set audio path to internal mic by default
/opt/ipnc/set_audio_path.sh int

#
# Format of the system priority setting utility for setting bandwidth regulator
#
# ./bin/sys_pri.out --L3-bw-reg-set <L3-bw-reg-initiator-name> <L3-pressure-High> <L3-pressure-Low> <L3-Bandwidth> <L3-Watermark-cycles>
#
# "L3-bw-reg-initiator-name" can be HDVICP0 or HDVICP1 or HDVICP2 or other initiators
# "L3-pressure-High" can be 0 (low), 1 (medium), 3 (high)
# "L3-pressure-Low"  can be 0 (low), 1 (medium), 3 (high)
# "L3-Bandwidth" is in MB/s
# "L3-Watermark-cycles" is in bytes
#
# See also ./bin/sys_pri.out --help for more details
#
# IVA-HD BW requlator programing.
# TODO. THIS IS CURRENTLY HARDCODED.
# THIS SHOULD CALCULATED FROM EXPECTED IVA BANDWIDTH USAGE
#
# IVA-HD0, IVA-HD1, IVA-HD2
#
./bin/sys_pri.out --L3-bw-reg-set HDVICP0 0 0 900 2500
./bin/sys_pri.out --L3-bw-reg-set ISS 3 3 400 2500
./bin/sys_pri.out --dmm-pri-set ISS 0
./bin/sys_pri.out --dmm-pri-set HDVICP0 1
