#!/bin/sh

gaugetool
if [ $? -eq 2 ]; then
    echo "Force update Fuel Gauge"
    gaugetool -f
fi
