#!/bin/sh

#==============================================================================
#
# This file contains functions to set and get a peripheral's frequency
# and to set and get the device operating point
#
# Usage: . /<PATH>/debug_opp_func.sh
#
#==============================================================================

#==============================================================================
# Definitions and helper functions
#==============================================================================
SYSTEM_DIR="/sys/devices/system"
PERIPHERALS="cpu iva iss l3"
GOVERNOR="/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

DEBUG_INTERFACE="/sys/devices/system/lbgov_debug/lbgov_debug0"

#==============================================================================
# Functions to set and get peripheral frequencies
#==============================================================================
#------------------------------------------------------------------------------
# Get peripheral frequency
# Usage: getfreq <cpu|iva|iss|l3> <cur|min|max>
#------------------------------------------------------------------------------
getfreq() {
    if [ "$#" -lt "2" ]; then
        echo "Insufficient arguments" 1>&2
        echo "getfreq <cpu|iva|iss|l3> <cur|min|max>" 1>&2
        return 1
    fi

    local peripheral=$1
    local ftype=$2
    local dir_name
    local freq

    if [ "$peripheral" = "cpu" ]; then
        dir_name="$SYSTEM_DIR/cpu/cpu0/cpufreq"
    else
        dir_name="$SYSTEM_DIR/${peripheral}/${peripheral}0/freqscaling"
    fi

    if [ ! -d $dir_name ]; then
        echo "Invalid peripheral" 1>&2
        return 1
    fi

    if [ ! -f $dir_name/scaling_${ftype}_freq ]; then
        echo "Invalid frequency type" 1>&2
        return 1
    fi

    freq=`cat $dir_name/scaling_${ftype}_freq`
    freq=`expr $freq / 1000`
    echo $freq
}

#------------------------------------------------------------------------------
# Setups peripheral frequency for given power mode
# Usage: setup_gov_freq [<mode>|cur] <cpu|iva|iss|l3> <freq_in_MHz> [now]
#------------------------------------------------------------------------------
setup_gov_freq() {
    if [ "$#" -lt "3" ]; then
        echo "Insufficient arguments" 1>&2
        echo "setup_gov_freq [<mode>|cur] <cpu|iva|iss|l3> <freq_in_MHz> [now]" 1>&2
        return 1
    fi

    local mode=$1
    local peripheral=$2
    local frequency=$3
    local now=$4

    if [ "$mode" = "cur" ]; then
        mode=`cat $GOVERNOR`
    fi

    if [ ! -d "$DEBUG_INTERFACE" ]; then
        echo "Debug interface not enabled. Please configure kernel with CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG" 1>&2
        return 1
    fi

    if [ ! -d "$DEBUG_INTERFACE/$mode" ]; then
        echo "Invalid power mode. Supported power modes:" 1>&2
        ls -1 $DEBUG_INTERFACE/ 1>&2
        return 1
    fi

    if [ ! -f "$DEBUG_INTERFACE/$mode/${peripheral}_freq" ]; then
        echo "Invalid peripheral. Supported peripherals:" 1>&2
        ls -1 $DEBUG_INTERFACE/$mode/ | sed 's/_freq//g' 1>&2
        return 1
    fi

    echo "$frequency" > $DEBUG_INTERFACE/$mode/${peripheral}_freq

    if [ "$now" = "now" ]; then
        echo $mode > $GOVERNOR
    fi
}

#------------------------------------------------------------------------------
# Gets the peripheral frequency for given power mode
# Usage: get_gov_freq <mode> <cpu|iva|iss|l3>
#------------------------------------------------------------------------------
get_gov_freq() {
    if [ "$#" -lt "2" ]; then
        echo "Insufficient arguments" 1>&2
        echo "get_gov_freq <mode> <cpu|iva|iss|l3>" 1>&2
        return 1
    fi

    local mode=$1
    local peripheral=$2

    if [ ! -d "$DEBUG_INTERFACE" ]; then
        echo "Debug interface not enabled. Please configure kernel with CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG" 1>&2
        return 1
    fi

    if [ ! -d "$DEBUG_INTERFACE/$mode" ]; then
        echo "Invalid power mode. Supported power modes:" 1>&2
        ls -1 $DEBUG_INTERFACE/ 1>&2
        return 1
    fi

    if [ ! -f "$DEBUG_INTERFACE/$mode/${peripheral}_freq" ]; then
        echo "Invalid peripheral. Supported peripherals:" 1>&2
        ls -1 $DEBUG_INTERFACE/$mode/ | sed 's/_freq//g' 1>&2
        return 1
    fi

    cat $DEBUG_INTERFACE/$mode/${peripheral}_freq
}

#==============================================================================
# Functions to set and get peripheral frequencies
#==============================================================================
#------------------------------------------------------------------------------
# Get current operating point
# Usage: getopp
#------------------------------------------------------------------------------
getopp() {
    local i

    echo "********************************************"
    echo "*        CURRENT FREQUENCY SETTINGS        *"
    echo "********************************************"
    echo ""

    for i in  $PERIPHERALS
    do
        freq=`getfreq $i cur`
        name=`echo $i | tr '[a-z]' '[A-Z]'`
        echo $name":    " $freq "MHz"
    done
}

printgov() {
    local name
    local cpu
    local iva
    local iss
    local l3
    local current
    local is_current

    if [ ! -d "$DEBUG_INTERFACE" ]; then
        echo "Debug interface not enabled. Please configure kernel with CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG" 1>&2
        return 1
    fi

    current=`cat $GOVERNOR`

    echo "*************************************************************"
    echo "*                GOVERNOR FREQUENCY SETTIGNS                *"
    echo "*************************************************************"
    echo ""
    echo "    Gov name | cpu [MHz] | iva [MHz] | iss [MHz] |  l3 [MHz]"
    for i in $DEBUG_INTERFACE/*
    do
        name=$(basename $i)

        if [ "$name" = "$current" ]; then
            is_current="*"
        else
            is_current=""
        fi

        cpu=`get_gov_freq $name cpu`
        iva=`get_gov_freq $name iva`
        iss=`get_gov_freq $name iss`
        l3=`get_gov_freq $name l3`
        printf "%1s %10s | %9s | %9s | %9s | %9s\\n" "$is_current" "$name" "$cpu" "$iva" "$iss" "$l3"
    done
}

