#!/bin/sh

WORKINGDIR=/mnt/userdata/bluetooth

mkdir -p $WORKINGDIR
cd $WORKINGDIR

fdtquery -I /dev/fdtexport /options/bluetooth/bt-mac-address | sed 's/://g' > /run/BTMacAddress

/usr/sbin/SS1BTPM &
echo $! > /run/bluetopia.pid

# Wait for the socket to be created before exiting
cnt=0
while [ ! -S /tmp/SS1BTPMS ]
do
	if [ $cnt -gt 5 ]; then
		echo "Timeout waiting for /tmp/SS1BTPMS"
		exit 1;
	fi

	sleep 1
	cnt=$((cnt+1))
done

