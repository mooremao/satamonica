#!/bin/sh

# Mounts TTCI images as loop back device and executes it.

set -e

TTCI_IMG=${1}
TTCI_MNT=/mnt/ttci
TTCI_EXEC=${TTCI_MNT}/bin/ttci

if [ "${TTCI_IMG}" == "" ]
then
	echo "Usage: ${0} <path-to-ttci.img>"
	exit 1
fi

modprobe squashfs
if [ $? -eq 0 ]
then
	mount -o loop ${TTCI_IMG} ${TTCI_MNT}
else
	echo "Failed to load squashfs driver"
fi

${TTCI_EXEC}
if [ -e /tmp/reboot ]; then
	umount ${TTCI_MNT}
	losetup -d /dev/loop0
	systemctl stop userdata.service userdata-mmc.service
	reboot
elif [ -e /tmp/poweroff ]; then
	umount ${TTCI_MNT}
	losetup -d /dev/loop0
	systemctl stop userdata.service userdata-mmc.service
	poweroff
elif [ -e /tmp/DeleteAll ]; then
	ttci-remove
	systemctl stop userdata.service userdata-mmc.service
	poweroff
else
	echo "Leave console available for debugging."
fi



