#!/bin/sh

# Mounts test images as loop back device and executes it.

set -e

TEST_IMG=${1}
TEST_MNT=/mnt/test

if [ "${TEST_IMG}" == "" ]
then
	echo "Usage: ${0} <path-to-test.img>"
	exit 1
fi

mount -o loop ${TEST_IMG} ${TEST_MNT}

cd ${TEST_MNT}

# Following commands are based on previous S99finish.sh script.
./autorun_test.sh &

./wifi_sdio_init.sh

# Init BT here since the BT_HCI_RTS pin is output high under shutdown and after power up.
# It will affect the "DEVOSC_WAKE" pin HW logic design(can't wakeup from sleep).
# After BT FW init done, BT_HCI_RTS then becomes functional, so low when UART is able to Rx and high when it is not.
# Then suspend/resume can work fine.
/opt/ipnc/bt_init.sh

