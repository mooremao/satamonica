#!/bin/sh
#==============================================================================
#
# This script checks for the presence of 'crossopp' file on the SDCard
# If present, the script enables cross OPP support in the kernel
# This script runs as a part of systemd
#
# Usage:
# To set the operating mode to Cross OPP:
# 1) Create a file named 'crossopp' on the SDCard. The content of the file is
#    ignored. This script only checks for the presence or absence of the file
#    on the SDCard
# 3) Reboot
#
# To set the operating mode to Single OPP:
# 1) Delete the file named 'crossopp' on the SDCard if present
# 3) Reboot
#
#==============================================================================


FILE=crossopp

# Print regulator voltages
get_voltage() {
	if [ $1 = "core" ]
	then
		voltage=`cat /sys/class/regulator/regulator.2/microvolts`
	else
		voltage=`cat /sys/class/regulator/regulator.1/microvolts`
	fi

	echo $1 "voltage is set to " $voltage "uV"
}

# Check if singleopp file does exists
if [ -f /mnt/mmc/$FILE ]
then
	echo "Cross OPP"
	echo 1 > /sys/power/crossopp_enabled
else
	echo "Single OPP"
	echo 0 > /sys/power/crossopp_enabled
fi

get_voltage core
get_voltage hdvicp
