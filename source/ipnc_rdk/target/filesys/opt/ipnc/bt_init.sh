#!/bin/sh
chip_vendor=texas
hw_btspeed=3000000
hw_btdev="ttyO1"
bdaddr_file="/mnt/userdata/bdaddr"

init_step=0
max_retry=3
hciattach_retry=0
idx=0

do_error_handling() {
  echo "do init_step: " $init_step " error handling..."
  if [ $init_step -eq 1 ]; then
    echo 0 > /sys/class/gpio/gpio98/value
  elif [ $init_step -eq 2 ]; then
    killall hciattach
    echo 0 > /sys/class/gpio/gpio98/value
  elif [ $init_step -eq 3 ]; then
    hciconfig hci0 down
    killall hciattach
    echo 0 > /sys/class/gpio/gpio98/value
  else
    echo "init_step: " $init_step " unknown!!!" 
  fi
}

check_bdaddr_format() {
  FAIL=0
  BD_ADDR=`cat $bdaddr_file`
  BD_ADDR_LEN=`echo ${#BD_ADDR}`
  echo "BD_ADDR: " $BD_ADDR " BD_ADDR_LEN: " $BD_ADDR_LEN
  if [ $BD_ADDR_LEN -ne 17 ]; then
    echo "Error Length Of BD_ADDR: " $BD_ADDR
    do_error_handling
    exit 1
  fi

  c_idx=0
  is_digit=0
  is_colon=0
  is_digit_pos="0 1 3 4 6 7 9 10 12 13 15 16"
  is_colon_pos="2 5 8 11 14"
  echo "==============BD_ADDR Format Checking==============" 
  while read -n1 char; do
   echo $c_idx " : " $char
   case $char in
    *[0-9])
          for pos in $is_digit_pos
          do
             if [ $c_idx -eq $pos ]; then
                is_digit=1
                break
             fi
          done
          if [ $is_digit -eq 0 ]; then
             FAIL=1
          fi
          ;;
    *[A-F])
          for pos in $is_digit_pos
          do
             if [ $c_idx -eq $pos ]; then
                is_digit=1
                break
             fi
          done
          if [ $is_digit -eq 0 ]; then
             FAIL=1
          fi
          ;;
    *[a-f])
          for pos in $is_digit_pos
          do
             if [ $c_idx -eq $pos ]; then
                is_digit=1
                break
             fi
          done
          if [ $is_digit -eq 0 ]; then
             FAIL=1
          fi
          ;;
    *[:])
          for pos in $is_colon_pos
          do
             if [ $c_idx -eq $pos ]; then
                is_colon=1
                break
             fi
          done
          if [ $is_colon -eq 0 ]; then
             FAIL=1
          fi
          ;;
    *)
          FAIL=1
          ;;
   esac
   if [ $FAIL -eq 1 ]; then
     break
   fi
   c_idx=$((c_idx+1))
   is_digit=0
   is_colon=0
   if [ $c_idx -eq 17 ]; then
     break
   fi
  done <$bdaddr_file
  echo "==============End BD_ADDR Format Checking==========="  

  if [ $FAIL -eq 1 ]; then
    echo "Error BD_ADDR Format: " $BD_ADDR
    do_error_handling
    exit 1
  else
    echo "==> BD_ADDR Format Check OK!!!"
  fi
}

write_bdaddr() {
  if [ -f $bdaddr_file ]; then
    init_step=3
    echo "init_step: " $init_step
    echo "Found BDADDR File!!!"
    check_bdaddr_format 
    BD_ADDR_LIST=`cat $bdaddr_file | sed 's/:/ /g'`
    #echo "BD_ADDR_LIST: " $BD_ADDR_LIST
    for BD_ADDR in $BD_ADDR_LIST
    do
      if [ $idx -eq 0 ]; then
        BD_ADDR_0=`echo "0x"$BD_ADDR`
      fi

      if [ $idx -eq 1 ]; then
        BD_ADDR_1=`echo "0x"$BD_ADDR`
      fi

      if [ $idx -eq 2 ]; then
        BD_ADDR_2=`echo "0x"$BD_ADDR`
      fi

      if [ $idx -eq 3 ]; then
        BD_ADDR_3=`echo "0x"$BD_ADDR`
      fi

      if [ $idx -eq 4 ]; then
        BD_ADDR_4=`echo "0x"$BD_ADDR`
      fi

      if [ $idx -eq 5 ]; then
        BD_ADDR_5=`echo "0x"$BD_ADDR`
      fi
      idx=$((idx+1))
    done
    WRITE_BDADDR_CMD="hcitool cmd 0x3f 0x006 $BD_ADDR_0 $BD_ADDR_1 $BD_ADDR_2 $BD_ADDR_3 $BD_ADDR_4 $BD_ADDR_5"
    echo "WRITE_BDADDR_CMD: "$WRITE_BDADDR_CMD
    $WRITE_BDADDR_CMD
    if [ $? -ne 0 ]; then
      echo "BT Init Fail($WRITE_BDADDR_CMD fail)!!!"
      do_error_handling
      exit 1
    fi
    hciconfig hci0 reset
    if [ $? -ne 0 ]; then
      echo "BT Init Fail(hciconfig hci0 reset fail)!!!"
      do_error_handling
      exit 1
    fi
    sleep 1
    echo "==> Write BD_ADDR OK!!!"
  fi
}

do_hciattach() {
  init_step=1
  echo "init_step: " $init_step
  killall hciattach
  echo 0 > /sys/class/gpio/gpio98/value
  sleep 0.5 
  echo 1 > /sys/class/gpio/gpio98/value
  sleep 0.5
  #hciattach /dev/ttyO1 texas 3000000
  hciattach /dev/${hw_btdev} ${chip_vendor} ${hw_btspeed}
}

check_hciattach_need_retry_or_not() {
  while [ $? -eq 1 ]
  do
    if [ $hciattach_retry -eq $max_retry ]; then
      echo "reach max_retry of hciattach..."
      exit 1
    fi
    echo "hciattach fail! do $hciattach_retry retry..."
    hciattach_retry=$((hciattach_retry+1))
    do_hciattach
  done
}

echo "===== Bluetooth Initialization ======"
# do hciattach
do_hciattach

# check hciattach need retry or not
# Should skip retry mechanism in the beginning of project development
#check_hciattach_need_retry_or_not
if [ $? -ne 0 ]; then
  echo "BT Init Fail(do_hciattach fail)!!!"
  do_error_handling
  exit 1
fi

# bring bluetooth device up
init_step=2
echo "init_step: " $init_step
hciconfig hci0 up
if [ $? -ne 0 ]; then
  echo "BT Init Fail(hciconfig hci0 up fail)!!!"
  do_error_handling
  exit 1
fi
sleep 1

write_bdaddr

