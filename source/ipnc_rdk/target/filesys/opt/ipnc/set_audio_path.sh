#!/bin/sh
audio_path=$1

if [ $audio_path == "ext" ]; then
  echo "set audio path to external MIC"
	amixer cset name='Left Input Selection DIF1_L switch' off
	amixer cset name='Left Input Selection DIF3_L switch' on
	amixer cset name='Right Input Selection DIF1_R switch' on
	amixer cset name='Right Input Selection DIF3_R switch' off
	amixer cset name='Mic Bias 2 Voltage' '2.5V'
fi
if [ $audio_path == "int" ]; then
	echo "set audio path to internal MIC"
	amixer cset name='Left Input Selection DIF1_L switch' on
	amixer cset name='Left Input Selection DIF3_L switch' off
	amixer cset name='Right Input Selection DIF1_R switch' off
	amixer cset name='Right Input Selection DIF3_R switch' on
	amixer cset name='Mic Bias 2 Voltage' 'off'
fi
