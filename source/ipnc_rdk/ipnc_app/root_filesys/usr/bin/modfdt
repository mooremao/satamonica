#!/bin/sh
#
# Modifies the FDT
#
# Execute "lockdevice" to lock device
# Execute "unlockdevice" to unlock device
#
# By default, fdt-<device-serial>.backup.bin is created in /mnt/userdata if it does not exists yet.
# You can override this location by setting BACKUP_DIR to some other location, such as /mnt/mmc
#
# The 'current' FDT is read from from /tmp/fdt.dtb /mnt/userdata/fdt.bin or /dev/fdtexport.
# The /tmp/fdt.dtb is created by this script and is volatile.
# The /mnt/userdata/fdt.bin is created by TTCI
# The /dev/fdtexport contains a in memory copy of the FDT and is not updated when writting
# the FDT partition.
#
TMP_FDT="/tmp/fdt.dtb"
INPUT_DTB_LOCATION="${TMP_FDT} /mnt/userdata/fdt.bin /dev/fdtexport"
BACKUP_DIR=${BACKUP_DIR="/mnt/userdata"}
NEW_FDT_FILE="/tmp/fdt.new.dtb"
FDT_MTD="/dev/mtd7"

module=`basename $0`
dtc_arg=""

get_device_class()
{
    local inputdtb=$1
    local device_class

    device_class=`fdtquery -i -I ${inputdtb} /features/device-class` || return -1
    if [ "$device_class" = "1" ]
    then
        return 1
    else
        return 0
    fi
}

# Check module
case $module in
    "unlockdevice" )
        dtc_arg="-i /features/device-class=1"
        ;;
    "lockdevice" )
        dtc_arg="-i /features/device-class=0"
        ;;
    *)
        echo "Unknown module $0"
        exit 1
        ;;
esac

# Find input fdt.
inputdtb=""
for dtb in $INPUT_DTB_LOCATION
do
    if [ -e $dtb ]
    then
        inputdtb=$dtb
        break
    fi
done

if [ -z $inputdtb ]
then
    echo "Cannot find input factory data"
    exit 1
fi

echo "Using $inputdtb as input"

# Check if programming is really needed
if [ "$module" = "unlockdevice" ]
then
    get_device_class $inputdtb
    if [ $? -eq 1 ]
    then
        echo "Device already unlocked"
	    exit 0
    fi
elif [ "$module" = "lockdevice" ]
then
    get_device_class $inputdtb
    if [ $? -eq 0 ]
    then
        echo "Device already unlocked"
        exit 0
    fi
fi

# Read device serial
device_serial=`fdtquery -i -I $inputdtb /features/device-serial`
if [ "$device_serial" = "" ]
then
    echo "Failed to read device-serial from $inputdtb"
    exit 1
fi

# Create backup file if it does not exist yet.
backupfile=${BACKUP_DIR}/fdt-${device_serial}.backup.bin
if [ ! -f $backupfile ]
then
    cp $inputdtb $backupfile
    if [ $? -ne 0 ]
    then
        echo "Failed to create backup $backupfile"
        exit 1
    fi
fi

# Convert FDT
dtc -q ${dtc_arg} -I dtb -O dtb $inputdtb > ${NEW_FDT_FILE}
if [ $? -ne 0 ]
then
    echo "Failed to convert fdt"
    exit 1
fi

# Erase FDT partition
flash_erase ${FDT_MTD} 0 0
if [ $? -ne 0 ]
then
    echo "Failed to erase fdt"
    exit 1
fi

# Write FDT partition
nandwrite -p ${FDT_MTD} ${NEW_FDT_FILE}
if [ $? -ne 0 ]
then
    echo "Failed to write fdt"
    exit 1
fi

# Create /tmp/fdt.dtb. /dev/fdtexport will not be updated until restart.
cp ${NEW_FDT_FILE} ${TMP_FDT}
rm -rf ${NEW_FDT_FILE}

echo "FDT successfully written. Reboot device to apply changes."

