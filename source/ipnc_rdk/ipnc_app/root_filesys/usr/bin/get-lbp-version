#!/bin/sh
#
# Prints platform version number.
#
# Usage:
# get-lbp-version [-l]
#
# Prints platform version. When platform version is not a release build, it will print the git revision instead. In this case the script exit code is 1.
# When it is a release build, it prints the date (<yyyymmddv>) from the release tag and exit code is 0.
#
# When -l (long) option is given, the script will print the complete git revision / tag name and exit code is 0. For release builds, it prints complete tag,
# such as lbp_chronos_<yyyymmdv>. For non release build, behaviour is the same as without -l flag, except for the exit code which is 0 instead of 1.
#
# Exit codes above 1 indicate errors.
#

# Read version file and parse it.
git_line=`cat /etc/lbp-version | grep "git="`
date_version=`expr match "$git_line" "git=.*_\([0-9]\{9,9\}\)"`
git_ref=`expr match "$git_line" "git=lbp_g\(.*\)"`
long_version=`expr match "$git_line" "git=\(.*\)"`

if [ "$1" = "-l" ]
then
	echo $long_version
	exit 0
fi

if [ "$date_version" != "" ]
then
	echo $date_version
	exit 0
elif [ "$git_ref" != "" ]
then
	echo $git_ref
	exit 1
else
	echo "Failed to determine platform software version" >&2
	exit 2
fi

