#!/bin/sh
#
# Script for starting dropbear
#

PIDFILE="/run/dropbear.pid"
DROPBEAR_ARGS=""
DEFAULT_SERIAL_NUMBER="FAKEWIN@"
DEFAULT_DEVICE_CLASS="1"

SERIAL_NUMBER=$(fdtquery -I /dev/fdtexport /features/device-serial || echo "$DEFAULT_SERIAL_NUMBER")
DEVICE_CLASS=$(fdtquery -I /dev/fdtexport -i /features/device-class || echo "$DEFAULT_DEVICE_CLASS")

# Allow blank password in case of a developer device or when serial is not valid.
# Disable password logins otherwise
if [ "${DEVICE_CLASS}" = "1" -o "${SERIAL_NUMBER}" = "${DEFAULT_SERIAL_NUMBER}" ]
then
    DROPBEAR_ARGS="${DROPBEAR_ARGS} -B"
else
    DROPBEAR_ARGS="${DROPBEAR_ARGS} -s"
fi

# Bind to all 32 USB ip-addresses of the device but not to WiFi addresses
for i in `seq 0 31`
do
    DROPBEAR_ARGS="${DROPBEAR_ARGS} -p 169.254.255.$((1+4*i)):22"
done

#Start drop. Dropbear will fork to background
dropbear -r /etc/dropbear/dropbear_rsa_host_key -P ${PIDFILE} ${DROPBEAR_ARGS}

#Wait for pid file
cnt=0
while [ ! -f ${PIDFILE} ]
do
	if [ $cnt -gt 5 ]; then
		echo "Timeout waiting for ${PIDFILE}"
		exit 1;
	fi

	sleep 1
	cnt=$((cnt+1))
done

