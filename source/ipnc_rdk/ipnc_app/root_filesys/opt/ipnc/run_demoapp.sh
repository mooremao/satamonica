#!/bin/sh

# Runs the demo-app and waits for it to finish. On successful exit, powers down the device.
# On failure (any exit code other than 0), do not power down, so that you can debug.

# Ingore SIGPWR, SIGTERM and SIGUSR1 signal. Needed to allow systemd to send signals to all process
# started by camera-app.service and let the application decided what to do.
trap '' PWR TERM USR1 USR2

cd /opt/ipnc

./bin/lbpDemoApp
RESULT=$?
if [[ $RESULT == 0 ]] ; then
  echo "Demo-app exited successfully; powering down!"
  MCU_ONLINE=$(cat /sys/class/power_supply/tomtom_mcu/online)
  if [ "$MCU_ONLINE" = "1" ] ; then
    # The 'reboot usbpoweron' command is blocking. As we execute this from the context of the camera-app service
    # the reboot command is blocking on it self. The systemctl command does not allow specifing the reboot parameter.
    # So we set the reboot parameter differently via this little bit dirty way.
    echo usbpoweron > /run/systemd/reboot-param
    systemctl --no-block reboot
  else
    systemctl --no-block poweroff
  fi
elif [[ $RESULT == 15 ]] ; then
  echo "Demo app exited by TERM signal. Not shutting down;"
  exit 0
elif [[ $RESULT == 30 ]] ; then
  echo "Battery critical low! Not shutting down; systemd will trigger poweroff"
  exit 30
elif [[ $RESULT == 12 ]] ; then
  echo "Temperature beyond threshold! Not shutting down; systemd will trigger poweroff"
  exit 12
else
  echo "Demo app exited with error code $RESULT. Not shutting down; happy debugging!"
  exit $RESULT
fi

