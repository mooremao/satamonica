#!/bin/sh

# Ingore SIGPWR, SIGTERM and SIGUSR1 signal. Needed to allow systemd to send signals to all process
# started by camera-app.service and let the application decided what to do.
trap '' PWR TERM USR1 USR2

cd /opt/ipnc

# If system crashes before this point, system boots into recovery.
acknowledge_boot

if [ -f ./scripts/cameraApp.sh ]
then
	./scripts/cameraApp.sh
else
	if [ -f ./bin/cameraApp ]
	then
		# Trigger watchdog on behalf of cameraApp as long as /etc/disable_trigger_wd does not
		# exist. This decouples implementation of watchdog functionality in cameraApp and enabling
		# watchdog functionality in platform releases.
		# So this is temporary and _can_ be removed once application team integrated watchdog
		# functionality.
		if [ ! -f /etc/disable_trigger_wd ]
		then
			temporary_trigger_wd &
		fi
		./bin/cameraApp.sh
	else
		echo "/opt/ipnc/scripts/cameraApp.sh or bin/cameraApp not found!"
		echo "Copy it to SD card and put it in <sdcard>/scripts/ and reboot."
		echo "Same applies to <sdcard>/bin/cameraApp."
		echo "Now starting lbpDemoApp. Will shutdown if app exits cleanly."
		./run_demoapp.sh
	fi
fi


