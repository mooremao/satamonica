#!/bin/sh
CONSOLE_DEV="/dev/console"
MNT_DEV="/mnt/mmc"
UPDATE_IMG_FILE_POSTFIX=.tar.gz
UPDATE_TARBALL_VERSION_STRING=*${UPDATE_IMG_FILE_POSTFIX}

exec > $CONSOLE_DEV

check_and_trigger_fw_update()
{
    local update_found=0

    for file in $MNT_DEV/$UPDATE_TARBALL_VERSION_STRING
    do

        update_version=`expr match "$file" ".*_\([0-9]\{9,9\}\)$UPDATE_IMG_FILE_POSTFIX"`
        if [ "${update_version}" == "" ]; then
            continue;
        fi

        update_found=1
    done

    if [ ${update_found} -eq "1" ]; then
        echo "[Recovery] Found system update file on $DEVNAME"

        # Force reboot. To ensure startup does not continue
        # afterwards when this script is called during startup.
        reboot -f update
    fi
}

check_and_trigger_fw_update

echo "[Recovery] FINISHED"
echo
