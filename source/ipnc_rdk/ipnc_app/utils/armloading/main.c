/*
 * ctrl.c
 *
 * ============================================================================
 * Copyright (c) Texas Instruments Inc 2007
 *
 * Use of this software is controlled by the terms and conditions found in the
 * license agreement under which this software has been supplied or provided.
 * ============================================================================
 */

/* Standard Linux headers */
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* Function error codes */
#define SUCCESS         0
#define FAILURE         -1

/* True of false enumeration */
#define TRUE            1
#define FALSE           0

#ifdef __DEBUG
#define DBG(fmt, args...) fprintf(stderr, "Encode Debug: " fmt, ## args)
#else
#define DBG(fmt, args...)
#endif

#define ERR(fmt, args...) fprintf(stderr, "Encode Error: " fmt, ## args)

#define ARMLOAD_NUM_FRAMERATE		(100)

static int Quit_flag = FALSE;
static void signalHandler(int signum)
{
    fprintf(stderr, "Quitting!!!\n");
    Quit_flag = TRUE;
}

/******************************************************************************
 * getArmCpuLoad
 ******************************************************************************/
int getArmCpuLoad(int *procLoad, int *cpuLoad, int *ioWaitLoad)
{
    static unsigned long prevWaiting  = 0;
    static unsigned long prevTotal    = 0;
    static unsigned long prevProc     = 0;
    static unsigned long prevIOWait   = 0;
    int                  cpuLoadFound = FALSE;
    unsigned long        user, nice, sys, idle, iowait, irq, softirq;
    unsigned long        waiting, total, proc;
    unsigned long        uTime, sTime, cuTime, csTime;
    unsigned long        deltaTotal, deltaWaiting, deltaIOWait, deltaProc;
    char                 textBuf[4];
    FILE                *fptr;

    /* Read the overall system information */
    fptr = fopen("/proc/stat", "r");

    if (fptr == NULL) {
        ERR("/proc/stat not found. Is the /proc filesystem mounted?\n");
        return FAILURE;
    }

    /* Scan the file line by line */
    while (fscanf(fptr, "%4s %lu %lu %lu %lu %lu %lu %lu", textBuf,
                &user, &nice, &sys, &idle, &iowait, &irq, &softirq) != EOF) {
        if (strcmp(textBuf, "cpu") == 0) {
            cpuLoadFound = TRUE;
            break;
        }
    }

    if (fclose(fptr) != 0) {
        return FAILURE;
    }

    if (!cpuLoadFound) {
        return FAILURE;
    }

    /* Read the current process information */
    fptr = fopen("/proc/self/stat", "r");

    if (fptr == NULL) {
        ERR("/proc/self/stat not found. Is the /proc filesystem mounted?\n");
        return FAILURE;
    }

    if (fscanf(fptr, "%*d %*s %*s %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %lu "
                "%lu %lu %lu", &uTime, &sTime, &cuTime, &csTime) != 4) {
        ERR("Failed to get process load information.\n");
        fclose(fptr);
        return FAILURE;
    }

    if (fclose(fptr) != 0) {
        return FAILURE;
    }

    total = user + nice + sys + idle + iowait + irq + softirq;
    waiting = idle + iowait;
    proc = uTime + sTime + cuTime + csTime;

    /* Check if this is the first time, if so init the prev values */
    if (prevWaiting == 0 && prevTotal == 0 && prevProc == 0) {
        prevWaiting = waiting;
        prevIOWait = iowait;
        prevTotal = total;
        prevProc = proc;
        return SUCCESS;
    }

    deltaWaiting = waiting - prevWaiting;
    deltaIOWait = iowait - prevIOWait;
    deltaTotal = total - prevTotal;
    deltaProc = proc - prevProc;

    prevWaiting = waiting;
    prevIOWait = iowait;
    prevTotal = total;
    prevProc = proc;

    *cpuLoad = 100 - deltaWaiting * 100 / deltaTotal;
    *ioWaitLoad = deltaIOWait * 100 / deltaTotal;
    *procLoad = deltaProc * 100 / deltaTotal;

    return SUCCESS;
}

int main(int argc, char **argv)
{
    struct sigaction sigAction;
    double total = 0.0;
    int proc = 0, cpu = 0, iowait = 0, max = 0;
    int cnt = 0, i = 0;
    int armLoadCnt = ARMLOAD_NUM_FRAMERATE;
    FILE *fp;

    /* ensure a clean shutdown if user types ctrl-c */
    sigAction.sa_handler = signalHandler;
    sigemptyset(&sigAction.sa_mask);
    sigAction.sa_flags = 0;
    sigaction(SIGINT, &sigAction, NULL);

    if(argc == 1)
        armLoadCnt = ARMLOAD_NUM_FRAMERATE;
    else
        armLoadCnt = atoi(argv[1]);

    printf("ARM LOAD RUNNING FOR COUNT: %d\n", armLoadCnt);

    if ((fp = fopen("/run/log/timelog.txt", "wb")) == NULL) {
        printf("Can't create log file\n");
        Quit_flag = TRUE;
    }

    printf("   #\t  busy\t ( iowait )\n");

    while (!Quit_flag) {
        getArmCpuLoad(&proc, &cpu, &iowait);
        if ((cnt > 0) && (cnt <= armLoadCnt)) {
            if (cpu > max)
                max = cpu;
            total += cpu;i ++;
            fprintf(fp,"%4d\t%6d\t ( %6d )\n", cnt, cpu, iowait);
            printf("%4d\t%6d\t ( %6d )\n", cnt, cpu, iowait);
        } else if (cnt > armLoadCnt) {
            Quit_flag = TRUE;
        }
        cnt++;
        sleep(1);
    }

    total /= i;
    if (fp)
        fprintf(fp, "Average:%4.3f\t\tMax:%d\n", total, max);
    printf("Average:%4.3f\t\tMax:%d\n", total, max);

    if(fp)
        fclose(fp);
    return 0;
}
