#
# Makefile for LongBeach rescue image
# Copyright (C) 2014 TomTom International B.V.
#

$(RELEASE_DIR)/tt_recovery.img: $(RELEASE_DIR)/uImage
	@install -T $< $(RELEASE_DIR)/tt_recovery.img
	@find -iname tt_recovery.img | xargs md5sum | sed -e 's;\.\/release\/;;g' > $(RELEASE_DIR)/tt_recovery.img.md5
	@cp $(RELEASE_DIR)/tt_recovery.img $(RELEASE_DIR)/tt_recovery.img.keep
	@gzip  -f $(RELEASE_DIR)/tt_recovery.img
	@mv $(RELEASE_DIR)/tt_recovery.img.keep $(RELEASE_DIR)/tt_recovery.img

$(RELEASE_DIR)/$(RESCUE_FW_RAMFS): $(ASSETS) $(FB_DISPLAY) $(TUXERA_MOD) $(TARGET_SCRIPTS) $(BB) $(MTD) $(TOOLS)

$(RELEASE_DIR)/$(RESCUE_FW_RAMFS): $(RESCUE_FW_RAMFS_LIST)
	@mkdir -p `dirname $@`
	 $(GEN_INITRAMFS) -o $@ $(CPIO_FLAGS) $<

$(BB): config.busybox
	@install -CT $< $(BB_DIR)/.config
	$(MAKE) -C $(BB_DIR) -j$(NUMCPUS)

$(ASSETS):
	$(MAKE) -C $(ASSETS_DIR) install OUTPUT_DIR=$(ASSETS_INSTALL_DIR)

$(FB_DISPLAY):
	$(MAKE) -C $(FB_DISPLAY_DIR)
	cp $(FB_DISPLAY_DIR)/fb-display $@

$(TUXERA_INSTALL_DIR):
	mkdir -p $(TUXERA_INSTALL_DIR)

$(TUXERA_MOD): $(TUXERA_DIR)/tuxera-exfat-*-dm388-recovery.tgz | $(TUXERA_INSTALL_DIR)
	$(MAKE) -C $(TUXERA_DIR) KERNEL_MODULE_INSTALL_DIR=$(TUXERA_INSTALL_DIR) BUILD_TARGET=dm388-recovery install_module

.PHONY: recovery_updateexfatmod
recovery_updateexfatmod:
	$(MAKE) -C $(TUXERA_DIR) KERNEL_BUILD_DIR=$(KERNEL_BUILD_DIR) BUILD_TARGET=dm388-recovery update_module
	$(MAKE) -C $(TUXERA_DIR) KERNEL_MODULE_INSTALL_DIR=$(TUXERA_INSTALL_DIR) BUILD_TARGET=dm388-recovery install_module

$(MTD):
	$(MAKE) -C $(MTD_DIR) CROSS=$(BUILD_TOOL_CROSS)

$(RELEASE_DIR)/uImage: config.kernel $(RELEASE_DIR)/$(RESCUE_FW_RAMFS)
	@mkdir -p `dirname $@` $(KERNEL_BUILD_DIR)
	$(TMAKE) -C $(KERN_DIR) mrproper
	@install -CT $< $(KERNEL_BUILD_DIR)/.config
	$(TMAKE) -j$(NUMCPUS) -C $(KERN_DIR) O=$(KERNEL_BUILD_DIR) uImage
	$(MAKE) -C $(TUXERA_DIR) KERNEL_BUILD_DIR=$(KERNEL_BUILD_DIR) BUILD_TARGET=dm388-recovery check_module || ( echo "Binary module texfat needs rebuild. Please run \"make recovery_updateexfatmod\"" || false )
	@install -t $(RELEASE_DIR) $(addprefix $(KERNEL_BUILD_DIR)/, arch/arm/boot/uImage vmlinux System.map)
	@install -T $(KERNEL_BUILD_DIR)/.config $(RELEASE_DIR)/config.kernel

# EOF
