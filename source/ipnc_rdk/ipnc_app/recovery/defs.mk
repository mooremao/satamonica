#
# Makefile for LongBeach rescue image
# Copyright (C) 2014 TomTom International B.V.
#

BASE_INSTALL_DIR    := $(CURDIR)/../../..
# Defining the install base directory for IPNC RDK
IPNC_INSTALL_DIR    := $(BASE_INSTALL_DIR)/ipnc_rdk
TOOLS_INSTALL_DIR   := $(BASE_INSTALL_DIR)/ti_tools

# Defining the build tools directory for IPNC RDK
BUILD_TOOL_CROSS    := arm-arago-linux-gnueabi-
BUILD_TOOL_DIR      := $(TOOLS_INSTALL_DIR)/linux_devkit
BUILD_TOOL_PREFIX   := $(BUILD_TOOL_DIR)/bin/arm-arago-linux-gnueabi-
CODEGEN_PATH_A8     := $(BUILD_TOOL_DIR)

# Defining all the tools that are required for IPNC RDK
linuxutils_PATH     := $(TOOLS_INSTALL_DIR)/linuxutils_3_23_00_01

# The directory that points to the Linux Support Package
lsp_PATH	        := $(TOOLS_INSTALL_DIR)/ipnc_psp_arago
KERN_DIR            := $(lsp_PATH)/kernel

# The directory pointing to the assets directory
ASSETS_DIR          := $(BASE_INSTALL_DIR)/tt_tools/assets
ASSETS_INSTALL_DIR  := $(CURDIR)/gen_assets
ASSETS              := $(CURDIR)/gen_assets

# The directory pointing to the fb-display directory
FB_DISPLAY_DIR          := $(BASE_INSTALL_DIR)/tt_tools/fb-display
FB_DISPLAY_INSTALL_DIR  := $(CURDIR)/bin_tools
FB_DISPLAY              := $(CURDIR)/bin_tools/fb-display

# Tuxera exfat driver
TUXERA_DIR			:= $(BASE_INSTALL_DIR)/ext_tools/tuxera
TUXERA_INSTALL_DIR  := $(CURDIR)/bin_modules/
TUXERA_MOD			:= $(TUXERA_INSTALL_DIR)/texfat.ko

# The directory to CMEM library
CMEM_LIB_DIR := $(linuxutils_PATH)/packages/ti/sdo/linuxutils/cmem/lib
CMEM_INC_DIR := $(linuxutils_PATH)/packages/ti/sdo/linuxutils/cmem/include

# Kernel build dir
KERNEL_BUILD_DIR	:= $(CURDIR)/kbuild

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$(BUILD_TOOL_DIR)/bin/
export PATH

# Number of CPUs for parallel make jobs
NUMCPUS := `grep -c '^processor' /proc/cpuinfo`
export NUMCPUS

TOOLDIR		:= $(CURDIR)/tools
PATH		:= $(TOOLDIR):$(PATH)
GEN_INITRAMFS	:= gen_initramfs_list.sh
CPIO_FLAGS	:= -u 0 -g 0
TOOLS		:= $(TOOLDIR)/gen_init_cpio
E2FSIMAGE	:= $(TOOLDIR)/e2fsimage
CROSS_COMPILE	:= $(BUILD_TOOL_PREFIX)
TMAKE		:= ARCH=arm CROSS_COMPILE=$(CROSS_COMPILE) make
MKIMAGE		:= @mkimage

BB_VER		:= 1.18.2
BB_DIR		:= ./source/busybox-$(BB_VER)
BB			:= $(BB_DIR)/busybox

MTD_VER     := 1.4.4
MTD_DIR     := ./source/mtd-utils-$(MTD_VER)
MTD			:= $(MTD_DIR)/mtd-utils

TARGET_SCRIPTS	:= $(addprefix target_scripts/, init.ash)
RESCUE_FW_RAMFS_LIST	:= rescue_fw.list

RELEASE_DIR	:= $(CURDIR)/release

RESCUE_FW_RAMFS	:= rescue_fw.cpio.lzma

OUTPUT		:= $(addprefix $(RELEASE_DIR)/, uImage tt_recovery.img)

# EOF
