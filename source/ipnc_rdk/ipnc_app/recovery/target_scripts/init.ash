#!/bin/ash -u

# init.ash: LB recovery / update control program
# Based on NOR firmware control program for recovery of Strasbourg
#
# Copyright (c) TomTom International B.V. 2014
# Original author: Martin Jackson <martin.jackson@tomtom.com>
#

# ash can't trap on ERR. The trap would be use full to trigger a reboot
# when something fails.
# Instead we ignore errors in this script and defer almost all
# work to child processes instead. If on off these scripts fails, this script
# is still able to issue the reboot.
# If this script fails, we still have a WD watching us.
set +e

#
# Script parameters
#

export MIN_BATTERY_VOLTAGE=3600
export BATTERY_SOH_INVALID=0
export BATTERY_SOH_INSTANT=1
export BATTERY_SOH_INITIAL=2
export BATTERY_SOH_READY=3
export BATTERY_SOH_PERCENTAGE=40
export BATTERY_SOC_CAPACITY=50
export BATTERY_SOC_CAPACITY_PLUGGED=20
export TEST=""

###
setup ()
{
	busybox mkdir -p /sys /proc /tmp /bin /sbin /dev
	busybox mount -t proc proc /proc
	busybox --install -s
	mount -t sysfs sys /sys
	. ./wdt-kick &
}

###
teardown ()
{
    #  Flip flop cleared by kernel as part of reboot syscall.
    reboot -f
}

#
# Main script
#

setup

# Bootscreen is performed as child process. Do it as soon as possible, waiting 5
# second for interrupt, which triggers the recovery shell
./bootscreen

# 5 seconds to interrupt into recovery shell
echo "Hit [enter] for recovery shell ..."
read -t 5 && setsid cttyhack sh || echo "Proceeding with update"

# Update is performed as child process.
./update

# Whatever happens, teardown the recovery kernel.
teardown

# EOF
