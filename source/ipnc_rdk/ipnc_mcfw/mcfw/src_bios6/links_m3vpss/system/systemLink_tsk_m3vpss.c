/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <xdc/std.h>
#include <mcfw/interfaces/link_api/system_tiler.h>
#include <mcfw/src_bios6/utils/utils_tiler_allocator.h>
#include "systemLink_priv_m3vpss.h"
#include <mcfw/src_bios6/links_m3vpss/avsync/avsync_m3vpss.h>
#include <mcfw/interfaces/common_def/ti_vsys_common_def.h>
#include <mcfw/interfaces/common_def/ti_vdis_common_def.h>
#include <mcfw/src_bios6/utils/utils_iss.h>
#include <mcfw/src_bios6/utils/utils_mem_shared_pool.h>

#ifdef SINGLE_M3_CODE
	#include <mcfw/src_bios6/links_m3video/codec_utils/utils_encdec.h>
	Int Utils_encdecSetCh2IvahdMap(SystemVideo_Ivahd2ChMap_Tbl *Tbl);
#endif	

/* HOST_MAX_MEM_INFO is chosen considering number of memory allocations from host.
 * Max of 2 instances of IPC_BITS_OUT each capable max 4 memory pool allocations.
 * And also an instance of DCC memory allocation from the application
 */
#define HOST_MAX_MEM_INFO 10

#pragma DATA_ALIGN(gSystemLink_tskStack, 32)
#pragma DATA_SECTION(gSystemLink_tskStack, ".bss:taskStackSection")
UInt8 gSystemLink_tskStack[SYSTEM_TSK_STACK_SIZE];

SystemLink_Obj gSystemLink_obj;

SystemHost_MemInfo memInfo[HOST_MAX_MEM_INFO];
static Int hostId = 0;

Int32 SystemLink_cmdHandler(SystemLink_Obj * pObj, UInt32 cmd, Void * pPrm)
{
    Int32 status = FVID2_SOK;

    switch (cmd)
    {
        case SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES:
#ifndef _IPNC_HW_PLATFORM_
#ifdef SYSTEM_USE_VIDEO_DECODER
            status = System_videoResetVideoDevices();
#endif
#endif // #ifndef IPNC
            break;

        case SYSTEM_M3VPSS_CMD_GET_PLATFORM_INFO:
        {
            SystemVpss_PlatformInfo *prm = (SystemVpss_PlatformInfo *) pPrm;

            prm->cpuRev = Vps_platformGetCpuRev();
            prm->boardId = System_getBoardId();
            prm->baseBoardRev = System_getBaseBoardRev();
            prm->dcBoardRev = System_getDcBoardRev();
        }
            break;
#ifdef IPNC_DSS_ON
        case SYSTEM_M3VPSS_CMD_GET_DISPLAYCTRL_INIT:
        {
            VDIS_PARAMS_S * prm = (VDIS_PARAMS_S *) pPrm;

            status = System_displayCtrlInit(prm);
        }
            break;

        case SYSTEM_M3VPSS_CMD_GET_DISPLAYCTRL_DEINIT:
        {
            VDIS_PARAMS_S * prm = (VDIS_PARAMS_S *) pPrm;
            System_displayCtrlDeInit(prm);
        }
            break;
#endif
        case SYSTEM_COMMON_CMD_CPU_LOAD_CALC_START:
            Utils_prfLoadCalcStart();
            break;

        case SYSTEM_COMMON_CMD_CPU_LOAD_CALC_STOP:
            Utils_prfLoadCalcStop();
            break;

        case SYSTEM_COMMON_CMD_CPU_LOAD_CALC_RESET:
            Utils_prfLoadCalcReset();
            break;

        case SYSTEM_COMMON_CMD_PRINT_STATUS:
        {
            SystemCommon_PrintStatus *prm = (SystemCommon_PrintStatus *) pPrm;

            if (prm->printCpuLoad)
            {
                status = Utils_prfLoadPrintAll(prm->printTskLoad);
            }
            if (prm->printHeapStatus)
            {
                System_memPrintHeapStatus();
            }
        }
            break;

        case SYSTEM_COMMON_CMD_TILER_ALLOC:
        {
            SystemCommon_TilerAlloc *tilerAlloc =
                (SystemCommon_TilerAlloc *) pPrm;
            UInt32 utilsTilerCntMode =
                (tilerAlloc->cntMode - SYSTEM_TILER_CNT_FIRST) +
                UTILS_TILER_CNT_FIRST;

            if (((Int32) utilsTilerCntMode >= UTILS_TILER_CNT_FIRST)
                && (utilsTilerCntMode <= UTILS_TILER_CNT_LAST))
            {
                tilerAlloc->tileAddr =
                    Utils_tilerAllocatorAlloc(utilsTilerCntMode,
                                              tilerAlloc->width,
                                              tilerAlloc->height);
                if (UTILS_TILER_INVALID_ADDR ==
                    tilerAlloc->tileAddr)
                {
                    tilerAlloc->tileAddr = SYSTEM_TILER_INVALID_ADDR;
                }
            }
            else
            {
                tilerAlloc->tileAddr = SYSTEM_TILER_INVALID_ADDR;

            }
            break;
        }
        case SYSTEM_COMMON_CMD_TILER_FREE:
        {
            SystemCommon_TilerFree *tilerFree = (SystemCommon_TilerFree *) pPrm;

            status = Utils_tilerAllocatorFree(tilerFree->tileAddr);

            break;
        }

        case SYSTEM_COMMON_CMD_TILER_FREE_ALL:
        {
            status = Utils_tilerAllocatorFreeAll();

            break;
        }

        case SYSTEM_COMMON_CMD_TILER_DISABLE_ALLOCATOR:
        {
            status = Utils_tilerAllocatorDisable();

            break;
        }
        case SYSTEM_COMMON_CMD_TILER_ENABLE_ALLOCATOR:
        {
            status = Utils_tilerAllocatorEnable();

            break;
        }

        case SYSTEM_COMMON_CMD_TILER_IS_ALLOCATOR_DISABLED:
        {
            SystemCommon_TilerIsDisabled *tilerAllocatorStatus =
              (SystemCommon_TilerIsDisabled *) pPrm;

            tilerAllocatorStatus->isAllocatorDisabled =
            Utils_tilerAllocatorIsDisabled();

            status = 0;
            break;
        }
        case SYSTEM_COMMON_CMD_TILER_ALLOC_RAW:
        {
            SystemCommon_TilerAllocRaw *tilerRawAlloc =
                (SystemCommon_TilerAllocRaw *) pPrm;

            tilerRawAlloc->allocAddr  =
            (UInt32) Utils_tilerAllocatorAllocRaw(tilerRawAlloc->size,
                                                  tilerRawAlloc->align);
            break;
        }
        case SYSTEM_COMMON_CMD_TILER_FREE_RAW:
        {
            SystemCommon_TilerFreeRaw *tilerRawFree =
                (SystemCommon_TilerFreeRaw *) pPrm;

            Utils_tilerAllocatorFreeRaw((Ptr)tilerRawFree->allocAddr,
                                        tilerRawFree->size);

            break;
        }
        case SYSTEM_COMMON_CMD_TILER_GET_FREE_SIZE:
        {
            SystemCommon_TilerGetFreeSize *tilerFreeSize =
                (SystemCommon_TilerGetFreeSize *) pPrm;

            tilerFreeSize->freeSizeRaw = Utils_tilerAllocatorGetFreeSizeRaw();
            tilerFreeSize->freeSize8b  = Utils_tilerAllocatorGetFreeSize(UTILS_TILER_CNT_8BIT);
            tilerFreeSize->freeSize16b = Utils_tilerAllocatorGetFreeSize(UTILS_TILER_CNT_16BIT);
            tilerFreeSize->freeSize32b = Utils_tilerAllocatorGetFreeSize(UTILS_TILER_CNT_32BIT);

            break;
        }
#ifdef IPNC_DSS_ON
	case SYSTEM_M3VPSS_CMD_SET_DISPLAYCTRL_VENC_OUTPUT:
        {
            VDIS_DEV_PARAM_S * prm = (VDIS_DEV_PARAM_S *) pPrm;

            status = System_displayCtrlSetVencOutput(prm);
            break;
        }
#endif
        case SYSTEM_M3VPSS_CMD_GET_AVSYNC_SHAREDOBJ_PTR:
        {
            status = AVSYNC_GetSharedDataStructure( (SystemVpss_AvSyncSharedObjPtr*) pPrm );
            break;
        }
#ifdef SINGLE_M3_CODE
        case SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL:
        {
            SystemVideo_Ivahd2ChMap_Tbl *tbl = 
                        (SystemVideo_Ivahd2ChMap_Tbl*) pPrm;
            Utils_encdecSetCh2IvahdMap(tbl);
            break;
        }		

        case SYSTEM_COMMON_CMD_INIT_GPIO_FOR_IVA_VOLTAGE_SCALING:
        {
            Utils_encdecInitGPIOForIVAVoltageScaling(((SystemVideo_InitGPIOForIvaVoltageScaling*)pPrm)->iva,
                                                     ((SystemVideo_InitGPIOForIvaVoltageScaling*)pPrm)->gpio_bank,    
                                                     ((SystemVideo_InitGPIOForIvaVoltageScaling*)pPrm)->gpio_num,
                                                     ((SystemVideo_InitGPIOForIvaVoltageScaling*)pPrm)->gpio_level_for_high_voltage,
                                                     ((SystemVideo_InitGPIOForIvaVoltageScaling*)pPrm)->iva_init_voltage);
            break;
        }
		
        case SYSTEM_COMMON_CMD_SET_IVA_FREQ_VOLTAGE:
        {
            Utils_encdecSetIvaFreqVoltage(((SystemVideo_SetIvaFreqVoltage*)pPrm)->iva,
										  ((SystemVideo_SetIvaFreqVoltage*)pPrm)->freq,
										  ((SystemVideo_SetIvaFreqVoltage*)pPrm)->voltage);
            break;
        }		
#endif		
        case SYSTEM_COMMON_CMD_CORE_STATUS:
            Vps_printf(" %d: Core is active\n",Utils_getCurTimeInMsec());
            break;

        case SYSTEM_M3VPSS_CMD_SET_SIMCOP_ON:
            Utils_setSimcopOn();
            break;

        case SYSTEM_M3VPSS_CMD_SET_SIMCOP_OFF:
            Utils_setSimcopOff();
            break;

		case SYSTEM_M3VPSS_CMD_SYSTEM_USE_TILER:
			gSystemUseTiler = *((UInt32*)pPrm);
			UTILS_assert((gSystemUseTiler == 1) || (gSystemUseTiler == 0));
			break;

        case SYSTEM_CMD_SYSTEM_STORE_HOST_MEM_INFO:
            {
                SystemHost_MemInfo *prm = (SystemHost_MemInfo *) pPrm;
                if(hostId < HOST_MAX_MEM_INFO) {
                    memInfo[hostId].bufAddr = prm->bufAddr;
                    memInfo[hostId].bufSize = prm->bufSize;
                    hostId++;
                }
                else
                    Vps_printf("%d: Not enough space to store the host memory information\n",
                                            Utils_getCurTimeInMsec());
                break;
            }

        case SYSTEM_CMD_SYSTEM_FREE_HOST_MEM:
            {
                Int32 i;
                for(i=0; i<hostId; i++) {
                    if(memInfo[i].bufAddr != NULL) {
                        Utils_memFree(memInfo[i].bufAddr, memInfo[i].bufSize);
                        memInfo[i].bufAddr = NULL;
                        memInfo[i].bufSize = NULL;
                    }
		}
                hostId = 0;
                break;
            }
        case SYSTEM_CMD_ALLOC_FROM_SHARED_POOL:
            {
              SystemCommon_SharedPoolAlloc *sharedPoolAlloc =
                       (SystemCommon_SharedPoolAlloc *)pPrm;

              sharedPoolAlloc->allocAddr  = 
                        (UInt32 )Utils_memSharedAllocFromPool((UInt32) sharedPoolAlloc->size,
                                            sharedPoolAlloc->align);
              break;
            }
        default:
            break;
    }

    return status;
}

Void SystemLink_tskMain(struct Utils_TskHndl * pTsk, Utils_MsgHndl * pMsg)
{
    Int32 status;
    SystemLink_Obj *pObj = (SystemLink_Obj *) pTsk->appData;

    status = SystemLink_cmdHandler(pObj,
                                   Utils_msgGetCmd(pMsg),
                                   Utils_msgGetPrm(pMsg));
    Utils_tskAckOrFreeMsg(pMsg, status);

    return;
}

Int32 SystemLink_init()
{
    Int32 status;
    System_LinkObj linkObj;
    SystemLink_Obj *pObj;
    char tskName[32];

    pObj = &gSystemLink_obj;

    memset(pObj, 0, sizeof(*pObj));

    pObj->tskId = SYSTEM_LINK_ID_M3VPSS;

    linkObj.pTsk = &pObj->tsk;
    linkObj.linkGetFullFrames = NULL;
    linkObj.linkPutEmptyFrames = NULL;
    linkObj.getLinkInfo = NULL;

    System_registerLink(pObj->tskId, &linkObj);

    sprintf(tskName, "SYSTEM_M3VPSS%d", pObj->tskId);

    status = Utils_tskCreate(&pObj->tsk,
                             SystemLink_tskMain,
                             SYSTEM_TSK_PRI_HIGH,
                             gSystemLink_tskStack,
                             SYSTEM_TSK_STACK_SIZE, pObj, tskName);
    UTILS_assert(status == FVID2_SOK);

    return status;
}

Int32 SystemLink_deInit()
{
    Utils_tskDelete(&gSystemLink_obj.tsk);

    return FVID2_SOK;
}
