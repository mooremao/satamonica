/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <mcfw/src_bios6/utils/utils.h>
#include <mcfw/interfaces/link_api/system.h>
#include <mcfw/src_bios6/links_common/system/system_priv_common.h>

#include <ti/psp/proxyServer/vps_proxyServer.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/Notify.h>
#include <ti/syslink/SysLink.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Task.h>

#ifdef SINGLE_M3_CODE
	#include <mcfw/src_bios6/links_m3video/codec_utils/hdvicp2_config.h>
#endif

#define VPS_PROXY_SERVER_SUPPORT_A8
#define VPS_PS_MAX_NO_OF_CORES              (0x02u)
#define VPS_PS_STARTING_EVENT_NO            (0x0Au)
#define VPS_PS_TASK_PRIORITY_TO_START       (0x08u)
#define VPS_PS_RESERVED_EVENT_NO            (0x09u)

#define VPS_POWER_EVENT_NO                  (0x1Fu)
#define VPS_POWER_LINE_NO                   (0x00u)
#define VPS_SUSPEND_PAYLOAD                 (0x00u)
#define VPS_RESUME_PAYLOAD                  (0x01u)

#define VPS_NVIC_MAX_INTERRUPT              (0x100u)
#define VPS_A8_TO_M3_MBOX_INT               (0x36u)

static UInt8 hostNotifyMem[100*1024];
#pragma DATA_SECTION(hostNotifyMem,".vpss:extMemNonCache:notify")

Int32 System_start(Task_FuncPtr chainsMainFunc);
Void System_procStart();
Void System_procStop();
Void System_procInit();
Void System_procDeinit();
Void System_systemUseTiler(UInt32 systemUseTiler);
/*
 * Global variables for Power Management Functions
 */
static Semaphore_Handle Power_semSuspend = NULL;
UInt32 Power_initDone = 0;

/*
 * Initialize the proxy server
 */

static Int32 initProxyServer(void)
{
    Int32   rtnVal;
    UInt32  noOfCores, noOfEvents, eventNos, taskIndex, taskPri;
    VPS_PSrvInitParams  psInitParams;
    UInt16 remoteProcId;

    psInitParams.sysLnkNoOfCores        =   0x01;

    for(noOfCores = 0x0; noOfCores < VPS_PS_MAX_NO_OF_CORES; noOfCores++)
    {
        eventNos    =   VPS_PS_STARTING_EVENT_NO;
        psInitParams.sysLnkNoOfNtyEvt[noOfCores]    =   16;
        for(noOfEvents = 0x0;
                noOfEvents < psInitParams.sysLnkNoOfNtyEvt[noOfCores];
                        noOfEvents++)
        {
            psInitParams.sysLnkNtyEvtNo[noOfCores][noOfEvents]  =   eventNos;
            eventNos++;
        }
        psInitParams.resSysLnkNtyNo[noOfCores]      =   VPS_PS_RESERVED_EVENT_NO;
    }
    taskPri =   VPS_PS_TASK_PRIORITY_TO_START;
    for (taskIndex = 0x0; taskIndex < 0x05u; taskIndex++)
    {
        psInitParams.taskPriority[taskIndex]        =   taskPri;
        taskPri--;
    }

    psInitParams.completionCbTaskPri = VPS_PS_TASK_PRIORITY_TO_START + 1;

    remoteProcId = System_getSyslinkProcId(SYSTEM_PROC_HOSTA8);
    do {
        rtnVal = Notify_attach(remoteProcId, (Ptr)hostNotifyMem);
        Vps_printf ("notify_attach  rtnVal  %d\n", rtnVal);
    } while (rtnVal < 0);

    psInitParams.sysLnkCoreNames[0x0]       =   System_getProcName(SYSTEM_PROC_HOSTA8);
    rtnVal  =   VPS_PSrvInit(&psInitParams);

    return (rtnVal);
}

static Void Power_notifyCb(UInt16 procId, UInt16 lineId,
        UInt32 eventId, UArg arg, UInt32 payload)
{
    if(payload == VPS_SUSPEND_PAYLOAD)
    {
        Semaphore_post(Power_semSuspend);
    }
}

Void Power_suspendTaskFxn(UArg arg0, UArg arg1)
{
    UInt32 i;
    UInt32 hwiKey;
    UInt32 swiKey;
    UInt32 restoreVal[VPS_NVIC_MAX_INTERRUPT];

    while (1)
    {
        /* Wait for suspend notification from host-side */
        Semaphore_pend(Power_semSuspend, BIOS_WAIT_FOREVER);

        if(Power_initDone)
        {
            /* Disable all interrupts except mailbox */

            swiKey = Swi_disable();
            hwiKey = Hwi_disable();

            for(i = 0; i < VPS_NVIC_MAX_INTERRUPT; i++)
            {
                if(i != VPS_A8_TO_M3_MBOX_INT)
                {
                    restoreVal[i] = Hwi_disableInterrupt(i);
                }
            }

            Hwi_restore(hwiKey);
            Swi_restore(swiKey);

            /* WFI */
            Vps_printf("Entering WFI\n");
            asm(" wfi");
            Vps_printf ("Exiting WFI\n");

            /* Enable all interrupts */

            swiKey = Swi_disable();
            hwiKey = Hwi_disable();

            for(i = 0; i < VPS_NVIC_MAX_INTERRUPT; i++)
            {
                if((i != VPS_A8_TO_M3_MBOX_INT) && restoreVal[i])
                {
                    Hwi_enableInterrupt(i);
                }
            }

            Hwi_restore(hwiKey);
            Swi_restore(swiKey);
        }
    }
}

Void idleFunction(void)
{
    /* This function gets executed in the idle task */
    asm("\twfi\n");
}

Void M3VPSS_main(UArg arg0, UArg arg1)
{
    char ch = 0;
    System_M3InitInfo *m3InitInfo = NULL;

    System_procStart();

    System_getM3InitCheckPhyAddr(&m3InitInfo);
    UTILS_assert(m3InitInfo != NULL);

    m3InitInfo->isInitDone = TRUE;
    while (1)
    {
        Task_sleep(100);
        if(System_getShutdownFlag() == TRUE) {
            System_stop();
            System_delete();
            System_procStop();
            System_procStart();
        }

        Utils_getChar(&ch, BIOS_NO_WAIT);
        if (ch == 'x')
            break;
    }
}

Int32 main(void)
{
    Int32 retVal;
    Int32 remoteProcId;
    Task_Params taskParams;

    Utils_setCpuFrequency(SYSTEM_M3VPSS_FREQ);

    SysLink_setup ();

    retVal = initProxyServer();
    Vps_printf ("initProxyServer  rtnVal  %d\n", retVal);
    
#ifdef SINGLE_M3_CODE   	
	HDVICP2_ClearIVAInterrupts();	
#endif	

    Power_semSuspend = Semaphore_create(0, NULL, NULL);

    Task_Params_init(&taskParams);
    taskParams.priority = Task_numPriorities - 1; /* Highest priority */
    taskParams.instance->name = "ti.pm.Power_tskSuspend";
    Task_create(Power_suspendTaskFxn, &taskParams, NULL);

    /* M3 Power Management Initializations */
    remoteProcId = System_getSyslinkProcId(SYSTEM_PROC_HOSTA8);
    retVal = Notify_registerEvent(remoteProcId,
            VPS_POWER_LINE_NO,
            VPS_POWER_EVENT_NO,
            Power_notifyCb,
            0);

    Power_initDone = 1;

    System_start(M3VPSS_main);
    BIOS_start();

    return (0);
}

