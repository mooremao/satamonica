/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/ipc/SharedRegion.h>
#include <mcfw/src_bios6/utils/utils_mem.h>
#include <mcfw/interfaces/link_api/system_debug.h>
#include <mcfw/interfaces/link_api/system_common.h>
#include <mcfw/interfaces/link_api/system_tiler.h>
#include <mcfw/src_bios6/utils/utils_mem_shared_pool.h>

/* See utils_mem.h for function documentation */

#define UTILS_MEM_DEBUG

#ifdef CAMERA_OUT_MEMORY_TEST
#define CAMERA_EXTRA_OUT_MEM_TOP       (256)
#define CAMERA_EXTRA_OUT_MEM_BOTTOM    (256)
#define CAMERA_EXTRA_OUT_MEM_SIZE      (2 * (CAMERA_EXTRA_OUT_MEM_TOP + CAMERA_EXTRA_OUT_MEM_BOTTOM))
#define BOXCAR_EXTRA_OUT_MEM_TOP       (4096)
#define BOXCAR_EXTRA_OUT_MEM_BOTTOM    (4096)
#define BOXCAR_EXTRA_OUT_MEM_SIZE      (BOXCAR_EXTRA_OUT_MEM_TOP + BOXCAR_EXTRA_OUT_MEM_BOTTOM)
#endif


/* Memory pool handle */
IHeap_Handle gUtils_heapMemHandle[UTILS_MEM_NUM_SHARED_REGION_HEAP] = {NULL};
Bool         gUtils_memClearBuf[UTILS_MEM_NUM_SHARED_REGION_HEAP] = {FALSE};

/* Used for RPE - ENSURE that this is cached as this is used for alg memtab allocation */
void* Utils_getAlgMemoryHeapHandle (void)
{
    return ((void*) gUtils_heapMemHandle[UTILS_MEM_VID_HEAP_SR1]);
}

Int32 Utils_memInit()
{
    SharedRegion_Entry srEntry;
    Int                srStatus = SharedRegion_S_SUCCESS;
    UInt32 srId[UTILS_MEM_NUM_SHARED_REGION_HEAP], i;

    srId[UTILS_MEM_VID_HEAP_SR1]  = SYSTEM_IPC_SR1;
    srId[UTILS_MEM_VID_HEAP_SR2] = SYSTEM_IPC_SR2;


    for (i=0; i<UTILS_MEM_NUM_SHARED_REGION_HEAP; i++)
    {
        SharedRegion_entryInit(&srEntry);
        SharedRegion_getEntry(srId[i], &srEntry);
    #ifdef UTILS_MEM_DEBUG
        Vps_printf (" %d: MEM: Shared Region %d: Base = 0x%08x, Length = 0x%08x (%d MB) \n",
                    Utils_getCurTimeInMsec(), srId[i],srEntry.base,srEntry.len, srEntry.len/(1024*1024));
    #endif
        if ((FALSE == srEntry.isValid)
            &&
            (0 != srEntry.len))
        {
            srEntry.isValid     = TRUE;
            do {
                srStatus = SharedRegion_setEntry(srId[i], &srEntry);

                if (srStatus != SharedRegion_S_SUCCESS) {
                    Vps_printf(" %d: MEM: ERROR: SharedRegion_setEntry (%d, 0x%08x) FAILED !!! "
                               " (status=%d) \n", Utils_getCurTimeInMsec(), srId[i], &srEntry, srStatus);
                    Task_sleep(10);
                }
            } while (srStatus != SharedRegion_S_SUCCESS);
        }
        if (srEntry.len)
        {
            gUtils_heapMemHandle[i] = SharedRegion_getHeap(srId[i]);
            UTILS_assert(gUtils_heapMemHandle[i] != NULL);
            gUtils_memClearBuf[i] = FALSE;
        }
    }
    return 0;
}

Int32 Utils_memDeInit()
{
    UInt32 i;

    for (i=0; i<UTILS_MEM_NUM_SHARED_REGION_HEAP; i++)
    {
        /* delete memory pool heap */
        gUtils_heapMemHandle[i] = NULL;
    }
    return 0;
}

/**
  Get buffer size based on data format

  pFormat - data format information
  *size - buffer size
  *cOffset - C plane offset for YUV420SP data
*/
Int32 Utils_memFrameGetSize(FVID2_Format * pFormat,
                            UInt32 * size, UInt32 * cOffset)
{
    UInt32 bufferHeight;
    Int32 status = 0;

    bufferHeight = pFormat->height;

    switch (pFormat->dataFormat)
    {
        case FVID2_DF_YUV422I_YUYV:
        case FVID2_DF_YUV422I_YVYU:
        case FVID2_DF_YUV422I_UYVY:
        case FVID2_DF_YUV422I_VYUY:
        case FVID2_DF_YUV444I:
        case FVID2_DF_RGB24_888:
        case FVID2_DF_RAW_VBI:
		case FVID2_DF_BAYER_RAW:

            /* for single plane data format's */
            *size = pFormat->pitch[0] * bufferHeight;
            break;

        case FVID2_DF_YUV422SP_UV:
        case FVID2_DF_YUV420SP_UV:
        case FVID2_DF_YUV420SP_VU:

            /* for Y plane */
            *size = pFormat->pitch[0] * bufferHeight;

            /* cOffset is at end of Y plane */
            *cOffset = *size;

            if (pFormat->dataFormat == FVID2_DF_YUV420SP_UV ||
                pFormat->dataFormat == FVID2_DF_YUV420SP_VU)
            {
                /* C plane height is 1/2 of Y plane */
                bufferHeight = bufferHeight / 2;
            }

            /* for C plane */
            *size += pFormat->pitch[1] * bufferHeight;
            break;

        default:
            /* illegal data format */
            status = -1;
            break;
    }

    /* align size to minimum required frame buffer alignment */
    *size = VpsUtils_align(*size, VPS_BUFFER_ALIGNMENT);

    return status;
}

Int32 Utils_memFrameAlloc(FVID2_Format * pFormat,
                          FVID2_Frame * pFrame, UInt16 numFrames, Int32 srId)
{
    UInt32 size, cOffset, frameId;
    Int32 status;
    UInt8 *pBaseAddr;

    /* init FVID2_Frame to 0's */
    memset(pFrame, 0, sizeof(*pFrame)*numFrames);

    /* align height to multiple of 16 */
    pFormat->height = VpsUtils_align(pFormat->height, 16);

    /* get frame size for given pFormat */
    status = Utils_memFrameGetSize(pFormat, &size, &cOffset);

    if (status == 0)
    {
        /* allocate the memory for 'numFrames' */

        /* for all 'numFrames' memory is contigously allocated */
        pBaseAddr = Utils_memAlloc(size * numFrames, VPS_BUFFER_ALIGNMENT, srId);

        if (pBaseAddr == NULL)
        {
            status = -1;                                   /* Error in
                                                            * allocation,
                                                            * exit with error
                                                            */
        }
    }

    if (status == 0)
    {
        /* init memory pointer for 'numFrames' */
        for (frameId = 0; frameId < numFrames; frameId++)
        {

            /* copy channelNum to FVID2_Frame from FVID2_Format */
            pFrame->channelNum = pFormat->channelNum;
            pFrame->addr[0][0] = pBaseAddr;

            switch (pFormat->dataFormat)
            {
                case FVID2_DF_RAW_VBI:
                case FVID2_DF_YUV422I_UYVY:
                case FVID2_DF_YUV422I_VYUY:
                case FVID2_DF_YUV422I_YUYV:
                case FVID2_DF_YUV422I_YVYU:
                case FVID2_DF_YUV444I:
                case FVID2_DF_RGB24_888:
                case FVID2_DF_BAYER_RAW:
                    break;
                case FVID2_DF_YUV422SP_UV:
                case FVID2_DF_YUV420SP_UV:
                case FVID2_DF_YUV420SP_VU:
                    /* assign pointer for C plane */
                    pFrame->addr[0][1] = (UInt8 *) pFrame->addr[0][0] + cOffset;
                    break;
                default:
                    /* illegal data format */
                    status = -1;
                    break;
            }
            /* go to next frame */
            pFrame++;

            /* increment base address */
            pBaseAddr += size;
        }
    }

    if (status != 0)
        Vps_printf("Memory allocation failed due to insufficient free memory \n");
    /* commented out, so please always check the status immediately after the
     * Utils_memFrameAlloc() call and handle accordingly */
    // UTILS_assert(status == 0);

    return status;
}

Int32 Utils_memFrameAlloc_capture(FVID2_Format * pFormat,
                                  FVID2_Frame * pFrame, UInt16 numFrames,
                                  Int32 Padding, Int32 BoxCarDataSize, Int srId, UInt32 createPool)
{
    UInt32 size, cOffset, frameId;

    Int32 status;

    UInt8 *pBaseAddr = NULL;
	UInt8 *pBaseAddrBC = NULL;

    /* init FVID2_Frame to 0's */
    memset(pFrame, 0, sizeof(*pFrame));

    /* align height to multiple of 16 */
    pFormat->height = VpsUtils_align(pFormat->height, 16);

    /* get frame size for given pFormat */
    status = Utils_memFrameGetSize(pFormat, &size, &cOffset);
    if (status == 0)
    {
#ifdef CAMERA_OUT_MEMORY_TEST
        size += CAMERA_EXTRA_OUT_MEM_SIZE;
        if (BoxCarDataSize != 0)
          BoxCarDataSize += BOXCAR_EXTRA_OUT_MEM_SIZE;
#endif
        /* allocate the memory for 'numFrames' */

        /* for all 'numFrames' memory is contigously allocated */
        pBaseAddr =
            Utils_memAlloc((size + Padding ) * numFrames, VPS_BUFFER_ALIGNMENT, srId);
        if(BoxCarDataSize != 0)
            pBaseAddrBC = Utils_memAlloc(( BoxCarDataSize) * numFrames, 4*1024, -1);

#ifdef CAMERA_OUT_MEMORY_TEST
      /*Fill known pattern*/
      if (pBaseAddr)
          memset(pBaseAddr, 0x00, (size + Padding ) * numFrames);
      if (pBaseAddrBC)
          memset(pBaseAddrBC, 0x00, (BoxCarDataSize) * numFrames);
#endif

        if (pBaseAddr == NULL)
        {
            status = -1;                                   /* Error in
                                                            * allocation,
                                                            * exit with error
                                                            */
        }

        if (createPool == TRUE)
        {
            status = Utils_memSharedPoolInit(pBaseAddr, (size + Padding ) * numFrames);
        }

    }

    if (status == 0)
    {
        /* init memory pointer for 'numFrames' */
        for (frameId = 0; frameId < numFrames; frameId++)
        {

            /* copy channelNum to FVID2_Frame from FVID2_Format */
            pFrame->channelNum = pFormat->channelNum;
#ifdef CAMERA_OUT_MEMORY_TEST
            pFrame->addr[1][0] = pBaseAddr + CAMERA_EXTRA_OUT_MEM_TOP;
            pFrame->addr[0][0] = pBaseAddr + CAMERA_EXTRA_OUT_MEM_TOP;
            if(BoxCarDataSize != 0)
              pFrame->blankData = pBaseAddrBC + BOXCAR_EXTRA_OUT_MEM_TOP;
#else
            pFrame->addr[1][0] = pBaseAddr;
            pFrame->addr[0][0] = pBaseAddr;
			if(BoxCarDataSize != 0)
				pFrame->blankData = pBaseAddrBC;
#endif
				
			{	
				switch (pFormat->dataFormat)
				{
					case FVID2_DF_RAW_VBI:
					case FVID2_DF_BAYER_RAW:
					case FVID2_DF_YUV422I_UYVY:
					case FVID2_DF_YUV422I_VYUY:
					case FVID2_DF_YUV422I_YUYV:
					case FVID2_DF_YUV422I_YVYU:
					case FVID2_DF_YUV444I:
					case FVID2_DF_RGB24_888:
					case FVID2_DF_YUV422P:
						break;
					case FVID2_DF_YUV422SP_UV:
					case FVID2_DF_YUV420SP_UV:
					case FVID2_DF_YUV420SP_VU:
#ifdef CAMERA_OUT_MEMORY_TEST
						/* assign pointer for C plane */
						pFrame->addr[0][1] = (UInt8 *) pFrame->addr[0][0] + cOffset + CAMERA_EXTRA_OUT_MEM_BOTTOM +/*End of Y buffer*/
																	CAMERA_EXTRA_OUT_MEM_TOP ; /*Padding for UV top*/
						pFrame->addr[1][1] = pFrame->addr[0][1];
#else
						/* assign pointer for C plane */
						pFrame->addr[0][1] = (UInt8 *) pFrame->addr[0][0] + cOffset;
						pFrame->addr[1][1] = (UInt8 *) pFrame->addr[0][0] + cOffset;
#endif
						break;
					default:
						/* illegal data format */
						status = -1;
						break;
				}
			}	
				
            /* go to next frame */
            pFrame++;

            /* increment base address */
            pBaseAddr += (size + Padding );
			if(BoxCarDataSize != 0) pBaseAddrBC += (  BoxCarDataSize);
        }
    }

    UTILS_assert(status == 0);

    return status;
}
Int32 Utils_memFrameFree(FVID2_Format * pFormat,
                         FVID2_Frame * pFrame, UInt16 numFrames)
{
    UInt32 size, cOffset;
    Int32 status;

	pFormat->height = VpsUtils_align(pFormat->height, 16);
	
    /* get frame size for given 'pFormat' */
    status = Utils_memFrameGetSize(pFormat, &size, &cOffset);

    if (status == 0)
    {
        /* free the frame buffer memory */

        /* for all 'numFrames' memory is allocated contigously during alloc,
         * so first frame memory pointer points to the complete memory block
         * for all frames */
        Utils_memFree(pFrame->addr[0][0], size * numFrames);
    }

    return 0;
}

Int32 Utils_memFrameFree_Capture(FVID2_Format * pFormat,
                                 FVID2_Frame * pFrame, UInt16 numFrames,
                                 Int32 Padding, Int32 BoxCarDataSize, UInt32 deInitPool)
{
    UInt32 size, cOffset;
    UInt32 poolSize;
    UInt8* poolStartAddr;

    Int32 status;

	pFormat->height = VpsUtils_align(pFormat->height, 16);
	
	{	
		/* get frame size for given 'pFormat' */
		status = Utils_memFrameGetSize(pFormat, &size, &cOffset);
	}	

    if (status == 0)
    {
#ifdef CAMERA_OUT_MEMORY_TEST
        size += CAMERA_EXTRA_OUT_MEM_SIZE;
        pFrame->addr[1][0] = (UInt8 *)pFrame->addr[1][0] - CAMERA_EXTRA_OUT_MEM_TOP; /*actual allocated pointer*/
        if(BoxCarDataSize != 0)
        {
          BoxCarDataSize += BOXCAR_EXTRA_OUT_MEM_SIZE;
          pFrame->blankData =(UInt8 *)pFrame->blankData - BOXCAR_EXTRA_OUT_MEM_TOP;
        }
#endif

        if ( deInitPool && Utils_memSharedPoolIsActive() )
        {
            Utils_memSharedPoolGetDetails (&poolStartAddr, &poolSize);
            if ((poolStartAddr == pFrame->addr[1][0]) &&
                (poolSize == (size + Padding ) * numFrames))
            {
                Utils_memSharedPoolDeinit();
            }
            else
            {
                return -1;
            }
        }

        /* free the frame buffer memory */

        /* for all 'numFrames' memory is allocated contigously during alloc,
         * so first frame memory pointer points to the complete memory block
         * for all frames */
        Utils_memFree(pFrame->addr[1][0], (size + Padding ) * numFrames);

        if(BoxCarDataSize != 0)
            Utils_memFree(pFrame->blankData, (  BoxCarDataSize) * numFrames);
    }

    return 0;
}

Ptr Utils_memAlloc(UInt32 size, UInt32 align, Int32 srId)
{
    Ptr addr;
    Error_Block ebObj;
    Error_Block *eb = &ebObj;

    Error_init(eb);

    if (srId == UTILS_MEM_VID_SHARED_POOL_ID)
    {
        #ifdef UTILS_MEM_DEBUG
        Vps_printf(" UTILS: MEM: Alloc'ing from SR%d. (required size = %d B, free space = %d B)\n",
            srId+1,
            size,
            Utils_memSharedPoolGetFreeSpace());
        #endif

        addr = Utils_memSharedAllocFromPool(size, align);
        return (addr);
    }

    if ((srId < UTILS_MEM_VID_HEAP_SR1) ||
        (srId > UTILS_MEM_VID_HEAP_SR2))
    {
        srId = UTILS_MEM_VID_HEAP_SR1;
    }

    #ifdef UTILS_MEM_DEBUG
    Vps_printf(" UTILS: MEM: Alloc'ing from SR%d. (required size = %d B, free space = %d B)\n",
        srId+1,
        size,
        (srId == UTILS_MEM_VID_HEAP_SR1) ? Utils_memGetSR1HeapFreeSpace() : Utils_memGetSR2HeapFreeSpace()
        );
    #endif

    /* allocate memory */
    addr = Memory_alloc(gUtils_heapMemHandle[srId], size, align, eb);

    if(addr==NULL)
    {
        srId = (srId == UTILS_MEM_VID_HEAP_SR1) ? UTILS_MEM_VID_HEAP_SR2 : UTILS_MEM_VID_HEAP_SR1;
        {
            #ifdef UTILS_MEM_DEBUG
            Vps_printf(" UTILS: MEM: Alloc'ing from SR%d. (required size = %d B, free space = %d B)\n",
              srId+1,
              size,
              (srId == UTILS_MEM_VID_HEAP_SR1) ? Utils_memGetSR1HeapFreeSpace() : Utils_memGetSR2HeapFreeSpace()
              );
            #endif

            Error_init(eb);
            addr = Memory_alloc(gUtils_heapMemHandle[srId], size, align, eb);
        }

    }

    #ifdef UTILS_MEM_DEBUG
    Vps_printf(" UTILS: MEM: FRAME ALLOC, addr = 0x%08x, size = %d bytes \n",
                addr, size
               );
    #endif

    if (!Error_check(eb)
        &&
        (addr != NULL)
        &&
        gUtils_memClearBuf[srId])
    {
        memset(addr, 0x80, size);
    }

    return addr;
}


Int32 Utils_memFree(Ptr addr, UInt32 size)
{
    SharedRegion_Entry srEntry;
    Int32 status;
    UInt32 poolSize;
    UInt8* poolStartAddr;

    if ( Utils_memSharedPoolIsActive() )
    {
        Utils_memSharedPoolGetDetails (&poolStartAddr, &poolSize);
        if (( addr < (poolStartAddr + poolSize) ) &&
            ( addr >= poolStartAddr) )
        {
            /* Buffer is allocated from shared pool*/
            /* Do not deallocate */
            status = FVID2_SOK;
            return (status);
        }

    }

    SharedRegion_getEntry(SYSTEM_IPC_SR2, &srEntry);

    if((UInt32)addr >= (UInt32)srEntry.base && (UInt32)addr < ((UInt32)srEntry.base + srEntry.len) )
    {
        /* if address falls in frame buffer  heap then free from that heap */
        #ifdef UTILS_MEM_DEBUG
        Vps_printf(" UTILS: MEM: FRAME FREE, addr = 0x%08x, size = %d bytes, heap = [SR2]\n", addr, size);
        #endif

        /* free previously allocated memory */
        Memory_free(gUtils_heapMemHandle[UTILS_MEM_VID_HEAP_SR2], addr, size);
    }
    else
    {

        SharedRegion_getEntry(SYSTEM_IPC_SR1, &srEntry);

        if((UInt32)addr >= (UInt32)srEntry.base && (UInt32)addr < ((UInt32)srEntry.base + srEntry.len) )
        {
            /* if address falls in bitstream buffer  heap then free from that heap */
            #ifdef UTILS_MEM_DEBUG
            Vps_printf(" UTILS: MEM: FRAME FREE, addr = 0x%08x, size = %d bytes, heap = [SR1]\n", addr, size);
            #endif

            /* free previously allocated memory */
            Memory_free(gUtils_heapMemHandle[UTILS_MEM_VID_HEAP_SR1], addr, size);

        }
        else
        {
            #ifdef UTILS_MEM_DEBUG
            Vps_printf(" UTILS: MEM: FRAME FREE, addr = 0x%08x, size = %d bytes, heap = [TILER BUF]\n", addr, size);
            #endif

            /* else address falls in tiler memory area */
            status = SystemTiler_freeRaw(addr,size);
            UTILS_assert(status == 0);
        }
    }

    return 0;
}

Int32 Utils_memClearOnAlloc(Bool enable)
{
    UInt32 i;

    for (i=0; i<UTILS_MEM_NUM_SHARED_REGION_HEAP; i++)
    {
        gUtils_memClearBuf[i] = enable;
    }
    return 0;
}

UInt32 Utils_memGetSystemHeapFreeSpace(void)
{
    Memory_Stats stats;
    extern const IHeap_Handle Memory_defaultHeapInstance;

    Memory_getStats(Memory_defaultHeapInstance, &stats);

    return ((UInt32) (stats.totalFreeSize));
}

UInt32 Utils_memGetSR0HeapFreeSpace(void)
{
    UInt32 size;
    Memory_Stats stats;

    Memory_getStats(SharedRegion_getHeap(SYSTEM_IPC_SR_NON_CACHED_DEFAULT), &stats);

    size = stats.totalFreeSize;
    return ((UInt32) (size));
}

UInt32 Utils_memGetSR1HeapFreeSpace(void)
{
    UInt32 size;
    Memory_Stats stats;

    Memory_getStats(gUtils_heapMemHandle[UTILS_MEM_VID_HEAP_SR1], &stats);

    size = stats.totalFreeSize;

    return ((UInt32) (size));
}

UInt32 Utils_memGetSR2HeapFreeSpace(void)
{
    Memory_Stats stats;

    Memory_getStats(gUtils_heapMemHandle[UTILS_MEM_VID_HEAP_SR2], &stats);

    return ((UInt32) (stats.totalFreeSize));
}

Int32 Utils_memBitBufAlloc(Bitstream_Buf * pBuf, UInt32 size, UInt16 numFrames)
{
    UInt32 bufId;
    Int32 status = 0;
    UInt8 *pBaseAddr;
    Error_Block ebObj;
    Error_Block *eb = &ebObj;

    Error_init(eb);

    /* init Bitstream_Buf to 0's */
    memset(pBuf, 0, sizeof(*pBuf));

    /* align size to minimum required frame buffer alignment */
    size = VpsUtils_align(size, IVACODEC_VDMA_BUFFER_ALIGNMENT);

    pBaseAddr = Utils_memAlloc(size * numFrames, IVACODEC_VDMA_BUFFER_ALIGNMENT, -1);

    if (!Error_check(eb)
        &&
        (pBaseAddr != NULL)
        &&
        gUtils_memClearBuf[UTILS_MEM_VID_HEAP_SR1])
    {
        memset(pBaseAddr, 0x80, (size * numFrames));
    }

    if (pBaseAddr == NULL)
    {
        status = -1;                                       /* Error in
                                                            * allocation,
                                                            * exit with error
                                                            */
    }

    if (!UTILS_ISERROR(status))
    {
        /* init memory pointer for 'numFrames' */
        for (bufId = 0; bufId < numFrames; bufId++)
        {

            /* copy channelNum to Bitstream_Buf from FVID2_Format */
            pBuf->channelNum = bufId;
            pBuf->addr = pBaseAddr;
            /* On the RTOS side, we assume Virtual addr == PhyAddr */
            pBuf->phyAddr = (UInt32)pBaseAddr;
            pBuf->bufSize = size;
            /* go to next frame */
            pBuf++;

            /* increment base address */
            pBaseAddr += size;
        }
    }

    UTILS_assert(status == 0);

    return status;
}

Int32 Utils_memBitBufFree(Bitstream_Buf * pBuf, UInt16 numFrames)
{
    UInt32 size;

    size = pBuf->bufSize;

    /* free the frame buffer memory */

    Utils_memFree(pBuf->addr, (size * numFrames));

    return 0;
}
