#include <mcfw/src_bios6/utils/utils_mem_shared_pool.h>

/* Current implementation supports only one shared pool.*/
SharedMemInfo gSharedPoolInfo;

/* Initialises the pool.*/
Int32 Utils_memSharedPoolInit(Ptr addr, Int32 size)
{
    Int32 status = FVID2_EFAIL;

    if (gSharedPoolInfo.initDone == FALSE)
    {
        gSharedPoolInfo.freeAddr     = gSharedPoolInfo.poolStartAddr = addr;
        gSharedPoolInfo.availableSize = gSharedPoolInfo.poolSize = size;

        gSharedPoolInfo.initDone      = TRUE;

        status = FVID2_SOK;
    }

    return(status);
}

/* Deinitialises the pool.*/
Int32 Utils_memSharedPoolDeinit()
{
    memset(&gSharedPoolInfo, 0, sizeof(gSharedPoolInfo));

    return(FVID2_SOK);
}

/*Allocates memory from the pool*/
Ptr Utils_memSharedAllocFromPool(Int32 size, UInt32 align)
{
    UInt8 *addr = NULL;

    if ((size > gSharedPoolInfo.availableSize) ||
        (gSharedPoolInfo.initDone == FALSE))
    {
        return (addr);
    }

    /*Align start address*/
    addr = (UInt8 *)VpsUtils_align((UInt32)gSharedPoolInfo.freeAddr, align);

    if ((addr - gSharedPoolInfo.freeAddr + size) >
        gSharedPoolInfo.availableSize )
    {
        return (NULL);
    }

    gSharedPoolInfo.availableSize -=  (size + (addr - gSharedPoolInfo.freeAddr));
    gSharedPoolInfo.freeAddr      = addr + size;

    return (addr);

}

UInt32 Utils_memSharedPoolGetFreeSpace()
{
    return (gSharedPoolInfo.availableSize);
}

UInt32 Utils_memSharedPoolIsActive()
{
    return (gSharedPoolInfo.initDone);
}


UInt32 Utils_memSharedPoolGetDetails(UInt8 **addr, UInt32 *size)
{
    Int32 status = FVID2_EFAIL;

    if (gSharedPoolInfo.initDone)
    {
        *addr = gSharedPoolInfo.poolStartAddr;
        *size = gSharedPoolInfo.poolSize;
        status = FVID2_SOK;
    }

    return (status);
}
