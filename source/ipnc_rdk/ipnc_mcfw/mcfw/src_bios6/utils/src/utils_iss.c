#include <mcfw/src_bios6/utils/utils_iss.h>

#define ISS_CLKCTRLREG        (0x55040084)
#define SIMCOP_ON             (0x1)

void Utils_setSimcopOn()
{
    *((volatile unsigned int*)ISS_CLKCTRLREG) |= SIMCOP_ON;
}

void Utils_setSimcopOff()
{
    *((volatile unsigned int*)ISS_CLKCTRLREG) &= ~(SIMCOP_ON);
}
