
#ifndef _UTILS_MEM_SHARED_POOL_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _UTILS_MEM_SHARED_POOL_H_

#include <xdc/std.h>
#include <mcfw/src_bios6/utils/utils_mem.h>

typedef struct SharedMemInfo {
  UInt8 *poolStartAddr;
  UInt32 poolSize;
  UInt8 *freeAddr;
  Int32 availableSize;
  UInt32 initDone;
} SharedMemInfo ;

/*There are two shared region heaps. */
#define UTILS_MEM_VID_SHARED_POOL_ID       (UTILS_MEM_NUM_SHARED_REGION_HEAP)

Int32 Utils_memSharedPoolInit(Ptr addr, Int32 size);
Int32 Utils_memSharedPoolDeinit();
Ptr Utils_memSharedAllocFromPool(Int32 size, UInt32 align);
UInt32 Utils_memSharedPoolGetFreeSpace();
UInt32 Utils_memSharedPoolIsActive();
UInt32 Utils_memSharedPoolGetDetails(UInt8 **addr, UInt32 *size);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif
