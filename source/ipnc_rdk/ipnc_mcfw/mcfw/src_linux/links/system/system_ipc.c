/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/



#include "system_priv_ipc.h"
#include <mcfw/interfaces/link_api/ipcLink.h>
#include <ti/syslink/SysLink.h>
#include <ti/syslink/ProcMgr.h>

/* TT */
#include <ti/syslink/inc/IpcDrvDefs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

System_IpcObj gSystem_ipcObj;


Int32 System_printProcId(UInt32 procId)
{
    char *procName;
    UInt32 syslinkProcId;

    procName = System_getProcName(procId);
    syslinkProcId = System_getSyslinkProcId(procId);

    if(syslinkProcId == MultiProc_INVALIDID)
    {
        OSA_printf(" %u: SYSTEM: CPU [%s] is NOT available on this platform !!!\n",
            OSA_getCurTimeInMsec(),
            procName
            );
    }
    else
    {
        OSA_printf(" %u: SYSTEM: CPU [%s] syslink proc ID is [%d] !!!\n",
            OSA_getCurTimeInMsec(),
            procName,
            syslinkProcId
            );
    }

    return 0;
}

Int32 System_ipcInit()
{
    SysLink_setup ();

#ifdef SYSTEM_DEBUG
    {
        UInt32 procId;

        for(procId=0; procId<SYSTEM_PROC_MAX; procId++)
            System_printProcId(procId);
    }
#endif

    /* TT: Open Ipc Handle*/
    gSystem_ipcObj.ipcHandle =  open("/dev/syslinkipc_Ipc",O_SYNC | O_RDWR);
    if(gSystem_ipcObj.ipcHandle < 0)
    {
        OSA_printf(" %u: SYSTEM: Ipc handle open failed\n",OSA_getCurTimeInMsec());
    }

    System_ipcMsgQInit();

    System_ipcNotifyInit();


#ifdef SYSTEM_DEBUG
    OSA_printf(" %u: SYSTEM: IPC init DONE !!!\n", OSA_getCurTimeInMsec());
#endif

    return OSA_SOK;
}

Int32 System_ipcDeInit()
{
    System_ipcNotifyDeInit();

    System_ipcMsgQDeInit();

    SysLink_destroy ();

    close(gSystem_ipcObj.ipcHandle);

#ifdef SYSTEM_DEBUG
    OSA_printf(" %u: SYSTEM: IPC de-init DONE !!!\n", OSA_getCurTimeInMsec());
#endif

    return OSA_SOK;
}

Int32 System_ipcGetSlaveCoreSymbolAddress(char *symbolName,UInt16 procId,
                                    UInt32 *symAddressPtr)
{
    Int32 status;
    ProcMgr_Handle procMgrHandle = NULL;
    UInt32 symbolAddress = 0;
    UInt32 fileId;
    UInt32 syslinkProcId;

    syslinkProcId = System_getSyslinkProcId(procId);

    *symAddressPtr = 0;
    status = ProcMgr_open (&procMgrHandle, syslinkProcId);
    OSA_assert(status >= 0);

    OSA_assert(procMgrHandle != NULL);

    fileId = ProcMgr_getLoadedFileId (procMgrHandle);

    status = ProcMgr_getSymbolAddress(procMgrHandle,
                                      fileId,
                                      symbolName,
                                      &symbolAddress);

    OSA_assert(status >= 0);
    OSA_assert(symbolAddress != 0);
    *symAddressPtr = symbolAddress;
    status = ProcMgr_close(&procMgrHandle);
    OSA_assert(status > 0);
    return OSA_SOK;
}

Int32 System_ipcCopySlaveCoreSymbolContents(char *symbolName,
                                      UInt16 procId,
                                      Ptr  dstPtr,
                                      UInt32 copySize)
{
    UInt32 symbolAddress;
    Int32 status;
    Ptr   symbolAddressMapped;

    status =
    System_ipcGetSlaveCoreSymbolAddress(symbolName,procId,&symbolAddress);
    OSA_assert(OSA_SOK  == status);

    status = OSA_mapMem(symbolAddress,copySize,&symbolAddressMapped);
    OSA_assert((status == 0) && (symbolAddressMapped != NULL));

    memcpy(dstPtr,symbolAddressMapped,copySize);

    status = OSA_unmapMem(symbolAddressMapped,copySize);

    OSA_assert(status == 0);

    return status;
}

Int32 System_ipcRegisterTerminateHandler()
{
    Int32 status;
    IpcDrv_CmdArgs cmdArgs;
    UInt32 syslinkProcId;

    cmdArgs.args.addTermEvent.pid = getpid();
    cmdArgs.args.addTermEvent.payload = 0xff;
    cmdArgs.args.addTermEvent.policy = Ipc_TERMINATEPOLICY_NOTIFY;

    syslinkProcId = System_getSyslinkProcId(SYSTEM_PROC_M3VPSS);
    cmdArgs.args.addTermEvent.procId = syslinkProcId;
    status =  ioctl(gSystem_ipcObj.ipcHandle,CMD_IPC_ADDTERMINATEEVENT, &cmdArgs);
    if(status < 0)
    {
        OSA_printf(" %u: SYSTEM: Add termination event failed on %s !!!\n",
                            OSA_getCurTimeInMsec(),SYSTEM_IPC_PROC_NAME_M3VPSS);
    }
    return status;
}
