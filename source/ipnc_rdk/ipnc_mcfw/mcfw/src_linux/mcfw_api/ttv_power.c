// TomTom Video platform API
// Power/sleep functionality

#ifdef PLATFORM_LIB

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <ttsystem.h>

#include "tt_video_priv.h"

/***************************************************************************************
 * ttv_sleep:
 *	Software or hardware for sleep 'timeout' milliseconds.
 *
 *	timeout = -1: power off
 *	timeout =  0: sleep indefinitely or until button or bluetooth wakeup
 *	timeout >  0: sleep for timeout seconds or until button or bluetooth wakeup
 *	timeout <  0 but not -1: return
 *
 *	Returns source of wakeup
 *
 * Dependencies:
 * - LBP-784: Wakeup timer on MCU should have required granularity
 * - LBP-699: System time should be set correctly after wakeup
 ***************************************************************************************/
ttv_wakeup_t ttv_sleep(int timeout)
{
	return ttsystem_sleep(timeout);
}

#ifdef LONGBEACH_POWERMODE_OBSOLETE
#ifdef LONGBEACH_FREQUENCY_DEBUG

static const char *powermode_name[] = {"lb_low", "lb_high1", "lb_high2",
	"lb_high3", "lb_optimal"};
static const char *peripheral_name[] = {"cpu_freq", "l3_freq", "iva_freq", "iss_freq"};

/***************************************************************************************
 * ttv_get_frequency:
 *	Get peripheral frequency for a given powermode
 *	Inputs:
 *		powermode - The powermode for which frequency is to be retrieved
 *		peripheral - The peripheral for which frequency is to be retrieved
 *
 *	Supported peripherals: TTV_PERIPHERAL_ARM, TTV_PERIPHERAL_IVA,
 *			TTV_PERIPHERAL_ISS, TTV_PERIPHERAL_L3
 *
 *	Returns frequency in MHz on success, -errno on failure
 ***************************************************************************************/
int ttv_get_frequency(ttv_powermode_t powermode, ttv_peripheral_t peripheral)
{
	int fd_governor_debug;
	char buf[12];
	char governor_debug_file_path[100];
	int size;

	if(powermode > TTV_POWERMODE_CURRENT) {
		debuglog(1, "ttv_get_frequency: invalid power mode", 0);
		return -EINVAL;
	}

	if(peripheral > TTV_PERIPHERAL_ISS) {
		debuglog(1, "ttv_get_frequency: invalid peripheral", 0);
		return -EINVAL;
	}

	if(powermode == TTV_POWERMODE_CURRENT) {
		int fd_governor = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", O_RDWR | O_CLOEXEC);
		if(fd_governor < 0) {
			debuglog(1, "ttv_get_frequency: cannot open frequency governor interface", 0);
			return -errno;
		}

		if((size = read(fd_governor, buf, 12)) < 0) {
			debuglog(1, "ttv_get_frequency: cannot read current power mode", 0);
			close(fd_governor);
			return -errno;
		}

		buf[size - 1] = '\0';
		close(fd_governor);
	}

	size = snprintf(governor_debug_file_path, 100, "/sys/devices/system/lbgov_debug/lbgov_debug0/%s/%s",
			powermode == TTV_POWERMODE_CURRENT ? buf : powermode_name[powermode],
			peripheral_name[peripheral]);
	governor_debug_file_path[size - 1] = '\0';

	fd_governor_debug = open(governor_debug_file_path, O_RDWR | O_CLOEXEC);
	if (fd_governor_debug < 0) {
		debuglog(1, "ttv_get_frequency: cannot open frequency governor debug interface", 0);
		debuglog(1, "ttv_get_frequency: please configure kernel with CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG", 0);
		return -errno;
	}

	if(read(fd_governor_debug, buf, 12) < 0) {
		debuglog(1, "ttv_get_frequency: could not read frequency", 0);
		close(fd_governor_debug);
		return -errno;
	}

	close(fd_governor_debug);
	return strtoul(buf, NULL, 10);
}

/***************************************************************************************
 * ttv_set_frequency:
 *	Setup peripheral frequency for a given powermode
 *	Inputs:
 *		powermode - The powermode for which frequency is to be set
 *
 *		peripheral - The peripheral for which frequency is to be set
 *		frequency - Frequency in MHz
 *		now - If true, powermode is changed to new powermode. The frequency
 *			changes take effect immediately
 *		      If false, current powermode is not changed. Frequency changes take
 *			effect the next time powermode is changed
 *
 *	Supported peripherals: TTV_PERIPHERAL_ARM, TTV_PERIPHERAL_IVA,
 *			TTV_PERIPHERAL_ISS, TTV_PERIPHERAL_L3
 *
 *	Returns true on success, false on failure
 ***************************************************************************************/
bool ttv_set_frequency(ttv_powermode_t powermode, ttv_peripheral_t peripheral,
		unsigned int freq_in_MHz, bool now)
{
	int size;
	int fd_governor, fd_governor_debug;
	char frequency[12];
	char governor[12];
	char governor_debug_file_path[100];

	if (powermode > TTV_POWERMODE_CURRENT) {
		debuglog(1, "ttv_set_frequency: invalid power mode", 0);
		return false;
	}

	if (peripheral > TTV_PERIPHERAL_ISS) {
		debuglog(1, "ttv_set_frequency: invalid peripheral", 0);
		return false;
	}

	fd_governor = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", O_RDWR | O_CLOEXEC);
	if (fd_governor < 0) {
		debuglog(1, "ttv_set_frequency: cannot open frequency governor interface", 0);
		return false;
	}

	if (powermode == TTV_POWERMODE_CURRENT) {
		if ((size = read(fd_governor, governor, 12)) < 0) {
			debuglog(1, "ttv_set_frequency: cannot read current power mode", 0);
			close(fd_governor);
			return false;
		}
	}
	else
		size = snprintf(governor, 12, powermode_name[powermode]);

	governor[size - 1] = '\0';
	size = snprintf(governor_debug_file_path, 100, "/sys/devices/system/lbgov_debug/lbgov_debug0/%s/%s",
			governor, peripheral_name[peripheral]);
	governor_debug_file_path[size - 1] = '\0';

	fd_governor_debug = open(governor_debug_file_path, O_RDWR | O_CLOEXEC);
	if (fd_governor_debug < 0) {
		debuglog(1, "ttv_set_frequency: cannot open frequency governor debug interface", 0);
		debuglog(1, "ttv_set_frequency: please configure kernel with CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG", 0);
		close(fd_governor);
		return false;
	}

	size = snprintf(frequency, 12, "%u", freq_in_MHz);
	if (write(fd_governor_debug, frequency, size) < 0) {
		debuglog(1, "ttv_set_frequency: could not set frequency", 0);
		close(fd_governor);
		close(fd_governor_debug);
		return false;
	}

	if (now) {
		if (write(fd_governor, governor, 12) < 0) {
			debuglog(1, "ttv_set_frequency: setting power mode failed", 0);
			debuglog(1, "ttv_set_frequency: new frequency will take effect when power mode is set", 0);
			close(fd_governor_debug);
			close(fd_governor);
			return false;
		}
	}

	close(fd_governor_debug);
	close(fd_governor);
	return true;
}
#endif
#endif

#endif
