/** ==================================================================
 *  @file   multich_tomtom.c
 *
 *  @path    ipnc_mcfw/mcfw/src_linux/mcfw_api/usecases/
 *
 *  @desc   This  File contains.
 * ===================================================================
 *  Copyright (c) Texas Instruments Inc 2013, 2014
 *
 *  Use of this software is controlled by the terms and conditions found
 *  in the license agreement under which this software has been supplied
 * ===================================================================*/

/*------------------------------ TRIPLE OUT <1080p + D1 + 1080p> ---------------------------

                                      SENSOR
                                ________||_______
                                |                |
                                |   CAMERA LINK  |
                                |____(VPSSM3)____|
                                 RSZA        RSZB
                             (1920x1080)   (720x480)
                               (420SP)      (420SP)
                                  |            |
                                  |            |
                                  |-----|------|
                                        |
                                    MUX(VPSSM3)
                                        |
                                        |
                                   SWOSD(VPSSM3)
                                        |
                                     (1)|
                          |------- DUP(VPSSM3)
                          |                |(0)
                   DIS_HDMI(VPSSM3)        |
                                    ENC(VPSSM3)
                                        |
                                        |
                                IPC BITS OUT(VPSSM3)
                                        |
                                        |
                                  IPC BITS IN(A8)
                                        |
                                        |
                                  *************
                                  * STREAMING *
                                  * FILE SAVE *
                                  *************
------------------------------------------------------------------------------------------*/

#ifndef PLATFORM_LIB

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "demos/mcfw_api_demos/multich_usecase/ti_mcfw_ipnc_main.h"
#include "demos/mcfw_api_demos/multich_usecase/ti_mcfw_ipnc_api.h"
#include "demos/mcfw_api_demos/multich_usecase/tt_mcfw_transcode.h"

#ifdef BUTTON_MENU
#define UART_MENU
#endif

#ifdef FACTORY_TEST
#define UART_MENU
#endif

/*
 *    If STREAMING_ON is defined then bitstream is written to streaming circular
 *  buffer else if not defined then 'MultiCh_ipcBitsInHostTomTomCb()' callback
 *  is invoked where in user should consume the bitstream.
 */
#define STREAMING_ON

/*
*          For UART menu streaming is disabled.
*/
#ifdef UART_MENU
  #undef STREAMING_ON
#endif

/* Display Enable/Disable Switch */
#define DISABLE_DISPLAY

#ifdef FACTORY_TEST
#undef DISABLE_DISPLAY
#endif

/* VNF Enable/Disable Switch */
#define DISABLE_VNF

/* Single/Triple Stream Switch*/
#define SINGLE_STREAM

#define NUM_ENCODE_BUFFERS      (4)

static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl = {
    .isPopulated = 1,
    .ivaMap[0] = {
                  .EncNumCh = 2,
                  .EncChList = {0, 1},
                  .DecNumCh = 2,
                  .DecChList = {0, 1},
                  },

};

Int8 gCodecName[3][12] = {"h264","h264","h264"};
UInt32 gCodecType[3] = {IVIDEO_H264HP,IVIDEO_H264HP,IVIDEO_H264HP};

/* decode usecase parameter ;
 * contains the file pointer to read from file, thread handle and everything
 */
transcodeUseCase_IpcBitsCtrl gDecodeUseCase_obj;
transcodeUseCase_StreamConfig transcodeStrCfg;

#ifdef FACTORY_TEST
UInt8 gDisplay = TRUE;
#endif

/* ===================================================================
 *  @func     MultiCh_ipcBitsInHostTomTomCb
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int8* MultiCh_getCodecName(UInt32 chId)
{
    return (gCodecName[chId]);
}

UInt32 MultiCh_getCodecType(UInt32 chId)
{
    return (gCodecType[chId]);
}

#ifndef STREAMING_ON
Void MultiCh_ipcBitsInHostTomTomCb(Ptr cbCtx)
{
    UInt32 i,chId;
    Bitstream_BufList fullBitsBufList;

    /* Get the Bitstream buf */
    IpcBitsInLink_getFullVideoBitStreamBufs(gVencModuleContext.ipcBitsInHLOSId,&fullBitsBufList);

    if(fullBitsBufList.numBufs > 0)
    {
        for(i = 0;i < fullBitsBufList.numBufs;i++)
        {
            /*
             *    Consume the encoded bitstream here:
             *  Addr        :fullBitsBufList.bufs[i]->addr + fullBitsBufList.bufs[i]->startOffset
             *  Size        :fullBitsBufList.bufs[i]->fillLength
             *  Channel No    :fullBitsBufList.bufs[i]->channelNum
             *  Codec Type    :fullBitsBufList.bufs[i]->codingType
             *  Timestamp    :fullBitsBufList.bufs[i]->timeStamp
             *  Frame Type    :fullBitsBufList.bufs[i]->isKeyFrame    :0 -> P frame 1 -> I frame
             */
            if(fullBitsBufList.bufs[i]->channelNum == 0)
            {
                App_ipncApi_dumpBitstreamDump((Int8*)(fullBitsBufList.bufs[i]->addr + fullBitsBufList.bufs[i]->startOffset),
                                              fullBitsBufList.bufs[i]->fillLength,
                                              fullBitsBufList.bufs[i]->isKeyFrame,
                                              fullBitsBufList.bufs[i]->codingType,
                                              fullBitsBufList.bufs[i]->timeStamp);

                /* Still capture dump */
                if(fullBitsBufList.bufs[i]->codingType == IVIDEO_MJPEG)
                {
                    App_ipncApi_dumpStillCapture((Int8*)(fullBitsBufList.bufs[i]->addr + fullBitsBufList.bufs[i]->startOffset),
                                                 fullBitsBufList.bufs[i]->fillLength);
                }
            }

            /*
             *    Save the codec type
             */
            chId = fullBitsBufList.bufs[i]->channelNum;
            if(gCodecType[chId] != fullBitsBufList.bufs[i]->codingType)
            {
                gCodecType[chId] = fullBitsBufList.bufs[i]->codingType;
                if((gCodecType[chId] == IVIDEO_H264BP) ||
                   (gCodecType[chId] == IVIDEO_H264MP) ||
                   (gCodecType[chId] == IVIDEO_H264HP))
                {
                    strcpy(gCodecName[chId],"h264");
                }
                     else
                     {
                        strcpy(gCodecName[chId],"mjpeg");
                     }
            }
            if(fullBitsBufList.bufs[i]->channelNum == 1) //->codingType == IVIDEO_MJPEG)
                App_ipncApi_dumpJpg((Int8*)(fullBitsBufList.bufs[i]->addr + fullBitsBufList.bufs[i]->startOffset),fullBitsBufList.bufs[i]->fillLength);

            /*
             *    Real time Encoder FPS calculation using frame time stamps
             */
            App_ipncApi_setFrameTimeStamp(fullBitsBufList.bufs[i]->channelNum,
                                          fullBitsBufList.bufs[i]->timeStamp);
        }
    }

    /* Put back the Bitstream buf */
    IpcBitsInLink_putEmptyVideoBitStreamBufs(gVencModuleContext.ipcBitsInHLOSId,&fullBitsBufList);
}
#endif

/* ===================================================================
 *  @func     MultiCh_createTomTomUseCase
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Void MultiCh_createTomTomUseCase(SYSTEM_Standard standard,UInt32 systemUseTiler)
{
    UInt32 i;
    UInt32 dupId;
    CameraLink_CreateParams cameraPrm;
    CameraLink_VipInstParams *pCameraInstPrm;
    CameraLink_OutParams *pCameraOutPrm;
    VnfLink_CreateParams vnfPrm;
    VnfLink_ChCreateParams *vnfParams;
    MuxLink_CreateParams muxPrm;
    DupLink_CreateParams dupPrm;
    DisplayLink_CreateParams displayPrm;
    EncLink_CreateParams encPrm;
    IpcBitsOutLinkRTOS_CreateParams ipcBitsOutVpssPrm;
    IpcBitsInLinkHLOS_CreateParams ipcBitsInHostPrm;
    UInt32 frameRate;
    UInt32 numEncBuf;
#ifdef CONFIG_LB_OSD_ENABLE
    SwosdLink_CreateParams swosdPrm;
    Vsys_swOsdPrm swosdGuiPrm;
#endif

    OSA_printf("\n********************* Entered TOM TOM USECASE ********************\n\n");

    MultiCh_detectBoard();

    System_linkControl(SYSTEM_LINK_ID_M3VPSS,
                       SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES, NULL, 0, TRUE);

    System_linkControl(SYSTEM_LINK_ID_M3VPSS,
                       SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
                       &systemVid_encDecIvaChMapTbl,
                       sizeof(SystemVideo_Ivahd2ChMap_Tbl),TRUE);

    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams,ipcBitsOutVpssPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,ipcBitsInHostPrm);

    /*
     *    Link parameter population
     */

    /* Link IDs */
    gVcamModuleContext.cameraId = SYSTEM_LINK_ID_CAMERA;
    gVcamModuleContext.vnfId = SYSTEM_LINK_ID_VNF;
    gVsysModuleContext.muxId = SYSTEM_VPSS_LINK_ID_MUX_0;
#ifdef CONFIG_LB_OSD_ENABLE
    gVsysModuleContext.swOsdId = SYSTEM_LINK_ID_SWOSD_0;
#endif
    dupId = SYSTEM_VPSS_LINK_ID_DUP_0;
    gVdisModuleContext.displayId[VDIS_DEV_HDMI] = SYSTEM_LINK_ID_DISPLAY_0;
    gVencModuleContext.encId = SYSTEM_LINK_ID_VENC_0;
    gVencModuleContext.ipcBitsOutRTOSId = SYSTEM_VPSS_LINK_ID_IPC_BITS_OUT_0;
    gVencModuleContext.ipcBitsInHLOSId = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;

    /* CAMERA */
    CameraLink_CreateParams_Init(&cameraPrm);

    cameraPrm.captureMode              = CAMERA_LINK_CAPMODE_ISIF;
    cameraPrm.numAudioChannels         = 1;
    cameraPrm.numVipInst               = 1;
    if(systemUseTiler == 1)
    {
      cameraPrm.tilerEnable          = TRUE;
    }
    else
    {
      cameraPrm.tilerEnable          = FALSE;
    }
    cameraPrm.vsEnable                 = FALSE;

  if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
  {
    cameraPrm.outQueParams[0].nextLink = gVsysModuleContext.muxId;
  }
  else
  {
#ifdef DISABLE_VNF
    cameraPrm.outQueParams[0].nextLink = gVsysModuleContext.muxId;
#else
    cameraPrm.outQueParams[0].nextLink = gVcamModuleContext.vnfId;
#endif
  }

    cameraPrm.outQueParams[1].nextLink = gVsysModuleContext.muxId;
    cameraPrm.t2aConfig.n2A_vendor     = gUI_mcfw_config.n2A_vendor;
    cameraPrm.t2aConfig.n2A_mode       = gUI_mcfw_config.n2A_mode;

    pCameraInstPrm = &cameraPrm.vipInst[0];
    pCameraInstPrm->vipInstId       = SYSTEM_CAMERA_INST_VP_CSI2;
    pCameraInstPrm->videoDecoderId  = MultiCh_getSensorId(gUI_mcfw_config.sensorId);
    pCameraInstPrm->inDataFormat    = SYSTEM_DF_YUV420SP_UV;
    pCameraInstPrm->standard        = standard;

    if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
    {
      pCameraInstPrm->numOutput       = 1;
      }
    else
    {
#ifdef SINGLE_STREAM
    pCameraInstPrm->numOutput       = 1;
#else
    pCameraInstPrm->numOutput       = 2;
#endif
    }

    pCameraOutPrm = &pCameraInstPrm->outParams[0];
    pCameraOutPrm->dataFormat  = SYSTEM_DF_YUV420SP_UV;
    pCameraOutPrm->scEnable    = FALSE;

    switch(pCameraInstPrm->standard)
    {
        default:
        case SYSTEM_STD_1080P_60:
            pCameraInstPrm->sensorOutWidth  = 1920;
            pCameraInstPrm->sensorOutHeight = 1080;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 60;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_4K_4608_2592_15:
            pCameraInstPrm->sensorOutWidth  = 4336;
            pCameraInstPrm->sensorOutHeight = 2432;
            pCameraOutPrm->scOutWidth       = 3840;
            pCameraOutPrm->scOutHeight      = 2160;
            frameRate                       = 15;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = FALSE;
#endif
            break;

        case SYSTEM_STD_4K_4608_2592_12_5:
            pCameraInstPrm->sensorOutWidth  = 4336;
            pCameraInstPrm->sensorOutHeight = 2432;
            pCameraOutPrm->scOutWidth       = 3840;
            pCameraOutPrm->scOutHeight      = 2160;
            frameRate                       = 12.5;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_2_7K_4608_2592_30:
            pCameraInstPrm->sensorOutWidth  = 3072;
            pCameraInstPrm->sensorOutHeight = 1728;
            pCameraOutPrm->scOutWidth       = 2704;
            pCameraOutPrm->scOutHeight      = 1524;
            frameRate                       = 30;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = FALSE;
#endif
            break;

        case SYSTEM_STD_2_7K_4608_2592_25:
            pCameraInstPrm->sensorOutWidth  = 3072;
            pCameraInstPrm->sensorOutHeight = 1728;
            pCameraOutPrm->scOutWidth       = 2704;
            pCameraOutPrm->scOutHeight      = 1524;
            frameRate                       = 25;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_2_7K_3840_2160_25:
            pCameraInstPrm->sensorOutWidth  = 3072;
            pCameraInstPrm->sensorOutHeight = 1728;
            pCameraOutPrm->scOutWidth       = 2704;
            pCameraOutPrm->scOutHeight      = 1524;
            frameRate                       = 25;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_1080P_4608_2592_60:
            pCameraInstPrm->sensorOutWidth  = 2048;
            pCameraInstPrm->sensorOutHeight = 1152;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 60;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = TRUE;
#endif
            break;

        case SYSTEM_STD_1080P_3840_2160_60:
            pCameraInstPrm->sensorOutWidth  = 1920;
            pCameraInstPrm->sensorOutHeight = 1080;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 60;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = TRUE;
#endif
            break;

        case SYSTEM_STD_1080P_4608_2592_50:
            pCameraInstPrm->sensorOutWidth  = 2304;
            pCameraInstPrm->sensorOutHeight = 1296;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 50;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_1080P_3840_2160_50:
            pCameraInstPrm->sensorOutWidth  = 1920;
            pCameraInstPrm->sensorOutHeight = 1080;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 50;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_1080P_4608_2592_30:
            pCameraInstPrm->sensorOutWidth  = 2304;
            pCameraInstPrm->sensorOutHeight = 1296;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 30;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_1080P_3840_2160_30:
            pCameraInstPrm->sensorOutWidth  = 1920;
            pCameraInstPrm->sensorOutHeight = 1080;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 30;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = TRUE;
#endif
            break;

        case SYSTEM_STD_1080P_4608_2592_25:
            pCameraInstPrm->sensorOutWidth  = 2304;
            pCameraInstPrm->sensorOutHeight = 1296;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 25;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_1080P_3840_2160_25:
            pCameraInstPrm->sensorOutWidth  = 1920;
            pCameraInstPrm->sensorOutHeight = 1080;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 25;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_720P_4608_2592_120:
            pCameraInstPrm->sensorOutWidth  = 1536;
            pCameraInstPrm->sensorOutHeight = 864;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 120;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = TRUE;
#endif
            break;

        case SYSTEM_STD_720P_3840_2160_120:
            pCameraInstPrm->sensorOutWidth  = 1280;
            pCameraInstPrm->sensorOutHeight = 720;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 120;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_720P_4608_2592_100:
            pCameraInstPrm->sensorOutWidth  = 1536;
            pCameraInstPrm->sensorOutHeight = 864;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 100;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_720P_3840_2160_100:
            pCameraInstPrm->sensorOutWidth  = 1280;
            pCameraInstPrm->sensorOutHeight = 720;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 100;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_720P_4608_2592_60:
            pCameraInstPrm->sensorOutWidth  = 1536;
            pCameraInstPrm->sensorOutHeight = 864;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 60;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_720P_3840_2160_60:
            pCameraInstPrm->sensorOutWidth  = 1280;
            pCameraInstPrm->sensorOutHeight = 720;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 60;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_720P_4608_2592_50:
            pCameraInstPrm->sensorOutWidth  = 1536;
            pCameraInstPrm->sensorOutHeight = 864;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 50;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_720P_3840_2160_50:
            pCameraInstPrm->sensorOutWidth  = 1280;
            pCameraInstPrm->sensorOutHeight = 720;
            pCameraOutPrm->scOutWidth       = 1280;
            pCameraOutPrm->scOutHeight      = 720;
            frameRate                       = 50;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
            break;

        case SYSTEM_STD_WVGA_4608_2592_180:
            pCameraInstPrm->sensorOutWidth  = 1024;
            pCameraInstPrm->sensorOutHeight = 576;
            pCameraOutPrm->scOutWidth       = 848;
            pCameraOutPrm->scOutHeight      = 480;
            frameRate                       = 180;
            numEncBuf                       = NUM_ENCODE_BUFFERS + 2;
#ifdef FACTORY_TEST
            gDisplay                        = TRUE;
#endif
            break;

        case SYSTEM_STD_WVGA_4608_2592_150:
            pCameraInstPrm->sensorOutWidth  = 1024;
            pCameraInstPrm->sensorOutHeight = 576;
            pCameraOutPrm->scOutWidth       = 848;
            pCameraOutPrm->scOutHeight      = 480;
            frameRate                       = 150;
            numEncBuf                       = NUM_ENCODE_BUFFERS + 2;
            break;

        case SYSTEM_STD_4656_3492_10:
            pCameraInstPrm->sensorOutWidth  = 4656;
            pCameraInstPrm->sensorOutHeight = 3488;
            pCameraOutPrm->scOutWidth       = 4656;
            pCameraOutPrm->scOutHeight      = 3488;
            cameraPrm.numBufPerCh           = 3;
            frameRate                       = 10;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = FALSE;
#endif
            break;

        case SYSTEM_STD_4664_3496_10:
            pCameraInstPrm->sensorOutWidth  = 4664;
            pCameraInstPrm->sensorOutHeight = 3496;
            pCameraOutPrm->scOutWidth       = 4656;
            pCameraOutPrm->scOutHeight      = 3496;
            cameraPrm.numBufPerCh           = 3;
            frameRate                       = 10;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = FALSE;
#endif
            break;

        case SYSTEM_STD_1080P_1920_1080_C_30:
        case SYSTEM_STD_1080P_1920_1080_LT_30:
        case SYSTEM_STD_1080P_1920_1080_RT_30:
        case SYSTEM_STD_1080P_1920_1080_LB_30:
        case SYSTEM_STD_1080P_1920_1080_RB_30:
            pCameraInstPrm->sensorOutWidth  = 1920;
            pCameraInstPrm->sensorOutHeight = 1080;
            pCameraOutPrm->scOutWidth       = 1920;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 30;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = TRUE;
#endif
            break;

        case SYSTEM_STD_1440_1080_2332_1748_30:
            pCameraInstPrm->sensorOutWidth  = 2332;
            pCameraInstPrm->sensorOutHeight = 1748;
            pCameraOutPrm->scOutWidth       = 1440;
            pCameraOutPrm->scOutHeight      = 1080;
            frameRate                       = 30;
            numEncBuf                       = NUM_ENCODE_BUFFERS;
#ifdef FACTORY_TEST
            gDisplay                        = TRUE;
#endif
            break;
    }

    if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
    {
        pCameraInstPrm->sensorOutWidth  = 4664;
        pCameraInstPrm->sensorOutHeight = 3496;
        pCameraOutPrm->scOutWidth       = 4656;
        pCameraOutPrm->scOutHeight      = 3496;
        cameraPrm.numBufPerCh           = 3;
        frameRate                       = 10;
    }

    pCameraOutPrm->outQueId    = 0;

    pCameraOutPrm = &pCameraInstPrm->outParams[1];
    pCameraOutPrm->dataFormat  = SYSTEM_DF_YUV420SP_UV;
    pCameraOutPrm->scEnable    = FALSE;
    pCameraOutPrm->scOutWidth  = 720;
    pCameraOutPrm->scOutHeight = 480;
    pCameraOutPrm->outQueId    = 1;

    /* VNF */
    VnfLink_CreateParams_Init(&vnfPrm);

    vnfPrm.inQueParams.prevLinkId    = gVcamModuleContext.cameraId;
    vnfPrm.inQueParams.prevLinkQueId = 0;
    vnfPrm.outQueParams.nextLink     = gVsysModuleContext.muxId;

    vnfPrm.chCreateParams[0].bSnfEnable = 1;
    vnfPrm.chCreateParams[0].bTnfEnable = 0;
    vnfPrm.chCreateParams[0].strength   = NF_STRENGTH_AUTO;

    vnfParams = &vnfPrm.chCreateParams[0];
    vnfParams->sParams.eOutputFormat = SYSTEM_DF_YUV420SP_UV;
    vnfParams->sParams.eOperateMode  = VNF_LINK_NSF2;
    vnfParams->sParams.eNsfSet       = VNF_LINK_NSF_LUMA_CHROMA;

    vnfParams->dParams.ldcParams.eInterpolationLuma = VNF_LINK_YINTERPOLATION_BILINEAR;
    vnfParams->dParams.ldcParams.unPixelPad         = 4;

    vnfParams->dParams.nsfParams.bLumaEn         = TRUE;
    vnfParams->dParams.nsfParams.bChromaEn       = TRUE;
    vnfParams->dParams.nsfParams.eSmoothVal      = VNF_LINK_SMOOTH_2;
    vnfParams->dParams.nsfParams.bSmoothLumaEn   = TRUE;
    vnfParams->dParams.nsfParams.bSmoothChromaEn = FALSE;

    vnfParams->dParams.tnfParams.useDefault = TRUE;

    vnfParams->dParams.nsfFilter.eFilterParam      = VNF_LINK_PARAM_DEFAULT;
    vnfParams->dParams.nsfEdge.eEdgeParam          = VNF_LINK_PARAM_DISABLE;
    vnfParams->dParams.nsfShading.eShdParam        = VNF_LINK_PARAM_DISABLE;
    vnfParams->dParams.nsfDesaturation.eDesatParam = VNF_LINK_PARAM_DISABLE;

    if(gUI_mcfw_config.demoCfg.resolution_combo != RES_16MP_D1_STREAM)
    {
    #ifndef DISABLE_VNF
        if(vnfParams->sParams.eOperateMode == VNF_LINK_NSF2)
        {
            cameraPrm.vnfFullResolution = TRUE;
        }
    #endif
    }

    /* MUX */
    muxPrm.numInQue = 2;

    if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
    {
      muxPrm.inQueParams[0].prevLinkId    = gVcamModuleContext.cameraId;
    }
    else
    {
    #ifdef DISABLE_VNF
        muxPrm.inQueParams[0].prevLinkId    = gVcamModuleContext.cameraId;
    #else
        muxPrm.inQueParams[0].prevLinkId    = gVcamModuleContext.vnfId;
    #endif
    }

    muxPrm.inQueParams[0].prevLinkQueId = 0;
    muxPrm.inQueParams[1].prevLinkId    = gVcamModuleContext.cameraId;
    muxPrm.inQueParams[1].prevLinkQueId = 1;

#ifdef CONFIG_LB_OSD_ENABLE
    muxPrm.outQueParams.nextLink = gVsysModuleContext.swOsdId;
#else
    muxPrm.outQueParams.nextLink = dupId;
#endif

    if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
    {
        muxPrm.muxNumOutChan       = 1;
    }
    else
    {
#ifdef SINGLE_STREAM
        muxPrm.muxNumOutChan       = 1;
#else
        muxPrm.muxNumOutChan       = 3;
#endif
    }

    muxPrm.outChMap[0].inQueId = 0;
    muxPrm.outChMap[0].inChNum = 0;
    muxPrm.outChMap[1].inQueId = 1;
    muxPrm.outChMap[1].inChNum = 0;
    muxPrm.outChMap[2].inQueId = 0;
    muxPrm.outChMap[2].inChNum = 0;

#ifdef CONFIG_LB_OSD_ENABLE
    /* SWOSD */
    swosdPrm.inQueParams.prevLinkId    = gVsysModuleContext.muxId;
    swosdPrm.inQueParams.prevLinkQueId = 0;
    swosdPrm.outQueParams.nextLink     = dupId;
#endif

    /* DUP */
#ifdef CONFIG_LB_OSD_ENABLE
    dupPrm.inQueParams.prevLinkId    = gVsysModuleContext.swOsdId;
#else
    dupPrm.inQueParams.prevLinkId    = gVsysModuleContext.muxId;
#endif
    dupPrm.inQueParams.prevLinkQueId = 0;

    if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
    {
        dupPrm.numOutQue                 = 1; /* DISABLE_DISPLAY */
    }
    else
    {
#ifdef FACTORY_TEST
        if(gDisplay == FALSE)
        {
            dupPrm.numOutQue                 = 1;
        }
        else
#endif
        {
#ifdef DISABLE_DISPLAY
        dupPrm.numOutQue                 = 1;
#else
        dupPrm.numOutQue                 = 2;
#endif
        }
    }

    dupPrm.outQueParams[0].nextLink  = gVencModuleContext.encId;
    dupPrm.outQueParams[1].nextLink  = gVdisModuleContext.displayId[VDIS_DEV_HDMI];
    dupPrm.notifyNextLink            = TRUE;

    /* HDMI DISPLAY */
    MULTICH_INIT_STRUCT(DisplayLink_CreateParams,displayPrm);
    displayPrm.inQueParams[0].prevLinkId    = dupId;
    displayPrm.inQueParams[0].prevLinkQueId = 1;
    displayPrm.displayRes                   = gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_HDMI].resolution;
    displayPrm.displayId                    = DISPLAY_LINK_DISPLAY_SC2;

    /* ENC */
    MULTICH_INIT_STRUCT(EncLink_CreateParams,encPrm);
    {
        EncLink_ChCreateParams   *pLinkChPrm;
        EncLink_ChDynamicParams  *pLinkDynPrm;
        VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
        VENC_CHN_PARAMS_S        *pChPrm;

        if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM ||
           pCameraInstPrm->standard == SYSTEM_STD_4664_3496_10 ||
           pCameraInstPrm->standard == SYSTEM_STD_4656_3492_10)
        {
            encPrm.chCreateParams[0].format = IVIDEO_MJPEG;
            encPrm.chCreateParams[0].profile = 0;
            encPrm.chCreateParams[0].dataLayout = IVIDEO_PROGRESSIVE;
            encPrm.chCreateParams[0].fieldMergeEncodeEnable = FALSE;
            encPrm.chCreateParams[0].defaultDynamicParams.intraFrameInterval = 0;
            encPrm.chCreateParams[0].encodingPreset = 0;
            encPrm.chCreateParams[0].enableAnalyticinfo = 0;
            encPrm.chCreateParams[0].enableWaterMarking = 0;
            encPrm.chCreateParams[0].defaultDynamicParams.inputFrameRate = 60;
            encPrm.chCreateParams[0].rateControlPreset = 0;
            encPrm.chCreateParams[0].defaultDynamicParams.targetBitRate = 100 * 1000;
            encPrm.chCreateParams[0].defaultDynamicParams.interFrameInterval = 0;
            encPrm.chCreateParams[0].defaultDynamicParams.mvAccuracy = 0;

            encPrm.chCreateParams[0].defaultDynamicParams.qpInit = 80;   /* Quality Factor */
            encPrm.chCreateParams[0].defaultDynamicParams.rcAlg  = 2;    /* 0:    RATECONTROL_VBR */
                                                                         /* 1:    RATECONTROL_CBR */
                                                                         /* 2:    RATECONTROL_OFF */
            encPrm.numBufPerCh[0] = numEncBuf;

            gVencModuleContext.encFormat[0] = encPrm.chCreateParams[0].format;
        }
        else
        {
            for (i = 0; i < VENC_PRIMARY_CHANNELS; i++)
            {
                pLinkChPrm  = &encPrm.chCreateParams[i];
                pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

                pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
                pDynPrm     = &pChPrm->dynamicParam;

                pLinkChPrm->format                  = IVIDEO_H264HP;
                pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
                pLinkChPrm->dataLayout              = IVIDEO_PROGRESSIVE;
                pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
                pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
                pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
                pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
                pLinkChPrm->rateControlPreset       = IVIDEO_USER_DEFINED;
                pLinkChPrm->enableHighSpeed         = FALSE;
                pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
                pLinkChPrm->StreamPreset            = 1;    /* 0:    ALG_VID_ENC_PRESET_AUTO */
                                                          /* 1:    ALG_VID_ENC_PRESET_HIGHSPEED */
                                                          /* 2:    ALG_VID_ENC_PRESET_HIGHQUALITY */
                                                          /* 3:    ALG_VID_ENC_PRESET_SVC_AUTO */

                pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
                pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
                pLinkDynPrm->interFrameInterval     = 1;
                pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
                pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
                pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
                pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
                pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
                pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
                pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
                pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;

                encPrm.numBufPerCh[i]           = numEncBuf;
                gVencModuleContext.encFormat[i] = pLinkChPrm->format;
            }
        }
    }

    encPrm.chCreateParams[0].defaultDynamicParams.inputFrameRate = 60;
    encPrm.chCreateParams[1].defaultDynamicParams.inputFrameRate = 30;
    encPrm.vsEnable                                              = 0;
    encPrm.isVaUseCase                                           = 0;

    if(gUI_mcfw_config.demoCfg.resolution_combo != RES_16MP_D1_STREAM &&
       pCameraInstPrm->standard != SYSTEM_STD_4664_3496_10 &&
       pCameraInstPrm->standard != SYSTEM_STD_4656_3492_10)
    {
        for (i = VENC_PRIMARY_CHANNELS; i < (VENC_CHN_MAX - 1); i++)
        {
            encPrm.chCreateParams[i].format = IVIDEO_MJPEG;
            encPrm.chCreateParams[i].profile = 0;
            encPrm.chCreateParams[i].dataLayout = IVIDEO_PROGRESSIVE;
            encPrm.chCreateParams[i].fieldMergeEncodeEnable = FALSE;
            encPrm.chCreateParams[i].defaultDynamicParams.intraFrameInterval = 0;
            encPrm.chCreateParams[i].encodingPreset = 0;
            encPrm.chCreateParams[i].enableAnalyticinfo = 0;
            encPrm.chCreateParams[i].enableWaterMarking = 0;
            encPrm.chCreateParams[i].defaultDynamicParams.inputFrameRate = 60;
            encPrm.chCreateParams[i].rateControlPreset = 0;
            encPrm.chCreateParams[i].defaultDynamicParams.targetBitRate = 100 * 1000;
            encPrm.chCreateParams[i].defaultDynamicParams.interFrameInterval = 0;
            encPrm.chCreateParams[i].defaultDynamicParams.mvAccuracy = 0;

            encPrm.chCreateParams[i].defaultDynamicParams.qpInit = 80;    /* Quality Factor */
            encPrm.chCreateParams[i].defaultDynamicParams.rcAlg  = 2;     /* 0:    RATECONTROL_VBR */
                                                                          /* 1:    RATECONTROL_CBR */
                                                                          /* 2:    RATECONTROL_OFF */
            encPrm.numBufPerCh[i] = numEncBuf;

            gVencModuleContext.encFormat[i] = encPrm.chCreateParams[i].format;
        }
    }

    encPrm.inQueParams.prevLinkId    = dupId;
    encPrm.inQueParams.prevLinkQueId = 0;
    encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;

    /* IPC BITS OUT */
    ipcBitsOutVpssPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.encId;
    ipcBitsOutVpssPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsOutVpssPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsOutVpssPrm.baseCreateParams.outQueParams[0].nextLink  = gVencModuleContext.ipcBitsInHLOSId;

    if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
    {
      MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&ipcBitsOutVpssPrm,TRUE);
    }
    else
    {
#ifdef STREAMING_ON
        MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&ipcBitsOutVpssPrm,TRUE);
#else
      ipcBitsOutVpssPrm.baseCreateParams.noNotifyMode              = FALSE;
      ipcBitsOutVpssPrm.baseCreateParams.notifyNextLink            = TRUE;
      ipcBitsOutVpssPrm.baseCreateParams.notifyPrevLink            = TRUE;
#endif
    }

    /* IPC BITS IN HOST */
    ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.ipcBitsOutRTOSId;
    ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;

    if(gUI_mcfw_config.demoCfg.resolution_combo == RES_16MP_D1_STREAM)
    {
      MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&ipcBitsInHostPrm);
    }
    else
    {
#ifdef STREAMING_ON
        MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&ipcBitsInHostPrm);
#else
        ipcBitsInHostPrm.baseCreateParams.noNotifyMode              = FALSE;
        ipcBitsInHostPrm.baseCreateParams.notifyNextLink            = TRUE;
        ipcBitsInHostPrm.baseCreateParams.notifyPrevLink            = TRUE;
        ipcBitsInHostPrm.cbFxn                                      = MultiCh_ipcBitsInHostTomTomCb;
        ipcBitsInHostPrm.cbCtx                                      = NULL;
#endif
    }

    /*
     *    Link Creation
     */

    /* CAMERA */
    System_linkCreate(gVcamModuleContext.cameraId,&cameraPrm,sizeof(cameraPrm));
    System_linkControl(gVcamModuleContext.cameraId,CAMERA_LINK_CMD_DETECT_VIDEO,NULL,0,TRUE);

    if(gUI_mcfw_config.demoCfg.resolution_combo != RES_16MP_D1_STREAM)
    {
#ifndef DISABLE_VNF
        /* VNF */
        System_linkCreate(gVcamModuleContext.vnfId,&vnfPrm,sizeof(vnfPrm));
#endif
    }

    /* MUX */
    System_linkCreate(gVsysModuleContext.muxId,&muxPrm,sizeof(muxPrm));

#ifdef CONFIG_LB_OSD_ENABLE
    /* SWOSD */
    System_linkCreate(gVsysModuleContext.swOsdId,&swosdPrm,sizeof(swosdPrm));
#endif

    /* DUP */
    System_linkCreate(dupId,&dupPrm,sizeof(dupPrm));

    if(gUI_mcfw_config.demoCfg.resolution_combo != RES_16MP_D1_STREAM)
    {
#ifdef FACTORY_TEST
        if(gDisplay == TRUE)
#endif
        {
#ifndef DISABLE_DISPLAY
            /* HDMI DISPLAY */
            System_linkCreate(gVdisModuleContext.displayId[VDIS_DEV_HDMI],&displayPrm,sizeof(displayPrm));
#endif
        }
    }

    /* ENC */
    System_linkCreate(gVencModuleContext.encId,&encPrm,sizeof(encPrm));

    /* IPC BITS OUT */
    System_linkCreate(gVencModuleContext.ipcBitsOutRTOSId,&ipcBitsOutVpssPrm,sizeof(ipcBitsOutVpssPrm));

    /* IPC BITS IN HOST */
    System_linkCreate(gVencModuleContext.ipcBitsInHLOSId,&ipcBitsInHostPrm,sizeof(ipcBitsInHostPrm));

    /* Set Camera and Encoder target frame rate for channel */
    App_ipncApi_changeFrameRate(0,frameRate);

#ifdef CONFIG_LB_OSD_ENABLE
    if(
      (pCameraInstPrm->standard == SYSTEM_STD_1080P_1920_1080_C_30) ||
      (pCameraInstPrm->standard == SYSTEM_STD_1080P_1920_1080_LT_30) ||
      (pCameraInstPrm->standard == SYSTEM_STD_1080P_1920_1080_RT_30) ||
      (pCameraInstPrm->standard == SYSTEM_STD_1080P_1920_1080_LB_30) ||
      (pCameraInstPrm->standard == SYSTEM_STD_1080P_1920_1080_RB_30)
      )
    {
        /* SWOSD configuration */
        swosdGuiPrm.streamId           = 0;
        swosdGuiPrm.transparencyEnable = 0;
        swosdGuiPrm.dateEnable         = 0;
        swosdGuiPrm.timeEnable         = 0;
        swosdGuiPrm.logoEnable         = 0;
        swosdGuiPrm.logoPos            = 0;
        swosdGuiPrm.textEnable         = 0;
        swosdGuiPrm.textPos            = 0;
        swosdGuiPrm.detailedInfo       = 1;
        swosdGuiPrm.pUsrString         = (UInt8 *)"\0";

        Vsys_setSwOsdPrm(VSYS_SWOSDGUIPRM,&swosdGuiPrm);
    }
#endif
    OSA_printf("USECASE SETUP DONE\n");
}

/* ===================================================================
 *  @func     MultiCh_deleteTomTomUseCase
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Void MultiCh_deleteTomTomUseCase(Void)
{
    UInt32 dupId;

    dupId = SYSTEM_VPSS_LINK_ID_DUP_0;

    /*
     *    Link Deletion in reverse order
     */

    /* IPC BITS IN HOST */
    System_linkDelete(gVencModuleContext.ipcBitsInHLOSId);

    /* IPC BITS OUT */
    System_linkDelete(gVencModuleContext.ipcBitsOutRTOSId);

    /* ENC */
    System_linkDelete(gVencModuleContext.encId);

    if(gUI_mcfw_config.demoCfg.resolution_combo != RES_16MP_D1_STREAM)
    {
#ifdef FACTORY_TEST
        if(gDisplay == TRUE)
#endif
        {
#ifndef DISABLE_DISPLAY
            /* HDMI DISPLAY */
            System_linkDelete(gVdisModuleContext.displayId[VDIS_DEV_HDMI]);
#endif
        }
    }

    /* DUP */
    System_linkDelete(dupId);

#ifdef CONFIG_LB_OSD_ENABLE
    /* SWOSD */
    System_linkDelete(gVsysModuleContext.swOsdId);
#endif

    /* MUX */
    System_linkDelete(gVsysModuleContext.muxId);

    if(gUI_mcfw_config.demoCfg.resolution_combo != RES_16MP_D1_STREAM)
    {
#ifndef DISABLE_VNF
        /* VNF */
        System_linkDelete(gVcamModuleContext.vnfId);
#endif
    }

    /* CAMERA */
    System_linkDelete(gVcamModuleContext.cameraId);

    OSA_printf("USECASE TEARDOWN DONE\n");
}

/* ===================================================================
 *  @func     MultiCh_TomTomTranscodeUseCasecbFxn
 *
 *  @desc     Callback function registered with ipcBitsOutlink in the
 *            HLOS; it gets called whenever a buffer is available after
 *            encoding
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Void MultiCh_TomTomTranscodeUseCasecbFxn(Ptr cbCtx)
{
    Bitstream_BufList fullBitsBufList;
    Int32 i = 0;

    /* Get the Bitstream buf */
    IpcBitsInLink_getFullVideoBitStreamBufs(gVencModuleContext.ipcBitsInHLOSId,&fullBitsBufList);

    for(i = 0;i < fullBitsBufList.numBufs;i++)
    {
        fwrite(fullBitsBufList.bufs[i]->addr + fullBitsBufList.bufs[i]->startOffset,
               1,
               fullBitsBufList.bufs[i]->fillLength,
               gDecodeUseCase_obj.fpOutStream);
        OSA_printf("PTS for the current frame : %d\n",fullBitsBufList.bufs[i]->timeStamp);
    }
    /* Put the Emptied Buffer  */
    IpcBitsInLink_putEmptyVideoBitStreamBufs(gVencModuleContext.ipcBitsInHLOSId,&fullBitsBufList);

    /************************************************************************
     * Exit the Transcoding use-case if End of Stream is reached            *
     ************************************************************************/
    if(fullBitsBufList.bufs[i - 1]->isEOS)
    {
        gDecodeUseCase_obj.thrExit = TRUE;
    }
}

/* ===================================================================
 *  @func     MultiCh_createTomTomTranscodeUseCase
 *
 *  @desc     Creates a usecase for decoding bitstream
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This function takes the following inputs
 *            SYSTEM_Standard standard
 *            tells which video standard it uses
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
/*------------------------------ TRANSCODE USECASE -----------------------------------------

                                   ***********
                                   *  SDCARD *
                                   ***********
                                        |
                                        |
                                   IPC BITS OUT(A8)
                                        |
                                        |
                                IPC BITS IN(VPSSM3)
                                        |
                                        |
                                    DEC(VPSSM3)
                                        |
                                        |
                                    ENC(VPSSM3)
                                        |
                                        |
                                IPC BITS OUT(VPSSM3)
                                        |
                                        |
                                  IPC BITS IN(A8)
                                        |
                                        |
                                  *************
                                  * STREAMING *
                                  * FILE SAVE *
                                  *************
------------------------------------------------------------------------------------------*/
Void MultiCh_createTomTomTranscodeUseCase(SYSTEM_Standard standard)
{
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVpssPrm;
    DecLink_CreateParams              decPrm;
    EncLink_CreateParams              encPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutVpssPrm;
    IpcBitsInLinkHLOS_CreateParams    ipcBitsInHostPrm;
    IspLink_CreateParams              ispPrm;
    UInt32                            ispId;
    UInt32                            inputWidth, inputHeight;
    UInt32                            outputWidth, outputHeight;
    UInt32                            inputFps, outputFps;
    UInt32                            outputCodecType;
    UInt32                            numBuffers;

    EncLink_ChCreateParams            *pLinkChPrm;
    EncLink_ChDynamicParams           *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S          *pDynPrm;
    VENC_CHN_PARAMS_S                 *pChPrm;

    Int32 i = 0, chId = 0;

    MULTICH_INIT_STRUCT(IpcBitsOutLinkHLOS_CreateParams, ipcBitsOutHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams, ipcBitsInVpssPrm);
    MULTICH_INIT_STRUCT(DecLink_CreateParams, decPrm);
    MULTICH_INIT_STRUCT(EncLink_CreateParams, encPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams, ipcBitsOutVpssPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams, ipcBitsInHostPrm);

    OSA_printf("\n********************* Entered TOM TOM TRANSCODE    USECASE ********************\n\n");

    /*
     * Transcoding configuration
     */
    inputWidth      = transcodeStrCfg.inputWidth;
    inputHeight     = transcodeStrCfg.inputHeight;
    outputWidth     = transcodeStrCfg.outputWidth;
    outputHeight    = transcodeStrCfg.outputHeight;
    inputFps        = transcodeStrCfg.inputFps;
    outputFps       = transcodeStrCfg.outputFps;
    outputCodecType = transcodeStrCfg.outputCodecType;

    /*
     * For 4K resolution, memory is not sufficient, hence reducing the
     * number of buffers
     */
    if((inputWidth == 3840) && (inputHeight == 2160))
    {
        numBuffers = NUM_ENCODE_BUFFERS >> 1;
    }
    else
    {
        numBuffers = NUM_ENCODE_BUFFERS;
    }

    /*
     *    Link parameter population
     */

    /* LINK IDs */
    gVencModuleContext.encId             = SYSTEM_LINK_ID_VENC_1;
    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_VPSS_LINK_ID_IPC_BITS_OUT_1;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;
    gVencModuleContext.ipcM3OutId        = SYSTEM_LINK_ID_INVALID;
    gVencModuleContext.ipcM3InId         = SYSTEM_LINK_ID_INVALID;

    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_1;
    gVdecModuleContext.ipcBitsInRTOSId   = SYSTEM_VPSS_LINK_ID_IPC_BITS_IN_1;
    gVdecModuleContext.decId             = SYSTEM_LINK_ID_VDEC_0;
    gVdecModuleContext.ipcM3OutId        = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcM3InId         = SYSTEM_LINK_ID_INVALID;
    ispId                                = SYSTEM_LINK_ID_ISP_0;

    gVdecModuleContext.vdecConfig.numChn = 1;

    /* ipc bits out link */
    ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink= gVdecModuleContext.ipcBitsInRTOSId;
    ipcBitsOutHostPrm.baseCreateParams.notifyNextLink       = TRUE;
    ipcBitsOutHostPrm.baseCreateParams.notifyPrevLink       = TRUE;
    ipcBitsOutHostPrm.baseCreateParams.noNotifyMode         = FALSE;
    ipcBitsOutHostPrm.baseCreateParams.numOutQue            = 1;
    ipcBitsOutHostPrm.inQueInfo.numCh                       = gVdecModuleContext.vdecConfig.numChn;

    for (i=0; i<ipcBitsOutHostPrm.inQueInfo.numCh; i++)
    {
        ipcBitsOutHostPrm.inQueInfo.chInfo[i].width = inputWidth;

        ipcBitsOutHostPrm.inQueInfo.chInfo[i].height = inputHeight;

        ipcBitsOutHostPrm.inQueInfo.chInfo[i].scanFormat =
            SYSTEM_SF_PROGRESSIVE;

        ipcBitsOutHostPrm.numBufPerCh[i] = numBuffers;
    }

    /* ipc bits in link */
    ipcBitsInVpssPrm.baseCreateParams.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsOutHLOSId;
    ipcBitsInVpssPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsInVpssPrm.baseCreateParams.outQueParams[0].nextLink  = gVdecModuleContext.decId;
    ipcBitsInVpssPrm.baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsInVpssPrm.baseCreateParams.notifyNextLink            = TRUE;
    ipcBitsInVpssPrm.baseCreateParams.notifyPrevLink            = TRUE;
    ipcBitsInVpssPrm.baseCreateParams.numOutQue                 = 1;

    /* decoder link */
    for (i=0; i<ipcBitsOutHostPrm.inQueInfo.numCh; i++)
    {

        decPrm.chCreateParams[i].format = IVIDEO_H264HP;

        decPrm.chCreateParams[i].numBufPerCh
                         = gVdecModuleContext.vdecConfig.decChannelParams[i].numBufPerCh;
        decPrm.chCreateParams[i].profile = IH264VDEC_PROFILE_ANY;
        decPrm.chCreateParams[i].displayDelay
                         = gVdecModuleContext.vdecConfig.decChannelParams[i].displayDelay;
        decPrm.chCreateParams[i].dpbBufSizeInFrames = IH264VDEC_DPB_NUMFRAMES_AUTO;
        decPrm.chCreateParams[i].fieldMergeDecodeEnable = FALSE;

        decPrm.chCreateParams[i].targetMaxWidth  =
            ipcBitsOutHostPrm.inQueInfo.chInfo[i].width;

        decPrm.chCreateParams[i].targetMaxHeight =
            ipcBitsOutHostPrm.inQueInfo.chInfo[i].height;

        decPrm.chCreateParams[i].defaultDynamicParams.targetFrameRate =
            gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;

        decPrm.chCreateParams[i].defaultDynamicParams.targetBitRate =
            gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;

        decPrm.chCreateParams[i].numBufPerCh = numBuffers;
    }

    decPrm.inQueParams.prevLinkId       = gVdecModuleContext.ipcBitsInRTOSId;
    decPrm.inQueParams.prevLinkQueId    = 0;
    decPrm.outQueParams.nextLink        = ispId;

     /* isp link params */
    ispPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    ispPrm.inQueParams.prevLinkQueId = 0;
    ispPrm.numOutQueue               = 1;
    ispPrm.outQueInfo[0].nextLink    = gVencModuleContext.encId;
    ispPrm.outQueInfo[1].nextLink    = SYSTEM_LINK_ID_INVALID;
    ispPrm.clkDivM                   = 1;
    ispPrm.clkDivN                   = 2;
    ispPrm.vsEnable                  = FALSE;
    ispPrm.vnfFullResolution         = FALSE;

    ispPrm.outQueuePrm[0].dataFormat = SYSTEM_DF_YUV420SP_UV;
    ispPrm.outQueuePrm[0].width      = outputWidth;
    ispPrm.outQueuePrm[0].height     = outputHeight;
    ispPrm.outQueuePrm[0].standard   = SYSTEM_STD_1080P_60; // SYSTEM_STD_NTSC;//SYSTEM_STD_PAL;//SYSTEM_STD_NTSC;
    ispPrm.doRszCfg                  = TRUE;

    /* enc Link */
    encPrm.inQueParams.prevLinkId       = ispId;
    encPrm.inQueParams.prevLinkQueId    = 0;
    encPrm.outQueParams.nextLink        = gVencModuleContext.ipcBitsOutRTOSId;

    pLinkChPrm  = &encPrm.chCreateParams[0];
    pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

    pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[0];
    pDynPrm     = &pChPrm->dynamicParam;

    if(outputCodecType == IVIDEO_H264HP) /* H264 HP output codec type */
    {
        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[0];
        pLinkChPrm->dataLayout              = IVIDEO_PROGRESSIVE;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = IVIDEO_USER_DEFINED;
        pLinkChPrm->enableHighSpeed         = FALSE;
        pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
        pLinkChPrm->StreamPreset            = 1;    /* 0:    ALG_VID_ENC_PRESET_AUTO */
                                                    /* 1:    ALG_VID_ENC_PRESET_HIGHSPEED */
                                                    /* 2:    ALG_VID_ENC_PRESET_HIGHQUALITY */
                                                    /* 3:    ALG_VID_ENC_PRESET_SVC_AUTO */

        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }
    else if(outputCodecType == IVIDEO_H264MP) /* H264 MP output codec type */
    {
        pLinkChPrm->format                  = IVIDEO_H264MP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[0];
        pLinkChPrm->dataLayout              = IVIDEO_PROGRESSIVE;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = IVIDEO_USER_DEFINED;
        pLinkChPrm->enableHighSpeed         = FALSE;
        pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
        pLinkChPrm->StreamPreset            = 1;    /* 0:    ALG_VID_ENC_PRESET_AUTO */
                                                    /* 1:    ALG_VID_ENC_PRESET_HIGHSPEED */
                                                    /* 2:    ALG_VID_ENC_PRESET_HIGHQUALITY */
                                                    /* 3:    ALG_VID_ENC_PRESET_SVC_AUTO */

        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }
    else if(outputCodecType == IVIDEO_MJPEG) /* MJPEG output codec type */
    {
        pLinkChPrm->format                  = IVIDEO_MJPEG;
        pLinkChPrm->profile                 = 0;
        pLinkChPrm->dataLayout              = IVIDEO_PROGRESSIVE;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->encodingPreset          = 0;
        pLinkChPrm->rateControlPreset       = 0;
        pLinkChPrm->enableAnalyticinfo      = 0;
        pLinkChPrm->enableWaterMarking      = 0;

        pLinkDynPrm->intraFrameInterval     = 0;
        pLinkDynPrm->inputFrameRate         = 60;
        pLinkDynPrm->targetBitRate          = 100 * 1000;
        pLinkDynPrm->mvAccuracy             = 0;
        pLinkDynPrm->qpInit                 = 80;
        pLinkDynPrm->rcAlg                  = 2;
    }
    encPrm.numBufPerCh[0]               = numBuffers;
    gVencModuleContext.encFormat[0]     = pLinkChPrm->format;

    encPrm.chCreateParams[0].defaultDynamicParams.inputFrameRate = outputFps;
    encPrm.chCreateParams[1].defaultDynamicParams.inputFrameRate = 30;
    encPrm.vsEnable                                              = 0;
    encPrm.isVaUseCase                                           = 0;

    /* ipc bit out vpss */
    ipcBitsOutVpssPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.encId;
    ipcBitsOutVpssPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsOutVpssPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsOutVpssPrm.baseCreateParams.outQueParams[0].nextLink  = gVencModuleContext.ipcBitsInHLOSId;
    ipcBitsOutVpssPrm.baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsOutVpssPrm.baseCreateParams.notifyNextLink            = TRUE;
    ipcBitsOutVpssPrm.baseCreateParams.notifyPrevLink            = TRUE;

    /* ipc bit in A8 */
    ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.ipcBitsOutRTOSId;
    ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsInHostPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsInHostPrm.baseCreateParams.outQueParams[0].nextLink   = SYSTEM_LINK_ID_INVALID;
    ipcBitsInHostPrm.baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsInHostPrm.baseCreateParams.notifyNextLink            = FALSE;
    ipcBitsInHostPrm.baseCreateParams.notifyPrevLink            = TRUE;
    ipcBitsInHostPrm.cbFxn = MultiCh_TomTomTranscodeUseCasecbFxn;
    ipcBitsInHostPrm.cbCtx = NULL;

    /*
     *    Link Creation
     */

    /* IPC BITS OUT */
    System_linkCreate(gVdecModuleContext.ipcBitsOutHLOSId,&ipcBitsOutHostPrm,sizeof(ipcBitsOutHostPrm));
    /* IPC BITS IN */
    System_linkCreate(gVdecModuleContext.ipcBitsInRTOSId,&ipcBitsInVpssPrm,sizeof(ipcBitsInVpssPrm));
    /* DEC */
    System_linkCreate(gVdecModuleContext.decId, &decPrm, sizeof(decPrm));
    /* ISP */
    System_linkCreate(ispId, &ispPrm, sizeof(ispPrm));
    /* ENC */
    System_linkCreate(gVencModuleContext.encId, &encPrm , sizeof(encPrm));
    /* IPC BITS OUT */
    System_linkCreate(gVencModuleContext.ipcBitsOutRTOSId, &ipcBitsOutVpssPrm , sizeof(ipcBitsOutVpssPrm));
    /* IPC BITS IN */
    System_linkCreate(gVencModuleContext.ipcBitsInHLOSId, &ipcBitsInHostPrm , sizeof(ipcBitsInHostPrm));

    /* 
     * Transcoding - Frame rate settings configuration
     * Only frame rate decrease is supported
     */
    if(inputFps > outputFps)
        transcodeConfigureFps(inputFps, outputFps);

    /* Set the encoder frame rate to transcoding output frame rate */
    pDynPrm->frameRate = outputFps;
    Venc_setInputFrameRate(chId, pDynPrm->frameRate);
    Venc_setDynamicParam(chId, 0, pDynPrm, VENC_FRAMERATE);

    /* IP ratio is made equal to framerate so that we have an IDR frame
       every second */
    pDynPrm->intraFrameInterval = pDynPrm->frameRate;
    Venc_setDynamicParam(chId, 0, pDynPrm, VENC_IPRATIO);

    OSA_printf("TRANSCODE USECASE : Create is done \n");
}

/* ===================================================================
 *  @func     MultiCh_deleteTomTomTranscodeUseCase
 *
 *  @desc     Creates a usecase for decoding bitstream
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This function takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Void MultiCh_deleteTomTomTranscodeUseCase(Void)
{
    Int32 ispId = SYSTEM_LINK_ID_ISP_0;
    /*
     * LINK delete
     */
    /* IPC BITS IN */
    System_linkDelete(gVencModuleContext.ipcBitsInHLOSId);
    /* IPC BITS OUT */
    System_linkDelete(gVencModuleContext.ipcBitsOutRTOSId);
    /* ENC */
    System_linkDelete(gVencModuleContext.encId);
    /* ISP */
    System_linkDelete(ispId);
    /* DEC */
    System_linkDelete(gVdecModuleContext.decId);
    /* IPC BITS IN */
    System_linkDelete(gVdecModuleContext.ipcBitsInRTOSId);
    /* IPC BITS OUT */
    System_linkDelete(gVdecModuleContext.ipcBitsOutHLOSId);
}

#endif
