/** ==================================================================
 *  @file   multich_tomtom_rel.c
 *
 *  @path    ipnc_mcfw/mcfw/src_linux/mcfw_api/usecases/
 *
 *  @desc   This  File contains.
 * ===================================================================
 *  Copyright (c) Texas Instruments Inc 2013, 2014
 *
 *  Use of this software is controlled by the terms and conditions found
 *  in the license agreement under which this software has been supplied
 * ===================================================================*/

/*------------------------------ TOMTOM LONGBEACH PIPELINE ---------------------------

                                      SENSOR                         FILES (TRANSCODING)
                                ________||_______                           |
                                |                |                          |
                                |   CAMERA LINK  |                    IPC BITSOUT (A8)
                                |____(VPSSM3)____|                          |
                                 RSZA        RSZB                           |
                             (1920x1080)   (848x480)                  IPC BITSIN (A8)
                               (420SP)      (420SP)                         |
                                  |            |                            |
                                  |            |                       DEC (VPSSM3)
                                  |            |                            |
                                  |            |                            |
                                  |            |                        ISP LINK
                                  |            |                            |
                                  |            |                            |
                                  |   +--------+                            |
                                  |   |                                     |
                                  |   |   +---------------------------------+
                                  |   |   |
                                  |   |   |
                              MERGE LINK (VPSSM3)
                                      |
                                      |
                              SELECT LINK (VPSSM3)
                                 |           |
                                 |           |
                            ENC(H264)   SIMCOP (MJPEG)
                                 |           |
                                 |           |
                             IPCBITSOUT   IPCBITSOUT
                                 |           |
                                 |           |
                             IPC BITSIN   IPCBITSIN
                                 |           |
                                 |           |
                                *****************
                                STREAM / FILEDUMP
                                *****************
------------------------------------------------------------------------------------------*/
#ifdef PLATFORM_LIB

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "mcfw/interfaces/link_api/system_tiler.h"
#include "tt_video_priv.h"

#define LONGBEACH_USECASE_CAM_NUM_BUFFERS     (2)
#define LONGBEACH_USECASE_DEC_NUM_BUFFERS     (3)
#define LONGBEACH_USECASE_IPC_POST_H264       (0)
#define LONGBEACH_USECASE_IPC_POST_MJPEG      (1)
#define LONGBEACH_USECASE_ISP_MAX_OUT_WIDTH   (1920)
#define LONGBEACH_USECASE_ISP_MAX_OUT_HEIGHT  (1080)

#define LONGBEACH_USECASE_CAMERA_Q0_TO_MERGE   (0)
#define LONGBEACH_USECASE_CAMERA_Q1_TO_MERGE   (1)
#define LONGBEACH_USECASE_ISP_TO_MERGE         (2)

#define LONGBEACH_USECASE_MERGE_OUT_CAM_CH0    (0)
#define LONGBEACH_USECASE_MERGE_OUT_CAM_CH1    (1)
#define LONGBEACH_USECASE_MERGE_OUT_ISP_CH0    (2)

#define LONGBEACH_SENSOR_MAX_OUT_WIDTH        (1940)
#define LONGBEACH_SENSOR_MAX_OUT_HEIGHT       (1091)
#define LONGBEACH_CAMERA_CH0_OUT_WIDTH        (1920)
#define LONGBEACH_CAMERA_CH0_OUT_HEIGHT       (1080)
#define LONGBEACH_CAMERA_CH1_OUT_WIDTH        (768)
#define LONGBEACH_CAMERA_CH1_OUT_HEIGHT       (480)
/* Setting secondary channel out put as 768x432 instead of 848x480 as a
 * a workaround for LBP-609 */
#define LONGBEACH_SECONDARY_CH_OUT_WIDTH      (768)
#define LONGBEACH_SECONDARY_CH_OUT_HEIGHT     (432)
#define LONGBEACH_USECASE_DEC_MAX_WIDTH       (LONGBEACH_CAMERA_CH0_OUT_WIDTH)
#define LONGBEACH_USECASE_DEC_MAX_HEIGHT      (LONGBEACH_CAMERA_CH0_OUT_HEIGHT)
#define LONGBEACH_CAMERA_MAX_OUT_WIDTH        (4656)
#define LONGBEACH_CAMERA_MAX_OUT_HEIGHT       (3492)

#define LONGBEACH_USECASE_IPC_BITSOUT_NUM_BUFFERS      (6)
/*Total number of output buffer headers available at encLink is 10000
  Dividing these across both channels*/
#define LONGBEACH_USECASE_ENCODER_NUM_BUFFERS          (5000)

/* default qp for jpeg endoder */
#define TTV_DEFAULT_JPEG_QP (90)

//#define LONGBEACH_ENABLE_RAW_CAPTURE

/*
 * Macro which change the pipeline so that channel 0 will be connected to
 * MJPEG link and channal 1 will be connected to iva encoder link
 * also decoder is configuared with WVGA resolution so as to meet the
 * memory needs for the still image pipeline
 */
/*#define TTV_STILL_IMAGE_PIPELINE (1)*/

static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl = {
    .isPopulated = 1,
    .ivaMap[0] = {
        .EncNumCh = 2,
        .EncChList = {0, 1},
        .DecNumCh = 2,
        .DecChList = {0, 1},
    },

};

void ttv_ipcBitsInHostCb(void * cbCtx)
{
    UInt32 i;
    Bitstream_BufList fullBitsBufList;
    UInt32 primaryEOS = 0;

    UInt32 ipcBitsInHostID = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;
    Bitstream_BufList fullBitsBufList2;

    /* Get the Bitstream buf */
    IpcBitsInLink_getFullVideoBitStreamBufs(gVencModuleContext.ipcBitsInHLOSId,&fullBitsBufList);

    for(i = 0; i < fullBitsBufList.numBufs; i++)
    {
        /* Mapping the channel name based on the mode. */

        if (fullBitsBufList.bufs[i]->codingType == IVIDEO_MJPEG)
            fullBitsBufList.bufs[i]->channelNum = TTV_CHANNEL_JPEG_FULL;
        else
            fullBitsBufList.bufs[i]->channelNum = TTV_CHANNEL_H264;

        if(g_cfg.mode == TTV_MODE_STILL)
            g_cfg.stillCapHighQualityCnt++;

        if (fullBitsBufList.bufs[i]->isEOS == 1)
            primaryEOS = 1;
        ttv_fifo_write(fullBitsBufList.bufs[i]);
    }
    /*MJPEG Write*/
    /*
     * MJPEG encoder running in the SIMCOP will give output to the second IPC bits out Link
     * Both MJPEG encoder and H264 encoder share the same callback function
     */
    IpcBitsInLink_getFullVideoBitStreamBufs(ipcBitsInHostID,&fullBitsBufList2);

    for(i = 0; i < fullBitsBufList2.numBufs; i++)
    {
        if(g_cfg.mode == TTV_MODE_STILL)
            g_cfg.stillCapLowQualityCnt++;
        if (fullBitsBufList2.bufs[i]->frameWidth == LONGBEACH_SECONDARY_CH_OUT_WIDTH)
            fullBitsBufList2.bufs[i]->channelNum = TTV_CHANNEL_JPEG_WVGA;
        else
            fullBitsBufList2.bufs[i]->channelNum = TTV_CHANNEL_JPEG_VGA;
        ttv_fifo_write(fullBitsBufList2.bufs[i]);
    }

    /* Currently Low quality frame count is not checked as
     * fps is fixed to 30 */
    if((g_cfg.stillCapHighQualityCnt >= g_cfg.stillCapNum || primaryEOS == 1)/*&& (g_cfg.stillCapLowQualityCnt >= g_cfg.stillCapNum)*/)
    {
        OSA_semSignal(&g_cfg.stillCapDoneSem);
    }

}

Void ttv_putEmptyBitstreamBuf(void *address)
{
    Bitstream_BufList emptyBitsBufList;
    UInt32 ipcBitsInHostID = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;

    emptyBitsBufList.bufs[0] = (Bitstream_Buf*) address;
    emptyBitsBufList.numBufs = 1;

    // Put back the bitstream buffer
    if(emptyBitsBufList.bufs[0]->channelNum == TTV_CHANNEL_JPEG_VGA
    || emptyBitsBufList.bufs[0]->channelNum == TTV_CHANNEL_JPEG_WVGA )
    {
        /*Secondary channel*/
        IpcBitsInLink_putEmptyVideoBitStreamBufs(ipcBitsInHostID,&emptyBitsBufList);
    }
    else
    {
        /*Primary channel*/
        IpcBitsInLink_putEmptyVideoBitStreamBufs(gVencModuleContext.ipcBitsInHLOSId,&emptyBitsBufList);
    }
}

/* ===================================================================
 *  @func     ttv_createPipeline
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */

void ttv_createPipeline(void)
{
#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    UInt32 i;
    UInt32 ispId;
#endif
    UInt32 mjpegEncId;
    UInt32 mergeId, selectId;
    UInt32 ipcBitsOutRTOSId,ipcBitsInHLOSId;

    /*Record path*/
    CameraLink_CreateParams cameraPrm;
    CameraLink_VipInstParams *pCameraInstPrm;
    CameraLink_OutParams *pCameraOutPrm;

#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    /*transcode path*/
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVpssPrm;
    DecLink_CreateParams              decPrm;
    IspLink_CreateParams              ispPrm;
#endif

    /*Common pipeline*/
    MergeLink_CreateParams    mergePrm;
    SelectLink_CreateParams   selectPrm;
    EncLink_CreateParams      encPrm;
    MjpegLink_CreateParams    mjpegEncPrm;
    IpcBitsOutLinkRTOS_CreateParams ipcBitsOutVpssPrm[2];
    IpcBitsInLinkHLOS_CreateParams  ipcBitsInHostPrm[2];

    debuglog(1,"\n********************* Entered TOM TOM SINGLE PIPELINE USECASE NEW USECASE ********************\n\n",0);

    MultiCh_detectBoard();

    System_linkControl(SYSTEM_LINK_ID_M3VPSS, SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES, NULL, 0, TRUE);

    System_linkControl(SYSTEM_LINK_ID_M3VPSS,
                       SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
                       &systemVid_encDecIvaChMapTbl,
                       sizeof(SystemVideo_Ivahd2ChMap_Tbl),
                       TRUE);

    MULTICH_INIT_STRUCT(CameraLink_CreateParams, cameraPrm);

#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    MULTICH_INIT_STRUCT(IpcBitsOutLinkHLOS_CreateParams, ipcBitsOutHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams, ipcBitsInVpssPrm);
    MULTICH_INIT_STRUCT(DecLink_CreateParams, decPrm);
    memset(&ispPrm, 0, sizeof(IspLink_CreateParams));
#endif

    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams, ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,  ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264]);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams, ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,  ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG]);

    /*************************Populate link Id*******************************/

    gVcamModuleContext.cameraId          = SYSTEM_LINK_ID_CAMERA;

#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId   = SYSTEM_VPSS_LINK_ID_IPC_BITS_IN_0;
    gVdecModuleContext.decId             = SYSTEM_LINK_ID_VDEC_0;
    gVdecModuleContext.ipcM3OutId        = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcM3InId         = SYSTEM_LINK_ID_INVALID;

    ispId                                = SYSTEM_LINK_ID_ISP_0;
#endif
    mergeId                              = SYSTEM_VPSS_LINK_ID_MERGE_0;
    selectId                             = SYSTEM_VPSS_LINK_ID_SELECT_0;

    gVencModuleContext.encId             = SYSTEM_LINK_ID_VENC_0;
    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_VPSS_LINK_ID_IPC_BITS_OUT_0;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    gVencModuleContext.ipcM3OutId        = SYSTEM_LINK_ID_INVALID;
    gVencModuleContext.ipcM3InId         = SYSTEM_LINK_ID_INVALID;

    mjpegEncId                           = SYSTEM_LINK_ID_MJPEG;
    ipcBitsOutRTOSId                     = SYSTEM_VPSS_LINK_ID_IPC_BITS_OUT_1;
    ipcBitsInHLOSId                      = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;

    /************************ Poulate link parameters ************************/

    /* CAMERA link */
    CameraLink_CreateParams_Init(&cameraPrm);

    cameraPrm.captureMode              = CAMERA_LINK_CAPMODE_ISIF;
    cameraPrm.numAudioChannels         = 1;
    cameraPrm.numVipInst               = 1;
    cameraPrm.tilerEnable              = FALSE;
    cameraPrm.vsEnable                 = FALSE;

    cameraPrm.outQueParams[0].nextLink = mergeId;
    cameraPrm.outQueParams[1].nextLink = mergeId;
    cameraPrm.t2aConfig.n2A_vendor     = UI_AEWB_ID_TI; //gUI_mcfw_config.n2A_vendor;
    cameraPrm.t2aConfig.n2A_mode       = UI_AEWB_AEWB; //gUI_mcfw_config.n2A_mode;

    cameraPrm.useMaxFrameSize          = TRUE;
    cameraPrm.maxWidth[0]              = LONGBEACH_CAMERA_MAX_OUT_WIDTH;
    cameraPrm.maxHeight[0]             = LONGBEACH_CAMERA_MAX_OUT_HEIGHT;
    cameraPrm.maxWidth[1]              = LONGBEACH_CAMERA_CH1_OUT_WIDTH;
    cameraPrm.maxHeight[1]             = LONGBEACH_CAMERA_CH1_OUT_HEIGHT;

    pCameraInstPrm = &cameraPrm.vipInst[0];
    pCameraInstPrm->vipInstId       = SYSTEM_CAMERA_INST_VP_CSI2;
    pCameraInstPrm->videoDecoderId  = MultiCh_getSensorId(SS_S5K2P1); //(gUI_mcfw_config.sensorId);
    pCameraInstPrm->inDataFormat    = SYSTEM_DF_YUV420SP_UV;
    pCameraInstPrm->standard        = SYSTEM_STD_1080P_4608_2592_60;
    pCameraInstPrm->numOutput       = 2;

    pCameraOutPrm = &pCameraInstPrm->outParams[0];
    pCameraOutPrm->dataFormat  = SYSTEM_DF_YUV420SP_UV;
    pCameraOutPrm->scEnable    = FALSE;

    pCameraInstPrm->sensorOutWidth  = LONGBEACH_SENSOR_MAX_OUT_WIDTH;
    pCameraInstPrm->sensorOutHeight = LONGBEACH_SENSOR_MAX_OUT_HEIGHT;
    pCameraOutPrm->scOutWidth       = LONGBEACH_CAMERA_CH0_OUT_WIDTH;
    pCameraOutPrm->scOutHeight      = LONGBEACH_CAMERA_CH0_OUT_HEIGHT;
    pCameraOutPrm->outQueId         = 0;
    cameraPrm.numBufPerCh           = LONGBEACH_USECASE_CAM_NUM_BUFFERS;

    pCameraOutPrm = &pCameraInstPrm->outParams[1];
    pCameraOutPrm->dataFormat  = SYSTEM_DF_YUV420SP_UV;
    pCameraOutPrm->scEnable    = FALSE;
    pCameraOutPrm->scOutWidth  = LONGBEACH_SECONDARY_CH_OUT_WIDTH;
    pCameraOutPrm->scOutHeight = LONGBEACH_SECONDARY_CH_OUT_HEIGHT;
    pCameraOutPrm->outQueId    = 1;

#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    /* IPC_BITS_OUT link */
    ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink = gVdecModuleContext.ipcBitsInRTOSId;
    ipcBitsOutHostPrm.baseCreateParams.notifyNextLink           = TRUE;
    ipcBitsOutHostPrm.baseCreateParams.notifyPrevLink           = TRUE;
    ipcBitsOutHostPrm.baseCreateParams.noNotifyMode             = FALSE;
    ipcBitsOutHostPrm.baseCreateParams.numOutQue                = 1;
    ipcBitsOutHostPrm.inQueInfo.numCh                           = 1;

    for (i=0; i<ipcBitsOutHostPrm.inQueInfo.numCh; i++)
    {
#ifndef TTV_STILL_IMAGE_PIPELINE
        ipcBitsOutHostPrm.inQueInfo.chInfo[i].width      = LONGBEACH_USECASE_DEC_MAX_WIDTH;
        ipcBitsOutHostPrm.inQueInfo.chInfo[i].height     = LONGBEACH_USECASE_DEC_MAX_HEIGHT;
#else
        ipcBitsOutHostPrm.inQueInfo.chInfo[i].width      =
                                              LONGBEACH_CAMERA_CH1_OUT_WIDTH;
        ipcBitsOutHostPrm.inQueInfo.chInfo[i].height     =
                                              LONGBEACH_CAMERA_CH1_OUT_HEIGHT;
#endif
        ipcBitsOutHostPrm.inQueInfo.chInfo[i].scanFormat = SYSTEM_SF_PROGRESSIVE;
        ipcBitsOutHostPrm.numBufPerCh[i]                 = LONGBEACH_USECASE_IPC_BITSOUT_NUM_BUFFERS;
    }

    /* IPC_BITS_IN link */
    ipcBitsInVpssPrm.baseCreateParams.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsOutHLOSId;
    ipcBitsInVpssPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsInVpssPrm.baseCreateParams.outQueParams[0].nextLink  = gVdecModuleContext.decId;
    ipcBitsInVpssPrm.baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsInVpssPrm.baseCreateParams.notifyNextLink            = TRUE;
    ipcBitsInVpssPrm.baseCreateParams.notifyPrevLink            = TRUE;
    ipcBitsInVpssPrm.baseCreateParams.numOutQue                 = 1;

    /* DECODER link */
    for (i=0; i<ipcBitsOutHostPrm.inQueInfo.numCh; i++)
    {

        decPrm.chCreateParams[i].format   = IVIDEO_H264HP;
        decPrm.chCreateParams[i].profile  = IH264VDEC_PROFILE_ANY;
        decPrm.chCreateParams[i].displayDelay
            = gVdecModuleContext.vdecConfig.decChannelParams[i].displayDelay;
        decPrm.chCreateParams[i].dpbBufSizeInFrames     = 2;
        decPrm.chCreateParams[i].fieldMergeDecodeEnable = FALSE;

        decPrm.chCreateParams[i].targetMaxWidth  = LONGBEACH_USECASE_DEC_MAX_WIDTH;
        decPrm.chCreateParams[i].targetMaxHeight = LONGBEACH_USECASE_DEC_MAX_HEIGHT;

        decPrm.chCreateParams[i].defaultDynamicParams.targetFrameRate =
            gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;

        decPrm.chCreateParams[i].defaultDynamicParams.targetBitRate =
            gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;

        decPrm.chCreateParams[i].numBufPerCh = LONGBEACH_USECASE_DEC_NUM_BUFFERS;
    }

    decPrm.inQueParams.prevLinkId       = gVdecModuleContext.ipcBitsInRTOSId;
    decPrm.inQueParams.prevLinkQueId    = 0;
    decPrm.outQueParams.nextLink        = ispId;

    /* ISP link params */
    ispPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    ispPrm.inQueParams.prevLinkQueId = 0;
    ispPrm.numOutQueue               = 1;
    ispPrm.outQueInfo[0].nextLink    = mergeId;
    ispPrm.outQueInfo[1].nextLink    = SYSTEM_LINK_ID_INVALID;
    ispPrm.clkDivM                   = 1;
    ispPrm.clkDivN                   = 2;
    ispPrm.vsEnable                  = FALSE;
    ispPrm.vnfFullResolution         = FALSE;

    ispPrm.outQueuePrm[0].dataFormat = SYSTEM_DF_YUV420SP_UV;
    ispPrm.outQueuePrm[0].width      = LONGBEACH_USECASE_ISP_MAX_OUT_WIDTH;
    ispPrm.outQueuePrm[0].height     = LONGBEACH_USECASE_ISP_MAX_OUT_HEIGHT;
    ispPrm.outQueuePrm[0].standard   = SYSTEM_STD_2_7K_4608_2592_30;
    if(g_cfg.mode == TTV_MODE_TRANSCODE)
    {
        ispPrm.doRszCfg                  = TRUE;
    }
    else
    {
        ispPrm.doRszCfg                  = FALSE;
    }

    /* MERGE link params */
    mergePrm.numInQue   = 3;
#else
    mergePrm.numInQue   = 2;
#endif

    mergePrm.inQueParams[LONGBEACH_USECASE_CAMERA_Q0_TO_MERGE].prevLinkId    = gVcamModuleContext.cameraId;
    mergePrm.inQueParams[LONGBEACH_USECASE_CAMERA_Q0_TO_MERGE].prevLinkQueId = 0;

    mergePrm.inQueParams[LONGBEACH_USECASE_CAMERA_Q1_TO_MERGE].prevLinkId    = gVcamModuleContext.cameraId;
    mergePrm.inQueParams[LONGBEACH_USECASE_CAMERA_Q1_TO_MERGE].prevLinkQueId = 1;

#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    mergePrm.inQueParams[LONGBEACH_USECASE_ISP_TO_MERGE].prevLinkId    = ispId;
    mergePrm.inQueParams[LONGBEACH_USECASE_ISP_TO_MERGE].prevLinkQueId = 0;
#endif

    mergePrm.outQueParams.nextLink  = selectId;
    mergePrm.notifyNextLink         = TRUE;

    /*SELECT link params*/
    selectPrm.numOutQue  = 2;

    selectPrm.inQueParams.prevLinkId    = mergeId;
    selectPrm.inQueParams.prevLinkQueId = 0;

    selectPrm.outQueParams[LONGBEACH_USECASE_IPC_POST_H264].nextLink  = gVencModuleContext.encId;
    selectPrm.outQueParams[LONGBEACH_USECASE_IPC_POST_MJPEG].nextLink = mjpegEncId;

    selectPrm.outQueChInfo[LONGBEACH_USECASE_IPC_POST_H264].numOutCh   = 2;
    selectPrm.outQueChInfo[LONGBEACH_USECASE_IPC_POST_H264].inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH0;
    selectPrm.outQueChInfo[LONGBEACH_USECASE_IPC_POST_H264].inChNum[1] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH0;

    selectPrm.outQueChInfo[LONGBEACH_USECASE_IPC_POST_MJPEG].numOutCh   = 1;
    selectPrm.outQueChInfo[LONGBEACH_USECASE_IPC_POST_MJPEG].inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH1;

    /* ENC */
    MULTICH_INIT_STRUCT(EncLink_CreateParams,encPrm);
    {
        EncLink_ChCreateParams   *pLinkChPrm;
        EncLink_ChDynamicParams  *pLinkDynPrm;
        VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
        VENC_CHN_PARAMS_S        *pChPrm;

        pLinkChPrm  = &encPrm.chCreateParams[0];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[0];
        pDynPrm     = &pChPrm->dynamicParam;

        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[0];
        pLinkChPrm->dataLayout              = IVIDEO_PROGRESSIVE;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = IVIDEO_USER_DEFINED;
        pLinkChPrm->enableHighSpeed         = FALSE;
        pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
        pLinkChPrm->StreamPreset            = 1;    /* 0:    ALG_VID_ENC_PRESET_AUTO */
        /* 1:    ALG_VID_ENC_PRESET_HIGHSPEED */
        /* 2:    ALG_VID_ENC_PRESET_HIGHQUALITY */
        /* 3:    ALG_VID_ENC_PRESET_SVC_AUTO */
        pLinkChPrm->minBitRate              = pChPrm->minBitRate;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
        gVencModuleContext.encFormat[0]     = pLinkChPrm->format;

        encPrm.chCreateParams[1].format = IVIDEO_MJPEG;
        encPrm.chCreateParams[1].profile = 0;
        encPrm.chCreateParams[1].dataLayout = IVIDEO_PROGRESSIVE;
        encPrm.chCreateParams[1].fieldMergeEncodeEnable = FALSE;
        encPrm.chCreateParams[1].defaultDynamicParams.intraFrameInterval = 0;
        encPrm.chCreateParams[1].encodingPreset = 0;
        encPrm.chCreateParams[1].enableAnalyticinfo = 0;
        encPrm.chCreateParams[1].enableWaterMarking = 0;
        encPrm.chCreateParams[1].defaultDynamicParams.inputFrameRate = 15;
        encPrm.chCreateParams[1].rateControlPreset = 0;
        encPrm.chCreateParams[1].defaultDynamicParams.targetBitRate = 100 * 1000;
        encPrm.chCreateParams[1].defaultDynamicParams.interFrameInterval = 0;
        encPrm.chCreateParams[1].defaultDynamicParams.mvAccuracy = 0;
        encPrm.chCreateParams[1].defaultDynamicParams.rcAlg = 2;    /* IJPEGVENC_RATECONTROL_DISABLE; */
        encPrm.chCreateParams[1].defaultDynamicParams.qpInit = TTV_DEFAULT_JPEG_QP;  /* qualityFactor */

        gVencModuleContext.encFormat[1] = encPrm.chCreateParams[1].format;
    }

    encPrm.chCreateParams[0].defaultDynamicParams.inputFrameRate = 15;
    encPrm.chCreateParams[1].defaultDynamicParams.inputFrameRate = 15;
    encPrm.vsEnable                                              = 0;
    encPrm.isVaUseCase                                           = 0;

    encPrm.inQueParams.prevLinkId    = selectId;
    encPrm.inQueParams.prevLinkQueId = 0;
    encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;
    encPrm.numBufPerCh[0]            = LONGBEACH_USECASE_ENCODER_NUM_BUFFERS;
    encPrm.numBufPerCh[1]            = LONGBEACH_USECASE_ENCODER_NUM_BUFFERS;

    /* IPC BITS OUT : [LONGBEACH_USECASE_IPC_POST_H264]*/
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.encId;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.numOutQue                 = 1;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.outQueParams[0].nextLink  = gVencModuleContext.ipcBitsInHLOSId;

    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.notifyNextLink            = TRUE;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.notifyPrevLink            = TRUE;

    /* IPC BITS IN HOST : [LONGBEACH_USECASE_IPC_POST_H264] */
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.ipcBitsOutRTOSId;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.inQueParams.prevLinkQueId = 0;

    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.notifyNextLink            = TRUE;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264].baseCreateParams.notifyPrevLink            = TRUE;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264].cbFxn                                      = ttv_ipcBitsInHostCb;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264].cbCtx                                      = NULL;

    /*MJPEG Link*/
    MULTICH_INIT_STRUCT(MjpegLink_CreateParams, mjpegEncPrm);
    {
        mjpegEncPrm.chCreateParams[0].format                               = IVIDEO_MJPEG;
        mjpegEncPrm.chCreateParams[0].profile                              = 0;
        mjpegEncPrm.chCreateParams[0].dataLayout                           = IVIDEO_PROGRESSIVE;
        mjpegEncPrm.chCreateParams[0].defaultDynamicParams.mjpegQp         = 45;
        mjpegEncPrm.chCreateParams[0].defaultDynamicParams.skipFrameFactor = 1;

        mjpegEncPrm.chCreateParams[1].format                               = IVIDEO_MJPEG;
        mjpegEncPrm.chCreateParams[1].profile                              = 0;
        mjpegEncPrm.chCreateParams[1].dataLayout                           = IVIDEO_PROGRESSIVE;
        mjpegEncPrm.chCreateParams[1].defaultDynamicParams.mjpegQp         = 95;
        mjpegEncPrm.chCreateParams[1].defaultDynamicParams.skipFrameFactor = 1;
    }
    mjpegEncPrm.inQueParams.prevLinkId    = selectId;
    mjpegEncPrm.inQueParams.prevLinkQueId = 1;
    mjpegEncPrm.outQueParams.nextLink     = ipcBitsOutRTOSId;

    /* IPC BITS OUT */
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.inQueParams.prevLinkId    = mjpegEncId;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.numOutQue                 = 1;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.outQueParams[0].nextLink  = ipcBitsInHLOSId;

    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.notifyNextLink            = TRUE;
    ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.notifyPrevLink            = TRUE;

    /* IPC BITS IN HOST */
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.inQueParams.prevLinkId    = ipcBitsOutRTOSId;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.inQueParams.prevLinkQueId = 0;

    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.noNotifyMode              = FALSE;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.notifyNextLink            = TRUE;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].baseCreateParams.notifyPrevLink            = TRUE;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].cbFxn                                      = ttv_ipcBitsInHostCb;
    ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG].cbCtx                                      = NULL;

    // Link Creation

    /* CAMERA */
    System_linkCreate(gVcamModuleContext.cameraId,&cameraPrm,sizeof(cameraPrm));
    System_linkControl(gVcamModuleContext.cameraId,CAMERA_LINK_CMD_DETECT_VIDEO,NULL,0,TRUE);

#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    /*Transcode Path*/
    /* IPC BITS OUT */
    System_linkCreate(gVdecModuleContext.ipcBitsOutHLOSId,&ipcBitsOutHostPrm,sizeof(ipcBitsOutHostPrm));
    /* IPC BITS IN */
    System_linkCreate(gVdecModuleContext.ipcBitsInRTOSId,&ipcBitsInVpssPrm,sizeof(ipcBitsInVpssPrm));
    /* DEC */
    System_linkCreate(gVdecModuleContext.decId, &decPrm, sizeof(decPrm));
    /* ISP */
    System_linkCreate(ispId, &ispPrm, sizeof(ispPrm));
#endif

    /*Common Path*/
    /*MERGE*/
    System_linkCreate(mergeId, &mergePrm, sizeof(mergePrm));
    /*SELECT*/
    System_linkCreate(selectId, &selectPrm, sizeof(selectPrm));

    /*H264 ENC Path*/
    /* ENC */
    System_linkCreate(gVencModuleContext.encId,&encPrm,sizeof(encPrm));
    /* IPC BITS OUT */
    System_linkCreate(gVencModuleContext.ipcBitsOutRTOSId,
            &ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264],
            sizeof(ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_H264]));
    /* IPC BITS IN HOST */
    System_linkCreate(gVencModuleContext.ipcBitsInHLOSId,
            &ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264],
            sizeof(ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_H264]));

    /*MJPEG ENC Path*/
    /*ENC*/
    System_linkCreate(mjpegEncId, &mjpegEncPrm, sizeof(mjpegEncPrm));
    /* IPC BITS OUT */
    System_linkCreate(ipcBitsOutRTOSId,
            &ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG],
            sizeof(ipcBitsOutVpssPrm[LONGBEACH_USECASE_IPC_POST_MJPEG]));
    /* IPC BITS IN HOST */
    System_linkCreate(ipcBitsInHLOSId,
            &ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG],
            sizeof(ipcBitsInHostPrm[LONGBEACH_USECASE_IPC_POST_MJPEG]));

    debuglog(1,"USECASE SETUP DONE\n",0);
}

/* ===================================================================
 *  @func     ttv_changeMode
 *
 *  @desc     Function does the following
 *
 *  @modif    This function changes the mode of operation
 *            mode can be still or video capture or transcode
 *
 *  @inputs   This function takes the following inputs
 *            mode - Can be still, video or transcode
 *
 *  @outputs  none
 *
 *  @return   none
 *  ==================================================================
 */
void ttv_changeMode(int mode,ttv_frame_type_t CodecType)
{
    SelectLink_OutQueChInfo outQueInfo;
    UInt32 selectId = SYSTEM_VPSS_LINK_ID_SELECT_0;

    /*Current implementaion is hardcoded to
      h.264 transcoding. To change to MJPEG
      transcoding modify the following under
      transcode = TRUE if check
      1. outQueInfo.numOutCh = 0 for outQueId 0
      2. outQueInfo.numOutCh = 1 for outQueId 1
      3. outQueInfo.inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_ISP_CH0; for outQueId 1
      */

    /*Reset channel map for both queues*/
    outQueInfo.outQueId = 0;
    outQueInfo.numOutCh = 0;
    System_linkControl(selectId,
                       SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                       &outQueInfo,
                       sizeof(outQueInfo),
                       TRUE);

    outQueInfo.outQueId = 1;
    System_linkControl(selectId,
                       SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                       &outQueInfo,
                       sizeof(outQueInfo),
                       TRUE);

    /*Queue 0 */
    outQueInfo.outQueId = 0;
    if(mode == TTV_MODE_TRANSCODE)
    {
        /* Transcoding of both H.264/(M)JPEG will happen on IVAHD */
        if(CodecType == TTV_TYPE_MJPEG)
        {
            outQueInfo.numOutCh = 2;
            outQueInfo.inChNum[0] = SYSTEM_MAX_CH_PER_OUT_QUE;
            outQueInfo.inChNum[1] = LONGBEACH_USECASE_MERGE_OUT_ISP_CH0;
        }
        else
        {
            outQueInfo.numOutCh = 1;
            outQueInfo.inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_ISP_CH0;
        }

    }
    else if (mode == TTV_MODE_STILL)
    {
        if(CodecType == TTV_TYPE_MJPEG)
        {
            outQueInfo.numOutCh = 2;
            outQueInfo.inChNum[0] = SYSTEM_MAX_CH_PER_OUT_QUE;
            outQueInfo.inChNum[1] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH0;
        }
        else if(CodecType == TTV_TYPE_H264HP)
        {
            outQueInfo.numOutCh = 1;
            outQueInfo.inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH0;
        }

    }
    else
    {
        outQueInfo.numOutCh = 1;
        outQueInfo.inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH0;
    }
    System_linkControl(selectId,
                       SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                       &outQueInfo,
                       sizeof(outQueInfo),
                       TRUE);

    /*Queue 1 */
    outQueInfo.outQueId = 1;
    if(mode == TTV_MODE_TRANSCODE)
    {
        /* Transcoding will always happen on IVAHD */
        outQueInfo.numOutCh = 0;
    }
    else if (mode == TTV_MODE_STILL)
    {
        outQueInfo.numOutCh = 1;
        outQueInfo.inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH1;
    }
    else
    {
        outQueInfo.numOutCh = 1;
        outQueInfo.inChNum[0] = LONGBEACH_USECASE_MERGE_OUT_CAM_CH1;
    }
    System_linkControl(selectId,
                       SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                       &outQueInfo,
                       sizeof(outQueInfo),
                       TRUE);
    if(CodecType == TTV_TYPE_H264HP)
    {
        if(Venc_switchCodecAlgCh(0, VCODEC_TYPE_H264, 1) != OSA_SOK)
            debuglog(1," Venc_switchCodecAlgCh failed for H264 HP",0);
    }
    else if(CodecType == TTV_TYPE_H264MP)
    {
        if(Venc_switchCodecAlgCh(0, VCODEC_TYPE_H264_MP, 0) != OSA_SOK)
            debuglog(1," Venc_switchCodecAlgCh failed for H264 Main Profile",0);
    }
    return;
}

/* ===================================================================
 *  @func     ttv_deletePipeline
 *
 *  @desc     Function does the following
 *
 *  @modif    This function deletes the TomTom pipeline
 *
 *  @inputs   none
 *
 *  @outputs  none
 *
 *  @return   none
 *  ==================================================================
 */
void ttv_deletePipeline(void)
{
#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    UInt32 ispId;
#endif
    UInt32 mjpegEncId;
    UInt32 mergeId, selectId;
    UInt32 ipcBitsOutRTOSId,ipcBitsInHLOSId;

    debuglog(1,"SINGLE PIPELINE TEARDOWN STARTS\n",0);

#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    ispId            = SYSTEM_LINK_ID_ISP_0;
#endif
    mergeId          = SYSTEM_VPSS_LINK_ID_MERGE_0;
    selectId         = SYSTEM_VPSS_LINK_ID_SELECT_0;
    mjpegEncId       = SYSTEM_LINK_ID_MJPEG;
    ipcBitsOutRTOSId = SYSTEM_VPSS_LINK_ID_IPC_BITS_OUT_1;
    ipcBitsInHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;

    // Link Deletion

    /* IPC BITS IN HOST */
    System_linkDelete(ipcBitsInHLOSId);
    /* IPC BITS OUT */
    System_linkDelete(ipcBitsOutRTOSId);
    /*ENC*/
    System_linkDelete(mjpegEncId);
    /* IPC BITS IN HOST */
    System_linkDelete(gVencModuleContext.ipcBitsInHLOSId);
    /* IPC BITS OUT */
    System_linkDelete(gVencModuleContext.ipcBitsOutRTOSId);
    /*ENC*/
    System_linkDelete(gVencModuleContext.encId);
    /*SELECT*/
    System_linkDelete(selectId);
    /*MERGE*/
    System_linkDelete(mergeId);
#ifndef LONGBEACH_ENABLE_RAW_CAPTURE
    /* ISP */
    System_linkDelete(ispId);
    /* DEC */
    System_linkDelete(gVdecModuleContext.decId);
    /* IPC BITS IN */
    System_linkDelete(gVdecModuleContext.ipcBitsInRTOSId);
    /* IPC BITS OUT */
    System_linkDelete(gVdecModuleContext.ipcBitsOutHLOSId);
#endif
    /* CAMERA */
    System_linkDelete(gVcamModuleContext.cameraId);

    debuglog(1,"SINGLE PIPELINE TEARDOWN DONE\n",0);
}

Int32 ttv_mjpeg_enableChn(MJPEG_CHN mjpegChnId)
{
    UInt32 mjpegEncId;
    MjpegLink_ChannelInfo params = { 0 };

    OSA_printf("\r\nEnable Channel: %d", mjpegChnId);

    params.chId = mjpegChnId;
    mjpegEncId = SYSTEM_LINK_ID_MJPEG;

    System_linkControl(mjpegEncId,
                       MJPEG_LINK_CMD_ENABLE_CHANNEL, &params, sizeof(params),
                       TRUE);

    return 0;

}

Int32 ttv_mjpeg_disableChn(MJPEG_CHN mjpegChnId)
{
    UInt32 mjpegEncId;
    MjpegLink_ChannelInfo params = { 0 };

    OSA_printf("\r\nDisable Channel: %d", mjpegChnId);

    params.chId = mjpegChnId;
    mjpegEncId = SYSTEM_LINK_ID_MJPEG;

    System_linkControl(mjpegEncId,
                       MJPEG_LINK_CMD_DISABLE_CHANNEL, &params, sizeof(params),
                       TRUE);

    return 0;
}

#endif
