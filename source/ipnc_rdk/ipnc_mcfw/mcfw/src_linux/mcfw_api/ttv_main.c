// TomTom Video platform API
// Initialisation and configuration functions
// Nebojsa Sumrak 2014.

#ifdef PLATFORM_LIB

#include "tt_video_priv.h"
#include "ttsystem_priv.h"

#include <osa.h>
#include "ti_vsys.h"
#include "ti_vcam.h"
#include "ti_venc.h"
#include "stream.h"

#define TTV_CHANNEL_PRIMARY   (0)
#define TTV_CHANNEL_SECONDARY (1)
#define TTV_CHANNEL_MJPEG_LQ  (0)
#define TTV_CHANNEL_MJPEG_HQ  (1)

#define TTV_LQ_RES_WIDTH_768 (768)
#define TTV_LQ_RES_WIDTH_640 (640)
#define TTV_LQ_RES_HEIGHT_480 (480)
#define TTV_LQ_RES_HEIGHT_432 (432)
#define TTV_MIN_ASPECT_RATIO_FOR_4_3  (1.31)
#define TTV_MAX_ASPECT_RATIO_FOR_4_3  (1.35)
#define TTV_MIN_ASPECT_RATIO_FOR_16_9 (1.75)
#define TTV_MAX_ASPECT_RATIO_FOR_16_9 (1.79)
#define TTV_ASPECT_RATIO_ERROR 0
#define TTV_DEFAULT_BITRATE_PERCENTAGE (100)

// Maximum and Minimum Bitrate for encoder
#define TTV_ENC_MAX_BITRATE (35000000)
#define TTV_ENC_MIN_BITRATE (2000000)
#define TTV_ENC_TIMELAPSE_MAX_BITRATE (100000000)
#define TTV_ENC_RECORDING_MAX_BITRATE (-1)
#define TTV_ENC_TIMELAPSE_VBR_DURATION (1)
#define TTV_ENC_RECORDING_VBR_DURATION (8)

// Default QP value for jpegencoder
#define TTV_JPEG_DEFAULT_QP (90)

/* Transcoding bitrates */
#define TTV_TRANSCODE_BITRATE_4K          (25000000)
#define TTV_TRANSCODE_BITRATE_2_7K        (25000000)
#define TTV_TRANSCODE_BITRATE_1080P       (25000000)
#define TTV_TRANSCODE_BITRATE_720P_HIGH   (20000000)
#define TTV_TRANSCODE_BITRATE_720P_LOW    (18000000)
#define TTV_TRANSCODE_BITRATE_480P        (16000000)

/* Encoder bitrate limits */
#define TTV_ENCODER_BITRATE_MIN           (16384)

struct ttv_global_config_t g_cfg = {
	.mode		= TTV_MODE_NONE,
	.started	= 0,

	.framerate	= 60,
	/*Pipeline is created with 4k_15 sensor config to meet the buffer requirements
	Before starting the pipeline it will be switched to 108060W*/
	.resolution	= TTV_RES_1080_60_WIDE,
	.resolution_new	= TTV_RES_1080_60_WIDE,
	.bitrate	= 100,

	.brightness	= 128,
	.saturation	= 128,
	.contrast	= 128,
	.hue		= 128,
	.sharpness	= 128,
	.aewbmode	= 3,
	/*maximum capture exposure in milliseconds
	 used for still capture and timelapse*/
	.ae_time	= 100,
	.wb_mode	= TTV_WB_AUTO,
	.ae_metering    = TTV_AE_CENTER,
	.ev_compensation= 0,

	.stillCapNum            = 0,
	.stillCapHighQualityCnt = 0,
	.stillCapLowQualityCnt  = 0,

	.emptyBitsBufList = { .numBufs = 0 },
	.want_to_change_mode = true,
	.waiting_for_frame = false,

	.is_fov_wide = true,

	.num_idr_per_second = 1,

	/* Encoder framerate is set only if the
	 * recording mode has changed, Hence the
	 * value should be the one corresponding
	 * to resolution_new
	 */
	.encoder_framerate = 60,
};

#ifdef LONGBEACH_ENABLE_RAW_CAPTURE

#define LONGBEACH_MAX_SENSOR_OUT_WIDTH    (3880)
#define LONGBEACH_MAX_SENSOR_OUT_HEIGHT   (2182)
#define LONGBEACH_RAW_CAPTURE_PROPERTIES_SIZE  (64)

CameraLink_rawCapture gRawCaptureInfo = {0};
Vsys_AllocBufInfo     gRawBuffer      = {0};

#endif

// resolution data table -- HAS TO BE EQUAL TO ttv_resolution_t ENTRIES
static const ttv_res_def_t resolution_defs[TTV_RES_MAX] = {
                                  {SYSTEM_STD_1080P_4608_2592_60, 1940, 1091, 1920, 1080, 1920,  600, 25000000, 1},
                                  {SYSTEM_STD_1080P_3840_2160_60, 1920, 1080, 1920, 1080, 1920,  600, 25000000, 0},
                                  {SYSTEM_STD_1080P_4608_2592_50, 1940, 1091, 1920, 1080, 1920,  500, 25000000, 1},
                                  {SYSTEM_STD_1080P_3840_2160_50, 1920, 1080, 1920, 1080, 1920,  500, 25000000, 0},
                                  {SYSTEM_STD_1080P_4608_2592_30, 1940, 1091, 1920, 1080, 1920,  300, 25000000, 1},
                                  {SYSTEM_STD_1080P_3840_2160_30, 1920, 1080, 1920, 1080, 1920,  300, 25000000, 0},
                                  {SYSTEM_STD_1080P_4608_2592_25, 1940, 1091, 1920, 1080, 1920,  250, 25000000, 1},
                                  {SYSTEM_STD_1080P_3840_2160_25, 1920, 1080, 1920, 1080, 1920,  250, 25000000, 0},
                                  {SYSTEM_STD_4K_4608_2592_15,    3880, 2182, 3840, 2160, 3840,  150, 25000000, 1},
                                  {SYSTEM_STD_4K_4608_2592_12_5,  3880, 2182, 3840, 2160, 3840,  125, 25000000, 1},
                                  {SYSTEM_STD_2_7K_4608_2592_30,  2730, 1536, 2704, 1520, 2704,  300, 25000000, 1},
                                  {SYSTEM_STD_2_7K_4608_2592_25,  2730, 1536, 2704, 1520, 2704,  250, 25000000, 1},
                                  {SYSTEM_STD_2_7K_3840_2160_25,  2792, 1570, 2704, 1520, 2704,  250, 25000000, 0},
                                  {SYSTEM_STD_720P_4608_2592_120, 1292,  727, 1280,  720, 1280, 1200, 20000000, 1},
                                  {SYSTEM_STD_720P_3840_2160_120, 1280,  720, 1280,  720, 1280, 1200, 20000000, 0},
                                  {SYSTEM_STD_720P_4608_2592_100, 1292,  727, 1280,  720, 1280, 1000, 20000000, 1},
                                  {SYSTEM_STD_720P_3840_2160_100, 1280,  720, 1280,  720, 1280, 1000, 20000000, 0},
                                  {SYSTEM_STD_720P_4608_2592_60,  1292,  727, 1280,  720, 1280,  600, 18000000, 1},
                                  {SYSTEM_STD_720P_3840_2160_60,  1280,  720, 1280,  720, 1280,  600, 18000000, 0},
                                  {SYSTEM_STD_720P_4608_2592_50,  1292,  727, 1280,  720, 1280,  500, 18000000, 1},
                                  {SYSTEM_STD_720P_3840_2160_50,  1280,  720, 1280,  720, 1280,  500, 18000000, 0},
                                  {SYSTEM_STD_WVGA_4608_2592_180,  876,  493,  848,  480,  848, 1800, 16000000, 1},
                                  {SYSTEM_STD_WVGA_4608_2592_150,  876,  493,  848,  480,  848, 1500, 16000000, 1},
                                  {SYSTEM_STD_WVGA_4608_2592_30,   768,  432,  768,  432,  768,  300, 10000000, 1},
                                  {SYSTEM_STD_4656_3492_10,       4656, 3488, 4656, 3488, 4656,  100, 10000000, 1},
                                  {SYSTEM_STD_8MP_4656_3492_15,   3386, 2539, 3264, 2448, 3264,  150, 10000000, 1},
                                  {SYSTEM_STD_4656_3492_5,        4656, 3488, 4656, 3488, 4656,   50, 10000000, 1},
                                  {SYSTEM_STD_8MP_4656_3492_5,    3386, 2539, 3264, 2448, 3264,   50, 10000000, 1},
                                  {SYSTEM_STD_WVGA_3840_2160_30,   768,  432,  768,  432,  768,  300, 10000000, 0},
                                  {SYSTEM_STD_WVGA_4608_2592_50,  1024,  576,  848,  480,  848,  500, 10000000, 1},
                                  {SYSTEM_STD_4K_4608_2592_14,    3880, 2182, 3840, 2160, 3840,  140, 25000000, 1},
                                  {SYSTEM_STD_WVGA_4608_2592_5,   1024,  576,  848,  480,  848,   50, 10000000, 1}
			};

void debuglog_raw(int level, const char *filej, int line, const char *fmt, ...)
{
	va_list varg;
	if(level>=5) return;
	va_start(varg, fmt);
	time_t t = time(NULL);
	struct timeval tv;
	gettimeofday(&tv,0);
	int msec = tv.tv_usec /= 1000;
	struct tm tm = *localtime(&t);
	printf("[API] %02d:%02d:%02d.%03d (%d):%s(%d): ", tm.tm_hour, tm.tm_min, tm.tm_sec, msec, level, filej, line);
	vprintf(fmt, varg);
	printf("\n");
	va_end(varg);
}


static bool changemode(int still)
{
	if(still) {
		if(Venc_switchCodecAlgCh(0, VCODEC_TYPE_MJPEG, 1) != OSA_SOK) return false;
		g_cfg.mode = TTV_MODE_STILL;
	} else {
		if(Venc_switchCodecAlgCh(0, VCODEC_TYPE_H264, 1) != OSA_SOK) return false;
		g_cfg.mode = TTV_MODE_VIDEO;
	}
	return true;
}


static void ttv_dccParamInit()
{
	FILE *dccVideoPtr;
	FILE *dccStillPtr;

	SystemHost_MemInfo  memInfo;
	Int status;

	dccVideoPtr = fopen(DCC_VIDEO_FILE_NAME, "rb");
	if(!dccVideoPtr) {
		debuglog(2, "DCC Video file (%s) not present!", DCC_VIDEO_FILE_NAME);
		return;
	}

	fseek(dccVideoPtr,0,SEEK_END);
	g_cfg.dccVideoParams.dccSize = ftell(dccVideoPtr);
	fseek(dccVideoPtr,0,SEEK_SET);
	Vsys_allocBuf(VSYS_SR1_SHAREMEM, g_cfg.dccVideoParams.dccSize, 32, &g_cfg.dccVideoBufInfo);
	if(!g_cfg.dccVideoBufInfo.virtAddr)
		debuglog(1, "DCC Video memory allocation failed!", 0);
	else  {
		fread(g_cfg.dccVideoBufInfo.virtAddr, sizeof(UInt32), g_cfg.dccVideoParams.dccSize, dccVideoPtr);
		debuglog(5, "DCC Video buffer allocated for size %d", g_cfg.dccVideoParams.dccSize);
	}
	fclose(dccVideoPtr);

	dccStillPtr = fopen(DCC_STILL_FILE_NAME, "rb");
	if(!dccStillPtr) {
		debuglog(2, "DCC Still file (%s) not present!", DCC_STILL_FILE_NAME);
		return;
	}

	fseek(dccStillPtr,0,SEEK_END);
	g_cfg.dccStillParams.dccSize = ftell(dccStillPtr);
	fseek(dccStillPtr,0,SEEK_SET);
	Vsys_allocBuf(VSYS_SR1_SHAREMEM, g_cfg.dccStillParams.dccSize, 32, &g_cfg.dccStillBufInfo);
	if(!g_cfg.dccStillBufInfo.virtAddr)
		debuglog(1, "DCC Still memory allocation failed!", 0);
	else  {
		fread(g_cfg.dccStillBufInfo.virtAddr, sizeof(UInt32), g_cfg.dccStillParams.dccSize, dccStillPtr);
		debuglog(5, "DCC Still buffer allocated for size %d", g_cfg.dccStillParams.dccSize);
	}
	fclose(dccStillPtr);

	memInfo.bufAddr = g_cfg.dccVideoBufInfo.physAddr;
	memInfo.bufSize = g_cfg.dccVideoParams.dccSize;

	status = System_linkControl(
		SYSTEM_LINK_ID_M3VPSS,
		SYSTEM_CMD_SYSTEM_STORE_HOST_MEM_INFO,
		&memInfo, sizeof(memInfo),
		TRUE);

	UTILS_assert(status==OSA_SOK);

	memInfo.bufAddr = g_cfg.dccStillBufInfo.physAddr;
	memInfo.bufSize = g_cfg.dccStillParams.dccSize;

	status = System_linkControl(
		SYSTEM_LINK_ID_M3VPSS,
		SYSTEM_CMD_SYSTEM_STORE_HOST_MEM_INFO,
		&memInfo, sizeof(memInfo),
		TRUE);

	UTILS_assert(status==OSA_SOK);
	debuglog(5, "DCC init done!", 0);
}


static void ttv_paramUpdate()
{
	VCAM_CHN_DYNAMIC_PARAM_S params = { 0 };

	// Update dcc address to camera link.
	if((g_cfg.dccVideoBufInfo.physAddr) && (g_cfg.dccStillBufInfo.physAddr))
	{
		params.dccVideoBufAddr = (Int32) g_cfg.dccVideoBufInfo.physAddr;
		params.dccStillBufAddr = (Int32) g_cfg.dccStillBufInfo.physAddr;
		params.dccVideoSize    = g_cfg.dccVideoParams.dccSize;
		params.dccStillSize    = g_cfg.dccStillParams.dccSize;
		Vcam_setDynamicParamChn(0, &params, VCAM_DCCBUFADDR);
	}

        /********************************************************************
         * Commenting the below image rotation settings because rotation is *
         * done by camera sensor. Configured camera sensor to achieve this. *
         ********************************************************************/
	// set mirror mode to vertical
	// params.mirrorMode = (UInt32)3; //==MIRROR_MODE_BOTH;
	// Vcam_setDynamicParamChn(0, &params, VCAM_MIRROR_MODE);

	// set image properties
	params.brightness = g_cfg.brightness;
	Vcam_setDynamicParamChn(0, &params, VCAM_BRIGHTNESS);
	params.contrast = g_cfg.contrast;
	Vcam_setDynamicParamChn(0, &params, VCAM_CONTRAST);
	params.saturation = g_cfg.saturation;
	Vcam_setDynamicParamChn(0, &params, VCAM_SATURATION);
	params.sharpness = g_cfg.sharpness;
	Vcam_setDynamicParamChn(0, &params, VCAM_SHARPNESS);
	params.aewbMode = g_cfg.aewbmode;
	Vcam_setDynamicParamChn(0, &params, VCAM_AEWB_MODE);
	params.hue = g_cfg.hue;
	Vcam_setDynamicParamChn(0, &params, VCAM_HUE);
	params.ae_time = g_cfg.ae_time;
	Vcam_setDynamicParamChn(0, &params, VCAM_AE_TIME);
	params.awbMode = g_cfg.wb_mode;
	Vcam_setDynamicParamChn(0, &params, VCAM_AWBMODE);
	params.aeMetering = g_cfg.ae_metering;
	Vcam_setDynamicParamChn(0, &params, VCAM_AE_METERING);
	params.evCompensation = g_cfg.ev_compensation;
	Vcam_setDynamicParamChn(0, &params, VCAM_EV_COMPENSATION);
}

bool ttv_set_sensor_rotation(ttv_rotation_t r)
{
	UInt32 rotation = r;
	debuglog(5, "ttv_sensor_rotation : %d", rotation);

	if((gVcamModuleContext.cameraId != SYSTEM_LINK_ID_INVALID) && !(g_cfg.started & TTV_STREAM_PRI))
	{
		// Checking if viewfinder is running
		// Sensor rotation cannot be done while streaming so disabling the streaming
		// and enabling the streaming after the sensor rotation change
		if(g_cfg.started) Vcam_enableSensorStreaming(false);
		// Set sensor orientation
		System_linkControl(gVcamModuleContext.cameraId,
				CAMERA_LINK_CMD_SET_ORIENTATION,
				&rotation,
				sizeof(UInt32),
				TRUE);
		// Set the bayer format in ISS IPIPE
		System_linkControl(gVcamModuleContext.cameraId,
				CAMERA_LINK_CMD_SET_BAYER_FORMAT,
				&rotation,
				sizeof(UInt32),
				TRUE);
		if(g_cfg.started) {
			Vcam_2AStabilization(20);
			Vcam_enableSensorStreaming(true);
		}
	}
	return true;
}


static inline void ttv_enable_channel(unsigned ch, bool enable, bool waitTillCompletion)
{
	debuglog(5,"ttv_enable_channel(%d,%s)",ch,enable?"true":"false");
	if (enable) {
		Vcam_enableChannel(ch);
		g_cfg.started |= (1 << ch);
	} else {
		Vcam_disableChannel(ch, waitTillCompletion);
		g_cfg.started &= ~(1 << ch);
	}
}


static void init(bool linksonly, bool transcode)
{
	VSYS_PARAMS_S   vsysParams;
	VCAM_PARAMS_S   vcamParams;
	VENC_PARAMS_S   vencParams;
	//VDEC_PARAMS_S   vdecParams;

	debuglog(5, "usecase init", 0);

	Vsys_params_init(&vsysParams);
	vsysParams.systemUseCase		= transcode ? VSYS_TRANSCODE_USECASE : VSYS_USECASE_TOMTOM;
	vsysParams.enableSecondaryOut   = FALSE;
	vsysParams.enableNsf            = FALSE;
	vsysParams.enableCapture        = TRUE;
	vsysParams.enableNullSrc        = FALSE;
	vsysParams.numDeis              = 0;
	vsysParams.numSwMs              = 0;
	vsysParams.numDisplays			= 0;
	Vsys_init(&vsysParams,(Bool)linksonly);

	Vcam_params_init(&vcamParams);
	Vcam_init(&vcamParams);

	Venc_params_init(&vencParams);
	// Enabling generation of motion vector for channel 0 only
	vencParams.encChannelParams[0].enableAnalyticinfo = 0;
	// Disable SEI WaterMark generation from codec
	vencParams.encChannelParams[0].enableWaterMarking = 0;
	vencParams.encChannelParams[1].enableWaterMarking = 0;
	// Set max and min BitRate for VBR
	vencParams.encChannelParams[0].
        maxBitRate = vencParams.encChannelParams[0].dynamicParam.targetBitRate * ENC_LINK_MAX_BITRATE_PCT / 100;
	vencParams.encChannelParams[0].
        minBitRate = ENC_LINK_MIN_BITRATE;

	Venc_init(&vencParams);

	if(transcode) {
		VDEC_PARAMS_S vdecParams;
		Vdec_params_init(&vdecParams);
		vdecParams.decChannelParams[0].dynamicParam.targetBitRate = 10000 * 1000;
		vdecParams.numChn = 1;
		vdecParams.forceUseDecChannelParams = TRUE;
		Vdec_init(&vdecParams);
		debuglog(4,"----Decoder initialized",0);
	}

#if 0 // display
        Vdis_tiedVencInit(VDIS_DEV_DVO2, VDIS_DEV_HDCOMP, &vdisParams);
        vdisParams.deviceParams[VDIS_DEV_SD].resolution = VSYS_STD_NTSC;
        vdisParams.enableLayoutGridDraw = FALSE;
        Vdis_init(&vdisParams);
#endif

	ttv_dccParamInit();
	ttv_fifo_init();
	OSA_mutexCreate(&g_cfg.apiaccess);

	// Create Link instances and connects compoent blocks
	Vsys_setCamStandard(transcode ? SYSTEM_STD_1080P_4608_2592_60 : resolution_defs[g_cfg.resolution].standard);
	ttv_createPipeline();
	if(!transcode) {
		// Set Camera and Encoder target frame rate for channel
		ttv_set_framerate(0, g_cfg.framerate);
		ttv_set_framerate(1, 24);

		// Set Encoder bitrate for H.264 channel
		ttv_bitrate(g_cfg.bitrate);

		g_cfg.aewbmode = 3; // AEAWB turned on by default

		// Update dcc address to camera link.
		ttv_paramUpdate();

		g_cfg.mode = TTV_MODE_VIDEO;
	} else {
		g_cfg.mode = TTV_MODE_TRANSCODE;
	}
	g_cfg.want_to_change_mode = false;

	ttv_changeMode(g_cfg.mode,TTV_TYPE_H264HP);

	// channels are disabled by default
	ttv_enable_channel(TTV_CHANNEL_PRIMARY, false, false);
	ttv_enable_channel(TTV_CHANNEL_SECONDARY, false, false);

	/* Start and stop the capture pipeline */
	Vcam_start();

	ttv_paramUpdate();

	debuglog(5, "usecase init finished", 0);
}


static void deinit(bool linksonly)
{
	debuglog(5, "usecase deinit", 0);

#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
	if (gRawCaptureInfo.rawBuffer != NULL)
	{
			Vsys_freeBuf(VSYS_SR1_SHAREMEM,
										gRawBuffer.virtAddr,
										(gRawCaptureInfo.rawBufferSize + LONGBEACH_RAW_CAPTURE_PROPERTIES_SIZE) );
			memset(&gRawCaptureInfo, 0, sizeof(gRawCaptureInfo));
			memset(&gRawBuffer, 0, sizeof(gRawBuffer));
	}
#endif

	// cancel wait_for_frame
	g_cfg.want_to_change_mode = true;
	if(g_cfg.waiting_for_frame) {
		OSA_semSignal(&g_cfg.bitsInNotifySem);
		while(g_cfg.waiting_for_frame) usleep(10000);
	}

	ttv_fifo_deinit();
	OSA_mutexDelete(&g_cfg.apiaccess);
	ttv_deletePipeline();
	/* Free the memory allocated by Host since there is a random memory leak
	 * while freeing memory from ipcBitsOutHost */

	System_linkControl(SYSTEM_LINK_ID_M3VPSS,
				SYSTEM_CMD_SYSTEM_FREE_HOST_MEM,
				NULL, 0, TRUE);

	if((g_cfg.dccVideoBufInfo.virtAddr) && (g_cfg.dccStillBufInfo.virtAddr)) {
		//dccParamDelete();
		memset(&g_cfg.dccVideoBufInfo, 0, sizeof(g_cfg.dccVideoBufInfo));
		memset(&g_cfg.dccStillBufInfo, 0, sizeof(g_cfg.dccStillBufInfo));
		memset(&g_cfg.dccVideoParams, 0, sizeof(g_cfg.dccVideoParams));
		memset(&g_cfg.dccStillParams, 0, sizeof(g_cfg.dccStillParams));
	}

	Vsys_exit(linksonly?1:0);

	g_cfg.mode = TTV_MODE_NONE;

	debuglog(5, "usecase deinit finished", 0);
}


#ifdef LONGBEACH_POWERMODE_OBSOLETE
/* This function sets the platform power mode based on ttv mode and resolution
 * It should be called every time the mode and/or resolution is changed
 */
static bool ttv_change_powermode(ttv_mode_t mode, ttv_resolution_t resolution)
{
	ttsystem_priv_powermode_t powermode;

	if(mode == TTV_MODE_NONE) {
		debuglog(5, "_ttv_set_powermode deactivating video power", 0);
		return ttsystem_deactivate_video_power();
	}

	/* Use high video rate power mode for transcoding and still images */
	if((mode == TTV_MODE_TRANSCODE) || (mode == TTV_MODE_STILL)) {
		debuglog(5, "_ttv_set_powermode setting power mode to %d", TTSYSTEM_PRIV_POWERMODE_HIGHVIDEORATE);
		return ttsystem_activate_video_power(TTSYSTEM_PRIV_POWERMODE_HIGHVIDEORATE);
	}

	/* For TTV_MODE_VIDEO, power mode is decided based on resolution */
	switch(resolution) {
		/* 720p120, 720p100, 1080p60, 1080p30, 4K, 2.7K*/
		case TTV_RES_1080_60_WIDE:
		case TTV_RES_1080_60_NORMAL:
		case TTV_RES_1080_50_WIDE:
		case TTV_RES_1080_50_NORMAL:
		case TTV_RES_4K_15_WIDE:
		case TTV_RES_4K_12_5_WIDE:
		case TTV_RES_2_7K_30_WIDE:
		case TTV_RES_2_7K_25_WIDE:
		case TTV_RES_2_7K_25_NORMAL:
		case TTV_RES_720_120_WIDE:
		case TTV_RES_720_120_NORMAL:
		case TTV_RES_720_100_WIDE:
		case TTV_RES_720_100_NORMAL:
			powermode = TTSYSTEM_PRIV_POWERMODE_HIGHVIDEORATE;
			break;

		/* 1080p30, 1080p25, 720p60, 720p50, WVGA */
		case TTV_RES_1080_30_WIDE:
		case TTV_RES_1080_30_NORMAL:
		case TTV_RES_1080_25_WIDE:
		case TTV_RES_1080_25_NORMAL:
		case TTV_RES_720_60_WIDE:
		case TTV_RES_720_60_NORMAL:
		case TTV_RES_720_50_WIDE:
		case TTV_RES_720_50_NORMAL:
		case TTV_RES_WVGA_180_WIDE:
		case TTV_RES_WVGA_150_WIDE:
		case TTV_RES_WVGA_50_WIDE:
		case TTV_RES_WVGA_5_WIDE:
			powermode = TTSYSTEM_PRIV_POWERMODE_LOWVIDEORATE;
			break;

		/* Viewfinder */
		case TTV_RES_WVGA_30_WIDE:
		case TTV_RES_WVGA_30_NORMAL:
			powermode = TTSYSTEM_PRIV_POWERMODE_VIEWFINDER;
			break;

		default:
			debuglog(1, "_ttv_set_powermode invalid resolution or framerate", 0);
			return false;
	}

	debuglog(5, "_ttv_set_powermode setting power mode to %d", powermode);
	return ttsystem_activate_video_power(powermode);
}
#else
static bool ttv_change_powermode(ttv_mode_t mode, ttv_resolution_t resolution)
{
	debuglog(5, "power mode switching not enabled", 0);
	return true;
}
#endif

static void ttv_reset_encoder(ttv_frame_type_t codec_type)
{
	EncLink_ResetCodec resetCodec = {0};

	/* MJPEG codec does not require a reset.*/
	if ((codec_type == TTV_TYPE_H264BP) ||
			(codec_type == TTV_TYPE_H264MP) ||
			(codec_type == TTV_TYPE_H264HP))
	{
		resetCodec.chId = 0;

		System_linkControl(SYSTEM_LINK_ID_VENC_0, ENC_LINK_CMD_RESET_CODEC, &resetCodec, sizeof(EncLink_ResetCodec), TRUE);
	}
}

static void _stop_stream(ttv_stream_t stream);

bool ttv_mode(ttv_mode_t mode)
{
	int m=0;
	debuglog(5, "ttv_mode with param %d", mode);
	if(g_cfg.mode == mode) {
		debuglog(5, "ttv_mode is current, exiting...", 0);
		return true;
	}
	if(g_cfg.mode == TTV_MODE_NONE)
		init(false, false);

	switch(mode) {
		case TTV_MODE_NONE:
			ttv_change_powermode(mode, TTV_RES_NONE);
			deinit(false);
			return true;
		case TTV_MODE_STILL:
			g_cfg.mode = TTV_MODE_STILL;
			return true;
		case TTV_MODE_VIDEO:
			switch(g_cfg.mode) {
				case TTV_MODE_VIDEO:
					return true;
				case TTV_MODE_STILL:
					g_cfg.mode = TTV_MODE_VIDEO;
					ttv_changeMode(g_cfg.mode,TTV_TYPE_H264HP);
				case TTV_MODE_TRANSCODE:
					g_cfg.mode = TTV_MODE_VIDEO;
					ttv_changeMode(g_cfg.mode,TTV_TYPE_H264HP);
					return true;
				default:
					debuglog(3,"ttv_mode: video subsystem is in invalid state", 0);
					// pass through
				case TTV_MODE_NONE:
					init(false,false);
					return changemode(m);
			}
		case TTV_MODE_TRANSCODE:
			if(g_cfg.mode != TTV_MODE_TRANSCODE)
			{
				_stop_stream(TTV_STREAM_ALL);
				Vcam_stop();
				g_cfg.mode = TTV_MODE_TRANSCODE;
			}
			return true;
		default:
			debuglog(3,"ttv_mode called with wrong mode %d",(int)mode);
			return false;
	}
}


ttv_stream_t ttv_get_started()
{
	OSA_mutexLock(&g_cfg.apiaccess);
	ttv_stream_t ret = g_cfg.started;
	OSA_mutexUnlock(&g_cfg.apiaccess);
	return ret;
}

static bool _start_stream(ttv_stream_t stream)
{
	VENC_CHN_DYNAMIC_PARAM_S	*pDynPrm;
	VENC_CHN_PARAMS_S				*pChPrm;
	pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[0];
	pDynPrm     = &pChPrm->dynamicParam;
	EncLink_ChBitRateParams bitrateParams;
	UInt32 targetBitRate;

	if(g_cfg.mode == TTV_MODE_NONE) return false;

	// do not count for already started
	stream &= ~(g_cfg.started);
	// sanity check, only 2 streams supported
	if(!(stream&=3)) return true;

	if(g_cfg.mode == TTV_MODE_VIDEO) {

		if(!g_cfg.started) {
			Vcam_start();
		}

		if (!(g_cfg.started & TTV_STREAM_PRI) &&
			(stream & TTV_STREAM_PRI)) {
			if(g_cfg.resolution != g_cfg.resolution_new) {
				ttv_set_recording_mode(g_cfg.resolution_new);
				/*
				 * Clear the fifo before the recording starts if viewfinder is on.
				 * This is to avoid the unwanted low quality frames to be written
				 * when a recording session is started.
				 */
				if (g_cfg.started & TTV_STREAM_MJPG)
					ttv_fifo_clear();
			}

			/*
			 * If previous operation was transcoding this will set the proper frame rate
			 * for the current recording
			 */
			ttv_set_framerate(0, g_cfg.framerate);
			ttv_bitrate(g_cfg.bitrate);

			bitrateParams.chId = 0;
			targetBitRate = g_cfg.bitrate * (resolution_defs[g_cfg.resolution].defaultBitrate / 100) ;
			bitrateParams.maxBitRate =  (targetBitRate / 100 ) * 120;
			System_linkControl(gVencModuleContext.encId, ENC_LINK_CMD_SET_CODEC_MAX_BITRATE,
					&bitrateParams, sizeof(bitrateParams), TRUE);

			if (!(g_cfg.started & TTV_STREAM_MJPG) &&
				!(stream & TTV_STREAM_MJPG))
				ttv_enable_channel(TTV_CHANNEL_SECONDARY, false, false);

			/*Reset rate control*/
			ttv_reset_encoder(TTV_TYPE_H264HP);

			ttv_enable_channel(TTV_CHANNEL_PRIMARY, true, false);
			Venc_forceIDR(0,0);

			/*Hardcode Intraframe interval to 30 for 4K 0.5Sec timelapse case
			 *TODO: Let Application set the value through an API.
			 */
			if(g_cfg.resolution == TTV_RES_4K_14_WIDE)
			{
				pDynPrm->intraFrameInterval = 30;
			}
			else
			{
				pDynPrm->intraFrameInterval = ( resolution_defs[g_cfg.resolution].maxFramerate / 10) / g_cfg.num_idr_per_second;
			}
			Venc_setDynamicParam(0, 0, pDynPrm, VENC_IPRATIO);

			ttv_change_powermode(g_cfg.mode, g_cfg.resolution_new);
		}
		if (!(g_cfg.started & TTV_STREAM_MJPG) && (stream & TTV_STREAM_MJPG)) {
			if (!(g_cfg.started & TTV_STREAM_PRI)) {
				if(g_cfg.is_fov_wide) {
					ttv_set_recording_mode(TTV_RES_WVGA_30_WIDE);
					ttv_change_powermode(g_cfg.mode, TTV_RES_WVGA_30_WIDE);
				}
				else {
					ttv_set_recording_mode(TTV_RES_WVGA_30_NORMAL);
					ttv_change_powermode(g_cfg.mode, TTV_RES_WVGA_30_NORMAL);
				}
			}
			if (!(g_cfg.started & TTV_STREAM_PRI))
				ttv_enable_channel(TTV_CHANNEL_PRIMARY, false, false);
			ttv_enable_channel(TTV_CHANNEL_SECONDARY, true, false);
		}
		/*
		Using max no frames as 20 because even in worst case
		AE stabilization completes within 17 to 18 frmaes
		*/
		Vcam_2AStabilization(20);
	}

	if(g_cfg.mode == TTV_MODE_TRANSCODE) {
		if(!g_cfg.started) {
			ttv_fifo_clear();
			ttv_change_powermode(g_cfg.mode, TTV_RES_NONE);
			Venc_start();
			Visp_start();
			Vdec_start();
			Venc_forceIDR(0,0);
			g_cfg.started = 1;
		}
	}
	return true;

}

bool ttv_start(ttv_stream_t stream)
{
	debuglog(4,"ttv_start",0);
	OSA_mutexLock(&g_cfg.apiaccess);
	bool retValue = _start_stream(stream);
	OSA_mutexUnlock(&g_cfg.apiaccess);
	return retValue;
}

static void _stop_stream(ttv_stream_t stream)
{
	ttv_resolution_t prev_resolution;
	if(!g_cfg.started) return;

	if (stream == TTV_STREAM_PRI)
		if(g_cfg.is_fov_wide)
			ttv_change_powermode(g_cfg.mode, TTV_RES_WVGA_30_WIDE);
		else
			ttv_change_powermode(g_cfg.mode, TTV_RES_WVGA_30_NORMAL);
	else
		ttv_change_powermode(TTV_MODE_NONE, TTV_RES_NONE);

	if(g_cfg.mode == TTV_MODE_VIDEO) {

		if (stream & (TTV_STREAM_PRI | TTV_STREAM_MJPG)) {
			if ((g_cfg.started & TTV_STREAM_MJPG) && (stream & TTV_STREAM_MJPG))
			{
				ttv_enable_channel(TTV_CHANNEL_SECONDARY, false, false);
			}
			if ((g_cfg.started & TTV_STREAM_PRI) && (stream & TTV_STREAM_PRI)) {
				ttv_enable_channel(TTV_CHANNEL_PRIMARY, false, true);
				if (g_cfg.started & TTV_STREAM_MJPG) {
					prev_resolution = g_cfg.resolution;
					if(g_cfg.is_fov_wide)
						ttv_set_recording_mode(TTV_RES_WVGA_30_WIDE);
					else
						ttv_set_recording_mode(TTV_RES_WVGA_30_NORMAL);
					/*stabilize after switching to WVGA W30 for better viewfinder quality*/
					Vcam_2AStabilization(20);
					g_cfg.resolution_new = prev_resolution;
				}
			}
		}
		if (!g_cfg.started) {
			// should stop video pipeline
			debuglog(4,"ttv_stop: stop pipeline",0);
			/*Once the ieEOS flag is set streaming will be stopped from camera link*/
			usleep((999+g_cfg.framerate)*4000/g_cfg.framerate); // wait for 4 frames to be processed
		}
	}

	if(g_cfg.mode == TTV_MODE_TRANSCODE)
	{
		Visp_stop();
		g_cfg.started = 0;
		g_cfg.resolution = TTV_RES_NONE;
	}

}

void ttv_stop(ttv_stream_t stream)
{
	debuglog(4,"ttv_stop",0);
	OSA_mutexLock(&g_cfg.apiaccess);
	_stop_stream(stream);
	OSA_mutexUnlock(&g_cfg.apiaccess);

}

void ttv_set_framerate(unsigned chId, unsigned frameRate)
{
	VENC_CHN_DYNAMIC_PARAM_S params = {0};
	VCAM_CHN_DYNAMIC_PARAM_S params_camera = {0};
	VENC_CHN encChId;

	// Change Camera Frame Rate
	params_camera.frameRate = (frameRate * 1000);
	Vcam_setDynamicParamChn(chId,&params_camera,VCAM_FRAMERATE);

	/*h264 enc link is used only for channel 0
		Framerate needs to be set only for h264enc*/
	if(chId == 0)
	// The camera link returns back the actual frame rate set.
	// The returned frame rate value should be used for the encoder.

	// Change Encoder Frame Rate
	{
		/*Channel 0 of camera is given to both channels 0 and 1
			of iva enc link. Parameter updation is required for
			both channels*/
		encChId = 0;
		params.frameRate = params_camera.frameRate/1000;
		Venc_setInputFrameRate(encChId,params.frameRate);
		params.frameRate = g_cfg.encoder_framerate;
		Venc_setDynamicParam(encChId,0,&params,VENC_FRAMERATE);
		encChId = 1;
		Venc_setInputFrameRate(encChId,params.frameRate);
		params.frameRate = g_cfg.encoder_framerate;
		Venc_setDynamicParam(encChId,0,&params,VENC_FRAMERATE);
	}
}

void ttv_set_encoder_framerate(unsigned framerate)
{
	if(framerate > g_cfg.framerate)
		debuglog(3, "ttv_set_encoder_framerate: %d not supported", framerate);
	else
		g_cfg.encoder_framerate = framerate;
}


bool ttv_configure(ttv_resolution_t resolution, int framerate)
{
	ttv_resolution_t prev_resolution;
	debuglog(5,"ttv_configure res=%d fr=%d",resolution,framerate);
	if(resolution>=TTV_RES_MAX) {
		debuglog(3, "resolution set is not supported (%d)", (int)resolution);
		return false;
	}
	/*New resolution is backed up here and is updated only
		when the recording is stared. This avoids change in
		sensor standard as we scroll through the video mode list*/
	if (resolution != g_cfg.resolution_new) {
		g_cfg.resolution_new = resolution;
		g_cfg.is_fov_wide = resolution_defs[g_cfg.resolution_new].isFOVWide;
		/* when there is a mode change reset the bitrate to 100% of default bitrate */
		g_cfg.bitrate = TTV_DEFAULT_BITRATE_PERCENTAGE;
		g_cfg.encoder_framerate = resolution_defs[g_cfg.resolution_new].maxFramerate / 10;
	}
	if(g_cfg.started & TTV_STREAM_MJPG) {
		prev_resolution = g_cfg.resolution_new;
		if(resolution_defs[resolution].isFOVWide)
			ttv_set_recording_mode(TTV_RES_WVGA_30_WIDE);
		else
			ttv_set_recording_mode(TTV_RES_WVGA_30_NORMAL);
		g_cfg.resolution_new = prev_resolution;
		/*stabilize after switching to WVGA W30 for better viewfinder quality*/
		Vcam_2AStabilization(20);
	}

	/* Resetting idr frequency to 1 frame per second */
	g_cfg.num_idr_per_second = 1;

	return true;
}


bool ttv_config_transcode(ttv_video_res_t inputres, int inputfps, ttv_video_res_t outputres, int outputfps, ttv_frame_type_t outputenc)
{
	IspLink_DynOutQParams ispOutQParam;
	VENC_CHN_DYNAMIC_PARAM_S	*pDynPrm;
	VENC_CHN_PARAMS_S				*pChPrm;
	int decOutfps, highres;
	pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[0];
	pDynPrm     = &pChPrm->dynamicParam;
	int inputfpsx10, outputfpsx10;

	debuglog(5, "TTV Transcode configure inputres = %d outputres = %d",
		inputres, outputres);

	if((inputres != TTV_VRES_480P) &&
	   (inputres != TTV_VRES_720P) &&
	   (inputres != TTV_VRES_1080P) &&
	   (inputres != TTV_VRES_2_7K) &&
	   (inputres != TTV_VRES_4K)) {
		debuglog(4,"TTV Transcode: Unsupported input resolution %d", inputres);
		return false;
	}
	if((outputres != TTV_VRES_480P) &&
	   (outputres != TTV_VRES_2_7K) &&
	   (outputres != TTV_VRES_4K) &&
	   (outputres != TTV_VRES_720P) &&
	   (outputres != TTV_VRES_1080P)) {
		debuglog(4,"TTV Transcode: Unsupported output resolution %d", outputres);
		return false;
	}
	if((outputenc != TTV_TYPE_H264HP) &&
	   (outputenc != TTV_TYPE_MJPEG)) {
		debuglog(4,"TTV Transcode: Unsupported output codec %d", outputenc);
		return false;
	}
	if ((inputres == TTV_VRES_4K) &&
			(inputfps == 12)) {
			/* 4K12.5 FPS*/
			inputfpsx10 = 125;
	}
	else {
			inputfpsx10 = inputfps * 10;
	}
	outputfpsx10 = outputfps * 10;
	if ((outputfpsx10 > inputfpsx10) &&
			(outputfpsx10 % inputfpsx10)){
		debuglog(4,"TTV Transcode: Frame rate upscale to non-multiple of input FPS not supported", 0);
		return false;
	}

	ispOutQParam.numOutQueue = 1;

	decOutfps = (outputfps > inputfps) ? inputfps : outputfps;

	ttv_changeMode(TTV_MODE_TRANSCODE,outputenc);
	transcodeConfigureFps(inputfps, decOutfps);
	Venc_setInputFrameRate(0, decOutfps);
	pDynPrm->frameRate = outputfps;
	Venc_setDynamicParam(0, 0, pDynPrm, VENC_FRAMERATE);
	pDynPrm->intraFrameInterval = outputfps;
	Venc_setDynamicParam(0, 0, pDynPrm, VENC_IPRATIO);
	ispOutQParam.outQueuePrm[0].dataFormat = SYSTEM_DF_YUV420SP_UV;
	ispOutQParam.outQueuePrm[0].standard   = SYSTEM_STD_1080P_60;

	switch(outputres)
	{
		case TTV_VRES_480P:
			ispOutQParam.outQueuePrm[0].width          = 848;
			ispOutQParam.outQueuePrm[0].height         = 480;
			g_cfg.transcode_bitrate = TTV_TRANSCODE_BITRATE_480P;
			break;
		case TTV_VRES_720P:
			ispOutQParam.outQueuePrm[0].width	   = 1280;
			ispOutQParam.outQueuePrm[0].height	   = 720;
			if (decOutfps > 60) {
				g_cfg.transcode_bitrate = TTV_TRANSCODE_BITRATE_720P_HIGH;
			}
			else {
				g_cfg.transcode_bitrate = TTV_TRANSCODE_BITRATE_720P_LOW;
			}
			break;
		case TTV_VRES_1080P:
			ispOutQParam.outQueuePrm[0].width	   = 1920;
			ispOutQParam.outQueuePrm[0].height	   = 1080;
			 g_cfg.transcode_bitrate = TTV_TRANSCODE_BITRATE_1080P;
			break;
		case TTV_VRES_2_7K:
			ispOutQParam.outQueuePrm[0].width	   = 2704;
			ispOutQParam.outQueuePrm[0].height	   = 1520;
			g_cfg.transcode_bitrate = TTV_TRANSCODE_BITRATE_2_7K;
			break;
		case TTV_VRES_4K:
			ispOutQParam.outQueuePrm[0].width	   = 3840;
			ispOutQParam.outQueuePrm[0].height	   = 2160;
			g_cfg.transcode_bitrate = TTV_TRANSCODE_BITRATE_4K;
			break;
		default:
			debuglog(4,"TTV Transcode: Output resolution is not supported %d", outputres);
			return false;

	}
	System_linkControl(SYSTEM_LINK_ID_ISP_0,ISPLINK_CMD_SET_OUTQUEUERES,&ispOutQParam,sizeof(IspLink_DynOutQParams),FALSE);

	if(outputres == TTV_VRES_4K)
		highres = 1;
	else
		highres = 0;
	System_linkControl(SYSTEM_LINK_ID_ISP_0, ISPLINK_CMD_SET_TRANSCODE_RES, &highres, sizeof(highres), FALSE);

	/* Reset encoder rate control logic */
	ttv_reset_encoder(outputenc);

	return true;
}

void ttv_abort_transcode()
{
    void *addr = NULL;
    unsigned long size = sizeof(h264_wvga_buf);
    debuglog(4, "entering ttv_abort_transcode()", 0);
    addr = ttv_request_decode_frame(size);
    memcpy(addr, h264_wvga_buf, sizeof(h264_wvga_buf));
    ttv_put_frame(addr, size, 0, 1);
}

void ttv_bitrate(int brate)
{
	VENC_CHN_DYNAMIC_PARAM_S params = { 0 };

	if(brate<1) {
		debuglog(3, "ttv_bitrate: parameter (brate=%d) out of bounds", brate);
		return;
	}

	if(g_cfg.mode == TTV_MODE_TRANSCODE) {
		params.targetBitRate = brate * (g_cfg.transcode_bitrate / 100);
	}
	else {
		g_cfg.bitrate = brate;

		if(g_cfg.resolution != TTV_RES_NONE){
			params.targetBitRate = brate * (resolution_defs[g_cfg.resolution].defaultBitrate / 100);
		}
		else{
			params.targetBitRate = brate * (resolution_defs[g_cfg.resolution_new].defaultBitrate / 100);
		}
	}

	debuglog(4,"Selected bitrate = %d", params.targetBitRate);

	if (params.targetBitRate < TTV_ENCODER_BITRATE_MIN) {
		/*
		 * Encoder crashes if a value less than
		 * TTV_ENCODER_BITRATE_MIN is set.
		 * Resetting the value to TTV_ENCODER_BITRATE_MIN
		 */
		debuglog (4, "ttv_bitrate: Calculated bitrate is less than min supported bitrate(%d)", TTV_ENCODER_BITRATE_MIN);
		debuglog (4, "ttv_bitrate: Reseting the value to %d", TTV_ENCODER_BITRATE_MIN);
		params.targetBitRate = TTV_ENCODER_BITRATE_MIN ;
	}

	Venc_setDynamicParam(0, 0, &params, VENC_BITRATE);
}


void ttv_set_thumb_fps(unsigned fps)
{
	if(fps<1 || fps>24) {
		debuglog(3, "ttv_set_thumb_fps: parameter (fps=%d) out of bounds", fps);
		return;
	}

	ttv_set_framerate(1,fps);

	debuglog(1, "ttv_set_thumb_fps: not implemented", 0);
}


int ttv_get_thumb_fps()
{
	if((int)resolution_defs[g_cfg.resolution_new].maxFramerate > 300)
	{
		return 300;
	}
	else
	{
		return  (int)resolution_defs[g_cfg.resolution_new].maxFramerate;
	}
}


/* This function stabilizes aewb.If aewb is not converged within max_num_frames it stops the stabilization process.
   For optimal performance switch to a mode with high fps and use this function.No frames will
   come out of capture driver during stabilization process */

void ttv_aewb_stabilize(int max_num_frames)
{

   /*this function try to stabilize aewb*/
     Vcam_2AStabilization(max_num_frames);
}

static void ttv_set_timelapse_bitrate_params (int maxbitrate, int vbrduration)
{
	EncLink_ChBitRateParams bitrateParams;
        EncLink_ChCVBRDurationParams vbrDurationParams;
	bitrateParams.chId = 0;
	debuglog(5, "%s : Bitrate=%d\n", __func__, maxbitrate);

	/* New bitrate value */
	bitrateParams.maxBitRate = maxbitrate;
	System_linkControl(gVencModuleContext.encId, ENC_LINK_CMD_SET_CODEC_MAX_BITRATE,
	    &bitrateParams, sizeof(bitrateParams), TRUE);

	/* VBR Duration value */
	vbrDurationParams.chId = 0;
	vbrDurationParams.vbrDuration = vbrduration;
	System_linkControl(gVencModuleContext.encId, ENC_LINK_CMD_SET_CODEC_VBRD,
                    &vbrDurationParams, sizeof(vbrDurationParams), TRUE);

}

/*
 * The function ttv_take_snapshot() should not modify the video recording or transcoding
 * variables. If it modifies, it should restore the old value before exiting the function
 */

bool ttv_take_snapshot(int number, ttv_frame_type_t CodecType, ttv_resolution_t resolution, int qp, bool first_frame, int IDR_interval)
{
	ttv_stream_t prev_stream;
	ttv_mode_t oldmode;
	ttv_resolution_t aewbstab_resolution;
	VENC_CHN_DYNAMIC_PARAM_S	*pDynPrm;
	VENC_CHN_PARAMS_S               *pChPrm;
	int old_encoder_framerate;
	bool viewfinder_on;
	bool result = true;

	if(number<1 || number>10) {
		debuglog(3, "ttv_take_snapshot: parameter (num=%d) out of bounds", number);
		return false;
	}
	if (IDR_interval <= 0 || IDR_interval > resolution_defs[resolution].maxFramerate) {
		debuglog(3, "ttv_take_snapshot: parameter (IDR_interval=%d) out of bounds", number);
		return false;
	}
	OSA_mutexLock(&g_cfg.apiaccess);

	pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[0];
	pDynPrm     = &pChPrm->dynamicParam;
	old_encoder_framerate = g_cfg.encoder_framerate;

	/*
	 * We want to start both the primary and secondary channel for still
     * image.
	 * Primary for HQ jpeg image.
	 * Secondary for LQ images (thumbnails).
	 * g_cfg.started contains information about the streams which are
	 * started.
	 *
	 * abort if both the streams are started; it means the recording
	 * mode is on
	 * abort if transcoding is in progress
	 */
	if (g_cfg.started & TTV_STREAM_PRI) {
		debuglog(3, "ttv_take_snapshot:  recording or transcoding is in progress\n",0);
		result = false;
		goto cleanup;
	}

	if (!g_cfg.started) {
		Vcam_start();
	}

	if (g_cfg.started & TTV_STREAM_MJPG) {
		viewfinder_on = true;
	}
	else {
		viewfinder_on = false;
	}

	/*Backup current context*/
	prev_stream = g_cfg.started;
	oldmode = g_cfg.mode;

	/*Stop both streams*/
	_stop_stream(TTV_STREAM_ALL);


	if(CodecType == TTV_TYPE_H264HP)
        {
                Vcam_enterTimelapseMode(first_frame);
                ttv_set_timelapse_bitrate_params(TTV_ENC_TIMELAPSE_MAX_BITRATE, TTV_ENC_TIMELAPSE_VBR_DURATION);
        }

	/*Select a lower framerate preview for timelapse if ae_time is greater than 100ms*/
	if((g_cfg.ae_time > 100) && (CodecType == TTV_TYPE_H264HP))
	{
		aewbstab_resolution = TTV_RES_WVGA_5_WIDE;
	}
	else
	{
		aewbstab_resolution = TTV_RES_WVGA_50_WIDE;
	}

	Vcam_enterStillCapture( resolution_defs[aewbstab_resolution].inWidth,
			resolution_defs[aewbstab_resolution].inHeight,
			resolution_defs[aewbstab_resolution].maxFramerate/10,
			resolution_defs[resolution].maxFramerate/10);

	/* Reset the sem count to zero */
	OSA_semWait(&g_cfg.stillCapDoneSem,OSA_TIMEOUT_NONE);

	g_cfg.stillCapNum            = number;
	g_cfg.stillCapHighQualityCnt = 0;
	g_cfg.stillCapLowQualityCnt  = 0;

	g_cfg.encoder_framerate = resolution == TTV_RES_4K_15_WIDE ? 15 : 30; /* set encoder framerate for timelapse */

	ttv_mode(TTV_MODE_STILL);

	/* connect the camera output to mjpeg/iva encoder link */
	ttv_changeMode(TTV_MODE_STILL, CodecType);

	if(CodecType == TTV_TYPE_H264HP)
	{
		if (first_frame == TRUE)
		{
			pDynPrm->intraFrameInterval = IDR_interval;
			Venc_setDynamicParam(0, 0, pDynPrm, VENC_IPRATIO);
			/*Reset rate control*/
			ttv_reset_encoder(TTV_TYPE_H264HP);
		}
	}
	else
	{
		if(qp == -1 || qp > 99)
			qp = TTV_JPEG_DEFAULT_QP;
		pDynPrm->qpInit = qp;
		/* to set quality factor, rateControl should be disalbed,
		 * it will use qpInit as the quality factor */
		pDynPrm->rateControl = 2; //IJPEGVENC_RATECONTROL_DISABLE
		Venc_setDynamicParam(1, 0, pDynPrm, VENC_QPVAL_I);
		Venc_setDynamicParam(1, 0, pDynPrm, VENC_RATECONTROL);
	}

	/*Switch to lower resolution for aewb stabilize if viewfinder off/still capture*/
	if((viewfinder_on == false) || (CodecType == TTV_TYPE_MJPEG))
	{
		ttv_set_recording_mode(aewbstab_resolution);
	}

	/*Clear fifo*/
	ttv_fifo_clear();

	/*Enable both channels*/
	ttv_enable_channel(TTV_CHANNEL_PRIMARY, true, false);
	ttv_enable_channel(TTV_CHANNEL_SECONDARY, true, false);

	/*
	 * still capture related calls
	 */

	/*Stabilize AEWB if viewfinder off/still capture*/
	if((viewfinder_on == false) || (CodecType == TTV_TYPE_MJPEG))
	{
		if((g_cfg.ae_time > 100) && (CodecType == TTV_TYPE_H264HP))
		{
			/*As the preview frame rate is low reduce the maximum number of frames for stabilization to 20
			 *if ae_time is greater than 100ms
			 */
			Vcam_2AStabilization(20);
		}
		else
		{
			Vcam_2AStabilization(30);
		}
	}

	/*Switch to required resolution */
	ttv_set_recording_mode(resolution);

	/*Set bitrate based on resolution*/
	if((CodecType == TTV_TYPE_H264HP) && (first_frame == TRUE))
	{
		ttv_bitrate(g_cfg.bitrate);
	}

	ttv_change_powermode(g_cfg.mode, TTV_RES_NONE);

	Vcam_startStillCapture(0,0,0,0,0,0,FALSE,number);

	/*
	 * wait until required number of frames are written into fifo
	 */
	OSA_semWait(&g_cfg.stillCapDoneSem,OSA_TIMEOUT_FOREVER);

	g_cfg.stillCapNum            = 0;
	g_cfg.stillCapHighQualityCnt = 0;
	g_cfg.stillCapLowQualityCnt  = 0;

	Vcam_exitStillCapture();

        if(CodecType == TTV_TYPE_H264HP)
        {
                Vcam_exitTimelapseMode();
                ttv_set_timelapse_bitrate_params(TTV_ENC_RECORDING_MAX_BITRATE, TTV_ENC_RECORDING_VBR_DURATION);
        }

	ttv_change_powermode(TTV_MODE_NONE, TTV_RES_NONE);

	/*Disable both channels*/
	ttv_enable_channel(TTV_CHANNEL_PRIMARY, false, false);
	ttv_enable_channel(TTV_CHANNEL_SECONDARY, false, false);

	/*Restore previous context*/
	g_cfg.encoder_framerate = old_encoder_framerate;

	/*Invalidate the current sensor mode to force a mode switch*/
	g_cfg.resolution = TTV_RES_NONE;

	ttv_mode(oldmode);
	_start_stream(prev_stream);
cleanup:
	OSA_mutexUnlock(&g_cfg.apiaccess);
	return result;
}


bool ttv_set_feature(ttv_feature_t feature, int val)
{
	if (g_cfg.mode == TTV_MODE_NONE) {
		debuglog(4,"Unable to set feature value when mode is set to TTV_MODE_NONE",0);
		return false;
	}

	VCAM_CHN_DYNAMIC_PARAM_S params = {0};
	VCAM_PARAMS_E pe = 0;

	switch(feature)
	{
	case TTV_IMG_SATURATION:
		if(val<0 || val>255) return false;
		params.saturation = g_cfg.saturation = val;
		pe = VCAM_SATURATION;
		debuglog(5,"saturation is set to %d",val);
		break;
	case TTV_IMG_BRIGHTNESS:
		if(val<0 || val>255) return false;
		params.brightness = g_cfg.brightness = val;
		pe = VCAM_BRIGHTNESS;
		debuglog(5,"brightness is set to %d",val);
		break;
	case TTV_IMG_CONTRAST:
		if(val<0 || val>255) return false;
		params.contrast = g_cfg.contrast = val;
		pe = VCAM_CONTRAST;
		debuglog(5,"contrast is set to %d",val);
		break;
	case TTV_IMG_HUE:
		if(val<0 || val>255) return false;
		params.hue = g_cfg.hue = val;
		pe = VCAM_HUE;
		debuglog(5,"hue is set to %d",val);
		break;
	case TTV_IMG_SHARPNESS:
		if(val<0 || val>255) return false;
		params.sharpness = g_cfg.sharpness = val;
		pe = VCAM_SHARPNESS;
		debuglog(5,"sharpness is set to %d",val);
		break;
	case TTV_IMG_AE:
		if(val) {
			params.aewbMode = (g_cfg.aewbmode |= 1);
		} else {
			params.aewbMode = (g_cfg.aewbmode &= 2);
		}
		pe = VCAM_AEWB_MODE;
		debuglog(5,"aewb is set to %d",val);
		break;
	case TTV_IMG_AWB:
		if(val) {
			params.aewbMode = (g_cfg.aewbmode |= 2);
		} else {
			params.aewbMode = (g_cfg.aewbmode &= 1);
		}
		pe = VCAM_AEWB_MODE;
		debuglog(5,"aewb is set to %d",val);
		break;
	case TTV_IMG_WB_MODE:
		if(val<0 || val>=TTV_WB_MAX) return false;
		params.awbMode = g_cfg.wb_mode = val;
		pe = VCAM_AWBMODE;
		debuglog(5,"wb mode is set to %d",val);
		break;
	case TTV_IMG_AE_METERING:
		if(val<0 || val>=TTV_AE_MAX) return false;
		params.aeMetering = g_cfg.ae_metering = val;
		pe = VCAM_AE_METERING;
		debuglog(5,"ae metering is set to %d",val);
		break;
	case TTV_IMG_EV_COMPENSATION:
		if(val<-200 || val> 200) return false;
		params.evCompensation = g_cfg.ev_compensation = val;
		pe = VCAM_EV_COMPENSATION;
		debuglog(5,"ev compensation is set to %d",val);
		break;
	/*sets maximum exposure for still image and timelapse in milliseconds*/
	case TTV_IMG_AE_TIME:
		if(val<0 || val>400) return false;
		params.ae_time = g_cfg.ae_time = val;
		pe = VCAM_AE_TIME;
		debuglog(5,"maximum capture exposure is set to %d ms",val);
		break;
	default:
		debuglog(2,"unknown feature %d (value to set %d)",(int)feature,val);
		return false;
	}
	OSA_mutexLock(&g_cfg.apiaccess);
	Vcam_setDynamicParamChn(0, &params, pe);
	OSA_mutexUnlock(&g_cfg.apiaccess);
	return true;
}


bool ttv_get_feature(ttv_feature_t feature, int *val)
{
	if (!val) {
		debuglog(2,"value argument is null",0);
		return false;
	}

	if(g_cfg.mode == TTV_MODE_NONE) {
		debuglog(4,"Unable to get feature value when mode is set to TTV_MODE_NONE",0);
		return false;
	}

	switch(feature)
	{
	case TTV_IMG_SATURATION:
		*val = g_cfg.saturation;
		debuglog(5,"saturation is currently set to %d", *val);
		break;
	case TTV_IMG_BRIGHTNESS:
		*val = g_cfg.brightness;
		debuglog(5,"brightness is currently set to %d", *val);
		break;
	case TTV_IMG_CONTRAST:
		*val = g_cfg.contrast;
		debuglog(5,"contrast is currently set to %d", *val);
		break;
	case TTV_IMG_HUE:
		*val = g_cfg.hue;
		debuglog(5,"hue is currently set to %d", *val);
		break;
	case TTV_IMG_SHARPNESS:
		*val = g_cfg.sharpness;
		debuglog(5,"sharpness is currently set to %d", *val);
		break;
	case TTV_IMG_AE:
		*val = (g_cfg.aewbmode & 1) == 0 ? 0 : 1;
		debuglog(5,"aewb is currently set to %d", *val);
		break;
	case TTV_IMG_AWB:
		*val = (g_cfg.aewbmode & 2) == 0 ? 0 : 1;
		debuglog(5,"aewb is currently set to %d", *val);
		break;
	case TTV_IMG_WB_MODE:
		*val = g_cfg.wb_mode;
		debuglog(5,"wb mode is currently set to %d", *val);
		break;
	case TTV_IMG_AE_METERING:
		*val = g_cfg.ae_metering;
		debuglog(5,"ae metering is currently set to %d", *val);
		break;
	case TTV_IMG_EV_COMPENSATION:
		*val = g_cfg.ev_compensation;
		debuglog(5,"ev compensation is currently set to %d", *val);
		break;
	/*sets maximum exposure for still image and timelapse in milliseconds*/
	case TTV_IMG_AE_TIME:
		*val = g_cfg.ae_time;
		debuglog(5,"maximum capture exposure is currently set to %d", *val);
		break;
	default:
		debuglog(2,"unknown feature %d",(int)feature);
		return false;
	}
	return true;
}


unsigned int ttv_get_timestamp()
{
	return Vcam_getCurVpssM3Time();
}


unsigned int ttv_get_bitrate()
{
	return g_cfg.bitrate * resolution_defs[g_cfg.resolution].defaultBitrate / 100;
}


#include "mcfw/src_linux/mcfw_api/ti_vdec_priv.h"
extern VDEC_MODULE_CONTEXT_S gVdecModuleContext;

void *ttv_request_decode_frame(unsigned int len)
{
	IpcBitsOutLinkHLOS_BitstreamBufReqInfo ipcReqInfo = {0};

	if(g_cfg.mode != TTV_MODE_TRANSCODE) {
		debuglog(1, "ttv_request_decode_frame called but mode is not transcode",0);
		return 0;
	}

	OSA_mutexLock(&g_cfg.apiaccess);

	if(g_cfg.emptyBitsBufList.numBufs) {
		debuglog(3, "ttv_request_decode_frame called but there is already decode frame allocated!",0);
		if(len > g_cfg.emptyBitsBufList.bufs[0]->bufSize) {
			IpcBitsInLink_putEmptyVideoBitStreamBufs(gVdecModuleContext.ipcBitsOutHLOSId, &g_cfg.emptyBitsBufList);
			g_cfg.emptyBitsBufList.numBufs = 0;
		}
	}
	memset(&g_cfg.emptyBitsBufList, 0, sizeof(g_cfg.emptyBitsBufList));

	ipcReqInfo.numBufs = 1;
	ipcReqInfo.minBufSize[0] = len;
	debuglog(5,"requesting frame of size: %lu", ipcReqInfo.minBufSize[0]);

        IpcBitsOutLink_getEmptyVideoBitStreamBufs(gVdecModuleContext.ipcBitsOutHLOSId, &g_cfg.emptyBitsBufList, &ipcReqInfo, TIMEOUT_FOREVER);
	OSA_mutexUnlock(&g_cfg.apiaccess);
	if(!g_cfg.emptyBitsBufList.numBufs) debuglog(3,"request decode frame: numBufs == 0",0);
	return (g_cfg.emptyBitsBufList.numBufs ? g_cfg.emptyBitsBufList.bufs[0]->addr : 0);
}


void ttv_put_frame(void *frame, unsigned int filllen, unsigned int pts, bool lastframe)
{
	if(g_cfg.mode != TTV_MODE_TRANSCODE) {
		debuglog(1, "ttv_request_decode_frame called but mode is not transcode",0);
		return;
	}

	if(!g_cfg.emptyBitsBufList.numBufs || g_cfg.emptyBitsBufList.bufs[0]->bufSize<filllen || g_cfg.emptyBitsBufList.bufs[0]->addr!=frame) {
		debuglog(1, "ttv_put_frame wants to put unknown block of memory?! (numbufs=%u, size=%u, len=%u, addr%sframe)", g_cfg.emptyBitsBufList.numBufs, g_cfg.emptyBitsBufList.bufs[0]->bufSize, filllen, (g_cfg.emptyBitsBufList.bufs[0]->addr==frame?"==":"!="));
		return;
	}

	if(!filllen) {
		IpcBitsInLink_putEmptyVideoBitStreamBufs(gVdecModuleContext.ipcBitsOutHLOSId, &g_cfg.emptyBitsBufList);
		g_cfg.emptyBitsBufList.numBufs = 0;
		return;
	}

	OSA_mutexLock(&g_cfg.apiaccess);
	g_cfg.emptyBitsBufList.bufs[0]->channelNum = 0;
	g_cfg.emptyBitsBufList.bufs[0]->fillLength = filllen;
	g_cfg.emptyBitsBufList.bufs[0]->startOffset = 0;
	g_cfg.emptyBitsBufList.bufs[0]->timeStamp = pts;
	g_cfg.emptyBitsBufList.bufs[0]->isEOS = (lastframe ? 1 :0);
	g_cfg.emptyBitsBufList.numBufs = 1;
	IpcBitsOutLink_putFullVideoBitStreamBufs(gVdecModuleContext.ipcBitsOutHLOSId, &g_cfg.emptyBitsBufList);
	g_cfg.emptyBitsBufList.numBufs = 0;
	OSA_mutexUnlock(&g_cfg.apiaccess);
}

void ttv_set_recording_mode(ttv_resolution_t resolution)
{
	float aspectRatio;
	g_cfg.framerate = resolution_defs[resolution].maxFramerate/10;
	g_cfg.resolution = resolution;

	VCAM_CHN_PARAM_S chPrms;
	chPrms.inputWidth = resolution_defs[resolution].inWidth;
	chPrms.inputHeight = resolution_defs[resolution].inHeight;
	chPrms.strmEnable[0] = TRUE;
	chPrms.strmEnable[1] = TRUE;
	chPrms.strmResolution[0].width = resolution_defs[resolution].outWidth;
	chPrms.strmResolution[0].height = resolution_defs[resolution].outHeight;
	aspectRatio = (float)resolution_defs[resolution].outWidth / resolution_defs[resolution].outHeight;
	/*
	 * check if the resolution is either 4:3 or 16:9
	 * if it is not, throw an assertion
	 */
	if (aspectRatio <= TTV_MAX_ASPECT_RATIO_FOR_4_3 &&
		aspectRatio >= TTV_MIN_ASPECT_RATIO_FOR_4_3) {
		chPrms.strmResolution[1].width = TTV_LQ_RES_WIDTH_640;
		chPrms.strmResolution[1].height = TTV_LQ_RES_HEIGHT_480;
	} else if (aspectRatio <= TTV_MAX_ASPECT_RATIO_FOR_16_9 &&
			aspectRatio >= TTV_MIN_ASPECT_RATIO_FOR_16_9) {
		chPrms.strmResolution[1].width = TTV_LQ_RES_WIDTH_768;
		chPrms.strmResolution[1].height = TTV_LQ_RES_HEIGHT_432;
	}
	else {
		/*
		 * Asserting here as the function cannot return any error code.
		 * This situation should never happen. The check is added to
		 * make sure that the video/image is encoded in proper resolution.
		 */
		debuglog(1, "Unsupported aspect ratio for selected resolution", 0);
		debuglog(1,"Resolution=%d OutWidth=%d OutHeight=%d\n",resolution,resolution_defs[resolution].outWidth,resolution_defs[resolution].outHeight);
		UTILS_assert(TTV_ASPECT_RATIO_ERROR);
	}
	chPrms.standard = resolution_defs[resolution].standard;

	Vcam_changeCaptMode(0, &chPrms);

	/*Set Camera and Encoder framerates*/
	ttv_set_framerate(0, g_cfg.framerate);

	/*WVGA MJPEG framerate will never be more than 30*/
	if(g_cfg.framerate >= 30)
		ttv_set_framerate(1, 30);
	else
		ttv_set_framerate(1, g_cfg.framerate);

	ttv_paramUpdate();

	g_cfg.is_fov_wide = resolution_defs[g_cfg.resolution].isFOVWide;
}

int ttv_getFifoStats()
{
	EncLink_FifoStats fifoInfo;

	fifoInfo.chId = 0;
	System_linkControl(SYSTEM_LINK_ID_VENC_0,ENC_LINK_CMD_GET_FIFO_STATS,&fifoInfo,sizeof(EncLink_FifoStats),TRUE);

	return(fifoInfo.fifoStats);

}

bool ttv_set_idr_frequency(int num_idr_per_second)
{
	int defaultFrameRate;

	defaultFrameRate = resolution_defs[g_cfg.resolution_new].maxFramerate/10;
	if (num_idr_per_second <= 0)
		g_cfg.num_idr_per_second = 1;
	else if (num_idr_per_second > defaultFrameRate) {
		debuglog(5, "ERROR : num_idr_per_second=%d is greater than framerate=%d\n", num_idr_per_second, defaultFrameRate);
		return false;
	}
	else {
		g_cfg.num_idr_per_second = num_idr_per_second;
	}

	return true;

}

#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
void ttv_enable_raw_capture (void)
{
	/* Allocate raw buffer if not done*/
	if ( gRawCaptureInfo.rawBuffer == NULL )
	{
		gRawCaptureInfo.rawBufferSize = LONGBEACH_MAX_SENSOR_OUT_WIDTH * LONGBEACH_MAX_SENSOR_OUT_HEIGHT * 4;

		Vsys_allocBuf(VSYS_SR1_SHAREMEM,
                  (gRawCaptureInfo.rawBufferSize + LONGBEACH_RAW_CAPTURE_PROPERTIES_SIZE),
                  32,
                  &gRawBuffer);
		if(gRawBuffer.virtAddr == NULL)
		{
				debuglog(3, "Raw buffer allocation failed!!", 0);
				return;
		}
		gRawCaptureInfo.rawBuffer = gRawBuffer.physAddr;
		gRawCaptureInfo.rawInfoAddressOffset = gRawCaptureInfo.rawBufferSize;
	}

	memset(gRawBuffer.virtAddr, 0, gRawCaptureInfo.rawBufferSize);
	/*Start raw capture*/
	System_linkControl(SYSTEM_LINK_ID_CAMERA,
											CAMERA_LINK_CMD_ENABLE_RAW_DUMP,
											&gRawCaptureInfo,
											sizeof(gRawCaptureInfo),
											TRUE);
}

void ttv_write_raw_buffer (int filenum, ttv_resolution_t resolution, bool dump_yuv, int cnt)
{
	char filename[64];
	unsigned int *buf;
	FILE *fRawDump, *fRawDumpInfo,*fYuvDump;
	unsigned int raw_width  = (resolution_defs[resolution].inWidth/16)*16;
	unsigned int raw_height = (resolution_defs[resolution].inHeight/2)*2;
	unsigned int yuv_width  = resolution_defs[resolution].outWidth;
	unsigned int yuv_height = resolution_defs[resolution].outHeight;

	char *basename = "/mnt/mmc/TTCamVideo";
	char suffix[5];
	sprintf(suffix, "_%04d", cnt);
	char *pSuffix = (cnt >= 0 && cnt < 10000) ? suffix : "";

	sprintf(filename,"%s%02d_GBRG_%dx%d_12bit%s.raw",basename,filenum,raw_width,raw_height,pSuffix);
	fRawDump = fopen (filename,"wb");
	if(fRawDump == NULL)
	{
		debuglog(3, "File open failed\n", 0);
		return;
	}
	fwrite((char *)gRawBuffer.virtAddr, 1,(raw_width*raw_height*2), fRawDump);
	fclose(fRawDump);

	if(dump_yuv == true)
	{
		sprintf(filename,"%s%02d_NV12_%dx%d%s.yuv",basename,filenum,yuv_width,yuv_height, pSuffix);
		fYuvDump = fopen (filename,"wb");
		if(fYuvDump == NULL)
		{
			debuglog(3, "File open failed\n", 0);
			return;
		}
		fwrite((char *)gRawBuffer.virtAddr + (gRawCaptureInfo.rawBufferSize/2), 1,
					(yuv_width*yuv_height*3)/2, fYuvDump);
		fclose(fYuvDump);
	}

	sprintf(filename,"%s%02d%s.rawinfo", basename, filenum, pSuffix);
	fRawDumpInfo = fopen (filename,"wb");
	if(fRawDumpInfo == NULL)
	{
		debuglog(3, "File open failed\n", 0);
		return;
	}
	buf = (unsigned int*)((char *)gRawBuffer.virtAddr +
												gRawCaptureInfo.rawInfoAddressOffset);
  printf("raw : %p buf : %p ofst: %x",gRawBuffer.virtAddr, buf, gRawCaptureInfo.rawInfoAddressOffset);
	fprintf(fRawDumpInfo, "Exposure Time : %u\n", *buf);
	buf++;
	fprintf(fRawDumpInfo, "Analog gain   : %u\n", *buf);
	buf++;
	fprintf(fRawDumpInfo, "Digital gain  : %u\n", *buf);
	buf++;
	fprintf(fRawDumpInfo, "Colour temp   : %u\n", *buf);
	buf++;
	fprintf(fRawDumpInfo, "Aweb rGain    : %u\n", *buf);
	buf++;
	fprintf(fRawDumpInfo, "Aweb grGain   : %u\n", *buf);
	buf++;
	fprintf(fRawDumpInfo, "Aweb gbGain   : %u\n", *buf);
	buf++;
	fprintf(fRawDumpInfo, "Aweb bGain    : %u\n", *buf);
	fclose(fRawDumpInfo);
}

void ttv_set_manual_iso_gain(unsigned int iso_gain)
{
        System_linkControl(SYSTEM_LINK_ID_CAMERA,
                                     CAMERA_LINK_CMD_SET_MANUAL_ISO_GAIN,
                                     &iso_gain,
                                     sizeof(iso_gain),
                                     TRUE);

}

#endif

#endif
