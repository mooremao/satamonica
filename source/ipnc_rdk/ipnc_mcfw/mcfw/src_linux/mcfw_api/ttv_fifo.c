// TomTom Video platform API
// FIFO functions
// Nebojsa Sumrak 2014.

#ifdef PLATFORM_LIB

#include "tt_video_priv.h"
#include <osa.h>
#include <osa_dma.h>

/*Max number of outframes is decided based on fifo size
  Current Fifo Size = 100MBytes
  Lets take the worst case scenario
  WVGA 180 fps @ 16 Mbps can run for
  (100 * 8 MBits) / (16 Mbits /s) = 50 seconds
  So 180 fps * 50 secs = 9000 frames can be accomodated in the fifo
  Since the bitrate is VBR, we will add few buffer headers extra
  and hence keeping it as 10000
  Adding the secondary channel buffer header of 2000

  Extra 16 buffer headers are allocated for EOS headers
*/
#define FIFO_SIZE	(12000 + 16)

static struct {
	void *address;
	Bitstream_Buf *bitbuf;
	bool released;
} fifo_buf[FIFO_SIZE];

static volatile int fifo_towrite = 0, fifo_toread = 0, fifo_torelease = 0;
static volatile unsigned long fifo_num_written = 0, fifo_num_read = 0, fifo_num_released = 0, fifo_num_dropped = 0, fifoAvailability = 0;


void ttv_fifo_clear()
{
	debuglog(5,"ttv_fifo_clear",0);
	OSA_mutexLock(&g_cfg.fifo_waccess);
	// release remaining buffers
	while(fifo_torelease != fifo_towrite)
	{
		ttv_putEmptyBitstreamBuf(fifo_buf[fifo_torelease].bitbuf);
		fifo_buf[fifo_torelease].address = NULL;
		if(++fifo_torelease >= FIFO_SIZE)
			fifo_torelease = 0;
	}

	fifo_towrite = fifo_toread = fifo_torelease = 0;
	fifo_num_written = fifo_num_read = fifo_num_released = fifo_num_dropped = fifoAvailability = 0;
	OSA_mutexUnlock(&g_cfg.fifo_waccess);
}


void ttv_fifo_init()
{
	debuglog(5,"ttv_fifo_init",0);
	fifo_towrite = fifo_toread = fifo_torelease = 0;
	fifo_num_written = fifo_num_read = fifo_num_released = fifo_num_dropped = fifoAvailability = 0;
	OSA_semCreate(&g_cfg.bitsInNotifySem, MAX_PENDING_RECV_SEM_COUNT, 0);
	OSA_semCreate(&g_cfg.stillCapDoneSem, 1, 0);
	OSA_mutexCreate(&g_cfg.fifo_waccess);
}


void ttv_fifo_deinit()
{
	debuglog(5,"ttv_fifo_deinit",0);
	ttv_fifo_clear();
	OSA_semDelete(&g_cfg.bitsInNotifySem);
	OSA_semDelete(&g_cfg.stillCapDoneSem);
	OSA_mutexDelete(&g_cfg.fifo_waccess);
}


bool ttv_fifo_write(Bitstream_Buf *bitbuf)
{
	debuglog(5,"addr=%lx off=%x size=%x",bitbuf->addr, bitbuf->startOffset, bitbuf->fillLength);
	OSA_mutexLock(&g_cfg.fifo_waccess);
	if(bitbuf->codingType != TTV_TYPE_MJPEG)
		fifoAvailability = bitbuf->fifoStats;

	// check for full fifo
	int tw = fifo_towrite + 1;
	if(tw >= FIFO_SIZE)
		tw = 0;
	if(tw == fifo_torelease) {
		fifo_num_dropped++;
		ttv_putEmptyBitstreamBuf(bitbuf);
		OSA_mutexUnlock(&g_cfg.fifo_waccess);
		return false;
	}

	fifo_buf[fifo_towrite].address = (void*)(bitbuf->addr + bitbuf->startOffset);
	fifo_buf[fifo_towrite].bitbuf = bitbuf;
	fifo_buf[fifo_towrite].released = false;
	fifo_towrite = tw;
	fifo_num_written++;
	OSA_semSignal(&g_cfg.bitsInNotifySem);
	OSA_mutexUnlock(&g_cfg.fifo_waccess);
	return true;
}


bool ttv_get_frame(ttv_frame_t *frame)
{
	debuglog(5,"ttv_get_frame",0);
	OSA_mutexLock(&g_cfg.fifo_waccess);
	if(fifo_toread==fifo_towrite) {
		OSA_mutexUnlock(&g_cfg.fifo_waccess);
		return false;
	}
	Bitstream_Buf *bitbuf = fifo_buf[fifo_toread].bitbuf;
	frame->address = fifo_buf[fifo_toread].address;
	frame->size = bitbuf->fillLength;
	frame->pts = bitbuf->timeStamp;
	frame->channel = (char)bitbuf->channelNum;
	frame->type = (char)bitbuf->codingType;
	frame->iskey = (char)bitbuf->isKeyFrame;
	frame->status = (bitbuf->isEOS ? TTV_STATUS_EOS : 0);
	frame->exposuretime = bitbuf->exposureTime;
	frame->isogain = bitbuf->isoGain;
	if(++fifo_toread >= FIFO_SIZE)
		fifo_toread = 0;
	fifo_num_read++;
	OSA_mutexUnlock(&g_cfg.fifo_waccess);
	return true;
}


void ttv_release_frame(void *addr)
{
	if(g_cfg.mode == TTV_MODE_NONE)
		return;
	debuglog(5,"ttv_release_frame",0);
	OSA_mutexLock(&g_cfg.fifo_waccess);
	if(fifo_buf[fifo_torelease].address == addr) {
		do {
			ttv_putEmptyBitstreamBuf(fifo_buf[fifo_torelease].bitbuf);
			if(++fifo_torelease >= FIFO_SIZE)
				fifo_torelease = 0;
			fifo_num_released++;
		} while(fifo_torelease!=fifo_toread && fifo_buf[fifo_torelease].released);
	} else {
		int i;
		debuglog(5,"ttv_release - not next one",0);
		for(i = fifo_torelease; i != fifo_toread; ) {
			if(fifo_buf[i].address == addr) {
				fifo_buf[i].released = true;
				OSA_mutexUnlock(&g_cfg.fifo_waccess);
				return;
			}
			if(++i >= FIFO_SIZE)
				i=0;
		}
		debuglog(2,"ttv_release: frame not found!",0);
	}
	OSA_mutexUnlock(&g_cfg.fifo_waccess);
}


bool ttv_wait_for_frame()
{
	debuglog(5,"ttv_wait_for_frame",0);
	if(g_cfg.mode == TTV_MODE_NONE || g_cfg.want_to_change_mode)
		return false;
	g_cfg.waiting_for_frame = true;
	bool ret = (OSA_semWait(&g_cfg.bitsInNotifySem, -1) == OSA_SOK);
	if(g_cfg.want_to_change_mode)
		ret = false;
	g_cfg.waiting_for_frame = false;
	return ret;
}

// The following function is set inline in order to avoid unused function warning by the compiler.
// Since warnings are treated as errors, this breaks the build.
// Removed from API as obsolete, but left here as static for possible further debug use.
inline static void ttv_get_fifo_stats(unsigned long *written, unsigned long *read, unsigned long *released, unsigned long *dropped)
{
	if(written)
		*written = fifo_num_written;
	if(read)
		*read = fifo_num_read;
	if(released)
		*released = fifo_num_released;
	if(dropped)
		*dropped = fifo_num_dropped;
}


int ttv_get_fifo_fill()
{
	return ttv_getFifoStats();
}

#endif
