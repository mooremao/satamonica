/*!    \file   ttv.h`
// TomTom Video platform API
// External Definitions
// Nebojsa Sumrak 2014.
*/

#ifndef TTV_H_
#define TTV_H_

#include <stdbool.h>

#if defined (__cplusplus)
extern "C" {
#endif

typedef enum {
	TTV_MODE_NONE = 0,
	TTV_MODE_VIDEO,
	TTV_MODE_STILL,
	TTV_MODE_TRANSCODE,
} ttv_mode_t;


typedef enum {
        TTV_RES_1080_60_WIDE = 0,
        TTV_RES_1080_60_NORMAL,
        TTV_RES_1080_50_WIDE,
        TTV_RES_1080_50_NORMAL,
        TTV_RES_1080_30_WIDE,
        TTV_RES_1080_30_NORMAL,
        TTV_RES_1080_25_WIDE,
        TTV_RES_1080_25_NORMAL,
        TTV_RES_4K_15_WIDE,
        TTV_RES_4K_12_5_WIDE,
        TTV_RES_2_7K_30_WIDE,
        TTV_RES_2_7K_25_WIDE,
        TTV_RES_2_7K_25_NORMAL,
        TTV_RES_720_120_WIDE,
        TTV_RES_720_120_NORMAL,
        TTV_RES_720_100_WIDE,
        TTV_RES_720_100_NORMAL,
        TTV_RES_720_60_WIDE,
        TTV_RES_720_60_NORMAL,
        TTV_RES_720_50_WIDE,
        TTV_RES_720_50_NORMAL,
        TTV_RES_WVGA_180_WIDE,
        TTV_RES_WVGA_150_WIDE,
        TTV_RES_WVGA_30_WIDE,
        TTV_RES_16MP_STILL,
        TTV_RES_8MP_STILL,
        TTV_RES_16MP_5_BURST,
        TTV_RES_8MP_5_BURST,
        TTV_RES_WVGA_30_NORMAL,
        TTV_RES_WVGA_50_WIDE,
        TTV_RES_4K_14_WIDE,
        TTV_RES_WVGA_5_WIDE,

        TTV_RES_NONE,
        TTV_RES_MAX
} ttv_resolution_t;

typedef enum {
	TTV_VRES_480P = 0,
	TTV_VRES_720P,
	TTV_VRES_1080P,
	TTV_VRES_2_7K,
	TTV_VRES_4K,
} ttv_video_res_t;

typedef enum {
	TTV_IMG_SATURATION=0,
	TTV_IMG_BRIGHTNESS,
	TTV_IMG_CONTRAST,
	TTV_IMG_HUE,
	TTV_IMG_SHARPNESS,
	TTV_IMG_AE,
	TTV_IMG_AWB,
	TTV_IMG_WB_MODE,
	TTV_IMG_AE_METERING,
	TTV_IMG_EV_COMPENSATION,
	TTV_IMG_AE_TIME,
	TTV_FEATURE_MAX,
} ttv_feature_t;

/* Any modification to this enum requires    *
 * a similar change in aewb_xdm.h at M3 side */
typedef enum {
	TTV_AE_CENTER = 0,
	TTV_AE_NORMAL,
	TTV_AE_WIDE,
	TTV_AE_MAX,
} ttv_ae_metering_mode_t;

/* Any modification to this enum requires    *
 * a similar change in aewb_xdm.h at M3 side */
typedef enum {
	TTV_WB_AUTO = 0,
	TTV_WB_7500K,
	TTV_WB_5000K,
	TTV_WB_4000K,
	TTV_WB_3000K,
	TTV_WB_2000K,
	TTV_WB_RED_FILTER,
	TTV_WB_MAGENTA_FILTER,
	TTV_WB_MAX
} ttv_wb_mode_t;

// Wakeup from deepsleep takes ~370 ms. Using a maximum value of 500ms.
#define MAX_SYSTEM_WAKEUP_TIME_IN_MS 500

typedef enum {
	TTV_WAKE_OTHER = 0,               // < Wakeup for unknown reason.
	TTV_WAKE_BUTTON,                  // < Wakeup from button.
	TTV_WAKE_TIMER,	                  // < Wakeup from timer; successfully slept for requested duration.
	TTV_WAKE_BLE,                     // < Wakeup from BLE (e.g. remote control button press).
	TTV_SLEEP_MORE,                   // < Woke up before requested timeout; please do a software sleep for the remainder.
	TTV_WAKE_EXT_MIC,                 // < External mic detection triggered wakeup (please ignore and go back to sleep).
	TTV_WAKE_POWER_PRN,               // < Power present change triggered wakeup (insertion/removal of charging cable).
	TTV_WAKE_BATT_TEMP,               // < Battery temperature triggered wakeup (could be a reason for not charging).
	TTV_WAKE_SOC_BATT_LOW,            // < Battery capacity becoming low.
	TTV_WAKE_SOC_TURN_OFF,            // < FG indicates a critical low battery. But FG might be wrong, so check capacity level separately.
	TTV_SLEEP_ERROR_INVALID_TIMEOUT,  // < Invalid timeout argument given. Valid values are -1 to approx. 65,000,000, but please don't use -1.
	TTV_SLEEP_ERROR_INTERNAL,         // < Failed to enter deep sleep for some internal reason. Wait a second and try again.
	TTV_SLEEP_ERROR_BLE_STATE, 	  // < BLE stack not currently able to go to sleep. Wait a sec and retry.
	TTV_SLEEP_ERROR_TIMEOUT_TOO_SHORT, // < Timeout is too short. Lowest useful timeout value is about 1500ms; if you need shorter, do a software sleep.
	TTV_SLEEP_ERROR_WOKEN_UP // < Failed to enter deep sleep because someone woke the system up in the meantime.

} ttv_wakeup_t;

// encoded frame types - TT supported
typedef enum {
	TTV_TYPE_H264BP = 7,      // < Video format is H.264 stream, Base Profile
	TTV_TYPE_H264MP = 8,      // < Video format is H.264 stream, Main Profile
	TTV_TYPE_H264HP = 9,      // < Video format is H.264 stream, High Profile
	TTV_TYPE_MJPEG = 24       // < Video format is motion JPEG
} ttv_frame_type_t;

// Capture orientation
typedef enum {
	TTV_ROTATION_UPSIDE_DOWN,
	TTV_ROTATION_RIGHT_WAY_UP
} ttv_rotation_t;


typedef unsigned ttv_stream_t;

#define TTV_STREAM_PRI		1
#define TTV_STREAM_MJPG		2
#define TTV_STREAM_ALL		0xFF

/*
 * bitstream buffer channel mapping...
 * The FIFO output will report the channel number as one of the below.
 * The stream parameters defined above are used heavily through the code.
 * Touching them would be dangerous. SOOO we choose to use these channel values instead */
#define TTV_CHANNEL_H264		0	//H264 stream
#define TTV_CHANNEL_JPEG_FULL		1	//Still Capture JPEG
#define TTV_CHANNEL_JPEG_VGA		2	//LQ image corresponding to Full resolution JPEG
#define TTV_CHANNEL_JPEG_WVGA		3	//WVGA: secondary stream MJPEG video

typedef struct ttv_frame_s {
	void *address;
	unsigned int size;
	unsigned long pts;
	unsigned char type;
	unsigned char channel;
	unsigned char iskey;
	unsigned char status;
	unsigned int exposuretime;
	unsigned int isogain;
} ttv_frame_t;

// resolution data table -- HAS TO BE EQUAL TO ttv_resolution_t ENTRIES
typedef struct ttv_res_def_s{
	unsigned int standard;
	unsigned int inWidth;
	unsigned int inHeight;
	unsigned int outWidth;
	unsigned int outHeight;
	unsigned int outPitch;
	unsigned int maxFramerate;
	unsigned int defaultBitrate;
	unsigned int isFOVWide;
} ttv_res_def_t;

#define TTV_STATUS_EOS	0x80
#define TTV_STATUS_RELEASED	0x1


bool ttv_mode(ttv_mode_t mode);
// Starts video pipeline.
bool ttv_start(ttv_stream_t stream);
// Sets orientation
bool ttv_set_sensor_rotation(ttv_rotation_t r);
// Get info on started streams
ttv_stream_t ttv_get_started();
// Stops video capture.
void ttv_stop(ttv_stream_t stream);
// Configures main HQ video channel for specified h264 (VIDEO) or JPG (STILL) destination resolution and frame rate.
bool ttv_configure(ttv_resolution_t resolution, int framerate);
// Configures decoder in TRANSCODE mode.
bool ttv_config_transcode(ttv_video_res_t inputres, int inputfps, ttv_video_res_t outputres, int outputfps, ttv_frame_type_t outputenc);
// Select encoding bitrate for main HQ video channel (in VIDEO and TRANSCODE mode).
void ttv_bitrate(int brate);
// Return approximate current bitrate
unsigned int ttv_get_bitrate();
// Select LQ thumbnail channel desired frame rate.
void ttv_set_thumb_fps(unsigned fps);
// Retrieves secondary (LQ thumbnail) channel frame rate.
int ttv_get_thumb_fps();
// Creates HQ snapshot(s). of desired codec type
// Parameters : number     = Number of still shots required.
//              CodecType  = H264 (for timelapse) or MJPEG (for still image shots).
//              resolution = Required resolution for the snapshot.
//              qp         = Quality factor ranges from 2 to 99.
//                           If set to -1; it will take the default qp value (90).
//              first_frame= Set it to true for the first frame in timelpase video.
//                           This paramter is not used for still capture.
//              IDR_interval=Interval of IDR frames occuring out of the total number of frames.
//                           If set to one, every frame is IDR, if set to 30, every 30th frame is IDR.
//...........................Valid values range is from 1 to framerate of the resolution chosen (30 for 1080_30, 15 for 4k_15, etc.)
bool ttv_take_snapshot(int number, ttv_frame_type_t CodecType, ttv_resolution_t resolution, int qp, bool first_frame, int IDR_interval);
// Aborts transcoding
void ttv_abort_transcode();
// Checks/waits for encoded frame.
bool ttv_wait_for_frame();
// Retrieve encoded frame from the buffer.
bool ttv_get_frame(ttv_frame_t *);
// Release frame got from ttv_get_frame
void ttv_release_frame(void *addr);
// Request frame from decoder buffer for transcode operation
void *ttv_request_decode_frame(unsigned int len);
// Give frame to decoder for transcode operation.
void ttv_put_frame(void *frame, unsigned int filllen, unsigned int pts, bool lastframe);
// Turn or setup image processing features.
bool ttv_set_feature(ttv_feature_t feature, int val);
// Get image processing features set using ttv_set_feature.
bool ttv_get_feature(ttv_feature_t feature, int *val);
// Get timestamp from multimedia timer
unsigned int ttv_get_timestamp();
// Get fifo buffer fill percentage
int ttv_get_fifo_fill();
//Set num idrs to be generated per second.
//if num_idr_per_second <= 0, idrs are generated at framerate interval.
//if num_idr_per_second > framerate, returns false
bool ttv_set_idr_frequency(int num_idr_per_second);
// Call this API for timelapse < 1s interval with proper framerate
// If we require timelapse for every 0.5s, then
// call ttv_set_encoder_framerate(2);
void ttv_set_encoder_framerate(unsigned framerate);
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
// Start raw capture
void ttv_enable_raw_capture (void);
//Dump captured data to file
void ttv_write_raw_buffer (int filenum, ttv_resolution_t resolution, bool dump_yuv, int cnt);
//Manual ISO
void ttv_set_manual_iso_gain(unsigned int iso_gain);
#endif

/** CLOCK_BOOTTIME was added in Linux 2.6.39 and backported to Longbeach project.
 *  If the glibc definition is missing we define it here to be able to access the kernel function.
 *  Use clock_gettime(CLOCK_BOOTTIME,..) to get monotonic time including time spent in suspend.
 */
#ifndef CLOCK_BOOTTIME
#define CLOCK_BOOTTIME 7
#endif

/**
 *  @func	ttv_sleep
 *
 *  @desc	Software or hardware for sleep 'timeout' milliseconds
 *
 *  @param[in]	timeout timeout in milliseconds
 * 		\n timeout = -1: power off. This feature is supported but deprecated.
 * 		\n timeout =  0: sleep indefinitely or until button or bluetooth wakeup
 * 		\n timeout >  0: sleep for timeout seconds or until button or bluetooth wakeup
 * 		\n timeout <  0 but not -1: return
 *
 *  @return
 * 		\n TTV_WAKE_TIMER:  Device was woken up by timer after 'timeout' milliseconds
 * 		\n TTV_WAKE_BUTTON: Device was woken up by button press
 * 		\n TTV_WAKE_BLE:    Device was woken up by bluetooth
 * 		\n TTV_WAKE_OTHER:  Device was woken up by another source
 *		\n TTV_SLEEP_MORE:  Function returns before 'timeout' milliseconds have elapsed
 *                    		    and device was not woken up by any external source.
 */
ttv_wakeup_t ttv_sleep(int timeout);

#if defined (__cplusplus)
}
#endif

#endif // TTV_H_
