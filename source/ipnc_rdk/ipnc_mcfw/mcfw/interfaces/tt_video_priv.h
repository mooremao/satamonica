// TomTom Long Beach Video Platform API
// Private declarations
// Nebojsa Sumrak 2014.

#ifndef TT_VIDEO_PRIV_H_
#define TT_VIDEO_PRIV_H_

#include "ttv.h"
#include "ti_vsys.h"
#include <osa.h>
#include <osa_sem.h>
#include <osa_mutex.h>
#include "mcfw/src_linux/mcfw_api/ti_vdec_priv.h"
#include "ttsystem_priv.h"

#if defined (__cplusplus)
extern "C" {
#endif

#define DCC_VIDEO_FILE_NAME    "/opt/ipnc/dcc/dcc_video_default.bin"
#define DCC_STILL_FILE_NAME    "/opt/ipnc/dcc/dcc_still_default.bin"

#define STREAM_BUF_MEM_SIZE 	(0x01E00000)
#define MAX_PENDING_RECV_SEM_COUNT	512

// AEWB Stabilization
#define AEWB_STAB_MODE					SYSTEM_STD_1080P_60
#define AEWB_STAB_SENSOR_WIDTH			1920	
#define AEWB_STAB_SENSOR_HEIGHT			1080
#define AEWB_STAB_WIDTH					1920
#define AEWB_STAB_HEIGHT				1080
#define AEWB_STAB_PITCH					1984
#define AEWB_STAB_FRAMERATE				60
#define AEWB_STAB_CNT					5

void debuglog_raw(int level, const char *filej, int line, const char *fmt, ...);
#define debuglog(level, fmt, ...)  debuglog_raw(level, __FILE__, __LINE__, fmt, __VA_ARGS__)

// private fifo functions
void ttv_fifo_init();
void ttv_fifo_deinit();
void ttv_fifo_clear();
bool ttv_fifo_write(Bitstream_Buf *bitbuf);
void ttv_set_framerate(unsigned chId, unsigned frameRate);

void ttv_createPipeline(void);
void ttv_changeMode(int mode,ttv_frame_type_t CodecType);
void ttv_deletePipeline(void);
void ttv_set_recording_mode(ttv_resolution_t resolution);
Int32 ttv_mjpeg_disableChn(MJPEG_CHN mjpegChnId);
Int32 ttv_mjpeg_enableChn(MJPEG_CHN mjpegChnId);

struct ttv_global_config_t {
	ttv_mode_t mode;
	unsigned started;

	// Config
	ttv_resolution_t resolution;
	ttv_resolution_t resolution_new;
	int framerate;
	int bitrate;

	// Imaging
	int brightness;
	int saturation;
	int contrast;
	int hue;
	int sharpness;
	int aewbmode;
	int ae_time;
	ttv_wb_mode_t wb_mode;
	ttv_ae_metering_mode_t ae_metering;
	int ev_compensation;

	// Transcoding
	ttv_video_res_t  transcodeInputRes;
	ttv_video_res_t  transcodeOutputRes;
	int              transcodeInputFps;
	int              transcodeOutputFps;
	ttv_frame_type_t transcodeOutCodec;

	// Still Capture
	int              stillCapNum;
	int              stillCapHighQualityCnt;
	int              stillCapLowQualityCnt;
	OSA_SemHndl      stillCapDoneSem;

	// DCC
	Vsys_AllocBufInfo dccVideoBufInfo;
	Vsys_AllocBufInfo dccStillBufInfo;
	Vsys_dccPrm dccVideoParams;
	Vsys_dccPrm dccStillParams;

	// FIFO
	OSA_SemHndl bitsInNotifySem;
	OSA_MutexHndl fifo_waccess;
	// Multithread prevention
	OSA_MutexHndl apiaccess;
	Bitstream_BufList emptyBitsBufList;

	volatile bool waiting_for_frame;
	volatile bool want_to_change_mode;

	bool is_fov_wide;

	int num_idr_per_second;
	int encoder_framerate;

	int transcode_bitrate;
};

void ttv_createTomTomTranscodeUseCase(struct ttv_global_config_t *);
void ttv_createRecordingUseCase();
Void ttv_putEmptyBitstreamBuf(void *address);
int  ttv_getFifoStats();

extern struct ttv_global_config_t g_cfg;

#ifdef LONGBEACH_FREQUENCY_DEBUG

// ttv platform peripherals
typedef enum {
	TTV_PERIPHERAL_ARM,
	TTV_PERIPHERAL_L3,
	TTV_PERIPHERAL_IVA,
	TTV_PERIPHERAL_ISS,
} ttv_peripheral_t;

/**
 *  @brief	Get peripheral frequency for a particular power mode
 *
 *  @param[in]	powermode power mode of type ttv_powermode_t
 *  @param[in]	peripheral peripheral of type ttv_peripheral_t
 *
 *  @return
 *		frequency in MHz on success
 *		-errno on failure
 */
int ttv_get_frequency(ttsystem_priv_powermode_t powermode, ttv_peripheral_t peripheral);

/**
 *  @brief	Set peripheral frequency for a particular power mode
 *
 *  @param[in]	powermode power mode of type ttv_powermode_t
 *  @param[in]	peripheral peripheral of type ttv_peripheral_t
 *  @param[in]	freq_in_MHz frequency in MHz
 *  @param[in]	now if true, changes take effect immediately
 *		    if false, changes take effect next time powermode is set
 *
 *  @return	true on success, false on failure
 */
bool ttv_set_frequency(ttsystem_priv_powermode_t powermode, ttv_peripheral_t peripheral,
		unsigned int freq_in_MHz, bool now);
#endif

#endif /* TT_VIDEO_PRIV_H_ */
