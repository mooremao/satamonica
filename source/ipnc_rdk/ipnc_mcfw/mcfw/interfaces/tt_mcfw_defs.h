// TomTom Long Beach Video Platform API
// Private MCFW declarations
// Nebojsa Sumrak 2014.

#ifndef TT_MCFW_PRIV_H_
#define TT_MCFW_PRIV_H_

#if defined (__cplusplus)
extern "C" {
#endif

typedef enum {
    NF_STRENGTH_AUTO = 0,
    NF_STRENGTH_LOW,
    NF_STRENGTH_MEDIUM,
    NF_STRENGTH_HIGH,
    NF_STRENGTH_MAXNUM
} UI_NF_STRENGTH;

typedef enum {
    DSS_VNF_ON = 0,
    ISS_VNF_ON,
    NOISE_FILTER_OFF
} UI_NF_MODE;

typedef enum {
    UI_AEWB_ID_NONE = 0,
    UI_AEWB_ID_APPRO,
    UI_AEWB_ID_TI,
    UI_AEWB_ID_MAXNUM = 3
} UI_AEWB_VENDOR;

typedef enum {
    UI_AEWB_OFF = 0,
    UI_AEWB_AE,
    UI_AEWB_AWB,
    UI_AEWB_AEWB,
    UI_AEWB_MODE_MAXNUM
} UI_AEWB_MODE;


typedef enum {
    MT_9J003 = 0,
    MT_AR0331,
    PN_MN34041,
    SN_IMX035,
    OM_OV2715,
    SN_IMX036,
    OM_OV9712,
    OM_OV10630,
    MT_9P031,
    MT_9D131,
    MT_9M034,
    TVP_514X,
    OM_OV7740,
    MT_9M034DUAL,
    SN_IMX136,
    MT_AR0330,
    SN_IMX104,
    AL_AL30210,
    OM_OV2710,
    SN_IMX122,
    SN_IMX140,	
	MT_AR1820HS,
	SS_S5K2P1
} UI_SENSOR_MODE;

#if defined (__cplusplus)
}
#endif

#endif
