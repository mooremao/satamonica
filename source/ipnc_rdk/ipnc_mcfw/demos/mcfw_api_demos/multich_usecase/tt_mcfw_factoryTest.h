// TomTom Cam factory test definitions

#ifndef IPNC_FACTORY_TEST_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define IPNC_FACTORY_TEST_H_

enum {
  FACTORY_NOP = 0,
  FACTORY_IMAGE_CAPTURE,
  FACTORY_VIDEOMODE_1080P,
  FACTORY_VIDEOMODE_720P,
  FACTORY_VIDEOMODE_WVGA,
  FACTORY_VIDEOMODE_4K,
  FACTORY_VIDEOMODE_2_7K,
  FACTORY_AWB_RATIO,
  FACTORY_REAL_FPS,
  FACTORY_VIDEOMODE_1080P_30,
  FACTORY_VIDEOMODE_FULL,
  FACTORY_VIDEOMODE_1080P_C,
  FACTORY_VIDEOMODE_1080P_LT,
  FACTORY_VIDEOMODE_1080P_RT,
  FACTORY_VIDEOMODE_1080P_LB,
  FACTORY_VIDEOMODE_1080P_RB,
  FACTORY_VIDEOMODE_FULL_4_3
};

void App_factory_test();

void tt_mcfw_fty_imageCapture(void);
void tt_mcfw_fty_getAWBGain(UInt32 *rGain, UInt32 *grGain, UInt32 *gbGain, UInt32 *bGain);
UInt32 tt_mcfw_fty_getRealFPS(void);
void tt_mcfw_fty_setVideoMode(int aMode);
void tt_mcfw_fty_setStreaming(UInt32 aEnable);
void tt_mcfw_fty_quit(void);
void tt_mcfw_fty_enableAE(void);
void tt_mcfw_fty_disableAE(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif

