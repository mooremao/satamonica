// TomTom Cam button menu implementation

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <sys/mman.h>

#include "ti_vsys.h"
#include "ti_vcam.h"
#include "ti_venc.h"
#include "ti_vdec.h"
#include "ti_vdis.h"
#include "ti_mcfw_ipnc_api.h" 

#ifndef false
#define false 0
#define true (!false)
#endif

// off after 2 seconds holding of OFF key
#define OFF_PRESS_TIME	(2*1000000)
#define GPIO_LED1	8
#define GPIO_BUTTONS	71

#define BUT_RECORD	0
#define BUT_OFF		1
#define BUT_UP		2
#define BUT_DOWN	3
#define BUT_LEFT	4
#define BUT_RIGHT	5
#define BUT_NONE1	6
#define BUT_BATLOCK	7
#define BUT_NUM		8

#define FB_DEV_NODE "/dev/fb0"

#define PATH_SYS_CLASS_LEDS "/sys/class/leds"

#define LED_NAME__LED1 "LED1"

#define timediff_us(start, end) ((((end).tv_sec - (start).tv_sec) * 1000000) + ((end).tv_usec - (start).tv_usec))

#define LCM_BYTES_PER_LINE 18 // 144 bits
#define LCM_HEIGHT 168

#define TEMP_GAUGE "/tmp/temp.log"

static int exportgpio(int gpio, int direction, int active_low)
{
	int fd_export;
	char buf[256];

	if ((fd_export = open("/sys/class/gpio/export", O_WRONLY)) < 0) return false;
	sprintf(buf,"%d",gpio);
	write(fd_export, buf, strlen(buf));
	close(fd_export);

	sprintf(buf, "/sys/class/gpio/gpio%d/active_low", gpio);
	if ((fd_export = open(buf, O_WRONLY)) < 0) return false;
	write(fd_export, active_low?"1":"0", 1);
	close(fd_export);

	sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio);
	if ((fd_export = open(buf, O_WRONLY)) < 0) return false;
	if(direction) write(fd_export, "out\n", 4);
	else  write(fd_export, "in\n", 3);
	close(fd_export);

	return true;
}

static int setgpio(int gpio, int value)
{
    char buf[256];
    sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
    int ff = open(buf, O_WRONLY);
    if(ff<0) return false;
    if (write(ff, value?"1":"0", 1) != 1) {
        close(ff);
        return false;
    }
    close(ff);
    return true;
}

static int getgpio(int gpio)
{
    char buf[256];
    sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
    int ff = open(buf, O_RDONLY);
    if(ff<0) return -1;
    if (read(ff, buf, 1) != 1) {
        close(ff);
        return -1;
    }
    close(ff);
    return (buf[0]=='0'?0:1);
}

static int setLedBrightness(const char* ledName, int brightness)
{
    char filepath[256];
    char data[32];
    int n, rv;

    snprintf(filepath, sizeof(filepath), "%s/%s/brightness", PATH_SYS_CLASS_LEDS, ledName);
    int fd = open(filepath, O_WRONLY);
    if(fd < 0)
    {
        perror(filepath);
        return false;
    }

    n = snprintf(data, sizeof(data) - 1, "%d", brightness);
    data[n] = '\0';

    rv = write(fd, data, n);
    if(rv != n) {
        perror(filepath);
        close(fd);
        return false;
    }

    close(fd);
    return true;
}

int button_states[BUT_NUM] = {0};
struct timeval button_time[BUT_NUM] = {{0}};

static int getbutton()
{
	int i;
	for(i=0;i<BUT_NUM;i++) {
		int curstate = getgpio(GPIO_BUTTONS+i);
		if(curstate<0) continue;
		if(curstate != button_states[i]) {
			button_states[i] = curstate;
			if(curstate) {
				gettimeofday(&button_time[i], 0);
				return (i+1);
			} else return -(i+1);
		}
	}
	return 0;
}

static int isRecording = 0;
static int gFileCnt = -1;
static struct timeval recstart = {0};

static void startRecording()
{
	char fileName[256];
	if(isRecording) return;
	if(++gFileCnt == 5) gFileCnt = 0;
	sprintf(fileName,"/mnt/mmc/TTCamVideo%d.h264",gFileCnt+1);
	App_ipncApi_startBitstreamDump(fileName);
	gettimeofday(&recstart,0);
	isRecording = 1;
	setLedBrightness(LED_NAME__LED1, 255);
	printf("recording to file '%s'\n",fileName);
}

static void stopRecording()
{
	if(!isRecording) return;
	isRecording = 0;
	setgpio(GPIO_LED1, 0);
	setLedBrightness(LED_NAME__LED1, 0);
	App_ipncApi_stopBitstreamDump();
	printf("stopped recording\n");
}

static int fpssel = 0;
static int ressel = 0;

void reconfigure()
{
	printf("Reconfigure: fps=%d res=%d\n",fpssel,ressel);
	switch(fpssel) {
		default: App_ipncApi_changeFrameRate(0,60); break;
		case 1: App_ipncApi_changeFrameRate(0,30); break;
		case 2: App_ipncApi_changeFrameRate(0,15); break;
	};
	switch(ressel) {
		default: App_ipncApi_changeResolution(0,1920,1080); break;
		case 1: App_ipncApi_changeResolution(0,1280,720); break;
		case 2: App_ipncApi_changeResolution(0,848,480); break;
	}
}


static int fbfd = -1;
static struct fb_fix_screeninfo fix_scr_info;
static struct fb_var_screeninfo var_scr_info;
static uint8_t *fbmem;
static char fbbackbuffer[LCM_BYTES_PER_LINE*LCM_HEIGHT];

void flushDisplay()
{
	if(fbfd<0) return;
	memcpy(fbmem, fbbackbuffer, LCM_BYTES_PER_LINE*LCM_HEIGHT);
	fsync(fbfd);
}

void initDisplay()
{
	fbfd = open(FB_DEV_NODE, O_RDWR);
	if (fbfd == -1) {
		printf("Unable to open %s", FB_DEV_NODE);
		return;
	}

	if (ioctl(fbfd, FBIOGET_FSCREENINFO, &fix_scr_info) || ioctl(fbfd, FBIOGET_VSCREENINFO, &var_scr_info)) {
		printf("unable to get screen info");
		goto err;
	}

	fbmem = mmap(NULL, fix_scr_info.smem_len, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);
	if (fbmem == NULL) {
		printf("Unable to mmap %s", FB_DEV_NODE);
		goto err;
	}

	if (ioctl(fbfd, FBIOBLANK, FB_BLANK_UNBLANK)) {
		printf("Unable to turn on the LCD");
		goto err;
	}

	// clear all frame buffer
	memset(fbmem, 0x00, fix_scr_info.smem_len);
	return;

err:
	close(fbfd);
	fbfd = -1;
}

void drawBitmap(int x, int y, int w, int h, char *bmp)
{
	char *fb = fbbackbuffer + y*LCM_BYTES_PER_LINE + x;
	for(;h>0;h--) {
		memcpy(fb, bmp, w);
		bmp += w;
		fb += LCM_BYTES_PER_LINE;
	}
}

void ditherRgn(int x, int y, int w, int h)
{
	int i;
	char d=0xaa;
	char *fb = fbbackbuffer + y*LCM_BYTES_PER_LINE + x;
	for(;h>0;h--) {
		d ^= 0xff;
		for(i=0;i<w;i++) *(fb++) &= d;
		fb += LCM_BYTES_PER_LINE-w;
	}

}

void readTemp(char* buf)
{
  char cmd[128];
  char data[32];
  int value;

  sprintf(cmd, "i2cget -f -y 3 0x55 0x2 w > %s", TEMP_GAUGE);
  system(cmd);
  
  FILE *fp = fopen(TEMP_GAUGE, "r");
  if (fp != NULL)
  {
    if (fscanf(fp, "%s", data) > 0)
    {  
      if (1 == sscanf(data, "0x%04X", &value)) // string to hex
      {
        sprintf(buf, "%d", value);
      }
    }
    fclose(fp);
  }
}

void refreshDisplay()
{
	#include "tt_demo_bitmap.h"
	int min=0, sec=0;
	char buf[16];
	char bufT[16];

	memset(fbbackbuffer, 0, sizeof(fbbackbuffer));
	if(isRecording) {
		struct timeval now;
		gettimeofday(&now,0);
		sec = now.tv_sec - recstart.tv_sec;
		min = sec/60;
		sec %= 60;
		if(min>99) min=99;
		drawBitmap(7,0,4,20,bmp_rec);
	}
	sprintf(buf,"%02d%02d",min,sec);

	drawBitmap(0,0,5,20,ressel==2?bmp_480p:ressel?bmp_720p:bmp_1080p);
	drawBitmap(15,0,3,20,fpssel==2?bmp_15f:fpssel?bmp_30f:bmp_60f);
	drawBitmap(1,25,4,71,bmp_digits[buf[0]-'0']);
	drawBitmap(5,25,4,71,bmp_digits[buf[1]-'0']);
	drawBitmap(9,25,4,71,bmp_digits[buf[2]-'0']);
	drawBitmap(13,25,4,71,bmp_digits[buf[3]-'0']);

	/* Read out temperature of fuel gauge and display */
	readTemp(bufT);
	drawBitmap(1,96,4,71,bmp_digits[bufT[0]-'0']);
	drawBitmap(5,96,4,71,bmp_digits[bufT[1]-'0']);
	drawBitmap(9,96,4,71,bmp_digits[bufT[2]-'0']);
	drawBitmap(13,96,4,71,bmp_digits[bufT[3]-'0']);

	ditherRgn(1,25,8,142);
	flushDisplay();
}

void App_button_Menu()
{
	int but, shouldExit=false;
	int repsec = 0;

	printf("***** TOMTOM BUTTON MENU *****\n");
	exportgpio(GPIO_BUTTONS,0,0);
	exportgpio(GPIO_BUTTONS+1,0,1);
	exportgpio(GPIO_BUTTONS+2,0,1);
	exportgpio(GPIO_BUTTONS+3,0,1);
	exportgpio(GPIO_BUTTONS+4,0,1);
	exportgpio(GPIO_BUTTONS+5,0,1);
	exportgpio(GPIO_BUTTONS+7,0,0);
	App_ipncApi_mountSDCard(TRUE,FALSE);

	initDisplay();
	refreshDisplay();

	while(!shouldExit) {
		// process button input
		while((but=getbutton())) { printf("button detected %d\n",but); switch(but-1) {

		case BUT_UP:
			if(!isRecording) {
				if(--fpssel<0) fpssel = 2;
				reconfigure();
				refreshDisplay();
			}
			break;
		case BUT_DOWN:
			if(!isRecording) {
				if(++fpssel==3) fpssel = 0;
				reconfigure();
				refreshDisplay();
			}
			break;
		case BUT_LEFT:
			if(!isRecording) {
				if(--ressel<0) ressel = 2;
				reconfigure();
				refreshDisplay();
			}
			break;
		case BUT_RIGHT:
			if(!isRecording) {
				if(++ressel==3) ressel = 0;
				reconfigure();
				refreshDisplay();
			}
			break;
		case BUT_RECORD:
			repsec = 0;
			startRecording();
			refreshDisplay();
			break;
		case BUT_OFF:
			stopRecording();
			refreshDisplay();
			break;
		case BUT_BATLOCK:
			shouldExit = 1;
			printf("app goes off by BAT_LOCK\n");
			break;
		case -BUT_OFF-2:
		  {
			struct timeval now;
			gettimeofday(&now, 0);
			if(timediff_us(button_time[BUT_OFF],now) >= OFF_PRESS_TIME) {
				shouldExit = 1;
				printf("app goes off by button OFF button\n");
			}
			break;
		  }
		}}
		usleep(50000);
		if(isRecording) {
			struct timeval now;
			int sec;
			gettimeofday(&now,0);
			sec = now.tv_sec - recstart.tv_sec;
			if(sec>repsec) {
				repsec = sec;
				refreshDisplay();
			}
		}
	}
	if(isRecording) stopRecording();
	if(fbfd>=0) close(fbfd);
	sync();
	App_ipncApi_mountSDCard(FALSE,FALSE);
	gUI_mcfw_config.demoCfg.stopDemo   = TRUE;
	gUI_mcfw_config.demoCfg.unloadDemo = TRUE;
	gUI_mcfw_config.demoCfg.delImgTune = TRUE;
	gUI_mcfw_config.demoCfg.exitDemo   = TRUE;
}

