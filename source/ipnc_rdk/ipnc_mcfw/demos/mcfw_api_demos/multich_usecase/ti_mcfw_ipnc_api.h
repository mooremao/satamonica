/** ==================================================================
 *  @file   ti_mcfw_ipnc_api.h
 *
 *  @path    ipnc_mcfw/demos/mcfw_api_demos/multich_usecase/
 *
 *  @desc   This  File contains.
 * ===================================================================
 *  Copyright (c) Texas Instruments Inc 2011, 2012
 *
 *  Use of this software is controlled by the terms and conditions found
 *  in the license agreement under which this software has been supplied
 * ===================================================================*/
/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

/**
  \file ti_mcfw_ipnc_api.h
  \brief  function related to ipnc mcfw main routine
  */

#ifndef IPNC_API_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define IPNC_API_H_

#include <mcfw/src_linux/osa/inc/osa_mutex.h>
#include <mcfw/src_linux/osa/inc/osa_sem.h>

/*
 *	Defines
 */
/* Still Capture */ 
#define STILL_CAPTURE_MODE				SYSTEM_STD_4656_3492_10
#define STILL_CAPTURE_SENSOR_WIDTH		4656
#define STILL_CAPTURE_SENSOR_HEIGHT		3488
#define STILL_CAPTURE_WIDTH				4656
#define STILL_CAPTURE_HEIGHT			3488
#define STILL_CAPTURE_PITCH				4720
#define STILL_CAPTURE_FRAMERATE			10

/* AEWB Stabilization */
#define AEWB_STAB_MODE					SYSTEM_STD_1080P_60
#define AEWB_STAB_SENSOR_WIDTH			1920	
#define AEWB_STAB_SENSOR_HEIGHT			1080
#define AEWB_STAB_WIDTH					1920
#define AEWB_STAB_HEIGHT				1080
#define AEWB_STAB_PITCH					1984
#define AEWB_STAB_FRAMERATE				60
#define AEWB_STAB_CNT					5

/* Real time FPS calculation */
#define FPS_CALC_FRAME_CNT				200
 
/*
 *	STRUCTURES
 */
typedef struct
{
	OSA_MutexHndl lock;
	Int8 fileName[128];
	FILE *fp;
	Bool dumpStarted;
	UInt32 dumpCnt;
	UInt32 jpgCnt;
	UInt32 firstKeyFrameFound;
    struct timeval startTime;
	
	UInt32 mpeg4VolSaved;
	Int8 mpeg4Vol[256];
	UInt32 mpeg4VolSize;
}BitstreamDumpCtx_t;

typedef struct
{
	OSA_SemHndl done;	
	UInt32 numCap;		// Number of stills to capture
	UInt32 capNum;		// Current number of stills captured
}StillCaptureCtx_t;

typedef struct
{	
	UInt32 frameCnt;
	UInt32 startTimeStamp;
	UInt32 curTimeStamp;
	float realFps;
}EncFps_t;

/*
 *	ENUM
 */
typedef enum 
{
	MIRROR_MODE_OFF = 0,
	MIRROR_MODE_HOR,
	MIRROR_MODE_VERT,
	MIRROR_MODE_BOTH,
}IPNCAPI_MIRROR_MODE;

typedef enum 
{
    AEWB_OFF = 0,
    AEWB_AE,
    AEWB_AWB,
    AEWB_AEWB,
    AEWB_MODE_MAXNUM
} AEWB_MODE;

typedef enum
{
  VOLTAGE_STANDARD_60HZ = 0,
  VOLTAGE_STANDARD_50HZ
} VOLTAGE_STANDARD;

/* ===================================================================
 *  @func     App_ipncApi_init
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_init();

/* ===================================================================
 *  @func     App_ipncApi_deInit
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_deInit();

/* ===================================================================
 *  @func     App_ipncApi_changeFrameRate
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeFrameRate(UInt32 chId,
								  UInt32 frameRate);
								  
/* ===================================================================
 *  @func     App_ipncApi_changeEncBitRate
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeEncBitRate(UInt32 chId,
								   UInt32 bitRate);		

/* ===================================================================
 *  @func     App_ipncApi_changeMJPEGQualityFactor
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeMJPEGQualityFactor(UInt32 chId,UInt32 qualityFactor);								   
								   
/* ===================================================================
 *  @func     App_ipncApi_changeEncType
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeEncType(UInt32 chId,
								VCODEC_TYPE_E codecType,
								Int32 encPreset);								   

/* ===================================================================
 *  @func     App_ipncApi_enableEnc
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_enableEnc(UInt32 chId,Bool enable);

/* ===================================================================
 *  @func     App_ipncApi_printEncParams
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_printEncParams(UInt32 chId);
					
/* ===================================================================
 *  @func     App_ipncApi_changeMuxMap
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeMuxMap(VSYS_MUX_IN_Q_CHAN_MAP_INFO_S *pMapPrm);		

/* ===================================================================
 *  @func     App_ipncApi_changeResolution
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeResolution(UInt32 chId,
								   UInt32 width,
								   UInt32 height);								

/* ===================================================================
 *  @func     App_ipncApi_displayEnable
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_displayEnable(Bool disEnable);								   

/* ===================================================================
 *  @func     App_ipncApi_changeIvaFreq
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeIvaFreq(UInt32 ivaFreq);

/* ===================================================================
 *  @func     App_ipncApi_startBitstreamDump
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_startBitstreamDump(Int8 *pFileName);

/* ===================================================================
 *  @func     App_ipncApi_dumpBitstreamDump
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_dumpBitstreamDump(Int8 *addr,UInt32 size,UInt32 isKeyFrame,UInt32 codingType,UInt32 timeStamp);
void App_ipncApi_dumpJpg(Int8 *addr, UInt32 size);

/* ===================================================================
 *  @func     App_ipncApi_stopBitstreamDump
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stopBitstreamDump();

/* ===================================================================
 *  @func     App_ipncApi_mountSDCard
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_mountSDCard(Bool mount,Bool format);

/* ===================================================================
 *  @func     App_ipncApi_histogramEnable
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_histogramEnable(Bool enable);

/* ===================================================================
 *  @func     App_ipncApi_vnfOnOff
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_vnfOnOff(UInt32 chId,Bool onOff);

/* ===================================================================
 *  @func     App_ipncApi_getH3AData
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_getH3AData(UInt32 *pAfData,UInt32 *pAewbData);

/* ===================================================================
 *  @func     App_ipncApi_enableSensorStreaming
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_enableSensorStreaming(UInt32 enable);

/* ===================================================================
 *  @func     App_ipncApi_stillCaptureEnter
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stillCaptureEnter();

/* ===================================================================
 *  @func     App_ipncApi_stillCaptureExit
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stillCaptureExit();

/* ===================================================================
 *  @func     App_ipncApi_dumpStillCapture
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_dumpStillCapture(Int8 *addr,UInt32 size);

/* ===================================================================
 *  @func     App_ipncApi_stillCaptureStart
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stillCaptureStart(UInt32 numCap);

/* ===================================================================
 *  @func     App_ipncApi_setMirrorMode
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setMirrorMode(UInt32 chId,IPNCAPI_MIRROR_MODE mirrorMode);

/* ===================================================================
 *  @func     App_ipncApi_setSystemUseCase
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setSystemUseCase(VSYS_USECASES_E systemUseCase,
								   SYSTEM_Standard camStandard);

/* ===================================================================
 *  @func     App_ipncApi_setAewbMode
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setAewbMode(AEWB_MODE aewbMode);

/* ===================================================================
 *  @func     App_ipncApi_resetEncRealFpsPrm
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_resetEncRealFpsPrm();

/* ===================================================================
 *  @func     App_ipncApi_setFrameTimeStamp
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setFrameTimeStamp(UInt32 chId,UInt32 timeStamp);

/* ===================================================================
 *  @func     App_ipncApi_getEncRealFps
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
float App_ipncApi_getEncRealFps(UInt32 chId);

/* ===================================================================
 *  @func     App_ipncApi_getWbGains
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_getWbGains(UInt32 *pRGain,
                             UInt32 *pGrGain,
							 UInt32 *pGbGain,
							 UInt32 *pBGain);

/* ===================================================================
 *  @func     App_ipncApi_getSensorInfo
 *
 *  @desc     This function calls the MCFW Vcam Api to get the Sensor
 *            frame Rate
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes the following inputs
 *            pSensorInfo
 *            This is the paramaeter through which it returns the
 *            Sensor Information
 *
 *  @outputs  VCAM_SENSOR_INFO_S *pSensorInfo
 *            contains the information related to sensor like fps etc
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_getSensorInfo(VCAM_SENSOR_INFO_S *pSensorInfo);

/* ===================================================================
 *  @func     App_ipncApi_setSimcopOn
 *
 *  @desc     This function calls the MCFW VSYS Api to set the SIMCOP
 *            to ON State
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   It doesnt take any inputs
 *
 *  @outputs  It doesnt give any outputs
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_setSimcopOn();

/* ===================================================================
 *  @func     App_ipncApi_setSimcopOff
 *
 *  @desc     This function calls the MCFW VSYS api to set the SIMCOP
 *            to OFF state
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   It doesnt take any inputs
 *
 *  @outputs  It doesnt give any outputs
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_setSimcopOff();

/* ===================================================================
 *  @func     App_ipncApi_setFlickerFrequency
 *
 *  @desc     This function calls the MCFW VCAM api to set the frequency
 *            of disabling flicker
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   It doesnt take any inputs
 *
 *  @outputs  It doesnt give any outputs
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_setFlickerFrequency(UInt32 flicker);

								   
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif  
                                                   /* IPNC_API_H_ */
												   
