// TomTom factory test implementation

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include "ti_vsys.h"
#include "ti_vcam.h"
#include "ti_venc.h"
#include "ti_vdec.h"
#include "ti_vdis.h"
#include "ti_mcfw_ipnc_api.h"
#include "tt_mcfw_factoryTest.h"

static UInt32 quit = 0;

void tt_mcfw_fty_imageCapture()
{
	printf("***** tt_mcfw_fty_imageCapture *****\n");

	/* Apply capture APIs */
	App_ipncApi_stillCaptureEnter();
	App_ipncApi_stillCaptureStart(1);
	App_ipncApi_stillCaptureExit();
}

void tt_mcfw_fty_setVideoMode(int aMode)
{
	printf("***** tt_mcfw_fty_setVideoMode *****\n");

	switch(aMode)
	{
		default:
		case FACTORY_VIDEOMODE_1080P_30:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1080P_3840_2160_30);
			break;
		case FACTORY_VIDEOMODE_1080P:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1080P_3840_2160_60);
			break;
		case FACTORY_VIDEOMODE_720P:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_720P_4608_2592_120);
			break;
		case FACTORY_VIDEOMODE_WVGA:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_WVGA_4608_2592_180);
			break;
		case FACTORY_VIDEOMODE_4K:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_4K_4608_2592_15);
			break;
		case FACTORY_VIDEOMODE_2_7K:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_2_7K_4608_2592_30);
			break;
		case FACTORY_VIDEOMODE_FULL:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_4656_3492_10);
			break;
		case FACTORY_VIDEOMODE_1080P_C:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1080P_1920_1080_C_30);
			break;
		case FACTORY_VIDEOMODE_1080P_LT:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1080P_1920_1080_LT_30);
			break;
		case FACTORY_VIDEOMODE_1080P_RT:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1080P_1920_1080_RT_30);
			break;
		case FACTORY_VIDEOMODE_1080P_LB:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1080P_1920_1080_LB_30);
			break;
		case FACTORY_VIDEOMODE_1080P_RB:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1080P_1920_1080_RB_30);
			break;
		case FACTORY_VIDEOMODE_FULL_4_3:
			App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM, SYSTEM_STD_1440_1080_2332_1748_30);
			break;
	}
}

UInt32 tt_mcfw_fty_getRealFPS()
{
	UInt32 fps;

	printf("***** tt_mcfw_fty_getRealFPS *****\n");
	fps = App_ipncApi_getEncRealFps(0);
	printf("Real time FPS : %d\n", fps);

	return fps;
}

void tt_mcfw_fty_getAWBGain(UInt32 *rGain, UInt32 *grGain, UInt32 *gbGain, UInt32 *bGain)
{
	printf("***** tt_mcfw_fty_getAWBGain *****\n");
	App_ipncApi_getWbGains(rGain,grGain,gbGain,bGain);
	printf("R gain  = %d\n", *rGain);
	printf("Gr gain = %d\n", *grGain);
	printf("Gb gain = %d\n", *gbGain);
	printf("B gain  = %d\n", *bGain);
}

void tt_mcfw_fty_setStreaming(UInt32 aEnable)
{
	printf("***** tt_mcfw_fty_setStreaming *****\n");
	App_ipncApi_enableSensorStreaming(aEnable);
}

void tt_mcfw_fty_quit(void)
{
	printf("***** tt_mcfw_fty_quit *****\n");
	quit = 1;
}

void tt_mcfw_fty_enableAE(void)
{
	printf("***** tt_mcfw_fty_enableAE *****\n");
	App_ipncApi_setAewbMode(AEWB_AEWB);
}

void tt_mcfw_fty_disableAE(void)
{
	printf("***** tt_mcfw_fty_disableAE *****\n");
	App_ipncApi_setAewbMode(AEWB_AWB);
}

void App_factory_test(void)
{
	printf("***** TOMTOM FACTORY TEST *****\n");

	App_ipncApi_init();
	while(!quit)
	{
		usleep(50000);
	}
	gUI_mcfw_config.demoCfg.stopDemo   = TRUE;
	gUI_mcfw_config.demoCfg.unloadDemo = TRUE;
	gUI_mcfw_config.demoCfg.delImgTune = TRUE;
	gUI_mcfw_config.demoCfg.exitDemo   = TRUE;

	App_ipncApi_deInit();
}
