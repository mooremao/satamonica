/** ==================================================================
 *  @file  tt_mcfw_transcode.h
 *
 *  @path  ipnc_rdk/ipnc_mcfw/demos/mcfw_api_demos/multich_usecase
 *
 *  @desc  This file contains the structure and functions for transcoding
 * =================================================================== */


#ifndef MCFW_TRANSCODE_H_
#define MCFW_TRANSCODE_H_

#include <mcfw/interfaces/link_api/system_const.h>

#include <osa.h>

#include <mcfw/interfaces/link_api/system.h>
#include <mcfw/interfaces/link_api/captureLink.h>
#include <mcfw/interfaces/link_api/cameraLink.h>
#include <mcfw/interfaces/link_api/mjpegLink.h>
#include <mcfw/interfaces/link_api/deiLink.h>
#include <mcfw/interfaces/link_api/nsfLink.h>
#include <mcfw/interfaces/link_api/displayLink.h>
#include <mcfw/interfaces/link_api/nullLink.h>
#include <mcfw/interfaces/link_api/grpxLink.h>
#include <mcfw/interfaces/link_api/dupLink.h>
#include <mcfw/interfaces/link_api/selectLink.h>
#include <mcfw/interfaces/link_api/swMsLink.h>
#include <mcfw/interfaces/link_api/mergeLink.h>
#include <mcfw/interfaces/link_api/nullSrcLink.h>
#include <mcfw/interfaces/link_api/ipcLink.h>
#include <osa_thr.h>
#include <osa_sem.h>


#define   NAL_ACCESS_UNIT_DELIMITER_CODE 0x00000109
#define   NALU_TYPE_SEI   6
#define   NALU_TYPE_SPS   7
#define   NALU_TYPE_PPS   8
#define   NALU_TYPE_AUD   9
#define   NALU_TYPE_EOS   10
#define   NALU_TYPE_FILL  12

/* =============================================================================
 * Structure
 * =============================================================================
 */
/* structure for the transcode usecase */
typedef struct
{
    FILE *fpRdConfig;
    /* one file contains the bitstream;
     * and other contains the size of each unit in the stream
     */
    FILE *fpRdStream;
    FILE *fpRdStreamSize;
    FILE *fpOutStream;
    volatile Bool thrExit;
    OSA_SemHndl   thrStartSem;
    OSA_ThrHndl   thrHandle;
    Int32         numFrames;
    Int32         bufCount;
    Int32         eof;

    Int32 ipcFramesOutVpssID;
    Int32 ipcFramesInHostID;
}transcodeUseCase_IpcBitsCtrl;

typedef struct
{
    Int8  inpFilename[128];
    Int8  outFilename[128];
    UInt32 inputWidth;
    UInt32 inputHeight;
    UInt32 inputFps;
    UInt32 outputWidth;
    UInt32 outputHeight;
    UInt32 outputFps;
    UInt32 outputCodecType; /* 8 - H264 HP, 9 - H264 MP, 24 - MJPG */
} transcodeUseCase_StreamConfig;


/* =============================================================================
 * Functions
 * =============================================================================
 */

/* ===================================================================
 *  @func    transcodeUseCase_bitsRdInit
 *
 *  @desc     it will initialize the file handles and thread for file read
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdInit();

/* ===================================================================
 *  @func    transcodeUseCase_DeInit
 *
 *  @desc     Cancels the thread and closes all file handles
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void transcodeUseCase_DeInit();

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdInitFileHandles
 *
 *  @desc     it will open the files to transcode and the output file
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdInitFileHandles();

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdCloseFileHandles
 *
 *  @desc     this function will close the file handles
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdCloseFileHandles();

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdInitThrObj
 *
 *  @desc     thread function is initialized by this function
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdInitThrObj();

/* ===================================================================
 *  @func     transcodeUseCase_requestBitStreamBuffer
 *
 *  @desc     function which is called to get an empty buffer
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_requestBitStreamBuffer( Bitstream_BufList *pIpcBufList);

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdReadData
 *
 *  @desc     this function is used for reading from sdcard
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdReadData(Bitstream_BufList *pIpcBufList);

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdSendFullBitBufs
 *
 *  @desc     passes a filled buffer to the next link
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdSendFullBitBufs(Bitstream_BufList *pIpcBufList);

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdSendFxn
 *
 *  @desc     thread function which reads from the file and pass buffers
 *            to the next link
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void *transcodeUseCase_bitsRdSendFxn(void * pParam);

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdStart
 *
 *  @desc     This function will start the transcoding thread function
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Void transcodeUseCase_bitsRdStart();

/**
 ******************************************************************************
 *  @fn    H264VID_getFrameSize
 *  @brief This function finds out each frame size of the given input bit
 *         stream.
 *
 *  @param[in]  stream    : Pointer to the bit stream
 *  @param[in]  bufferLen :
 *  @param[in]  new_nal   :
 *
 *  @return[out]  : None
 ******************************************************************************
 */
int H264VID_getFrameSize ( int * ,unsigned int, int );

/**
 ******************************************************************************
 *  @fn    generateH264SizeFile
 *  @brief function used to generate the h264 size file which
 *         describes size of each frame in the input bit stream
 *
 *  @param[in]  filename  : bitstream file whose size information to be generated
 *
 *  @return[out]  : returns the no of NAL units in the h264 stream
 ******************************************************************************
 */
int generateH264SizeFile(char *filename);

#endif
