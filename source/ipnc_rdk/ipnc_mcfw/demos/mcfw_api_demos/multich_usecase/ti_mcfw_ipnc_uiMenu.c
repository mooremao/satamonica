/*==============================================================================
 * @file:       ti_mcfw_ipnc_uiMenu.c
 *
 * @brief:      Video capture mcfw function definition.
 *
 * @vers:       0.5.0.0 2011-06
 *
 *==============================================================================
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "ti_vsys.h"
#include "ti_vcam.h"
#include "ti_venc.h"
#include "ti_vdec.h"
#include "ti_vdis.h"
#include "osa.h"
#include "ti_mcfw_ipnc_api.h" 
#include "ti_mcfw_ipnc_uiMenu.h"
#include <mcfw/src_linux/mcfw_api/ti_vsys_priv.h>

UInt32 gFileCnt = 0;

Int8 gUI_mainMenu[] = {

  " ==============\n"
  "    Main Menu\n"
  " ==============\n"
  " \n"
  " 1: Frame Rate Change\n"
  " 2: Encoder Bitrate Change\n"
  " 3: Encoder Codec Change\n"
  " 4: Encoder enable/disable\n"
  " 5: Resolution Change\n"
  " 6: Display ON/OFF\n"
  " 7: IVA freq Change\n"
  " 8: SD card Menu\n"
  " 9: Bitstream Dump\n"
  " a: Histogram enable/disable\n"
  " b: VNF On/Off\n"
  " c: Print H3A data\n"
  " d: Encoder Print Parameters\n"
  " f: Sensor Streaming\n"
  " g: Still Capture\n"
  " h: Mirror Menu\n"
  " i: System Usecase Menu\n"
  " j: AEWB Menu\n"
  " k: Enc Real FPS Menu\n"
  " l: Get WB Gains\n"
  " m: Sensor Information\n"
  " n: SIMCOP Menu\n"
  " o: FLICKER Menu\n"
  " p: Transcoding\n"
  " \n"
  " e: Stop and Exit\n"
  " r: Reboot\n"
  " \n"
};

Int8 gUI_frameRateMenu[] = {

  " ======================\n"
  "    Frame Rate Menu\n"
  " ======================\n"
  " \n"
  " 1: Channel 0 Frame Rate 15 fps\n"
  " 2: Channel 0 Frame Rate 30 fps\n"
  " 3: Channel 0 Frame Rate 60 fps\n"
  " 4: Channel 1 Frame Rate 15 fps\n"
  " 5: Channel 1 Frame Rate 30 fps\n"
  " 6: Channel 1 Frame Rate 60 fps\n"  
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_bitRateMenu[] = {

  " ============================\n"
  "    Encoder Bit Rate Menu\n"
  " ============================\n"
  " \n"
  " 1: Channel 0 Bit Rate 8000000 bps\n"
  " 2: Channel 0 Bit Rate 10000000 bps\n"
  " 3: Channel 0 MJPEG quality factor 80\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_encCodecTypeMenu[] = {

  " ============================\n"
  "    Encoder Codec Type Menu\n"
  " ============================\n"
  " \n"
  " 1: Channel 0 H264\n"
  " 2: Channel 0 MPEG4\n"
  " 3: Channel 0 MJPEG\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_encEnableMenu[] = {

  " ============================\n"
  "    Encoder Enable Menu\n"
  " ============================\n"
  " \n"
  " 1: Channel 0 enable\n"
  " 2: Channel 0 disable\n"
  " 3: Channel 1 enable\n"
  " 4: Channel 1 disable\n"  
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_resolutionMenu[] = {

  " ============================\n"
  "      Resolution Menu\n"
  " ============================\n"
  " \n"
  " 1: Channel 0 Resolution [720x480]\n"
  " 2: Channel 0 Resolution [1280x720]\n"
  " 3: Channel 0 Resolution [1920x1080]\n"  
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_displayMenu[] = {

  " ============================\n"
  "      Display Menu\n"
  " ============================\n"
  " \n"
  " 1: Display ON\n"
  " 2: Display OFF\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_ivaFreqMenu[] = {

  " ============================\n"
  "      IVA Freq Menu\n"
  " ============================\n"
  " \n"
  " 1: IVA freq 410 MHz\n"
  " 2: IVA freq 266 MHz\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_sdMountMenu[] = {

  " ============================\n"
  "      SD Card Menu\n"
  " ============================\n"
  " \n"
  " 1: Mount SD card\n"
  " 2: Unmount SD card\n"
  " 3: Format SD card\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_bitstreamDumpMenu[] = {

  " ============================\n"
  "      Bitstream Dump Menu\n"
  " ============================\n"
  " \n"
  " 1: Start bitstream Dump\n"
  " 2: Stop bistream Dump\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_histogramMenu[] = {

  " ============================\n"
  "      Histogram Menu\n"
  " ============================\n"
  " \n"
  " 1: Histogram enable\n"
  " 2: Histogram disable\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_vnfMenu[] = {

  " ============================\n"
  "      VNF Menu\n"
  " ============================\n"
  " \n"
  " 1: VNF On\n"
  " 2: VNF Off\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_encPrintParamsMenu[] = {

  " =============================\n"
  " Encoder Print Parameters Menu\n"
  " =============================\n"
  " \n"
  " 1: Print Channel 0 parameters\n"
  " 2: Print Channel 1 parameters\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_sensorStreamingMenu[] = {

  " ============================\n"
  "      Sensor Streaming Menu\n"
  " ============================\n"
  " \n"
  " 1: Sensor Streaming ON\n"
  " 2: Sensor Streaming OFF\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_stillCaptureMenu[] = {

  " ============================\n"
  "      Still Capture Menu\n"
  " ============================\n"
  " \n"
  " 1: Still Capture 1 frame\n"
  " 2: Still Capture 5 frames\n"
  " 3: Still Capture 10 frames\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_mirrorMenu[] = {

  " ============================\n"
  "      Mirror Menu\n"
  " ============================\n"
  " \n"
  " 1: Mirror Off\n"
  " 2: Mirror Horizontal\n"
  " 3: Mirror Vertical\n"
  " 4: Mirror Both\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_sysUseCaseMenu[] = {

  " ============================\n"
  "      System Usecase Menu\n"
  " ============================\n"
  " \n"
  " a: TomTom - SYSTEM_STD_4K_4608_2592_15\n"
  " b: TomTom - SYSTEM_STD_4K_4608_2592_12_5\n"
  " c: TomTom - SYSTEM_STD_2_7K_4608_2592_30\n"
  " d: TomTom - SYSTEM_STD_2_7K_4608_2592_25\n"
  " f: TomTom - SYSTEM_STD_2_7K_3840_2160_25\n"
  " g: TomTom - SYSTEM_STD_1080P_4608_2592_60\n"
  " h: TomTom - SYSTEM_STD_1080P_3840_2160_60\n"
  " i: TomTom - SYSTEM_STD_1080P_4608_2592_50\n"
  " j: TomTom - SYSTEM_STD_1080P_3840_2160_50\n"
  " k: TomTom - SYSTEM_STD_1080P_4608_2592_30\n"
  " l: TomTom - SYSTEM_STD_1080P_3840_2160_30\n"
  " m: TomTom - SYSTEM_STD_1080P_4608_2592_25\n"
  " n: TomTom - SYSTEM_STD_1080P_3840_2160_25\n"
  " o: TomTom - SYSTEM_STD_720P_4608_2592_120\n"
  " p: TomTom - SYSTEM_STD_720P_3840_2160_120\n"
  " q: TomTom - SYSTEM_STD_720P_4608_2592_100\n"
  " r: TomTom - SYSTEM_STD_720P_3840_2160_100\n"
  " s: TomTom - SYSTEM_STD_720P_4608_2592_60\n"
  " t: TomTom - SYSTEM_STD_720P_3840_2160_60\n"
  " u: TomTom - SYSTEM_STD_720P_4608_2592_50\n"
  " v: TomTom - SYSTEM_STD_720P_3840_2160_50\n"
  " w: TomTom - SYSTEM_STD_WVGA_4608_2592_180\n"
  " x: TomTom - SYSTEM_STD_WVGA_4608_2592_150\n"
  " \n"
  " 1: TomTom - SYSTEM_STD_4664_3496_10\n"
  " 2: Transcode\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_aewbMenu[] = {

  " ============================\n"
  "      AEWB Menu\n"
  " ============================\n"
  " \n"
  " 1: OFF\n"
  " 2: AE\n"
  " 3: AWB\n"
  " 4: AEWB\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_encRealFpsMenu[] = {

  " ============================\n"
  "      Enc Real FPS Menu\n"
  " ============================\n"
  " \n"
  " 1: Get Enc Real FPS for Channel 0\n"
  " 2: Get Enc Real FPS for Channel 1\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_sensorInformation[] = {

  " ============================\n"
  "      Sensor Information Menu\n"
  " ============================\n"
  " \n"
  " 1: sensor FPS\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_simcopMenu[] = {

  " ============================\n"
  "      SIMCOP Menu\n"
  " ============================\n"
  " \n"
  " 1: SIMCOP on\n"
  " 2: SIMCOP off\n"
  " \n"
  " e: Exit\n"
  " \n"
};

Int8 gUI_flickerMenu[] = {

  " ============================\n"
  "      FLICKER Menu\n"
  " ============================\n"
  " \n"
  " 1: Disable flicker for NTSC (60Hz)\n"
  " 2: Disable flicker for PAL (50Hz)\n"
  " \n"
  " e: Exit\n"
  " \n"
};

/* ===================================================================
 *  @func     App_ipncUi_frameRateMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_frameRateMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_frameRateMenu);
        printf(" Enter Choice : "); 
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_changeFrameRate(0,15);
                break;
                
            case '2':
                App_ipncApi_changeFrameRate(0,30);
                break;

            case '3':
                App_ipncApi_changeFrameRate(0,60);
                break;
                
            case '4':
                App_ipncApi_changeFrameRate(1,15);
                break;

            case '5':
                App_ipncApi_changeFrameRate(1,30);
                break;

            case '6':
                App_ipncApi_changeFrameRate(1,60);
                break;
 
            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_encBitRateMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encBitRateMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_bitRateMenu);
        printf(" Enter Choice : ");
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_changeEncBitRate(0,8000000);
                break;
                
            case '2':
                App_ipncApi_changeEncBitRate(0,10000000);
                break;

            case '3':
                App_ipncApi_changeMJPEGQualityFactor(0,80);
                break;
                
            case 'e':
                exitFlag = TRUE;
                break;
            
            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_encCodecTypeMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encCodecTypeMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_encCodecTypeMenu);
        printf(" Enter Choice : ");
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_changeEncType(0,VCODEC_TYPE_H264,0);
                break;
                
            case '2':
                App_ipncApi_changeEncType(0,VCODEC_TYPE_MPEG4,0);
                break;

            case '3':
                App_ipncApi_changeEncType(0,VCODEC_TYPE_MJPEG,0);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_encEnableMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encEnableMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_encEnableMenu);
        printf(" Enter Choice : ");
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_enableEnc(0,TRUE);
                break;
                
            case '2':
                App_ipncApi_enableEnc(0,FALSE);
                break;

            case '3':
                App_ipncApi_enableEnc(1,TRUE);
                break;

            case '4':
                App_ipncApi_enableEnc(1,FALSE);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_resolutionMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_resolutionMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_resolutionMenu);
        printf(" Enter Choice : ");        
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_changeResolution(0,720,480);
                break;        
        
            case '2':
                App_ipncApi_changeResolution(0,1280,720);
                break;
                
            case '3':
                App_ipncApi_changeResolution(0,1920,1080);
                break;
                                
            case 'e':
                exitFlag = TRUE;
                break;                
            
            default:
                break;
        }
    }
    
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_displayMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_displayMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_displayMenu);
        printf(" Enter Choice : ");        
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_displayEnable(TRUE);
                break;        
        
            case '2':
                App_ipncApi_displayEnable(FALSE);
                break;
                                                
            case 'e':
                exitFlag = TRUE;
                break;                
            
            default:
                break;
        }
    }
    
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_ivaFreqMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_ivaFreqMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_ivaFreqMenu);
        printf(" Enter Choice : ");        
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_changeIvaFreq(410);
                break;        
        
            case '2':
                App_ipncApi_changeIvaFreq(266);
                break;
                                                
            case 'e':
                exitFlag = TRUE;
                break;                
            
            default:
                break;
        }
    }
    
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_sdMountMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_sdMountMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_sdMountMenu);
        printf(" Enter Choice : ");        
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_mountSDCard(TRUE,FALSE);
                break;        
        
            case '2':
                App_ipncApi_mountSDCard(FALSE,FALSE);
                break;

            case '3':
                App_ipncApi_mountSDCard(TRUE,TRUE);
                break;
                
            case 'e':
                exitFlag = TRUE;
                break;                
            
            default:
                break;
        }
    }
    
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_bitstreamDumpMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_bitstreamDumpMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;
    Int8 fileName[128];

    /* Stop Sensor streaming */
    App_ipncApi_enableSensorStreaming(FALSE);
    
    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_bitstreamDumpMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                sprintf(fileName,"/mnt/mmc/bitstream_%s_%d.bin",MultiCh_getCodecName(0),gFileCnt++);
                App_ipncApi_startBitstreamDump(fileName);
                break;

            case '2':
                App_ipncApi_stopBitstreamDump();
                break;

            case 'e':
                exitFlag = TRUE;
                App_ipncApi_stopBitstreamDump();
                break;

            default:
                break;
        }
    }

    /* Start Sensor streaming */
    App_ipncApi_enableSensorStreaming(TRUE);

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_histogramMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_histogramMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_histogramMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_histogramEnable(TRUE);
                break;

            case '2':
                App_ipncApi_histogramEnable(FALSE);
                break;

            case 'e':
                exitFlag = TRUE;
                App_ipncApi_stopBitstreamDump();
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_vnfMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_vnfMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_vnfMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_vnfOnOff(0,TRUE);
                break;

            case '2':
                App_ipncApi_vnfOnOff(0,FALSE);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_encPrintParamMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encPrintParamMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_encPrintParamsMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_printEncParams(0);
                break;

            case '2':
                App_ipncApi_printEncParams(1);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_sensorStreamingMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_sensorStreamingMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_sensorStreamingMenu);
        printf(" Enter Choice : ");        
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_enableSensorStreaming(TRUE);
                break;

            case '2':
                App_ipncApi_enableSensorStreaming(FALSE);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_stillCaptureMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_stillCaptureMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    /* Enter Still Capture Mode */
    App_ipncApi_stillCaptureEnter();
    
    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_stillCaptureMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_stillCaptureStart(1);
                break;

            case '2':
                App_ipncApi_stillCaptureStart(5);
                break;

            case '3':
                App_ipncApi_stillCaptureStart(10);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    /* Exit Still Capture Mode */
    App_ipncApi_stillCaptureExit();

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_mirrorMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_mirrorMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_mirrorMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_setMirrorMode(0,MIRROR_MODE_OFF);
                break;

            case '2':
                App_ipncApi_setMirrorMode(0,MIRROR_MODE_HOR);
                break;

            case '3':
                App_ipncApi_setMirrorMode(0,MIRROR_MODE_VERT);
                break;

            case '4':
                App_ipncApi_setMirrorMode(0,MIRROR_MODE_BOTH);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }

    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_sysUseCaseMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_sysUseCaseMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_sysUseCaseMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case 'a':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_4K_4608_2592_15);
                break;

            case 'b':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_4K_4608_2592_12_5);
                break;

            case 'c':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_2_7K_4608_2592_30);
                break;

            case 'd':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_2_7K_4608_2592_25);
                break;

            case 'f':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_2_7K_3840_2160_25);
                break;

            case 'g':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_4608_2592_60);
                break;
                
            case 'h':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_3840_2160_60);
                break;

            case 'i':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_4608_2592_50);
                break;

            case 'j':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_3840_2160_50);
                break;

            case 'k':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_4608_2592_30);
                break;

            case 'l':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_3840_2160_30);
                break;

            case 'm':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_4608_2592_25);
                break;

            case 'n':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_1080P_3840_2160_25);
                break;

            case 'o':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_4608_2592_120);
                break;

            case 'p':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_3840_2160_120);
                break;

            case 'q':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_4608_2592_100);
                break;

            case 'r':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_3840_2160_100);
                break;

            case 's':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_4608_2592_60);
                break;

            case 't':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_3840_2160_60);
                break;

            case 'u':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_4608_2592_50);
                break;

            case 'v':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_720P_3840_2160_50);
                break;

            case 'w':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_WVGA_4608_2592_180);
                break;

            case 'x':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_WVGA_4608_2592_150);
                break;

            case '1':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                             SYSTEM_STD_4664_3496_10);
                break;        
        
            case '2':
                App_ipncApi_setSystemUseCase(VSYS_USECASE_ENC_A8_DEC,
                                             SYSTEM_STD_1080P_60);
                break;

            case 'e':
                exitFlag = TRUE;
                break;
            
            default:
                break;
        }
    }
 
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_aewbMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_aewbMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_aewbMenu);
        printf(" Enter Choice : ");        
        ch = getchar();
        
        switch(ch)
        {
            case '1':
                App_ipncApi_setAewbMode(AEWB_OFF);
                break;        
        
            case '2':
                App_ipncApi_setAewbMode(AEWB_AE);
                break;

            case '3':
                App_ipncApi_setAewbMode(AEWB_AWB);
                break;

            case '4':
                App_ipncApi_setAewbMode(AEWB_AEWB);
                break;
                
            case 'e':
                exitFlag = TRUE;
                break;                
            
            default:
                break;
        }
    }
    
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_encRealFpsMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encRealFpsMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;
    float encRealFps;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_encRealFpsMenu);
        printf(" Enter Choice : ");        
        ch = getchar();

        switch(ch)
        {
            case '1':
                encRealFps = App_ipncApi_getEncRealFps(0);        
                printf("\n");
                printf("UI:Enc Real FPS for ch 0 is %f fps\n",encRealFps);
                printf("\n");                
                break;

            case '2':
                encRealFps = App_ipncApi_getEncRealFps(1);
                printf("\n");
                printf("UI:Enc Real FPS for ch 1 is %f fps\n",encRealFps);
                printf("\n");
                break;

            case 'e':
                exitFlag = TRUE;
                break;
            
            default:
                break;
        }            
    }
 
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_getSensorInfo
 *
 *  @desc     it is called from the UART menu to get the sensor
 *            information
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes no inputs
 *
 *  @outputs  This function doesnt give any output
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncUi_getSensorInfo()
{
    Bool exitFlag = FALSE;
    VCAM_SENSOR_INFO_S sensorInfos;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_sensorInformation);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_getSensorInfo(&sensorInfos);
                printf("\nSENSOR INFORMATION : FPS = %d\n",sensorInfos.fps);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_simcopMenu
 *
 *  @desc     it is called from the UART menu to get the SIMCOP menu
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes no inputs
 *
 *  @outputs  This function doesnt give any output
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncUi_simcopMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_simcopMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_setSimcopOn();
                break;

            case '2':
                App_ipncApi_setSimcopOff();
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_flickerMenu
 *
 *  @desc     it is called from the UART menu to get the FLICKER menu
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes no inputs
 *
 *  @outputs  This function doesnt give any output
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncUi_flickerMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_flickerMenu);
        printf(" Enter Choice : ");
        ch = getchar();

        switch(ch)
        {
            case '1':
                App_ipncApi_setFlickerFrequency(0);
                break;

            case '2':
                App_ipncApi_setFlickerFrequency(1);
                break;

            case 'e':
                exitFlag = TRUE;
                break;

            default:
                break;
        }
    }
    return 0;
}

/* ===================================================================
 *  @func     App_ipncUi_mainMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_mainMenu()
{
    Bool exitFlag = FALSE;
    Int8 ch;
    UInt32 afData,aewbData;
    UInt32 rGain,grGain,gbGain,bGain;

	/* App Init */
    App_ipncApi_init();

    while(exitFlag == FALSE)
    {
        printf("\n");
        printf(gUI_mainMenu);
        printf(" Enter Choice : ");		
        ch = getchar();

        switch(ch)
        {
            case '1':
                    App_ipncUi_frameRateMenu();
                    break;

            case '2':
                    App_ipncUi_encBitRateMenu();
                    break;

            case '3':
                    App_ipncUi_encCodecTypeMenu();
                    break;

            case '4':
                    App_ipncUi_encEnableMenu();
                    break;

            case '5':
                    App_ipncUi_resolutionMenu();
                    break;
	
            case '6':
                    App_ipncUi_displayMenu();
                    break;
	
            case '7':
                    App_ipncUi_ivaFreqMenu();
                    break;

            case '8':
                    App_ipncUi_sdMountMenu();
                    break;

            case '9':
                    App_ipncUi_bitstreamDumpMenu();
                    break;

            case 'a':
                    App_ipncUi_histogramMenu();
                    break;

            case 'b':
                    App_ipncUi_vnfMenu();
                    break;

            case 'c':
                    App_ipncApi_getH3AData(&afData,&aewbData);
                    printf("\n");
                    printf("UI:AF value = %d,AEWB value = %d\n",afData,aewbData);
                    printf("\n");
                    break;

            case 'd':
                    App_ipncUi_encPrintParamMenu();
                    break;

            case 'f':
                    App_ipncUi_sensorStreamingMenu();
                    break;

            case 'g':
                    App_ipncUi_stillCaptureMenu();
                    break;
		
            case 'h':
                    App_ipncUi_mirrorMenu();
                    break;

            case 'i':
                    App_ipncUi_sysUseCaseMenu();
                    break;

           case 'j':
                   App_ipncUi_aewbMenu();
                   break;

           case 'k':
                   App_ipncUi_encRealFpsMenu();
                   break;

           case 'l':
                   App_ipncApi_getWbGains(&rGain,&grGain,&gbGain,&bGain);
                   printf("\n");
                   printf("UI:R gain  = %d\n",rGain);
                   printf("UI:Gr gain = %d\n",grGain);
                   printf("UI:Gb gain = %d\n",gbGain);
                   printf("UI:B gain  = %d\n",bGain);
                   printf("\n");
                  break;

          case 'm':
                  App_ipncUi_getSensorInfo();
                  break;

          case 'n':
                  App_ipncUi_simcopMenu();
                  break;

          case 'o':
                  App_ipncUi_flickerMenu();
                  break;

          case 'p':
                  App_ipncApi_setSystemUseCase(VSYS_TRANSCODE_USECASE,
                                               SYSTEM_STD_1080P_4608_2592_60);
                  break;

          case 'r':
                  /*
                   * REBOOT option is required for test automation
                   */
                  system("reboot");
                  break;

          case 'e':
                  exitFlag                           = TRUE;
                  gUI_mcfw_config.demoCfg.stopDemo   = TRUE;
                  gUI_mcfw_config.demoCfg.unloadDemo = TRUE;
                  gUI_mcfw_config.demoCfg.delImgTune = TRUE;
                  gUI_mcfw_config.demoCfg.exitDemo   = TRUE;
                  break;

          default:
                 break;
        }
    }

    /* App Deinit */
    App_ipncApi_deInit();

    return 0;
}

