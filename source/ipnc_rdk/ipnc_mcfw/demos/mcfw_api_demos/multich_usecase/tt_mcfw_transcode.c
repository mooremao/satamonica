/** ==================================================================
 *  @file  tt_mcfw_transcode.c
 *
 *  @path  ipnc_rdk/ipnc_mcfw/demos/mcfw_api_demos/multich_usecase
 *
 *  @desc  This file contains the functions that will help in transcoding
 * =================================================================== */

#include "tt_mcfw_transcode.h"
#include "mcfw/src_linux/mcfw_api/ti_vdec_priv.h"
/* globals used in finding the h264 frame length */
int  prev_nal_type     = 9;
int  prev_vcl_nal_flag = 0;

extern transcodeUseCase_IpcBitsCtrl gDecodeUseCase_obj;
extern VDEC_MODULE_CONTEXT_S gVdecModuleContext;
extern transcodeUseCase_StreamConfig transcodeStrCfg;

/* ===================================================================
 *  @func    transcodeUseCase_bitsRdInit
 *
 *  @desc     it will initialize the file handles and thread for file read
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdInit()
{
    Int32 retval = 0;

    /* initialises the file handles for read and initializes
     * the thread function for reading
     */
    gDecodeUseCase_obj.bufCount = 0;
    gDecodeUseCase_obj.eof = 0;
    transcodeUseCase_bitsRdInitFileHandles();
    transcodeUseCase_bitsRdInitThrObj();

    return retval;
}

/* ===================================================================
 *  @func    transcodeUseCase_DeInit
 *
 *  @desc     Cancels the thread and closes all file handles
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void transcodeUseCase_DeInit()
{
    OSA_thrDelete(&gDecodeUseCase_obj.thrHandle);
    OSA_semDelete(&gDecodeUseCase_obj.thrStartSem);
    transcodeUseCase_bitsRdCloseFileHandles();
    return;
}

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdInitFileHandles
 *
 *  @desc     it will open the files to transcode and the output file
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdInitFileHandles()
{
    Int32 retval = 0;

    /* file /mnt/mmc/stream.h264 contains the bitstream
     * file /mnt/mmc/stream_size.txt contains the size of each unit in the
     * bit stream
     */
    system("mount /dev/mmcblk0 /mnt/mmc/");
    system("mount /dev/mmcblk1 /mnt/mmc/");

    /*
     * Open the Transcode configuration file
     * This file contains the details about input/output resolution
     * input/output frame rate, output codec type
     */
    if((gDecodeUseCase_obj.fpRdConfig = fopen("/mnt/mmc/transcode_config.txt", "r"))
       == NULL)
    {
        retval = -1;
        OSA_printf("TRANSCODE USECASE: Transcode config file open fail\n");
        goto EXIT;
    }

    /*
     * Read the transcode configuration from the file
     */
    fscanf(gDecodeUseCase_obj.fpRdConfig, "%s %d %d %d %d %d %d %d %s",
           &transcodeStrCfg.inpFilename[0],
           &transcodeStrCfg.inputWidth,
           &transcodeStrCfg.inputHeight,
           &transcodeStrCfg.inputFps,
           &transcodeStrCfg.outputWidth,
           &transcodeStrCfg.outputHeight,
           &transcodeStrCfg.outputFps,
           &transcodeStrCfg.outputCodecType,
           &transcodeStrCfg.outFilename[0]);

    if((transcodeStrCfg.outputCodecType != IVIDEO_H264HP) &&
       (transcodeStrCfg.outputCodecType != IVIDEO_H264MP) &&
       (transcodeStrCfg.outputCodecType != IVIDEO_MJPEG))
    {
        retval = -1;
        OSA_printf("TRANSCODE USECASE: Invalid output codec type.\n");
        goto EXIT;
    }

    /* generating a stream size file */
    gDecodeUseCase_obj.numFrames = 
         generateH264SizeFile(transcodeStrCfg.inpFilename);

    gDecodeUseCase_obj.fpRdStream = fopen(transcodeStrCfg.inpFilename, "rb");

    gDecodeUseCase_obj.fpRdStreamSize = fopen("/mnt/mmc/stream_size.txt","r");

    gDecodeUseCase_obj.fpOutStream = fopen(transcodeStrCfg.outFilename,"wb");
    if(gDecodeUseCase_obj.fpOutStream == NULL)
    {
        retval = -1;
        OSA_printf("TRANSCODE USECASE : failed to open out put file\n");
    }


    if(gDecodeUseCase_obj.fpRdStream == NULL ||
       gDecodeUseCase_obj.fpRdStreamSize == NULL)
    {
        retval = -1;
        OSA_printf("TRANSCODE USECASE : failed to open input files\n");
    }

EXIT:
    return retval;
}

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdCloseFileHandles
 *
 *  @desc     this function will close the file handles
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdCloseFileHandles()
{
    Int32 retval = 0;
    if(gDecodeUseCase_obj.fpRdStream)
        fclose(gDecodeUseCase_obj.fpRdStream);

    if(gDecodeUseCase_obj.fpOutStream)
        fclose(gDecodeUseCase_obj.fpOutStream);

    if(gDecodeUseCase_obj.fpRdStreamSize)
        fclose(gDecodeUseCase_obj.fpRdStreamSize);

    if(gDecodeUseCase_obj.fpRdConfig)
        fclose(gDecodeUseCase_obj.fpRdConfig);

    system("rm /mnt/mmc/stream_size.txt");
    system("umount /mnt/mmc/");

    return retval;
}

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdInitThrObj
 *
 *  @desc     thread function is initialized by this function
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdInitThrObj()
{
    Int32 status = OSA_SOK;

    gDecodeUseCase_obj.thrExit = FALSE;
    status = OSA_semCreate(&gDecodeUseCase_obj.thrStartSem, 1, 0);
    OSA_assert(status == OSA_SOK);

    status = OSA_thrCreate(&gDecodeUseCase_obj.thrHandle,
            transcodeUseCase_bitsRdSendFxn,
            2,// ipc link task priority
            0,// ipc link stack size ; 0 means default system size will be used
            &gDecodeUseCase_obj);

    OSA_assert(status == OSA_SOK);

    return status;
}

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdSendFxn
 *
 *  @desc     thread function which reads from the file and pass buffers
 *            to the next link
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void *transcodeUseCase_bitsRdSendFxn(void * pParam)
{
    Bitstream_BufList ipcBufList;
    Int32 i = 0;

    OSA_semWait(&gDecodeUseCase_obj.thrStartSem, OSA_TIMEOUT_FOREVER);

    OSA_printf("TRANSCODE USECASE : Transcoding is started \n");

    while (FALSE == gDecodeUseCase_obj.thrExit)
    {
        OSA_waitMsecs(8);
        if(gDecodeUseCase_obj.eof == 0)
        {
            transcodeUseCase_requestBitStreamBuffer(&ipcBufList);

            if(ipcBufList.numBufs == 0)
                continue;

            for(i = 0; i < ipcBufList.numBufs; i++)
            {
                static int counter = 0;
                ipcBufList.bufs[i]->timeStamp = counter++;
                ipcBufList.bufs[i]->channelNum = 0;
                gDecodeUseCase_obj.bufCount++;
                if(gDecodeUseCase_obj.bufCount == gDecodeUseCase_obj.numFrames)
                {
                    ipcBufList.bufs[i]->isEOS = 1;
                }
                else
                {
                    ipcBufList.bufs[i]->isEOS = 0;
                }
            }

            transcodeUseCase_bitsRdReadData(&ipcBufList);
            transcodeUseCase_bitsRdSendFullBitBufs(&ipcBufList);
        }
    }

    OSA_printf("TRANSCODE USECASE : Transcoding is Done \n");

    return NULL;
}

/* ===================================================================
 *  @func     transcodeUseCase_requestBitStreamBuffer
 *
 *  @desc     function which is called to get an empty buffer
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_requestBitStreamBuffer(Bitstream_BufList *pIpcBufList)
{
    IpcBitsOutLinkHLOS_BitstreamBufReqInfo ipcReqInfo;

    ipcReqInfo.numBufs = 1;
    /*
     * This is required for the next link to proceed
     */
    ipcReqInfo.minBufSize[0] = transcodeStrCfg.inputWidth *
                               transcodeStrCfg.inputHeight;

    IpcBitsOutLink_getEmptyVideoBitStreamBufs(gVdecModuleContext.ipcBitsOutHLOSId,
                                              pIpcBufList,
                                              &ipcReqInfo,
                                              TIMEOUT_FOREVER);

    return 0;
}

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdSendFullBitBufs
 *
 *  @desc     passes a filled buffer to the next link
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdSendFullBitBufs(Bitstream_BufList *pIpcBufList)
{
    if(pIpcBufList->numBufs)
    {
        IpcBitsOutLink_putFullVideoBitStreamBufs(gVdecModuleContext.ipcBitsOutHLOSId,
                                                 pIpcBufList);
    }
    return 0;
}

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdReadData
 *
 *  @desc     this function is used for reading from sdcard
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 transcodeUseCase_bitsRdReadData(Bitstream_BufList *pIpcBufList)
{
    Int32 nSize = 0;
    Int32 i = 0;

    for ( i = 0 ; i < pIpcBufList->numBufs; i++)
    {
        fscanf(gDecodeUseCase_obj.fpRdStreamSize, "%d", &nSize);

        pIpcBufList->bufs[i]->fillLength = fread(pIpcBufList->bufs[i]->addr,
                                              1,
                                              nSize,
                                              gDecodeUseCase_obj.fpRdStream);

       if(pIpcBufList->bufs[i]->fillLength <= 0)
       {
           gDecodeUseCase_obj.eof = 1;
           return -1;
       }

       pIpcBufList->bufs[i]->startOffset = 0;

    }
    return 0;
}

/* ===================================================================
 *  @func     transcodeUseCase_bitsRdStart
 *
 *  @desc     This function will start the transcoding thread function
 *
 *  @modif    This function modifies no structures
 *
 *  @inputs   This funcion takes no inputs
 *
 *  @outputs  It outputs nothing
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Void transcodeUseCase_bitsRdStart()
{
    OSA_semSignal(&gDecodeUseCase_obj.thrStartSem);
}

/**
 ******************************************************************************
 *  @fn    generateH264SizeFile
 *  @brief function used to generate the h264 size file which
 *         describes size of each frame in the input bit stream
 *
 *  @param[in]  filename  : bitstream file whose size information to be generated
 *
 *  @return[out]  : Return the no of NAL unit in the h264 bit stream
 ******************************************************************************
 */
int generateH264SizeFile(char *filename)
{
    FILE *file ,*file2;
    unsigned int x, j, k;
    int numFrames = 0;

    int framesize = 0;
    int tempframesize = 0;
    int retValue  = 0;

    char Framesizefilename[1024];

    int stream[100048];
    int *Strptr;
    int new_nal = 1;

    unsigned int i         = 0;
    unsigned int Total     = 0;
    unsigned int bufferlen = 100000 ;

    prev_nal_type     = 9;
    prev_vcl_nal_flag = 0;

    file = fopen(filename, "rb" );

    bzero(Framesizefilename,1024);
    strcat(Framesizefilename, "/mnt/mmc/stream_size.txt");

    if(file == NULL)
    {
        printf("Error: can't open Input file.\n");
        exit(1);
    }
    else
    {
        printf(" Input file [%s] opened successfully !!!\n", filename);
    }

    if((file2 = fopen(Framesizefilename, "w+")) == NULL)
    {
        printf("Error: Cannot open Output file.\n");
        exit(1);
    }
    else
    {
        printf(" Output file [%s] opened successfully !!!\n", Framesizefilename);
    }

    x = getc( file );

    while( x != EOF )
    {
        stream[i++] = x;
        if (i >= bufferlen)
        {
            if ((retValue = H264VID_getFrameSize( stream ,bufferlen, new_nal)) == 0)
            {
                framesize += bufferlen;
                i = 0;
                new_nal = 0;
            }
            else
            {
                new_nal    = 1;
                framesize += retValue;
                Total     += framesize;

                fprintf(file2,"%d\n",framesize);
                numFrames++;

                for (j=retValue, k=0; j < bufferlen; j++, k++)
                {
                    stream[k] = stream[j];
                }

                i         = k;
                framesize = 0;
            }
        }

        x = getc( file );
    }

    bufferlen = i;
    Strptr    = stream;
    tempframesize = framesize;
    while ((framesize = H264VID_getFrameSize( Strptr ,bufferlen, new_nal)) != 0)
    {
	Total += (framesize + tempframesize);
        //       printf ("frame size( %d ) = %d \tTotal= 0x%x\n", index++, framesize, Total);
        fprintf(file2,"%d\n",(framesize + tempframesize));
        numFrames++;

        Strptr    += framesize;
        bufferlen -= framesize;
        tempframesize = 0;
    }
    bufferlen += tempframesize;
    Total += bufferlen;
    //    printf ("frame size( %d ) = %d \tTotal= 0x%x\n", index++, bufferlen, Total);
    fprintf(file2,"%d\n",bufferlen);

    fclose(file);
    fclose(file2);

    //    printf("File Closed\n");
    return numFrames + 1;
}

/**
 ******************************************************************************
 *  @fn    H264VID_getFrameSize
 *  @brief This function finds out each frame size of the given input bit
 *         stream.
 *
 *  @param[in]  stream    : Pointer to the bit stream
 *  @param[in]  bufferLen :
 *  @param[in]  new_nal   :
 *
 *  @return[out]  : None
 ******************************************************************************
 */
int H264VID_getFrameSize (int *stream, unsigned int bufferLen, int new_nal)
{
    unsigned int wordCount,nalLength,frameSize = 0;
    unsigned int loopCount;
    unsigned short int  firstMbInSliceFlag;
    unsigned short int NewNalType;
    int vcl_nal_flag;

    if (bufferLen < 8) return (0);

    if(new_nal)
    {
        prev_nal_type     = stream[4] & 0x1f;
        prev_vcl_nal_flag = ((prev_nal_type <=5) && (prev_nal_type > 0));
    }

    nalLength   = 0;

    for (wordCount = 4; wordCount < bufferLen; wordCount++)
    {
        loopCount = wordCount;

        if ((stream[loopCount++] == 0 && stream[loopCount++] == 0 &&
                    stream[loopCount++] == 0 && stream[loopCount++] == 0x01)  ||
                (stream[loopCount++] == 0 && stream[loopCount++] == 0 &&
                 stream[loopCount++] == 1))
        {
            NewNalType         = stream[loopCount++] & 0x1f;
            firstMbInSliceFlag = stream[loopCount++] & 0x80;
            vcl_nal_flag       = ((NewNalType <=5) && (NewNalType > 0));

            if(vcl_nal_flag && prev_vcl_nal_flag)
            {
                prev_nal_type     = NewNalType;
                prev_vcl_nal_flag = vcl_nal_flag;

                if( (firstMbInSliceFlag))
                    break;
            }

            if ((prev_vcl_nal_flag && !vcl_nal_flag))
            {
                prev_nal_type     = NewNalType;
                prev_vcl_nal_flag = vcl_nal_flag;

                break;
            }

            prev_nal_type     = NewNalType;
            prev_vcl_nal_flag = vcl_nal_flag;
        }

        nalLength++;
    }

    frameSize = nalLength + 4;

    if (wordCount >= bufferLen)
    {
        return (0);
    }
    else
    {
        return (frameSize);
    }

}
