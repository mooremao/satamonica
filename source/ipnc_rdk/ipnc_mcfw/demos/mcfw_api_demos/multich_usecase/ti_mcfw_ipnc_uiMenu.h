/** ==================================================================
 *  @file   ti_mcfw_ipnc_uiMenu.h
 *
 *  @path    ipnc_mcfw/demos/mcfw_api_demos/multich_usecase/
 *
 *  @desc   This  File contains.
 * ===================================================================
 *  Copyright (c) Texas Instruments Inc 2011, 2012
 *
 *  Use of this software is controlled by the terms and conditions found
 *  in the license agreement under which this software has been supplied
 * ===================================================================*/
/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

/**
  \file ti_mcfw_ipnc_api.h
  \brief  function related to ipnc mcfw main routine
  */

#ifndef IPNC_UI_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define IPNC_UI_H_

/* ===================================================================
 *  @func     App_ipncUi_frameRateMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_frameRateMenu();

/* ===================================================================
 *  @func     App_ipncUi_encBitRateMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encBitRateMenu();

/* ===================================================================
 *  @func     App_ipncUi_encEnableMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encEnableMenu();

/* ===================================================================
 *  @func     App_ipncUi_resolutionMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_resolutionMenu();

/* ===================================================================
 *  @func     App_ipncUi_displayMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_displayMenu();

/* ===================================================================
 *  @func     App_ipncUi_ivaFreqMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_ivaFreqMenu();

/* ===================================================================
 *  @func     App_ipncUi_sdMountMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_sdMountMenu();

/* ===================================================================
 *  @func     App_ipncUi_bitstreamDumpMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_bitstreamDumpMenu();

/* ===================================================================
 *  @func     App_ipncUi_histogramMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_histogramMenu();

/* ===================================================================
 *  @func     App_ipncUi_vnfMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_vnfMenu();

/* ===================================================================
 *  @func     App_ipncUi_encPrintParamMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encPrintParamMenu();

/* ===================================================================
 *  @func     App_ipncUi_sensorStreamingMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_sensorStreamingMenu();

/* ===================================================================
 *  @func     App_ipncUi_stillCaptureMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_stillCaptureMenu();

/* ===================================================================
 *  @func     App_ipncUi_mirrorMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_mirrorMenu();

/* ===================================================================
 *  @func     App_ipncUi_sysUseCaseMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_sysUseCaseMenu();

/* ===================================================================
 *  @func     App_ipncUi_aewbMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_aewbMenu();

/* ===================================================================
 *  @func     App_ipncUi_encRealFpsMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_encRealFpsMenu();

/* ===================================================================
 *  @func     App_ipncUi_getSensorInfo
 *
 *  @desc     it is called from the UART menu to get the sensor
 *            information
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes no inputs
 *
 *  @outputs  This function doesnt give any output
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncUi_getSensorInfo();

/* ===================================================================
 *  @func     App_ipncUi_simcopMenu
 *
 *  @desc     it is called from the UART menu to get the SIMCOP menu
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes no inputs
 *
 *  @outputs  This function doesnt give any output
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncUi_simcopMenu();

/* ===================================================================
 *  @func     App_ipncUi_flickerMenu
 *
 *  @desc     it is called from the UART menu to get the FLICKER menu
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes no inputs
 *
 *  @outputs  This function doesnt give any output
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncUi_flickerMenu();

/* ===================================================================
 *  @func     App_ipncUi_mainMenu
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncUi_mainMenu();

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif  
                                                   /* IPNC_UI_H_ */
												   
