/*==============================================================================
 * @file:       ti_mcfw_ipnc_api.c
 *
 * @brief:      Video capture mcfw function definition.
 *
 * @vers:       0.5.0.0 2011-06
 *
 *==============================================================================
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

//#define SDCARD_1

#include "ti_vsys.h"
#include "ti_vcam.h"
#include "ti_venc.h"
#include "ti_vdec.h"
#include "ti_vdis.h"
#include "ti_mcfw_ipnc_api.h" 
#include <mcfw/src_linux/mcfw_api/ti_vsys_priv.h>

/*
 *	Defined
 */

//#define PRINT_TS 

Bool gSdCardMounted = FALSE;
BitstreamDumpCtx_t gBitstreamDumpCtx = {.dumpStarted = FALSE,
										.mpeg4VolSaved = 0};
StillCaptureCtx_t gStillCaptureCtx = {.numCap = 1,
                                      .capNum = ~0x0};
EncFps_t gEncFps[3];
									  
/* ===================================================================
 *  @func     App_ipncApi_init
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_init()
{
	Int32 status;

	/* Create mutex for Bit stream dump */
    status = OSA_mutexCreate(&gBitstreamDumpCtx.lock);
    UTILS_assert(status == OSA_SOK);	
	
	/* Create Semaphore for Still Capture */
	OSA_semCreate(&gStillCaptureCtx.done,1,0);
	
	/* Init the Enc FPS structure */
	App_ipncApi_resetEncRealFpsPrm();
	
	return status;
} 

/* ===================================================================
 *  @func     App_ipncApi_deInit
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_deInit()
{
	Int32 status;

	/* Delete mutex used for Bitstream dump */
    status = OSA_mutexDelete(&gBitstreamDumpCtx.lock);
    UTILS_assert(status == OSA_SOK);

	/* Delete Semaphore used for Still Capture */
	OSA_semDelete(&gStillCaptureCtx.done);
	
	return status;	
}

/* ===================================================================
 *  @func     App_ipncApi_changeFrameRate
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */

Int32 App_ipncApi_changeFrameRate(UInt32 chId,
								  UInt32 frameRate)
{
	Int32 ret;
	VENC_CHN_DYNAMIC_PARAM_S params = {0};
	VCAM_CHN_DYNAMIC_PARAM_S params_camera = {0};

	/* Change Camera Frame Rate */
	params_camera.frameRate = (frameRate * 1000);
	ret = Vcam_setDynamicParamChn(chId,&params_camera,VCAM_FRAMERATE);
	
	/*
	 *	The camera link returns back the actual frame rate set.
	 *	The returned frame rate value should be used for the encoder.
	 */
	
	/* Change Encoder Frame Rate */
	params.frameRate = params_camera.frameRate/1000;
	Venc_setInputFrameRate(chId,params.frameRate);
	Venc_setDynamicParam(chId,0,&params,VENC_FRAMERATE);
	
	/*
	 *	IP ratio is made equal to framerate so that we have an IDR
	 *  frame every second.
     */	 
	params.intraFrameInterval = params_camera.frameRate/1000; 
	Venc_setDynamicParam(chId,0,&params,VENC_IPRATIO); 
	
	return ret;
} 
 
/* ===================================================================
 *  @func     App_ipncApi_changeEncBitRate
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeEncBitRate(UInt32 chId,
								   UInt32 bitRate)
{
	Int32 ret;
	VENC_CHN_DYNAMIC_PARAM_S params = {0};

	params.targetBitRate = bitRate;
	ret = Venc_setDynamicParam(chId,0,&params,VENC_BITRATE);
	
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_changeMJPEGQualityFactor
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeMJPEGQualityFactor(UInt32 chId,UInt32 qualityFactor)
{
	Int32 ret = 0;
	VENC_CHN_DYNAMIC_PARAM_S params = {0};
	
	if(MultiCh_getCodecType(chId) == IVIDEO_MJPEG)
	{
		params.rateControl = RATECONTROL_OFF;
		ret = Venc_setDynamicParam(chId, 	// vencChnId
								   0, 		// vencStrmID
								   &params, 
								   VENC_RATECONTROL);
		
		params.qpMin  = 1;
		params.qpMax  = 51;
		params.qpInit = qualityFactor;
		ret |= Venc_setDynamicParam(chId, 	// vencChnId
									0, 		// vencStrmID
									&params, 
									VENC_QPVAL_I);	
	}							
	
	return ret;
} 

/* ===================================================================
 *  @func     App_ipncApi_changeEncType
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeEncType(UInt32 chId,
								VCODEC_TYPE_E codecType,
								Int32 encPreset)
{
	Int32 ret;
	
	ret = Venc_switchCodecAlgCh(chId,codecType,encPreset);
	
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_enableEnc
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_enableEnc(UInt32 chId,Bool enable)
{	
	Int32 ret;

	if(enable == TRUE)
		ret = Venc_enableChn(chId);
	else
		ret = Venc_disableChn(chId);
		
	return ret;	
}

/* ===================================================================
 *  @func     App_ipncApi_printEncParams
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_printEncParams(UInt32 chId)
{
	Int32 ret;
	VENC_CHN_DYNAMIC_PARAM_S vencDynPrm;
	
	ret = Venc_getDynamicParam(chId,		// vencChnId
							   0,			// vencStrmID
							   &vencDynPrm,	// ptEncDynamicParam
							   VENC_ALL);	// veParamId
							   
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_changeMuxMap
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeMuxMap(VSYS_MUX_IN_Q_CHAN_MAP_INFO_S *pMapPrm)
{
	Int32 ret;

	ret = Vsys_setMuxQChanMap(gVsysModuleContext.muxId,pMapPrm);
	
	return ret;
}		

/* ===================================================================
 *  @func     App_ipncApi_changeResolution
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeResolution(UInt32 chId,
								   UInt32 width,
								   UInt32 height)
{
	Int32 ret;
	VCAM_CHN_DYNAMIC_PARAM_S camChnDynaParam;
	
    camChnDynaParam.InputWidth  = width;
    camChnDynaParam.InputHeight = height;
	
	ret = Vcam_setDynamicParamChn(chId,&camChnDynaParam,VCAM_RESOLUTION);
	
	return ret;
}		

/* ===================================================================
 *  @func     App_ipncApi_displayEnable
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_displayEnable(Bool disEnable)
{
	Int32 ret;

	if(disEnable == 1)
		ret = Vdis_startDevAll();
	else if(disEnable == 0)
             ret = Vdis_stopDevAll();	
		 else 	 
		     ret = 0;
			 
	return ret;		 
} 

/* ===================================================================
 *  @func     App_ipncApi_changeIvaFreq
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_changeIvaFreq(UInt32 ivaFreq)
{
	Int32 ret;
	VSYS_IVA_FREQ_VOLTAGE_S ivaFreqPrm;
	
	ivaFreqPrm.iva     = 0;
	ivaFreqPrm.freq    = ivaFreq;
	ivaFreqPrm.voltage = VSYS_IVA_VOLTAGE_HIGH;	// currently not used
	
	ret = Vsys_setIVAFreqVoltage(&ivaFreqPrm);
	
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_startBitstreamDump
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_startBitstreamDump(Int8 *pFileName)
{
	if(gBitstreamDumpCtx.dumpStarted == FALSE)
	{
		strcpy(gBitstreamDumpCtx.fileName,pFileName);
		gBitstreamDumpCtx.fp = fopen(pFileName,"wb");	
		
		if(gBitstreamDumpCtx.fp != NULL)
		{
			printf("IPNCAPI: Started Bitstream Dump into file %s !!!\n",pFileName);
			gBitstreamDumpCtx.dumpStarted = TRUE;
		}	
		else	
		{
			printf("IPNCAPI: File open(%s) failed !!!\n",pFileName);
			gBitstreamDumpCtx.dumpStarted = FALSE;
		}

		gBitstreamDumpCtx.dumpCnt            = 0;
		gBitstreamDumpCtx.jpgCnt             = 0;
		gBitstreamDumpCtx.firstKeyFrameFound = 0;
        gettimeofday(&(gBitstreamDumpCtx.startTime), NULL);
	}	
	
	/* AEWB Stabilization */
	if(gBitstreamDumpCtx.dumpStarted == TRUE)
	{
		/*
		 *	This API does AEWB stabilization and starts sensor streaming
         */  		 
		Vcam_aewbStabilization(AEWB_STAB_MODE,
							   AEWB_STAB_SENSOR_WIDTH,
                               AEWB_STAB_SENSOR_HEIGHT,
							   AEWB_STAB_WIDTH,
                               AEWB_STAB_HEIGHT,
                               AEWB_STAB_PITCH, 
							   AEWB_STAB_FRAMERATE,
							   AEWB_STAB_CNT);		
							   
		/* Force Next frame to be IDR frame */
		Venc_forceIDR(0,0);	
	}	
	
	return 0;
} 

/* ===================================================================
 *  @func     App_ipncApi_dumpBitstreamDump
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_dumpBitstreamDump(Int8 *addr,UInt32 size,UInt32 isKeyFrame,UInt32 codingType,UInt32 timeStamp)
{
	UInt32 writeSize;
	Int8 *pData,*pCur;
	UInt32 cnt;
	
	OSA_mutexLock(&gBitstreamDumpCtx.lock);
	
	/*
	 *	MPEG4 bitstream requires the first frame to contain VOL header.
	 *  The VOL header is saved into a buffer.
	 */
	if((gBitstreamDumpCtx.mpeg4VolSaved == 0) &&
	   (codingType == IVIDEO_MPEG4SP) &&
	   (isKeyFrame == 1))
	{
		pData = addr;
	
		for(cnt = 0; cnt < 256; cnt++)
		{
			pCur = (Int8 *)pData + cnt;

			if ((*pCur == 0) &&
				(*(pCur + 1) == 0) &&
				(*(pCur + 2) == 0) && 
				(*(pCur + 3) == 1) && 
				(*(pCur + 4) == 0x25))
			{				
				break;
			}

			if ((*pCur == 0) &&
				(*(pCur + 1) == 0) && 
				(*(pCur + 2) == 1) && 
				(*(pCur + 3) == 0xB6))
			{
				break;
			}
		}		
	
		memcpy(gBitstreamDumpCtx.mpeg4Vol,addr,cnt);
		gBitstreamDumpCtx.mpeg4VolSize  = cnt;
		gBitstreamDumpCtx.mpeg4VolSaved = 1;			
	}
	

	if(gBitstreamDumpCtx.dumpStarted == TRUE)
	{
	  if(gBitstreamDumpCtx.firstKeyFrameFound == 0)
		{
			if(isKeyFrame == 1)
			{
				gBitstreamDumpCtx.firstKeyFrameFound = 1;	
				
				/*
				 *	Copy MPEG4 VOL for the first frame
				 */
				if(codingType == IVIDEO_MPEG4SP)
				{
					writeSize = fwrite(gBitstreamDumpCtx.mpeg4Vol,
					                   1,
									   gBitstreamDumpCtx.mpeg4VolSize,
									   gBitstreamDumpCtx.fp);	
					if(writeSize != gBitstreamDumpCtx.mpeg4VolSize)	
					{
						printf("IPNCAPI: File write failed for the file %s\n",gBitstreamDumpCtx.fileName);
						fclose(gBitstreamDumpCtx.fp);
						gBitstreamDumpCtx.dumpStarted = FALSE;
					}	
				}
			}
		}

    if(gBitstreamDumpCtx.firstKeyFrameFound == 1)
		{
			writeSize = fwrite(addr,1,size,gBitstreamDumpCtx.fp);
			if(writeSize != size)
			{
				printf("IPNCAPI: File write failed for the file %s\n",gBitstreamDumpCtx.fileName);
				fclose(gBitstreamDumpCtx.fp);
				gBitstreamDumpCtx.dumpStarted = FALSE;
			}
			else
			{
				gBitstreamDumpCtx.dumpCnt ++;
			}
#ifdef PRINT_TS			
			printf("IPNCAPI: Bitstream Dump: Frame # = %d,Time Stamp = %d\n",gBitstreamDumpCtx.dumpCnt,timeStamp);
#endif			
		}
	}

	OSA_mutexUnlock(&gBitstreamDumpCtx.lock);
	
	return 0;
} 

void App_ipncApi_dumpJpg(Int8 *addr, UInt32 size)
{
	char buf[64];
	FILE *filet;

	if(!gBitstreamDumpCtx.dumpStarted) return;

	sprintf(buf,"/mnt/mmc/mjpg/%08d.JPG",gBitstreamDumpCtx.jpgCnt);
	filet = fopen(buf, "wb");
	if(!filet) return;
	fwrite(addr,1,size,filet);
	fclose(filet);
	gBitstreamDumpCtx.jpgCnt++;
}



/* ===================================================================
 *  @func     App_ipncApi_stopBitstreamDump
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stopBitstreamDump()
{
    OSA_mutexLock(&gBitstreamDumpCtx.lock);
    struct timeval curTime;
    if(gBitstreamDumpCtx.dumpStarted == TRUE)
    {
        fclose(gBitstreamDumpCtx.fp);
        gBitstreamDumpCtx.dumpStarted = FALSE;		
        gettimeofday(&curTime, NULL); 
        printf("IPNCAPI: Stopped Bitstream Dump into file %s,frames dumped = %d "
               "in %ld sec !!!\n",
        gBitstreamDumpCtx.fileName,gBitstreamDumpCtx.dumpCnt,
               (curTime.tv_sec - gBitstreamDumpCtx.startTime.tv_sec) + 
               ((curTime.tv_usec - gBitstreamDumpCtx.startTime.tv_usec) / 1000000));
		   
        /* Stop Sensor streaming */
        App_ipncApi_enableSensorStreaming(FALSE);	
    }
    OSA_mutexUnlock(&gBitstreamDumpCtx.lock);

    return 0;
}

/* ===================================================================
 *  @func     App_ipncApi_mountSDCard
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_mountSDCard(Bool mount,Bool format)
{
	Int32 ret = 0;
	
	if(format == TRUE)
	{
#ifndef SDCARD_1
		system("mkdosfs /dev/mmcblk0p1\n");
		system("mkdosfs /dev/mmcblk0\n");
#else
		system("mkdosfs /dev/mmcblk1p1\n");
		system("mkdosfs /dev/mmcblk1\n");
#endif
	}
	
	if(mount == TRUE)
	{
		if(gSdCardMounted == FALSE)
		{
#ifndef SDCARD_1
			ret = system("mount /dev/mmcblk0p1 /mnt/mmc/\n");
			ret = system("mount /dev/mmcblk0 /mnt/mmc\n");
#else
			ret = system("mount /dev/mmcblk1p1 /mnt/mmc/\n");
			ret = system("mount /dev/mmcblk1 /mnt/mmc\n");
#endif
		}	
	}	
	else
	{
		if(gSdCardMounted == TRUE)
		{
			ret = system("umount /mnt/mmc\n");	
		}	
	}	
		
	if(ret != 0)
	{
	    printf("IPNCAPI: SD card mount/unmount FAILED !!!\n");
		gSdCardMounted = FALSE;
	}	
	else
	{
		printf("IPNCAPI: SD card mount/unmount PASSED !!!\n");
		gSdCardMounted = TRUE;
	}	
	
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_histogramEnable
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_histogramEnable(Bool enable)
{
	Int32 ret;
	Vsys_swOsdPrm swosdGuiPrm;
	
	swosdGuiPrm.histEnable = enable;
    ret = Vsys_setSwOsdPrm(VSYS_SWOSDHISTEN,&swosdGuiPrm);
	
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_vnfOnOff
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_vnfOnOff(UInt32 chId,Bool onOff)
{
	Int32 ret;
	
	if(onOff == TRUE)
		ret = Vsys_enableForwardVnf(chId,FALSE);
	else
		ret = Vsys_enableForwardVnf(chId,TRUE);
	
	return ret;
}  

/* ===================================================================
 *  @func     App_ipncApi_getH3AData
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_getH3AData(UInt32 *pAfData,UInt32 *pAewbData)
{
	return (Vcam_getH3AData(pAfData,pAewbData));	
}

/* ===================================================================
 *  @func     App_ipncApi_enableSensorStreaming
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_enableSensorStreaming(UInt32 enable)
{
	return (Vcam_enableSensorStreaming(enable));
}

/* ===================================================================
 *  @func     App_ipncApi_stillCaptureEnter
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stillCaptureEnter()
{
	Int32 ret = 0;

	/* 
	 * 	1.Disable Sensor streaming 
	 */
	Vcam_enableSensorStreaming(FALSE);

	/*
	 *	2.Stll Capture Mode Entry for ISS driver 
	 */
	Vcam_enterStillCapture(AEWB_STAB_WIDTH,
						AEWB_STAB_HEIGHT,
						AEWB_STAB_FRAMERATE,
						STILL_CAPTURE_FRAMERATE);
	
	/*
	 *	3.The system usecase is changed to Still Capture mode
	 *    This will create the chain for Still Capture resolution,
	 *    so that all the buffers are allocated for Still Capture resolution
	 */
	App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
								 STILL_CAPTURE_MODE);	 
	
	/*
	 *	4.This will change the Camera mode to lower IDLE mode resolution
	 *    The sensor and the ISS driver are changed to lower IDLE mode resolution
	 *    AEWB stabilization before Still Capture will happen with this lower resolution
	 */
	Vcam_changeCameraMode(AEWB_STAB_MODE,
						  AEWB_STAB_SENSOR_WIDTH,
						  AEWB_STAB_SENSOR_HEIGHT,
						  AEWB_STAB_WIDTH,
						  AEWB_STAB_HEIGHT,
						  AEWB_STAB_PITCH);
		
	return ret;
} 

/* ===================================================================
 *  @func     App_ipncApi_stillCaptureExit
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stillCaptureExit()
{
	Int32 ret = 0;
	
	/*
	 *	1.Still Capture Mode exit for ISS driver
	 */
	Vcam_exitStillCapture();
	
	/* 
	 *	2.The System Usecase is changed to 1080P wide angle mode
	      on Still Capture Mode Exit
	 */
#ifdef FACTORY_TEST
	App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                     SYSTEM_STD_1080P_3840_2160_60);
#else
	App_ipncApi_setSystemUseCase(VSYS_USECASE_TOMTOM,
                                     SYSTEM_STD_1080P_4608_2592_60);
#endif

	/* Reset Enc Real time FPS params */
	App_ipncApi_resetEncRealFpsPrm();
 
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_dumpStillCapture
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_dumpStillCapture(Int8 *addr,UInt32 size)
{
	Int32 ret = 0;
	FILE *fp;
	Int8 fileName[128];
	UInt32 writeSize;

	if(gStillCaptureCtx.capNum < gStillCaptureCtx.numCap)
	{
#ifdef FACTORY_TEST
			sprintf(fileName,"/tmp/still_%d.jpg",gStillCaptureCtx.capNum);
#else
			sprintf(fileName,"/mnt/mmc/still_%d.jpg",gStillCaptureCtx.capNum);
#endif
			fp = fopen(fileName,"wb");
			if(fp != NULL)
			{
				writeSize = fwrite(addr,1,size,fp);
				if(writeSize != size)
				{
					printf("IPNCAPI: File write failed for %s !!!\n",fileName);
				}

				fclose(fp);	
			}
			else
			{
				printf("IPNCAPI: File open(%s) failed !!!\n",fileName);
			}
				
		gStillCaptureCtx.capNum ++;
	}	
	
	if(gStillCaptureCtx.capNum == gStillCaptureCtx.numCap)
	{
		gStillCaptureCtx.capNum = ~0x0;
		
		/* Post the done semaphore */
		OSA_semSignal(&gStillCaptureCtx.done);
	}
	
	return ret;
} 

/* ===================================================================
 *  @func     App_ipncApi_stillCaptureStart
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_stillCaptureStart(UInt32 numCap)
{
	Int32 ret = 0;
	struct timeval start,end;
	UInt32 timeInterval;

	gStillCaptureCtx.numCap = numCap;
	gStillCaptureCtx.capNum = 0;
	
	gettimeofday(&start, NULL);
	
	/* 1. Start the Still Capture in Camera Link */
	Vcam_startStillCapture(STILL_CAPTURE_MODE,
						   STILL_CAPTURE_SENSOR_WIDTH,
						   STILL_CAPTURE_SENSOR_HEIGHT,
						   STILL_CAPTURE_WIDTH,
						   STILL_CAPTURE_HEIGHT,
						   STILL_CAPTURE_PITCH,
						   TRUE,
						   numCap);	
	
	/* 2. Wait for still capture to complete */
	OSA_semWait(&gStillCaptureCtx.done,OSA_TIMEOUT_FOREVER);
	
	/* 3. Disable the sensor streaming */
	Vcam_enableSensorStreaming(FALSE);

	/*Work around for factory test to work*/
	Vcam_enableChannel(0);
	Vcam_enableChannel(1);

	/* 4. Change the Camera mode back to AEWB Stabilization Mode */
	Vcam_changeCameraMode(AEWB_STAB_MODE,
						  AEWB_STAB_SENSOR_WIDTH,
						  AEWB_STAB_SENSOR_HEIGHT,
						  AEWB_STAB_WIDTH,
						  AEWB_STAB_HEIGHT,
						  AEWB_STAB_PITCH);
	
	gettimeofday(&end, NULL);
	timeInterval = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec)/1000;
	
	printf("IPNCAPI: Still Capture time = %d msec\n",timeInterval);
	
	return ret;
} 

/* ===================================================================
 *  @func     App_ipncApi_setMirrorMode
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setMirrorMode(UInt32 chId,IPNCAPI_MIRROR_MODE mirrorMode)
{
	Int32 ret;
	VCAM_CHN_DYNAMIC_PARAM_S params = {0};
	
	params.mirrorMode = (UInt32)mirrorMode;
	ret = Vcam_setDynamicParamChn(chId, 
								  &params, 
								  VCAM_MIRROR_MODE);
								  
	return ret;							  
} 

/* ===================================================================
 *  @func     App_ipncApi_systemUseTiler
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
UInt32 App_ipncApi_systemUseTiler(SYSTEM_Standard camStandard)
{
        /*****************************************************************
         * Set the below variable to enable TILER. Disabling TILER as    *
         * 4K and 2.7k video resolutions fail with TILER ON              *
         *****************************************************************/
        UInt32 systemUseTiler = 0;
	
	if((camStandard == SYSTEM_STD_4664_3496_10) ||
	   (camStandard == SYSTEM_STD_4656_3492_10))
	{
		systemUseTiler = 0;		
	}
	
	return systemUseTiler;
}

/* ===================================================================
 *  @func     App_ipncApi_setSystemUseCase
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setSystemUseCase(VSYS_USECASES_E systemUseCase,
								   SYSTEM_Standard camStandard)
{
	Int32 ret = 0;
	struct timeval start,end;
	UInt32 timeInterval;
	
	gettimeofday(&start, NULL);

	/* 1.	Set the camera standard */
	Vsys_setCamStandard(camStandard);	
	Vsys_sysUseCaseChangeOn(TRUE);
	
	/* 2.	Disable Sensor Streaming */
	Vcam_enableSensorStreaming(FALSE);
	
	/* 3.	Stop the current usecase and deinit the VPSS links */
	App_stopUsecase();

	App_startUsecase((UInt32)systemUseCase);
	
	Vsys_sysUseCaseChangeOn(FALSE);
	
	gettimeofday(&end, NULL);
	timeInterval = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec)/1000;
	
	/* Reset Enc Real time FPS params */
	App_ipncApi_resetEncRealFpsPrm();
	
	printf("IPNCAPI: System Usecase Change time = %d msec\n",timeInterval);	

	OSA_waitMsecs(20);
	
	return ret;
} 

/* ===================================================================
 *  @func     App_ipncApi_setAewbMode
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setAewbMode(AEWB_MODE aewbMode)
{
	Int32 ret = 0;
	VCAM_CHN_DYNAMIC_PARAM_S params = { 0 };
	
	params.aewbMode = (UInt32)aewbMode;
    Vcam_setDynamicParamChn(0, 
							&params, 
							VCAM_AEWB_MODE);
	
	return ret;
} 

/* ===================================================================
 *  @func     App_ipncApi_resetEncRealFpsPrm
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_resetEncRealFpsPrm()
{
	Int32 ret = 0;
	UInt32 i;
	
	/* Init the Enc FPS structure */
	for(i = 0;i < 3;i++)
	{
		gEncFps[i].frameCnt       = 0;
		gEncFps[i].startTimeStamp = 0;
		gEncFps[i].realFps        = 0.0;
	}	
	
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_setFrameTimeStamp
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_setFrameTimeStamp(UInt32 chId,UInt32 timeStamp)
{
	Int32 ret = 0;
	UInt32 timeInterval;
	float timePerFrame;
	
	gEncFps[chId].frameCnt ++;
	
	gEncFps[chId].curTimeStamp = timeStamp;
	
	if(gEncFps[chId].frameCnt == 1)
	{
		gEncFps[chId].startTimeStamp = timeStamp;
	}	
		
	if(gEncFps[chId].frameCnt == FPS_CALC_FRAME_CNT)	
	{
		if(timeStamp > gEncFps[chId].startTimeStamp)
		{			
			timeInterval = (timeStamp - gEncFps[chId].startTimeStamp);
			timePerFrame = (float)timeInterval/(FPS_CALC_FRAME_CNT - 1);
			gEncFps[chId].realFps = (float)1000/timePerFrame;	
		}
		
		gEncFps[chId].frameCnt = 0;
	}
		
	return ret;
} 

/* ===================================================================
 *  @func     App_ipncApi_getEncRealFps
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
float App_ipncApi_getEncRealFps(UInt32 chId)
{
	UInt32 timeInterval;
	float timePerFrame;

	if(gEncFps[chId].realFps == 0.0)
	{
		timeInterval = (gEncFps[chId].curTimeStamp - gEncFps[chId].startTimeStamp);
		timePerFrame = (float)timeInterval/(gEncFps[chId].frameCnt - 1);
		gEncFps[chId].realFps = (float)1000/timePerFrame;	
	}

	return (gEncFps[chId].realFps);
} 

/* ===================================================================
 *  @func     App_ipncApi_getWbGains
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
Int32 App_ipncApi_getWbGains(UInt32 *pRGain,
                             UInt32 *pGrGain,
							 UInt32 *pGbGain,
							 UInt32 *pBGain)
{
	Int32 ret = 0;
	
	ret = Vcam_getWbGains(pRGain,pGrGain,pGbGain,pBGain);
	
	return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_getSensorInfo
 *
 *  @desc     This function calls the MCFW Vcam Api to get the Sensor
 *            frame Rate
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   This function takes the following inputs
 *            pSensorInfo
 *            This is the paramaeter through which it returns the
 *            Sensor Information
 *
 *  @outputs  VCAM_SENSOR_INFO_S *pSensorInfo
 *            contains the information related to sensor like fps etc
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_getSensorInfo(VCAM_SENSOR_INFO_S *pSensorInfo)
{
    Int32 ret = 0;

    Vcam_getSensorInfo(pSensorInfo);

    return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_setSimcopOn
 *
 *  @desc     This function calls the MCFW VSYS Api to set the SIMCOP
 *            to ON State
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   It doesnt take any inputs
 *
 *  @outputs  It doesnt give any outputs
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_setSimcopOn()
{
    Int32 ret = 0;

    Vsys_setSimcopOn();    

    return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_setSimcopOff
 *
 *  @desc     This function calls the MCFW VSYS api to set the SIMCOP
 *            to OFF state
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   It doesnt take any inputs
 *
 *  @outputs  It doesnt give any outputs
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_setSimcopOff()
{
    Int32 ret = 0;

    Vsys_setSimcopOff();

    return ret;
}

/* ===================================================================
 *  @func     App_ipncApi_setFlickerFrequency
 *
 *  @desc     This function calls the MCFW VCAM api to set the frequency
 *            of disabling flicker
 *
 *  @modif    It doesnt modify any structures
 *
 *  @inputs   It doesnt take any inputs
 *
 *  @outputs  It doesnt give any outputs
 *
 *  @return   Return 0 if success
 *  ==================================================================
 */
Int32 App_ipncApi_setFlickerFrequency(UInt32 flicker)
{
    Int32 ret = 0;
    VCAM_CHN_DYNAMIC_PARAM_S params = { 0 };

    if (flicker == 0)
    {
        params.env50_60hz = VOLTAGE_STANDARD_60HZ;
    }
    else if (flicker == 1)
    {
        params.env50_60hz = VOLTAGE_STANDARD_50HZ;
    }
    else
    {
        params.env50_60hz = VOLTAGE_STANDARD_60HZ;
    }
    Vcam_setDynamicParamChn(0, &params, VCAM_ENV_50_60HZ);

    return ret;
}
