#!/bin/sh

set -e

# Load VPSS-M3 firmware
/opt/ipnc/bin/fw_load.out startup VPSS-M3 /opt/ipnc/firmware/ipnc_rdk_fw_m3vpss.xem3

# Enable M3 suspend after firmware is loaded
echo 1 > /sys/devices/platform/dm38x_vpss_power/suspend
