// TomTom Video platform API
// Web server's service implementation. Relies on HTTP.
// Nebojsa Sumrak 2014.

#pragma once

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <memory.h>
#include "debug.h"
#include "tttime.h"
#include "http.h"
#include "viewfinder.h"
#include "avifilereader.h"

#define DCIM_CAMERA_DIR_PATH "/mnt/mmc"

class TTweb:public GBhttp {
 public:
	static bool doSetImg(const char *entry, int val) {
		static const struct {
			const char *e;
			ttv_feature_t f;
		} conv[] = {
			{
			"saturation", TTV_IMG_SATURATION,}, {
			"brightness", TTV_IMG_BRIGHTNESS,}, {
			"contrast", TTV_IMG_CONTRAST,},
			    //{ "hue", TTV_IMG_HUE, },
			{
			"sharpness", TTV_IMG_SHARPNESS,}, {
			"ae", TTV_IMG_AE,}, {
			"awb", TTV_IMG_AWB,}, {
			"ae_time", TTV_IMG_AE_TIME,}, {
			0, TTV_FEATURE_MAX}
		};
		for (int i = 0; conv[i].e; i++)
			if (!strcasecmp(conv[i].e, entry))
				return ttv_set_feature(conv[i].f, val);

		return false;
	}

	virtual bool process(httpReq & request, char *dirName, const char *entryName) {
		debuglog(5, "http access: dir='%s' entry='%s'", dirName, entryName);

		if (!strcmp(dirName, "/thumbs")) {
			const char *i = getVariable(request, "i");
			if (!i)
				return false;
			debuglog(5, "sending thumbnail '%s' i=%s", entryName, i);
			TTaviReader avireader;
			char jpgbuf[48000];
			unsigned size = sizeof(jpgbuf);
			if (!avireader.open(atoi(entryName))) {
				debuglog(5, "can't open file %s", entryName);
				return false;
			}
			if (!avireader.read_thumb(atoi(i), jpgbuf, size)) {
				debuglog(5, "no thumb %s in file %s", i, entryName);
				return false;
			}
			setContentType(request, "image/jpeg");
			sendHeaders(request, size, 1L);
			::write(request.m_clientsock, jpgbuf, size);
			return true;
		} else if (!strcmp(dirName, "/videos")) {
			debuglog(5, "sending video '%s'", entryName);
			return sendFile(request, "/mnt/mmc/", entryName);
		} else if (!strcmp(dirName, "/img")) {
			const char *v = getVariable(request, "val");
			int val = v ? atoi(v) : -1;
			debuglog(4, "image property '%s'='%d'", entryName, val);
			setContentType(request, "text/plain");
			addNoCacheHeaders(request);
			hprintf(request, "<%s/>", doSetImg(entryName, val) ? "ok" : "error");
			return true;
		} else if (!strcmp(dirName, "/")) {
			if (!strcasecmp(entryName, "favicon.ico")) {
#include "gfx/tomtom-favicon.c"
				sendHeaders(request, sizeof(tomtom_favicon), 1L);
				::write(request.m_clientsock, tomtom_favicon, sizeof(tomtom_favicon));
				return true;
			} else if (!strcasecmp(entryName, "tomtom-logo.jpg")) {
#include "gfx/tomtom-logo.c"
				setContentType(request, "image/jpeg");
				sendHeaders(request, sizeof(tomtom_logo_jpgdata), 1L);
				::write(request.m_clientsock, tomtom_logo_jpgdata, sizeof(tomtom_logo_jpgdata));
				return true;
			} else if (!strncmp(entryName, "recordbutton", 12)) {
#include "gfx/tomtom-record.c"
#include "gfx/tomtom-stop.c"
				setContentType(request, "image/png");
				addNoCacheHeaders(request);
				if (isRecording) {
					sendHeaders(request, sizeof(tomtom_stop), 1L);
					::write(request.m_clientsock, tomtom_stop, sizeof(tomtom_stop));
				} else {
					sendHeaders(request, sizeof(tomtom_record), 1L);
					::write(request.m_clientsock, tomtom_record, sizeof(tomtom_record));
				}
				return true;
			} else if (!strncmp(entryName, "dorecord", 8)) {
				setContentType(request, "text/plain");
				addNoCacheHeaders(request);
				hprintf(request, "<ok/>");
				keys.inject(TTKEY_WEB_RECSTOP);
				return true;
			} else if (!strncmp(entryName, "viewfinder", 10)) {
				void *address;
				unsigned int size;
				if (!viewfinder.getframe(address, size))
					return false;
				addNoCacheHeaders(request);
				setContentType(request, "image/jpeg");
				sendHeaders(request, size, 1L);
				::write(request.m_clientsock, address, size);
				return true;
			} else if (!strcmp(entryName, "showvf.html")) {
				setContentType(request, "text/html");
				hprintf(request,
					"<!DOCTYPE html>\n<html><head><script>\nacount=0;\nbcount=0;\nfunction loadRecBut(){document.getElementById(\"recb\").src=\"recordbutton#\"+bcount;bcount++;setTimeout(loadRecBut,1000);}\nsetTimeout(loadRecBut,1000);\nfunction recPress() {var xmlHttp=new XMLHttpRequest();\nxmlHttp.open(\"GET\",\"dorecord#\"+bcount,true);\nxmlHttp.send();\nbcount++;\n}\nfunction loadImage(){document.getElementById(\"vf\").src=\"viewfinder#\"+acount;acount++;}\n");
				hprintf(request,
					"function set(a,val) {\ndocument.querySelector('#'+a+'v').value = val;\nvar xmlHttp=new XMLHttpRequest();\nxmlHttp.open(\"GET\",\"/img/\"+a+\"?val=\"+val+\"&c=\"+bcount,false);\nxmlHttp.send();\nbcount++;\n}\nfunction showBox() {var mb=document.getElementById('mybox');\nif(mb.style.display=='none') {\nmb.style.display='block';\nmb.style.left=(window.document.body.clientWidth-mb.offsetWidth)+'px';\nmb.style.top=(document.getElementById('par').offsetTop-10)+'px'; } else mb.style.display='none';\n}\n</script>");
				hprintf(request,
					"<title>TomTom Longbeach Camera</title>\n<style type=\"text/css\">\n.thumb {width:180px;padding=10px;border-style:dotted;border-width:1px;margin:5px;float:left;font-familiy: Tahoma,Geneva,sans-serif; font-size: 12px;}\n</style></head>\n<body color=\"#ffffff\">\n");
				hprintf(request,
					"<h1 style=\"font-family: Tahoma,Geneva,sans-serif; font-size: 24px;\"><table border=\"0\" width=\"100%\"><tr><td align=\"left\" width=\"190\"><img src=\"tomtom-logo.jpg\" border=\"0\" width=\"188\" align=\"top\"></td><td align=\"center\">Longbeach Camera Viewfinder</td><td align=\"right\"><a href=\"#\" onclick=\"showBox()\">Image</a></td></tr></table></h1>\n");
				hprintf(request,
					"<p align=\"center\" valign=\"middle\" id=\"par\"><img id=\"vf\" src=\"viewfinder\" width=\"848\" height=\"480\" onload=\"loadImage()\" onerror=\"setTimeout(loadImage,50)\"></p>");
				hprintf(request,
					"<h1 style=\"font-family: Tahoma,Geneva,sans-serif; font-size: 24px;\" align=\"left\"><table border=\"0\" width=\"100%\"><tr><td><a href=\"/index.html\">&lt;&lt;&nbsp;Movies&nbsp;&lt;&lt;</a></td><td align=\"right\"><a href=\"#\" onclick=\"recPress()\"><img id=\"recb\" src=\"recordbutton\"></a></td></tr></table></h1>\n");
				hprintf(request,
					"<div id=\"mybox\" style=\"position: absolute;top:0px;left:0px;z-index:50; background:#F9E5B9;opacity: .75;font-family: Tahoma,Geneva,sans-serif; font-size: 12px;display:none;\">\n<table border=\"0\">\n<tr><td colspan=\"3\" align=\"center\"><b>Image properties</b></td></tr>\n");
				hprintf(request,
					"<tr valign=\"middle\"><td>Brightness:</td><td><input type=\"range\" id=\"brightness\" min=\"1\" max=\"255\" value=\"128\" step=\"1\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"brightness\" id=\"brightnessv\">128</output></td></tr>\n");
				hprintf(request,
					"<tr valign=\"middle\"><td>Contrast:</td><td><input type=\"range\" id=\"contrast\" min=\"1\" max=\"255\" value=\"128\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"contrast\" id=\"contrastv\">128</output></td></tr>\n");
				hprintf(request,
					"<tr valign=\"middle\"><td>Saturation:</td><td><input type=\"range\" id=\"saturation\" min=\"1\" max=\"255\" value=\"128\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"saturation\" id=\"saturationv\">128</output></td></tr>\n");
				//hprintf(request, "<tr valign=\"middle\"><td>Hue:</td><td><input type=\"range\" id=\"hue\" min=\"0\" max=\"255\" value=\"128\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"hue\" id=\"huev\">128</output></td></tr>\n");
				hprintf(request,
					"<tr valign=\"middle\"><td>Sharpness:</td><td><input type=\"range\" id=\"sharpness\" min=\"0\" max=\"255\" value=\"128\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"sharpness\" id=\"sharpnessv\">128</output></td></tr>\n");
				hprintf(request,
					"<tr valign=\"middle\"><td>BLC:</td><td><input type=\"range\" id=\"blc\" min=\"0\" max=\"5\" value=\"0\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"blc\" id=\"blcv\">0</output></td></tr>\n");
				hprintf(request,
					"<tr valign=\"middle\"><td>AE:</td><td><input type=\"range\" id=\"ae\" min=\"0\" max=\"1\" value=\"1\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"ae\" id=\"aev\">1</output></td></tr>\n");
				hprintf(request,
					"<tr valign=\"middle\"><td>AWB:</td><td><input type=\"range\" id=\"awb\" min=\"0\" max=\"1\" value=\"1\" oninput=\"set(this.id,value)\"></td><td align=\"right\" width=\"30\"><output for=\"awb\" id=\"awbv\">1</output></td></tr>\n");
				hprintf(request, "</table>\n</div>\n</body></html>");
				return true;
			}
			// index.html
			setContentType(request, "text/html");
			addNoCacheHeaders(request);
			hprintf(request,
				"<!DOCTYPE html>\n<html><head><title>TomTom Longbeach camera</title>\n<script>\nfunction pad(number, length) {var str=''+number;\nwhile(str.length<length){str='0'+str;}\nreturn str;}\ncurvideo=-1;curimage=0;\n");
			hprintf(request,
				"function loadImage(n,i) {document.getElementById(\"vid\"+n).src=\"/thumbs/\"+pad(n,2)+\"?i=\"+i;}\nfunction doLoadImage() {if(curvideo>=0) loadImage(curvideo,++curimage);}\n");
			hprintf(request,
				"function loadedImage(n){if(curvideo==n) setTimeout(doLoadImage,30);\n}\nfunction errorImage(n){if(curvideo==n) {curimage=0;loadImage(n,0);}\n}\nfunction inImage(n){if(curvideo>=0) loadImage(curvideo,0);\ncurvideo=n;curimage=0;setTimeout(doLoadImage,30);}\nfunction outImage(n){loadImage(n,0);curvideo=-1;curimage=0;}\n</script>");
			hprintf(request,
				"<style type=\"text/css\">\n.thumb {width:444px;padding=10px;border-style:dotted;border-width:1px;margin:5px;float:left;font-familiy: Tahoma,Geneva,sans-serif; font-size: 12px;}\n</style></head>\n<body color=\"#ffffff\">\n");
			hprintf(request,
				"<h1 style=\"font-family: Tahoma,Geneva,sans-serif; font-size: 24px;\"><table border=\"0\" width=\"100%\"><tr><td><img src=\"tomtom-logo.jpg\" border=\"0\" width=\"188\" align=\"top\"></td><td align=\"center\">Longbeach Camera Movies</td><td align=\"right\"><a href=\"showvf.html\">&gt;&gt;&nbsp;Viewfinder&nbsp;&gt;&gt;</a></td></tr></table></h1>\n");

			// for each movie
			struct dirent *df;
			DIR *dir = opendir(DCIM_CAMERA_DIR_PATH);
			if (dir) {
				while ((df = readdir(dir)) != NULL) {
					int len = strlen(df->d_name);
					if (len > 7 && !strcasecmp(df->d_name + len - 5, ".h264")
					    && isdigit(df->d_name[len - 6]) && isdigit(df->d_name[len - 7])
					    && !(df->d_type & DT_DIR)) {
						int num = atoi(&df->d_name[len - 6]);
						hprintf(request,
							"<a href=\"#\" onclick=\"window.open('/videos/%s')\"><div class=\"thumb\"><center><img border=\"0\" id=\"vid%d\" onLoad=\"loadedImage(%d)\" onerror=\"errorImage(%d)\" onmouseover=\"inImage(%d)\" onmouseout=\"outImage(%d)\" src=\"/thumbs/%02d?i=0\" width=\"424\" height=\"240\" vspace=\"5\" hspace=\"5\">%s</center></div></a>\n",
							df->d_name, num, num, num, num, num, num, df->d_name);
					}
				}
				closedir(dir);
			}

			hprintf(request, "</body></html>");
			return true;
		}
		return false;
	}

 TTweb(const char *fixdir = 0):GBhttp(fixdir) {
	}
};

extern TTweb webserver;
