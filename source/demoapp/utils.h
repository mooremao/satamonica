// TomTom camera platform
// Various utility definitions
// (c) 2014. Nebojsa Sumrak

#pragma once

#include <debug.h>

#define COUNTOF(arr) (sizeof (arr) == 0 ? 0 : (sizeof (arr) / sizeof (arr[0])))

#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

union Variant {
	void *p;
	int i;
	unsigned u;
	struct {
		short s1, s2;
	};
	struct {
		char c1, c2, c3, c4;
	};

	 Variant() {
		p = 0;
	} Variant(void *pp) {
		p = pp;
	}
	Variant(void **pp) {
		p = *pp;
	}
	Variant(int pp) {
		i = pp;
	}
	Variant(long pp) {
		i = (int)pp;
	}
	Variant(unsigned pp) {
		u = pp;
	}
	Variant(short p1, short p2) {
		s1 = p1;
		s2 = p2;
	}
	Variant(char p1, char p2, char p3, char p4) {
		c1 = p1;
		c2 = p2;
		c3 = p3;
		c4 = p4;
	}
	operator  void *() {
		return p;
	}
	operator  int () {
		return i;
	}
	operator  void *&() {
		return p;
	}

	void *getpointer() {
		return p;
	}
};
