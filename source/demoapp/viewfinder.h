// TomTom Video platform API
// Viewfinder services
// Nebojsa Sumrak 2014.

#pragma once

#include <unistd.h>
#include <ttv.h>
#include "debug.h"
#include "tttime.h"

#define VF_FIFO_COUNT		4
#define VF_INACTIVE_TIME 5000

extern bool isRecording;

class TTviewfinder {
 protected:
	struct {
		void *address;
		unsigned int size;
	} buffers[VF_FIFO_COUNT];
	int toread, torelease, towrite;
	TTtime lastset;
	bool viewfinderStarted;

	void inc_fifo_counter(int &a) {
		if (++a == VF_FIFO_COUNT)
			a = 0;
	} void release_frame() {
		if (toread != torelease) {
			ttv_release_frame(buffers[torelease].address);
			inc_fifo_counter(torelease);
		}
	}

 public:
 TTviewfinder():lastset(0) {
		toread = torelease = towrite = 0;
		viewfinderStarted = false;
		memset(buffers, 0, sizeof(buffers));
	}

	bool getframe(void *&address, unsigned int &size) {
		release_frame();
		if (!viewfinderStarted) {
			if (!(ttv_get_started() & TTV_STREAM_MJPG)) {
				debuglog(4, "starting viewfinder", 0);
				if( ttv_mode(TTV_MODE_VIDEO) ) {
					if (!ttv_start(TTV_STREAM_MJPG)) {
						debuglog(4, "vf.getframe cannot start stream", 0);
						return false;
					}
				}
				else
					return false;
			}
			viewfinderStarted = true;
			lastset.now();
		}
		debuglog(5, "vf.getframe - toread: %d, towrite: %d", toread, towrite);
		for (int i = 0; i < 100 && toread == towrite; i++)
			::usleep(10000);
		if (toread == towrite) {
			debuglog(4, "vf.getframe timeout", 0);
			return false;
		}
		debuglog(5, "vf.getframe could read now", 0);
		address = buffers[toread].address;
		size = buffers[toread].size;
		inc_fifo_counter(toread);
		return true;
	}

	bool setframe(void *address, unsigned int size) {
		if (!viewfinderStarted)
			return false;
		if (TTtime().diffms(lastset) > VF_INACTIVE_TIME) {
			if (ttv_get_started() & TTV_STREAM_MJPG) {
				debuglog(4, "----- no access for too long, finishing viewfinder", 0);
				while (toread != towrite)
					getframe(address, size);
				release_frame();
				if (!isRecording)
					ttv_stop(TTV_STREAM_MJPG);
				viewfinderStarted = false;
			}
			return false;
		}
		int tw = towrite;
		inc_fifo_counter(tw);
		if (tw == torelease)
			return false;
		buffers[towrite].address = address;
		buffers[towrite].size = size;
		towrite = tw;
		lastset.now();
		return true;
	}

	bool isStarted() {
		return viewfinderStarted;
	}
};

extern TTviewfinder viewfinder;
