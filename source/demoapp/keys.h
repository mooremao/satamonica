// TomTom Video platform API
// keys wrapper
// Nebojsa Sumrak 2014.

#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <pthread.h>
#include "tttime.h"
#include "debug.h"

enum {
	TTKEY_OFF = 0,
	TTKEY_RECORD,
	TTKEY_LEFT,
	TTKEY_RIGHT,
	TTKEY_UP,
	TTKEY_DOWN,

	TTKEY_WEB_RECSTOP,

	TTKEY_MAX,
};

enum {
	TTKEV_RELEASED = 0,
	TTKEV_PRESSED = 1
};

class TTkeys {
 protected:
	int keyfd;
	int simkeyfd;
	TTtime pressed[TTKEY_MAX];

	int getkey(int linuxkey) {
		switch (linuxkey) {
		case KEY_STOP:
			return TTKEY_OFF;
			case KEY_RECORD:return TTKEY_RECORD;
			case KEY_LEFT:return TTKEY_LEFT;
			case KEY_RIGHT:return TTKEY_RIGHT;
			case KEY_UP:return TTKEY_UP;
			case KEY_DOWN:return TTKEY_DOWN;
			default:return -1;
	}}
	char m_evbuff[256];
	size_t m_inbuff, m_readbuff;

	volatile bool enabled;
	pthread_mutex_t m_mutex;

	bool insertev(char key, char evt) {
		evt <<= 4;
		evt |= key;
		pthread_mutex_lock(&m_mutex);
		register size_t inbuff = m_inbuff + 1;
		if (inbuff >= sizeof(m_evbuff))
			inbuff = 0;
		if (inbuff == m_readbuff) {
			pthread_mutex_unlock(&m_mutex);
			return false;	//full
		}
		m_evbuff[m_inbuff] = evt;
		m_inbuff = inbuff;
		pthread_mutex_unlock(&m_mutex);
		return true;
	}

	bool readev(int &ev, unsigned long &modi) {
		pthread_mutex_lock(&m_mutex);
		if (m_inbuff == m_readbuff) {
			pthread_mutex_unlock(&m_mutex);
			return false;
		}
		register char c = m_evbuff[m_readbuff++];
		if (m_readbuff >= sizeof(m_evbuff))
			m_readbuff = 0;
		pthread_mutex_unlock(&m_mutex);
		ev = c & 0xf;
		modi = c >> 4;
		return true;
	}

 public:
	TTkeys() {
		keyfd =::open("/dev/input/by-path/platform-gpio-keys-event", O_RDONLY);
		if (keyfd < 0) {
			debuglog(2, "failed opening key driver", 0);
		}

		simkeyfd =::open("/dev/input/by-path/virtual-key-event", O_RDONLY);
		if (simkeyfd < 0) {
			debuglog(5, "failed opening simulated key driver", 0);
		}

		m_inbuff = m_readbuff = 0;
		enabled = true;
		pthread_mutex_init(&m_mutex, NULL);
		memset(m_evbuff, 0, sizeof(m_evbuff));
	}

	void enable(bool enable) {
		enabled = enable;
	};

	bool inject(int key, bool pressed = true) {
		return insertev((char)key, pressed ? 1 : 0);
	}

	bool get(int &key, unsigned long &modi) {
		if (keyfd < 0)
			return false;
		if (readev(key, modi))
			return enabled;

		struct input_event keyev;
		fd_set rfds;
		struct timeval tv;

		FD_ZERO(&rfds);
		FD_SET(keyfd, &rfds);
		if (simkeyfd >= 0)
			FD_SET(simkeyfd, &rfds);
		tv.tv_sec = 0;
		tv.tv_usec = 0;

		if (select((keyfd > simkeyfd ? keyfd : simkeyfd) + 1, &rfds, NULL, NULL, &tv)) {
			int fdtoread;
			if (simkeyfd >= 0)
				fdtoread = FD_ISSET(simkeyfd, &rfds) ? simkeyfd : keyfd;
			else
				fdtoread = keyfd;

			if (read(fdtoread, &keyev, sizeof(struct input_event)) == sizeof(struct input_event)
					&& keyev.type == EV_KEY) {
				debuglog(5, "key code: %d, value: %d simulated: %s", keyev.code, keyev.value,
					fdtoread == simkeyfd ? "yes" : "no");
				key = getkey(keyev.code);
				if (key < 0) {
					debuglog(5, "unknown key", 0);
					return false;
				}
				switch (keyev.value) {
					default:
					case 0:
						modi = TTKEV_RELEASED;
						break;
					case 1:
						modi = TTKEV_PRESSED;
						pressed[key].set(keyev.time);
						break;
					case 2:
						modi = pressed[key].diffmsfrom(keyev.time);
						break;
				}
				debuglog(5, "known key: %d, modi=%lu", key, modi);
				return enabled;
			}
		}
		return false;
	}

	~TTkeys() {
		if (keyfd >= 0)
			close(keyfd);
		pthread_mutex_destroy(&m_mutex);
	}
};

extern TTkeys keys;
