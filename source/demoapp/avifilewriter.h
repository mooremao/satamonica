// TomTom Video platform API
// video file services h264+MJPG_avi
// Nebojsa Sumrak 2014.

#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

class TTvideoFile {
	
 protected:
	int frameRate;
	FILE * filet, *milf, *dex, *info_pri, *info_sec;
	int byteswritten, numaviframes;
	long hdrmarker, nfhmarker, nfsmarker;

	bool add_avi_frame(unsigned size) {
		// write size to index tmp file
		if (fwrite(&size, 1, 4, dex) != 4)
			return false;
		numaviframes++;
		return true;
	} bool write_int(unsigned int n) {
		char buf[4];
		buf[0] = n & (0xff);
		buf[1] = (n >> 8) & (0xff);
		buf[2] = (n >> 16) & (0xff);
		buf[3] = (n >> 24) & (0xff);
		return (fwrite(buf, 1, 4, milf) == 4);
	}

	bool write_short(unsigned int n) {
		char buf[2];
		buf[0] = n & (0xff);
		buf[1] = (n >> 8) & (0xff);
		return (fwrite(buf, 1, 2, milf) == 2);
	}

	bool write_chars(const char *s, unsigned count) {
		return (fwrite(s, 1, count, milf) == count);
	}

	bool write_avi_header() {
		bool r = true;

		r &= write_chars("LIST", 4);
		long marker = ftell(milf);
		r &= write_int(0);
		r &= write_chars("hdrl", 4);

		// AVI header
		r &= write_chars("avih", 4);
		long h_marker = ftell(milf);
		r &= write_int(0);
		if(frameRate < 1 || frameRate > 300)
			frameRate = 300;
		r &= write_int(10000000/frameRate);	// time_delay = 1000000/fps
		r &= write_int(1221120);	// data_rate = width*height*3
		r &= write_int(0);	// reserved
		r &= write_int(0x10);	// flags
		nfhmarker = ftell(milf);
		r &= write_int(0);	// number_of_frames
		r &= write_int(0);	// initial_frames
		r &= write_int(1);	// data_streams
		r &= write_int(1221120);	// buffer_size = width*height*3
		r &= write_int(848);	// width
		r &= write_int(480);	// height
		r &= write_int(1);	// time_scale
		r &= write_int(0);	// playback_data_rate
		r &= write_int(0);	// starting_time
		r &= write_int(0);	// data_length

		long t = ftell(milf);
		fseek(milf, h_marker, SEEK_SET);
		r &= write_int(t - h_marker - 4);
		fseek(milf, t, SEEK_SET);

		// stream list
		r &= write_chars("LIST", 4);
		long sub_marker = ftell(milf);
		r &= write_int(0);
		r &= write_chars("strl", 4);

		// stream header
		r &= write_chars("strh", 4);
		long s_marker = ftell(milf);
		r &= write_int(0);

		r &= write_chars("vids", 4);
		r &= write_chars("MJPG", 4);
		r &= write_int(0);	// flags
		r &= write_int(0);	// priority
		r &= write_int(0);	// initial_frames
		r &= write_int(10);	// time_scale
		r &= write_int(frameRate);	// data_rate
		r &= write_int(0);	// start_time
		nfsmarker = ftell(milf);
		r &= write_int(0);	// data_length
		r &= write_int(1221120);	// buffer_size = width*height*3
		r &= write_int(0);	// quality
		r &= write_int(0);	// sample_size
		r &= write_int(0);
		r &= write_int(0);

		t = ftell(milf);
		fseek(milf, s_marker, SEEK_SET);
		r &= write_int(t - s_marker - 4);
		fseek(milf, t, SEEK_SET);

		// format header
		r &= write_chars("strf", 4);
		long f_marker = ftell(milf);
		r &= write_int(0);

		r &= write_int(40);	// header_size
		r &= write_int(848);	// width
		r &= write_int(480);	// height
		r &= write_short(1);	// num_planes
		r &= write_short(24);	// bits_per_pixel
		r &= write_int(((unsigned int)'G' << 24) | ((unsigned int)'P' << 16) | ((unsigned int)'J' << 8) | ((unsigned int)'M'));	// compression_type
		r &= write_int(1221120);	// image_size = width*height*3
		r &= write_int(0);	// x_pels_per_meter
		r &= write_int(0);	// y_pels_per_meter
		r &= write_int(0);	// colors_used
		r &= write_int(0);	// colors_important

		t = ftell(milf);
		fseek(milf, f_marker, SEEK_SET);
		r &= write_int(t - f_marker - 4);
		fseek(milf, t, SEEK_SET);

		// fill rest of avi header
		t = ftell(milf);
		fseek(milf, sub_marker, SEEK_SET);
		r &= write_int(t - sub_marker - 4);
		fseek(milf, marker, SEEK_SET);
		r &= write_int(t - marker - 4);
		fseek(milf, t, SEEK_SET);

		// and a junk to the end of 4096b
		r &= write_chars("JUNK", 4);
		marker = ftell(milf);
		r &= write_int(0);

		int rest = 4096 - ftell(milf);
		static const char *junk = "SUMRAK WAS HERE!";
		const char *j = junk;

		for (int t = 0; t < rest; t++) {
			putc(*j++, milf);
			if (!*j)
				j = junk;
		}

		t = ftell(milf);
		fseek(milf, marker, SEEK_SET);
		r &= write_int(t - marker - 4);
		fseek(milf, t, SEEK_SET);

		return r;
	}

	void write_avi_index() {
		fseek(dex, 0, SEEK_SET);

		write_chars("idx1", 4);
		long marker = ftell(milf);
		write_int(0);

		int offset = 4, sz;
		for (int i = 0; i < numaviframes; i++) {
			write_chars("00dc", 4);
			write_int(0x10);
			write_int(offset);
			fread(&sz, 1, 4, dex);
			write_int(sz);
			offset += sz + 8;
		}

		long t = ftell(milf);
		fseek(milf, marker, SEEK_SET);
		write_int(t - marker - 4);
		fseek(milf, t, SEEK_SET);
	}

	void write_avi_footer() {
		long t = ftell(milf);
		fseek(milf, hdrmarker, SEEK_SET);
		write_int(t - hdrmarker - 4);
		fseek(milf, t, SEEK_SET);

		write_avi_index();

		t = ftell(milf);
		fseek(milf, nfhmarker, SEEK_SET);
		write_int(numaviframes);
		fseek(milf, nfsmarker, SEEK_SET);
		write_int(numaviframes);

		fseek(milf, 4, SEEK_SET);
		write_int(t - 8);
		fseek(milf, t, SEEK_SET);
	}

 public:
	TTvideoFile() {
		frameRate = 300;
		filet = milf = dex = info_pri = info_sec = 0;
		byteswritten = numaviframes = 0;
		hdrmarker = nfhmarker = nfsmarker = 0;
	}

	bool open(int num, const char *add = 0) {
		close();
		char buf[256];
		sprintf(buf, "/mnt/mmc/TTCamVideo%02d%s.h264", num, (add ? add : ""));
		filet = fopen(buf, "w");
		byteswritten = 0;
		if (!filet) {
			debuglog(2, "failed to open file '%s'", buf);
			reject();
			return false;
		}

		sprintf(buf, "/mnt/mmc/TTCamVideo%02d%s_pri.info", num, (add ? add : ""));
		info_pri = fopen(buf, "w");
		if (!info_pri) {
			debuglog(2, "failed to open file '%s'", buf);
			reject();
			return false;
		}
		sprintf(buf, "/mnt/mmc/TTCamVideo%02d%s_sec.info", num, (add ? add : ""));
		info_sec = fopen(buf, "w");
		if (!info_sec) {
			debuglog(2, "failed to open file '%s'", buf);
			reject();
			return false;
		}

		dex = tmpfile();
		if (!dex) {
			debuglog(2, "failed to create index", 0);
			reject();
			return false;
		}

		hdrmarker = nfhmarker = nfsmarker = 0;
		numaviframes = 0;
		sprintf(buf, "/mnt/mmc/TTCamVideo%02d%s.avi", num, (add ? add : ""));
		milf = fopen(buf, "w");
		if (!milf) {
			debuglog(2, "failed to open file '%s'", buf);
			reject();
			return false;
		}

		if (!write_chars("RIFF", 4) ||
		    !write_int(0) || !write_chars("AVI ", 4) || !write_avi_header() || !write_chars("LIST", 4)
		    ) {
			reject();
			return false;
		}

		hdrmarker = ftell(milf);

		if (!write_int(0) || !write_chars("movi", 4)) {
			reject();
			return false;
		}

		/* Disable stream buffering for both primary and secondary streams.
		 * This helps in reducing the ARM load by avoiding unnecessary
		 * memcpy in user space. Also there is sufficient buffering in kernel
		 * space for block drivers.
		 */
		setvbuf(filet, NULL, _IONBF, 0);
		setvbuf(milf, NULL, _IONBF, 0);

		return true;
	}

	bool write_thumb(void *buf, unsigned size, unsigned long pts) {
		if (!isopen())
			return false;
		unsigned padsize = (size + 3) & (~3);

		if (!add_avi_frame(padsize) ||
		    !write_chars("00dc", 4) || !write_int(padsize) || fwrite(buf, 1, size, milf) != size)
			return false;

		static const char padbuf[4] = { 0, 0, 0, 0 };
		register unsigned pad = padsize - size;
		if (info_sec)
			fprintf(info_sec, "%lu\n" ,pts);
		if (pad && fwrite(padbuf, 1, pad, milf) != pad)
			return false;

		return true;
	}
	void set_thumb_frameRate(int fpts)
	{
		frameRate = fpts;
	}
	bool write_frame(void *buf, unsigned size, unsigned long pts) {
		if (!filet)
			return 0;
		unsigned res = fwrite(buf, 1, size, filet);
		byteswritten += res;
		if (info_pri)
			fprintf(info_pri, "%lu\n" ,pts);
		return (res == size);
	}

	bool write_fileinfo(ttv_video_res_t transcodeOutRes, int transcodeOutfps) {
		int width, height;
		float fps;
		switch (transcodeOutRes) {
			case TTV_VRES_480P:
				width = 848;
				height = 480;
				break;
			case TTV_VRES_720P:
				width = 1280;
				height = 720;
				break;
			case TTV_VRES_1080P:
				width = 1920;
				height = 1080;
				break;
			case TTV_VRES_2_7K:
				width = 2704;
				height = 1524;
				break;
			case TTV_VRES_4K:
				width = 2704;
				height = 1524;
				break;
			default :
				width = 1920;
				height = 1080;
				break;
		}
		fps = transcodeOutfps;
		if (info_pri)
			fprintf(info_pri, "%d\n%d\n%.1f\n", width, height, fps);
		if (info_sec)
			fprintf(info_sec, "%d\n%d\n%.1f\n", width, height, fps);
		return true;
	}
	bool write_fileinfo(ttv_resolution_t res) {
		int width, height;
		float fps;
		switch (res) {
			case TTV_RES_1080_60_WIDE:
				width = 1920;
				height = 1080;
				fps = 60;
				break;
			case TTV_RES_1080_60_NORMAL:
				width = 1920;
				height = 1080;
				fps = 60;
				break;
			case TTV_RES_1080_50_WIDE:
				width = 1920;
				height = 1080;
				fps = 50;
				break;
			case TTV_RES_1080_50_NORMAL:
				width = 1920;
				height = 1080;
				fps = 50;
				break;
			case TTV_RES_1080_30_WIDE:
				width = 1920;
				height = 1080;
				fps = 30;
				break;
			case TTV_RES_1080_30_NORMAL:
				width = 1920;
				height = 1080;
				fps = 30;
				break;
			case TTV_RES_1080_25_WIDE:
				width = 1920;
				height = 1080;
				fps = 25;
				break;
			case TTV_RES_1080_25_NORMAL:
				width = 1920;
				height = 1080;
				fps = 25;
				break;
			case TTV_RES_4K_15_WIDE:
				width = 3840;
				height = 2160;
				fps = 15;
				break;
			case TTV_RES_4K_12_5_WIDE:
				width = 3840;
				height = 2160;
				fps = 12.5;
				break;
			case TTV_RES_2_7K_30_WIDE:
				width = 2704;
				height = 1524;
				fps = 30;
				break;
			case TTV_RES_2_7K_25_WIDE:
				width = 2704;
				height = 1524;
				fps = 25;
				break;
			case TTV_RES_2_7K_25_NORMAL:
				width = 2704;
				height = 1524;
				fps = 25;
				break;
			case TTV_RES_720_120_WIDE:
				width = 1280;
				height = 720;
				fps = 120;
				break;
			case TTV_RES_720_120_NORMAL:
				width = 1280;
				height = 720;
				fps = 120;
				break;
			case TTV_RES_720_100_WIDE:
				width = 1280;
				height = 720;
				fps = 100;
				break;
			case TTV_RES_720_100_NORMAL:
				width = 1280;
				height = 720;
				fps = 100;
				break;
			case TTV_RES_720_60_WIDE:
				width = 1280;
				height = 720;
				fps = 60;
				break;
			case TTV_RES_720_60_NORMAL:
				width = 1280;
				height = 720;
				fps = 60;
				break;
			case TTV_RES_720_50_WIDE:
				width = 1280;
				height = 720;
				fps = 50;
				break;
			case TTV_RES_720_50_NORMAL:
				width = 1280;
				height = 720;
				fps = 50;
				break;
			case TTV_RES_WVGA_180_WIDE:
				width = 848;
				height = 480;
				fps = 180;
				break;
			case TTV_RES_WVGA_150_WIDE:
				width = 848;
				height = 480;
				fps = 150;
				break;
			default :
				width = 1920;
				height = 1080;
				fps = 60;
				break;
		}
		if (info_pri)
			fprintf(info_pri, "%d\n%d\n%.1f\n", width, height, fps);

		if (info_sec)
			fprintf(info_sec, "848\n480\n%.1f\n",(float)frameRate/10);
		return true;
	}

	void close() {
		if (milf)
			write_avi_footer();
		reject();
	}

	void reject() {
		if (filet) {
			fclose(filet);
			filet = 0;
			byteswritten = 0;
		}
		if (milf) {
			fclose(milf);
			milf = 0;
			hdrmarker = nfhmarker = nfsmarker = 0;
			numaviframes = 0;
		}
		if (info_pri) {
			fclose(info_pri);
			info_pri = 0;
		}
		if (info_sec) {
			fclose(info_sec);
			info_sec = 0;
		}
		if (dex) {
			fclose(dex);
			dex = 0;
		}
	}

	~TTvideoFile() {
		close();
	}

	int getBytesWritten() {
		return byteswritten;
	}

	bool isopen() {
		return (filet != 0 || milf != 0);
	}
};
