// TomTom camera platform
// time services
// (c) 2012.-2014. Nebojsa Sumrak

#pragma once

#include <sys/types.h>
#include <stdint.h>
#include <sys/time.h>
#include <string.h>

class TTtime {
 protected:
	struct timeval m_t;

 public:
	void setzero() {
		memset(&m_t, 0, sizeof(m_t));
	}

	void now() {
		struct timespec time;
		clock_gettime(CLOCK_MONOTONIC, &time);
		m_t.tv_sec = time.tv_sec;
		m_t.tv_usec = (time.tv_nsec / 1000);
	}

	void set(struct timeval &t) {
		m_t.tv_sec = t.tv_sec;
		m_t.tv_usec = t.tv_usec;
	}

	TTtime() {
		now();
	}

	TTtime(int sec) {
		m_t.tv_sec = sec;
		m_t.tv_usec = 0;
	}

	TTtime(struct timeval &t) {
		set(t);
	}

	long diffms(TTtime & timebefore) {
		struct timeval result;
		timersub(&m_t, &timebefore.m_t, &result);
		return result.tv_sec * 1000 + result.tv_usec / 1000;
	}

	long diffmsfrom(TTtime & timeafter) {
		struct timeval result;
		timersub(&timeafter.m_t, &m_t, &result);
		return result.tv_sec * 1000 + result.tv_usec / 1000;
	}

	long diffms(struct timeval &timebefore) {
		struct timeval result;
		timersub(&m_t, &timebefore, &result);
		return result.tv_sec * 1000 + result.tv_usec / 1000;
	}

	long diffmsfrom(struct timeval &timeafter) {
		struct timeval result;
		timersub(&timeafter, &m_t, &result);
		return result.tv_sec * 1000 + result.tv_usec / 1000;
	}

	operator  struct timeval () {
		return m_t;
	}

	bool iszero() {
		return (!m_t.tv_sec && !m_t.tv_usec);
	}

	void operator +=(long b) {
		if (b >= 1000) {
			m_t.tv_sec += b / 1000;
			b = b % 1000;
		}
		m_t.tv_usec += b * 1000;
		while (m_t.tv_usec > 1000000) {
			m_t.tv_sec++;
			m_t.tv_usec -= 1000000;
		}
	}

	uint64_t getstamp64() {
		return (uint64_t) m_t.tv_sec * 1000000 + (uint64_t) m_t.tv_usec;
	}
	unsigned long getstamp() {
		return m_t.tv_sec * 1000 + m_t.tv_usec / 1000;
	}
	unsigned long getsec() {
		return m_t.tv_sec;
	}
	int getmsec() {
		return m_t.tv_usec / 1000;
	}
};
