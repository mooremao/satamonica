// TomTom Video platform API
// h264 parse service (transcoding)
// (c) 2012-2014. Nebojsa Sumrak

#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

class TTh264parser {
 protected:
	FILE * milf, *info;
	unsigned long m_parserlastread, m_readerlastread;
	unsigned long m_filesize;
	int fileno;

 public:
	 TTh264parser() {
		milf = 0;
		info = 0;
		m_parserlastread = m_readerlastread = 0;
		m_filesize = 0;
		fileno = 0;
	}

	 ~TTh264parser() {
		close();
	}

	bool open(int fileno) {
		int width, height; float fps;
		close();
		char buf[256];

		m_parserlastread = m_readerlastread = 0;
		m_filesize = 0;

		sprintf(buf, "/mnt/mmc/TTCamVideo%02d.h264", fileno);
		milf = fopen(buf, "r");
		if (!milf) {
			debuglog(2, "h264p: can't open file %s", buf);
			return false;
		}

		sprintf(buf, "/mnt/mmc/TTCamVideo%02d_pri.info", fileno);
		info = fopen(buf, "r");
		if (!info) {
			debuglog(2, "h264p: can't open file %s", buf);
			return false;
		}
		else {
			/* seeking the filepointer towards the first pts */
			/* %41f: -3.40282*10^38: 41 character */
			fscanf(info, "%10d\n%10d\n%41f\n",&width, &height, &fps);
		}

		fseek(milf, 0, SEEK_END);
		m_filesize = ftell(milf);
		fseek(milf, 0, SEEK_SET);
		return true;
	}

	bool isopen() {
		return (milf != 0);
	}

	long getnextpts() {
		long pts = 0;
		if(info)
			/* -2^31+1: 11 characters */
			fscanf(info, "%11ld\n",&pts);

		return pts;
	}
	unsigned long getnextframesize() {
		if (!milf)
			return 0;
		unsigned long curpos = m_parserlastread;
		int state = 0;
		bool foundframe = false;
		fseek(milf, curpos, SEEK_SET);
		debuglog(5, "h264p: enter, seek to: %lu", curpos);
		while (1) {
			int c = getc(milf);
			if (c == EOF) {
				debuglog(5, "h264p: eof", 0);
				break;
			}
			curpos++;
			if (state == 4) {
				c &= 0x1f;
				debuglog(5, "h264p: nal unit: %d", c);

				if (foundframe) {
					curpos -= 5;
					break;
				}
				if (c == 1 || c == 5)
					foundframe = true;
				state = 0;
			} else if (c == 0) {
				if (state == 3) {	// already 3 zero, c is fourth...
					debuglog(4, "h264p: 4*0 ?! reset state", 0);
					state = 0;
				} else {
					state++;
				}
			} else if (c == 1 && state == 3) {
				debuglog(5, "h264p: nal marker found on: %lu", curpos);
				state++;
			} else
				state = 0;
		}
		register unsigned long size = curpos - m_parserlastread;
		m_parserlastread = curpos;
		debuglog(5, "h264 frame of size: %lu", size);
		return size;
	}

	bool read(void *addr, unsigned long size) {
		if (!milf)
			return 0;
		fseek(milf, m_readerlastread, SEEK_SET);
		register bool ret = (fread(addr, 1, size, milf) == size);
		m_readerlastread = ftell(milf);
		if(m_readerlastread == m_filesize) {
			debuglog(5, "h264p: last read == file size", 0);
			close();
		}
		return ret;
	}

	void close() {
		if (milf) {
			fclose(milf);
			milf = 0;
		}
		if(info) {
			fclose(info);
			info = 0;
		}
	}

	int getfilereadpercentage() {
		if (!milf)
			return 100;
		return (int)((((long long)m_readerlastread) * 100LL) / (long long)m_filesize);
	}

	void setfileno(int num){
		fileno = num;
	}

	int readfileno(){
		return fileno;
	}
};
