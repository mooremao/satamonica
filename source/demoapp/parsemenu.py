#!/usr/bin/env python

"""
        TomTom LongBeach project
		Menu flow converter XML -> C
"""
        
import xml.etree.ElementTree as ET
import sys


def parsescreen(screenel):
  global filet
  global vcounter

  for el in screenel.findall('item'):
    sel = el.find('submenu')
    if sel is not None:
      sel.set("name", parsescreen(sel))

  hel = screenel.find('header')
  if hel is not None:
    sel = hel.find('submenu')
    if sel is not None:
      sel.set("name", parsescreen(sel))

  fel = screenel.find('footer')
  if fel is not None:
    sel = fel.find('submenu')
    if sel is not None:
      sel.set("name", parsescreen(sel))

  itemsname = "dummy_items_name{c}".format(c=vcounter)
  vcounter = vcounter + 1
  filet.write("static TTmenu::MenuItem {c}[] = {{\n".format(c=itemsname))
  for el in screenel.findall('item'):
    parseitem(el)
  filet.write("\t{0}\n};\n\n");

  name = screenel.attrib.get("name")
  namestat = ""
  if name is None:
    name = "dummy_screen_name{c}".format(c=vcounter)
    vcounter = vcounter + 1
    namestat = "static "

  filet.write("{s}TTmenu::MenuScreen {n} = {{\n".format(s=namestat,n=name))
  if hel is not None:
    parseitem(hel)
  else:
    filet.write("\t{0},\n")

  if fel is not None:
    parseitem(fel)
  else:
    filet.write("\t{0},\n")

  filet.write("\t{i}, 0, {{0}}\n}};\n\n".format(i=itemsname))
  return name


def getTagOrAttrib(itemel, tag):
  content = itemel.find(tag)
  if content is None:
    content = itemel.get(tag)
    if content is None:
      content = "0"
  else:
    content = content.text
  return content

                      
def parseitem(it):
  global filet
  global vcounter

  subname = "0"
  el = it.find('submenu')
  if el is None:
    el = it.get('submenu')
    if el is not None:
     subname = "&"+el
  else:
    subname = "&"+el.get("name")

  content = getTagOrAttrib(it,'content')
  data = getTagOrAttrib(it,'data')
  if data != "0":
	data = '(void*)' + data
  txt = getTagOrAttrib(it,'text')
  if txt != "0":
    content = '(void*)L"' + txt + '"'
  flags = getTagOrAttrib(it,'flags')
  align = it.get('align')
  if align is not None:
    align = align.lower()
  if align == 'center':
    flags = flags + "|TTmenu::MENU_CENTER_ALIGN"
  elif align == 'right':
    flags = flags + "|TTmenu::MENU_RIGHT_ALIGN"
  valign = it.get('valign')
  if valign is not None:
    valign = valign.lower()
  if valign == 'center':
    flags = flags + "|TTmenu::MENU_V_CENTER_ALIGN"
  elif valign == 'bottom':
    flags = flags + "|TTmenu::MENU_V_BOTTOM_ALIGN"
  brk = it.get('breakbefore')
  if brk is not None and brk != '0' and brk.lower() != 'false' and brk.lower() != 'no':
    flags = flags + "|TTmenu::MENU_BREAK_BEFORE"
  disab = it.get('disabled')
  if disab is not None and disab != '0' and disab.lower() != 'false' and disab.lower() != 'no':
    flags = flags + "|TTmenu::MENU_DISABLED"

  defin = it.get('def')
  if defin is not None:
    filet.write("#ifdef " + defin + "\n")

  ndef = it.get('ndef')
  if ndef is not None:
    filet.write("#ifndef " + ndef + "\n")

  filet.write("\t{{ {c},{cb},0,0,{w},{h},(void*){p},{d},{f},{s} }},\n".format(c=content,cb=getTagOrAttrib(it,'widget'),w=getTagOrAttrib(it,'width'),h=getTagOrAttrib(it,'height'),p=getTagOrAttrib(it,'param'),d=data,f=flags,s=subname))

  if (defin is not None) or (ndef is not None):
    filet.write("#endif\n")

filet = None
vcounter = 0
                             
if len(sys.argv) != 3:
  print "usage: parsemenu <menu_xml> <menu_c>"
  sys.exit(1)
                                  
tree = ET.parse(sys.argv[1])
if not tree:
  print "error in menu_xml file"
  sys.exit(2)
                                      
root = tree.getroot()
if root is None:
  print "error in menu_xml file (no root)"
  sys.exit(3)
                                          
filet = open(sys.argv[2], "w")
filet.write("// TomTom camera platform\n\n// AUTO GENERATED FILE FROM XML '{x}'\n// PLEASE DO NOT EDIT\n\n#pragma once\n\n".format(x=sys.argv[1]))
                                          
for scr in root.findall('screen'):
  parsescreen(scr)
                                            
filet.close()
                                            