// GoBandit platform
// ->TomTom camera platform
// debug services
// (c) 2012-2014. Nebojsa Sumrak

#pragma once

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

// #define LOG_PANIC 0
// #define LOG_FAILURE 1
// #define LOG_ERROR 2
// #define LOG_WARNING 3
// #define LOG_INFO 4
// #define LOG_TRACE 5

#define debuglog(level, fmt, ...)  debuglog_raw(level, __FILE__, __LINE__, fmt, __VA_ARGS__)

#ifdef DEBUG
#define assert(p) if(!(p)) { debuglog(2, "Assertation failed! "  #p, 0); exit(127); }
#else
#define assert(p) do { } while(0)
#endif

//extern volatile const char *_exceptionsave_file, *_exceptionsave_desc, *_exceptionsave2_file, *_exceptionsave2_desc;
//extern volatile int _exceptionsave_line, _exceptionsave2_line;
//extern volatile pthread_t _exceptionsave_tid, _exceptionsave2_tid;

inline void debuglog_raw(int level, const char *filej, int line, const char *fmt, ...)
{
//      _exceptionsave2_file = filej; _exceptionsave2_desc = fmt; _exceptionsave2_line = line; _exceptionsave2_tid = pthread_self();
	if (level >= 5)
		return;
	va_list varg;
	va_start(varg, fmt);
	time_t t = time(NULL);
	struct timeval tv;
	gettimeofday(&tv, 0);
	int msec = tv.tv_usec /= 1000;
	struct tm tm = *localtime(&t);
#if 0
	if (level < 4) {
		FILE *filet = fopen("/mnt/log.txt", "a");
		if (filet) {
			fprintf(filet, "%04d-%02d-%02d %02d:%02d:%02d.%03d (%d):%s(%d): ", tm.tm_year + 1900,
				tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, msec, level, filej, line);
			vfprintf(filet, fmt, varg);
			fprintf(filet, "\n");
			fclose(filet);
			if (!level)
				system("dmesg|tail >> /mnt/log.txt");
		}
	}
#endif
	printf("[APP] %02d:%02d:%02d.%03d (L%d):%s(%d): ", tm.tm_hour, tm.tm_min, tm.tm_sec, msec, level, filej, line);
	vprintf(fmt, varg);
	printf("\n");
	va_end(varg);
}

//inline int _find_exception()
//{
//      pthread_t tid = pthread_self();
//      for(int i=0; i<numexceptionsaves; i++) if(exceptionsaves[i].tid == tid) return i;
//      if(numexceptionsaves<10) {
//              exceptionsaves[numexceptionsaves].tid = tid;
//              return numexceptionsaves++;
//      }
//      return -1;
//}
//
//#define debugpoint(description) do{int i=_find_exception(); if(i>=0) { int sig=setjmp(exceptionsaves[i].exceptionsave); if(sig) {debuglog_raw(1, __FILE__, __LINE__, "exception point (%d): [%d:%d] %s", sig, getpid(), pthread_self(), description);exit(-1);}}}while(0);

//#define debugpoint(description) do{_exceptionsave_file=__FILE__; _exceptionsave_line=__LINE__;_exceptionsave_desc=description;}while(0)
