// TomTom Video platform API
// Platform demo application
// Nebojsa Sumrak 2014.

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <memory.h>
#include <signal.h>
#include <pthread.h>

#include <ttv.h>
#include <ttsystem.h>

#include "leds.h"
#include "display.h"
#include "keys.h"
#include "tttime.h"
#include "debug.h"
#include "web.h"
#include "viewfinder.h"
#include "sd.h"
#include "avifilewriter.h"
#include "h264parser.h"
#include "menu.h"
#include "stats.h"

// include fonts
#include "gfx/fn_boldcn_62.cpp"
#include "gfx/fnp_bold_42.cpp"
#include "gfx/fn_boldcn_18.cpp"
#include "gfx/fn_heavycn_22.cpp"

#include "widgets.h"

#define LED_NAME__LED1      "LED1"
#define LED_NAME__BACKLIGHT	 "Backlight"

#define MAX_FILE_SIZE	2000000000

/* Not using SIGPWR / SIGTERM directly here. As these values are hardcoded in run_demoapp.sh as well. */
#define EXIT_REASON__POWEROFF 1
#define EXIT_REASON__SIGTERM 15 /* Maps to SIGTERM */
#define EXIT_REASON__BATTERY_CRITICAL_LOW 30 /* Maps to SIGPWR */
#define EXIT_REASON__SIGTEMP 12 /* Maps to SIGUSR2 */
//#define DUMP_STILL_IMAGE_ADDITIONAL_INFO
#define STILL_ADDITIONAL_INFO_SIZE 1544

TTdisplay disp;
TTkeys keys;
TTviewfinder viewfinder;
TTweb webserver;
TTvideoFile videofile;
TTh264parser infile;
TTmenu menu(disp);
TTstats stats;

TTtime recstart;
bool isRecording = false;
bool isTranscoding = false;
bool firstkeyframe = false;

bool isStillCapturing = false;
bool isBurstCapturing = false;

/*continuous mode variables*/
bool isContinuousCapturing = false;
bool firstContinuousCapturing = false;
int continuousInterval = 0;
ttv_resolution_t continuousImageRes = TTV_RES_NONE;

/*timelapse mode variables*/
bool isTimelapseCapturing = false;
bool firstTimelapseCapturing = false;
int timelapseInterval = 0;
ttv_resolution_t timelapseImageRes = TTV_RES_NONE;

int stillimagecount = 0;
int numfiles = 0;
int frameno = 0;
unsigned long transcodepts = 0;
unsigned long freespace;
int backlighttick = 0;
int backlight = 255;
bool eos_on_primary = false;
bool eos_on_secondary = false;
bool insertTranscodeEOS = false;
int numSecFrames = 0;
int secWriteSpeed = 30;
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
bool rawDumpEnable = false;
bool rawDumpStarted = false;
int rawTlFrameCnt = 0;
ttv_resolution_t rawCaptureRes = TTV_RES_1080_60_WIDE;
#endif

/* Used only during transcoding */
bool isViewFinderEnabled = false;

static int exit_reason = 0;
static volatile sig_atomic_t trigger_wd = 1; /* if zero, watchdog will not be triggered. */

#ifdef SINGLE_THREAD_TRANSCODE
int transcodeFrameMultiplyer = 2; // 60->30 fps by default, should be changed by transcode config
#endif
ttv_video_res_t transcodeInputRes, transcodeOutputRes;
ttv_frame_type_t transcodeOutCodec;
int transcodeInputFps, transcodeOutputFps;

// for still image capture resolution selection
ttv_resolution_t stillImageRes = TTV_RES_NONE;
bool isStillMode = false;
/*
 * for recording usecase, application should have an idea about
 * its recording resolution and fps; use recordingRes for saving
 * the current resolution
 */
ttv_resolution_t recordingRes = TTV_RES_1080_60_WIDE;

// fwd declaration for transcode screens
extern TTmenu::MenuScreen transcodeoptionscreen;
extern TTmenu::MenuScreen transcodescreen;

// repair sd card variables
extern TTmenu::MenuScreen repairsdcardscreen;
static volatile bool isRepairingSdcard = false;
static pthread_t repairsdTh;
static bool repairsdThJoined = true;

bool processframes(int);

static void setTranscodeInputParams(int fileno);
// continuous mode helper function
void stopContinuousCapture();
// timelapse mode helper function
void stopTimelapseCapture();

void stopTranscoding()
{
	if (!isTranscoding)
		return;
	ttv_stop(TTV_STREAM_ALL);
	isTranscoding = false;
	videofile.close();
	infile.close();
	debuglog(5, "transcoding stopped", 0);
	setLedBrightness(LED_NAME__LED1, 0);
	transcodescreen.invalidate();

	/*
	 * If viewfinder was ON before start transcoding
	 * enable it back
	 */
	if (isViewFinderEnabled) {
			ttv_mode(TTV_MODE_VIDEO);
			ttv_start(TTV_STREAM_MJPG);
			isViewFinderEnabled = false;
	}

}
#ifdef SINGLE_THREAD_TRANSCODE
void readTranscodingFrame()
#else
static void *readTranscodingFrame(void *arg)
#endif
{
	debuglog(5,"readTranscodingFrame",0);
#ifdef SINGLE_THREAD_TRANSCODE
	for(int i=0; i<transcodeFrameMultiplyer; i++)
#else
	while(isTranscoding)
#endif
	{
		void *addr = NULL;
		unsigned long size = infile.getnextframesize();
		if (!size) {
			debuglog(5,"got 0 end of transcoding",0);
			break;
		}
		addr = NULL;
		while ((addr == NULL) && (isTranscoding)) {
			addr = ttv_request_decode_frame(size);
		}
		if (!addr) {
			debuglog(2, "request frame returns 0 for size: %lu, stopping", size);
			stopTranscoding();
			break;
		} else {
			transcodepts = infile.getnextpts();
			infile.read(addr,size);
			ttv_put_frame(addr, size, transcodepts, (!infile.isopen() || insertTranscodeEOS) );
			if (insertTranscodeEOS) {
				insertTranscodeEOS = false;
				break;
			}
		}
	}
	transcodescreen.invalidate();
#ifdef SINGLE_THREAD_TRANSCODE
	return;
#else
	processframes(10000);
	return 0;
#endif
}

void startTranscoding(int fileno)
{
	pthread_t th;
	pthread_attr_t attr;

	if (isRecording || isTranscoding) {
		debuglog(3, "Can't transcode, (trans)recording in progress", 0);
		return;
	}
	if (!freespace) {
		debuglog(4, "start recording but no free space... fail", 0);
		return;
	}
	if (!videofile.open(fileno, "-trans") || !infile.open(fileno)) {
		debuglog(4, "no file? can't start transcoding... fail", 0);
		return;
	}

	if (!videofile.write_fileinfo(transcodeOutputRes, transcodeOutputFps)) {
		debuglog(4, "error in writing file information", 0);
	}

	/*
	 * Transcoding can be triggered with viewfinder ON.
	 * In that case after transcoding is stopped
	 * viewfinder should restart
	 */
	if (ttv_get_started() & TTV_STREAM_MJPG) {
		isViewFinderEnabled = true;
	}

	recstart.now();
	frameno = 0;
	transcodepts = 0;
	setLedBrightness(LED_NAME__LED1, 64);
	debuglog(4, "-------SWITCH to transcode", 0);
	ttv_mode(TTV_MODE_TRANSCODE);
	debuglog(4, "-------SWITCH to transcode finished", 0);
	ttv_start(TTV_STREAM_PRI);
	isTranscoding = true;
	insertTranscodeEOS = false;
#ifdef SINGLE_THREAD_TRANSCODE
	transcodeFrameMultiplyer = 2;
	readTranscodingFrame();
	readTranscodingFrame();
	readTranscodingFrame();
#else
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(&th, &attr, readTranscodingFrame, 0);

#endif

	debuglog(5, "transcoding started", 0);
}

void resetBacklight()
{
	backlighttick = 0;
	setLedBrightness(LED_NAME__BACKLIGHT, backlight);
}

bool globalTicker(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_SECOND_TICK) {
		// get sd card free space
		freespace = sd_getFreeSpace();	// once per second

		stats.update();

		// turn off backlight after 5 sec
		if (backlighttick <= 4) {
			backlighttick++;
		} else {
			setLedBrightness(LED_NAME__BACKLIGHT, 0);
		}
	}
	return false;
}
bool ispWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "ISP option Wg: value=%d", (int)item->param);
        }
        return optionWg(e, item, p1);
}

bool brightnessWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "Brightness option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_BRIGHTNESS,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool contrastWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "Contrast option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_CONTRAST,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool saturationWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "Saturation option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_SATURATION,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool hueWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "hueWg option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_HUE,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool sharpnessWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "Sharpness option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_SHARPNESS,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool whiteBalanceModeWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "whiteBalanceModeWg option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_WB_MODE,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool aeMeteringWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "aeMeteringWg option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_AE_METERING,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool evCompensationWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "evCompensationWg option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_EV_COMPENSATION,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool manualISOgainWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
		debuglog(4, "manualISOgainWg option Wg: value=%d", (int)item->param);
                ttv_set_manual_iso_gain((unsigned int)item->param);
#else
                debuglog(4, "Build does not support manual iso",0);
#endif
        }
        return optionWg(e, item, p1);
}

bool autoExposureWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "Auto Exposure option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_AE,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool autoWhiteBalanceWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "Auto White Balance option Wg: value=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_AWB,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool setCaptureExposureLimitWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "Capture exposure limit is set to=%d", (int)item->param);
                ttv_set_feature(TTV_IMG_AE_TIME,(int)item->param);
        }
        return optionWg(e, item, p1);
}

bool mainWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	static int prevmin = -1, prevsec = -1;
	static bool previsrec = false;

	switch (e) {
	case TTmenu::EVENT_DRAW:
		{
			TTfont *font = &font_fn_boldcn_62;
			TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);
			wchar_t buf[16];

			int l = fb->getTextWidth(L"16", font);
			int r = fb->getTextWidth(L"59", font);
			int x = (item->width - l - r) / 2;
			int y = (item->height - fb->getTextHeight(font)) / 2;
			debuglog(5, "x=%d,y=%d,l=%d,r=%d", x, y, l, r);
			swprintf(buf, 16, L"%02d", prevmin);
			fb->drawText(x, y, buf, font, TTframebuffer::DRAW_DITHERED);
			swprintf(buf, 16, L"%02d", prevsec);
			fb->drawText(x + l, y, buf, font, TTframebuffer::DRAW_NORMAL);

			if (previsrec) {
				font = &font_fn_heavycn_22;
				fb->drawText(0, 0, L"REC", font, TTframebuffer::DRAW_NORMAL);
			}
			break;
		}
	case TTmenu::EVENT_ENTER:
		debuglog(4, "mainWg enter", 0);
	case TTmenu::EVENT_SECOND_TICK:
		{
			int min = 0, sec = 0;

			if (isRecording) {
				sec = TTtime().diffms(recstart) / 1000;
				min = sec / 60;
				sec %= 60;
				min %= 100;
			}
			if (prevmin == min && prevsec == sec && previsrec == isRecording)
				break;
			prevmin = min;
			prevsec = sec;
			previsrec = isRecording;
			item->invalidate();
			break;
		}
	case TTmenu::EVENT_MEASURE_ITEM:
		{
			debuglog(5, "option_callback: EVENT_MEASURE_ITEM!", 0);
			p1.s1 = -1;
			p1.s2 = -1;
			break;
		}
	}

	return false;
}

bool statsWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	switch (e) {
	case TTmenu::EVENT_DRAW:
		{
			TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);
			wchar_t buf[16];
			wchar_t *hdr = (wchar_t *) (item->content);
			TTstatsType type = (TTstatsType)(int)item->param;

			fb->drawText((LCM_BITS_PER_LINE - fb->getTextWidth(hdr, &font_fn_boldcn_18)) / 2, 1, hdr, &font_fn_boldcn_18, TTframebuffer::DRAW_NORMAL);
			fb->fillRect(0, 20, LCM_BITS_PER_LINE, 1);

			swprintf(buf, 16, L"%04.1f", stats.get(type));
			int w = fb->getTextWidth(buf, &font_fnp_bold_42);
			fb->drawText(115-w, 22, buf, &font_fnp_bold_42, TTframebuffer::DRAW_NORMAL);

			fb->fillRect(0, LCM_HEIGHT-1, LCM_BITS_PER_LINE, 1);
			fb->fillRect(0, LCM_HEIGHT-51, LCM_BITS_PER_LINE, 1, 0xaa);
			fb->fillRect(0, LCM_HEIGHT-101, LCM_BITS_PER_LINE, 1, 0x55);

			for(int i=1; i<=36; i++) {
				int y = (int)(stats.get(type,i)+0.5);
				if(y>100) y=100;
				else if(y<0) y=0;
				fb->fillRect((i-1)<<2, LCM_HEIGHT-2-y, 3, 3);
				if(i==36) {
					fb->fillRect(142, 40, 1, LCM_HEIGHT-42-y);
					fb->fillRect(120, 40, 22, 1);
					fb->fillRect(120, 32, 1, 20);
				}
			}

			break;
		}
	case TTmenu::EVENT_ENTER:
		debuglog(5, "tempWg enter", 0);
	case TTmenu::EVENT_SECOND_TICK:
		item->invalidate();
		break;
	case TTmenu::EVENT_MEASURE_ITEM:
		p1.s1 = LCM_WIDTH;
		p1.s2 = LCM_HEIGHT;
		break;
	}

	return false;
}

static void readFile(char * buf, size_t maxLength, const char * filepath)
{
	int len;
	int fd;

	fd = open(filepath, O_RDONLY);
	if(fd == -1) {
		perror("Failed to open file entry");
		buf[0] = '\0';
		return;
	}

	len = read(fd, buf, maxLength);
	if(len == -1) {
		perror("Read failed");
		buf[0] = '\0';
		close(fd);
		return;
	}
	buf[len - 1] = '\0';
	close(fd);
}

static void readIntFromFile(int * result, const char * filepath)
{
	char buf[16];

	readFile(buf, sizeof buf, filepath);

	*result = atoi(buf);
}

bool governorWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	switch (e) {
	case TTmenu::EVENT_DRAW:
		{
			TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);
			char filevalue[16];
			wchar_t buf[16];
			wchar_t *hdr = (wchar_t *) (item->content);
			int y = LCM_HEIGHT-101;
			int w;
			int freq;

			fb->drawText((LCM_BITS_PER_LINE - fb->getTextWidth(hdr, &font_fn_boldcn_18)) / 2, 1, hdr, &font_fn_boldcn_18, TTframebuffer::DRAW_NORMAL);
			fb->fillRect(0, 20, LCM_BITS_PER_LINE, 1);

			readFile(filevalue, sizeof filevalue, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
			swprintf(buf, 16, L"%s", filevalue);
			w = fb->getTextWidth(buf, &font_fn_heavycn_22);
			fb->drawText((LCM_BITS_PER_LINE - w) / 2, 30, buf, &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);

			readIntFromFile(&freq, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
			swprintf(buf, 16, L"%d", freq / 1000);
			w = fb->getTextWidth(L"CPU:", &font_fn_heavycn_22);
			fb->drawText(LCM_BITS_PER_LINE / 2 - w - 2, y, L"CPU:", &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
			fb->drawText(LCM_BITS_PER_LINE / 2 + 2, y, buf, &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
			y += 22;

			readIntFromFile(&freq, "/sys/devices/system/iva/iva0/freqscaling/scaling_cur_freq");
			swprintf(buf, 16, L"%d", freq / 1000);
			w = fb->getTextWidth(L"IVA:", &font_fn_heavycn_22);
			fb->drawText(LCM_BITS_PER_LINE / 2 - w - 2, y, L"IVA:", &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
			fb->drawText(LCM_BITS_PER_LINE / 2 + 2, y, buf, &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
			y += 22;

			readIntFromFile(&freq, "/sys/devices/system/iss/iss0/freqscaling/scaling_cur_freq");
			swprintf(buf, 16, L"%d", freq / 1000);
			w = fb->getTextWidth(L"ISS:", &font_fn_heavycn_22);
			fb->drawText(LCM_BITS_PER_LINE / 2 - w - 2, y, L"ISS:", &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
			fb->drawText(LCM_BITS_PER_LINE / 2 + 2, y, buf, &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
			y += 22;

			readIntFromFile(&freq, "/sys/devices/system/l3/l30/freqscaling/scaling_cur_freq");
			swprintf(buf, 16, L"%d", freq / 1000);
			w = fb->getTextWidth(L"L3:", &font_fn_heavycn_22);
			fb->drawText(LCM_BITS_PER_LINE / 2 - w - 2, y, L"L3:", &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
			fb->drawText(LCM_BITS_PER_LINE / 2 + 2, y, buf, &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);

			break;
		}
	case TTmenu::EVENT_ENTER:
		debuglog(5, "governorWd enter", 0);
	case TTmenu::EVENT_SECOND_TICK:
		item->invalidate();
		break;
	case TTmenu::EVENT_MEASURE_ITEM:
		p1.s1 = LCM_WIDTH;
		p1.s2 = LCM_HEIGHT;
		break;
	}

	return false;
}


bool resoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
		int idr_frequency, timelapse0_5sec_case = 0;

		if (isRecording)
			return true;
		debuglog(4, "res option Wg: res=%d", (int)item->param);

		if(((int)item->param) < 0) {
			switch ((int)item->param) {
				case -1:
					recordingRes = TTV_RES_1080_30_WIDE;
					idr_frequency = 1;
					break;
				case -2:
					recordingRes = TTV_RES_4K_14_WIDE;
					idr_frequency = 1;
					break;
			}
			timelapse0_5sec_case = 1;
		}
		else if(((int)item->param) <= TTV_RES_WVGA_150_WIDE) {
			/* Regular video recording */
			recordingRes = (ttv_resolution_t)((int)item->param);
			idr_frequency = 1;
		}
		else {
			/* Slow motion case */
			switch ((int)item->param) {
				case 23 :
					recordingRes = TTV_RES_1080_60_WIDE;
					idr_frequency = 2;
					break;
				case 24 :
					recordingRes = TTV_RES_720_120_WIDE;
					idr_frequency = 4;
					break;
				case 25 :
					recordingRes = TTV_RES_WVGA_180_WIDE;
					idr_frequency = 6;
					break;
			}
		}
		ttv_configure(recordingRes, 0);
		ttv_set_idr_frequency(idr_frequency);
		if(timelapse0_5sec_case) {
			ttv_set_encoder_framerate(2);
		}
		videofile.set_thumb_frameRate(ttv_get_thumb_fps());
	}
	return optionWg(e, item, p1);
}

bool wlanoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
		debuglog(4, "wlan option Wg: res=%d", (int)item->param);
		system((int)item->param ? "wlan_on" : "wlan_off");
	}
	return optionWg(e, item, p1);
}

bool backlightoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
		debuglog(4, "backlight option Wg: res=%d", (int)item->param);
		backlight = (int)item->param;
		resetBacklight();
	}
	return optionWg(e, item, p1);
}

bool beepoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
		debuglog(4, "beep option Wg: res=%d", (int)item->param);
		ttsystem_beep_enabled((int)item->param);
	}
	return optionWg(e, item, p1);
}

bool bitrateoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
        if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "bitrate option Wg: res=%d", (int)item->param);
                ttv_bitrate((int)item->param);
        }
        return optionWg(e, item, p1);
}

bool rawdumpWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
		debuglog(4, "rawdump enable=%d", (int)item->param);
		rawDumpEnable    = (int)item->param;
#else
		debuglog(4, "Build does not support raw dump",0);
#endif
	}
	return optionWg(e, item, p1);
}

bool transcodeoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		char buf[256];
		int numitems = 0;
		for (TTmenu::MenuItem * i = transcodeoptionscreen.items; !i->isEmpty(); i++) {
			sprintf(buf, "/mnt/mmc/TTCamVideo%02d.h264", (int)i->param);
			bool exist = sd_fileExist(buf);
			debuglog(5, "transcode: file %s %s", buf, exist ? "exist" : "notfound");
			if (exist) {
				i->flags &= ~TTmenu::MENU_DISABLED;
				numitems++;
			} else
				i->flags |= TTmenu::MENU_DISABLED;
		}
		transcodeoptionscreen.recalculate();
		if (!numitems)
			return true;
	}
	return optionWg(e, item, p1);
}

bool transoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		setTranscodeInputParams((int)item->param);
		infile.setfileno((int)item->param);
	}
	return optionWg(e, item, p1);
}

bool secWriteSpeedWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
		debuglog(4, "Sec Write speed param=%d !!!", (int)item->param);
		secWriteSpeed = (int)item->param;
	}
	return optionWg(e, item, p1);
}

bool powerModeWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	ttsystem_powermode_t mode;

	if (e == TTmenu::EVENT_ENTER) {
		debuglog(4, "powermode option Wg: param=%d", (int)item->param);


#ifdef LONGBEACH_POWERMODE_OBSOLETE
		switch((int) item->param) {
		case 0: /* active */
			mode = TTSYSTEM_POWERMODE_ACTIVE;
			break;

		case 1: /* passive */
			mode = TTSYSTEM_POWERMODE_PASSIVE;
			break;
		}
#else
		mode = TTSYSTEM_POWERMODE_DEFAULT;
#endif

		ttsystem_set_powermode(mode);
	}
	return optionWg(e, item, p1);
}

bool sensorOrientationWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	ttv_rotation_t rotation;
	 if (e == TTmenu::EVENT_ENTER) {
                debuglog(4, "sensor orientation option Wg: param=%d", (int)item->param);

                switch((int) item->param) {
                case 0: /* UPSIDE DOWN */
                        rotation = TTV_ROTATION_UPSIDE_DOWN;
                        break;

                case 1: /* RIGHT SIDE UP */
                        rotation = TTV_ROTATION_RIGHT_WAY_UP;
                        break;
                }

                ttv_set_sensor_rotation(rotation);
        }
	return optionWg(e, item, p1);
}

static void setTranscodeInputParams(int fileno)
{
	FILE *fpFileInfo = NULL;
	char str[256];
	int fps;
	int width;
	int height;
	snprintf(str, sizeof(str), "/mnt/mmc/TTCamVideo%02d_pri.info", fileno);
	fpFileInfo = fopen(str,"r");
	if (!fpFileInfo) {
		debuglog(4, "Failed to open info file for transcode", 0);
		return;
	}
	fscanf(fpFileInfo, "%10d %10d %10d\n",&width, &height, &fps);
	switch (width) {
		case 3840:
			transcodeInputRes = TTV_VRES_4K;
			break;
		case 2704:
			transcodeInputRes = TTV_VRES_2_7K;
			break;
		case 1920:
			transcodeInputRes = TTV_VRES_1080P;
			break;
		case 1280:
			transcodeInputRes = TTV_VRES_720P;
			break;
		case 848:
			transcodeInputRes = TTV_VRES_480P;
			break;
		default:
			transcodeInputRes = TTV_VRES_1080P;
		}
	debuglog(4,"Transcoding %dx%d (fps = %d) video\n",width, height,fps);
	transcodeInputFps = fps;
	fclose(fpFileInfo);
}

bool transcodeWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_EXIT_SUBMENU) {
		debuglog(4, "transcodeWg exit submenu", 0);
		if (isTranscoding) {
			insertTranscodeEOS = true;
		}
		return false;
	}
	if (e == TTmenu::EVENT_DRAW) {
		TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);
		if(isTranscoding) {
			fb->fillRect(0, 0, (infile.getfilereadpercentage() * LCM_BITS_PER_LINE) / 100, item->height);
		} else {
			fb->drawText(4, 0, L"< FINISHED!", &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
		}
		return false;
	}
	if (e == TTmenu::EVENT_SECOND_TICK) {
		item->invalidate();
		return false;
	}
	return optionWg(e, item, p1);
}

bool transcodecodecWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		debuglog(4,"selected the transcoding codec %d\n",(int)item->param);
		switch((int)item->param)
		{
			case 0:
				transcodeOutCodec = TTV_TYPE_H264HP;
				break;
			case 1:
				transcodeOutCodec = TTV_TYPE_H264MP;
				break;
		}
		if((int)item->param == 2) {//MJPEG
			transcodeOutCodec = TTV_TYPE_MJPEG;
			transcodeOutputRes = transcodeInputRes;
			transcodeOutputFps = transcodeInputFps;

			debuglog(4,"MJPEG selected",0);
			if (ttv_config_transcode(transcodeInputRes, transcodeInputFps,
								transcodeOutputRes, transcodeOutputFps,
								transcodeOutCodec) ) {
				startTranscoding(infile.readfileno());
			}
			else
				debuglog(4, "Transcoding is not supported", 0);
		}
	}
	return optionWg(e, item, p1);
}

bool transcoderesolutionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		debuglog(4,"selected the transcoding resolution %d\n",(int)item->param);
		switch((int)item->param)
		{
			case 0:
				transcodeOutputRes = TTV_VRES_1080P;
				break;
			case 1:
				transcodeOutputRes = TTV_VRES_720P;
				break;
			case 3:
				transcodeOutputRes = TTV_VRES_2_7K;
				break;
		}
		if((int)item->param == 2) {//WVGA
			debuglog(4,"WVGA selected",0);
			transcodeOutputRes = TTV_VRES_480P;
			transcodeOutputFps = 30;

			if (ttv_config_transcode(transcodeInputRes, transcodeInputFps,
								transcodeOutputRes, transcodeOutputFps,
								transcodeOutCodec) ) {

				startTranscoding(infile.readfileno());
			}
			else
				debuglog(4, "Transcoding is not supported", 0);
		}
	}
	return optionWg(e, item, p1);
}

bool transcodefpsWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		debuglog(4,"selected the transcoding fps %d\n",(int)item->param);
		switch((int)item->param)
		{
			case 0:
				transcodeOutputFps = 60;
				break;
			case 1:
				transcodeOutputFps = 50;
				break;
			case 2:
				transcodeOutputFps = 30;
				break;
			case 3:
				transcodeOutputFps = 25;
				break;
			case 4:
				transcodeOutputFps = 120;
				break;
			case 5:
				transcodeOutputFps = 100;
				break;
			case 6:
				transcodeOutputFps = 15;
				break;
		}
		if (ttv_config_transcode(transcodeInputRes, transcodeInputFps,
								transcodeOutputRes, transcodeOutputFps,
								transcodeOutCodec)) {
#ifdef SINGLE_THREAD_TRANSCODE
			transcodeFrameMultiplyer = transcodeInputFps / transcodeOutputFps;
			debuglog(5,"transcodeFrameMultiplyer = %d iFps = %d oFps=%d\n",transcodeFrameMultiplyer,transcodeInputFps,transcodeOutputFps);
			if( ( transcodeInputFps % transcodeOutputFps) > 0)
			{
				transcodeFrameMultiplyer++;
				debuglog(4,"readTranscodingFrame = %d\n",transcodeFrameMultiplyer);
			}
#endif
			startTranscoding(infile.readfileno());
		}
		else
			debuglog(4, "Transcoding is not supported", 0);
	}
	return optionWg(e, item, p1);
}

bool deviceoptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_DRAW) {
		wchar_t  devw[32];
		char dev[32];

		ttv_get_device_serial(dev, 32);
		swprintf(devw, 32, L"%hs", dev);
		TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);
		fb->drawText(4, 0, devw, &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
		return false;
	}
	if (e == TTmenu::EVENT_SECOND_TICK) {
		item->invalidate();
		return false;
	}
	return optionWg(e, item, p1);
}

bool stillimageWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
		stillImageRes = (ttv_resolution_t)(int)item->param;
		isBurstCapturing = false;
	}

	if (e == TTmenu::EVENT_EXIT_SUBMENU)
		isStillMode = false;

	if (e == TTmenu::EVENT_ENTER_SUBMENU)
		isStillMode = true;

	return optionWg(e, item, p1);
}

bool burstimageWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER) {
		stillImageRes = (ttv_resolution_t)(int)item->param;
		isBurstCapturing = true;
	}

	return optionWg(e, item, p1);
}

bool timelapseWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	debuglog(4, "timelapse event e = %d",e);
	if (e == TTmenu::EVENT_EXIT_SUBMENU)
		isTimelapseCapturing = false;
	else if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		if (isRecording || isTranscoding || isStillCapturing || isContinuousCapturing) {
			debuglog(4, "Cannot take timelapse as transcoding/recoring is in progress", 0);
			return false;
		}
		if (!freespace) {
			debuglog(3, "SD card full! Canceling timelapse!", 0);
			return optionWg(e,item,p1);
		}
		timelapseInterval = (int)item->param;
		debuglog(4, "timelapse interval set to %d",timelapseInterval);
		timelapseImageRes = TTV_RES_1080_30_WIDE;
		if (timelapseInterval > 60) {
			timelapseImageRes = TTV_RES_4K_15_WIDE;
			timelapseInterval -= 60;
		}
		if (++numfiles == 100)
			numfiles = 1;
		if (!videofile.open(numfiles)) {
			debuglog(4, "no file? can't start recording... fail", 0);
			return false;
		}
		if (!videofile.write_fileinfo(timelapseImageRes)) {
			debuglog(4, "error in writing file information", 0);
		}
		isTimelapseCapturing = true;
		firstTimelapseCapturing = true;
		firstkeyframe = false;

	}
	return optionWg(e,item,p1);
}

bool timelapseSubmenuWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	debuglog(4, "timelapse submenu event e = %d",e);
	if (e == TTmenu::EVENT_EXIT_SUBMENU) {
	if (isTimelapseCapturing)
		stopTimelapseCapture();
	}
	else if (e == TTmenu::EVENT_DRAW) {
		if (!isTimelapseCapturing) {
			TTframebuffer *fb = static_cast<TTframebuffer*> (p1.p);
			fb->fillRect(0, 0, item->width, item->height-1, 0xFF);
			fb->drawText(4, 0, L"SD card full!", &font_fn_heavycn_22, TTframebuffer::DRAW_INVERTED);
			return false;
		}
	}
	return optionWg(e,item,p1);
}

bool continuousWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	debuglog(4, "continuous event e = %d",e);
	if (e == TTmenu::EVENT_EXIT_SUBMENU)
		isContinuousCapturing = false;
	else if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		if (isRecording || isTranscoding || isStillCapturing || isTimelapseCapturing) {
			debuglog(4, "Cannot take continuous images as transcoding/recoring is in progress", 0);
			return false;
		}
		if (!freespace) {
			debuglog(3, "SD card full! Canceling continuous capture!", 0);
			return optionWg(e,item,p1);
		}
		continuousInterval = (int)item->param;
		debuglog(4, "continuous interval set to %d",continuousInterval);
		continuousImageRes = TTV_RES_8MP_STILL;
		if (continuousInterval > 60) {
			continuousImageRes = TTV_RES_16MP_STILL;
			continuousInterval -= 60;
		}
		isContinuousCapturing = true;
		firstContinuousCapturing = true;

	}

	return optionWg(e,item,p1);
}

bool continuousSubmenuWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	debuglog(4, "continuous submenu event e = %d",e);
	if (e == TTmenu::EVENT_EXIT_SUBMENU) {
		stopContinuousCapture();
	}
	else if (e == TTmenu::EVENT_DRAW) {
		if (!isContinuousCapturing) {
			TTframebuffer *fb = static_cast<TTframebuffer*> (p1.p);
			fb->fillRect(0, 0, item->width, item->height-1, 0xFF);
			fb->drawText(4, 0, L"SD card full!", &font_fn_heavycn_22, TTframebuffer::DRAW_INVERTED);
			return false;
		}
	}
	return optionWg(e,item,p1);
}

void *doRepairSdcard(void *arg)
{
	debuglog(4, "repairing sd card started", 0);

	TTtime t;
	int ret = ttsystem_check_sdcard(true);
	debuglog(4, "===repair took %.1f seconds", (float)TTtime().diffms(t) / 1000.0);

	if (ret)
		debuglog(4, "error repairing sd card, ret = %d", ret);

	isRepairingSdcard = false;
	return (void *)ret;
}

void startRepairSdcard()
{
	assert(!isRepairingSdcard);

	if (isRecording || isTranscoding) {
		debuglog(3, "Can't transcode, (trans)recording in progress", 0);
		return;
	}

	isRepairingSdcard = true;
	repairsdThJoined = false;
	pthread_create(&repairsdTh, 0, doRepairSdcard, 0);
}

int stopRepairSdcard()
{
	int status;
	int rc = pthread_join(repairsdTh, (void **)&status);
	repairsdThJoined = true;
	keys.enable(true);

	return rc ? -1 : status;
}

bool repairSdcardOptionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	if (e == TTmenu::EVENT_ENTER_SUBMENU) {
		keys.enable(false);
		startRepairSdcard();
		return  false;
	}

	return optionWg(e, item, p1);
}

bool repairSdcardWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	static wchar_t progressText[LCM_BITS_PER_LINE];
	static unsigned progressIndex = 0;

	if (e == TTmenu::EVENT_DRAW) {
		TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);
		if (isRepairingSdcard)
			fb->drawText(4, 0, progressText, &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
		else {
			static int repairStatus = 0;
			if (!repairsdThJoined) {
				repairStatus = stopRepairSdcard();
				progressIndex = 0;
				progressText[0] = 0;
			}
			fb->drawText(4, 0, repairStatus ? L"< ERROR" : L"< FINISHED!", &font_fn_heavycn_22, TTframebuffer::DRAW_NORMAL);
		}
		return false;
	} else if (e == TTmenu::EVENT_SECOND_TICK) {
		if (isRepairingSdcard && progressIndex < LCM_BYTES_PER_LINE - 1) {
			progressText[progressIndex++] = L' ';
			progressText[progressIndex++] = L'.';
			progressText[progressIndex] = 0;
		}
		item->invalidate();
		return false;
	}

	return optionWg(e, item, p1);
}

// auto gen menus
#include "gfx/menu.c"

static void do_trigger_wd();

void startContinuousCapture()
{
	static TTtime ContinuousTime;

	if (continuousImageRes == TTV_RES_NONE || continuousInterval == 0) {
		debuglog(4, "error in continuous parameters", 0);
		return;
	}
	if (TTtime().diffms(ContinuousTime) > continuousInterval*1000 || firstContinuousCapturing == true) {
		static unsigned char leds = 255;

		ContinuousTime = TTtime();
		firstContinuousCapturing = false;
		if (!ttv_take_snapshot(1, TTV_TYPE_MJPEG, continuousImageRes, -1, false, 30)) {
			debuglog(4, "error in taking snapshot in continuous mode", 0);
			return;
		}
		setLedBrightness(LED_NAME__LED1, leds);
		leds = ~leds;
	}
}

void stopContinuousCapture()
{
	isContinuousCapturing = false;
	firstContinuousCapturing = false;
	setLedBrightness(LED_NAME__LED1, 0);
	continuousInterval = 0;
	continuousImageRes = TTV_RES_NONE;
}

void stopTimelapseCapture()
{
	isTimelapseCapturing = false;
	firstTimelapseCapturing = false;
	setLedBrightness(LED_NAME__LED1, 0);
	timelapseInterval = 0;
	timelapseImageRes = TTV_RES_NONE;
	if(videofile.isopen())
		videofile.close();
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
	if (rawDumpEnable) {
		rawTlFrameCnt = 0;
	}
#endif
}

void startTimelapseCapture()
{
	static TTtime TimelapseTime;

	if (timelapseImageRes == TTV_RES_NONE || timelapseInterval == 0) {
		debuglog(4, "error in timelapse parameters", 0);
		return;
	}
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
	if (rawDumpEnable) {
		ttv_enable_raw_capture();
		rawCaptureRes = timelapseImageRes;
	}
#endif
	if (TTtime().diffms(TimelapseTime) > timelapseInterval*1000 || firstTimelapseCapturing == true) {
		static unsigned char leds = 255;

		TimelapseTime = TTtime();
		if (!ttv_take_snapshot(1, TTV_TYPE_H264HP, timelapseImageRes, -1, firstTimelapseCapturing, 30)) {
			debuglog(4, "error in taking snapshot in timelapse mode", 0);
			return;
		}
		firstTimelapseCapturing = false;
		setLedBrightness(LED_NAME__LED1, leds);
		leds = ~leds;
	}
}

void stoprecording()
{
	if (isTranscoding)
		stopTranscoding();
	if (!isRecording)
		return;
	ttv_stop(viewfinder.isStarted()? TTV_STREAM_PRI : TTV_STREAM_ALL);
	mainscreen.invalidate();
	menu.update();
	processframes(10000);
	//File will be closed once EOS is received
	//isRecording = false;
	//videofile.close();
	debuglog(4, "recording stopped", 0);
	setLedBrightness(LED_NAME__LED1, 0);
	//refreshdisplay();
}

void startrecording()
{
	if (isTranscoding)
		stopTranscoding();
	if (isRecording)
		stoprecording();
	if (!freespace) {
		debuglog(4, "start recording but no free space... fail", 0);
		return;
	}
	if (++numfiles == 100)
		numfiles = 1;
	if (!videofile.open(numfiles)) {
		debuglog(4, "no file? can't start recording... fail", 0);
		return;
	}

	if (!videofile.write_fileinfo(recordingRes)) {
		debuglog(4, "error in writing file information", 0);
	}

	{
		TTtime start;
		debuglog(4, "-------SWITCH to video", 0);
		ttv_mode(TTV_MODE_VIDEO);
		debuglog(4, "-------SWITCH to video finished", 0);

#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
		if (rawDumpEnable) {
			ttv_enable_raw_capture();
			rawDumpStarted = true;
			rawCaptureRes  = recordingRes;
		}
#endif

		isRecording = ttv_start(TTV_STREAM_PRI | TTV_STREAM_MJPG);
		debuglog(4, "start video %lu ms", TTtime().diffms(start));
	}
	firstkeyframe = false;
	recstart.now();
	frameno = 0;
	setLedBrightness(LED_NAME__LED1, 255);
	debuglog(5, "recording started", 0);
}

void startstillcapture()
{
	int count;
	if (isStillCapturing)
	{
		debuglog(4, "already trying to capture still image", 0);
		return;
	}
	isStillCapturing = true;

	if (isBurstCapturing)
		stillimagecount = 10;
	else
		stillimagecount = 1;
	/*Printing file names of all the still images*/
	for(count=1;count<=(stillimagecount*2);count++)
	{
		printf("\n##still_%02d.jpg##",numfiles+count);
	}
	fflush(stdout);

	setLedBrightness(LED_NAME__LED1, 255);

#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
	if (rawDumpEnable) {
		ttv_enable_raw_capture();
		rawCaptureRes  = stillImageRes;
	}
#endif
	if (!ttv_take_snapshot(stillimagecount, TTV_TYPE_MJPEG, stillImageRes, -1, false, 30)) {
		isStillCapturing = false;
		debuglog(4, "error in taking snapshot", 0);
	}
}

void stopstillcapture()
{
	isStillCapturing = false;
	setLedBrightness(LED_NAME__LED1, 0);
}

void writestillimage(void * addr, int size, int timestamp)
{
	FILE *fpstill = NULL, *fpInfo = NULL ;
	char filename[250];

#ifdef DUMP_STILL_IMAGE_ADDITIONAL_INFO
	FILE *fpstillinfo = NULL;
	char filenameinfo[250];
#endif
	sprintf(filename, "/mnt/mmc/still_%02d.jpg", ++numfiles);
	debuglog(4, "capturing image %s\n", filename);
	fpstill = fopen(filename, "wb");
#ifdef DUMP_STILL_IMAGE_ADDITIONAL_INFO
	sprintf(filenameinfo, "/mnt/mmc/still_%02d.txt", numfiles);
	fpstillinfo = fopen(filenameinfo, "w");
#endif
	if (fpstill) {
		fwrite(addr, size, 1, fpstill);
		fclose(fpstill);
	}

	sprintf(filename, "/mnt/mmc/still_%02d.info", numfiles);
	fpInfo = fopen(filename, "wb");
	if (fpInfo) {
		fprintf(fpInfo, "JPG Size  : %d\n", size);
		fprintf(fpInfo, "Timestamp : %d\n", timestamp);
		fclose(fpInfo);
	}

#ifdef DUMP_STILL_IMAGE_ADDITIONAL_INFO
	if (fpstillinfo){
		unsigned int stillinfo_buf[STILL_ADDITIONAL_INFO_SIZE];

		memcpy(stillinfo_buf,((char *)addr+size),(STILL_ADDITIONAL_INFO_SIZE*sizeof(unsigned int)));
		fprintf(fpstillinfo,"\nExposureTime   - %d\n",stillinfo_buf[0]);
		fprintf(fpstillinfo,"\nApertureLevel  - %d\n",stillinfo_buf[1]);
		fprintf(fpstillinfo,"\nSensorGain     - %d\n",stillinfo_buf[2]);
		fprintf(fpstillinfo,"\nIpipeGain      - %d\n",stillinfo_buf[3]);
		fprintf(fpstillinfo,"\nRGain          - %d\n",stillinfo_buf[4]);
		fprintf(fpstillinfo,"\nGGain          - %d\n",stillinfo_buf[5]);
		fprintf(fpstillinfo,"\nBGain          - %d\n",stillinfo_buf[6]);
		fprintf(fpstillinfo,"\nColorTemp      - %d\n",stillinfo_buf[7]);

		for(int i=8;i<STILL_ADDITIONAL_INFO_SIZE;i++){
			fprintf(fpstillinfo," %d ",stillinfo_buf[i]);
		}
		fclose(fpstillinfo);
	}
#endif
	stillimagecount--;
}

bool processframes(int maxframes = 10)
{
	bool res = false;
	ttv_frame_t frame;
	bool do_not_release_frame = false;

	for (int i = 0; i < maxframes && ttv_get_frame(&frame); i++) {
		/* Trigger this every frame to avoid mishaps :-) */
		do_trigger_wd();

		res = true;
		do_not_release_frame = false;

		if (!frame.address || !frame.size) {
			if (!(frame.status & TTV_STATUS_EOS)) {
				debuglog(1, "FAILED frame?! addr=%lu, size=%d, pts=%lu, ch=%d, key=%d, type=%d, status=%x",
					 (unsigned long)frame.address, frame.size, frame.pts, frame.channel, frame.iskey,
					 frame.type, frame.status);
			}
			goto eos;
		}

		debuglog(5, "Got frame size=%d, pts=%lu, ch=%d, key=%d, type=%d", frame.size, frame.pts, frame.channel,
			 frame.iskey, frame.type);

		switch (frame.channel) {
			case TTV_CHANNEL_JPEG_WVGA:
				if (isTranscoding || isTimelapseCapturing)
					videofile.write_thumb(frame.address, frame.size, frame.pts);
				else if(isRecording) {
					if((numSecFrames % secWriteSpeed) == 0) {
						videofile.write_thumb(frame.address, frame.size, frame.pts);
						numSecFrames = 0;
					}
					++numSecFrames;
				}
				if (viewfinder.setframe(frame.address, frame.size))
					do_not_release_frame = true;
			break;

			case TTV_CHANNEL_JPEG_FULL:
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
				if (isStillCapturing && rawDumpEnable && freespace) {
						debuglog(3, "Writing RAW still image %02d", numfiles);
						ttv_write_raw_buffer(numfiles+1, rawCaptureRes, false, -1);
				}
				/* break intentionally left because below code is common for JPEG_FULL and JPEG_VGA*/
#endif
			case TTV_CHANNEL_JPEG_VGA:
				debuglog(4, "Got still image with exposure time =%d micro seconds, ISO=%d", frame.exposuretime, frame.isogain);
				if (!freespace) {
					debuglog(3, "sd card full! closing file!", 0);
					ttv_release_frame(frame.address);
					if (isContinuousCapturing)
					{
						stopContinuousCapture();
						coutinuous_stop.invalidate();
					}
					return res;
				}
				writestillimage(frame.address, frame.size, frame.pts);
			break;

			case TTV_CHANNEL_H264: {
				if (isTranscoding) {
					debuglog(5,"transcode write %lu", frame.size);
					videofile.write_frame(frame.address, frame.size, frame.pts);
				}
				else if (!(isRecording || isTimelapseCapturing) || !videofile.isopen())
					debuglog(3, "Frame acquired but no file or not started?!", 0);
				else if (!firstkeyframe && !frame.iskey)
					debuglog(3, "Frame skipped, waiting for key frame", 0);
				else {
					if (frame.iskey) {
						if (!freespace) {
							debuglog(3, "sd card full! closing file!", 0);
							ttv_release_frame(frame.address);
							if(isRecording)
								stoprecording();
							else if(isTimelapseCapturing)
							{
								stopTimelapseCapture();
								timelapsescreen.invalidate();
							}
							return res;
						}
						if (videofile.getBytesWritten() > MAX_FILE_SIZE) {
							if (++numfiles == 100)
								numfiles = 1;
							if (!videofile.open(numfiles)) {
								debuglog(1, "file reopen failed! stopping recording!", 0);
								ttv_release_frame(frame.address);
								stoprecording();
								return res;
							}
							/*Use the set resolution for recording (recordingRes), unless
							* the timelapse recording is in progress. Then use timelapseImageRes */
							ttv_resolution_t tempRes = recordingRes;
							if (isTimelapseCapturing)
								tempRes = timelapseImageRes;
							if (!videofile.write_fileinfo(tempRes)) {
								debuglog(4, "error in writing file information", 0);
							}

							do_trigger_wd();
						}
					}
					firstkeyframe = true;
					videofile.write_frame(frame.address, frame.size, frame.pts);
#ifdef LONGBEACH_ENABLE_RAW_CAPTURE
					if (isTimelapseCapturing && rawDumpEnable && freespace) {
							debuglog(3, "Writing RAW timelapse video %02d, frame %03d", numfiles, rawTlFrameCnt);
							ttv_write_raw_buffer(numfiles, rawCaptureRes, true, rawTlFrameCnt);
							++rawTlFrameCnt;
					}
					/* write only the first raw frame of the video */
					if (isRecording && rawDumpStarted && freespace) {
							debuglog(3, "Writing 1st frame of video %02d", numfiles);
							ttv_write_raw_buffer(numfiles, rawCaptureRes, true, -1);
							rawDumpStarted = false;
					}
#endif
				}
			}
			break;
		}
eos:
		if (frame.status & TTV_STATUS_EOS) {
			switch (frame.channel) {
				case TTV_CHANNEL_H264:
				case TTV_CHANNEL_JPEG_FULL:
					eos_on_primary = true;
				break;
				case TTV_CHANNEL_JPEG_WVGA:
				case TTV_CHANNEL_JPEG_VGA:
					eos_on_secondary = true;
				break;
			}

			if ((eos_on_primary && (eos_on_secondary || viewfinder.isStarted())) ||
				(isTranscoding && (eos_on_primary || eos_on_secondary))) {
				if (isTranscoding)
					stopTranscoding();
				else if (isRecording) {
					debuglog(4, "EOS got for h264 while record", 0);
					isRecording = false;
					videofile.close();
				}
				else if (isStillCapturing) {
					debuglog(4, "EOS got for still capture", 0);
					stopstillcapture();
				}
				eos_on_primary = false;
				eos_on_secondary = false;
			}
		}

		if (!do_not_release_frame)
			ttv_release_frame(frame.address);
	}

	return res;
}

/**
 * Empty signal handler needed by do_sleep_block_sigterm()
 */
static void handle_sigalrm(int signal)
{
	/* Nothing here */
}

/**
 * Performs a sleep while blocking SIGTERM signal.
 */
static void do_sleep_block_sigterm(int seconds)
{
	struct sigaction sa;
	sigset_t mask;

	/* Intercept and ignore SIGALRM, Remove the handler after first signal */
	sa.sa_handler = &handle_sigalrm;
	sa.sa_flags = SA_RESETHAND;
	sigfillset(&sa.sa_mask);
	sigaction(SIGALRM, &sa, NULL);

	/* Get the current signal mask */
	sigprocmask(0, NULL, &mask);

	/* Unblock SIGALRM and block SIGTERM */
	sigdelset(&mask, SIGALRM);
	sigaddset(&mask, SIGTERM);

	/* Wait with this mask */
	alarm(seconds);
	sigsuspend(&mask);
}

/**
 * Handles SIGUSR2 signal which is sent to us when device temperature exceeds threshold
 *
 * Can be tested by running:
 *
 * systemctl kill --signal=SIGUSR2 camera-app.service;
 */
static void device_temperature_signal_handler(int signum)
{
	exit_reason = EXIT_REASON__SIGTEMP;
}

/**
 * Handles SIGPWR signal which is send to us when battery becomes critical low
 *
 * Can be tested by running:
 *
 * systemctl kill --signal=SIGPWR camera-app.service;
 */
static void battery_critical_low_signal_handler(int signum)
{
	exit_reason = EXIT_REASON__BATTERY_CRITICAL_LOW;
}

static void terminate_signal_handler(int signum) {
	if (exit_reason == 0) {
		exit_reason = EXIT_REASON__SIGTERM;
	}
}

/*
 * \brief Handles SIGUSR1 signal. Stops triggering watchdog for testing purpose.
 */
static void stop_wd_trigger_signal_handler(int signum) {
	trigger_wd = 0;
}

/*
 * \brief Trigger watchdog. Do not do this after receiving SIGUSR1 signal
 */
static void do_trigger_wd() {
	long wdtrigger_diff;
	static TTtime wdTriggerTime;

	wdtrigger_diff = TTtime().diffms(wdTriggerTime);
	if (trigger_wd && wdtrigger_diff >= 1000) {
		ttsystem_trigger_watchdog();
		wdTriggerTime.now();
	}

	if(wdtrigger_diff >= 3000)
		debuglog(3, "Time between watchdog triggers: %d", wdtrigger_diff);
}

int main()
{
	int key = 0;
	unsigned long modi = 0;
	struct sigaction sigpwr_action;
	struct sigaction sigterm_action;
	struct sigaction sigusr1_action;
	struct sigaction sigusr2_action;
	sigset_t sigterm_set;

	/* Trigger SW Watchdog */
	do_trigger_wd();

	/* Install SIGPWR signal handler */
	memset(&sigpwr_action, 0, sizeof(sigpwr_action));
	sigpwr_action.sa_handler = battery_critical_low_signal_handler;
	sigaction(SIGPWR, &sigpwr_action, NULL);

	/* Install SIGTERM signal handler */
	memset(&sigterm_action, 0, sizeof(sigterm_action));
	sigterm_action.sa_handler = terminate_signal_handler;
	sigaction(SIGTERM, &sigterm_action, NULL);

	/* Install SIGUSR1 signal handler */
	memset(&sigusr1_action, 0, sizeof(sigusr1_action));
	sigusr1_action.sa_handler = stop_wd_trigger_signal_handler;
	sigaction(SIGUSR1, &sigusr1_action, NULL);

	/* Install SIGUSR2 signal handler */
	memset(&sigusr2_action, 0, sizeof(sigusr2_action));
	sigusr2_action.sa_handler = device_temperature_signal_handler;
	sigaction(SIGUSR2, &sigusr2_action, NULL);

	/* Unblock SIGTERM, blocked by startup script */
	sigemptyset(&sigterm_set);
	sigaddset(&sigterm_set, SIGTERM);
	sigprocmask(SIG_UNBLOCK, &sigterm_set, NULL);

	debuglog(4, "========== Hello, this is platform demo!", 0);

	setLedBrightness(LED_NAME__LED1, 0);

	menu.init(&introscreen);
	if (!sd_checkCard()) {
		ttsystem_app_started();
		debuglog(0, "no SD card present!", 0);
		menu.init(&nosdcard);

		/* Allow user to poweroff device */
		while (!exit_reason) {
			if (keys.get(key, modi)) {
				resetBacklight();
				if (key == TTKEY_OFF && modi == 1) {
					exit_reason = EXIT_REASON__POWEROFF;
					menu.init(&offscreen);
					break;
				}

			}
			if (!menu.update())
				::usleep(10000);

			do_trigger_wd();
		}
		return exit_reason == EXIT_REASON__POWEROFF ? 0 : exit_reason;
	}
	freespace = sd_getFreeSpace();	// once per second
	sd_getFileCount(numfiles);


	resetBacklight();
	menu.setMenuTicker(globalTicker);

	{
		TTtime start;
		ttv_mode(TTV_MODE_VIDEO);
		debuglog(4, "create pipline took %lu ms", TTtime().diffms(start));
	}

	system("systemctl start boottimekpi.target");
	ttsystem_app_started();

	{
		TTtime introendt(0);
		bool introend = false;
		while (!exit_reason) {
			if (!introend && keys.get(key, modi)) {
				resetBacklight();
				if (key == TTKEY_RECORD && modi == 1) {	// make record start instantly
					keys.inject(TTKEY_RECORD);
					key = TTKEY_RIGHT;
				}
				menu.keyEventHandler(key, modi);
				if (key == TTKEY_OFF && modi == 1)
					exit_reason = EXIT_REASON__POWEROFF;
				if (key == TTKEY_RIGHT && modi == 1) {
					debuglog(4, "rightkeypress", 0);
					introendt.now();
					introend = true;
				}
			}
			if (!menu.update() && !introend)
				::usleep(10000);
			if (introend && TTtime().diffms(introendt) > 800)
				break;

			do_trigger_wd();
		}
	}

	if (!exit_reason) {
		debuglog(4, "main screen started", 0);
		menu.init(&mainscreen);
		menu.setLeftScreen(&statscreen);
	}

	while (!exit_reason) {
		if (keys.get(key, modi)) {
			resetBacklight();
			if (!menu.keyEventHandler(key, modi))
				switch (key) {

				case TTKEY_RECORD:
					debuglog(5, "RECORD key", 0);
					if (modi == 1) {
						if (isStillMode)
							startstillcapture();
						else
							startrecording();
					}
					break;

				case TTKEY_OFF:
					debuglog(5, "OFF key", 0);
					if (modi == 1)
						stoprecording();
					else if (modi >= 2000)
						exit_reason = EXIT_REASON__POWEROFF;
					break;

				case TTKEY_WEB_RECSTOP:
					if (isRecording)
						stoprecording();
					else
						startrecording();
					break;

				}
		}
		if (isContinuousCapturing)
			startContinuousCapture();
		else if (isTimelapseCapturing)
			startTimelapseCapture();

		if(isRecording) {
			if(stats.get(TTSTATS_FIFO_LOAD)>90) {
				debuglog(1,"FIFO is near overflow (%d %%)!!! stopping!!!",(int)stats.get(TTSTATS_FIFO_LOAD));
				stoprecording();
			}
		}

		if (!(menu.update() || processframes()))
			::usleep(16600);  /* one frame delay for 1080p60 considering as optimal */

		do_trigger_wd();
	}

	debuglog(4, "============ Exiting platform demo", 0);

	/* We are existing. Block SIGTERM to ensure child process forked from this point are not killed */
	sigprocmask(SIG_BLOCK, &sigterm_set, NULL);

	if (exit_reason == EXIT_REASON__POWEROFF) {
		menu.init(&offscreen);
	}


	if (exit_reason == EXIT_REASON__SIGTERM || exit_reason == EXIT_REASON__BATTERY_CRITICAL_LOW || exit_reason == EXIT_REASON__SIGTEMP) {
		/* Stop recording when we are doing it. */
		if(isRecording) {
			stoprecording();
		}
	}

	ttv_mode(TTV_MODE_NONE);
	setLedBrightness(LED_NAME__LED1, 0);
	setLedBrightness(LED_NAME__BACKLIGHT, 0);

	if (exit_reason == EXIT_REASON__BATTERY_CRITICAL_LOW) {
		/* Demonstrates how to sleep while blocking SIGTERM. sleep() does not block SIGTERM */
		do_sleep_block_sigterm(1);
		debuglog(4, "Showing battery critical alert!", 0);
		system("cam-critical note_critical_battery small");
	} else if (exit_reason == EXIT_REASON__SIGTEMP) {
		debuglog(4, "Showing too hot temperature alert!", 0);
		system("/usr/bin/cam-critical note_high_temp small user");
	}

	return exit_reason == EXIT_REASON__POWEROFF ? 0 : exit_reason;
}
