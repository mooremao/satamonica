// wait for frame cancelation test
// Nebojsa Sumrak 2014.

#include <stdio.h>
#include <pthread.h>
#include "ttv.h"

static void *thread_func(void *arg)
{
	while(ttv_wait_for_frame()) printf("wait for frame == true\n");
	printf("wait for frame == false\n");
	return 0;
}


int main()
{
	pthread_t th;

	ttv_mode(TTV_MODE_VIDEO);
	pthread_create(&th, NULL, thread_func, 0);

	while(1)
	{
		printf("Menu: 1 - exit\n: ");
		if(getchar()=='1') break;
	}
	ttv_mode(TTV_MODE_NONE);
	return 0;
}
