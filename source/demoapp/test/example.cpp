// frame record example
// LB Video API document
// Nebojsa Sumrak 2014.

#include <stdio.h>
#include "ttv.h"

int main()
{
	FILE *filet = 0;
	int firstkeyframe = 0;
	int numframes = 0;

	filet = fopen("/mnt/mmc/video.h264","w");
	if(!filet) return -1;

	ttv_mode(TTV_MODE_VIDEO);
	ttv_start(TTV_STREAM_PRI);

	while(numframes<100) {
		ttv_frame_t frame;
		if(!ttv_wait_for_frame()) continue;
		while(ttv_get_frame(&frame)) {
			if(firstkeyframe || frame.iskey) {
				firstkeyframe = 1;
				fwrite(frame.address, 1, frame.size, filet);
numframes++;
			}
			ttv_release_frame(frame.address);
		}
	}

	ttv_stop(TTV_STREAM_ALL);
	fclose(filet);
	ttv_mode(TTV_MODE_NONE);
	return 0;
}
