TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    gfx/fn_boldcn_18.cpp \
    gfx/fn_boldcn_62.cpp \
    gfx/fn_heavycn_22.cpp \
    gfx/fnp_bold_42.cpp \
    gfx/menu.c \
    gfx/tomtom-favicon.c \
    gfx/tomtom-logo.c \
    gfx/tomtom-record.c \
    gfx/tomtom-stop.c \
    gfx/tt_demo_bitmap.c

DISTFILES += \
    menu.xml \
    gfx/tt-create-images.sh \
    gfx/tomtom-logo.jpg \
    gfx/record.png \
    gfx/stop.png \
    gfx/tomtom.ico

HEADERS += \
    avifilereader.h \
    avifilewriter.h \
    debug.h \
    display.h \
    framebuffer.h \
    h264parser.h \
    http.h \
    keys.h \
    leds.h \
    menu.h \
    sd.h \
    stats.h \
    tttime.h \
    utils.h \
    viewfinder.h \
    web.h \
    widgets.h

INCLUDEPATH +=	$$PWD/../ipnc_rdk/ipnc_mcfw/mcfw/interfaces \
		$$PWD/../tt_tools/libttsystem

ARAGO_DEVKIT_LIB_DIR = $$PWD/../ti_tools/linux_devkit/arm-arago-linux-gnueabi/lib


CONFIG( debug, debug|release ) {
SDK_LIB_DIR = $$PWD/../ipnc_rdk/target/longbeach_sdk/builds/PLATFORM_LIB_debug/lib
DESTDIR = qt_debug_bin
OBJ_DIR = qt_debug_bin
} else {
SDK_LIB_DIR = $$PWD/../ipnc_rdk/target/longbeach_sdk/builds/PLATFORM_LIB_release/lib
DESTDIR = qt_release_bin
OBJ_DIR = qt_release_bin
}

DEFINES += PLATFORM_LINUX_ARM
DEFINES += _FILE_OFFSET_BITS=64
DEFINES += _LARGEFILE64_SOURCE
DEFINES += _LARGEFILE_SOURCE
DEFINES += OS_LINUX
DEFINES += ARCH_ARM

LIBS += -L$$SDK_LIB_DIR
LIBS += \
    $$ARAGO_DEVKIT_LIB_DIR/libpthread.so.0 \
    $$ARAGO_DEVKIT_LIB_DIR/libstdc++.so \
    $$SDK_LIB_DIR/libttvdevice.a \
    $$SDK_LIB_DIR/libttsystem.a \
    $$SDK_LIB_DIR/libfdt.so \
    $$SDK_LIB_DIR/ipnc_rdk_mcfw_api.a \
    $$SDK_LIB_DIR/ipnc_rdk_link_api.a \
    $$SDK_LIB_DIR/ipnc_rdk_osa.a \
    $$SDK_LIB_DIR/syslink.a_release \
    $$SDK_LIB_DIR/libGlbceSupport_ti.a \
    $$SDK_LIB_DIR/cmem.a470MV \
    $$SDK_LIB_DIR/libsystemd.so \
    $$SDK_LIB_DIR/libblkid.so

QMAKE_POST_LINK += $$quote(cp $$PWD/$$DESTDIR/$$TARGET $$PWD/bin-debug/lbpDemoApp)
