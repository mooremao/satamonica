// TomTom Video platform API
// video file services h264+MJPG_avi demux
// Nebojsa Sumrak 2014.

#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

class TTaviReader {
 protected:
	FILE * milf;
	unsigned numaviframes;
	long movimarker, dexmarker;

	unsigned int read_int() {
		char buf[4];
		if (fread(buf, 1, 4, milf) != 4)
			 return 0;
		 return ((unsigned)buf[0]) | (((unsigned)buf[1]) << 8) | (((unsigned)buf[2]) << 16) |
		    (((unsigned)buf[3]) << 24);
	} unsigned short read_short(unsigned short n) {
		char buf[2];
		if (fread(buf, 1, 2, milf) != 4)
			return 0;
		return ((unsigned short)buf[0]) | (((unsigned short)buf[1]) << 8);
	}

	bool read_chars(char *s, unsigned count) {
		return (fread(s, 1, count, milf) == count);
	}

 public:
	TTaviReader() {
		milf = 0;
		numaviframes = 0;
		movimarker = dexmarker = 0;
	}

	bool open(int filenum) {
		close();
		char buf[256];
		sprintf(buf, "/mnt/mmc/TTCamVideo%02d.avi", filenum);
		milf = fopen(buf, "r");
		if (!milf)
			return false;

		// read struct:
		//              RIFF <len> 'AVI LIST' <len> 'hdrlavih' <len> <16b> <num_frames> ...<up to LIST len>... JUNK <len>
		//              LIST <len> ^data_marker^ 'movi'
		//              idx1 <len> <'00dc' 0x00000010 <offset> <size>>-repeated num frames
		long dummy, listlen;
		read_chars(buf, 4);
		if (strncmp(buf, "RIFF", 4)) {
			close();
			return false;
		}
		dummy = read_int();
		read_chars(buf, 8);
		if (strncmp(buf, "AVI LIST", 8)) {
			close();
			return false;
		}
		listlen = read_int();
		if (listlen < 20) {
			close();
			return false;
		}
		read_chars(buf, 16);
		numaviframes = read_int();
		fseek(milf, listlen - 20, SEEK_CUR);
		read_chars(buf, 4);
		dummy = read_int();
		if (!strncmp(buf, "JUNK", 4)) {
			fseek(milf, dummy, SEEK_CUR);
			read_chars(buf, 4);
			dummy = read_int();
		}
		movimarker = ftell(milf);
		if (strncmp(buf, "LIST", 4)) {
			close();
			return false;
		}
		read_chars(buf, 4);
		if (strncmp(buf, "movi", 4)) {
			close();
			return false;
		}
		fseek(milf, dummy - 4, SEEK_CUR);
		read_chars(buf, 4);
		if (strncmp(buf, "idx1", 4)) {
			close();
			return false;
		}
		dexmarker = ftell(milf);
		return true;
	}

	bool read_thumb(unsigned jpgnum, void *buf, unsigned &size) {
		if (!isopen())
			return false;
		if (jpgnum >= numaviframes)
			return false;
		fseek(milf, dexmarker + (jpgnum << 4) + 12, SEEK_SET);
		unsigned off = read_int();
		unsigned sz = read_int();
		if (sz > size)
			return false;
		fseek(milf, movimarker + off + 8, SEEK_SET);
		if (fread(buf, 1, sz, milf) != sz)
			return false;
		size = sz;
		return true;
	}

	void close() {
		if (milf) {
			fclose(milf);
			milf = 0;
			numaviframes = 0;
			movimarker = dexmarker = 0;
		}
	}

	~TTaviReader() {
		close();
	}

	bool isopen() {
		return (milf != 0);
	}

	unsigned getNumFrames() {
		return numaviframes;
	}
};
