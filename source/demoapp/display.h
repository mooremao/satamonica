// TomTom Video platform API
// display wrapper
// Nebojsa Sumrak 2014.

#pragma once

#include <debug.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define FB_DEV_NODE "/dev/fb0"

#define LCM_BITS_PER_LINE 144
#define LCM_BYTES_PER_LINE 18	// 144 bits
#define LCM_HEIGHT 168

class TTdisplay {
 protected:
	int fbfd;
	struct fb_fix_screeninfo fix_scr_info;
	struct fb_var_screeninfo var_scr_info;
	unsigned char *fbmem;
	char fbbackbuffer[LCM_BYTES_PER_LINE * LCM_HEIGHT];

	void init() {
		fbfd = open(FB_DEV_NODE, O_RDWR);
		if (fbfd == -1) {
			debuglog(1,"Unable to open %s", FB_DEV_NODE);
			return;
		}

		if (ioctl(fbfd, FBIOGET_FSCREENINFO, &fix_scr_info) || ioctl(fbfd, FBIOGET_VSCREENINFO, &var_scr_info)) {
			debuglog(1,"unable to get screen info",0);
			goto err;
		}

		fbmem = (unsigned char *)mmap(NULL, fix_scr_info.smem_len, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);
		if (fbmem == NULL) {
			debuglog(1,"Unable to mmap %s", FB_DEV_NODE);
			goto err;
		}

		if (ioctl(fbfd, FBIOBLANK, FB_BLANK_UNBLANK)) {
			debuglog(1,"Unable to turn on the LCD",0);
			goto err;
		}
		// clear all frame buffer
		memset(fbmem, 0x00, fix_scr_info.smem_len);
		memset(fbbackbuffer, 0, sizeof(fbbackbuffer));
		return;

 err:
		close(fbfd);
		fbfd = -1;
	}

 public:
	void flush(const unsigned char *buf = 0) {
		if (fbfd < 0)
			return;
		if (!buf)
			buf = (const unsigned char *)fbbackbuffer;
		memcpy(fbmem, buf, LCM_BYTES_PER_LINE * LCM_HEIGHT);
		fsync(fbfd);
	}

	void drawBitmap(int x, int y, int w, int h, char *bmp) {
		if (fbfd < 0)
			return;
		char *fb = fbbackbuffer + y * LCM_BYTES_PER_LINE + x;
		for (; h > 0; h--) {
			memcpy(fb, bmp, w);
			bmp += w;
			fb += LCM_BYTES_PER_LINE;
		}
	}

	void ditherRgn(int x, int y, int w, int h) {
		if (fbfd < 0)
			return;
		int i;
		char d = 0xaa;
		char *fb = fbbackbuffer + y * LCM_BYTES_PER_LINE + x;
		for (; h > 0; h--) {
			d ^= 0xff;
			for (i = 0; i < w; i++)
				*(fb++) &= d;
			fb += LCM_BYTES_PER_LINE - w;
		}
	}

	void clear() {
		memset(fbbackbuffer, 0, sizeof(fbbackbuffer));
	}

	TTdisplay() {
		fbfd = -1;
		init();
	}

	~TTdisplay() {
		if (fbfd >= 0) {
			clear();
			flush();
			close(fbfd);
			fbfd = -1;
		}
	}
};
