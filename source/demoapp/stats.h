// TomTom camera platform
// readout of various platform stats
// (c) 2014. Nebojsa Sumrak

#pragma once

#include <debug.h>
#include "sd.h"

enum TTstatsType {
	TTSTATS_TEMP_AMBIENT = 0,
	TTSTATS_TEMP_CPU,
	TTSTATS_TEMP_BATTERY,
	TTSTATS_BAT_CAPACITY,
	TTSTATS_CPU_BUSY,
	TTSTATS_CPU_IOWAIT,
	TTSTATS_FIFO_LOAD,
	TTSTATS_DCP_CHARGER,

	TTSTATS_MAX
};


class TTstats
{
protected:
	double m_stats[TTSTATS_MAX][36];
	int m_statcurrent[TTSTATS_MAX];

public:
	TTstats()
	{
		memset(m_stats,0,sizeof(m_stats));
		memset(m_statcurrent,0,sizeof(m_statcurrent));
	}

	void sendData(TTstatsType type, double value)
	{
		int cur = ++m_statcurrent[type];
		if(cur >= 36) cur = m_statcurrent[type] = 0;
		m_stats[type][cur] = value;
	}

	double get(TTstatsType type, int index = 0)
	{
		index %= 36;
		index += m_statcurrent[type];
		if(index<0) index+=36;
		else if(index>36) index-=36;
		return m_stats[type][index];
	}

	void update()
	{
		// get fifo stats
		int fifofill = ttv_get_fifo_fill();
		debuglog(5,"FIFO fill: %d %%", fifofill);
		sendData(TTSTATS_FIFO_LOAD, (double)fifofill);

		// get cpu load
		double user, nice, system, idle, iowait, irq, softirq;
		if(readProc("/proc/stat", "%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf", &user, &nice, &system, &idle, &iowait, &irq, &softirq)) {
			static double s_busy = 0.0, s_waiting = 0.0, s_iowait = 0.0;
			double busy, waiting;

			busy = user + nice + system + irq + softirq;
			waiting = idle + iowait;

			if (s_waiting != 0.0) { /* skip first time. We need to sample to calculate the difference between them. */
				double busyperc = ((busy - s_busy) / ((busy + waiting) - (s_busy + s_waiting))) * 100.0;
				double iowaitperc = ((iowait - s_iowait) / ((busy + waiting) - (s_busy + s_waiting))) * 100.0;
				sendData(TTSTATS_CPU_BUSY, busyperc);
				sendData(TTSTATS_CPU_IOWAIT, iowaitperc);
				debuglog(5, "The current CPU utilization is: %Lf. iowait: %Lf", busyperc, iowaitperc);
			}
			s_busy = busy;
			s_waiting = waiting;
			s_iowait = iowait;
		}

		// get temperatures
		int temp = 0;
		readProc("/sys/bus/i2c/devices/3-0072/temp1_input", "%05d", &temp); // soc
		sendData(TTSTATS_TEMP_CPU, (double)temp/1000.0);
		readProc("/sys/class/power_supply/bq27421-0/temp", "%04d", &temp); // battery
		sendData(TTSTATS_TEMP_BATTERY, (double)temp/10.0);

		readProc("/sys/class/power_supply/bq27421-0/capacity", "%04d", &temp); // battery capactity
		sendData(TTSTATS_BAT_CAPACITY, (double)temp);

		int ac_online;
		readProc("/sys/devices/platform/omap/omap_i2c.3/i2c-3/3-0044/ac_online", "%04d", &ac_online);
		sendData(TTSTATS_DCP_CHARGER, (double)ac_online);
	}
};
