// GoBandit platform
// ->TomTom Video platform API
// ->Platform demo application
// http server services
// (c) 2012. Nebojsa Sumrak

#pragma once

#include "debug.h"
#include <signal.h>
#include <unistd.h>
#include <time.h>

#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netdb.h>
#include <time.h>
#include <stdarg.h>
#include <signal.h>

#define MAX_HTTP_THREADS	4

class GBhttp {
 protected:
	int m_sock;
	const char *m_fixedDirs;

	bool init(const char *host = NULL, unsigned short port = 80) {
		// Setup the socket
		m_sock = socket(AF_INET, SOCK_STREAM, 0);
		if (m_sock < 0)
			return false;

#ifdef SO_REUSEADDR
		int opt = 1;
		 setsockopt(m_sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(int));
#endif

		struct sockaddr_in addr;
		 memset(&addr, 0, sizeof(addr));
		 addr.sin_family = AF_INET;
		 addr.sin_addr.s_addr = (host == NULL ? htonl(INADDR_ANY) : inet_addr(host));
		 addr.sin_port = htons(port);
		if (bind(m_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
			::close(m_sock);
			return false;
		}
		listen(m_sock, 128);
		//m_startTime = time(NULL);
		return true;
	}

#define	HTTP_IP_ADDR_LEN	20
#define HTTP_MAX_LEN		10240
#define HTTP_MAX_URL		1024
#define HTTP_MAX_HEADERS	1024
#define HTTP_MAX_AUTH		128
#define HTTP_READ_BUF_LEN	4095
#define	HTTP_TIME_STRING_LEN	40

	struct httpVar {
		struct httpVar *next;
		char *value;
		char name[1];
	};

	struct httpReq {
		int m_clientsock;
		char m_clientAddr[HTTP_IP_ADDR_LEN];
		struct sockaddr_in addr;

		int m_readBufRemain;
		char *m_readBufPtr;

		enum { HTTP_GET = 1, HTTP_POST, HTTP_PUT, HTTP_DELETE } method;
		int contentLength, authLength;
		char m_readBuf[HTTP_READ_BUF_LEN + 1];
		char path[HTTP_MAX_URL],
		    host[HTTP_MAX_URL],
		    referer[HTTP_MAX_URL],
		    ifModified[HTTP_MAX_URL],
		    contentType[HTTP_MAX_URL],
		    userAgent[HTTP_MAX_URL], authUser[HTTP_MAX_AUTH], authPassword[HTTP_MAX_AUTH];

		// response
		unsigned responseLength;
		bool headersSent;
		char headers[HTTP_MAX_HEADERS],
		    response[HTTP_MAX_URL], httpresponse[HTTP_MAX_URL], responseContentType[HTTP_MAX_URL];

		off_t rangefrom, rangeto;
		bool http11, keepalive, chunked;
		struct httpVar *m_variables;
	};

	int getconnection(struct timeval *timeout, httpReq & request) {
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(m_sock, &fds);
		for (int result = 0; !result;) {
			result =::select(m_sock + 1, &fds, 0, 0, timeout);
			if (result < 0)
				return -1;
			if (timeout != 0 && result == 0)
				return 0;
		}

		socklen_t addrLen = sizeof(request.addr);
		memset(&request.addr, 0, sizeof(request.addr));
		request.m_clientsock = accept(m_sock, (struct sockaddr *)&request.addr, &addrLen);
		char *ipaddr = inet_ntoa(request.addr.sin_addr);
		if (ipaddr)
			strncpy(request.m_clientAddr, ipaddr, HTTP_IP_ADDR_LEN);
		else
			*request.m_clientAddr = 0;
		request.m_readBufRemain = 0;
		request.m_readBufPtr = NULL;
		return 1;
	}

	bool readChar(httpReq & request, char *cp) {
		if (request.m_readBufRemain == 0) {
			memset(request.m_readBuf, 0, HTTP_READ_BUF_LEN + 1);
			if ((request.
			     m_readBufRemain =::read(request.m_clientsock, request.m_readBuf, HTTP_READ_BUF_LEN)) < 1)
				return false;
			request.m_readBuf[request.m_readBufRemain] = 0;
			request.m_readBufPtr = request.m_readBuf;
		}
		*cp = *request.m_readBufPtr++;
		request.m_readBufRemain--;
		return true;
	}

	bool readLine(httpReq & request, char *destBuf, int len) {
		char curChar, *dst = destBuf;

		for (int count = 0; count < len;) {
			if (!readChar(request, &curChar))
				return false;
			if (curChar == '\n')
				break;
			if (curChar == '\r')
				continue;
			*dst++ = curChar;
			count++;
		}
		*dst = 0;
		return true;
	}

	bool readBuf(httpReq & request, char *destBuf, int len) {
		char curChar, *dst = destBuf;

		for (int count = 0; count < len; count++) {
			if (!readChar(request, &curChar))
				return false;
			*dst++ = curChar;
		}
		return true;
	}

	static void sanitiseUrl(char *url) {
		char *from, *to, *last;

		for (from = to = last = url; *from;) {
			// Remove multiple slashes
			if (*from == '/' && *(from + 1) == '/') {
				from++;
				continue;
			}
			// Get rid of ./ sequences
			if (*from == '/' && *(from + 1) == '.' && *(from + 2) == '/') {
				from += 2;
				continue;
			}
			// Catch use of /../ sequences and remove them.  Must track the
			// path structure and remove the previous path element.
			if (*from == '/' && *(from + 1) == '.' && *(from + 2) == '.' && *(from + 3) == '/') {
				to = last;
				from += 3;
				continue;
			}
			if (*from == '/')
				last = to;

			*to = *from;
			to++;
			from++;
		}
		*to = 0;
	}

	static char c2h(char c) {
		return c >= '0' && c <= '9' ? c - '0' : c >= 'A' && c <= 'F' ? c - 'A' + 10 : c - 'a' + 10;	/* accept small letters just in case */
	}

	static char *unescape(char *str) {
		char *p, *q;
		p = q = str;

		if (!str) {
			static char blank[] = "";
			return blank;
		}
		while (*p) {
			if (*p == '%') {
				p++;
				if (*p)
					*q = c2h(*p++) * 16;
				if (*p)
					*q = (*q + c2h(*p++));
				q++;
			} else {
				if (*p == '+') {
					*q++ = ' ';
					p++;
				} else {
					*q++ = *p++;
				}
			}
		}

		*q++ = 0;
		return str;
	}

	void storeData(httpReq & request, char *query) {
		if (!query)
			return;

		char var[50], *cp = query, *cp2 = var, *val = 0, *tmpVal;

		memset(var, 0, sizeof(var));
		while (*cp) {
			if (*cp == '=') {
				cp++;
				*cp2 = 0;
				val = cp;
				continue;
			}
			if (*cp == '&') {
				*cp = 0;
				tmpVal = unescape(val);
				addVariable(request, var, tmpVal);
				cp++;
				cp2 = var;
				val = NULL;
				continue;
			}
			if (val)
				cp++;
			else {
				*cp2 = *cp++;
				if (*cp2 == '.') {
					strcpy(cp2, "_dot_");
					cp2 += 5;
				} else
					cp2++;
			}
		}
		*cp = 0;
		tmpVal = unescape(val);
		addVariable(request, var, tmpVal);
	}

	int decode(char *bufcoded, char *bufplain, int outbufsize) {
		static unsigned char pr2six[256];

		// single character decode
#define DEC(c) pr2six[(int)c]
#define _DECODE_MAXVAL 63

		static int first = 1;

		int nbytesdecoded;
		register char *bufin = bufcoded;
		register unsigned char *bufout = (unsigned char *)bufplain;
		register int nprbytes;

		// If this is the first call, initialize the mapping table.
		// This code should work even on non-ASCII machines.
		if (first) {
			int j;

			static char six2pr[64] = {
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
				'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
				'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
			};

			first = 0;
			for (j = 0; j < 256; j++)
				pr2six[j] = _DECODE_MAXVAL + 1;
			for (j = 0; j < 64; j++)
				pr2six[(int)six2pr[j]] = (unsigned char)j;
		}
		// Strip leading whitespace.
		while (isspace(*bufcoded))
			bufcoded++;

		// Figure out how many characters are in the input buffer.
		// If this would decode into more bytes than would fit into
		// the output buffer, adjust the number of input bytes downwards.
		bufin = bufcoded;
		while (pr2six[(int)*(bufin++)] <= _DECODE_MAXVAL) ;
		nprbytes = bufin - bufcoded - 1;
		nbytesdecoded = ((nprbytes + 3) / 4) * 3;
		if (nbytesdecoded > outbufsize)
			nprbytes = (outbufsize * 4) / 3;
		bufin = bufcoded;

		while (nprbytes > 0) {
			*(bufout++) = (unsigned char)(DEC(*bufin) << 2 | DEC(bufin[1]) >> 4);
			*(bufout++) = (unsigned char)(DEC(bufin[1]) << 4 | DEC(bufin[2]) >> 2);
			*(bufout++) = (unsigned char)(DEC(bufin[2]) << 6 | DEC(bufin[3]));
			bufin += 4;
			nprbytes -= 4;
		}
		if (nprbytes & 03) {
			if (pr2six[(int)bufin[-2]] > _DECODE_MAXVAL)
				nbytesdecoded -= 2;
			else
				nbytesdecoded -= 1;
		}
		bufplain[nbytesdecoded] = 0;
		return nbytesdecoded;
	}

	void formatTimeString(char *ptr, time_t clock = 0) {
		if (!clock)
			clock = time(NULL);
		struct tm *timePtr = gmtime((time_t *) & clock);
		strftime(ptr, HTTP_TIME_STRING_LEN, "%a, %d %b %Y %T GMT", timePtr);
	}

	void sendHeaders(httpReq & request, unsigned long contentLength = 0, time_t modTime = 0) {
		if (request.headersSent)
			return;
		char timeBuf[HTTP_TIME_STRING_LEN];

		request.headersSent = true;
		if (request.http11)
			::write(request.m_clientsock, "HTTP/1.1 ", 9);
		else
			::write(request.m_clientsock, "HTTP/1.0 ", 9);
		::write(request.m_clientsock, request.httpresponse, strlen(request.httpresponse));
		::write(request.m_clientsock, request.headers, strlen(request.headers));

		formatTimeString(timeBuf, 0);
		::write(request.m_clientsock, "Date: ", 6);
		::write(request.m_clientsock, timeBuf, strlen(timeBuf));
		::write(request.m_clientsock, "\r\n", 2);

		formatTimeString(timeBuf, modTime);
		::write(request.m_clientsock, "Last-Modified: ", 15);
		::write(request.m_clientsock, timeBuf, strlen(timeBuf));
		::write(request.m_clientsock, "\r\n", 2);

		//if(request.keepalive) ::write(request.m_clientsock, "Connection: Keep-Alive\r\nKeep-Alive: timeout=5, max=150\r\n", 56);
		::write(request.m_clientsock, "Connection: close\r\n", 19);
		::write(request.m_clientsock, "Accept-Ranges: bytes\r\n", 22);
		::write(request.m_clientsock, "Content-Type: ", 14);
		::write(request.m_clientsock, request.responseContentType, strlen(request.responseContentType));
		::write(request.m_clientsock, "\r\n", 2);

		if (contentLength > 0) {
			::write(request.m_clientsock, "Content-Length: ", 16);
			snprintf(timeBuf, sizeof(timeBuf), "%lu\r\n", contentLength);
			::write(request.m_clientsock, timeBuf, strlen(timeBuf));
		} else if (request.http11) {
			::write(request.m_clientsock, "Transfer-Encoding: chunked\r\n", 28);
			request.chunked = true;
		}
		::write(request.m_clientsock, "\r\n", 2);
	}

	void lingerRequest(httpReq & request) {
		fd_set rfds;
		struct timeval tv;
		char buffek[4096];
		TTtime linstart;
		do {
			tv.tv_sec = 0;
			tv.tv_usec = 500000;
			FD_ZERO(&rfds);
			FD_SET(request.m_clientsock, &rfds);
			int res = select(request.m_clientsock + 1, &rfds, 0, 0, &tv);
			if (res < 0 && (errno == EINTR || errno == EAGAIN))
				continue;
			if (res <= 0)
				break;
			res =::read(request.m_clientsock, buffek, 4096);
			if (res < 0 && (errno == EINTR || errno == EAGAIN))
				continue;
			if (res <= 0)
				break;
		} while (TTtime().diffms(linstart) < 500);	// allow 500 ms max linger
	}

	void sendErrorResponse(httpReq & request) {
		sendHeaders(request);
		hprintf(request,
			"<HTML><HEAD><TITLE>%s</TITLE></HEAD>\n<BODY><H1>Error processing request: %s!</H1>\n</BODY></HTML>\n",
			request.httpresponse, request.httpresponse);
	}

	int readRequest(httpReq & request) {
		char buf[HTTP_MAX_LEN];
		char *cp, *cp2;

		// Setup for a standard response
		strcpy(request.headers, "Server: GoBanditEmbeddedCameraServer/1.0\r\n");
		strcpy(request.responseContentType, "text/html");
		strcpy(request.httpresponse, "200 OK\r\n");
		request.rangefrom = request.rangeto = -1;
		request.http11 = false;
		request.keepalive = false;
		request.headersSent = false;
		request.chunked = false;

		// Read the request
		int count = 0;
		int inHeaders = 1;
		while (readLine(request, buf, HTTP_MAX_LEN)) {
			count++;

			// Special case for the first line.  Scan the request method and path etc
			if (count == 1) {
				debuglog(5, "http req: %s", buf);
				cp = cp2 = buf;
				while (isalpha(*cp2))
					cp2++;
				*cp2 = 0;
				if (!strcasecmp(cp, "GET"))
					request.method = httpReq::HTTP_GET;
				else if (!strcasecmp(cp, "POST")) {
					request.method = httpReq::HTTP_POST;
					request.keepalive = true;
				} else if (!strcasecmp(cp, "PUT"))
					request.method = httpReq::HTTP_PUT;
				else if (!strcasecmp(cp, "DELETE"))
					request.method = httpReq::HTTP_DELETE;
				else {
					debuglog(3, "http: invalid method received (%s)", cp);
					strcpy(request.httpresponse, "400 Bad Request\r\n");
					sendErrorResponse(request);
					return false;
				}

				cp = cp2 + 1;
				while (isspace(*cp))
					cp++;
				cp2 = cp;
				while (*cp2 && !isspace(*cp2))
					cp2++;
				*cp2 = 0;

				// Process any URL data
				if ((cp2 = strchr(cp, '?'))) {
					*cp2 = 0;
					storeData(request, cp2 + 1);
				}

				sanitiseUrl(cp);
				strncpy(request.path, cp, HTTP_MAX_URL);
				continue;
			}
			// Process the headers
			if (inHeaders) {
				if (*buf == 0) {
					// End of headers.  Continue if there's data to read
					if (request.contentLength == 0)
						break;
					inHeaders = 0;
					break;
				}
				if (!strncasecmp(buf, "Cookie: ", 7)) {
					for (char *var = buf + 7; var;) {
						var++;
						char *val = strchr(var, '=');
						if (!val)
							break;
						*val = 0;
						val++;
						char *end;
						if ((end = strchr(val, ';')))
							*end = 0;
						addVariable(request, var, val);
						var = end;
					}
					continue;
				}
				if (!strncasecmp(buf, "Authorization: ", 15)) {
					cp = buf + 15;
					if (!strncmp(cp, "Basic ", 6)) {
						char authBuf[HTTP_MAX_AUTH * 3];

						cp = strchr(cp, ' ') + 1;
						decode(cp, authBuf, HTTP_MAX_AUTH * 3);
						request.authLength = strlen(authBuf);
						if ((cp = strchr(authBuf, ':'))) {
							*cp = 0;
							strncpy(request.authPassword, cp + 1, HTTP_MAX_AUTH);
						}
						strncpy(request.authUser, authBuf, HTTP_MAX_AUTH);
					}
					continue;
				}
				if (!strncasecmp(buf, "Host: ", 6)) {
					strncpy(request.host, buf + 6, HTTP_MAX_URL);
					continue;
				}
				if (!strncasecmp(buf, "Referer: ", 9)) {
					strncpy(request.referer, buf + 9, HTTP_MAX_URL);
					continue;
				}
				if (!strncasecmp(buf, "If-Modified-Since: ", 19)) {
					cp = buf + 19;
					strncpy(request.ifModified, cp, HTTP_MAX_URL);
					if ((cp = strchr(request.ifModified, ';')))
						*cp = 0;
					continue;
				}
				if (!strncasecmp(buf, "Content-Type: ", 14)) {
					strncpy(request.contentType, buf + 14, HTTP_MAX_URL);
					continue;
				}
				if (!strncasecmp(buf, "Content-Length: ", 16)) {
					request.contentLength = strtoul(buf + 16, 0, 10);
					continue;
				}
				if (!strncasecmp(buf, "User-Agent: ", 12)) {
					strncpy(request.userAgent, buf + 12, HTTP_MAX_URL);
					continue;
				}
				if (!strncasecmp(buf, "Connection: ", 12)) {
					if (!strcasecmp(buf + 12, "keep-alive"))
						request.keepalive = true;
					continue;
				}
				if (!strncasecmp(buf, "Range: ", 7)) {
					debuglog(5, "http range req: %s", buf);
					cp = buf + 7;
					while (isspace(*cp))
						cp++;
					if (strncasecmp(cp, "bytes", 5))
						continue;
					cp += 5;
					while (isspace(*cp))
						cp++;
					if (*cp != '=')
						continue;
					cp++;
					while (isspace(*cp))
						cp++;
					char *minus = strchr(cp, '-');
					if (!minus)
						continue;
					*(minus++) = 0;
					request.rangefrom = strtoull(cp, 0, 10);	//atoi(cp);
					request.rangeto = strtoull(minus, 0, 10);	//atoi(minus);
					debuglog(5, "http range request %lu-%lu (%s-%s)",
						 (unsigned long)request.rangefrom, (unsigned long)request.rangeto, cp,
						 minus);
					continue;
				}
				debuglog(5, "unk header: %s", buf);
				continue;
			}
		}

		// Process and POST data
		if (request.contentLength > 0
		    && (!*request.contentType
			|| !strcasecmp(request.contentType, "application/x-www-form-urlencoded"))) {
			memset(buf, 0, HTTP_MAX_LEN);
			readBuf(request, buf, request.contentLength);
			storeData(request, buf);
		}
		return false;
	}

	void freeVariables(httpReq & request) {
		if (!request.m_variables)
			return;
		for (httpVar * v = request.m_variables; v;) {
			httpVar *n = v->next;
			free(v);
			v = n;
		}
		request.m_variables = 0;
	}

	void endRequest(httpReq & request) {
		if (request.keepalive)
			lingerRequest(request);
		freeVariables(request);
		::shutdown(request.m_clientsock, 2);
		::close(request.m_clientsock);
		memset(&request, 0, sizeof(request));
		request.m_clientsock = -1;
	}

	const char *findPath(const char *dir) {
		for (const char *fd = m_fixedDirs; fd && *fd;) {
			if (!strcmp(fd, dir))
				return fd + strlen(fd) + 1;
			fd += strlen(fd) + 1;
			fd += strlen(fd) + 1;
		}
		return 0;
	}

	static const char *getMimeType(const char *ext) {
		if (ext) {
			static const struct {
				const char *ext, *mime;
			} extmime[] = {
				{
				"a", "application/octet-stream"}, {
				"aab", "application/x-authorware-bin"}, {
				"aam", "application/x-authorware-map"}, {
				"aas", "application/x-authorware-seg"}, {
				"ai", "application/postscript"}, {
				"aif", "audio/x-aiff"}, {
				"aifc", "audio/x-aiff"}, {
				"aiff", "audio/x-aiff"}, {
				"asc", "text/plain"}, {
				"asf", "video/x-ms-asf"}, {
				"asx", "video/x-ms-asf"}, {
				"au", "audio/basic"}, {
				"avi", "video/x-msvideo"}, {
				"bcpio", "application/x-bcpio"}, {
				"bin", "application/octet-stream"}, {
				"bmp", "image/bmp"}, {
				"cdf", "application/x-netcdf"}, {
				"class", "application/x-java-vm"}, {
				"cpio", "application/x-cpio"}, {
				"cpt", "application/mac-compactpro"}, {
				"crl", "application/x-pkcs7-crl"}, {
				"crt", "application/x-x509-ca-cert"}, {
				"csh", "application/x-csh"}, {
				"css", "text/css"}, {
				"dcr", "application/x-director"}, {
				"dir", "application/x-director"}, {
				"djv", "image/vnd.djvu"}, {
				"djvu", "image/vnd.djvu"}, {
				"dll", "application/octet-stream"}, {
				"dms", "application/octet-stream"}, {
				"doc", "application/msword"}, {
				"dtd", "text/xml"}, {
				"dump", "application/octet-stream"}, {
				"dvi", "application/x-dvi"}, {
				"dxr", "application/x-director"}, {
				"eps", "application/postscript"}, {
				"etx", "text/x-setext"}, {
				"exe", "application/octet-stream"}, {
				"ez", "application/andrew-inset"}, {
				"fgd", "application/x-director"}, {
				"fh", "image/x-freehand"}, {
				"fh4", "image/x-freehand"}, {
				"fh5", "image/x-freehand"}, {
				"fh7", "image/x-freehand"}, {
				"fhc", "image/x-freehand"}, {
				"gif", "image/gif"}, {
				"gtar", "application/x-gtar"}, {
				"hdf", "application/x-hdf"}, {
				"hqx", "application/mac-binhex40"}, {
				"htm", "text/html"}, {
				"html", "text/html"}, {
				"h264", "video/avc"}, {
				"ice", "x-conference/x-cooltalk"}, {
				"ief", "image/ief"}, {
				"iges", "model/iges"}, {
				"igs", "model/iges"}, {
				"iv", "application/x-inventor"}, {
				"jar", "application/x-java-archive"}, {
				"jfif", "image/jpeg"}, {
				"jpe", "image/jpeg"}, {
				"jpeg", "image/jpeg"}, {
				"jpg", "image/jpeg"}, {
				"js", "application/x-javascript"}, {
				"kar", "audio/midi"}, {
				"latex", "application/x-latex"}, {
				"lha", "application/octet-stream"}, {
				"lzh", "application/octet-stream"}, {
				"m3u", "audio/x-mpegurl"}, {
				"man", "application/x-troff-man"}, {
				"mathml", "application/mathml+xml"}, {
				"me", "application/x-troff-me"}, {
				"mesh", "model/mesh"}, {
				"mid", "audio/midi"}, {
				"midi", "audio/midi"}, {
				"mif", "application/vnd.mif"}, {
				"mime", "message/rfc822"}, {
				"mml", "application/mathml+xml"}, {
				"mov", "video/quicktime"}, {
				"movie", "video/x-sgi-movie"}, {
				"mp2", "audio/mpeg"}, {
				"mp3", "audio/mpeg"}, {
				"mp4", "video/mp4"}, {
				"mpe", "video/mpeg"}, {
				"mpeg", "video/mpeg"}, {
				"mpg", "video/mpeg"}, {
				"mpga", "audio/mpeg"}, {
				"ms", "application/x-troff-ms"}, {
				"msh", "model/mesh"}, {
				"mv", "video/x-sgi-movie"}, {
				"mxu", "video/vnd.mpegurl"}, {
				"nc", "application/x-netcdf"}, {
				"o", "application/octet-stream"}, {
				"oda", "application/oda"}, {
				"ogg", "application/x-ogg"}, {
				"pac", "application/x-ns-proxy-autoconfig"}, {
				"pbm", "image/x-portable-bitmap"}, {
				"pdb", "chemical/x-pdb"}, {
				"pdf", "application/pdf"}, {
				"pgm", "image/x-portable-graymap"}, {
				"pgn", "application/x-chess-pgn"}, {
				"png", "image/png"}, {
				"pnm", "image/x-portable-anymap"}, {
				"ppm", "image/x-portable-pixmap"}, {
				"ppt", "application/vnd.ms-powerpoint"}, {
				"ps", "application/postscript"}, {
				"qt", "video/quicktime"}, {
				"ra", "audio/x-realaudio"}, {
				"ram", "audio/x-pn-realaudio"}, {
				"ras", "image/x-cmu-raster"}, {
				"rdf", "application/rdf+xml"}, {
				"rgb", "image/x-rgb"}, {
				"rm", "audio/x-pn-realaudio"}, {
				"roff", "application/x-troff"}, {
				"rpm", "audio/x-pn-realaudio-plugin"}, {
				"rss", "application/rss+xml"}, {
				"rtf", "text/rtf"}, {
				"rtx", "text/richtext"}, {
				"sgm", "text/sgml"}, {
				"sgml", "text/sgml"}, {
				"sh", "application/x-sh"}, {
				"shar", "application/x-shar"}, {
				"silo", "model/mesh"}, {
				"sit", "application/x-stuffit"}, {
				"skd", "application/x-koan"}, {
				"skm", "application/x-koan"}, {
				"skp", "application/x-koan"}, {
				"skt", "application/x-koan"}, {
				"smi", "application/smil"}, {
				"smil", "application/smil"}, {
				"snd", "audio/basic"}, {
				"so", "application/octet-stream"}, {
				"spl", "application/x-futuresplash"}, {
				"src", "application/x-wais-source"}, {
				"stc", "application/vnd.sun.xml.calc.template"}, {
				"std", "application/vnd.sun.xml.draw.template"}, {
				"sti", "application/vnd.sun.xml.impress.template"}, {
				"stw", "application/vnd.sun.xml.writer.template"}, {
				"sv4cpio", "application/x-sv4cpio"}, {
				"sv4crc", "application/x-sv4crc"}, {
				"svg", "image/svg+xml"}, {
				"svgz", "image/svg+xml"}, {
				"swf", "application/x-shockwave-flash"}, {
				"sxc", "application/vnd.sun.xml.calc"}, {
				"sxd", "application/vnd.sun.xml.draw"}, {
				"sxg", "application/vnd.sun.xml.writer.global"}, {
				"sxi", "application/vnd.sun.xml.impress"}, {
				"sxm", "application/vnd.sun.xml.math"}, {
				"sxw", "application/vnd.sun.xml.writer"}, {
				"t", "application/x-troff"}, {
				"tar", "application/x-tar"}, {
				"tcl", "application/x-tcl"}, {
				"tex", "application/x-tex"}, {
				"texi", "application/x-texinfo"}, {
				"texinfo", "application/x-texinfo"}, {
				"tif", "image/tiff"}, {
				"tiff", "image/tiff"}, {
				"tr", "application/x-troff"}, {
				"tsp", "application/dsptype"}, {
				"tsv", "text/tab-separated-values"}, {
				"txt", "text/plain"}, {
				"ustar", "application/x-ustar"}, {
				"vcd", "application/x-cdlink"}, {
				"vrml", "model/vrml"}, {
				"vx", "video/x-rad-screenplay"}, {
				"wav", "audio/x-wav"}, {
				"wax", "audio/x-ms-wax"}, {
				"wbmp", "image/vnd.wap.wbmp"}, {
				"wbxml", "application/vnd.wap.wbxml"}, {
				"wm", "video/x-ms-wm"}, {
				"wma", "audio/x-ms-wma"}, {
				"wmd", "application/x-ms-wmd"}, {
				"wml", "text/vnd.wap.wml"}, {
				"wmlc", "application/vnd.wap.wmlc"}, {
				"wmls", "text/vnd.wap.wmlscript"}, {
				"wmlsc", "application/vnd.wap.wmlscriptc"}, {
				"wmv", "video/x-ms-wmv"}, {
				"wmx", "video/x-ms-wmx"}, {
				"wmz", "application/x-ms-wmz"}, {
				"wrl", "model/vrml"}, {
				"wsrc", "application/x-wais-source"}, {
				"wvx", "video/x-ms-wvx"}, {
				"xbm", "image/x-xbitmap"}, {
				"xht", "application/xhtml+xml"}, {
				"xhtml", "application/xhtml+xml"}, {
				"xls", "application/vnd.ms-excel"}, {
				"xml", "text/xml"}, {
				"xpm", "image/x-xpixmap"}, {
				"xsl", "text/xml"}, {
				"xwd", "image/x-xwindowdump"}, {
				"xyz", "chemical/x-xyz"}, {
				"zip", "application/zip"}, {
				0, 0}
			}, *t;

			for (t = extmime; t->ext; t++)
				if (!strcasecmp(t->ext, ext))
					return t->mime;
		}

		return "text/plain";
	}

	bool sendFile(httpReq & request, const char *fixpath, const char *entryName) {
		char filetname[1024];
		struct stat st;
		strcpy(filetname, fixpath);
		strcat(filetname, entryName);
		if (stat(filetname, &st))
			st.st_size = 0;
		debuglog(5, "http.sendfile file '%s' size %d", filetname, (int)st.st_size);
		int filet =::open(filetname, O_RDONLY);
		if (filet < 0)
			return false;

		// set mime type
		const char *ext = strchr(filetname, '.');
		if (ext)
			ext++;
		strcpy(request.responseContentType, getMimeType(ext));
		sendHeaders(request, (int)st.st_size);

		char buf[4096];
		int len;
		while ((len =::read(filet, buf, 4096)) > 0)
			::write(request.m_clientsock, buf, len);
		::close(filet);
		return true;
	}

	void processRequest(httpReq & request) {
		char dirName[HTTP_MAX_URL];
		const char *entryName;

		request.responseLength = 0;
		if (!request.path[0]) {
			strcpy(dirName, "/");
			entryName = dirName + 1;
		} else {
			char *cp;

			strncpy(dirName, request.path, HTTP_MAX_URL);
			/*Just in case the path wasn't ended with terminator, i.e. maybe was longer than max value*/
			dirName[HTTP_MAX_URL-1] = '\0';
			cp = strrchr(dirName, '/');
			if (!cp) {
				debuglog(4, "invalid http request path received '%s'", request.path);
				strcpy(request.httpresponse, "400 Bad Request\r\n");
				sendErrorResponse(request);
				return;
			}
			if (cp != dirName)
				*cp = 0;
			else
				*(cp + 1) = 0;
			entryName = request.path + (cp - dirName) + 1;
		}
		if (!*entryName)
			entryName = "index.html";

		// find fixed path
		const char *fixpath = findPath(dirName);
		if (fixpath && sendFile(request, fixpath, entryName)) {
		} else if (!process(request, dirName, entryName)) {	// call process virtual function
			// send 404
			debuglog(4, "http 404 not found request (%s (%s) / %s)", dirName, fixpath, entryName);
			strcpy(request.httpresponse, "404 Not Found\r\n");
			sendErrorResponse(request);
		}
		writeWaitingResponse(request, true);
	}

	volatile bool m_threadquit;
	volatile int m_runningth;
	bool m_initialized;
	pthread_t m_httpthread;

	struct httpReqThread {
		httpReq request;
		pthread_t threadid;
		volatile int busy;
		GBhttp *me;
	} m_threads[MAX_HTTP_THREADS];

	static void *http_thread_req_func_disp(void *arg) {
		httpReqThread *rth = reinterpret_cast <httpReqThread *> (arg);
		rth->me->m_runningth++;
		sigset_t set;
		sigemptyset(&set);
		sigaddset(&set, SIGUSR1);
		sigaddset(&set, SIGUSR2);
		while (true) {
			rth->busy = 0;
			int sig;
			if (sigwait(&set, &sig))
				break;
			if (sig == SIGUSR2)
				break;
			if (sig != SIGUSR1)
				continue;
			rth->busy = 1;
			//debuglog(4, "new http job on thread", 0);
			rth->me->processRequest(rth->request);
			rth->me->endRequest(rth->request);
		}
		rth->busy = -1;
		rth->me->m_runningth--;
		return 0;
	}

	void http_thread_func() {
		if (!init()) {
			debuglog(1, "error: Can't create http server", 0);
			return;
		}

		sigset_t set;
		sigemptyset(&set);
		sigaddset(&set, SIGUSR1);
		sigaddset(&set, SIGUSR2);
		sigprocmask(SIG_BLOCK, &set, NULL);
		memset(m_threads, 0, sizeof(m_threads));

		for (int i = 0; i < MAX_HTTP_THREADS; i++) {
			m_threads[i].me = this;
			m_threads[i].busy = -1;
			pthread_create(&m_threads[i].threadid, NULL, http_thread_req_func_disp, &m_threads[i]);
		}

		m_initialized = true;
		m_runningth = 0;
		struct timeval timeout;
		httpReq request;
		debuglog(5, "httpd intialized and ready", 0);

		while (!m_threadquit) {
			int result;

			memset(&request, 0, sizeof(request));
			request.m_clientsock = -1;
			timeout.tv_sec = 0;
			timeout.tv_usec = 500000;
			result = getconnection(&timeout, request);
			if (result == 0)
				continue;
			if (result < 0) {
				debuglog(2, "http get connection error", 0);
				continue;
			}
			debuglog(5, "http connection received", 0);
			if (readRequest(request) < 0) {
				debuglog(3, "http request rejected", 0);
				endRequest(request);
				continue;
			}
			debuglog(5, "http processing request", 0);
			// select thread to do this
			int selth = -1;
			for (int i = 0; i < MAX_HTTP_THREADS; i++) {
				if (!m_threads[i].busy) {
					selth = i;
					break;
				}
			}
			if (selth >= 0) {
				m_threads[selth].request = request;
				if (!pthread_kill(m_threads[selth].threadid, SIGUSR1)) {
					m_threads[selth].busy = 1;
					continue;
				}
			}
			// No threads available, I should do it...
			processRequest(request);
			endRequest(request);
		}
		for (int i = 0; i < MAX_HTTP_THREADS; i++)
			pthread_kill(m_threads[i].threadid, SIGUSR2);
		for (int i = 0; i < 20 && m_runningth; i++)
			::usleep(100000);
		m_httpthread = 0;
	}

	static void *http_thread_func_disp(void *arg) {
		static_cast<GBhttp *>(arg)->http_thread_func();
		return 0;
	}

	void writeWaitingResponse(httpReq & request, bool finishing = false) {
		if (request.responseLength) {
			if (!request.headersSent)
				sendHeaders(request, finishing ? request.responseLength : 0);
			if (request.chunked) {
				char chbuf[16];
				::write(request.m_clientsock, chbuf, sprintf(chbuf, "%x\r\n", request.responseLength));
				::write(request.m_clientsock, request.response, request.responseLength);
				::write(request.m_clientsock, "\r\n", 2);
			} else
				::write(request.m_clientsock, request.response, request.responseLength);
			request.responseLength = 0;
		}
		if (finishing && request.chunked)
			::write(request.m_clientsock, "0\r\n", 3);
	}

 public:
	void setContentType(httpReq & request, const char *ct) {
		strcpy(request.responseContentType, ct);
	}

	void hprintf(httpReq & request, const char *fmt, ...) {
		va_list args;
		char buf[HTTP_MAX_LEN];

		va_start(args, fmt);
		vsnprintf(buf, HTTP_MAX_LEN, fmt, args);
		int len = strlen(buf);
		if (!request.chunked) {
			if (!request.headersSent)
				sendHeaders(request);
			::write(request.m_clientsock, buf, len);
			return;
		}

		if (request.responseLength + len > sizeof(request.response))
			writeWaitingResponse(request);
		if (request.headersSent && !request.chunked) {
			::write(request.m_clientsock, buf, len);
			return;
		}
		strcpy(request.response + request.responseLength, buf);
		request.responseLength += len;
	}

	bool addVariable(httpReq & request, char *var, char *val) {
		int nlen = strlen(var) + 1, vlen = strlen(val) + 1;
		httpVar *nv = reinterpret_cast <httpVar *> (malloc(sizeof(httpVar) + nlen + vlen));
		if (!nv)
			return false;
		nv->next = request.m_variables;
		nv->value = nv->name + nlen;
		strcpy(nv->name, var);
		strcpy(nv->value, val);
		request.m_variables = nv;
		return true;
	}

	const char *getVariable(httpReq & request, const char *name) {
		for (httpVar * v = request.m_variables; v; v = v->next)
			if (!strcasecmp(v->name, name))
				return v->value;
		return 0;
	}

	void addHeader(httpReq & request, const char *msg) {
		strcat(request.headers, msg);
		if (msg[strlen(msg) - 1] != '\n')
			strcat(request.headers, "\r\n");
	}

	void addNoCacheHeaders(httpReq & request) {
		addHeader(request, "Cache-Control: no-store, no-cache, private, must-revalidate, max-age=0");
		addHeader(request, "Pragma: no-cache");
		addHeader(request, "Expires: Sat, 26 Jul 1997 05:00:00 GMT");
	}

	void setCookie(httpReq & request, char *name, char *value) {
		char buf[HTTP_MAX_URL];

		snprintf(buf, HTTP_MAX_URL, "Set-Cookie: %s=%s; path=/;", name, value);
		addHeader(request, buf);
	}

	GBhttp(const char *fixedDirs = 0) {
		m_initialized = false;
		m_threadquit = false;
		m_httpthread = 0;
		m_sock = -1;
		m_fixedDirs = fixedDirs;

		signal(SIGPIPE, SIG_IGN);

		if (pthread_create(&m_httpthread, NULL, http_thread_func_disp, this))
			debuglog(1, "error: Can't create http server thread", 0);
	}

	~GBhttp() {
		shutdown();
	}

	void shutdown() {
		if (m_httpthread) {
			m_threadquit = true;
			pthread_join(m_httpthread, 0);
		}
		if (m_sock >= 0) {
			::close(m_sock);
			m_sock = -1;
		}
	}

	virtual bool process(httpReq & request, char *dirName, const char *entryName) {
		return false;
	}

};
