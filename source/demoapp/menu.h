// TomTom camera platform
// TTmenu class
// (c) 2014. Milos Stankovic, Nebojsa Sumrak

#pragma once

#include <debug.h>
#include "framebuffer.h"
#include "utils.h"
#include <display.h>
#include <keys.h>
#include <tttime.h>

#define MENU_SCROLL_WINDOW	15
#define VISITED_STACK_SIZE	32
#define SCREEN_ANIM_TIME	250
#define BOUNCE_PIXELS		8

class TTmenu {
 public:
	enum MenuFlag {
		MENU_LEFT_ALIGN = 0,
		MENU_RIGHT_ALIGN = 0x8000,
		MENU_CENTER_ALIGN = 0x4000,
		MENU_ALIGN_MASK = 0xc000,
		MENU_V_TOP_ALIGN = 0,
		MENU_V_CENTER_ALIGN = 0x2000,
		MENU_V_BOTTOM_ALIGN = 0x1000,
		MENU_V_ALIGN_MASK = 0x3000,
		MENU_BREAK_BEFORE = 0x0800,
		MENU_FOCUSED = 0x0400,
		MENU_REDRAW = 0x0200,
		MENU_DISABLED = 0x0100,
		MENU_SCREEN_LAYOUT = 1,
	};

	enum CallbackEvent {
		EVENT_ENTER,	// means EVENT_COULD_FOCUS
		EVENT_EXIT,	// means EVENT_COULD_LOOSE_FOCUS
		EVENT_ENTER_SUBMENU,
		EVENT_EXIT_SUBMENU,
		EVENT_SECOND_TICK,
		EVENT_TICK,
		EVENT_SET,
		EVENT_AUTOEXIT,
		EVENT_DRAW,
		EVENT_MEASURE_ITEM,
		EVENT_KEY
	};

	struct MenuItem;
	struct MenuScreen;
	typedef bool(*MenuItemCallback) (CallbackEvent e, MenuItem * it, Variant & p1);

	struct MenuItem {
		void *content;
		MenuItemCallback callback;
		short x, y, width, height;
		void *param;	// eg. "videomode"
		void *wgdata;
		unsigned flags;
		MenuScreen *submenu;

		bool isEmpty() {
			return (!callback && !content);
		} void invalidate() {
			flags |= MENU_REDRAW;
		} bool shouldRedraw() {
			return (flags & MENU_REDRAW);
		}
	};

	class TTmenuAnimate
	{
	protected:
		struct AnimElement {
			struct AnimElement *next;
			short what;
			unsigned short duration;
			short startpos, destpos;
			MenuItem *destItem;
		};

		AnimElement *m_first;
		TTtime m_startAnim;
		MenuScreen *m_curscreen;

		void set(short what, short pos, MenuItem *i)
		{
			// set what on screen
			if(!m_curscreen)
				debuglog(1,"anim screen not set!",0);
			else
				m_curscreen->doAnim(what, pos, i);
		}

		void finish()
		{
			debuglog(5,"anim: finish",0);
			if(!m_first) return;
			m_startAnim += m_first->duration;
			set(m_first->what, m_first->destpos, m_first->destItem);
			// remove el
			AnimElement *e = m_first->next;
			debuglog(5,"finish: first=%x, first->next=%x",m_first,e);
			delete m_first;
			m_first = e;
		}

	public:
		TTmenuAnimate()
		{
			m_curscreen = 0;
			m_first = 0;
		}

		bool addAnim(short what, unsigned short duration, short startpos, short destpos, MenuItem *destItem=0)
		{
			debuglog(5,"anim: add",0);
			AnimElement *e = new AnimElement;
			if(!e) return false;
			debuglog(5,"anim: add el=%x first=%x",e, m_first);
			e->what = what;
			e->duration = duration;
			e->startpos = startpos;
			e->destpos = destpos;
			e->destItem = destItem;
			e->next = 0;
			if(!m_first) {
				m_startAnim.now(); // else it is in the queue after last entry
				m_first = e;
			} else {
				AnimElement *l;
				for(l=m_first; l->next; l=l->next) ;
				l->next = e;
			}
			TTtime now;
			updateAnim(now);
			return true;
		}

		void setScreen(MenuScreen *scr)
		{
			debuglog(5,"anim: set screen (%x)",scr);
			while(m_first) // finish all
				finish();
			m_curscreen = scr;
		}

		void updateAnim(TTtime &now)
		{
			if(m_first) {
				unsigned long diffms = now.diffms(m_startAnim);
				if(diffms >= m_first->duration) {
					finish();
					updateAnim(now);
				} else
					set(m_first->what, (int)m_first->startpos+((int)m_first->destpos-(int)m_first->startpos)*(int)diffms/(int)m_first->duration, m_first->destItem);
			}
		}
	} m_screenAnim;

	struct MenuScreen {
		MenuItem header, footer, *items;
		unsigned flags;
		struct {
			MenuItem *m_focusedItem;
			int m_clientH, m_screenH;
			int m_scrollPos, m_destScrollPos;
			TTmenuAnimate *m_anim;
		} d;

		enum {
			SCR_ANIM_SCROLL=0,
			SCR_ANIM_FOCUS,
		};

		void init(TTmenuAnimate *anim) {
			memset(&d, 0, sizeof(d));
			for (MenuItem * i = items; !i->isEmpty(); i++)
				i->flags &= ~MENU_FOCUSED;
			d.m_anim = anim;
			invalidate();
		}

		void doLayout() {
			if (flags & MENU_SCREEN_LAYOUT)
				return;
			Variant headerMeasure(header.width, header.height), footerMeasure(footer.width, footer.height);

			if (header.width < 0)
				header.width += LCM_WIDTH + 1;
			if (header.height < 0)
				header.height += LCM_HEIGHT + 1;
			if (footer.width < 0)
				footer.width += LCM_WIDTH + 1;
			if (footer.height < 0)
				footer.height += LCM_HEIGHT + 1;
			if (header.callback && header.callback(EVENT_MEASURE_ITEM, &header, headerMeasure)) {
				if (headerMeasure.s1 < 0)
					headerMeasure.s1 += LCM_WIDTH + 1;
				if (headerMeasure.s2 < 0)
					headerMeasure.s2 += LCM_HEIGHT + 1;
			}
			header.width = headerMeasure.s1;
			if ((header.height = headerMeasure.s2))
				headerMeasure.s2 += 2;	// add separator line
			header.y = 0;
			header.x = (header.flags & MENU_CENTER_ALIGN) ? (LCM_WIDTH - header.width) / 2 :
			    (header.flags & MENU_RIGHT_ALIGN) ? (LCM_WIDTH - header.width) : 0;
			if (footer.callback && footer.callback(EVENT_MEASURE_ITEM, &footer, footerMeasure)) {
				if (footerMeasure.s1 < 0)
					footerMeasure.s1 += LCM_WIDTH + 1;
				if (footerMeasure.s2 < 0)
					footerMeasure.s2 += LCM_HEIGHT + 1;
			}
			footer.width = footerMeasure.s1;
			footer.y = LCM_HEIGHT - footerMeasure.s2;
			footer.x = (footer.flags & MENU_CENTER_ALIGN) ? (LCM_WIDTH - footer.width) / 2 :
			    (footer.flags & MENU_RIGHT_ALIGN) ? (LCM_WIDTH - footer.width) : 0;
			if ((footer.height = footerMeasure.s2))
				footerMeasure.s2 += 2;	// add separator line

			d.m_clientH = LCM_HEIGHT - (headerMeasure.s2 + footerMeasure.s2);

			if (items) {
				int leftw = LCM_WIDTH;
				int curry = 0;
				int maxlh = 0;
				int centerw = 0;
				MenuItem *firstonline = items;
				Variant measure;
				for (MenuItem * i = firstonline;; i++) {
					if (i->flags & MENU_DISABLED)
						continue;
					measure.s1 = i->width;
					measure.s2 = i->height;
					if (i->width < 0)
						measure.s1 += leftw + 1;
					if (i->height < 0)
						measure.s2 += d.m_clientH + 1;
					if (i->callback)
						i->callback(EVENT_MEASURE_ITEM, i, measure);
					if (measure.s1 < 0)
						measure.s1 += leftw + 1;
					if (measure.s2 < 0)
						measure.s2 += d.m_clientH + 1;
					if (measure.s1 > LCM_WIDTH)
						measure.s1 = LCM_WIDTH;

					if (measure.s1 > leftw || i->flags & MENU_BREAK_BEFORE || i->isEmpty()) {
						if (i->isEmpty() && firstonline->y + maxlh < d.m_clientH)
							maxlh = d.m_clientH - firstonline->y;	// vcenter on rest of the screen
						// do the left and vertical line alignment
						int lx = 0;
						for (MenuItem * j = firstonline; j != i; j++) {
							if ((j->flags & MENU_ALIGN_MASK) == MENU_LEFT_ALIGN) {
								j->x = lx;
								lx += j->width;
							}
							// vertical alignment
							if (j->flags & MENU_V_CENTER_ALIGN)
								j->y += (maxlh - j->height) / 2;
							else if (j->flags & MENU_V_BOTTOM_ALIGN)
								j->y += maxlh - j->height;
						}
						// do the right line alignment
						int rx = LCM_WIDTH;
						for (MenuItem * j = i; j != firstonline; j--) {
							MenuItem *k = j - 1;
							if (k->flags & MENU_RIGHT_ALIGN) {
								rx -= k->width;
								k->x = rx;
							}
						}
						// do the center line alignment
						lx += (rx - lx - centerw) / 2;
						for (MenuItem * j = firstonline; j != i; j++) {
							if (j->flags & MENU_CENTER_ALIGN) {
								j->x = lx;
								lx += j->width;
							}
						}
						// switch to new line
						curry += maxlh;
						firstonline = i;
						leftw = LCM_WIDTH;
						maxlh = centerw = 0;
						if (i->isEmpty()) {
							d.m_screenH = curry;
							break;
						}
					}
					// place item
					if (measure.s2 > maxlh)
						maxlh = measure.s2;
					leftw -= measure.s1;
					i->width = measure.s1;
					i->height = measure.s2;
					i->y = curry;
					if (i->flags & MENU_CENTER_ALIGN)
						centerw += i->width;
				}
			}
			flags |= MENU_SCREEN_LAYOUT;
			if (!d.m_focusedItem)
				focusNextItemInt();
		}

		bool doDraw(TTframebuffer & fb, bool force = false) {
			Variant fbv(&fb);
			bool didredraw = false;

			if (flags & MENU_REDRAW || !(flags & MENU_SCREEN_LAYOUT))
				force = didredraw = true;
			doLayout();
			flags &= (~MENU_REDRAW);

			if (force)
				fb.clear();

			debuglog(5, "header x,y,w,h: %d,%d,%d,%d", header.x, header.y, header.width, header.height);
			if (header.callback && (force || header.shouldRedraw())) {
				didredraw = true;
				header.flags &= (~MENU_REDRAW);
				fb.setOrigin(header.x, header.y);
				fb.setClippingRegion(header.x, header.y, header.x + header.width,
						     header.y + header.height);
				if (!force)
					fb.clearRect(0, 0, header.width, header.height);
				header.callback(EVENT_DRAW, &header, fbv);
			}
			debuglog(5, "footer x,y,w,h: %d,%d,%d,%d", footer.x, footer.y, footer.width, footer.height);
			if (footer.callback && (force || footer.shouldRedraw())) {
				didredraw = true;
				footer.flags &= (~MENU_REDRAW);
				fb.setOrigin(footer.x, footer.y);
				fb.setClippingRegion(footer.x, footer.y, footer.x + footer.width,
						     footer.y + footer.height);
				if (!force)
					fb.clearRect(0, 0, footer.width, footer.height);
				footer.callback(EVENT_DRAW, &footer, fbv);
			}

			if (items) {
				int bottom, top = header.y + header.height;
				if (top)
					top += 2;	// add separator line
				bottom = top + d.m_clientH;
				debuglog(5, "items client - top: %d, bottom: %d, h: %d", top, bottom, d.m_clientH);
				fb.resetClippingRegion();
				fb.resetOrigin();
				int bouncebottom =
				    (d.m_screenH > d.m_clientH ? d.m_screenH : d.m_clientH) + top - d.m_scrollPos;
				if (d.m_scrollPos < 0)
					fb.fillRect(0, top, LCM_WIDTH, -d.m_scrollPos);	// top bounce white area
				if (bouncebottom < bottom)
					fb.fillRect(0, bouncebottom, LCM_WIDTH, bottom - bouncebottom);	// bottom bounce white area
				for (MenuItem * i = items; !i->isEmpty(); i++) {
					if (i->flags & MENU_DISABLED)
						continue;
					if (i->callback) {
						int h = i->height, y = i->y + top - d.m_scrollPos;
						int clipy = y;
						if (y < top) {	// upper part is clipped
							if (y + h < top)
								continue;	// fully clipped item
							clipy = top;
							h -= top - y;
						}
						if (y > bottom)
							continue;
						if (y + h > bottom)	// lower part is clipped
							h = bottom - y;

						debuglog(5,
							 "item (%S), x: %d, y: %d, oy: %d, clipy: %d, w: %d, h: %d, mh: %d",
							 i->content, i->x, i->y, y, clipy, i->width, i->height, h);
						if (force || i->shouldRedraw()) {
							didredraw = true;
							i->flags &= (~MENU_REDRAW);
							fb.setOrigin(i->x, y);
							fb.setClippingRegion(i->x, clipy, i->x + i->width, clipy + h);
							if (!force)
								fb.clearRect(0, 0, i->width, i->height);
							i->callback(EVENT_DRAW, i, fbv);
						}
					}
				}
			}
			return didredraw;
		}

		static inline int absi(int a) {
			return (a<0 ? -a : a);
		}

		void scrollTo(int scrollPos) {
			// should implement up-down scrolling
			debuglog(5,"d.m_anim->add(...) m_anim=%x",d.m_anim);
			d.m_anim->addAnim(SCR_ANIM_SCROLL, absi(scrollPos-d.m_destScrollPos)*SCREEN_ANIM_TIME/LCM_HEIGHT, d.m_destScrollPos, scrollPos);
			//invalidate();
			d.m_destScrollPos = scrollPos;
			debuglog(5, "scroll to destination %d", scrollPos);
		}

		void bounceUp() {
			debuglog(5,"d.m_anim->add(...) m_anim=%x",d.m_anim);
			d.m_anim->addAnim(SCR_ANIM_SCROLL, SCREEN_ANIM_TIME/4, 0, -BOUNCE_PIXELS);
			d.m_anim->addAnim(SCR_ANIM_SCROLL, SCREEN_ANIM_TIME/4, -BOUNCE_PIXELS, 0);
			debuglog(5, "bounce up!", 0);
		}

		void bounceDown() {
			debuglog(5,"d.m_anim->add(...) m_anim=%x",d.m_anim);
			int lp = d.m_destScrollPos;
			d.m_anim->addAnim(SCR_ANIM_SCROLL, SCREEN_ANIM_TIME/4, lp, lp+BOUNCE_PIXELS);
			d.m_anim->addAnim(SCR_ANIM_SCROLL, SCREEN_ANIM_TIME/4, lp+BOUNCE_PIXELS, lp);
			debuglog(5, "bounce down!", 0);
		}

		int getDestScrollPos() {
			// should return final scroll pos if there is current scroller active
			return d.m_destScrollPos;
		}

		bool ensureVisible(MenuItem * i) {
			assert(i);
			debuglog(5,"ensureVisible: item '%S' i->x=%d i->y=%d", (i->content?i->content:L"NULL"), i->x, i->y);
			int bottom, top = header.y + header.height;
			if (top)
				top += 2;	// add separator line
			bottom = top + d.m_clientH;
			int scrollPos = getDestScrollPos();

			int y = i->y + top - scrollPos;
			int h = i->height;
			if (y < top + MENU_SCROLL_WINDOW) {
				if (h >= d.m_clientH - MENU_SCROLL_WINDOW)
					scrollTo(i->y);
				else
					scrollTo(i->y < MENU_SCROLL_WINDOW ? 0 : i->y - MENU_SCROLL_WINDOW);
				return true;
			}
			if (y + h > bottom - MENU_SCROLL_WINDOW) {
				if (h >= d.m_clientH - MENU_SCROLL_WINDOW) {
					int nsp = scrollPos - (bottom - (y + h));
					if (nsp + d.m_clientH > d.m_screenH)
						nsp = d.m_screenH - d.m_clientH;
					scrollTo(nsp);
				} else {
					int nsp = scrollPos - ((bottom - MENU_SCROLL_WINDOW) - (y + h));
					if (nsp + d.m_clientH > d.m_screenH)
						nsp = d.m_screenH - d.m_clientH;
					scrollTo(nsp);
				}
				return true;
			}
			return false;
		}

		MenuItem *getFocusedItem() {
			return d.m_focusedItem;
		}

		bool setFocusedItem(MenuItem * i) {
			if (!i) {
				return false;
			}
			debuglog(5,"set focused item i->content=%S",i->content?i->content:L"NULL");
			if (i->flags & MENU_DISABLED)
				return false;
			Variant cf(d.m_focusedItem);
			if (i->callback && !i->callback(EVENT_ENTER, i, cf)) {
				Variant tf(i);
				if (!d.m_focusedItem
				    || (d.m_focusedItem->callback
					&& !d.m_focusedItem->callback(EVENT_EXIT, d.m_focusedItem, tf))) {
					if (d.m_focusedItem)
						d.m_focusedItem->flags &= ~MENU_FOCUSED;
					ensureVisible(i);
					debuglog(5,"d.m_anim->add(...)",0);
					if(i)
						d.m_anim->addAnim(SCR_ANIM_FOCUS, 0, 0, 1, i);
					invalidate();
					return true;
				}
			}
			return false;
		}

		void doAnim(short what, short pos, MenuItem *i)
		{
			switch(what) {

			case SCR_ANIM_SCROLL:
				d.m_scrollPos = pos;
				invalidate();
				break;
			case SCR_ANIM_FOCUS:
				i->flags |= MENU_FOCUSED;
				d.m_focusedItem = i;
				i->invalidate();
				break;

			};
		}

 protected:
		bool focusNextItemInt() {
			for (MenuItem * i = d.m_focusedItem ? d.m_focusedItem + 1 : items; !i->isEmpty(); i++)
				if (setFocusedItem(i))
					return true;

			return false;
		}

 public:
		bool focusNextItem() {
			if (focusNextItemInt())
				return true;
			// scroll one screen down or bounce down
			int scrollPos = getDestScrollPos();
			if (scrollPos + d.m_clientH >= d.m_screenH)
				bounceDown();
			else
				scrollTo(scrollPos + 2 * d.m_clientH <
					 d.m_screenH ? scrollPos + d.m_clientH : d.m_screenH - d.m_clientH);
			return false;
		}

		bool focusPrevItem() {
			if (!d.m_focusedItem)
				return false;
			for (MenuItem * i = d.m_focusedItem - 1; i >= items; i--)
				if (setFocusedItem(i))
					return true;

			// scroll one screen up or bounce up
			int scrollPos = getDestScrollPos();
			if (!scrollPos)
				bounceUp();
			else
				scrollTo(scrollPos > d.m_clientH ? scrollPos - d.m_clientH : 0);
			return false;
		}

		void invalidate() {
			flags |= MENU_REDRAW;
		}

		void recalculate() {
			flags &= ~MENU_SCREEN_LAYOUT;
			invalidate();
		}
	};

	enum Direction {
		DIRECTION_UP,
		DIRECTION_DOWN,
		DIRECTION_LEFT,
		DIRECTION_RIGHT,
	};

 protected:
	// Menu Screen Animation stuff
	Direction m_curAnimDirection;
	TTtime m_curAnimStartTime;
	TTtime m_tickerTime;
	struct {
		MenuScreen *screen;
		Direction direction;
	} m_visitedScreenStack[VISITED_STACK_SIZE];
	int m_inStack;
	MenuScreen *m_prevScreen;
	TTframebuffer m_csfb, m_psfb;

	// Menu stuff
	MenuScreen *m_currentScreen;
	TTdisplay *m_display;
	MenuItemCallback m_ticker;
	MenuScreen *m_leftScreen;

	Direction getOppositeDirection(Direction dir) {
		switch (dir) {
		case DIRECTION_UP:
			return DIRECTION_DOWN;
		case DIRECTION_DOWN:
			return DIRECTION_UP;
		case DIRECTION_LEFT:
			return DIRECTION_RIGHT;
		case DIRECTION_RIGHT:
			return DIRECTION_LEFT;
		}
		return dir;
	}

	void doAnimate(MenuScreen * to, Direction dir) {
		assert(to);
		m_curAnimDirection = dir;
		m_curAnimStartTime.now();
		to->d.m_anim = &m_screenAnim;
		m_screenAnim.setScreen(to);
		m_prevScreen = m_currentScreen;
		m_psfb.copy(m_csfb);
		m_currentScreen = to;
		to->invalidate();
	}

 public:
	TTmenu(TTdisplay & display, MenuScreen * screens = 0) {
		m_ticker = 0;
		m_display = &display;
		memset(m_visitedScreenStack, 0, sizeof(m_visitedScreenStack));
		init(screens);
	}

	void init(MenuScreen * screens) {
		m_prevScreen = 0;
		m_leftScreen = 0;
		m_inStack = 0;
		if ((m_currentScreen = screens)) {
			m_screenAnim.setScreen(screens);
			m_currentScreen->init(&m_screenAnim);
		}

		update();
	}

	void setMenuTicker(MenuItemCallback ticker) {
		m_ticker = ticker;
		m_tickerTime.now();
	}

	void setLeftScreen(MenuScreen *leftscr=0)
	{
		m_leftScreen = leftscr;
	}

	bool keyEventHandler(char key, long modifier) {
		if (!m_currentScreen || modifier != TTKEV_PRESSED)
			return false;

		switch (key) {
		case TTKEY_UP:
			if (m_currentScreen->focusPrevItem())
				return true;
			if (m_currentScreen->header.submenu) {
				animateTo(m_currentScreen->header.submenu, DIRECTION_DOWN);
				return true;
			}
			if (m_inStack && m_visitedScreenStack[m_inStack - 1].direction == DIRECTION_DOWN) {
				back();
				return true;
			}
			break;
		case TTKEY_DOWN:
			if (m_currentScreen->focusNextItem())
				return true;
			if (m_currentScreen->footer.submenu) {
				animateTo(m_currentScreen->footer.submenu, DIRECTION_UP);
				return true;
			}
			if (m_inStack && m_visitedScreenStack[m_inStack - 1].direction == DIRECTION_UP) {
				back();
				return true;
			}
			break;
		case TTKEY_LEFT:
			if(canBack())
				back();
			else if(m_leftScreen)
				animateTo(m_leftScreen, DIRECTION_RIGHT);
			else
				return false;
			return true;
		case TTKEY_RIGHT:
			if (m_inStack && m_visitedScreenStack[m_inStack - 1].direction == DIRECTION_LEFT) {
				back();
				return true;
			}
			if (m_currentScreen->d.m_focusedItem && m_currentScreen->d.m_focusedItem->submenu) {
				Variant c(m_currentScreen->d.m_focusedItem->submenu);
				if (m_currentScreen->d.m_focusedItem->callback
				    && m_currentScreen->d.m_focusedItem->callback(EVENT_ENTER_SUBMENU,
										  m_currentScreen->d.m_focusedItem, c))
					return false;
				animateTo(m_currentScreen->d.m_focusedItem->submenu, DIRECTION_LEFT);
			}
			return true;
		}
		return false;
	}

	bool canBack() {
		return (m_inStack != 0);
	}

	void back() {
		if (!m_inStack)
			return;
		Variant c(m_visitedScreenStack[m_inStack - 1].screen);
		if (m_currentScreen->d.m_focusedItem && m_currentScreen->d.m_focusedItem->callback
		    && m_currentScreen->d.m_focusedItem->callback(EVENT_EXIT_SUBMENU, m_currentScreen->d.m_focusedItem,
								  c))
			return;
		if (!m_inStack)
			return;	// another check if widget already performed back()
		m_inStack--;
		doAnimate(m_visitedScreenStack[m_inStack].screen, m_visitedScreenStack[m_inStack].direction);
	}

	void animateTo(MenuScreen * to, Direction dir) {
		assert(m_inStack < VISITED_STACK_SIZE);
		m_visitedScreenStack[m_inStack].screen = m_currentScreen;
		m_visitedScreenStack[m_inStack].direction = getOppositeDirection(dir);
		m_inStack++;
		doAnimate(to, dir);
	}

	bool update() {
		if (!m_currentScreen)
			return false;

		TTtime now;
		m_screenAnim.updateAnim(now);
		{
			Variant cf(now.diffms(m_tickerTime));
			if (cf > 1000) {
				MenuItem *i = m_currentScreen->getFocusedItem();
				m_tickerTime += 1000;
				if (i && i->callback)
					i->callback(EVENT_SECOND_TICK, i, cf);
				if (m_ticker)
					m_ticker(EVENT_SECOND_TICK, i, cf);
			}
		}
		bool didredraw = m_currentScreen->doDraw(m_csfb);
		if (m_prevScreen) {
			long dt = now.diffms(m_curAnimStartTime);
			if (dt > SCREEN_ANIM_TIME) {
				m_prevScreen = 0;	// end animation
				m_currentScreen->invalidate();
				didredraw = true;
			} else {
				int animpos, to;
				TTframebuffer fb;
				m_prevScreen->doDraw(m_psfb);

				switch (m_curAnimDirection) {	// according to time with max LCM_HEIGHT for UP/DOWN and LCM_BYTES_PER_LINE for LEFT/RIGHT

				case DIRECTION_LEFT:
					animpos = dt * LCM_BYTES_PER_LINE / SCREEN_ANIM_TIME;
					to = LCM_BYTES_PER_LINE - animpos;
					fb.copyHorizontal(m_psfb, animpos, to);
					fb.copyHorizontal(m_csfb, 0, animpos, to);
					break;
				case DIRECTION_RIGHT:
					animpos = (SCREEN_ANIM_TIME - dt) * LCM_BYTES_PER_LINE / SCREEN_ANIM_TIME;
					to = LCM_BYTES_PER_LINE - animpos;
					fb.copyHorizontal(m_csfb, animpos, to);
					fb.copyHorizontal(m_psfb, 0, animpos, to);
					break;
				case DIRECTION_UP:
					animpos = dt * LCM_HEIGHT / SCREEN_ANIM_TIME;
					to = LCM_HEIGHT - animpos;
					fb.copyVertical(m_psfb, animpos, to);
					fb.copyVertical(m_csfb, 0, animpos, to);
					break;
				case DIRECTION_DOWN:
					animpos = (SCREEN_ANIM_TIME - dt) * LCM_HEIGHT / SCREEN_ANIM_TIME;
					to = LCM_HEIGHT - animpos;
					fb.copyVertical(m_csfb, animpos, to);
					fb.copyVertical(m_psfb, 0, animpos, to);
					break;
				}
				m_display->flush(fb.getFramebufferData());
				return true;
			}
		}
		if (didredraw) {
			m_display->flush(m_csfb.getFramebufferData());
			return true;
		}
		return false;
	}
};
