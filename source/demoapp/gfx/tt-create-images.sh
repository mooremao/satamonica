(echo "static const unsigned char tomtom_logo_jpgdata[] = {"; od -txC -v tomtom-logo.jpg | sed -e "s/^[0-9]*//" -e s"/ \([0-9a-f][0-9a-f]\)/0x\1,/g" -e"\$d" | sed -e"\$s/,$/};/") >tomtom-logo.c
(echo "static const unsigned char tomtom_favicon[] = {"; od -txC -v tomtom.ico | sed -e "s/^[0-9]*//" -e s"/ \([0-9a-f][0-9a-f]\)/0x\1,/g" -e"\$d" | sed -e"\$s/,$/};/") >tomtom-favicon.c
(echo "static const unsigned char tomtom_record[] = {"; od -txC -v record.png | sed -e "s/^[0-9]*//" -e s"/ \([0-9a-f][0-9a-f]\)/0x\1,/g" -e"\$d" | sed -e"\$s/,$/};/") >tomtom-record.c
(echo "static const unsigned char tomtom_stop[] = {"; od -txC -v stop.png | sed -e "s/^[0-9]*//" -e s"/ \([0-9a-f][0-9a-f]\)/0x\1,/g" -e"\$d" | sed -e"\$s/,$/};/") >tomtom-stop.c
