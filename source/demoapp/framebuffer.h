// TomTom platform
// TTframebuffer class
// (c) 2014. Milos Stankovic

#pragma once

#include <string.h>
#include <wchar.h>
#include <stdint.h>
#include <stdio.h>

#include <debug.h>
#include "display.h"

#define LCM_WIDTH LCM_BITS_PER_LINE
#define LCM_HEIGHT         168

typedef struct {
	uint16_t width;		// in bits
	uint16_t height;
	const unsigned char *bmp;
} TTbitmap;

typedef struct {
	uint16_t height;
	uint16_t size;		// number of glyphs
	const wchar_t *all_chars;	// all characters covered by this font
	const TTbitmap *bitmaps;	// glyphs as bitmap structures
} TTfont;

class TTframebuffer {
 protected:
	unsigned char m_display[LCM_BYTES_PER_LINE * LCM_HEIGHT];

	// Clipping region
	int m_top_clp;
	int m_left_clp;
	int m_bottom_clp;
	int m_right_clp;

	// Origin
	int m_x_orig;
	int m_y_orig;

 public:
	enum DrawFlag {
		DRAW_NORMAL = 0x00,
		DRAW_INVERTED = 0x01,
		DRAW_DITHERED = 0x02,
		DRAW_DITHERED_INVERTED = 0x03,
	};

	 TTframebuffer() {
		init();
	} ~TTframebuffer() {
		shutdown();
	}

	void init() {
		memset(m_display, 0, sizeof(m_display));

		m_left_clp = 0;
		m_top_clp = 0;
		m_right_clp = LCM_BITS_PER_LINE;
		m_bottom_clp = LCM_HEIGHT;

		m_x_orig = 0;
		m_y_orig = 0;
	}

	void shutdown() {

	}

	const unsigned char *getFramebufferData() {
		return m_display;
	}

	void setClippingRegion(int left, int top, int right, int bottom) {
		debuglog(5, "setClippingRegion called with arguments (%d,%d,%d,%d)", left, top, right, bottom);

		if (left > 0) {
			m_left_clp = left;
		} else {
			debuglog(5,
				 "setClippingRegion: Left margin of clipping region CANNOT be set on value less than 0. It will be set to 0!",
				 0);
			m_left_clp = 0;
		}

		if (top > 0) {
			m_top_clp = top;
		} else {
			debuglog(5,
				 "setClippingRegion: Top margin of clipping region CANNOT be set on value less than 0. It will be set to 0!",
				 0);
			m_top_clp = 0;
		}

		if (right > 0) {
			m_right_clp = right;
		} else {
			debuglog(5,
				 "setClippingRegion: Right margin of clipping region CANNOT be set on value greater than %d. It will be set to %d!",
				 LCM_BITS_PER_LINE, LCM_BITS_PER_LINE);
			m_right_clp = 0;
		}

		if (bottom > 0) {
			m_bottom_clp = bottom;
		} else {
			debuglog(5,
				 "setClippingRegion: Bottom margin of clipping region CANNOT be set on value greater than %d. It will be set to %d!",
				 LCM_HEIGHT, LCM_HEIGHT);
			m_bottom_clp = 0;
		}
	}

	void resetClippingRegion() {
		m_left_clp = 0;
		m_top_clp = 0;
		m_right_clp = LCM_BITS_PER_LINE;
		m_bottom_clp = LCM_HEIGHT;
	}

	void setOrigin(int x, int y) {
		m_x_orig = x;
		m_y_orig = y;
	}

	void resetOrigin() {
		m_x_orig = 0;
		m_y_orig = 0;
	}

	void addToOrigin(int x, int y) {
		m_x_orig += x;
		m_y_orig += y;
	}

	void clear() {
		memset(m_display, 0, sizeof(m_display));
		resetOrigin();
	}

	/// @brief Fills intersection of the given rectangle and clipping region using given pattern
	///
	/// If given rectangle and clipping region are not intersecting,
	/// this function has no effect.
	///
	/// @param x - x coordinate of the top left corner of the rectangle
	/// @param y - y coordinate of the top left corner of the rectangle
	/// @param w - width of the rectangle
	/// @param h - height of the rectangle
	/// @param pattern - pattern used for filling the region
	/// @return void.
	void fillRect(int x, int y, int w, int h, unsigned char pattern = 0xff) {
		// Move the rectangle according to the origin of the display
		x += m_x_orig;
		y += m_y_orig;

		if (x < m_right_clp && y < m_bottom_clp && x + w > m_left_clp && y + h > m_top_clp) {

			// Adjust column to the left edge of the clipping region
			if (x < m_left_clp) {
				w -= m_left_clp - x;
				x = m_left_clp;
			}
			// Adjust row to the top of the clipping region
			if (y < m_top_clp) {
				h -= m_top_clp - y;
				y = m_top_clp;
			}
			// If necessary adjust desired width to fit width of the clipping region
			if (w > m_right_clp - x)
				w = m_right_clp - x;

			// If necessary adjust desired height to fit height of the clipping region
			if (h > m_bottom_clp - y)
				h = m_bottom_clp - y;

			unsigned char *wry = m_display + (x / 8) + (y * LCM_BYTES_PER_LINE);
			unsigned char bitx = x & 7;

			if (bitx + w <= 8) {
				unsigned char mask = 0xff >> bitx;
				mask &= 0xff << (8 - (bitx + w));

				for (; h > 0; h--, y++, wry += LCM_BYTES_PER_LINE) {
					*wry &= ~mask;
					*wry |= pattern & mask;
				}
			} else {
				unsigned char *wr;
				unsigned char mask1 = 0xff >> bitx;
				unsigned char mask2 = 0xff << (8 - ((x + w) & 7));
				size_t len = (w - (bitx ? (8 - bitx) : 0)) >> 3;

				for (; h > 0; h--, y++, wry += LCM_BYTES_PER_LINE) {
					wr = wry;

					if (mask1 != 0xff) {
						*wr &= ~mask1;
						*wr |= pattern & mask1;
						wr++;
					}

					if (len) {
						memset(wr, pattern, len);
						wr += len;
					}

					if (mask2 != 0) {
						*wr &= ~mask2;
						*wr |= pattern & mask2;
					}
				}
			}
		}
	}

	/// @brief Clears intersection of the given rectangle and clipping region
	///
	/// If given rectangle and clipping region are not intersecting,
	/// this function has no effect.
	///
	/// @param x - x coordinate of the top left corner of the rectangle
	/// @param y - y coordinate of the top left corner of the rectangle
	/// @param w - width of the rectangle
	/// @param h - height of the rectangle
	/// @return void.
	void clearRect(int x, int y, int w, int h) {
		fillRect(x, y, w, h, 0x00);
	}

	void patternRect(int x, int y, int w, int h, unsigned char pattern_even, unsigned char pattern_odd) {
		// Move the rectangle according to the origin of the display
		x += m_x_orig;
		y += m_y_orig;

		if (x < m_right_clp && y < m_bottom_clp && x + w > m_left_clp && y + h > m_top_clp) {

			// Adjust column to the left edge of the clipping region
			if (x < m_left_clp) {
				w -= m_left_clp - x;
				x = m_left_clp;
			}
			// Adjust row to the top of the clipping region
			if (y < m_top_clp) {
				h -= m_top_clp - y;
				y = m_top_clp;
			}
			// If necessary adjust desired width to fit width of the clipping region
			if (w > m_right_clp - x)
				w = m_right_clp - x;

			// If necessary adjust desired height to fit height of the clipping region
			if (h > m_bottom_clp - y)
				h = m_bottom_clp - y;

			unsigned char *wry = m_display + (x >> 3) + (y * LCM_BYTES_PER_LINE);
			unsigned char bitx = x & 7;

			if (bitx + w <= 8) {
				unsigned char mask = 0xff >> bitx;
				mask &= 0xff << (8 - (bitx + w));

				for (; h > 0; h--, y++, wry += LCM_BYTES_PER_LINE) {
					*wry &= ~mask | ((y & 1 ? pattern_odd : pattern_even) & mask);
				}
			} else {
				unsigned char mask1 = 0xff >> bitx;
				unsigned char mask2 = 0xff << (8 - ((x + w) & 7));
				size_t len = (w - (bitx ? 8 - bitx : 0)) >> 3;

				for (; h > 0; h--, y++, wry += LCM_BYTES_PER_LINE) {
					unsigned char *wr;
					unsigned char pattern;

					wr = wry;
					pattern = y & 1 ? pattern_odd : pattern_even;

					if (mask1 != 0xff) {
						*wr &= ~mask1 | (pattern & mask1);
						wr++;
					}

					for (size_t i = 0; i < len; i++, *wr &= pattern, wr++) ;

					if (mask2 != 0) {
						*wr &= ~mask2 | (pattern & mask2);
					}
				}
			}
		}
	}

	void drawArrow(uint16_t y, uint16_t h) {
		uint16_t w = (h - 1) / 2;
		uint16_t x = m_right_clp - m_x_orig - w;
		int inc = -1;

		for (int i = 0, j = w; i < h; ++i, j += inc, x -= inc) {
			if (i == (h - 1) / 2 || i == h / 2) {
				j = 0;
				x = m_right_clp - m_x_orig;
				inc = 1;
			} else {
				clearRect(x, y + i, j, 1);
			}
		}
	}

	void bitBlit(const TTbitmap * map, int x, int y) {
		if (!map) {
			debuglog(2, "bitBlit: Invalid bitmap pointer!", 0);
			return;
		}
		// Move the rectangle according to the origin of the display
		x += m_x_orig;
		y += m_y_orig;

		if (x < m_right_clp && y < m_bottom_clp && x + map->width > m_left_clp && y + map->height > m_top_clp) {

			int bmp_x = 0;	// x coordinate of the pixel from bitmap in current row
			int bmp_y = 0;	// Current row in the bitmap

			// Adjust column to the left edge of the clipping region
			if (x < m_left_clp) {
				bmp_x = m_left_clp - x;
				x = m_left_clp;
			}
			// Adjust row to the top of the clipping region
			if (y < m_top_clp) {
				bmp_y = m_top_clp - y;
				y = m_top_clp;
			}

			unsigned int bmp_bytes_per_line = (map->width + 7) >> 3;
			unsigned char *wry = m_display + (x >> 3) + (y * LCM_BYTES_PER_LINE);
			const unsigned char *bmp_wry = map->bmp + (bmp_x >> 3) + (bmp_y * bmp_bytes_per_line);

			while (y < m_bottom_clp && bmp_y < map->height) {
				unsigned char *wr = wry;	// Pointer of the current byte from the m_display
				const unsigned char *bmp_wr = bmp_wry;	// Pointer of the current byte from the bitmap
				int x_pos = x;	// x coordinate of the pixel on display which should be updated
				int bmp_x_pos = bmp_x;	// x coordinate of the pixel from bitmap which should be copied
				unsigned char bitx = x_pos & 7;	// Current bit inside of wr
				unsigned char bmp_bitx = bmp_x_pos & 7;	// Current bit inside of bpm_wr

				while (x_pos < m_right_clp && bmp_x_pos < map->width) {
					if ((*bmp_wr) & (0x80 >> bmp_bitx))
						*wr |= (0x80 >> bitx);

					if (++bmp_bitx == 8) {
						bmp_bitx = 0;
						bmp_wr++;
					}

					if (++bitx == 8) {
						bitx = 0;
						wr++;
					}
					x_pos++;
					bmp_x_pos++;
				}

				y++;
				bmp_y++;
				wry += LCM_BYTES_PER_LINE;
				bmp_wry += bmp_bytes_per_line;
			}
		}
	}

	void bitBlitInverted(const TTbitmap * map, int x, int y) {
		if (!map) {
			debuglog(2, "bitBlitInverted: Invalid bitmap pointer!", 0);
			return;
		}
		// Move the rectangle according to the origin of the display
		x += m_x_orig;
		y += m_y_orig;

		if (x < m_right_clp && y < m_bottom_clp && x + map->width > m_left_clp && y + map->height > m_top_clp) {

			int bmp_x = 0;	// x coordinate of the pixel from bitmap in current row
			int bmp_y = 0;	// Current row in the bitmap

			// Adjust column to the left edge of the clipping region
			if (x < m_left_clp) {
				bmp_x = m_left_clp - x;
				x = m_left_clp;
			}
			// Adjust row to the top of the clipping region
			if (y < m_top_clp) {
				bmp_y = m_top_clp - y;
				y = m_top_clp;
			}

			unsigned int bmp_bytes_per_line = (map->width + 7) >> 3;
			unsigned char *wry = m_display + (x >> 3) + (y * LCM_BYTES_PER_LINE);
			const unsigned char *bmp_wry = map->bmp + (bmp_x >> 3) + (bmp_y * bmp_bytes_per_line);

			while (y < m_bottom_clp && bmp_y < map->height) {
				unsigned char *wr = wry;	// Pointer of the current byte from the m_display
				const unsigned char *bmp_wr = bmp_wry;	// Pointer of the current byte from the bitmap
				int x_pos = x;	// x coordinate of the pixel on display which should be updated
				int bmp_x_pos = bmp_x;	// x coordinate of the pixel from bitmap which should be copied
				unsigned char bitx = x_pos & 7;	// Current bit inside of wr
				unsigned char bmp_bitx = bmp_x_pos & 7;	// Current bit inside of bpm_wr

				while (x_pos < m_right_clp && bmp_x_pos < map->width) {
					if ((*bmp_wr) & (0x80 >> bmp_bitx))
						*wr &= ~(0x80 >> bitx);

					if (++bmp_bitx == 8) {
						bmp_bitx = 0;
						bmp_wr++;
					}

					if (++bitx == 8) {
						bitx = 0;
						wr++;
					}
					x_pos++;
					bmp_x_pos++;
				}

				y++;
				bmp_y++;
				wry += LCM_BYTES_PER_LINE;
				bmp_wry += bmp_bytes_per_line;
			}
		}
	}

	void bitBlitDithered(const TTbitmap * map, int x, int y) {
		if (!map) {
			debuglog(2, "bitBlitDithered: Invalid bitmap pointer!", 0);
			return;
		}
		// Move the rectangle according to the origin of the display
		x += m_x_orig;
		y += m_y_orig;

		if (x < m_right_clp && y < m_bottom_clp && x + map->width > m_left_clp && y + map->height > m_top_clp) {

			int bmp_x = 0;	// x coordinate of the pixel from bitmap in current row
			int bmp_y = 0;	// Current row in the bitmap

			// Adjust column to the left edge of the clipping region
			if (x < m_left_clp) {
				bmp_x = m_left_clp - x;
				x = m_left_clp;
			}
			// Adjust row to the top of the clipping region
			if (y < m_top_clp) {
				bmp_y = m_top_clp - y;
				y = m_top_clp;
			}

			unsigned int bmp_bytes_per_line = (map->width + 7) >> 3;
			unsigned char *wry = m_display + (x >> 3) + (y * LCM_BYTES_PER_LINE);
			const unsigned char *bmp_wry = map->bmp + (bmp_x >> 3) + (bmp_y * bmp_bytes_per_line);

			while (y < m_bottom_clp && bmp_y < map->height) {
				unsigned char *wr = wry;	// Pointer of the current byte from the m_display
				const unsigned char *bmp_wr = bmp_wry;	// Pointer of the current byte from the bitmap
				int x_pos = x;	// x coordinate of the pixel on display which should be updated
				int bmp_x_pos = bmp_x;	// x coordinate of the pixel from bitmap which should be copied
				unsigned char bitx = x_pos & 7;	// Current bit inside of wr
				unsigned char bmp_bitx = bmp_x_pos & 7;	// Current bit inside of bpm_wr

				while (x_pos < m_right_clp && bmp_x_pos < map->width) {
					if ((*bmp_wr) & (0x80 >> bmp_bitx))
						*wr |= (0x80 >> bitx);

					if (++bmp_bitx == 8) {
						bmp_bitx = 0;
						bmp_wr++;
					}

					if (++bitx == 8) {
						bitx = 0;
						*wr &= y & 1 ? 0xaa : 0x55;	// Dither current byte before going to next one
						wr++;
					}
					x_pos++;
					bmp_x_pos++;
				}

				// If current byte was not dithered, dither it now
				if (bitx) {
					(*wr) &= y & 1 ? 0xaa : 0x55;
				}

				y++;
				bmp_y++;
				wry += LCM_BYTES_PER_LINE;
				bmp_wry += bmp_bytes_per_line;
			}
		}
	}

	void bitBlitDitheredInverted(const TTbitmap * map, int x, int y) {
		if (!map) {
			debuglog(2, "bitBlitDitheredInverted: Invalid bitmap pointer!", 0);
			return;
		}
		// Move the rectangle according to the origin of the display
		x += m_x_orig;
		y += m_y_orig;

		if (x < m_right_clp && y < m_bottom_clp && x + map->width > m_left_clp && y + map->height > m_top_clp) {

			int bmp_x = 0;	// x coordinate of the pixel from bitmap in current row
			int bmp_y = 0;	// Current row in the bitmap

			// Adjust column to the left edge of the clipping region
			if (x < m_left_clp) {
				bmp_x = m_left_clp - x;
				x = m_left_clp;
			}
			// Adjust row to the top of the clipping region
			if (y < m_top_clp) {
				bmp_y = m_top_clp - y;
				y = m_top_clp;
			}

			unsigned int bmp_bytes_per_line = (map->width + 7) >> 3;
			unsigned char *wry = m_display + (x >> 3) + (y * LCM_BYTES_PER_LINE);
			const unsigned char *bmp_wry = map->bmp + (bmp_x >> 3) + (bmp_y * bmp_bytes_per_line);

			while (y < m_bottom_clp && bmp_y < map->height) {
				unsigned char *wr = wry;	// Pointer of the current byte from the m_display
				const unsigned char *bmp_wr = bmp_wry;	// Pointer of the current byte from the bitmap
				int x_pos = x;	// x coordinate of the pixel on display which should be updated
				int bmp_x_pos = bmp_x;	// x coordinate of the pixel from bitmap which should be copied
				unsigned char bitx = x_pos & 7;	// Current bit inside of wr
				unsigned char bmp_bitx = bmp_x_pos & 7;	// Current bit inside of bpm_wr

				while (x_pos < m_right_clp && bmp_x_pos < map->width) {
					if ((*bmp_wr) & (0x80 >> bmp_bitx))
						*wr &= ~(0x80 >> bitx);

					if (++bmp_bitx == 8) {
						bmp_bitx = 0;
						bmp_wr++;
					}

					if (++bitx == 8) {
						bitx = 0;
						*wr &= y & 1 ? 0xaa : 0x55;	// Dither current byte before going to next one
						wr++;
					}
					x_pos++;
					bmp_x_pos++;
				}

				// If current byte was not dithered, dither it now
				if (bitx) {
					(*wr) &= y & 1 ? 0xaa : 0x55;
				}

				y++;
				bmp_y++;
				wry += LCM_BYTES_PER_LINE;
				bmp_wry += bmp_bytes_per_line;
			}
		}
	}

	int drawText(int x, int y, const wchar_t * text, const TTfont * f, int flag) {
		if (!text || !f) {
			debuglog(2, "drawText: Invalid arguments (text=%d, f=%d)!", text, f);
			return 0;
		}

		size_t i = 0;
		void (TTframebuffer::*pBitBlit) (const TTbitmap * map, int x, int y);

		switch (flag) {
		case DRAW_NORMAL:
			pBitBlit = &TTframebuffer::bitBlit;
			break;
		case DRAW_DITHERED_INVERTED:
			pBitBlit = &TTframebuffer::bitBlitDitheredInverted;
			break;
		case DRAW_INVERTED:
			pBitBlit = &TTframebuffer::bitBlitInverted;
			break;
		case DRAW_DITHERED:
			pBitBlit = &TTframebuffer::bitBlitDithered;
			break;
		default:
			pBitBlit = &TTframebuffer::bitBlit;
			break;
		}

		while (text[i]) {
			const TTbitmap *character = getGlyphBitmap(text[i], f);

			if (!character) {
				debuglog(2, "drawText: Given font does not contain character 0x%04x!", text[i]);
				return 0;
			}

			(this->*pBitBlit) (character, x, y);

			x += character->width;
			++i;
		}

		return 1;
	}

	static int getCharWidth(wchar_t c, const TTfont * f) {
		if (!f) {
			debuglog(2, "getCharWidth: Invalid font pointer!", 0);
			return 0;
		}

		const TTbitmap *character = getGlyphBitmap(c, f);

		if (!character) {
			debuglog(2, "getCharWidth: Given font does not contain character 0x%04x!", c);
			return 0;
		}

		return character->width;
	}

	static int getTextWidth(const wchar_t * text, const TTfont * f) {
		if (!text || !f) {
			debuglog(2, "getTextWidth: Invalid arguments (text=%d, f=%d)!", text, f);
			return 0;
		}

		int width = 0;
		size_t i = 0;

		while (text[i]) {
			int w = getCharWidth(text[i], f);

			//assert(w >= 0);

			width += w;
			++i;
		}

		return width;
	}

	static int getTextHeight(const TTfont * f) {
		if (!f) {
			debuglog(2, "getTextHeight: Invalid font pointer!", 0);
			return 0;
		}

		return f->height;
	}

	void copyVertical(TTframebuffer & from, int fromline, int height, int toline = 0) {
		assert(fromline >= 0 && toline >= 0);
		if (fromline + height > LCM_HEIGHT)
			height = LCM_HEIGHT - fromline;
		if (toline + height > LCM_HEIGHT)
			height = LCM_HEIGHT - toline;
		if (height <= 0)
			return;
		unsigned char *tod = m_display + toline * LCM_BYTES_PER_LINE;
		const unsigned char *fromd = from.getFramebufferData() + fromline * LCM_BYTES_PER_LINE;
		memcpy(tod, fromd, height * LCM_BYTES_PER_LINE);
	}

	void copy(TTframebuffer & from) {
		memcpy(m_display, from.getFramebufferData(), sizeof(m_display));
	}

	void copyHorizontal(TTframebuffer & from, int frombyte, int width, int tobyte = 0) {
		assert(frombyte >= 0 && tobyte >= 0);
		if (frombyte + width > LCM_BYTES_PER_LINE)
			width = LCM_BYTES_PER_LINE - frombyte;
		if (tobyte + width > LCM_BYTES_PER_LINE)
			width = LCM_BYTES_PER_LINE - tobyte;
		if (width <= 0)
			return;
		unsigned char *tod = m_display + tobyte;
		const unsigned char *fromd = from.getFramebufferData() + frombyte;
		for (int i = 0; i < LCM_HEIGHT; i++) {
			memcpy(tod, fromd, width);
			tod += LCM_BYTES_PER_LINE;
			fromd += LCM_BYTES_PER_LINE;
		}
	}

 protected:
	static const TTbitmap *getGlyphBitmap(wchar_t c, const TTfont * f) {
		if (NULL == f) {
			debuglog(2, "getGlyphBitmap: Invalid font pointer!", 0);
			return NULL;
		}

		int start, end, mid;

		start = 0;
		end = f->size - 1;
		mid = (start + end) / 2;

		const TTbitmap *tmp = &f->bitmaps[end];

		while (start <= end) {
			if (f->all_chars[mid] < c) {
				start = mid + 1;
			} else if (f->all_chars[mid] == c) {
				tmp = &f->bitmaps[mid];
				break;
			} else {
				end = end - 1;
			}
			mid = (start + end) / 2;
		}

		return tmp;
	}

};
