// TomTom camera platform
// Menu widgets
// (c) 2014. Nebojsa Sumrak

#pragma once

#include "menu.h"
#include "debug.h"
#include <ctype.h>
#include <wchar.h>

bool footerWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	assert(item && item->content);

	wchar_t *name = (wchar_t *) (item->content);
	TTfont *font = &font_fn_boldcn_18;

	switch (e) {
	case TTmenu::EVENT_DRAW:
		{
			debuglog(5, "footer_callback: EVENT_DRAW!", 0);
			if (!p1.p) {
				debuglog(2, "footer_callback: Invalid argument!", 0);
				break;
			}

			TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);
			fb->fillRect(0, 0, LCM_BITS_PER_LINE, 1);
			fb->drawText((LCM_BITS_PER_LINE - fb->getTextWidth(name, font)) / 2, 1, name, font,
				     TTframebuffer::DRAW_NORMAL);
			break;
		}
	case TTmenu::EVENT_MEASURE_ITEM:
		p1.s1 = (short)(LCM_BITS_PER_LINE);
		if (!p1.s2)
			p1.s2 = (short)(font->height + 1);
		break;
	}

	return false;
}

bool labelWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	assert(item && item->content);

	wchar_t *name = (wchar_t *) (item->content);
	TTfont *font;
	switch((int)item->param) {
		case 62: font = &font_fn_boldcn_62; break;
		case 42: font = &font_fnp_bold_42; break;
		case 18: font = &font_fn_boldcn_18; break;
		default: font = &font_fn_heavycn_22;
	};

	switch (e) {
	case TTmenu::EVENT_ENTER:
	case TTmenu::EVENT_EXIT:
		return true;
	case TTmenu::EVENT_DRAW:
		{
			debuglog(5, "label_callback: EVENT_DRAW!", 0);
			TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);

			int drawFlag = TTframebuffer::DRAW_NORMAL;
			int x;
			switch (item->flags & TTmenu::MENU_ALIGN_MASK) {
			case TTmenu::MENU_RIGHT_ALIGN:
				x = item->width - TTframebuffer::getTextWidth(name, font);
				break;
			case TTmenu::MENU_CENTER_ALIGN:
				x = (item->width - TTframebuffer::getTextWidth(name, font)) / 2;
				break;
			default:
				x = 0;
			}
			fb->drawText(x, 0, name, font, drawFlag);
			break;
		}
	case TTmenu::EVENT_MEASURE_ITEM:
		debuglog(5, "option_callback: EVENT_MEASURE_ITEM!", 0);
		if (!p1.s1)
			p1.s1 = TTframebuffer::getTextWidth(name, font);
		if (!p1.s2)
			p1.s2 = TTframebuffer::getTextHeight(font);
		break;
	}

	return false;
}

bool optionWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	assert(item && item->content);

	wchar_t *name = (wchar_t *) (item->content);
	TTfont *font = &font_fn_heavycn_22;

	switch (e) {
	case TTmenu::EVENT_DRAW:
		{
			debuglog(5, "option_callback: EVENT_DRAW!", 0);
			if (!p1.p) {
				debuglog(2, "option_callback: Invalid argument!", 0);
				break;
			}

			TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);

			debuglog(5, "flags = %d", item->flags);

			int drawFlag = TTframebuffer::DRAW_NORMAL;
			//if(item->flags&CONTENT_DISABLED) {
			//      drawFlag|=TTframebuffer::DRAW_DITHERED;
			//} else
			if (item->flags & TTmenu::MENU_FOCUSED) {
				fb->fillRect(0, 0, LCM_BITS_PER_LINE, item->height-1);
				drawFlag |= TTframebuffer::DRAW_INVERTED;
			}

			int x;
			switch (item->flags & TTmenu::MENU_ALIGN_MASK) {
			case TTmenu::MENU_RIGHT_ALIGN:
				x = item->width - TTframebuffer::getTextWidth(name, font);
				if (x > 4)
					x -= 4;
				break;
			case TTmenu::MENU_CENTER_ALIGN:
				x = (item->width - TTframebuffer::getTextWidth(name, font)) / 2;
				break;
			default:
				x = 4;
			}

			fb->drawText(x, 0, name, font, drawFlag);

			if (item->flags & TTmenu::MENU_FOCUSED && item->submenu) {
				fb->drawArrow(0, font->height);
			}
			break;
		}
	case TTmenu::EVENT_MEASURE_ITEM:
		{
			debuglog(5, "option_callback: EVENT_MEASURE_ITEM!", 0);
			p1.s1 = (short)(LCM_BITS_PER_LINE);
			if (!p1.s2)
				p1.s2 = (short)(font->height + 1);
			break;
		}
	}

	return false;
}

bool spinWg(TTmenu::CallbackEvent e, TTmenu::MenuItem * item, Variant & p1)
{
	assert(item && item->content);

	struct spinnerParser {
		int selected;
		int rl, rh;
		int type;

		void setSelected(int sel) {
			selected = sel;
			if (sel < rl)
				selected = rl;
			if (sel > rh)
				selected = rh;
			debuglog(5, "sel=%d, rl=%d, rh=%d, selected=%d", sel, rl, rh, selected);
		} spinnerParser(wchar_t * name, int sel = 0) {
			selected = rl = rh = 0;
			if (!(type = !isdigit(*name))) {
				debuglog(5, "sp digit type (%S)", name);
				rl = wcstol(name, 0, 10);
				wchar_t *minus = wcschr(name, L'-');
				if (minus)
					rh = wcstol(minus + 1, 0, 10);
				debuglog(5, "rl-rh = %d-%d", rl, rh);
				if (rl > rh) {
					int k = rh;
					rh = rl;
					rl = k;
				}
			} else {
				debuglog(5, "sp enum type (%S)", name);
				wchar_t *t = name;
				rl = 1;
				rh = 0;
				do {
					t = wcschr(t, L'|');
					rh++;
					if (!t)
						break;
					t++;
				} while (1);
			}
			setSelected(sel);
		}

		void copyStr(wchar_t * name, int which, wchar_t * dest) {
			if (!type) {
				swprintf(dest, 64, L"%d", which);
				return;
			}
			*dest = 0;
			wchar_t *t = name;
			int i = 1;
			do {
				wchar_t *tt = wcschr(t, L'|');
				if (i == which) {
					if (!tt)
						wcscpy(dest, t);
					else {
						wcsncpy(dest, t, tt - t);
						dest[tt - t] = 0;
					}
					break;
				}
				if (!tt)
					break;
				t = tt + 1;
				i++;
			} while (1);
		}
	};

	wchar_t *name = (wchar_t *) (item->content);
	TTfont *font = &font_fn_heavycn_22;

	switch (e) {
	case TTmenu::EVENT_KEY:
		break;
	case TTmenu::EVENT_DRAW:
		{
			static const unsigned char uparrbmp[] = { 0x20, 0x70, 0xf8 };
			static const unsigned char dnarrbmp[] = { 0xf8, 0x70, 0x20 };
			static const TTbitmap uparrow = { 5, 3, uparrbmp };
			static const TTbitmap downarrow = { 5, 3, dnarrbmp };
			debuglog(5, "draw", 0);
			spinnerParser sp(name, (int)item->wgdata);
			wchar_t buffek[64];
			TTframebuffer *fb = static_cast<TTframebuffer *>(p1.p);

			debuglog(5, "flags = %d", item->flags);

			int aw = (item->width - 5) / 2;
			sp.copyStr(name, sp.selected, buffek);
			fb->drawText((item->width - fb->getTextWidth(buffek, font)) / 2, 7, buffek, font,
				     TTframebuffer::DRAW_NORMAL);
			if (item->flags & TTmenu::MENU_FOCUSED) {
				fb->bitBlit(&uparrow, aw, 0);
				fb->fillRect(1, 4, item->width - 2, 1);
				fb->fillRect(1, item->height - 6, item->width - 2, 1);
				fb->bitBlit(&downarrow, aw, item->height - 4);
			}
			break;
		}
	case TTmenu::EVENT_MEASURE_ITEM:
		{
			debuglog(5, "measure", 0);
			spinnerParser sp(name);

			if (!p1.s1) {
				wchar_t buffek[64];
				debuglog(5, "measure sp type: %d", sp.type);
				if (!sp.type) {
					swprintf(buffek, 64, L"%d", sp.rh);
					for (wchar_t * t = buffek; *t; t++)
						*t = L'W';
					p1.s1 = TTframebuffer::getTextWidth(buffek, font);
				} else {
					p1.s1 = 0;
					for (int i = 0; i < sp.rh; i++) {
						sp.copyStr(name, i, buffek);
						int w = TTframebuffer::getTextWidth(buffek, font);
						if (w > p1.s1)
							p1.s1 = w;
					}
				}
				p1.s1 += 2;	// border
			}
			if (!p1.s2)
				p1.s2 = (short)(font->height + 14);
			break;
		}
	}

	return false;
}
