// TomTom Video platform API
// leds wrapper
// TT, Jeroen Jensen 2014.

#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#define PATH_SYS_CLASS_LEDS "/sys/class/leds"

bool setLedBrightness(const char *ledName, int brightness)
{
	char filepath[256];
	char data[32];
	int n, rv;

	snprintf(filepath, sizeof(filepath), "%s/%s/brightness", PATH_SYS_CLASS_LEDS, ledName);
	int fd = open(filepath, O_WRONLY);
	if (fd < 0) {
		perror(filepath);
		return false;
	}

	n = snprintf(data, sizeof(data) - 1, "%d", brightness);
	data[n] = '\0';

	rv = write(fd, data, n);
	if (rv != n) {
		perror(filepath);
		close(fd);
		return false;
	}

	close(fd);
	return true;
}
