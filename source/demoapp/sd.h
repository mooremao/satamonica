// TomTom Video platform API
// SD card services
// Nebojsa Sumrak 2014.

#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>

// margin in KB
#define SDCARD_FREESPACE_MARGIN	(10UL*1024UL)

#define SDCARD_MNT_PATH "/mnt/mmc"

void sd_getFileCount(int &numfil)
{
	struct dirent *df;
	DIR *dir = opendir(DCIM_CAMERA_DIR_PATH);
	if (!dir) {
		debuglog(3, "Can't open sd card!", 0);
		return;
	}
	while ((df = readdir(dir)) != NULL) {
		int len = strlen(df->d_name);
		if (len > 7 && !strcasecmp(df->d_name + len - 5, ".h264") && isdigit(df->d_name[len - 6])
		    && isdigit(df->d_name[len - 7]) && !(df->d_type & DT_DIR)) {
			int num = atoi(&df->d_name[len - 7]);
			if (num > numfil)
				numfil = num;
		} else if (len > 6 && !strcasecmp(df->d_name + len - 4, ".jpg") && isdigit(df->d_name[len - 6])
			&& isdigit(df->d_name[len - 6]) && !(df->d_type & DT_DIR)) {
			int num = atoi(&df->d_name[len - 6]);
			if (num > numfil)
				numfil = num;
		}
		else if (len > 6 && !strcasecmp(df->d_name + len - 4, ".raw") && isdigit(df->d_name[len - 5])
                    && isdigit(df->d_name[len - 6]) && !(df->d_type & DT_DIR)) {
                        int num = atoi(&df->d_name[len - 6]);
                        if (num > numfil)
                                numfil = num;
                }
	}
	closedir(dir);
	debuglog(4, "numfiles found on sdcard: %d", numfil);
}

inline bool sd_checkCard()
{
	if (system("mount|grep \"/mnt/mmc\""))
		return false;
	return true;
	//return (!mkdir("/mnt/mmc/thumbs", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) || errno==EEXIST);
}

unsigned long sd_getFreeSpace(unsigned long *total_capacity = 0)
{
	int mount_fd;
	struct stat mp, mp_parent;
	struct statfs buf;
	unsigned long freespace;

	/* Stat parent directory of mount point, we need it to compare device ids later on. */
	if (stat(SDCARD_MNT_PATH "/..", &mp_parent) != 0) {
		perror("stat \"" SDCARD_MNT_PATH "/..\" failed");
		if (total_capacity)
			*total_capacity = 0;
		return 0;
	}

	/* We need to keep mount point open to ensure the fstatfs is on at the same mounted file system */
	mount_fd = open(SDCARD_MNT_PATH, O_RDONLY);
	if (mount_fd < 0) {
		perror("Failed to open mount point: \"" SDCARD_MNT_PATH "\"");
		if (total_capacity)
			*total_capacity = 0;
		return 0;
	}

	/* Stat mount point to get device id of mount point */
	if (fstat(mount_fd, &mp) != 0) {
		perror("fstat \"" SDCARD_MNT_PATH "\" failed");
		close(mount_fd);
		if (total_capacity)
			*total_capacity = 0;
		return 0;
	}

	/* Compare device id. When they equal, the mount point is not mounted. */
	if (mp.st_dev == mp_parent.st_dev) {
		close(mount_fd);
		if (total_capacity)
			*total_capacity = 0;
		return 0;
	}

	/* Calculate free space */
	if (fstatfs(mount_fd, &buf) != 0) {
		perror("fstatfs \"" SDCARD_MNT_PATH "\" failed");
		close(mount_fd);
		if (total_capacity)
			*total_capacity = 0;
		return 0;
	}

	if (total_capacity)
		*total_capacity = ((unsigned long long)buf.f_bsize * buf.f_blocks) / (1024ULL);
	freespace = (unsigned long)(((unsigned long long)buf.f_bsize * (unsigned long long)buf.f_bavail) / (1024ULL));
	if (freespace < SDCARD_FREESPACE_MARGIN)
		freespace = 0;
	else
		freespace -= SDCARD_FREESPACE_MARGIN;

	close(mount_fd);

	return freespace;
}

inline bool sd_fileExist(const char *filetname)
{
	struct stat st;
	return (!stat(filetname, &st));
}

inline off_t sd_fileSize(const char *filetname)
{
	struct stat st;
	return (!stat(filetname, &st) ? st.st_size : 0);
}

inline bool readProc(const char *procpath, const char *fmt, ...)
{
	va_list varg;
	FILE *filet = fopen(procpath, "r");
	if(!filet) {
		debuglog(2, "sd:readProc: failed to read '%s'", procpath);
		return false;
	}
	va_start(varg, fmt);
	vfscanf(filet, fmt, varg);
	fclose(filet);
	va_end(varg);
	return true;
}
