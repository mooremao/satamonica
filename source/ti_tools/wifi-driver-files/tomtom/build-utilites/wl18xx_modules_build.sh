#!/bin/bash

if [ ! -e setup-env ]
then
	echo "No setup-env"
	exit 1
fi
source setup-env
ME=$0

old_dir=`pwd`

function compat-wireless()
{
	stage=$1

	if [ x"$stage" = "xbuild" -o x"$stage" = "xall" ]
	then
		cd ${WORK_SPACE}/compat-wireless
		#./scripts/admin-refresh.sh network
		#./scripts/driver-select wl18xx
		#echo "# Use platform data with kernels older then 3.8 that dont use DT" >> config.mk
		#echo "export CONFIG_WILINK_PLATFORM_DATA=y" >> config.mk
		#echo "export CONFIG_WLCORE_SDIO=m" >> config.mk
		#cp -f ${WORK_SPACE}/../build-utilites/sdio.c ${WORK_SPACE}/wl18xx/drivers/net/wireless/ti/wlcore/.
		#cp -f ${WORK_SPACE}/../build-utilites/sdio.c ${WORK_SPACE}/compat-wireless/drivers/net/wireless/ti/wlcore/.
		make KLIB_BUILD=${KLIB_BUILD} KLIB=${ROOTFS} || exit 1
	fi
	if [ x"$stage" = "xinstall"  -o x"$stage" = "xall" ]
	then
		find ${ROOTFS} -name "wl12xx*.ko" | xargs rm -f
		find ${ROOTFS} -name "sch_codel.ko" | xargs rm -f
		find ${ROOTFS} -name "sch_fq_codel.ko" | xargs rm -f
		find ${ROOTFS} -name "wlcore_sdio.ko" | xargs rm -f
		find ${ROOTFS} -name "wlcore_spi.ko" | xargs rm -f
		find ${ROOTFS} -name "wlcore.ko" | xargs rm -f
		find ${ROOTFS} -name "wl18xx.ko" | xargs rm -f
		find ${ROOTFS} -name "mac80211.ko" | xargs rm -f
		find ${ROOTFS} -name "cfg80211.ko" | xargs rm -f
		find ${ROOTFS} -name "compat.ko" | xargs rm -f
		cd ${WORK_SPACE}/compat-wireless
		make KLIB_BUILD=${KLIB_BUILD} KLIB=${ROOTFS} INSTALL_MOD_PATH=${ROOTFS} install-modules
	fi

	if [ x"$stage" = "xclean" ]
	then
		cd $WORK_SPACE/compat-wireless
		make KLIB_BUILD=${KLIB_BUILD} KLIB=${ROOTFS} clean || exit 1
#		cd $WORK_SPACE && rm -rf compat-wireless
	fi

	cd $WORK_SPACE
}


function usage ()
{
	echo "This script compiles one of following utilities: libnl, openssl, hostapd, wpa_supplicant,wl18xx_modules,firmware,crda,calibrator,wlconf"
	echo "by calling specific utility name and action."
	echo "In case the options is 'all' all utilities will be downloaded and installed on root file system."
	echo "File setup-env contains all required environment variables, for example:"
	echo "	ROOTFS=<path to target root file system>."
	echo "Part of operations requires root access."
	echo "Usage: `basename $ME` action <clean|build|all|install>"

}

function package_dir_exists()
{
	if [ -d "$1" ]
	then
		echo "Package $2 already downloaded at: $1"
		return 1
	fi
	return 0
}
function check_env()
{
	[ -e ${WORK_SPACE}/.check_env.stamp ] && return 0
	which dpkg 2>&1>/dev/null || return 0
	err=0
	ret=0
	packages="python python-m2crypto bash bison flex perl bc corkscrew"
	for p in ${packages}
	do
		echo -n "Checking ${p}..."
		present=`dpkg --get-selections ${p} 2>/dev/null | awk '{print $1}'`
		[ x"${present}" != x"${p}" ] && echo "Package ${p} is not found. Please run 'apt-get install ${p}' to install it." && err=1 && ret=1
		[ ${err} -ne 1 ] && echo "OK"
		err=0
	done
	return ${ret}
}
############################# MAIN ##############################################
# First building environment should be checked
check_env || exit 1
if [ -z $CROSS_COMPILE ]
then
	#lets find some
	tool_path=`which ${CROSS_COMPILE}gcc`
	if [ $? -ne 0 ]
	then
		echo "No tool chain is found"
		exit 1
	fi	
	export CROSS_COMPILE=`dirname $tool_path`/${CROSS_COMPILE}
fi

if [ -z $KLIB_BUILD ]
then
	echo "Path to kernel sources has to be defined"
	exit 1
fi

if [ -z $ROOTFS ]
then
	echo "No path to root file system"
	exit 1
fi
argc=$#
if [ $argc -lt 1 ]
then
	usage
	exit 1
fi

if [ $argc -eq 1 ]
then
	package=wl18xx_modules
	stage=$1
fi
if [ ! -d $WORK_SPACE ]
then
	mkdir -p $WORK_SPACE
	touch ${WORK_SPACE}/.check_env.stamp
fi
cd ${WORK_SPACE}

case $package in
	wl18xx_modules)
		case $stage in
			build)
				if [ ! -d ${WORK_SPACE}/compat-wireless ]
				then
					echo "compat-wireless source doesn't exist"
					exit 1
				fi
				cd ${WORK_SPACE}/compat-wireless
				compat-wireless "build"
				;;
			install)
				if [ ! -d ${WORK_SPACE}/compat-wireless ]
				then
					echo "compat-wireless source doesn't exist"
					exit 1
				else
					cd ${WORK_SPACE}/compat-wireless
					compat-wireless "install"
				fi
				;;
			all)
				compat-wireless "clean"
				compat-wireless "all"
				;;
			clean)
				compat-wireless "clean"
			;;
			*)
				echo "Error: illegal action for wl18xx_modules"
				exit 1
				;;
		esac

		;;
	*)
		usage
		exit 1
esac
