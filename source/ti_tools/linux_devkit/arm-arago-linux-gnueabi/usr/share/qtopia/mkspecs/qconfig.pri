#configuration
CONFIG +=  def_files_disabled exceptions no_mocdepend release stl qt_no_framework
QT_ARCH = arm
QT_EDITION = OpenSource
QT_CONFIG +=  minimal-config small-config medium-config large-config full-config qt3support phonon phonon-backend egl opengl opengles2 embedded reduce_relocations ipv6 clock-gettime clock-monotonic mremap getaddrinfo ipv6ifname getifaddrs inotify system-jpeg system-png png system-tiff system-freetype system-zlib glib gstreamer dbus openssl alsa pulseaudio xmlpatterns multimedia audio-backend svg script scripttools declarative release

#versioning
QT_VERSION = 4.7.2
QT_MAJOR_VERSION = 4
QT_MINOR_VERSION = 7
QT_PATCH_VERSION = 2

#namespaces
QT_LIBINFIX = E
QT_NAMESPACE = 
QT_NAMESPACE_MAC_CRC = 

