var searchData=
[
  ['mapmask',['mapMask',['../struct_proc_mgr___addr_info__tag.html#adb10afaa4a6940d337ebbb1d83d9433a',1,'ProcMgr_AddrInfo_tag::mapMask()'],['../struct_proc_mgr___mapped_mem_entry.html#afcd24b5c66e2fc7c557a610d6b85781f',1,'ProcMgr_MappedMemEntry::mapMask()']]],
  ['maxallocatedblocks',['maxAllocatedBlocks',['../struct_heap_buf_m_p___extended_stats.html#a94d85b9a5a7632bfa0d0962917055df1',1,'HeapBufMP_ExtendedStats']]],
  ['maxinstances',['maxInstances',['../struct_frame_q___config___tag.html#a9bb4cbb04e3de4e727fa1157e4254245',1,'FrameQ_Config_Tag::maxInstances()'],['../struct_frame_q_buf_mgr___config___tag.html#a39c6b7a0a605e5e8a787f36e55771359',1,'FrameQBufMgr_Config_Tag::maxInstances()']]],
  ['maxmemoryregions',['maxMemoryRegions',['../struct_proc_mgr___proc_info__tag.html#acf9846d4b78029e9ba66ef4441335bb3',1,'ProcMgr_ProcInfo_tag']]],
  ['maxnamelen',['maxNameLen',['../struct_name_server___params.html#a8fcc951ae29abaa33f11360cc4fa6441',1,'NameServer_Params']]],
  ['maxruntimeentries',['maxRuntimeEntries',['../struct_name_server___params.html#a26ee68e20b559b4c238c1e5a8646191f',1,'NameServer_Params']]],
  ['maxvaluelen',['maxValueLen',['../struct_name_server___params.html#afa31d4aaa38615278dd7d8b25603bf5d',1,'NameServer_Params']]],
  ['mementries',['memEntries',['../struct_proc_mgr___proc_info__tag.html#aafeb12b7be37f12c8bc80579fe37d40a',1,'ProcMgr_ProcInfo_tag']]],
  ['memmgrtype',['memMgrType',['../struct_syslink_mem_mgr___create_params__tag.html#a96389c67637b0e1cc75a2c710aab98ab',1,'SyslinkMemMgr_CreateParams_tag']]],
  ['minalign',['minAlign',['../struct_ipc_mem_mgr___config__t.html#a9fffabf317e4053b19eab932541cdf89',1,'IpcMemMgr_Config_t']]],
  ['msgid',['msgId',['../struct_message_q___msg_header.html#abb97c08bbc06479c4a5e5fde91412e57',1,'MessageQ_MsgHeader']]],
  ['msgsize',['msgSize',['../struct_message_q___msg_header.html#adee0e6ed997b218584e2009854fa5214',1,'MessageQ_MsgHeader']]]
];
