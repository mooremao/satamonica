var searchData=
[
  ['memoryos_5fcacheflags_5fcached',['MemoryOS_CacheFlags_Cached',['../_memory_defs_8h.html#a16d07cac30b18488465f2b7d9b55a4bba8117281ad37ed9269ddb17d23540d505',1,'MemoryDefs.h']]],
  ['memoryos_5fcacheflags_5fdefault',['MemoryOS_CacheFlags_Default',['../_memory_defs_8h.html#a16d07cac30b18488465f2b7d9b55a4bbaad35d33bcdeb6c4d590818b779e455fc',1,'MemoryDefs.h']]],
  ['memoryos_5fcacheflags_5fendvalue',['MemoryOS_CacheFlags_EndValue',['../_memory_defs_8h.html#a16d07cac30b18488465f2b7d9b55a4bbae34b5308327719f2d5043206452657fc',1,'MemoryDefs.h']]],
  ['memoryos_5fcacheflags_5funcached',['MemoryOS_CacheFlags_Uncached',['../_memory_defs_8h.html#a16d07cac30b18488465f2b7d9b55a4bbabef2ade801007716d916d1fb05434f2d',1,'MemoryDefs.h']]],
  ['memoryos_5fmemtypeflags_5fdefault',['MemoryOS_MemTypeFlags_Default',['../_memory_defs_8h.html#a453a4733642b2fd8f94d40882f1997dda3ce5e290d718168de0335fbede0fa4ed',1,'MemoryDefs.h']]],
  ['memoryos_5fmemtypeflags_5fdma',['MemoryOS_MemTypeFlags_Dma',['../_memory_defs_8h.html#a453a4733642b2fd8f94d40882f1997ddad0c6939ee334b6c3200ebcc708b635af',1,'MemoryDefs.h']]],
  ['memoryos_5fmemtypeflags_5fendvalue',['MemoryOS_MemTypeFlags_EndValue',['../_memory_defs_8h.html#a453a4733642b2fd8f94d40882f1997dda05983a09729b3105745ea5f1c72a3d90',1,'MemoryDefs.h']]],
  ['memoryos_5fmemtypeflags_5fphysical',['MemoryOS_MemTypeFlags_Physical',['../_memory_defs_8h.html#a453a4733642b2fd8f94d40882f1997ddaaa1f0581b3032dc38adff317282331e1',1,'MemoryDefs.h']]],
  ['messageq_5fhighpri',['MessageQ_HIGHPRI',['../_message_q_8h.html#a01e825f1f15fa12643eabea99ca76248ae06ba6f7cd0cafb89a56b1224693755d',1,'MessageQ.h']]],
  ['messageq_5fnormalpri',['MessageQ_NORMALPRI',['../_message_q_8h.html#a01e825f1f15fa12643eabea99ca76248a12365e9d70b3fc967dc147e2f8a0d4fb',1,'MessageQ.h']]],
  ['messageq_5freservedpri',['MessageQ_RESERVEDPRI',['../_message_q_8h.html#a01e825f1f15fa12643eabea99ca76248a3baaafcfd52556f649576a8c08748d03',1,'MessageQ.h']]],
  ['messageq_5furgentpri',['MessageQ_URGENTPRI',['../_message_q_8h.html#a01e825f1f15fa12643eabea99ca76248a18496bc5d4650cd4eb8db0c399d95399',1,'MessageQ.h']]]
];
