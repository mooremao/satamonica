var searchData=
[
  ['ringio_5fdatabuf_5fmaintaincache',['RingIO_DATABUF_MAINTAINCACHE',['../_ring_i_o_8h.html#a03c43c9ea543bfedbcb7af2318cd90e2a9158af25eb0eed931c27aa6209620215',1,'RingIO.h']]],
  ['ringio_5fmode_5freader',['RingIO_MODE_READER',['../_ring_i_o_8h.html#ade2943110d698cdaf160762aa46c12afa7ab53118795e0b4c7b39c267a360b459',1,'RingIO.h']]],
  ['ringio_5fmode_5fwriter',['RingIO_MODE_WRITER',['../_ring_i_o_8h.html#ade2943110d698cdaf160762aa46c12afae5bd86863699a3486f8ae23b10e3b17a',1,'RingIO.h']]],
  ['ringio_5fneed_5fexact_5fsize',['RingIO_NEED_EXACT_SIZE',['../_ring_i_o_8h.html#a03c43c9ea543bfedbcb7af2318cd90e2a715bd8bc3cae827b30c7b5b514872699',1,'RingIO.h']]],
  ['ringio_5fnotification_5falways',['RingIO_NOTIFICATION_ALWAYS',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512aa69da6141eebd93c6884d76f35731cfd',1,'RingIO.h']]],
  ['ringio_5fnotification_5fhdwrfifo_5falways',['RingIO_NOTIFICATION_HDWRFIFO_ALWAYS',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512a48759b1e28965bf62dc8b4e1be9f1731',1,'RingIO.h']]],
  ['ringio_5fnotification_5fhdwrfifo_5fonce',['RingIO_NOTIFICATION_HDWRFIFO_ONCE',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512a71fbbc50a29bb516271d16853c0a5ea8',1,'RingIO.h']]],
  ['ringio_5fnotification_5fnone',['RingIO_NOTIFICATION_NONE',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512ab46646b73b1991c99a33ebfff81df127',1,'RingIO.h']]],
  ['ringio_5fnotification_5fonce',['RingIO_NOTIFICATION_ONCE',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512a1a74f19cd78c92f11b6c1c0146776176',1,'RingIO.h']]],
  ['ringio_5ftype_5fsharedmem',['RingIO_Type_SHAREDMEM',['../_ring_i_o_8h.html#a405eecc38425e76968873bda24e55386af94714dbc3b1a3aaa6848e9ef4f2cfe2',1,'RingIO.h']]]
];
