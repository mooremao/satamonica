var searchData=
[
  ['cache_5fmode_5fbypass',['Cache_Mode_BYPASS',['../_cache_8h.html#ae53d0eaf8524cfd3070610dae3678a75a38897fd9bf9f205635e09995bfbf0738',1,'Cache.h']]],
  ['cache_5fmode_5ffreeze',['Cache_Mode_FREEZE',['../_cache_8h.html#ae53d0eaf8524cfd3070610dae3678a75a97b8325d18207a6a394b9cb825a4cde1',1,'Cache.h']]],
  ['cache_5fmode_5fnormal',['Cache_Mode_NORMAL',['../_cache_8h.html#ae53d0eaf8524cfd3070610dae3678a75a0f8f8773ad047a7109c64f3c92938420',1,'Cache.h']]],
  ['cache_5ftype_5fall',['Cache_Type_ALL',['../_cache_8h.html#a7c5093cfd9e0058581605d93da8c2be3ac450e839b6465b30130be259786bbe2f',1,'Cache.h']]],
  ['cache_5ftype_5fl1',['Cache_Type_L1',['../_cache_8h.html#a7c5093cfd9e0058581605d93da8c2be3a20a5b5885b751e2a93fb0540d6d835d0',1,'Cache.h']]],
  ['cache_5ftype_5fl1d',['Cache_Type_L1D',['../_cache_8h.html#a7c5093cfd9e0058581605d93da8c2be3a077a4622a127cd775ce21848c8cae3b9',1,'Cache.h']]],
  ['cache_5ftype_5fl1p',['Cache_Type_L1P',['../_cache_8h.html#a7c5093cfd9e0058581605d93da8c2be3ab93ffb7eeaa4bcbb861be95af24a43a6',1,'Cache.h']]],
  ['cache_5ftype_5fl2',['Cache_Type_L2',['../_cache_8h.html#a7c5093cfd9e0058581605d93da8c2be3ab4432a98846565b5d063f149a695ce07',1,'Cache.h']]],
  ['cache_5ftype_5fl2d',['Cache_Type_L2D',['../_cache_8h.html#a7c5093cfd9e0058581605d93da8c2be3ae055ce904f950e3443731131380e7bb5',1,'Cache.h']]],
  ['cache_5ftype_5fl2p',['Cache_Type_L2P',['../_cache_8h.html#a7c5093cfd9e0058581605d93da8c2be3ad829968b003616538cb1b2d39c4b7af6',1,'Cache.h']]]
];
