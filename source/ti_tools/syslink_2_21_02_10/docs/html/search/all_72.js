var searchData=
[
  ['refcount',['refCount',['../struct_proc_mgr___addr_info__tag.html#a2cb7a0bb948b70fa1554c01607628df6',1,'ProcMgr_AddrInfo_tag']]],
  ['regionid',['regionId',['../struct_gate_m_p___params.html#a5af9a96f1fcf6f29b69c07207a33d453',1,'GateMP_Params::regionId()'],['../struct_heap_buf_m_p___params.html#a22f33c326f94898613b8d40335be4eee',1,'HeapBufMP_Params::regionId()'],['../struct_heap_mem_m_p___params.html#ad1a5b94d015d0b5b75f091ca9be8a04e',1,'HeapMemMP_Params::regionId()'],['../struct_list_m_p___params.html#af812a3b0ea4698b841bfbf83eada903a',1,'ListMP_Params::regionId()'],['../struct_frame_q___sh_mem___params__tag.html#a414846d369bbe1c7454e43ea9875ffed',1,'FrameQ_ShMem_Params_tag::regionId()'],['../struct_frame_q_buf_mgr___sh_mem___params__tag.html#ae96ccd5bb7926660d167a96d575ad877',1,'FrameQBufMgr_ShMem_Params_tag::regionId()']]],
  ['remoteprocid',['remoteProcId',['../struct_ring_i_o_shm___params___tag.html#a2517878d7fa7b2309db59453c2b87705',1,'RingIOShm_Params_Tag']]],
  ['remoteprotect',['remoteProtect',['../struct_gate_m_p___params.html#a32a1870109d6aa6f583932aff19ebb50',1,'GateMP_Params::remoteProtect()'],['../struct_frame_q___sh_mem___params__tag.html#a805c9885e808828811b7f9f4af3cd51c',1,'FrameQ_ShMem_Params_tag::remoteProtect()'],['../struct_frame_q_buf_mgr___sh_mem___params__tag.html#a80720e6fdf5c5fb8b87433a534450cf7',1,'FrameQBufMgr_ShMem_Params_tag::remoteProtect()'],['../struct_ring_i_o_shm___params___tag.html#a0a2bfc144955bbc99fd04234de8444df',1,'RingIOShm_Params_Tag::remoteProtect()']]],
  ['replyid',['replyId',['../struct_message_q___msg_header.html#ac933455354a8b5735404cbc81147dd06',1,'MessageQ_MsgHeader']]],
  ['replyproc',['replyProc',['../struct_message_q___msg_header.html#a9c6cbc63347f25ea6cc47338d570cb48',1,'MessageQ_MsgHeader']]],
  ['reserved',['reserved',['../struct_message_q___msg_header.html#a66d4a1e8d4a20da8bad4feca1c710e53',1,'MessageQ_MsgHeader::reserved()'],['../struct_proc_mgr___start_params__tag.html#a2428841b79140fec9d07e095ec15b25f',1,'ProcMgr_StartParams_tag::reserved()'],['../struct_gate_process___params.html#af97ede282482561ddc9701af4d91ed3a',1,'GateProcess_Params::reserved()']]],
  ['reserved0',['reserved0',['../struct_message_q___msg_header.html#aa8269e435d09e486b167c0b6f9cc90b0',1,'MessageQ_MsgHeader::reserved0()'],['../struct_frame___frame_header___tag.html#afee57277be4a69238f9856d19ad927c0',1,'Frame_FrameHeader_Tag::reserved0()']]],
  ['reserved1',['reserved1',['../struct_message_q___msg_header.html#a1e12c3020f3f175128bf2ae4c5d1df7c',1,'MessageQ_MsgHeader::reserved1()'],['../struct_frame___frame_header___tag.html#a48c962e25f65ef1a750c89aa0cee6974',1,'Frame_FrameHeader_Tag::reserved1()']]],
  ['restrack_2eh',['ResTrack.h',['../_res_track_8h.html',1,'']]],
  ['restrack_5fcreate',['ResTrack_create',['../_res_track_8h.html#aaceea21ac9fee3f3860056e4afbcf5a1',1,'ResTrack.h']]],
  ['restrack_5fdelete',['ResTrack_delete',['../_res_track_8h.html#af6cde6756b24165092920c28faa73fba',1,'ResTrack.h']]],
  ['restrack_5fe_5ffail',['ResTrack_E_FAIL',['../_res_track_8h.html#af1599596181784cd3e4e5658bb2705f3',1,'ResTrack.h']]],
  ['restrack_5fe_5fmemory',['ResTrack_E_MEMORY',['../_res_track_8h.html#aa1c0f52ec44bac3a660da178deb32014',1,'ResTrack.h']]],
  ['restrack_5fe_5fnotfound',['ResTrack_E_NOTFOUND',['../_res_track_8h.html#aec417aee455a81055315582ec7fc8e68',1,'ResTrack.h']]],
  ['restrack_5fe_5fpid',['ResTrack_E_PID',['../_res_track_8h.html#a919a5fa7954d3b0852ce7eb6bd27ac5a',1,'ResTrack.h']]],
  ['restrack_5ffxn',['ResTrack_Fxn',['../_res_track_8h.html#aee5760100874a14b477dd7a2c2f8448e',1,'ResTrack.h']]],
  ['restrack_5fhandle',['ResTrack_Handle',['../_res_track_8h.html#ab0e27a75ee6f434bc93cb349295dca5e',1,'ResTrack.h']]],
  ['restrack_5fparams',['ResTrack_Params',['../struct_res_track___params.html',1,'']]],
  ['restrack_5fparams_5finit',['ResTrack_Params_init',['../_res_track_8h.html#a628ac88ad735475be5fab8a7466530a1',1,'ResTrack.h']]],
  ['restrack_5fpop',['ResTrack_pop',['../_res_track_8h.html#ab84340380c7ab2c5a0d1a70aba059f6f',1,'ResTrack.h']]],
  ['restrack_5fpush',['ResTrack_push',['../_res_track_8h.html#a19b8fa3487d9489878501bb66bca9c34',1,'ResTrack.h']]],
  ['restrack_5fregister',['ResTrack_register',['../_res_track_8h.html#a5698ef7fce2c5a3b7da90768ff16e7f1',1,'ResTrack.h']]],
  ['restrack_5fremove',['ResTrack_remove',['../_res_track_8h.html#ad0db7890961fcda47e3a6dd945d70015',1,'ResTrack.h']]],
  ['restrack_5fs_5fsuccess',['ResTrack_S_SUCCESS',['../_res_track_8h.html#ae717c33cec9f3d603c430a16e4778984',1,'ResTrack.h']]],
  ['restrack_5funregister',['ResTrack_unregister',['../_res_track_8h.html#a2811d746263633806d99da4b4f2d2b90',1,'ResTrack.h']]],
  ['ringio_2eh',['RingIO.h',['../_ring_i_o_8h.html',1,'']]],
  ['ringio_5facquire',['RingIO_acquire',['../_ring_i_o_8h.html#af699ee3375b21bc2d8a79b25063c080c',1,'RingIO.h']]],
  ['ringio_5fbufptr',['RingIO_BufPtr',['../_ring_i_o_8h.html#a66039fc2dccee9dec03b87601968aba5',1,'RingIO.h']]],
  ['ringio_5fcancel',['RingIO_cancel',['../_ring_i_o_8h.html#aef78642a097433fc89ea7429fdd11b8b',1,'RingIO.h']]],
  ['ringio_5fclose',['RingIO_close',['../_ring_i_o_8h.html#ae5d596df2da6a1f573e91884bc946e15',1,'RingIO.h']]],
  ['ringio_5fcreate',['RingIO_create',['../_ring_i_o_8h.html#aaaa3d4c44c24e832caf3173f28d56543',1,'RingIO.h']]],
  ['ringio_5fdatabuf_5fmaintaincache',['RingIO_DATABUF_MAINTAINCACHE',['../_ring_i_o_8h.html#a03c43c9ea543bfedbcb7af2318cd90e2a9158af25eb0eed931c27aa6209620215',1,'RingIO.h']]],
  ['ringio_5fdelete',['RingIO_delete',['../_ring_i_o_8h.html#a0c2021dc279e463d77606b5aea282690',1,'RingIO.h']]],
  ['ringio_5fe_5falreadyexists',['RingIO_E_ALREADYEXISTS',['../_ring_i_o_8h.html#a083cdcdeafcdb9a402adee7d3d64786d',1,'RingIO.h']]],
  ['ringio_5fe_5fbadversion',['RingIO_E_BADVERSION',['../_ring_i_o_8h.html#a66319bce0836fb8dd8b152b83a43f24c',1,'RingIO.h']]],
  ['ringio_5fe_5fbufempty',['RingIO_E_BUFEMPTY',['../_ring_i_o_8h.html#a69aca38b830032e90c6fb10bba038b3a',1,'RingIO.h']]],
  ['ringio_5fe_5fbuffull',['RingIO_E_BUFFULL',['../_ring_i_o_8h.html#a522126c3e1d4c62796d9a0572dee293d',1,'RingIO.h']]],
  ['ringio_5fe_5fbufwrap',['RingIO_E_BUFWRAP',['../_ring_i_o_8h.html#ac70aa3ea830acafd9dae7f2267421fd0',1,'RingIO.h']]],
  ['ringio_5fe_5ffail',['RingIO_E_FAIL',['../_ring_i_o_8h.html#a6878624fb811820d8e70e0c4e1efc42f',1,'RingIO.h']]],
  ['ringio_5fe_5finuse',['RingIO_E_INUSE',['../_ring_i_o_8h.html#afad113c16baa3fcf4abb25e7ebe5ccf9',1,'RingIO.h']]],
  ['ringio_5fe_5finvalidarg',['RingIO_E_INVALIDARG',['../_ring_i_o_8h.html#acf37344841fc49609001c05f46195602',1,'RingIO.h']]],
  ['ringio_5fe_5finvalidcontext',['RingIO_E_INVALIDCONTEXT',['../_ring_i_o_8h.html#a38ae08f11a910b4f40025af5b9cb5ec0',1,'RingIO.h']]],
  ['ringio_5fe_5finvalidheapid',['RingIO_E_INVALIDHEAPID',['../_ring_i_o_8h.html#ab34f93145eb69448e6bc871b023f18b2',1,'RingIO.h']]],
  ['ringio_5fe_5finvalidmsg',['RingIO_E_INVALIDMSG',['../_ring_i_o_8h.html#af3a79638a878949d0d9e8eb5db46ef75',1,'RingIO.h']]],
  ['ringio_5fe_5finvalidprocid',['RingIO_E_INVALIDPROCID',['../_ring_i_o_8h.html#a846bbda2f4b7d82a15ddb1ef6442862a',1,'RingIO.h']]],
  ['ringio_5fe_5finvalidstate',['RingIO_E_INVALIDSTATE',['../_ring_i_o_8h.html#a43c718204a218e0241cb364a92dc05be',1,'RingIO.h']]],
  ['ringio_5fe_5fmaxreached',['RingIO_E_MAXREACHED',['../_ring_i_o_8h.html#af4b0a6c01045c0040cbc825ef79c38e7',1,'RingIO.h']]],
  ['ringio_5fe_5fmemory',['RingIO_E_MEMORY',['../_ring_i_o_8h.html#a46bdd290bdba27644142b9c51051eca4',1,'RingIO.h']]],
  ['ringio_5fe_5fnotfound',['RingIO_E_NOTFOUND',['../_ring_i_o_8h.html#aee8a61e6b8e88cdbb0da7c58683504a2',1,'RingIO.h']]],
  ['ringio_5fe_5fnotifyfail',['RingIO_E_NOTIFYFAIL',['../_ring_i_o_8h.html#a746b3a907620372d59bd8a48b0f45b96',1,'RingIO.h']]],
  ['ringio_5fe_5fnotowner',['RingIO_E_NOTOWNER',['../_ring_i_o_8h.html#a26b49f12556bfc8221def1a5ce0b45b0',1,'RingIO.h']]],
  ['ringio_5fe_5fosfailure',['RingIO_E_OSFAILURE',['../_ring_i_o_8h.html#a75ed8794d4810b57bbe36f36023bced9',1,'RingIO.h']]],
  ['ringio_5fe_5fpendingattribute',['RingIO_E_PENDINGATTRIBUTE',['../_ring_i_o_8h.html#a796b5ed26ddafa69ca64cb1a5024885f',1,'RingIO.h']]],
  ['ringio_5fe_5fpendingdata',['RingIO_E_PENDINGDATA',['../_ring_i_o_8h.html#a16c1d10aa5e95142797a3ae450921c8d',1,'RingIO.h']]],
  ['ringio_5fe_5fremoteactive',['RingIO_E_REMOTEACTIVE',['../_ring_i_o_8h.html#afc0d04143639d61211054f5806998444',1,'RingIO.h']]],
  ['ringio_5fe_5fresource',['RingIO_E_RESOURCE',['../_ring_i_o_8h.html#ad8eda5cda0eff81ab7f5c5711248874c',1,'RingIO.h']]],
  ['ringio_5fe_5frestart',['RingIO_E_RESTART',['../_ring_i_o_8h.html#aeb4841df1aad72cf6620e2919db9bf93',1,'RingIO.h']]],
  ['ringio_5fe_5ftimeout',['RingIO_E_TIMEOUT',['../_ring_i_o_8h.html#a50bcf58afda4e88c308969f7792ce56d',1,'RingIO.h']]],
  ['ringio_5fe_5fvariableattribute',['RingIO_E_VARIABLEATTRIBUTE',['../_ring_i_o_8h.html#ac1fb7e455a501a9a8c589183a9273696',1,'RingIO.h']]],
  ['ringio_5fe_5fwrongstate',['RingIO_E_WRONGSTATE',['../_ring_i_o_8h.html#af1c6a9b1260cd169aa422d90c4ba5cd3',1,'RingIO.h']]],
  ['ringio_5fflags',['RingIO_Flags',['../_ring_i_o_8h.html#a36e0a36dc4020b8fdea63af56d493e5a',1,'RingIO.h']]],
  ['ringio_5fflags_5ftag',['RingIO_Flags_tag',['../_ring_i_o_8h.html#a03c43c9ea543bfedbcb7af2318cd90e2',1,'RingIO.h']]],
  ['ringio_5fflush',['RingIO_flush',['../_ring_i_o_8h.html#abc9eee7d6623190ff7765aa4c8404156',1,'RingIO.h']]],
  ['ringio_5fgetacquiredoffset',['RingIO_getAcquiredOffset',['../_ring_i_o_8h.html#a9e66c44a7ca26473529a3fcdae45db42',1,'RingIO.h']]],
  ['ringio_5fgetacquiredsize',['RingIO_getAcquiredSize',['../_ring_i_o_8h.html#ae96bdd74e5e568f693f2bd03f768e7f7',1,'RingIO.h']]],
  ['ringio_5fgetattribute',['RingIO_getAttribute',['../_ring_i_o_8h.html#a7daad1746deb7fd187f4bcd26856198f',1,'RingIO.h']]],
  ['ringio_5fgetemptyattrsize',['RingIO_getEmptyAttrSize',['../_ring_i_o_8h.html#a9dbf7dc060f285706210945702bbc432',1,'RingIO.h']]],
  ['ringio_5fgetemptysize',['RingIO_getEmptySize',['../_ring_i_o_8h.html#a008f58fec526550e366f88f3ee9b8c39',1,'RingIO.h']]],
  ['ringio_5fgetvalidattrsize',['RingIO_getValidAttrSize',['../_ring_i_o_8h.html#a28f664e8b8829f96a6a5feb2f0b4c79b',1,'RingIO.h']]],
  ['ringio_5fgetvalidsize',['RingIO_getValidSize',['../_ring_i_o_8h.html#a3c86639d02fdb0e20d2a4f3806edd366',1,'RingIO.h']]],
  ['ringio_5fgetvattribute',['RingIO_getvAttribute',['../_ring_i_o_8h.html#a863c7b9893eabbc0c136c08730fe8b11',1,'RingIO.h']]],
  ['ringio_5fgetwatermark',['RingIO_getWaterMark',['../_ring_i_o_8h.html#a7cb541642b0f3db8d073dac3021ddde5',1,'RingIO.h']]],
  ['ringio_5fhandle',['RingIO_Handle',['../_ring_i_o_8h.html#a48f02425c5f8cb3cd3b7fc9456c821f7',1,'RingIO.h']]],
  ['ringio_5fmode_5freader',['RingIO_MODE_READER',['../_ring_i_o_8h.html#ade2943110d698cdaf160762aa46c12afa7ab53118795e0b4c7b39c267a360b459',1,'RingIO.h']]],
  ['ringio_5fmode_5fwriter',['RingIO_MODE_WRITER',['../_ring_i_o_8h.html#ade2943110d698cdaf160762aa46c12afae5bd86863699a3486f8ae23b10e3b17a',1,'RingIO.h']]],
  ['ringio_5fneed_5fexact_5fsize',['RingIO_NEED_EXACT_SIZE',['../_ring_i_o_8h.html#a03c43c9ea543bfedbcb7af2318cd90e2a715bd8bc3cae827b30c7b5b514872699',1,'RingIO.h']]],
  ['ringio_5fnotification_5falways',['RingIO_NOTIFICATION_ALWAYS',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512aa69da6141eebd93c6884d76f35731cfd',1,'RingIO.h']]],
  ['ringio_5fnotification_5fhdwrfifo_5falways',['RingIO_NOTIFICATION_HDWRFIFO_ALWAYS',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512a48759b1e28965bf62dc8b4e1be9f1731',1,'RingIO.h']]],
  ['ringio_5fnotification_5fhdwrfifo_5fonce',['RingIO_NOTIFICATION_HDWRFIFO_ONCE',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512a71fbbc50a29bb516271d16853c0a5ea8',1,'RingIO.h']]],
  ['ringio_5fnotification_5fnone',['RingIO_NOTIFICATION_NONE',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512ab46646b73b1991c99a33ebfff81df127',1,'RingIO.h']]],
  ['ringio_5fnotification_5fonce',['RingIO_NOTIFICATION_ONCE',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512a1a74f19cd78c92f11b6c1c0146776176',1,'RingIO.h']]],
  ['ringio_5fnotifyfxn',['RingIO_NotifyFxn',['../_ring_i_o_8h.html#a1d7b3ff478f343490ef954a21b34d506',1,'RingIO.h']]],
  ['ringio_5fnotifymsg',['RingIO_NotifyMsg',['../_ring_i_o_8h.html#ad57f867de5e23181c8645334f2e5b62c',1,'RingIO.h']]],
  ['ringio_5fnotifytype',['RingIO_NotifyType',['../_ring_i_o_8h.html#af6539e23f0b67fcf58dd193ff115316c',1,'RingIO.h']]],
  ['ringio_5fnotifytype_5ftag',['RingIO_NotifyType_Tag',['../_ring_i_o_8h.html#a27f3fee178619582326b7faa856a6512',1,'RingIO.h']]],
  ['ringio_5fopen',['RingIO_open',['../_ring_i_o_8h.html#a0e6386f4e55fa2c7b85063c1528d60a9',1,'RingIO.h']]],
  ['ringio_5fopenbyaddr',['RingIO_openByAddr',['../_ring_i_o_8h.html#abdb6c8d32b8938ee735245b6499f60fb',1,'RingIO.h']]],
  ['ringio_5fopenmode',['RingIO_OpenMode',['../_ring_i_o_8h.html#af5d3eacb67d9fa112527fb1c15e921c1',1,'RingIO.h']]],
  ['ringio_5fopenmode_5ftag',['RingIO_OpenMode_Tag',['../_ring_i_o_8h.html#ade2943110d698cdaf160762aa46c12af',1,'RingIO.h']]],
  ['ringio_5fopenparams',['RingIO_openParams',['../_ring_i_o_8h.html#a165d614850131fb308ce64b24da2e4fd',1,'RingIO.h']]],
  ['ringio_5fopenparams_5ftag',['RingIO_openParams_Tag',['../struct_ring_i_o__open_params___tag.html',1,'']]],
  ['ringio_5fparams',['RingIO_Params',['../_ring_i_o_8h.html#a5a3433613c8ea76ac5ffb2a2e4c476aa',1,'RingIO.h']]],
  ['ringio_5fparams_5finit',['RingIO_Params_init',['../_ring_i_o_8h.html#ab8de4e4b144e7ff3eae55693926795c6',1,'RingIO.h']]],
  ['ringio_5fparams_5ftag',['RingIO_Params_Tag',['../struct_ring_i_o___params___tag.html',1,'']]],
  ['ringio_5fregisternotifier',['RingIO_registerNotifier',['../_ring_i_o_8h.html#a5f94a9ae4d7a56ceee7fe1a7e65f091d',1,'RingIO.h']]],
  ['ringio_5frelease',['RingIO_release',['../_ring_i_o_8h.html#a727d2e93ed40487ffe0d7a287d7e3eb6',1,'RingIO.h']]],
  ['ringio_5fs_5falreadysetup',['RingIO_S_ALREADYSETUP',['../_ring_i_o_8h.html#a514287d9b48aab8342711c006615b355',1,'RingIO.h']]],
  ['ringio_5fs_5fbusy',['RingIO_S_BUSY',['../_ring_i_o_8h.html#a8ea9c21c2fd993c9bbca33b5518060ab',1,'RingIO.h']]],
  ['ringio_5fs_5fnotcontiguousdata',['RingIO_S_NOTCONTIGUOUSDATA',['../_ring_i_o_8h.html#ac7b9dc9c3756b7a79ed636c6bd436fc9',1,'RingIO.h']]],
  ['ringio_5fs_5fpendingattribute',['RingIO_S_PENDINGATTRIBUTE',['../_ring_i_o_8h.html#ac2fee83299d11a6ec4cb25ac889a16a4',1,'RingIO.h']]],
  ['ringio_5fs_5fsuccess',['RingIO_S_SUCCESS',['../_ring_i_o_8h.html#a70a60bd8ecdb1f5ef51142707b927558',1,'RingIO.h']]],
  ['ringio_5fsendnotify',['RingIO_sendNotify',['../_ring_i_o_8h.html#aaef0b6391a1daeeeab7c5ad5964660ce',1,'RingIO.h']]],
  ['ringio_5fsetattribute',['RingIO_setAttribute',['../_ring_i_o_8h.html#a332c26b37784d51de8c8339a0486431e',1,'RingIO.h']]],
  ['ringio_5fsetnotifytype',['RingIO_setNotifyType',['../_ring_i_o_8h.html#a850609695e520bbda0f2832488dea06f',1,'RingIO.h']]],
  ['ringio_5fsetvattribute',['RingIO_setvAttribute',['../_ring_i_o_8h.html#a0ad3dc0e103c00369ef44aae1bddb52f',1,'RingIO.h']]],
  ['ringio_5fsetwatermark',['RingIO_setWaterMark',['../_ring_i_o_8h.html#a878ca3170af53c94b21d38bfa23f2d33',1,'RingIO.h']]],
  ['ringio_5fsharedmemreq',['RingIO_sharedMemReq',['../_ring_i_o_8h.html#a4787150745c9da5cb7ec542734b148b0',1,'RingIO.h']]],
  ['ringio_5fsharedmemreqdetails',['RingIO_sharedMemReqDetails',['../_ring_i_o_8h.html#a9c9abf7bc815fcc1053ca067be6cf4a7',1,'RingIO.h']]],
  ['ringio_5fsharedmemreqdetails_5ftag',['RingIO_sharedMemReqDetails_tag',['../struct_ring_i_o__shared_mem_req_details__tag.html',1,'']]],
  ['ringio_5ftype',['RingIO_Type',['../_ring_i_o_8h.html#acf1e1a2e8f6d7706e059107865ae5201',1,'RingIO.h']]],
  ['ringio_5ftype_5fsharedmem',['RingIO_Type_SHAREDMEM',['../_ring_i_o_8h.html#a405eecc38425e76968873bda24e55386af94714dbc3b1a3aaa6848e9ef4f2cfe2',1,'RingIO.h']]],
  ['ringio_5ftype_5ftag',['RingIO_Type_Tag',['../_ring_i_o_8h.html#a405eecc38425e76968873bda24e55386',1,'RingIO.h']]],
  ['ringio_5funregisternotifier',['RingIO_unregisterNotifier',['../_ring_i_o_8h.html#a2f8c7dd479c9f7dc7a26c3120ffcf7c4',1,'RingIO.h']]],
  ['ringioshm_2eh',['RingIOShm.h',['../_ring_i_o_shm_8h.html',1,'']]],
  ['ringioshm_5fe_5fbadversion',['RingIOShm_E_BADVERSION',['../_ring_i_o_shm_8h.html#a31c3049d1e520f7c7ae471233f039559',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fbufempty',['RingIOShm_E_BUFEMPTY',['../_ring_i_o_shm_8h.html#a4a110fae669bd81c407107ca567e8bcb',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fbuffull',['RingIOShm_E_BUFFULL',['../_ring_i_o_shm_8h.html#a9da8a2e7d650154a5b3a41605650d757',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fbufwrap',['RingIOShm_E_BUFWRAP',['../_ring_i_o_shm_8h.html#a03526b2a0becd3a710a0cc8c3fcb45f7',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5ffail',['RingIOShm_E_FAIL',['../_ring_i_o_shm_8h.html#aec7bfef010c8bc4bd67bc3de30d623f3',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5finvalidarg',['RingIOShm_E_INVALIDARG',['../_ring_i_o_shm_8h.html#a1d641c04ce9f4912fbf8428c77477811',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5finvalidcontext',['RingIOShm_E_INVALIDCONTEXT',['../_ring_i_o_shm_8h.html#a361c57ed8fdf241f281f6b2ca9511270',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5finvalidprocid',['RingIOShm_E_INVALIDPROCID',['../_ring_i_o_shm_8h.html#a9d5e8edfc91b4d34fc80602d5e885b41',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5finvalidstate',['RingIOShm_E_INVALIDSTATE',['../_ring_i_o_shm_8h.html#aef7fd471b27b84c7ddb3b6a095f39f58',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fmemory',['RingIOShm_E_MEMORY',['../_ring_i_o_shm_8h.html#a8cb932116fd1e79ee0e3ecfe7cbe43ca',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fosfailure',['RingIOShm_E_OSFAILURE',['../_ring_i_o_shm_8h.html#a16092b5a404df126ef7f5e3ca77b0a51',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fpendingdata',['RingIOShm_E_PENDINGDATA',['../_ring_i_o_shm_8h.html#aaa0c2bd81f9281f27fd2fce30fb324c3',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fvariableattribute',['RingIOShm_E_VARIABLEATTRIBUTE',['../_ring_i_o_shm_8h.html#a99edd2adb8cdd483f5fcd5a70d561d66',1,'RingIOShm.h']]],
  ['ringioshm_5fe_5fwrongstate',['RingIOShm_E_WRONGSTATE',['../_ring_i_o_shm_8h.html#a3ced316ff6a2cd57d165296a5f9025f1',1,'RingIOShm.h']]],
  ['ringioshm_5fmoduleid',['RingIOShm_MODULEID',['../_ring_i_o_shm_8h.html#a5cf194bac43fcd0c5f0f6c51f9b29ce0',1,'RingIOShm.h']]],
  ['ringioshm_5fparams',['RingIOShm_Params',['../_ring_i_o_shm_8h.html#a45908a9439bf828253aa08e86036f8a1',1,'RingIOShm.h']]],
  ['ringioshm_5fparams_5finit',['RingIOShm_Params_init',['../_ring_i_o_shm_8h.html#a92b8296f27fb82f02b99b79682437b15',1,'RingIOShm.h']]],
  ['ringioshm_5fparams_5ftag',['RingIOShm_Params_Tag',['../struct_ring_i_o_shm___params___tag.html',1,'']]],
  ['ringioshm_5fs_5falreadysetup',['RingIOShm_S_ALREADYSETUP',['../_ring_i_o_shm_8h.html#a6977b743a1112fc9294290f4b6288d9f',1,'RingIOShm.h']]],
  ['ringioshm_5fs_5fpendingattribute',['RingIOShm_S_PENDINGATTRIBUTE',['../_ring_i_o_shm_8h.html#a895661be9235cf9b5f831eae157174d1',1,'RingIOShm.h']]],
  ['ringioshm_5fs_5fsuccess',['RingIOShm_S_SUCCESS',['../_ring_i_o_shm_8h.html#a3714985ec22abf08f349a21193079741',1,'RingIOShm.h']]]
];
