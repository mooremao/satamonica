var searchData=
[
  ['largestfreesize',['largestFreeSize',['../struct_memory___stats__tag.html#a9662e7dc06bc38f06743939785f386e6',1,'Memory_Stats_tag']]],
  ['leave',['leave',['../struct_i_gate_provider___object.html#a9ad1dcc267f63c018ca069ed80280a08',1,'IGateProvider_Object']]],
  ['len',['len',['../struct_shared_region___entry.html#ae71dcd036cf9b3dbfd8be2541322f64e',1,'SharedRegion_Entry::len()'],['../struct_syslink_mem_mgr___alloc_block__tag.html#a83a298ede31bc83619a3c7e43f66652a',1,'SyslinkMemMgr_AllocBlock_tag::len()']]],
  ['localprotect',['localProtect',['../struct_gate_m_p___params.html#ac080433fff177b3d5b1cd5dcafc9e28c',1,'GateMP_Params::localProtect()'],['../struct_frame_q___sh_mem___params__tag.html#a75f5fc682db0c044feef36386675082f',1,'FrameQ_ShMem_Params_tag::localProtect()'],['../struct_frame_q_buf_mgr___sh_mem___params__tag.html#ae2ab1a4103695e9535b46a5ab91dd0b8',1,'FrameQBufMgr_ShMem_Params_tag::localProtect()'],['../struct_ring_i_o_shm___params___tag.html#a150c67f2e2ee1fa0792a8dfb07cca9c5',1,'RingIOShm_Params_Tag::localProtect()']]]
];
