#!/bin/bash

# still need to build all the user space tools and download firmwares
if [ ! -e wifi-downloaded ]
then

   # For tt build already have a build_wl18xx-tt.sh checked in
   #git clone git://git.ti.com/wilink8-wlan/build-utilites.git
   cd build-utilites || exit 
   ./build_wl18xx-tt.sh init ol_r8.a9.14
  
   # add a patch required for 2.6.37
   cd src/backports
   patch -p1 < ../../../patches/compat-patches/0001-compat-fix-2-6-37.patch || exit 4
   patch -p1 < ../../../patches/compat-patches/0002-allow-wlcore-sdio-to-compile-on-2-6-37.patch
   cd ../.. 
   
   touch ../wifi-downloaded
   cd ..

  
fi
   # just do a build and install wifi drivers

   # In IPNC there is a customised libtoolize in devkit, whilst building wifi drivers
   # copy in one from /usr/bin
   cp ../linux_devkit/bin/libtoolize ../linux_devkit/bin/libtoolize-ipnc 
   cp /usr/bin/libtoolize ../linux_devkit/bin/libtoolize     

   echo "build drivers**************************"
   cd build-utilites
   # do a clean and build
    . ./build_wl18xx-tt.sh clean
   cd ..

   mv ../linux_devkit/bin/libtoolize-ipnc ../linux_devkit/bin/libtoolize 
 
