/*
 *  ======== package.xdc ========
 *
 *! Revision History
 *! ================
 *! 02-Jul-2013 sg      created
 */

/*!
 *  ======== ti.catalog.arm.cortexm4.tiva.l ========
 *  Tiva Low power Embedded device boot time initialization support
 */
package ti.catalog.arm.cortexm4.tiva.l [1,0,0] {
    module Boot;
}
