/*
 *  Copyright (c) 2013 by Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * */

/*
 *  ======== OMAPL138.xdc ========
 *
 */

metaonly module OMAPL138 inherits ITMS320DA8xx
{
}
/*
 *  @(#) ti.catalog.arm; 1, 0, 1,332; 12-20-2013 12:45:42; /db/ztree/library/trees/platform/platform-o37x/src/
 */

