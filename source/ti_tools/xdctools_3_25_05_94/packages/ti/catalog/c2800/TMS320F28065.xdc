/*
 *  Copyright (c) 2013 by Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * */

/*
 *  ======== TMS320F28065.xdc ========
 */
package ti.catalog.c2800;

/*!
 *  ======== TMS320F28065 ========
 *  The F28065 device data sheet module.
 *
 */
metaonly module TMS320F28065 inherits ITMS320F28065
{
}
/*
 *  @(#) ti.catalog.c2800; 1, 0, 0, 0,446; 12-20-2013 12:45:49; /db/ztree/library/trees/platform/platform-o37x/src/
 */

