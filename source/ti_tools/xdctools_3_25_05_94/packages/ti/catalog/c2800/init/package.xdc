/*
 *  Copyright (c) 2013 by Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * */

/*
 *  ======== package.xdc ========
 *
 */

/*!
 *  ======== ti.catalog.c2800.init ========
 *  C28xx boot time initialization support
 */
package ti.catalog.c2800.init[1,0,0] {
    module Boot;
}
/*
 *  @(#) ti.catalog.c2800.init; 1, 0, 0,295; 12-20-2013 12:45:49; /db/ztree/library/trees/platform/platform-o37x/src/
 */

