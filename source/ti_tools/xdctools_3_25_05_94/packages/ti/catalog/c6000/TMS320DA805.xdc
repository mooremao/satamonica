/*
 *  Copyright (c) 2013 by Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * */

/*
 *  ======== TMS320DA805.xdc ========
 *
 */
package ti.catalog.c6000;

/*!
 *  ======== TMS320DA805 ========
 */
metaonly module TMS320DA805 inherits ITMS320DA8xx
{
};
/*
 *  @(#) ti.catalog.c6000; 1, 0, 0, 0,444; 12-20-2013 12:45:54; /db/ztree/library/trees/platform/platform-o37x/src/
 */

