/*
 *  Copyright 2013 by Texas Instruments Incorporated.
 *
 */

/*!
 *  ======== package.xdc ========
 *  Simple TI COFF parser support
 */
package ti.targets.omf.cof {
    module Settings;
};
/*
 *  @(#) ti.targets.omf.cof; 1,0,0,132; 12-20-2013 16:13:27; /db/ztree/library/trees/xdctargets/xdctargets-g41x/src/ xlibrary

 */

