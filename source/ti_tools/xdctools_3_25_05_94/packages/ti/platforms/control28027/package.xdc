/*
 *  Copyright (c) 2013 by Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * */

requires ti.catalog.c2800;
requires xdc.platform [1,0,1];

/*!
 *  ======== ti.platforms.control28027 ========
 *  Platform package for the 28027 control card.
 *
 *  This package implements the interface (xdc.platform.IPlatform)
 *  necessary to build and run executables on the 28027 controlCARD platform.
 */
package ti.platforms.control28027 [1,0,0] {
    module Platform;
}
/*
 *  @(#) ti.platforms.control28027; 1, 0, 0,184; 12-20-2013 12:46:00; /db/ztree/library/trees/platform/platform-o37x/src/
 */

