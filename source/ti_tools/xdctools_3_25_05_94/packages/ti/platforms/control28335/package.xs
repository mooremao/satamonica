/*
 *  Copyright (c) 2013 by Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * */

/*
 *  ======== package.xs ========
 *
 */

/*
 *  ======== Package.getLibs ========
 */
function getLibs(prog)
{
    return (null);    /* no library required for this package */
}
/*
 *  @(#) ti.platforms.control28335; 1, 0, 0,228; 12-20-2013 12:46:02; /db/ztree/library/trees/platform/platform-o37x/src/
 */

