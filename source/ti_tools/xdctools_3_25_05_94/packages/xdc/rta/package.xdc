/* 
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * */
requires xdc.rov;

/*!
 *  ======== xdc.rta ========
 *  @_nodoc
 */
package xdc.rta [1, 0, 0, 0] {

}
/*
 *  @(#) xdc.rta; 1, 0, 0, 0,439; 12-19-2013 19:53:33; /db/ztree/library/trees/xdc/xdc-z63x/src/packages/
 */

