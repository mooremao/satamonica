/* 
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * */
/*!
 *  ======== xdc.tools ========
 *  Command line tool support
 */
package xdc.tools [1, 0, 0, 0] {
    interface ICmd;
    interface IProductView;
    interface IRtscProductTemplate;
    module Cmdr;
    module ResourceBundle;
}
/*
 *  @(#) xdc.tools; 1, 0, 0, 0,450; 12-19-2013 19:54:15; /db/ztree/library/trees/xdc/xdc-z63x/src/packages/
 */

