/*
 *  Copyright 2013 by Texas Instruments Incorporated.
 *
 */

requires xdc.runtime;

/*!
 *  ======== microsoft.targets.arm.rts ========
 *  Runtime support package for the Arm targets for use with Microsoft tools.
 *
 *  This package builds a library of standard XDC system support functions.
 *  In particular, it builds several implementations of the 
 *  xdc.runtime.ISystemProvider interface.  Moreover, this package uses sources
 *  that are part of the xdc.runtime package (to avoid having multiple copies
 *  of the same source in different runtime support packages for different
 *  ISAs.
 */
package microsoft.targets.arm.rts [1,0,0] {
}
/*
 *  @(#) microsoft.targets.arm.rts; 1, 0, 0,181; 12-20-2013 16:12:40; /db/ztree/library/trees/xdctargets/xdctargets-g41x/src/ xlibrary

 */

