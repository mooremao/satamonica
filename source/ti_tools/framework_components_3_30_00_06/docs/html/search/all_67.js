var searchData=
[
  ['gethandle',['getHandle',['../struct_i_r_e_s_m_a_n___fxns.html#ade93bfa0ddc3adb8e4364398b140ff02',1,'IRESMAN_Fxns']]],
  ['getmemrecs',['getMemRecs',['../struct_i_r_e_s_m_a_n___construct_fxns.html#a4ab9b1d3fd768d37f99cad4302f893e1',1,'IRESMAN_ConstructFxns']]],
  ['getname',['getName',['../struct_i_r_e_s_m_a_n___construct_fxns.html#a05e474b72c94eabbe6d0b81c2250c78c',1,'IRESMAN_ConstructFxns']]],
  ['getnummemrecs',['getNumMemRecs',['../struct_i_r_e_s_m_a_n___construct_fxns.html#aea2c0395f0d57386d9d82a7c97d6d907',1,'IRESMAN_ConstructFxns']]],
  ['getprotocolname',['getProtocolName',['../struct_i_r_e_s_m_a_n___fxns.html#ab2d4b4fbc77e7a84549266022b9669a7',1,'IRESMAN_Fxns']]],
  ['getprotocolrevision',['getProtocolRevision',['../struct_i_r_e_s_m_a_n___fxns.html#aeedc2cd2aa193cf9e014db5dbb8611b7',1,'IRESMAN_Fxns']]],
  ['getresourcedescriptors',['getResourceDescriptors',['../struct_i_r_e_s___fxns.html#a7a09d4c2d0b9d6ea00a238434f13a896',1,'IRES_Fxns']]],
  ['getrevision',['getRevision',['../struct_i_r_e_s_m_a_n___construct_fxns.html#aea8d658a3140f2a7ef9c7bc5e1d7dd77',1,'IRESMAN_ConstructFxns']]],
  ['getscratchidfxn',['getScratchIdFxn',['../struct_e_c_p_y___cfg_params.html#a0493036e00791d10fd0e31ee1bd4d85e',1,'ECPY_CfgParams']]],
  ['getstaticproperties',['getStaticProperties',['../struct_i_r_e_s___obj.html#a6f851ab721d73bb546632ca59176fcc9',1,'IRES_Obj']]],
  ['globalregs',['globalRegs',['../struct_i_r_e_s___e_d_m_a3_c_h_a_n___properties.html#a617dc2263a7b5e3ce910840494e1bd1e',1,'IRES_EDMA3CHAN_Properties::globalRegs()'],['../struct_i_r_e_s___h_d_v_i_c_p___properties.html#aac58b5a8cbb86d0c5f0ff397d795fb93',1,'IRES_HDVICP_Properties::globalRegs()'],['../struct_i_r_e_s___h_d_v_i_c_p2___properties.html#ab0ef58b1868d8263b659f77b5ada49d7',1,'IRES_HDVICP2_Properties::globalRegs()'],['../struct_i_r_e_s___v_i_c_p___properties.html#a6de89d575cca5a32a08ad0e3534047d1',1,'IRES_VICP_Properties::globalRegs()']]]
];
