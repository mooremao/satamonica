
ifndef CC
   CC = gcc
endif

ifndef AR
   AR = ar
endif

ifndef BLUETOPIA_PATH
   BLUETOPIA_PATH = ../../../Bluetopia
endif

INCLDDIRS = -I../../include                                    \
	    -I../../include/server                             \
	    -I../../modules/btpmaudm/server                    \
	    -I../../modules/btpmbasm/server                    \
	    -I../../modules/btpmfmpm/server                    \
	    -I../../modules/btpmftpm/server                    \
	    -I../../modules/btpmhogm/server                    \
	    -I../../modules/btpmhdpm/server                    \
	    -I../../modules/btpmhfrm/server                    \
	    -I../../modules/btpmhrpm/server                    \
	    -I../../modules/btpmhidm/server                    \
	    -I../../modules/btpmmapm/server                    \
	    -I../../modules/btpmoppm/server                    \
	    -I../../modules/btpmpanm/server                    \
	    -I../../modules/btpmpbam/server                    \
	    -I../../modules/btpmpxpm/server                    \
	    -I$(BLUETOPIA_PATH)/include                        \
	    -I$(BLUETOPIA_PATH)/profiles/A2DP/include     \
	    -I$(BLUETOPIA_PATH)/profiles/Audio/include    \
	    -I$(BLUETOPIA_PATH)/profiles/AVRCP/include    \
	    -I$(BLUETOPIA_PATH)/profiles/AVCTP/include    \
	    -I$(BLUETOPIA_PATH)/profiles/GATT/include     \
	    -I$(BLUETOPIA_PATH)/SBC/include               \
	    -I$(BLUETOPIA_PATH)/profiles/HDP/include      \
	    -I$(BLUETOPIA_PATH)/profiles/HDSET/include    \
	    -I$(BLUETOPIA_PATH)/profiles/HFRE/include     \
	    -I$(BLUETOPIA_PATH)/profiles/MAP/include      \
	    -I$(BLUETOPIA_PATH)/profiles/OPP/include      \
	    -I$(BLUETOPIA_PATH)/profiles/PAN/include      \
	    -I$(BLUETOPIA_PATH)/profiles/PBAP/include     \
	    -I$(BLUETOPIA_PATH)/profiles/HID/include      \
	    -I$(BLUETOPIA_PATH)/profiles/HID_Host/include 

CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDLIBS = -lpthread

OBJS = BTPMMODC.o

.PHONY:
all: libBTPMMODC.a

libBTPMMODC.a: $(OBJS)
	$(AR) r $@ $?

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm *.o
	-rm libBTPMMODC.a

