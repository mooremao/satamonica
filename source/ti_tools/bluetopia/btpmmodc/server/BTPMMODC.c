/*****< btpmmodc.c >***********************************************************/
/*      Copyright 2010 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTPMMOD - Installable Module Handler for Stonestreet One Bluetooth        */
/*            Protocol Stack Platform Manager.                                */
/*                                                                            */
/*  Author:  Damon Lange                                                      */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   06/12/10  D. Lange       Initial creation.                               */
/******************************************************************************/

#include "SS1BTPM.h"             /* BTPM API Prototypes and Constants.        */

#include "BTPMMODC.h"            /* BTPM Module Handler List.                 */

#include "SS1BTAUDM.h"           /* Audio Manager Module.                     */
#include "SS1BTBASM.h"           /* Battery Service Manager Module.           */
#include "SS1BTFMPM.h"           /* Find Me Manager Module.                   */
#include "SS1BTFTPM.h"           /* File Transfer Manager Module.             */
#include "SS1BTHDPM.h"           /* Health Device Manager Module.             */
#include "SS1BTHFRM.h"           /* Hands Free Manager Module.                */
#include "SS1BTHIDM.h"           /* HID Host Manager Module.                  */
#include "SS1BTHOGM.h"           /* HID over GATT Manager Module.             */
#include "SS1BTHRPM.h"           /* Heart Rate Manager Module.                */
#include "SS1BTOPPM.h"           /* Object Push Manager Module.               */
#include "SS1BTMAPM.h"           /* Message Access Manager Module.            */
#include "SS1BTPANM.h"           /* PAN Manager Module.                       */
#include "SS1BTPBAM.h"           /* Phone Book Access Host Manager Module.    */
#include "SS1BTPXPM.h"           /* Proximity Manager Module.                 */
#include "SS1BTCSCM.h"           /* Cycling Speed and Cadence Manager Module. */

   /* Internal Variables to this Module (Remember that all variables    */
   /* declared static are initialized to 0 automatically by the         */
   /* compiler as part of standard C/C++).                              */

   /* Audio Manager default initialization.                             */
   /*   - A2DP SRC AND AVRCP Support.                                   */
static AUD_Stream_Initialization_Info_t SRCInitializationInfo =
{
   0,
   "A2DP Source",

#if BTPS_CONFIGURATION_AUD_SUPPORT_AAC

   8,
   {
      /* The supported AAC Stream Formats.                              */
      { 44100, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },
      { 48000, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },
      { 44100, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },
      { 48000, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },

#else

   4,
   {

#endif 

      /* The supported SBC Stream Formats.                              */
      { 44100, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC },
      { 48000, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC },
      { 44100, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC },
      { 48000, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC }
   },
   0
} ;

   /* Audio Manager default initialization.                             */
   /*   - A2DP SNK AND AVRCP Support.                                   */
static AUD_Stream_Initialization_Info_t SNKInitializationInfo =
{
   0,
   "A2DP Sink",

#if BTPS_CONFIGURATION_AUD_SUPPORT_AAC

   8,
   {
      /* The supported AAC Stream Formats.                              */
      { 44100, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },
      { 48000, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },
      { 44100, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },
      { 48000, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_AAC | AUD_STREAM_FORMAT_FLAGS_AAC_SUPPORT_VBR },

#else

   4,
   {

#endif 

      /* The supported SBC Stream Formats.                              */
      { 44100, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC },
      { 48000, 2, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC },
      { 44100, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC },
      { 48000, 1, AUD_STREAM_FORMAT_FLAGS_CODEC_TYPE_SBC }
   },
   0
} ;

   /* Specify the AVRCP Controller Role Information.                    */
static AUD_Remote_Control_Role_Info_t ControllerRemoteControlRoleInfo =
{
   SDP_AVRCP_SUPPORTED_FEATURES_CONTROLLER_CATEGORY_1 | SDP_AVRCP_SUPPORTED_FEATURES_CONTROLLER_CATEGORY_2,
   "Stonestreet One",
   "AVRCP Controller"
} ;

   /* Specify the AVRCP Target Role Information.                        */
static AUD_Remote_Control_Role_Info_t TargetRemoteControlRoleInfo =
{
   SDP_AVRCP_SUPPORTED_FEATURES_TARGET_CATEGORY_1 | SDP_AVRCP_SUPPORTED_FEATURES_CONTROLLER_CATEGORY_2,
   "Stonestreet One",
   "AVRCP Target"
} ;

   /* Specify the AVRCP Initialization Information.                     */
static AUD_Remote_Control_Initialization_Info_t RemoteControlInitializationInfo =
{
   0,
   apvVersion1_4,
   &ControllerRemoteControlRoleInfo,
   &TargetRemoteControlRoleInfo
} ;

   /* Audio Manager container initialization.                           */
   /* Specify support for A2DP SRC, A2DP SNK, AVRCP CT, and AVRCP TG.   */
static AUDM_Initialization_Data_t AudioInitializationInfo =
{
   AUDM_INCOMING_CONNECTION_FLAGS_REQUIRE_ENCRYPTION,
   0,
   &SRCInitializationInfo,
   &SNKInitializationInfo,
   &RemoteControlInitializationInfo
} ;

   /* Health Device Profile initialization.                             */
static HDPM_Initialization_Info_t HDPMInitializationInfo =
{
   "Health Device Service",
   "Stonestreet One"
} ;


   /* Hands Free Manager default initialization.                        */
   /*   - Audio Gateway Role Support.                                   */
static HFRM_Initialization_Data_t HFRMInitializationData_AG =
{
   "Hands Free Audio Gateway",
   3,
   (HFRM_INCOMING_CONNECTION_FLAGS_REQUIRE_AUTHENTICATION | HFRM_INCOMING_CONNECTION_FLAGS_REQUIRE_ENCRYPTION),
   (HFRE_THREE_WAY_CALLING_SUPPORTED_BIT | HFRE_AG_SOUND_ENHANCEMENT_SUPPORTED_BIT | HFRE_REJECT_CALL_SUPPORT_BIT | HFRE_AG_ENHANCED_CALL_STATUS_SUPPORTED_BIT | HFRE_AG_EXTENDED_ERROR_RESULT_CODES_SUPPORTED_BIT | HFRE_AG_VOICE_RECOGNITION_SUPPORTED_BIT),
   (HFRE_RELEASE_ALL_HELD_CALLS | HFRE_RELEASE_ALL_ACTIVE_CALLS_ACCEPT_WAITING_CALL | HFRE_PLACE_ALL_ACTIVE_CALLS_ON_HOLD_ACCEPT_THE_OTHER | HFRE_ADD_A_HELD_CALL_TO_CONVERSATION | HFRE_CONNECT_TWO_CALLS_DISCONNECT_SUBSCRIBER),
   HFRE_NETWORK_TYPE_ABILITY_TO_REJECT_CALLS,
   0,
   NULL
} ;

   /* Hands Free Manager default initialization.                        */
   /*   - Hands Free Role Support.                                      */
static HFRM_Initialization_Data_t HFRMInitializationData_HF =
{
   "Hands Free",
   1,
   HFRM_INCOMING_CONNECTION_FLAGS_REQUIRE_ENCRYPTION,
   (HFRE_HF_SOUND_ENHANCEMENT_SUPPORTED_BIT | HFRE_CALL_WAITING_THREE_WAY_CALLING_SUPPORTED_BIT | HFRE_CLI_SUPPORTED_BIT | HFRE_HF_VOICE_RECOGNITION_SUPPORTED_BIT | HFRE_REMOTE_VOLUME_CONTROL_SUPPORTED_BIT | HFRE_HF_ENHANCED_CALL_STATUS_SUPPORTED_BIT | HFRE_HF_ENHANCED_CALL_CONTROL_SUPPORTED_BIT),
   (HFRE_RELEASE_ALL_ACTIVE_CALLS_ACCEPT_WAITING_CALL | HFRE_PLACE_ALL_ACTIVE_CALLS_ON_HOLD_ACCEPT_THE_OTHER),
   0,
   0,
   NULL
} ;

   /* Hands Free Manager container initialization.                      */
static HFRM_Initialization_Info_t HFRMInitializationInfo =
{
   &HFRMInitializationData_AG,
   &HFRMInitializationData_HF
} ;   
/* The following function is responsible for initializing/cleaning up*/
/* the Bluetopia Platform Manager CSC Manager Module.  This function */
/* should be registered with the Bluetopia Platform Manager Module   */
/* Handler and will be called when the Platform Manager is           */
/* initialized (or shut down).                                       */
void BTPSAPI CSCM_InitializationHandlerFunction(Boolean_t Initialize, void *InitializationData);
void BTPSAPI CSCM_DeviceManagerHandlerFunction(DEVM_Event_Data_t *EventData);

#if 0
   /* Headset Manager default initialization.                           */
   /*   - Audio Gateway Role Support.                                   */
static HDSM_Initialization_Data_t HDSMInitializationData_AG =
{
   "Headset - AG",
   4,
   HDSM_INCOMING_CONNECTION_FLAGS_REQUIRE_ENCRYPTION,
   0
};

   /* Headset Manager default initialization.                           */
   /*   - Headset Role Support.                                         */
static HDSM_Initialization_Data_t HDSMInitializationData_Headset =
{
   "Headset",
   2,
   HDSM_INCOMING_CONNECTION_FLAGS_REQUIRE_ENCRYPTION,
   HDSM_SUPPORTED_FEATURES_MASK_HEADSET_SUPPORTS_REMOTE_AUDIO_VOLUME_CONTROLS
};

   /* Head Set Manager container initialization.                      */
static HDSM_Initialization_Info_t HDSMInitializationInfo =
{
   &HDSMInitializationData_AG,
   &HDSMInitializationData_Headset
} ;

#endif
   /* HOG Manager container initialization.                             */
static HOGM_Initialization_Data_t HOGMInitializationInfo =
{
   HOGM_SUPPORTED_FEATURES_FLAGS_REPORT_MODE
} ;

   /* Message Access Profile (MAP) default initialization.              */
static MAPM_Initialization_Data_t MAPMInitializationData =
{
   0,
   "MAP Notification Server"
} ;

   /* PAN Manager default initialization.                               */
   /*    - PAN User Service (naming data).                              */
static PANM_Naming_Data_t NamingData_User =
{
   "PAN-U Service",
   "PAN User"
} ;

   /* PAN Manager default initialization.                               */
   /*    - PAN Group Ad-Hoc Service (naming data).                      */
static PANM_Naming_Data_t NamingData_GroupAdHoc =
{
   "PAN-GRP Service",
   "PAN Group Ad-Hoc"
} ;

   /* PAN Manager default initialization.                               */
   /*    - PAN Network Access Point Service (naming data).              */
static PANM_Naming_Data_t NamingData_AccessPoint =
{
   "PAN-NAP Service",
   "PAN Network Access Point"
} ;

   /* PAN Manager default initialization.                               */
   /*    - Network Packet Type List (IPV4/ARP).                         */
static Word_t NetworkPacketTypeList[] =
{
   0x0800,
   0x0806
} ;

   /* PAN Manager container initialization.                             */
static PANM_Initialization_Info_t PANMInitializationInfo =
{
   (PAN_PERSONAL_AREA_NETWORK_USER_SERVICE | PAN_GROUP_ADHOC_NETWORK_SERVICE | PAN_NETWORK_ACCESS_POINT_SERVICE),
   0,
   &NamingData_User,
   &NamingData_AccessPoint,
   &NamingData_GroupAdHoc,
   1,
   NetworkPacketTypeList,
   PAN_SECURITY_DESCRIPTION_SERVICE_LEVEL_ENFORCED_SECURITY,
   PAN_NETWORK_ACCESS_TYPE_GSM,
   0,
   (PANM_INCOMING_CONNECTION_FLAGS_REQUIRE_AUTHENTICATION | PANM_INCOMING_CONNECTION_FLAGS_REQUIRE_ENCRYPTION)
} ;

   /* PXP Manager container initialization.                             */
static PXPM_Initialization_Info_t PXPMInitializationInfo =
{
   PXPM_CONFIGURATION_DEFAULT_REFRESH_TIME,
   PXPM_CONFIGURATION_DEFAULT_ALERT_LEVEL,
   PXPM_CONFIGURATION_DEFAULT_PATH_LOSS_THRESHOLD
} ;

   /* Main Module list that contains all configured modules.            */
static MOD_ModuleHandlerEntry_t ModuleHandlerList[] =
{
   { AUDM_InitializationHandlerFunction, (void *)(&AudioInitializationInfo), AUDM_DeviceManagerHandlerFunction },
   { BASM_InitializationHandlerFunction, NULL,                               BASM_DeviceManagerHandlerFunction },
   { FMPM_InitializationHandlerFunction, NULL,                               FMPM_DeviceManagerHandlerFunction },
   { FTPM_InitializationHandlerFunction, NULL,                               FTPM_DeviceManagerHandlerFunction },
   { HDPM_InitializationHandlerFunction, (void *)(&HDPMInitializationInfo),  HDPM_DeviceManagerHandlerFunction },
   { HFRM_InitializationHandlerFunction, (void *)(&HFRMInitializationInfo),  HFRM_DeviceManagerHandlerFunction },
   { HIDM_InitializationHandlerFunction, NULL,                               HIDM_DeviceManagerHandlerFunction },
   { HOGM_InitializationHandlerFunction, (void *)(&HOGMInitializationInfo),  HOGM_DeviceManagerHandlerFunction },
   { HRPM_InitializationHandlerFunction, NULL,                               HRPM_DeviceManagerHandlerFunction },
   { CSCM_InitializationHandlerFunction, NULL,                               CSCM_DeviceManagerHandlerFunction },
   { MAPM_InitializationHandlerFunction, (void *)(&MAPMInitializationData),  MAPM_DeviceManagerHandlerFunction },
   { OPPM_InitializationHandlerFunction, NULL,                               OPPM_DeviceManagerHandlerFunction },
   { PANM_InitializationHandlerFunction, (void *)(&PANMInitializationInfo),  PANM_DeviceManagerHandlerFunction },
   { PBAM_InitializationHandlerFunction, NULL,                               PBAM_DeviceManagerHandlerFunction },
   { PXPM_InitializationHandlerFunction, (void *)(&PXPMInitializationInfo),  PXPM_DeviceManagerHandlerFunction },
   { NULL                              , NULL,                               NULL                              }
} ;

   /* The following function is responsible for initializing the        */
   /* Bluetopia Platform Manager Module Handler Service.  This function */
   /* returns a pointer to a Module Handler Entry List (or NULL if there*/
   /* were no Modules installed).  The returned list will simply be an  */
   /* array of Module Handler Entries, with the last entry in the list  */
   /* signified by an entry that has NULL present for all Module Handler*/
   /* Functions.                                                        */
MOD_ModuleHandlerEntry_t *BTPSAPI MOD_GetModuleList(void)
{
   MOD_ModuleHandlerEntry_t *ret_val;

   DebugPrint((BTPM_DEBUG_ZONE_MODULE_MANAGER | BTPM_DEBUG_LEVEL_FUNCTION), ("Enter\n"));

   ret_val = ModuleHandlerList;

   DebugPrint((BTPM_DEBUG_ZONE_MODULE_MANAGER | BTPM_DEBUG_LEVEL_FUNCTION), ("Exit: 0x%08X\n", ret_val));

   /* Simply return the Module Handler List.                            */
   return(ret_val);
}

