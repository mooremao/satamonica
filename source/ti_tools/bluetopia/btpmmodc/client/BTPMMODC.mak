
ifndef CC
   CC = gcc
endif

ifndef AR
   AR = ar
endif

ifndef BLUETOPIA_PATH
   BLUETOPIA_PATH = ../../../Bluetopia
endif

INCLDDIRS = -I../../include                               \
	    -I../../include/client                        \
	    -I../../modules/btpmaudm/client               \
	    -I../../modules/btpmbasm/client               \
	    -I../../modules/btpmfmpm/client               \
	    -I../../modules/btpmftpm/client               \
	    -I../../modules/btpmhdpm/client               \
	    -I../../modules/btpmhogm/client               \
	    -I../../modules/btpmhrpm/client               \
	    -I../../modules/btpmhidm/client               \
	    -I../../modules/btpmmapm/client               \
	    -I../../modules/btpmoppm/client               \
	    -I../../modules/btpmpanm/client               \
	    -I../../modules/btpmpbam/client               \
	    -I../../modules/btpmpxpm/client               \
	    -I$(BLUETOPIA_PATH)/include                   \
	    -I$(BLUETOPIA_PATH)/profiles/A2DP/include     \
	    -I$(BLUETOPIA_PATH)/profiles/Audio/include    \
	    -I$(BLUETOPIA_PATH)/profiles/AVRCP/include    \
	    -I$(BLUETOPIA_PATH)/profiles/AVCTP/include    \
	    -I$(BLUETOPIA_PATH)/profiles/GATT/include     \
	    -I$(BLUETOPIA_PATH)/SBC/include               \
	    -I$(BLUETOPIA_PATH)/profiles/HDP/include      \
	    -I$(BLUETOPIA_PATH)/profiles/MAP/include      \
	    -I$(BLUETOPIA_PATH)/profiles/OPP/include      \
	    -I$(BLUETOPIA_PATH)/profiles/PAN/include      \
	    -I$(BLUETOPIA_PATH)/profiles/PBAP/include     \
	    -I$(BLUETOPIA_PATH)/profiles/HID/include      \
	    -I$(BLUETOPIA_PATH)/profiles/HID_Host/include 

CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDLIBS = -lpthread

OBJS = BTPMMODC.o

.PHONY:
all: libBTPMMODC.a

libBTPMMODC.a: $(OBJS)
	$(AR) r $@ $?

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm *.o
	-rm libBTPMMODC.a

