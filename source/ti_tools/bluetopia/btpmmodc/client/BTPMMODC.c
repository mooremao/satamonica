/*****< btpmmodc.c >***********************************************************/
/*      Copyright 2010 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTPMMOD - Installable Module Handler for Stonestreet One Bluetooth        */
/*            Protocol Stack Platform Manager.                                */
/*                                                                            */
/*  Author:  Damon Lange                                                      */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   06/12/10  D. Lange       Initial creation.                               */
/******************************************************************************/

#include "SS1BTPM.h"             /* BTPM API Prototypes and Constants.        */

#include "BTPMMODC.h"            /* BTPM Module Handler List.                 */

#include "SS1BTAUDM.h"           /* Audio Manager Module.                     */
#include "SS1BTBASM.h"           /* Battery Service Manager Module.           */
#include "SS1BTFMPM.h"           /* Find Me Manager Module.                   */
#include "SS1BTFTPM.h"           /* File Transfer Manager Module.             */
#include "SS1BTHDPM.h"           /* Health Device Manager Module.             */
#include "SS1BTHFRM.h"           /* Hands Free Manager Module.                */
#include "SS1BTHIDM.h"           /* HID Host Manager Module.                  */
#include "SS1BTHOGM.h"           /* HID over GATT Manager Module.             */
#include "SS1BTHRPM.h"           /* Heart Rate Manager Module.                */
#include "SS1BTMAPM.h"           /* Message Access Manager Module.            */
#include "SS1BTOPPM.h"           /* Object Push Manager Module.               */
#include "SS1BTPANM.h"           /* PAN Manager Module.                       */
#include "SS1BTPBAM.h"           /* Phone Book Access Host Manager Module.    */
#include "SS1BTPXPM.h"           /* Proximity Manager Module.                 */
#include "SS1BTCSCM.h"           /* Cycling Speed and Cadence Manager Module. */

   /* Internal Variables to this Module (Remember that all variables    */
   /* declared static are initialized to 0 automatically by the         */
   /* compiler as part of standard C/C++).                              */

   /* Main Module list that contains all configured modules.            */
static MOD_ModuleHandlerEntry_t ModuleHandlerList[] =
{
   { AUDM_InitializationHandlerFunction, NULL, AUDM_DeviceManagerHandlerFunction },
   { BASM_InitializationHandlerFunction, NULL, NULL                              },
   { FMPM_InitializationHandlerFunction, NULL, FMPM_DeviceManagerHandlerFunction },
   { FTPM_InitializationHandlerFunction, NULL, FTPM_DeviceManagerHandlerFunction },
   { HDPM_InitializationHandlerFunction, NULL, HDPM_DeviceManagerHandlerFunction },
   { HFRM_InitializationHandlerFunction, NULL, HFRM_DeviceManagerHandlerFunction },
   { HIDM_InitializationHandlerFunction, NULL, HIDM_DeviceManagerHandlerFunction },
   { HOGM_InitializationHandlerFunction, NULL, HOGM_DeviceManagerHandlerFunction },
   { HRPM_InitializationHandlerFunction, NULL, NULL                              },
   { CSCM_InitializationHandlerFunction, NULL, NULL                              },
   { MAPM_InitializationHandlerFunction, NULL, MAPM_DeviceManagerHandlerFunction },
   { OPPM_InitializationHandlerFunction, NULL, OPPM_DeviceManagerHandlerFunction },
   { PANM_InitializationHandlerFunction, NULL, PANM_DeviceManagerHandlerFunction },
   { PBAM_InitializationHandlerFunction, NULL, PBAM_DeviceManagerHandlerFunction },
   { PXPM_InitializationHandlerFunction, NULL, PXPM_DeviceManagerHandlerFunction },
   { NULL,                               NULL, NULL                              }
} ;

   /* The following function is responsible for initializing the        */
   /* Bluetopia Platform Manager Module Handler Service.  This function */
   /* returns a pointer to a Module Handler Entry List (or NULL if there*/
   /* were no Modules installed).  The returned list will simply be an  */
   /* array of Module Handler Entries, with the last entry in the list  */
   /* signified by an entry that has NULL present for all Module Handler*/
   /* Functions.                                                        */
MOD_ModuleHandlerEntry_t *BTPSAPI MOD_GetModuleList(void)
{
   MOD_ModuleHandlerEntry_t *ret_val;

   DebugPrint((BTPM_DEBUG_ZONE_MODULE_MANAGER | BTPM_DEBUG_LEVEL_FUNCTION), ("Enter"));

   ret_val = ModuleHandlerList;

   DebugPrint((BTPM_DEBUG_ZONE_MODULE_MANAGER | BTPM_DEBUG_LEVEL_FUNCTION), ("Exit: 0x%08X", ret_val));

   /* Simply return the Module Handler List.                            */
   return(ret_val);
}
