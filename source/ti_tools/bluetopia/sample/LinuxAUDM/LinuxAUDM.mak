
ifndef CC
   CC = gcc
endif

ifndef BLUETOPIA_PATH
   BLUETOPIA_PATH = ../../../Bluetopia
endif

INCLDDIRS = -I../../include                            \
	    -I../../include/client                     \
	    -I$(BLUETOPIA_PATH)/include                \
	    -I$(BLUETOPIA_PATH)/profiles/A2DP/include  \
	    -I$(BLUETOPIA_PATH)/profiles/Audio/include \
	    -I$(BLUETOPIA_PATH)/profiles/AVCTP/include \
	    -I$(BLUETOPIA_PATH)/profiles/AVRCP/include \
	    -I$(BLUETOPIA_PATH)/profiles/GAVD/include  \
	    -I$(BLUETOPIA_PATH)/profiles/GATT/include  \
	    -I$(BLUETOPIA_PATH)/SBC/include

ifndef SYSTEMLIBS
   SYSTEMLIBS = -lpthread -lm -lrt
endif
	
CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDFLAGS = -L../../lib/client $(GLOBLDFLAGS)

LDLIBS = -lSS1BTPM $(SYSTEMLIBS) $(GLOBLDLIBS)

OBJS = LinuxAUDM.o AudioEncoderPM.o

ifdef DISABLE_AUDIO_SINK_AUDIO_PROCESSING
   CFLAGS += -DDISABLE_AUDIO_SINK_AUDIO_PROCESSING
else
   OBJS += AudioDecoder.o

   LDLIBS += -lasound
endif

.PHONY:
all: LinuxAUDM

LinuxAUDM: $(OBJS)

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm *.o
	-rm LinuxAUDM

