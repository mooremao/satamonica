/******< audiodecoder.c >******************************************************/
/*      Copyright 2011 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/* AUDIODECODER  - Sample abstraction code to decode an SBC Audio Frame.      */
/*                                                                            */
/*  Author:  Greg Hensley                                                     */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   01/13/11  G. Hensley     Initial creation. (Based on LinuxAUDM)          */
/*   12/04/12  S. Hishmeh     Added ALSA API calls.                           */
/******************************************************************************/
#include "SS1BTAUDM.h"     /* Audio Manager Application Programming Interface.*/
#include "SS1BTPM.h"       /* BTPM Application Programming Interface.         */
#include "AudioDecoder.h"   /* Audio Decoder sample.                          */
#include <alsa/asoundlib.h> /* ALSA Linux Sound Library.                      */
#include <time.h>           /* Linux Time Definitions.                        */
#include <sys/time.h>       /* Linux Time Function Prototypes.                */

   /* The following enumerates the states that the playback state       */
   /* machine may be in.                                                */
typedef enum
{
   asIdle,
   asBuffering,
   asDecoding,
   asPlaying,
   asWaiting
} AudioState_t;

   /* When the incoming SBC buffer gets low or near full the application*/
   /* adds or drops samples to attempt to sustain the buffer.  The      */
   /* following enumeration tracks the state to determine if we are     */
   /* playing continuously, dropping samples, or adding samples.        */
typedef enum
{
   saContinuous,
   saDroppingSamples,
   saAddingSamples
} SampleAdjustment_t;

   /* Constant that defines the size of the buffer to hold the SBC      */
   /* Frames.                                                           */
#define SBC_BUFFER_SIZE                                         (8 * 1024)

   /* Each SBC frame will yield 128 left samples and 128 right samples. */
   /* Setup the buffer to hold 16 decoded SBC frames.                   */
#define NUM_CODEC_BUFFER_SAMPLES                                (256 * 16)

   /* The following defines the number of audio samples that are        */
   /* generated for each SBC frame.                                     */
#define NUM_AUDIO_SAMPLES_PER_SBC_FRAME                                256

   /* The number of bytes in one audio sample.                          */
#define NUM_BYTES_PER_AUDIO_SAMPLE                  sizeof(unsigned short)

   /* The maximum number of supported channels.                         */
#define MAX_NUM_CHANNELS                                                 2

   /* The following defines a constant that is used to control the      */
   /* jitter of audio playback.  The decode routine monitors the amount */
   /* of audio samples in the playback buffer.  If the system decodes   */
   /* samples faster than they playback then the buffer will fill.  If  */
   /* the playback is faster than the decoder then we will run out of   */
   /* samples to play.  The following defines how many time units       */
   /* (frames) of audio will be added or dropped each second to attempt */
   /* to sustain the incoming buffer.                                   */
#define SAMPLE_ADJUSTMENT_RATE                                         200

   /* Constant that defines the size (in bytes) of the SBC Decode       */
   /* Thread.                                                           */
#define DECODE_THREAD_STACK_SIZE                                     16384

   /* The following is used as a printf() replacement.  Uncomment the   */
   /* definition below to enable debug output.                          */
#define Display(_x)              // do { BTPS_OutputMessage _x; } while(0)

   /* The following type definition represents the structure which holds*/
   /* the current audio playback context information.                   */
typedef struct _tagPlaybackContext
{
   unsigned int                BluetoothStackID;

   Decoder_t                   DecoderHandle;
   AUD_Stream_Configuration_t  StreamConfig;
   SBC_Decode_Configuration_t  DecodeConfiguration;

   Mutex_t                     Mutex;
   Event_t                     SBCDecodeEvent;
   Event_t                     ShutdownEvent;
   volatile Boolean_t          Exit;

   AudioState_t                AudioState;
   unsigned int                SBCIn;
   unsigned int                SBCOut;
   unsigned int                SBCEnd;
   unsigned int                SBCFree;
   unsigned int                SBCUsed;
   unsigned char               SBCBuffer[SBC_BUFFER_SIZE];

   unsigned int                DisplayFormatInfo;
   unsigned long               CurrentSampleRate;
   SampleAdjustment_t          SampleAdjustmentState;
   unsigned int                NumFramesAdjustmentLimit;
   unsigned int                NumAdjustmentFrames;
   unsigned int                BufferHighLimit;
   unsigned int                BufferLowLimit;
   unsigned int                BufferStartLimit;

   unsigned short             *PCMBuffer;
   unsigned int                PCMNumUsed;
   unsigned int                PCMNumFree;
   unsigned int                PCMMaxNumSamples;
   unsigned int                MinNumSamplesToSendCodec;
   snd_pcm_t                  *CodecHandle;
   struct timeval              PlaybackStartTime;
} PlaybackContext_t;

   /* Single instance of the Playback Context                           */
static PlaybackContext_t PlaybackContext;

   /* Flag to indicated if the module is initialized                    */
static Boolean_t Initialized = FALSE;

static int Decode(unsigned int DataLength, unsigned char *DataPtr);
static void *BTPSAPI PlaybackThread(void *ThreadParameter);

static int _ConfigureOutputDevice(PlaybackContext_t *Context);
static void DisplayElapsedTime(struct timeval *StartTime);

   /* The following function issued to decode a block of SBC data.  When*/
   /* a full buffer of data is decoded, the data is delivered to the    */
   /* upper layer for playback.                                         */
static int Decode(unsigned int DataLength, unsigned char *DataPtr)
{
   int               ret_val;
   unsigned int      UnusedDataLength;
   SBC_Decode_Data_t DecodedData;
   unsigned int      NumDecodedSamples;
   unsigned int      NumDecodedFrames;
   unsigned int      NumChannels;
   unsigned int      Index;

   /* Note the amount of unprocessed data.                              */
   UnusedDataLength = DataLength;

   /* Verify that the parameters passed in appear valid.                */
   if((DataLength) && (DataPtr))
   {
      while((UnusedDataLength) && (PlaybackContext.PCMNumUsed < PlaybackContext.MinNumSamplesToSendCodec) && (PlaybackContext.PCMNumFree > NUM_AUDIO_SAMPLES_PER_SBC_FRAME))
      {
         /* Initialize the Decode Data structure for this iteration.    */
         DecodedData.LeftChannelDataLength  = 0;
         DecodedData.RightChannelDataLength = 0;
         DecodedData.LeftChannelDataPtr     = &(PlaybackContext.PCMBuffer[PlaybackContext.PCMNumUsed]);
         DecodedData.RightChannelDataPtr    = &(PlaybackContext.PCMBuffer[PlaybackContext.PCMNumUsed + 1]);
         DecodedData.ChannelDataSize        = NUM_AUDIO_SAMPLES_PER_SBC_FRAME;

         /* Pass the SBC data into the Decoder.  If a complete Frame was*/
         /* decoded then we need to write the decoded data to the output*/
         /* file.                                                       */
         ret_val = SBC_Decode_Data(PlaybackContext.DecoderHandle, UnusedDataLength, &DataPtr[(DataLength - UnusedDataLength)], &PlaybackContext.DecodeConfiguration, &DecodedData, &UnusedDataLength);
         if(ret_val == SBC_PROCESSING_COMPLETE)
         {
            if(PlaybackContext.DisplayFormatInfo)
            {
               PlaybackContext.DisplayFormatInfo = 0;

               Display(("Frame Length     : %d\r\n", PlaybackContext.DecodeConfiguration.FrameLength));
               Display(("Bit Pool         : %d\r\n", PlaybackContext.DecodeConfiguration.BitPool));
               Display(("Bit Rate         : %d\r\n", PlaybackContext.DecodeConfiguration.BitRate));
               Display(("Buffer Length    : %d\r\n", DecodedData.LeftChannelDataLength));
               Display(("Frames/GAVD      : %d\r\n", (DataLength/PlaybackContext.DecodeConfiguration.FrameLength)));
            }

            NumDecodedSamples = DecodedData.LeftChannelDataLength + DecodedData.RightChannelDataLength;
            NumChannels       = PlaybackContext.StreamConfig.StreamFormat.NumberChannels;
            NumDecodedFrames  = (NumDecodedSamples / NumChannels);

            /* Determine if we need to add or drop any samples.         */
            if((PlaybackContext.SampleAdjustmentState != saContinuous) && (NumDecodedFrames > 0))
            {
               /* Increment the variable the counts the number of       */
               /* received frames in the current window.                */
               PlaybackContext.NumAdjustmentFrames += NumDecodedFrames;

               /* Check if we've reached the limit and if it's time to  */
               /* add or drop samples                                   */
               if(PlaybackContext.NumAdjustmentFrames > PlaybackContext.NumFramesAdjustmentLimit)
               {
                  if(PlaybackContext.SampleAdjustmentState == saDroppingSamples)
                  {
                     /* If we are dropping samples then we decrement the*/
                     /* number of samples by the number of channels.  We*/
                     /* cannot simply decrement by 1 because if playback*/
                     /* is stereo and the left and right channels are   */
                     /* interleaved, decrementing by 1 will swap the    */
                     /* left and right channels.  I (Sam) tested and    */
                     /* cannot hear when samples are dropped using this */
                     /* method.                                         */
                     NumDecodedSamples -= NumChannels;
                  }
                  else
                  {
                     if(PlaybackContext.SampleAdjustmentState == saAddingSamples)
                     {
                        /* If we are adding samples then set the sample */
                        /* being added to the value of the sample just  */
                        /* before it.  This has to be done for each     */
                        /* channel.  I (Sam) tested and cannot hear when*/
                        /* samples are added using this method.         */
                        for(Index = 0; Index < NumChannels; Index++)
                           DecodedData.LeftChannelDataPtr[NumDecodedSamples + Index] = DecodedData.LeftChannelDataPtr[(NumDecodedSamples - NumChannels) + Index];

                        /* Increment the number of decoded samples      */
                        NumDecodedSamples += NumChannels;
                     }
                  }

                  /* We have reached the limit point and have added or  */
                  /* removed sample(s).  Now set the number of received */
                  /* frames in this window to 0 to resume counting to   */
                  /* know when to drop or when to add the next          */
                  /* sample(s).                                         */
                  PlaybackContext.NumAdjustmentFrames = 0;
               }
            }

            if(NumDecodedSamples > 0)
            {
               PlaybackContext.PCMNumUsed += NumDecodedSamples;
               PlaybackContext.PCMNumFree -= NumDecodedSamples;
            }
         }
         else
            break;
      }
   }

   /* Return the number of bytes that were processed.                   */
   return(DataLength - UnusedDataLength);
}

   /* The following thread function is used to process SBC data         */
   /* retrieved by the remote device.                                   */
static void *BTPSAPI PlaybackThread(void *ThreadParameter)
{
   int               BytesUsed;
   snd_pcm_sframes_t NumFramesToWrite;
   unsigned int      TotalBytesUsed;
   snd_pcm_sframes_t NumFrames;
   Boolean_t         FirstPCMWrite;
   unsigned short*   FirstPCMWriteBuffer;

   Display(("Enter Decode Thread\r\n"));
   FirstPCMWrite = TRUE;

   /* Verify the parameters that we require.                            */
   if((!PlaybackContext.Exit) && (PlaybackContext.SBCDecodeEvent) && (PlaybackContext.ShutdownEvent))
   {
      while(1)
      {
         /* Wait until there is something to do.                        */
         if(BTPS_WaitEvent(PlaybackContext.SBCDecodeEvent, BTPS_INFINITE_WAIT))
         {
            /* Event signalled, make sure we have not been instructed to*/
            /* exit.                                                    */
            if(!PlaybackContext.Exit)
            {
               /* If we do not have any buffers allocated to decode data*/
               /* into, then we need to allocate one.                   */
               if(!PlaybackContext.PCMBuffer)
               {
                  /* Allocate memory for the PCM buffer.  We really     */
                  /* don't need a PCM buffer at all because ALSA buffers*/
                  /* the PCM samples.  However, calls to ALSA are       */
                  /* blocking and if we write small amounts of data to  */
                  /* ALSA we can not process incoming audio fast enough */
                  /* to prevent underrun.  Here we allocate enough      */
                  /* memory for the minimum number of samples plus any  */
                  /* samples that might be added if the SBC buffer      */
                  /* becomes low.                                       */
                  PlaybackContext.PCMMaxNumSamples  = PlaybackContext.MinNumSamplesToSendCodec + NUM_AUDIO_SAMPLES_PER_SBC_FRAME;
                  PlaybackContext.PCMMaxNumSamples += ((PlaybackContext.PCMMaxNumSamples / NUM_AUDIO_SAMPLES_PER_SBC_FRAME) * MAX_NUM_CHANNELS);
                  PlaybackContext.PCMBuffer         = (unsigned short *)BTPS_AllocateMemory(PlaybackContext.PCMMaxNumSamples * NUM_BYTES_PER_AUDIO_SAMPLE);

                  PlaybackContext.PCMNumUsed        = 0;
                  PlaybackContext.PCMNumFree        = PlaybackContext.PCMMaxNumSamples;
                  gettimeofday(&PlaybackContext.PlaybackStartTime, NULL);
               }

               /* Verify that we have audio data to process.            */
               if((PlaybackContext.SBCUsed) && (PlaybackContext.AudioState != asIdle))
               {
                  /* Check to see if we have started to receive SBC     */
                  /* packets and are in the process of buffering the SBC*/
                  /* packets.                                           */
                  if(PlaybackContext.AudioState == asBuffering)
                  {
                     /* Buffer SBC packets until the number of bytes in */
                     /* the SBC packet buffer reaches the start limit.  */
                     if(PlaybackContext.SBCUsed >= PlaybackContext.BufferStartLimit)
                        PlaybackContext.AudioState = asDecoding;
                  }

                  /* Check to see if we are decoding or playing.        */
                  if(PlaybackContext.AudioState != asBuffering)
                  {
                     TotalBytesUsed = 0;

                     /* While there are SBC packets in the buffer then  */
                     /* we will attempt to decode the packet.           */
                     while((PlaybackContext.PCMNumUsed < PlaybackContext.MinNumSamplesToSendCodec) && (TotalBytesUsed < PlaybackContext.SBCUsed))
                     {
                        /* Determine the amount of data that we can     */
                        /* process.                                     */
                        if(PlaybackContext.SBCIn > PlaybackContext.SBCOut)
                           BytesUsed = (PlaybackContext.SBCIn - PlaybackContext.SBCOut);
                        else
                           BytesUsed = (PlaybackContext.SBCEnd - PlaybackContext.SBCOut);

                        /* Attempt to decode an SBC frame.              */
                        BytesUsed = Decode(BytesUsed, &(PlaybackContext.SBCBuffer[PlaybackContext.SBCOut]));

                        if(BytesUsed > 0)
                        {
                           /* Adjust the index and free counts based on */
                           /* the number of bytes processed.            */
                           TotalBytesUsed         += BytesUsed;
                           PlaybackContext.SBCOut += BytesUsed;
                           if(PlaybackContext.SBCOut >= PlaybackContext.SBCEnd)
                              PlaybackContext.SBCOut = 0;
                        }
                        else
                           break;
                     }

                     if(PlaybackContext.AudioState != asPlaying)
                     {
                        /* Check to see if we are waiting to build up   */
                        /* samples before we start playing the audio.   */
                        if(PlaybackContext.AudioState == asDecoding)
                        {
                           /* Check to see if we have enough samples to */
                           /* start.                                    */
                           if(PlaybackContext.PCMNumUsed >= PlaybackContext.MinNumSamplesToSendCodec)
                           {
                              /* It is time to start the audio, so set  */
                              /* the audio state to playing.            */
                              PlaybackContext.AudioState = asPlaying;
                           }
                        }
                     }

                     if(TotalBytesUsed > 0)
                     {
                        /* Re-acquire the mutex because we need to      */
                        /* operate on some variables that need to be    */
                        /* protected.                                   */
                        BTPS_WaitMutex(PlaybackContext.Mutex, BTPS_INFINITE_WAIT);

                        /* Adjust the index and free counts based on the*/
                        /* number of bytes processed.                   */
                        PlaybackContext.SBCFree += TotalBytesUsed;
                        PlaybackContext.SBCUsed -= TotalBytesUsed;

                        if(PlaybackContext.SBCUsed == 0)
                        {
                           BTPS_ResetEvent(PlaybackContext.SBCDecodeEvent);
                           PlaybackContext.AudioState = asIdle;
                        }

                        /* If we got here then it means we still hold   */
                        /* the Lock, so we need to go ahead and release */
                        /* the mutex.                                   */
                        BTPS_ReleaseMutex(PlaybackContext.Mutex);
                     }
                  }
               }

               /* If we are playing and have enough items to send the   */
               /* codec then write the data to the codec.               */
               if((PlaybackContext.AudioState == asPlaying) && (PlaybackContext.PCMNumUsed >= PlaybackContext.MinNumSamplesToSendCodec))
               {
                  /* Get the number of available frames in ALSA's       */
                  /* buffer.                                            */
                  NumFrames = snd_pcm_avail_update(PlaybackContext.CodecHandle);

                  /* Calculate the number of frames we have available to*/
                  /* write.                                             */
                  NumFramesToWrite = PlaybackContext.PCMNumUsed / PlaybackContext.StreamConfig.StreamFormat.NumberChannels;

                  if(NumFrames >= NumFramesToWrite)
                  {
                     if(FirstPCMWrite)
                     {
                        /* On the first iteration we fill ALSA's buffer */
                        /* with 0's to prevent underrun.                */
                        FirstPCMWriteBuffer = (unsigned short *)BTPS_AllocateMemory(NumFrames * PlaybackContext.StreamConfig.StreamFormat.NumberChannels * NUM_BYTES_PER_AUDIO_SAMPLE);
                        if(FirstPCMWriteBuffer)
                        {
                           BTPS_MemInitialize(FirstPCMWriteBuffer, 0, NumFrames * PlaybackContext.StreamConfig.StreamFormat.NumberChannels * NUM_BYTES_PER_AUDIO_SAMPLE);
                           NumFrames = snd_pcm_writei(PlaybackContext.CodecHandle, FirstPCMWriteBuffer, NumFrames);
                           BTPS_FreeMemory(FirstPCMWriteBuffer);
                        }
                        FirstPCMWrite = FALSE;
                     }
                     else
                     {
                        /* Write the samples.                           */
                        NumFrames = snd_pcm_writei(PlaybackContext.CodecHandle, PlaybackContext.PCMBuffer, NumFramesToWrite);

                        if(NumFrames > 0)
                        {
                           if(NumFrames != NumFramesToWrite)
                              Display(("Error: snd_pcm_writei() partial write: %d of %d\r\n", NumFrames, NumFramesToWrite));

                           PlaybackContext.PCMNumUsed = 0;
                           PlaybackContext.PCMNumFree = PlaybackContext.PCMMaxNumSamples;
                        }
                     }

                     /* Display the elapsed time every time the SBC In  */
                     /* Index crosses the middle of the buffer.         */
                     if(PlaybackContext.SBCIn > (PlaybackContext.SBCEnd / 2))
                        DisplayElapsedTime(&PlaybackContext.PlaybackStartTime);
                  }

                  if(NumFrames == -EPIPE)
                  {
                     /* EPIPE means underrun, attempt to recover.       */
                     Display(("\r\nError: ALSA underrun.\r\n"));
                     snd_pcm_drain(PlaybackContext.CodecHandle);
                     snd_pcm_prepare(PlaybackContext.CodecHandle);

                     PlaybackContext.AudioState = asBuffering;
                     FirstPCMWrite              = TRUE;
                  }
               }
            }
            else
               break;
         }
         else
             break;
      }
   }

   if(PlaybackContext.CodecHandle != NULL)
   {
      snd_pcm_close(PlaybackContext.CodecHandle);
      PlaybackContext.CodecHandle = NULL;
   }

   /* Make sure we signal that the thread is now shutting down.         */
   if(PlaybackContext.ShutdownEvent)
      BTPS_SetEvent(PlaybackContext.ShutdownEvent);

   Display(("Exit Decode Thread\r\n"));

   return(NULL);
}

   /* Utility function to configure the Output device.  This function   */
   /* should return zero if if the hardware was configured correctly, or*/
   /* a non-zero value if there was an error.                           */
static int _ConfigureOutputDevice(PlaybackContext_t *Context)
{
   snd_pcm_hw_params_t *PcmHwParams;
   snd_pcm_sw_params_t *PcmSwParams;
   snd_pcm_uframes_t    BufferSizeInFrames;
   snd_pcm_uframes_t    PeriodSizeInFrames;
   int                  SubUnitDirection;
   int                  Result;

   SubUnitDirection = 0;
   PcmHwParams      = NULL;
   PcmSwParams      = NULL;

   if((Result = snd_pcm_open(&(Context->CodecHandle), "default", SND_PCM_STREAM_PLAYBACK, SND_PCM_ASYNC)) >= 0)
   {
      /* Allocate a hardware parameters object.                         */
      snd_pcm_hw_params_alloca(&PcmHwParams);

      /* Fill it in with default values.                                */
      if((Result = snd_pcm_hw_params_any(Context->CodecHandle, PcmHwParams)) >= 0)
      {
         /* Set the desired hardware parameters.                        */

         /* Interleaved mode                                            */
         if((Result = snd_pcm_hw_params_set_access(Context->CodecHandle, PcmHwParams, SND_PCM_ACCESS_RW_INTERLEAVED)) >= 0)
         {
            /* Signed 16-bit little-endian format                       */
            if((Result == snd_pcm_hw_params_set_format(Context->CodecHandle, PcmHwParams, SND_PCM_FORMAT_S16_LE)) >= 0)
            {
               /* Set the channels                                      */
               if((Result = snd_pcm_hw_params_set_channels(Context->CodecHandle, PcmHwParams, PlaybackContext.StreamConfig.StreamFormat.NumberChannels)) >= 0)
               {
                  /* Configure the frequency                            */
                  if((Result = snd_pcm_hw_params_set_rate_near(Context->CodecHandle, PcmHwParams, (unsigned int *)&((PlaybackContext.StreamConfig.StreamFormat.SampleFrequency)), &SubUnitDirection)) >= 0)
                  {
                     /* Set the ALSA ring buffer size.                  */
                     BufferSizeInFrames = NUM_CODEC_BUFFER_SAMPLES / Context->StreamConfig.StreamFormat.NumberChannels;
                     if((Result = snd_pcm_hw_params_set_buffer_size_near(Context->CodecHandle, PcmHwParams, &BufferSizeInFrames)) >= 0)
                     {
                        /* Specify the ALSA period size in frames to be */
                        /* 1/4th the size of the sample buffer size.    */
                        /* The period is the number of frames between   */
                        /* each hardware interrupt.                     */
                        PeriodSizeInFrames = BufferSizeInFrames / 4;
                        if((Result = snd_pcm_hw_params_set_period_size_near(Context->CodecHandle, PcmHwParams, &PeriodSizeInFrames, &SubUnitDirection)) >= 0)
                        {
                           /* Specify the number of samples to send to  */
                           /* the codec at once as twice the size of the*/
                           /* period.  Testing has shown that it has to */
                           /* be greater than or equal to the period    */
                           /* size for continuous playback.             */
                           Context->MinNumSamplesToSendCodec = PeriodSizeInFrames * Context->StreamConfig.StreamFormat.NumberChannels * 2;

                           /* Write the parameters to the driver        */
                           if((Result = snd_pcm_hw_params(Context->CodecHandle, PcmHwParams)) >= 0)
                           {
                              /* Configure software                     */
                              snd_pcm_sw_params_alloca(&PcmSwParams);
                              if((Result = snd_pcm_sw_params_current(Context->CodecHandle, PcmSwParams)) >= 0)
                              {
                                 /* Disable any buffering in software.  */
                                 if((Result = snd_pcm_sw_params_set_start_threshold(Context->CodecHandle, PcmSwParams, 2048)) >= 0)
                                 {
                                    if((Result = snd_pcm_prepare(Context->CodecHandle)) >= 0)
                                       Result = 0;
                                    else
                                       Display(("Unable to prepare audio interface for use %s\r\n", snd_strerror(Result)));
                                 }
                                 else
                                    Display(("Unable to set start threshold %s\n", snd_strerror(Result)));
                              }
                              else
                                 Display(("Unable to determine current swparams %s\n", snd_strerror(Result)));
                           }
                           else
                              Display(("Failed to write, not supported (%s)\n", snd_strerror(Result)));
                        }
                        else
                           Display(("Failed to write buffer size (%s)\n", snd_strerror(Result)));
                     }
                     else
                        Display(("Failed to write period (%s)\n", snd_strerror(Result)));
                  }
                  else
                     Display(("Sample rate, %d, not supported (%s)\n", (int)PlaybackContext.StreamConfig.StreamFormat.SampleFrequency, snd_strerror(Result)));
               }
               else
                  Display(("Channels, %d, not supported (%s)\n", PlaybackContext.StreamConfig.StreamFormat.NumberChannels, snd_strerror(Result)));
            }
            else
               Display(("Unable to set format (%s)\n", snd_strerror(Result)));
         }
         else
            Display(("Failed to set access (%s)\n", snd_strerror(Result)));
      }
   }

   snd_config_update_free_global();

   return(Result);
}

   /* Function calculates and displays the elapsed time.  The first     */
   /* parameter is the start time used to reference the elapsed time to.*/
static void DisplayElapsedTime(struct timeval *StartTime)
{
   struct timeval CurrentTime;
   int            NanoSeconds;
   int            Seconds;
   static int     LastSeconds = -1;

   if(gettimeofday(&CurrentTime, NULL) >= 0)
   {
      if(CurrentTime.tv_usec < StartTime->tv_usec)
      {
         NanoSeconds         = (StartTime->tv_usec - CurrentTime.tv_usec) / 1000000 + 1;
         StartTime->tv_usec -= 1000000 * NanoSeconds;
         StartTime->tv_sec  += NanoSeconds;
      }

      if(CurrentTime.tv_usec - StartTime->tv_usec > 1000000)
      {
         NanoSeconds         = (CurrentTime.tv_usec - StartTime->tv_usec) / 1000000;
         StartTime->tv_usec += 1000000 * NanoSeconds;
         StartTime->tv_sec  -= NanoSeconds;
      }

      Seconds = (CurrentTime.tv_sec - StartTime->tv_sec);
      if(Seconds != LastSeconds)
      {
         Display(("\rElapsed Time: %i:%.2i", (Seconds / 60), (Seconds % 60)));
         LastSeconds = Seconds;
      }
   }
}

   /* The following function initializes the audio decoder.  The first  */
   /* parameter is a valid Bluetooth Stack ID This function will return */
   /* zero on success and negative on error.                            */
int InitializeAudioDecoder(unsigned int BluetoothStackID)
{
   int Result = -1;

   /* Verify that we are not Initialized.                               */
   if(!Initialized)
   {
      /* Initialize the Context structure information.                  */
      BTPS_MemInitialize(&PlaybackContext, 0, sizeof(PlaybackContext_t));

      /* Note the Bluetooth Stack ID.                                   */
      PlaybackContext.BluetoothStackID = BluetoothStackID;

      /* Flag the entire SBC buffer is empty.                           */
      PlaybackContext.SBCFree          = SBC_BUFFER_SIZE;

      /* Set the Audio State to Idle.                                   */
      PlaybackContext.AudioState        = asIdle;
      PlaybackContext.DisplayFormatInfo = 1;

      /* Retrieve the current Stream Configuration.                     */
      Result = AUDM_Query_Audio_Stream_Configuration(astSNK, &PlaybackContext.StreamConfig);
      if(!Result)
      {
         /* Note the current frequency                                  */
         PlaybackContext.CurrentSampleRate = PlaybackContext.StreamConfig.StreamFormat.SampleFrequency;

         if((PlaybackContext.StreamConfig.MediaCodecType == A2DP_MEDIA_CODEC_TYPE_SBC) && (PlaybackContext.StreamConfig.MediaCodecInfoLength == sizeof(A2DP_SBC_Codec_Specific_Information_Element_t)))
         {
            if(((PlaybackContext.ShutdownEvent = BTPS_CreateEvent(FALSE)) != NULL) && ((PlaybackContext.Mutex = BTPS_CreateMutex(FALSE)) != NULL) && ((PlaybackContext.SBCDecodeEvent = BTPS_CreateEvent(FALSE)) != NULL))
            {
               /* Go ahead and configure the output device.             */
               if(!_ConfigureOutputDevice(&PlaybackContext))
               {
                  /* Now, we are ready to start decoding.  First, let's */
                  /* initialize the Decoder.                            */
                  if((PlaybackContext.DecoderHandle = SBC_Initialize_Decoder()) != NULL)
                  {
                     Initialized = TRUE;

                     if(BTPS_CreateThread(PlaybackThread, DECODE_THREAD_STACK_SIZE, NULL))
                     {
                        Result = 0;
                     }
                     else
                     {
                        Initialized = FALSE;

                        Display(("Unable to start Playback Thread.\r\n"));
                     }
                  }
                  else
                     Display(("Failed to initialize decoder.\r\n"));
               }
               else
                  Display(("Failed to configure output device.\r\n"));
            }
            else
               Display(("Failed to create mutex/event.\r\n"));
         }
         else
            Display(("Unsupported stream type or invalid configuration\r\n"));
      }
   }
   else
      Display(("Audio Decoder Already Initialized\r\n"));

   return(Result);
}

   /* The following function is responsible for freeing all resources   */
   /* that were previously allocated for an audio decoder.              */
void CleanupAudioDecoder(void)
{
   if(Initialized)
   {
      Initialized = FALSE;

      /* Set the event to kill the thread                               */
      if((PlaybackContext.ShutdownEvent) && (PlaybackContext.SBCDecodeEvent))
      {
         /* Flag that we would like the decode thread to exit.          */
         PlaybackContext.AudioState = asIdle ;
         PlaybackContext.Exit       = TRUE;

         BTPS_SetEvent(PlaybackContext.SBCDecodeEvent);

         Display(("Waiting for thread...\r\n"));

         BTPS_WaitEvent(PlaybackContext.ShutdownEvent, BTPS_INFINITE_WAIT);

         Display(("Wait done\r\n"));

         /* Thread has been shut down, go ahead and clean up all the    */
         /* resources.                                                  */
         BTPS_CloseEvent(PlaybackContext.ShutdownEvent);
         PlaybackContext.ShutdownEvent = NULL;

         BTPS_CloseEvent(PlaybackContext.SBCDecodeEvent);
         PlaybackContext.SBCDecodeEvent = NULL;
      }

      if(PlaybackContext.DecoderHandle)
      {
         /* We are all finished with the decoder, so we can inform the  */
         /* library that we are finished with the handle that we opened.*/
         SBC_Cleanup_Decoder(PlaybackContext.DecoderHandle);

         PlaybackContext.DecoderHandle = NULL;
      }

      PlaybackContext.AudioState = asIdle;

      if(PlaybackContext.PCMBuffer)
         BTPS_FreeMemory(PlaybackContext.PCMBuffer);
   }
}

   /* The following function is used to process audio data.  The        */
   /* parameters to this function specify the raw encoded audio data    */
   /* length and data, respectively.  A negative value will be returned */
   /* on error.                                                         */
int ProcessAudioData(void *InData, unsigned int DataLength)
{
   int          Result = -1;
   unsigned int SBCFrameLength;
   unsigned int NumSBCFrames;
   unsigned char *DataPtr;

   /* Ignore data if we are not initialized                             */
   if(Initialized)
   {
      DataPtr = InData;

      /* Confirm we have a buffer, and the length.                      */
      if((DataPtr) && (DataLength))
      {
         NumSBCFrames = (DataPtr[0] & A2DP_SBC_HEADER_NUMBER_FRAMES_MASK);

         /* Determine the Number and Size of each SBC frames that are in*/
         /* the GAVD packet.                                            */
         SBCFrameLength = (DataLength / NumSBCFrames);
         if(PlaybackContext.AudioState == asIdle)
         {
            /* This is the first SBC frame received since the stream was*/
            /* started.  Initialize information about the data being    */
            /* received.                                                */
            PlaybackContext.SBCIn                    = 0;
            PlaybackContext.SBCOut                   = 0;
            PlaybackContext.SBCEnd                   = SBC_BUFFER_SIZE;
            PlaybackContext.SBCFree                  = PlaybackContext.SBCEnd;
            PlaybackContext.SBCUsed                  = 0;
            PlaybackContext.AudioState               = asBuffering;
            PlaybackContext.BufferLowLimit           = SBC_BUFFER_SIZE / 4;
            PlaybackContext.BufferHighLimit          = PlaybackContext.SBCEnd - PlaybackContext.BufferLowLimit;
            PlaybackContext.BufferStartLimit         = ((PlaybackContext.BufferHighLimit + PlaybackContext.BufferLowLimit) / 2);
            PlaybackContext.NumFramesAdjustmentLimit = PlaybackContext.CurrentSampleRate / SAMPLE_ADJUSTMENT_RATE;
            PlaybackContext.NumAdjustmentFrames      = 0;
            PlaybackContext.SampleAdjustmentState    = saContinuous;

            Display(("Buffer High Limit: %d\r\n", PlaybackContext.BufferHighLimit));
            Display(("Buffer Low Limit : %d\r\n", PlaybackContext.BufferLowLimit));

            /* Go ahead and inform the decode thread that there is data */
            /* to process.                                              */
            if(PlaybackContext.SBCDecodeEvent)
               BTPS_SetEvent(PlaybackContext.SBCDecodeEvent);
         }

         /* The first byte of the data buffer contains the number of    */
         /* frames.  We need to account for this.                       */
         DataLength--;
         DataPtr++;

         /* Go ahead and acquire the mutex to protect the variables that*/
         /* are shared between this module and the playback thread.     */
         BTPS_WaitMutex(PlaybackContext.Mutex, BTPS_INFINITE_WAIT);

         /* Move the SBC frames to the SBC buffer as long as there is   */
         /* room for the data.                                          */
         /* * NOTE * There are two choices here.  We can either:        */
         /*                                                             */
         /*             - drop the frames that we don't have room       */
         /*               for (i.e. these newest frames)                */
         /*             - drop the oldest frames to make room.          */
         /*                                                             */
         /*          Currently we are going to drop the newest          */
         /*          (incoming) frames.  Either way frames will have    */
         /*          to be dropped.                                     */
         if(PlaybackContext.SBCFree >= DataLength)
         {
            /* Verify that this copy will not go beyond the end the     */
            /* buffer.                                                  */
            if((PlaybackContext.SBCIn + DataLength) <= SBC_BUFFER_SIZE)
            {
               /* Single copy operation.                                */
               BTPS_MemCopy(&(PlaybackContext.SBCBuffer[PlaybackContext.SBCIn]), DataPtr, DataLength);
            }
            else
            {
               /* Multiple copy operation.                              */
               BTPS_MemCopy(&(PlaybackContext.SBCBuffer[PlaybackContext.SBCIn]), DataPtr, (SBC_BUFFER_SIZE - PlaybackContext.SBCIn));

               BTPS_MemCopy(&(PlaybackContext.SBCBuffer[0]), &(DataPtr[SBC_BUFFER_SIZE - PlaybackContext.SBCIn]), DataLength - (SBC_BUFFER_SIZE - PlaybackContext.SBCIn));
            }

            PlaybackContext.SBCFree -= DataLength;
            PlaybackContext.SBCIn   += DataLength;
            PlaybackContext.SBCUsed += DataLength;
            if(PlaybackContext.SBCIn >= SBC_BUFFER_SIZE)
            {
               /* Wrap the buffer pointer.                              */
               PlaybackContext.SBCIn -= SBC_BUFFER_SIZE;
            }
         }
         else
         {
            /* We need to truncate the amount of data we copy to into   */
            /* the buffer as there is not enough room for all the       */
            /* frames.                                                  */

            /* Truncation will occur on SBC frame boundaries.  This     */
            /* means that we will not insert partial SBC frames.        */

            while(NumSBCFrames)
            {
               /* See if the current SBC frame will fit into the buffer.*/
               if((SBCFrameLength = SBC_CalculateDecoderFrameSize(DataLength, DataPtr)) != 0)
               {
                  if(SBCFrameLength <= PlaybackContext.SBCFree)
                  {
                     /* Frame will fit, go ahead and copy it into the   */
                     /* buffer.                                         */

                     /* Verify that this copy will not go beyond the end*/
                     /* of the buffer.                                  */
                     if((PlaybackContext.SBCIn + SBCFrameLength) <= SBC_BUFFER_SIZE)
                     {
                        /* Single copy operation.                       */
                        BTPS_MemCopy(&(PlaybackContext.SBCBuffer[PlaybackContext.SBCIn]), DataPtr, SBCFrameLength);
                     }
                     else
                     {
                        /* Multiple copy operation.                     */
                        BTPS_MemCopy(&(PlaybackContext.SBCBuffer[PlaybackContext.SBCIn]), DataPtr, (SBC_BUFFER_SIZE - PlaybackContext.SBCIn));

                        BTPS_MemCopy(&(PlaybackContext.SBCBuffer[0]), &(DataPtr[(SBC_BUFFER_SIZE - PlaybackContext.SBCIn)]), SBCFrameLength - (SBC_BUFFER_SIZE - PlaybackContext.SBCIn));
                     }

                     PlaybackContext.SBCFree -= SBCFrameLength;
                     PlaybackContext.SBCIn   += SBCFrameLength;
                     PlaybackContext.SBCUsed += SBCFrameLength;
                     if(PlaybackContext.SBCIn >= SBC_BUFFER_SIZE)
                     {
                        /* Wrap the buffer pointer.                     */
                        PlaybackContext.SBCIn -= SBC_BUFFER_SIZE;
                     }

                     /* Move to the next SBC frame to process.          */
                     DataPtr += SBCFrameLength;

                     NumSBCFrames--;
                  }
                  else
                     NumSBCFrames = 0;
               }
               else
                  NumSBCFrames = 0;
            }
         }

         /* Finished.  Go ahead and release the mutex.                  */
         BTPS_ReleaseMutex(PlaybackContext.Mutex);

         if(PlaybackContext.AudioState != asBuffering)
         {
            if(PlaybackContext.SampleAdjustmentState == saContinuous)
            {
               if(PlaybackContext.SBCUsed >= PlaybackContext.BufferHighLimit)
               {
                  PlaybackContext.SampleAdjustmentState = saDroppingSamples;
                  PlaybackContext.NumAdjustmentFrames = 0;
                  Display(("\r\nWarning: Buffer Near Full, Dropping Samples.\r\n"));
               }
               else
               {
                  if(PlaybackContext.SBCUsed <= PlaybackContext.BufferLowLimit)
                  {
                     PlaybackContext.SampleAdjustmentState = saAddingSamples;
                     PlaybackContext.NumAdjustmentFrames = 0;
                     Display(("\r\nWarning: Buffer Low, Adding Samples.\r\n"));
                  }
               }
            }
            else
            {
               if((PlaybackContext.SampleAdjustmentState == saDroppingSamples) && (PlaybackContext.SBCUsed <= PlaybackContext.BufferStartLimit))
               {
                  PlaybackContext.SampleAdjustmentState = saContinuous;
               }
               else
               {
                  if((PlaybackContext.SampleAdjustmentState == saAddingSamples) && (PlaybackContext.SBCUsed >= PlaybackContext.BufferStartLimit))
                     PlaybackContext.SampleAdjustmentState = saContinuous;
               }

               if(PlaybackContext.SampleAdjustmentState == saContinuous)
                  Display(("\r\nStatus: Buffer Size Sustained, Resuming Continuous Playback.\r\n"));
            }
         }

         /* Flag that we were successful.                               */
         Result = 0;
      }

      if(PlaybackContext.AudioState == asWaiting)
      {
         BTPS_SetEvent(PlaybackContext.SBCDecodeEvent);
         PlaybackContext.AudioState = asPlaying;
      }
   }

   return(Result);
}
