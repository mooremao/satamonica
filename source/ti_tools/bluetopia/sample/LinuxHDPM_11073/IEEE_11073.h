/*****< IEEE_11073.h >*********************************************************/
/*      Copyright 2001 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  IEEE_11073 - IEEE 11073 Header File.                                      */
/*                                                                            */
/*  Author:  Tim Thomas                                                       */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   09/19/10  T. Thomas      Initial creation.                               */
/******************************************************************************/
#ifndef __IEEE_11073H__
#define __IEEE_11073H__

#include "PHDTypes.h"

#define EUI_64_SIZE                                      (8)
typedef struct _tagAssoc_Info_t
{
  Word_t  EncodingRules;
  Word_t  DeviceConfigID;
  DWord_t SystemType;
  Word_t  SystemIDLength;
  Byte_t  SystemID[EUI_64_SIZE];
} Assoc_Info_t;

typedef struct _tagPHD_Data_Req_Mode_Capab
{
   Word_t Flags;
   Byte_t AgentCount;
   Byte_t ManagerCount;
} PHD_Data_Req_Mode_Capab_t;

typedef struct _tagPHD_Assoc_Info_t
{
   DWord_t                   ProtocolVersion;
   Word_t                    EncodingRules;
   DWord_t                   NomenclatureVersion;
   DWord_t                   FunctionalUnits;
   DWord_t                   SystemType;
   Word_t                    SystemIDLength;
   Byte_t                    SystemID[EUI_64_SIZE];
   Word_t                    DeviceConfigID;
   PHD_Data_Req_Mode_Capab_t DataReqModeCapab;
   Word_t                    OptionsCount;
   Word_t                    OptionsLength;
} PHD_Assoc_Info_t;

typedef struct _tagEvent_Info_t
{
   Word_t  InvokeID;
   Word_t  Service;
   Word_t  EventHandle;
   DWord_t CurrentTime;
   Word_t  EventType;
   Word_t  NumData;
   Word_t  Data[3];
} Event_Info_t;

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Release_Req_t
{
   NonAlignedWord_t Reason;
} __PACKED_STRUCT_END__ PHD_Release_Req_t;

#define PHD_RELEASE_REQ_DATA_SIZE         (sizeof(PHD_Release_Req_t))

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Release_Rsp_t
{
   NonAlignedWord_t Reason;
} __PACKED_STRUCT_END__ PHD_Release_Rsp_t;

#define PHD_RELEASE_RSP_DATA_SIZE         (sizeof(PHD_Release_Rsp_t))

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Abort_Req_t
{
   NonAlignedWord_t Reason;
} __PACKED_STRUCT_END__ PHD_Abort_Req_t;

#define PHD_ABORT_REQ_DATA_SIZE         (sizeof(PHD_Abort_Req_t))

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_APDU_t
{
   NonAlignedWord_t Choice;
   NonAlignedWord_t ChoiceLength;
   Byte_t           Data[1];
} __PACKED_STRUCT_END__ PHD_APDU_t;

#define PHD_APDU_DATA_SIZE(_x)            (STRUCTURE_OFFSET(PHD_APDU_t, Data) + (_x))

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Assoc_Rsp_t
{
   NonAlignedWord_t           Choice;
   NonAlignedWord_t           ChoiceLength;
   NonAlignedWord_t           Result;
   NonAlignedWord_t           DataProtoID;
   NonAlignedWord_t           DataProtoInfoLength;
   NonAlignedDWord_t          ProtocolVersion;
   NonAlignedWord_t           EncodingRules;
   NonAlignedDWord_t          NomenclatureVersion;
   NonAlignedDWord_t          FunctionalUnits;
   NonAlignedDWord_t          SystemType;
   NonAlignedWord_t           SystemIDLength;
   Byte_t                     SystemID[EUI_64_SIZE];
   NonAlignedWord_t           DeviceConfigID;
   PHD_Data_Req_Mode_Capab_t  DataReqModeCapab;
   NonAlignedWord_t           OptionsCount;
   NonAlignedWord_t           OptionsLength;
} __PACKED_STRUCT_END__ PHD_Assoc_Rsp_t;

#define PHD_ASSOC_RSP_DATA_SIZE           sizeof(PHD_Assoc_Rsp_t)
#define PHD_DATA_PROTO_INFO_LENGTH        (PHD_ASSOC_RSP_DATA_SIZE-STRUCTURE_OFFSET(PHD_Assoc_Rsp_t, ProtocolVersion))

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Data_t
{
   NonAlignedWord_t           InvokeID;
   NonAlignedWord_t           Choice;
   NonAlignedWord_t           ChoiceLength;
   Byte_t                     Data[1];
} __PACKED_STRUCT_END__ PHD_Data_t;

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Event_Report_t
{
   NonAlignedWord_t  Handle;
   NonAlignedDWord_t EventTime;
   NonAlignedWord_t  EventType;
   NonAlignedWord_t  Length;
   Byte_t            EventInfo[1];
} PHD_Event_Report_t;

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Observation_t
{
   NonAlignedWord_t Handle;
   NonAlignedWord_t Length;
   NonAlignedWord_t Data[1];
} PHD_Observation_t;

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Fixed_Scan_Report_t
{
   NonAlignedWord_t  DataRequestID;
   NonAlignedWord_t  ScanReportNumber;
   NonAlignedWord_t  Count;
   NonAlignedWord_t  Length;
   PHD_Observation_t Observation[1];
} PHD_Fixed_Scan_Report_t;


typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Event_Rsp_t
{
   NonAlignedWord_t           Choice;
   NonAlignedWord_t           ChoiceLength;
   NonAlignedWord_t           DataLength;
   NonAlignedWord_t           InvokeID;
   NonAlignedWord_t           Service;
   NonAlignedWord_t           ReportLength;
   NonAlignedWord_t           Handle;
   NonAlignedDWord_t          RelativeTime;
   NonAlignedWord_t           EventType;
   NonAlignedWord_t           OptionsLength;
} __PACKED_STRUCT_END__ PHD_Event_Rsp_t;

#define PHD_EVENT_RSP_DATA_SIZE           sizeof(PHD_Event_Rsp_t)
#define PHD_EVENT_RSP_CHIOCE_LENGTH       (PHD_EVENT_RSP_DATA_SIZE-STRUCTURE_OFFSET(PHD_Event_Rsp_t, DataLength))
#define PHD_EVENT_RSP_DATA_LENGTH         (PHD_EVENT_RSP_DATA_SIZE-STRUCTURE_OFFSET(PHD_Event_Rsp_t, InvokeID))
#define PHD_EVENT_RSP_REPORT_LENGTH       (PHD_EVENT_RSP_DATA_SIZE-STRUCTURE_OFFSET(PHD_Event_Rsp_t, Handle))

typedef __PACKED_STRUCT_BEGIN__ struct _tagPHD_Config_Report_t
{
   NonAlignedWord_t  ReportID;
   NonAlignedWord_t  Count;
   NonAlignedWord_t  Length;
   Byte_t            ConfigObject[1];
} PHD_Config_Report_t;

int ProcessAssociationRequest(int Length, AARQ_apdu *APDU, Assoc_Info_t *AssocInfo);
int FormatAssociationResponse(PHD_Assoc_Rsp_t *APDU, Word_t Result, Assoc_Info_t *AssocInfo);

int ParseEventReport(PHD_Event_Report_t *EventReport, Event_Info_t *EventInfo);
int FormatEventResponse(PHD_Event_Rsp_t *APDU, Event_Info_t *EventInfo);

#endif


