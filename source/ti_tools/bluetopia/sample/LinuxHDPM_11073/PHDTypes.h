#ifndef PHD_TYPES
#define PHD_TYPES

#include "BTTypes.h"

/* The following typedefs may need to be changed depending on the       */
/* Compiler and machine architecture.                                   */
/* Partition codes */
#define MDC_PART_OBJ                                  1   /* Object Infrastr.    */
#define MDC_PART_SCADA                                2   /* SCADA (Physio IDs   */
#define MDC_PART_DIM                                  4   /* Dimension           */
#define MDC_PART_INFRA                                8   /* Infrastructure      */
#define MDC_PART_PHD_DM                               128 /* Disease Mgmt        */
#define MDC_PART_PHD_HF                               129 /* Health and Fitness  */
#define MDC_PART_PHD_AI                               130 /* Aging Independently */
#define MDC_PART_RET_CODE                             255 /* Return Codes        */
#define MDC_PART_EXT_NOM                              256 /* Ext. Nomenclature   */

/* From Object Infrastructure (MDC_PART_OBJ)                            */
#define MDC_MOC_VMO_METRIC                            4
#define MDC_MOC_VMO_METRIC_ENUM                       5
#define MDC_MOC_VMO_METRIC_NU                         6
#define MDC_MOC_VMO_METRIC_SA_RT                      9
#define MDC_MOC_SCAN                                  16
#define MDC_MOC_SCAN_CFG                              17
#define MDC_MOC_SCAN_CFG_EPI                          18
#define MDC_MOC_SCAN_CFG_PERI                         19
#define MDC_MOC_VMS_MDS_SIMP                          37
#define MDC_MOC_VMO_PMSTORE                           61
#define MDC_MOC_PM_SEGMENT                            62
#define MDC_ATTR_CONFIRM_MODE                         2323
#define MDC_ATTR_CONFIRM_TIMEOUT                      2324
#define MDC_ATTR_ID_HANDLE                            2337
#define MDC_ATTR_ID_INSTNO                            2338
#define MDC_ATTR_ID_LABEL_STRING                      2343
#define MDC_ATTR_ID_MODEL                             2344
#define MDC_ATTR_ID_PHYSIO                            2347
#define MDC_ATTR_ID_PROD_SPECN                        2349
#define MDC_ATTR_ID_TYPE                              2351
#define MDC_ATTR_METRIC_STORE_CAPAC_CNT               2369
#define MDC_ATTR_METRIC_STORE_SAMPLE_ALG              2371
#define MDC_ATTR_METRIC_STORE_USAGE_CNT               2372
#define MDC_ATTR_MSMT_STAT                            2375
#define MDC_ATTR_NU_ACCUR_MSMT                        2378
#define MDC_ATTR_NU_CMPD_VAL_OBS                      2379
#define MDC_ATTR_NU_VAL_OBS                           2384
#define MDC_ATTR_NUM_SEG                              2385
#define MDC_ATTR_OP_STAT                              2387
#define MDC_ATTR_POWER_STAT                           2389
#define MDC_ATTR_SA_SPECN                             2413
#define MDC_ATTR_SCALE_SPECN_I16                      2415
#define MDC_ATTR_SCALE_SPECN_I32                      2416
#define MDC_ATTR_SCALE_SPECN_I8                       2417
#define MDC_ATTR_SCAN_REP_PD                          2421
#define MDC_ATTR_SEG_USAGE_CNT                        2427
#define MDC_ATTR_SYS_ID                               2436
#define MDC_ATTR_SYS_TYPE                             2438
#define MDC_ATTR_TIME_ABS                             2439
#define MDC_ATTR_TIME_BATT_REMAIN                     2440
#define MDC_ATTR_TIME_END_SEG                         2442
#define MDC_ATTR_TIME_PD_SAMP                         2445
#define MDC_ATTR_TIME_REL                             2447
#define MDC_ATTR_TIME_STAMP_ABS                       2448
#define MDC_ATTR_TIME_STAMP_REL                       2449
#define MDC_ATTR_TIME_START_SEG                       2450
#define MDC_ATTR_TX_WIND                              2453
#define MDC_ATTR_UNIT_CODE                            2454
#define MDC_ATTR_UNIT_LABEL_STRING                    2457
#define MDC_ATTR_VAL_BATT_CHARGE                      2460
#define MDC_ATTR_VAL_ENUM_OBS                         2462
#define MDC_ATTR_TIME_REL_HI_RES                      2536
#define MDC_ATTR_TIME_STAMP_REL_HI_RES                2537
#define MDC_ATTR_DEV_CONFIG_ID                        2628
#define MDC_ATTR_MDS_TIME_INFO                        2629
#define MDC_ATTR_METRIC_SPEC_SMALL                    2630
#define MDC_ATTR_SOURCE_HANDLE_REF                    2631
#define MDC_ATTR_SIMP_SA_OBS_VAL                      2632
#define MDC_ATTR_ENUM_OBS_VAL_SIMP_OID                2633
#define MDC_ATTR_ENUM_OBS_VAL_SIMP_STR                2634
#define MDC_ATTR_REG_CERT_DATA_LIST                   2635
#define MDC_ATTR_NU_VAL_OBS_BASIC                     2636
#define MDC_ATTR_PM_STORE_CAPAB                       2637
#define MDC_ATTR_PM_SEG_MAP                           2638
#define MDC_ATTR_PM_SEG_PERSON_ID                     2639
#define MDC_ATTR_SEG_STATS                            2640
#define MDC_ATTR_SEG_FIXED_DATA                       2641
#define MDC_ATTR_PM_SEG_ELEM_STAT_ATTR                2642
#define MDC_ATTR_SCAN_HANDLE_ATTR_VAL_MAP             2643
#define MDC_ATTR_SCAN_REP_PD_MIN                      2644
#define MDC_ATTR_ATTRIBUTE_VAL_MAP                    2645
#define MDC_ATTR_NU_VAL_OBS_SIMP                      2646
#define MDC_ATTR_PM_STORE_LABEL_STRING                2647
#define MDC_ATTR_PM_SEG_LABEL_STRING                  2648
#define MDC_ATTR_TIME_PD_MSMT_ACTIVE                  2649
#define MDC_ATTR_SYS_TYPE_SPEC_LIST                   2650
#define MDC_ATTR_METRIC_ID_PART                       2655
#define MDC_ATTR_ENUM_OBS_VAL_PART                    2656
#define MDC_ATTR_SUPPLEMENTAL_TYPES                   2657
#define MDC_ATTR_TIME_ABS_ADJUST                      2658
#define MDC_ATTR_CLEAR_TIMEOUT                        2659
#define MDC_ATTR_TRANSFER_TIMEOUT                     2660
#define MDC_ATTR_ENUM_OBS_VAL_SIMP_BIT_STR            2661
#define MDC_ATTR_ENUM_OBS_VAL_BASIC_BIT_STR           2662
#define MDC_ATTR_METRIC_STRUCT_SMALL                  2675
#define MDC_ATTR_NU_CMPD_VAL_OBS_SIMP                 2676
#define MDC_ATTR_NU_CMPD_VAL_OBS_BASIC                2677
#define MDC_ATTR_ID_PHYSIO_LIST                       2678
#define MDC_ATTR_SCAN_HANDLE_LIST                     2679

/* Partition: ACT                                                       */
#define MDC_ACT_SEG_CLR                               3084
#define MDC_ACT_SEG_GET_INFO                          3085
#define MDC_ACT_SET_TIME                              3095
#define MDC_ACT_DATA_REQUEST                          3099
#define MDC_ACT_SEG_TRIG_XFER                         3100
#define MDC_NOTI_CONFIG                               3356
#define MDC_NOTI_SCAN_REPORT_FIXED                    3357
#define MDC_NOTI_SCAN_REPORT_VAR                      3358
#define MDC_NOTI_SCAN_REPORT_MP_FIXED                 3359
#define MDC_NOTI_SCAN_REPORT_MP_VAR                   3360
#define MDC_NOTI_SEGMENT_DATA                         3361
#define MDC_NOTI_UNBUF_SCAN_REPORT_VAR                3362
#define MDC_NOTI_UNBUF_SCAN_REPORT_FIXED              3363
#define MDC_NOTI_UNBUF_SCAN_REPORT_GROUPED            3364
#define MDC_NOTI_UNBUF_SCAN_REPORT_MP_VAR             3365
#define MDC_NOTI_UNBUF_SCAN_REPORT_MP_FIXED           3366
#define MDC_NOTI_UNBUF_SCAN_REPORT_MP_GROUPED         3367
#define MDC_NOTI_BUF_SCAN_REPORT_VAR                  3368
#define MDC_NOTI_BUF_SCAN_REPORT_FIXED                3369
#define MDC_NOTI_BUF_SCAN_REPORT_GROUPED              3370
#define MDC_NOTI_BUF_SCAN_REPORT_MP_VAR               3371
#define MDC_NOTI_BUF_SCAN_REPORT_MP_FIXED             3372
#define MDC_NOTI_BUF_SCAN_REPORT_MP_GROUPED           3373

   /* From Medical supervisory control and data acquisition             */
   /* (MDC_PART_SCADA)                                                  */
#define MDC_TEMP_BODY                                 19292 /*TEMPbody  */
#define MDC_MASS_BODY_ACTUAL                          57664

   /* Partition: SCADA/Other                                            */
#define MDC_BODY_FAT                                  57676

   /* From Dimensions (MDC_PART_DIM)                                    */
#define MDC_DIM_PERCENT                               544  /* %   */
#define MDC_DIM_KILO_G                                1731 /* kg  */
#define MDC_DIM_MIN                                   2208 /* min */
#define MDC_DIM_HR                                    2240 /* h   */
#define MDC_DIM_DAY                                   2272 /* d   */
#define MDC_DIM_DEGC                                  6048 /* oC  */

   /* From Communication Infrastructure (MDC_PART_INFRA)                */
#define MDC_DEV_SPEC_PROFILE_PULS_OXIM                4100
#define MDC_DEV_SPEC_PROFILE_BP                       4103
#define MDC_DEV_SPEC_PROFILE_TEMP                     4104
#define MDC_DEV_SPEC_PROFILE_SCALE                    4111
#define MDC_DEV_SPEC_PROFILE_GLUCOSE                  4113
#define MDC_DEV_SPEC_PROFILE_HF_CARDIO                4137
#define MDC_DEV_SPEC_PROFILE_HF_STRENGTH              4138
#define MDC_DEV_SPEC_PROFILE_AI_ACTIVITY_HUB          4167
#define MDC_DEV_SPEC_PROFILE_AI_MED_MINDER            4168

   /* Placed 256 back from the start of the last Partition:             */
   /* OptionalPackageIdentifiers (i.e., 8192-256).                      */
#define MDC_TIME_SYNC_NONE                            7936 /* no time synchronization protocol supported */
#define MDC_TIME_SYNC_NTPV3                           7937 /* RFC 1305 1992 Mar obs: 1119,1059,958 */
#define MDC_TIME_SYNC_NTPV4                           7938 /* <under development at ntp.org> */
#define MDC_TIME_SYNC_SNTPV4                          7939 /* RFC 2030 1996 Oct obs: 1769 */
#define MDC_TIME_SYNC_SNTPV4330                       7940 /* RFC 4330 2006 Jan obs: 2030,1769 */
#define MDC_TIME_SYNC_BTV1                            7941 /* Bluetooth Medical Device Profile */

   /* From Return Codes (MDC_PART_RET_CODE)                             */
#define MDC_RET_CODE_UNKNOWN                          1 /* Generic error code */

   /* Partition MDC_PART_RET_CODE/OBJ Object errors                     */
#define MDC_RET_CODE_OBJ_BUSY                         1000 /* Object is busy so cannot handle the request */

   /* Partition MDC_PART_RETURN_CODES/STORE Storage errors              */
#define MDC_RET_CODE_STORE_EXH                        2000 /* Storage such as disk is full */
#define MDC_RET_CODE_STORE_OFFLN                      2001 /* Storage such as disk is offline */

typedef struct _tagAny
{
   Word_t length;
   Byte_t  value[1]; /* first element of the array */
} Any;

typedef Word_t OID_Type;
typedef Word_t PrivateOid;
typedef Word_t Handle;
typedef Word_t InstNumber;
typedef Word_t NomPartition;

#define NOM_PART_UNSPEC                                  0
#define NOM_PART_OBJ                                     1
#define NOM_PART_METRIC                                  2
#define NOM_PART_ALERT                                   3
#define NOM_PART_DIM                                     4
#define NOM_PART_VATTR                                   5
#define NOM_PART_PGRP                                    6
#define NOM_PART_SITES                                   7
#define NOM_PART_INFRASTRUCT                             8
#define NOM_PART_FEF                                     9
#define NOM_PART_ECG_EXTN                                10
#define NOM_PART_PHD_DM                                  128
#define NOM_PART_PHD_HF                                  129
#define NOM_PART_PHD_AI                                  130
#define NOM_PART_RET_CODE                                255
#define NOM_PART_EXT_NOM                                 256
#define NOM_PART_PRIV                                    1024

typedef struct _tagTYPE
{
   NomPartition partition;
   OID_Type     code;
} TYPE;

typedef struct _tagAVA_Type
{
   OID_Type attribute_id;
   Any      attribute_value;
} AVA_Type;

typedef struct _tagAttributeList
{
   Word_t   count;
   Word_t   length;
   AVA_Type value[1]; /* first element of the array */
} AttributeList;

typedef struct _tagAttributeIdList
{
   Word_t   count;
   Word_t   length;
   OID_Type value[1]; /* first element of the array */
} AttributeIdList;

typedef DWord_t FLOAT_Type;
typedef Word_t SFLOAT_Type;
typedef DWord_t RelativeTime;

typedef struct _tagHighResRelativeTime
{
   Byte_t value[8];
} HighResRelativeTime;

typedef struct _tagAbsoluteTimeAdjust
{
   Byte_t value[6];
} AbsoluteTimeAdjust;

typedef struct _tagAbsoluteTime
{
   Byte_t century;
   Byte_t year;
   Byte_t month;
   Byte_t day;
   Byte_t hour;
   Byte_t minute;
   Byte_t second;
   Byte_t sec_fractions;
} AbsoluteTime;

typedef Word_t OperationalState;

#define OS_DISABLED                       0
#define OS_ENABLED                        1
#define OS_NOT_AVAILABLE                  2

typedef struct _tagoctet_string
{
   Word_t length;
   Byte_t  value[1]; /* first element of the array */
} octet_string;

typedef struct _tagSystemModel
{
   octet_string manufacturer;
   octet_string model_number;
} SystemModel;

#define UNSPECIFIED                       0
#define SERIAL_NUMBER                     1
#define PART_NUMBER                       2
#define HW_REVISION                       3
#define SW_REVISION                       4
#define FW_REVISION                       5
#define PROTOCOL_REVISION                 6
#define PROD_SPEC_GMDN                    7

typedef struct _tagProdSpecEntry
{
   Word_t       spec_type;
   PrivateOid   component_id;
   octet_string prod_spec;
} ProdSpecEntry;

typedef struct _tagProductionSpec
{
   Word_t        count;
   Word_t        length;
   ProdSpecEntry value[1]; /* first element of the array */
} ProductionSpec;

typedef Word_t PowerStatus;

#define ON_MAINS                          0x8000
#define ON_BATTERY                        0x4000
#define CHARGING_FULL                     0x0080
#define CHARGING_TRICKLE                  0x0040
#define CHARGING_OFF                      0x0020

typedef struct _tagBatMeasure
{
   FLOAT_Type value;
   OID_Type unit;
} BatMeasure;

typedef Word_t MeasurementStatus;

#define MS_INVALID                        0x8000
#define MS_QUESTIONABLE                   0x4000
#define MS_NOT_AVAILABLE                  0x2000
#define MS_CALIBRATION_ONGOING            0x1000
#define MS_TEST_DATA                      0x0800
#define MS_DEMO_DATA                      0x0400
#define MS_VALIDATED_DATA                 0x0080
#define MS_EARLY_INDICATION               0x0040
#define MS_MSMT_ONGOING                   0x0020

typedef struct _tagNuObsValue
{
   OID_Type          metric_id;
   MeasurementStatus state;
   OID_Type          unit_code;
   FLOAT_Type        value;
} NuObsValue;

typedef struct _tagNuObsValueCmp
{
   Word_t     count;
   Word_t     length;
   NuObsValue value[1]; /* first element of the array */
} NuObsValueCmp;

typedef struct _tagSampleType
{
   Byte_t sample_size;
   Byte_t significant_bits;
} SampleType;

#define SAMPLE_TYPE_SIGNIFICANT_BITS_SIGNED_SAMPLES 255

typedef Word_t SaFlags;

#define SMOOTH_CURVE                      0x8000
#define DELAYED_CURVE                     0x4000
#define STATIC_SCALE                      0x2000
#define SA_EXT_VAL_RANGE                  0x1000

typedef struct _tagSaSpec
{
   Word_t     array_size;
   SampleType sample_type;
   SaFlags    flags;
} SaSpec;

typedef struct _tagScaleRangeSpec8
{
   FLOAT_Type lower_absolute_value;
   FLOAT_Type upper_absolute_value;
   Byte_t      lower_scaled_value;
   Byte_t      upper_scaled_value;
} ScaleRangeSpec8;

typedef struct _tagScaleRangeSpec16
{
   FLOAT_Type lower_absolute_value;
   FLOAT_Type upper_absolute_value;
   Word_t     lower_scaled_value;
   Word_t     upper_scaled_value;
} ScaleRangeSpec16;

typedef struct _tagScaleRangeSpec32
{
   FLOAT_Type lower_absolute_value;
   FLOAT_Type upper_absolute_value;
   DWord_t     lower_scaled_value;
   DWord_t     upper_scaled_value;
} ScaleRangeSpec32;

#define OBJ_ID_CHOSEN                     0x0001
#define TEXT_STRING_CHOSEN                0x0002
#define BIT_STR_CHOSEN                    0x0010

typedef struct _tagEnumVal
{
   Word_t choice;
   Word_t length;
   union
   {
      OID_Type     enum_obj_id;
      octet_string enum_text_string;
      DWord_t       enum_bit_str; // BITS-32
   } u;
} EnumVal;

typedef struct _tagEnumObsValue
{
   OID_Type          metric_id;
   MeasurementStatus state;
   EnumVal           value;
} EnumObsValue;

typedef struct _tagAttrValMapEntry
{
   OID_Type attribute_id;
   Word_t   attribute_len;
} AttrValMapEntry;

typedef struct _tagAttrValMap
{
   Word_t          count;
   Word_t          length;
   AttrValMapEntry value[1]; /* first element of the array */
} AttrValMap;

typedef struct _tagHandleAttrValMapEntry
{
   Handle     obj_handle;
   AttrValMap attr_val_map;
} HandleAttrValMapEntry;

typedef Word_t ConfirmMode;

#define UNCONFIRMED                                0x0000
#define CONFIRMED                                  0x0001

typedef struct _tagHandleAttrValMap
{
   Word_t                count;
   Word_t                length;
   HandleAttrValMapEntry value[1]; /* first element of the array */
} HandleAttrValMap;

typedef Word_t StoSampleAlg;

#define ST_ALG_NOS                                 0x0000
#define ST_ALG_MOVING_AVERAGE                      0x0001
#define ST_ALG_RECURSIVE_                          0x0002
#define ST_ALG_MIN_PICK                            0x0003
#define ST_ALG_MAX_PICK                            0x0004
#define ST_ALG_MEDIAN                              0x0005
#define ST_ALG_TRENDED                             0x0200
#define ST_ALG_NO_DOWNSAMPLING                     0x0400

typedef struct _tagSetTimeInvoke
{
   AbsoluteTime date_time;
   FLOAT_Type    accuracy;
} SetTimeInvoke;

typedef struct _tagSegmIdList
{
   Word_t     count;
   Word_t     length;
   InstNumber value[1]; /* first element of the array */
} SegmIdList;

typedef struct _tagAbsTimeRange
{
   AbsoluteTime from_time;
   AbsoluteTime to_time;
} AbsTimeRange;

typedef struct _tagSegmentInfo
{
   InstNumber    seg_inst_no;
   AttributeList seg_info;
} SegmentInfo;

typedef struct _tagSegmentInfoList
{
   Word_t      count;
   Word_t      length;
   SegmentInfo value[1]; /* first element of the array */
} SegmentInfoList;

#define ALL_SEGMENTS_CHOSEN                        0x0001
#define SEGM_ID_LIST_CHOSEN                        0x0002
#define ABS_TIME_RANGE_CHOSEN                      0x0003

typedef struct _tagSegmSelection
{
   Word_t choice;
   Word_t length;
   union
   {
      Word_t       all_segments;
      SegmIdList   segm_id_list;
      AbsTimeRange abs_time_range;
   } u;
} SegmSelection;

typedef Word_t PMStoreCapab;

#define PMSC_VAR_NO_OF_SEGM                        0x8000
#define PMSC_EPI_SEG_ENTRIES                       0x0800
#define PMSC_PERI_SEG_ENTRIES                      0x0400
#define PMSC_ABS_TIME_SELECT                       0x0200
#define PMSC_CLEAR_SEGM_BY_LIST_SUP                0x0100
#define PMSC_CLEAR_SEGM_BY_TIME_SUP                0x0080
#define PMSC_CLEAR_SEGM_REMOVE                     0x0040
#define PMSC_MULTI_PERSON                          0x0008

typedef Word_t SegmEntryHeader;

#define SEG_ELEM_HDR_ABSOLUTE_TIME                 0x8000
#define SEG_ELEM_HDR_RELATIVE_TIME                 0x4000
#define SEG_ELEM_HDR_HIRES_RELATIVE_TIME           0x2000

typedef struct _tagSegmEntryElem
{
   OID_Type   class_id;
   TYPE       metric_type;
   Handle     handle;
   AttrValMap attr_val_map;
} SegmEntryElem;

typedef struct _tagSegmEntryElemList
{
   Word_t        count;
   Word_t        length;
   SegmEntryElem value[1]; /* first element of the array */
} SegmEntryElemList;

typedef struct _tagPmSegmentEntryMap
{
   SegmEntryHeader   segm_entry_header;
   SegmEntryElemList segm_entry_elem_list;
} PmSegmentEntryMap;

typedef struct _tagSegmElemStaticAttrEntry
{
   OID_Type      class_id;
   TYPE          metric_type;
   AttributeList attribute_list;
} SegmElemStaticAttrEntry;

typedef struct _tagPmSegmElemStaticAttrList
{
   Word_t                  count;
   Word_t                  length;
   SegmElemStaticAttrEntry value[1]; /* first element of the array */
} PmSegmElemStaticAttrList;

typedef struct _tagTrigSegmDataXferReq
{
   InstNumber seg_inst_no;
} TrigSegmDataXferReq;

typedef Word_t TrigSegmXferRsp;

#define TSXR_SUCCESSFUL                         0
#define TSXR_FAIL_NO_SUCH_SEGMENT               1
#define TSXR_FAIL_SEGM_TRY_LATER                2
#define TSXR_FAIL_SEGM_EMPTY                    3
#define TSXR_FAIL_OTHER                         512

typedef struct _tagTrigSegmDataXferRsp
{
   InstNumber      seg_inst_no;
   TrigSegmXferRsp trig_segm_xfer_rsp;
} TrigSegmDataXferRsp;

typedef Word_t SegmEvtStatus;

#define SEVTSTA_FIRST_ENTRY                     0x8000
#define SEVTSTA_LAST_ENTRY                      0x4000
#define SEVTSTA_AGENT_ABORT                     0x0800
#define SEVTSTA_MANAGER_CONFIRM                 0x0080
#define SEVTSTA_MANAGER_ABORT                   0x0008

typedef struct _tagSegmDataEventDescr
{
   InstNumber    segm_instance;
   DWord_t        segm_evt_entry_index;
   DWord_t        segm_evt_entry_count;
   SegmEvtStatus segm_evt_status;
} SegmDataEventDescr;

typedef struct _tagSegmentDataEvent
{
   SegmDataEventDescr segm_data_event_descr;
   octet_string       segm_data_event_entries;
} SegmentDataEvent;

typedef struct _tagSegmentDataResult
{
   SegmDataEventDescr segm_data_event_descr;
} SegmentDataResult;

typedef Word_t SegmStatType;

#define SEGM_STAT_TYPE_MINIMUM                 1
#define SEGM_STAT_TYPE_MAXIMUM                 2
#define SEGM_STAT_TYPE_AVERAGE                 3

typedef struct _tagSegmentStatisticEntry
{
   SegmStatType segm_stat_type;
   octet_string segm_stat_entry;
} SegmentStatisticEntry;

typedef struct _tagSegmentStatistics
{
   Word_t                count;
   Word_t                length;
   SegmentStatisticEntry value[1]; /* first element of the array */
} SegmentStatistics;

typedef struct _tagObservationScan
{
   Handle        obj_handle;
   AttributeList attributes;
} ObservationScan;

typedef OID_Type TimeProtocolId;
typedef DWord_t AssociationVersion;

#define ASSOC_VERSION1                          0x80000000

typedef DWord_t ProtocolVersion;

#define PROTOCOL_VERSION1                       0x80000000

typedef Word_t EncodingRules;

#define MDER                                    0x8000
#define XER                                     0x4000
#define PER                                     0x2000

typedef struct _tagUUID_Ident
{
   Byte_t value[16];
} UUID_Ident;
typedef Word_t DataProtoId;

#define DATA_PROTO_ID_20601                     20601
#define DATA_PROTO_ID_EXTERNAL                  65535

typedef struct _tagDataProto
{
   DataProtoId data_proto_id;
   Any         data_proto_info;
} DataProto;

typedef struct _tagDataProtoList
{
   Word_t    count;
   Word_t    length;
   DataProto value[1]; /* first element of the array */
} DataProtoList;

typedef struct _tagAARQ_apdu
{
   AssociationVersion assoc_version;
   DataProtoList      data_proto_list;
} AARQ_apdu;

typedef Word_t Associate_result;

#define ACCEPTED                                0
#define REJECTED_PERMANENT                      1
#define REJECTED_TRANSIENT                      2
#define ACCEPTED_UNKNOWN_CONFIG                 3
#define REJECTED_NO_COMMON_PROTOCOL             4
#define REJECTED_NO_COMMON_PARAMETER            5
#define REJECTED_UNKNOWN                        6
#define REJECTED_UNAUTHORIZED                   7
#define REJECTED_UNSUPPORTED_ASSOC_VERSION      8

typedef struct _tagAARE_apdu
{
   Associate_result result;
   DataProto        selected_data_proto;
} AARE_apdu;

typedef Word_t Release_request_reason;

#define RELEASE_REQUEST_REASON_NORMAL           0

typedef struct _tagRLRQ_apdu
{
   Release_request_reason reason;
} RLRQ_apdu;

typedef Word_t Release_response_reason;

#define RELEASE_RESPONSE_REASON_NORMAL          0

typedef struct _tagRLRE_apdu
{
   Release_response_reason reason;
} RLRE_apdu;

typedef Word_t Abort_reason;

#define ABORT_REASON_UNDEFINED                  0
#define ABORT_REASON_BUFFER_OVERFLOW            1
#define ABORT_REASON_RESPONSE_TIMEOUT           2
#define ABORT_REASON_CONFIGURATION_TIMEOUT      3

typedef struct _tagABRT_apdu
{
   Abort_reason reason;
} ABRT_apdu;

typedef octet_string PRST_apdu;
typedef Word_t       InvokeIDType;

typedef struct _tagEventReportArgumentSimple
{
   Handle       obj_handle;
   RelativeTime event_time;
   OID_Type     event_type;
   Any          event_info;
} EventReportArgumentSimple;

typedef struct _tagGetArgumentSimple
{
   Handle          obj_handle;
   AttributeIdList attribute_id_list;
} GetArgumentSimple;

typedef Word_t ModifyOperator;

#define REPLACE                  0
#define ADD_VALUES               1
#define REMOVE_VALUES            2
#define SET_TO_DEFAULT           3

typedef struct _tagAttributeModEntry
{
   ModifyOperator modify_operator;
   AVA_Type       attribute;
} AttributeModEntry;

typedef struct _tagModificationList
{
   Word_t            count;
   Word_t            length;
   AttributeModEntry value[1]; /* first element of the array */
} ModificationList;

typedef struct _tagSetArgumentSimple
{
   Handle           obj_handle;
   ModificationList modification_list;
} SetArgumentSimple;

typedef struct _tagActionArgumentSimple
{
   Handle   obj_handle;
   OID_Type action_type;
   Any      action_info_args;
} ActionArgumentSimple;

typedef struct _tagEventReportResultSimple
{
   Handle       obj_handle;
   RelativeTime currentTime;
   OID_Type     event_type;
   Any          event_reply_info;
} EventReportResultSimple;

typedef struct _tagGetResultSimple
{
   Handle        obj_handle;
   AttributeList attribute_list;
} GetResultSimple;

typedef struct _tagTypeVer
{
   OID_Type type;
   Word_t   version;
} TypeVer;

typedef struct _tagTypeVerList
{
   Word_t  count;
   Word_t  length;
   TypeVer value[1]; /* first element of the array */
} TypeVerList;

typedef struct _tagSetResultSimple
{
   Handle        obj_handle;
   AttributeList attribute_list;
} SetResultSimple;

typedef struct _tagActionResultSimple
{
   Handle   obj_handle;
   OID_Type action_type;
   Any      action_info_args;
} ActionResultSimple;

typedef Word_t Error;

#define NO_SUCH_OBJECT_INSTANCE                    1
#define ACCESS_DENIED                              2
#define NO_SUCH_ACTION                             9
#define INVALID_OBJECT_INSTANCE                    17
#define PROTOCOL_VIOLATION                         23
#define NOT_ALLOWED_BY_OBJECT                      24
#define ACTION_TIMED_OUT                           25
#define ACTION_ABORTED                             26

typedef struct _tagErrorResult
{
   Error error_value;
   Any   parameter;
} ErrorResult;

typedef Word_t RorjProblem;

#define UNRECOGNIZED_APDU                          0
#define BADLY_STRUCTURED_APDU                      2
#define UNRECOGNIZED_OPERATION                     101
#define RESOURCE_LIMITATION                        103
#define UNEXPECTED_ERROR                           303

typedef struct _tagRejectResult
{
   RorjProblem problem;
} RejectResult;

#define ROIV_CMIP_EVENT_REPORT_CHOSEN              0x0100
#define ROIV_CMIP_CONFIRMED_EVENT_REPORT_CHOSEN    0x0101
#define ROIV_CMIP_GET_CHOSEN                       0x0103
#define ROIV_CMIP_SET_CHOSEN                       0x0104
#define ROIV_CMIP_CONFIRMED_SET_CHOSEN             0x0105
#define ROIV_CMIP_ACTION_CHOSEN                    0x0106
#define ROIV_CMIP_CONFIRMED_ACTION_CHOSEN          0x0107
#define RORS_CMIP_CONFIRMED_EVENT_REPORT_CHOSEN    0x0201
#define RORS_CMIP_GET_CHOSEN                       0x0203
#define RORS_CMIP_CONFIRMED_SET_CHOSEN             0x0205
#define RORS_CMIP_CONFIRMED_ACTION_CHOSEN          0x0207
#define ROER_CHOSEN                                0x0300
#define RORJ_CHOSEN                                0x0400

typedef struct _tagDATA_apdu
{
   InvokeIDType invoke_id;
   struct
   {
      Word_t choice;
      Word_t length;
      union
      {
         EventReportArgumentSimple roiv_cmipEventReport;
         EventReportArgumentSimple roiv_cmipConfirmedEventReport;
         GetArgumentSimple         roiv_cmipGet;
         SetArgumentSimple         roiv_cmipSet;
         SetArgumentSimple         roiv_cmipConfirmedSet;
         ActionArgumentSimple      roiv_cmipAction;
         ActionArgumentSimple      roiv_cmipConfirmedAction;
         EventReportResultSimple   rors_cmipConfirmedEventReport;
         GetResultSimple           rors_cmipGet;
         SetResultSimple           rors_cmipConfirmedSet;
         ActionResultSimple        rors_cmipConfirmedAction;
         ErrorResult               roer;
         RejectResult              rorj;
      } u;
   } choice;
} DATA_apdu;

#define AARQ_CHOSEN                                0xE200
#define AARE_CHOSEN                                0xE300
#define RLRQ_CHOSEN                                0xE400
#define RLRE_CHOSEN                                0xE500
#define ABRT_CHOSEN                                0xE600
#define PRST_CHOSEN                                0xE700

typedef struct _tagAPDU
{
   Word_t choice;
   Word_t length;
   union
   {
      AARQ_apdu aarq;
      AARE_apdu aare;
      RLRQ_apdu rlrq;
      RLRE_apdu rlre;
      ABRT_apdu abrt;
      PRST_apdu prst;
   } u;
} APDU;

typedef DWord_t NomenclatureVersion;

#define NOM_VERSION1                               0x80000000

typedef DWord_t FunctionalUnits;

#define FUN_UNITS_UNIDIRECTIONAL                   0x80000000
#define FUN_UNITS_HAVETESTCAP                      0x40000000
#define FUN_UNITS_CREATETESTASSOC                  0x20000000

typedef DWord_t SystemType;

#define SYS_TYPE_MANAGER                           0x80000000
#define SYS_TYPE_AGENT                             0x00800000

typedef Word_t ConfigId;

#define MANAGER_CONFIG_RESPONSE                    0x0000
#define STANDARD_CONFIG_START                      0x0001
#define STANDARD_CONFIG_END                        0x3FFF
#define EXTENDED_CONFIG_START                      0x4000
#define EXTENDED_CONFIG_END                        0x7FFF
#define RESERVED_START                             0x8000
#define RESERVED_END                               0xFFFF

typedef Word_t DataReqModeFlags;

typedef struct _tagDataReqModeCapab
{
   DataReqModeFlags data_req_mode_flags;
   Byte_t            data_req_init_agent_count;
   Byte_t            data_req_init_manager_count;
} DataReqModeCapab;


#define DATA_REQ_SUPP_STOP                         0x8000
#define DATA_REQ_SUPP_SCOPE_ALL                    0x0800
#define DATA_REQ_SUPP_SCOPE_CLASS                  0x0400
#define DATA_REQ_SUPP_SCOPE_HANDLE                 0x0200
#define DATA_REQ_SUPP_MODE_SINGLE_RSP              0x0080
#define DATA_REQ_SUPP_MODE_TIME_PERIOD             0x0040
#define DATA_REQ_SUPP_MODE_TIME_NO_LIMIT           0x0020
#define DATA_REQ_SUPP_PERSON_ID                    0x0010
#define DATA_REQ_SUPP_INIT_AGENT                   0x0001

typedef struct _tagPhdAssociationInformation
{
   ProtocolVersion     protocolVersion;
   EncodingRules       encodingRules;
   NomenclatureVersion nomenclatureVersion;
   FunctionalUnits     functionalUnits;
   SystemType          systemType;
   octet_string        system_id;
   Word_t              dev_config_id;
   DataReqModeCapab    data_req_mode_capab;
   AttributeList       optionList;
} PhdAssociationInformation;

typedef struct _tagManufSpecAssociationInformation
{
   UUID_Ident data_proto_id_ext;
   Any        data_proto_info_ext;
} ManufSpecAssociationInformation;

typedef Word_t MdsTimeCapState;

#define MDS_TIME_CAPAB_REAL_TIME_CLOCK                0x8000
#define MDS_TIME_CAPAB_SET_CLOCK                      0x4000
#define MDS_TIME_CAPAB_RELATIVE_TIME                  0x2000
#define MDS_TIME_CAPAB_HIGH_RES_RELATIVE_TIME         0x1000
#define MDS_TIME_CAPAB_SYNC_ABS_TIME                  0x0800
#define MDS_TIME_CAPAB_SYNC_REL_TIME                  0x0400
#define MDS_TIME_CAPAB_SYNC_HI_RES_RELATIVE_TIME      0x0200
#define MDS_TIME_STATE_ABS_TIME_SYNCED                0x0080
#define MDS_TIME_STATE_REL_TIME_SYNCED                0x0040
#define MDS_TIME_STATE_HI_RES_RELATIVE_TIME_SYNCED    0x0020
#define MDS_TIME_MGR_SET_TIME                         0x0010

typedef struct _tagMdsTimeInfo
{
   MdsTimeCapState mds_time_cap_state;
   TimeProtocolId  time_sync_protocol;
   RelativeTime    time_sync_accuracy;
   Word_t          time_resolution_abs_time;
   Word_t          time_resolution_rel_time;
   DWord_t          time_resolution_high_res_time;
} MdsTimeInfo;

typedef octet_string EnumPrintableString;
typedef Word_t PersonId;

#define UNKNOWN_PERSON_ID                 0xFFFF

typedef Word_t MetricSpecSmall;

#define MSS_AVAIL_INTERMITTENT            0x8000
#define MSS_AVAIL_STORED_DATA             0x4000
#define MSS_UPD_APERIODIC                 0x2000
#define MSS_MSMT_APERIODIC                0x1000
#define MSS_MSMT_PHYS_EV_ID               0x0800
#define MSS_MSMT_BTB_METRIC               0x0400
#define MSS_ACC_MANAGER_INITIATED         0x0080
#define MSS_ACC_AGENT_INITIATED           0x0040
#define MSS_CAT_MANUAL                    0x0008
#define MSS_CAT_SETTING                   0x0004
#define MSS_CAT_CALCULATION               0x0002

#define MS_STRUCT_SIMPLE                  0
#define MS_STRUCT_COMPOUND                1
#define MS_STRUCT_RESERVED                2
#define MS_STRUCT_COMPOUND_FIX            3

typedef struct _tagMetricStructureSmall
{
   Byte_t ms_struct;
   Byte_t ms_comp_no;
} MetricStructureSmall;

typedef struct _tagMetricIdList
{
   Word_t   count;
   Word_t   length;
   OID_Type value[1]; /* first element of the array */
} MetricIdList;

typedef struct _tagSupplementalTypeList
{
   Word_t count;
   Word_t length;
   TYPE   value[1]; /* first element of the array */
} SupplementalTypeList;

typedef struct _tagObservationScanList
{
   Word_t          count;
   Word_t          length;
   ObservationScan value[1]; /* first element of the array */
} ObservationScanList;

typedef struct _tagScanReportPerVar
{
   Word_t              person_id;
   ObservationScanList obs_scan_var;
} ScanReportPerVar;

typedef struct _tagScanReportPerVarList
{
   Word_t           count;
   Word_t           length;
   ScanReportPerVar value[1]; /* first element of the array */
} ScanReportPerVarList;

typedef Word_t DataReqId;

#define DATA_REQ_ID_MANAGER_INITIATED_MIN    0x0000
#define DATA_REQ_ID_MANAGER_INITIATED_MAX    0xEFFF
#define DATA_REQ_ID_AGENT_INITIATED          0xF000

typedef struct _tagScanReportInfoMPVar
{
   DataReqId            data_req_id;
   Word_t               scan_report_no;
   ScanReportPerVarList scan_per_var;
} ScanReportInfoMPVar;

typedef struct _tagObservationScanFixed
{
   Handle       obj_handle;
   octet_string obs_val_data;
} ObservationScanFixed;

typedef struct _tagObservationScanFixedList
{
   Word_t               count;
   Word_t               length;
   ObservationScanFixed value[1]; /* first element of the array */
} ObservationScanFixedList;

typedef struct _tagScanReportPerFixed
{
   Word_t                   person_id;
   ObservationScanFixedList obs_scan_fix;
} ScanReportPerFixed;

typedef struct _tagScanReportPerFixedList
{
   Word_t             count;
   Word_t             length;
   ScanReportPerFixed value[1]; /* first element of the array */
} ScanReportPerFixedList;

typedef struct _tagScanReportInfoMPFixed
{
   DataReqId              data_req_id;
   Word_t                 scan_report_no;
   ScanReportPerFixedList scan_per_fixed;
} ScanReportInfoMPFixed;

typedef struct _tagScanReportInfoVar
{
   DataReqId           data_req_id;
   Word_t              scan_report_no;
   ObservationScanList obs_scan_var;
} ScanReportInfoVar;

typedef struct _tagScanReportInfoFixed
{
   DataReqId                data_req_id;
   Word_t                   scan_report_no;
   ObservationScanFixedList obs_scan_fixed;
} ScanReportInfoFixed;

typedef octet_string ObservationScanGrouped;

typedef struct _tagScanReportInfoGroupedList
{
   Word_t                 count;
   Word_t                 length;
   ObservationScanGrouped value[1]; /* first element of the array */
} ScanReportInfoGroupedList;

typedef struct _tagScanReportInfoGrouped
{
   Word_t                    data_req_id;
   Word_t                    scan_report_no;
   ScanReportInfoGroupedList obs_scan_grouped;
} ScanReportInfoGrouped;

typedef struct _tagScanReportPerGrouped
{
   PersonId               person_id;
   ObservationScanGrouped obs_scan_grouped;
} ScanReportPerGrouped;

typedef struct _tagScanReportPerGroupedList
{
   Word_t               count;
   Word_t               length;
   ScanReportPerGrouped value[1]; /* first element of the array */
} ScanReportPerGroupedList;

typedef struct _tagScanReportInfoMPGrouped
{
   Word_t                   data_req_id;
   Word_t                   scan_report_no;
   ScanReportPerGroupedList scan_per_grouped;
} ScanReportInfoMPgrouped;

typedef struct _tagConfigObject
{
   OID_Type      obj_class;
   Handle        obj_handle;
   AttributeList attributes;
} ConfigObject;

typedef struct _tagConfigObjectList
{
   Word_t       count;
   Word_t       length;
   ConfigObject value[1]; /* first element of the array */
} ConfigObjectList;

typedef struct _tagConfigReport
{
   ConfigId         config_report_id;
   ConfigObjectList config_obj_list;
} ConfigReport;

typedef Word_t ConfigResult;

#define ACCEPTED_CONFIG                      0x0000
#define UNSUPPORTED_CONFIG                   0x0001
#define STANDARD_CONFIG_UNKNOWN              0x0002

typedef struct _tagConfigReportRsp
{
   ConfigId config_report_id;
   ConfigResult config_result;
} ConfigReportRsp;

typedef Word_t DataReqMode;

#define DATA_REQ_START_STOP                  0x8000
#define DATA_REQ_CONTINUATION                0x4000
#define DATA_REQ_SCOPE_ALL                   0x0800
#define DATA_REQ_SCOPE_TYPE                  0x0400
#define DATA_REQ_SCOPE_HANDLE                0x0200
#define DATA_REQ_MODE_SINGLE_RSP             0x0080
#define DATA_REQ_MODE_TIME_PERIOD            0x0040
#define DATA_REQ_MODE_TIME_NO_LIMIT          0x0020
#define DATA_REQ_MODE_DATA_REQ_PERSON_ID     0x0008

typedef struct _tagHANDLEList
{
   Word_t count;
   Word_t length;
   Handle value[1]; /* first element of the array */
} HANDLEList;

typedef struct _tagDataRequest
{
   DataReqId    data_req_id;
   DataReqMode  data_req_mode;
   RelativeTime data_req_time;
   Word_t       DataRequest_data_req_person_id;
   OID_Type     data_req_class;
   HANDLEList   data_req_obj_handle_list;
} DataRequest;

typedef Word_t DataReqResult;

#define DATA_REQ_RESULT_NO_ERROR                      0
#define DATA_REQ_RESULT_UNSPECIFIC_ERROR              1
#define DATA_REQ_RESULT_NO_STOP_SUPPORT               2
#define DATA_REQ_RESULT_NO_SCOPE_ALL_SUPPORT          3
#define DATA_REQ_RESULT_NO_SCOPE_CLASS_SUPPORT        4
#define DATA_REQ_RESULT_NO_SCOPE_HANDLE_SUPPORT       5
#define DATA_REQ_RESULT_NO_MODE_SINGLE_RSP_SUPPORT    6
#define DATA_REQ_RESULT_NO_MODE_TIME_PERIOD_SUPPORT   7
#define DATA_REQ_RESULT_NO_MODE_TIME_NO_LIMIT_SUPPORT 8
#define DATA_REQ_RESULT_NO_PERSON_ID_SUPPORT          9
#define DATA_REQ_RESULT_UNKNOWN_PERSON_ID             11
#define DATA_REQ_RESULT_UNKNOWN_CLASS                 12
#define DATA_REQ_RESULT_UNKNOWN_HANDLE                13
#define DATA_REQ_RESULT_UNSUPP_SCOPE                  14
#define DATA_REQ_RESULT_UNSUPP_MODE                   15
#define DATA_REQ_RESULT_INIT_MANAGER_OVERFLOW         16
#define DATA_REQ_RESULT_CONTINUATION_NOT_SUPPORTED    17
#define DATA_REQ_RESULT_INVALID_REQ_ID                18

typedef struct _tagDataResponse
{
   RelativeTime  rel_time_stamp;
   DataReqResult data_req_result;
   OID_Type      event_type;
   Any           event_info;
} DataResponse;

typedef FLOAT_Type SimpleNuObsValue;

typedef struct _tagSimpleNuObsValueCmp
{
   Word_t           count;
   Word_t           length;
   SimpleNuObsValue value[1]; /* first element of the array */
} SimpleNuObsValueCmp;

typedef SFLOAT_Type BasicNuObsValue;

typedef struct _tagBasicNuObsValueCmp
{
   Word_t          count;
   Word_t          length;
   BasicNuObsValue value[1]; /* first element of the array */
} BasicNuObsValueCmp;

#endif /* PHD_TYPES */


