/*****< IEEE_11073.c >*********************************************************/
/*      Copyright 2001 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  IEEE_11073 - 11073 Implementation Information Functions.                  */
/*                                                                            */
/*  Author:  Tim Thomas                                                       */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   09/19/10  T. Thomas      Initial creation.                               */
/******************************************************************************/
#include <stdio.h>
#include <stddef.h>
#include "SS1BTPM.h"
#include "IEEE_11073.h"

#define STRUCTURE_HEADER_SIZE                         (WORD_SIZE * 2)

typedef enum
{
  csDISCONNECTED,
  csDISASSOCIATING,
  csUNASSOCIATED,
  csASSOCIATING,
  csSENDING_CONFIG,
  csWAITING_APPROVAL,
  csOPERATING
} ConnectionState_t;

int ProcessAssociationRequest(int Length, AARQ_apdu *APDU, Assoc_Info_t *AssocInfo)
{
   int            ret_val = -2;
   DWord_t        Version;
   int            NumProtocols;
   int            Len;
   DataProtoList *DataProtocolList;
   DataProto     *Protocol;

   Version = READ_UNALIGNED_DWORD_BIG_ENDIAN(&(APDU->assoc_version));
   if(Version == ASSOC_VERSION1)
   {
      Length -= DWORD_SIZE;
      if(Length >= STRUCTURE_HEADER_SIZE)
      {
         DataProtocolList = (DataProtoList *)&(APDU->data_proto_list);
         if(DataProtocolList)
         {
            NumProtocols = READ_UNALIGNED_WORD_BIG_ENDIAN(&(DataProtocolList->count));
            Len          = READ_UNALIGNED_WORD_BIG_ENDIAN(&(DataProtocolList->length));
            Length      -= STRUCTURE_HEADER_SIZE;
            if(Length >= Len)
            {
               Protocol = (DataProto *)&(DataProtocolList->value);
               while((Length) && (NumProtocols--))
               {
                  ret_val = READ_UNALIGNED_WORD_BIG_ENDIAN(&(Protocol->data_proto_id));
                  if(ret_val != DATA_PROTO_ID_20601)
                  {
                     Len       = (READ_UNALIGNED_WORD_BIG_ENDIAN(&(Protocol->data_proto_info.length)) + STRUCTURE_HEADER_SIZE);
                     Length   -= Len;
                     Protocol  = (DataProto *)(((Byte_t *)Protocol) + Len);
                  }
                  else
                  {
                     AssocInfo->EncodingRules  = READ_UNALIGNED_WORD_BIG_ENDIAN(&((PhdAssociationInformation *)(Protocol->data_proto_info.value))->encodingRules);
                     AssocInfo->DeviceConfigID = READ_UNALIGNED_WORD_BIG_ENDIAN(&((PhdAssociationInformation *)(Protocol->data_proto_info.value))->dev_config_id);
                     AssocInfo->SystemType     = READ_UNALIGNED_DWORD_BIG_ENDIAN(&((PhdAssociationInformation *)(Protocol->data_proto_info.value))->systemType);
                     AssocInfo->SystemIDLength = READ_UNALIGNED_WORD_BIG_ENDIAN(&((PhdAssociationInformation *)(Protocol->data_proto_info.value))->system_id.length);
                     AssocInfo->DeviceConfigID = READ_UNALIGNED_WORD_BIG_ENDIAN(AssocInfo->SystemID + AssocInfo->SystemIDLength);
                     BTPS_MemCopy(AssocInfo->SystemID, (((PhdAssociationInformation *)(Protocol->data_proto_info.value))->system_id.value), EUI_64_SIZE);
                  }
               }
            }
         }
      }
   }

   return(ret_val);
}

int ParseEventReport(PHD_Event_Report_t *EventReport, Event_Info_t *EventInfo)
{
   int                  ret_val;
//   Word_t               ReportID;
//   Word_t               Count;
//   Word_t               Length;
   Word_t               Handle;
   DWord_t              EventTime;
   Word_t               EventType;
//   PHD_Config_Report_t *ConfigReport;
   PHD_Observation_t   *Observation;

   if(EventReport)
   {
      Handle    = READ_UNALIGNED_WORD_BIG_ENDIAN(&(EventReport->Handle));
      EventTime = READ_UNALIGNED_DWORD_BIG_ENDIAN(&(EventReport->EventTime));
      EventType = READ_UNALIGNED_WORD_BIG_ENDIAN(&(EventReport->EventType));

      if(EventInfo)
      {
         EventInfo->EventHandle = Handle;
         EventInfo->EventType   = EventType;
         EventInfo->CurrentTime = EventTime;
      }
      switch(EventType)
      {
         case MDC_NOTI_CONFIG:
//            ConfigReport = (PHD_Config_Report_t *)(EventReport->EventInfo);
//            ReportID     = READ_UNALIGNED_WORD_BIG_ENDIAN(&(ConfigReport->ReportID));
//            Count        = READ_UNALIGNED_WORD_BIG_ENDIAN(&(ConfigReport->Count));
//            Length       = READ_UNALIGNED_WORD_BIG_ENDIAN(&(ConfigReport->Length));
            break;
         case MDC_NOTI_SCAN_REPORT_FIXED:
            Observation = ((PHD_Fixed_Scan_Report_t *)&(EventReport->EventInfo))->Observation;
            if(EventInfo->Service == ROIV_CMIP_EVENT_REPORT_CHOSEN)
            {
               EventInfo->Data[0] = READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Data[0]));
               EventInfo->Data[1] = READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Data[7]));
               EventInfo->NumData = 2;
            }
            if(EventInfo->Service == ROIV_CMIP_CONFIRMED_EVENT_REPORT_CHOSEN)
            {
               if(READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Handle)) == 1)
               {
                  EventInfo->Data[0] = READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Data[2]));
                  EventInfo->Data[1] = READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Data[3]));
                  EventInfo->Data[2] = READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Data[4]));
                  EventInfo->NumData = 3;
               }
               if(READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Handle)) == 2)
               {
                  EventInfo->Data[0] = READ_UNALIGNED_WORD_BIG_ENDIAN(&(Observation->Data[0]));
                  EventInfo->NumData = 1;
               }
            }
            break;
      }
   }
   else
      ret_val = -1;

   return(ret_val);
}

int FormatEventResponse(PHD_Event_Rsp_t *APDU, Event_Info_t *EventInfo)
{
   int ret_val;

   if((APDU) && (EventInfo))
   {
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->Choice), PRST_CHOSEN);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->ChoiceLength), PHD_EVENT_RSP_CHIOCE_LENGTH);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->DataLength), PHD_EVENT_RSP_DATA_LENGTH);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->InvokeID), EventInfo->InvokeID);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->Service), EventInfo->Service+0x100);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->ReportLength), PHD_EVENT_RSP_REPORT_LENGTH);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->Handle), EventInfo->EventHandle);
     ASSIGN_HOST_DWORD_TO_BIG_ENDIAN_UNALIGNED_DWORD(&(APDU->RelativeTime), 0xFFFFFFFF);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->EventType), EventInfo->EventType);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->OptionsLength), 0);
     ret_val = PHD_EVENT_RSP_DATA_SIZE;
   }
   else
      ret_val = -1;

   return(ret_val);
}

int FormatAssociationResponse(PHD_Assoc_Rsp_t *APDU, Word_t Result, Assoc_Info_t *AssocInfo)
{
   int ret_val;

   if((APDU) && (AssocInfo))
   {
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->Choice), AARE_CHOSEN);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->ChoiceLength), (PHD_ASSOC_RSP_DATA_SIZE-DWORD_SIZE));
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->Result), Result);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->DataProtoID), DATA_PROTO_ID_20601);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->DataProtoInfoLength), PHD_DATA_PROTO_INFO_LENGTH);
     ASSIGN_HOST_DWORD_TO_BIG_ENDIAN_UNALIGNED_DWORD(&(APDU->ProtocolVersion), PROTOCOL_VERSION1);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->EncodingRules), MDER);
     ASSIGN_HOST_DWORD_TO_BIG_ENDIAN_UNALIGNED_DWORD(&(APDU->NomenclatureVersion), NOM_VERSION1);
     ASSIGN_HOST_DWORD_TO_BIG_ENDIAN_UNALIGNED_DWORD(&(APDU->FunctionalUnits), 0);
     ASSIGN_HOST_DWORD_TO_BIG_ENDIAN_UNALIGNED_DWORD(&(APDU->SystemType), SYS_TYPE_MANAGER);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->SystemIDLength), EUI_64_SIZE);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->DeviceConfigID), 0);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->DataReqModeCapab.Flags), 0);
     APDU->DataReqModeCapab.AgentCount   = 0;
     APDU->DataReqModeCapab.ManagerCount = 0;
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->OptionsCount), 0);
     ASSIGN_HOST_WORD_TO_BIG_ENDIAN_UNALIGNED_WORD(&(APDU->OptionsLength), 0);
     BTPS_MemCopy(APDU->SystemID, "\x00\x1B\x5C\x00\x00\x00\x00\x5E", EUI_64_SIZE);
     ret_val = PHD_ASSOC_RSP_DATA_SIZE;
   }
   else
      ret_val = -1;

   return(ret_val);
}

