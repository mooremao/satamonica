/*****< audiodecoder.h >*******************************************************/
/*      Copyright 2011 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/* AUDIODECODER  - Sample abstraction code to decode an SBC Audio Frame.      */
/*                                                                            */
/*  Author:  Greg Hensley                                                     */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   01/13/11  G. Hensley     Initial creation. (Based on LinuxDEVM)          */
/******************************************************************************/
#ifndef __AUDIODECODERH__
#define __AUDIODECODERH__

   /* The following function initializes the audio decoder. The first   */
   /* parameter is an optional configuration flags. This function will  */
   /* return zero on success and negative on error.                     */
int InitializeAudioDecoder(unsigned int BluetoothStackID, unsigned int ConfigFlags, BD_ADDR_t BD_ADDR);

   /* The following function is responsible for freeing all resources   */
   /* that were previously allocated for an audio decoder.              */
void CleanupAudioDecoder(void);

   /* The following function is used to process audio data. The first   */
   /* parameter is buffer to the raw SBC audio. The second parameter is */
   /* the length of the audio buffer. A negative value will be returned */
   /* on error                                                          */
int ProcessAudioData(BD_ADDR_t BD_ADDR, void *Buffer, unsigned int Length);

#endif

