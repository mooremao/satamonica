
ifndef CC
   CC = gcc
endif

INCLDDIRS = -I../../AVRCP/include    \
	    -I../../AVCTP/include    \
	    -I../../../include       \
	    -I../../../debug/include

ifndef SYSTEMLIBS
   SYSTEMLIBS = -lpthread -lm
endif
	
CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS) -fno-strict-aliasing

LDFLAGS = -L../../../lib -L../../AVCTP/lib -L../../AVRCP/lib -L../../../debug/lib $(GLOBLDFLAGS)

LDLIBS = -lSS1BTPS -lSS1BTAVC -lSS1BTAVR -lBTPSVEND -lSS1BTDBG $(SYSTEMLIBS) $(GLOBLDLIBS)

OBJS = LinuxAVR.o

.PHONY:
all: LinuxAVR

LinuxAVR: $(OBJS)

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm *.o
	-rm LinuxAVR

