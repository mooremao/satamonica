/*****< btvs.c >***************************************************************/
/*      Copyright 2011 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTVS - Vendor specific implementation of a set of vendor specific         */
/*         functions supported for a specific hardware platform.              */
/*                                                                            */
/*  Author:  Damon Lange                                                      */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   05/20/11  D. Lange       Initial creation.                               */
/*   04/24/14  S. Hishmeh     Updated for Revised 12/2013 WL18xx User's Guide.*/
/******************************************************************************/
#include "SS1BTPS.h"          /* Bluetopia API Prototypes/Constants.          */
#include "BTPSKRNL.h"         /* BTPS Kernel Prototypes/Constants.            */
#include "SS1BTVS.h"          /* Vendor Specific Prototypes/Constants.        */

   /* The following MACRO is a utility MACRO that exists to aid in      */
   /* converting an unsigned long in milliseconds to 2 Baseband slots.  */
#define CONVERT_TO_TWO_BASEBAND_SLOTS(_x)                         ((unsigned long)((((4000L * (_x)) / 500L) + 5L)/10L))

   /* Vendor Specific Command Opcodes.                                  */
#define VS_DRPB_SET_POWER_VECTOR_COMMAND_OPCODE                  ((Word_t)(0xFD82))
#define VS_HCILL_PARAMETERS_COMMAND_OPCODE                       ((Word_t)(0xFD2B))
#define VS_SLEEP_MODE_CONFIGURATIONS_COMMAND_OPCODE              ((Word_t)(0xFD0C))
#define VS_UPDATE_UART_HCI_BAUDRATE_COMMAND_OPCODE               ((Word_t)(0xFF36))
#define VS_WRITE_BD_ADDR_COMMAND_OPCODE                          ((Word_t)(0xFC06))
#define VS_WRITE_CODEC_CONFIG_COMMAND_OPCODE                     ((Word_t)(0xFD06))
#define VS_A3DP_OPEN_STREAM_COMMAND_OPCODE                       ((Word_t)(0xFD8C))
#define VS_A3DP_CLOSE_STREAM_COMMAND_OPCODE                      ((Word_t)(0xFD8D))
#define VS_A3DP_CODEC_CONFIGURATION_COMMAND_OPCODE               ((Word_t)(0xFD8E))
#define VS_A3DP_START_STREAM_COMMAND_OPCODE                      ((Word_t)(0xFD8F))
#define VS_A3DP_STOP_STREAM_COMMAND_OPCODE                       ((Word_t)(0xFD90))
#define VS_AVPR_ENABLE_COMMAND_OPCODE                            ((Word_t)(0xFD92))
#define VS_DRPB_ENABLE_RF_CALIBRATION_ENHANCED_COMMAND_OPCODE    ((Word_t)(0xFDFB))
#define VS_DRPB_TESTER_CON_TX_COMMAND_OPCODE                     ((Word_t)(0xFDCA))
#define VS_PACKET_TX_RX_COMMAND_OPCODE                           ((Word_t)(0xFDCC))
#define VS_CONTINUOUS_RX_COMMAND_OPCODE                          ((Word_t)(0xFDCB))
#define VS_DRPB_BER_METER_START_COMMAND_OPCODE                   ((Word_t)(0xFD8B))
#define VS_DRP_READ_BER_METER_RESULT_COMMAND_OPCODE              ((Word_t)(0xFD13))
#define VS_A3DP_SINK_OPEN_STREAM_COMMAND_OPCODE                  ((Word_t)(0xFD9A))
#define VS_A3DP_SINK_CLOSE_STREAM_COMMAND_OPCODE                 ((Word_t)(0xFD9B))
#define VS_A3DP_SINK_CODEC_CONFIGURATION_COMMAND_OPCODE          ((Word_t)(0xFD9C))
#define VS_A3DP_SINK_START_STREAM_COMMAND_OPCODE                 ((Word_t)(0xFD9D))
#define VS_A3DP_SINK_STOP_STREAM_COMMAND_OPCODE                  ((Word_t)(0xFD9E))

#define VS_COMMAND_OGF(_CommandOpcode)                           ((Byte_t)((_CommandOpcode) >> 10))
#define VS_COMMAND_OCF(_CommandOpcode)                           ((Word_t)((_CommandOpcode) & (0x3FF)))

   /* Internal Function Prototypes.                                     */
static int MapSendRawResults(int Result, Byte_t Status, Byte_t LengthResult, Byte_t *ReturnData);

   /* The following function is a utility function that is used to map  */
   /* the return results that we returned from Send Raw (ONLY for       */
   /* commands that return command complete event) to a negative error  */
   /* code.                                                             */
   /* * NOTE * As an internal function no check is done on the return   */
   /*          parameters.                                              */
static int MapSendRawResults(int Result, Byte_t Status, Byte_t LengthResult, Byte_t *ReturnData)
{
   int ret_val;

   /* Check to see if the API returned an error.                        */
   if((!Result) && (!Status) && (LengthResult >= 1))
   {
      /* Check to see if the chip returned an error.                    */
      if(ReturnData[0] == HCI_ERROR_CODE_NO_ERROR)
         ret_val = 0;
      else
         ret_val = BTPS_ERROR_CODE_HCI_STATUS_BASE - ReturnData[0];
   }
   else
      ret_val = BTPS_ERROR_DEVICE_HCI_ERROR;

   return(ret_val);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to change the Bluetooth UART for the Local */
   /* Bluetooth Device specified by the Bluetooth Protocol Stack that   */
   /* is specified by the Bluetooth Protocol Stack ID. The second       */
   /* parameter specifies the new baud rate to set.  This change        */
   /* encompasses both changing the speed of the Bluetooth chip (by     */
   /* issuing the correct commands) and then, if successful, informing  */
   /* the HCI Driver of the change (so the driver can communicate with  */
   /* the Bluetooth device at the new baud rate).  This function returns*/
   /* zero if successful or a negative return error code if there was   */
   /* an error.                                                         */
int BTPSAPI VS_Update_UART_HCI_Baudrate(unsigned int BluetoothStackID, DWord_t BaudRate)
{
   int                                 ret_val;
   Byte_t                              Length;
   Byte_t                              Status;
   Byte_t                              OGF;
   Word_t                              OCF;
   unsigned long                       LBaudRate;
   NonAlignedDWord_t                   _BaudRate;
   HCI_Driver_Reconfigure_Data_t       DriverReconfigureData;

   union
   {
      Byte_t                           Buffer[16];
      HCI_COMMReconfigureInformation_t COMMReconfigureInformation;
   } Data;

   /* Before continuing, make sure the input parameters appear to be    */
   /* semi-valid.                                                       */
   if((BluetoothStackID) && (BaudRate))
   {
      /* Write the Baud Rate.                                           */
      ASSIGN_HOST_DWORD_TO_LITTLE_ENDIAN_UNALIGNED_DWORD(&_BaudRate, BaudRate);

      LBaudRate = (unsigned long)BaudRate;

      /* Next, write the command to the device.                         */
      Length  = sizeof(Data.Buffer);
      OGF     = VS_COMMAND_OGF(VS_UPDATE_UART_HCI_BAUDRATE_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_UPDATE_UART_HCI_BAUDRATE_COMMAND_OPCODE);

      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, sizeof(NonAlignedDWord_t), (Byte_t *)&_BaudRate, &Status, &Length, Data.Buffer, TRUE);

      /* Map the return results into an error code.                     */
      if((!ret_val) && (!(ret_val = MapSendRawResults(ret_val, Status, Length, Data.Buffer))))
      {
         /* We were successful, now we need to change the baud rate of  */
         /* the driver.                                                 */
         BTPS_MemInitialize(&(Data.COMMReconfigureInformation), 0, sizeof(Data.COMMReconfigureInformation));

         Data.COMMReconfigureInformation.BaudRate         = LBaudRate;
         Data.COMMReconfigureInformation.ReconfigureFlags = HCI_COMM_RECONFIGURE_INFORMATION_RECONFIGURE_FLAGS_CHANGE_BAUDRATE;

         DriverReconfigureData.ReconfigureCommand         = HCI_COMM_DRIVER_RECONFIGURE_DATA_COMMAND_CHANGE_COMM_PARAMETERS;
         DriverReconfigureData.ReconfigureData            = (void *)&Data.COMMReconfigureInformation;

         ret_val = HCI_Reconfigure_Driver(BluetoothStackID, FALSE, &DriverReconfigureData);
      }
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to change the HCILL parameters that are    */
   /* used for the HCILL Low Power protocol for the Local Bluetooth     */
   /* Device specified by the Bluetooth Protocol Stack that is specified*/
   /* by the Bluetooth Protocol Stack ID.  The second is the            */
   /* InactivityTimeout on the Uart in ms.  If no traffic on Uart lines */
   /* after this time the Controller sends a Sleep Indication.  The     */
   /* third is the RetransmitTimeout (ms) for the Sleep Indication if no*/
   /* Sleep Acknowledgement (from the Host) is received.  The fourth is */
   /* the Controller RTS pulse width during Controller wakeup (specified*/
   /* in us).  This function returns zero if successful or a negative   */
   /* return error code if there was an error.                          */
int BTPSAPI VS_HCILL_Parameters(unsigned int BluetoothStackID, Word_t InactivityTimeout, Word_t RetransmitTimeout, Byte_t RTSPulseWidth)
{
   int    ret_val;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[8];
   Byte_t CommandBuffer[5];
   Byte_t OGF;
   Word_t OCF;

   /* Before continuing, make sure the input parameters appear to be    */
   /* semi-valid.                                                       */
   if(BluetoothStackID)
   {
      /* Format the command to send.                                    */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&(CommandBuffer[0]), CONVERT_TO_TWO_BASEBAND_SLOTS(InactivityTimeout));
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&(CommandBuffer[NON_ALIGNED_WORD_SIZE]), CONVERT_TO_TWO_BASEBAND_SLOTS(RetransmitTimeout));
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[NON_ALIGNED_WORD_SIZE*2]), RTSPulseWidth);

      /* Send the command to update the HCILL Parameters.               */
      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_HCILL_PARAMETERS_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_HCILL_PARAMETERS_COMMAND_OPCODE);

      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, (Byte_t)sizeof(CommandBuffer), (Byte_t *)(CommandBuffer), &Status, &Length, ReturnBuffer, TRUE);

      /* Map the return results into an error code.                     */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function implements the vendor-specific             */
   /* Sleep_Mode_Configurations command.  Refer to the Bluetooth (WL18xx*/
   /* and WL18xxQ) Vendor-Specific HCI Commands User's Guide for a      */
   /* description of the command's parameters.  This function returns 0 */
   /* on success and a negative error code otherwise.                   */
int BTPSAPI VS_Sleep_Mode_Configurations(unsigned int BluetoothStackID, Byte_t DeepSleepEnable)
{
   int    ret_val;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[8];
   Byte_t CommandBuffer[9];
   Byte_t OGF;
   Word_t OCF;

   /* Before continuing, make sure the input parameters appear to be    */
   /* semi-valid.                                                       */
   if(BluetoothStackID)
   {
      /* Format the command to send.                                    */
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[0]), 0x00);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[1]), DeepSleepEnable);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[2]), 0x00);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[3]), 0xFF);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[4]), 0xFF);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[5]), 0xFF);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[6]), 0xFF);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&(CommandBuffer[7]), 0x0000);

      /* Send the command to set the sleep mode configuration.          */
      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_SLEEP_MODE_CONFIGURATIONS_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_SLEEP_MODE_CONFIGURATIONS_COMMAND_OPCODE);

      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, sizeof(CommandBuffer), (Byte_t *)(CommandBuffer), &Status, &Length, ReturnBuffer, TRUE);

      /* Map the return results into an error code.                     */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to change the public Bluetooth Address     */
   /* (also known as a MAC address) for the local Bluetooth Device      */
   /* specified by the Bluetooth Protocol Stack ID.  The second         */
   /* parameter contains the Bluetooth Address to set.  This function   */
   /* returns zero if successful or a negative return error code if     */
   /* there was an error.                                               */
int BTPSAPI VS_Write_BD_Addr(unsigned int BluetoothStackID, BD_ADDR_t BD_ADDR)
{
   int          ret_val;
   Byte_t       Length;
   Byte_t       Status;
   Byte_t       ReturnBuffer[8];
   Byte_t       OGF;
   Word_t       OCF;
   unsigned int Index;

   /* Before continuing, make sure the input parameters appear to be    */
   /* semi-valid.                                                       */
   if(BluetoothStackID)
   {
      /* The TI chipset expects the BD_ADDR to be in big endian format  */
      /* so convert the little endian BD_ADDR to big endian.            */
      for(Index=0;Index<(BD_ADDR_SIZE/2);Index++)
      {
         Length                                     = ((Byte_t *)&BD_ADDR)[BD_ADDR_SIZE-Index-1];
         ((Byte_t *)&BD_ADDR)[BD_ADDR_SIZE-Index-1] = ((Byte_t *)&BD_ADDR)[Index];
         ((Byte_t *)&BD_ADDR)[Index]                = Length;
      }

      /* Send the command to set the sleep mode configuration.          */
      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_WRITE_BD_ADDR_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_WRITE_BD_ADDR_COMMAND_OPCODE);

      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, BD_ADDR_SIZE, (Byte_t *)(&BD_ADDR), &Status, &Length, ReturnBuffer, TRUE);

      /* Map the return results into an error code.                     */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function implements the vendor-specific             */
   /* VS_DRPb_Set_Power_Vector command.  Refer to the Bluetooth (WL18xx */
   /* and WL18xxQ) Vendor-Specific HCI Commands User's Guide for a      */
   /* description of the command's parameters.  This function returns 0 */
   /* on success and a negative error code otherwise.                   */
int BTPSAPI VS_DRPb_Set_Power_Vector(unsigned int BluetoothStackID, Byte_t ModulationType, Byte_t *PowerLevels, Byte_t PowerLevelIndex)
{
   const int    NumberPowerLevels = 8;
   int          ret_val;
   unsigned int Index;
   Byte_t       Length;
   Byte_t       Status;
   Byte_t       ReturnBuffer[8];
   Byte_t       CommandBuffer[BYTE_SIZE + (sizeof(PowerLevels[0]) * NumberPowerLevels) + BYTE_SIZE + WORD_SIZE];
   Byte_t       OGF;
   Word_t       OCF;

   /* Before continuing, make sure the input parameters appear to be    */
   /* semi-valid.                                                       */
   if(BluetoothStackID)
   {
      Index = 0;

      /* Format the command to send.                                    */
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[Index++]), ModulationType);
      BTPS_MemCopy(&(CommandBuffer[Index]), PowerLevels, (sizeof(PowerLevels[0]) * NumberPowerLevels));
      Index += (sizeof(PowerLevels[0]) * NumberPowerLevels);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&(CommandBuffer[Index++]), PowerLevelIndex);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&(CommandBuffer[Index]), 0x0000);

      /* Send the command to set the sleep mode configuration.          */
      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_DRPB_SET_POWER_VECTOR_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_DRPB_SET_POWER_VECTOR_COMMAND_OPCODE);

      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, sizeof(CommandBuffer), (Byte_t *)(CommandBuffer), &Status, &Length, ReturnBuffer, TRUE);

      /* Map the return results into an error code.                     */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function prototype represents the function which is */
   /* used to put the Local Bluetooth Device specified by the Bluetooth */
   /* Protocol Stack that is specified by the Bluetooth Protocol Stack  */
   /* ID into RF Signal Test Mode.  This function returns zero if       */
   /* successful or a negative return error code if there was an error. */
   /* * NOTE * Once the Local Bluetooth Device is in RF Signal Test Mode*/
   /*          it will remain in this mode until an HCI Reset is issued.*/
int BTPSAPI VS_Enable_RF_SIG_Test_Mode(unsigned int BluetoothStackID)
{
   int         ret_val;
   Byte_t      StatusResult;
   Condition_t ConditionStructure;

   /* Before continuing, make sure the input parameter appears to be    */
   /* semi-valid.                                                       */
   if(BluetoothStackID)
   {
      /* Send HCI Write Scan Enable Command.                            */
      if((!HCI_Write_Scan_Enable(BluetoothStackID, HCI_SCAN_ENABLE_INQUIRY_SCAN_ENABLED_PAGE_SCAN_ENABLED, &StatusResult)) && (StatusResult == HCI_ERROR_CODE_NO_ERROR))
      {
         /* Configure and send the HCI Set Event Filter Command.        */
         ConditionStructure.Condition.Connection_Setup_Filter_Type_All_Devices_Condition.Auto_Accept_Flag = HCI_AUTO_ACCEPT_FLAG_DO_AUTO_ACCEPT;

         if((!HCI_Set_Event_Filter(BluetoothStackID, HCI_FILTER_TYPE_CONNECTION_SETUP, HCI_FILTER_CONDITION_TYPE_CONNECTION_SETUP_ALL_DEVICES, ConditionStructure, &StatusResult)) && (StatusResult == HCI_ERROR_CODE_NO_ERROR))
         {
            /* Finally send the HCI Enable Device Under Test Command.   */
            if((!HCI_Enable_Device_Under_Test_Mode(BluetoothStackID, &StatusResult)) && (StatusResult == HCI_ERROR_CODE_NO_ERROR))
               ret_val = 0;
            else
               ret_val = BTPS_ERROR_DEVICE_HCI_ERROR;
         }
         else
            ret_val = BTPS_ERROR_DEVICE_HCI_ERROR;
      }
      else
         ret_val = BTPS_ERROR_DEVICE_HCI_ERROR;
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to write the codec interface       */
   /* parameters and the PCM and Frame Sync frequencies.  This function */
   /* returns zero if successful or a negative return error code if     */
   /* there was an error.                                               */
int BTPSAPI VS_PCM_Codec_Config(unsigned int BluetoothStackID, Word_t PCMFreq, DWord_t FSyncFreq)
{
   int    ret_val;
   Byte_t CommandBuffer[34];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if(BluetoothStackID)
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      /* PCM clock rate and direction                                   */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[0], PCMFreq);
      CommandBuffer[2] = 0x00;

      /* Frame-sync frequency, duty cycle, edge, and polarity           */
      ASSIGN_HOST_DWORD_TO_LITTLE_ENDIAN_UNALIGNED_DWORD(&CommandBuffer[3], FSyncFreq);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[7], 0x0001);

      /* 0 = rising edge, 1 = falling edge                              */
      CommandBuffer[9]  = 0x00;

      /* 0 = active high, 1 = active low                                */
      CommandBuffer[10] = 0x00;

      /* channel 1 data out size, offset, and edge                      */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[12], 16);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[14], 1);
      CommandBuffer[16] = 0x00;

      /* channel 1 data in size, offset, and edge                       */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[17], 16);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[19], 1);

      /* 0 = rising edge, 1 = falling edge                              */
      CommandBuffer[21] = 0x01;

      /* channel 2 data out size, offset, and edge                      */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[23], 16);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[25], 18);
      CommandBuffer[27] = 0x00;

      /* channel 2 data in size, offset, and edge                       */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[28], 16);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[30], 18);
      CommandBuffer[32] = 0x01;

      Length        = sizeof(ReturnBuffer);
      CommandLength = (Byte_t)sizeof(CommandBuffer);
      OGF           = VS_COMMAND_OGF(VS_WRITE_CODEC_CONFIG_COMMAND_OPCODE);
      OCF           = VS_COMMAND_OCF(VS_WRITE_CODEC_CONFIG_COMMAND_OPCODE);

      ret_val       = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to enable or disable the AVPR      */
   /* features.  This function returns zero if successful or a negative */
   /* return error code if there was an error.                          */
int BTPSAPI VS_AVPR_Enable(unsigned int BluetoothStackID, Boolean_t AVPREnable, Boolean_t LoadCode, Byte_t A3DPRole)
{
   int    ret_val;
   Byte_t CommandBuffer[5];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if(BluetoothStackID)
   {
      /* Initialize the command buffer.                                 */
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      /* Determine whether we should enable or disable AVPR.            */
      CommandBuffer[0] = (Byte_t)((AVPREnable)?1:0);
      CommandBuffer[1] = A3DPRole;
      CommandBuffer[2] = (Byte_t)((LoadCode)?1:0);

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_AVPR_ENABLE_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_AVPR_ENABLE_COMMAND_OPCODE);

      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function implements the vendor-specific             */
   /* DRPb_Enable_RF_Calibration_Enhanced command.  Refer to the        */
   /* Bluetooth (WL18xx and WL18xxQ) Vendor-Specific HCI Commands User's*/
   /* Guide for a description of the command's parameters.  This        */
   /* function returns 0 on success and a negative error code otherwise.*/
int BTPSAPI VS_DRPb_Enable_RF_Calibration_Enhanced(unsigned int BluetoothStackID, Byte_t Mode, Byte_t PeriodicOptions, DWord_t CalibrationProcedures, Byte_t OverrideTemperatureCondition)
{
   int    ret_val;
   Byte_t Length;
   Byte_t Status;
   Byte_t OGF;
   Word_t OCF;
   Byte_t CommandBuffer[BYTE_SIZE + BYTE_SIZE + DWORD_SIZE + BYTE_SIZE];
   Byte_t ReturnBuffer[1];
   if(BluetoothStackID)
   {
      /* Format the HCI Vendor-Specific DRPB Enable RF Calibration      */
      /* Enhanced Command.                                              */
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[0],   Mode);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[1],   PeriodicOptions);
      ASSIGN_HOST_DWORD_TO_LITTLE_ENDIAN_UNALIGNED_DWORD(&CommandBuffer[2], CalibrationProcedures);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[6],   OverrideTemperatureCondition);

      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_DRPB_ENABLE_RF_CALIBRATION_ENHANCED_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_DRPB_ENABLE_RF_CALIBRATION_ENHANCED_COMMAND_OPCODE);
      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, sizeof(CommandBuffer), (Byte_t *)CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   return(ret_val);
}

   /* The following function implements the vendor-specific             */
   /* VS_DRPb_Tester_Con_TX command.  Refer to the Bluetooth (WL18xx and*/
   /* WL18xxQ) Vendor-Specific HCI Commands User's Guide for a          */
   /* description of the command's parameters.  This function returns 0 */
   /* on success and a negative error code otherwise.                   */
int BTPSAPI VS_DRPb_Tester_Con_TX(unsigned int BluetoothStackID, Word_t Frequency, VS_Modulation_Type_t Modulation, VS_Test_Pattern_t TestPattern, Byte_t PowerLevelIndex)
{
   int    ret_val;
   Byte_t Length;
   Byte_t Status;
   Byte_t OGF;
   Word_t OCF;
   Byte_t CommandBuffer[13];
   Byte_t ReturnBuffer[1];

   if(BluetoothStackID)
   {
      /* Format the HCI Vendor-Specific DRPB Continuous Tx Command.     */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[0],   Frequency);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[2],   Modulation);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[3],   TestPattern);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[4],   PowerLevelIndex);
      ASSIGN_HOST_DWORD_TO_LITTLE_ENDIAN_UNALIGNED_DWORD(&CommandBuffer[5], 0x00000000);
      ASSIGN_HOST_DWORD_TO_LITTLE_ENDIAN_UNALIGNED_DWORD(&CommandBuffer[9], 0x00000000);

      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_DRPB_TESTER_CON_TX_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_DRPB_TESTER_CON_TX_COMMAND_OPCODE);
      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, (Byte_t)sizeof(CommandBuffer), (Byte_t *)CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   return(ret_val);
}

   /* The following function implements the vendor-specific             */
   /* VS_DRPb_Tester_Packet_TX_RX command.  Refer to the Bluetooth      */
   /* (WL18xx and WL18xxQ) Vendor-Specific HCI Commands User's Guide for*/
   /* a description of the command's parameters.  This function returns */
   /* 0 on success and a negative error code otherwise.                 */
int BTPSAPI VS_DRPb_Tester_Packet_TX_RX(unsigned int BluetoothStackID, VS_DRPb_Tester_Packet_TX_RX_Params_t *VS_DRPb_Tester_Packet_TX_RX_Params)
{
   int          ret_val;
   unsigned int Index;
   Byte_t       Length;
   Byte_t       Status;
   Byte_t       OGF;
   Word_t       OCF;
   Byte_t       CommandBuffer[BYTE_SIZE + BYTE_SIZE + WORD_SIZE + WORD_SIZE + BYTE_SIZE + BYTE_SIZE + WORD_SIZE + BYTE_SIZE + BYTE_SIZE + WORD_SIZE];
   Byte_t       ReturnBuffer[1];

   if(BluetoothStackID)
   {
      Index = 0;

      /* Format the HCI Vendor-Specific Packet Tx/Rx Command.           */
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], VS_DRPb_Tester_Packet_TX_RX_Params->ACLTXPacketType);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], VS_DRPb_Tester_Packet_TX_RX_Params->FrequencyMode);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[Index], VS_DRPb_Tester_Packet_TX_RX_Params->TXSingleFrequency);
      Index += WORD_SIZE;
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[Index], VS_DRPb_Tester_Packet_TX_RX_Params->RXSingleFrequency);
      Index += WORD_SIZE;
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], VS_DRPb_Tester_Packet_TX_RX_Params->ACLTXPacketDataPattern);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], 0x00);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[Index], VS_DRPb_Tester_Packet_TX_RX_Params->ACLPacketDataLength);
      Index += WORD_SIZE;
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], VS_DRPb_Tester_Packet_TX_RX_Params->PowerLevelIndex);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], VS_DRPb_Tester_Packet_TX_RX_Params->DisableWhitening);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[Index], VS_DRPb_Tester_Packet_TX_RX_Params->PRBS9InitValue);

      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_PACKET_TX_RX_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_PACKET_TX_RX_COMMAND_OPCODE);
      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, (Byte_t)sizeof(CommandBuffer), (Byte_t *)CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   return(ret_val);
}

   /* The following function implements the vendor-specific             */
   /* VS_DRPb_Tester_Con_RX command.  Refer to the Bluetooth (WL18xx and*/
   /* WL18xxQ) Vendor-Specific HCI Commands User's Guide for a          */
   /* description of the command's parameters.  This function returns 0 */
   /* on success and a negative error code otherwise.                   */
int BTPSAPI VS_DRPb_Tester_Con_RX(unsigned int BluetoothStackID, Word_t Frequency, Byte_t RxMode, Byte_t ModulationType)
{
   int    ret_val;
   Byte_t Length;
   Byte_t Status;
   Byte_t OGF;
   Word_t OCF;
   Byte_t CommandBuffer[4];
   Byte_t ReturnBuffer[1];

   if(BluetoothStackID)
   {
      /* Format the HCI Vendor-Specific Packet Tx/Rx Command.           */
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[0], Frequency);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[2], RxMode);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[3], ModulationType);

      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_CONTINUOUS_RX_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_CONTINUOUS_RX_COMMAND_OPCODE);
      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, (Byte_t)sizeof(CommandBuffer), (Byte_t *)CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   return(ret_val);
}

   /* The following function implements the vendor-specific             */
   /* VS_DRPb_BER_Meter_Start command which is commonly used for        */
   /* Production Line Tests.  Refer to the Bluetooth (WL18xx and        */
   /* WL18xxQ) Vendor-Specific HCI Commands User's Guide for a          */
   /* description of the command's parameters.  This function returns 0 */
   /* on success and a negative error code otherwise.                   */
int BTPSAPI VS_Production_Line_Test(unsigned int BluetoothStackID, BD_ADDR_t BD_ADDR, Word_t PacketLength)
{
   int          ret_val;
   unsigned int Index;
   Byte_t       Length;
   Byte_t       Status;
   Byte_t       OGF;
   Word_t       OCF;
   Byte_t       CommandBuffer[(BYTE_SIZE * 5) + (WORD_SIZE * 3) + sizeof(BD_ADDR)];
   Byte_t       ReturnBuffer[1];

   if(BluetoothStackID)
   {
      Index = 0;

      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], 0x00);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], 0x00);

      BTPS_MemCopy(&CommandBuffer[Index], &BD_ADDR, sizeof(BD_ADDR));
      Index += sizeof(BD_ADDR);

      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], 0x01);
      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index++], 0x01);

      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[Index],   PacketLength);
      Index += WORD_SIZE;

      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[Index],   1000);
      Index += WORD_SIZE;

      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[Index],   0x1FF);
      Index += WORD_SIZE;

      ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[Index],   0x01);

      Length  = sizeof(ReturnBuffer);
      OGF     = VS_COMMAND_OGF(VS_DRPB_BER_METER_START_COMMAND_OPCODE);
      OCF     = VS_COMMAND_OCF(VS_DRPB_BER_METER_START_COMMAND_OPCODE);
      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, (Byte_t)sizeof(CommandBuffer), (Byte_t *)CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      if(!(ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer)))
      {
         Length  = sizeof(ReturnBuffer);
         OGF     = VS_COMMAND_OGF(VS_DRP_READ_BER_METER_RESULT_COMMAND_OPCODE);
         OCF     = VS_COMMAND_OCF(VS_DRP_READ_BER_METER_RESULT_COMMAND_OPCODE);
         ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, 0, (Byte_t *)CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

         ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
      }
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   return(ret_val);
}

   /* The following function is called when an A2DP connection is in the*/
   /* open state to tell the controller the L2CAP parameters of the     */
   /* channel.  This function returns zero if successful or a negative  */
   /* return error code if there was an error.                          */
int BTPSAPI VS_A3DP_Open_Stream(unsigned int BluetoothStackID, Byte_t Connection_Handle, Word_t CID, Word_t MTU)
{
   int    ret_val;
   Byte_t CommandBuffer[15];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if((BluetoothStackID) && (Connection_Handle))
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));
      CommandBuffer[0] = Connection_Handle;

      /* AVDTP version parameter [0x00 - 0x03].                         */
      CommandBuffer[5] = 0x02;

      /* AVDTP payload parameter [0x30 - 0xFF].                         */
      CommandBuffer[6] = 0x65;

      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[1], CID);
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[3], MTU);

      CommandLength = (Byte_t)sizeof(CommandBuffer);
      Length        = sizeof(ReturnBuffer);
      OGF           = VS_COMMAND_OGF(VS_A3DP_OPEN_STREAM_COMMAND_OPCODE);
      OCF           = VS_COMMAND_OCF(VS_A3DP_OPEN_STREAM_COMMAND_OPCODE);
      ret_val       = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to inform the controller when A2DP */
   /* connection closes.  This function returns zero if successful or a */
   /* negative return error code if there was an error.                 */
int BTPSAPI VS_A3DP_Close_Stream(unsigned int BluetoothStackID, Byte_t Connection_Handle)
{
   int    ret_val;
   Byte_t CommandBuffer[5];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if((BluetoothStackID) && (Connection_Handle))
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));
      CommandBuffer[0] = Connection_Handle;

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_CLOSE_STREAM_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_CLOSE_STREAM_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to configure the SBC Encoder or    */
   /* Decoder parameters.  This function returns zero if successful or a*/
   /* negative return error code if there was an error.                 */
int BTPSAPI VS_A3DP_Codec_Configuration(unsigned int BluetoothStackID, Byte_t AudioFormat, Byte_t SBCFormat, Byte_t BitPoolSize)
{
   int    ret_val;
   Byte_t CommandBuffer[19];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if(BluetoothStackID)
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      CommandBuffer[0] = AVRP_SOURCE_PCM;
      CommandBuffer[1] = (AudioFormat & AVRP_AUDIO_FORMAT_PCM_SAMPLE_RATE_MASK);
      CommandBuffer[2] = (Byte_t)(((AudioFormat & AVRP_AUDIO_FORMAT_SBC_MODE_MASK) == AVRP_AUDIO_FORMAT_SBC_MODE_MONO)?1:2);
      CommandBuffer[3] = ((AudioFormat & AVRP_AUDIO_FORMAT_SBC_SAMPLE_RATE_MASK) >> 4);
      CommandBuffer[4] = ((AudioFormat & AVRP_AUDIO_FORMAT_SBC_MODE_MASK) >> 6);

      /* Convert from 0,1,2,3 to 4,8,12,16 for the block length.        */
      CommandBuffer[5] = ((SBCFormat & AVRP_SBC_FORMAT_BLOCK_LENGTH_MASK) + 1) << 2;
      CommandBuffer[6] = 8;
      CommandBuffer[7] = ((SBCFormat & AVRP_SBC_FORMAT_ALLOCATION_METHOD_MASK) >> 2);

      /* old value used to be 12 minimum bit pool.                      */
      CommandBuffer[8] = 20;
      CommandBuffer[9] = BitPoolSize;

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_CODEC_CONFIGURATION_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_CODEC_CONFIGURATION_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to start assisted A2DP streaming.  */
   /* This function returns zero if successful or a negative return     */
   /* error code if there was an error.                                 */
int BTPSAPI VS_A3DP_Start_Stream(unsigned int BluetoothStackID, Byte_t Connection_Handle)
{
   int    ret_val;
   Byte_t CommandBuffer[5];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if((BluetoothStackID) && (Connection_Handle))
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));
      CommandBuffer[0] = Connection_Handle;

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_START_STREAM_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_START_STREAM_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to stop assisted A2DP streaming.   */
   /* This function returns zero if successful or a negative return     */
   /* error code if there was an error.                                 */
int BTPSAPI VS_A3DP_Stop_Stream(unsigned int BluetoothStackID, Byte_t Connection_Handle, Byte_t Flags)
{
   int    ret_val;
   Byte_t CommandBuffer[7];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if((BluetoothStackID) && (Connection_Handle))
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));
      CommandBuffer[0] = Connection_Handle;
      CommandBuffer[1] = (Byte_t)((Flags & STOP_STREAM_FLAG_FLUSH_DATA)?1:0);
      CommandBuffer[2] = (Byte_t)((Flags & STOP_STREAM_FLAG_GENERATE_STOP_EVENT)?1:0);

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_STOP_STREAM_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_STOP_STREAM_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to open a stream as an A2DP SNK    */
   /* device. This function returns zero if successful or a negative    */
   /* return error code if there was an error.                          */
int BTPSAPI VS_A3DP_Sink_Open_Stream(unsigned int BluetoothStackID, Byte_t Connection_Handle, Word_t CID)
{
   int    ret_val;
   Byte_t CommandBuffer[11];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if((BluetoothStackID) && (Connection_Handle) && (CID))
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      CommandBuffer[0] = Connection_Handle;
      ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[1], CID);

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_SINK_OPEN_STREAM_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_SINK_OPEN_STREAM_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to close a SNK stream previously   */
   /* opened using VS_A3DP_Sink_Open_Stream. This function returns zero */
   /* if successful or a negative return error code if there was an     */
   /* error.                                                            */
int BTPSAPI VS_A3DP_Sink_Close_Stream(unsigned int BluetoothStackID)
{
   int    ret_val;
   Byte_t CommandBuffer[4];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if(BluetoothStackID)
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_SINK_CLOSE_STREAM_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_SINK_CLOSE_STREAM_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to configure an A3DP device as an  */
   /* A2DP SNK, giving it PCM and SBC parameters. This configuration    */
   /* should be performed after VS_PCM_CodecConfig and VS_AVPR_Enable.  */
   /* This function returns zero if successful or a negative return     */
   /* error code if there was an error.                                 */
int BTPSAPI VS_A3DP_Sink_Codec_Configuration(unsigned int BluetoothStackID, Byte_t AudioFormat, Byte_t SBCFormat)
{
   int    ret_val;
   Byte_t CommandBuffer[18];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if(BluetoothStackID)
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      CommandBuffer[0] = (Byte_t)(((AudioFormat & AVRP_AUDIO_FORMAT_SBC_MODE_MASK) == AVRP_AUDIO_FORMAT_SBC_MODE_MONO)?1:2);;
      CommandBuffer[1] = ((AudioFormat & AVRP_AUDIO_FORMAT_SBC_SAMPLE_RATE_MASK) >> 4);
      CommandBuffer[2] = ((AudioFormat & AVRP_AUDIO_FORMAT_SBC_MODE_MASK) >> 6);
      CommandBuffer[3] = ((SBCFormat & AVRP_SBC_FORMAT_BLOCK_LENGTH_MASK) + 1) << 2;
      CommandBuffer[4] = 8;
      CommandBuffer[5] = ((SBCFormat & AVRP_SBC_FORMAT_ALLOCATION_METHOD_MASK) >> 2);

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_SINK_CODEC_CONFIGURATION_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_SINK_CODEC_CONFIGURATION_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to change a stream previously      */
   /* opened via VS_A3DP_Sink_Open_Stream to the "Playing" state. This  */
   /* function returns zero if successful or a negative return error    */
   /* code if there was an error.                                       */
int BTPSAPI VS_A3DP_Sink_Start_Stream(unsigned int BluetoothStackID)
{
   int    ret_val;
   Byte_t CommandBuffer[4];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if(BluetoothStackID)
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_SINK_START_STREAM_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_SINK_START_STREAM_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is used to change a stream previously      */
   /* opened via VS_A3DP_Sink_Open_stream to the "Stopped" state.  This */
   /* function returns zero if successful or a negative return error    */
   /* code if there was an error.                                       */
int BTPSAPI VS_A3DP_Sink_Stop_Stream(unsigned int BluetoothStackID)
{
   int    ret_val;
   Byte_t CommandBuffer[4];
   Byte_t CommandLength;
   Word_t OCF;
   Byte_t OGF;
   Byte_t Length;
   Byte_t Status;
   Byte_t ReturnBuffer[1];

   /* Verify that the parameters that were passed in appear valid.      */
   if(BluetoothStackID)
   {
      BTPS_MemInitialize(CommandBuffer, 0, sizeof(CommandBuffer));

      CommandLength    = (Byte_t)sizeof(CommandBuffer);
      Length           = sizeof(ReturnBuffer);
      OGF              = VS_COMMAND_OGF(VS_A3DP_SINK_STOP_STREAM_COMMAND_OPCODE);
      OCF              = VS_COMMAND_OCF(VS_A3DP_SINK_STOP_STREAM_COMMAND_OPCODE);
      ret_val          = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);

      /* Map the Send Raw return results.                               */
      ret_val = MapSendRawResults(ret_val, Status, Length, ReturnBuffer);
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}
