
MODULENAME := SS1BTVS
OBJS       := BTVS.o
LIBOUTPUT  := ./lib$(MODULENAME).a

ifndef CC
   CC = gcc
endif

ifndef AR
   AR = ar
endif

INCLDDIRS = -I../../include
	
CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDLIBS = -lpthread

.PHONY:
all: $(LIBOUTPUT)

$(LIBOUTPUT): $(OBJS)
	$(AR) r $@ $?

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm -f $(OBJS)
	-rm -f *~
	-rm -f $(LIBOUTPUT)

