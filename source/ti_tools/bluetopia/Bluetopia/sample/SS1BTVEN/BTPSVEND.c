/*****< btpsvend.c >***********************************************************/
/*      Copyright 2013 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTPSVEND - Vendor specific functions/definitions used to define a set of  */
/*             vendor specific functions supported by the Bluetopia Protocol  */
/*             Stack.  These functions may be unique to a given hardware      */
/*             platform.                                                      */
/*                                                                            */
/*  Author:  Damon Lange                                                      */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   07/31/09  D. Lange       Initial creation.                               */
/*   09/06/13  S. Hishmeh     Created for use with AM335x platforms.          */
/******************************************************************************/
#include <stdlib.h>           /* For system() call.                           */
#include <stdio.h>               
#include <string.h>       
#include "BTVSAPI.h"         
#include "BTPSVEND.h"         /* Bluetooth Vend. Spec. Prototypes/Constants.  */
#include "SS1BTPS.h"          /* Bluetopia API Prototypes/Constants.          */
#include "HCIDRV.h"           /* Main HCI Driver Prototypes/Constants.        */
#include "BTPSKRNL.h"         /* BTPS Kernel Prototypes/Constants.            */
#include "SS1TIBTS.h"         /* TI BTS Script Parser Prototypes/Constants.   */

   /* The following definition specifies the UART COMM port number used */
   /* by the Bluetooth radio.                                           */
#define BLUETOOTH_HCI_DEV_TTY_COM_PORT_NUMBER     1

   /* The following definitions specifies the default baud rate at which*/
   /* the Bluetooth radio will use to communicate after it is reset.    */
#define BLUETOOTH_CONTROLLER_POWER_ON_BAUD_RATE   115200

   /* The following definition specifies the baud rate that the         */
   /* Bluetooth Controller is configured to before the BTS              */
   /* Initialization Script is executed, set this to 0 to use the       */
   /* command line-specified baud rate.                                 */
#define BLUETOOTH_HCI_TARGET_BAUD_RATE            3000000

   /* The following definitions select the UART protocol.  It is        */
   /* currently set to HCILL+RTS/CTS because commands to enable HCILL   */
   /* are embedded in the BTS Initialization Script and HCILL is        */
   /* therefore enabled when the BTS Script is executed.                */
#define BLUETOOTH_HCI_UART_PROTOCOL               cpHCILL_RTS_CTS

   /* The following definition specifies the delay in milliseconds      */
   /* required to wait while the Bluetooth radio power cycles.  The     */
   /* large delay of 2 seconds was selected to work around an issue with*/
   /* the WL8 and the AM335x RTS/CTS mux configuration when the WLAN is */
   /* initialized before Bluetooth: if the WLAN is turned on before     */
   /* Bluetooth, due to a long handshake between Bluetooth and the WLAN,*/
   /* it takes more time for the controller to de-assert RTS, if during */
   /* this time the host is sending commands then the data is lost and  */
   /* Bluetooth initialization will fail.                               */
   /* * NOTE * This issue is found in the Linux kernel provided with TI */
   /*          SDK 6.0 and has been resolved in SDK 7.0.  If you are    */
   /*          using SDK 7.0 this delay can be safely lowered to 75     */
   /*          milliseconds.                                            */
#define BLUETOOTH_CONTROLLER_POWER_ON_DELAY       75            /* 2000 */

   /* The following definition specifies the delay in milliseconds to   */
   /* wait while the Bluetooth controller powers off.  This delay is    */
   /* used for safety to ensure that the radio is not power cycled off  */
   /* and on too quickly which could cause Bluetooth initialization to  */
   /* fail.                                                             */
#define BLUETOOTH_CONTROLLER_POWER_OFF_DELAY      75

   /* The following definitions specifies the HCI Command Op Code that  */
   /* is used to change the Controller's Baud Rate.                     */
#define UPDATE_UART_HCI_BAUDRATE_COMMAND_OPCODE   ((Word_t)(0xFF36))

   /* The following variable determines if the Bluetooth Controller's   */
   /* Initialization Script has be loaded on to the device.             */
static Boolean_t InitializationScriptLoaded;

   /* The following variable stores the HCI driver information and is   */
   /* used to reconfigure the driver.                                   */
static HCI_DriverInformation_t TargetHCIDriverInformation;

   /* Internal Function Prototypes.                                     */
static void Enable_Bluetooth_Controller(void);
static void Disable_Bluetooth_Controller(void);
static int ExecuteScriptFile(unsigned int BluetoothStackID, TIBTS_Script_Version_Info_t *ScriptVersionInfo);
static int ProcessSendHCICommandAction(unsigned int BluetoothStackID, TIBTS_Send_HCI_Command_Action_Data_t *SendHCICommandActionData);
static int ConfigureHCIBaudRate(unsigned int BluetoothStackID, DWord_t BaudRate);
static int MapSendRawResults(int Result, Byte_t Status, Byte_t LengthResult, Byte_t *ReturnData);


/* File to read a MAC address from filesystem and use HCI to set it */
static  Boolean_t SetMACAddressFromFileSystem(unsigned int BluetoothStackID);


   /* The following function enables the Bluetooth Controller.          */
static void Enable_Bluetooth_Controller(void)
{

   /* Set the Bluetooth Enable line high.                               */
   if(!system("insmod /lib/modules/2.6.37_DM385_IPNC_3.80.00/kernel/drivers/bt_enable/gpio_en.ko"))
   {
      /* Delay while we wait for the Bluetooth Controller to boot.      */
      BTPS_Delay(BLUETOOTH_CONTROLLER_POWER_ON_DELAY);
   }
}

   /* The following function disables the Bluetooth Controller.         */
static void Disable_Bluetooth_Controller(void)
{
   /* Set the Bluetooth Enable line low.                                */
   if(!system("rmmod gpio_en > /dev/null 2>&1"))
   {
      /* Delay for safety in case the line is immediately brought back  */
      /* high, which may or may not cause problems with the Bluetooth   */
      /* Controller.                                                    */
      BTPS_Delay(BLUETOOTH_CONTROLLER_POWER_OFF_DELAY);
   }

   /* The Bluetooth Controller has been disabled, flag that the script  */
   /* is no longer loaded so that the script will be loaded when the    */
   /* controller is re-enabled.                                         */
   InitializationScriptLoaded = FALSE;
}

   /* The following function is a utility function that is used to map  */
   /* the return results that we returned from Send Raw (ONLY for       */
   /* commands that return command complete event) to a negative error  */
   /* code.                                                             */
   /* * NOTE * As an internal function no check is done on the return   */
   /*          parameters.                                              */
static int MapSendRawResults(int Result, Byte_t Status, Byte_t LengthResult, Byte_t *ReturnData)
{
   int ret_val;

   /* Check to see if the API returned an error.                        */
   if((!Result) && (!Status) && (LengthResult >= 1))
   {
      /* Check to see if the chip returned an error.                    */
      if(ReturnData[0] == HCI_ERROR_CODE_NO_ERROR)
         ret_val = 0;
      else
         ret_val = BTPS_ERROR_CODE_HCI_STATUS_BASE - ReturnData[0];
   }
   else
      ret_val = BTPS_ERROR_DEVICE_HCI_ERROR;

   return(ret_val);
}

   /* The following function executes the vendor specific command which */
   /* is used to change the Bluetooth UART Baud Rate for the Local      */
   /* Bluetooth Device.  The first parameter is the Bluetooth Stack ID  */
   /* and the second parameter is the baud rate which the HCI interface */
   /* will be configured to.  This change encompasses both changing the */
   /* speed of the Bluetooth chip (by issuing the correct commands) and */
   /* then, if successful, informing the HCI Driver of the change (so   */
   /* the driver can communicate with the Bluetooth device at the new   */
   /* baud rate).  This function returns zero if successful or a        */
   /* negative return error code otherwise.                             */
static int ConfigureHCIBaudRate(unsigned int BluetoothStackID, DWord_t BaudRate)
{
   int                                 ret_val;
   Byte_t                              Length;
   Byte_t                              Status;
   Byte_t                              OGF;
   Word_t                              OCF;
   unsigned long                       LBaudRate;
   NonAlignedDWord_t                   _BaudRate;
   HCI_Driver_Reconfigure_Data_t       DriverReconfigureData;

   union
   {
      Byte_t                           Buffer[16];
      HCI_COMMReconfigureInformation_t COMMReconfigureInformation;
   } Data;

   /* Before continuing, make sure the input parameters appear to be    */
   /* semi-valid.                                                       */
   if((BluetoothStackID) && (BaudRate))
   {
      /* Write the Baud Rate.                                           */
      ASSIGN_HOST_DWORD_TO_LITTLE_ENDIAN_UNALIGNED_DWORD(&_BaudRate, BaudRate);

      LBaudRate = (unsigned long)BaudRate;

      /* Next, write the command to the device.                         */
      Length  = sizeof(Data.Buffer);
      OGF     = TIBTS_COMMAND_OPCODE_TO_OGF(UPDATE_UART_HCI_BAUDRATE_COMMAND_OPCODE);
      OCF     = TIBTS_COMMAND_OPCODE_TO_OCF(UPDATE_UART_HCI_BAUDRATE_COMMAND_OPCODE);

      ret_val = HCI_Send_Raw_Command(BluetoothStackID, OGF, OCF, sizeof(NonAlignedDWord_t), (Byte_t *)&_BaudRate, &Status, &Length, Data.Buffer, TRUE);

      /* Map the return results into an error code.                     */
      if((!ret_val) && (!(ret_val = MapSendRawResults(ret_val, Status, Length, Data.Buffer))))
      {
         /* Note that after this command is issued the Bluetooth        */
         /* controller will set its RTS output high while it            */
         /* re-configures its UART.  There is an issue in v6.00 of the  */
         /* AM335x SDK where UART disregards its CTS input.  It is      */
         /* therefore possible for us to start sending while the        */
         /* Bluetooth controller has its RTS output high and is not     */
         /* ready to receive data.  To workaround this issue we add a   */
         /* delay here while we wait for the Bluetooth controller to    */
         /* de-assert its RTS output.                                   */
         /* * NOTE * This issue will be resolved in the next release of */
         /*          the AM335x SDK and this delay will be removed then */
         /*          when the updated SDK is released.                  */
         /* * NOTE * The delay from the time the radio sent a response  */
         /*          to the baud-rate change command to the time when   */
         /*          RTS was set low was measured to be 0.58            */
         /*          milliseconds using a WL1835, this value was        */
         /*          measured with a baud rate of 921,600 and 3,000,000 */
         /*          bps and was the same for both baud rates.  In the  */
         /*          line of code below we delay longer than the minimum*/
         /*          of 0.58 milliseconds for additional safety.        */
         BTPS_Delay(5);

         /* We were successful, now we need to change the baud rate of  */
         /* the driver.                                                 */
         BTPS_MemInitialize(&(Data.COMMReconfigureInformation), 0, sizeof(Data.COMMReconfigureInformation));

         Data.COMMReconfigureInformation.BaudRate         = LBaudRate;
         Data.COMMReconfigureInformation.ReconfigureFlags = HCI_COMM_RECONFIGURE_INFORMATION_RECONFIGURE_FLAGS_CHANGE_BAUDRATE;

         DriverReconfigureData.ReconfigureCommand         = HCI_COMM_DRIVER_RECONFIGURE_DATA_COMMAND_CHANGE_COMM_PARAMETERS;
         DriverReconfigureData.ReconfigureData            = (void *)&Data.COMMReconfigureInformation;

         ret_val = HCI_Reconfigure_Driver(BluetoothStackID, FALSE, &DriverReconfigureData);
      }
   }
   else
      ret_val = BTPS_ERROR_INVALID_PARAMETER;

   /* Return the result the caller.                                     */
   return(ret_val);
}

   /* The following function is responsible for processing and sending  */
   /* an HCI Command.  This function returns 0 on success and a negative*/
   /* error code otherwise.                                             */
static int ProcessSendHCICommandAction(unsigned int BluetoothStackID, TIBTS_Send_HCI_Command_Action_Data_t *SendHCICommandActionData)
{
   int    ret_val;
   Byte_t Length;
   Byte_t Status;
   Byte_t Buffer[32];

   /* Check if this command to change the Controller's UART Baud Rate.  */
   if(TIBTS_OGF_OCF_TO_COMMAND_OPCODE(SendHCICommandActionData->OGF, SendHCICommandActionData->OCF) != UPDATE_UART_HCI_BAUDRATE_COMMAND_OPCODE)
   {
      /* This is not a command to change the Controller's UART Baud     */
      /* Rate, next, write the command to the device.                   */
      Length  = sizeof(Buffer);

      ret_val = HCI_Send_Raw_Command(BluetoothStackID, SendHCICommandActionData->OGF, SendHCICommandActionData->OCF, SendHCICommandActionData->CommandDataLength, SendHCICommandActionData->CommandData, &Status, &Length, Buffer, TRUE);

      /* Map the return results into an error code.                     */
      if(!ret_val)
         ret_val = MapSendRawResults(ret_val, Status, Length, Buffer);
   }
   else
   {
      /* Skip the baud rate change command so that the Host and         */
      /* Controller Baud Rates do not get out of sync.                  */
      ret_val = 0;
   }

   return(ret_val);
}

   /* The following function executes a BTS Script from a file in the   */
   /* file system.  This function returns 0 on success and a negative   */
   /* error code otherwise.                                             */
static int ExecuteScriptFile(unsigned int BluetoothStackID, TIBTS_Script_Version_Info_t *ScriptVersionInfo)
{
   int                            ret_val;
   int                            Status;
   char                           ScriptPath[sizeof("/lib/firmware/") + sizeof("TIInit_xxx.xxx.xxx.bts")];
   TIBTS_Script_Open_Parameters_t ScriptOpenParameters;
   TIBTS_Script_Handle_t          ScriptHandle;
   TIBTS_Script_Action_t          ScriptAction;

   /* Using the Script's Version Information create the expected BTS    */
   /* Script's filename.                                                */
   if((ret_val = BTPS_SprintF(ScriptPath, "/lib/firmware/TIInit_%u.%u.%u.bts", ScriptVersionInfo->ProjectVersion, ScriptVersionInfo->MajorVersion, ScriptVersionInfo->MinorVersion)) > 0)
   {
      printf("Status: Executing BTS Script %s.\r\n", ScriptPath);

      /* Configure the Script Open Parameters for the TIBTS library.    */
      BTPS_MemInitialize(&ScriptOpenParameters, 0x00, sizeof(TIBTS_Script_Open_Parameters_t));

      ScriptOpenParameters.ScriptFlags = 0;
      ScriptOpenParameters.ScriptPath  = ScriptPath;

      /* Attempt to open the script.                                    */
      if((ScriptHandle = TIBTS_Open_Script(&ScriptOpenParameters)))
      {
         ret_val = 0;

         /* Loop through all of the HCI Commands in the script.         */
         while((!ret_val) && (!(Status = TIBTS_Get_Next_Action(ScriptHandle, &ScriptAction))))
         {
            switch(ScriptAction.Type)
            {
               case atSendHCICommand:
                  ret_val = ProcessSendHCICommandAction(BluetoothStackID, &ScriptAction.ActionData.SendHCICommandActionData);
                  break;

               case atDelay:
                  BTPS_Delay((unsigned long)(ScriptAction.ActionData.DelayActionData.Delay));
                  break;

               default:
                  printf("Error: Unexpected action type %d returned from TIBTS_Get_Next_Action().\r\n", ScriptAction.Type);
                  ret_val = TIBTS_ERROR_INVALID_SCRIPT_FORMAT;
                  break;
            }
         }

         /* Check if any error occurred while processing the script.    */
         if(!ret_val)
         {
            /* No error occurred, check if the status returned from     */
            /* getting the last command was an end of file error.       */
            if(Status != TIBTS_ERROR_END_OF_SCRIPT)
            {
               printf("Error: TIBTS_Get_Next_Action() returned unexpected result (%d).\r\n", Status);

               /* The status of the last function call to get the next  */
               /* HCI Command was not an end of file error as it should */
               /* have been, flag the error to the caller.              */
               ret_val = TIBTS_ERROR_INVALID_SCRIPT_FORMAT;
            }
         }

         /* Close the script.                                           */
         TIBTS_Close_Script(ScriptHandle);
      }
      else
      {
         printf("Error: Could not open the script file %s.\r\n", ScriptPath);
         ret_val = BTPS_ERROR_INVALID_PARAMETER;
      }
   }
   else
   {
      printf("Error: Could not create the filename. BTPS_SprintF() Result: %d.\r\n", ret_val);
      ret_val = BTPS_ERROR_INVALID_PARAMETER;
   }

   return(ret_val);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to implement any needed Bluetooth device   */
   /* vendor specific functionality that needs to be performed before   */
   /* the HCI Communications layer is opened.  This function is called  */
   /* immediately prior to calling the initialization of the HCI        */
   /* Communications layer.  This function should return a BOOLEAN TRUE */
   /* indicating successful completion or should return FALSE to        */
   /* indicate unsuccessful completion.  If an error is returned the    */
   /* stack will fail the initialization process.                       */
   /* * NOTE * The parameter passed to this function is the exact       */
   /*          same parameter that was passed to BSC_Initialize() for   */
   /*          stack initialization.  If this function changes any      */
   /*          members that this pointer points to, it will change the  */
   /*          structure that was originally passed.                    */
   /* * NOTE * No HCI communication calls are possible to be used in    */
   /*          this function because the driver has not been initialized*/
   /*          at the time this function is called.                     */
Boolean_t BTPSAPI HCI_VS_InitializeBeforeHCIOpen(HCI_DriverInformation_t *HCI_DriverInformation)
{
   InitializationScriptLoaded = FALSE;

   if(BLUETOOTH_HCI_TARGET_BAUD_RATE != 0)
   {
      /* Set the baud rate.                                             */
      HCI_DriverInformation->DriverInformation.COMMDriverInformation.BaudRate = BLUETOOTH_HCI_TARGET_BAUD_RATE;
   }

   /* Set the COMM port information.                                    */
   HCI_DRIVER_SET_COMM_INFORMATION(HCI_DriverInformation, BLUETOOTH_HCI_DEV_TTY_COM_PORT_NUMBER, HCI_DriverInformation->DriverInformation.COMMDriverInformation.BaudRate, BLUETOOTH_HCI_UART_PROTOCOL);

   /* Save the target HCI driver configuration, we will use it when the */
   /* patch is applied to re-configure the driver.                      */
   BTPS_MemCopy(&TargetHCIDriverInformation, HCI_DriverInformation, sizeof(TargetHCIDriverInformation));

   /* Set the baud rate to the default rate that chip boots to.         */
   HCI_DriverInformation->DriverInformation.COMMDriverInformation.BaudRate = BLUETOOTH_CONTROLLER_POWER_ON_BAUD_RATE;

   /* Reset the Bluetooth Controller by disabling it off if it's not    */
   /* already disabled, and then re-enable it.                          */
   Disable_Bluetooth_Controller();
   Enable_Bluetooth_Controller();

   return(TRUE);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to implement any needed Bluetooth device   */
   /* vendor specific functionality after the HCI Communications layer  */
   /* is initialized (the driver only).  This function is called        */
   /* immediately after returning from the initialization of the HCI    */
   /* Communications layer (HCI Driver).  This function should return a */
   /* BOOLEAN TRUE indicating successful completion or should return    */
   /* FALSE to indicate unsuccessful completion.  If an error is        */
   /* returned the stack will fail the initialization process.          */
   /* * NOTE * No HCI layer function calls are possible to be used in   */
   /*          this function because the actual stack has not been      */
   /*          initialized at this point.  The only initialization that */
   /*          has occurred is with the HCI Driver (hence the HCI       */
   /*          Driver ID that is passed to this function).              */
Boolean_t BTPSAPI HCI_VS_InitializeAfterHCIOpen(unsigned int HCIDriverID)
{
   return(TRUE);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to implement any needed Bluetooth device   */
   /* vendor specific functions after the HCI Communications layer AND  */
   /* the HCI Stack layer has been initialized.  This function is called*/
   /* after all HCI functionality is established, but before the initial*/
   /* HCI Reset is sent to the stack.  The function should return a     */
   /* BOOLEAN TRUE to indicate successful completion or should return   */
   /* FALSE to indicate unsuccessful completion.  If an error is        */
   /* returned the stack will fail the initialization process.          */
   /* * NOTE * At the time this function is called HCI Driver and HCI   */
   /*          layer functions can be called, however no other stack    */
   /*          layer functions are able to be called at this time       */
   /*          (hence the HCI Driver ID and the Bluetooth Stack ID      */
   /*          passed to this function).                                */
Boolean_t BTPSAPI HCI_VS_InitializeBeforeHCIReset(unsigned int HCIDriverID, unsigned int BluetoothStackID)
{
   return(TRUE);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to implement any needed Bluetooth device   */
   /* vendor specific functionality after the HCI layer has issued any  */
   /* HCI Reset as part of the initialization.  This function is called */
   /* after all HCI functionality is established, just after the initial*/
   /* HCI Reset is sent to the stack.  The function should return a     */
   /* BOOLEAN TRUE to indicate successful completion or should return   */
   /* FALSE to indicate unsuccessful completion.  If an error is        */
   /* returned the stack will fail the initialization process.          */
   /* * NOTE * At the time this function is called HCI Driver and HCI   */
   /*          layer functions can be called, however no other stack    */
   /*          layer functions are able to be called at this time (hence*/
   /*          the HCI Driver ID and the Bluetooth Stack ID passed to   */
   /*          this function).                                          */
Boolean_t BTPSAPI HCI_VS_InitializeAfterHCIReset(unsigned int HCIDriverID, unsigned int BluetoothStackID)
{
   int                         ret_val = -1;
   TIBTS_Script_Version_Info_t ScriptVersionInfo;

   /* Verify that the parameters that were passed in appear valid.      */
   if((HCIDriverID) && (BluetoothStackID))
   {
      if(!InitializationScriptLoaded)
      {
         /* Check to see if we need to change the Baud Rate.            */
         if(TargetHCIDriverInformation.DriverInformation.COMMDriverInformation.BaudRate != BLUETOOTH_CONTROLLER_POWER_ON_BAUD_RATE)
         {
            printf("Changing HCI baud rate to %lu\r\n", TargetHCIDriverInformation.DriverInformation.COMMDriverInformation.BaudRate);

            if((ret_val = ConfigureHCIBaudRate(BluetoothStackID, TargetHCIDriverInformation.DriverInformation.COMMDriverInformation.BaudRate)) != 0)
               printf("Error: Could not change Bluetooth Chip's Baud Rate to %lu. BTS_Configure_HCI_Baud_Rate() Result: %d.\r\n", TargetHCIDriverInformation.DriverInformation.COMMDriverInformation.BaudRate, ret_val);
         }
         else
            ret_val = 0;

         /* Check if an error occurred.                                 */
         if(!ret_val)
         {
            /* Get the Script Version Information from the Controller.  */
            if(!(ret_val = TIBTS_Get_Script_Version(BluetoothStackID, &ScriptVersionInfo)))
            {
               /* An error did not occur, attempt to execute the BTS    */
               /* Initialization Script.                                */
               if(!(ret_val = ExecuteScriptFile(BluetoothStackID, &ScriptVersionInfo)))
               {
                  printf("Status: BTS Script successfully executed.\r\n");

                  /* An error did not occur, flag that the              */
                  /* initialization script has been loaded.             */
                  InitializationScriptLoaded = TRUE;

                  SetMACAddressFromFileSystem(BluetoothStackID);
 

               }
            }
            else
            {
               printf("Error: Could not get the script version from the controller. TIBTS_Get_Script_Version() Result: %d.\r\n", ret_val);
            }
         }
      }
   }

   /* Check if an error occurred in the processing above.               */
   if(ret_val)
   {
      /* An error occurred, disable the Bluetooth Controller.           */
      Disable_Bluetooth_Controller();
   }

   return((Boolean_t)(!ret_val));
}

   /* The following function prototype represents the vendor specific   */
   /* function which would is used to implement any needed Bluetooth    */
   /* device vendor specific functionality before the HCI layer is      */
   /* closed.  This function is called at the start of the HCI_Cleanup()*/
   /* function (before the HCI layer is closed), at which time all HCI  */
   /* functions are still operational.  The caller is NOT able to call  */
   /* any other stack functions other than the HCI layer and HCI Driver */
   /* layer functions because the stack is being shutdown (i.e.         */
   /* something has called BSC_Shutdown()).  The caller is free to      */
   /* return either success (TRUE) or failure (FALSE), however, it will */
   /* not circumvent the closing down of the stack or of the HCI layer  */
   /* or HCI Driver (i.e. the stack ignores the return value from this  */
   /* function).                                                        */
   /* * NOTE * At the time this function is called HCI Driver and HCI   */
   /*          layer functions can be called, however no other stack    */
   /*          layer functions are able to be called at this time (hence*/
   /*          the HCI Driver ID and the Bluetooth Stack ID passed to   */
   /*          this function).                                          */
Boolean_t BTPSAPI HCI_VS_InitializeBeforeHCIClose(unsigned int HCIDriverID, unsigned int BluetoothStackID)
{
   return(TRUE);
}

   /* The following function prototype represents the vendor specific   */
   /* function which is used to implement any needed Bluetooth device   */
   /* vendor specific functionality after the entire Bluetooth Stack is */
   /* closed.  This function is called during the HCI_Cleanup()         */
   /* function, after the HCI Driver has been closed.  The caller is    */
   /* free return either success (TRUE) or failure (FALSE), however, it */
   /* will not circumvent the closing down of the stack as all layers   */
   /* have already been closed.                                         */
   /* * NOTE * No Stack calls are possible in this function because the */
   /*          entire stack has been closed down at the time this       */
   /*          function is called.                                      */
Boolean_t BTPSAPI HCI_VS_InitializeAfterHCIClose(void)
{
   Disable_Bluetooth_Controller();
   return(TRUE);
}

   /* The following function is responsible for the specified string    */
   /* into data of type BD_ADDR.  The first parameter of this function  */
   /* is the BD_ADDR string to be converted to a BD_ADDR.  The second   */
   /* parameter of this function is a pointer to the BD_ADDR in which   */
   /* the converted BD_ADDR String is to be stored.                     */
static void StrToBD_ADDR(char *BoardStr, BD_ADDR_t *Board_Address)
{
   unsigned int Address[sizeof(BD_ADDR_t)];

   if((BoardStr) && (strlen(BoardStr) == sizeof(BD_ADDR_t)*2) && (Board_Address))
   {
      sscanf(BoardStr, "%02X%02X%02X%02X%02X%02X", &(Address[5]), &(Address[4]), &(Address[3]), &(Address[2]), &(Address[1]), &(Address[0]));

      Board_Address->BD_ADDR5 = (Byte_t)Address[5];
      Board_Address->BD_ADDR4 = (Byte_t)Address[4];
      Board_Address->BD_ADDR3 = (Byte_t)Address[3];
      Board_Address->BD_ADDR2 = (Byte_t)Address[2];
      Board_Address->BD_ADDR1 = (Byte_t)Address[1];
      Board_Address->BD_ADDR0 = (Byte_t)Address[0];
   }
   else
   {
      if(Board_Address)
         BTPS_MemInitialize(Board_Address, 0, sizeof(BD_ADDR_t));
   }
}

/* File to read a MAC address from filesystem and use HCI to set it */
static Boolean_t SetMACAddressFromFileSystem(unsigned int BluetoothStackID)
{
  FILE *fP;
  Boolean_t ret_val=FALSE; 
  int bytes_read;
  BD_ADDR_t MACAddr;

 
  if ((fP=fopen("/run/BTMacAddress","r")) == NULL)
     {printf("File /run/BTMacAddress not found, use default device MAC\n");
      }
  else
     {// Read it and set the address in Wilink device
      char sAddr[20]; // file should contain 12 char address, make it 20 for safety

      printf("File /run/BTMacAddress used\n");


      // Read the MAC address as a string
      bytes_read=fscanf(fP,"%s",sAddr);

      // To protect code from a badly formed text file, force a NULL to end string
      sAddr[19]=0;
      
      // only process string if it was correct length for a MAC address
      if (strlen(sAddr) == (sizeof(BD_ADDR_t)*2))
         {

          // convert from string to hex 
          StrToBD_ADDR(sAddr, &MACAddr);

          printf("MAC is now %02X%02X%02X%02X%02X%02X\n", MACAddr.BD_ADDR5,
                                                        MACAddr.BD_ADDR4,
                                                        MACAddr.BD_ADDR3,
                                                        MACAddr.BD_ADDR2,
                                                        MACAddr.BD_ADDR1,
                                                        MACAddr.BD_ADDR0);

           // Write it to device
          ret_val = VS_Write_BD_Addr(BluetoothStackID, MACAddr);
          }
      else
          {
          printf("File was %d characters long, not the 12 characters for a MAC address - no colons should be used\n",strlen(sAddr));
          ret_val=FALSE;
          } 
 
      // Only close if it had been opened
      fclose(fP);
     }


    return (ret_val);

}


