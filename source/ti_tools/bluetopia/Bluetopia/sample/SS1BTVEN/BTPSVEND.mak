
MODULENAME := BTPSVEND
OBJS       := $(MODULENAME).o
LIBOUTPUT  := ./lib$(MODULENAME).a

ifndef CC
   CC = gcc
endif

ifndef AR
   AR = ar
endif

INCLDDIRS = -I../../include -I../../tibts/include
	
CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDLIBS = -lpthread

.PHONY:
all: $(LIBOUTPUT)

$(LIBOUTPUT): $(OBJS)
	$(AR) -x ../../tibts/lib/libSS1TIBTS.a
	$(AR) r $@ $? TIBTS.o TIBTSIM.o
	@rm TIBTS*

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm -f $(OBJS)
	-rm -f *~
	-rm -f $(LIBOUTPUT)
