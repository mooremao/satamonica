
ifndef CC
   CC = gcc
endif

INCLDDIRS = -I../../include       \
	    -I../../debug/include

ifndef SYSTEMLIBS
   SYSTEMLIBS = -lpthread -lm
endif
	
CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDFLAGS = -L../../lib -L../../btpsvend -L../../debug/lib $(GLOBLDFLAGS)

LDLIBS = -lSS1BTPS -lBTPSVEND -lSS1BTDBG $(SYSTEMLIBS) $(GLOBLDLIBS)

OBJS = LinuxSPP.o

.PHONY:
all: LinuxSPP

LinuxSPP: $(OBJS)

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm *.o
	-rm LinuxSPP

