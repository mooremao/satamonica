/*****< ss1tibts.h >***********************************************************/
/*      Copyright 2013 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  SS1TIBTS - The symbols provided in this API help with parsing and         */
/*             executing Bluetooth Scripts formatted in Texas Instrument's    */
/*             proprietary BTS format.                                        */
/*                                                                            */
/*  Author:  Samuel Hishmeh                                                   */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   09/19/13  S. Hishmeh     Initial creation.                               */
/******************************************************************************/
#ifndef __SS1TIBTSH__
#define __SS1TIBTSH__

  /* If building with a C++ compiler, make all of the definitions in    */
  /* this header have a C binding.                                      */
#ifdef __cplusplus

extern "C"
{

#endif

#include "TIBTSAPI.h"                  /* Vendor Specific API Prototypes. */

   /* Mark the end of the C bindings section for C++ compilers.         */
#ifdef __cplusplus

}

#endif

#endif
