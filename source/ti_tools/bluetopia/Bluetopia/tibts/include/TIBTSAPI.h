/*****< tibtsapi.h >***********************************************************/
/*      Copyright 2013 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  TIBTSAPI - The symbols provided in this API help with parsing and         */
/*             executing Bluetooth Scripts formatted in Texas Instrument's    */
/*             proprietary BTS format.                                        */
/*                                                                            */
/*  Author:  Samuel Hishmeh                                                   */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   09/19/13  S. Hishmeh     Initial creation.                               */
/******************************************************************************/
#ifndef __TIBTSAPIH__
#define __TIBTSAPIH__

#include "SS1BTPS.h"            /* Bluetopia API Prototypes/Constants.  */

   /* Error codes that can be returned by this module.  Error Codes that*/
   /* are smaller than these (less than -1000) are related to the       */
   /* Bluetooth protocol stack itself (see BTERRORS.H).                 */
#define TIBTS_ERROR_COULD_NOT_READ_SCRIPT      (-2000)
#define TIBTS_ERROR_INVALID_SCRIPT_FORMAT      (-2001)

#define TIBTS_ERROR_END_OF_SCRIPT              (0x01)

   /* The following macros converts the OGF and OCF HCI Command         */
   /* Parameters to and from a 2 Byte Command Op Code.  They are        */
   /* provided as means to easily switch between the OGF/OCF, which are */
   /* commonly used in Bluetopia APIs, to the 2 byte op code, which TI  */
   /* commonly uses.  For the first macro the first parameter is the OGF*/
   /* and the second parameter is the OCF.  For the second and third    */
   /* macros the input parameter for both is the 2 byte command op code.*/
#define TIBTS_OGF_OCF_TO_COMMAND_OPCODE(_x, _y) HCI_MAKE_COMMAND_OPCODE(_x, _y)
#define TIBTS_COMMAND_OPCODE_TO_OGF(_x)         ((Byte_t)((_x) >> 10))
#define TIBTS_COMMAND_OPCODE_TO_OCF(_x)         ((Word_t)((_x) & (0x3FF)))

   /* The following structure is used to store a Bluetooth Controller's */
   /* Version information and is a parameter to the                     */
   /* TIBTS_Get_Script_Version() function.                              */
typedef struct _tagTIBTS_Script_Version_Info_t
{
   Byte_t ProjectVersion;
   Byte_t MajorVersion;
   Byte_t MinorVersion;
} TIBTS_Script_Version_Info_t;

   /* The following type definition is used as a handle to access data  */
   /* contained in the Bluetooth Script.                                */
typedef void *TIBTS_Script_Handle_t;

   /* The following structure is stores information used when opening a */
   /* Bluetooth Script and is used with the TIBTS_Open_Script()         */
   /* function.                                                         */
typedef struct _tagTIBTS_Script_Open_Parameters_t
{
   char    *ScriptPath;
   DWord_t  ScriptFlags;
} TIBTS_Script_Open_Parameters_t;

   /* The following flags are used with the ScriptFlags member of the   */
   /* TIBTS_Script_Open_Parameters_t structure to specify script parsing*/
   /* behavior.                                                         */

   /* The following flag specifies if the format of the script's data is*/
   /* that of a BTS Script (*.bts file) that has been converted to a    */
   /* Memory Array using a TI BTS conversion tool.  If this flag is set */
   /* then the format of the data is expected to be a memory array, if  */
   /* the flag is not set the format of the data is expected to be a BTS*/
   /* Script.                                                           */
   /* * NOTE * Scripts converted to a memory array contain only Send HCI*/
   /*          Command Actions.                                         */
#define TIBTS_SCRIPT_FLAGS_MEMORY_ARRAY_FORMAT    0x00000001

   /* Script action types.                                              */
typedef enum
{
   atSendHCICommand,
   atDelay
} TIBTS_Action_Type_t;

   /* The following structure stores information about an HCI Command   */
   /* Action that was parsed from the Bluetooth Script.                 */
typedef struct _tagTIBTS_Send_HCI_Command_Action_Data_t
{
   Byte_t  OGF;
   Word_t  OCF;
   Byte_t  CommandDataLength;
   Byte_t *CommandData;
} TIBTS_Send_HCI_Command_Action_Data_t;

   /* The following structure stores information about an Delay Action  */
   /* that was parsed from the Bluetooth Script.  The delay is specified*/
   /* here in milliseconds.                                             */
typedef struct _tagTIBTS_Delay_Action_Data_t
{
   DWord_t Delay;
} TIBTS_Delay_Action_Data_t;

   /* The following structure is used with TIBTS_Get_Next_Action() and  */
   /* stores the an action that was parsed from the BTS script.         */
typedef struct _tagTIBTS_Script_Action_t
{
   TIBTS_Action_Type_t Type;
   union
   {
      TIBTS_Send_HCI_Command_Action_Data_t SendHCICommandActionData;
      TIBTS_Delay_Action_Data_t            DelayActionData;
   } ActionData;
} TIBTS_Script_Action_t;

   /* The following function returns the Bluetooth Script Version       */
   /* Information for the Bluetooth Controller currently in use.  This  */
   /* function retrieves the Controller's Firmware Version Information  */
   /* and uses it to choose an appropriate Bluetooth Script Version.    */
   /* This function takes as input a Bluetooth Stack Identifier and an  */
   /* address specifying where the resulting Bluetooth Script Version   */
   /* Information should be stored.  This function returns 0 on success */
   /* and a negative value if an error occurs.  On success the Bluetooth*/
   /* Script's Version Information is stored in the last parameter.     */
BTPSAPI_DECLARATION int BTPSAPI TIBTS_Get_Script_Version(unsigned int BluetoothStackID, TIBTS_Script_Version_Info_t *ScriptVersionInfo);

#ifdef INCLUDE_BLUETOOTH_API_PROTOTYPES
   typedef int (BTPSAPI *PFN_TIBTS_Get_Script_Version_t)(unsigned int BluetoothStackID, TIBTS_Script_Version_Info_t *ScriptVersionInfo);
#endif

   /* The following function Opens a BTS Script.  This function takes as*/
   /* input an address that specifies where the open parameters should  */
   /* be read from.  This function returns a handle that can be used to */
   /* retrieve commands from the BTS Script on success and NULL if an   */
   /* error occurred.                                                   */
BTPSAPI_DECLARATION TIBTS_Script_Handle_t BTPSAPI TIBTS_Open_Script(TIBTS_Script_Open_Parameters_t *ScriptOpenParameters);

#ifdef INCLUDE_BLUETOOTH_API_PROTOTYPES
   typedef int (BTPSAPI *PFN_TIBTS_Open_Script_t)(TIBTS_Script_Open_Parameters_t *ScriptOpenParameters);
#endif

   /* The following function closes the Bluetooth Script and releases   */
   /* any memory associated with the script.  It returns 0 on success   */
   /* and a negative error code otherwise.                              */
BTPSAPI_DECLARATION int BTPSAPI TIBTS_Close_Script(TIBTS_Script_Handle_t ScriptHandle);

#ifdef INCLUDE_BLUETOOTH_API_PROTOTYPES
   typedef int (BTPSAPI *PFN_TIBTS_Close_Script_t)(TIBTS_Script_Handle_t ScriptHandle);
#endif

   /* The following function retrieves the next SCript Action from the  */
   /* BTS Script.  It takes as input a Script Handle and an address     */
   /* specifying where the Action's Parameters will be written.  This   */
   /* function returns 0 on success, TIBTS_ERROR_END_OF_SCRIPT if no    */
   /* more actions were found in the script, and a negative value if an */
   /* error occurred.                                                   */
BTPSAPI_DECLARATION int BTPSAPI TIBTS_Get_Next_Action(TIBTS_Script_Handle_t ScriptHandle, TIBTS_Script_Action_t *ScriptAction);

#ifdef INCLUDE_BLUETOOTH_API_PROTOTYPES
   typedef int (BTPSAPI *PFN_TIBTS_Get_Next_Action_t)(TIBTS_Script_Handle_t ScriptHandle, TIBTS_Script_Action_t *ScriptAction);
#endif

   /* The following function retrieves the next HCI Command from a BTS  */
   /* Script that is stored in a Memory Array.  It takes as input an    */
   /* address where the length of the data can be read from.  The second*/
   /* parameter specifies the location of the data.  And the third      */
   /* parameter specifies the address where the Command's Parameters    */
   /* will be written to.  This function returns 0 on success,          */
   /* TIBTS_ERROR_END_OF_SCRIPT if no more HCI Commands were found in   */
   /* the script, and a negative value if an error occurred.            */
   /* * NOTE * This command automatically updates the Data Length       */
   /*          parameter to the number of remaining bytes in the array, */
   /*          and also updates the the current location of the Data    */
   /*          pointer within the array.                                */
   /* * NOTE * This function only works with BTS scripts stored in a    */
   /*          memory array format.  BTS scripts stored in a memory     */
   /*          array format have been stripped of their action          */
   /*          information and contain only HCI commands.  Refer to TI's*/
   /*          Bluetooth Hardware Evaluation Tool (BHET) for more       */
   /*          information.                                             */
BTPSAPI_DECLARATION int BTPSAPI TIBTS_Get_Next_HCI_Command_Memory_Array(DWord_t *DataLength, Byte_t **Data, TIBTS_Send_HCI_Command_Action_Data_t *SendHCICommandActionData);

#ifdef INCLUDE_BLUETOOTH_API_PROTOTYPES
   typedef int (BTPSAPI *PFN_TIBTS_Get_Next_HCI_Command_Memory_Array_t)(DWord_t *DataLength, Byte_t **Data, TIBTS_Send_HCI_Command_Action_Data_t *SendHCICommandActionData);
#endif

#endif
