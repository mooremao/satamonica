/*****< btpstmr.h >************************************************************/
/*      Copyright 2000 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTPSTMR - Timer Module for Stonestreet One Bluetooth Protocol Stack       */
/*            Prototypes and Constants.                                       */
/*                                                                            */
/*  Author:  Damon Lange                                                      */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   04/25/00  D. Lange       Initial creation.                               */
/*   09/12/00  D. Lange       Modified to work with with Bluetooth Protocol   */
/*                            Stack Dynamic Link Library (DLL).               */
/*   05/31/01  D. Lange       Modified to work with the Generic Bluetooth     */
/*                            Protocol Stack.                                 */
/******************************************************************************/
#ifndef __BTPSTMRH__
#define __BTPSTMRH__

#include "BTPSKRNL.h"           /* BTPS Kernel Prototypes and Constants.      */

#include "BTPSCFG.h"            /* BTPS Configuration Constants.              */

#define MINIMUM_TIMER_RESOLUTION        (BTPS_CONFIGURATION_TIMER_MINIMUM_TIMER_RESOLUTION_MS)
                                                /* This is the minimum time   */
                                                /* (in Milliseconds) that a   */
                                                /* a Timer can wait.  Any     */
                                                /* Number less than this will */
                                                /* be forced to wait for this */
                                                /* length of time.            */

#define BTPS_TIMER_NO_ASYNCHRONOUS_CALLBACK_BLUETOOTH_STACK_ID  ((unsigned int)(-1))
                                                /* This constant can be passed*/
                                                /* to functions that require  */
                                                /* a BluetoothStackID.  This  */
                                                /* functionality is present   */
                                                /* so that other internal     */
                                                /* modules (such as HCIDRV/   */
                                                /* HCICOMM/HCIUSB) can use the*/
                                                /* BTPSTMR timers when they   */
                                                /* do not possess a Bluetooth */
                                                /* Stack ID.  The caveat here */
                                                /* is that this can only be   */
                                                /* used for embedded devices  */
                                                /* where the entire library is*/
                                                /* built (i.e. the HCIDRV/    */
                                                /* HCICOMM/HCIUSB modules are */
                                                /* linked into the same       */
                                                /* library.                   */
                                                /* * NOTE * When this ID is   */
                                                /*          specified, it     */
                                                /*          also means the    */
                                                /*          callback will be  */
                                                /*          called directly   */
                                                /*          instead of being  */
                                                /*          queued into the   */
                                                /*          asynchronous      */
                                                /*          HCI callback for  */
                                                /*          dispatching.      */

   /* The following is a type definition of the User Supplied Timer     */
   /* Callback function.  This function is called in the Timer Thread   */
   /* and NOT the thread that the timer was established in.             */
   /* If the Callback function returns TRUE, then the Timer remains     */
   /* active (periodic), else if the function returns FALSE, then the   */
   /* timer is destroyed and the TimerID is invalid.                    */
   /* ** NOTE ** The caller should keep the processing of these Timer   */
   /*            Callbacks small because other Timers will not be able  */
   /*            to be called while one is being serviced.              */
   /* ** NOTE ** If the callback function gets hung in an infinite loop */
   /*            then ALL Timers will be hung and Timer System will be  */
   /*            Useless !!!!!!!  If this sounds like an outrageous     */
   /*            limitation, remember that unless a thread is created   */
   /*            for every timer, there is NO way around this in even   */
   /*            Windows.  This limitation exists in the Windows        */
   /*            SetTimer()/KillTimer() schema as well.  This is a      */
   /*            limitation placed on the way Windows handles Timer     */
   /*            Events (i.e. Not at a system level, but at an          */
   /*            Application level).                                    */
   /* ** NOTE ** BTPS_ChangeTimer() can be called from a Timer Callback,*/
   /*            however, the constraints placed on this mechanism mean */
   /*            that if the Timer is Changed, and the Callback function*/
   /*            returns FALSE (non periodic), then the Timer is        */
   /*            deleted.  If a Timer Callback changes a timer which    */
   /*            is itself, then the Timer Callback *MUST* return TRUE, */
   /*            or the Timer will be deleted, making the call to       */
   /*            BTPS_ChangeTimer() worthless.                          */
   /* ** NOTE ** The return value of this function is irrelevant if     */
   /*            the Timer that the callback is servicing is deleted    */
   /*            via a call to BTPS_StopTimer() during the Timer        */
   /*            Callback.                                              */
typedef Boolean_t (BTPSAPI *BTPS_TimerCallbackFunction_t)(unsigned int BluetoothStackID, unsigned int TimerID, unsigned long CallbackParameter);

   /* Initialize the Timer Module.  This function must be called before */
   /* any other Timer Module functions can be called.  The Timer Module */
   /* provides a means for other application modules to use Timing      */
   /* Features (only accessible via a Timer Callback) that provide      */
   /* Timing functions.  This module does not provide any functions that*/
   /* are of a delay type nature (i.e. wait for a specified amount of   */
   /* time and then return).  This function returns zero if the         */
   /* Timer Module was successfully initialized, or a non-zero return   */
   /* code if the Timer Module was unable to be initialized.            */
int BTPS_InitializeTimerModule(void);

   /* This function cleans up any resources that the Timer Module       */
   /* currently owns.  Once this function is called, NO other Timer     */
   /* Module functions will work until a successful call to             */
   /* BTPS_InitializeTimerModule() is performed.  If the specified      */
   /* parameter is TRUE, then the module will make sure that NO         */
   /* functions that would require waiting/blocking on Mutexes/Events   */
   /* are called.  This parameter would be set to TRUE if this function */
   /* was called in a context where threads would not be allowed to run.*/
   /* If this function is called in the context where threads are       */
   /* allowed to run then this parameter should be set to FALSE.        */
void BTPS_CleanupTimerModule(Boolean_t ForceCleanup);

   /* Start a Timer of the specified Time out (in Milliseconds and NOT  */
   /* zero), and call the specified Timer Callback function when this   */
   /* completes.  The UserParameter is a parameter that is passed to    */
   /* the Timer Callback function when it is called.  This function     */
   /* returns the Timer ID of the newly created Timer.  This Timer ID   */
   /* can be used when calling BTPS_StopTimer() if the specified Timer  */
   /* has not expired.  The Timer ID's are greater than zero, and if    */
   /* zero is returned, then a Timer was not established.               */
   /* ** NOTE ** This function will NOT return Timer ID's that could be */
   /*            construed as negative (if the return value is typecast */
   /*            to an integer).  This is to guard against cases where  */
   /*            code might typecast the return to an integer and check */
   /*            to see if an error occurred (as negative return values */
   /*            usually indicate an error condition).                  */
   /* ** NOTE ** The minimum timout valus is MINIMUM_TIMER_RESOLUTION.  */
   /*            If a timeout that is less than this value (non zero)   */
   /*            is specified, the MINIMUM_TIMER_RESOLUTION will become */
   /*            the Time Out value the timer will have !!!!!           */
unsigned int BTPS_StartTimer(unsigned int BluetoothStackID, unsigned long CallbackParameter, BTPS_TimerCallbackFunction_t CallbackFunction, unsigned long TimeOut);

   /* This function Stops the specified Timer from expiring.  The       */
   /* Timer ID parameter must be obtained via a call to                 */
   /* BTPS_StartTimer().  This function returns TRUE if the Timer was   */
   /* aborted, or FALSE if the operation was unable to complete         */
   /* successfully.                                                     */
Boolean_t BTPS_StopTimer(unsigned int BluetoothStackID, unsigned int TimerID);

   /* This function Changes the specified Timer Time Out Period.  The   */
   /* Timer ID parameter must be obtained via a call to                 */
   /* BTPS_StartTimer().  This function returns TRUE if the Timer       */
   /* timeout was changed, FALSE if the operation was unable to complete*/
   /* successfully.                                                     */
   /* ** NOTE ** The minimum timout valus is MINIMUM_TIMER_RESOLUTION.  */
   /*            If a timeout that is less than this value (non zero)   */
   /*            is specified, the MINIMUM_TIMER_RESOLUTION will become */
   /*            the Time Out value the timer will have !!!!!           */
   /* ** NOTE ** This function can be called from a Timer Callback,     */
   /*            however, the constraints placed on this mechanism mean */
   /*            that if the Timer is Changed, and the Callback function*/
   /*            returns FALSE (non periodic), then the Timer is        */
   /*            deleted.  If a Timer Callback changes a timer which    */
   /*            is itself, then the Timer Callback *MUST* return TRUE, */
   /*            or the Timer will be deleted, making the call to       */
   /*            BTPS_ChangeTimer() worthless.                          */
Boolean_t BTPS_ChangeTimer(unsigned int BluetoothStackID, unsigned int TimerID, unsigned long NewTimeOut);

   /* This function is a utility function that exists to determine if   */
   /* there are currently any active timers.  This function can be used */
   /* to provide better power management handling on single threaded    */
   /* systems because if there are no active timers then the main       */
   /* scheduler loop does not need to execute (at least to service the  */
   /* the timers).  This function returns TRUE if there are currently   */
   /* active timers or FALSE if there are currently no active timers.   */
   /* * NOTE * This function is ONLY applicable to single threaded      */
   /*          systems.  Multi-threaded systems will always return TRUE.*/
Boolean_t BTPS_QueryTimersActive(unsigned int BluetoothStackID);

   /* This function is used to process and expire asynchronous timers   */
   /* from within stack for non-threaded applications.  only timers that*/
   /* were created with a Bluetooth stack ID that was                   */
   /* BTPS_TIMER_NO_ASYNCHRONOUS_CALLBACK_BLUETOOTH_STACK_ID will be    */
   /* processed.                                                        */
void BTPS_ProcessAsynchronousTimers(void);

#endif
