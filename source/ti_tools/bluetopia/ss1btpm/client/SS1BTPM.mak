
ifndef CC
   CC = gcc
endif

ifndef AR
   AR = ar
endif

ifndef BLUETOPIA_PATH
   BLUETOPIA_PATH = ../../../Bluetopia
endif

INCLDDIRS = -I../../include                           \
            -I../../include/client                    \
            -I$(BLUETOPIA_PATH)/include               \
            -I$(BLUETOPIA_PATH)/profiles/GATT/include

CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDLIBS = -lpthread

OBJS =  

SRC_LIBS = libSS1BTPML.a                               \
           ../../btpmerr/libBTPMERR.a                  \
           ../../btpmmodc/client/libBTPMMODC.a         \
           ../../modules/btpmaudm/client/libBTPMAUDM.a \
           ../../modules/btpmbasm/client/libBTPMBASM.a \
           ../../modules/btpmfmpm/client/libBTPMFMPM.a \
           ../../modules/btpmftpm/client/libBTPMFTPM.a \
           ../../modules/btpmhdpm/client/libBTPMHDPM.a \
           ../../modules/btpmhfrm/client/libBTPMHFRM.a \
           ../../modules/btpmhrpm/client/libBTPMHRPM.a \
           ../../modules/btpmhidm/client/libBTPMHIDM.a \
           ../../modules/btpmhogm/client/libBTPMHOGM.a \
           ../../modules/btpmmapm/client/libBTPMMAPM.a \
           ../../modules/btpmoppm/client/libBTPMOPPM.a \
           ../../modules/btpmpanm/client/libBTPMPANM.a \
           ../../modules/btpmpbam/client/libBTPMPBAM.a \
           ../../modules/btpmpxpm/client/libBTPMPXPM.a 

.PHONY:
all: libSS1BTPM.a

libSS1BTPM.a: $(OBJS) $(SRC_LIBS)
	$(AR) cr $@ $(OBJS)
	@for i in $?; do                                  \
	   if echo "$$i" | grep -q '\.a$$'; then          \
	      echo -n "Merging library $$i ... " &&       \
	      DIR=`basename "_$$i"` &&                    \
	      mkdir -p "$$DIR" &&                         \
	      cd "$$DIR" &&                               \
	      $(AR) x "../$$i" &&                         \
	      $(AR) cr "../$@" * &&                       \
	      cd .. &&                                    \
	      rm -rf "$$DIR" &&                           \
	      echo "Success" || (echo "Failed!" && exit); \
	   fi;                                            \
	done 2>/dev/null

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm *.o
	-rm libSS1BTPM.a

