
ifndef CC
   CC = gcc
endif

ifndef BLUETOPIA_PATH
   BLUETOPIA_PATH = ../../../Bluetopia
endif

INCLDDIRS = -I../../include             \
	    -I../../include/server                    \
	    -I$(BLUETOPIA_PATH)/include               \
	    -I$(BLUETOPIA_PATH)/profiles/GATT/include

ifndef SYSTEMLIBS
   SYSTEMLIBS = -lpthread -lm
endif

CFLAGS = -Wall $(DEFS) $(INCLDDIRS) $(GLOBINCLDDIRS) -O2 $(GLOBCFLAGS)

LDFLAGS = -L.                                         \
	  -L../../btpmerr                             \
	  -L../../btpmmodc/server                     \
	  -L../../modules/btpmaudm/server             \
	  -L../../modules/btpmbasm/server             \
	  -L../../modules/btpmfmpm/server             \
	  -L../../modules/btpmftpm/server             \
	  -L../../modules/btpmhdpm/server             \
	  -L../../modules/btpmhfrm/server             \
	  -L../../modules/btpmhidm/server             \
	  -L../../modules/btpmhogm/server             \
	  -L../../modules/btpmhrpm/server             \
	  -L../../modules/btpmmapm/server             \
	  -L../../modules/btpmoppm/server	      \
	  -L../../modules/btpmpanm/server             \
	  -L../../modules/btpmpbam/server             \
	  -L../../modules/btpmpxpm/server             \
	  -L../../modules/btpmcscm/server             \
	  -L$(BLUETOPIA_PATH)/lib                     \
	  -L$(BLUETOPIA_PATH)/debug/lib               \
	  -L$(BLUETOPIA_PATH)/profiles/AVCTP/lib      \
	  -L$(BLUETOPIA_PATH)/profiles/ISPP/lib       \
	  -L$(BLUETOPIA_PATH)/profiles/AVRCP/lib      \
	  -L$(BLUETOPIA_PATH)/profiles/GAVD/lib       \
	  -L$(BLUETOPIA_PATH)/profiles/GATT/lib       \
	  -L$(BLUETOPIA_PATH)/profiles/Audio/lib      \
	  -L$(BLUETOPIA_PATH)/profiles/HDP/lib        \
	  -L$(BLUETOPIA_PATH)/profiles/HDSET/lib      \
	  -L$(BLUETOPIA_PATH)/profiles/HFRE/lib       \
	  -L$(BLUETOPIA_PATH)/profiles/HID/lib        \
	  -L$(BLUETOPIA_PATH)/profiles/MAP/lib        \
	  -L$(BLUETOPIA_PATH)/profiles/OPP/lib        \
	  -L$(BLUETOPIA_PATH)/profiles/PAN/lib        \
	  -L$(BLUETOPIA_PATH)/profiles/PBAP/lib       \
	  -L$(BLUETOPIA_PATH)/profiles/HID_Host/lib   \
	  -L$(BLUETOPIA_PATH)/profiles/ISPP/lib       \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/BAS/lib   \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/DIS/lib   \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/HIDS/lib  \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/HRS/lib   \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/IAS/lib   \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/LLS/lib   \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/SCPS/lib  \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/TPS/lib   \
	  -L$(BLUETOPIA_PATH)/profiles_gatt/CSCS/lib   \
	  -L$(BLUETOPIA_PATH)/VNET/lib                \
	  $(GLOBLDFLAGS)

LDLIBS = -lSS1BTPML   \
	 -lBTPMERR    \
	 -lBTPMMODC   \
	 -lBTPMAUDM   \
	 -lBTPMBASM   \
	 -lBTPMFMPM   \
	 -lBTPMFTPM   \
	 -lBTPMHDPM   \
	 -lBTPMHFRM   \
	 -lBTPMHRPM   \
	 -lBTPMHIDM   \
	 -lBTPMHOGM   \
	 -lBTPMMAPM   \
	 -lBTPMOPPM   \
	 -lBTPMPANM   \
	 -lBTPMPBAM   \
	 -lBTPMPXPM   \
	 -lSS1BTPS    \
	 -lBTPSFILE   \
	 -lSS1BTDBG   \
	 -lSS1BTAUD   \
	 -lSS1BTAVR   \
	 -lSS1BTAVC   \
	 -lSS1BTBAS   \
	 -lSS1BTDIS   \
	 -lSS1BTGAV   \
	 -lSS1BTGAT   \
	 -lSS1BTHDP   \
	 -lSS1BTHFR   \
	 -lSS1BTHIDH  \
	 -lSS1BTHID   \
	 -lSS1BTHIDS  \
	 -lSS1BTHRS   \
	 -lSS1BTIAS   \
	 -lSS1BTLLS   \
	 -lSS1BTMAP   \
	 -lSS1BTOPP   \
	 -lSS1BTPAN   \
	 -lSS1BTSCPS  \
	 -lSS1BTPBA   \
	 -lSS1BTTPS   \
	 -lSS1VNET    \
	 -lBTPSVEND   \
	 -lSS1BTVS    \
	 -lBTPMCSCM   \
	 -lSS1BTCSCS  \
	 $(SYSTEMLIBS) \
	 $(GLOBLDLIBS)

OBJS = BTPMSRVR.o

.PHONY:
all: SS1BTPM
	$(CC) $(LDFLAGS) $(OBJS) $(LDLIBS) -o SS1BTPM

SS1BTPM: $(OBJS)

.PHONY: clean veryclean realclean
clean veryclean realclean:
	-rm *.o
	-rm SS1BTPM

