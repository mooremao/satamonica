/*****< btpmsrvr.c >***********************************************************/
/*      Copyright 2010 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTPMSRVR - Bluetopia Platform Manager Main Application Entry point for    */
/*             Linux.                                                         */
/*                                                                            */
/*  Author:  Damon Lange                                                      */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   07/07/10  D. Lange        Initial creation.                              */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "SS1BTPM.h"             /* BTPM API Prototypes and Constants.        */
#include "BTPMSRVR.h"            /* BTPM Main Application Proto./Constants.   */


#define SLEEP_NOT_ALLOWED_FILENAME   "/tmp/BT_Sleep_Not_Allowed"
#define SLEEP_REQ_FILENAME "/tmp/BT_Sleep_Req"

   /* Internal Function Prototypes.                                     */
static void BTPSAPI InitializationDoneCallback(void *CallbackParameter);
static void BTPSAPI DEVM_Event_Callback(DEVM_Event_Data_t *EventData, void *CallbackParameter);
static void BTPSAPI HCI_Sleep_Callback(Boolean_t SleepAllowed, unsigned long CallbackParameter);
static void SignalSleepAllowed(void);
static void SignalSleepDisallowed(void);

   /* The following callback is issued after the PM Server has been     */
   /* initialized and is used to register for DEVM Events.              */
static void BTPSAPI InitializationDoneCallback(void *CallbackParameter)
{
   int Result;

   /* Register for DEVM Events.                                         */
   if((Result = DEVM_RegisterEventCallback(DEVM_Event_Callback, NULL)) <= 0)
   {
      /* An error occurred, display the event in the terminal.          */
      printf("Error: DEVM_RegisterEventCallback() Failure: %s. Result: %d\r\n", ERR_ConvertErrorCodeToString(Result), Result);
   }
}

   /* The following callback is issued when DEVM Events occur.          */
static void BTPSAPI DEVM_Event_Callback(DEVM_Event_Data_t *EventData, void *CallbackParameter)
{
   int                           Result;
   unsigned int                  BluetoothStackID;
   HCI_HCILLConfiguration_t      HCILLConfig;
   HCI_Driver_Reconfigure_Data_t DriverReconfigureData;
   
   /* Check if this is a powered-on event.                              */
   if((EventData) && (EventData->EventType == detDevicePoweredOn))
   {
  
      /* This is a powered-on event, query the Bluetooth Stack ID.      */
      if((Result = DEVM_QueryDeviceBluetoothStackID()) > 0)
      {
         BluetoothStackID = (unsigned int)Result;
         
         HCILLConfig.SleepCallbackFunction        = HCI_Sleep_Callback;
         HCILLConfig.SleepCallbackParameter       = 0;
         DriverReconfigureData.ReconfigureCommand = HCI_COMM_DRIVER_RECONFIGURE_DATA_COMMAND_CHANGE_HCILL_PARAMETERS;
         DriverReconfigureData.ReconfigureData    = (void *)&HCILLConfig;

         /* Register the sleep mode callback.  Note that if this        */
         /* function returns greater than 0 then sleep is currently     */
         /* enabled.                                                    */
         Result = HCI_Reconfigure_Driver(BluetoothStackID, FALSE, &DriverReconfigureData);
         
         if(Result > 0)
         {
            /* Flag that sleep mode is enabled.                         */
            SignalSleepAllowed();
         }
         else
         {
            SignalSleepDisallowed();
            
            if(Result < 0)
            {
               printf("Error: HCI_Reconfigure_Driver() returned the error code %d.\r\n", Result);
            }
         }
      }
      else
      {
         /* An error occurred while querying the Bluetooth Stack ID,    */
         /* display the error in the terminal.                          */
         printf("Error: DEVM_QueryDeviceBluetoothStackID() Failure: %s. Result: %d\r\n", ERR_ConvertErrorCodeToString(Result), Result);
      }
   }

   /* If this was a powered off message, then host can go to sleep */
   if((EventData) && (EventData->EventType == detDevicePoweredOff))
   {
      SignalSleepAllowed();
   } 
}

   /* The following is the HCI Sleep Callback.  This is registered with */
   /* the stack to note when the Host processor may enter into a sleep  */
   /* mode.                                                             */
static void BTPSAPI HCI_Sleep_Callback(Boolean_t SleepAllowed, unsigned long CallbackParameter)
{
  
	struct stat st;

	/* wait if the app has created the sleep request file */
	while (stat(SLEEP_REQ_FILENAME, &st) == 0) {
		BTPS_Delay(1);  // 1ms delay
	}

	if(SleepAllowed) {
		SignalSleepAllowed();
	}
	else {
		SignalSleepDisallowed();
	}
}

   /* This function is called when the processor is allowed to sleep.   */
static void SignalSleepAllowed(void)
{

	/* Remove the sleep allowed file. Note that this function is called  */
	/* when SS1BTPM is started and in this case the file may or may not  */
	/* exist and therefore we don't check on the return value of the     */
	/* system call.                                                      */
	if (remove(SLEEP_NOT_ALLOWED_FILENAME) != 0) {
		printf("remove() failed\n");
	}
}

   /* This function is called when the processor is not allowed to      */
   /* sleep.                                                            */
static void SignalSleepDisallowed(void)
{
	FILE *fp;

	fp = fopen(SLEEP_NOT_ALLOWED_FILENAME,"w");
	if (fp == NULL) {
	printf("Error: Could not create the sleep not allowed file.r\n");
	}
	else {
	fclose(fp);
	}
}
   /* Main Program Entry Point.                                         */
int main(int argc, char *argv[])
{
   int                                ret_val;
   BTPM_Initialization_Info_t         InitializationInfo;
   BTPM_Debug_Initialization_Data_t   DebugInitializationInfo;
   DEVM_Initialization_Data_t         DeviceManagerInitializationInfo;
   DEVM_Default_Initialization_Data_t DefaultInitializationData;

#if 0

   /* This is the bare minimum Server initialization.  Initialize:      */
   /*    - All default values.                                          */
   ret_val = BTPM_Main(NULL, NULL, NULL);

#endif

   /* Let's go ahead and specify a Platform Specific debugging string.  */
   BTPS_MemInitialize(&DebugInitializationInfo, 0, sizeof(DebugInitializationInfo));
   DebugInitializationInfo.PlatformSpecificInitData = "SS1BTPMS";
   
   /* Initialize the DEVM default initialization parameters.            */
   BTPS_MemInitialize(&DefaultInitializationData, 0, sizeof(DefaultInitializationData));
   DefaultInitializationData.InitializationOverrideFlags                           = DEVM_INITIALIZATION_DATA_OVERRIDE_FLAGS_DEVM_ALLOW_LE_PARAM_DATA;
   DefaultInitializationData.DefaultLEAllowableParameters.Flags                    = DEVM_DEFAULT_ALLOWABLE_LE_PARAMETERS_MINIMUM_INTERVALS_VALID;
   DefaultInitializationData.DefaultLEAllowableParameters.Minimum_Minimum_Interval = BTPM_CONFIGURATION_DEVICE_MANAGER_DEFAULT_MIN_ALLOWABLE_MIN_CONNECTION_INTERVAL;
   DefaultInitializationData.DefaultLEAllowableParameters.Minimum_Maximum_Interval = BTPM_CONFIGURATION_DEVICE_MANAGER_DEFAULT_MIN_ALLOWABLE_MAX_CONNECTION_INTERVAL;
   
   /* Initialize the DEVM initialization parameters.                    */
   BTPS_MemInitialize(&DeviceManagerInitializationInfo, 0, sizeof(DeviceManagerInitializationInfo));
   DeviceManagerInitializationInfo.DefaultInitializationData = &DefaultInitializationData;

   /* Now go ahead and initialize the correct global configuration      */
   /* structure.                                                        */
   BTPS_MemInitialize(&InitializationInfo, 0, sizeof(InitializationInfo));
   InitializationInfo.DebugInitializationInfo         = &DebugInitializationInfo;
   InitializationInfo.DeviceManagerInitializationInfo = &DeviceManagerInitializationInfo;
   
   /* Do nothing other than call the Library entry point.               */
   ret_val = BTPM_Main(&InitializationInfo, InitializationDoneCallback, NULL);

   return(ret_val);
}

