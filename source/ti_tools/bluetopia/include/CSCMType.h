/*****< cscmtype.h >***********************************************************/
/*      Copyright 2012 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  CSCMTYPE - Cycling Speed & Cadence Manager API Type Definitions and       */
/*    Constants for Stonestreet One Bluetooth Protocol Stack Platform Manager.*/
/*                                                                            */
/*  Author:  Ryan Byrne                                                       */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   11/12/12  R. Byrne       Initial creation.                               */
/******************************************************************************/
#ifndef __CSCMTYPEH__
#define __CSCMTYPEH__

#include "SS1BTPS.h"      /* BTPS Protocol Stack Prototypes/Constants.        */

   /* The following enumerated type represents all of the Connection    */
   /* types that are supported by CSCM.                                 */
typedef enum _CSCM_ConnectionType_t
{
   CSCM_CT_Sensor,
   CSCM_CT_Collector,
   CSCM_ConnectionTypes
} CSCM_ConnectionType_t;

typedef enum _CSCM_SensorType_t
{
	CSCM_ST_Wheel,
	CSCM_ST_Crank,
} CSCM_SensorType_t;


   /* The following enumerated type represents all of the Category types*/
   /* that are supported by CSCM.                                       */
typedef enum _CSCM_SensorLocation_t
{
   CSCM_SL_Other,
   CSCM_SL_TopOfShow,
   CSCM_SL_InShow,
   CSCM_SL_Hip,
   CSCM_SL_FrontWheel,
   CSCM_SL_LeftCrank,
   CSCM_SL_RightCrank,
   CSCM_SL_LeftPedal,
   CSCM_SL_RightPedal,
   CSCM_SL_FrontHub,
   CSCM_SL_RearDropout,
   CSCM_SL_Chainstay,
   CSCM_SL_RearWheel,
   CSCM_SL_RearHub,
   CSCM_SL_Chest,
   CSCM_SL_Reserved,
} CSCM_Sensor_Location_t;

#endif

