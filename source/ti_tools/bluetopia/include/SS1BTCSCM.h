/*****< ss1btcscm.h >**********************************************************/
/*      Copyright 2012 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  SS1BTCSCM - TI Bluetooth Cycling Speed and Cadence (CSCP) Manager for     */
/*              Bluetopia Platform Manager Type Definitions,  Prototypes,     */
/*              and Constants.                                                */
/*                                                                            */
/*  Author:  Ryan Byrne                                                       */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   11/12/12  R. Byrne       Initial creation.                               */
/******************************************************************************/
#ifndef __SS1BTCSCMH__
#define __SS1BTCSCMH__

#include "SS1BTPM.h"             /* Platform Manager Prototypes/Constants.    */

#include "CSCMAPI.h"             /* CSC Manager Prototypes/Constants.         */

#include "SS1BTPS.h"             /* Bluetopia Core Prototypes/Constants.      */

   /* The following function is responsible for initializing/cleaning up*/
   /* the Bluetopia Platform Manager CSC Manager Module.  This function */
   /* should be registered with the Bluetopia Platform Manager Module   */
   /* Handler and will be called when the Platform Manager is           */
   /* initialized (or shut down).                                       */
void BTPSAPI CSCM_InitializationHandlerFunction(Boolean_t Initialize, void *InitializationData);
void BTPSAPI CSCM_DeviceManagerHandlerFunction(DEVM_Event_Data_t *EventData);

#endif
