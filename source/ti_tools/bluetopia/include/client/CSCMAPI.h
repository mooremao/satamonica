/*****< cscmapi.h >************************************************************/
/*      Copyright 2015 Texas Instruments LTD.                                 */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  CSCMAPI - Cycling Speed and Cadence (CSC) Manager API for TI              */
/*            Bluetooth Protocol Stack Platform Manager.                      */
/*                                                                            */
/*  Author:  Kobi L.                                                          */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  Name           Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   02/06/15  Kobi L.        Initial creation.                               */
/******************************************************************************/
#ifndef __CSCMAPIH__
#define __CSCMAPIH__

#include "BTAPITyp.h"            /* Bluetooth API Type Definitions.           */

#include "SS1BTPM.h"             /* Platform Manager Prototypes/Constants.    */
#include "CSCMType.h"



/******************************************************************************
 * User Events Definitions -
 *  User shall register an event callback either through RegisterSensor or
 *  RegisterCollector. The 'CSCM_Event_Data_t' stucture defines the general
 *  CSC Event Data for in a Type-Length-Value format. The Events includes
 *  Both A-Synchronous Notification/Indication and Responses to User
 *  "GET" Requests
 *******************************************************************************/


   /* The following enumerated type represents the CSC Manager Event
    * Types that are notified to the user that is registered on CSCM
    * Collector Events (corresponding structures can be found below).
    */
typedef enum
{
   etCSC_Connected,
         /* Indicates a successful connection to CSC Sensor */

   etCSC_Disconnected,
         /* Indicates a disconnection from CSC Sensor */

   etCSC_Measurement,
         /* Indicates the reception of CSC Measurement Notification */

   etCSC_GetSensorFeaturesResponse,
         /* Sensor Features Response */

   etCSC_GetSensorLocationResponse,
      /* Sensor Location Response */

   etCSC_GetSupportedLocationsResponse,
      /* Supported Sensor Location Response */

   etCSC_GenericStatusResponse
      /* Generic Status Response */
} CSCM_Event_Type_t;


   /* The following structure is a container structure that holds the   */
   /* information that is returned in a etCSC_Connected event.          */
   /* Note that this event is NOT issued upon connecting to a device    */
   /* (which is indicated by DEVM event), but after the remote services */
   /* are identified (and CSC support is there). If device is already   */
   /* known, the event will be notified automatically upon successful   */
   /* ConnectToRemoteDevice that supports CSC service. Otherwise, it    */
   /* be notified upon QueryDeviceServices request.                     */
#define CSCM_CONNECTED_FLAG_MEASUREMENT_NOTIFCATIONS_ENABLED 	0x00000001
#define CSCM_CONNECTED_FLAG_CP_SUPPORTED			            0x00000002
typedef struct _CSCM_Connected_Event_Data_t
{
   BD_ADDR_t              RemoteDeviceAddress;
   CSCM_ConnectionType_t  ConnectionType;
   unsigned long          ConnectedFlags;
   unsigned int           CSCFlags;
} CSCM_Connected_Event_t;


   /* The following structure is a container structure that holds the   */
   /* information that is returned in a etCSC_Disconnected event.       */
typedef struct _CSCM_Disconnected_Event_Data_t
{
   BD_ADDR_t              RemoteDeviceAddress;
   CSCM_ConnectionType_t  ConnectionType;
   unsigned long          DisconnectedFlags;
} CSCM_Disconnected_Event_t;

   /* The following structure is a container structure that holds the   */
   /* information that is returned in a etCSC_Measurement               */
   /* event.                                                            */
typedef struct _CSCM_Measurement_Event_t
{
   BD_ADDR_t      RemoteDeviceAddress;
   Byte_t         Flags;
   unsigned long  ComulativeWheelValue;
   unsigned short LastWheelEventTime;
   unsigned long  ComulativeCrankValue;
   unsigned short LastCrankEventTime;
} CSCM_Measurement_Event_t;

   /* The following structure is a container structure that holds the   */
   /* information that is returned in a                                 */
   /* etCSC_GetSensorLocationResponse event.                            */
typedef struct _CSCM_GetSensorLocationResponse_Event_t
{
   BD_ADDR_t               RemoteDeviceAddress;
   int                     Status;
   Byte_t                  Location;
} CSCM_GetSensorLocationResponse_Event_t;

   /* The following structure is a container structure that holds the   */
   /* information that is returned in a                                 */
   /* etCSC_GetSensorFeaturesResponse event.                            */
typedef struct _CSCM_GetSensorFeaturesResponse_Event_t
{
   BD_ADDR_t               RemoteDeviceAddress;
   int                     Status;
   Word_t                  SupportedFeatures;
} CSCM_GetSensorFeaturesResponse_Event_t;

   /* The following structure is a container structure that holds the   */
   /* information that is returned in a                                 */
   /* etCSC_GetSupportedLocationsResponse event.                        */
typedef struct _CSCM_GetSupportedLocationsResponse_Event_t
{
   BD_ADDR_t               RemoteDeviceAddress;
   int                     Status;
   unsigned int            SensorLocationsBitMask;
} CSCM_GetSupportedLocationResponse_Event_t;

/* The following structure is a container structure that holds the   */
 /* information that is returned in a                                 */
 /* etCSC_GetSupportedLocationsResponse event.                        */
typedef struct _CSCM_GenericStatusResponse_Event_t
{
 BD_ADDR_t               RemoteDeviceAddress;
 int                     Status;
} CSCM_GenericStatusResponse_Event_t;



   /* Generic CSCM Event TLV structure                                  */
typedef struct _CSCM_Event_Data_t
{
   unsigned int      EventType; /* CSCM_Event_Type_t */
   unsigned int      EventLength;
   unsigned int      EventClientID;
   union
   {
      CSCM_Connected_Event_t                    ConnectedEventData;
      CSCM_Disconnected_Event_t                 DisconnectedEventData;
      CSCM_Measurement_Event_t                  MeasurementEventData;
      CSCM_GetSensorLocationResponse_Event_t    GetSesnorLocationEventData;
      CSCM_GetSensorFeaturesResponse_Event_t    GetSesnorFeaturesEventData;
      CSCM_GetSupportedLocationResponse_Event_t GetSupportedLocationsEventData;
      CSCM_GenericStatusResponse_Event_t        GenericStatusRepsonseEventData;
   } EventData;
} CSCM_Event_Data_t;

/* The following declared type represents the Prototype Function for */
/* an Event Callback.  This function will be called whenever the     */
/* Cycling Speed and Cadence Profile (CSC) Manager dispatches an     */
/* event. This function passes to the caller the CSC Manager Event   */
/* and the User Handler that was specified when this Callback was    */
/* installed.  The caller is free to use the contents of the Event   */
/* Data ONLY in the context of this callback.                        */
/* If the caller requires the Data for a longer period of            */
/* time, then the callback function MUST copy the data into another  */
/* Data Buffer.  This function is guaranteed NOT to be invoked more  */
/* than once simultaneously for the specified installed callback     */
/* (i.e. this function DOES NOT have be reentrant).  Because of      */
/* this, the processing in this function should be as efficient as   */
/* possible.  It should also be noted that this function is called in*/
/* the Thread Context of a Thread that the User does NOT own.        */
/* Therefore, processing in this function should be as efficient as  */
/* possible (this argument holds anyway because another Message will */
/* not be processed while this function call is outstanding).        */
/* ** NOTE ** This function MUST NOT block and wait for events that  */
/*            can only be satisfied by Receiving other Events.  A    */
/*            deadlock WILL occur because NO Event Callbacks will be */
/*            issued while this function is currently outstanding.   */
typedef void (BTPSAPI *CSCM_Event_Callback_t)(CSCM_Event_Data_t *EventData, void *CallbackParameter);



/******************************************************************************
 * User Request Definitions -
 *  User shall register an CSCM event callback (either as Sensor or Collector)
 *  before it can use any other services of the profile.
 *  It shall first receive the etCSC_Connected event before issuing further
 *  requests to a specific device
 ******************************************************************************/



   /* The following structure is a container structure that holds the   */
   /* information that is required by 'CSCM_RegisterSensor'.            */
#define CSCM_SENSOR_FEATURE_WHEEL_SUPPORT             0x0001
#define CSCM_SENSOR_FEATURE_CRANK_SUPPORT             0x0002
#define CSCM_SENSOR_FEATURE_MULTI_LOCATION_SUPPORT    0x0004

#define CSCM_SENSOR_LOCATION_OTHER             (1<<CSCM_SL_Other)
#define CSCM_SENSOR_LOCATION_TOP_OF_SHOE       (1<<CSCM_SL_TopOfShow)
#define CSCM_SENSOR_LOCATION_IN_SHOE           (1<<CSCM_SL_InShow)
#define CSCM_SENSOR_LOCATION_HIP               (1<<CSCM_SL_Hip)
#define CSCM_SENSOR_LOCATION_FRONT_WHEEL       (1<<CSCM_SL_FrontWheel)
#define CSCM_SENSOR_LOCATION_LEFT_CRANK        (1<<CSCM_SL_LeftCrank)
#define CSCM_SENSOR_LOCATION_RIGHT_CRANK       (1<<CSCM_SL_RightCrank)
#define CSCM_SENSOR_LOCATION_LEFT_PEDAL        (1<<CSCM_SL_LeftPedal)
#define CSCM_SENSOR_LOCATION_RIGHT_PEDAL       (1<<CSCM_SL_RightPedal)
#define CSCM_SENSOR_LOCATION_FRONT_HUB         (1<<CSCM_SL_FrontHub)
#define CSCM_SENSOR_LOCATION_REAR_DROPOUT      (1<<CSCM_SL_RearDropout)
#define CSCM_SENSOR_LOCATION_CHAINSTAY         (1<<CSCM_SL_Chainstay)
#define CSCM_SENSOR_LOCATION_REAR_WHEEL        (1<<CSCM_SL_RearWheel)
#define CSCM_SENSOR_LOCATION_REAR_HUB          (1<<CSCM_SL_RearHub)
#define CSCM_SENSOR_LOCATION_CHEST             (1<<CSCM_SL_Chest)
#define CSCM_SENSOR_LOCATION_ALL               ((1<<CSCM_SL_Reserved)-1)

typedef struct _CSCM_ServiceInfo_t
{
   Word_t        SensorFeatures;
   unsigned int  SensorLocationsBitMask;
} CSCM_ServiceInfo_t;


   /* The following structure is a generic structure that holds the   */
   /* information that is required by:                           */
   /* CSCM_GetSensorFeatures, CSCM_GetSensorLocation,                 */
   /* CSCM_GetSupportedSensorLocations                                */
typedef struct _CSCM_GetValue_t
{
   BD_ADDR_t     RemoteDeviceAddress;
} CSCM_GetValue_t;

   /* The following structure that holds the information that is      */
   /* required by CSCM_SetSensorLocation                              */
typedef struct _CSCM_SetSensorLocation_t
{
   BD_ADDR_t     RemoteDeviceAddress;
   Word_t        SensorLocation;
} CSCM_SetSensorLocation_t;

   /* The following structure that holds the information that is       */
   /* required by CSCM_SetCumulativeValue                              */
typedef struct _CSCM_SetCumulativeValue_t
{
   BD_ADDR_t           RemoteDeviceAddress;
   unsigned int        CumulativeValue;
} CSCM_SetCumulativeValue_t;

   /* The following structure that holds the information that is      */
   /* required by CSCM_TriggerCalibarion                              */
typedef struct _CSCM_TriggerCalibarion_t
{
   BD_ADDR_t     RemoteDeviceAddress;
} CSCM_TriggerCalibarion_t;

   /* The following structure that holds the information that is      */
   /* required by CSCM_EnableNotifications                            */
typedef struct _CSCM_EnableNotifications_t
{
   BD_ADDR_t     RemoteDeviceAddress;
   Byte_t        Enable;          /* 0 - Disable, 1 - Enable          */
} CSCM_EnableNotifications_t;


   /* The following structure that holds the information that is      */
   /* required by _CSCM_ReportMeasurement                             */
typedef struct _CSCM_ReportMeasurement_t
{
   CSCM_SensorType_t         SensorType;
   unsigned long             MeasuredValue;
   unsigned short            LastEventTime;
} CSCM_ReportMeasurement_t;

   /* The following structure is a container structure that holds the  */
   /* information that is returned in a etCSCMeasurement               */
   /* event.                                                           */
typedef struct _CSCM_StartPeriodicNotification_t
{
   unsigned int NotificationPeriod; /* Unit is 100 Milliseconds, 0 - to stop the measurements */
} CSCM_StartPeriodicNotification_t;



   /* The following function is provided for registration of Cycling    */
   /* Speed and Cadence (CSC) Sensor Context.                           */
   /* It should be called before any other calls to Sensor methods.     */
   /* The Service contains the configurable service info:               */
   /*   SupportedFeatures - CSCS support features                       */
   /*   SensorLocationsBitMask - CSCS supported Locations               */
   /* Callback Function and Callback parameter will be used for CSC     */
   /* Specific Event notification (see above).                          */
   /* * NOTE * If this function returns success (greater than zero) then*/
   /*          this value (ClientID) should be passed as first parameter*/
   /*          for all the CSC Sensor methods.                          */
BTPSAPI_DECLARATION int BTPSAPI CSCM_RegisterSensor(CSCM_ServiceInfo_t *ServiceInfo,
                                       CSCM_Event_Callback_t CallbackFunction,
                                       void *CallbackParameter);

#ifdef INCLUDE_BLUETOOTH_API_PROTOTYPES
   typedef int (BTPSAPI *PFN_CSCM_RegisterSensor_t)(CSCM_ServiceRegistration_t *ServiceInfo);
#endif

   /* The following function is provided for registration of Cycling    */
   /* Speed and Cadence (CSC) Collector Context.                        */
   /* It should be called before any other calls to Collecotr methods.  */
   /* This Callback will be dispatched by the CSC                      */
   /* Manager when various CSC Manager Events occur.  This function     */
   /* accepts the Callback Function and Callback Parameter              */
   /* (respectively) to call when a CSC Manager Event needs to be       */
   /* dispatched.  This function returns a positive (non-zero) value if */
   /* successful, or a negative return error code if there was an error.*/
   /* * NOTE * If this function returns success (greater than zero) then*/
   /*          this value (ClientID) should be passed as first parameter*/
   /*          for all the CSC Collector methods.                       */
BTPSAPI_DECLARATION int BTPSAPI CSCM_RegisterCollector(CSCM_Event_Callback_t CallbackFunction,
                                          void *CallbackParameter);

#ifdef INCLUDE_BLUETOOTH_API_PROTOTYPES
   typedef int (BTPSAPI *PFN_CSCM_RegisterCollector_t)();
#endif
   /* The following function is provided to allow a mechanism to        */
   /* un-register a previously registered CSC Manager Event Callback    */
   /* (registered via a successful call to the CSCM_RegisterCollector or*/
   /* CSCM_RegisterSensor functions).  This function accepts as input   */
   /* the CSC Manager Event Client ID (return value of the              */
   /* registration function).                                           */
BTPSAPI_DECLARATION int BTPSAPI CSCM_UnRegister(unsigned int ClientID);


   /* A SENSOR method for reporting new measurement.                    */
   /* If periodic measurement notification is not started, this will    */
   /* trigger new notifications to the connected clients.               */
   /* If periodic measurement is on, the CSC manager will accumulate the*/
   /* measurement data and will notify the new cumulative value in its  */
   /* next periodic notification.                                       */
BTPSAPI_DECLARATION int BTPSAPI CSCM_ReportMeasurement(unsigned int ClientID,
                                                       CSCM_ReportMeasurement_t *);

   /* A SENSOR method for starting/Stoping periodic notification of    */
   /* measurement.                                                     */
BTPSAPI_DECLARATION int BTPSAPI CSCM_SetPeriodicMeasurementNotifications(unsigned int ClientID,
                                                       CSCM_StartPeriodicNotification_t *);

   /* A COLLECTOR method for retrieving connected Sensor Features.     */
   /* Note that the value will be retrieved through the CSCM Event     */
   /* Callback (see etCSC_GetSensorFeaturesResponse)                   */
BTPSAPI_DECLARATION int BTPSAPI CSCM_GetSensorFeatures(unsigned int ClientID,
                                                     CSCM_GetValue_t *);

   /* A COLLECTOR method for retrieving connected Sensor Location.     */
   /* Note that the value will be retrieved through the CSCM Event     */
   /* Callback (see etCSC_GetSensorLocationResponse)                   */
BTPSAPI_DECLARATION int BTPSAPI CSCM_GetSensorLocation(unsigned int ClientID,
                                                                           CSCM_GetValue_t *);

   /* A COLLECTOR method for retrieving the  Locations supported by the */
   /* Sensor.                                                           */
BTPSAPI_DECLARATION int BTPSAPI CSCM_GetSupportedSensorLocations(unsigned int ClientID,
                                          CSCM_GetValue_t *);

   /* A COLLECTOR method for updating connected Sensor Location.       */
BTPSAPI_DECLARATION int BTPSAPI CSCM_SetSensorLocation(unsigned int ClientID,
                                          CSCM_SetSensorLocation_t *);

   /* A COLLECTOR method for updating connected Sensor Cumulative wheel*/
    /* Value.                                                           */
BTPSAPI_DECLARATION int BTPSAPI CSCM_SetComulativeValue(unsigned int ClientID,
                                          CSCM_SetCumulativeValue_t *);


   /* A COLLECTOR method for triggering calibration procedure by the   */
    /* connected Sensor.                                                */
BTPSAPI_DECLARATION int BTPSAPI CSCM_TriggerCalibration(unsigned int ClientID,
                                          CSCM_TriggerCalibarion_t *);

   /* A COLLECTOR method for enabling/disabling reception of measurement*/
   /* notification.                                                     */
BTPSAPI_DECLARATION int BTPSAPI CSCM_EnableMeasurementNotifications(unsigned int ClientID,
                                          CSCM_EnableNotifications_t *);
#endif
