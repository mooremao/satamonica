/*****< cscmgri.h >*************************************************************/
/*      Copyright 2012 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  CSCMGR - CSC Manager Internal Definitions for TI Bluetooth                */
/*           Protocol Stack Platform Manager.                                 */
/*                                                                            */
/*  Author:  Ryan Byrne                                                        */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   11/12/12  R. Byrne       Initial creation.                               */
/******************************************************************************/
#ifndef __CSCMGRIH__
#define __CSCMGRIH__

#include "BTAPITyp.h"            /* Bluetooth API Type Definitions.           */
#include "CSCMAPI.h"


struct _ClientContext_t;

typedef struct _ModuleContext_t
{
	int                        ModuleId;
	BTPM_HANDLE 		       PMSRVHandle;
	struct _ClientContext_t   *Clients[2]; // Currently only one sensor and one collector are suppoerted
} ModuleContext_t;

typedef struct _ClientContext_t
{
	BTPM_EventCallback_t 		EventCallback;
	BTPM_HANDLE 				UserHandle;
	int 						CallbackId;
	ModuleContext_t        		*Module;
	CSCM_ConnectionType_t 		Role;
} ClientContext_t;

/**** CSC Collector Prototypes *****/

/* The following is the Collector manager registration method
 * It gets no parameters and allocates sensor context.
 * In addition it will register to all the Collector services.
 * It returns the pointer to allocated context (or NULL) 
 * */
ClientContext_t * CSCCLC_RegisterClient();

/* Collector Manager un-registration method -
 * It should be called when client unregister its events callback.
 * The parameters are:
 *  ClientCtx - Client Context pointer (returned by CSCCLC_RegisterClient)
 */
void CSCCLC_UnRegisterClient(ClientContext_t *ClientCtx);

/* Collector's User request handler -
 */
int CSCCLC_ProcessUserRequest(ClientContext_t *ClientCtx, unsigned int FunctionId,
								void *Request, unsigned int RequestLen,
								void *Response, unsigned int ResponseLen);

/* The following function is responsible for processing asynchronous */
/* Device Manager (DEVM) Events (including Power On/Off events).     */
/* This function should be registered with the Bluetopia Platform    */
/* Manager Module Handler and will be called when an asynchronous    */
/* Device Manager event is dispatched.                               */
void BTPSAPI CSCCLC_DeviceMgrEventHandler(ClientContext_t *ClientCtx, DEVM_Event_Data_t *EventData);


/**** CSC Sensor Prototypes *****/

/* Sensor manager registration method -
 * It gets no parameters and allocates sensor context.
 * In addition it will register to all the sensor services.
 * It returns the pointer to allocated context (or NULL)
 * */
ClientContext_t *CSCSNS_RegisterClient();

/* Sensor Manager un-registration method -
 * It should be called when client unregister its events callback.
 * The parameters are:
 *  ClientCtx - Client Context pointer (returned by CSCCLC_RegisterClient)
 */
void CSCSNS_UnRegisterClient(ClientContext_t *ClientCtx);

/* Sensor's User request handler -
 */
int CSCSNS_ProcessUserRequest(ClientContext_t *ClientCtx, unsigned int FunctionId,
								void *Request, unsigned int RequestLen,
								void *Response, unsigned int ResponseLen);


/**** CSC Manager Prototypes *****/

/* Event Dispatcher Method
 * Gets Client and event info and propagate the message to the registered event handler
 */
void CSCMGR_DispatchCSCMEvent(ClientContext_t *ClientCtx, CSCM_Event_Data_t *CSCMEventData);


#endif
