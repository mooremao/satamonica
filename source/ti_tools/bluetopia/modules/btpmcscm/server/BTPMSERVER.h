/*****< btpmclt.h >*************************************************************/
/*      Copyright 2012 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTPMCLT - (IPC) Server Manager Implementation for TI Bluetooth            */
/*           Protocol Stack Platform Manager.                                 */
/*                                                                            */
/*  Author:  Ryan Byrne                                                       */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   11/12/12  R. Byrne       Initial creation.                               */
/******************************************************************************/
#ifndef __BTPMMGRH__
#define __BTPMMGRH__

#include "BTAPITyp.h"            /* Bluetooth API Type Definitions.           */

#include "SS1BTPM.h"             /* Platform Manager Prototypes/Constants.    */

#include "SS1BTPS.h"             /* Bluetopia Core Prototypes/Constants.      */
#include "BTPMMSGT.h"


/* the following identifies the client (socket). It is set upon event registration
 * and is for a checking valid state.
 */
typedef int	 (*ProcessUserRequestCallback_t)(BTPM_HANDLE hClient, unsigned int FunctionId, void *Request, unsigned int RequestLen, void *Response, unsigned int ResponseLen);
typedef void (*DeInitCallback_t)(BTPM_HANDLE hModule);
typedef BTPM_HANDLE (*RegisterClientCallback_t)(BTPM_HANDLE hModule, unsigned int Role, BTPM_EventCallback_t EventCallback, BTPM_HANDLE EventParameter);
typedef void (*UnRegisterClientCallback_t)(BTPM_HANDLE hClient);

void *PMSRV_Open(unsigned int ModuleId,
		ProcessUserRequestCallback_t  ProcessUserRequestCallback,
		RegisterClientCallback_t      RegisterClientCallback,
		UnRegisterClientCallback_t    UnRegisterClientCallback,
		DeInitCallback_t              DeInitCallback,
		BTPM_HANDLE					  hModule
		);

void PMSRV_Close(void *SRVHandle);

#endif
