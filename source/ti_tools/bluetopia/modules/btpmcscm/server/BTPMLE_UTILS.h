/*****< btpmle_utils.h >*************************************************************/
/*      Copyright 2012 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  CSCMGR - CSC Manager Implementation for Stonestreet One Bluetooth         */
/*           Protocol Stack Platform Manager.                                 */
/*                                                                            */
/*  Author:  Ryan Byrne                                                        */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   11/12/12  R. Byrne       Initial creation.                               */
/******************************************************************************/
#ifndef __BTPMLE_UTILSH__
#define __BTPMLE_UTILSH__

#include "BTAPITyp.h"            /* Bluetooth API Type Definitions.           */
#include "GATTAPI.h"


/*** Type Definitions ***/
typedef void* BTPM_HANDLE;
typedef GATT_Server_Indication_Data_t LE_ASyncEvent_t;

/* The following structure represents the container structure for    */
/* holding all CSC collector response data.                          */
/* This would be the type of parameter for the Response callback     */
typedef struct
{
	union
	{
		GATT_Read_Response_Data_t 	ReadResponseData;
		GATT_Write_Response_Data_t 	WriteResponseData;
	};
	unsigned int 				TxnType;
	Byte_t		 				Status;
} LE_Response_t;



typedef void (*LE_ASyncEventHandler_t)(LE_ASyncEvent_t *NotificationData, BTPM_HANDLE UserHandle);
typedef void (*LE_ResponseHandler_t)(LE_Response_t *ResponseInfo, BTPM_HANDLE UserHandle);
typedef void* (*ServiceDiscoveryCallback_t)(BD_ADDR_t *BD_ADDR, GATT_Service_Discovery_Indication_Data_t *, BTPM_HANDLE UserHandle);



/*** Utility Functions Prototypes ***/
void LE_DiscoverFormalService(BD_ADDR_t *BluetoothAddress, unsigned char *ServiceUUID, ServiceDiscoveryCallback_t Callback, BTPM_HANDLE UserHandle);
Word_t LE_FindCCDHandle(GATT_Characteristic_Information_t *CurrentCharacteristic);

int LE_MapGATTErrorCode(int ErrorCodeToMap);


void LE_BDAddrToStr(BD_ADDR_t Board_Address, char *BoardStr);

int LE_WriteValue(BD_ADDR_t *SensorAddr, unsigned int  TxnType, Word_t Handle, Byte_t *AttrValue, Word_t AttrLength, LE_ResponseHandler_t ResponseCallbcak, BTPM_HANDLE UserParam);
/* The following function will submit a Write Measurement CCD request*/
/* to a remote sensor.  The first parameter is a pointer to a        */
/* dynamically allocated Txn structure that will be used to  */
/* submit the request and provide information in the corresponding   */
/* response.  The transaction data will be passed back in the        */
/* corresponding event, and it is NOT freed internal to this module. */
/* The second parameter is the Attribute Handle of the Heart Rate    */
/* Client Characteristic Descriptor on the remote sensor.  This      */
/* function will return zero on success; otherwise, a negative error */
/* code will be returned.                                            */
int LE_ReadValue(BD_ADDR_t *SensorAddr, unsigned int  TxnType, Word_t Handle, LE_ResponseHandler_t ResponseCallbcak, BTPM_HANDLE UserParam);

/* The following function will submit a Write Measurement CCD request*/
/* to a remote sensor.  The first parameter is a pointer to a        */
/* dynamically allocated Txn structure that will be used to  */
/* submit the request and provide information in the corresponding   */
/* response.  The transaction data will be passed back in the        */
/* corresponding event, and it is NOT freed internal to this module. */
/* The second parameter is the Attribute Handle of the Heart Rate    */
/* Client Characteristic Descriptor on the remote sensor.  This      */
/* function will return zero on success; otherwise, a negative error */
/* code will be returned.                                            */
int LE_WriteCCD(BD_ADDR_t *SensorAddr, unsigned int TxnType, Word_t Handle, Word_t ClientConfigurationValue, LE_ResponseHandler_t ResponseCallbcak, BTPM_HANDLE UserParam);

unsigned int LE_RegisterModule(LE_ASyncEventHandler_t IndicationHandler, LE_ASyncEventHandler_t NotificationHandler, BTPM_HANDLE UserHandle);

void LE_UnRegisterModule(unsigned int CallbackID);

unsigned int LE_GetBluetoothStackID();

#endif
