/*****< cscmmsg.h >************************************************************/
/*      Copyright 2012 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  CSCMMSG - Defined Interprocess Communication Messages for the Cycling     */
/*            Speed and Cadence Profile (CSCP) Manager for TI Bluetooth       */
/*            Stack Platform Manager.                                         */
/*                                                                            */
/*  Author:  Ryan Byrne                                                       */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   11/12/12  R. Byrne       Initial creation.                               */
/******************************************************************************/
#ifndef __CSCMMSGH__
#define __CSCMMSGH__

#include "BTAPITyp.h"            /* Bluetooth API Type Definitions.           */
#include "SS1BTPS.h"             /* BTPS Protocol Stack Prototypes/Constants. */

#include "SS1BTCSCM.h"           /* CSC Framework Prototypes/Constants.       */

#include "BTPMMSGT.h"            /* BTPM Message Type Definitions/Constants.  */

#include "CSCMType.h"            /* BTPM CSC Manager Type Definitions.        */
#include "CSCMAPI.h"

   /* The following Message Group constant represents the Bluetopia     */

   /* Platform Manager Message Group that specifies the Cycling Speed and Cadence      */
   /* Profile (CSC) Manager.                                            */
#define BTPM_MESSAGE_GROUP_CSC_MANAGER                             0x0000110A

   /* The following constants represent the defined Bluetopia Platform  */
   /* Manager Message Functions that are valid for the Cycling Speed and Cadence (CSC) */
   /* Manager.                                                          */

	/* Cycling Speed and Cadence Profile Manager (CSCM) Sensor Commands.         */

	/* Cycling Speed and Cadence Profile Manager (CSCM) Sensor Commands.         */
#define CSCM_MESSAGE_FUNCTION_SET_SENSOR_INFO                         0x00002001
#define CSCM_MESSAGE_FUNCTION_REPORT_MEASUREMENT                      0x00002002
#define CSCM_MESSAGE_FUNCTION_SET_PERIODIC_MEASUREMENT_NOTIFICATION   0x00002003

   /* Cycling Speed and Cadence Profile Manager (CSCM) Collector Commands.         */
#define CSCM_MESSAGE_FUNCTION_GET_SENSOR_FEATURES                     0x00003001
#define CSCM_MESSAGE_FUNCTION_GET_SENSOR_LOCATION                     0x00003002
#define CSCM_MESSAGE_FUNCTION_GET_SUPPORTED_SENSOR_LOCATIONS          0x00003003
#define CSCM_MESSAGE_FUNCTION_SET_SENSOR_LOCATION                     0x00003004
#define CSCM_MESSAGE_FUNCTION_SET_CUMMULATIVE_VALUE				      0x00003005
#define CSCM_MESSAGE_FUNCTION_TRIGGER_CALIBATION                      0x00003006
#define CSCM_MESSAGE_FUNCTION_ENABLE_MEASUREMENT_NOTIFICATION         0x00002007


#endif
