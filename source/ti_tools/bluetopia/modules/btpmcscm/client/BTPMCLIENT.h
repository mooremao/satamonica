/*****< btpmclt.h >*************************************************************/
/*      Copyright 2012 - 2014 Stonestreet One.                                */
/*      All Rights Reserved.                                                  */
/*                                                                            */
/*  BTPMCLT - (IPC) Client Manager Implementation for TI Bluetooth            */
/*           Protocol Stack Platform Manager.                                 */
/*                                                                            */
/*  Author:  Ryan Byrne                                                       */
/*                                                                            */
/*** MODIFICATION HISTORY *****************************************************/
/*                                                                            */
/*   mm/dd/yy  F. Lastname    Description of Modification                     */
/*   --------  -----------    ------------------------------------------------*/
/*   11/12/12  R. Byrne       Initial creation.                               */
/******************************************************************************/
#ifndef __BTPMMGRH__
#define __BTPMMGRH__

#include "BTAPITyp.h"            /* Bluetooth API Type Definitions.           */

#include "SS1BTPM.h"             /* Platform Manager Prototypes/Constants.    */

#include "SS1BTPS.h"             /* Bluetopia Core Prototypes/Constants.      */
#include "BTPMMSGT.h"

typedef void* BTPM_HANDLE;

typedef void (BTPSAPI *BTPM_EventCallback_t)(BTPM_HANDLE UserHandle, BTPM_UserEventData_t *EventData);

/* The following is the PM module initialization method
 * It should be called once upon Client init with the
 * The parameters are:
 *  ModuleId - ModuleId (a.k.a. Message GroupId)
 *  ModuleMutex - Pre-allocated Module Mutex
 * It will return an Handle for the module in case of success or NULL otherwise.
 */
BTPM_HANDLE PMCLT_Init(unsigned int ModuleId, Mutex_t ModuleMutex);

/* The following is the PM module Termination method.
 * It should be called once when closing the client app.
 * The parameters are:
 *  hModule - Module Handle (returned by PMCLT_Init)
 */
void PMCLT_DeInit(BTPM_HANDLE hModule);


/* The following is the PM client registration method
 * It should be called when client register its events callback.
 * The parameters are:
 *  hModule - Module Handle (returned by PMCLT_Init)
 *  Role - Role of te client (e.g. Sensor or Collector) as defined by specific module.
 *  EventCallback - Event Callback function
 *  EventParameter - User Handle to be received within the Event Callback
 * It will return an Handle for the client in case of success or NULL otherwise.
 * NOTE: currently, only one Client is allowed per module per PM_Client
 */
BTPM_HANDLE PMCLT_RegisterClient(BTPM_HANDLE hModule, unsigned int Role, BTPM_EventCallback_t EventCallback, BTPM_HANDLE UserHandle);

/* The following is the PM client un-registration method
 * It should be called when client unregister its events callback.
 * The parameters are:
 *  hClinet - Client Handle (returned by PMCLT_RegisterClient)
 */
void PMCLT_UnRegisterClient(BTPM_HANDLE hClinet);

/* The following is the PM client message processing method
 * It should be called when client unregister its events callback.
 * The parameters are:
 *  hClinet - Client Handle (returned by PMCLT_RegisterClient)
 *  FunctionId - Message Function (as define in module's MSG API)
 *  Request - Pointer to the request message payload (as defined in module's API)
 *  RequestLen - Length of the request payload
 *  Response - Pointer to the pre-allocated response buffer (as defined in module's API)
 *             in case of success, the response buffer will be filled.
 *  ResponseLen - Length of the response buffer
 * It will return an response status upon success or PM ERROR CODE otherwise.
 */
int PMCLT_ProcessUserRequest(BTPM_HANDLE hClient, unsigned int FunctionId,
		void *Request, unsigned int RequestLen,
		void *Response, unsigned int ResponseLen);

#endif
