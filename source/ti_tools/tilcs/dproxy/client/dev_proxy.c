/*
 * dev_proxy.c
 *
 * Client API procedures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "release.h"
#include "device.h"
#include "dev_proxy.h"
#include "gnss.h"
#include "rpc.h"
#include "os_services.h"
#include "logger.h"
#include "utils.h"
#include "nvs.h"

#define FAIL     -1
#define SUCCESS   0

#define CLIENT_RD_FIFO_PATH "/tmp/dproxy.client"
#define CLIENT_WR_FIFO_PATH "/tmp/dproxy.server"

struct thread_sync {
	void *mutex;
	int  flag;
} cli_thread_sync[1];


void *cli_term_sig;



/* To hold callbacks from app */
static struct ti_dev_callbacks dev_cbs[1] = {

	{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
};

static int dev_send_req(unsigned short elem_id_i, unsigned char oper_id_i,
						unsigned int length_i, void *data_i,
						unsigned int length_o, void *data_o, 
						unsigned int *length_r)
{
	struct ie_desc *ie_adu_i, *ie_adu_o;
	struct rpc_msg *rmsg_o = NULL;
	struct rpc_msg *rmsg_i = rpc_msg_alloc();
    int ret_val = FAIL;
	if(!rmsg_i)
		goto dreq_exit1;
	if (length_r)
		*length_r = 0;

	ie_adu_i = rpc_ie_attach(rmsg_i, elem_id_i, oper_id_i, length_i,data_i);
	if(!ie_adu_i)
		goto dreq_exit2;

	if(!(rmsg_o = rpc_msg_alloc()))
		goto dreq_exit3;

	ie_adu_o = rpc_ie_attach(rmsg_o, REI_NOTDEF, OPER_NOP, length_o,data_o);
	if(!ie_adu_o)
		goto dreq_exit4;

	if(!rpc_cli_read(rmsg_i, rmsg_o)) {

		ret_val = SUCCESS;
		if (length_r)
			*length_r = rmsg_o->info_elem->head.tot_len;
	}

	rpc_ie_detach(rmsg_o, ie_adu_o);

dreq_exit4:
	rpc_msg_free(rmsg_o);

dreq_exit3:
	rpc_ie_detach(rmsg_i, ie_adu_i);

dreq_exit2:
	rpc_msg_free(rmsg_i);

dreq_exit1:
	return ret_val;
}

static int dev_send_cmd(unsigned short elem_id, unsigned char oper_id,
						unsigned int length, void *data)
{
	struct ie_desc *ie_adu;
	struct rpc_msg *rmsg = rpc_msg_alloc();
	int ret_val = FAIL;
	if(!rmsg)
		goto dcmd_exit1;

	ie_adu = rpc_ie_attach(rmsg, elem_id, oper_id, length, data);
	if(!ie_adu)
		goto dcmd_exit2;

	if(!rpc_cli_send(rmsg))
		ret_val = SUCCESS;

	rpc_ie_detach(rmsg, ie_adu);

dcmd_exit2:
	rpc_msg_free(rmsg);

dcmd_exit1:
	return ret_val;
}

int ti_dev_connect(struct dev_start_up_info *start_info)
{
	int ret_val = -1;

	ret_val = dev_send_cmd(REI_DEV_CONNECT, OPER_NOP,
				sizeof(struct dev_start_up_info), start_info);

	return ret_val;
}

void ti_dev_release(void)
{
	
	os_mutex_take(cli_thread_sync->mutex);
	cli_thread_sync->flag = 1;
	os_mutex_give(cli_thread_sync->mutex);


	dev_send_cmd(REI_DEV_RELEASE, OPER_NOP, 0, NULL);


	
}

int ti_dev_config_reports(unsigned int async_events, unsigned int dev_agnss,
			unsigned int nmea_flags, unsigned int msr_reports,
			unsigned int pos_reports, unsigned short interval)
{
	struct dev_rpt_sel reports[] = {
		{ async_events, dev_agnss, nmea_flags, msr_reports,
			pos_reports, interval }
	};

	return dev_send_cmd(REI_DEV_CFG_RPT, OPER_SET, sizeof(reports),
						reports);
}

int ti_dev_set_app_profile(unsigned int use_cases, unsigned int gnss_select)
{
	struct dev_app_desc app_desc[] = {{use_cases, gnss_select}};

	return dev_send_cmd(REI_DEV_NAV_SEL, OPER_SET, sizeof(app_desc),
						app_desc);
}

int ti_dev_version(struct dev_version *version)
{
	return dev_send_req(REI_DEV_VERSION, OPER_GET, 0,  NULL,
			    sizeof(struct dev_version), version, NULL);
}

struct action_desc {

	enum dev_action  act_id;
	unsigned short   rpc_id;
	unsigned short   obj_sz;
};

struct action_desc act_tbl [] = {

	{dev_active, REI_DEV_ACTIVE, 0},
	{dev_de_act, REI_DEV_DE_ACT, 0},
	{dev_dsleep, REI_DEV_DSLEEP, 0},
	{dev_wakeup, REI_DEV_WAKEUP, 0}
};

int ti_dev_exec_action(enum dev_action dev_act)
{
	struct action_desc *action = act_tbl + dev_act;
	int ret_val;

	ret_val = dev_send_cmd(action->rpc_id, OPER_SET,
						   action->obj_sz, NULL);

	return ret_val;
}

int ti_dev_is_alive(void)
{
	int status = 0;   /* Move this to rpc.h */

	if(dev_send_req(REI_DEV_WSTATUS, OPER_NOP, 0, NULL, sizeof(status), 
			&status, NULL))
		return FAIL;

	return status ? 1 : 0;
}

int ti_dev_plt(const struct dev_plt_param* plt_test)
{

	int ret_val = -1;
	struct dev_version version;

	ret_val = dev_send_cmd(REI_DEV_PLT, OPER_NOP, sizeof(struct dev_plt_param ), plt_test);

	return ret_val;

}
struct assist_desc
{
	enum loc_assist assist;
	unsigned short  rpc_id;
	unsigned int    obj_sz;
};

/* WARN: Do NOT change this order. Should match with dev_proxy.h enum. */
struct assist_desc assist_tbl[ ] = {

	{a_REF_POS, REI_REF_POS, sizeof(struct location_desc)},

	{a_GPS_TIM, REI_GPS_TIM, sizeof(struct gps_ref_time)},
	{a_GPS_DGP, REI_GPS_DGP, sizeof(struct dgps_sat)},
	{a_GPS_EPH, REI_GPS_EPH, sizeof(struct gps_eph)},
	{a_GPS_ION, REI_GPS_ION, sizeof(struct gps_ion)},
	{a_GPS_UTC, REI_GPS_UTC, sizeof(struct gps_utc)},
	{a_GPS_ALM, REI_GPS_ALM, sizeof(struct gps_alm)},
	{a_GPS_ACQ, REI_GPS_ACQ, sizeof(struct gps_acq)},
	{a_GPS_RTI, REI_GPS_RTI, sizeof(struct gps_rti)},

	{a_GLO_EPH, REI_GLO_EPH, sizeof(struct glo_eph)},
	{a_GLO_ALM, REI_GLO_ALM, sizeof(struct glo_alm)},
	{a_GLO_UTC, REI_GLO_UTC, sizeof(struct glo_utc_model)},
	{a_GLO_ION, REI_GLO_ION, sizeof(struct ganss_ion_model)},
	{a_GLO_TIM, REI_GLO_TIM, sizeof(struct ganss_ref_time)},

	{a_GLO_ACQ_G1, REI_GLO_ACQ, sizeof(struct ganss_ref_msr)},
	{a_GLO_ACQ_G2, REI_GLO_ACQ, sizeof(struct ganss_ref_msr)},

	{a_GLO_RTI,    REI_GLO_RTI, sizeof(struct ganss_rti)},

	{a_GLO_DGC_G1, REI_GLO_DGP, sizeof(struct dganss_sat)},
	{a_GLO_DGC_G2, REI_GLO_DGP, sizeof(struct dganss_sat)},

};

int ti_dev_loc_aiding_add(enum loc_assist assist, void *assist_array, int num)
{
	struct assist_desc *a_desc = assist_tbl + assist;

	LOGPRINT(api, info, "ti_dev_loc_aiding_add: enum %d and n_objs [%d]", assist, num);
	if(a_desc->assist != assist) {  /* Convert this to assert */
		LOGPRINT(api, err, "ti_dev_loc_aiding_add: invalid enum %d", assist);
		return FAIL;
	}

	return dev_send_cmd(a_desc->rpc_id, OPER_ADD,
						a_desc->obj_sz*num,
						assist_array);
}

int ti_dev_loc_aiding_del(enum loc_assist assist, unsigned int sv_id_map,
						  unsigned int mem_flags)
{
	struct dev_aid_del aid_del[] = {{sv_id_map, mem_flags}};
	struct assist_desc *a_desc   = assist_tbl + assist;

	LOGPRINT(api, info, "ti_dev_loc_aiding_del: enum %d, sv_map [0x%x] and mem_flags [0x%x]", assist, sv_id_map, mem_flags);
	if(a_desc->assist != assist)   /* Convert this to assert */ {
		LOGPRINT(api, err, "ti_dev_loc_aiding_del: invalid enum %d", assist);
		return FAIL;
	}

	return dev_send_cmd(a_desc->rpc_id, OPER_DEL,
					sizeof(struct dev_aid_del), aid_del);
}


int ti_dev_loc_aiding_get(enum loc_assist assist, void *assist_array, int num)
{
	struct assist_desc *a_desc   = assist_tbl + assist;
	unsigned int rec_sz;

	LOGPRINT(api, info, "ti_dev_loc_aiding_get: enum %d and n_objs [%d]", assist, num);
	if(a_desc->assist != assist)   /* Convert this to assert */ {
		LOGPRINT(api, err, "ti_dev_loc_aiding_get: invalid enum %d", assist);
		return FAIL;
	}

	if (SUCCESS == dev_send_req(a_desc->rpc_id, OPER_GET,
						a_desc->obj_sz * num, assist_array,
				    a_desc->obj_sz * num, assist_array, 
				    &rec_sz)) {
		LOGPRINT(api, info, "ti_dev_loc_aiding_get: n_objs [%d]", 
			 rec_sz / a_desc->obj_sz);
		return (rec_sz / a_desc->obj_sz);
	}

	LOGPRINT(api, err, "ti_dev_loc_aiding_get: failed");

	return FAIL;
}

struct assist_desc assist_stat_tbl[ ] = {

	{a_REF_POS, REI_REF_POS_AID_STAT, sizeof(struct loc_gnss_info)},

	{a_GPS_TIM, REI_GPS_TIM_AID_STAT, sizeof(struct gps_ref_time)},
	{a_GPS_DGP, REI_GPS_DGP_AID_STAT, sizeof(struct dgps_sat)},
	{a_GPS_EPH, REI_GPS_EPH_AID_STAT, sizeof(struct gps_eph)},
	{a_GPS_ION, REI_GPS_ION_AID_STAT, sizeof(struct gps_ion)},
	{a_GPS_UTC, REI_GPS_UTC_AID_STAT, sizeof(struct gps_utc)},
	{a_GPS_ALM, REI_GPS_ALM_AID_STAT, sizeof(struct gps_alm)},
	{a_GPS_ACQ, REI_GPS_ACQ_AID_STAT, sizeof(struct gps_acq)},
	{a_GPS_RTI, REI_GPS_RTI_AID_STAT, sizeof(struct gps_rti)},

	{a_GLO_EPH,    REI_GLO_EPH_AID_STAT,    sizeof(struct glo_eph)},
	{a_GLO_ALM,    REI_GLO_ALM_AID_STAT,    sizeof(struct glo_alm)},
	{a_GLO_UTC,    REI_GLO_UTC_AID_STAT,    sizeof(struct glo_utc_model)},
	{a_GLO_ION,    REI_GLO_ION_AID_STAT,    sizeof(struct ganss_ion_model)},
	{a_GLO_TIM,    REI_GLO_TIM_AID_STAT,    sizeof(struct ganss_ref_time)},
	{a_GLO_ACQ_G1, REI_GLO_ACQ_G1_AID_STAT, sizeof(struct ganss_ref_msr)},
	{a_GLO_ACQ_G2, REI_GLO_ACQ_G2_AID_STAT, sizeof(struct ganss_ref_msr)},
	{a_GLO_RTI,    REI_GLO_RTI_AID_STAT,    sizeof(struct ganss_rti)},
	{a_GLO_DGC_G1, REI_GLO_DGC_G1_AID_STAT, sizeof(struct dganss_sat)},
	{a_GLO_DGC_G2, REI_GLO_DGC_G2_AID_STAT, sizeof(struct dganss_sat)},

};

int ti_dev_loc_aiding_status(enum loc_assist assist,
							 struct aiding_status *array, int num)
{
	struct assist_desc *a_desc = assist_stat_tbl + assist;
	unsigned int rec_sz;

	LOGPRINT(api, info, "ti_dev_loc_aiding_status: enum %d and n_objs [%d]", assist, num);

	if(a_desc->assist != assist)
		return FAIL;

	if (SUCCESS == dev_send_req(a_desc->rpc_id, OPER_GET, 
						num * ST_SIZE(aiding_status), array, 
				    num * ST_SIZE(aiding_status), array, 
				    &rec_sz)) {
		LOGPRINT(api, info, "ti_dev_loc_aiding_status: enum [%d], n_objs [%d]", 
			 assist, rec_sz / sizeof(struct aiding_status));
		return (rec_sz / sizeof(struct aiding_status));
	}

	LOGPRINT(api, err, "ti_dev_loc_aiding_status: failed");

	return FAIL;
}


struct oper_param_desc
{
	enum oper_param o_parm;
	unsigned short  rpc_id;
	unsigned int    obj_sz;
};

struct oper_param_desc oper_param_tbl [] = {

	{ts_pulse_info, REI_TS_PULSE_INFO, sizeof(struct dev_ts_pulse_info)},
	{dev_ref_clock, REI_DEV_REF_CLOCK, sizeof(struct dev_ref_clk_param)},
	{clk_calibrate, REI_CLK_CALIBRATE, sizeof(struct dev_clk_calibrate)},
	{area_geofence, REI_AREA_GEOFENCE, sizeof(struct geofencing_params)},
	{buf_loc_fixes, REI_BUF_LOC_FIXES, sizeof(struct buffered_location)},
	{qop_specifier, REI_QOP_SPECIFIER, sizeof(struct dev_qop_specifier)},
	{maximize_pdop, REI_MAXIMIZE_PDOP, 0},
	{apm_ctrl, REI_APM_CTRL, sizeof(struct dev_apm_ctrl)},
	{pa_blank,      REI_PA_BLANK, sizeof(struct dev_pa_blank)},
	/*add for GNSS recovery ++*/
	{err_recovery, REI_ERR_RECOVERY, 0},
	/* error trigger test code, to be removed */
	{fatal_err, REI_FATAL_ERR, 0},
	{exception_err, REI_EXCEPTION_ERR, 0},
	/*add for GNSS recovery --*/
	{gnss_meas_rep_recovery, REI_MEAS_REP_RECOVERY, 0},
	{gnss_no_resp_recovery, REI_NO_RESP_RECOVERY, 0},
	{gnss_hard_reset_recovery, REI_HARD_RESET_RECOVERY, 0}
};

int ti_dev_setup_oper_param(const enum oper_param id, void *value)
{
	struct oper_param_desc *oper = oper_param_tbl + id;

	return dev_send_cmd(oper->rpc_id, OPER_SET, oper->obj_sz, value);
}

/*============================================================================*/
/* Receive support                                                            */
/*============================================================================*/

#define FREE_AND_RET_IF_NO_APP(app_func, adu)   \
	if(!dev_cbs->app_func || !adu->data) {                \
		if(adu->data)                   \
		free(adu->data);                \
		return;                         \
	}

struct cli_rei;

typedef void (*cli_rei_rx_fn)(struct cli_rei*, struct ie_desc*);

struct cli_rei {

	unsigned short  elem_id;
	unsigned short  obj_len;
	cli_rei_rx_fn   rx_func;
	void           *cb_data;
};

static void location_rpt_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
#define REI_2_POS_RPT(cb_data) ((enum ue_pos_id) cb_data)

	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(location_rpt, ie_adu);


	dev_cbs->location_rpt(REI_2_POS_RPT(cli_re->cb_data), ie_adu->data,
					(ie_hdr->tot_len/cli_re->obj_len));

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

static void measure_info_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
#define REI_2_MSR_RPT(cb_data) ((enum ue_msr_id) cb_data)

	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(measure_info, ie_adu);

	dev_cbs->measure_info(REI_2_MSR_RPT(cli_re->cb_data), ie_adu->data,
					(ie_hdr->tot_len/cli_re->obj_len));
	UNUSED(cli_re);

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

static void location_evt_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(location_evt, ie_adu);

	dev_cbs->location_evt((enum lc_evt_id)(*(unsigned int *)ie_adu->data));

	UNUSED(cli_re);

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

static void nmea_message_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
#define REI_2_NMEA_ID(cb_data) ((enum nmea_sn_id)cb_data)

	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(nmea_message, ie_adu);

	dev_cbs->nmea_message(REI_2_NMEA_ID(cli_re->cb_data), 
					(char*)ie_adu->data, ie_hdr->tot_len);

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

static void device_agnss_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
#define REI_2_ASSIST(cb_data) ((enum loc_assist)cb_data)

	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(device_agnss, ie_adu);

	dev_cbs->device_agnss(REI_2_ASSIST(cli_re->cb_data), ie_adu->data,
					(ie_hdr->tot_len/cli_re->obj_len));

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

static void device_error_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
	// struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(device_error, ie_adu);

	dev_cbs->device_error((enum dev_err_id)(*(unsigned int*)ie_adu->data));

	UNUSED(cli_re);

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

static void device_event_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
#define REI_2_EVENT(cb_data) ((enum dev_evt_id)cb_data)

	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(device_event, ie_adu);

	dev_cbs->device_event(REI_2_EVENT(cli_re->cb_data), ie_adu->data, 
					(ie_hdr->tot_len/cli_re->obj_len));

	UNUSED(cli_re);

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

static void cw_test_rpt_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
#define REI_2_CWID(cb_data) ((enum dev_cw_test)cb_data)

	LOGPRINT(api, info, "REI_DEV_PLT_RES: cw_test_rpt_ind");
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(cw_test_rpt, ie_adu);

	dev_cbs->cw_test_rpt(REI_2_CWID(cli_re->cb_data), ie_adu->data,
					(ie_hdr->tot_len/cli_re->obj_len));
	UNUSED(cli_re);

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}
static void ucase_notify_ind(struct cli_rei *cli_re, struct ie_desc *ie_adu)
{
#define REI_2_UCASE_NOTIFY(cb_data) ((enum app_rpt_id) cb_data)

	//	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	FREE_AND_RET_IF_NO_APP(ucase_notify, ie_adu);

	dev_cbs->ucase_notify(REI_2_UCASE_NOTIFY(cli_re->cb_data),ie_adu->data);

	if(ie_adu->data) {
		free(ie_adu->data);
		ie_adu->data = NULL;
	}

	return;
}

#define REI_CB_DATA(data) ((void*)(data))

#define CLI_REI(rei, structure, rx_func, data)                \
{rei, ST_SIZE(structure), rx_func, REI_CB_DATA(data)}

struct no_object {
	/* To facilitate compilation; finally, this has to be removed. */
};

#define NO_OBJECT no_object

static struct cli_rei cli_re_rcv[] = {

	/* 3GPP location information from device        */
	CLI_REI(REI_LOC_GPS, loc_gnss_info,  location_rpt_ind, loc_gnss_ie),
	CLI_REI(REI_LOC_GAN, loc_gnss_info,  location_rpt_ind, loc_gnss_ie),
	CLI_REI(REI_VEL_GNS, vel_gnss_info,  location_rpt_ind, vel_gnss_ie),
	CLI_REI(REI_GPS_TIM, gps_time,       location_rpt_ind, tim_gps_ie),
	CLI_REI(REI_GLO_TIM, ganss_ref_time, location_rpt_ind, tim_glo_ie),

	/* Native location information from device      */
	CLI_REI(REI_DEV_EXL_POS, dev_pos_info, location_rpt_ind, loc_exl_native),
	CLI_REI(REI_DEV_SAT_INF, dev_sat_info, location_rpt_ind, loc_sat_native),
	CLI_REI(REI_DEV_DOP_INF, dev_dop_info, location_rpt_ind, loc_dop_native),
	CLI_REI(REI_DEV_VEL_INF, dev_vel_info, location_rpt_ind, vel_native),
	CLI_REI(REI_DEV_GPS_TIM, dev_gps_time, location_rpt_ind, tim_gps_native),
	CLI_REI(REI_DEV_GLO_TIM, dev_glo_time, location_rpt_ind, tim_glo_native),
	CLI_REI(REI_DEV_HDG_INF, dev_hdg_info , location_rpt_ind, hdg_native),

	/* Native measurement information from device      */
	CLI_REI(REI_DEV_MSR_GPS, dev_msr_info, measure_info_ind, msr_gps_native),
	CLI_REI(REI_DEV_MSR_GLO, dev_msr_info, measure_info_ind, msr_glo_native),

	/* 3GPP measurement information from device     */
	CLI_REI(REI_MSR_GPS,     gps_msr_info, measure_info_ind, msr_gps_rpt_ie),
	CLI_REI(REI_MSR_GLO_SAT, glo_sig_info, measure_info_ind, msr_glo_signal),
	CLI_REI(REI_MSR_GLO_SG1, glo_msr_info, measure_info_ind, msr_glo_sig_G1),
	CLI_REI(REI_MSR_GLO_SG2, glo_msr_info, measure_info_ind, msr_glo_sig_G2),
	CLI_REI(REI_MSR_RSLT, 	 dev_msr_result, measure_info_ind, msr_device_result),
	/*Glonass Measurement reports from the device */
	CLI_REI(REI_MSR_GANSS,	 ganss_msr_info, measure_info_ind, msr_ganss_rpt_ie),
	

	/* GPS NMEA messages from device                */
	CLI_REI(REI_NMEA_GPGGA, no_object, nmea_message_ind, gpgga),
	CLI_REI(REI_NMEA_GPGLL, no_object, nmea_message_ind, gpgll),
	CLI_REI(REI_NMEA_GPGSA, no_object, nmea_message_ind, gpgsa),
	CLI_REI(REI_NMEA_GPGSV, no_object, nmea_message_ind, gpgsv),
	CLI_REI(REI_NMEA_GPRMC, no_object, nmea_message_ind, gprmc),
	CLI_REI(REI_NMEA_GPVTG, no_object, nmea_message_ind, gpvtg),
	/*SRK: Added to enable the NMEA for DR*/	
	CLI_REI(REI_NMEA_GPGRS, no_object, nmea_message_ind, gpgrs),
	CLI_REI(REI_NMEA_GPGST, no_object, nmea_message_ind, gpgst),

	/* GLONASS NMEA messages from device            */
	CLI_REI(REI_NMEA_GLGGA, no_object, nmea_message_ind, glgga),
	CLI_REI(REI_NMEA_GLGLL, no_object, nmea_message_ind, glgll),
	CLI_REI(REI_NMEA_GLGSA, no_object, nmea_message_ind, glgsa),
	CLI_REI(REI_NMEA_GLGSV, no_object, nmea_message_ind, glgsv),
	CLI_REI(REI_NMEA_GLRMC, no_object, nmea_message_ind, glrmc),
	CLI_REI(REI_NMEA_GLVTG, no_object, nmea_message_ind, glvtg),

	CLI_REI(REI_NMEA_QZGSV, no_object, nmea_message_ind, qzgsv),

	CLI_REI(REI_NMEA_WSGSV, no_object, nmea_message_ind, wsgsv),
	/*SRK: Added to enable the NMEA for DR*/	
	CLI_REI(REI_NMEA_GNGRS, no_object, nmea_message_ind, gngrs),
	CLI_REI(REI_NMEA_GNGLL, no_object, nmea_message_ind, gngll),
	CLI_REI(REI_NMEA_GNGNS, no_object, nmea_message_ind, gngns),
	CLI_REI(REI_NMEA_GNGSA, no_object, nmea_message_ind, gngsa),
	CLI_REI(REI_NMEA_GNVTG, no_object, nmea_message_ind, gnvtg),

	/* 3GPP GPS assistance from device, not all applicable, review list */
	CLI_REI(REI_REF_POS,     loc_gnss_info,   device_agnss_ind, a_REF_POS),

	/* 3GPP GPS assistance from device */
	CLI_REI(REI_GPS_EPH,     gps_eph,         device_agnss_ind, a_GPS_EPH),
	CLI_REI(REI_GPS_ALM,     gps_alm,         device_agnss_ind, a_GPS_ALM),
	CLI_REI(REI_GPS_ION,     gps_ion,         device_agnss_ind, a_GPS_ION),
	CLI_REI(REI_GPS_UTC,     gps_utc,         device_agnss_ind, a_GPS_UTC),
	CLI_REI(REI_GPS_REF_TIM, gps_ref_time,    device_agnss_ind, a_GPS_TIM),

	/* 3GPP GLONASS assistance from device */
	CLI_REI(REI_GLO_EPH,     glo_eph,         device_agnss_ind, a_GLO_EPH),
	CLI_REI(REI_GLO_ALM,     glo_alm,         device_agnss_ind, a_GLO_ALM),
	CLI_REI(REI_GLO_ION,     ganss_addl_iono, device_agnss_ind, a_GLO_ION),
	CLI_REI(REI_GLO_UTC,     glo_utc_model,   device_agnss_ind, a_GLO_UTC),
	CLI_REI(REI_GLO_REF_TIM, ganss_ref_time,  device_agnss_ind, a_GLO_TIM),

	CLI_REI(REI_LOC_EVT,     no_object, location_evt_ind, 0),
	CLI_REI(REI_DEV_ERR_INF, no_object, device_error_ind, 0),

	CLI_REI(REI_DEV_VERSION, dev_version, device_event_ind, evt_dev_ver),
	CLI_REI(REI_DEV_EVT_INF, dev_version, device_event_ind, evt_dev_ver),
	CLI_REI(REI_DEV_PLT_RES, dev_plt_param, cw_test_rpt_ind, ecw_test),

	CLI_REI(REI_NOTDEF, no_object, NULL, 0)
};

static struct cli_rei *cli_rei_find(unsigned short elem_id)
{
	struct cli_rei *cli_re = cli_re_rcv + 0;

	while(cli_re->elem_id != REI_NOTDEF) {
		if(cli_re->elem_id == elem_id)
			return cli_re;

		cli_re++;
	}

	return NULL;
}

void wq_xact_signal(struct ie_head *ie_hdr);

static void dev_msg_rx(struct rpc_msg *rmsg)
{
	struct rm_head *rm_hdr = RM_HEADER(rmsg);
	int n_ie = 0, num_ie   = rm_hdr->num_ie;

	for(n_ie = 0; n_ie < num_ie; n_ie++) {
		struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
		struct ie_head *ie_hdr = IE_HEADER(ie_adu);

		struct cli_rei *cli_re = cli_rei_find(ie_hdr->elem_id);
		if(!cli_re || !cli_re->rx_func) {
			wq_xact_signal(ie_hdr);

			rpc_ie_detach(rmsg, ie_adu);         /* Taken, so detach it  */

			continue;                    /* No takers, free data */
		}

		cli_re->rx_func(cli_re, ie_adu);     /* Prepare for apps     */

		rpc_ie_detach(rmsg, ie_adu);         /* Taken, so detach it  */
	}

	rpc_msg_free(rmsg);
}

static void *ti_fifo_thread_handle;

static void * cli_recv_thrd(void *arg)
{
	struct thread_sync *th_sync = (struct thread_sync *) arg;
	int flag;

	LOGPRINT(api, info, "cli_recv_thrd starting");

	os_mutex_take(th_sync->mutex);
	flag = th_sync->flag;
	os_mutex_give(th_sync->mutex);

	while (!flag) {

		struct rpc_msg *rmsg = rpc_cli_recv();
		if(!rmsg)
			continue;

		dev_msg_rx(rmsg);

		os_mutex_take(th_sync->mutex);
		flag = th_sync->flag;
		os_mutex_give(th_sync->mutex);
	}

	os_mutex_take(th_sync->mutex);
	th_sync->flag = 0;
	os_mutex_give(th_sync->mutex);

	LOGPRINT(api, info, "cli_recv_thrd exiting");

	return NULL;
}

int ti_dev_proxy_connect(struct ti_dev_callbacks *callbacks,
					struct ti_dev_proxy_params *params)
{
	char cli_thrname[25] = "D-PROXY_CONNECT";
	*dev_cbs = *callbacks;

	if (rpc_cli_init(CLIENT_RD_FIFO_PATH, CLIENT_WR_FIFO_PATH) < 0) {
		return -1;
	}

	cli_thread_sync->mutex = os_mutex_init();
	cli_thread_sync->flag = 0;

	if ((ti_fifo_thread_handle = os_create_thread(cli_recv_thrd, 
												  cli_thread_sync, cli_thrname)) == NULL) {
		return -1;
	}

	UNUSED(params);

	return 0;
}

void ti_dev_proxy_release(void)
{
	os_destroy_thread(ti_fifo_thread_handle);

	rpc_cli_exit();
}

static struct dev_proxy_version proxy_version = {
	MAJOR, 
	MINOR, 
	PATCH, 
	BUILD, 
	EXTEN,
	0, 	
	QFIER, 
	DAY, 
	MONTH, 
	YEAR
};

int ti_dev_proxy_version(struct dev_proxy_version *version)
{
	*version = proxy_version;

	return 0;
}

int ti_get_dev_caps(struct dev_caps *array, int num)
{
	int count;
	unsigned int rec_sz;
	int ret_val = dev_send_req(REI_DEV_CAPS,               OPER_GET, 
				   num * sizeof(struct dev_caps), array, 
				   num * sizeof(struct dev_caps), array, 
				   &rec_sz);
	struct dev_caps *caps = array;

	return (rec_sz / sizeof(struct dev_caps));
	}

