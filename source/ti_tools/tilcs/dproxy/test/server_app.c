/*
 * server_app.c
 *
 * RPC Server test application
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdio.h>
#include <sys/uio.h>
#include <string.h>

#include "device.h"
#include "dev_proxy.h"
#include "rpc.h"

#define SERVER_READ_FIFO         "/home1/anshuman/NewArchitecture/dev-proxy/working/server_fifo"
#define SERVER_WRITE_FIFO        "/home1/anshuman/NewArchitecture/dev-proxy/working/client_fifo_1"

static int send_ver_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
	struct dev_version ver[] = {
		{
			'1',    /* chip_id_major;*/
			'2',    /* chip_id_minor */
			'3',    /* rom_id_major */
			'4',    /* rom_id_minor */
			'5',    /* rom_id_sub_minor1 */
			'6',    /* rom_id_sub_minor2 */
			'7',    /* rom_day */
			'8',    /* rom_month */
			'9',    /* rom_year */
			'a',    /* patch_major */
			'b',    /* patch_minor */
			'c',    /* patch_day */
			'd',    /* patch_month */
			14     /* patch_year */
		}
	};
	struct ie_desc *adu;
	struct rpc_msg *rmsg = rpc_msg_alloc();

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, elem_id, OPER_NOP, 
				sizeof(struct dev_proxy_version), ver)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return 0;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return -1;        
}

static int send_dev_stat_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
	int status = 63;
	struct ie_desc *adu;
	struct rpc_msg *rmsg = rpc_msg_alloc();

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, elem_id, OPER_NOP, sizeof(int), &status)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return 0;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return -1;        
}

static int send_gps_eph_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_alm_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_utc_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_ion_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_tim_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_pos_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_acq_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_rti_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_gps_dgp_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_glo_eph_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_glo_alm_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

static int send_glo_ion_resp(unsigned short elem_id, 
		unsigned short length, void *data)
{
}

int display_connect(unsigned short elem_id, 
		unsigned short length, void *data)
{
	struct dev_start_up_info *start_info = (struct dev_start_up_info *) data;

	printf("Received parameters:\n");
	printf("aid_config  [%d]\n", start_info->nvs_aid_cfg.aid_config);
	printf("epoch_secs  [%d]\n", start_info->nvs_aid_cfg.epoch_secs);
	printf("sevice_pack [%s]\n", start_info->sevice_pack);
	printf("config_file [%s]\n", start_info->config_file);

	return 0;
}

int display_cfg_rpt(unsigned short elem_id, unsigned short length, void *data)
{
	struct dev_rpt_sel *report = data;

	printf("Received parameters:\n");
	printf("async_events	: [0x%x]\n", report->async_event);
	printf("dev_agnss		: [0x%x]\n", report->bcast_agnss);
	printf("nmea_flags		: [%d]\n", report->nmea_select);
	printf("msr_reports		: [0x%x]\n", report->msr_reports);
	printf("pos_reports		: [0x%x]\n", report->pos_reports);
	printf("interval		: [%d]\n", report->update_secs);

	return 0;
}

int display_nav_sel(unsigned short elem_id, unsigned short length, void *data)
{
	struct dev_app_desc *profile = data;

	printf("Received parameters:\n");
	printf("use_cases		: [0x%x]\n", profile->use_cases);
	printf("gnss_select		: [0x%x]\n", profile->gnss_select);

	return 0;
}

typedef int (*srv_request_handler)(unsigned short elem_id, 
		unsigned short length, void *data);

struct srv_rpc_desc {
	unsigned short rpc_id;
	char *rpc_str;
	srv_request_handler request_handler;
};

struct srv_rpc_desc srv_rpc_arr[] = {
	{REI_DEV_CONNECT, "REI_DEV_CONNECT", display_connect},
	{REI_DEV_RELEASE, "REI_DEV_RELEASE", NULL},
	{REI_DEV_CFG_RPT, "REI_DEV_CFG_RPT", display_cfg_rpt},
	{REI_DEV_NAV_SEL, "REI_DEV_NAV_SEL", display_nav_sel},
	{REI_DEV_VERSION, "REI_DEV_VERSION", send_ver_resp},
	{REI_DEV_WSTATUS, "REI_DEV_WSTATUS", send_dev_stat_resp},
	{REI_DEV_ACTIVE, "REI_DEV_ACTIVE", NULL},
	{REI_DEV_DE_ACT, "REI_DEV_DE_ACT", NULL},
	{REI_DEV_DSLEEP, "REI_DEV_DSLEEP", NULL},
	{REI_DEV_WAKEUP, "REI_DEV_WAKEUP", NULL},
	{REI_TS_PULSE_INFO, "REI_TS_PULSE_INFO", NULL},
	{REI_DEV_REF_CLOCK, "REI_DEV_REF_CLOCK", NULL},
	{REI_CLK_CALIBRATE, "REI_CLK_CALIBRATE", NULL},
	{REI_AREA_GEOFENCE, "REI_AREA_GEOFENCE", NULL},
	{REI_BUF_LOC_FIXES, "REI_BUF_LOC_FIXES", NULL},
	{REI_QOP_SPECIFIER, "REI_QOP_SPECIFIER", NULL},
	{0xFFFF, NULL, NULL}
};

static struct srv_rpc_desc *get_srv_rpc(unsigned short rpc_id)
{
	struct srv_rpc_desc *srv_rpc = srv_rpc_arr;

	while (srv_rpc->rpc_id != 0xFFFF) {
		if (srv_rpc->rpc_id == rpc_id)
			return srv_rpc;

		srv_rpc++;
	}

	return NULL;
}

static int get_integer_input(void)
{
	char arr[100];

	fgets(arr, sizeof(arr), stdin);
	return atoi(arr);
}

static int get_choice(const char *str, int lower_limit, int upper_limit)
{
	int choice;

	do {
		printf("%s", str);

		choice = get_integer_input();

		printf("\nChoice [%d] entered\n", choice);
	} while ((choice < lower_limit && (printf("Invalid choice [%d]\n", choice))) || 
			((choice > upper_limit && printf("Invalid choice [%d]\n", choice))));

	return choice;
}

static int menu(void)
{
	int choice;

	do {
		printf("	MENU\n");
		printf("************\n");
		printf("\t1. Send Device Location indication\n");
		printf("\t2. Send SV Signal Measurement indication\n");
		printf("\t3. Send Location Error indication\n");
		printf("\t4. Send NMEA Message indication\n");
		printf("\t5. Send Assistance indication\n");
		printf("\t6. Send Error indication\n");
		printf("\t7. Send Event indication\n");
		printf("\t8. Send Usecase indication\n");
		printf("Enter choice [1-8]: ");

		choice = get_integer_input();

		printf("\nChoice [%d] entered\n", choice);
	} while ((choice < 1 && (printf("Invalid choice [%d]\n", choice))) || 
			((choice > 8) && printf("Invalid choice [%d]\n", choice)));

	printf("\n");

	return choice;
}

static void send_loc_info_ind(void)
{
}

static void send_meas_info_ind(void)
{
	struct ie_desc *adu;
	struct dev_msr_info dev_msr[] = {
		{
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8,
			9,
			10,
			11,
			12,
			13,
			14,
			15,
			16,
			17,
			18,
			19,
			20,
			21,
			22,
			23
		}
	};

	struct rpc_msg *rmsg = rpc_msg_alloc();
	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_MSR_GPS, OPER_NOP, 
					sizeof(struct dev_msr_info), dev_msr)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return;        
}

static void send_loc_err_ind(void)
{
	struct ie_desc *adu;
	unsigned int loc_err = (unsigned int) not_enough_gps_sv;
	struct rpc_msg *rmsg = rpc_msg_alloc();

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_LOC_EVT, OPER_NOP, 
					sizeof(loc_err), &loc_err)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return;        
}

static void send_nmea_ind(void)
{
	char nmea_str[] = "$GPGGA1234";
	struct ie_desc *adu;
	struct rpc_msg *rmsg = rpc_msg_alloc();

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_NMEA_GPGGA, OPER_NOP, 
					strlen(nmea_str)+1, nmea_str)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return;        
}

static void send_assist_ind(void)
{
}

static void send_dev_error_ind(void)
{
	struct ie_desc *adu;
	unsigned int dev_err = (unsigned int) dummy;
	struct rpc_msg *rmsg = rpc_msg_alloc();

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_DEV_ERR_INF, OPER_INF, 
					sizeof(dev_err), &dev_err)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return;        
}

static void send_dev_event_ind(void)
{
	struct ie_desc *adu;
	unsigned int dev_evt = (unsigned int) evt_dev_ver;
	struct rpc_msg *rmsg = rpc_msg_alloc();

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_DEV_EVT_INF, OPER_INF, 
					sizeof(dev_evt), &dev_evt)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return;        
}

static void send_user_notify_ind(void)
{
	struct ie_desc *adu;
	unsigned int dev_evt = (unsigned int) evt_dev_ver;
	struct rpc_msg *rmsg = rpc_msg_alloc();

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_DEV_EVT_INF, OPER_INF, 
					sizeof(dev_evt), &dev_evt)))
		goto exit2;

	if(rpc_srv_send(rmsg) < 0)
		goto exit3;

	return;

exit3:
	rpc_ie_detach(rmsg, adu);

exit2:
	rpc_msg_free(rmsg);

exit1:
	return;        
}

void *send_indication(void *arg)
{
	int choice;

	while (1) {
		choice = menu();

		switch (choice) {
			case 1:
				send_loc_info_ind();
				break;

			case 2:
				send_meas_info_ind();
				break;

			case 3:
				send_loc_err_ind();
				break;

			case 4:
				send_nmea_ind();
				break;

			case 5:
				send_assist_ind();
				break;

			case 6:
				send_dev_error_ind();
				break;

			case 7:
				send_dev_event_ind();
				break;

			case 8:
				send_user_notify_ind();
				break;

			default:
				break;
		}
	}
}

int main(int argc, char **argv)
{
	int choice;
	int ret_val;
	struct rpc_msg *rmsg;
	struct rm_head *rm_hdr;
	struct ie_head *ie_hdr;
	struct ie_desc *adu;
	int num_ie;
	struct srv_rpc_desc *srv_rpc;
	void *thread_handle;

	rpc_srv_init(SERVER_READ_FIFO, SERVER_WRITE_FIFO);

	thread_handle = os_create_thread(send_indication, NULL);

	while (1) {
		if(rmsg = rpc_srv_recv()) {
			printf("Received message\n");

			rm_hdr = RM_HEADER(rmsg);

			for (num_ie = 0; num_ie < rm_hdr->num_ie; num_ie++) {
				adu = rmsg->info_elem + num_ie;
				ie_hdr = IE_HEADER(rmsg->info_elem + num_ie);

				if (IS_CT_WORD_REQ(ie_hdr->ct_word)) {

					wq_ie_add_to(ie_hdr->elem_id, 
							CTW_2_XACT_ID(ie_hdr->ct_word));

					if (srv_rpc = get_srv_rpc(ie_hdr->elem_id)) {

						printf("Received req : %s\n", srv_rpc->rpc_str);

						if (srv_rpc->request_handler) {
							if (srv_rpc->request_handler(ie_hdr->elem_id, 
										ie_hdr->tot_len, adu->data) == 0)
								printf("Successfully sent back response for \
										request [%s]\n", srv_rpc->rpc_str);
							else
								printf("Error in sending response for request \
										[%s]\n", srv_rpc->rpc_str);
						} else {
							printf("Unsupported req\n");
							rpc_send_err(ie_hdr->elem_id, 
									CTW_2_XACT_ID(ie_hdr->ct_word));
						}
					}
				} else if (IS_CT_WORD_CMD(ie_hdr->ct_word)) {

					if (srv_rpc = get_srv_rpc(ie_hdr->elem_id)) {

						printf("Received cmd : %s\n", srv_rpc->rpc_str);

						if (srv_rpc->request_handler)
							srv_rpc->request_handler(ie_hdr->elem_id, 
										ie_hdr->tot_len, adu->data);

						rpc_send_ack(ie_hdr->elem_id, 
								CTW_2_XACT_ID(ie_hdr->ct_word));
					} else {
						printf("Unsupported cmd\n");
						rpc_send_err(ie_hdr->elem_id, 
								CTW_2_XACT_ID(ie_hdr->ct_word));
					}
				} else
					printf("Unsupported packet\n");
			}

			rpc_msg_free(rmsg);
		}
	}

	return 0;
}

