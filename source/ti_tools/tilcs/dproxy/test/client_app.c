/*
 * client_app.c
 *
 * RPC Client test application
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "device.h"
#include "dev_proxy.h"
#include "rpc.h"
#include "utils.h"

static int location_rpt_cb(enum ue_pos_id id, void *data_array, int size)
{
	printf("location_rpt indication callback: enum [%d] and size [%d]\n", id, size);

	switch (id) {
		case loc_exl_native:
			{
				struct dev_pos_info *dev_pos = (struct dev_pos_info *) data_array;
				struct dev_ellp_unc *dev_ell = dev_pos->ell =
					BUF_OFFSET_ST(dev_pos, struct dev_pos_info,
							struct dev_ellp_unc);
				struct dev_utc_info *dev_utc = dev_pos->utc =
					BUF_OFFSET_ST(dev_ell, struct dev_ellp_unc,
							struct dev_utc_info);

				printf("pos_flags         [%10d]\n", dev_pos->pos_flags);
				printf("lat_rad           [%10d]\n", dev_pos->lat_rad);
				printf("lon_rad           [%10d]\n", dev_pos->lon_rad);
				printf("alt_wgs84         [%10d]\n", dev_pos->alt_wgs84);
				printf("has_msl           [%10d]\n", dev_pos->has_msl);
				printf("alt_msl           [%10d]\n", dev_pos->alt_msl);
				printf("unc_east          [%10d]\n", dev_pos->unc_east);
				printf("unc_north         [%10d]\n", dev_pos->unc_north);
				printf("unc_vertical      [%10d]\n", dev_pos->unc_vertical);

				printf("orientation       [%10d]\n", dev_ell->orientation);
				printf("semi_major        [%10d]\n", dev_ell->semi_major);
				printf("semi_minor        [%10d]\n", dev_ell->semi_minor);

				printf("hours             [%10d]\n", dev_utc->hours);
				printf("minutes           [%10d]\n", dev_utc->minutes);
				printf("seconds           [%10d]\n", dev_utc->seconds);
				printf("sec_1by10         [%10d]\n", dev_utc->sec_1by10);
			}
			break;

		case loc_sat_native:
			{
				struct dev_sat_info *dev_sat = (struct dev_sat_info *) data_array;

				printf("No. of satellites     [%10d]\n", size);

				while (size--) {
					printf("gnss_id           [%10d]\n", dev_sat->gnss_id);
					printf("svid              [%10d]\n", dev_sat->svid);
					printf(" ->iode           [%10d]\n", dev_sat->iode);
					printf(" ->msr_residual   [%10d]\n", dev_sat->msr_residual);
					printf(" ->weigh_in_fix   [%10d]\n", dev_sat->weigh_in_fix);

					dev_sat++;
				}
			}
			break;

		case loc_dop_native:
			{
				struct dev_dop_info *dev_dop = (struct dev_dop_info *) data_array;

				printf("position          [%10d]\n", dev_dop->position);
				printf("horizontal        [%10d]\n", dev_dop->horizontal);
				printf("vertical          [%10d]\n", dev_dop->vertical);
				printf("time              [%10d]\n", dev_dop->time);
			}
			break;

		case loc_all_native:
			{
				struct dev_pos_info *dev_pos = (struct dev_pos_info *) data_array;
				struct dev_ellp_unc *dev_ell = dev_pos->ell =
					BUF_OFFSET_ST(dev_pos, struct dev_pos_info,
							struct dev_ellp_unc);
				struct dev_utc_info *dev_utc = dev_pos->utc =
					BUF_OFFSET_ST(dev_ell, struct dev_ellp_unc,
							struct dev_utc_info);
				struct dev_dop_info *dev_dop =
					BUF_OFFSET_ST(dev_utc, struct dev_utc_info,
							struct dev_dop_info);
				struct dev_vel_info *dev_vel =
					BUF_OFFSET_ST(dev_dop, struct dev_dop_info,
							struct dev_vel_info);
				struct dev_hdg_info *dev_hdg =
					BUF_OFFSET_ST(dev_vel, struct dev_vel_info,
							struct dev_hdg_info);
				struct dev_sat_info *dev_sat =
					BUF_OFFSET_ST(dev_hdg, struct dev_hdg_info,
							struct dev_sat_info);

				printf("pos_flags         [%10d]\n", dev_pos->pos_flags);
				printf("lat_rad           [%10d]\n", dev_pos->lat_rad);
				printf("lon_rad           [%10d]\n", dev_pos->lon_rad);
				printf("alt_wgs84         [%10d]\n", dev_pos->alt_wgs84);
				printf("has_msl           [%10d]\n", dev_pos->has_msl);
				printf("alt_msl           [%10d]\n", dev_pos->alt_msl);
				printf("unc_east          [%10d]\n", dev_pos->unc_east);
				printf("unc_north         [%10d]\n", dev_pos->unc_north);
				printf("unc_vertical      [%10d]\n", dev_pos->unc_vertical);
				printf("orientation       [%10d]\n", dev_ell->orientation);
				printf("semi_major        [%10d]\n", dev_ell->semi_major);
				printf("semi_minor        [%10d]\n", dev_ell->semi_minor);
				printf("hours             [%10d]\n", dev_utc->hours);
				printf("minutes           [%10d]\n", dev_utc->minutes);
				printf("seconds           [%10d]\n", dev_utc->seconds);
				printf("sec_1by10         [%10d]\n", dev_utc->sec_1by10);

				printf("position          [%10d]\n", dev_dop->position);
				printf("horizontal        [%10d]\n", dev_dop->horizontal);
				printf("vertical          [%10d]\n", dev_dop->vertical);
				printf("time              [%10d]\n", dev_dop->time);

				printf("gnss_id           [%10d]\n", dev_sat->gnss_id);
				printf("svid              [%10d]\n", dev_sat->svid);
				printf("iode              [%10d]\n", dev_sat->iode);
				printf("msr_residual      [%10d]\n", dev_sat->msr_residual);
				printf("weigh_in_fix      [%10d]\n", dev_sat->weigh_in_fix);
			}
			break;

		case loc_gnss_ie:
			{
				struct loc_gnss_info  *dev_gnss = (struct loc_gnss_info *) data_array;
				struct location_desc *loc = &dev_gnss->location;

				printf("fix_type          [%10d]\n", dev_gnss->fix_type);
				printf("gnss_tid          [%10d]\n", dev_gnss->gnss_tid);
				//printf("loc_time          [%10d]\n", dev_gnss->loc_time);
				printf("fix_type          [%10d]\n", dev_gnss->fix_type);
				printf("pos_bits          [%10d]\n", dev_gnss->pos_bits);
				printf("shape             [%10d]\n", loc->shape);
				printf("lat_sign          [%10d]\n", loc->lat_sign);
				printf("latitude          [%10d]\n", loc->latitude_N);
				printf("longitude         [%10d]\n", loc->longitude_N);
				printf("alt_dir           [%10d]\n", loc->alt_dir);
				printf("altitude          [%10d]\n", loc->altitude_N);
				printf("unc_semi_major    [%10d]\n", loc->unc_semi_maj_K);
				printf("unc_semi_minor    [%10d]\n", loc->unc_semi_min_K);
				printf("orientation       [%10d]\n", loc->orientation);
				printf("unc_altitude      [%10d]\n", loc->unc_altitude_K);
				printf("confidence        [%10d]\n", loc->confidence);
			}
			break;

		case vel_native:
			{
				struct dev_vel_info *dev_vel = (struct dev_vel_info *) data_array;
				dev_vel->hdg_info = BUF_OFFSET_ST(dev_vel, struct dev_vel_info,
												  struct dev_hdg_info);

				printf("east              [%10d]\n", dev_vel->east);
				printf("north             [%10d]\n", dev_vel->north);
				printf("vertical          [%10d]\n", dev_vel->vertical);
				printf("uncetainty        [%10d]\n", dev_vel->uncertainty);
				printf("truthful          [%10d]\n", dev_vel->hdg_info->truthful);
				printf("magnetic          [%10d]\n", dev_vel->hdg_info->magnetic);

			}
			break;

		case vel_gnss_ie:
			{
				struct vel_gnss_info *dev_vel = (struct vel_gnss_info *) data_array;

				printf("type              [%10d]\n", dev_vel->type);
				printf("bearing           [%10d]\n", dev_vel->bearing);
				printf("h_speed           [%10d]\n", dev_vel->h_speed);
				printf("ver_dir           [%10d]\n", dev_vel->ver_dir);
				printf("v_speed           [%10d]\n", dev_vel->v_speed);
				printf("h_unc             [%10d]\n", dev_vel->h_unc);
				printf("v_unc             [%10d]\n", dev_vel->v_unc);
			}
			break;

		case tim_gps_native:
			{
				struct dev_gps_time *dev_time = (struct dev_gps_time *) data_array;

				printf("timer_count       [%10d]\n", dev_time->timer_count);
				printf("week_num          [%10d]\n", dev_time->week_num);
				printf("time_msec         [%10d]\n", dev_time->time_msec);
				printf("time_bias         [%10d]\n", dev_time->time_bias);
				printf("time_unc          [%10d]\n", dev_time->time_unc);
				printf("freq_bias         [%10d]\n", dev_time->freq_bias);
				printf("freq_unc          [%10d]\n", dev_time->freq_unc);
				printf("is_utc_diff       [%10d]\n", dev_time->is_utc_diff);
				printf("utc_diff          [%10d]\n", dev_time->utc_diff);
			}
			break;

		case tim_glo_native:
			break;

		case tim_gps_ie:
			{
				struct gps_time *time = (struct gps_time *) data_array;

				printf("week_nr           [%10d]\n", time->week_nr);
				printf("tow_23b           [%10d]\n", time->tow);
			}
			break;

		case tim_glo_ie:
			break;

		default:
			break;
	}

	return 0;
}

static int measure_info_cb(enum ue_msr_id id, void *data_array, int size)
{
	printf("measure_info indication callback: enum [%d] and size [%d]\n", id, size);

	switch (id) {
		case msr_gps_native:
			{
				struct dev_msr_info *dev_msr = (struct dev_msr_info *) data_array;

				printf("No. of satellites     [%10d]\n", size);

				while (size--) {
					printf("gnss_id           [%10d]\n", dev_msr->gnss_id);
					printf("svid              [%10d]\n", dev_msr->svid);
					/*printf(" ->snr            [%10d]\n", dev_msr->snr);
					printf(" ->cno            [%10d]\n", dev_msr->cno);
					printf(" ->latency_ms     [%10d]\n", dev_msr->latency_ms);
					printf(" ->pre_int        [%10d]\n", dev_msr->pre_int);
					printf(" ->post_int       [%10d]\n", dev_msr->post_int);*/
					printf(" ->msec           [%10d]\n", dev_msr->msec);
					printf(" ->sub_msec       [%10d]\n", dev_msr->sub_msec);
					/*printf(" ->time_unc       [%10d]\n", dev_msr->time_unc);
					printf(" ->speed          [%10d]\n", dev_msr->speed);
					printf(" ->speed_unc      [%10d]\n", dev_msr->speed_unc);
					printf(" ->msr_flags      [%10d]\n", dev_msr->msr_flags);
					printf(" ->ch_states      [%10d]\n", dev_msr->ch_states);
					printf(" ->accum_cp       [%10d]\n", dev_msr->accum_cp);
					printf(" ->carrier_vel    [%10d]\n", dev_msr->carrier_vel);
					printf(" ->carrier_acc    [%10d]\n", dev_msr->carrier_acc);
					printf(" ->loss_lock_ind  [%10d]\n", dev_msr->loss_lock_ind);
					printf(" ->good_obv_cnt   [%10d]\n", dev_msr->good_obv_cnt);
					printf(" ->total_obv_cnt  [%10d]\n", dev_msr->total_obv_cnt);
					printf(" ->sv_slot        [%10d]\n", dev_msr->sv_slot);
					printf(" ->diff_sv        [%10d]\n", dev_msr->diff_sv);
					printf(" ->early_term     [%10d]\n", dev_msr->early_term);*/
					printf(" ->elevation      [%10d]\n", dev_msr->elevation);
					printf(" ->azimuth        [%10d]\n", dev_msr->azimuth);

					dev_msr++;
				}
			}
			break;

		case msr_gps_rpt_ie:
			{
				struct gps_msr_info *dev_msr = (struct gps_msr_info *) data_array;

				printf("No. of satellites [%10d]\n", size);

				while (size--) {
					/*
					printf("svid              [%10d]\n", dev_msr->svid);
					printf(" ->c_no           [%10d]\n", dev_msr->c_no);
					printf(" ->doppler        [%10d]\n", dev_msr->doppler);
					printf(" ->whole_chips    [%10d]\n", dev_msr->whole_chips);
					printf(" ->frac_chips     [%10d]\n", dev_msr->frac_chips);
					printf(" ->mpath_ind      [%10d]\n", dev_msr->mpath_indc);
					*/
					//printf(" ->pr_rms_err     [%10d]\n", dev_msr->pr_rms_err);

					dev_msr++;
				}
			}
			break;

		case msr_glo_native:
			{
			}
			break;

		case msr_glo_signal:
			{
				struct glo_sig_info *dev_msr =
					(struct glo_sig_info *) data_array;
				struct ganss_sig_info *dev_sig;

				printf("No. of satellites [%10d]\n", size);

				while (size--) {
					dev_sig = (struct ganss_sig_info *)&dev_msr->signal;

					printf("options           [%10d]\n", dev_sig->ganss_id);
					printf(" ->signal_id      [%10d]\n", dev_sig->signal_id);
					printf(" ->num_sat        [%10d]\n", dev_sig->n_sat);
					printf(" ->ambiguity      [%10d]\n", dev_sig->ambiguity);

					dev_msr++;
				}
			}
			break;

		case msr_glo_sig_G1:
			{
			}
			break;

		case msr_glo_sig_G2:
			{
			}
			break;

		default:
			break;
	}

	return 0;
}

static int location_evt_cb(enum lc_evt_id id)
{
	printf("location_evt indication callback\n");

	UNUSED(id);
	return 0;
}

static int nmea_message_cb(enum nmea_sn_id id, char *data, int size)
{
	char *nmea = calloc(size+1, sizeof(char));

	if(nmea==NULL){
		printf("error: nmea var could not be allocated\n");
		return 0;
	}
	printf("nmea_message indication callback\n");

	memcpy(nmea, data, size);
	nmea[size]= '\0';
	
	printf("NMEA [%s]\n", nmea);

	free(nmea);

	UNUSED(id);
	return 0;
}

static int device_agnss_cb(enum loc_assist assist, void *assist_array, int size)
{
	printf("device_agnss indication callback\n");

	UNUSED(assist);
	UNUSED(assist_array);
	UNUSED(size);
	return 0;
}

static int device_error_cb(enum dev_err_id id)
{
	printf("device_error indication callback\n");

	UNUSED(id);
	return 0;
}

static int device_event_cb(enum dev_evt_id id, void *data_array, int size)
{
	printf("device_event indication callback: enum [%d] and size [%d]\n", id,
		   size);

	switch (id) {
		case evt_dev_ver:
			{
				struct dev_version *ver = (struct dev_version *)data_array;

				printf("chip_id_major       : [%d]\n", ver->chip_id_major);
				printf("chip_id_minor       : [%d]\n", ver->chip_id_minor);
				printf("rom_id_major        : [%d]\n", ver->rom_id_major);
				printf("rom_id_minor        : [%d]\n", ver->rom_id_minor);
				printf("rom_id_sub_minor1   : [%d]\n", ver->rom_id_sub_minor1);
				printf("rom_id_sub_minor2   : [%d]\n", ver->rom_id_sub_minor2);
				printf("rom_day             : [%d]\n", ver->rom_day);
				printf("rom_month           : [%d]\n", ver->rom_month);
				printf("rom_year            : [%d]\n", ver->rom_year);
				printf("patch_major         : [%d]\n", ver->patch_major);
				printf("patch_minor         : [%d]\n", ver->patch_minor);
				printf("patch_day           : [%d]\n", ver->patch_day);
				printf("patch_month         : [%d]\n", ver->patch_month);
				printf("patch_year          : [%d]\n", ver->patch_year);
			}
			break;

		default:
			break;
	}

	printf("device_event indication callback: enum [%d] and size [%d] done\n", id,
		   size);
	return 0;
}

static int ucase_notify_cb(enum app_rpt_id id, void *data)
{
	printf("ucase_notify indication callback\n");

	UNUSED(id);
	UNUSED(data);
	return 0;
}

/* ti_dev_proxy_connect parameters */
struct ti_dev_callbacks cbs[] = {
	{
		location_rpt_cb,    /* Device location info              */
		measure_info_cb,    /* SV signal Measurement info        */
		location_evt_cb,    /* Location Error                    */
		nmea_message_cb,    /* Report NMEA message               */
		device_agnss_cb,    /* Assist info decoded by dev        */
		device_error_cb,    /* Notify multiple errors            */
		device_event_cb,    /* Events from device                */
		ucase_notify_cb     /* Usecase indication from dev       */
	}
};

struct ti_dev_proxy_params params[] = {
	{
		10	/* Timeout for communication w/ dev */
	}
};

/* ti_dev_config_reports parameters */
unsigned int async_events;
unsigned int dev_agnss;
unsigned int nmea_flags;
unsigned int msr_reports;
unsigned int pos_reports;
unsigned short interval;

/* ti_dev_setup_oper_param parameters */
int param_id;

struct dev_ts_pulse_info pulse;
struct dev_ref_clk_param ref_ck;
struct dev_clk_calibrate clk_calib;
struct geofencing_params geofence;
struct buffered_location buff_loc;
struct dev_qop_specifier qop;

void * param_value_arr[] = {
	(void *)&pulse,
	(void *)&ref_ck,
	(void *)&clk_calib,
	(void *)&geofence,
	(void *)&buff_loc,
	(void *)&qop
};

/* ti_dev_set_app_profile parameters */
unsigned int use_cases;
unsigned int gnss_select;

/* ti_dev_exec_action parameters */
enum dev_action act_exec;

/* ti_dev_version parameters */
struct dev_version version;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_eph eph;
struct dev_gps_eph *dev_gps_eph_info = &eph;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_alm gps_almanac;
struct dev_gps_alm *dev_gps_alm_info = &gps_almanac;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_ion gps_ion;
struct dev_gps_ion *dev_gps_ion_info = &gps_ion;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_utc utc;
struct dev_gps_utc *dev_gps_utc_info = &utc;

static int get_integer_input(void)
{
	char arr[100];

	fgets(arr, sizeof(arr), stdin);
	return atoi(arr);
}

static int get_choice(const char *str, int lower_limit, int upper_limit)
{
	int choice;

	do {
		printf("%s", str);

		choice = get_integer_input();

		printf("\nChoice [%d] entered\n", choice);
	} while ((choice < lower_limit && (printf("Invalid choice [%d]\n", choice))) ||
			((choice > upper_limit && printf("Invalid choice [%d]\n", choice))));

	return choice;
}

static int menu(void)
{
	int choice;

	do {
		printf("	MENU\n");
		printf("************\n");
		printf("\t1. ti_dev_proxy_connect\n");
		printf("\t2. ti_dev_proxy_release\n");
		printf("\t3. ti_dev_connect\n");
		printf("\t4. ti_dev_release\n");
		printf("\t5. ti_dev_version\n");
		printf("\t6. ti_dev_config_reports\n");
		printf("\t7. ti_dev_set_app_profile\n");
		printf("\t8. ti_dev_setup_oper_param\n");
		printf("\t9. ti_dev_exec_seq\n");
		printf("\t10. ti_dev_is_alive\n");
		printf("\t11. ti_dev_proxy_version\n");
		printf("\t12. ti_dev_loc_assist_get gps ephemeris\n");
		printf("\t13. ti_dev_loc_assist_get gps almanac\n");
		printf("\t14. ti_dev_loc_assist_get gps ionospheric info\n");
		printf("\t15. ti_dev_loc_assist_get gps utc\n");
		printf("Enter choice [1-15]: ");

		choice = get_integer_input();

		printf("\nChoice [%d] entered\n", choice);
	} while ((choice < 1 && (printf("Invalid choice [%d]\n", choice))) ||
			((choice > 15) && printf("Invalid choice [%d]\n", choice)));

	switch (choice) {
		case 6:
			//async_events = get_choice("Enter async_events value: ", 1, 100);
			//dev_agnss = get_choice("Enter dev_agnss value: ", 1, 100);
			//nmea_flags = get_choice("Enter nmea_flags value: ", 1, 100);
			//msr_reports = get_choice("Enter msr_reports value: ", 1, 100);
			//pos_reports = get_choice("Enter pos_reports value: ", 1, 100);
			interval = get_choice("Enter interval value: ", 1, 100);
			break;

		case 7:
			use_cases = get_choice("Enter use_cases value: ", 1, 100);
			gnss_select = get_choice("Enter gnss_select value: ", 1, 100);
			break;

		case 8:
			param_id = get_choice("Setup parameter:\n1. dev_ts_pulse_info\n2. dev_ref_clk_param\n3. dev_clk_calibrate\n4. geofencing_params\n5. buffered_location\n6. dev_qop_specifier\nEnter choice [1-6]: ", 1, 6);
			break;

		case 9:
			act_exec = get_choice("Enter act to execute:\n1. act_active\n2. act_de_act\n3. act_dsleep\n4. act_wakeup\nEnter choice [1-4]: ", 1, 4);
			break;

		case 12:
			memset(dev_gps_eph_info, 0, sizeof(struct dev_gps_eph));
			dev_gps_eph_info->prn = (unsigned char)get_choice("Enter gps sv id:\nEnter choice [1-32]: ", 1, 32);
			break;

		case 13:
			memset(dev_gps_alm_info, 0, sizeof(struct dev_gps_alm));
			dev_gps_alm_info->prn = (unsigned char)get_choice("Enter gps sv id:\nEnter choice [1-32]: ", 1, 32);
			break;

		case 14:
			memset(dev_gps_ion_info, 0, sizeof(struct dev_gps_ion));
			break;

		case 15:
			memset(dev_gps_utc_info, 0, sizeof(struct dev_gps_utc));
			break;

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 10:
		case 11:
		default:
			break;
	}

	printf("\n");

	return choice;
}

int main(int argc, char **argv)
{
	int choice;
	int ret_val;
	struct dev_proxy_version proxy_version;

	while (1) {
		choice = menu();

		switch (choice) {
			case 1:
				if ((ret_val = ti_dev_proxy_connect(cbs, params)) < 0)
					printf("ti_dev_proxy_connect failed\n");
				else
					printf("ti_dev_proxy_connect successful\n");
				break;

			case 2:
				ti_dev_proxy_release();
				break;

			case 3:
				{
					struct dev_start_up_info start_info;

					if ((ret_val = ti_dev_connect(&start_info)) < 0)
						printf("ti_dev_connect failed\n");
					else
						printf("ti_dev_connect successful\n");
				}
				break;

			case 4:
				ti_dev_release();
				break;

			case 5:
				if ((ret_val = ti_dev_version(&version)) < 0)
					printf("Failed to obtain TI dev version\n");
				else {
					printf("TI dev version:\n");
					printf("chip_id_major       : [%d]\n", version.chip_id_major);
					printf("chip_id_minor       : [%d]\n", version.chip_id_minor);
					printf("rom_id_major        : [%d]\n", version.rom_id_major);
					printf("rom_id_minor        : [%d]\n", version.rom_id_minor);
					printf("rom_id_sub_minor1   : [%d]\n", version.rom_id_sub_minor1);
					printf("rom_id_sub_minor2   : [%d]\n", version.rom_id_sub_minor2);
					printf("rom_day             : [%d]\n", version.rom_day);
					printf("rom_month           : [%d]\n", version.rom_month);
					printf("rom_year            : [%d]\n", version.rom_year);
					printf("patch_major         : [%d]\n", version.patch_major);
					printf("patch_minor         : [%d]\n", version.patch_minor);
					printf("patch_day           : [%d]\n", version.patch_day);
					printf("patch_month         : [%d]\n", version.patch_month);
					printf("patch_year          : [%d]\n", version.patch_year);
				}
				break;

			case 6:
				async_events = 0;

				dev_agnss = 0;

				/* SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) |
				   SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg)
				 */
				nmea_flags = 0;

				/* MSR_RPT_GPS_NATIVE | MSR_RPT_GPS_IE_SEL | MSR_RPT_GLO_NATIVE |
				   MSR_RPT_GLO_IE_SEL
				 */
				msr_reports = MSR_RPT_GPS_NATIVE;

				/* POS_RPT_LOC_NATIVE | POS_RPT_GPS_IE_SEL | POS_RPT_GLO_IE_SEL |
				   POS_RPT_VEL_NATIVE | POS_RPT_VEL_IE_SEL |
				   POS_RPT_TIM_GPS_NATIVE | POS_RPT_TIM_GLO_NATIVE |
				   POS_RPT_TIM_GPS_IE_SEL | POS_RPT_TIM_GLO_IE_SEL
				 */
				//pos_reports = POS_RPT_NATIVE_ALL | POS_RPT_TIM_GPS_NATIVE | POS_RPT_VEL_NATIVE;
				pos_reports = POS_RPT_GPS_IE_SEL | POS_RPT_SAT_NATIVE |
					POS_RPT_VEL_NATIVE | POS_RPT_DOP_NATIVE | POS_RPT_TIM_GPS_IE_SEL;

				interval = 1;

				if ((ret_val = ti_dev_config_reports(async_events,
								dev_agnss, nmea_flags, msr_reports,
								pos_reports, interval)) < 0)
					printf("ti_dev_config_reports failed\n");
				else {
					printf("Successfully set TI dev-proxy parameters:\n");
					printf("async_events	: [0x%x]\n", async_events);
					printf("dev_agnss		: [0x%x]\n", dev_agnss);
					printf("nmea_flags		: [%d]\n", nmea_flags);
					printf("msr_reports		: [0x%x]\n", msr_reports);
					printf("pos_reports		: [0x%x]\n", pos_reports);
					printf("interval		: [%d]\n", interval);
				}
				break;

			case 7:
				if ((ret_val = ti_dev_set_app_profile(use_cases, gnss_select)) < 0)
					printf("ti_dev_set_app_profile failed\n");
				else
					printf("ti_dev_set_app_profile was successful\n");
				break;

			case 8:
				if ((ret_val = ti_dev_setup_oper_param(param_id,
							param_value_arr[param_id])) < 0)
					printf("ti_dev_setup_oper_param failed");
				else
					printf("Successfully set operation parameter [%d]\n",
							param_id);
				break;

			case 9:
				if ((ret_val = ti_dev_exec_action(--act_exec)) < 0)
					printf("ti_dev_exec_seq failed");
				else
					printf("Successfully executed sequence\n");
				break;

			case 10:
				if ((ret_val = ti_dev_is_alive()) < 0)
					printf("ti_dev_is_alive failed");
				else
					printf("Received device status [%d]\n", ret_val);
				break;

			case 11:
				if ((ret_val = ti_dev_proxy_version(&proxy_version)) < 0)
					printf("ti_dev_proxy_version failed");
				else {
					printf("TI devproxy version:\n");
					printf("major               : [%d]\n", proxy_version.major);
					printf("minor               : [%d]\n", proxy_version.minor);
					printf("patch               : [%d]\n", proxy_version.patch);
					printf("build               : [%d]\n", proxy_version.build);
					printf("exten               : [%c]\n", proxy_version.exten);
					printf("qfier               : [%s]\n", proxy_version.qfier);
					printf("day                 : [%d]\n", proxy_version.day);
					printf("month               : [%d]\n", proxy_version.month);
					printf("year                : [%d]\n", proxy_version.year);
				}
				break;

			case 12:
				if ((ret_val = ti_dev_loc_aiding_get(a_GPS_EPH, dev_gps_eph_info, 1)) < 0)
					printf("ti_dev_loc_aiding_get gps eph failed");
				else {
					printf("TI GPS EPH details:\n");
					printf("prn                 : [%10d]\n", dev_gps_eph_info->prn);
					printf("Code_on_L2          : [%10d]\n", dev_gps_eph_info->Code_on_L2);
					printf("SV_Accuracy         : [%10d]\n", dev_gps_eph_info->Accuracy);
					printf("SV_Health           : [%10d]\n", dev_gps_eph_info->Health);
					printf("Tgd                 : [%10d]\n", dev_gps_eph_info->Tgd);
					printf("IODC                : [%10d]\n", dev_gps_eph_info->IODC);
					printf("Toc                 : [%10d]\n", dev_gps_eph_info->Toc);
					printf("Af2                 : [%10d]\n", dev_gps_eph_info->Af2);
					printf("Af1                 : [%10d]\n", dev_gps_eph_info->Af1);
					printf("Af0                 : [%10d]\n", dev_gps_eph_info->Af0);
					printf("IODE                : [%10d]\n", dev_gps_eph_info->IODE);
					printf("Crs                 : [%10d]\n", dev_gps_eph_info->Crs);
					printf("DeltaN              : [%10d]\n", dev_gps_eph_info->DeltaN);
					printf("Mo                  : [%10d]\n", dev_gps_eph_info->Mo);
					printf("Cuc                 : [%10d]\n", dev_gps_eph_info->Cuc);
					printf("E                   : [%10d]\n", dev_gps_eph_info->E);
					printf("Cus                 : [%10d]\n", dev_gps_eph_info->Cus);
					printf("SqrtA               : [%10d]\n", dev_gps_eph_info->SqrtA);
					printf("Toe                 : [%10d]\n", dev_gps_eph_info->Toe);
					printf("Cic                 : [%10d]\n", dev_gps_eph_info->Cic);
					printf("Omega0              : [%10d]\n", dev_gps_eph_info->Omega0);
					printf("Cis                 : [%10d]\n", dev_gps_eph_info->Cis);
					printf("Io                  : [%10d]\n", dev_gps_eph_info->Io);
					printf("Crc                 : [%10d]\n", dev_gps_eph_info->Crc);
					printf("Omega               : [%10d]\n", dev_gps_eph_info->Omega);
					printf("OmegaDot            : [%10d]\n", dev_gps_eph_info->OmegaDot);
					printf("Idot                : [%10d]\n", dev_gps_eph_info->Idot);
					printf("Week                : [%10d]\n", dev_gps_eph_info->Week);
					printf("UpdateWeek          : [%10d]\n", dev_gps_eph_info->UpdateWeek);
					printf("UpdateMS            : [%10d]\n", dev_gps_eph_info->UpdateMS);
				}
				break;

			case 13:
				if ((ret_val = ti_dev_loc_aiding_get(a_GPS_ALM, dev_gps_alm_info, 1)) < 0)
					printf("ti_dev_loc_aiding_get gps alm failed");
				else {
					printf("TI GPS ALM details:\n");
					printf("prn                 : [%10d]\n", dev_gps_alm_info->prn);
					printf("Health              : [%10d]\n", dev_gps_alm_info->Health);
					printf("Toa                 : [%10d]\n", dev_gps_alm_info->Toa);
					printf("E                   : [%10d]\n", dev_gps_alm_info->E);
					printf("DeltaI              : [%10d]\n", dev_gps_alm_info->DeltaI);
					printf("OmegaDot            : [%10d]\n", dev_gps_alm_info->OmegaDot);
					printf("SqrtA               : [%10d]\n", dev_gps_alm_info->SqrtA);
					printf("OmegaZero           : [%10d]\n", dev_gps_alm_info->OmegaZero);
					printf("Omega               : [%10d]\n", dev_gps_alm_info->Omega);
					printf("MZero               : [%10d]\n", dev_gps_alm_info->MZero);
					printf("Af0                 : [%10d]\n", dev_gps_alm_info->Af0);
					printf("Af1                 : [%10d]\n", dev_gps_alm_info->Af1);
					printf("Week                : [%10d]\n", dev_gps_alm_info->Week);
					printf("UpdateWeek          : [%10d]\n", dev_gps_alm_info->UpdateWeek);
					printf("UpdateMS            : [%10d]\n", dev_gps_alm_info->UpdateMS);
				}
				break;

			case 14:
				if ((ret_val = ti_dev_loc_aiding_get(a_GPS_ION, dev_gps_ion_info, 1)) < 0)
					printf("ti_dev_loc_aiding_get gps ion failed");
				else {
					printf("TI GPS ION details:\n");
					printf("Alpha0              : [%10d]\n", dev_gps_ion_info->Alpha0);
					printf("Alpha1              : [%10d]\n", dev_gps_ion_info->Alpha1);
					printf("Alpha2              : [%10d]\n", dev_gps_ion_info->Alpha2);
					printf("Alpha3              : [%10d]\n", dev_gps_ion_info->Alpha3);
					printf("Beta0               : [%10d]\n", dev_gps_ion_info->Beta0);
					printf("Beta1               : [%10d]\n", dev_gps_ion_info->Beta1);
					printf("Beta2               : [%10d]\n", dev_gps_ion_info->Beta2);
					printf("Beta3               : [%10d]\n", dev_gps_ion_info->Beta3);
				}
				break;

			case 15:
				if ((ret_val = ti_dev_loc_aiding_get(a_GPS_UTC, dev_gps_utc_info, 1)) < 0)
					printf("ti_dev_loc_aiding_get gps utc failed");
				else {
					printf("TI GPS UTC details:\n");
					printf("A0                  : [%10d]\n", dev_gps_utc_info->A0);
					printf("A1                  : [%10d]\n", dev_gps_utc_info->A1);
					printf("DeltaTls            : [%10d]\n", dev_gps_utc_info->DeltaTls);
					printf("Tot                 : [%10d]\n", dev_gps_utc_info->Tot);
					printf("WNt                 : [%10d]\n", dev_gps_utc_info->WNt);
					printf("WNlsf               : [%10d]\n", dev_gps_utc_info->WNlsf);
					printf("DN                  : [%10d]\n", dev_gps_utc_info->DN);
					printf("DeltaTlsf           : [%10d]\n", dev_gps_utc_info->DeltaTlsf);
				}
				break;
			default:
				printf("Invalid choice [%d]\n", choice);
				break;
		}
	}

	UNUSED(argc);
	UNUSED(argv);
	return 0;
}

