/*
 * refapp.c
 *
 * RPC Client test application
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <string.h>

#include <device.h>
#include <dev_proxy.h>
#include <rpc.h>

/* Callbacks. */
static int refapp_pos_rpt_cb(enum ue_pos_id id, void *data_array, int num)
{
	printf("[info][refapp]refapp_pos_rpt_cb\n");
	return 0;
}

static int refapp_mes_rpt_cb(enum ue_msr_id id, void *data_array, int num)
{
	printf("[info][refapp]refapp_mes_rpt_cb\n");
	return 0;
}

static int refapp_evt_rpt_cb(enum lc_evt_id id)
{
	printf("[info][refapp]refapp_evt_rpt_cb\n");
	return 0;
}

static int refapp_nmea_cb(enum nmea_sn_id nmea, char *data, int num)
{
	printf("[info][refapp]refapp_nmea_cb\n");
	printf("%s\n", data);
	return 0;
}

static int refapp_assist_cb(enum loc_assist assist, void *assist_array, int num)
{
	printf("[info][refapp]refapp_assist_cb\n");
	return 0;
}

static int refapp_dev_err_cb(enum dev_err_id id)
{
	printf("[info][refapp]refapp_dev_err_cb\n");
	return 0;
}

static int refapp_dev_evt_cb(enum dev_evt_id id)
{
	printf("[info][refapp]refapp_dev_evt_cb\n");
	return 0;
}

static int refapp_ucase_ind_cb(enum app_rpt_id id, void *data)
{
	printf("[info][refapp]refapp_ucase_ind_cb\n");
	return 0;
}


/* Parameters. */
static struct ti_dev_callbacks cbs[1] = {
        {
                refapp_pos_rpt_cb,
                refapp_mes_rpt_cb,
                refapp_err_rpt_cb,
                refapp_nmea_cb,
                refapp_assist_cb,
                refapp_dev_err_cb,
                refapp_dev_evt_cb,
                refapp_ucase_ind_cb
        }
};

struct ti_dev_proxy_params params[1] = {
	{10}
};

#define GPS_CONFIG_FILE "./dproxy.config"
#define GPS_PATCH_FILE "./dproxy.patch"
static struct dev_start_up_info start_info[1];

static unsigned int async_events;
static unsigned int dev_agnss;
static unsigned int nmea_flags;
static unsigned int msr_reports;
static unsigned int pos_reports;
static unsigned short interval;

static void refapp_dev_init(void)
{
	int ret_val = -1;

	/* 1. Proxy Connect. */
	printf("[info][refapp]calling ti_dev_proxy_connect()\n");
	if ((ret_val = ti_dev_proxy_connect(cbs, params)) < 0)
		printf("[err][refapp]ti_dev_proxy_connect() returns %d\n", ret_val);

	/* 2. Device connect. */
	printf("[info][refapp]calling ti_dev_connect()\n");
	memcpy(start_info->sevice_pack, GPS_PATCH_FILE, strlen(GPS_PATCH_FILE));
	memcpy(start_info->config_file, GPS_CONFIG_FILE,
						strlen(GPS_CONFIG_FILE));
	if ((ret_val = ti_dev_connect(start_info)) < 0)
		printf("[err][refapp]ti_dev_connect() returns %d\n", ret_val);

	/* 3. App. profile. */
//	printf("[info][refapp]calling ti_dev_set_app_profile() to DEV_GNSS_GPS_EN\n");
//	if ((ret_val = ti_dev_set_app_profile(0, DEV_GNSS_GPS_EN)) < 0)
//		printf("[err][refapp]ti_dev_set_app_profile() returns %d\n", ret_val);

	/* 4. Put device to sleep. */
	printf("[info][refapp]calling ti_dev_exec_act(sleep)\n");
	if ((ret_val = ti_dev_exec_action(dev_dsleep)) < 0)
		printf("[err][refapp]ti_dev_exec_act(sleep) returns %d\n", ret_val);
}

static void refapp_gps_start_uc_cold_start_auto(void)
{
	int ret_val = -1;

	 /* 1. Wakeup device from sleep. */
	printf("[info][refapp]calling ti_dev_exec_act(wake)\n");
	if ((ret_val = ti_dev_exec_action(dev_wakeup)) < 0)
		printf("[err][refapp]ti_dev_exec_act(wake) returns %d\n", ret_val);

	/* 2. Configure reports. */
	async_events = 0;
	dev_agnss = NEW_EPH_GPS_NATIVE | NEW_ALM_GPS_NATIVE;

	nmea_flags = SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) |
                                                        SET_BIT(gpgsv);
	msr_reports = 0;
	pos_reports = loc_exl_native;
	interval = 1;
	printf("[info][refapp]calling ti_dev_config_reports()\n");
	if ((ret_val = ti_dev_config_reports(async_events, dev_agnss, nmea_flags,
				msr_reports, pos_reports, interval)) < 0)
		printf("[err][refapp]ti_dev_config_reports() returns %d\n", ret_val);

	/* 3. Active action. */
	printf("[info][refapp]calling ti_dev_exec_act(active)\n");
	if ((ret_val = ti_dev_exec_action(dev_active)) < 0)
		printf("[err][refapp]ti_dev_exec_act(active) returns %d\n", ret_val);

}

static void refapp_gps_stop_uc(void)
{
	int ret_val = -1;

	/* 1. Deactivate action. */
	printf("[info][refapp]calling ti_dev_exec_act(deact)\n");
	if ((ret_val = ti_dev_exec_action(dev_de_act)) < 0)
		printf("[err][refapp]ti_dev_exec_act(deact) returns %d\n", ret_val);

	/* 2. Put device to sleep. */
	printf("[info][refapp]calling ti_dev_exec_act(sleep)\n");
	if ((ret_val = ti_dev_exec_action(dev_dsleep)) < 0)
		printf("[err][refapp]ti_dev_exec_act(sleep) returns %d\n", ret_val);
}


static void refapp_dev_deinit(void)
{
	/* 1. Release the device. */
	printf("[info][refapp]calling ti_dev_release()\n");
        ti_dev_release();

	/* 2. Release the proxy. */
	printf("[info][refapp]calling ti_dev_proxy_release()\n");
        ti_dev_proxy_release();
}

int main()
{
	refapp_dev_init();

	refapp_gps_start_uc_cold_start_auto();

	getchar();

	refapp_gps_stop_uc();

	getchar();

	refapp_dev_deinit();

	return 0;
}
