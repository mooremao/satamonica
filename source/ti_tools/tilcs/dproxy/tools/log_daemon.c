#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>


#define DLE 0x10
#define ETX 0x03

struct log_cfg {
  
        FILE *d2h_bin_fp;
        FILE *h2d_bin_fp;
        FILE *bin_ts_fp;
        int pty_fd;

};

#define OBJ_PTR(x) x[1]    

static struct log_cfg OBJ_PTR(daemon_log_cfg);


int daemon_init(char *path,char *time_stamp)
{
	/* Our buffer for the PATH NAMES OF H2D D2H and TIMESTAMP LOGS*/
	unsigned char buffer[1024];

	/* Name of H2D Log with the correct path*/
        sprintf(buffer, "%s-%s-%s", path, "H2D" , time_stamp);
        daemon_log_cfg->h2d_bin_fp = fopen(buffer, "rb");
        if(!daemon_log_cfg->h2d_bin_fp)
                return -1;

        /* Name of D2H Log with the correct path*/
        sprintf(buffer, "%s-%s-%s", path, "D2H" , time_stamp);
        daemon_log_cfg->d2h_bin_fp = fopen(buffer, "rb");
        if(!daemon_log_cfg->d2h_bin_fp)
                return -1;
        
	/* Name of TIMESTAMP Log with the correct path*/
        sprintf(buffer, "%s-%s-%s", path, "TIMESTAMP",time_stamp);
        daemon_log_cfg->bin_ts_fp = fopen(buffer, "r");
        if(!daemon_log_cfg->bin_ts_fp)
               	return -1;

	int  rc; 
	/* open the master side of psuedo-terminal*/
	daemon_log_cfg->pty_fd = open("/dev/ptmx",O_RDWR); 
	if (daemon_log_cfg->pty_fd < 0) 
		return -1; 
	
	rc = grantpt(daemon_log_cfg->pty_fd); 
	if (rc != 0) 
		return -1; 
	
	rc = unlockpt(daemon_log_cfg->pty_fd); 
	if (rc != 0) 
		return -1; 

	return 0;

}



int daemon_receive_msg(unsigned char *buffer)
{
        int rd_len = 0;
	fd_set rdset;
 
        FD_ZERO(&rdset);
	FD_SET(daemon_log_cfg->pty_fd, &rdset);
        if (select(FD_SETSIZE, &rdset, NULL, NULL, NULL) < 0)
                return -1;
        ioctl(daemon_log_cfg->pty_fd, FIONREAD, &rd_len);
	if ((rd_len = read(daemon_log_cfg->pty_fd, buffer,
                                             rd_len)) < 0)
		return -1;
        return rd_len;   
}   
 
int daemon_send_msg(unsigned char *buffer , int wr_len)
{
        int send_bytes = 0;
        while(1) {
        	if((send_bytes = write(daemon_log_cfg->pty_fd,
                                      buffer,wr_len))== wr_len) {
        		return 0;   
		}
        	else if (send_bytes < 0) {
   			return -1;
		}
                else {
                        buffer += send_bytes;
			wr_len -= send_bytes;
		}
	}

} 

unsigned int convert_str_time(struct timeval *time )
{
	
        struct tm tm;
        char  local_buffer[31],dir;
	       
	if(fread(local_buffer, sizeof(char) ,sizeof(local_buffer) , 
               	daemon_log_cfg->bin_ts_fp) != sizeof(local_buffer))
       		return -1;
	/*Get the direction from timestamp file*/
        dir = strstr(local_buffer , "H2D") ? 1 : 0;
	/*Get the timestamp and convert it into sec*/ 
        time->tv_usec = atoi(strtok(1 + strrchr(local_buffer , '-')," "));
       	strptime(strtok(local_buffer , " ") , 
                   "%Y-%m-%d-%H-%M-%S" ,&tm);   
	time->tv_sec = mktime(&tm);
 	return (unsigned int)dir;

}

unsigned int calculate_diff_time(struct timeval *pres_time ,
                                  struct timeval *prev_time)
{
	return  ((1000000 *(pres_time->tv_sec - prev_time->tv_sec)) +
                           (pres_time->tv_usec - prev_time->tv_usec)) ; 
		

}
        
int main(int argc ,char *argv[]) {
        
        /* Our process ID and Session ID */
        pid_t pid, sid;
    

        if(argc < 3)
        	printf("USAGE:PASS PATH AND TIMESTAMP OF LOGS \n");


        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) 
                exit(EXIT_FAILURE);
        

        /* If we got a good PID, then
           we can exit the parent process. */
        if (pid > 0) 
                exit(EXIT_SUCCESS);
       
              
        /* Change the file mode mask */
        umask(0);


 	/* Open any logs here */        
        if(daemon_init(argv[1],argv[2]))
		return -1; 


        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) 
                /* Log the failure */
                exit(EXIT_FAILURE);
       
                
        /* Change the current working directory */
        if ((chdir("/")) < 0) 
                /* Log the failure */
                exit(EXIT_FAILURE);
        
        
        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        
        /* Daemon-specific initialization goes here */
        
        /* The Big Loop */
        /* This part of code emulates the device .*/
        while (1) {
                static unsigned char local_buffer[4096],temp_buffer[4096];
               	int rd_len = 0 ;
        	struct timeval pres_time , prev_time;       
        		
		/* This part of code emulates the receiver part of the 
		   device and waits for the host to transmit the request 
                   messages.Once it receives the message, it compares with
		   logs. If both are matching it checks whether the next
		   message is sent by device or received by device ,if 
		   received by device ,it loops here*/

		   
                if (convert_str_time(&pres_time)) {
		
                	rd_len = daemon_receive_msg(temp_buffer);
			if(!rd_len)
				continue; 
                	if (fread((void *)local_buffer , sizeof(char) ,
                             		rd_len , daemon_log_cfg->h2d_bin_fp)
								  != rd_len)
				return -1;
                	if (strncmp(temp_buffer , local_buffer , rd_len))
				return -1;

		} else {
		
		/*This part of the code emulates the transmitter part of
		  the device and send the response from the logs with
		  proper delays*/
                
                	int wr_len = 0, time = 0;
                       			
                       	time=calculate_diff_time(&pres_time , &prev_time);
                        sleep(time / 1000000);
			usleep(time % 1000000); 
			while ((local_buffer[wr_len++] = 
                                fgetc(daemon_log_cfg->d2h_bin_fp)) != EOF) 
				if(local_buffer[wr_len - 2] == DLE &&
				   local_buffer[wr_len - 1] == ETX)
					break; 
			daemon_send_msg(local_buffer , wr_len);
				
                } 
                prev_time = pres_time;                
        }
        
   exit(EXIT_SUCCESS);
}


