ifeq ($(TARGET_ARCH),arm)

LOCAL_PATH             := $(call my-dir)

# Build devproxy
include $(CLEAR_VARS)
LOCAL_MODULE           := devproxy
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0 -Wall
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2 -Wall
endif
LOCAL_SHARED_LIBRARIES := libcutils liblog libclientlogger
LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                 \
    server/ai2.c                          \
    server/ai2_comm.c                     \
    server/ai2_log.c                      \
    server/config.c                       \
    server/clock_report.c                 \
    server/dev2host.c                     \
    server/host2dev.c                     \
    server/main.c                         \
    server/meas_ext_report.c              \
    server/msc_inject.c                   \
    server/msc_report.c                   \
    server/nmea_report.c                  \
    server/pos_report.c                   \
    server/seq_inject.c                   \
    server/seq_report.c                   \
    server/sequencer.c                    \
    server/ver_report.c                   \
    server/alm_report.c                   \
    server/eph_report.c                   \
    server/ion_report.c                   \
    server/utc_report.c                   \
    server/alm_status_report.c            \
    server/eph_status_report.c            \
    server/meas_status_report.c           \
    server/pos_status_report.c            \
    server/sv_dir_report.c                \
    server/sv_health_report.c             \
    server/log_report.c                   \
    server/nvs.c                          \
    server/nvs_mod_init.c                 \
    server/nvs_utils.c			  \
    server/assist_injection.c             \
    server/assist_operation.c             \
    server/nvs_inject.c                   \
    server/nvs_validate.c                 \
    server/msr_qualify.c              	  \
    server/calib.c            	  	  \
    server/apm.c	     	  	  \
    server/pa_blank.c         	  	  \
    server/glo_sv2slot_map.c           	  \
    server/glo_sv_slot_map_report.c 	  \
    server/def_qop.c   			  \
    server/cust_config.c                  \
    server/heading_filter.c               \
    common/logger.c                       \
    common/rpc.c                          \
    common/os_services.c             \
	server/Cw_test_report.c  \
	server/error_handler.c 

LOCAL_C_INCLUDES       :=                 \
    $(LOCAL_PATH)/include                 \
    $(LOCAL_PATH)/../include/common       \
    $(LOCAL_PATH)/../include/dproxy       \
    $(LOCAL_PATH)/../infrastructure/log/include       \
    $(LOCAL_PATH)/server
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_EXECUTABLE)


# Build libdevproxy.so
include $(CLEAR_VARS)
LOCAL_MODULE           := libdevproxy
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2
endif
LOCAL_SHARED_LIBRARIES := libcutils liblog libclientlogger
LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                 \
    client/dev_proxy.c                    \
    common/logger.c                       \
    common/rpc.c                          \
    common/os_services.c
LOCAL_C_INCLUDES       :=                 \
    $(LOCAL_PATH)/include                 \
    $(LOCAL_PATH)/../include/common       \
    $(LOCAL_PATH)/../include/dproxy       \
	$(LOCAL_PATH)/../infrastructure/log/include       \
    $(LOCAL_PATH)/server                  \
	hardware/libhardware/include/hardware \
    bionic/libc/include                   \
    bionic/libc/private
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_SHARED_LIBRARY)


# Build client_app
include $(CLEAR_VARS)
LOCAL_MODULE           := client_app
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2
endif
LOCAL_SHARED_LIBRARIES := libcutils liblog libclientlogger
LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                 \
    client/dev_proxy.c                    \
    common/logger.c                       \
    common/rpc.c                          \
    common/os_services.c                  \
    test/client_app.c
LOCAL_C_INCLUDES       :=                 \
    $(LOCAL_PATH)/include                 \
    $(LOCAL_PATH)/../include/common       \
    $(LOCAL_PATH)/../include/dproxy       \
    $(LOCAL_PATH)/../infrastructure/log/include       \
    $(LOCAL_PATH)/server
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_EXECUTABLE)

endif
