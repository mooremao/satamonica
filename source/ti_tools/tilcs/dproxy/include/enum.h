/*
 * enum.h
 *
 * {module description}
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

/** Dillution of precision         */
dev_dop_info


/** Device location ellipse (orientation) uncertainty information
  @note Tend to match with section 5.3 of 3GPP specification 23.032
 */
dev_ellp_unc

/** UTC information */
dev_utc_info


/** Satellite used for position fix */
dev_sat_info

/** Device position information in native format */
dev_pos_info

/** Device heading information */
dev_hdg_info

dev_vel_info

dev_msr_info

/** Device GPS Time information (native format) */
dev_gps_time

dev_glo_time

dev_version

/** @defgroup msrpos_group The MSR-POS information
    Defines and identifies various flavors of measurement & position reports 
    
    @warning GLONASS Measurement report is not supported by the GNSS device
    
    @{
*/
enum msr_pos_id {

        gps_msr_native, /**< Native & device specific GPS measurement report */
        glo_msr_native, /**< Native & device specific GLONASS measurements   */
        gps_msr_ie,     /**< 3GPP specified GPS measurement information (IE) */
        glo_msr_ie,     /**< 3GPP specified GLONASS measurement IE           */

        loc_native,     /**< Native & device specific position report   */
        gps_loc_ie,     /**< 3GPP specified GPS location information IE */
        glo_loc_ie,     /**< 3GPP specified GLONASS location IE         */   

        vel_native,
        gps_vel_ie,
        glo_vel_ie,

        gps_tim_native,
        glo_tim_native,
        gps_tim_ie,
        glo_tim_ie
};

enum loc_err_id { 
        
        undefined              =  0,
        not_enough_gps_sv      =  2,
        no_gps_assist_data     =  6,
        not_enough_ganss_sv    = 11,
        no_ganss_assist_data   = 12
        
        /* Need to add more */
};

enum nmea_sn_id {

        /** GPS specific sentences */
        gpgga,
        gpgll,
        gpgsa,
        gpgsv,
        gprmc,
        gpvtg,
		gpgrs,
		gpgst,

        /** GLONASS specific sentences; not supported as of now */
        glgga,
        glgll,
        glgsa,
        glgsv,
        glrmc,
        glvtg,

		qzgsv,

		wsgsv,
		/*SRK:Added for residuals*/
		gngrs,
		gngll,
		gngns,
		gngsa,
		gnvtg
};

/** Various user application specific updates */
enum app_rpt_id {
        
        app_pos_old, /**< There is no new position to report                     */
        app_blf_out, /**< Buffered location fixes now available; no data content */
        app_gfc_evt  /**< Geo-fencing trigger @see struct geofence_status        */
};


struct ti_dev_proxy_params {
        
        unsigned int fn_call_timeout_ms; /* Timeout for communication w/ dev */
        
};


