/*
 * logger.h
 *
 * Logging procedure prototypes
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _DPROXY_LOGGER_
#define _DPROXY_LOGGER_

/** @file ai2.h
    This file describes the interface for logging.
    This interface is undefined in release mode builds.

    @note Do NOT include any code in the logging macro that can affect the rest
    of the code. Logging macro WILL NOT be available in release mode.
*/

#define LOG_BUFFER_SIZE 1024

enum mod_id {

	generic,				/*  use "none" if printing generic and debug logs */
		
	d2h,				/*  Device2Host  */
	h2d,				/*  Host2Device  */
	seq,				/*  Sequence related logs */
	api,				/*  Device Proxy API logs */
	rpc,				/*  RPC logs */ 
	os,					/*  OS logs */ 
	ai2,				/*  AI2 parsing/Constructing logs */ 
	nvs,				/*  NVS logs */
	cfg,				/*  All configuration related logs*/   
	ver,				/*  Version logs */   
	async_evnt,			/*  All ASYNC events */
	rx_err,				/*  Device Errors */
	rx_off,				/*  Device OFF */
	rx_on,				/*  Device ON */
	rx_idle,			/*  Device IDLE */
	rx_wake,			/*  Device Wake up */
	del_ast,			/*  Delete Assistance */
	qop_parm,			/*  QOP logs */
	patch,				/*  Patch download logs */
	msr_flt,			/*  MSR Filtering logs */
	freq_est,			/*  Freq Estimate injection */
	tim_unc,			/*  Time Unc injection */
	tim_inj,			/*  Time Injection logs */
	pos_inj,			/*  POS Injection logs */
	eph_inj,			/*  EPH Injection logs */
	alm_inj,			/*  ALM Injection logs */
	acq_inj,			/*  ACQ Injection logs */
	svdir_inj,			/*  SVDIR Injection logs */
	hlt_inj,			/*  Health Injection logs */
	ion_inj,			/*  ION Injection logs */
	utc_inj,			/*  UTC  Injection logs */
	rti_inj,			/*  RTI Injection logs */
	dgps_inj,			/*  DGPS Injection logs */
	tim_rpt,			/*  Clock report logs */
    eph_rpt,			/*  EPH report logs */
    alm_rpt,			/*  ALM report logs */
    pos_rpt,			/*  POS report logs */
    msr_rpt,			/*  MSR report logs */
	hlt_rpt,			/*  Health report logs */
	ion_rpt,			/*  ION report logs */
	utc_rpt,			/*  UTC report logs */
	svdir_rpt			/*  SVDIR report logs */

	/* add here */
};


enum log_cat {
    info,
	warn,
	err,
	critl
};


#define LOGPRINT(mod_id, log_cat, ...) \
		logprint(mod_id, log_cat, __FILE__, __LINE__, __VA_ARGS__);


void logprint(enum mod_id mod_id, enum log_cat log_cat, const char *file,
	const int line, const char *fmt, ...);

void update_logmask(unsigned int logging_mask);

#endif

