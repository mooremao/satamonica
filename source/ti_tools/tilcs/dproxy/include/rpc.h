/*
 * rpc.h
 *
 * RPC procedure protoypes
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef __RPC_H__
#define __RPC_H__

#include<stdio.h>


/*------------------------------------------------------------------------------
 * Operation ID - 4 bit construct
 *------------------------------------------------------------------------------
 */
/* Standard definition 0x0 to 0x7 */
#define OPER_NOP         (0x0)   /* No operation or information  */
#define OPER_SET         (0x1)   /* Set entity to this value     */
#define OPER_GET         (0x2)   /* Get this value from entity   */
#define OPER_ADD         (0x3)   /* Add this value to entity     */
#define OPER_DEL         (0x4)   /* Delete specified  entity     */
#define OPER_INF         (0x5)   /* Information but no operation */

/* Custom defintion 0x0 to 0xF    */


/*------------------------------------------------------------------------------
 * Information Element ID to be used for RPC
 *
 * RPC ELEMENT IDENTITY       --> REI
 * GLONASS                    --> GLO
 * Global Positionign system  --> GPS
 * GANSS                      --> GAN
 * GNSS                       --> GNS
 *------------------------------------------------------------------------------
 */

/*============================================================================*/
/* Device specific RPC Element Indentifiers                                   */
/*============================================================================*/
#define RPC_DEV_ELEM_BASE       (0x0000)
#define RPC_DEV_ELEM_ID(x)      (RPC_DEV_ELEM_BASE + x)
#define REI_RDEV                 RPC_DEV_ELEM_ID

#define REI_NOTDEF              REI_RDEV(0x000)

#define REI_DEV_CONNECT         REI_RDEV(0x001)
#define REI_DEV_RELEASE         REI_RDEV(0x002)
#define REI_DEV_CFG_RPT         REI_RDEV(0x003) /* Config Reports from dev    */
#define REI_DEV_NAV_SEL         REI_RDEV(0x004) /* Select GPS, GLO            */
#define REI_DEV_VERSION         REI_RDEV(0x005) /* Device version             */
#define REI_DEV_WSTATUS         REI_RDEV(0x006) /* Work status                */
#define REI_DEV_EVT_INF         REI_RDEV(0x007)
#define REI_DEV_ERR_INF         REI_RDEV(0x008)
#define REI_DEV_CAPS            REI_RDEV(0x009)

#define REI_SRV_CFG_INF         REI_RDEV(0x010)

#define REI_DEV_PLT				REI_RDEV(0x011)
/*GPS Status info*/
#define REI_DEV_STATUS			REI_RDEV(0x012)
#define REI_DEV_ACTIVE          REI_RDEV(0x020)
#define REI_DEV_DE_ACT          REI_RDEV(0x021)
#define REI_DEV_DSLEEP          REI_RDEV(0x022)
#define REI_DEV_WAKEUP          REI_RDEV(0x023)

#define REI_TS_PULSE_INFO       REI_RDEV(0x060)
#define REI_DEV_REF_CLOCK       REI_RDEV(0x061)
#define REI_CLK_CALIBRATE       REI_RDEV(0x062)
#define REI_AREA_GEOFENCE       REI_RDEV(0x063)
#define REI_BUF_LOC_FIXES       REI_RDEV(0x064)
#define REI_QOP_SPECIFIER       REI_RDEV(0x065)
#define REI_MAXIMIZE_PDOP       REI_RDEV(0x066)
#define REI_APM_CTRL		    REI_RDEV(0x067)
#define REI_PA_BLANK		    REI_RDEV(0x068)
/* add for GNSS recovery ++*/
#define REI_ERR_RECOVERY	    REI_RDEV(0x070)
/* error trigger test code, to be removed */
#define REI_FATAL_ERR	    	REI_RDEV(0x071)
#define REI_EXCEPTION_ERR	    REI_RDEV(0x072)
/* add for GNSS recovery --*/
#define REI_MEAS_REP_RECOVERY	REI_RDEV(0x073)
#define REI_NO_RESP_RECOVERY 	REI_RDEV(0x074)
#define REI_HARD_RESET_RECOVERY REI_RDEV(0x075)


#define REI_DEV_OLD_POS         REI_RDEV(0x080)
#define REI_DEV_BUF_LOC         REI_RDEV(0x081)
#define REI_DEV_GFC_INF         REI_RDEV(0x082)

#define REI_DEV_EXL_POS         REI_RDEV(0x200)
#define REI_DEV_SAT_INF         REI_RDEV(0x201)
#define REI_DEV_DOP_INF         REI_RDEV(0x202)
#define REI_DEV_VEL_INF         REI_RDEV(0x203)
#define REI_DEV_HDG_INF         REI_RDEV(0x204)
#define REI_DEV_GPS_TIM         REI_RDEV(0x205)
#define REI_DEV_GLO_TIM         REI_RDEV(0x206)

#define REI_DEV_MSR_GPS         REI_RDEV(0x210)
#define REI_DEV_MSR_GLO         REI_RDEV(0x211)
#define REI_DEV_MSR_STAT        REI_RDEV(0x212)
#define REI_DEV_EPH_STAT        REI_RDEV(0x213)
#define REI_DEV_ALM_STAT        REI_RDEV(0x214)
#define REI_DEV_POS_STAT        REI_RDEV(0x215)
#define REI_DEV_SV_HLT          REI_RDEV(0x216)
#define REI_DEV_SV_DIR          REI_RDEV(0x217)
#define REI_DEV_PLT_RES			REI_RDEV(0x218)

#define REI_DEV_GPS_EPH         REI_RDEV(0x300)
#define REI_DEV_GPS_ALM         REI_RDEV(0x301)
#define REI_DEV_GPS_UTC         REI_RDEV(0x302)
#define REI_DEV_GPS_ION         REI_RDEV(0x303)
#define REI_DEV_GPS_POS         REI_RDEV(0x304)
#define REI_DEV_GPS_ACQ         REI_RDEV(0x305)
#define REI_DEV_GPS_RTI         REI_RDEV(0x306)
#define REI_DEV_GPS_DGP         REI_RDEV(0x307)
#define REI_DEV_QZS_ALM         REI_RDEV(0x308)

#define REI_DEV_GLO_EPH         REI_RDEV(0x400)
#define REI_DEV_GLO_ALM         REI_RDEV(0x401)
#define REI_DEV_GLO_UTC         REI_RDEV(0x402)
#define REI_DEV_GLO_ION         REI_RDEV(0x403)
#define REI_DEV_GLO_POS         REI_RDEV(0x404)
#define REI_DEV_GLO_ACQ         REI_RDEV(0x405)
#define REI_DEV_GLO_RTI         REI_RDEV(0x406)
#define REI_DEV_GLO_DGP         REI_RDEV(0x407)

#define REI_DEV_ELEM_CEIL       REI_RDEV(0xFFF) /* Do not remove */


#define RPC_GAN_3GPP_BASE       (0x2000)
#define RPC_GAN_3GPP_ID(x)      (RPC_GAN_3GPP_BASE + x)
#define REI_3GPP                RPC_GAN_3GPP_ID

#define REI_LOC_GPS             REI_3GPP(0x000)
#define REI_LOC_GAN             REI_3GPP(0x002)
#define REI_LOC_EVT             REI_3GPP(0x00F)

#define REI_VEL_GNS             REI_3GPP(0x010)

#define REI_TIM_GPS             REI_3GPP(0x020)
#define REI_TIM_GLO             REI_3GPP(0x021)
#define REI_TIM_GAN             REI_3GPP(0x02F)

#define REI_MSR_GPS             REI_3GPP(0x030)
#define REI_MSR_GLO_SAT         REI_3GPP(0x031)
#define REI_MSR_GLO_SG1         REI_3GPP(0x032)
#define REI_MSR_GLO_SG2         REI_3GPP(0x033)
#define REI_MSR_RSLT			REI_3GPP(0x034)
#define REI_MSR_GANSS             REI_3GPP(0x035)

#define REI_GPS_EPH             REI_3GPP(0x100)
#define REI_GPS_ALM             REI_3GPP(0x101)
#define REI_GPS_UTC             REI_3GPP(0x102)
#define REI_GPS_ION             REI_3GPP(0x103)
#define REI_GPS_TIM             REI_3GPP(0x104)
#define REI_GPS_ACQ             REI_3GPP(0x105)
#define REI_GPS_RTI             REI_3GPP(0x106)
#define REI_GPS_DGP             REI_3GPP(0x107)
#define REI_GPS_HLT             REI_3GPP(0x108)
#define REI_GPS_DIR             REI_3GPP(0x109)
#define REI_GPS_REF_TIM         REI_3GPP(0x10A)

#define REI_REF_POS             REI_3GPP(0x1FF)

#define REI_GLO_EPH             REI_3GPP(0x200)
#define REI_GLO_ALM             REI_3GPP(0x201)
#define REI_GLO_UTC             REI_3GPP(0x202)
#define REI_GLO_ION             REI_3GPP(0x203)
#define REI_GLO_TIM             REI_3GPP(0x204)
#define REI_GLO_ACQ				REI_3GPP(0x205)
#define REI_GLO_RTI             REI_3GPP(0x206)
#define REI_GLO_DGP		        REI_3GPP(0x207)
#define REI_GLO_HLT             REI_3GPP(0x209)
#define REI_GLO_DIR             REI_3GPP(0x20A)
#define REI_GLO_REF_TIM         REI_3GPP(0x20B)
#define REI_GPS_EPH_AID_STAT             REI_3GPP(0x300)
#define REI_GPS_ALM_AID_STAT             REI_3GPP(0x301)
#define REI_GPS_UTC_AID_STAT             REI_3GPP(0x302)
#define REI_GPS_ION_AID_STAT             REI_3GPP(0x303)
#define REI_GPS_TIM_AID_STAT             REI_3GPP(0x304)
#define REI_GPS_ACQ_AID_STAT             REI_3GPP(0x305)
#define REI_GPS_RTI_AID_STAT             REI_3GPP(0x306)
#define REI_GPS_DGP_AID_STAT             REI_3GPP(0x307)
#define REI_GPS_HLT_AID_STAT             REI_3GPP(0x308)
#define REI_GPS_DIR_AID_STAT             REI_3GPP(0x309)
#define REI_GPS_REF_TIM_AID_STAT         REI_3GPP(0x30A)

#define REI_REF_POS_AID_STAT             REI_3GPP(0x3FF)

#define REI_GLO_EPH_AID_STAT             REI_3GPP(0x400)
#define REI_GLO_ALM_AID_STAT             REI_3GPP(0x401)
#define REI_GLO_UTC_AID_STAT             REI_3GPP(0x402)
#define REI_GLO_ION_AID_STAT             REI_3GPP(0x403)
#define REI_GLO_TIM_AID_STAT             REI_3GPP(0x404)
#define REI_GLO_ACQ_G1_AID_STAT          REI_3GPP(0x405)
#define REI_GLO_ACQ_G2_AID_STAT          REI_3GPP(0x406)
#define REI_GLO_RTI_AID_STAT             REI_3GPP(0x407)
#define REI_GLO_DGC_G1_AID_STAT          REI_3GPP(0x408)
#define REI_GLO_DGC_G2_AID_STAT          REI_3GPP(0x409)
#define REI_GLO_HLT_AID_STAT             REI_3GPP(0x40A)
#define REI_GLO_DIR_AID_STAT             REI_3GPP(0x40B)
#define REI_GLO_REF_TIM_AID_STAT         REI_3GPP(0x40C)

#define RPC_GAN_3GPP_CEIL       REI_3GPP(0xFFF)

#define RPC_NMEA_MSG_BASE       (0x4000)
#define RPC_NMEA_ELEM_ID(x)     (RPC_NMEA_MSG_BASE + x)
#define REI_NMEA                RPC_NMEA_ELEM_ID

#define REI_NMEA_GPGGA          REI_NMEA(0x000)
#define REI_NMEA_GPGLL          REI_NMEA(0x001)
#define REI_NMEA_GPGSA          REI_NMEA(0x002)
#define REI_NMEA_GPGSV          REI_NMEA(0x003)
#define REI_NMEA_GPRMC          REI_NMEA(0x004)
#define REI_NMEA_GPVTG          REI_NMEA(0x005)
#define REI_NMEA_GPGRS          REI_NMEA(0x006)
#define REI_NMEA_GPGST          REI_NMEA(0x007)

#define REI_NMEA_GLGGA          REI_NMEA(0x100)
#define REI_NMEA_GLGLL          REI_NMEA(0x101)
#define REI_NMEA_GLGSA          REI_NMEA(0x102)
#define REI_NMEA_GLGSV          REI_NMEA(0x103)
#define REI_NMEA_GLRMC          REI_NMEA(0x104)
#define REI_NMEA_GLVTG          REI_NMEA(0x105)
#define REI_NMEA_GNGRS          REI_NMEA(0x200)
#define REI_NMEA_GNGLL			REI_NMEA(0x201)
#define REI_NMEA_GNVTG			REI_NMEA(0x202)
#define REI_NMEA_GNGSA			REI_NMEA(0x203)
#define REI_NMEA_GNGNS			REI_NMEA(0x204)
#define REI_NMEA_QZGSV			REI_NMEA(0x300)
#define REI_NMEA_WSGSV			REI_NMEA(0x400)

#define RPC_NMEA_MSG_OVER       REI_NMEA(0xFFF)


/* Not compiled, draft 0.2 */

/* Do not change the order of following structures. They are directly transacted
   over the RPC and changes without diligence will break the functionality.
*/

enum rpc_xact {

	rpc_ack,    /* Acknowledgement, both ways   */
	rpc_cmd,    /* Command, client to server    */
	rpc_req,    /* Request, client to server    */
	rpc_rsp,    /* Response, server to client   */
	rpc_ind,    /* Indication, server to client */
	rpc_err     /* Error, both ways             */

};

/* Information element header */
struct ie_head {

        unsigned int    ct_word;
        unsigned short  elem_id;
        unsigned short  tot_len;
};

/* Information Element */
struct ie_desc {

        struct ie_head  head;
        void           *data;

        /* Not part of the RPC message */
        unsigned short  objs;   /* Num of objects in data content */
};

#define IE_HEADER(desc) ((struct ie_head*) desc)
#define IE_HDR_SZ       (sizeof(struct ie_head))

#define MAX_IE 12

/* RPC Message header */
struct rm_head {

        unsigned short  length;
        unsigned char   num_ie;

};

/* RPC Message */
struct rpc_msg {

        struct rm_head  rmsg_head;          /* RPC Message Header  */
        struct ie_desc  info_elem[MAX_IE];  /* Information element */
};

#define RM_HEADER(rmsg) ((struct rm_head*) rmsg)
#define RM_HDR_SZ       (sizeof(struct rm_head))

/* RPC memory management */
void rpc_msg_free(struct rpc_msg *rmsg);
struct rpc_msg *rpc_msg_alloc(void);


struct rpc_msg *rpc_cli_recv(void);
struct rpc_msg *rpc_svr_recv(void);

struct ie_desc* rpc_ie_attach(struct rpc_msg *rmsg,  unsigned short elem_id,
                              unsigned char oper_id, unsigned short tot_len,
                              void *data);
void rpc_ie_freeup(struct rpc_msg *rmsg, struct ie_desc *ie_adu);
void rpc_ie_detach(struct rpc_msg *rmsg, struct ie_desc *ie_adu);

int rpc_svr_send(struct rpc_msg *rmsg);

int rpc_send_ack(unsigned short oper_id, unsigned int xact_id);
int rpc_send_err(unsigned short oper_id, unsigned int xact_id);

int rpc_cli_send(struct rpc_msg *rmsg);
int rpc_cli_read(struct rpc_msg *rmsg_i, struct rpc_msg *rmsg_o);

int rpc_cli_init(char *rd_fifo, char *wr_fifo);
int rpc_cli_exit(void);

int rpc_svr_init(char *rd_fifo, char *wr_fifo);
int rpc_svr_exit(void);

#define CTW_2_OPER_ID(ct_word) ((ct_word & 0x0F000000) >> 24)
#define CTW_2_XACT_ID(ct_word) ((ct_word & 0x0007FFFF))
#define CTW_2_XACT_TY(ct_word) ((ct_word & 0xC0000000) >> 30)
#define OPER_ID_2_CTW(oper_id) ((oper_id & 0xF) << 24)

#define IS_CT_WORD_ACK(ct_word) ((((ct_word & 0xE0000000) >> 30) == 0) ? 1 : 0)
#define IS_CT_WORD_CMD(ct_word) ((((ct_word & 0xE0000000) >> 29) == 3) ? 1 : 0)
#define IS_CT_WORD_REQ(ct_word) ((((ct_word & 0xE0000000) >> 29) == 5) ? 1 : 0)
#define IS_CT_WORD_RSP(ct_word) ((((ct_word & 0xE0000000) >> 29) == 4) ? 1 : 0)
#define IS_CT_WORD_IND(ct_word) ((((ct_word & 0xE0000000) >> 29) == 2) ? 1 : 0)
#define IS_CT_WORD_ERR(ct_word) ((((ct_word & 0xE0000000) >> 30) == 3) ? 1 : 0)

#endif	/* __RPC_CLIENT_H__ */

