#ifndef __VERSION_H__
#define __VERSION_H__

#define MAJOR 8        /** MM: major version; 1 byte                   */
#define MINOR 5        /** mm: minor version; 1 byte                   */
#define PATCH 0        /** pp: bug-fix level; 2 bytes                  */
#define BUILD 29      /** bb: iterate in lifetime of package; 2 bytes */

#define EXTEN 'a'      /** a character extension to ver; 1 byte        */

/** Quality: alpha<n>, beta<n>......; 12 bytes  */
#define QFIER {'a', 'l', 'p', 'h', 'a', '1', '\0'}

/** Date of proxy-server compilation */
#define DAY   1  /** dd format; 1 byte                           */
#define MONTH 6  /** mm format, 1 to 12; 1 byte                  */
#define YEAR  2015     /** yyyy format; 2 bytes                        */

#endif /* __VERSION_H__ */
