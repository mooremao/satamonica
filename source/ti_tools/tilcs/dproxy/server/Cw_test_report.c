/*
 * cw_test_report.c
 *
 * CW test report 
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "log_report.h"
#include "gnss.h"


#define RESPTYP_WIDEBAND_SNR 	0x04
#define RESPTYP_NARROWBAND_SNR 	0x05
#define RESPTYP_TCXO_OFFSET		0x06
#define RESPTYP_NOISE_FIGURE 	0x07



/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int setup_cw_test_rep(struct dev_cw_test_report *dev_cw_test_rep, struct cw_test_report *cw_test_rep)
{
	int i;

	if(dev_cw_test_rep->rep_type == cw_test_rept){	
		cw_test_rep->rep_type = cw_test_rept;
		cw_test_rep->resp_type = (unsigned char)dev_cw_test_rep->response_type;
		cw_test_rep->tot_packts = dev_cw_test_rep->total_packts;
		cw_test_rep->pack_num = dev_cw_test_rep->pack_num;

		if(dev_cw_test_rep->response_type == RESPTYP_WIDEBAND_SNR)
		{
			cw_test_rep->wb_snr.peaks = dev_cw_test_rep->wb_snr.peaks;
			for(i = 0; i< cw_test_rep->wb_snr.peaks ; i++){
				cw_test_rep->wb_snr.wb_peak_index[i] = dev_cw_test_rep->wb_snr.wb_peak_index[i];
				cw_test_rep->wb_snr.wb_peak_SNR[i] = (float)(dev_cw_test_rep->wb_snr.wb_peak_SNR[i] * 0.1f);
				LOGPRINT(ai2,info,"RESPTYP_WIDEBAND_SNR Peak Index =%d\t Peak SNR =%d\n", dev_cw_test_rep->wb_snr.wb_peak_index[i], dev_cw_test_rep->wb_snr.wb_peak_SNR[i]);
				LOGPRINT(ai2,info,"Peak Index =%d\t Peak SNR =%f\n", cw_test_rep->wb_snr.wb_peak_index[i], cw_test_rep->wb_snr.wb_peak_SNR[i]);
			}			
			
		}
		else if(dev_cw_test_rep->response_type == RESPTYP_NARROWBAND_SNR)
			{
				cw_test_rep->nb_snr.peaks = dev_cw_test_rep->nb_snr.peaks;
				cw_test_rep->nb_snr.c_freq = dev_cw_test_rep->nb_snr.center_freq;
				for(i = 0; i< cw_test_rep->nb_snr.peaks ; i++){
					cw_test_rep->nb_snr.nb_peak_index[i] = dev_cw_test_rep->nb_snr.nb_peak_index[i];
					cw_test_rep->nb_snr.nb_peak_SNR[i] = (float)(dev_cw_test_rep->nb_snr.nb_peak_SNR[i] * 0.1f);
					LOGPRINT(ai2, info, "Peak Index =%d\t Peak SNR =%f\n", cw_test_rep->nb_snr.nb_peak_index[i], cw_test_rep->nb_snr.nb_peak_SNR[i]);
				}			
			}
		else if(dev_cw_test_rep->response_type == RESPTYP_TCXO_OFFSET)
			{
				cw_test_rep->tcxo_offset = (float)(dev_cw_test_rep->tcxo_offset * 1/pow(2,16));
				LOGPRINT(ai2, info, "setup_cw_test_rep RESPTYP_TCXO_OFFSET %f", cw_test_rep->tcxo_offset );

				
			}
		else if(dev_cw_test_rep->response_type == RESPTYP_NOISE_FIGURE)
			{
				cw_test_rep->noise_figure = (float)(dev_cw_test_rep->noise_figure * 0.1f);
				
			}
	}
	if(dev_cw_test_rep->rep_type == sigacq_test_rep){
				cw_test_rep->rep_type = sigacq_test_rep;
		cw_test_rep->svs_acquired = dev_cw_test_rep->svs_acquired;
		for(i = 0; i< dev_cw_test_rep->svs_acquired; i++)
		{
			cw_test_rep->prn[i] = dev_cw_test_rep->prn[i];
			cw_test_rep->cno[i] = dev_cw_test_rep->cno[i] * 0.1f;
		}
		
	}	
	return 0;

}

static struct ie_desc *mk_ie_adu_cw_test_rep(struct rpc_msg *rmsg,
			unsigned short oper_id, struct dev_cw_test_report *dev_cw_test_rep)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(cw_test_report);
	struct cw_test_report *new_rep = (struct cw_test_report*)malloc(st_size);
	if(!new_rep)
		goto exit1;

	memset(new_rep, 0x0, st_size);
	setup_cw_test_rep(dev_cw_test_rep, new_rep );

	//LOGPRINT(ai2, info, "mk_ie_adu_cw_test_rep  %f", new_rep->tcxo_offset );

	ie_adu = rpc_ie_attach(rmsg, oper_id, OPER_INF, st_size, new_rep);
	if(!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_rep);

exit1:
	return NULL;
}


/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int cw_test_rep_create_ie(struct rpc_msg *rmsg, unsigned short oper_id,
						struct dev_cw_test_report *dev_cw_test_rep)
{
		struct ie_desc *ie_adu[2];
		int n_adu = 0;
		LOGPRINT(ai2, info, "cw_test_rpc_ie Entering");

		ie_adu[n_adu] = mk_ie_adu_cw_test_rep(rmsg, oper_id, dev_cw_test_rep);
		if(!ie_adu[n_adu++])
			goto exit1;

			LOGPRINT(ai2, info, "cw_test_rpc_ie Exiting");
		return n_adu;
	
	exit1:
		while(--n_adu) {
			rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
		}
	
		return -1;

}

static int cw_test_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
							unsigned int rei_select)
{
	struct dev_cw_test_report *dev_cw_test_rep= (struct dev_version *)ai2_d2h->ai2_struct;

	UNUSED(rei_select);
	LOGPRINT(ai2, info, "cw_test_rpc_ie Entering");

	if(-1 == cw_test_rep_create_ie(rmsg, REI_DEV_PLT_RES, dev_cw_test_rep))
		return -1;

	return 0;
}

static int dev_cw_test_report_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset, unsigned char pkt_id)
{
	struct dev_cw_test_report *dev_cw_test_rep  = (struct dev_cw_test_report *)ai2_d2h->ai2_struct;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *logai2_pkt				 = (char*) ai2_d2h->ai2_packet + ai2_offset;
	int i;	

	char  *ai2_ref               = ai2_pkt;
	int idx = 0;
	LOGPRINT(ai2, info, "dev_cw_test_report_struct Entering %x", pkt_id);


	if(pkt_id == 0x15){	
		dev_cw_test_rep->rep_type = cw_test_rept;
		dev_cw_test_rep->response_type = BUF_LE_U1B_RD_INC(ai2_pkt);

		dev_cw_test_rep->total_packts = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_cw_test_rep->pack_num = BUF_LE_U2B_RD_INC(ai2_pkt);

		switch(dev_cw_test_rep->response_type)
		{
			case RESPTYP_WIDEBAND_SNR:
				{
					dev_cw_test_rep->wb_snr.peaks = BUF_LE_U1B_RD_INC(ai2_pkt);
					printf("WB Peak = %d\n", dev_cw_test_rep->wb_snr.peaks);
					for(idx = 0; idx < dev_cw_test_rep->wb_snr.peaks; idx++){
						dev_cw_test_rep->wb_snr.wb_peak_index[idx] = BUF_LE_S2B_RD_INC(ai2_pkt);
						dev_cw_test_rep->wb_snr.wb_peak_SNR[idx] = BUF_LE_U2B_RD_INC(ai2_pkt);
						LOGPRINT(ai2,info,"RESPTYP_WIDEBAND_SNR Peak Index =%d\t Peak SNR =%d\n", dev_cw_test_rep->wb_snr.wb_peak_index[idx], dev_cw_test_rep->wb_snr.wb_peak_SNR[idx]);
					}
				}
			break;
			case RESPTYP_NARROWBAND_SNR:
				{
					dev_cw_test_rep->nb_snr.peaks = BUF_LE_U1B_RD_INC(ai2_pkt);
					dev_cw_test_rep->nb_snr.center_freq= BUF_LE_S4B_RD_INC(ai2_pkt);
					for(idx = 0 ; idx < dev_cw_test_rep->nb_snr.peaks; idx++){
						dev_cw_test_rep->nb_snr.nb_peak_index[idx] = BUF_LE_S2B_RD_INC(ai2_pkt);
						dev_cw_test_rep->nb_snr.nb_peak_SNR[idx] = BUF_LE_U2B_RD_INC(ai2_pkt);
					}
				}
			break;
			case RESPTYP_TCXO_OFFSET:
				{

					dev_cw_test_rep->tcxo_offset = BUF_LE_S4B_RD_INC(ai2_pkt);
					
					LOGPRINT(ai2, info, "dev_cw_test_report_struct RESPTYP_TCXO_OFFSET %d", dev_cw_test_rep->tcxo_offset);
				}
			break;
			case RESPTYP_NOISE_FIGURE:
				{
					dev_cw_test_rep->noise_figure = BUF_LE_U2B_RD_INC(ai2_pkt);
				}
			break;
			default:
			break;
		}
	}
	if(pkt_id == 0xCA){
		dev_cw_test_rep->rep_type = sigacq_test_rep;

		dev_cw_test_rep->svs_acquired = BUF_LE_U1B_RD_INC(ai2_pkt);
		LOGPRINT(ai2, info, "dev_cw_test_report_struct Signal Acq Test %d",dev_cw_test_rep->svs_acquired);
		
		for(idx = 0; idx< dev_cw_test_rep->svs_acquired; idx++)
		{
			dev_cw_test_rep->prn[idx] = BUF_LE_U1B_RD_INC(ai2_pkt);
			dev_cw_test_rep->cno[idx] =  BUF_LE_U2B_RD_INC(ai2_pkt);
		}
		
	}

	print_data(REI_DEV_PLT_RES, dev_cw_test_rep);

	return (ai2_pkt - ai2_ref);
}

static int cw_test_ext_struct(struct d2h_ai2 *ai2_d2h)
{
//	int ai2_offset = 3; //skip the packet ID and length field

	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	char  *ai2_pkt				 = (char*) ai2_d2h->ai2_packet ;
	unsigned char pkt_id;	

	pkt_id = BUF_LE_U1B_RD_INC(ai2_pkt);
	int ai2_offset = 3;
	
	LOGPRINT(ai2, info, "cw_test_ext_struct Entering %d pkt_id %x",ai2_pkt_sz, pkt_id );
	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);


	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_cw_test_report_struct(ai2_d2h, ai2_offset, pkt_id);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

#define EXT_PVT_ST_LEN ST_SIZE(dev_cw_test_report)

struct d2h_ai2 d2h_dev_cw_test_report= {

	.ai2_st_len = EXT_PVT_ST_LEN,
	.gen_struct = cw_test_ext_struct,
	.do_ie_4rpc = cw_test_rpc_ie
};

