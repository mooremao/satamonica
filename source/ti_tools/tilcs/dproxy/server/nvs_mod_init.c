#include "nvs_mod_init.h"
#include "log_report.h"
#include "logger.h"
#include "rpc.h"
#include "utils.h"
#include "glo_sv_slot_map_report.h"

void nvs_display_all(void)
{
	static struct dev_gps_alm gps_alm[32];
	static struct dev_gps_eph gps_eph[32];
	static struct dev_gps_time gps_clk[1];
	static struct dev_pos_info gps_pos[1];
	static struct dev_gps_ion gps_ion[1];
	static struct dev_glo_alm glo_alm[24];
	static struct dev_glo_eph glo_eph[24];
	static struct dev_glo_time glo_clk[1];
	static struct dev_sv_dir svdr[32];
	static struct dev_sv_hlth hlth[1];
	static struct dev_sv_slot_info sv_slot[24];
	int obj_count;

	if ((obj_count = nvs_rec_get(nvs_gps_alm, gps_alm, 0xFFFFFFFF)) > 0) {
		LOGPRINT(nvs, info, "nvs gps alm count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GPS_ALM, gps_alm + obj_count);
	}

	if ((obj_count = nvs_rec_get(nvs_gps_eph, gps_eph, 0xFFFFFFFF)) > 0) {
		LOGPRINT(nvs, info, "nvs gps eph count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GPS_EPH, gps_eph + obj_count);
	}

	if ((obj_count = nvs_rec_get(nvs_gps_clk, gps_clk, 1)) > 0) {
		LOGPRINT(nvs, info, "nvs gps clk count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GPS_TIM, gps_clk);
	}

	if ((obj_count = nvs_rec_get(nvs_gps_pos, gps_pos, 1)) > 0) {
		LOGPRINT(nvs, info, "nvs gps pos count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GPS_POS, gps_pos);
	}

	if ((obj_count = nvs_rec_get(nvs_gps_iono, gps_ion, 1)) > 0) {
		LOGPRINT(nvs, info, "nvs gps ion count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GPS_ION, gps_ion);
	}

	if ((obj_count = nvs_rec_get(nvs_gps_svdr, svdr, 0xFFFFFFFF)) > 0) {
		LOGPRINT(nvs, info, "nvs gps sv dir count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_SV_DIR, svdr + obj_count);
	}

	if ((obj_count = nvs_rec_get(nvs_glo_alm, glo_alm, 0x00FFFFFF)) > 0) {
		LOGPRINT(nvs, info, "nvs glo alm count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GLO_ALM, glo_alm + obj_count);
	}

	if ((obj_count = nvs_rec_get(nvs_glo_eph, glo_eph, 0x00FFFFFF)) > 0) {
		LOGPRINT(nvs, info, "nvs glo eph count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GLO_EPH, glo_eph + obj_count);
	}

	if ((obj_count = nvs_rec_get(nvs_glo_clk, glo_clk, 1)) > 0) {
		LOGPRINT(nvs, info, "nvs glo clk count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_GLO_TIM, glo_clk);
	}

	if ((obj_count = nvs_rec_get(nvs_glo_svdr, svdr, 0x00FFFFFF)) > 0) {
		LOGPRINT(nvs, info, "nvs glo sv dir count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_SV_DIR, svdr + obj_count);
	}

	if ((obj_count = nvs_rec_get(nvs_hlth, hlth, 1)) > 0) {
		LOGPRINT(nvs, info, "nvs health count [%d]", obj_count);

		while (obj_count--)
			print_data(REI_DEV_SV_HLT, hlth);
	}

	if ((obj_count = nvs_rec_get(nvs_glo_sv_slot, sv_slot, 0x00FFFFFF)) > 0) {
		LOGPRINT(nvs, info, "nvs sv slot [%d]", obj_count);

		while (obj_count--){
			LOGPRINT(nvs,info,"nvs sv id [%d],slot [%d],status[%d]",
				 sv_slot[obj_count].svid,sv_slot[obj_count].slot,
				 sv_slot[obj_count].status);
		}
	}
}

static unsigned int gps_alm_obj_num_find(void *obj_data)
{
	unsigned int svid = ((struct dev_gps_alm*)obj_data)->prn;
	if (svid >= 1 && svid <= 32) {
		return svid;
	} else {
		LOGPRINT(nvs, err, "gps_svdir_obj_num_find: invalid prn [%d]", svid);
		return -1;
	}
}

static unsigned int clk_obj_num_find(void *obj_data)
{
	UNUSED(obj_data);
	return  1;
}

static unsigned int gps_eph_obj_num_find(void *obj_data)
{
	unsigned int svid = ((struct dev_gps_eph*)obj_data)->prn;
	if (svid >= 1 && svid <= 32) {
		return svid;
	} else {
		LOGPRINT(nvs, err, "gps_svdir_obj_num_find: invalid prn [%d]", svid);
		return -1;
	}
}

static unsigned int iono_obj_num_find(void *obj_data)
{
	UNUSED(obj_data);
	return  1;
}

static unsigned int pos_obj_num_find(void *obj_data)
{
	UNUSED(obj_data);
	return  1;
}


static unsigned int tcxo_obj_num_find(void *obj_data)
{
	UNUSED(obj_data);
	return  1;
}


static unsigned int utc_obj_num_find(void *obj_data)
{
	UNUSED(obj_data);
	return  1;
}

static unsigned int glo_eph_obj_num_find(void *obj_data)
{
	unsigned int glo_svid;

	glo_svid = ((struct dev_glo_eph *)obj_data)->prn;
	if (glo_svid >= 65 && glo_svid <=88) {
		return (glo_svid % 64);
	} else {
		LOGPRINT(nvs, info, "glo_eph_obj_num_find: invalid prn [%d]", glo_svid);
		return -1;
	}
}

static unsigned int glo_alm_obj_num_find(void *obj_data)
{
	unsigned int glo_svid;

	glo_svid = ((struct dev_glo_alm *)obj_data)->prn;
	if (glo_svid >= 65 && glo_svid <=88) {
		return (glo_svid % 64);
	} else {
		LOGPRINT(nvs, info, "glo_alm_obj_num_find: invalid prn [%d]", glo_svid);
		return -1;
	}
}

static unsigned int hlth_obj_num_find(void *obj_data)
{
	UNUSED(obj_data);
	return  1;
}

static unsigned int gps_svdir_obj_num_find(void *obj_data)
{
	unsigned int svid  = ((struct dev_sv_dir *)obj_data)->prn;

	if (svid >= 1 && svid <= 32) {
		return svid;
	} else {
		LOGPRINT(nvs, err, "gps_svdir_obj_num_find: invalid prn [%d]", svid);
		return -1;
	}
}

static unsigned int glo_svdir_obj_num_find(void *obj_data)
{
	unsigned int svid  = ((struct dev_sv_dir *)obj_data)->prn;

	if (svid >= 65 && svid <=88) {
		return (svid % 64);
	} else {
		LOGPRINT(nvs, err, "glo_svdir_obj_num_find: invalid prn [%d]", svid);
		return -1;
	}
}

static unsigned int glo_sv_slot_map_num_find(void *obj_data)
{
	unsigned char svid  = ((struct dev_sv_slot_map *)obj_data)->svid;

	if (svid >= 65 && svid <=88) {
		LOGPRINT(nvs, info, "glo_sv_slot_map_num_find: valid svid [%d]", svid);
		return (svid % 64);
	} 
	else {
		LOGPRINT(nvs, err, "glo_sv_slot_map_num_find: invalid svid [%d]", svid);
		return -1;
	}
}


/*  New init START */
static struct nvs_rec_param gps_alm_rec = {

	.mcache      = write_thru,
	.get_oid     = gps_alm_obj_num_find,
	.rec_sz	     = sizeof(struct dev_gps_alm),
	.n_objs	     = 32
};

static struct nvs_rec_param gps_clk_rec = {

	.mcache      = write_thru,
	.get_oid     = clk_obj_num_find,
	.rec_sz	     = sizeof(struct dev_gps_time),
	.n_objs	     = 1
};

static struct nvs_rec_param glo_clk_rec = {

	.mcache      = write_thru,
	.get_oid     = clk_obj_num_find,
	.rec_sz	     = sizeof(struct dev_glo_time),
	.n_objs	     = 1
};

static struct nvs_rec_param gps_eph_rec = {

	.mcache      = write_thru,
	.get_oid     = gps_eph_obj_num_find,
	.rec_sz	     = sizeof(struct dev_gps_eph),
	.n_objs	     = 32
};

static struct nvs_rec_param hlth_rec = {

	.mcache      = write_thru,
	.get_oid     = hlth_obj_num_find,
	.rec_sz	     = sizeof(struct dev_sv_hlth),
	.n_objs	     = 1
};

static struct nvs_rec_param gps_svdir_rec = {
	.mcache      = write_thru,
	.get_oid     = gps_svdir_obj_num_find,
	.rec_sz	     = sizeof(struct dev_sv_dir),
	.n_objs	     = 32
};

static struct nvs_rec_param iono_rec = {

	.mcache      = write_thru,
	.get_oid     = iono_obj_num_find,
	.rec_sz	     = sizeof(struct dev_gps_ion),
	.n_objs	     = 1
};

static struct nvs_rec_param pos_rec = {

	.mcache      = write_thru,
	.get_oid     = pos_obj_num_find,
	.rec_sz	     = 	sizeof(struct dev_pos_info) +
			sizeof(struct dev_ellp_unc) +
			sizeof(struct dev_utc_info),
	.n_objs	     = 1
};
/*  : enable this when handling device calibration */

static struct nvs_rec_param tcxo_rec = {

	.mcache      = write_thru,
	.get_oid     = tcxo_obj_num_find,
	.rec_sz	     = sizeof(struct dev_gps_tcxo), //pending
	.n_objs	     = 1
};


static struct nvs_rec_param gps_utc_rec = {

	.mcache      = write_thru,
	.get_oid     = utc_obj_num_find,
	.rec_sz	     = sizeof(struct dev_gps_utc),
	.n_objs	     = 1
};

static struct nvs_rec_param glo_utc_rec = {

	.mcache      = write_thru,
	.get_oid     = utc_obj_num_find,
	.rec_sz	     = sizeof(struct dev_glo_utc),
	.n_objs	     = 1
};

static struct nvs_rec_param glo_alm_rec = {

	.mcache      = write_thru,
	.get_oid     = glo_alm_obj_num_find,
	.rec_sz	     = sizeof(struct dev_glo_alm),
	.n_objs	     = 24
};

static struct nvs_rec_param glo_eph_rec = {

	.mcache      = write_thru,
	.get_oid     = glo_eph_obj_num_find,
	.rec_sz	     = sizeof(struct dev_glo_eph),
	.n_objs	     = 24
};

static struct nvs_rec_param glo_svdir_rec = {
	.mcache      = write_thru,
	.get_oid     = glo_svdir_obj_num_find,
	.rec_sz	     = sizeof(struct dev_sv_dir),
	.n_objs	     = 32
};

static struct nvs_rec_param glo_sv_slot_map = {
	.mcache      = write_thru,
	.get_oid     = glo_sv_slot_map_num_find,
	.rec_sz	     = sizeof(struct dev_sv_slot_map),
	.n_objs	     = 24
};

static struct nvs_rec_info rec_info_tbl[] ={

	{.record = nvs_gps_pos,  .param = &pos_rec},
	{.record = nvs_gps_eph,  .param = &gps_eph_rec},
	{.record = nvs_gps_alm,  .param = &gps_alm_rec},
	{.record = nvs_gps_iono, .param = &iono_rec},
	{.record = nvs_gps_utc,  .param = &gps_utc_rec},
	{.record = nvs_gps_tcxo, .param = &tcxo_rec},
	{.record = nvs_gps_clk,  .param = &gps_clk_rec},
	{.record = nvs_gps_svdr, .param = &gps_svdir_rec},
	{.record = nvs_glo_eph,  .param = &glo_eph_rec},
	{.record = nvs_glo_alm,  .param = &glo_alm_rec},
	{.record = nvs_glo_clk,  .param = &glo_clk_rec},
	{.record = nvs_hlth,     .param = &hlth_rec},
	{.record = nvs_glo_svdr, .param = &glo_svdir_rec},
	{.record = nvs_glo_utc,  .param = &glo_utc_rec},
	{.record = nvs_glo_sv_slot, .param = &glo_sv_slot_map},
	{.record = nvs_rec_end,  .param = NULL}
};

struct nvs_rec_info* get_rec_info_tbl(void)
{
	return rec_info_tbl;
}
