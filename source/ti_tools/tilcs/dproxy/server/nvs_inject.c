/*
 * nvs_inject.c
 *
 * This file has low level functions that injects assistance to the device
 * from NVS
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <math.h>

#include "dev_proxy.h"
#include "ai2.h"
#include "config.h"
#include "logger.h"
#include "device.h"
#include "utils.h"
#include "inject.h"
#include "nvs.h"
#include "nvs_validate.h"
#include "log_report.h"
#include "glo_sv2slot_map.h"

extern int h2d_ai2_hdl;

unsigned short convt_m11e5(float flt)
{
	unsigned short res;

	if( flt <= (float) 0.0 )
		res = 0;
	else if (flt >= (float) (2047.0 * 32768.0 * 65536.0))
		res = 0xffff;
	else {
		unsigned char exponent;
		unsigned int mantissa;

		if (flt < (float) (1 << 21)) {
			mantissa = (unsigned int) flt;
			exponent = 0;
		} else {
			mantissa = (unsigned int)(flt *
						  (float)(1.0/(float)(1L << 10)));
			exponent = 10;
		}

		while (mantissa > 2047) {
			exponent++;
			mantissa >>= 1;
		}

		res = (unsigned short) ((mantissa << 5) | (exponent));
	}

	return res;
}


/*
 *
 * 3. Add assistance data to device and NVS.
 */
/* Inject GPS time estimate. */
int h2d_inj_dev_gps_time_est(void *data, int num)
{
	struct ai2_inj_gps_time_est {
		unsigned int id;
		unsigned int len;
		unsigned int mode;
		unsigned int fcount;
		unsigned int gps_week;
		unsigned int gps_msec;
		int clk_time_bias;
		unsigned int clk_time_uncert;
		unsigned int force_flag;
	} tim[1];
	char ai2_inj_gps_time_est_sizes[] = {1, 2, 1, 4, 2, 4, 3, 2, 1, 0};
	unsigned char mem[20];
	struct dev_gps_time *in = (struct dev_gps_time *) data;

	tim->id = 0x0A;
	tim->len = 17;
	tim->mode = 1; /* Time now. */
	tim->fcount = 0; /* Not used in time now mode. */
	tim->gps_week = in->week_num;
	tim->gps_msec = in->time_msec;
	tim->clk_time_bias = in->time_bias;
	tim->clk_time_uncert = in->time_unc;
	tim->force_flag = AI2_DONOT_FORCE_FLAG;

	d2h_ai2_encode((void *)tim, mem, ai2_inj_gps_time_est_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(tim_inj, info, "gps time est injected");
			return OK;
		}
	}

	UNUSED(num);
	LOGPRINT(tim_inj, err, "error sending ai2 packet to device");
	return ERR;
}

int h2d_inj_inc_gps_time_unc(void *data)
{
	struct ai2_inj_gps_time_est {
		unsigned int id;
		unsigned int len;
		unsigned int mode;
		unsigned int fcount;
		unsigned int gps_week;
		unsigned int gps_msec;
		int clk_time_bias;
		unsigned int clk_time_uncert;
		unsigned int force_flag;
	} tim[1];
	char ai2_inj_gps_time_est_sizes[] = {1, 2, 1, 4, 2, 4, 3, 2, 1, 0};
	unsigned char mem[20];
	struct dev_gps_time *in = (struct dev_gps_time *) data;

	tim->id = 0x0A;
	tim->len = 17;
	tim->mode = 0; /* Arbitrary reference. */
	tim->fcount = in->timer_count;
	tim->gps_week = in->week_num;
	tim->gps_msec = in->time_msec;
	tim->clk_time_bias = in->time_bias;
	tim->clk_time_uncert = in->time_unc;
	tim->force_flag = AI2_FORCE_FLAG_DFLT;

	d2h_ai2_encode((void *)tim, mem, ai2_inj_gps_time_est_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(tim_unc, info, "gps time unc injected");
			return OK;
		}
	}

	LOGPRINT(tim_unc, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject GLONASS time estimate. */
int h2d_inj_dev_glo_time_est(void *data, int num)
{
#define TOD_IN_30SEC_SCALE (30U * 1000U)

	struct ai2_inj_glo_time_est {
		unsigned int id;
		unsigned int len;
		unsigned int mode;
		unsigned int fcount;
		unsigned int glo_day;
		unsigned int glo_month;
		unsigned int glo_year;
		unsigned int glo_msec;
		unsigned int clk_time_bias;
		unsigned int clk_time_uncert;
		unsigned int ggto;
		unsigned int time_status;
		unsigned int force_flag;
		unsigned int reserved;
	} tim[1];
	char ai2_inj_glo_time_est_sizes[] = {1, 2, 1, 4, 1, 1, 2, 4, 3, 2, 2, 1, 1,
		4, 0};
	unsigned char mem[29];
	struct dev_glo_time *in = (struct dev_glo_time *) data;

	tim->id = 0x26;
	tim->len = 26;
	tim->mode = 1; /* Time now. */
	tim->fcount = 0; /* Not used in time now mode. */
	tim->glo_day = in->time_day;
	tim->glo_month = in->time_month;
	tim->glo_year = in->time_year;
	tim->glo_msec = in->time_msec;
	tim->clk_time_bias = in->time_bias;
	tim->clk_time_uncert = in->time_unc;
	tim->ggto = in->ggto;
	tim->time_status = in->time_status;
	tim->force_flag = AI2_FORCE_FLAG_DFLT;
	tim->reserved = 0;

	d2h_ai2_encode((void *)tim, mem, ai2_inj_glo_time_est_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 29)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(tim_inj, info, "glo time est injected");
			return OK;
		}
	}

	UNUSED(num);
	LOGPRINT(tim_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject positon estimate. */
static int h2d_inj_dev_pos_est(void *data, int num)
{
	struct ai2_inj_pos_est {
		unsigned int id;
		unsigned int len;
		unsigned int lat;
		unsigned int lon;
		unsigned int alt;
		unsigned int pos_uncert;
		unsigned int pos_week;
		unsigned int pos_ms;
		unsigned int inj_mode;
		unsigned int force_flag;
	} pos[1];
	char ai2_inj_pos_est_sizes[] = {1, 2, 4, 4, 2, 4, 2, 4, 1, 1, 0};
	unsigned char mem[25];
	struct dev_pos_info *in = (struct dev_pos_info *) data;
	unsigned int uncert = 0;

	in->ell = BUF_OFFSET_ST(in, struct dev_pos_info,
				struct dev_ellp_unc);

	in->utc = BUF_OFFSET_ST(in->ell, struct dev_ellp_unc,
				struct dev_utc_info);

	if (in->contents & DEV_POS_HAS_ELP) {
		uncert = (int)(10 * (pow(1.1, in->ell->semi_major) - 1));
		uncert = (int)(uncert * C_RECIP_LSB_POS_SIGMA)/3;
	} else {
		// : Check below logic.

		/* 
		 * Uncert ellipsoid to sphere approx. Sphere includes ellipsoid.
		 */
		uncert = (in->unc_east > in->unc_north) ? in->unc_east :
			in->unc_north;
		uncert = (uncert > in->unc_vertical) ? uncert :
			in->unc_vertical;
	}

	pos->id = 0x0C; // : For glonass 0x27 is the ID
	pos->len = 22;
	pos->lat = in->lat_rad;
	pos->lon = in->lon_rad;
	pos->alt = in->alt_wgs84;
	pos->pos_uncert = uncert;
	pos->pos_week = 0;
	pos->pos_ms = 0;
	pos->inj_mode = 1; /* Use time in the receiver. */
	pos->force_flag = AI2_FORCE_FLAG_DFLT;

	d2h_ai2_encode((void *)pos, mem, ai2_inj_pos_est_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(pos_inj, info, "pos est injected");
			return OK;
		}
	}

	UNUSED(num);
	LOGPRINT(pos_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject altitude. */
static int h2d_inj_dev_alt_est(void *data, int num)
{
	struct ai2_inj_alt_est {
		unsigned int id;
		unsigned int len;
		int alt_est;
		unsigned int alt_unc;
		unsigned int force_flag;
	} alt[1];
	char ai2_inj_alt_est_sizes[] = {1, 2, 2, 2, 1, 0};
	unsigned char mem[8];
	struct dev_pos_info *in = (struct dev_pos_info *) data;
	unsigned int* val;

	alt->id = 0x1D;
	alt->len = 5;
	alt->alt_est = in->alt_wgs84;

	sys_param_get(cfg_alt_unc,(void**) &val);

	alt->alt_unc = (*val) * 2; /* Default value is 95m. */
	alt->force_flag = AI2_FORCE_FLAG_DFLT;

	d2h_ai2_encode((void *)alt, mem, ai2_inj_alt_est_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(pos_inj, info, "Altitude injected: alt [%d] unc [%d]",
				 alt->alt_est, alt->alt_unc);
			return OK;
		}
	}

	UNUSED(num);
	LOGPRINT(pos_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject both position and altitude */
int h2d_inj_dev_pos_alt_est(void *data, int num)
{
	int ret = OK;
	if (ERR == h2d_inj_dev_pos_est(data, num) ||
	    ERR == h2d_inj_dev_alt_est(data, num))
		ret = ERR;

	return ret;
}


extern unsigned int gps_visible_sats(const struct dev_pos_info *const pos,
				     unsigned int gps_msec,
				     const struct dev_gps_eph *const eph,
				     unsigned short no_sats);

/* Inject gps ephemeris. */
int h2d_inj_dev_gps_eph(void *data, int num)
{
	struct ai2_inj_gps_eph {
		unsigned int id;
		unsigned int len;
		unsigned int svid;
		unsigned int code_on_l2;
		unsigned int accuracy;
		unsigned int health;
		unsigned int tgd;
		unsigned int iodc;
		unsigned int toc;
		unsigned int af2;
		unsigned int af1;
		unsigned int af0;
		unsigned int iode;
		int          crs;
		int          deltan;
		int          mo;
		int          cuc;
		unsigned int e;
		int          cus;
		unsigned int sqrta;
		unsigned int toe;
		int          cic;
		int          omega0;
		int          cis;
		int          io;
		int          crc;
		int          omega;
		int          omegadot;
		int          idot;
		unsigned int week;
		unsigned int indication;
		unsigned int reserved;
	} eph[1];
	char ai2_inj_gps_eph_sizes[] = {1, 2, 1, 1, 1, 1, 1, 2, 2, 1, 2, 3, 1,
		2, 2, 4, 2, 4, 2, 4, 2, 2, 4, 2, 4, 2,
		4, 3, 2, 2, 1, 3, 0};
	unsigned char mem[70];
	struct dev_gps_eph *input = (struct dev_gps_eph *)data;
	unsigned int num_sv_injected = 0;

	while (num-- > 0) {

		eph->id           = 0x11;
		eph->len          = 67;
		eph->svid         = (unsigned int)input->prn;
		eph->code_on_l2   = (unsigned int)input->Code_on_L2;
		eph->accuracy     = (unsigned int)input->Accuracy;
		eph->health       = (unsigned int)input->Health;
		eph->tgd          = (unsigned int)input->Tgd;
		eph->iodc         = (unsigned int)input->IODC;
		eph->toc          = (unsigned int)input->Toc;
		eph->af2          = (unsigned int)input->Af2;
		eph->af1          = (unsigned int)input->Af1;
		eph->af0          = (unsigned int)input->Af0;
		eph->iode         = (unsigned int)input->IODE;
		eph->crs          = (int)input->Crs;
		eph->deltan       = (int)input->DeltaN;
		eph->mo           = (int)input->Mo;
		eph->cuc          = (int)input->Cuc;
		eph->e            = (unsigned int)input->E;
		eph->cus          = (int)input->Cus;
		eph->sqrta        = (unsigned int)input->SqrtA;
		eph->toe          = (unsigned int)input->Toe;
		eph->cic          = (int)input->Cic;
		eph->omega0       = (int)input->Omega0;
		eph->cis          = (int)input->Cis;
		eph->io           = (int)input->Io;
		eph->crc          = (int)input->Crc;
		eph->omega        = (int)input->Omega;
		eph->omegadot     = (int)input->OmegaDot;
		eph->idot         = (int)input->Idot;
		eph->week         = (unsigned int)input->Week;
		eph->indication   = (unsigned int) !(input->is_bcast);
		eph->reserved     = (unsigned int)0;

		d2h_ai2_encode((void *)eph, mem, ai2_inj_gps_eph_sizes);
		if (ai2_ok == ai2_ephemeris_flush(h2d_ai2_hdl, mem, 70)) {
			LOGPRINT(alm_inj, info, "gps eph injected");
		} else {
			LOGPRINT(alm_inj, err, "error sending ai2 packet to device");
			return ERR;
		}

		num_sv_injected++;
		input++;
		if(num_sv_injected > 4){
			num_sv_injected = 0;
			usleep(200 * 1000);

		}
	}

	if (ai2_ok != ai2_flush(h2d_ai2_hdl)) {
		LOGPRINT(eph_inj, err, "error sending ai2 packet to device");
		return ERR;
	}

	LOGPRINT(eph_inj, info, "gps ephemeris injected");
	return OK;
}

/* Inject glo ephemeris. */
int h2d_inj_dev_glo_eph(void *data, int num)
{
	struct ai2_inj_glo_eph {
		unsigned int id;
		unsigned int len;
		unsigned int svid;
		unsigned int m;
		unsigned int tk;
		unsigned int tb;
		unsigned int M;
		int          epsilon;
		int          tau;
		int          xc;
		int          yc;
		int          zc;
		int          xv;
		int          yv;
		int          zv;
		int          xa;
		int          ya;
		int          za;
		unsigned int b;
		unsigned int p;
		unsigned int nt;
		unsigned int ft;
		unsigned int n;
		int          deltatau;
		unsigned int en;
		unsigned int p1;
		unsigned int p2;
		unsigned int p3;
		unsigned int p4;
		unsigned int ln;
		unsigned int indication;
	} eph[1];
	char ai2_inj_glo_eph_sizes[] = {1, 2, 1, 1, 2, 1, 1, 2, 3, 4, 4, 4, 3,
		3, 3, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 0};
	unsigned char mem[52];
	struct dev_glo_eph *input = (struct dev_glo_eph *)data;
	unsigned int num_sv_injected = 0;

	while (num-- > 0) {
		eph->id         = 0x11;
		eph->len        = 49;
		eph->svid       = (unsigned int)input->prn;
		eph->m          = (unsigned int)input->m;
		eph->tk         = (unsigned int)input->Tk;
		eph->tb         = (unsigned int)input->Tb;
		eph->M          = (unsigned int)input->M;
		eph->epsilon    = (int)input->Epsilon;
		eph->tau        = (int)input->Tau;
		eph->xc         = (int)input->Xc;
		eph->yc         = (int)input->Yc;
		eph->zc         = (int)input->Zc;
		eph->xv         = (int)input->Xv;
		eph->yv         = (int)input->Yv;
		eph->zv         = (int)input->Zv;
		eph->xa         = (int)input->Xa;
		eph->ya         = (int)input->Ya;
		eph->za         = (int)input->Za;
		eph->b          = (unsigned int)input->B;
		eph->p          = (unsigned int)input->P;
		eph->nt         = (unsigned int)input->Nt;
		eph->ft         = (unsigned int)input->Ft;
		eph->n          = (unsigned int)input->n;
		eph->deltatau   = (int)input->DeltaTau;
		eph->en         = (unsigned int)input->En;
		eph->p1         = (unsigned int)input->P1;
		eph->p2         = (unsigned int)input->P2;
		eph->p3         = (unsigned int)input->P3;
		eph->p4         = (unsigned int)input->P4;
		eph->ln         = (unsigned int)input->ln;
		eph->indication = (unsigned int) !(input->is_bcast);

		d2h_ai2_encode((void *)eph, mem, ai2_inj_glo_eph_sizes);
		if (ai2_ok == ai2_ephemeris_flush(h2d_ai2_hdl, mem, sizeof(mem))) {
			LOGPRINT(alm_inj, info, "Glo eph injected");
		} else {
			LOGPRINT(alm_inj, err, "error sending ai2 packet to device");
			return ERR;
		}

		num_sv_injected++;
		input++;
		if(num_sv_injected > 4){
			num_sv_injected = 0;
			usleep(200 * 1000);

		}
	}

	if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
		LOGPRINT(eph_inj, info, "glo eph injected");
	} else {
		LOGPRINT(eph_inj, err, "error sending ai2 packet to device");
		return ERR;
	}

	return OK;
}

/* Inject gps almanac. */
int h2d_inj_dev_gps_alm(void *data, int num)
{
	struct ai2_inj_gps_alm {
		unsigned int id;
		unsigned int len;
		unsigned int svid;
		unsigned int health;
		unsigned int toa;
		unsigned int e;
		int          deltai;
		int          omegadot;
		unsigned int sqrta;
		int          omegazero;
		int          omega;
		int          mzero;
		int          af0;
		int          af1;
		unsigned int week;
	} alm[1];
	char ai2_inj_gps_alm_sizes[] = {1, 2, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 2,
		2, 2, 0};
	unsigned char mem[30];
	struct dev_gps_alm *input = (struct dev_gps_alm *)data;
	unsigned int num_sv_injected = 0;
	
	while (num-- > 0) {
		alm->id         = 0x13;
		alm->len        = 27;
		alm->svid       = (unsigned int)input->prn;
		alm->health     = (unsigned int)input->Health;
		alm->toa        = (unsigned int)input->Toa;
		alm->e          = (unsigned int)input->E;
		alm->deltai     = (int)input->DeltaI;
		alm->omegadot   = (int)input->OmegaDot;
		alm->sqrta      = (unsigned int)input->SqrtA;
		alm->omegazero  = (int)input->OmegaZero;
		alm->omega      = (int)input->Omega;
		alm->mzero      = (int)input->MZero;
		alm->af0        = (int)input->Af0;
		alm->af1        = (int)input->Af1;
		alm->week       = (unsigned int)input->Week;

		d2h_ai2_encode((void *)alm, mem, ai2_inj_gps_alm_sizes);
		if (ai2_ok == ai2_almanac_flush(h2d_ai2_hdl, mem, 30)) {
			LOGPRINT(alm_inj, info, "gps alm injected");
		} else {
			LOGPRINT(alm_inj, err, "error sending ai2 packet to device");
			return ERR;
		}

		num_sv_injected++;
		input++;
		if(num_sv_injected > 4){
			num_sv_injected = 0;
			usleep(200 * 1000);

		}
	
	}

	if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
		LOGPRINT(alm_inj, info, "gps alm injected");
	} else {
		LOGPRINT(alm_inj, err, "error sending ai2 packet to device");
		return ERR;
	}

	return OK;
}

/* Inject glo almanac. */
int h2d_inj_dev_glo_alm(void *data, int num)
{
	struct ai2_inj_glo_alm {
		unsigned int id;
		unsigned int len;
		unsigned int svid;
		int          tauc;
		int          taugps;
		unsigned int n4;
		unsigned int NA;
		unsigned int na;
		unsigned int hna;
		int          lambdana;
		unsigned int tlambdana;
		int          deltainacorrection;
		int          deltatnarate;
		int          deltatnaratedot;
		unsigned int varepsilonna;
		int          omegana;
		unsigned int mna;
		int          b1;
		int          b2;
		unsigned int kp;
		int          tauna;
		unsigned int cna;
	} alm[1];
	char ai2_inj_glo_alm_sizes[] = {1, 2, 1, 4, 3, 1, 2, 2, 1, 3, 3, 3, 3, 1,
		2, 2, 1, 2, 2, 1, 2, 1, 0};
	unsigned char mem[41];
	struct dev_glo_alm *input   = (struct dev_glo_alm *)data;
	unsigned int num_sv_injected = 0;
	while (num-- > 0) {
		alm->id                  = 0x13;
		alm->len                 = 38;
		alm->svid                = (unsigned int)input->prn;
		alm->tauc                = (int)input->TauC;
		alm->taugps              = (int)input->TauGPS;
		alm->n4                  = (unsigned int)input->N4;
		alm->NA                  = (unsigned int)input->NA;
		alm->na                  = (unsigned int)input->nA;
		alm->hna                 = (unsigned int)input->HnA;
		alm->lambdana            = (int)input->Lambda_nA;
		alm->tlambdana           = (unsigned int)input->t_Lambda_nA;
		alm->deltainacorrection  = (int)input->Delta_i_nAcorrection;
		alm->deltatnarate        = (int)input->Delta_T_nArate;
		alm->deltatnaratedot     = (int)input->Delta_T_nArate_Dot;
		alm->varepsilonna        = (unsigned int)input->Varepsilon_nA;
		alm->omegana             = (int)input->Omega_nA;
		alm->mna                 = (unsigned int)input->M_nA;
		alm->b1                  = (int)input->B1;
		alm->b2                  = (int)input->B2;
		alm->kp                  = (unsigned int)input->Kp;
		alm->tauna               = (int)input->Tau_nA;
		alm->cna                 = (unsigned int)input->C_nA;

		d2h_ai2_encode((void *)alm, mem, ai2_inj_glo_alm_sizes);
			if (ai2_ok == ai2_almanac_flush(h2d_ai2_hdl, mem, sizeof(mem))) {
				 LOGPRINT(alm_inj, info, "gps alm injected");
			 } else {
				 LOGPRINT(alm_inj, err, "error sending ai2 packet to device");
				 return ERR;
			 }

		 num_sv_injected++;
        input++;
        if(num_sv_injected > 4){
                num_sv_injected = 0;
                usleep(200 * 1000);

        }
	}

	if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
		LOGPRINT(alm_inj, info, "glo alm injected");
	} else {
		LOGPRINT(alm_inj, err, "error sending ai2 packet to device");
		return ERR;
	}

	return OK;
}

/* Inject iono. */
int h2d_inj_dev_gps_iono(void *data, int num)
{
	struct ai2_inj_iono {
		unsigned int id;
		unsigned int len;
		int          alpha0;
		int          alpha1;
		int          alpha2;
		int          alpha3;
		int          beta0;
		int          beta1;
		int          beta2;
		int          beta3;
	} iono[1];
	char ai2_inj_iono_sizes[] = {1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 0};
	unsigned char mem[11];
	struct dev_gps_ion *input = (struct dev_gps_ion *)data;

	iono->id      = 0x15;
	iono->len     = 8;
	iono->alpha0  = (int)input->Alpha0;
	iono->alpha1  = (int)input->Alpha1;
	iono->alpha2  = (int)input->Alpha2;
	iono->alpha3  = (int)input->Alpha3;
	iono->beta0   = (int)input->Beta0;
	iono->beta1   = (int)input->Beta1;
	iono->beta2   = (int)input->Beta2;
	iono->beta3   = (int)input->Beta3;

	d2h_ai2_encode((void *)iono, mem, ai2_inj_iono_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 11)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(ion_inj, info, "iono injected");
			return OK;
		}
	}

	UNUSED(num);
	LOGPRINT(ion_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject GPS UTC. */
int h2d_inj_dev_gps_utc(void *data, int num)
{
	struct ai2_inj_gps_utc {
		unsigned int id;
		unsigned int len;
		int          a0;
		int          a1;
		int          deltatls;
		unsigned int tot;
		unsigned int wnt;
		unsigned int wnlsf;
		unsigned int dn;
		int          deltatlsf;
	} utc[1];
	char ai2_inj_gps_utc_sizes[] = {1, 2, 4, 4, 1, 1, 1, 1, 1, 1, 0};
	unsigned char mem[17];
	struct dev_gps_utc *input = (struct dev_gps_utc *)data;

	utc->id         = 0x17;
	utc->len        = 14;
	utc->a0         = (int)input->A0;
	utc->a1         = (int)input->A1;
	utc->deltatls   = (int)input->DeltaTls;
	utc->tot        = (unsigned int)input->Tot;
	utc->wnt        = (unsigned int)input->WNt;
	utc->wnlsf      = (unsigned int)input->WNlsf;
	utc->dn         = (unsigned int)input->DN;
	utc->deltatlsf  = (int)input->DeltaTlsf;

	d2h_ai2_encode((void *)utc, mem, ai2_inj_gps_utc_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 17)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(utc_inj, info, "gps utc injected");
			return OK;
		}
	}

	UNUSED(num);
	LOGPRINT(utc_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject GLONASS UTC. */
int h2d_inj_dev_glo_utc(void *data, int num)
{
	struct ai2_inj_glo_utc {
		unsigned int id;
		unsigned int len;
		int          b1;
		int          b2;
		int          kp;
	} utc[1];
	char ai2_inj_glo_utc_sizes[] = {1, 2, 2, 2, 1, 0};
	unsigned char mem[8];
	struct dev_glo_utc *input = (struct dev_glo_utc *)data;

	utc->id         = 0x28;
	utc->len        = 5;
	utc->b1         = (int)input->B1;
	utc->b2         = (int)input->B2;
	utc->kp		= (int)input->KP;

	d2h_ai2_encode((void *)utc, mem, ai2_inj_glo_utc_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(utc_inj, info, "glo utc injected");
			return OK;
		}
	}

	UNUSED(num);
	LOGPRINT(utc_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject SV health. */
int h2d_inj_dev_svhlth(void *data, int num)
{
	struct ai2_inj_svhlth {
		unsigned int id;
		unsigned int len;
		unsigned int hlth[56];
	} svhlth[1];
	char ai2_inj_svhlth_sizes[] = {1, 2, 56, 0};
	unsigned char mem[64];
	unsigned int i;
	struct dev_sv_hlth *input = (struct dev_sv_hlth *)data;
	struct dev_app_desc *desc = get_app_desc_cfg();

	svhlth->id = 0x19;
	/* Adjust the size based on constellation. */
	svhlth->len = 0;
	if ((desc->gnss_select & DEV_GNSS_GPS_EN) ||
	    (!desc->gnss_select))
		svhlth->len += 32;
	if (desc->gnss_select & DEV_GNSS_GLO_EN)
		svhlth->len += 24;

	/* Some sanity check. */
	if (0 == svhlth->len) {
		LOGPRINT(hlt_inj, err, "app profile not set");
		return ERR;
	}

	for(i = 0; i < svhlth->len; i++) {
		svhlth->hlth[i] = input->health_status[i];
	}

	d2h_ai2_encode((void *)svhlth, mem, ai2_inj_svhlth_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, svhlth->len + 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(hlt_inj, info, "svhlth injected");
			return OK;
		}
	}

	LOGPRINT(hlt_inj, err, "error sending ai2 packet to device");

	UNUSED(num);
	return ERR;
}

/* Inject frequency estimate. */
int h2d_inj_dev_freq_est(void *data, int num)
{
	struct ai2_inj_freq_est {
		unsigned int id;
		unsigned int len;
		int          clk_freq_bias;
		unsigned int clk_freq_uncert;
		unsigned int force_flag;
	} freq[1];
	char ai2_inj_freq_est_sizes[] = {1, 2, 4, 4, 1, 0};
	unsigned char mem[12];

	struct dev_gps_tcxo*input = (struct dev_gps_tcxo *)data;

	freq->id = 0x20;
	freq->len = 9;
	freq->clk_freq_bias   = input->freq_bias;
	freq->clk_freq_uncert = input->freq_unc;
	freq->force_flag = AI2_DONOT_FORCE_FLAG;

	d2h_ai2_encode((void *)freq, mem, ai2_inj_freq_est_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 12)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(freq_est, info, "freq est injected , \
				 freq bias = %d, freq_unc = %d",
				 input->freq_bias,input->freq_unc );
			return OK;
		}
	}

	UNUSED(num);
	UNUSED(data);
	LOGPRINT(freq_est, err, "error sending ai2 packet to device");
	return ERR;
}

int h2d_inj_dev_nvs_gps_time(void *data, int num)
{
	struct dev_gps_time *in = (struct dev_gps_time *) data;

	struct dev_gps_time gps_time;	
	unsigned int elapsed_tim = 0; 
	float time_unc;
	unsigned short *rtc_unc;
	unsigned int now_secs = os_utc_secs();

	unsigned int rec_secs = nvs_rec_ts_s(nvs_gps_clk, &gps_time);

	elapsed_tim = now_secs - rec_secs;

	LOGPRINT(nvs, info, "h2d_inj_dev_nvs_gps_time elapsed[%d]", 
		 now_secs,rec_secs,elapsed_tim); 

	// Retrive from config file
	sys_param_get(cfg_host_rtc_unc ,(void**) &rtc_unc);

	//time_unc = (elapsed_tim * ((*rtc_unc) * 1e-6));
	time_unc = (elapsed_tim * ((*rtc_unc) * 1e-7));   /*1e-7 including the 0.1 scaling factor*/

	if (time_unc < 2.0f)
		time_unc = 2.0f;

	in->time_unc = convt_m11e5(time_unc*1e9 );

	LOGPRINT(nvs, info, "h2d_inj_dev_nvs_gps_time IT Unc [%u], Time Unc [%f]", 
		 in->time_unc, time_unc);

	return h2d_inj_dev_gps_time_est(data,num);
}


/* Inject SV direction. */
int h2d_inj_dev_svdir(void *data, int num)
{
	int i;
	unsigned char mem[4 + (3 * 64)];
	unsigned char *buf = mem;
	struct dev_sv_dir *input = (struct dev_sv_dir *)data;


	buf = BUF_LE_U1B_WR_INC(buf, 0x23);
	buf = BUF_LE_U2B_WR_INC(buf, 1 + (3 * num));
	buf = BUF_LE_U1B_WR_INC(buf, SV_SELECT_ALGO_NOW);

	for(i = 0; i < num; i++) {
		buf = BUF_LE_U1B_WR_INC(buf, input->prn);
		buf = BUF_LE_S1B_WR_INC(buf, input->elevation);
		buf = BUF_LE_U1B_WR_INC(buf, input->azimuth);

		input++;
	}

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4 + (3 * num))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(svdir_inj, info, "svdir injected");
			return OK;
		}
	}

	LOGPRINT(svdir_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Part 2. NVS Validation and Injection. */
struct h2d_inj_assist_tbl {
	enum nvs_record nvs_id;
	int (*injector)(void *data, int num);
};

/*
 * NVS assistance injection table.
 */

// : Add GLO UTC and IONO.
static struct h2d_inj_assist_tbl h2d_inj_assist_tbl[] = {
	// {nvs_gps_clk,  h2d_inj_dev_gps_time_est},
	{nvs_gps_clk, h2d_inj_dev_nvs_gps_time},
	/* Glonass injection commented as per FW team input, instead
	   UTC for Glo + Gps is injected */
	//{nvs_glo_clk,  h2d_inj_dev_glo_time_est},
	{nvs_gps_pos,  h2d_inj_dev_pos_est},
	{nvs_gps_alm,  h2d_inj_dev_gps_alm},
	{nvs_glo_alm,  h2d_inj_dev_glo_alm},
	{nvs_gps_eph,  h2d_inj_dev_gps_eph},
	{nvs_glo_eph,  h2d_inj_dev_glo_eph},
	{nvs_gps_iono, h2d_inj_dev_gps_iono},
	{nvs_gps_utc,  h2d_inj_dev_gps_utc},
	{nvs_glo_utc,  h2d_inj_dev_glo_utc},
	{nvs_gps_svdr, h2d_inj_dev_svdir},
	{nvs_hlth,     	h2d_inj_dev_svhlth},
	{nvs_glo_sv_slot, h2d_inj_glo_sv2slot_map}
	/* Add new entries here. */
};

static struct h2d_inj_assist_tbl *find_by_nvs(enum nvs_record id)
{
	struct h2d_inj_assist_tbl *rec = h2d_inj_assist_tbl;
	unsigned int cnt = sizeof(h2d_inj_assist_tbl) /
		sizeof(struct h2d_inj_assist_tbl);

	/* Return the first match. */
	while (cnt--) {
		if (rec->nvs_id == id)
			return rec;

		rec++;
	}

	return NULL;
}

/*
 * Local function to read all NVS assistance and validate and inject to the
 * device. This method should be called only once at the ready sequence. This
 * function uses the injector functions.
 */
int dpxy_inj_nvs_aiding(void *data)
{
	unsigned int count = 0;
	int n_aid = 0;
	int ret = OK;
	struct nvs_aid_data *nvs_rec;
	struct h2d_inj_assist_tbl *tbl = NULL;

	n_aid = nvs_get_num_aid_elem();

	LOGPRINT(nvs, info, "dpxy_inj_nvs_aiding(): nvs_get_num_aid_elem n_aid = %d", n_aid);

	nvs_rec = (struct nvs_aid_data*)malloc(n_aid * 
					       sizeof(struct nvs_aid_data));
	if (!nvs_rec){
		LOGPRINT(nvs, info, "memory allocation for nvs_aid_data failed");
		return ERR;
	}

	memset(nvs_rec, 0, n_aid * sizeof(struct nvs_aid_data));

	n_aid = _read_nvs_aiding(nvs_rec, n_aid, 1); /* Get NVS for device boot */

	LOGPRINT(nvs, info, "dpxy_inj_nvs_aiding(): read_nvs_aiding n_aid = %d", n_aid);

	/* Some sanity. */
	if (n_aid <= 0) {
		LOGPRINT(nvs, info, "no valid nvs data found");
		free(nvs_rec);
		return ERR;
	}

	count = n_aid;
	/* Push aiding one after another. */
	while(n_aid--) {
		/* Get an injector. */
		tbl = find_by_nvs(nvs_rec->record);
		if (tbl && nvs_rec->n_objs) {

			LOGPRINT(nvs, info, "found valid nvs aid type [%d]", tbl->nvs_id);

			if ((nvs_gps_clk == nvs_rec->record) || 
			    (nvs_glo_clk == nvs_rec->record))
				fixup_dev_gnss_time(nvs_rec->record, 
						    nvs_rec->buffer);

			if (nvs_gps_pos == nvs_rec->record)
				fixup_dev_gnss_pos(nvs_rec->buffer);

			/* Inject. */
			if (tbl->injector)
				ret = tbl->injector(nvs_rec->buffer, nvs_rec->n_objs) | ret;


		} else
			LOGPRINT(nvs, warn, "nvs data invalid for enum [%d]", 
				 nvs_rec->record);

		nvs_rec++;
	}

	nvs_rec -= count;

	free(nvs_rec);
	UNUSED(data);
	return 0;
}
