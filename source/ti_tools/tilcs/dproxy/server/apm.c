/*
 * apm.c
 *
 * Advance power management
 *
 * management of APM related functionalities in the receiver
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "ai2.h"
#include "apm.h"
#include "dev_proxy.h"
#include "rpc.h"
#include "logger.h"
#include "utils.h"
#include "config.h"

#define disable 0
#define enable 1

extern int h2d_ai2_hdl;

int h2d_dev_legacy_apm_ai2_inj(unsigned char control){
#define AI2_APM_PKT_SIZE 11
	struct ai2_qop_cfg {
		unsigned int id;
		unsigned int len;
		unsigned int sub_pkt_id;
		unsigned int apm_control;
		unsigned int reserved1;
		unsigned int reserved2;
	} apm[1];
	char ai2_apm_ctrl_sizes[] = {1, 2, 1, 1, 4, 2, 0};
	unsigned char mem[AI2_APM_PKT_SIZE];

	apm->id = 0xFB;
	apm->len = 8;

	apm->sub_pkt_id = 0x0C;

	apm->apm_control = control;

	apm->reserved1 = 0;
	apm->reserved2 = 0;

	d2h_ai2_encode((void *)apm, mem, ai2_apm_ctrl_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(generic, info, "Legacy apm control injected [%d]",control);
			return OK;
		}
	}

	LOGPRINT(generic, err, "error sending apm control ai2 packet to device");
	return ERR;
}


int h2d_dev_fast_apm_ai2_inj(unsigned int control){
#define AI2_FAST_APM_PKT_SIZE 9
	struct ai2_qop_cfg {
		unsigned int id;
		unsigned int len;
		unsigned int sub_pkt_id;
		unsigned int apm_control;
		unsigned int reserved1;
		unsigned int reserved2;
		unsigned int reserved3;
		unsigned int reserved4;
	} apm[1];
	char ai2_fast_apm_ctrl_sizes[] = {1, 2, 1, 1, 1, 1, 1, 1, 0};
	unsigned char mem[AI2_FAST_APM_PKT_SIZE];

	apm->id = 0xFB;
	apm->len = 6;

	apm->sub_pkt_id = 0x2D;
	
	apm->apm_control = control;

	apm->reserved1 = 0;
	apm->reserved2 = 0;
	apm->reserved4 = 0;
	apm->reserved3 = 0;

	d2h_ai2_encode((void *)apm, mem, ai2_fast_apm_ctrl_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(generic, info, "fast apm control injected [%d]",control);
			return OK;
		}
	}

	LOGPRINT(generic, err, "error sending fast apm control ai2 packet to device");
	return ERR;
}

/*
* ctrl
* 0 - disable all (No APM)
* 1 - fast APM
* 2 - legacy APM
* 3 - fast + legacy APM
*/
int apm_control(unsigned int ctrl){
	int ret = OK;
	
	LOGPRINT(generic, info, "apm_control: APM [%d]", ctrl);
	switch(ctrl){
		case APM_DISABLE:
			ret = h2d_dev_legacy_apm_ai2_inj(disable);
			ret = h2d_dev_fast_apm_ai2_inj(disable);
			break;
		case APM_LEVEL_1: 
			ret = h2d_dev_fast_apm_ai2_inj(enable);
			break;
		case APM_LEVEL_2:
			ret = h2d_dev_legacy_apm_ai2_inj(enable);
			break;
		case APM_LEVEL_3:
			ret = h2d_dev_fast_apm_ai2_inj(enable);
			ret = h2d_dev_legacy_apm_ai2_inj(enable);
			break;
		default:
			LOGPRINT(generic, err, "ERROR: Unknown APM request [%d]", ctrl);
			break;
	}

	return ret;
}

/* Call from the sequencer */
int h2d_inj_apm_ctrl_from_config(void *data){
	unsigned int *control;

	sys_param_get(cfg_apm_flag,(void**) &control);

	LOGPRINT(generic, info, "Config: APM is set to [%d]", *control);

	UNUSED(data);

	if (*control != APM_DISABLE)
		return apm_control(*control);
	else
		return OK;
}

/* Call from the sequencer */
int h2d_inj_apm_disable(void *data){

	int ret = OK;
	LOGPRINT(generic, info, "disable APM");
	ret = apm_control(APM_DISABLE);
	sleep(1);
	UNUSED(data);
	return ret;
}

/* Call from UE ADAPTER.cpp */
int h2d_apm_command(struct srv_rei *srv_re, struct ie_desc *ie_adu){
	struct dev_apm_ctrl *in = (struct dev_apm_ctrl *)ie_adu->data;

	LOGPRINT(generic, info, "h2d_apm_command: APM mode requested [%d]", in->control);

	UNUSED(srv_re);
	return apm_control(in->control);	
}

