#include <math.h>

#include "3gpp_utils.h"
#include "gnss_utils.h"

/* Flags for GLONASS ICD EPH structure */
#define GLO_CLK_MDL_3GPP (0x01)
#define GLO_ORB_MDL_3GPP (0x02)
#define GLO_ICD_EPH_INFO (GLO_CLK_MDL_3GPP | GLO_ORB_MDL_3GPP)

int add2glo_icd_eph(struct glo_eph_info* eph_info, unsigned int utc_secs,
					const struct glo_nav_model* nav_mdl, unsigned char num_nav)
{
	unsigned char i = 0;

	if(utc_secs < UTC_Y1996_M01_D01)
		return -1;

	for(i = 0; i < num_nav; i++) {
		struct glo_eph             *glo = &(eph_info->eph[i]);
		const struct glo_nav_model *nav =  nav_mdl + i;
		const struct glo_clk_model *clk = &(nav_mdl->clock);
		const struct glo_orb_model *orb = &(nav_mdl->orbit);

		glo->the_svid  = nav->the_svid + 65;
		glo->is_bcast  = nav->is_bcast;    /* Broadcast or predicted?   */

		glo->m         = 0;                /* Zero:as not used by firmware */
		glo->tk        = 0;                /* Zero:as not used by firmware */

		glo->tb        = nav->tb;          /* From IOD of ganns_nav_model  */
		glo->FT        = nav->FT;          /* Health param:ganss_nav_model */
		glo->Bn        = nav->Bn;          /* Health param:ganss_nav_model */

		glo->Gamma     = clk->Gamma;       /* From glo_clk_model */
		glo->Tau       = clk->Tau;         /* From glo_clk_model */
		glo->Delta_Tau = clk->Delta_Tau;   /* From glo_clk_model */

		eph_info->flags    |= GLO_CLK_MDL_3GPP;

		glo->M         = orb->M;           /* From glo_orb_model */
		glo->Xc        = orb->X;           /* From glo_orb_model */
		glo->Yc        = orb->Y;           /* From glo_orb_model */
		glo->Zc        = orb->Z;           /* From glo_orb_model */
		glo->Xv        = orb->X_dot;       /* From glo_orb_model */
		glo->Yv        = orb->Y_dot;       /* From glo_orb_model */
		glo->Zv        = orb->Z_dot;       /* From glo_orb_model */
		glo->Xa        = orb->X_dot_dot;   /* From glo_orb_model */
		glo->Ya        = orb->Y_dot_dot;   /* From glo_orb_model */
		glo->Za        = orb->Z_dot_dot;   /* From glo_orb_model */
		glo->En        = orb->En;          /* From glo_orb_model */
		glo->P1        = orb->P1;          /* From glo_orb_model */
		glo->P2        = orb->P2;          /* From glo_orb_model */

		eph_info->flags    |= GLO_CLK_MDL_3GPP;

		glo->P         = 0;                /* Zero:as not used by firmware */
		glo->NT        = utc2glo_NT_param(utc_secs); /* Calc from UTC  */
		glo->n         = nav->the_svid;

		glo->P3        = 0;
		glo->P4        = 0;

		glo->ln        = 0;
	}

	eph_info->n_sat = i;

	return 0;
}

unsigned int is_glo_icd_eph(const struct glo_eph_info* eph_info)
{
	return (eph_info->flags == GLO_ICD_EPH_INFO) ? 1 : 0;
}

/* Flags for GLONASS ICD ALM structure */
#define GLO_AKP_MDL_3GPP (0x01)
#define GLO_UTC_MDL_3GPP (0x02)
#define GAN_TIM_MDL_3GPP (0x04)
#define GLO_ICD_ALM_INFO (GLO_AKP_MDL_3GPP | GLO_UTC_MDL_3GPP | GAN_TIM_MDL_3GPP)

int add_kp2glo_icd_alm(struct glo_alm_info* alm_info, unsigned int utc_secs,
					   const struct glo_alm_kp_set *kp_set, unsigned char num_set)
{
	unsigned char i;

	if(utc_secs < UTC_Y1996_M01_D01)
		return -1;

	for(i = 0; i < num_set; i++) {
		const struct glo_alm_kp_set *kp  =  kp_set + i;
		struct glo_alm        *alm = &(alm_info->alm[i]);

		alm->N4               = utc2glo_N4_param(utc_secs);

		/*--------------------------------------------------------------
		 * Populate GLONASS ALM Info from 3GPP GLONAA Keplerian Model
		 *------------------------------------------------------------*/

		alm->NA               = kp->NA;

		alm->nA               = kp->nA;
		alm->HnA              = kp->HnA;
		alm->Lambda_nA        = kp->Lambda_nA;
		alm->t_Lambda_nA      = kp->t_Lambda_nA;
		alm->Delta_i_nA       = kp->Delta_i_nA;
		alm->Delta_T_nA       = kp->Delta_T_nA;
		alm->Delta_T_dot_nA   = kp->Delta_T_dot_nA;
		alm->Epsilon_nA       = kp->Epsilon_nA;
		alm->Omega_nA         = kp->Omega_nA;
		alm->Tau_nA           = kp->Tau_nA;
		alm->C_nA             = kp->C_nA;

		alm->M_nA             = kp->M_nA;
	}

	alm_info->n_sat  = i;
	alm_info->flags |= GLO_AKP_MDL_3GPP;

	return 0;
}

int add_utc2glo_icd_alm(struct glo_alm_info* alm_info, 
						const struct glo_utc_model* utc_mdl)
{
	unsigned char i, n_sat = MAX_SAT;

	if(alm_info->n_sat)
		n_sat = alm_info->n_sat;

	for(i = 0; i < n_sat; i++) {
		struct glo_alm        *alm = &(alm_info->alm[i]);

		/*--------------------------------------------------------------
		 * Populate GLONASS ALM Info from 3GPP GLONAA Keplerian Model
		 *------------------------------------------------------------*/

		alm->TauC  = utc_mdl->TauC;
		alm->B1    = utc_mdl->B1;
		alm->B2    = utc_mdl->B2;
		alm->KP    = utc_mdl->KP;
	}

	alm_info->flags |= GLO_UTC_MDL_3GPP;

	return 0;
}

static int glo2gps_time_3gpp(const struct ganss_ref_time* time_glo, 
							 struct gps_time* time_gps)
{
	/* GLONASS (local) Time in Seconds */
	unsigned int glo_now = glo_time_now(time_glo->day, time_glo->tod);

	unsigned int utc_now = glo2utc_time(glo_now);
	unsigned int elapsed = utc_now - UTC_AT_GPS_ORIGIN;
	unsigned int nr_week = elapsed / MAX_SECS_IN_1WEEK;
	unsigned int wk_secs = elapsed - nr_week * MAX_SECS_IN_1WEEK;

	time_gps->n_wk_ro  = nr_week / 1024;  /* # of roll overs of GPS time */
	time_gps->week_nr  = nr_week % 1024;  /* Num of weeks since, last RO */
	time_gps->tow      = wk_secs;         /* Seconds info into this week */
	time_gps->tow_unit = clear_secs;      /* Unit of tow in this  struct */

	return 0;
}

static int get_TauGPS(const struct ganss_ref_time gnss_time, 
					  const struct ganss_time_model time_mdl)
{
	struct gps_time time_gps;

	glo2gps_time_3gpp(&gnss_time, &time_gps);

	/* TBD */
	/* Use Estimated values of GPS time and co-efficients provided in 
	   GANSS Time Model (for GPS) to implement polynomial equation outlined 
	   in section 30.3.3.8.2 of GPS ICD 200E 
	 */
	return 0;

}

int add_gps2glo_icd_alm(struct glo_alm_info* alm_info, 
						const struct ganss_ref_time* glo_time, 
						const struct ganss_time_model* time_mdl)
{
	unsigned char i, n_sat = MAX_SAT;

	if(alm_info->n_sat)
		n_sat = alm_info->n_sat;

	for(i = 0; i < n_sat; i++) {
		struct glo_alm        *alm = &(alm_info->alm[i]);
		alm->TauGPS = get_TauGPS(*glo_time, *time_mdl);
	}

	alm_info->flags |= GAN_TIM_MDL_3GPP;

	return 0;
}

unsigned int is_glo_icd_alm(const struct glo_alm_info* alm_info)
{
	return (alm_info->flags == GLO_ICD_ALM_INFO) ? 1 : 0;
}

void reset_glo_icd_data(struct glo_eph_info *eph_info, 
						struct glo_alm_info *alm_info)
{
	eph_info->flags = alm_info->flags = 0;
	eph_info->n_sat = alm_info->n_sat = 0;
}
