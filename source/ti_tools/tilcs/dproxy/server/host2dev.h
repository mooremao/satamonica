/*
 * host2dev.h
 *
 * Framework to transfer the command/data from host to device.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#ifndef _HOST2DEV_H_
#define _HOST2DEV_H_

#include "rpc.h"

struct srv_rei;
typedef int (*srv_rei_rx_fn)(struct srv_rei*, struct ie_desc*);

struct srv_rei {

        unsigned short  elem_id;
        unsigned short  obj_len;
        srv_rei_rx_fn   rx_func;
};

#define REI_RX_DATA(data) ((void*)(data))

#define SRV_REI(rei, structure, rx_func, data)                \
        {rei, ST_SIZE(structure), rx_func}

struct no_obj {

};

#define NO_OBJECT no_obj

void *h2d_thread(void *arg);

#endif
