/*
 * error_handler.h
 *
 * Sequencer related reporting functions.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

/* Function declarations. */
static int d2h_ai2_decode(unsigned char *buf, unsigned int *data, 
							  char *sizes);




