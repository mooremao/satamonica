/*
 * utc_report.c
 *
 * UTC report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "nvs.h"
#include "log_report.h"

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int utc_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_utc *dev_utc);

int utc_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_utc *dev_utc);

int utc_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_utc *dev_utc);

int utc_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_utc *dev_utc);

static struct ie_desc *mk_ie_adu_4utc_gps_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_gps_utc *dev_utc)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_gps_utc);
	struct dev_gps_utc *new_utc = (struct dev_gps_utc *) malloc(st_sz);
	if (!new_utc)
		goto exit1;

	memcpy(new_utc, dev_utc, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GPS_UTC, oper_id, st_sz, new_utc);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_utc);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4utc_gps_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_gps_utc *dev_utc)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(gps_utc);
	struct gps_utc *new_utc = (struct gps_utc *) malloc(st_sz);
	if (!new_utc)
		goto exit1;

	new_utc->A1        = dev_utc->A1;
	new_utc->A0        = dev_utc->A0;
	new_utc->Tot       = dev_utc->Tot;
	new_utc->WNt       = dev_utc->WNt;
	new_utc->DeltaTls  = dev_utc->DeltaTls;
	new_utc->WNlsf     = dev_utc->WNlsf;
	new_utc->DN        = dev_utc->DN;
	new_utc->DeltaTlsf = dev_utc->DeltaTlsf;

	ie_adu = rpc_ie_attach(rmsg, REI_GPS_UTC, oper_id, st_sz, new_utc);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_utc);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4utc_glo_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_glo_utc *dev_utc)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_glo_utc);
	struct dev_glo_utc *new_utc = (struct dev_glo_utc *) malloc(st_sz);
	if (!new_utc)
		goto exit1;

	memcpy(new_utc, dev_utc, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GLO_UTC, oper_id, st_sz, new_utc);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_utc);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4utc_glo_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_glo_utc *dev_utc)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(glo_utc_model);
	struct glo_utc_model *new_utc = (struct glo_utc_model *) malloc(st_sz);
	if (!new_utc)
		goto exit1;

	//new_utc->NA   =;
	//new_utc->TauC =;
	new_utc->B1   = dev_utc->B1;
	new_utc->B2   = dev_utc->B2;
	new_utc->KP   = dev_utc->KP;

	ie_adu = rpc_ie_attach(rmsg, REI_GLO_UTC, oper_id, st_sz, new_utc);
	if (!ie_adu)
		goto exit2;

	UNUSED(dev_utc);

	return ie_adu;

exit2:
	free(new_utc);

exit1:
	return NULL;
}


/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int utc_gps_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_gps_utc *dev_utc)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4utc_gps_dev(rmsg, oper_id, dev_utc);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int utc_gps_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_gps_utc *dev_utc)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4utc_gps_3gpp(rmsg, oper_id, dev_utc);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int utc_glo_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_glo_utc *dev_utc)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4utc_glo_dev(rmsg, oper_id, dev_utc);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int utc_glo_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_glo_utc *dev_utc)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4utc_glo_3gpp(rmsg, oper_id, dev_utc);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int utc_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_utc *dev_utc)
{
	return utc_gps_create_dev_ie(rmsg, oper_id, dev_utc);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int utc_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_utc *dev_utc)
{
	return utc_gps_create_3gpp_ie(rmsg, oper_id, dev_utc);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int utc_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_utc *dev_utc)
{
	return utc_glo_create_dev_ie(rmsg, oper_id, dev_utc);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int utc_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_utc *dev_utc)
{
	return utc_glo_create_3gpp_ie(rmsg, oper_id, dev_utc);
}

static int utc_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  unsigned int rei_select)
{
	struct dev_gps_utc *utc_gps = (struct dev_gps_utc*) ai2_d2h->ai2_struct;
	struct dev_glo_utc *utc_glo = BUF_OFFSET_ST(utc_gps, struct dev_gps_utc,
												struct dev_glo_utc);

	if (d2h_get_msg_id(ai2_d2h) == 0x0D) {
		if (rei_select & NEW_UTC_GPS_NATIVE) {
			if (-1 == utc_gps_create_dev_ie(rmsg, OPER_INF, utc_gps))
				return -1;
		} else if (rei_select & NEW_UTC_GPS_IE_IND) {
			if (-1 == utc_gps_create_3gpp_ie(rmsg, OPER_INF, utc_gps))
				return -1;
		}
	} else {
		if (rei_select & NEW_UTC_GLO_NATIVE) {
			if (-1 == utc_glo_create_dev_ie(rmsg, OPER_INF, utc_glo))
				return -1;
		} else if (rei_select & NEW_UTC_GLO_IE_IND) {
			if (-1 == utc_glo_create_3gpp_ie(rmsg, OPER_INF, utc_glo))
				return -1;
		}
	}

	return 0;
}

static int dev_gps_utc_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_utc *utc_gps = (struct dev_gps_utc *)ai2_d2h->ai2_struct;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;
	int ai2_pkt_sz               = ai2_d2h->ai2_pkt_sz;

	if (ai2_offset < ai2_pkt_sz) {
		utc_gps->A0        = BUF_LE_S4B_RD_INC(ai2_pkt);
		utc_gps->A1        = BUF_LE_S4B_RD_INC(ai2_pkt);
		utc_gps->DeltaTls  = BUF_LE_S1B_RD_INC(ai2_pkt);
		utc_gps->Tot       = BUF_LE_U1B_RD_INC(ai2_pkt);
		utc_gps->WNt       = BUF_LE_U1B_RD_INC(ai2_pkt);
		utc_gps->WNlsf     = BUF_LE_U1B_RD_INC(ai2_pkt);
		utc_gps->DN        = BUF_LE_U1B_RD_INC(ai2_pkt);
		utc_gps->DeltaTlsf = BUF_LE_S1B_RD_INC(ai2_pkt);

		/* Dump the decoded data to logger. */
		print_data(REI_DEV_GPS_UTC, utc_gps);
	}

	return (ai2_pkt - ai2_ref);
}

static int dev_glo_utc_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_utc *utc_gps = (struct dev_gps_utc*) ai2_d2h->ai2_struct;
	struct dev_glo_utc *utc_glo = BUF_OFFSET_ST(utc_gps, struct dev_gps_utc,
												struct dev_glo_utc);
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;
	int ai2_pkt_sz               = ai2_d2h->ai2_pkt_sz;

	if (ai2_offset < ai2_pkt_sz) {
		utc_glo->B1 = BUF_LE_U2B_RD_INC(ai2_pkt);
		utc_glo->B2 = BUF_LE_U2B_RD_INC(ai2_pkt);
		utc_glo->KP = BUF_LE_U1B_RD_INC(ai2_pkt);

		/* Reserved */
		ai2_pkt += 2;

		/* Dump the decoded data to logger. */
		print_data(REI_DEV_GLO_UTC, utc_glo);
	}

	return (ai2_pkt - ai2_ref);
}

static int utc_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if (d2h_get_msg_id(ai2_d2h) == 0x0D) {
		if (ai2_offset < ai2_pkt_sz)
			ai2_offset += dev_gps_utc_struct(ai2_d2h, ai2_offset);
	} else {
		if (ai2_offset < ai2_pkt_sz)
			ai2_offset += dev_glo_utc_struct(ai2_d2h, ai2_offset);
	}

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int utc_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	struct dev_gps_utc *utc_gps = (struct dev_gps_utc*) ai2_d2h->ai2_struct;
	struct dev_glo_utc *utc_glo = BUF_OFFSET_ST(utc_gps, struct dev_gps_utc,
												struct dev_glo_utc);
	int wr_suc = 0;

	LOGPRINT(utc_rpt, info, "storing utc in nvs");

	if (d2h_get_msg_id(ai2_d2h) == 0x0D)
		wr_suc = nvs_rec_add(nvs_gps_utc, utc_gps, 1);
	else
		wr_suc = nvs_rec_add(nvs_glo_utc, utc_glo, 1);

	if (wr_suc != nvs_success) {
		LOGPRINT(utc_rpt, err, "error storing utc in nvs");
		return -1;
	}

	return 0;
}

#define EXT_PVT_ST_LEN (ST_SIZE(dev_gps_utc) + ST_SIZE(dev_glo_utc))

struct d2h_ai2 d2h_utc_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.rei_select = (NEW_UTC_GPS_IE_IND | NEW_UTC_GLO_IE_IND),

	.gen_struct = utc_ext_struct,
	.do_ie_4rpc = utc_rpc_ie,
	.nvs_action = utc_nvs_info
};

