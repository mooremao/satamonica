
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "error_handler.h"
#include "ai2.h"
#include "dev2host.h"
#include "rpc.h"
#include "os_services.h"
#include "utils.h"
#include "logger.h"
#include "log_report.h"
#include "config.h"
#include "dev_proxy.h"
#include "report.h"
#include "inject.h"


extern unsigned int time_start;

/* Error packet. */
struct ai2_error_pkt {
	unsigned int id;
	unsigned int len;
	unsigned int inv_pkt_id;
	enum dev_err_id  err_type;
};

static struct ai2_error_pkt err_obj;

extern int h2d_ai2_hdl;

/* Generic decoder. */
static int d2h_ai2_decode(unsigned char *buf, unsigned int *data, char *sizes)
{
	if (data == 0 || sizes == 0)
		return ERR;

	while(*sizes != 0) {
		*data = ai2_get_nbytes(&buf, *sizes);
		data++;
		sizes++;
	}

	return OK;
}

char ai2_inv_pkt_sizes[] = {1, 2, 1, 1, 0};

/* 0xF5 Invalid packet. */
static int dpxy_decode_invalid_pkt(struct d2h_ai2 *d2h_ai2)
{
	struct ai2_error_pkt *pkt = d2h_ai2->ai2_struct;
	char *pkt_err[] = {	"invalid message descriptor\0",
		"invalid message length\0",
		"invalid message checksum\0",
		"unknown error\0",
		"invalid packet id\0",
		"invalid packet data\0",
		"invalid packet length\0",
		"invalid software state transition commanded\0"
	};

	d2h_ai2_decode(d2h_ai2->ai2_packet, (unsigned int *)pkt,
							ai2_inv_pkt_sizes);

	LOGPRINT(d2h, err, "packet error: id %X %s", pkt->inv_pkt_id,
			 pkt_err[pkt->err_type]);

	err_obj.err_type = gnss_invalid_msg_err;

	return OK;
}

/* 0xF6 Fatal Error packet. */
static int dpxy_decode_fatal_err(struct d2h_ai2 *d2h_ai2)
{
	struct ai2_error_pkt *pkt = d2h_ai2->ai2_packet;
	char *ai2_pkt = (char*) d2h_ai2->ai2_packet; 
	unsigned int line;
	unsigned char file[20];

	LOGPRINT(d2h, info, "Pkt Sz =%d",d2h_ai2->ai2_pkt_sz);
	pkt += (1 + 2);

	pkt->err_type = gnss_fatal_err;
	ai2_pkt += 3;

	line = BUF_LE_U4B_RD_INC(ai2_pkt);

	memcpy(file, ai2_pkt, 20);

	LOGPRINT(d2h, err, "err_seq: fatal error @ line %d in file %s", line, file);

	return OK;
}

/* Auto sleep  */
static int dpxy_parse_auto_sleep_pkt(struct d2h_ai2 *d2h_ai2)
{
	LOGPRINT(d2h, info, "auto sleep response received");
	return OK;
}

int dpxy_auto_sleep_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  	   unsigned int rei_select)
{
	/* Prepare RPC to delegate the error message */
	struct ie_desc *adu;
	rmsg = rpc_msg_alloc();
	int st_sz = 4;
	unsigned int *error = (unsigned int *)malloc(st_sz);
	
	*error = err_obj.err_type;
	memset(&err_obj,0x00,sizeof(struct ai2_error_pkt));

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_DEV_ERR_INF, OPER_INF, st_sz, error)))
		goto exit2;


exit2:
	rpc_msg_free(rmsg);

exit1:
	return 0;
	
}

int dpxy_invalid_pkt_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  	   unsigned int rei_select)
{
	/* Prepare RPC to send the error message */
	struct ie_desc *adu;
	rmsg = rpc_msg_alloc();
	int st_sz = 4;
	struct ai2_error_pkt *pkt = ai2_d2h->ai2_packet;
	
	unsigned int *error = (unsigned int *)malloc(st_sz);
	*error = err_obj.err_type;

	memset(&err_obj,0x00,sizeof(struct ai2_error_pkt));

	if(!rmsg)
		goto exit1;

	if(!(adu = rpc_ie_attach(rmsg, REI_DEV_ERR_INF, OPER_INF, st_sz, error)))
		goto exit2;


exit2:
	rpc_msg_free(rmsg);

exit1:
	return 0;
	
}

int dpxy_fatal_error_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
							   unsigned int rei_select)
{

	struct ai2_error_pkt *ai2_err_pkt = (struct ai2_error_pkt *)ai2_d2h->ai2_struct;
	struct ie_desc *ie_adu[1];
	int n_adu = 0;
	int st_sz = 4;
	unsigned int *dev_er = (unsigned int *)malloc(st_sz);
	if (!dev_er)
		goto exit1;

	*dev_er = gnss_fatal_err;
	
	LOGPRINT(d2h, info, "err_seq: dpxy_fatal_error_ie: error type %d",*dev_er);

	ie_adu[n_adu] = rpc_ie_attach(rmsg, REI_DEV_ERR_INF, OPER_INF, st_sz, dev_er);
	if (!ie_adu[n_adu++])
		goto exit2;

	UNUSED(rei_select);

	return 0;

exit2:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

exit1:
	free(dev_er);

	return -1;
}

/* these fatal/exception inject packets are stimuli packets to test the error recovery, 
    intend to keep to debug further later with enhancements */
    
int dpxy_inj_fatal_pkt1(void *data)
{
		struct ai2_fatal_inj_pkt {
			unsigned int id;
			unsigned int b0;
			unsigned int b1;
			unsigned int b2;
			unsigned int b3;
			unsigned int b4;
			unsigned int b5;
			unsigned int b6;
			unsigned int b7;
			unsigned int b8;
			unsigned int b9;
			unsigned int b10;
						
		} fatal[1];
		char ai2_fatal_inj_pkt_sizes[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0};
		unsigned char mem[12];
	
		fatal->id  = 0xFB;
		fatal->b0  = 0x09;
		fatal->b1  = 0x00;
		fatal->b2  = 0xD0;
		fatal->b3  = 0x10;
		fatal->b4  = 0x1B;
		fatal->b5  = 0xC9;
		fatal->b6  = 0x10;
		fatal->b7  = 0x00;
		fatal->b8  = 0x01; 
		fatal->b9  = 0x01;
		fatal->b10 = 0x01;		
	
		d2h_ai2_encode((void *)fatal, mem, ai2_fatal_inj_pkt_sizes);
	
		if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 12)) {
			if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
							LOGPRINT(h2d, info, "err_seq: fatal error packet1 injected");
				return OK;
			}
		}
	
		UNUSED(data);
		LOGPRINT(h2d, err, "error sending ai2 packet to device");
		return ERR;
}

int dpxy_inj_fatal_pkt2(void *data)
{
		struct ai2_fatal_inj_pkt {
			unsigned int id;
			unsigned int len;
			unsigned int b0;
			unsigned int b1;
			unsigned int b2;

		} fatal[1];
		char ai2_fatal_inj_pkt_sizes[] = {1, 1, 1, 1, 1, 0};
		unsigned char mem[5];
	
		fatal->id 	= 0xFB;
		fatal->len 	= 0x02;
		fatal->b0 	= 0x00;
		fatal->b1   = 0xD0;
		fatal->b2   = 0x03;
	
		d2h_ai2_encode((void *)fatal, mem, ai2_fatal_inj_pkt_sizes);
	
		if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 5)) {
			if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
							LOGPRINT(h2d, info, "err_seq: fatal error packet2 injected");
				return OK;
			}
		}
	
		UNUSED(data);
		LOGPRINT(h2d, err, "error sending ai2 packet to device");
		return ERR;
}


int dpxy_inj_exception_pkt(void *data)
{
	struct ai2_exception_inj_pkt {
		unsigned int id;
		unsigned int b0;
		unsigned int b1;
		unsigned int b2;
		unsigned int b3;
		unsigned int b4;
		unsigned int b5;
		unsigned int b6;
		unsigned int b7;
		unsigned int b8;
		unsigned int b9;
		unsigned int b10;
		unsigned int b11;
		unsigned int b12;
		unsigned int b13;
		
	} exception[1];
	char ai2_exception_inj_pkt_sizes[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0};
	unsigned char mem[15];
	
	exception->id  = 0xFB;
	exception->b0  = 0x0C;
	exception->b1  = 0x00;
	exception->b2  = 0xD0;
	exception->b3  = 0x10;
	exception->b4  = 0x00;
	exception->b5  = 0x00;
	exception->b6  = 0x00;
	exception->b7  = 0x00;
	exception->b8  = 0x04;
	exception->b9  = 0x01;
	exception->b10 = 0x01;
	exception->b11 = 0x00;
	exception->b12 = 0x00;
	exception->b13 = 0x00;
	
	d2h_ai2_encode((void *)exception, mem, ai2_exception_inj_pkt_sizes);
	
	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 15)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
						LOGPRINT(h2d, info, "err_seq: exception error packet injected");
			return OK;
		}
	}
	
	UNUSED(data);
	LOGPRINT(h2d, err, "error sending ai2 packet to device");
	return ERR;

}

/* 0xF5 packet , disabled currently to send to client */
/*.do_ie_4rpc  = dpxy_invalid_pkt_ie*/

struct d2h_ai2 d2h_inv_pkt_err = {
	.ai2_st_len = sizeof(struct ai2_error_pkt),
	.gen_struct = dpxy_decode_invalid_pkt,
};

/* 0xF6 packet */
struct d2h_ai2 d2h_fatal_err = {
	.ai2_st_len = sizeof(struct ai2_error_pkt),
	.gen_struct = dpxy_decode_fatal_err,
	.do_ie_4rpc	 = dpxy_fatal_error_ie
};

/* cpu exception 0xD4 , disabled currently to send to client*/
/*.do_ie_4rpc  = dpxy_auto_sleep_ie*/

struct d2h_ai2 d2h_exception_err = {
	.ai2_st_len = sizeof(struct ai2_error_pkt),
	.gen_struct = dpxy_parse_auto_sleep_pkt,
};


