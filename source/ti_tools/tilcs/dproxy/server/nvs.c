#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#include <fcntl.h>

#include "os_services.h"
#include "nvs.h"
#include "logger.h"
#include "device.h"
#include "utils.h"
#include "config.h"
#include "rpc.h"
#include "log_report.h"

enum rec_op {

	rec_write,
	rec_read,
	rec_delete,
	rec_end
};

struct obj_head {

	unsigned int magic_id;
	unsigned int obj_time;
	unsigned int obj_time_ms;
	unsigned int chk_sum;
};
/*
   OBJECT ==> HEAD + RECORD
 */

struct nvs_rec_desc {
	/* Describe each of the member */
	char      *name;

	enum nvs_record     record;  /* Record type */
	int                 rec_sz;  /* Record Size */
	unsigned int	    n_objs;  /* Num of record objects */
	nvs_rec_find_oid    get_oid; /* Get Object ID i.e. oid */
	enum nvs_mcache     mcache;
	FILE    	        *nvs_fp;
	char                *buffer;
};


typedef int (*fop_fn_ptr)(void*, size_t, size_t, FILE*);
typedef int (*op_fn_ptr)(struct nvs_rec_desc*, char*, int);

struct rec_operations {

	op_fn_ptr       ops[rec_end];
	fop_fn_ptr      f_ops[rec_end];
	void            *rec_mutex;
};


#define OBJECT_HEADER_MARKER 0x5A5AA5A5

#define OBJ_SIZE(rec_desc) (rec_desc->rec_sz + sizeof(struct obj_head))

#define OBJ_OFST(rec_desc, obj_idx) OBJ_SIZE(rec_desc) * obj_idx

#define OBJ_HEAD(rec_desc, obj_idx)                     \
	(rec_desc->buffer + OBJ_OFST(rec_desc, obj_idx))

#define OBJ_DATA(rec_desc, obj_idx)                             \
	(OBJ_HEAD(rec_desc, obj_idx) + sizeof(struct obj_head))

static struct nvs_rec_desc rec_tbl [] = {

	{.record = nvs_gps_pos,    .name = "GPSPosFile"	},
	{.record = nvs_gps_eph,    .name = "GPSEphFile"	},
	{.record = nvs_gps_alm,    .name = "GPSAlmFile"	},
	{.record = nvs_gps_iono,   .name = "GPSIonoFile"},
	{.record = nvs_gps_utc,    .name = "GPSUtcFile"	},
	{.record = nvs_gps_tcxo,   .name = "GPSTcxoFile"},
	{.record = nvs_gps_clk,    .name = "GPSClkFile"	},
	{.record = nvs_gps_svdr,   .name = "GPSSVDrFile"},
	{.record = nvs_glo_eph,    .name = "GLOEphFile"	},
	{.record = nvs_glo_alm,    .name = "GLOAlmFile"	},
	{.record = nvs_glo_svdr,   .name = "GLOSVDrFile"},
	{.record = nvs_glo_clk,    .name = "GLOClkFile"	},
	{.record = nvs_glo_utc,    .name = "GLOUtcFile"	},
	{.record = nvs_glo_sv_slot,    .name = "GloSvSlotMapFile"},
	{.record = nvs_hlth,       .name = "HlthFile"},
	{.record = nvs_rec_end,    .name = "GPS_END"	}
};

/* Forward declaration */
int rec_file_ops(struct nvs_rec_desc* rec_desc, int obj_idx,
				 enum rec_op op);

static unsigned int checksum(char *data, int len)
{
	unsigned int chk_sum = 0;
	while ( len--)
		chk_sum += *data++;
	return chk_sum;
}
/* delete complete objects */
static int rec_del(struct nvs_rec_desc* rec_desc, char *rec_data, int obj_idx)
{
	memset(OBJ_HEAD(rec_desc, obj_idx), 0, OBJ_SIZE(rec_desc));

	UNUSED(rec_data);

	/* if write thru is set  then do NVS store */
	if (rec_desc->mcache == write_thru)
		return rec_file_ops(rec_desc, obj_idx, rec_write);
	
	return nvs_success;
}

/* store obj_data along with marker and timestamp into obj's memory */
static int rec_wr(struct nvs_rec_desc* rec_desc, char *rec_data, int obj_idx)
{
	struct obj_head *head = (struct obj_head*) OBJ_HEAD(rec_desc, obj_idx);

	head->magic_id  = OBJECT_HEADER_MARKER;
	os_time_value(&head->obj_time, &head->obj_time_ms);
	head->chk_sum   = checksum(rec_data, rec_desc->rec_sz);

	memcpy(OBJ_DATA(rec_desc, obj_idx), rec_data, rec_desc->rec_sz);

	/* if write thru is set then do NVS store */
	if (rec_desc->mcache == write_thru)
		return rec_file_ops(rec_desc, obj_idx, rec_write);
	
	return nvs_success;
}

/* get the obj_data from the record obj's memory. Returns 0 on success.
   Set Record Object to 0, if it is not present or TTL is not valid */
static int rec_rd(struct nvs_rec_desc* rec_desc, char *rec_data, int obj_idx)
{
#define OBJ_TTL(obj_time) (unsigned int)(os_time_secs()- obj_time)

	struct obj_head *head = (struct obj_head*) OBJ_HEAD(rec_desc, obj_idx);

	if (head->magic_id != OBJECT_HEADER_MARKER)
		return nvs_error;
	else
		memcpy(rec_data, OBJ_DATA(rec_desc, obj_idx), rec_desc->rec_sz);

	if (head->chk_sum != checksum(rec_data, rec_desc->rec_sz))
		return nvs_error;
	return nvs_success;
}

static struct rec_operations rec_oper = {

	.ops[rec_read]    = rec_rd,
	.ops[rec_write]   = rec_wr,
	.ops[rec_delete]  = rec_del,
	.f_ops[rec_read]  = (fop_fn_ptr)fread,
	.f_ops[rec_write] = (fop_fn_ptr)fwrite,
};



/* File read write operations, nvs_success on success nvs_error on failure */
int rec_file_ops(struct nvs_rec_desc* rec_desc, int obj_idx, enum rec_op op)
{
	int   len = OBJ_SIZE(rec_desc);
	int   ofs = OBJ_OFST(rec_desc, obj_idx);
	char *buf = OBJ_HEAD(rec_desc, obj_idx);
	void *stat = os_get_stat();
	char nm_buf[128];
	int ret_val = nvs_error;

	/* Full file path name. */
	snprintf(nm_buf, 128, "%s%s", NVS_FILE_PATH, rec_desc->name);

	if(stat == NULL){
		LOGPRINT(nvs, err, "stat could not be allocated ");
		goto error1;
	}

	if (os_stat_file(nm_buf, stat)) {
		LOGPRINT(nvs, err, "status req on %s failed", rec_desc->name);
        goto error;
	}

	if (((op == rec_read) && (os_file_size(stat) >= (ofs + len))) || (op != rec_read)) {

		if (fseek(rec_desc->nvs_fp, ofs, SEEK_SET) < 0) {
			LOGPRINT(nvs, err, "fseek on %s failed", rec_desc->name);
			goto error;
		}

		/* Perform the requested file operation */
		if (rec_oper.f_ops[op](buf, sizeof(char), len,
						rec_desc->nvs_fp) != len) {
			LOGPRINT(nvs, err, "oper on %s failed",	rec_desc->name);
			goto error;
		}

		LOGPRINT(nvs, info, "successfully read/wrote %s index %d", rec_desc->name, obj_idx);

		//fflush(rec_desc->nvs_fp);
	} else if (os_file_size(stat)) {
		LOGPRINT(nvs, info, "rec_file_ops unsuccessfull");                          
		goto  error;
	}

	ret_val = nvs_success;

	LOGPRINT(nvs, info, "rec_file_ops successfull");

  error:	
	os_free_stat(stat);
	return ret_val;

  error1:
	return ret_val;  	
}

/* rec obj's read write and del ops, returns obj's ops success */
static int rec_ops(struct nvs_rec_desc *rec_desc, unsigned int flags,
				   char *rec_data, enum rec_op op)
{
	unsigned int obj_idx, obj_count = 0;

	for (obj_idx = 0; obj_idx < rec_desc->n_objs && flags; obj_idx++) {

		if (!(flags & (1 << obj_idx)))
			continue; /* No object at this index */

		flags &= ~(1 << obj_idx); /* clear the idx's oper completed */

		os_mutex_take(rec_oper.rec_mutex);

		/* perform the requested operation */
		if (rec_oper.ops[op](rec_desc, rec_data, obj_idx)) {
			os_mutex_give(rec_oper.rec_mutex);
			continue;
		}

		os_mutex_give(rec_oper.rec_mutex);

		rec_data += rec_desc->rec_sz; /* Next */

		obj_count++;
	}

	return obj_count;
}

/* find the record descriptor */
static struct nvs_rec_desc *find_rec_desc(enum nvs_record rec)
{
	struct nvs_rec_desc *rec_desc = rec_tbl;

	while (rec_desc->record != nvs_rec_end) {
		if (rec_desc->record == rec)
			return rec_desc;

		rec_desc++;
	}

	LOGPRINT(nvs, err, "find_rec_desc: enum [%d] not found", rec);
	return NULL;
}

int nvs_rec_setup(enum nvs_record rec, struct nvs_rec_param *param)
{
	struct nvs_rec_desc *rec_desc = find_rec_desc(rec);
	unsigned int obj_idx = 0;

	if (!rec_desc)
		return nvs_error;

	rec_desc->rec_sz      = param->rec_sz;
	rec_desc->n_objs      = param->n_objs;
	rec_desc->mcache      = param->mcache;
	rec_desc->get_oid     = param->get_oid;

	LOGPRINT(nvs, info, "nvs_rec_setup: name        [%s]", rec_desc->name);
	LOGPRINT(nvs, info, "nvs_rec_setup: rec_sz      [%d]", rec_desc->rec_sz);
	LOGPRINT(nvs, info, "nvs_rec_setup: n_objs      [%d]", rec_desc->n_objs);
	LOGPRINT(nvs, info, "nvs_rec_setup: obj_data_sz [%d]",
			 (OBJ_SIZE(rec_desc) * rec_desc->n_objs));

	/* allocate the obj_data for each record */
	rec_desc->buffer = (char*) calloc(rec_desc->n_objs, OBJ_SIZE(rec_desc));
	if (!rec_desc->buffer) {
		LOGPRINT(nvs, err, "record setup no memory");
		return nvs_error;
	}

	/* read all the objects from file to memory  */
	while (obj_idx < rec_desc->n_objs) {
		if (rec_file_ops(rec_desc, obj_idx, rec_read)) {
			LOGPRINT(nvs, err, "record setup fail");
			return nvs_error;
		}
		obj_idx++;
	}

	return nvs_success;
}

void nvs_flush(void)
{
	struct nvs_rec_desc *rec_desc;
	int rec = 0;

	for(rec=0; rec < nvs_rec_end; rec++){
		rec_desc = find_rec_desc(rec);
                if(NULL!=rec_desc)
                        fflush(rec_desc->nvs_fp);
	}
}


int nvs_rec_add(enum nvs_record rec, void *rec_data, unsigned int num_obj)
{
	int wr_suc = 0;
	struct nvs_rec_desc *rec_desc = find_rec_desc(rec);

	if (!rec_desc || num_obj > rec_desc->n_objs) {

		LOGPRINT(nvs, err, 
			"nvs_rec_add: Exiting [%d] enum not found or invalid num of objs",
			nvs_error);

		if (rec_desc)
			LOGPRINT(nvs, err, 
			"nvs_rec_add: name [%s],  num_obj [%d] and rec_desc->n_objs [%d]", 
			rec_desc->name, num_obj, rec_desc->n_objs);

		return nvs_error;
	}

	while (num_obj--) {
		unsigned int obj_idx = rec_desc->get_oid(rec_data);
		if (obj_idx > rec_desc->n_objs) {
			LOGPRINT(nvs, err, "nvs_rec_add: Exiting [%d] oid mismatch", nvs_error);
			return nvs_error;
		}

		wr_suc += rec_ops(rec_desc, (1 << --obj_idx),
						  rec_data, rec_write);

		//	rec_data += rec_desc->rec_sz;
		rec_data = (char *)rec_data + rec_desc->rec_sz;
	}

	return nvs_success;
}

int nvs_rec_get(enum nvs_record rec, void *rec_data, unsigned int flags)
{
	struct nvs_rec_desc *rec_desc = find_rec_desc(rec);

	if (!rec_desc)
		return nvs_error;

	return rec_ops(rec_desc, flags, rec_data, rec_read);
}

unsigned long nvs_rec_ts_s(enum nvs_record rec, void *rec_data)
{

	struct nvs_rec_desc *rec_desc = find_rec_desc(rec);

	if (!rec_desc)
		return 0;

	unsigned int obj_idx =  rec_desc->get_oid(rec_data);

	if (obj_idx > rec_desc->n_objs)
		return 0;

	os_mutex_take(rec_oper.rec_mutex);
	struct obj_head *head = (struct obj_head*) OBJ_HEAD(rec_desc, --obj_idx);
	os_mutex_give(rec_oper.rec_mutex);

	if (head->magic_id != OBJECT_HEADER_MARKER) 
		return 0;

	return head->obj_time;
}

int nvs_rec_ts_ms(enum nvs_record rec, void *rec_data, unsigned long long *data)
{
	struct nvs_rec_desc *rec_desc = find_rec_desc(rec);
	*data = 0ULL;

	if (!rec_desc)
		return 0;

	unsigned int obj_idx =  rec_desc->get_oid(rec_data);

	if (obj_idx > rec_desc->n_objs)
		return 0;

	os_mutex_take(rec_oper.rec_mutex);
	struct obj_head *head = (struct obj_head*) OBJ_HEAD(rec_desc, --obj_idx);
	os_mutex_give(rec_oper.rec_mutex);

	if (head->magic_id != OBJECT_HEADER_MARKER) 
		return 0;

#ifdef VERBOSE_LOGGING
	if (rec == nvs_gps_clk)
		LOGPRINT(nvs, info, "nvs_rec_ts_ms: sec [%d], msec [%d] and total [%llu]", 
			 head->obj_time, head->obj_time_ms, 
			 ((((unsigned long long)head->obj_time) * 1000ULL) 
			 + ((unsigned long long)head->obj_time_ms)));
#endif
	*data = (((unsigned long long)head->obj_time) * 1000ULL) 
		+ ((unsigned long long)head->obj_time_ms);

	return 1;
}

int nvs_rec_del(enum nvs_record rec, unsigned int flags)
{
	struct nvs_rec_desc *rec_desc = find_rec_desc(rec);

	if (!rec_desc)
		return nvs_error;

	return rec_ops(rec_desc, flags, NULL, rec_delete);
}

int nvs_init(struct nvs_config *cfg)
{
	struct nvs_rec_desc *rec_desc = rec_tbl;

	/* Loop through all the records */
	while (rec_desc->record != nvs_rec_end) {

		char buf[128];

		/* Open the files for the obj_data storage (NVS) */
		snprintf(buf, 128, "%s%s", NVS_FILE_PATH, rec_desc->name);

		/* Check if file exists */
		if (os_is_file_exists(buf)) {
			rec_desc->nvs_fp = fopen(buf, "wb+");
			if (rec_desc->nvs_fp) {
				LOGPRINT(nvs, info, "creating nvs %s",
								rec_desc->name);
			} else {
				LOGPRINT(nvs, err, "create %s fail",
								rec_desc->name);
			}
		} else {
			rec_desc->nvs_fp = fopen(buf, "rb+");
			if (rec_desc->nvs_fp) {
				LOGPRINT(nvs, info, "opening nvs %s",
								rec_desc->name);
			} else {
				LOGPRINT(nvs, err, "open %s fail",
								rec_desc->name);
			}
		}

		if (!rec_desc->nvs_fp)
			return nvs_error;

		/* next record */
		rec_desc++;
	}

	/* initalise mutex for complete nvs record */
	if (!(rec_oper.rec_mutex = os_mutex_init()))
		return nvs_error;

	UNUSED(cfg);
	return nvs_success;
}


void nvs_exit(void)
{
	struct nvs_rec_desc *rec_desc = rec_tbl;
	unsigned int obj_idx = 0;

	/* loop through all the records */
	while (rec_desc->record != nvs_rec_end) {

		/* flush all ids of record into nvs, if write back is set */
		if (rec_desc->mcache == write_back)
			while (obj_idx < rec_desc->n_objs) {
				rec_file_ops(rec_desc, obj_idx, rec_write);
				obj_idx++;
			}

		/* close the file */
		if (rec_desc->nvs_fp)
			fclose(rec_desc->nvs_fp);

		/* free the obj_data buffer pointer */
		if (rec_desc->buffer)
			free(rec_desc->buffer);

		/* next record */
		rec_desc++;
	}

	/* destroy mutex for complete nvs record */
	os_mutex_exit(rec_oper.rec_mutex);
}

