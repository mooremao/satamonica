/*
 * ai2_comm.h
 *
 *  This file describes the interface that abstracts the transport mechanism 
 *  used to communicate with the GPS chip.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _AI2_COMM_H
#define _AI2_COMM_H

/** Open communication channel for reading or writing to device.
    @return 0  on success or errorcode
*/
int ai2_comm_open(void);

/** Transfer the buffer to the device.
    @param[in] buf is the data buffer
    @param[in] len is the length of 'buf'
    @return 0  on success or errorcode
*/
int ai2_comm_tx(unsigned const char *buf, unsigned int len);

/** Read data from the device and stores it into buffer.
    @param[in] buf is the data buffer for reading into
    @param[in/out] len is the length of 'buf' or number of bytes read`
    @return 0  on success or errorcode
*/
int ai2_comm_rx(unsigned char *buf, unsigned int *len);


int ai2_comm_rx_reset(void);

/** Close communication channel.
    @return 0  on success or errorcode
*/
int ai2_comm_close(void);


#endif
