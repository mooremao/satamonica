/*
 * $Header: /FoxProjects/FoxSource/win32/LocationManager 1/10/04 7:53p Lleirer $
 ******************************************************************************
 *  Copyright (C) 1999 SnapTrack, Inc.

 *

 *                  SnapTrack, Inc.

 *                  4040 Moorpark Ave, Suite 250

 *                  San Jose, CA  95117

 *

 *     This program is confidential and a trade secret of SnapTrack, Inc. The

 * receipt or possession of this program does not convey any rights to

 * reproduce or disclose its contents or to manufacture, use or sell anything

 * that this program describes in whole or in part, without the express written

 * consent of SnapTrack, Inc.  The recipient and/or possessor of this program

 * shall not reproduce or adapt or disclose or use this program except as

 * expressly allowed by a written authorization from SnapTrack, Inc.

 *

 *

 ******************************************************************************/


/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


  L O C A T I O N   S E R V I C E S   M A N A G E R   M O D U L E


  Copyright (c) 2002 by QUALCOMM INCORPORATED. All Rights Reserved.



  Export of this technology or software is regulated by the U.S. Government.

  Diversion contrary to U.S. law prohibited.

 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*******************************************************************************

  TI GPS Confidential

 *******************************************************************************/
/*
 *******************************************************************************
 *         TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION
 *
 * Property of Texas Instruments, Unauthorized reproduction and/or distribution
 * is strictly prohibited.  This product  is  protected  under  copyright  law
 * and  trade  secret law as an  unpublished work.
 * (C) Copyright Texas Instruments.  All rights reserved.
 *
 * FileName			:	msr_qualify.c
 *
 * Description     	:
 *  This file contains functions that handle GPS measurement sent from Sensor.
 *  It checks the quality of such a measurement, if good enough, it will call
 *  a function to report such measurement to SMLC if configuration calls for
 *  MS-Assisted; if not good enouch, it either simply wait for better ones, or
 *  will do time alignment aimed at improving measurement time uncertainty.
 *
 * Author         	: 	Gaurav Arora - gaurav@ti.com
 *
 *
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "msr_qualify.h"
#include "nvs.h"
#include "logger.h"
#include "config.h"

#define C_GPS_WEEK_UNKNOWN      (0xFFFF)

/* The C_HEALTH_NOEXIST field really only applies to the Almanac.
   The Almanac is assumed to be complete when every SV has an almanac
   health entry that is not C_HEALTH_UNKNOWN
 */
#define  C_HEALTH_UNKNOWN       (0<<0)  /* No health information */
#define  C_HEALTH_BAD           (1<<0)  /* Satellite is sick */
#define  C_HEALTH_GOOD          (2<<0)  /* Satellite is known good */
#define  C_HEALTH_NOEXIST       (3<<0)  /* Satellite does not exist */

/* The MEAS_STATUS defines identify the bits store in the w_MeasStatus field
   of the me_Meas data structure.
 */
#define  MEAS_STATUS_NULL       (0)     /* The don't know anything state */
#define  MEAS_STATUS_SM_VALID   (1<<0)  /* TRUE -> f_SatSubMs is valid */
#define  MEAS_STATUS_SB_VALID   (1<<1)  /* TRUE -> sub bit time known */
#define  MEAS_STATUS_MS_VALID   (1<<2)  /* TRUE -> satellite time is known */
#define  MEAS_STATUS_BE_CONFIRM (1<<3)  /* TRUE -> bit edge confirmed from
										   signal */
#define  MEAS_STATUS_VE_VALID   (1<<4)  /* TRUE -> measured velocity */
#define  MEAS_STATUS_VE_FINE    (1<<5)  /* TRUE/FALSE -> fine/coarse velocity */
#define  MEAS_STATUS_CONT_LOCK  (1<<6)  /* TRUE -> Channel has continous lock */
#define  MEAS_STATUS_HEALTHY    (1<<7)  /* Reserved */
#define  MEAS_STATUS_GOOD_DATA  (1<<8)  /* Reserved */
#define  MEAS_STATUS_FROM_DIFF  (1<<9)  /* TRUE -> Last update from difference,
										   not meas */
#define  MEAS_STATUS_UNCERTAIN  (1<<10) /* TRUE -> Weak SV but can't do definite
										   XCORR test */
#define  MEAS_STATUS_XCORR      (1<<11) /* TRUE -> Strong indication of cross
										   correlation */
#define  MEAS_STATUS_MULTIPATH  (1<<12) /* TRUE -> Had multipath indicators */
#define  MEAS_STATUS_DONT_USE   (1<<13) /* TRUE -> Dubious measurement, wait
										   for another dwell */

#define C_TIMEUNC_TABLE_SIZE    6
#define C_MAX_PRM_RMS_ERR       0.00037359
/* Threshold for relaxing the SV time unc check in PE */
/* light speed second - meters/seconds */
#define C_LIGHT_SPEED_SEC		LIGHT_SEC
#define C_SUB_MS_SCALE_FACTOR   0.000000059604644775390625
#define C_ONE_BY_SQUARE_ROOT_2 		0.70710678118654752440084436210485

#define SV_DIRECTION_UNKNOWN    (128) //AI2 read needs to be verified for -ve value
/* Number of hardware channels in the receiver */
#define N_CHAN                  25
/* # of C/A chips per msec */
#define CA_CHIPS_MSEC           1023
/* Number of available GPS satellites */
#define GPS_SV                  32
#define GLO_SV	                24
#define IS_SV_VALID(x)         ((GPS_SV_VALID(x)) || (GLO_SV_VALID(x)))
#define GPS_SV_VALID(x)        (((x)>=1)  && ((x)<=(32)))
#define GLO_SV_VALID(x)        (((x)>=65) && ((x)<=(88)))


/* TRUE -> satellite time is known */
#define GOOD_MEAS				MEAS_STATUS_MS_VALID
#define LIGHT_SEC               (299792458.0)
#define C_PI                    3.1415926535898

static void msr_compute_avg_cno(struct dev_msr_info_PVT* msr_info, 
								struct gnss_msr_filter_info* msr_filter_info );
static void msr_compute_avg_psuedorange_err(struct dev_msr_info_PVT* msr_info, 
											struct gnss_msr_filter_info* msr_filter_info );
static void msr_compute_pdop(struct dev_msr_info_PVT *msr_info, 
							 struct gnss_msr_filter_info *msr_filter_info);
static void msr_sv_steering(struct dev_msr_info_PVT * msr_info, 
							struct gnss_msr_filter_info *msr_filter_info);
static void msr_channel_state(struct dev_msr_info_PVT * msr_info, 
							  struct gnss_msr_filter_info *msr_filter_info);
static void msr_filter_criteria(struct gnss_msr_filter_info *msr_filter_info);
static unsigned char costab[65] = {
	255,255,255,254,254,253,252,251,
	250,249,247,246,244,242,240,238,
	236,233,231,228,225,222,219,215,
	212,208,205,201,197,193,189,185,
	180,176,171,167,162,157,152,147,
	142,136,131,126,120,115,109,103,
	98, 92, 86, 80, 74, 68, 62, 56,
	50, 44, 37, 31, 25, 19, 13,  6,
	0
};

typedef struct
{
  char  elevation;    		/* Elevation. (-128 == Unknown). 180/256 degs/bit */
  unsigned char  azimuth;   /* Azimuth. 360/256 degs/bit. */
} sv_direction;
/*
 ******************************************************************************
 * function: Cos
 *
 * Function description:
 * Approximate cosine from table with 8-bit resolution
 * Parameters:
 *
 *	arg	- degrees * 256/360, range 0 - 255
 *
 * Return value:
 *
 *	cosine approximation * 255, e.g., cos(0) = 0xFF.  
 *  This is approximate b8 scaling.
 *
 ******************************************************************************
 */
static int Cos(unsigned char arg)
{
	if (arg <= 64)
		return (costab[arg]);
	else if (arg <= 128)
		return ((int)0 - costab[128 - arg]);
	else if (arg <= 192)
		return ((int)0 - costab[arg - 128]);
	else
		return (costab[256 - arg]);
}

/*
 ******************************************************************************
 * function: UnitVector
 *
 * Function description:
 *
 *	UnitVector is used to compute the unit vector scaled b15 for a given Sv 
 *  using the Azimuth and Elevation estimates that are provided.
 *
 *	If no direction information is known indicated by 
 *  b_Elev == SV_DIRECTION_UNKNOWN), then no conversion occurs and a -1 
 *  return code is generated.
 *
 * Parameters:
 *
 *	p_AzEl - point to an AzEl pair
 *	p_Vector - Pointer to the 3 element x, y, z vector (output)
 *
 * Return value:
 *
 *	0 if a valid UnitVector is written to *p_Vector.
 *
 ******************************************************************************
 */
static unsigned int UnitVector(sv_direction *p_AzEl, short *p_Vector)
{
	unsigned char  u_Azim;
	char	b_Elev;
	int l_CosElev;

	/* Can't compute the direction vector if the direction is unknown */
	if (p_AzEl->elevation == SV_DIRECTION_UNKNOWN)
		return (0);

	u_Azim = p_AzEl->azimuth;
	b_Elev = (char)((p_AzEl->elevation+ 1) >> 1);


	/* Look up the cos of the elevation. Note that recasting
	   the (char) b_Elev to (unsigned char) handles the task of dealing
	   with negative elevations. */

	l_CosElev = Cos((unsigned char)b_Elev);

	/* x = cos(Elev) * sin(Azim) */
	*p_Vector++	= (short)(l_CosElev * Cos((unsigned char)(u_Azim - 64)) >> 1);

	/* y = cos(Elev) * cos(Azim) */
	*p_Vector++	= (short)(l_CosElev * Cos(u_Azim) >> 1);

	/* z = sin(Elev) */
	*p_Vector = (short)(Cos((unsigned char)(b_Elev - 64)) << 7);

	/* Indicate a successful translation */
	return (1);
}

/*
 ******************************************************************************
 * function: RowMult()
 *
 * Function description:
 *
 * 	RowMult multiplies a single row of the input matrix against a column
 *	of the transposed input matrix.  We actually form only the transpose.
 *	A row of the virtual matrix is effectively accessed by a stride of 3
 *	in the transpose.
 *
 *	(x0, x1 .... xn) * (x0   y0   z0)
 *  (y0, y1 .... yn)   (x1   y1   z1)
 *	(z0, z1 .... zn)   (..   ..   ..)
 *                     (xn   yn   zn)
 *
 *	On input, each element has a maximum magnitude of 1 (= 32767).
 *
 *	Scale down by 3 leaving enough headroom for summing 16 maximum product
 *	values then scale down by 16 so that later processes can make products in
 *	a 32-bit register.
 *
 * Parameters:
 *
 *	p_RowIn - Points to the column that corresponds to a row of the
 *				virtual input matrix
 *	p_MatIn - Points to a column of the transposed input matrix
 *	q_N - Number of elements in each row/column
 *
 * Return value:
 *
 *	Sum of products
 *
 ******************************************************************************
 */
static int RowMult(short *p_RowIn, short *p_MatIn, unsigned int q_N)
{
	int l_Sum = 0;

	for (; q_N; q_N--) {

		l_Sum += (int) *p_RowIn * *p_MatIn >> 3;
		p_RowIn += 3;
		p_MatIn += 3;
	}

	return (l_Sum >> 16);
}

/*
 ******************************************************************************
 * function: Determinant
 *
 * Function description:
 *
 * 	Returns the determinant of a 3X3 matrix
 *
 *
 * Parameters:
 *
 *	p_Mat - Points to the input matrix.  Values are preconditioned to have
 *			at most 15 significant bits.
 *
 * Return value:
 *
 *	Determinant
 *
 ******************************************************************************
 */
static int Determinant (int *p_M)
{
	int l_Sum;

	l_Sum = 0;

	l_Sum = p_M[0]*p_M[5]*p_M[10]*p_M[15] 
		- p_M[0]*p_M[5]*p_M[11]*p_M[14] 
		- p_M[0]*p_M[9]*p_M[6]*p_M[15] 
		+ p_M[0]*p_M[9]*p_M[7]*p_M[14] 
		+ p_M[0]*p_M[13]*p_M[6]*p_M[11] 
		- p_M[0]*p_M[13]*p_M[7]*p_M[10] 
		- p_M[4]*p_M[1]*p_M[10]*p_M[15]
		+ p_M[4]*p_M[1]*p_M[11]*p_M[14] 
		+ p_M[4]*p_M[9]*p_M[2]*p_M[15] 
		- p_M[4]*p_M[9]*p_M[3]*p_M[14] 
		- p_M[4]*p_M[13]*p_M[2]*p_M[11] 
		+ p_M[4]*p_M[13]*p_M[3]*p_M[10] 
		+ p_M[8]*p_M[1]*p_M[6]*p_M[15] 
		- p_M[8]*p_M[1]*p_M[7]*p_M[14]
		- p_M[8]*p_M[5]*p_M[2]*p_M[15] 
		+ p_M[8]*p_M[5]*p_M[3]*p_M[14] 
		+ p_M[8]*p_M[13]*p_M[2]*p_M[7] 
		- p_M[8]*p_M[13]*p_M[3]*p_M[6] 
		- p_M[12]*p_M[1]*p_M[6]*p_M[11] 
		+ p_M[12]*p_M[1]*p_M[7]*p_M[10] 
		+ p_M[12]*p_M[5]*p_M[2]*p_M[11]
		- p_M[12]*p_M[5]*p_M[3]*p_M[10] 
		- p_M[12]*p_M[9]*p_M[2]*p_M[7] 
		+ p_M[12]*p_M[9]*p_M[3]*p_M[6];

	l_Sum = l_Sum >> 11;

	return (l_Sum);
}

/*
 ******************************************************************************
 * function: PdopApprox()
 *
 * Function description:
 *
 * 	PdopApprox is a helper function for the application.  It uses an
 *	optimized integer approximation and the Sv Direction
 *	information to calculate the PDOP for a given set of satellites. The caller
 *	provides the direction angles for selected satellites.
 *
 * Parameters:
 *
 *	p_AzEl - point to array of SV directions
 *	q_N - number of SVs represented in the array.  Minimum 4.
 *
 * Return value:
 *
 *	PDOP. (Negative result indicates that PDOP is incalculable).
 *
 ******************************************************************************
 */
static float PdopApprox(sv_direction *p_AzEl, unsigned int q_N)
{
	unsigned int q_i;
	int l_Det;
	float f_DetInv;
	float f_Result;
	short *p_UnitVector;                /* x,y,z of SV */
	short x_XYZ[3*N_CHAN];              /* x,y,z of all SVs */
	int l_Mat[16],C11,C22,C33;          /* 3 by 3 intermediate matrix */
	sv_direction *sv_dir;
	short x_Ones[3*N_CHAN];

	/* Construct the pointing vectors. If any of the SVs does
	   not have a valid Elevation / Azimuth then we can't compute
	   a PDOP.  Unit vectors are scaled b15.
	 */
	/* The maximum number of SVs permitted for this calculation is N_LCHAN 
	   (Space limited). The minimum number of SVs permitted is 3 
	   (To solve a 3x3 matrix). But limit to 4 for practicality.
	 */
	if (q_N < 4)
		return (-1.0f);

	/* Use an arbitrary subset if too many SVs */
	q_N = q_N <= N_CHAN ? q_N : N_CHAN;

	p_UnitVector = x_XYZ;
	for (q_i = q_N, sv_dir = p_AzEl; q_i; q_i--, sv_dir++) {

		if (! UnitVector(sv_dir, p_UnitVector))
			return (-1.0f);		/* Missing Az/El */

		p_UnitVector += 3;
	}

	for (q_i=0;q_i<3*N_CHAN;q_i++)
		x_Ones[q_i] = (short) (1<<8);

	/* Multiply input matrix by transpose.  Actually, the matrix we have formed
	   is the transpose. The input matrix is virtual.  Do product of two b15
	   values then scale 19 to fit sum into short.  Thus values are scaled b11.
	 */
	l_Mat[0]  = RowMult(&x_XYZ[0], &x_XYZ[0], q_N);                /* x x */
	l_Mat[1]  = l_Mat[4] =	RowMult(&x_XYZ[0], &x_XYZ[1], q_N);    /* x y */
	l_Mat[2]  = l_Mat[8] =	RowMult(&x_XYZ[0], &x_XYZ[2], q_N);    /* x z */

	l_Mat[5]  = RowMult(&x_XYZ[1], &x_XYZ[1], q_N);                /* y y */
	l_Mat[6]  = l_Mat[9] =	RowMult(&x_XYZ[1], &x_XYZ[2], q_N);    /* y z */
	l_Mat[10] = RowMult(&x_XYZ[2], &x_XYZ[2], q_N);                /* z z */


	l_Mat[3]  = l_Mat[12] = RowMult(&x_XYZ[0], &x_Ones[0], q_N);   /* sum(x) */
	l_Mat[7]  = l_Mat[13] = RowMult(&x_XYZ[1], &x_Ones[0], q_N);   /* sum(y) */
	l_Mat[11] = l_Mat[14]= RowMult(&x_XYZ[2], &x_Ones[0], q_N);    /* sum(z) */
	l_Mat[15] = (short)(q_N)*(short)(1<<8);

	l_Det = 0;

	/* Scale down l_Mat by b5 to ensure that the determinant and the co-factors 
	   dont overflow
	 */
	for (q_i=0;q_i<16;q_i++)
		l_Mat[q_i] = (int)(l_Mat[q_i] >> 5);

	/* The typical task is to invert l_Mat[]. We are only interested in the
	   diagonal elements of the inverted matrix EX*EX, EY*EY and EZ*EZ.
	 */
	l_Det = 0;

	if ((l_Det = Determinant(l_Mat)) == 0)
		return (-2.0f);		/* Singular */

	/* Account for determinant made of four-factor products but cofactor uses
	   three-factor products.  l_Mat is scaled b6.  Determinant result is b28 
	   (4mul + 16additions) but scaled down by 11 to b17.  Cofactor product is 
	   b21 (3mul(b18) + 9 additions(b3)). Hence scaling differs by four bits.
	 */
	f_DetInv = (float)(1./(1<<4)) / (float)l_Det;

	/* Term of inverse = cofactor of term / determinant.  We need only
	   the main diagonal terms.
	 */
	C11 =  l_Mat[5] * (l_Mat[10]*l_Mat[15] - l_Mat[11]*l_Mat[14]) 
		- l_Mat[9] * (l_Mat[6]*l_Mat[15] + l_Mat[7]*l_Mat[14])
		+ l_Mat[13]*(l_Mat[6]*l_Mat[11] - l_Mat[7]*l_Mat[10]);

	C22 = l_Mat[0]*(l_Mat[10]*l_Mat[15] - l_Mat[11]*l_Mat[14]) 
		- l_Mat[8]*(l_Mat[2]*l_Mat[15] + l_Mat[3]*l_Mat[14])
		+ l_Mat[12]*(l_Mat[2]*l_Mat[11] - l_Mat[3]*l_Mat[10]);

	C33 = l_Mat[0]*(l_Mat[5]*l_Mat[15] - l_Mat[7]*l_Mat[13]) 
		- l_Mat[4]*(l_Mat[1]*l_Mat[15] + l_Mat[3]*l_Mat[13])
		+ l_Mat[12]*(l_Mat[1]*l_Mat[7] - l_Mat[3]*l_Mat[5]);

	f_Result = (float)(C11 + C22 + C33) * f_DetInv;

	return f_Result;
}

/* Structure to define the time uncertainty table with respect to SNR */
struct mc_TimeUnc {

	unsigned short w_SnrDB;    /* Background SNR */
	float f_SvTimeUncMs;       /* Observed Time Error on Simulated Data */
	float f_Slope;             /* Observed Slope of the Error curve */
};

/*
 * The following table is based on Norm's theoretical predictions
 * using the approximation sigma_error = 0.42 * SNR^(-0.41) chips
 */
static const struct mc_TimeUnc mcz_TimeUnc[C_TIMEUNC_TABLE_SIZE] = {
	{
		(unsigned short)150,   /* >= 15 dB */
		(float)(0.1019/CA_CHIPS_MSEC),
		(float)((0.0635-0.1019)/(50*CA_CHIPS_MSEC))
	},
	{
		(unsigned short)200,   /* >= 20 dB */
		(float)(0.0635/CA_CHIPS_MSEC),
		(float)((0.0396-0.0635)/(50*CA_CHIPS_MSEC))
	},
	{
		(unsigned short)250,   /* >= 25 dB */
		(float)(0.0396/CA_CHIPS_MSEC),
		(float)((0.0247-0.0396)/(50*CA_CHIPS_MSEC))
	},
	{
		(unsigned short)300,   /* >= 30 dB */
		(float)(0.0247/CA_CHIPS_MSEC),
		(float)((0.0154-0.0247)/(50*CA_CHIPS_MSEC))
	},
	{
		(unsigned short)350,   /* >= 35 dB */
		(float)(0.0154/CA_CHIPS_MSEC),
		(float)((0.0096-0.0154)/(50*CA_CHIPS_MSEC))
	},
	{
		(unsigned short)400,   /* >= 40 dB */
		(float)(0.0096/CA_CHIPS_MSEC),
		(float)0.0
	}
};

static const float mcf_SqrtBeqLookUp[ ] = {
	1.0f,
	0.41623f,
	0.31838f,
	0.26818f,
	0.23619f,
	0.21349f,
	0.19631f,
	0.18271f,
	0.17159f,
	0.16229f,
	0.15436f
};

/*
 ******************************************************************************
 * function: mc_TimeUncertainty
 *
 * Function description:
 *
 *  This function calculates the TimeUncertainty of a given measurement based
 *  on the background SNR of the measurement (from a lookup table)
 *
 * Parameters:
 *
 *  w_SnrDB   - Background SNR (* 10 dB)
 *  u_FilterN - Code Phase Filter coefficient
 *
 * Return value:
 *
 *  float   f_SvTimeUncMs
 *
 ******************************************************************************
 */
static float mc_TimeUncertainty(unsigned short w_SnrDB, unsigned char u_FilterN)
{
	const struct mc_TimeUnc *p_TimeUnc = &mcz_TimeUnc[0];
	float f_SvTimeUncMs;
	unsigned int i;

	/*
	 * p_TimeUnc is the table of time uncertainties at a few discrete SNRs
	 * For all the SNRs inbetween we perform a simple two-point interpolation
	 * To avoid any divisions we also stored the slope of the uncertainty curve
	 * at each discrete SNR.
	 *   Interpolated value Y = Slope * (X - X1) + Y1;
	 *   where   X  = Measured SNR
	 *       X1 = SNR from table which is just below the Measured SNR
	 *       Y1 = Time Uncertainty from table correponding to SNR X1.
	 */


	/* Find the table entry corresponding to an SNR just below the measurement 
	   SNR 
	 */
	for (i = 0; i < C_TIMEUNC_TABLE_SIZE; i++) {
		if (p_TimeUnc[i].w_SnrDB > w_SnrDB)
			break;
	}

	if (i)
		i--;

	/* Y = Slope * (X - X1) + Y1 */
	f_SvTimeUncMs = p_TimeUnc[i].f_Slope 
		* (float)((int)(w_SnrDB - p_TimeUnc[i].w_SnrDB)) 
		+ p_TimeUnc[i].f_SvTimeUncMs;

	/*
	 * Code Phase Filter used is based on p_Meas->u_FilterN (N)
	 *   Filter Equation is (for a filter co-efficient of N)
	 *      Yn = Yn-1 + (Xn-Yn-1)/(N+1) ;
	 *   i.e. Yn = Xn/(N+1) + N*Yn-1/(N+1);
	 *   i.e. Yn = (1-a)Xn + aYn-1;    where a = N/(N+1)
	 *           n
	 *           ---
	 *   i.e.   Yn = \   (1- a^k) Xk
	 *           /
	 *           ---
	 *           k=0
	 *
	 *   Relating this to a standard low-pass filter we have
	 *       a^n = exp(-t/tou)
	 *       n * ln(a) = - n*Ts/tou;
	 *
	 *   So the time constant of the filter is obtained by
	 *       tou = -Ts/ln(a);
	 *
	 *   Equivalent bandwidth of this Filter is
	 *       Beq = 1/(4*tou)
	 *         = -ln(a)/(4*Ts)
	 *
	 *   Since our measurements are 1sec apart Ts=1sec (or 1Hz Bandwidth)
	 *       Beq = -ln(a)/4;
	 *
	 *   Now Sigma of the Filtered Error estimate is Sigma(filter) = 
	 *   Sigma(no_filter) * Sqrt(Beq)
	 *
	 *   mcf_SqrtBeqLookUp contains the sqrt(Beq) for values of N ranging from 
	 *   0 to 10;
	 */
	/* Restrict the u_FilterN to a max of 10 */
	if (u_FilterN > 10)
		u_FilterN = 10;

	f_SvTimeUncMs *= mcf_SqrtBeqLookUp[u_FilterN];

	return (f_SvTimeUncMs);
}


/*
 ******************************************************************************
 * function : msr_compute_avg_cno() 
 *
 * description: 
 *	a)
 *
 ******************************************************************************
 */
static void msr_compute_avg_cno(struct dev_msr_info_PVT* msr_info, 
								struct gnss_msr_filter_info* msr_filter_info )
{
	unsigned short sat_id = 0;
	unsigned int int_time = 0;
	short latency = 0;
	unsigned int sv_id	 = 0;


	for (sat_id = 0; sat_id < msr_filter_info->n_sat; sat_id++, msr_info++) {

		sv_id = msr_info->svid;
		int_time = msr_info->pre_int * msr_info->post_int;

		if (int_time <= 4100)
			latency = 5001;
		else if  (int_time <= 17000)
			latency = 17001;	
		else
			latency = 32001;	

#ifdef VERBOSE_LOGGING
		LOGPRINT(msr_flt,info,"pre_int	[%10d]",msr_info->pre_int);
		LOGPRINT(msr_flt,info,"post_int	[%10d]",msr_info->post_int);
		LOGPRINT(msr_flt,info,"latency_ms	[%10d]",msr_info->latency_ms);
		LOGPRINT(msr_flt,info,"int_time	[%10d]",int_time);
		LOGPRINT(msr_flt,info,"latency	[%10d]",latency);
#endif
		/* check if it is good observation */
		if ((IS_SV_VALID(sv_id)) && 
			(msr_info->msr_flags & GOOD_MEAS) && 
			(~msr_info->msr_flags & MEAS_STATUS_FROM_DIFF)&& 
			(~msr_info->msr_flags & MEAS_STATUS_DONT_USE) && 
			(~msr_info->msr_flags & MEAS_STATUS_XCORR)	&& 
			(msr_info->latency_ms < latency)) {

#ifdef VERBOSE_LOGGING
			LOGPRINT(msr_flt,info,"sv_id	[%10d]",sv_id);
			LOGPRINT(msr_flt,info,"cno	[%10d]",msr_info->cno);
#endif
			msr_filter_info->avg_cno += msr_info->cno;
			msr_filter_info->n_sv_good_msr++;
		}
	}

	/* Compute average cno */
	if (msr_filter_info->n_sv_good_msr)
		msr_filter_info->avg_cno = 
			msr_filter_info->avg_cno/msr_filter_info->n_sv_good_msr;

#ifdef VERBOSE_LOGGING
	LOGPRINT(msr_flt,info,"avg_cno	[%10d]", msr_filter_info->avg_cno / 10);
#endif

}
/*
 ******************************************************************************
 * function : msr_compute_avg_psuedorange_err() 
 *
 * description: 
 *	a)
 *
 ******************************************************************************
 */
static void msr_compute_avg_psuedorange_err( struct dev_msr_info_PVT* msr_info, 
								 struct gnss_msr_filter_info* msr_filter_info )
{
	unsigned short sat_id = 0;
	unsigned int sv_id	 = 0;
	float  sv_time_unc = 0.0; /* in msec */
	double psuedo_range_err = 0;


	for (sat_id = 0; sat_id < msr_filter_info->n_sat; sat_id++, msr_info++) {

		sv_id = msr_info->svid;
		sv_time_unc = mc_TimeUncertainty(msr_info->snr,(unsigned char)0);

		/* Sameet suggested fixes for 189x MSR filtering */
		/* 1. Scale SV Time Unc if SV Id is in DLL for GPS or GLO SV ID*/
		/* Sameet suggested this fix to revert to handle multipath failures */
		/* if (msr_info->ch_states == MSR_CH_STATE_CL_TRK && IS_SV_VALID(sv_id))
		{
			sv_time_unc *= C_ONE_BY_SQUARE_ROOT_2;
		}
		*/

		/* 2. Scale SV Time Unc if SV Id is GLO */
		if (GLO_SV_VALID(sv_id))
		{
			sv_time_unc *= 2;
		}
#ifdef VERBOSE_LOGGING
	
		LOGPRINT(msr_flt,info,"sv_id					[%10d]",sv_id);
		LOGPRINT(msr_flt,info,"msr flags				[0x%x]",msr_info->msr_flags);
		LOGPRINT(msr_flt,info,"snr						[%10d]",msr_info->snr);
		LOGPRINT(msr_flt,info,"sv_time_unc				[%f]",sv_time_unc);
		LOGPRINT(msr_flt,info,"sv_time_unc_threshold	[%f]",
				msr_filter_info->sv_time_unc_threshold);
#endif		
		/* check if it is good observation */
		if ((IS_SV_VALID(sv_id)) &&
			(msr_info->msr_flags  & GOOD_MEAS) 			 &&
			(~msr_info->msr_flags & MEAS_STATUS_FROM_DIFF)&&
			(~msr_info->msr_flags & MEAS_STATUS_DONT_USE) &&
			(~msr_info->msr_flags & MEAS_STATUS_XCORR)    &&
			(sv_time_unc < (float)(msr_filter_info->sv_time_unc_threshold))) {
		
				/* subtracting c/no for each sv with 1.3 db */
				msr_filter_info->avg_cno += (msr_info->cno - 1.3);

				/* convert the sv time unc to seconds */
				psuedo_range_err = (double) (sv_time_unc * LIGHT_MSEC);
				msr_filter_info->avg_psuedo_range_err += psuedo_range_err;

				msr_filter_info->n_sv_good_msr++;

				if (GPS_SV_VALID(sv_id))
					msr_filter_info->gps_good_sv_bitmap |= 
												((unsigned int)1<<(sv_id - 1));
				else if (GLO_SV_VALID(sv_id))
					msr_filter_info->glo_good_sv_bitmap |= 
												((unsigned int)1<<(sv_id - 65));
			}
	}

	if (msr_filter_info->n_sv_good_msr) {
		msr_filter_info->avg_cno = 
			(msr_filter_info->avg_cno/msr_filter_info->n_sv_good_msr);
		msr_filter_info->avg_psuedo_range_err = (double) 
		msr_filter_info->avg_psuedo_range_err/msr_filter_info->n_sv_good_msr;
	}
}
/*
 ******************************************************************************
 * function : msr_compute_pdop() 
 *
 * description: 
 *	a)
 *
 ******************************************************************************
 */
static void msr_compute_pdop(struct dev_msr_info_PVT *msr_info,
								 struct gnss_msr_filter_info *msr_filter_info)
{
	unsigned short	sv_id = 0;
	unsigned int q_pdop_N = 0;
	unsigned int sat_id = 0;
	int is_good_sv = 0;
	struct dev_sv_dir svdir = {0};
	sv_direction pdop_sv_dir[32], *ptr_pdop_sv_dir;

	q_pdop_N = 0;
	memset(pdop_sv_dir,0x00,sizeof(sv_direction));
	ptr_pdop_sv_dir = &pdop_sv_dir[0];

	/* run the loop to for the num sat reported in the msrurement report */
	for (sat_id =0; sat_id < msr_filter_info->n_sat; sat_id++, msr_info++) {

		sv_id = msr_info->svid;
		is_good_sv = 0;
		
		/* For SVs with good measurement only */
		if (GPS_SV_VALID(sv_id)){
			is_good_sv = (msr_filter_info->gps_good_sv_bitmap & 
						((unsigned int)1<<(sv_id - 1))) ? 1:0;		
		} else if (GLO_SV_VALID(sv_id)){
			is_good_sv = (msr_filter_info->glo_good_sv_bitmap & 
						((unsigned int)1<<(sv_id - 65))) ? 1:0;			
		}
		
		/* get sv direction info for only good sv's */
		if( is_good_sv) {
					
			if (GPS_SV_VALID(sv_id)||GLO_SV_VALID(sv_id)){
				svdir.azimuth = msr_info->azimuth;
				svdir.elevation = msr_info->elevation;
			} else{
				svdir.elevation = SV_DIRECTION_UNKNOWN;
			}

#ifdef VERBOSE_LOGGING
			LOGPRINT(msr_flt,info,"is_good_sv:[%d] sv_id:[%3d] evation:[%3d] azimuth:0[%3d] ",
				is_good_sv, sv_id, svdir.elevation, svdir.azimuth);
#endif

			/* copy all the good SV's sv direction data from NVS to an array */
			if(svdir.elevation != SV_DIRECTION_UNKNOWN){
				ptr_pdop_sv_dir->elevation = svdir.elevation;
				ptr_pdop_sv_dir->azimuth = svdir.azimuth;
				ptr_pdop_sv_dir++;
				q_pdop_N++;
			}
		}
	}

	/* calculate pdop from the list of good SV direction data */
#ifdef VERBOSE_LOGGING	
	LOGPRINT(msr_flt,info,"good sv dir count for pdop [%2d]",q_pdop_N);
#endif
  	msr_filter_info->pdop = PdopApprox( pdop_sv_dir, q_pdop_N );
}
/*
 ******************************************************************************
 * function : msr_filter_criteria() 
 *
 * description: 
 *	a)
 *
 ******************************************************************************
 */
void msr_filter_criteria( struct gnss_msr_filter_info *msr_filter_info)
{

	//: read from config file
	/* get the pdop_mask from the configuration file */
	/*if (cfg_success != sys_param_get(cfg_pdop_mask, (void**) &pdop_mask)) {
	  LOGPRINT(msr_flt, warn, "failed to config param cfg_pdop_mask");
	  return;
	  }*/

	/* Check for good dop */
	if((msr_filter_info->pdop <= 0x0C/**pdop_mask*/) && 
	   (msr_filter_info->pdop > 0.00)){
		msr_filter_info->is_good_dop = 1;
	}

	/* Check CNocriteria, steering sv data and count of good observations for validating the 
	   measurement */
	if(msr_filter_info->n_sv_good_msr >= 4) /*&& 
			(msr_filter_info->pos_uncert < msr_filter_info->accuracy))*/ 
		msr_filter_info->msr_qual_status = 1;
	else 
		msr_filter_info->msr_qual_status = 0;
	

	/* Check number of sv's with good msr, pos unc & accuracy for validating the 
	measurement */
	if((msr_filter_info->avg_cno > 0) && (msr_filter_info->avg_cno < 290)) {
		if( (msr_filter_info->is_all_sv_searched) || 
			(msr_filter_info->cnt_good_obs >= 8))
			msr_filter_info->msr_qual_status = 1;
		else 
			msr_filter_info->msr_qual_status = 0;
	}

#ifdef VERBOSE_LOGGING

	LOGPRINT(msr_flt,info,"is_all_sv_searched %d, cnt_good_obs %d",
				msr_filter_info->is_all_sv_searched,msr_filter_info->cnt_good_obs);
	
	LOGPRINT(msr_flt,info,"gps_good_sv_bitmap:[0x%x] glo_good_sv_bitmap:[0x%x]", 
			 msr_filter_info->gps_good_sv_bitmap,
			 msr_filter_info->glo_good_sv_bitmap);
	LOGPRINT(msr_flt,info,"n_sv_good_msr:[%2d] avg_cno:[%4d] is_good_dop:[%1d]", 
			msr_filter_info->n_sv_good_msr,
			msr_filter_info->avg_cno,
			msr_filter_info->is_good_dop);

	LOGPRINT(msr_flt,info,"position uncertainity:[%f m] pdop:[%f] valid msr report:[%2d]", 
			 msr_filter_info->pos_uncert,
			 msr_filter_info->pdop,
			 msr_filter_info->msr_qual_status);
#endif

}

/*
 ******************************************************************************
 * function : msr_sv_steering() 
 *
 * description: 
 *	a)
 *
 ******************************************************************************
 */
static void msr_sv_steering( struct dev_msr_info_PVT * msr_info, 
							  struct gnss_msr_filter_info *msr_filter_info)
{
	/* to be removed */
	int sv_steer_inject_count = 0;
	int SvSteerId = 0;

	unsigned char sat_id,i;
	unsigned char sv_id;
	unsigned short total_obs_count = 0;
	double el = 0.0;

	for (sat_id=0; sat_id < msr_filter_info->n_sat; sat_id++, msr_info++){

		sv_id = (unsigned char)(msr_info->svid);

		if(GPS_SV_VALID(sv_id)){

			if (msr_info->elevation != SV_DIRECTION_UNKNOWN ){
				
				el = (double)( msr_info->elevation * 0.703125 );
						
				if(el > 4.5){ /* > 4.5 degrees*/

					if(msr_info->total_obv_cnt > 0)
						total_obs_count++;

					if(msr_info->good_obv_cnt > 0)
						msr_filter_info->cnt_good_obs++;

					sv_steer_inject_count++;
				}
			}
		}
	} 
#ifdef VERBOSE_LOGGING

	LOGPRINT(msr_flt,info,"total_obs_count %d, sv_steer_inject_count %d",
			total_obs_count,sv_steer_inject_count);
#endif				
	if (0 != sv_steer_inject_count) {                
		if(total_obs_count >= sv_steer_inject_count){
			msr_filter_info->is_all_sv_searched = 1;
		}
	}
}
/*
 ******************************************************************************
 * function : msr_filter_report() 
 *
 * description: 
 *	a)
 *
 ******************************************************************************
 */
char msr_filter_report(struct dev_msr_info_PVT * msr_info, unsigned short num_sat, 
					   	   struct dev_msr_result *msr_rpt_results)
{

#define POS_UNC_MAP_TO_K(unc_m, C, x)                                       \
	(unsigned char)(log10(((float)unc_m / C) + 1) / log10(1 + x))

	struct gnss_msr_filter_info* msr_filter_info = 
		malloc(sizeof(struct gnss_msr_filter_info));	
	if(!msr_filter_info)
		return -1;

	memset(msr_filter_info,0x00,sizeof(struct gnss_msr_filter_info));
	msr_filter_info->n_sat = num_sat;

	//LOGPRINT(msr_flt,info,"num sv used in msr filtering [%d]",msr_filter_info->n_sat);

	/* 1 --> compute average c/no of all good sv's */
	msr_compute_avg_cno(msr_info, msr_filter_info);

	/* 2 --> calculate Sv Time Unc threshold */
	if (msr_filter_info->avg_cno > (unsigned int)C_NO_THRESH_TIMEUNC)
		msr_filter_info->sv_time_unc_threshold = (float)(150.0/LIGHT_MSEC);
	else
		msr_filter_info->sv_time_unc_threshold = (float)(200.0/LIGHT_MSEC);

	msr_filter_info->n_sv_good_msr = 0;
	msr_filter_info->avg_cno= 0;

	/* 3 --> compute pseudo range error with the time uncertainity */
	msr_compute_avg_psuedorange_err(msr_info,msr_filter_info);

	/* 4 --> compute pdop using elevation and azimuth of good sv's */
	msr_compute_pdop(msr_info, msr_filter_info);

	/* 5 --> Calcualte Position Uncertainity */
	if (msr_filter_info->pdop > 0.0)
		msr_filter_info->pos_uncert = (double) 
			(3* msr_filter_info->avg_psuedo_range_err * (msr_filter_info->pdop));
	else
		msr_filter_info->pos_uncert = 
			3.0 * 25.0 * msr_filter_info->avg_psuedo_range_err;

	if (msr_filter_info->pos_uncert == 0)
		msr_filter_info->pos_uncert = 65535; 
	/* 6 --> search sv's with the steering data */
	msr_sv_steering(msr_info, msr_filter_info);
	/* 8 --> qualify the measurement, update pdop & position uncertainity */
	msr_filter_criteria(msr_filter_info);

	msr_rpt_results->msr_qual_status = msr_filter_info->msr_qual_status;
	msr_rpt_results->contents = (DEV_MSR_HAS_GPS_RSLT | DEV_MSR_HAS_GLO_RSLT );

	/* Convert to K parameter from position uncertainity */
	msr_rpt_results->glo_unc_K = POS_UNC_MAP_TO_K(msr_filter_info->pos_uncert, 
												  10, 0.1F);
	msr_rpt_results->gps_unc_K = POS_UNC_MAP_TO_K(msr_filter_info->pos_uncert, 
												  10, 0.1F);

#ifdef VERBOSE_LOGGING
	LOGPRINT(msr_flt,info,"msr results : contents:[0x%x] msr_qual_status:[%3d] gps_unc_K:[%3d] glo_unc_K:[%3d]",
					msr_rpt_results->contents,
					msr_filter_info->msr_qual_status,
					msr_rpt_results->gps_unc_K,
					msr_rpt_results->glo_unc_K);
#endif
	free(msr_filter_info);

	return 0;
}


