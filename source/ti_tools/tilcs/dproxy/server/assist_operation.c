/*
 * assist_operation.c
 *
 * This file has low level functions that deletes, reads, requests status of
 * various assistance data from the devproxy server (nvs and device). It
 * excludes injection of assistance, which is handled in assist_injection.c
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>

#include "dev_proxy.h"
#include "ai2.h"
#include "config.h"
#include "logger.h"
#include "device.h"
#include "utils.h"
#include "inject.h"
#include "nvs.h"
#include "nvs_validate.h"

/* CAUTION !!!! Do NOT change the order from enum loc_assit in dev_proxy.h. 
				keep appending additional log strings and maintain the s
				same order in enum loc_assit dev_proxy.h */
static char *assist_type_str[] = {

		"a_REF_POS", "a_GPS_TIM", "a_GPS_DGP", "a_GPS_EPH", "a_GPS_ION", 
		"a_GPS_UTC", "a_GPS_ALM", "a_GPS_ACQ", "a_GPS_RTI", "a_GLO_EPH",	 
		"a_GLO_ALM", "a_GLO_UTC", "a_GLO_ION", "a_GLO_TIM",	"a_GLO_ACQ_G1", 
		"a_GLO_ACQ_G2","a_GLO_RTI",	"a_GLO_DGC_G1", "a_GLO_DGC_G2"
};


extern int h2d_ai2_hdl;

/*
 * 1. Request assistance data from device.
 */
int dev_req_assist(enum h2d_assist assist, unsigned int prn)
{
	unsigned char mem[4];
	unsigned char *buf = mem;
	unsigned int mem_size = 0;

	switch(assist) {

		case h2d_time: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x09);
				buf = BUF_LE_U2B_WR_INC(buf, 0);
				mem_size = 3;

				break;
			}

		case h2d_pos: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x0B);
				buf = BUF_LE_U2B_WR_INC(buf, 0);
				mem_size = 3;

				break;
			}

		case h2d_msr: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x0F);
				buf = BUF_LE_U2B_WR_INC(buf, 0);
				mem_size = 3;

				break;
			}

		case h2d_eph: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x10);
				buf = BUF_LE_U2B_WR_INC(buf, 1);
				buf = BUF_LE_U1B_WR_INC(buf, prn);
				mem_size = 4;

				break;
			}

		case h2d_alm: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x12);
				buf = BUF_LE_U2B_WR_INC(buf, 1);
				buf = BUF_LE_U1B_WR_INC(buf, prn);
				mem_size = 4;

				break;
			}

		case h2d_iono: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x14);
				buf = BUF_LE_U2B_WR_INC(buf, 0);
				mem_size = 3;

				break;
			}

		case h2d_utc: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x16);
				buf = BUF_LE_U2B_WR_INC(buf, 0);
				mem_size = 3;

				break;
			}

		case h2d_hlth: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x18);
				buf = BUF_LE_U2B_WR_INC(buf, 0);
				mem_size = 3;

				break;
			}

		case h2d_dir: {
				buf = BUF_LE_U1B_WR_INC(buf, 0x22);
				buf = BUF_LE_U2B_WR_INC(buf, 1);
				/* Report svdir for all sv. */
				buf = BUF_LE_U1B_WR_INC(buf, 1);
				mem_size = 4;

				break;
			}

		case h2d_oscoff: {
				buf = BUF_LE_U1B_WR_INC(buf, 0xCC);
				buf = BUF_LE_U2B_WR_INC(buf, 0);
				mem_size = 3;

				break;
			}

		default:
			break;
	}

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, mem_size)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(generic, info, "assist request injected for assist id %d", assist);
			return 0;
		}
	}

	LOGPRINT(generic, err, "error injecting assist request");
	return -1;
}

#ifndef ANDROID
static unsigned char out_data_base[2048*32];
#endif
static int h2d_dev_req_assist(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	struct nvs_aid_data assist[1];
	int assist_cnt = 0;

	struct ie_head *head = &ie_adu->head;
	struct rpc_msg *rmsg;

#ifdef ANDROID
	unsigned char out_data_base[2048*32];
#endif
	unsigned char *out_data = out_data_base;
	unsigned int out_data_cnt = 0;

	unsigned int loop_in;
	unsigned int loop_out;
	int ret = 0;

	memset(assist, 0, sizeof(struct nvs_aid_data));
	memset(out_data_base, 0, 2048 *32 * sizeof(unsigned char));

	switch(srv_re->elem_id) {
		case REI_GPS_EPH:
			{
				struct dev_gps_eph *dev_eph;
				struct gps_eph *in_eph;

				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_gps_eph;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {

					in_eph = (struct gps_eph *) ie_adu->data;

					for (loop_in = 0; 
					     loop_in < head->tot_len / ST_SIZE(gps_eph); 
					     loop_in++) {

						dev_eph = (struct dev_gps_eph *) assist->buffer;

						for (loop_out = 0; loop_out < assist->n_objs; loop_out++) {
							if (dev_eph->prn == (in_eph->svid + 1)) {
								LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GPS_EPH svid found [%d]", in_eph->svid);

								memcpy(out_data, dev_eph, ST_SIZE(dev_gps_eph));
								out_data += ST_SIZE(dev_gps_eph);
								out_data_cnt++;
								break;
							}

							dev_eph++;
						}

						in_eph++;
					}

					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GPS_EPH out_data_cnt [%d]", out_data_cnt);
				}

				if (eph_gps_rpt_create_3gpp_ie(rmsg, 
							       CTW_2_OPER_ID(head->ct_word), 
							       out_data_base, 
							       out_data_cnt) < 0) {
					LOGPRINT(generic, err, "h2d_dev_req_assist: eph_gps_rpt_create_3gpp_ie failed");
					rpc_ie_attach(rmsg, REI_GPS_EPH, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GLO_EPH:
			{
				struct dev_glo_eph *dev_eph;
				struct glo_eph *in_eph;

				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_glo_eph;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {

					in_eph = (struct glo_eph *) ie_adu->data;

					for (loop_in = 0; 
					     loop_in < head->tot_len / ST_SIZE(glo_eph); 
					     loop_in++) {

						dev_eph = (struct dev_glo_eph *) assist->buffer;

						for (loop_out = 0; loop_out < assist->n_objs; loop_out++) {
							if (dev_eph->prn == (in_eph->the_svid + AI2_GLO_BASE + 1)) {
								LOGPRINT(generic, info, "h2d_dev_req_assist: REI_GLO_EPH svid found [%d]", in_eph->the_svid);
								memcpy(out_data, dev_eph, ST_SIZE(dev_glo_eph));
								out_data += ST_SIZE(dev_glo_eph);
								out_data_cnt++;
								break;
							}

							dev_eph++;
						}

						in_eph++;
					}

					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GLO_EPH out_data_cnt [%d]", out_data_cnt);
				}

				if (eph_glo_rpt_create_3gpp_ie(rmsg, 
							       CTW_2_OPER_ID(head->ct_word), 
							       out_data_base, 
							       out_data_cnt) < 0) {
					LOGPRINT(generic, err, "h2d_dev_req_assist: eph_glo_rpt_create_3gpp_ie failed");
					rpc_ie_attach(rmsg, REI_GLO_EPH, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GPS_ALM:
			{
				struct dev_gps_alm *dev_alm;
				struct gps_alm *in_alm;

				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_gps_alm;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {

					in_alm = (struct gps_alm *) ie_adu->data;

					for (loop_in = 0; 
					     loop_in < head->tot_len / ST_SIZE(gps_alm); 
					     loop_in++) {

						dev_alm = (struct dev_gps_alm *) assist->buffer;

						for (loop_out = 0; loop_out < assist->n_objs; loop_out++) {
							if (dev_alm->prn == (in_alm->svid + 1)) {
								LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GPS_ALM svid found [%d]", in_alm->svid);
								memcpy(out_data, dev_alm, ST_SIZE(dev_gps_alm));
								out_data += ST_SIZE(dev_gps_alm);
								out_data_cnt++;
								break;
							}

							dev_alm++;
						}

						in_alm++;
					}

					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GPS_ALM out_data_cnt [%d]", out_data_cnt);
				}

				if (alm_gps_rpt_create_3gpp_ie(rmsg, 
							       CTW_2_OPER_ID(head->ct_word), 
							       out_data_base, 
							       out_data_cnt) < 0) {
					LOGPRINT(generic, err, "h2d_dev_req_assist: alm_gps_rpt_create_3gpp_ie failed");
					rpc_ie_attach(rmsg, REI_GPS_ALM, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GLO_ALM:
			{
				struct dev_glo_alm *dev_alm;
				struct glo_alm *in_alm;

				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_glo_alm;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {

					in_alm = (struct glo_alm *) ie_adu->data;

					for (loop_in = 0; 
					     loop_in < head->tot_len / ST_SIZE(glo_alm); 
					     loop_in++) {

						dev_alm = (struct dev_glo_alm *) assist->buffer;

						for (loop_out = 0; loop_out < assist->n_objs; loop_out++) {
							if (dev_alm->prn == (in_alm->nA + AI2_GLO_BASE)) {
								LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GLO_ALM svid found [%d]", (in_alm->nA - 1));
								memcpy(out_data, dev_alm, ST_SIZE(dev_glo_alm));
								out_data += ST_SIZE(dev_glo_alm);
								out_data_cnt++;
							}

							dev_alm++;
						}

						in_alm++;
					}

					LOGPRINT(generic, info, "h2d_dev_req_assist: REI_GLO_ALM out_data_cnt [%d]", out_data_cnt);
				}

				if (alm_glo_rpt_create_3gpp_ie(rmsg, 
							       CTW_2_OPER_ID(head->ct_word), 
							       out_data_base, 
							       out_data_cnt) < 0) {
					LOGPRINT(generic, err, "h2d_dev_req_assist: alm_glo_rpt_create_3gpp_ie failed");
					rpc_ie_attach(rmsg, REI_GLO_ALM, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GPS_TIM:
			{
				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_gps_clk;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {
					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GPS_TIM found week_nr [%d] msec [%d]", 
						 ((struct dev_gps_time *)assist->buffer)->week_num, ((struct dev_gps_time *)assist->buffer)->time_msec);

					if (clk_gps_rpt_create_clk_ie(rmsg, 
								      CTW_2_OPER_ID(head->ct_word), 
								      (struct dev_gps_time *) 
								      assist->buffer, 
								      0) < 0) {
						LOGPRINT(generic, warn, "h2d_dev_req_assist: clk_gps_rpt_create_clk_ie failed");
						rpc_ie_attach(rmsg, REI_GPS_TIM, 
							      CTW_2_OPER_ID(head->ct_word), 
							      0, NULL);
					}
				} else {
					LOGPRINT(nvs, warn, "h2d_dev_req_assist: read_nvs_aiding failed");
					rpc_ie_attach(rmsg, REI_GPS_TIM, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GLO_TIM:
			{
				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_glo_clk;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {
					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GLO_TIM found year [%d] month [%d] day [%d] msec [%d]", 
						 ((struct dev_glo_time *)assist->buffer)->time_year, 
						 ((struct dev_glo_time *)assist->buffer)->time_month, 
						 ((struct dev_glo_time *)assist->buffer)->time_day, 
						 ((struct dev_glo_time *)assist->buffer)->time_msec);

					if (clk_glo_rpt_create_clk_ie(rmsg, 
								      CTW_2_OPER_ID(head->ct_word), 
								      (struct dev_glo_time *) 
								      assist->buffer, 
								      0) < 0) {
						LOGPRINT(generic, err, "h2d_dev_req_assist: clk_glo_rpt_create_clk_ie failed");
						rpc_ie_attach(rmsg, REI_GLO_TIM, 
							      CTW_2_OPER_ID(head->ct_word), 
							      0, NULL);
					}
				} else {
					LOGPRINT(nvs, warn, "h2d_dev_req_assist: read_nvs_aiding failed");
					rpc_ie_attach(rmsg, REI_GLO_TIM, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GPS_UTC:
			{
				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_gps_utc;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {
					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GPS_UTC found");

					if (utc_gps_rpt_create_3gpp_ie(rmsg, 
								       CTW_2_OPER_ID(head->ct_word), 
								       (struct dev_gps_utc *) 
								       assist->buffer, 
								       NEW_UTC_GPS_IE_IND) < 0) {
						LOGPRINT(generic, err, "h2d_dev_req_assist: utc_gps_rpt_create_3gpp_ie failed");
						rpc_ie_attach(rmsg, REI_GPS_UTC, 
							      CTW_2_OPER_ID(head->ct_word), 
							      0, NULL);
					}
				} else {
					LOGPRINT(nvs, warn, "h2d_dev_req_assist: read_nvs_aiding failed");
					rpc_ie_attach(rmsg, REI_GPS_UTC, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}


				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GLO_UTC:
			{
				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_glo_utc;
				assist_cnt = read_nvs_aiding(assist, 1, 0);

				if (assist_cnt) {
					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GLO_UTC found");

					if (utc_glo_rpt_create_3gpp_ie(rmsg, 
								       CTW_2_OPER_ID(head->ct_word), 
								       (struct dev_glo_utc *) 
								       assist->buffer, 
								       NEW_UTC_GLO_IE_IND) < 0) {
						LOGPRINT(generic, err, "h2d_dev_req_assist: utc_glo_rpt_create_3gpp_ie failed");
						rpc_ie_attach(rmsg, REI_GLO_UTC, 
							      CTW_2_OPER_ID(head->ct_word), 
							      0, NULL);
					}
				} else {
					LOGPRINT(nvs, warn, "h2d_dev_req_assist: read_nvs_aiding failed");
					rpc_ie_attach(rmsg, REI_GLO_UTC, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_REF_POS:
			{
				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_gps_pos;
				assist_cnt = read_nvs_aiding(assist, 1, 0);
				if (assist_cnt) {
					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_REF_POS found");

					if (pos_rpt_create_loc_ie(rmsg, 
								  CTW_2_OPER_ID(head->ct_word), 
								  (struct dev_pos_info *) 
								  assist->buffer, 
								  0) < 0) {
						LOGPRINT(generic, err, "h2d_dev_req_assist: pos_rpt_create_loc_ie failed");
						rpc_ie_attach(rmsg, REI_REF_POS, 
							      CTW_2_OPER_ID(head->ct_word), 
							      0, NULL);
					}
				} else {
					LOGPRINT(nvs, warn, "h2d_dev_req_assist: read_nvs_aiding failed");
					rpc_ie_attach(rmsg, REI_REF_POS, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}

				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GPS_ION:
			{
				if (!(rmsg = rpc_msg_alloc()))
					return -1;

				assist[0].record = nvs_gps_iono;
				assist_cnt = read_nvs_aiding(assist, 1, 0);

				if (assist_cnt) {
					LOGPRINT(nvs, info, "h2d_dev_req_assist: REI_GPS_ION found");

					if (ion_gps_rpt_create_3gpp_ie(rmsg, 
								       CTW_2_OPER_ID(head->ct_word), 
								       (struct dev_gps_ion *) 
								       assist->buffer, 
								       NEW_ION_GPS_IE_IND) < 0) {
						LOGPRINT(generic, err, "h2d_dev_req_assist: ion_gps_rpt_create_3gpp_ie failed");
						rpc_ie_attach(rmsg, REI_GPS_ION, 
							      CTW_2_OPER_ID(head->ct_word), 
							      0, NULL);
					}
				} else {
					LOGPRINT(nvs, warn, "h2d_dev_req_assist: read_nvs_aiding failed");
					rpc_ie_attach(rmsg, REI_GPS_ION, 
						      CTW_2_OPER_ID(head->ct_word), 
						      0, NULL);
				}


				if(rpc_svr_send(rmsg) < 0)
					LOGPRINT(generic, err, "rpc_svr_send failed");

				rpc_msg_free(rmsg);
			}
			break;

		case REI_GPS_HLT: 
			{
				ret = dev_req_assist(h2d_hlth, 0);
				LOGPRINT(generic, info, "requesting sv health");
			}
			break;

		case REI_GPS_DIR:
			{
				ret = dev_req_assist(h2d_dir, 0);
				LOGPRINT(generic, info, "requesting sv dir");
			}
			break;

		case REI_CLK_CALIBRATE:
			{
				ret = dev_req_assist(h2d_oscoff, 0);
				LOGPRINT(generic, info, "requesting clock offset");
			}
			break;

		default:
			assist_cnt = 0;
			break;
	}

	return 0;
}

/*
 * 2. Delete assistance data from device and NVS.
 */
#define AI2_DEL_TIME 4
#define AI2_DEL_POS 5
#define AI2_DEL_EPH 6
#define AI2_DEL_ALM 7
#define AI2_DEL_ION_UTC 8
#define AI2_DEL_SVHLTH 9

char ai2_rec_action_sizes[] = {1, 2, 1, 1, 0};

/* Delete assistance data from device. */
static int dev_del_assist(unsigned short assist, unsigned int sv_id_map, int act)
{
	struct ai2_rec_action action[1];
	unsigned char mem[5];
	int count = -1;
	unsigned int svid_base = 0;

	/* Validate combination. */
	if ((AI2_DEL_EPH != act) && (AI2_DEL_ALM != act)) {
		/* ID not used. */
		action->action_mod = 0;
		sv_id_map = 0;
	} else if (sv_id_map == 0) {
		/*
		 * Satellite constellation related assistance needs to be
		 * deleted, but sv_id map is '0'.
		 */
		LOGPRINT(del_ast, warn, "assist del cmd with no svid. cmd ignored");
		return 0;
	}

	/* Adjust the svid base according the constellation. */
	switch (assist) {
		case REI_GPS_ALM:
		case REI_GPS_EPH:
		case REI_DEV_GPS_ALM:
		case REI_DEV_GPS_EPH: {
			svid_base = AI2_GPS_BASE + 1;
			break;
		}

		case REI_GLO_ALM:
		case REI_GLO_EPH:
		case REI_DEV_GLO_ALM:
		case REI_DEV_GLO_EPH: {
			svid_base = AI2_GLO_BASE + 1;
			break;
		}

		default: {
			break;
		}

	}

	/* Range checking. */
	if (sv_id_map == 0xFFFFFFFF) {
		action->action_mod = 0;
		sv_id_map = 0;
	}

	action->id = 0x02;
	action->len = 2;
	action->action_type = act;

	/* If the delete can be done in single AI2 command. */
	if (sv_id_map == 0) {
		action->action_mod = 0;

		d2h_ai2_encode((void *)action, mem, ai2_rec_action_sizes);

		if (ai2_ok != ai2_write(h2d_ai2_hdl, mem, 5)) {
			LOGPRINT(del_ast, err, "ai2 write fail");
			return -1;
		}

		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(del_ast, info, "assist del from device reqsted");
		} else {
			LOGPRINT(del_ast, err, "ai2 flush fail");
			return -1;
		}

		return 0;
	}

	/* If the delete needs multiple AI2 command. */
	do {
		count++;

		if(!(sv_id_map & (1 << count)))
			continue;

		/* On a match, clear map and inject. */
		sv_id_map &= ~(1 << count);
		action->action_mod = svid_base + count;

		d2h_ai2_encode((void *)action, mem, ai2_rec_action_sizes);

		if (ai2_ok != ai2_write(h2d_ai2_hdl, mem, 5)) {
			LOGPRINT(del_ast, err, "ai2 write fail");
			return -1;
		}

	} while (sv_id_map != 0);

	if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
		LOGPRINT(del_ast, info, "assist del from device reqsted");
	} else {
		LOGPRINT(del_ast, err, "ai2 flush fail")
		return -1;
	}

	return 0;
}

int del_assist(enum nvs_record rec_nvs, unsigned short elem_id,
					struct dev_aid_del *del_aid, int act)
{
	/* Inject assist delete to device. */
	if (act && (del_aid->mem_flags & MEM_DEV_RAM)) {
		if (dev_del_assist(elem_id, del_aid->sv_id_map, act)) {
			LOGPRINT(del_ast, err, "err deleting device");
			return -1;
		} else
			LOGPRINT(del_ast, info, "deleting rec [%d] from device", rec_nvs);
	}

	/* Delete the record from NVS. */
	if ((del_aid->mem_flags & MEM_SRV_RAM) ||
		(del_aid->mem_flags & MEM_SRV_NVM)) {
		if (0 == nvs_rec_del(rec_nvs, del_aid->sv_id_map)) {
			LOGPRINT(del_ast, err, "err deleting nvs");
			return -1;
		} else
			LOGPRINT(del_ast, info, "deleting rec [%d] from nvs", rec_nvs);
	}

	return 0;
}

static int h2d_dev_del_assist(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	enum nvs_record rec_nvs;
	struct dev_aid_del *del_aid = (struct dev_aid_del *) ie_adu->data;
	int act = 0; /* Receiver action for deleting assist from device. */

	switch(srv_re->elem_id) {
	case REI_DEV_GPS_TIM:
	case REI_DEV_GLO_TIM:
	case REI_GPS_TIM:
	case REI_GLO_TIM: {
			act = AI2_DEL_TIME;
			rec_nvs = nvs_gps_clk;
			LOGPRINT(del_ast, info, "delete time/clk");
			break;
		}

	case REI_DEV_GPS_POS:
	case REI_DEV_GLO_POS:
	case REI_REF_POS: {
			act = AI2_DEL_POS;
			rec_nvs = nvs_gps_pos;
			LOGPRINT(del_ast, info, "delete pos");
			break;
		}

	case REI_DEV_GPS_ALM:
	case REI_GPS_ALM: {
			act = AI2_DEL_ALM;
			LOGPRINT(del_ast, info, "req del gps alm [0x%X]",
							del_aid->sv_id_map);
			rec_nvs = nvs_gps_alm;
			break;
		}

	case REI_DEV_GLO_ALM:
	case REI_GLO_ALM: {
			act = AI2_DEL_ALM;
			LOGPRINT(del_ast, info, "req del glo alm [0x%X]",
							del_aid->sv_id_map);
			rec_nvs = nvs_glo_alm;
			break;
		}

	case REI_DEV_GPS_EPH:
	case REI_GPS_EPH: {
			act = AI2_DEL_EPH;
			LOGPRINT(del_ast, info, "req del gps eph [0x%X]",
							del_aid->sv_id_map);
			rec_nvs = nvs_gps_eph;
			break;
		}

	case REI_DEV_GLO_EPH:
	case REI_GLO_EPH: {
			act = AI2_DEL_EPH;
			LOGPRINT(del_ast, info, "req del glo eph [0x%X]",
							del_aid->sv_id_map);
			rec_nvs = nvs_glo_eph;
			break;
		}

	case REI_DEV_GPS_ION:
	case REI_DEV_GLO_ION:
	case REI_GPS_ION:
	case REI_GLO_ION: {
			act = AI2_DEL_ION_UTC;
			rec_nvs = nvs_gps_iono;
			LOGPRINT(del_ast, info, "delete iono");
			break;
		}

	case REI_DEV_GPS_UTC:
	case REI_GPS_UTC: {
				act = AI2_DEL_ION_UTC;
				rec_nvs = nvs_gps_utc;
				LOGPRINT(del_ast, info, "delete gps utc");
				break;
			}

	case REI_DEV_GLO_UTC:
	case REI_GLO_UTC: {
			act = AI2_DEL_ION_UTC;
			rec_nvs = nvs_glo_utc;
			LOGPRINT(del_ast, info, "delete glo utc");
			break;
		}

        case REI_DEV_GLO_RTI:
        case REI_DEV_GPS_RTI:
        case REI_GPS_RTI:
        case REI_GLO_RTI:
	case REI_DEV_SV_HLT: {
			act = AI2_DEL_SVHLTH;
			rec_nvs = nvs_hlth;
			LOGPRINT(del_ast, info, "delete sv health");
			break;
		}

	case REI_DEV_SV_DIR: {
			/* sv dir not stored in device. Set act = 0. */
			act = 0;
			rec_nvs = nvs_gps_svdr;
			LOGPRINT(del_ast, info, "delete gps sv dir");
			del_assist(rec_nvs, srv_re->elem_id, del_aid, act);

			rec_nvs = nvs_glo_svdr;
			LOGPRINT(del_ast, info, "delete glo sv dir");
			break;
		}

	default: {
				 LOGPRINT(del_ast, err, "invalid assist del req detected [0x%x]",
							srv_re->elem_id);
			return -1;
		}
	}

	return del_assist(rec_nvs, srv_re->elem_id, del_aid, act);
}

static struct dev_sv_dir * retrieve_svdr_for_prn(struct dev_sv_dir *sv_dir_list, 
												 int cnt, int prn)
{
	int loop = 0;

	while (loop < cnt) {
		if (sv_dir_list && (sv_dir_list[loop].prn == prn))
			return sv_dir_list + loop;

		loop++;
	}

	return NULL;
}

static struct dev_gps_eph * retrieve_gps_eph_for_prn(struct dev_gps_eph *eph_list, 
													 int cnt, int prn)
{
	int loop = 0;

	while (loop < cnt) {
		if (eph_list && (eph_list[loop].prn == prn))
			return eph_list + loop;

		loop++;
	}

	return NULL;
}

static struct dev_gps_alm * retrieve_gps_alm_for_prn(struct dev_gps_alm *alm_list, 
													 int cnt, int prn)
{
	int loop = 0;

	while (loop < cnt) {
		if (alm_list && (alm_list[loop].prn == prn))
			return alm_list + loop;

		loop++;
	}

	return NULL;
}

static struct dev_glo_eph * retrieve_glo_eph_for_prn(struct dev_glo_eph *eph_list, 
													 int cnt, int prn)
{
	int loop = 0;

	while (loop < cnt) {
		if (eph_list && (eph_list[loop].prn == prn))
			return eph_list + loop;

		loop++;
	}

	return NULL;
}

static struct dev_glo_alm * retrieve_glo_alm_for_prn(struct dev_glo_alm *alm_list, 
													 int cnt, int prn)
{
	int loop = 0;

	while (loop < cnt) {
		if (alm_list && (alm_list[loop].prn == prn))
			return alm_list + loop;

		loop++;
	}

	return NULL;
}

int check_if_gps_tim_valid(struct dev_gps_time *time_data, int *num_obj)
{
	unsigned char assist_cnt;
	int time_index;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	assist[0].record = nvs_gps_clk;
	time_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		LOGPRINT(nvs, info, "gps time passed");
		memcpy(time_data, assist[time_index].buffer, 
			   assist[time_index].n_objs * sizeof(struct dev_gps_time));
		*num_obj = assist[time_index].n_objs;
		ret_val = 1;
	}

	return ret_val;
}

int check_if_gps_utc_valid(struct dev_gps_utc *utc_data, int *num_obj)
{
	unsigned char assist_cnt;
	int utc_index;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	assist[0].record = nvs_gps_utc;
	utc_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		LOGPRINT(nvs, info, "gps utc passed");
		memcpy(utc_data, assist[utc_index].buffer, 
			   assist[utc_index].n_objs * sizeof(struct dev_gps_utc));
		*num_obj = assist[utc_index].n_objs;
		ret_val = 1;
	}

	return ret_val;
}

int check_if_gps_ion_valid(struct dev_gps_ion *ion_data, int *num_obj)
{
	unsigned char assist_cnt;
	int ion_index;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	assist[0].record = nvs_gps_iono;
	ion_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		LOGPRINT(nvs, info, "gps iono passed");
		memcpy(ion_data, assist[ion_index].buffer, 
			   assist[ion_index].n_objs * sizeof(struct dev_gps_ion));
		*num_obj = assist[ion_index].n_objs;
		ret_val = 1;
	}

	return ret_val;
}

int check_if_gps_pos_valid(struct dev_pos_info *pos_data, int *num_obj)
{
	unsigned char assist_cnt;
	int pos_index;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	assist[0].record = nvs_gps_pos;
	pos_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		LOGPRINT(nvs, info, "gps pos passed");
		memcpy(pos_data, assist[pos_index].buffer, 
			   assist[pos_index].n_objs * sizeof(struct dev_pos_info));
		*num_obj = assist[pos_index].n_objs;
		ret_val = 1;
	}

	return ret_val;
}

int check_if_gps_hlth_valid(struct dev_sv_hlth *hlth_data, int *num_obj)
{
	unsigned char assist_cnt;
	int hlth_index;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	assist[0].record = nvs_hlth;
	hlth_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		LOGPRINT(nvs, info, "gps sv health passed");
		memcpy(hlth_data, assist[hlth_index].buffer, 
			   assist[hlth_index].n_objs * sizeof(struct dev_sv_hlth));
		*num_obj = assist[hlth_index].n_objs;
		ret_val = 1;
	}

	return ret_val;
}

int check_if_gps_svdir_valid(struct dev_sv_dir *sv_dir_data, int *num_obj)
{
	unsigned char assist_cnt;
	int svdr_index;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	assist[0].record = nvs_gps_svdr;
	svdr_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		LOGPRINT(nvs, info, "gps sv dir passed");
		memcpy(sv_dir_data, assist[svdr_index].buffer, 
			   assist[svdr_index].n_objs * sizeof(struct dev_sv_dir));
		*num_obj = assist[svdr_index].n_objs;
		ret_val = 1;
	}

	return ret_val;
}

int check_if_gps_eph_valid(struct dev_gps_eph *eph_data, int *num_obj)
{
#define  C_HEALTH_UNKNOWN (0)               /* No health information */
#define  C_HEALTH_BAD     (1)               /* Satellite is sick */
#define  C_HEALTH_GOOD    (2)               /* Satellite is known good */
#define  C_HEALTH_NOEXIST (3)               /* Satellite does not exist */

#define C_LSB_ELEV_MASK   (90.0f / 128.0f)  /* Elevation degrees */
#define C_LSB_AZIM_MASK   (360.0f / 256.0f) /* Azimuth degrees */

	unsigned char assist_cnt;

	struct dev_gps_eph *eph_info;
	int eph_index;

	int count = 0;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	*num_obj = 0;
	memset(eph_data, 0, 32 * sizeof(struct dev_gps_eph));

	assist[0].record = nvs_gps_eph;
	eph_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		for (count = 0; count < 32; count++) {

			eph_info = retrieve_gps_eph_for_prn((struct dev_gps_eph *)
							    assist[eph_index].buffer, 
							    assist[eph_index].n_objs, 
							    count + 1);
			if (eph_info == NULL) {
				LOGPRINT(nvs, warn, "gps svid [%d] eph non-existent", count);
				continue;
			}

			LOGPRINT(nvs, info, "gps svid [%d] eph passed", count);
			memcpy(eph_data, eph_info, sizeof(struct dev_gps_eph));
			eph_data++;
			(*num_obj)++;
			ret_val = 1;
		}
	}

	return ret_val;
}

int check_if_glo_eph_valid(struct dev_glo_eph *eph_data, int *num_obj)
{
#define  C_HEALTH_UNKNOWN (0)               /* No health information */
#define  C_HEALTH_BAD     (1)               /* Satellite is sick */
#define  C_HEALTH_GOOD    (2)               /* Satellite is known good */
#define  C_HEALTH_NOEXIST (3)               /* Satellite does not exist */

	unsigned char assist_cnt;

	struct dev_glo_eph *eph_info;
	int eph_index;

	int count = 0;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	*num_obj = 0;
	memset(eph_data, 0, 24 * sizeof(struct dev_glo_eph));

	assist[0].record = nvs_glo_eph;
	eph_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		for (count = 0; count < 24; count++) {

			eph_info = retrieve_glo_eph_for_prn((struct dev_glo_eph *)
							    assist[eph_index].buffer, 
							    assist[eph_index].n_objs, 
							    count + 1 + AI2_GLO_BASE);
			if (eph_info == NULL) {
				LOGPRINT(nvs, warn, "glo svid [%d] eph non-existent", count);
				continue;
			}

			LOGPRINT(nvs, info, "glo svid [%d] eph passed", count);
			memcpy(eph_data, eph_info, sizeof(struct dev_glo_eph));
			eph_data++;
			(*num_obj)++;
			ret_val = 1;
		}
	}

	return ret_val;
}

int check_if_gps_alm_valid(struct dev_gps_alm *alm_data, int *num_obj)
{
#define  C_HEALTH_UNKNOWN (0)               /* No health information */
#define  C_HEALTH_BAD     (1)               /* Satellite is sick */
#define  C_HEALTH_GOOD    (2)               /* Satellite is known good */
#define  C_HEALTH_NOEXIST (3)               /* Satellite does not exist */

	unsigned char assist_cnt = 0;

	struct dev_gps_alm *alm_info;
	int alm_index;

	int count = 0;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	*num_obj = 0;
	memset(alm_data, 0, 32 * sizeof(struct dev_gps_alm));

	assist[0].record = nvs_gps_alm;
	alm_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		/* Check to see how many SVs whose almanac health is known */
		for (count = 0; count < 32; count++) {

			alm_info = retrieve_gps_alm_for_prn((struct dev_gps_alm *)
							    assist[alm_index].buffer, 
							    assist[alm_index].n_objs, 
							    count + 1);
			if (alm_info == NULL) {
				LOGPRINT(nvs, warn, "gps svid [%d] alm non-existent", count);
				continue;
			}

			LOGPRINT(nvs, info, "gps svid [%d] alm passed", count);
			memcpy(alm_data, alm_info, sizeof(struct dev_gps_alm));
			alm_data++;
			(*num_obj)++;
			ret_val = 1;
		}
	}

	return ret_val;
}

int check_if_glo_alm_valid(struct dev_glo_alm *alm_data, int *num_obj)
{
#define  C_HEALTH_UNKNOWN (0)               /* No health information */
#define  C_HEALTH_BAD     (1)               /* Satellite is sick */
#define  C_HEALTH_GOOD    (2)               /* Satellite is known good */
#define  C_HEALTH_NOEXIST (3)               /* Satellite does not exist */

	unsigned char assist_cnt = 0;

	struct dev_glo_alm *alm_info;
	int alm_index;

	int count = 0;
	int ret_val = 0;
	struct nvs_aid_data assist[1];

	*num_obj = 0;
	memset(alm_data, 0, 24 * sizeof(struct dev_glo_alm));

	assist[0].record = nvs_glo_alm;
	alm_index = 0;

	assist_cnt = read_nvs_aiding(assist, 1, 0);
	if (assist_cnt == 1) {

		/* Check to see how many SVs whose almanac health is known */
		for (count = 0; count < 24; count++) {

			alm_info = retrieve_glo_alm_for_prn((struct dev_glo_alm *)
							    assist[alm_index].buffer, 
							    assist[alm_index].n_objs, 
							    count + 1 + AI2_GLO_BASE);
			if (alm_info == NULL) {
				LOGPRINT(nvs, warn, "glo svid [%d] alm non-existent", count);
				continue;
			}

			LOGPRINT(nvs, info, "glo svid [%d] alm passed", count);
			memcpy(alm_data, alm_info, sizeof(struct dev_glo_alm));
			alm_data++;
			(*num_obj)++;
			ret_val = 1;
		}
	}

	return ret_val;
}

int get_assist_status(enum loc_assist assist, 
							 struct nvs_aid_status *status)
{
	struct nvs_aid_status aid_status[32];
	struct dev_gps_eph eph_gps[32];
	struct dev_gps_alm alm_gps[32];
	struct dev_glo_eph eph_glo[24];
	struct dev_glo_alm alm_glo[24];
	struct dev_gps_utc utc[1];
	struct dev_gps_ion ion[1];
	struct dev_gps_time time[1];
	struct dev_pos_info pos[1];
	int cnt = 0;
	int ttl_cnt = 0;
	int loop_ttl;
	int loop_aid;

	memset(aid_status, 0, 32 * sizeof(struct nvs_aid_status));

	switch (assist) {
		case a_GPS_EPH:
			for (loop_ttl = 0; loop_ttl < 32; loop_ttl++)
				aid_status[loop_ttl].obj_id = loop_ttl + 1;

			ttl_cnt = nvs_aid_ttl(nvs_gps_eph, aid_status, 32);
			if (ttl_cnt && check_if_gps_eph_valid(eph_gps, &cnt) && cnt) {

				for (loop_ttl = 0; loop_ttl < ttl_cnt; loop_ttl++) {

					for (loop_aid = 0; loop_aid < cnt; loop_aid++) {

						if (aid_status[loop_ttl].obj_id == eph_gps[loop_aid].prn) {

							memcpy(status, &aid_status[loop_ttl], 
								   sizeof(struct nvs_aid_status));
							status++;
							break;
						}
					}
				}
			}
			break;

		case a_GLO_EPH:
			for (loop_ttl = 0; loop_ttl < 24; loop_ttl++)
				aid_status[loop_ttl].obj_id = loop_ttl + 1;

			ttl_cnt = nvs_aid_ttl(nvs_glo_eph, aid_status, 24);
			if (ttl_cnt && check_if_glo_eph_valid(eph_glo, &cnt) && cnt) {

				for (loop_ttl = 0; loop_ttl < ttl_cnt; loop_ttl++) {

					for (loop_aid = 0; loop_aid < cnt; loop_aid++) {

						if ((aid_status[loop_ttl].obj_id + AI2_GLO_BASE) == eph_glo[loop_aid].prn) {

							memcpy(status, &aid_status[loop_ttl], 
								   sizeof(struct nvs_aid_status));
							status++;
							break;
						}
					}
				}
			}
			break;

		case a_GPS_ALM:
			for (loop_ttl = 0; loop_ttl < 32; loop_ttl++)
				aid_status[loop_ttl].obj_id = loop_ttl + 1;

			ttl_cnt = nvs_aid_ttl(nvs_gps_alm, aid_status, 32);
			if (ttl_cnt && check_if_gps_alm_valid(alm_gps, &cnt) && cnt) {

				for (loop_ttl = 0; loop_ttl < ttl_cnt; loop_ttl++) {

					for (loop_aid = 0; loop_aid < cnt; loop_aid++) {

						if (aid_status[loop_ttl].obj_id == alm_gps[loop_aid].prn) {

							memcpy(status, &aid_status[loop_ttl], 
								   sizeof(struct nvs_aid_status));
							status++;
							break;
						}
					}
				}
			}
			break;

		case a_GLO_ALM:
			for (loop_ttl = 0; loop_ttl < 24; loop_ttl++)
				aid_status[loop_ttl].obj_id = loop_ttl + 1;

			ttl_cnt = nvs_aid_ttl(nvs_glo_alm, aid_status, 24);
			if (ttl_cnt && check_if_glo_alm_valid(alm_glo, &cnt) && cnt) {

				for (loop_ttl = 0; loop_ttl < ttl_cnt; loop_ttl++) {

					for (loop_aid = 0; loop_aid < cnt; loop_aid++) {

						if ((aid_status[loop_ttl].obj_id + AI2_GLO_BASE) == alm_glo[loop_aid].prn) {

							memcpy(status, &aid_status[loop_ttl], 
								   sizeof(struct nvs_aid_status));
							status++;
							break;
						}
					}
				}
			}
			break;

		case a_GPS_UTC:
			aid_status[0].obj_id = 1;

			ttl_cnt = nvs_aid_ttl(nvs_gps_utc, aid_status, 1);

			if (ttl_cnt && check_if_gps_utc_valid(utc, &cnt) && cnt) {
				memcpy(status, aid_status, ttl_cnt * sizeof(aid_status));
			}
			break;

		case a_GPS_ION:
			aid_status[0].obj_id = 1;

			ttl_cnt = nvs_aid_ttl(nvs_gps_iono, aid_status, 1);

			if (ttl_cnt && check_if_gps_ion_valid(ion, &cnt) && cnt) {
				memcpy(status, aid_status, ttl_cnt * sizeof(aid_status));
			}
			break;

		case a_GPS_TIM:
			aid_status[0].obj_id = 1;

			ttl_cnt = nvs_aid_ttl(nvs_gps_clk, aid_status, 1);

			if (ttl_cnt && check_if_gps_tim_valid(time, &cnt) && cnt) {
				memcpy(status, aid_status, ttl_cnt * sizeof(aid_status));
			}
			break;

		case a_REF_POS:
			aid_status[0].obj_id = 1;

			ttl_cnt = nvs_aid_ttl(nvs_gps_pos, aid_status, 1);

			if (ttl_cnt && check_if_gps_pos_valid(pos, &cnt) && cnt) {
				memcpy(status, aid_status, ttl_cnt * sizeof(aid_status));
			}
			break;


		case a_GPS_ACQ:
			ttl_cnt = 1;
			status->obj_id = 0;
			status->ttl_ms = 0;
			break;

		case a_GPS_RTI:
			ttl_cnt = 1;
			status->obj_id = 0;
			status->ttl_ms = 0;
			break;

		case a_GPS_DGP:
			ttl_cnt = 1;
			status->obj_id = 0;
			status->ttl_ms = 0;
			break;

		default:
			break;
	}

	return ttl_cnt;
}

/*
 * 4. Request the assistance status.
 */
static int h2d_dev_req_assit_stat(struct srv_rei *srv_re, 
				  struct ie_desc *ie_adu)
{
	int ret_val = -1;

	struct aiding_status *in_status;

	struct nvs_aid_status status[32];
	struct nvs_aid_status out_status[32];
	struct nvs_aid_status *new_status = NULL;
	unsigned int out_status_cnt = 0;

	int st_sz;

	struct ie_head *head = IE_HEADER(ie_adu);
	struct rpc_msg *rmsg;
	struct ie_desc *adu;

	unsigned int count = 0;
	unsigned int loop_in;
	unsigned int loop_out;

	/* added default init to overcome KW issue*/
	enum loc_assist assist_id = a_GPS_DGP;

	memset(status, 0, 32 * sizeof(struct nvs_aid_status));
	memset(out_status, 0, 32 * sizeof(struct nvs_aid_status));

	switch(srv_re->elem_id) {
		case REI_GPS_EPH_AID_STAT:
			count = get_assist_status(a_GPS_EPH, status);
			assist_id = a_GPS_EPH;
			break;

		case REI_GLO_EPH_AID_STAT:
			count = get_assist_status(a_GLO_EPH, status);
			assist_id = a_GLO_EPH;
			break;

		case REI_GPS_ALM_AID_STAT:
			count = get_assist_status(a_GPS_ALM, status);
			assist_id = a_GPS_ALM;
			break;

		case REI_GLO_ALM_AID_STAT:
			count = get_assist_status(a_GLO_ALM, status);
			assist_id = a_GLO_ALM;
			break;

		case REI_GPS_UTC_AID_STAT:
			count = get_assist_status(a_GPS_UTC, status);
			in_status = (struct aiding_status *) ie_adu->data;
			in_status->obj_id = 1;
			assist_id = a_GPS_UTC;
			break;

		case REI_GPS_ION_AID_STAT:
			count = get_assist_status(a_GPS_ION, status);
			in_status = (struct aiding_status *) ie_adu->data;
			in_status->obj_id = 1;
			assist_id = a_GPS_ION;
			break;

		case REI_GPS_TIM_AID_STAT:
			count = get_assist_status(a_GPS_TIM, status);
			in_status = (struct aiding_status *) ie_adu->data;
			in_status->obj_id = 1;
			assist_id = a_GPS_TIM;
			break;

		case REI_REF_POS_AID_STAT:
			count = get_assist_status(a_REF_POS, status);
			in_status = (struct aiding_status *) ie_adu->data;
			in_status->obj_id = 1;
			assist_id = a_REF_POS;
			break;


		case REI_GPS_ACQ_AID_STAT:
			count = get_assist_status(a_GPS_ACQ, status);
			in_status = (struct aiding_status *) ie_adu->data;
			in_status->obj_id = 1;
			assist_id = a_GPS_ACQ;
			break;

		case REI_GPS_RTI_AID_STAT:
			count = get_assist_status(a_GPS_RTI, status);
			in_status = (struct aiding_status *) ie_adu->data;
			in_status->obj_id = 1;
			assist_id = a_GPS_RTI;
			break;

		case REI_GPS_DGP_AID_STAT:
			count = get_assist_status(a_GPS_DGP, status);
			in_status = (struct aiding_status *) ie_adu->data;
			in_status->obj_id = 1;
			assist_id = a_GPS_DGP;
			break;

		default:
			count = 0;
			break;
	}

	in_status = (struct aiding_status *) ie_adu->data;

	out_status_cnt = 0;

	for (loop_in = 0, out_status_cnt = 0; 
			(loop_in < (head->tot_len / ST_SIZE(aiding_status)) && loop_in < 32) && 
			out_status_cnt <32; 
			loop_in++, out_status_cnt++) {
		out_status[out_status_cnt].obj_id = in_status[loop_in].obj_id;
	}

	LOGPRINT(nvs, info, "assist type [%d,%s]",assist_id,assist_type_str[assist_id]);

	for (loop_out = 0; loop_out < out_status_cnt; loop_out++) {

		for (loop_in = 0; (loop_in < count && loop_in < 32); loop_in++) {

			if (out_status[loop_out].obj_id == status[loop_in].obj_id) {
				out_status[loop_out].ttl_ms = status[loop_in].ttl_ms;
				LOGPRINT(nvs, info, "obj id %d, ttl ms %d", 
					 out_status[loop_out].obj_id, out_status[loop_out].ttl_ms);
				break;
			}
		}

		if (loop_in >= count) {
			out_status[loop_out].ttl_ms = -1;
			LOGPRINT(nvs, info, "obj id %d, ttl ms %d", 
				 out_status[loop_out].obj_id, out_status[loop_out].ttl_ms);
		}
	}

	if ((rmsg = rpc_msg_alloc())) {
		if (out_status_cnt) {
			st_sz = out_status_cnt * ST_SIZE(nvs_aid_status);
			new_status = (struct nvs_aid_status *) malloc(st_sz);
			if (!new_status)
				goto exit1;

			memcpy(new_status, out_status, st_sz);
		} else {
			st_sz = 0;
			new_status = NULL;
		}

		adu = rpc_ie_attach(rmsg, srv_re->elem_id, 
				    CTW_2_OPER_ID(head->ct_word), st_sz, new_status);
		if (!adu)
			goto exit2;

		if(rpc_svr_send(rmsg) < 0)
			goto exit3;

		ret_val = 0;

exit3:
		rpc_ie_detach(rmsg, adu);

exit2:
		if (new_status)
			free(new_status);

exit1:
		rpc_msg_free(rmsg);
	}

	return ret_val;
}

int h2d_use_sequencer(struct srv_rei *srv_re, struct ie_desc *ie_adu);
/*
 * 4. Routing function for various assistance operations
 */
int h2d_assist_command(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	unsigned int ct_word = ie_adu->head.ct_word;
	unsigned int oper = CTW_2_OPER_ID(ct_word);
	int ret = 0;

	switch(oper) {
		case OPER_ADD: {
				ret = h2d_dev_add_assist(srv_re, ie_adu);
				break;
			}

		case OPER_DEL: {
                           struct dev_aid_del *del_aid = 
                                   (struct dev_aid_del *) ie_adu->data;

                           if (srv_re->elem_id == REI_GPS_TIM && 
                               del_aid->sv_id_map == 0)
                                   ret = h2d_use_sequencer(srv_re, ie_adu);
                           else
                                   ret = h2d_dev_del_assist(srv_re, ie_adu);
                           break;
			}

		case OPER_GET: {
				ret = h2d_dev_req_assist(srv_re, ie_adu);
				break;
			}

		case OPER_INF: {
				ret = h2d_dev_req_assit_stat(srv_re, ie_adu);
				break;
			}
		default: {
				LOGPRINT(generic, err, "invalid assist rpc oper %d", oper);
				ret = -1;
				break;
			}
	}

	return ret;
}

/*
 * 4. Routing function for various assistance operations
 */
int h2d_assist_stat_command(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	unsigned int ct_word = ie_adu->head.ct_word;
	unsigned int oper = CTW_2_OPER_ID(ct_word);
	int ret = 0;

	switch(oper) {
		case OPER_GET: {
				ret = h2d_dev_req_assit_stat(srv_re, ie_adu);
				break;
			}

		default: {
				LOGPRINT(generic, err, "invalid assist rpc oper %d", oper);
				ret = -1;
				break;
			}
	}

	return ret;
}

