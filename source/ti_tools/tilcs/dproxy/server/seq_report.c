/*
 * seq_report.c
 *
 * Sequencer related reporting functions.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "report.h"
#include "inject.h"
#include "dev2host.h"
#include "ai2.h"
#include "logger.h"
#include "utils.h"
#include "config.h"
#include "os_services.h"
#include "device.h"

/* Function declarations. */
static int d2h_ai2_decode(unsigned char *buf, unsigned int *data, char *sizes);

/* Generic decoder. */
static int d2h_ai2_decode(unsigned char *buf, unsigned int *data, char *sizes)
{
	if (data == 0 || sizes == 0)
		return ERR;

	while(*sizes != 0) {
		*data = ai2_get_nbytes(&buf, *sizes);
		data++;
		sizes++;
	}

	return OK;
}

/* 0xF5 Invalid packet. */
struct ai2_inv_pkt {
	unsigned int id;
	unsigned int len;
	unsigned int inv_pkt_id;
	unsigned int err_type;
};
/* add for GNSS recovery ++*/

/* 0x80 Async Event. */
struct ai2_async_evt {
	unsigned int id;
	unsigned int len;
	unsigned int event;
	unsigned int event_data;
	unsigned int fcount;
	unsigned int sub_ms;
	unsigned int trunc_cur_fcount;
};


static int dpxy_decode_async_evt(struct d2h_ai2 *d2h_ai2)
{
	struct ai2_async_evt *pkt = d2h_ai2->ai2_struct;
	char ai2_async_evt_sizes[] = {1, 2, 1, 1, 4, 2, 2, 0};

	enum async_evt{
		ENGINE_ON = 0,
		ENGINE_OFF,
		NEW_ALMANAC,
		NEW_EPHEMERIS,
		NEW_IONO,
		NEW_HEALTH,
		EXT_HW_EVT,
		ENGINE_IDLE,
		NO_NEW_POS,
		NEW_UTC,
		SV_ASSIGN_MODE,
		POWER_SAVE_ENTRY,
		POWER_SAVE_EXIT,
		GPS_AWAKE,
		SV_DELETION = 16
	};
	
	unsigned int msec = os_time_msecs();

	LOGPRINT(async_evnt, info, "asyn evt [0x%x] recd", d2h_ai2->ai2_packet[3]);

	d2h_ai2_decode(d2h_ai2->ai2_packet, (unsigned int *)pkt,
							ai2_async_evt_sizes);

	switch (pkt->event) {
		case ENGINE_ON:
				LOGPRINT(async_evnt, info, "engine on: FC: %u, msec: %u",pkt->fcount, msec);
			set_rx_on_time(msec);
			break;
		case ENGINE_OFF:
				LOGPRINT(async_evnt, info, "engine off: FC: %u, msec: %u",pkt->fcount, msec);
			set_rx_on_time(0);
			break;
		case NEW_ALMANAC:
				LOGPRINT(async_evnt, info, "new almanac available: FC: %u, msec: %u",
							pkt->fcount, msec);
			dev_req_assist(h2d_alm, pkt->event_data);
			break;
		case NEW_EPHEMERIS:
				LOGPRINT(async_evnt, info, "new ephemeris available: FC: %u, msec: %u",
							pkt->fcount, msec);
			dev_req_assist(h2d_eph, pkt->event_data);
			break;
		case NEW_IONO:
				LOGPRINT(async_evnt, info, "new iono available: FC: %u, msec: %u",
							pkt->fcount, msec);
			dev_req_assist(h2d_iono, 0);
			dev_req_assist(h2d_utc, 0);
			break;
		case NEW_HEALTH:
				LOGPRINT(async_evnt, info, "new sv health available: FC: %u, msec: %u",
							pkt->fcount, msec);
			dev_req_assist(h2d_hlth, 0);
			break;
		case EXT_HW_EVT:
				LOGPRINT(async_evnt, info, "external hw event: FC: %u, msec: %u",
							pkt->fcount, msec);
			break;
		case ENGINE_IDLE:
				LOGPRINT(async_evnt, info, "engine idle: FC: %u, msec: %u",
							pkt->fcount, msec);
			set_rx_on_time(0);
			break;
		case NO_NEW_POS:
				LOGPRINT(async_evnt, info, "no new position: FC: %u, msec: %u",
							pkt->fcount, msec);
			break;
		case NEW_UTC:
				LOGPRINT(async_evnt, info, "new utc available: FC: %u, msec: %u",
							pkt->fcount, msec);
			dev_req_assist(h2d_utc, 0);
			break;
		case SV_ASSIGN_MODE:
				LOGPRINT(async_evnt, info, "sv assign mode: FC: %u, msec: %u",
							pkt->fcount, msec);
			break;
		case POWER_SAVE_ENTRY:
				LOGPRINT(async_evnt, info, "pwsv active entry condition met: FC: %u, msec: %u",
							pkt->fcount, msec);
			break;
		case POWER_SAVE_EXIT:
				LOGPRINT(async_evnt, info, "pwsv active exit condition met: FC: %u, msec: %u",
							pkt->fcount, msec);
			break;
		case GPS_AWAKE:
				LOGPRINT(async_evnt, info, "engine wakeup: FC: %u, msec: %u",pkt->fcount, msec);
			break;
		case SV_DELETION:
				LOGPRINT(async_evnt, info, "SV deletion: FC: %u, msec: %u",pkt->fcount, msec);
			break;
		default:
				LOGPRINT(async_evnt, info, "Unknown async event: FC: %u, msec: %u", pkt->fcount, msec);
			break;
	}

	return OK;
}

static int dpxy_async_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
							 unsigned int rei_select)
{
	struct ai2_async_evt *async_evt = (struct ai2_async_evt *)ai2_d2h->ai2_struct;
	struct ie_desc *ie_adu[1];
	int n_adu = 0;
	int st_sz = 4;
	unsigned int *loc_err = (unsigned int *)malloc(st_sz);
	if (!loc_err)
		goto exit1;

	if (async_evt->event == 8) {
		*loc_err = async_evt->event_data;
		LOGPRINT(async_evnt, info, "no new position, send REI_LOC_EVT as [%d]", 
				 async_evt->event_data);
	} else {
		goto exit1;
	}

	ie_adu[n_adu] = rpc_ie_attach(rmsg, REI_LOC_EVT, OPER_INF, st_sz, loc_err);
	if (!ie_adu[n_adu++])
		goto exit2;

	UNUSED(rei_select);

	return 0;

exit2:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

exit1:
	free(loc_err);

	return -1;
}

struct d2h_ai2 d2h_async_rec_evt = {
	.ai2_st_len = sizeof(struct ai2_async_evt),
	.gen_struct = dpxy_decode_async_evt,
	.do_ie_4rpc = dpxy_async_rpc_ie
};

/* 0xF3 & 0xF4 Patch download status. */
static int dpxy_decode_patch_err(struct d2h_ai2 *d2h_ai2)
{
	unsigned char *msg = d2h_ai2->ai2_packet;

	if (msg[0] == 0xF4 && msg[3] == 0) {
		LOGPRINT(patch, info, "patch dwnld complete");
	} else if (msg[0] == 0xF4 && msg[3] == 8) {
		LOGPRINT(patch, warn, "patch dwnld finished with err");
	} else if (msg[0] == 0xF3 && msg[5] == 0) {
		LOGPRINT(patch, info, "patch rec dwnld ok");
	} else if (msg[0] == 0xF3 && msg[5] != 0) {
		LOGPRINT(patch, err, "patch rec %d dwnld failed with code 0x%x",
				((msg[4] << 8 ) | msg[3]), msg[5]);
	}

	return 0;
}

struct d2h_ai2 d2h_ptch_dwld_stat = {
	.ai2_st_len = sizeof(struct no_object),
	.gen_struct = dpxy_decode_patch_err
};

/* Dummy function definition. */
int no_function(struct d2h_ai2 *d2h_ai2)
{
	UNUSED(d2h_ai2);
	return 0;
}

static int dev_status_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_status *dev_stat= (struct dev_gps_status *)ai2_d2h->ai2_struct;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;
	unsigned char chip_id;

	
	dev_stat->status_word		= BUF_LE_U2B_RD_INC(ai2_pkt);
	
	chip_id                      = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->chip_id_major       = (chip_id >> 4);
	dev_stat->gps_ver->chip_id_minor       = (chip_id & 0x0F);

	/* Ignore system config */
	ai2_pkt++;

	dev_stat->gps_ver->rom_id_sub_minor2   = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->rom_id_sub_minor1   = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->rom_id_minor		 = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->rom_id_major		 = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->rom_month           = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->rom_day             = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->rom_year            = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->patch_major         = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->patch_minor         = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->patch_month         = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->patch_day           = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_stat->gps_ver->patch_year          = BUF_LE_U2B_RD_INC(ai2_pkt);

	dev_stat->ref_clk					   = BUF_LE_U4B_RD_INC(ai2_pkt);

	sprintf(rom_version, "_%d_%d_%d_%d", dev_stat->gps_ver->rom_id_major,dev_stat->gps_ver->rom_id_minor,
		dev_stat->gps_ver->rom_id_sub_minor1, dev_stat->gps_ver->rom_id_sub_minor2);

	print_data(REI_DEV_STATUS,dev_stat);
	
	return (ai2_pkt - ai2_ref);
}

static int status_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_status_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

#define EXT_PVT_ST_LEN	ST_SIZE(dev_gps_status)
struct d2h_ai2 d2h_dev_status = {
	.ai2_st_len = EXT_PVT_ST_LEN,
	.gen_struct = status_ext_struct
};

struct d2h_ai2 d2h_dev_cust_cfg = {
	.ai2_st_len = sizeof(struct no_object),
	.gen_struct = no_function
};

struct d2h_ai2 d2h_evt_config = {
	.ai2_st_len = sizeof(struct no_object),
	.gen_struct = no_function
};

struct d2h_ai2 d2h_ping_resp = {
	.ai2_st_len = sizeof(struct no_object),
	.gen_struct = no_function
};

struct d2h_ai2 d2h_calib_resp = {
	.ai2_st_len = sizeof(struct no_object),
	.gen_struct = no_function
};

