/*
 * apm.h
 *
 * Advance power management functionalities
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include "host2dev.h"

int h2d_inj_apm_ctrl_from_config(void *data);
int h2d_inj_apm_disable(void *data);
int h2d_apm_command(struct srv_rei *srv_re, struct ie_desc *ie_adu);
