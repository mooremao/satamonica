#include "gnss_utils.h"
#include "os_services.h"

static unsigned char get_gps_wk_ro()
{
	return (os_utc_secs() - UTC_AT_GPS_ORIGIN) / wk2secs(1024);
}

static unsigned char get_glo_day_ro()
{
	return (os_utc_secs() - UTC_AT_GLO_ORIGIN) / day2secs(8192);
}

unsigned int gps_time_now(unsigned short nr_week, unsigned int wk_secs)
{
	return wk2secs(nr_week + 1024*get_gps_wk_ro()) + 
		wk_secs + UTC_AT_GPS_ORIGIN;
}

unsigned int glo_time_now(unsigned short day, unsigned int tod)
{
	return day2secs(day + 8192*get_glo_day_ro()) + 
		tod + GLO_START_RU_TIME;
} 

static unsigned char calc_glo_N4_param(unsigned int glo_now)
{
	unsigned int elapsed = glo_now - GLO_START_RU_TIME; 
	unsigned int calc_N4 = elapsed / MAX_SECS_4Y_BLOCK + 1;

	return (calc_N4 & 0xFF);
}

unsigned char get_glo_N4_param(unsigned int glo_now)
{
	return (glo_now > UTC_AT_GLO_ORIGIN)? calc_glo_N4_param(glo_now) : 0;  
}

static unsigned int utc2glo_secs(unsigned int utc_now)
{
	return (utc_now - UTC_AT_GLO_ORIGIN);
}

static unsigned int utc2gps_secs(unsigned int utc_now)
{
	return (utc_now - UTC_AT_GPS_ORIGIN);
}

unsigned char utc2glo_N4_param(unsigned int utc_now)
{
	return (utc_now > UTC_AT_GLO_ORIGIN)? 
		calc_glo_N4_param(utc2glo_secs(utc_now)) : 0;
}

static unsigned short calc_glo_NT_param(unsigned int glo_now)
{
	unsigned int elapsed = glo_now - GLO_START_RU_TIME;
	unsigned int calc_N4 = elapsed / MAX_SECS_4Y_BLOCK + 1;
	unsigned int calc_NT = elapsed - (calc_N4 - 1) * MAX_SECS_4Y_BLOCK;

	calc_NT = calc_NT / MAX_SECS_IN_A_DAY; // Convert to days

	return ((calc_NT + 1) & 0xFFFF); // NT as per GLONASS ICD is 1 based.
}

unsigned short utc2glo_NT_param(unsigned int utc_now)
{
	return (utc_now > UTC_AT_GLO_ORIGIN)? 
		calc_glo_NT_param(utc2glo_secs(utc_now)) : 0;
}

unsigned short get_glo_NT_param(unsigned int glo_now)
{
	return (glo_now > GLO_START_RU_TIME)? calc_glo_NT_param(glo_now) : 0;
}

