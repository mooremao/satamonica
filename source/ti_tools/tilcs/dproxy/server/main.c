/*
 * main.c
 *
 * devproxy main application
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>

#include "dev2host.h"
#include "host2dev.h"
#include "os_services.h"
#include "logger.h"
#include "utils.h"
#include "config.h"
#include "release.h"

#include "ti_log.h"
#include "log_id.h"

static void os_daemonize_process(void)
{
	pid_t pid;
	int fd;
	int lock_fd;
	int num_fd;
	char str[20];
	/* change running directory */
	chdir(RUNNING_DIR);

	lock_fd = open(LOCK_FILE, (O_RDWR | O_CREAT),
				   (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
	if (lock_fd < 0) {
		LOGPRINT(os, err, "open lock file failed");
		exit(1); /* can not open */
	}

	ftruncate(lock_fd, 0);

	if (flock(lock_fd, (LOCK_EX | LOCK_NB)) < 0) {
		LOGPRINT(os, err, "lock file failed, another instance already running");
		exit(0); /* can not lock */
	}

	/* first instance continues */

	/* record pid to lockfile */
	sprintf(str, "%d\n", (int)getpid());
	write(lock_fd, str, strlen(str));
	fsync(lock_fd);
        close(lock_fd);
}

void *term_sig;
extern void *cli_term_sig;

int main(int argc, char *argv[])
{
	struct thread_sync {
		void *mutex;
		int  flag;
	} th_sync[1];
	void *d2h_thrd;
	void *h2d_thrd;
	char h2dthr_name[25] = "D-PROXY_H2D_THREAD";
	char d2hthr_name[25] = "D-PROXY_D2H_THREAD";
	char dproxy_main_thr[25] = "D-PROXY_MAIN_THREAD";
	pthread_t thread_id;

#ifdef ANDROID
	//os_daemonize_process();
#else
	os_daemonize_process();

#endif
    ti_log_init(e_log_d_proxy);

	/* : Process command line arguments. */

	LOGPRINT(ver, info, "TI devproxy version:");
	LOGPRINT(ver, info, "Major : [%d]", MAJOR);
	LOGPRINT(ver, info, "Minor : [%d]", MINOR);
	LOGPRINT(ver, info, "Patch : [%d]", PATCH);
	LOGPRINT(ver, info, "Build : [%d]", BUILD);
	LOGPRINT(ver, info, "Exten : [%c]", EXTEN);
	LOGPRINT(ver, info, "Day   : [%d]", DAY);
	LOGPRINT(ver, info, "Month : [%d]", MONTH);
	LOGPRINT(ver, info, "Year  : [%d]", YEAR);

	os_services_init();

	term_sig = os_sem_init();
	


	th_sync->mutex = os_mutex_init();
	th_sync->flag = 0;

	thread_id = pthread_self();

	if(pthread_setname_np(thread_id, dproxy_main_thr) != 0)	{
		LOGPRINT(os, err, "pthread_setname_np failed in main");
	}
	/* h2d. */
	if ((h2d_thrd = os_create_thread(h2d_thread, th_sync, h2dthr_name)) == NULL)
		return -1;

	/* d2h. */
	if ((d2h_thrd = os_create_thread(d2h_thread, th_sync, d2hthr_name)) == NULL)
		return -1;

	os_sem_take(term_sig);

	os_destroy_thread(h2d_thrd);

	os_destroy_thread(d2h_thrd);

	rpc_svr_exit();

	os_services_exit();

	LOGPRINT(os, info, "server main exiting");
	UNUSED(argc);
	UNUSED(argv);

	return 0;
}
