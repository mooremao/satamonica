/*
 * ion_report.c
 *
 * Ionospheric report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "nvs.h"
#include "log_report.h"

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int ion_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_ion *ion_gps);

int ion_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_ion *ion_gps);

int ion_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_ion *ion_gps);

int ion_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_ion *ion_gps);

static struct ie_desc *mk_ie_adu_4ion_gps_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_gps_ion *ion_gps)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_gps_ion);
	struct dev_gps_ion *n_ion = (struct dev_gps_ion *)malloc(st_size);
	if (!n_ion)
		goto exit1;

	memcpy(n_ion, ion_gps, st_size);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GPS_ION, oper_id, st_size, n_ion);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(n_ion);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4ion_gps_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_gps_ion *ion_gps)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_gps_ion);
	struct gps_ion *n_ion = (struct gps_ion *) malloc(st_size);
	if (!n_ion)
		goto exit1;

	n_ion->Alpha0  = ion_gps->Alpha0;
	n_ion->Alpha1  = ion_gps->Alpha1;
	n_ion->Alpha2  = ion_gps->Alpha2;
	n_ion->Beta0   = ion_gps->Beta0;
	n_ion->Beta1   = ion_gps->Beta1;
	n_ion->Beta2   = ion_gps->Beta2;
	n_ion->Beta3   = ion_gps->Beta3;

	ie_adu = rpc_ie_attach(rmsg, REI_GPS_ION, oper_id, st_size, n_ion);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(n_ion);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4ion_glo_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_glo_ion *ion_gps)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_glo_ion);
	struct dev_glo_ion *n_ion = (struct dev_glo_ion *)malloc(st_size);
	if (!n_ion)
		goto exit1;

	memcpy(n_ion, ion_gps, st_size);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GLO_ION, oper_id, st_size, n_ion);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(n_ion);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4ion_glo_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_glo_ion *ion_gps)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_glo_ion);
	struct ganss_addl_iono *n_ion = (struct ganss_addl_iono *) malloc(st_size);
	if (!n_ion)
		goto exit1;

	n_ion->data_id = 0;
	n_ion->alfa0   = ion_gps->Alpha0;
	n_ion->alfa1   = ion_gps->Alpha1;
	n_ion->alfa2   = ion_gps->Alpha2;
	n_ion->alfa3   = ion_gps->Alpha3;
	n_ion->beta0   = ion_gps->Beta0;
	n_ion->beta1   = ion_gps->Beta1;
	n_ion->beta2   = ion_gps->Beta2;
	n_ion->beta3   = ion_gps->Beta3;

	ie_adu = rpc_ie_attach(rmsg, REI_GLO_ION, oper_id, st_size, n_ion);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(n_ion);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int ion_gps_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_gps_ion *ion_gps)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4ion_gps_dev(rmsg, oper_id, ion_gps);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int ion_gps_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_gps_ion *ion_gps)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4ion_gps_3gpp(rmsg, oper_id, ion_gps);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int ion_glo_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_glo_ion *ion_gps)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4ion_glo_dev(rmsg, oper_id, ion_gps);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg
   -1 --> Error
 */
static int ion_glo_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_glo_ion *ion_gps)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4ion_glo_3gpp(rmsg, oper_id, ion_gps);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int ion_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_ion *ion_gps)
{
	return ion_gps_create_dev_ie(rmsg, oper_id, ion_gps);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int ion_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_ion *ion_gps)
{
	return ion_gps_create_3gpp_ie(rmsg, oper_id, ion_gps);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int ion_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_ion *ion_gps)
{
	return ion_glo_create_dev_ie(rmsg, oper_id, ion_gps);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int ion_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_ion *ion_gps)
{
	return ion_glo_create_3gpp_ie(rmsg, oper_id, ion_gps);
}

static int ion_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  unsigned int rei_select)
{
	struct dev_gps_ion *ion_gps = (struct dev_gps_ion *)ai2_d2h->ai2_struct;
	struct dev_glo_ion *ion_glo = (struct dev_glo_ion *)ai2_d2h->ai2_struct;

	if (d2h_get_msg_id(ai2_d2h) == 0x0C) {
		if (rei_select & NEW_ION_GPS_NATIVE) {
			if (-1 == ion_gps_create_dev_ie(rmsg, OPER_INF, ion_gps))
				return -1;
		} else if (rei_select & NEW_ION_GPS_IE_IND) {
			if (-1 == ion_gps_create_3gpp_ie(rmsg, OPER_INF, ion_gps))
				return -1;
		}
	} else {
		if (rei_select & NEW_ION_GLO_NATIVE) {
			if (-1 == ion_glo_create_dev_ie(rmsg, OPER_INF, ion_glo))
				return -1;
		} else if (rei_select & NEW_ION_GLO_IE_IND) {
			if (-1 == ion_glo_create_3gpp_ie(rmsg, OPER_INF, ion_glo))
				return -1;
		}
	}

	return 0;
}

static int dev_ion_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_ion *ion_gps = (struct dev_gps_ion *)ai2_d2h->ai2_struct;
	char  *ai2_pkt               = (char*)ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;
	int ai2_pkt_sz               = ai2_d2h->ai2_pkt_sz;

	if (ai2_offset < ai2_pkt_sz) {
		ion_gps->Alpha0 = BUF_LE_S1B_RD_INC(ai2_pkt);
		ion_gps->Alpha1 = BUF_LE_S1B_RD_INC(ai2_pkt);
		ion_gps->Alpha2 = BUF_LE_S1B_RD_INC(ai2_pkt);
		ion_gps->Alpha3 = BUF_LE_S1B_RD_INC(ai2_pkt);
		ion_gps->Beta0  = BUF_LE_S1B_RD_INC(ai2_pkt);
		ion_gps->Beta1  = BUF_LE_S1B_RD_INC(ai2_pkt);
		ion_gps->Beta2  = BUF_LE_S1B_RD_INC(ai2_pkt);
		ion_gps->Beta3  = BUF_LE_S1B_RD_INC(ai2_pkt);

		LOGPRINT(ion_rpt, info, "ion report");
		print_data(REI_DEV_GPS_ION, ion_gps);
	} else
		LOGPRINT(ion_rpt, info, "no ion data");

	return (ai2_pkt - ai2_ref);
}

static int ion_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if (ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_ion_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int ion_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0;
	struct dev_gps_ion *iono_rpt = (struct dev_gps_ion*)ai2_d2h->ai2_struct;
		int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	LOGPRINT(ion_rpt, info, "storing iono report in nvs");

	if(ai2_pkt_sz == 0) {
		/*Return as nothing to write*/
		return 0;		
	}
	wr_suc = nvs_rec_add(nvs_gps_iono, iono_rpt, 1);

	if (wr_suc != nvs_success) {
		LOGPRINT(ion_rpt, err, "error storing iono report in nvs");
		return -1;
	}

	return 0;
}

#define EXT_PVT_ST_LEN (ST_SIZE(dev_gps_ion))

struct d2h_ai2 d2h_ion_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.rei_select = (NEW_ION_GPS_IE_IND | NEW_ION_GLO_IE_IND),

	.gen_struct = ion_ext_struct,
	.do_ie_4rpc = ion_rpc_ie,
	.nvs_action = ion_nvs_info
};

