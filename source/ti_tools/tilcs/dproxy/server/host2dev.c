/*
 * host2dev.c
 *
 * Framework to transfer the command/data from host to device.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>

#include "host2dev.h"
#include "rpc.h"
#include "ai2.h"
#include "logger.h"
#include "sequencer.h"
#include "inject.h"
#include "ai2_log.h"
#include "config.h"
#include "utils.h"
#include "device.h"
#include "nvs.h"
#include "dev_proxy.h"
#include "os_services.h"
#include "nvs_mod_init.h"
#include "pa_blank.h"
#include "apm.h"

#define H2D_OK 0
#define H2D_ERR -1

static struct thread_sync {
	void *mutex;
	int  flag;
} *h2d_sync;

static unsigned int new_log_flag = 0;

static struct dev_start_up_info startup_info[1];

extern void *term_sig;
extern void d2h_sync_flg_update(void);
/*
 * Code Group 1: Low leven functions driven by RPC.
 * This group of functions consists low level logic for generating AI2 data
 * specific to a RPC command received. It also contains related utility.
 */
 static int h2d_dev_connect(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	unsigned int *local_new_log_flag;
	unsigned int *logging_mask;
	int ret_val = -1;
	struct nvs_config nvs_cfg[1];
	struct ai2_log_cfg cfg[1];
	struct dev_start_up_info *start_info = (struct dev_start_up_info *)
								ie_adu->data;
	struct nvs_rec_info *rec_info =  get_rec_info_tbl( );
	struct timeval profile_time_start[1], profile_time_end[1];

	/* : Store patch file name and use it. */
	/* 1. Initialize per use case logging. */
	memcpy(cfg->log_name, AI2_LOG_FILE_PREFIX, sizeof(AI2_LOG_FILE_PREFIX));
	cfg->modes = AI2_LOG_MODE_TIMESTAMP;
	if (0 != ai2_log_init(cfg)) {
		LOGPRINT(h2d, err, "sensor dump init error");
	}

	// : Move this NVS initialization to main()
	/* 2. NVS initialization. */
	strncpy(nvs_cfg->path, NVS_FILE_PATH, MAX_FILE_SIZE - 1);
	ret_val = nvs_init(nvs_cfg);
	LOGPRINT(h2d, info, "nvs init %s", ret_val ? "err" : "ok");
	while (rec_info->record != nvs_rec_end) {
		ret_val = nvs_rec_setup(rec_info->record, rec_info->param);
		LOGPRINT(h2d, info, "nvs rec setup %s", ret_val ? "err" : "ok");
		rec_info++;
	}

	/* 3. Config file. */
	ret_val = sys_config_init(start_info->config_file);
	if (cfg_success != ret_val) {
		LOGPRINT(h2d, warn, "config init fail")
	}

	/* initlize logging */
	if (cfg_success == sys_param_get(cfg_new_log_flag,(void**) &local_new_log_flag))
		new_log_flag = *local_new_log_flag;

	if (cfg_success == sys_param_get(cfg_logging_mask,(void**) &logging_mask)) {
		update_logmask(*logging_mask);		
	}

	//nvs_display_all();

	// print_config_params();
	
	UNUSED(srv_re);

	gettimeofday(profile_time_start, NULL);
	ret_val = (SEQ_OK == seq_execute(ready, ie_adu->data)) ? H2D_OK : H2D_ERR;
	gettimeofday(profile_time_end, NULL);
	LOGPRINT(d2h, info, "Patch download and nvs injection complete duration [%u]", 
			 ((profile_time_end->tv_sec - profile_time_start->tv_sec) * 1000) 
			 + ((profile_time_end->tv_usec - profile_time_start->tv_usec) / 1000));

	memcpy(startup_info, start_info, sizeof(struct dev_start_up_info));

	return ret_val;
}

static int h2d_dev_release_resource(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	LOGPRINT(h2d, info, "h2d_dev_release_resource ++");
	
	//ai2_log_exit();

	nvs_exit();

	sys_config_exit();

	
	os_mutex_take(h2d_sync->mutex);
	h2d_sync->flag = 1;
	os_mutex_give(h2d_sync->mutex);
	
	os_sem_give(term_sig);
	LOGPRINT(h2d, info, "h2d_dev_release_resource --");	

	UNUSED(srv_re);
	UNUSED(ie_adu);

	return H2D_OK;
}

static int h2d_dev_release(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	LOGPRINT(h2d, info, "h2d_dev_release ++");
	seq_execute(cleanup, NULL);

	
	h2d_dev_release_resource(srv_re, ie_adu);

	LOGPRINT(h2d, info, "h2d_dev_release --");	
	UNUSED(srv_re);
		UNUSED(ie_adu);
		
	return H2D_OK;
}


int h2d_use_sequencer(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	struct ai2_log_cfg cfg[1];
	unsigned int *local_new_log_flag;
	unsigned int *logging_mask;
	enum seq_id seq_id = null;
		
	LOGPRINT(h2d, info, "using sequencer");
	switch(srv_re->elem_id) {
		case REI_DEV_ACTIVE:
			{
				seq_id = active;

				if (cfg_success != sys_config_init(startup_info->config_file))
					LOGPRINT(h2d, warn, "config init failed before READY_SEQ");

				if (cfg_success == sys_param_get(cfg_new_log_flag, 
												 (void**) &local_new_log_flag)) {
					if (new_log_flag != *local_new_log_flag) {

						LOGPRINT(h2d, info, "new sensor dump init");

						ai2_log_reset();

						memcpy(cfg->log_name, AI2_LOG_FILE_PREFIX, 
							   sizeof(AI2_LOG_FILE_PREFIX));
						cfg->modes = AI2_LOG_MODE_TIMESTAMP;

						if (0 != ai2_log_init(cfg)) {
							LOGPRINT(h2d, err, "sensor dump init error");
						}

						new_log_flag = *local_new_log_flag;
					} else
						LOGPRINT(h2d, info, "continue with old sensor dump");
				}

				
				if (cfg_success == sys_param_get(cfg_logging_mask, 
												 (void**) &logging_mask)) {
					update_logmask(*logging_mask);		
				}
			}
			break;

		case REI_DEV_DE_ACT:
			seq_id = deact;
			break;

		case REI_DEV_DSLEEP:
			seq_id = dsleep;
			break;

		case REI_DEV_WAKEUP:
			seq_id = wakeup;
			break;

		case REI_DEV_CFG_RPT:
			seq_id = reports;
			break;

		case REI_CLK_CALIBRATE:	{
			unsigned int* calib_enable_flag;
			unsigned int *calib_mode;
				
			struct dev_clk_calibrate *calib = 
								(struct dev_clk_calibrate *)ie_adu->data;
				
				if(calib->flags == DEV_CALIB_AUTO ){
					
					sys_param_get(cfg_calib_enable_flag,(void**) &calib_enable_flag);
					sys_param_get(cfg_calib_mode,(void**) &calib_mode);
					if(*calib_enable_flag){
						LOGPRINT(h2d, info, "Config: Calib Request Flag: %d",calib->flags);
						if (*calib_mode==0){
							seq_id = calib_single_shot;
							LOGPRINT(h2d, info, "Auto calib Single shot Enabled");
						}else if (*calib_mode==1){
							seq_id = calib_periodic;
							LOGPRINT(h2d, info, "Auto calib Periodic Enabled");
						}
					}
					break;
				}

				LOGPRINT(h2d, info, "Modem: Calib Request Flag: %d",calib->flags);
				if(calib->flags == DEV_CALIB_UNDO ){
					LOGPRINT(h2d, info, "Undo Calib");
					seq_id = calib_undo;
					break;
				}
				
				if(calib->flags & DEV_CALIB_SINGLE_SHOT){
					seq_id = calib_single_shot;
					LOGPRINT(h2d, info, "Single shot calib requested");
				} else {
					seq_id = calib_periodic;
					LOGPRINT(h2d, info, "Periodic calib requested");
				}					
				
			}
			break;
			/* add for GNSS recovery ++*/
		case REI_ERR_RECOVERY:
			seq_id = protocol_select;
			LOGPRINT(h2d, info, "err_seq: Error recovery sequence requested");
			break;			
			
		/* error trigger test code, to be removed */
		case REI_FATAL_ERR:
			seq_id = fatal_err_select;
			LOGPRINT(h2d, info, "err_seq: Fatal error sequence requested");
			break;			
		
		/* error trigger test code, to be removed */
		case REI_EXCEPTION_ERR:
			seq_id = exception_err_select;
			LOGPRINT(h2d, info, "err_seq: Exception error sequence requested");
			break;			
			
			/* add for GNSS recovery --*/
	    case REI_MEAS_REP_RECOVERY :
			seq_id = meas_rep_recovery;
			LOGPRINT(h2d, info, "err_seq: Measurement report recovery requested");
			break;
		case REI_NO_RESP_RECOVERY :
			seq_id = no_resp_recovery;
			LOGPRINT(h2d, info, "err_seq: No response recovery requested");
			break;
		case REI_HARD_RESET_RECOVERY :
			seq_id = hard_reset_recovery;
			LOGPRINT(h2d, info, "err_seq: hard reset recovery requested");
			break;
		case REI_GPS_TIM:
			seq_id = inc_clk_unc;
			break;

		default:
			break;
	}

	return (SEQ_OK == seq_execute(seq_id, ie_adu->data)) ? H2D_OK : H2D_ERR;
}

int populate_dev_caps(struct dev_caps *dev_cap, unsigned int n_obj)
{
#define UE_LCS_CAP_GPS_AUTO UE_LCS_CAP_GNSS_AUTO   /* From gnss.h */
#define UE_LCS_CAP_AGPS_POS UE_LCS_CAP_AGNSS_POS   /* From gnss.h */
#define UE_LCS_CAP_AGPS_MSR UE_LCS_CAP_AGNSS_MSR   /* From gnss.h */

#define UE_GPS_BACK_COMP 0
#define UE_GPS_ONLY      1
#define UE_GLO_ONLY      2
#define UE_GPS_AND_GLO   3
#define UE_GPS_QZSS 5
#define UE_GPS_GLONASS_QZSS 7
#define UE_GPS_SBAS 9
#define UE_GPS_QZSS_SBAS 13
#define UE_GPS_GLONASS_QZSS_SBAS 15

	struct dev_caps *ref_ptr = dev_cap;
	unsigned char *gnss_id, *sigs_bits, *sbas_bits, *constellation;
	unsigned int  *ganss_cmn, *caps_bits, *asst_bits;

	sys_param_get(cfg_const_config,(void**) &constellation);

	if(*constellation == UE_GPS_BACK_COMP ||
	   *constellation == UE_GPS_ONLY ||
	   *constellation == UE_GPS_AND_GLO||
	   *constellation == UE_GPS_QZSS||
	   *constellation == UE_GPS_GLONASS_QZSS||
   	   *constellation == UE_GPS_SBAS||
   	   *constellation == UE_GPS_QZSS_SBAS||
   	   *constellation == UE_GPS_GLONASS_QZSS_SBAS) {

		/* Set default values */
		dev_cap->gnss_id = e_gnss_gps;

		LOGPRINT(h2d, info, "populate_dev_caps: caps_bits gps %d\n", dev_cap->caps_bits);

		if (cfg_success == sys_param_get(cfg_gps_gnss_id, (void**) &gnss_id))
			dev_cap->gnss_id = *gnss_id;

		if (cfg_success == sys_param_get(cfg_gps_ganss_cmn, (void**) &ganss_cmn))
			dev_cap->ganss_cmn = *ganss_cmn;

		if (cfg_success == sys_param_get(cfg_gps_caps_bits, (void**) &caps_bits))
			dev_cap->caps_bits = *caps_bits;

		if (cfg_success == sys_param_get(cfg_gps_asst_bits, (void**) &asst_bits))
			dev_cap->asst_bits = *asst_bits;

		if (cfg_success == sys_param_get(cfg_gps_sigs_bits, (void**) &sigs_bits))
			dev_cap->sigs_bits = *sigs_bits;

		if (cfg_success == sys_param_get(cfg_gps_sbas_bits, (void**) &sbas_bits))
			dev_cap->sbas_bits = *sbas_bits;
		LOGPRINT(h2d, info, "populate_dev_caps: caps_bits gps %d\n", dev_cap->caps_bits);

	}

	if (n_obj > 1) {


	if(*constellation == UE_GLO_ONLY ||
	   *constellation == UE_GPS_AND_GLO||
	   *constellation == UE_GPS_GLONASS_QZSS||
   	   *constellation == UE_GPS_GLONASS_QZSS_SBAS) {
			dev_cap++;
		/* Set default values */
		dev_cap->gnss_id = e_gnss_glo;

			LOGPRINT(h2d, info, "populate_dev_caps: caps_bits glo %d\n", dev_cap->caps_bits);

		if (cfg_success == sys_param_get(cfg_glo_gnss_id,   (void**) &gnss_id))
			dev_cap->gnss_id = *gnss_id;

		if (cfg_success == sys_param_get(cfg_glo_ganss_cmn, (void**) &ganss_cmn))
			dev_cap->ganss_cmn = *ganss_cmn;

		if (cfg_success == sys_param_get(cfg_glo_caps_bits, (void**) &caps_bits))
			dev_cap->caps_bits = *caps_bits;

		if (cfg_success == sys_param_get(cfg_glo_asst_bits, (void**) &asst_bits))
			dev_cap->asst_bits = *asst_bits;

		if (cfg_success == sys_param_get(cfg_glo_sigs_bits, (void**) &sigs_bits))
			dev_cap->sigs_bits = *sigs_bits;

		if (cfg_success == sys_param_get(cfg_glo_sbas_bits, (void**) &sbas_bits))
			dev_cap->sbas_bits = *sbas_bits;
			LOGPRINT(h2d, info, "populate_dev_caps: caps_bits glo %d\n", dev_cap->caps_bits);

		}
	}
	return (dev_cap - ref_ptr) + 1; // assumes GPS is always present.
}

int h2d_dev_caps_req(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);
	int ret_val = -1;
	unsigned int n_obj = ie_hdr->tot_len / sizeof(struct dev_caps);
	struct rpc_msg *rmsg;
	struct dev_caps *dev_cap;
	int st_size = ie_hdr->tot_len;
	struct ie_desc *adu;
  
	LOGPRINT(h2d, info, "h2d_dev_caps_req: fetch caps for %d GNSS\n", n_obj);

	dev_cap = (struct dev_caps *) malloc(st_size);
	if (!dev_cap)
		goto caps_req_exit1;

	memset(dev_cap, 0, st_size);

	n_obj = populate_dev_caps(dev_cap, n_obj);

	if(!(rmsg = rpc_msg_alloc()))
		goto caps_req_exit2;

	adu = rpc_ie_attach(rmsg, REI_DEV_CAPS, 
						CTW_2_OPER_ID(ie_hdr->ct_word), 
			    n_obj * sizeof(struct dev_caps), dev_cap);
	if(!adu)
		goto caps_req_exit3;

	if(rpc_svr_send(rmsg) < 0)
		goto caps_req_exit4;

	UNUSED(srv_re);

	return 0;

caps_req_exit4:
	rpc_ie_detach(rmsg, adu);

caps_req_exit3:
	rpc_msg_free(rmsg);

caps_req_exit2:
	free(dev_cap);

caps_req_exit1:
	return ret_val;
}

/*
 * Code Group 2: RPC Table.
 * Table that maps REI item to handler function.
 */
static struct srv_rei srv_re_rcv[] = {
	/* Device control. */
	SRV_REI(REI_DEV_CONNECT, NO_OBJECT, h2d_dev_connect, 0),
	SRV_REI(REI_DEV_RELEASE, NO_OBJECT, h2d_dev_release, 0),
	SRV_REI(REI_DEV_ACTIVE, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_DEV_DE_ACT, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_DEV_DSLEEP, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_DEV_WAKEUP, NO_OBJECT, h2d_use_sequencer, 0),

	SRV_REI(REI_DEV_PLT, NO_OBJECT, h2d_dev_plt, 0),
	/* Rudimentary direct injections. */
	SRV_REI(REI_DEV_CFG_RPT, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_DEV_NAV_SEL, NO_OBJECT, h2d_dev_nav_sel, 0),
	SRV_REI(REI_DEV_VERSION, NO_OBJECT, h2d_dev_ver_req, 0),
	SRV_REI(REI_DEV_WSTATUS, NO_OBJECT, h2d_dev_ping_req, 0),

	/* Assistance commands - Device format. */
	SRV_REI(REI_DEV_SV_HLT, NO_OBJECT, h2d_assist_command, 0),
	SRV_REI(REI_DEV_SV_DIR, NO_OBJECT, h2d_assist_command, 0),

	/* Assistance commands - 3GPP format. */
    SRV_REI(REI_REF_POS, loc_gnss_info,   h2d_assist_command, 0),
	SRV_REI(REI_GPS_EPH, gps_eph, 		h2d_assist_command, 0),
	SRV_REI(REI_GPS_ALM, gps_alm, 		h2d_assist_command, 0),
	SRV_REI(REI_GPS_UTC, gps_utc, 		h2d_assist_command, 0),
	SRV_REI(REI_GPS_ION, gps_ion, 		h2d_assist_command, 0),
	SRV_REI(REI_GPS_TIM, gps_ref_time, 	h2d_assist_command, 0),
	
	SRV_REI(REI_GPS_ACQ, gps_acq, 		h2d_assist_command, 0),
	SRV_REI(REI_GPS_RTI, gps_rti, 		h2d_assist_command, 0),
	SRV_REI(REI_GPS_DGP, dgps_sat,	 	h2d_assist_command, 0),
	SRV_REI(REI_GLO_EPH, glo_eph, 		h2d_assist_command, 0),
	SRV_REI(REI_GLO_ALM, glo_alm, 		h2d_assist_command, 0),
	SRV_REI(REI_GLO_UTC, glo_utc_model, 	h2d_assist_command, 0),
	SRV_REI(REI_GLO_ION, ganss_ion_model, 	h2d_assist_command, 0),
	SRV_REI(REI_GLO_TIM, ganss_ref_time, 	h2d_assist_command, 0),
	
	SRV_REI(REI_GLO_ACQ, ganss_ref_msr, 	h2d_assist_command, 0),
	SRV_REI(REI_GLO_RTI, ganss_rti,		h2d_assist_command, 0),
	SRV_REI(REI_GLO_DGP, dganss_sat, 	h2d_assist_command, 0),
	SRV_REI(REI_REF_POS_AID_STAT, loc_gnss_info,   h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_EPH_AID_STAT, gps_eph, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_ALM_AID_STAT, gps_alm, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_UTC_AID_STAT, gps_utc, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_ION_AID_STAT, gps_ion, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_TIM_AID_STAT, gps_ref_time, 	  h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_ACQ_AID_STAT, gps_acq, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_RTI_AID_STAT, gps_rti, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GPS_DGP_AID_STAT, dgps_sat,	 	  h2d_assist_stat_command, 0),
	SRV_REI(REI_GLO_EPH_AID_STAT, glo_eph, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GLO_ALM_AID_STAT, glo_alm, 		  h2d_assist_stat_command, 0),
	SRV_REI(REI_GLO_UTC_AID_STAT, glo_utc_model,   h2d_assist_stat_command, 0),
	SRV_REI(REI_GLO_ION_AID_STAT, ganss_ion_model, h2d_assist_stat_command, 0),
	SRV_REI(REI_GLO_TIM_AID_STAT, ganss_ref_time,  h2d_assist_stat_command, 0),
	SRV_REI(REI_GLO_RTI_AID_STAT, ganss_rti,       h2d_assist_stat_command, 0),
	
	/* QoP. */
	SRV_REI(REI_QOP_SPECIFIER, dev_qop_specifier, h2d_qop_command, 0),

	/* maximize pdop */	
	SRV_REI(REI_MAXIMIZE_PDOP, NO_OBJECT, h2d_dev_maximize_pdop, 0),

	/* APM Control */	
	SRV_REI(REI_APM_CTRL, dev_apm_ctrl, h2d_apm_command, 0),

	/* PA Blank */
	SRV_REI(REI_PA_BLANK, dev_pa_blank, h2d_pa_blank_command, 0),
	
	/* Device caps */
	SRV_REI(REI_DEV_CAPS, dev_caps, h2d_dev_caps_req, 0),

	/* : Analyse and optimize. Remove response messages. */
	SRV_REI(REI_TS_PULSE_INFO, NO_OBJECT, NULL, 0),
	SRV_REI(REI_DEV_REF_CLOCK, NO_OBJECT, NULL, 0),
	SRV_REI(REI_CLK_CALIBRATE, NO_OBJECT, h2d_use_sequencer, 0),
	/* add for GNSS recovery ++*/	
	SRV_REI(REI_ERR_RECOVERY, NO_OBJECT, h2d_use_sequencer, 0),
	/* error trigger test code, to be removed */
	SRV_REI(REI_FATAL_ERR, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_EXCEPTION_ERR, NO_OBJECT, h2d_use_sequencer, 0),
	/* add for GNSS recovery --*/	
	SRV_REI(REI_MEAS_REP_RECOVERY, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_NO_RESP_RECOVERY, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_HARD_RESET_RECOVERY, NO_OBJECT, h2d_use_sequencer, 0),
	SRV_REI(REI_AREA_GEOFENCE, NO_OBJECT, NULL, 0),
	SRV_REI(REI_BUF_LOC_FIXES, NO_OBJECT, NULL, 0),
	SRV_REI(REI_DEV_OLD_POS, NO_OBJECT, NULL, 0),
	SRV_REI(REI_DEV_BUF_LOC, NO_OBJECT, NULL, 0),
	SRV_REI(REI_DEV_EXL_POS, NO_OBJECT, NULL, 0),
	SRV_REI(REI_DEV_MSR_GPS, NO_OBJECT, NULL, 0),
	SRV_REI(REI_DEV_MSR_GLO, NO_OBJECT, NULL, 0),
	SRV_REI(REI_LOC_GPS, NO_OBJECT, NULL, 0),
	SRV_REI(REI_LOC_GAN, NO_OBJECT, NULL, 0),
	SRV_REI(REI_LOC_EVT, NO_OBJECT, NULL, 0),
	SRV_REI(REI_VEL_GNS, NO_OBJECT, NULL, 0),
	SRV_REI(REI_TIM_GPS, NO_OBJECT, NULL, 0),
	SRV_REI(REI_TIM_GLO, NO_OBJECT, NULL, 0),
	SRV_REI(REI_TIM_GAN, NO_OBJECT, NULL, 0),
	SRV_REI(REI_MSR_GPS, NO_OBJECT, NULL, 0),
	SRV_REI(REI_MSR_GLO_SAT, NO_OBJECT, NULL, 0),
	SRV_REI(REI_MSR_GLO_SG1, NO_OBJECT, NULL, 0),
	SRV_REI(REI_MSR_GLO_SG2, NO_OBJECT, NULL, 0),
	SRV_REI(REI_MSR_GANSS, NO_OBJECT, NULL, 0)
};

/*
 * Code Group 3: RPC Engine.
 * Core logic for RPC decoding and execution.
 */
int wq_ie_add_to(unsigned short elem_id, unsigned char oper_id, 
				 unsigned int xact_id);
static void h2d_process_adu(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);
	unsigned int ct_word = ie_adu->head.ct_word;
	enum rpc_xact xtype = CTW_2_XACT_TY(ct_word);
	int ret_val;

	LOGPRINT(h2d, info, "processing rpc adu elem_id [0x%x] and xact id [0x%x]", 
		 ie_hdr->elem_id, CTW_2_XACT_ID(ct_word));

	if(xtype == rpc_req) {
		/* : Add xaction ID into RPC queue */
		wq_ie_add_to(ie_hdr->elem_id, CTW_2_OPER_ID(ct_word), 
					 CTW_2_XACT_ID(ct_word));
		LOGPRINT(h2d, info, "rpc ack added to queue");
	}

	ret_val = srv_re->rx_func(srv_re, ie_adu);

	/* : Fill proper params in the functions below */
	if(xtype == rpc_cmd) {
		if (H2D_OK == ret_val) {
			rpc_send_ack(CTW_2_OPER_ID(ct_word), CTW_2_XACT_ID(ct_word));
			LOGPRINT(h2d, info, "rpc ack sent");
		}
		else {
			rpc_send_err(CTW_2_OPER_ID(ct_word), CTW_2_XACT_ID(ct_word));
			LOGPRINT(h2d, warn, "rpc err sent");
		}
	}

	return;
}

static struct srv_rei *h2d_rei_find(unsigned short elem_id)
{
	struct srv_rei *srv_re = srv_re_rcv + 0;

	while(srv_re->elem_id != REI_NOTDEF) {
		if(srv_re->elem_id == elem_id)
			return srv_re;

		srv_re++;
	}

	return NULL;
}

static void h2d_app_msg_rx(struct rpc_msg *rmsg)
{
	struct rm_head *rm_hdr = RM_HEADER(rmsg);
	int n_ie = 0, num_ie   = rm_hdr->num_ie;

	for(n_ie = 0; n_ie < num_ie; n_ie++) {
		struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
		struct ie_head *ie_hdr = IE_HEADER(ie_adu);

		struct srv_rei *srv_re = h2d_rei_find(ie_hdr->elem_id);
		if(!srv_re || !srv_re->rx_func) {
			LOGPRINT(h2d, warn, "no h2d handler");
			continue;                    /* No takers, free data */
		}

		h2d_process_adu(srv_re, ie_adu);     /* Prepare for device   */

		rpc_ie_freeup(rmsg, ie_adu);         /* Taken, so detach it  */
	}

	rpc_msg_free(rmsg);
}

int h2d_ai2_hdl = -1;

static int h2d_init(void)
{
	/* Init sequencer. */
	if (H2D_OK != seq_init())
		return H2D_ERR;

	/* Init ai2. */
	if ((h2d_ai2_hdl = ai2_open(ai2_op_write)) < 0)
		return H2D_ERR;

	/* Init RPC. */
	if (rpc_svr_init(SERVER_FIFO, CLIENT_FIFO) < 0)
		return H2D_ERR;

	return H2D_OK;
}

static int h2d_deinit(void)
{

	/*Update the dev2host sync flag*/
	d2h_sync_flg_update();
 
	/* Deinit sequencer. */
	if (H2D_OK != seq_deinit())
		return H2D_ERR;

	return H2D_OK;
}

void* h2d_thread(void *arg)
{
	int flag = 0;

	h2d_sync = (struct thread_sync *) arg;

	/* Init. */
	if (H2D_OK != h2d_init())
		goto h2d_init_fail;

	while(1) {
		os_mutex_take(h2d_sync->mutex);
		flag = h2d_sync->flag;
		os_mutex_give(h2d_sync->mutex);

		if (flag) {
			LOGPRINT(h2d, info, "h2d_thread exiting");
			break;
		}

		LOGPRINT(h2d, info, "waiting on rpc");
		struct rpc_msg *rmsg = rpc_svr_recv();
		if(!rmsg)
			continue;
		LOGPRINT(h2d, info, "rpc message received");
		h2d_app_msg_rx(rmsg);
	}

	/* Deinit. */
	if (H2D_OK != h2d_deinit())
		goto h2d_deinit_fail;

	pthread_exit(NULL);
	return NULL;

h2d_deinit_fail:
h2d_init_fail:

	UNUSED(arg);

	/* No error handling possible. No grace. */
	LOGPRINT(h2d, err, "resource error");
	return NULL;
}
