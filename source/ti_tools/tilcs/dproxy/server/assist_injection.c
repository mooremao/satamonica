/*
 * assist_injection.c
 *
 * This file has low level functions that inject the assistance data from
 * outside source.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "dev_proxy.h"
#include "ai2.h"
#include "config.h"
#include "logger.h"
#include "device.h"
#include "utils.h"
#include "inject.h"
#include "nvs_validate.h"
#include "nvs_utils.h"
#include "sequencer.h"

extern int h2d_ai2_hdl;

#define BIT_LEN 24
#define GPS_SVID_TO_PRN(svid) (svid + AI2_GPS_BASE + 1)
#define GLO_SVID_TO_PRN(svid) (svid + AI2_GLO_BASE + 1)

int dpxy_inj_def_EE_qop(void);

/* Inject GPS reference position. */
static int h2d_inj_pos(void *data, int num)
{
	int ret = ERR;
	struct dev_pos_info *dev = NULL;
	struct location_desc *in = (struct location_desc *) data;
	int longitude = 0;
	int size = sizeof(struct dev_pos_info) + sizeof(struct dev_ellp_unc) + 
		sizeof(struct dev_utc_info);
	
	LOGPRINT(pos_inj, info, "external ref pos recvd");

	/* Get some memory. */
	dev = malloc(size);
	if (NULL == dev) {
		LOGPRINT(pos_inj, err, "malloc failed");
		return ERR;
	}

	memset(dev, 0x00, size);

	dev->ell = BUF_OFFSET_ST(dev, struct dev_pos_info,
							 struct dev_ellp_unc);
	dev->utc = BUF_OFFSET_ST(dev->ell, struct dev_ellp_unc,
							 struct dev_utc_info);

	if (in->longitude_N & (1<<(BIT_LEN-1)))
		in->longitude_N |= (0xffffffff << BIT_LEN);
	
	longitude = (int) in->longitude_N;
	
	dev->lat_rad 		= (int)((double)in->latitude_N * 
				C_90_OVER_2_23 * C_DEG_TO_RAD * C_LSB_LAT_REP);
	dev->lon_rad 		= (int)(longitude * 
				C_360_OVER_2_24	* C_DEG_TO_RAD * C_LSB_LON_REP);
	dev->alt_wgs84		= in->altitude_N * C_LSB_HT_REP;
	dev->ell->orientation	= in->orientation;
	dev->ell->semi_major	= in->unc_semi_maj_K;
	dev->ell->semi_minor	= in->unc_semi_min_K;
	dev->contents |= DEV_POS_HAS_ELP;

	if (south == in->lat_sign)
		dev->lat_rad *= -1;
	if (depth == in->alt_dir)
		dev->alt_wgs84 *= -1;

	/* Inject both position and altitude. */
	ret = h2d_inj_dev_pos_alt_est(dev, 1);

	free(dev);

	UNUSED(num);
	return ret;
}

struct h2d_tlm_how {
	unsigned char 	resv_bits;
	unsigned char 	flag_bits;
	unsigned short 	tlm_msg;
	unsigned int 	sub_frm_week;
	unsigned int 	bit_week;
	unsigned int 	pred_bits[2];
};

static const unsigned char gps_parity_array[C_N_PARITY] = {
	0x29, 0x16, 0x2a, 0x34,  /* D29*, D30*,   d1,   d2 */
	0x3b, 0x1c, 0x2f, 0x37,  /*   d3,   d4,   d5,   d6 */
	0x1a, 0x0d, 0x07, 0x23,  /*   d7,   d8,   d9,  d10 */
	0x31, 0x38, 0x3d, 0x3e,  /*  d11,  d12,  d13,  d14 */
	0x1f, 0x0e, 0x26, 0x32,  /*  d15,  d16,  d17,  d18 */
	0x19, 0x2c, 0x16, 0x0b,  /*  d19,  d20,  d21,  d22 */
	0x25, 0x13         	 /*  d23,  d24  */
};

/* This table is used to solve for d23 and d24 that will make D29 D30 zero. */
static const unsigned char gps_solve[4] = {
	0x00, 0x80, 0xC0, 0x40
};

static unsigned int gps_parity_sum(unsigned int gps_word)
{
	unsigned int sum, feed, i;
	const unsigned char *parity;

	sum = 0;
	feed = gps_word;
	parity = &gps_parity_array[0];

	/*
	 * Shift out D30* D29* .... d23 d24 and accumulate the parity effect of
	 * each data bit on the resulting parity sum. The loop counts down to
	 * 0 to optimize for the ARM implementation.
	 */
	for(i = C_N_PARITY; i; i--) {
		if(feed & 0x80000000) {
			sum ^= *parity;
		}

		feed <<= 1;
		parity++;
	}

	return sum;
}

static int h2d_atow_predict(struct h2d_tlm_how *tlm_how)
{
	unsigned int tlm, how, parity;
	unsigned int sfow_in_range, next_sfow;

	/*
	 * Build TLM word. Top 2 bits are final bits of prior word,
	 * zero by definition.
	 */
	tlm = ((unsigned int)0x8B << 22) | ((tlm_how->tlm_msg & 0x3FFF) << 8) |
						((tlm_how->resv_bits & 3) << 6);
	parity = gps_parity_sum(tlm);
	tlm_how->pred_bits[0] = tlm | parity;

	/*
	 * Ensure that the SFOW argument is within range.
	 * Prepare the HOW value that is the number of the following subframe
	 */
	sfow_in_range = tlm_how->sub_frm_week;
	while (sfow_in_range >= WEEK_ZCOUNTS) {
		sfow_in_range -= WEEK_ZCOUNTS;
	}

	if (sfow_in_range == (WEEK_ZCOUNTS-1))
		next_sfow = 0;
	else
		next_sfow = sfow_in_range + 1;
	/*
	 * The AS and Alert flags from the HOW word in the GPS TOW Assist
	 * message  have to be changed them to decoded (or source) bits in order
	 * to generate the parity bits. This has to be done only if the D30
	 * parity bit of the TLM word is a 1. This bit currently resides in the
	 * LSB of 'parity'.
	 */
	if (parity & 1) {
		tlm_how->flag_bits ^= 3;
	}

	/*
	 * Build HOW word preceeded by the low-order bits of TLM parity. These
	 * two bits are pertinent for generating HOW parity. The Sub Frame ID
	 * field cycles 1...5 starting with 1 in sub frame zero of the week.
	 */
	how = (parity << 30) | (next_sfow << 13) |
		((tlm_how->flag_bits & 3) << 11) | (sfow_in_range % 5 + 1) << 8;

	/*
	 * Initial version of parity bits might not be zero in D29 and D30.
	 * Solve for d23 and d24 that will make D29 D30 zero. Then recompute
	 * parity.
	 */
	parity = gps_parity_sum(how);
	how |= gps_solve[parity & 0x3];
	parity = gps_parity_sum(how);

	/*
	 * Invert HOW data bits if required by last TLM parity bit. This is
	 * because the sensor expects over the air format, not decoded bit
	 * format!
	 */
	if(how & 0x40000000)
		how ^= 0x3FFFFFC0;

	/*
	 * Align HOW word to the left, that is, delete D29* and D30*.
	 * Include parity bits.
	 */
	tlm_how->pred_bits[1] = (how | parity) << 2;

	/* Generate the bit of week that is two bits before designated SFOW. */
	if (sfow_in_range == 0) {
		tlm_how->bit_week = (WEEK_ZCOUNTS * 300 - 2);
	} else {
		tlm_how->bit_week = sfow_in_range * 300 - 2;
	}

	return OK;
}

/* Inject Predicted SV Data Bits. */
static int h2d_inj_pred_sv_bits(const struct gps_ref_time *in)
{
	struct h2d_tlm_how tlm_how;
	const struct gps_atow *data = &in->atow[0];
	unsigned int num = in->n_atow;

	unsigned char mem[3 + 7 + (4 * 2)];
	unsigned char *buf = mem;

	unsigned int num_bits = 62; /* Sparse Pattern Match uses 62 bits. */
	unsigned int pkt_len = 0;
	
	LOGPRINT(acq_inj, info, "external ref time atow recvd");

	pkt_len = (7 + num_bits / 8);
	if (num_bits % 8) {
		pkt_len++;
	}

	while (num--) {
		/* 1. Predict the data bits. */
		tlm_how.resv_bits	= (unsigned char) (data->tlm_rsvd);
		tlm_how.flag_bits	= (unsigned char) (data->alert_flag << 1);
		tlm_how.flag_bits 	|= data->spoof_flag;
		tlm_how.tlm_msg		= data->tlm_word;
		
		if (milli_secs == in->time.tow_unit) {
			tlm_how.sub_frm_week	= in->time.tow;
		} else if (sec_8by100 == in->time.tow_unit) {
			tlm_how.sub_frm_week	= in->time.tow * 80; 
		} else if (clear_secs == in->time.tow_unit) {
			tlm_how.sub_frm_week	= in->time.tow * 1000;
		}
		
		tlm_how.sub_frm_week	= ((tlm_how.sub_frm_week)/6000L + 1L);
		if (tlm_how.sub_frm_week >= WEEK_ZCOUNTS) {
			tlm_how.sub_frm_week = 0;
		}

		h2d_atow_predict(&tlm_how);

		/* 2. Write it. */
		buf = BUF_LE_U1B_WR_INC(buf, 0x1C);
		buf = BUF_LE_U2B_WR_INC(buf, pkt_len);
		buf = BUF_LE_U1B_WR_INC(buf, GPS_SVID_TO_PRN(data->svid));
		buf = BUF_LE_U4B_WR_INC(buf, tlm_how.bit_week);
		buf = BUF_LE_U2B_WR_INC(buf, num_bits);

		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
						(tlm_how.pred_bits[0] >> 24)));
		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
						(tlm_how.pred_bits[0] >> 16)));
		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
						(tlm_how.pred_bits[0] >> 8)));
		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
							tlm_how.pred_bits[0]));

		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
						(tlm_how.pred_bits[1] >> 24)));
		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
						(tlm_how.pred_bits[1] >> 16)));
		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
						(tlm_how.pred_bits[1] >> 8)));
		buf = BUF_LE_U1B_WR_INC(buf, ((unsigned char)
							tlm_how.pred_bits[1]));

		if (ai2_ok != ai2_write(h2d_ai2_hdl, mem, pkt_len + 3)) {
			LOGPRINT(acq_inj, err, "error sending ai2 pkt to device");
			return ERR;
		}

		data++;
		buf = mem;
	}

	/* 3. Flush it. */
	if (ai2_ok != ai2_flush(h2d_ai2_hdl)) {
		LOGPRINT(acq_inj, err, "error sending ai2 pkt to device");
		return ERR;
	}

	LOGPRINT(acq_inj, info, "predicted sv data bits injected");
	return OK;
}

static unsigned short convert_m11e5(float flt);

/* Inject GPS reference time. */
int h2d_inj_gps_tim(void *data)
{
	int ret = ERR;
	struct dev_gps_time *dev = NULL;
	struct gps_ref_time *in = (struct gps_ref_time *) data;

	LOGPRINT(tim_inj, info, "external gps ref time atow recvd");

	/* Get some memory. */
	dev = malloc(sizeof(struct dev_gps_time));
	if (NULL == dev) {
		LOGPRINT(tim_inj, err, "malloc failed");
		return ERR;
	}
	memset(dev, 0x00, sizeof(struct dev_gps_time));

	dev->week_num		= in->time.week_nr + (in->time.n_wk_ro * 1024);
	if (milli_secs == in->time.tow_unit) {
		dev->time_msec	= in->time.tow;
	} else if (sec_8by100 == in->time.tow_unit) {
		dev->time_msec	= in->time.tow * 80; /* 8/100secs * 1000 */
	} else if (clear_secs == in->time.tow_unit) {
		dev->time_msec	= in->time.tow * 1000;
	}

	dev->time_bias		= 0;
	/*
	 * 10.3.7.96a UE positioning GPS reference time uncertainty formula.
	 */
	dev->time_unc		= convert_m11e5((0.0022 * (pow(1.18, in->time_unc) - 1)) * 1000 * 1000);
	dev->freq_bias		= 0;
	dev->freq_unc		= 0;
	dev->is_utc_diff	= 0;
	dev->utc_diff		= 0;
	dev->timer_count	= 0;

	if (in->n_atow != 0) {
		h2d_inj_pred_sv_bits(in);
	}

	ret = h2d_inj_dev_gps_time_est(dev, 1);
	free(dev);

	return ret;
}

static int h2d_inj_gps_tim_seq(void *data, int num)
{
	UNUSED(num);
	return (SEQ_OK == seq_execute(inj_time, data)) ? OK : ERR;
}

/* Inject GLONASS reference time. */
static int h2d_inj_glo_tim(void *data, int num)
{
	int ret = ERR;
	struct dev_glo_time *dev = NULL;
	struct ganss_ref_time *in = (struct ganss_ref_time *) data;

	/* Some sanity. */
	if (glo_time_id != in->tid) {
		LOGPRINT(tim_inj, warn, "no glonass time. injecting still");
	}

	LOGPRINT(tim_inj, info, "external glo ref time atow recvd");

	/* Get some memory. */
	dev = malloc(sizeof(struct dev_glo_time));
	if (NULL == dev) {
		LOGPRINT(tim_inj, err, "malloc failed");
		return ERR;
	}

	memset(dev, 0, sizeof(struct dev_glo_time));

	dev->time_day 		= 1;
	dev->time_month 	= 1;
	dev->time_year 		= 1996;
	dev->time_msec 		= 0;
	unsigned int secs = (in->day * 24 * 60 * 60) + (in->tod/1000);
	add2dev_glo_time(dev, secs);	
	dev->time_bias 		= 0;
	dev->time_unc 		= in->tod_unc;
	dev->utc_diff 		= 0;
	dev->freq_bias 		= 0;
	dev->freq_unc 		= 0;
	dev->ggto 		= 0;
	dev->time_status 	= 0;

	ret = h2d_inj_dev_glo_time_est(dev, 1);
	free(dev);

	UNUSED(num);
	return ret;
}

/* Inject GPS ephemeris. */
static int h2d_inj_gps_eph(void *data, int num)
{
	struct dev_gps_eph *dev = NULL, *ptr = NULL;
	struct gps_eph *in = (struct gps_eph *) data;
	unsigned int i = num, ret = ERR, isEE = 0;

	LOGPRINT(eph_inj, info, "%d external gps eph recvd", num);

	/* Some sanity. */
	if (num > 32 || num <= 0) {
		LOGPRINT(eph_inj, warn, "inv assist num. cmd ignored ");
		return OK;
	}

	/* Get some memory. */
	ptr = dev = malloc(sizeof(struct dev_gps_eph) * num);
	if (NULL == dev) {
		LOGPRINT(eph_inj, err, "malloc failed");
		return ERR;
	}

	while (i--) {
		dev->prn		= GPS_SVID_TO_PRN(in->svid);
		dev->Code_on_L2		= in->Code_on_L2;
		dev->Accuracy		= in->Ura;
		if (!in->is_bcast) {
			dev->Accuracy	|= 0x80;
		}
		dev->Health		= in->Health;
		dev->Tgd		= in->Tgd;
		dev->IODC		= in->IODC;
		dev->Toc		= in->Toc;
		dev->Af2		= in->Af2;
		dev->Af1		= in->Af1;
		dev->Af0		= in->Af0;
		dev->Crs		= in->Crs;
		dev->DeltaN		= in->DeltaN;
		dev->Mo			= in->Mo;
		dev->Cuc		= in->Cuc;
		dev->E			= in->E;
		dev->Cus		= in->Cus;
		dev->SqrtA		= in->SqrtA;
		dev->Toe		= in->Toe;
		dev->Cic		= in->Cic;
		dev->Omega0		= in->Omega0;
		dev->Cis		= in->Cis;
		dev->Io			= in->Io;
		dev->Crc		= in->Crc;
		dev->Omega		= in->Omega;
		dev->OmegaDot		= in->OmegaDot;
		dev->Idot		= in->Idot;
		dev->IODE		= dev->IODC;
		dev->is_bcast	= in->is_bcast;
		dev->Week		= GPS_WEEK_UNKNOWN;
		/* : Fit these in.
		= in->status;
		*/
		LOGPRINT(eph_inj, info, "h2d_inj_gps_eph: eph svid [%d], Toe [%d], Ura [%d], Tgd [%d], Toc [%d], Omega [%d] and Idot [%d]", in->svid, in->Toe, in->Ura, in->Tgd, in->Toc, in->Omega, in->Idot);

                if (dev->is_bcast == 0)
                        isEE = 1; // EE is being injected

		dev++;
		in++;
	}

	/* Push all ephemeris down. */
	ret = h2d_inj_dev_gps_eph(ptr, num);

	/* Free up. */
	free(ptr);

        if(isEE)
                dpxy_inj_def_EE_qop();

	return ret;
}

/* Inject GLONASS ephemeris. */
static int h2d_inj_glo_eph(void *data, int num)
{
	struct dev_glo_eph *dev = NULL, *ptr = NULL;
	struct glo_eph *in = (struct glo_eph *) data;
	unsigned int i = num, ret = ERR;

	LOGPRINT(eph_inj, info, "%d external glo eph recvd", num);

	/* Some sanity. */
	if (num > 24 || num <= 0) {
		LOGPRINT(eph_inj, warn, "inv eph assist num. cmd ignored ");
		return OK;
	}

	/* Get some memory. */
	ptr = dev = malloc(sizeof(struct dev_glo_eph) * num);
	if (NULL == dev) {
		LOGPRINT(eph_inj, err, "malloc failed");
		return ERR;
	}

	while (i--) {

		dev->prn	= GLO_SVID_TO_PRN(in->the_svid);
		dev->m		= in->m;
		dev->Tk		= in->tk;
		dev->Tb		= in->tb;
		dev->M		= in->M;
		dev->Epsilon	= in->Gamma;
		dev->Tau	= in->Tau;
		dev->Xc		= in->Xc;
		dev->Yc		= in->Yc;
		dev->Zc		= in->Zc;
		dev->Xv		= in->Xv;
		dev->Yv		= in->Yv;
		dev->Zv		= in->Zv;
		dev->Xa		= in->Xa;
		dev->Ya		= in->Ya;
		dev->Za		= in->Za;
		dev->B		= in->Bn;
		dev->P		= in->P;
		dev->Nt		= in->NT;
		dev->Ft		= in->FT;
		if (!in->is_bcast) {
			dev->Ft	|= 0x80;
		}
		dev->n		= in->n;
		dev->DeltaTau	= in->Delta_Tau;
		dev->En		= in->En;
		dev->P1		= in->P1;
		dev->P2		= in->P2;
		dev->P3		= in->P3;
		dev->P4		= in->P4;
		dev->ln		= in->ln;
		dev->is_bcast	= in->is_bcast;

		LOGPRINT(eph_inj, info, "h2d_inj_glo_eph: eph svid [%d], NT [%d], tk [%d], M [%d], FT [%d], Bn [%d], Tau [%d], Xc [%d], Yc [%d], Zc [%d], Xv [%d], Yv [%d], Zv [%d], Xa [%d], Ya [%d] and Za [%d]", in->the_svid, in->NT, in->tk, in->M, in->FT, in->Bn, in->Tau, in->Xc, in->Yc, in->Zc, in->Xv, in->Yv, in->Zv, in->Xa, in->Ya, in->Za);

		dev++;
		in++;
	}

	/* Push all ephemeris down. */
	ret = h2d_inj_dev_glo_eph(ptr, num);

	/* Free up. */
	free(ptr);

	return ret;
}

/* Inject GPS almanac. */
static int h2d_inj_gps_alm(void *data, int num)
{
	struct dev_gps_alm *dev = NULL, *ptr = NULL;
	struct gps_alm *in = (struct gps_alm *) data;
	unsigned int i = num, ret = ERR;

	LOGPRINT(alm_inj, info, "%d external gps alm  recvd", num);

	/* Some sanity. */
	if (num > 32 || num <= 0) {
		LOGPRINT(alm_inj, warn, "inv assist num. ignored ");
		return OK;
	}

	/* Get some memory. */
	ptr = dev = malloc(sizeof(struct dev_gps_alm) * num);
	if (NULL == dev) {
		LOGPRINT(alm_inj, err, "malloc failed");
		return ERR;
	}

	while (i--) {
		dev->prn		= GPS_SVID_TO_PRN(in->svid);
		dev->Health		= in->Health;
		dev->Toa		= in->Toa;
		dev->E			= in->E;
		dev->DeltaI		= in->DeltaI;
		dev->OmegaDot		= in->OmegaDot;
		dev->SqrtA		= in->SqrtA;
		dev->OmegaZero		= in->OmegaZero;
		dev->Omega		= in->Omega;
		dev->MZero		= in->Mzero;
		dev->Af0		= in->Af0;
		dev->Af1		= in->Af1;
		dev->Week		= in->wna;

		dev++;
		in++;
	}

	/* Push all almanac down. */
	ret = h2d_inj_dev_gps_alm(ptr, num);

	/* Free up. */
	free(ptr);

	return ret;
}

/* Inject GLONASS almanac. */
static int h2d_inj_glo_alm(void *data, int num)
{
	struct dev_glo_alm *dev = NULL, *ptr = NULL;
	struct glo_alm *in = (struct glo_alm *) data;
	unsigned int i = num, ret = ERR;

	LOGPRINT(alm_inj, info, "%d external glo alm recvd", num);

	/* Some sanity. */
	if (num > 24 || num <= 0) {
		LOGPRINT(alm_inj, warn, "invalid num of assist. ignored ");
		return OK;
	}

	/* Get some memory. */
	ptr = dev = malloc(sizeof(struct dev_glo_alm) * num);
	if (NULL == dev) {
		LOGPRINT(alm_inj, err, "malloc failed");
		return ERR;
	}

	while (i--) {
		dev->prn			= in->nA + AI2_GLO_BASE;
		dev->TauC			= in->TauC;
		dev->TauGPS			= in->TauGPS;
		dev->N4				= in->N4;
		dev->NA				= in->NA;
		dev->nA				= in->nA;
		dev->HnA			= in->HnA;
		dev->Lambda_nA			= in->Lambda_nA;
		dev->t_Lambda_nA		= in->t_Lambda_nA;
		dev->Delta_i_nAcorrection	= in->Delta_i_nA;
		dev->Delta_T_nArate		= in->Delta_T_nA;
		dev->Delta_T_nArate_Dot 	= in->Delta_T_dot_nA;
		dev->Varepsilon_nA		= in->Epsilon_nA;
		dev->Omega_nA			= in->Omega_nA;
		dev->M_nA			= in->M_nA;
		dev->B1				= in->B1;
		dev->B2				= in->B2;
		dev->Kp				= in->KP;
		dev->Tau_nA			= in->Tau_nA;
		dev->C_nA			= in->C_nA;

		dev++;
		in++;
	}

	/* Push all almanac down. */
	ret = h2d_inj_dev_glo_alm(ptr, num);

	/* Free up. */
	free(ptr);

	return ret;
}


/* Inject GPS UTC. */
static int h2d_inj_gps_utc(void *data, int num)
{
	int ret = ERR;
	struct dev_gps_utc *dev = NULL;
	struct gps_utc *in = (struct gps_utc *) data;

	LOGPRINT(utc_inj, info, "external gps utc recvd");

	/* Get some memory. */
	dev = malloc(sizeof(struct dev_gps_utc));
	if (NULL == dev) {
		LOGPRINT(utc_inj, err, "malloc failed");
		return ERR;
	}

	dev->A0 		= in->A0;
	dev->A1 		= in->A1;
	dev->DeltaTls 		= in->DeltaTlsf;
	dev->Tot 		= in->Tot;
	dev->WNt 		= in->WNt;
	dev->WNlsf 		= in->WNlsf;
	dev->DN 		= in->DN;
	dev->DeltaTlsf 		= in->DeltaTls;

	ret = h2d_inj_dev_gps_utc(dev, 1);
	free(dev);

	UNUSED(num);
	return ret;

}

/* Inject GLONASS UTC. */
static int h2d_inj_glo_utc(void *data, int num)
{
	int ret = ERR;
	struct dev_glo_utc *dev = NULL;
	struct glo_utc_model *in = (struct glo_utc_model *) data;

	LOGPRINT(utc_inj, info, "external glo utc recvd");

	/* Get some memory. */
	dev = malloc(sizeof(struct dev_glo_utc));
	if (NULL == dev) {
		LOGPRINT(utc_inj, err, "malloc failed");
		return ERR;
	}

	dev->B1 = (unsigned short)((double)in->B1 / TWO_TO_10);
	dev->B2 = (unsigned short)((double)in->B2 / TWO_TO_16);
	dev->KP = in->KP;

	ret = h2d_inj_dev_glo_utc(dev, 1);
	free(dev);

	UNUSED(num);
	return ret;
}

/* Inject GPS Iono. */
static int h2d_inj_gps_ion(void *data, int num)
{
	/*
	 * dev_gps_ion and gps_ion
	 * Struct match exactly. Simply pass.
	 */
	UNUSED(num);

	LOGPRINT(ion_inj, info, "external gps ion recvd");

	return h2d_inj_dev_gps_iono(data, 1);
}

/* Inject GLONASS Iono. */
static int h2d_inj_glo_ion(void *data, int num)
{
	UNUSED(data);
	UNUSED(num);
	return OK;
}

/* Inject SV steering */
struct h2d_raw_svsteer {
	unsigned int 	svid;
	unsigned int	fcount;
	unsigned int	ms;
	float		subms;
	float		svtime_uncert;
	float		svspeed;
	float		svspeed_uncert;
	float		svaccel;
	
	unsigned int	use_flag;
};

static int h2d_inj_svtimdif(const struct h2d_raw_svsteer *in, int num, 
					const struct h2d_raw_svsteer *ref)
{
	int i;
	unsigned char mem[3 + 9 + (11 * (num-1))];
	unsigned char *buf = mem;
	
	/* Scrap variable. */
	float hold_flt = 0.0;

	unsigned int time_status = SV_DTIME_STATUS_MS_VALID |
				   SV_DTIME_STATUS_SB_VALID |
				   SV_DTIME_STATUS_SM_VALID;	
	float diff_time = 0.0;
	float non_diff_time_uncert = 0.0;
	float diff_speed = 0.0;
	float non_diff_speed_uncert = 0.0;

	/* Validate. */
	if (num <= 1) {
		LOGPRINT(acq_inj, warn, "one sv, sv time diff skipped");
		return OK;
	}
	
	
	/* Encode ref Sv. */
	buf = BUF_LE_U1B_WR_INC(buf, 0x24);
	buf = BUF_LE_U2B_WR_INC(buf, 9 + (11 * (num - 1)));
	buf = BUF_LE_U1B_WR_INC(buf, ref->svid); /* No need to adjust. Done. */ 
	
	hold_flt = (ref->svtime_uncert * 65536.0f) / 1.0e6f;
	if (hold_flt > 65535.0f)
		hold_flt = 65535.0f;
	buf = BUF_LE_U2B_WR_INC(buf, (unsigned short)hold_flt);
	
	hold_flt = ref->svspeed_uncert * 10.0f;
	if (hold_flt > 65535.0f)
		hold_flt = 65535.0f;
	buf = BUF_LE_U2B_WR_INC(buf, (unsigned short)hold_flt);
	buf = BUF_LE_U4B_WR_INC(buf, ref->fcount);
	
	/* Encode diff for remaining Svs. */
	for(i = 0; i < num; i++) {
		
		/* Don't encode ref Sv. */
		if (in == ref) {
			in++;
			continue;
		}
		
		buf = BUF_LE_U1B_WR_INC(buf, in->svid);
		buf = BUF_LE_U1B_WR_INC(buf, time_status);
		
		diff_time = (float)((int)(in->ms - ref->ms) + 
						(in->subms - ref->subms));
		hold_flt = diff_time / 40.0f * 8388608.0f;
		if (hold_flt > 8388607.0f) 
			hold_flt = 8388607.0f;
		else if (hold_flt < -8388608.0f) 
			hold_flt = -8388608.0f;			
		buf = BUF_LE_3B_WR_INC(buf, (int)hold_flt);
		
		non_diff_time_uncert = in->svtime_uncert;
		hold_flt = (non_diff_time_uncert * 65536.0f) / 1.0e6f;
		if (hold_flt > 65535.0f) 
			hold_flt = 65535.0f;
		buf = BUF_LE_U2B_WR_INC(buf, (unsigned short)hold_flt);
		
		diff_speed = in->svspeed - ref->svspeed;
		/* 1/(2000/32768) = 16.384 */
		hold_flt = diff_speed * 16.384f; 
		if (hold_flt > 32767.0f) 
			hold_flt = 32767.0f;
		else if (hold_flt < -32768.0f)
			hold_flt = -32768.0f;
		buf = BUF_LE_S2B_WR_INC(buf, (short)hold_flt);
		
		non_diff_speed_uncert = in->svspeed_uncert;
		hold_flt = non_diff_speed_uncert * 10.0f;
		if (hold_flt > 65535.0f) 
			hold_flt = 65535.0f;	
		buf = BUF_LE_U2B_WR_INC(buf, (unsigned short)hold_flt);
		
		in++;
	}
	
	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(acq_inj, info, "svtimediff injected");
			return OK;
		}
	}

        LOGPRINT(acq_inj, err, "error sending ai2 packet to device");
	return ERR;
}

static int h2d_inj_svsteer(const struct h2d_raw_svsteer *in, int num)
{
	/* Inject SV steering */
	struct ai2_inj_svsteer {
		unsigned int id;
		unsigned int len;
		unsigned int svid;
		unsigned int fcount;
		unsigned int subms;
		unsigned int ms;
		unsigned int svtime_uncert;
		int          svspeed;
		unsigned int svspeed_uncert;
		int          svaccel;
		unsigned int steer_cmd_flgs;
	} out[1];

	char ai2_inj_svsteer_sizes[] = {1, 2, 1, 4, 3, 4, 2, 3, 2, 2, 1, 0};
	unsigned char mem[25];
	
	float temp_spd;
	struct dev_gps_time gps_tim;
	
	nvs_rec_get(nvs_gps_clk, &gps_tim, 0x01);
	
	LOGPRINT(acq_inj,info,"SV Steer: TCount[%u] Msec[%u] TUnc:[%u]",
			gps_tim.timer_count, gps_tim.time_msec, gps_tim.time_unc);
	while(num--) {
		/* If assistance is not new, do not inject it. */
		if (in->use_flag != GPS_ACQ_FLAG_NEW) {
			LOGPRINT(acq_inj, info, "old svsteer for svid %d skipped", 
								in->svid);
			in++;
			continue;
		}
		
		LOGPRINT(acq_inj, info, 
		"h2d_inj_svsteer inp::%d\t%d\t%f\t%d\t%f\t%f\t%f\t%f\t%x\n", 
		in->svid, in->fcount, in->subms, in->ms, in->svtime_uncert, 
		in->svspeed, in->svspeed_uncert, in->svaccel, in->use_flag);
						
		/* Convert to AI2 format. */
		out->id = 0x1F;
		out->len = 22;
		out->svid = in->svid;
		out->fcount = in->fcount;
		
		unsigned int temp_uint = (in->subms * ((unsigned int)1 << 24));
		out->subms = temp_uint;
		
		out->ms = in->ms;

		unsigned short mantissa = (unsigned short)(gps_tim.time_unc >> 5);	
		unsigned char exponent = (unsigned char)(gps_tim.time_unc & 0x001F);
		float temp_flt = (float)ldexp(mantissa,exponent);

		unsigned short temp_ushort = convert_m11e5(in->svtime_uncert + temp_flt);
		out->svtime_uncert = (unsigned int) temp_ushort;
		
		
		temp_spd = in->svspeed;
		if (in->svspeed > 83886.07f) {
			temp_spd = 83886.07f;			
		} else if (in->svspeed < -83886.07f) {
			temp_spd = -83886.07f;
		}
		
		temp_flt = (temp_spd * 100.0f);
		out->svspeed = (int) temp_flt;
		
		temp_flt = (in->svspeed_uncert * 10.0f);
		out->svspeed_uncert = (unsigned int) temp_flt;
						
		temp_flt = (in->svaccel * 6400.0f);				
		out->svaccel = (int) temp_flt;
		
		out->steer_cmd_flgs = 0; /* Execute SV selection algorithm. */		
		
		LOGPRINT(acq_inj, info, "h2d_inj_svsteer::%u\t%u\t%u\t%u\t%u\t%d\t%u\t%d\t%u\n", 
			out->svid, 
			out->fcount,
			out->subms, 
			out->ms,
			out->svtime_uncert,
			out->svspeed,
			out->svspeed_uncert,
			out->svaccel,
			out->steer_cmd_flgs);
		
		d2h_ai2_encode((void *)out, mem, ai2_inj_svsteer_sizes);

		if (ai2_ok != ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
			LOGPRINT(acq_inj, err, "svsteer ai2 write fail");
			return -1;
		}
		
		in++;
	}

	if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
		LOGPRINT(acq_inj, info, "svsteer injected");
	} else {
		LOGPRINT(acq_inj, err, "error sending ai2 packet to device");
		return ERR;
	}

	return OK;
}


#define CA_CHIPS_MSEC  1023  /* # of C/A chips per msec */
#define CA_FREQ        (1000.0 * CA_CHIPS_MSEC)
#define L1_FREQ        (1540.0 * CA_FREQ)
#define LIGHT_SEC      (299792458.0)         /* m/s */
#define L1_WAVELENGTH  (LIGHT_SEC / L1_FREQ)

/* GPS Acquisition Assistance message Code Phase Search Window parameters.
 * Conversion is made from units of chips to ms. (1023 chips = 1ms)
 */
#define MSEC_PER_CHIP   (1.0 / 1023.0)
static const float time_uncert_table[16] = {
				1.0f,
		 (float)MSEC_PER_CHIP,
	002.0f * (float)MSEC_PER_CHIP,
	003.0f * (float)MSEC_PER_CHIP,
	004.0f * (float)MSEC_PER_CHIP,
	006.0f * (float)MSEC_PER_CHIP,
	008.0f * (float)MSEC_PER_CHIP,
	012.0f * (float)MSEC_PER_CHIP,
	016.0f * (float)MSEC_PER_CHIP,
	024.0f * (float)MSEC_PER_CHIP,
	032.0f * (float)MSEC_PER_CHIP,
	048.0f * (float)MSEC_PER_CHIP,
	064.0f * (float)MSEC_PER_CHIP,
	096.0f * (float)MSEC_PER_CHIP,
	128.0f * (float)MSEC_PER_CHIP,
	192.0f * (float)MSEC_PER_CHIP
};

static int minx_minusy(unsigned int x, unsigned int ymodr, int r)
{
	int c;

	c = ymodr - (x % r);

	if (c > r/2) {
		c -= r;
	} else if (c < -r/2) {
		c += r;
	}

	return c;
}

static void gps_msec_week_limit(unsigned int *gps_msecs, unsigned short *gps_week)
{
	if ((gps_msecs != NULL) && (*gps_msecs < 0)) {
		(*gps_msecs) += WEEK_MSECS;

		if ((gps_week != NULL) && (*gps_week != GPS_WEEK_UNKNOWN))
			(*gps_week) --;
	} else if ((gps_msecs != NULL) && (*gps_msecs >= WEEK_MSECS)) {
		(*gps_msecs) -= WEEK_MSECS;

		if ((gps_week != NULL) && (*gps_week != GPS_WEEK_UNKNOWN))
			(*gps_week) ++;
	}
}

static void h2d_time_delta(unsigned int *ms, float *subms, double time_delta,
							unsigned char eval_ms)
{
	double time_ms, int_ms;

	/*
	* Add the time delta to the sub millisecond time. Using arithmetic
	* limits the precision err from a 1 sec time delta to nanometers. Using
	* (float) here would result in a 18 meter error from a 1 sec time delta.
	*/
	time_ms = ((double)(*subms)) + time_delta;

	/* Extract the integer portion. */
	int_ms = floor(time_ms);

	/* And the fractional portion. */
	*subms = (float)(time_ms - int_ms);

	/* Evaluate the integer portion if required, else set to 0. */
	if(eval_ms) {
		volatile int temp;
		temp = (int) int_ms;
		*ms += temp;
		gps_msec_week_limit(ms, NULL);
	}
	else
		*ms = 0;
}

static unsigned short convert_m11e5(float flt)
{
	unsigned short res;

	if( flt <= (float) 0.0 )
		res = 0;
	else if (flt >= (float) (2047.0 * 32768.0 * 65536.0))
		res = 0xffff;
	else {
		unsigned char exponent;
		unsigned int mantissa;

		if (flt < (float) (1 << 21)) {
			mantissa = (unsigned int) flt;
			exponent = 0;
		} else {
			mantissa = (unsigned int)(flt *
						(float)(1.0/(float)(1L << 10)));
			exponent = 10;
		}

		while (mantissa > 2047) {
			exponent++;
			mantissa >>= 1;
		}

		res = (unsigned short) ((mantissa << 5) | (exponent));
	}

	return res;
}

static void h2d_gps_acq_propagate(const struct gps_acq *in,
						struct h2d_raw_svsteer *out)
{
	unsigned char ymodr = 0;
	unsigned short word = 0;
	int msec_corr_on_70ms = 0;
	double time_delta = 0.0;
	double prop_ms = 0.0;
	struct dev_gps_time gps_tim; // : Fill me and FCOUNT as well Move out of this function

	float subms = 0.0;
	float svspeed = 0.0;
	float svspeed_uncert = 0.0;
	float svtime_uncert = 0.0;
	float svaccel = 0.0;
	unsigned int ms = 0;

	// : Use flag and avoid propagation for reinjected Svs

	nvs_rec_get(nvs_gps_clk, &gps_tim, 0x01);

	LOGPRINT(acq_inj,info,"ACQ Prop: TCount[%u] Msec[%u] TUnc:[%u]",
			gps_tim.timer_count, gps_tim.time_msec, gps_tim.time_unc);
	ymodr = (unsigned char)(in->GPSBitNum * 20 + in->IntCodePhase);
	msec_corr_on_70ms = minx_minusy((unsigned int)(in->gps_tow - 73),
						(unsigned int)ymodr, (int )80);

	ms = in->gps_tow - 73 + msec_corr_on_70ms;
	subms = (float)(1022 - in->CodePhase) / (float)1023.0;
	svspeed = (float)in->Doppler0 * (float)2.5 * (float)L1_WAVELENGTH *
								(float)(-1.0);

	LOGPRINT(acq_inj,info,"ACQ Prop: in->Doppler0: %d\tsvspeed: %f\t IntCodePhase: %u\t GPSBitNum: %u\t gps_tow: %u\n",in->Doppler0, svspeed, in->IntCodePhase, in->GPSBitNum, in->gps_tow);

	if (in->DopplerUnc != 255 ) {
		word = (unsigned short)200;
		word <<= 8;
		word >>= in->DopplerUnc;
		svspeed_uncert = (float)(word >> 8);
		if (word & 0x08) {
			svspeed_uncert += 0.5;
		}
		svspeed_uncert *= (float)L1_WAVELENGTH;
	}

	svtime_uncert = (float)(time_uncert_table[in->SrchWin] * 1.0e6f)
						+ (svspeed_uncert * (float)(1.0f/(float)LIGHT_SEC) * 1.0e9f);

	if (in->Doppler1 != 255)  {
		svaccel = ((float)in->Doppler1 * 1.5f/63.0f - 1.0f ) *
						(float)L1_WAVELENGTH;
	}

	int ms_diff = gps_tim.time_msec - in->gps_tow;
		
	out->svid		= GPS_SVID_TO_PRN(in->svid);
	out->fcount		= gps_tim.timer_count -ms_diff;
	out->ms			= ms;
	out->subms		= subms;
	out->svtime_uncert	= svtime_uncert;
	out->svspeed		= svspeed;
	out->svspeed_uncert	= svspeed_uncert;
	out->svaccel		= svaccel;
	out->use_flag		= in->use_flag;

	LOGPRINT(acq_inj, info, "ACQ Prop::%d\t%d\t%f\t%d\t%f\t%f\t%f\t%f\t%x\n", out->svid, 
						out->fcount, out->subms, out->ms, out->svtime_uncert, 
						out->svspeed, out->svspeed_uncert, 
													out->svaccel, out->use_flag);
}

/* Inject GPS acquisition assitance. */
static int h2d_inj_gps_acq(void *data, int num)
{
	struct gps_acq *in = (struct gps_acq *)data;
	struct h2d_raw_svsteer *steer_out = NULL, *steer_array = NULL;
	struct dev_sv_dir *dir_out = NULL, *dir_array = NULL;
	int ret = OK, i = num, new_svdir_num = 0;
	struct h2d_raw_svsteer *min_svsteer = 0; /* Svsteer holding min time uncert. */

	LOGPRINT(acq_inj, info, "%d gps acq recvd", num);

	/* Some sanity. */
	if (num > 32 || num <= 0) {
		LOGPRINT(acq_inj, err, "invalid no of sats");
		return ERR;
	}

	/* Get some memory. */
	steer_array = malloc(sizeof(struct h2d_raw_svsteer) * num);
	steer_out = steer_array;
	
	if (NULL == steer_out) {
		LOGPRINT(acq_inj, err, "malloc failed");
		ret = ERR;
		goto error;
	}
	dir_array = malloc(sizeof(struct dev_sv_dir) * num);
	dir_out = dir_array;
	if (NULL == dir_out) {
		LOGPRINT(acq_inj, err, "malloc failed");
		ret = ERR;
		goto error;
	}
	
	/* Propogate/transform. */
	min_svsteer = steer_out;
	while (i--) {
		/* Normalize time input to millisecs. */
		if (sec_8by100 == in->tow_unit) {
			in->gps_tow *= 80; /* 8/100secs * 1000 */
		} else if (clear_secs == in->tow_unit) {
			in->gps_tow *= 1000;
		}
	
		/* 
		 * Propagate SV steering to h2d_raw_svsteer for all SVs. This 
		 * is needed to build the sv diff table. But only new svsteer
		 * are injected, by using use_flag values. 
		 */
		h2d_gps_acq_propagate(in, steer_out);
		
		/* Check for min uncertainity. */
		if (steer_out->svtime_uncert < min_svsteer->svtime_uncert) {
			min_svsteer = steer_out;
			LOGPRINT(acq_inj, info, "selected new ref SV - %d", min_svsteer->svid);
		}
		
		if (in->use_flag == GPS_ACQ_FLAG_NEW) {
		
			dir_out->prn = GPS_SVID_TO_PRN(in->svid);
			/* 8 = (360.0f/32.0f) / (360/256): RRLP and AI2 units */
			dir_out->azimuth = (unsigned char)((float)in->Azimuth 
									* 8);
			/* 16 = (90.0f/8.0f) / (180/256): RRLP and AI2 units */
			dir_out->elevation = (char)((float)in->Elevation * 16);
			
			new_svdir_num++;
			++dir_out;
		}
		
		++steer_out;
		
		++in;
	}
	
	/* 1. Inject the propagated SV Steering. */
	if (OK != h2d_inj_svsteer(steer_array, num)) {
		ret = ERR;
		goto error;
	}

	/* 2. Calculate and inject SV TimeDiff. */
	if (OK != h2d_inj_svtimdif(steer_array, num, min_svsteer)) {
		ret = ERR;
		goto error;
	}	
	
	/* 3. Inject the SV Direction. */
	if (OK != h2d_inj_dev_svdir(dir_array, new_svdir_num)) {
		ret = ERR;
		goto error;
	}

error:
	if (NULL != steer_array)
		free(steer_array);
	if (NULL != dir_array)
		free(dir_array);

	return ret;
}

/* Inject GLONASS acquisition assitance. */
static int h2d_inj_glo_acq(void *data, int num)
{
	UNUSED(data);
	UNUSED(num);
	return OK;
}

/* Inject GPS RTI. */
static int h2d_inj_gps_rti(void *data, int num)
{
	struct dev_sv_hlth dev;
	struct gps_rti *in = (struct gps_rti *) data;

	LOGPRINT(rti_inj, info, "%d external gps rti recvd", num);

	if (num > 32 || num <= 0) {
		LOGPRINT(rti_inj, err, "invalid no of sats");
		return ERR;
	}

	memset(&dev, 0x00, sizeof(struct dev_sv_hlth));

	dev.num_sat = num;
	while (num--) {
		dev.health_status[in->bad_svid - 1] = 1;
		in++;
	}

	return h2d_inj_dev_svhlth(&dev, 1);
}

#define GLO_OFFSET 32
/* Inject GLONASS RTI. */
static int h2d_inj_glo_rti(void *data, int num)
{
	struct dev_sv_hlth dev;
	struct ganss_rti *in = (struct ganss_rti *) data;

	LOGPRINT(rti_inj, info, "%d external glo rti recvd", num);

	if (num > 24 || num <= 0) {
		LOGPRINT(rti_inj, err, "invalid no of sats");
		return ERR;
	}

	memset(&dev, 0x00, sizeof(struct dev_sv_hlth));

	dev.num_sat = num;
	while (num--) {
		dev.health_status[GLO_OFFSET + in->bad_svid - 1] = 1;
		in++;
	}

	return h2d_inj_dev_svhlth(&dev, 1);
}

/* Inject DGPS corrections. */
static int h2d_inj_gps_dgps_cor(void *data, int num)
{
	int i;
	unsigned char mem[8 + (9 * 32)];
	unsigned char *buf = mem;
	struct dgps_sat *inp = (struct dgps_sat *)data;

	LOGPRINT(dgps_inj, info, "%d external dgps corr. recvd", num);

	/* As of now only GPS sats are supported at AI2. */
	if (num > 32 || num <= 0) {
		LOGPRINT(dgps_inj, err, "invalid no of sats");
		return ERR;
	}

	if (sec_8by100 == inp->tow_unit) {
		inp->gps_tow *= 80; /* 8/100secs * 1000 */
	} else if (clear_secs == inp->tow_unit) {
		inp->gps_tow *= 1000;
	}

	/* Common info for all sats. */
	buf = BUF_LE_U1B_WR_INC(buf, 0x21);
	buf = BUF_LE_U4B_WR_INC(buf, inp->gps_tow);
	buf = BUF_LE_U2B_WR_INC(buf, 0); /* Hardcoded to 0. 128x. */
	buf = BUF_LE_U1B_WR_INC(buf, 0); /* Hardcoded to 0. 128x. */

	for(i = 0; i < num; i++) {
		buf = BUF_LE_U1B_WR_INC(buf, GPS_SVID_TO_PRN(inp->svid));
		buf = BUF_LE_U1B_WR_INC(buf, inp->iod);
		buf = BUF_LE_U1B_WR_INC(buf, inp->udre);
		buf = BUF_LE_U1B_WR_INC(buf, 0); // Hardcoded to 0. 128x. */
		buf = BUF_LE_3B_WR_INC(buf, inp->prc); //: DS is 2 bytes only
		buf = BUF_LE_S2B_WR_INC(buf, inp->rrc);

		inp++;
	}

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(dgps_inj, info, "dgps injected");
			return OK;
		}
	}

        LOGPRINT(dgps_inj, err, "error sending ai2 packet to device");
	return ERR;
}

/* Inject GLONASS corrections. */
static int h2d_inj_glo_dgps_cor(void *data, int num)
{
	UNUSED(data);
	UNUSED(num);
	return OK;
}

struct h2d_inj_aiding_tbl {
	unsigned int rei_id;
	int (*injector)(void *data, int num);
};

/*
 * External assistance injection table.
 */
static struct h2d_inj_aiding_tbl h2d_inj_aiding_tbl[] = {
	{REI_REF_POS, h2d_inj_pos},

	{REI_GPS_TIM, h2d_inj_gps_tim_seq},
	{REI_GLO_TIM, h2d_inj_glo_tim},

	{REI_GPS_EPH, h2d_inj_gps_eph},
	{REI_GLO_EPH, h2d_inj_glo_eph},

	{REI_GPS_ALM, h2d_inj_gps_alm},
	{REI_GLO_ALM, h2d_inj_glo_alm},

	{REI_GPS_UTC, h2d_inj_gps_utc},
	{REI_GLO_UTC, h2d_inj_glo_utc},

	{REI_GPS_ION, h2d_inj_gps_ion},
	{REI_GLO_ION, h2d_inj_glo_ion},

	{REI_GPS_ACQ, h2d_inj_gps_acq},
	{REI_GLO_ACQ, h2d_inj_glo_acq},

	{REI_GPS_RTI, h2d_inj_gps_rti},
	{REI_GLO_RTI, h2d_inj_glo_rti},

	{REI_GPS_DGP, h2d_inj_gps_dgps_cor},
	{REI_GLO_DGP, h2d_inj_glo_dgps_cor},
};

static struct h2d_inj_aiding_tbl *find_by_rei(unsigned int id)
{
        struct h2d_inj_aiding_tbl *rec = h2d_inj_aiding_tbl;
	unsigned int cnt = sizeof(h2d_inj_aiding_tbl) /
					sizeof(struct h2d_inj_aiding_tbl);

	/* Return the first match. */
	while (cnt--) {
                if (rec->rei_id == id)
                        return rec;

		rec++;
        }

        return NULL;
}

int h2d_dev_add_assist(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	struct h2d_inj_aiding_tbl *tbl = NULL;
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	/* Get an injector. */
	tbl = find_by_rei(srv_re->elem_id);

	/* Some sanity checks. */
	if (NULL == tbl) {
		LOGPRINT(generic, warn, "unsupported assist inject [%d]. ignored",
							srv_re->elem_id);
		return OK;
	}
	if (NULL == ie_adu->data) {
		LOGPRINT(generic, warn, "assist data 'null' in add oper. ignored");
		return OK;
	}

	/* Inject and retain error messages. */
	return tbl->injector(ie_adu->data, (ie_hdr->tot_len/srv_re->obj_len));
}
