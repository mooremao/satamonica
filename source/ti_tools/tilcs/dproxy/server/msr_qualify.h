/*
 * $Header: /FoxProjects/FoxSource/win32/LocationManager 1/10/04 7:53p Lleirer $
 ******************************************************************************
 *  Copyright (C) 1999 SnapTrack, Inc.

 *

 *                  SnapTrack, Inc.

 *                  4040 Moorpark Ave, Suite 250

 *                  San Jose, CA  95117

 *

 *     This program is confidential and a trade secret of SnapTrack, Inc. The

 * receipt or possession of this program does not convey any rights to

 * reproduce or disclose its contents or to manufacture, use or sell anything

 * that this program describes in whole or in part, without the express written

 * consent of SnapTrack, Inc.  The recipient and/or possessor of this program

 * shall not reproduce or adapt or disclose or use this program except as

 * expressly allowed by a written authorization from SnapTrack, Inc.

 *

 *

 ******************************************************************************/


/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


  L O C A T I O N   S E R V I C E S   M A N A G E R   M O D U L E


  Copyright (c) 2002 by QUALCOMM INCORPORATED. All Rights Reserved.



  Export of this technology or software is regulated by the U.S. Government.

  Diversion contrary to U.S. law prohibited.

 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*********************************************************************************

  TI GPS Confidential

 *********************************************************************************/
/*
 *******************************************************************************
 *         TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION
 *
 * Property of Texas Instruments, Unauthorized reproduction and/or distribution
 * is strictly prohibited.  This product  is  protected  under  copyright  law
 * and  trade  secret law as an  unpublished work.
 * (C) Copyright Texas Instruments.  All rights reserved.
 *
 * FileName			:	gpsc_meas.c
 *
 * Description     	:
 *  This file contains functions that handle GPS measurement sent from Sensor.
 *  It checks the quality of such a measurement, if good enough, it will call
 *  a function to report such measurement to SMLC if configuration calls for
 *  MS-Assisted; if not good enouch, it either simply wait for better ones, or
 *  will do time alignment aimed at improving measurement time uncertainty.
 *
 * Author         	: 	Gaurav Arora - gaurav@ti.com
 *
 *
 ******************************************************************************
 */
#ifndef _MSR_QUALIFY
#define _MSR_QUALIFY

#include "device.h"
#include "gnss.h"

/* light speed second - meters/seconds */
#define C_NO_THRESH_TIMEUNC     150
#define LIGHT_MSEC              (double)(299792458.0/1000.0)

struct dev_msr_info_PVT {

#define MSR_CH_STATE_IDLE		0
#define MSR_CH_STATE_COARSE		1
#define MSR_CH_STATE_FINE		2
#define MSR_CH_STATE_OL_TRK		3
#define MSR_CH_STATE_CL_TRK		4
#define MSR_CH_STATE_CL_TRK_ACQ	5
		unsigned int 	Tcount;
        unsigned char  gnss_id;       /**< GPS, GLONASS, refer 3GPP spec     */
        unsigned char  svid;          /**< Space Vehicle ID,                 */ 

        unsigned short snr;           /**< SNR, 0.1 dB resolution            */
        unsigned short cno;           /**< C/No 0.1 dB resolution            */
        short          latency_ms;    /**< Latency                           */
        unsigned char  pre_int;       /**< description?, 1 msec resolution   */
        unsigned short post_int;      /**< description?, 1 msec resolution   */
        unsigned int   msec;          /**< Time msec, 1 msec resolution      */
        unsigned int   sub_msec;      /**< Sub  msec, 1/2^24 msec resolution */
        unsigned short time_unc;      /**< Time unc, 1 resolution            */
        int            speed;         /**< Speed, 0.01 meters/sec resolution */
        unsigned int   speed_unc;     /**< Unc,   0.01 meters/sec resolution */
        unsigned short msr_flags;     /**< Measurement status bitmap         */
        unsigned char  ch_states;     /**< channel measurement state         */
        int            accum_cp;      /**< accum carrier phase. -/+ 262144 m */
        int            carrier_vel;   /**< carrier velocity. -/+ 131072m/sec */
        short          carrier_acc;   /**< carrier accel.    -/+ 128m/s^2    */
//        unsigned char  loss_lock_ind; /**< loss of lock indication. 0-255    */
        unsigned char  rf_reg_val; /**< Changed to RF reg value for spur detect patch 0.169 onwards for F-Company */
        unsigned char  good_obv_cnt;  /**< good observation.  0 to 255       */
        unsigned char  total_obv_cnt; /**< total observation. 0 to 255       */
        unsigned char  sv_slot;       /**< description? -7 to 13             */
        unsigned char  diff_sv;       /**< description? 1 to 32              */
        unsigned char  early_term;    /** description?                       */
        char           elevation;     /**< resolution ?                      */
        unsigned char  azimuth;       /**< resolution ?                      */
		/*Added from Patch ver 0.166 onwards*/
		short 		   pos_residuals; /**< Position residuals 		*/
		short 		   velocity_residuals; /**< velocity residualsresiduals 		*/
};

/* gnss measurement filtering info structure */
struct gnss_msr_filter_info 
{
	unsigned char 	n_sat; 					/* num satellites taken from msr report */
	unsigned char	msr_qual_status; 			/* flag to indicate valid msr report */
	unsigned char   is_good_dop; 			/* good dop flag */
	unsigned char   is_all_sv_searched;		/* all sv search flag for sv steering data*/
	unsigned char	cnt_good_obs;	 		/* count of good observation for steering sv's */
	unsigned char   n_sv_good_msr;			/* num sv with good msr */
	unsigned char   n_good_sv;
	unsigned char   n_sv_with_timeSet;
	unsigned short	accuracy; 				/* qop accuracy */
	unsigned int	avg_cno; 				/* average cno for latency & time unc */
	unsigned int	gps_good_sv_bitmap;		/* sv id bit map for gps*/
	unsigned int	glo_good_sv_bitmap;		/* sv id bit map for glo*/
	float			sv_time_unc_threshold;  /* sv time unc threshold */
	float			pdop; 					/* dilution of precision */
	double			pos_uncert;				/* position uncertainity */
	double			avg_psuedo_range_err; 	/* average pseudo range error */
};

/* measurement filtering prototype functions */
char msr_filter_report(struct dev_msr_info_PVT *msr_info, unsigned short num_sat, 
					   struct dev_msr_result *msr_rpt_results);

#endif /* _MSR_QUALIFY */
