/*nvs_validate.h
* This file describe the mechanism to validate nvs data
*
* It gives the interface to read and validate nvs data.
*
*
* Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
* ALL RIGHTS RESERVED
*/

#ifndef _NVS_VALIDATE_H_
#define _NVS_VALIDATE_H_

#include "nvs.h"

struct nvs_aid_data {
        
        enum  nvs_record  record;
        unsigned int      n_objs;
        char              buffer[2048*32];
};
/*
  Returns number of elements populated in given array.
  Each element refers to entire data for a given assist (like eph)
  and is updated appropriately.
*/
int read_nvs_aiding(struct nvs_aid_data *aid_array, int num_aid, int boot_order);

/* Returns number of objects populated in buffer for selected SV ID */
int nvs_aid_get(enum nvs_record record, void *buffer, int flags);


struct nvs_aid_status {
        
        unsigned char obj_id;
                 int  ttl_ms;
};

unsigned int nvs_get_num_aid_elem();


/* Returns number of objects populated in the given array */
int nvs_aid_ttl(enum nvs_record record, struct nvs_aid_status *array, int num);

void add2dev_gps_time(struct dev_gps_time *gps_tim, unsigned int secs);
void add2dev_glo_time(struct dev_glo_time *glo_tim, unsigned int secs);

void add2dev_gps_time_ms(struct dev_gps_time *gps_tim, unsigned long long msecs);
void add2dev_glo_time_ms(struct dev_glo_time *glo_tim, unsigned long long msecs);

unsigned int do_gps_secs(struct dev_gps_time *gps_tim);
unsigned int do_glo_secs(struct dev_glo_time *glo_tim);

unsigned long long do_gps_msecs(struct dev_gps_time *gps_tim);
unsigned long long do_glo_msecs(struct dev_glo_time *glo_tim);

int elapsed_gps_secs(struct dev_gps_time *gps_tim);
int elapsed_glo_secs(struct dev_glo_time *glo_time);

long long elapsed_gps_msecs(struct dev_gps_time *gps_tim);
long long elapsed_glo_msecs(struct dev_glo_time *glo_time);

void fixup_dev_gnss_time(enum nvs_record record, void *data);
void fixup_dev_gnss_pos(void *data);

#endif /* _NVS_VALIDATE_H_ */