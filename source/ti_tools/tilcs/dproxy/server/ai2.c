/*
 * ai2.c
 *
 * This module takes care to wrap payload in AI2 protocol format and
 * communicates it to the GNSS device, through the configured transport
 * mechanism.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
 */

/* 	
	5. Optimize internal buffer length.
	6. Implement read time out.
	7. Reduce function size
*/

#include <string.h>

#include <logger.h>
#include "ai2.h"
#include "ai2_comm.h"
#include "config.h"
#include "ai2_log.h"
#include "os_services.h"

/* Configuration data. */
#define AI2_NO_PARALLEL_OPS 2

#define AI2_READ_HDL_IDX 0
#define AI2_WRITE_HDL_IDX 1

/* AI2 Protocol Details */
#define AI2_MIN_PKT_SIZE 5

#define DLE 0x10
#define ETX 0x03


struct ai2_hdl {
	void *mutex;
	int busy;
	unsigned char *buf;
	unsigned int idx;
	unsigned short chksm;
};

/* Internal memory for this module. */
static struct ai2_hdl ai2_hdl[AI2_NO_PARALLEL_OPS];
static struct ai2_cfg ai2_cfg;

static int ai2_partial_flush(const int hdl);

/*
 * AI2_WRITE_BUF_SIZE is a number used as trigger threshold for tx. This is the
 * max size the caller can write at once. This number is limited by MTU size as
 * defined in AI2 ICD and any HW limitation. This limit is derived by further
 * considering sync bytes, escaping, CRC etc.
 */
static unsigned char ai2_write_buf[2 * AI2_WRITE_BUF_SIZE];
static unsigned char ai2_read_buf[AI2_READ_BUF_SIZE];
static unsigned char ai2_app_buf[AI2_READ_BUF_SIZE];


/* ACK Abstractions. */
static void *ack_sem;
static unsigned char timed_out;

static void ai2_got_ack(void)
{
	if (1 != ai2_cfg.tx_req_ack)
		return; 
	
	timed_out = 0;
	
	/* Signal semaphore. */
	os_sem_give(ack_sem);
}

static int ai2_expect_ack(void)
{
	if (1 != ai2_cfg.tx_req_ack)
		return -1; 
		
	/* Wait on semaphore. */
	os_sem_take(ack_sem);
	
	return timed_out;
}

int ai2_open(enum ai2_op op)
{
	int res = ai2_error;

	if (ai2_op_write == op) {
		res = AI2_WRITE_HDL_IDX;

		if (1 == ai2_hdl[res].busy) {
                        LOGPRINT(ai2, err,"can't open ai2 for write - busy");
			return ai2_busy;
		}

		ai2_hdl[res].mutex = os_mutex_init();

		/* Setup message header and corresponding check sum. */
		ai2_write_buf[0] = DLE;
		ai2_write_buf[1] = ai2_cfg.tx_req_ack;
		ai2_hdl[res].chksm = DLE + ai2_cfg.tx_req_ack;

		ai2_hdl[res].busy = 1;
		ai2_hdl[res].buf = ai2_write_buf;
		ai2_hdl[res].idx = 2;
	} else if (ai2_op_read == op) {
		res = AI2_READ_HDL_IDX;

		if (1 == ai2_hdl[res].busy) {
			LOGPRINT(ai2, err, "can't open ai2 for read - busy");
			return ai2_busy;
		}

		ai2_hdl[res].mutex = os_mutex_init();

		ai2_hdl[res].busy = 1;
		ai2_hdl[res].buf = ai2_read_buf;
		ai2_hdl[res].idx = 0;

		if (ai2_ok != ai2_comm_open()) {
			LOGPRINT(ai2, err, "can't open gnss device");
			return ai2_comm_error;
		}
	}

	ack_sem = (void *)os_sem_init();

	return res;
}

int ai2_ephemeris_flush(const int hdl, const unsigned char *buf, unsigned int buflen)
{
	
    int ret = ai2_ok;

        os_mutex_take(ai2_hdl[hdl].mutex);

		ret = _ai2_ephemeris_flush(hdl, buf, buflen);
		
        os_mutex_give(ai2_hdl[hdl].mutex);

        return ret;
	
}
int _ai2_ephemeris_flush(const int hdl, const unsigned char *buf, unsigned int buflen)
{
	int ret = ai2_ok;
	unsigned int write_len = buflen;

	if (hdl >= AI2_NO_PARALLEL_OPS || NULL == buf) {
                LOGPRINT(ai2, err, "ai2 write error");
		return ai2_error;
	}

	if (1 != ai2_hdl[hdl].busy || AI2_WRITE_HDL_IDX != hdl) {
	        LOGPRINT(ai2, err, "ai2 write error");
		return ai2_op_invalid;
	}

	do {
		/* Handle single write of larger than life size. */
		if (buflen > AI2_2SV_EPHEMERIS_BUF_SIZE) {
			write_len = AI2_2SV_EPHEMERIS_BUF_SIZE;
			buflen -= write_len;
		}
		else {
			write_len = buflen;
			buflen = 0;
		}

		/* Flush the buffer to device and make some room for new data */
		if ((ai2_hdl[hdl].idx + write_len) > AI2_2SV_EPHEMERIS_BUF_SIZE) {
			if (ai2_ok != (ret = ai2_partial_flush(hdl))) {
				LOGPRINT(ai2, err, "ai2 write auto flush err");
				return ret;
			} else {
				LOGPRINT(ai2, info, "ai2 write auto flush pkt");
			}
		}

		/* Single pass escaping and checksum */
		while (0 != write_len--) {
			if (DLE == *buf) {
				*(ai2_hdl[hdl].buf + ai2_hdl[hdl].idx) = DLE;
				ai2_hdl[hdl].idx++;
			}

			ai2_hdl[hdl].chksm += (*(ai2_hdl[hdl].buf +
						ai2_hdl[hdl].idx) = *buf++);
			ai2_hdl[hdl].idx++;
		}

	} while(buflen != 0);

	return ret;
}
int ai2_almanac_flush(const int hdl, const unsigned char *buf, unsigned int buflen)
{
	
    int ret = ai2_ok;

        os_mutex_take(ai2_hdl[hdl].mutex);

		ret = _ai2_almanac_flush(hdl, buf, buflen);
		
        os_mutex_give(ai2_hdl[hdl].mutex);

        return ret;
	
}


int _ai2_almanac_flush(const int hdl, const unsigned char *buf, unsigned int buflen)
{
	int ret = ai2_ok;
	unsigned int write_len = buflen;

	if (hdl >= AI2_NO_PARALLEL_OPS || NULL == buf) {
                LOGPRINT(ai2, err, "ai2 write error");
		return ai2_error;
	}

	if (1 != ai2_hdl[hdl].busy || AI2_WRITE_HDL_IDX != hdl) {
	        LOGPRINT(ai2, err, "ai2 write error");
		return ai2_op_invalid;
	}

	do {
		/* Handle single write of larger than life size. */
		if (buflen > AI2_4SV_ALMANAC_BUF_SIZE) {
			write_len = AI2_4SV_ALMANAC_BUF_SIZE;
			buflen -= write_len;
		}
		else {
			write_len = buflen;
			buflen = 0;
		}

		/* Flush the buffer to device and make some room for new data */
		if ((ai2_hdl[hdl].idx + write_len) > AI2_4SV_ALMANAC_BUF_SIZE) {
			if (ai2_ok != (ret = ai2_partial_flush(hdl))) {
				LOGPRINT(ai2, err, "ai2 write auto flush err");
				return ret;
			} else {
				LOGPRINT(ai2, info, "ai2 write auto flush pkt");
			}
		}

		/* Single pass escaping and checksum */
		while (0 != write_len--) {
			if (DLE == *buf) {
				*(ai2_hdl[hdl].buf + ai2_hdl[hdl].idx) = DLE;
				ai2_hdl[hdl].idx++;
			}

			ai2_hdl[hdl].chksm += (*(ai2_hdl[hdl].buf +
						ai2_hdl[hdl].idx) = *buf++);
			ai2_hdl[hdl].idx++;
		}

	} while(buflen != 0);

	return ret;
}





int ai2_write(const int hdl, const unsigned char *buf, unsigned int buflen)
{
        int ret = ai2_ok;

        os_mutex_take(ai2_hdl[hdl].mutex);

        ret = _ai2_write(hdl, buf, buflen);

        os_mutex_give(ai2_hdl[hdl].mutex);

        return ret;

}

int _ai2_write(const int hdl, const unsigned char *buf, unsigned int buflen)
{
	int ret = ai2_ok;
	unsigned int write_len = buflen;

	if (hdl >= AI2_NO_PARALLEL_OPS || NULL == buf) {
                LOGPRINT(ai2, err, "ai2 write error");
		return ai2_error;
	}

	if (1 != ai2_hdl[hdl].busy || AI2_WRITE_HDL_IDX != hdl) {
	        LOGPRINT(ai2, err, "ai2 write error");
		return ai2_op_invalid;
	}

	do {
		/* Handle single write of larger than life size. */
		if (buflen > AI2_WRITE_BUF_SIZE) {
			write_len = AI2_WRITE_BUF_SIZE;
			buflen -= write_len;
		}
		else {
			write_len = buflen;
			buflen = 0;
		}

		/* Flush the buffer to device and make some room for new data */
		if ((ai2_hdl[hdl].idx + write_len) > AI2_WRITE_BUF_SIZE) {
			if (ai2_ok != (ret = ai2_partial_flush(hdl))) {
				LOGPRINT(ai2, err, "ai2 write auto flush err");
				return ret;
			} else {
				LOGPRINT(ai2, info, "ai2 write auto flush pkt");
			}
		}

		/* Single pass escaping and checksum */
		while (0 != write_len--) {
			if (DLE == *buf) {
				*(ai2_hdl[hdl].buf + ai2_hdl[hdl].idx) = DLE;
				ai2_hdl[hdl].idx++;
			}

			ai2_hdl[hdl].chksm += (*(ai2_hdl[hdl].buf +
						ai2_hdl[hdl].idx) = *buf++);
			ai2_hdl[hdl].idx++;
		}

	} while(buflen != 0);

	return ret;
}

int ai2_tell(const int hdl)
{
	if (hdl >= AI2_NO_PARALLEL_OPS) {
	        LOGPRINT(ai2, err, "ai2 tell error");
		return ai2_error;
	}

	if (1 != ai2_hdl[hdl].busy) {
	        LOGPRINT(ai2, err, "ai2 tell error");
		return ai2_op_invalid;
	}

	return ai2_hdl[hdl].idx;
}

static int ai2_partial_flush(const int hdl)
{
	int ret = 0;

	/* Dump to sensor. */
	if (ai2_log_bin(ai2_h2d, ai2_hdl[hdl].buf, ai2_hdl[hdl].idx) <= 0) {
		LOGPRINT(ai2, info, "sensor dump error h2d");
	}

	/* Transmit and reset. */
	ret = ai2_comm_tx(ai2_hdl[hdl].buf, ai2_hdl[hdl].idx);

	/* Reset only buffer state, not checksum. */
	ai2_hdl[hdl].idx = 0;

	return ret;
}

inline void ai2_log(char * buf,int length){
#define D_LINE_SIZE 16

	int i, j;
	int len = length;
	char  * pOut, outBuf[len];
	pOut = outBuf;
	
	for (i=0; i<len && pOut<(outBuf + sizeof(outBuf)); i += D_LINE_SIZE){

		for (j=0;
				j<D_LINE_SIZE && len>i+j && (pOut<(outBuf + sizeof(outBuf)));
				j++){
						pOut += sprintf(pOut, " %02x", *(buf+i+j));
				}
	
		LOGPRINT(ai2, err, "DUMPBUF: %s", outBuf);
		pOut = outBuf;
	}
}

int ai2_flush(const int hdl)
{
        int ret = ai2_ok;

        os_mutex_take(ai2_hdl[hdl].mutex);

        ret = _ai2_flush(hdl);

        os_mutex_give(ai2_hdl[hdl].mutex);

        return ret;
}

int _ai2_flush(const int hdl)
{
	unsigned char *buf_ptr = NULL;
	int ret = 0;
	int timed_out = 0;

	if (hdl >= AI2_NO_PARALLEL_OPS) {
                LOGPRINT(ai2, err, "ai2 flush error");
		return ai2_error;
	}

	if (1 != ai2_hdl[hdl].busy || AI2_WRITE_HDL_IDX != hdl) {
	        LOGPRINT(ai2, err, "ai2 flush error");
		return ai2_op_invalid;
	}

	if (ai2_hdl[hdl].idx < AI2_MIN_PKT_SIZE) {
	        LOGPRINT(ai2, err, "ai2 write incomplete stream");
		return ai2_incomplete_stream;
	}

	/* Place checksum and escape them, if needed. */
	buf_ptr = ai2_hdl[hdl].buf + ai2_hdl[hdl].idx;
	if (DLE == (*buf_ptr++ = (unsigned char)(ai2_hdl[hdl].chksm & 0xFF))) {
		*buf_ptr++ = DLE;
		ai2_hdl[hdl].idx++;
	}
	ai2_hdl[hdl].idx++;

	if (DLE == (*buf_ptr++ = (unsigned char)(ai2_hdl[hdl].chksm >> 8))) {
		*buf_ptr++ = DLE;
		ai2_hdl[hdl].idx++;
	}
	ai2_hdl[hdl].idx++;

	/* Add tail. */
	*buf_ptr++ = DLE;
	ai2_hdl[hdl].idx++;
	*buf_ptr++ = ETX;
	ai2_hdl[hdl].idx++;

	/* Dump to sensor. */
	if (ai2_log_bin(ai2_h2d, ai2_hdl[hdl].buf, ai2_hdl[hdl].idx) <= 0) {
		LOGPRINT(ai2, info, "sensor dump error h2d");
	}

	// ai2_log(ai2_hdl[hdl].buf,ai2_hdl[hdl].idx);
	
	do {
		/* Transmit. */
		ret = ai2_comm_tx(ai2_hdl[hdl].buf, ai2_hdl[hdl].idx);
		
		/* Wait for ACK. */
		if (1 == (timed_out = ai2_expect_ack())) {
			/* Delay before next retry. */
			// ai2_cfg.tx_retry_delay
			
		}
		
	} while ((timed_out == 1) && ai2_cfg.tx_retry_count--);
	
	/* Reset for next op. */
	ai2_cfg.tx_retry_count = AI2_RETRY_COUNT;
	
	/* Reset handle for next write. */
	ai2_reset(hdl);

	return ret;
}

int ai2_reset(const int hdl)
{
	if (hdl >= AI2_NO_PARALLEL_OPS) {
	        LOGPRINT(ai2, err, "ai2 reset error");
		return ai2_error;
        }

	if (1 != ai2_hdl[hdl].busy) {
	        LOGPRINT(ai2, err, "ai2 reset error");
		return ai2_op_invalid;
	}

	if (AI2_WRITE_HDL_IDX == hdl) {
		ai2_write_buf[0] = DLE;
		ai2_write_buf[1] = ai2_cfg.tx_req_ack;
		ai2_hdl[hdl].chksm = DLE + ai2_cfg.tx_req_ack;
		ai2_hdl[hdl].idx = 2;
	}
	else if (AI2_READ_HDL_IDX == hdl)
		ai2_hdl[hdl].idx = 0;

	return ai2_ok;
}

enum ai2_parse {
	ai2_sync,
	ai2_data,
	ai2_dle,
};

/*
 * Parse the raw buffer from serial interface and accumulates AI2 message. Once
 * the complete message is available the function returns ai2_ok.
 * Note: This function is NOT thread safe.
 */
static int ai2_strip_pkt(const int hdl, unsigned char *dbuf, unsigned int *dlen)
{
	static enum ai2_parse ai2_parser_fsm = ai2_sync;
	int pckt = 0;

	unsigned char *sbuf = ai2_hdl[hdl].buf;
	unsigned int slen = ai2_hdl[hdl].idx;

	unsigned char *dbuf_start = dbuf;

	static unsigned short calc_chksm = 0;
	static unsigned short recv_chksm = 0;

	*dlen = 0;

	while (slen && !pckt ) {
		switch(ai2_parser_fsm) {
		case ai2_sync: {
			if (DLE == *sbuf) {
				/* 1st byte. Just set the chksm. */
				calc_chksm = DLE;
				*dlen = 0;
				dbuf = dbuf_start;

				/*
				 * 2nd byte. Just add to chksm. If it is
				 * DLE then handle the escape
				 */
				sbuf++;
				slen--;
				calc_chksm += *sbuf;
				if (*sbuf == DLE) {
					sbuf++;
					slen--;
				}

				ai2_parser_fsm = ai2_data;
			}
			break;
		}

		case ai2_data: {
			if (DLE == *sbuf)
				ai2_parser_fsm = ai2_dle;
			else {
				calc_chksm += (*dbuf++ = *sbuf);
				(*dlen)++;
			}
			break;
		}

		case ai2_dle: {
			if (DLE == *sbuf) {
				/* Escape byte */
				calc_chksm += (*dbuf++ = DLE);
				(*dlen)++;
				ai2_parser_fsm = ai2_data;
			} else if (ETX == *sbuf) {
				/* Message termination */

				/* Retrieve checksum */
				recv_chksm = (unsigned char)*(sbuf - 3);
				recv_chksm = (*(sbuf - 2) << 8) | recv_chksm;

				/*
				 * Adjust calculated checksum to remove checksum
				 * itself.
				 */
				calc_chksm -= *(dbuf - 2);
				calc_chksm -= *(dbuf - 1);

				/* Adjust length to exclude checksum from dbuf*/
				(*dlen) -= 2;
				pckt = 1;
				ai2_parser_fsm = ai2_sync;
				calc_chksm = 0;
				recv_chksm = 0;
			} else {
				/*
				 * Sync byte in middle. Reset and restart. The
				 * parcel till now accumulated will be thrown
				 * away.
				 */
				ai2_parser_fsm = ai2_sync;
				sbuf -= 2;
				slen +=2;
				LOGPRINT(ai2, warn, "sync byte in middle");
			}
			break;
		}
                default:
                        break;
		} /* switch(ai2_parser_fsm) */

		sbuf++;
		slen--;
	} /* while (slen--) */

	/* Adjust the remainings of source buffer for next iteration. */
	ai2_hdl[hdl].idx = slen;
	if (slen) {
		memmove(ai2_hdl[hdl].buf, sbuf, slen);
	}

	if (pckt) {
		if (recv_chksm != calc_chksm) {
		        LOGPRINT(ai2, err, "ai2 read checksum error");
			return ai2_chksm_err;
		}
		else
			return ai2_ok;
	}

	return ai2_incomplete_stream;
}

/*
 * This call returns once a full AT2 packet is unstripped and full parcel are
 * accumulated and validated. This call retains the buffer context and calling
 * again continues accumulation from the internal buffer first. If there are no
 * more data in internal buffer then a read operation is attempted on the device
 */
int ai2_read(const int hdl, unsigned char **buf)
{
	int ret = ai2_ok;
	int pkt_recvd = 0;
	unsigned int parse_len = 0;
	unsigned int len = 0;

	if (hdl >= AI2_NO_PARALLEL_OPS || NULL == buf) {
                LOGPRINT(ai2, err, "ai2 read error");
		return ai2_error;
	}

	if (1 != ai2_hdl[hdl].busy || AI2_READ_HDL_IDX != hdl) {
                LOGPRINT(ai2, err, "ai2 read error");
		return ai2_op_invalid;
	}

	/* Set the app buffer and start reading and decoding. */
	*buf = ai2_app_buf;

	while (!pkt_recvd) {
		/*
		 * First strip the existing buffer. If a full packet available
		 * in the existing buffer, just return. Else go further for
		 * reading from the device.
		 */
		if (ai2_hdl[hdl].idx != 0) {
			ret = ai2_strip_pkt(hdl, (*buf) + parse_len, &len);
			if (ai2_incomplete_stream == ret) {
				pkt_recvd = 0;
				parse_len += len;
			}
			else if (ai2_ok == ret) {
				if (len == 0) {
					/* ACK recieved. Signal. */
					LOGPRINT(ai2, info, "ack recieved");
					ai2_got_ack();

					/* Proceed with next pkt. */
					pkt_recvd = 0;
				} else {
					/* Full pkt recieved. */
					pkt_recvd = 1;
				}
				parse_len += len;
				continue;
			}
			else
				return ret;
		}

		/* Read from the device. */
		len = AI2_READ_BUF_SIZE;
		ret = ai2_comm_rx(ai2_hdl[hdl].buf, &len);
		if (ai2_ok != ret)
			return ret;

		/* Dump to sensor. */
		if (ai2_log_bin(ai2_d2h, ai2_hdl[hdl].buf, len) <= 0) {
			LOGPRINT(ai2, info, "sensor dump error d2h");
		}

		ai2_hdl[hdl].idx += len;

		/* Overflow check. */
		if (parse_len > AI2_READ_BUF_SIZE) {
		        LOGPRINT(ai2, err, "ai2 read buffer overflow");
			return ai2_read_buf_full;
		}
	}
	return parse_len;
}

int ai2_close(const int hdl)
{
	if (hdl >= AI2_NO_PARALLEL_OPS) {
                LOGPRINT(ai2, err, "ai2 close error");
		return ai2_error;
	}

	if (1 != ai2_hdl[hdl].busy) {
                LOGPRINT(ai2, err, "ai2 close error");
		return ai2_op_invalid;
	}

	ai2_hdl[hdl].idx = 0;
	ai2_hdl[hdl].busy = 0;

	ai2_comm_close();
	
	os_sem_exit(ack_sem);
	os_mutex_exit(ai2_hdl[hdl].mutex);
	
	return ai2_ok;
}

int ai2_config(struct ai2_cfg *config)
{
	memcpy(&ai2_cfg, config, sizeof(struct ai2_cfg));
	return ai2_ok;
}

unsigned int ai2_get_nbytes(unsigned char **buf, unsigned char num_bytes)
{
	unsigned int result = 0;
	unsigned char i;

	if (num_bytes > 4)
		num_bytes = 4;

	*buf += (num_bytes - 1);

	for(i = num_bytes; i; i--) {
		result <<= 8;
		result |= (unsigned int) *((*buf)--);
	}

	*buf += (num_bytes + 1);
	return result;
}

unsigned char *ai2_put_nbytes(unsigned int bytes, unsigned char *buf,
						unsigned char num_bytes)
{
	if (num_bytes > 4)
		num_bytes = 4;

	for(; num_bytes; num_bytes--) {
		*buf++ = (unsigned char)bytes;
		bytes >>= 8;
	}

	return buf;
}

int d2h_ai2_encode(unsigned int *data, unsigned char *buf, char *sizes)
{
	if (data == 0 || buf == 0 || sizes == 0)
		return ai2_error;

	while(*sizes != 0) {
		buf = ai2_put_nbytes(*data++, buf, *sizes++);
	}

	return ai2_ok;
}
