/*
 * meas_status_report.c
 *
 * Measurement status report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "log_report.h"

struct dev_meas_stat {

	unsigned char  svid;
	short          latency_ms;
	unsigned short cno;
	unsigned char  ch_states;
	unsigned char  good_obv_cnt;
	unsigned char  total_obv_cnt;
	unsigned short status;
};

struct dev_meas_stat_rpt {
	unsigned short num_sat;
	struct dev_meas_stat *stat;
};

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int meas_stat_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								unsigned short num_sat, 
								struct dev_meas_stat *stat);

static struct ie_desc *mk_ie_adu_4meas_stat_dev(struct rpc_msg *rmsg,
												unsigned char oper_id, 
												struct dev_meas_stat_rpt *rpt)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_meas_stat) * rpt->num_sat;
	struct dev_meas_stat *stat = rpt->stat;
	struct dev_meas_stat *new_stat = (struct dev_meas_stat *)malloc(st_sz);
	if(!new_stat)
		goto exit1;

	memcpy(new_stat, stat, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_MSR_STAT, oper_id, st_sz, new_stat);
	if(!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_stat);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int meas_stat_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								   struct dev_meas_stat_rpt *rpt)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4meas_stat_dev(rmsg, oper_id, rpt);
	if(!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while(--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int meas_stat_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								unsigned short num_sat, 
								struct dev_meas_stat *stat)
{
	struct dev_meas_stat_rpt rpt = {num_sat, stat};

	return meas_stat_create_dev_ie(rmsg, oper_id, &rpt);
}

static int meas_stat_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
							unsigned int rei_select)
{
	struct dev_meas_stat_rpt *rpt = (struct dev_meas_stat_rpt*) 
		ai2_d2h->ai2_struct;

	if(-1 == meas_stat_create_dev_ie(rmsg, OPER_INF, rpt))
		return -1;

	UNUSED(rei_select);
	return 0;
}

static int meas_stat_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_meas_stat_rpt *rpt = (struct dev_meas_stat_rpt*) 
		ai2_d2h->ai2_struct;
	struct dev_meas_stat *stat = rpt->stat;
	char *ai2_pkt = (char *) ai2_d2h->ai2_packet + ai2_offset;
	char *ai2_ref = ai2_pkt;
	int ref_count = BUF_LE_U4B_RD_INC(ai2_pkt);
	unsigned short dev_msr_sz = 10;
	int num_sat = (ai2_d2h->ai2_pkt_sz - ai2_offset) / dev_msr_sz;

	rpt->num_sat    = num_sat;

	while(num_sat--) {

		stat->svid          = BUF_LE_U1B_RD_INC(ai2_pkt);
		stat->latency_ms    = BUF_LE_S2B_RD_INC(ai2_pkt);
		stat->cno           = BUF_LE_U2B_RD_INC(ai2_pkt);
		stat->ch_states     = BUF_LE_U1B_RD_INC(ai2_pkt);
		stat->good_obv_cnt  = BUF_LE_U1B_RD_INC(ai2_pkt);
		stat->total_obv_cnt = BUF_LE_U1B_RD_INC(ai2_pkt);
		stat->status        = BUF_LE_U2B_RD_INC(ai2_pkt);

		/* Dump the decoded data to logger. */
		print_data(REI_DEV_MSR_STAT, stat);

		stat++;
	}

	UNUSED(ref_count);
	return (ai2_pkt - ai2_ref);
}

static int meas_stat_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	ai2_offset += meas_stat_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int meas_stat_ext_setmem(struct d2h_ai2 *ai2_d2h)
{
	struct dev_meas_stat_rpt *rpt = (struct dev_meas_stat_rpt *) 
		ai2_d2h->ai2_struct;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	rpt->stat = BUF_OFFSET_ST(rpt, struct dev_meas_stat_rpt, 
							  struct dev_meas_stat);

	return 0;
}

#define EXT_PVT_ST_LEN (ST_SIZE(dev_meas_stat_rpt) + (200 * ST_SIZE(dev_meas_stat)))

struct d2h_ai2 d2h_meas_stat_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.st_set_mem = meas_stat_ext_setmem,
	.gen_struct = meas_stat_ext_struct,
	.do_ie_4rpc = meas_stat_rpc_ie
};

