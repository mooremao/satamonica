/*
 * inject.h
 *
 *   This file contains all the low level function that can be used to inject
 *   data to the GNSS device.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#ifndef _DPROXY_INJECT_H_
#define _DPROXY_INJECT_H_

#include "host2dev.h"

/*
 * Request assistance from device. Used for client requests as well as internal
 * nvs updates.
 */
enum h2d_assist {
	h2d_pos,
	h2d_time,
	h2d_alm,
	h2d_eph,
	h2d_iono,
	h2d_hlth,
	h2d_utc,
	h2d_dir,
	h2d_msr,
	h2d_oscoff
};

int dev_req_assist(enum h2d_assist assist, unsigned int prn);

/* Assistance related functions. */
int h2d_assist_command(struct srv_rei *srv_re, struct ie_desc *ie_adu);
int h2d_assist_stat_command(struct srv_rei *srv_re, struct ie_desc *ie_adu);
int h2d_dev_add_assist(struct srv_rei *srv_re, struct ie_desc *ie_adu);

int h2d_inj_dev_gps_time_est(void *data, int num);
int h2d_inj_inc_gps_time_unc(void *data);
int h2d_inj_dev_glo_time_est(void *data, int num);
int h2d_inj_dev_pos_alt_est(void *data, int num);
int h2d_inj_dev_gps_eph(void *data, int num);
int h2d_inj_dev_glo_eph(void *data, int num);
int h2d_inj_dev_gps_alm(void *data, int num);
int h2d_inj_dev_glo_alm(void *data, int num);
int h2d_inj_dev_iono(void *data, int num);
int h2d_inj_dev_utc(void *data, int num);
int h2d_inj_dev_svhlth(void *data, int num);
int h2d_inj_dev_freq_est(void *data, int num);
int h2d_inj_dev_svdir(void *data, int num);
int h2d_inj_dev_gps_utc(void *data, int num);
int h2d_inj_dev_glo_utc(void *data, int num);
int h2d_inj_dev_gps_iono(void *data, int num);

/* Sequencer related injection functions. */
int dpxy_inj_comm_prot(void *data);
int dpxy_get_gps_stat(void *data);
int dpxy_inj_ptch_dwld(void *data);
int dpxy_get_dwld_stat(void *data);
int dpxy_get_cust_cfg(void *data);
int dpxy_inj_cust_cfg(void *data);
int dpxy_inj_enable_notch_filter(void *data);
int dpxy_inj_recv_cfg(void *data);
int dpxy_inj_rtc_inj_ctrl(void *data);
int dpxy_inj_init_evt_cfg(void *data);
int dpxy_get_evt_cfg(void *data);
int dpxy_inj_dev_off(void *data);
int dpxy_inj_dev_on(void *data);
int dpxy_inj_dev_reset(void *data);
int dpxy_inj_dev_wakeup(void *data);
int dpxy_inj_dev_idle(void *data);
int dpxy_inj_nmea_out_ctrl(void *data);
int dpxy_inj_evt_cfg(void *data);
int dpxy_get_gps_ver(void *data);
int dpxy_inj_req_recv_cfg(void *data);
int dpxy_inj_nvs_aiding(void *data);
int dpxy_inj_freq_est(void *data);
int dpxy_inj_def_qop(void *dat);
/*New command added to shutdown GPS*/
int dpxy_inj_gps_shutdwn_ctrl(void *data);

/*New command added to get PPS_OUT*/
int dpxy_inj_pinmux_cfg(void *data);

/*New command added to get PPS_OUT*/
int dpxy_inj_pps_out_ctrl(void *data);

int dpxy_ping(void* data);
int dpxy_get_pos_ext(void* data);
int dpxy_evt_confg(void *data);
int dpxy_get_recvr_cfg(void* data);
int dpxy_get_meas(void* data);
int dpxy_get_meas_status(void* data);
/*Add for GNSS recovery ++*/
int dpxy_inj_fatal_pkt1(void *data);
int dpxy_inj_fatal_pkt2(void *data);
int dpxy_inj_exception_pkt(void *data);
/*Add for GNSS recovery --*/
int h2d_dproxy_sleep(void *data);

int h2d_inj_gps_tim(void *data);

int h2d_dproxy_gnss_core_off(void *data);
int h2d_dproxy_gnss_core_on(void *data);
int h2d_dproxy_hard_reset(void *data);
/* Misc injection functions. */
int h2d_dev_ver_req(struct srv_rei *srv_re, struct ie_desc *ie_adu);
int h2d_dev_nav_sel(struct srv_rei *srv_re, struct ie_desc *ie_adu);
int h2d_dev_ping_req(struct srv_rei *srv_re, struct ie_desc *ie_adu);
int h2d_qop_command(struct srv_rei *srv_re, struct ie_desc *ie_adu);
int h2d_dev_maximize_pdop(struct srv_rei *srv_re, struct ie_desc *ie_adu);
int h2d_dev_plt(struct srv_rei *srv_re, struct ie_desc *ie_adu);


/* Common data structures for injection. */
struct ai2_rec_action {
	unsigned int id;
	unsigned int len;
	unsigned int action_type;
	unsigned int action_mod; /* Optional. */
};
#endif
