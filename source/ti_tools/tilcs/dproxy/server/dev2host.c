/*
 * dev2host.c
 *
 * Framework to handle the data from device towards host.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "dev2host.h"
#include "os_services.h"
#include "rpc.h"
#include "ai2.h"
#include "logger.h"
#include "sequencer.h"
#include "utils.h"
#include "config.h"
#include "glo_sv_slot_map_report.h"
#include "error_handler.h"/* add for GNSS recovery ++*/

#define D2H_OK 0
#define D2H_ERR -1

extern unsigned int g_refTow;
static struct thread_sync {
	void *mutex;
	int  flag;
} *d2h_sync;

static int ai2_hdl = -1;

static void *mutex_hnd;

static struct svr_ai2 {

	unsigned short         key_id;    /* Key ID   */
	unsigned short         k_mask;    /* Key Mask */

	struct d2h_ai2        *ai2_d2h;
};

#define AI2_KEY_GEN(msg_id, sub_id) ((msg_id << 8) | (sub_id))
#define AI2_MSG_KEY(ai2)            ((ai2[0] << 8) | (ai2[3]))

#define AI2_MSG_ID_MASK       (0xFF00)  /* Indicates valid ai2 msg ID */
#define AI2_SUB_ID_MASK       (0x00FF)  /* Indicates valid ai2 sub ID */
#define AI2_K_MASK(sub_en)    (AI2_MSG_ID_MASK | (AI2_SUB_ID_MASK & sub_en))

#define AI2_OBJ(msg_id, sub_id, sub_en, d2h_name)                       \
{AI2_KEY_GEN(msg_id, sub_id), AI2_K_MASK(sub_en), d2h_name}

/*------------------------------------------------------------------------------
 * Section to add information about D2H instances
 *----------------------------------------------------------------------------*/
extern struct d2h_ai2 d2h_dev_status;
extern struct d2h_ai2 d2h_dev_version;
extern struct d2h_ai2 d2h_dev_cust_cfg;
extern struct d2h_ai2 d2h_evt_config;
extern struct d2h_ai2 d2h_ping_resp;
extern struct d2h_ai2 d2h_async_rec_evt;
//extern struct d2h_ai2 d2h_inv_pkt; /* removed for GNSS recovery ++*/
extern struct d2h_ai2 d2h_pos_rpt;
extern struct d2h_ai2 d2h_nmea;
extern struct d2h_ai2_msr d2h_msr_rpt;
extern struct d2h_ai2 d2h_ptch_dwld_stat;
extern struct d2h_ai2 d2h_clk_rpt;
extern struct d2h_ai2 d2h_fatal_err;
extern struct d2h_ai2 d2h_utc_rpt;
extern struct d2h_ai2 d2h_alm_rpt;
extern struct d2h_ai2 d2h_eph_rpt;
extern struct d2h_ai2 d2h_ion_rpt;
extern struct d2h_ai2 d2h_health_stat_rpt;
extern struct d2h_ai2 d2h_sv_dir_rpt;
extern struct d2h_ai2 d2h_alm_stat_rpt;
extern struct d2h_ai2 d2h_eph_stat_rpt;
extern struct d2h_ai2 d2h_meas_stat_rpt;
extern struct d2h_ai2 d2h_pos_stat_rpt;
extern struct d2h_ai2 d2h_calib_resp;
extern struct d2h_ai2 d2h_sv_slot_rpt;
extern struct d2h_ai2 d2h_dev_cw_test_report;
extern struct d2h_ai2  d2h_rep_diag_str;
/* add for GNSS recovery ++*/
extern struct d2h_ai2 d2h_exception_err;
//extern struct d2h_ai2 d2h_fatal_err;
extern struct d2h_ai2 d2h_inv_pkt_err;
/* add for GNSS recovery --*/

/*------------------------------------------------------------------------------
 * Look up table
 *----------------------------------------------------------------------------*/
struct svr_ai2 ai2_objs[] = {
	/*Info: m_id, s_id, s_en,  st_name  */
	AI2_OBJ(0x00, 0x00, 0x00, &d2h_ping_resp),
	AI2_OBJ(0x03, 0x00, 0x00, &d2h_evt_config),
	AI2_OBJ(0xF0, 0x00, 0x00, &d2h_dev_version),
	AI2_OBJ(0xF1, 0x00, 0x00, &d2h_dev_status),
	AI2_OBJ(0xF2, 0x00, 0x00, &d2h_ptch_dwld_stat),
	AI2_OBJ(0xF3, 0x00, 0x00, &d2h_ptch_dwld_stat),
	AI2_OBJ(0xF4, 0x00, 0x00, &d2h_ptch_dwld_stat),
	AI2_OBJ(0xDC, 0x00, 0x00, &d2h_dev_cust_cfg),

/* treat async rep as custom packet*/
	AI2_OBJ(0x80, 0x00, 0xFF, &d2h_async_rec_evt), /* Engine on */
	AI2_OBJ(0x80, 0x01, 0xFF, &d2h_async_rec_evt), /* Engine off */
	AI2_OBJ(0x80, 0x02, 0xFF, &d2h_async_rec_evt), /* New ALM */
	AI2_OBJ(0x80, 0x03, 0xFF, &d2h_async_rec_evt), /* New EPH */
	AI2_OBJ(0x80, 0x04, 0xFF, &d2h_async_rec_evt), /* New IONO */
	AI2_OBJ(0x80, 0x05, 0xFF, &d2h_async_rec_evt), /* New Health*/
	AI2_OBJ(0x80, 0x06, 0xFF, &d2h_async_rec_evt), /* Ext HW Evt */
	AI2_OBJ(0x80, 0x07, 0xFF, &d2h_async_rec_evt), /* Engine Idle */
	AI2_OBJ(0x80, 0x08, 0xFF, &d2h_async_rec_evt), /* No New Pos */
	AI2_OBJ(0x80, 0x0A, 0xFF, &d2h_async_rec_evt), /* SV Assign mode */
	AI2_OBJ(0x80, 0x0B, 0xFF, &d2h_async_rec_evt), /* Power Save Entry */
	AI2_OBJ(0x80, 0x0C, 0xFF, &d2h_async_rec_evt), /* Power Save Exit */
	AI2_OBJ(0x80, 0x0D, 0xFF, &d2h_async_rec_evt), /* GPS Awake */
	AI2_OBJ(0x80, 0x10, 0xFF, &d2h_async_rec_evt), /* SV deletion*/
	/* add for GNSS recovery ++*/
	AI2_OBJ(0xD4, 0x00, 0x00, &d2h_exception_err),	/* Auto sleep Error (CPU Exception) */
	AI2_OBJ(0xF5, 0x00, 0x00, &d2h_inv_pkt_err),
	AI2_OBJ(0xF6, 0x00, 0x00, &d2h_fatal_err),
	/* add for GNSS recovery --*/	
	AI2_OBJ(0xD3, 0x00, 0x00, &d2h_nmea),
	//AI2_OBJ(0x06, 0x00, 0x00, &d2h_pos_rpt),         /* Report Position           */
	AI2_OBJ(0xD5, 0x00, 0x00, &d2h_pos_rpt),         /* Report Position Ext       */
	AI2_OBJ(0x08, 0x00, 0x00, (struct d2h_ai2 *) &d2h_msr_rpt),         /* Report Measurement        */
	AI2_OBJ(0xF7, 0x01, 0xFF, (struct d2h_ai2 *) &d2h_msr_rpt),         /* Report Measurement Ext    */
	AI2_OBJ(0x05, 0x00, 0x00, &d2h_clk_rpt),         /* Report GPS Clock info     */
	AI2_OBJ(0x3C, 0x00, 0x00, &d2h_clk_rpt),         /* Report GLO Clock info     */
	AI2_OBJ(0x0B, 0x00, 0x00, &d2h_alm_rpt),         /* Report Almanac            */
	AI2_OBJ(0x0A, 0x00, 0x00, &d2h_eph_rpt),         /* Report Ephemeris          */
	AI2_OBJ(0x0C, 0x00, 0x00, &d2h_ion_rpt),         /* Report Ionospheric Info   */
	AI2_OBJ(0x0D, 0x00, 0x00, &d2h_utc_rpt),         /* Report UTC                */
	AI2_OBJ(0x3D, 0x00, 0x00, &d2h_utc_rpt),         /* Report GLO UTC            */
	AI2_OBJ(0x0E, 0x00, 0x00, &d2h_health_stat_rpt), /* Report SV Health          */
	AI2_OBJ(0x11, 0x00, 0x00, &d2h_sv_dir_rpt),      /* Report SV Direction       */
	AI2_OBJ(0x10, 0x00, 0x00, &d2h_alm_stat_rpt),    /* Report Almanac Status     */
	AI2_OBJ(0x0F, 0x00, 0x00, &d2h_eph_stat_rpt),    /* Report Ephemeris Status   */
	AI2_OBJ(0x09, 0x00, 0x00, &d2h_meas_stat_rpt),   /* Report Measurement Status */
	AI2_OBJ(0x07, 0x00, 0x00, &d2h_pos_stat_rpt),    /* Report Position Status    */
	AI2_OBJ(0xF7, 0x02, 0xFF, &d2h_calib_resp),
	AI2_OBJ(0xF7, 0x11, 0xFF, &d2h_sv_slot_rpt),     /* SV Slot mapping Report    */
	AI2_OBJ(0x15, 0x00, 0x00, &d2h_dev_cw_test_report),     /* Product Line test  */
	AI2_OBJ(0xCA, 0x00, 0x00, &d2h_dev_cw_test_report), 	/* Product Line test  */
	AI2_OBJ(0x13, 0x00, 0x00, &d2h_rep_diag_str),     /* Report Diag string test  */
	AI2_OBJ(0xFF, 0xFF, 0xFF, NULL)
};

/*------------------------------------------------------------------------------
 * D2H framework engine
 *-----------------------------------------------------------------------------*/

static int d2h_process_4rpc(struct d2h_ai2 *ai2_d2h)
{
	struct rpc_msg *rmsg;
	int ret_val = -1;
	unsigned int rei_select;

	if(!ai2_d2h->do_ie_4rpc)
		goto d2h4rpc_exit1;

	if(!(rmsg = rpc_msg_alloc()))
		goto d2h4rpc_exit1;

	/* Take global mutex */
	os_mutex_take(mutex_hnd);

	rei_select = ai2_d2h->rei_select;

	/* Give global mutex */
	os_mutex_give(mutex_hnd);

	if(ai2_d2h->do_ie_4rpc(ai2_d2h, rmsg, rei_select))
		goto d2h4rpc_exit2;

	if(rpc_svr_send(rmsg) < 0)
		goto d2h4rpc_exit2;

	ret_val = 0;

d2h4rpc_exit2:
	rpc_msg_free(rmsg);

d2h4rpc_exit1:
	return ret_val;
}

static int d2h_process_kick(struct d2h_ai2 *ai2_d2h)
{
	if(ai2_d2h->init_fixup && ai2_d2h->init_fixup(ai2_d2h))
		return -1;

	if(!(ai2_d2h->ai2_struct = malloc(ai2_d2h->ai2_st_len)))
		return -1;

	if(ai2_d2h->st_set_mem && ai2_d2h->st_set_mem(ai2_d2h))
		return -1;

	if(ai2_d2h->gen_struct(ai2_d2h)) {
		free(ai2_d2h->ai2_struct);
		ai2_d2h->ai2_struct = NULL;
		return -1;
	}

	return 0;
}

static struct d2h_ai2* d2h_ai2_find_obj(unsigned char pkt_id,
										unsigned char sub_id)
{
	unsigned short  key_id = AI2_KEY_GEN(pkt_id, sub_id);
	struct svr_ai2 *an_ai2 = ai2_objs;

	while(an_ai2->ai2_d2h) {
		if((an_ai2->key_id & an_ai2->k_mask) ==
		   (key_id & an_ai2->k_mask))
			break;

		an_ai2++;
	}

	return an_ai2->ai2_d2h;
}

/*------------------------------------------------------------------------------
 * Public functions to support D2H settings / configuration
 *-----------------------------------------------------------------------------*/

unsigned short d2h_ai2_key_id(unsigned char pkt_id,	unsigned char sub_id)
{
	unsigned short  key_id = AI2_KEY_GEN(pkt_id, sub_id);
	struct svr_ai2 *an_ai2 = ai2_objs;

	while(an_ai2->ai2_d2h) {
		if((an_ai2->key_id & an_ai2->k_mask) ==
		   (key_id & an_ai2->k_mask)) {
				key_id = (key_id & an_ai2->k_mask);
				return (((key_id >> 8) & 0x00FF) | ((key_id << 8) & 0xFF00));
		}

		an_ai2++;
	}

	return 0xFFFF;
}

/*
   Define (file) global d2h_mutex;
 */
void d2h_set_report_format(enum d2h_report rpt_id, unsigned int report_flags)
{
	unsigned char pkt_id, sub_id;
	struct d2h_ai2 *ai2_d2h;

	switch(rpt_id) {

		case d2h_pos:
			pkt_id = 0xD5; sub_id = 0x00; /* Ext PVT Report */
			/* Need to think about PVT report for BLF */
			break;

		case d2h_msr:
			pkt_id = 0xF7; sub_id = 0x01; /* Ext MSR Report */
			break;

		case d2h_clk:
			pkt_id = 0x05; sub_id = 0x00; /* Clock Report */
			break;

		case d2h_eph:
		case d2h_alm:
		case d2h_utc:
		case d2h_ion:
		default:
			pkt_id = 0xFF; sub_id = 0xFF;
			break;
	}

	ai2_d2h = d2h_ai2_find_obj(pkt_id, sub_id);
	if(!ai2_d2h)
		return;

	/* Take global mutex */
	os_mutex_take(mutex_hnd);

	ai2_d2h->rei_select = report_flags;

	/* Give global mutex */
	os_mutex_give(mutex_hnd);

	return;
}

unsigned int d2h_get_report_format(struct d2h_ai2 *ai2_d2h)
{
	unsigned int report_flags;

	/* Take global mutex */
	os_mutex_take(mutex_hnd);

	report_flags = ai2_d2h->rei_select;

	/* Give global mutex */
	os_mutex_give(mutex_hnd);

	return report_flags;
}

void d2h_sync_flg_update(void)
{

	LOGPRINT(d2h, info, "d2h_sync_flg_update ++");
	/*Set the sync flag to close the thread.*/	
	os_mutex_take(d2h_sync->mutex);
	d2h_sync->flag = 1;
	os_mutex_give(d2h_sync->mutex);
	LOGPRINT(d2h, info, "d2h_sync_flg_update --");	
}

static struct d2h_ai2* d2h_ai2_identify(const unsigned char *ai2_pkt)
{
	return d2h_ai2_find_obj(ai2_pkt[0], ai2_pkt[3]);
}

/*------------------------------------------------------------------------------
 * Core D2H processing and framework
 *-----------------------------------------------------------------------------*/

#define AI2_PKT_HDR_SIZE 3
static void d2h_framework(const unsigned char *ai2_pkt, unsigned int size)
{
	struct d2h_ai2 *ai2_d2h = d2h_ai2_identify(ai2_pkt);

	LOGPRINT(d2h, info, "got ai2 pkt %X of size %d", *ai2_pkt, size);
	
	if(!ai2_d2h) {
		LOGPRINT(d2h, warn, "no d2h handler for ai2 pkt %X", *ai2_pkt);
		return;
	}

	/*Discard the position packet that is just 9bytes(same as position flags being 0)*/
	if((*ai2_pkt == 0xD5) && (size <= 0x09)){
		LOGPRINT(d2h, warn, "Discard the position info", *ai2_pkt);	
		return;
	}

	ai2_d2h->ai2_packet = (unsigned char *)ai2_pkt;
	ai2_d2h->ai2_pkt_sz = size;

	/* Preprocessing and decoding. */
	if(!ai2_d2h->gen_struct || d2h_process_kick(ai2_d2h))
		goto d2hproc_exit1;

	/*
	 * Check if H2D is waiting for this ai2 packet. If yes, this call
	 * returns. If no, proceed with D2H framework of calls.
	 */
	if(SEQ_OK == seq_post_event(ai2_pkt)) {
		goto d2hproc_exit2;
	}

	if(ai2_d2h->first_prep && ai2_d2h->first_prep(ai2_d2h))
		goto d2hproc_exit2;

	if(ai2_d2h->assess_qoi && ai2_d2h->assess_qoi(ai2_d2h))
		goto d2hproc_exit2;

	if(ai2_d2h->nvs_action && ai2_d2h->nvs_action(ai2_d2h))
		goto d2hproc_exit2;

	if(ai2_d2h->final_prep && ai2_d2h->final_prep(ai2_d2h))
		goto d2hproc_exit2;

	if(ai2_d2h->do_ie_4rpc)
		d2h_process_4rpc(ai2_d2h);

d2hproc_exit2:
	free(ai2_d2h->ai2_struct);
	ai2_d2h->ai2_struct = NULL;

d2hproc_exit1:
	ai2_d2h->ai2_packet = NULL;
	ai2_d2h->ai2_pkt_sz = 0;

	return;
}

static void d2h_dev_msg_rx(const unsigned char *ai2_pkt, unsigned int size)
{
	unsigned int unit_size;

	while (size != 0) {
		unit_size = ((ai2_pkt[2] << 8) | ai2_pkt[1]) + AI2_PKT_HDR_SIZE;

		/* Some sanity. */
		if (unit_size > size) {
			LOGPRINT(d2h, err, "unit size %d, size %d", unit_size, size);
			size = 0;
		}
		else {
			LOGPRINT(d2h, info, "got ai2 pkt with id 0x%X and size %d", *ai2_pkt, unit_size);
			d2h_framework(ai2_pkt, unit_size);

			/* Move to next packet. */
			ai2_pkt+= unit_size;
			size -= unit_size;
		}
	}
	return;
}

static int d2h_init(void)
{
	struct ai2_cfg ai2_cfg[1];
	int ret;
	memset(ai2_cfg, 0x00, sizeof(struct ai2_cfg));

	mutex_hnd = os_mutex_init();

	/* Configure AI2 subsys before opening it. */
	ai2_cfg->tx_req_ack = AI2_REQUIRE_ACK;
	if (1 == ai2_cfg->tx_req_ack) {
		ai2_cfg->tx_retry_count = AI2_RETRY_COUNT;
		ai2_cfg->tx_retry_delay = AI2_RETRY_DELAY;
	} else {
		ai2_cfg->tx_retry_count = 0;
		ai2_cfg->tx_retry_delay = 0;	
	}

	ret = ai2_config(ai2_cfg);
	return (ai2_hdl = ai2_open(ai2_op_read) < 0) ? D2H_ERR : D2H_OK;
}

static int d2h_deinit(void)
{
	
	/* Deinit ai2. */
	if ((ai2_hdl = ai2_close(ai2_op_write)) < 0)
		return D2H_ERR;
	
	return D2H_OK;
}

void *d2h_thread(void *arg)
{
	int flag = 0;
	int res = 0;
	unsigned char *rxbuf = NULL;

	d2h_sync = (struct thread_sync *) arg;

	/* Init. */
	if (D2H_OK != d2h_init())
		goto d2h_init_fail;

	while(1) {
		os_mutex_take(d2h_sync->mutex);
		flag = d2h_sync->flag;
		os_mutex_give(d2h_sync->mutex);

		if (flag) {
			LOGPRINT(d2h, info, "d2h_thread exiting");
			break;
		}

		LOGPRINT(d2h, info, "waiting on device");
		res = ai2_read(ai2_hdl, &rxbuf);
		if (res <= 0) {
			LOGPRINT(d2h, err, "device read returns %d", res);
		}
		else {
			if (*rxbuf == 0) {
			LOGPRINT(d2h, err, "null %d", res);
			}
				
			d2h_dev_msg_rx(rxbuf, res);
		}
	}
	
	LOGPRINT(d2h, info, "out of d2h_thread");
	/* Deinit. */
	if (D2H_OK != d2h_deinit())
		goto d2h_deinit_fail;

	pthread_exit(NULL);
	return NULL;

d2h_deinit_fail:
d2h_init_fail:

	UNUSED(arg);

	/* No error handling possible. No grace. */
	LOGPRINT(d2h, err, "resource error");
	return NULL;

}

unsigned char d2h_get_msg_id(struct d2h_ai2 *ai2_d2h)
{
	return ai2_d2h->ai2_packet[0];
}
