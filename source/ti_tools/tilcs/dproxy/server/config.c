/*
 * config.c
 *
 * Server internal configuration.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "device.h"
#include "logger.h"

#define MK_STR(_str_) #_str_

#define ZERO                MK_STR(0)
#define SIMPLE_SERVER       MK_STR(1)
#define FDIC_MODE           MK_STR(1)
#define OPER_MODE           MK_STR(0)
#define WATCHDOG_CTRL       MK_STR(0)
#define FEATURE_CTRL        MK_STR(0)



struct sys_configuration {

	unsigned int   			int_test;
	unsigned int    		urt_baud;
	unsigned char   		cnst_cfg;
	unsigned char 			svr_mode;
	unsigned char			fdc_mode;
	unsigned char 			alt_hold;
	unsigned short  		freq_unc;
	unsigned short  		clk_acc;
	unsigned short  		usr_acc;
	signed char     		ele_mask;
	unsigned char 			pdop_msk;
	unsigned char 			fea_ctrl;
	unsigned char   		d2op_msk;
	unsigned char   		stmp_evt;
	unsigned char   		opr_mode;
	struct oscillator_parameters	os_param;
	unsigned char   		tpol_sel;
	unsigned char   		blk_ctrl;
	float           		c_by_n_l;
	unsigned char   		pps_o_en;
	unsigned char 			wt_ctrl;
	unsigned char 			utc_lsec;
	unsigned char 			cal_ctrl;
	unsigned char   		ln_e_cry;
	struct sbas_control		sbs_ctrl;
	unsigned char   		lfx_mode;
	unsigned int  			ref_freq;
	unsigned int 			evt_per;
	unsigned int 			evt_asyn;
	unsigned short			rep_rate;
	unsigned char 			rep_sec;
	unsigned int 			pos_vel;
	unsigned int 			pos_unc;
	unsigned int 			gps_alma;
	unsigned int 			gps_ephe;
	unsigned int 			gps_hlth;
	unsigned char   		dwld_opt;
	unsigned int 			glo_alma;
	unsigned int 			glo_ephe;
	unsigned int            qop_acc;
	unsigned int            qop_time;
	char                    pac_path[50];

	unsigned char                   gps_gnss_id;
	unsigned int                    gps_ganss_cmn;
	unsigned int                    gps_caps_bits;   
	unsigned int                    gps_asst_bits;    
	unsigned char                   gps_sigs_bits;    
	unsigned char                   gps_sbas_bits;

	unsigned char	                glo_gnss_id;
	unsigned int	                glo_ganss_cmn;
	unsigned int	                glo_caps_bits;	
	unsigned int	                glo_asst_bits;	 
	unsigned char	                glo_sigs_bits;    
	unsigned char	                glo_sbas_bits;   
    unsigned int 					pdop_mask_time_out;

	unsigned int 					tcxo_short_term_freq_un;
	unsigned int 					tcxo_long_term_freq_un;
	
	unsigned int					single_pe_a3;;
	unsigned int					single_pe_pdop_mask;

	unsigned int					tcxo_calib_enable_flag;

	unsigned char 					pa_blank_polarity;

	unsigned int            new_log_flag;
	unsigned int			notch_filter_cfg;

	
	unsigned int 					logging_mask;
	unsigned int					rtc_inj_ctrl;
	unsigned int 					rtc_time_qual_src;
	unsigned short 					host_rtc_unc;

	unsigned int 			cold_t1;
	unsigned int 			cold_t2;
	unsigned int 			cold_a1;
	unsigned int 			cold_a2;
	
	unsigned int 			warm_t1;
	unsigned int 			warm_t2;
	unsigned int 			warm_a1;
	unsigned int 			warm_a2;

	unsigned int 			hot_t1;
	unsigned int 			hot_t2;
	unsigned int 			hot_a1;
	unsigned int 			hot_a2;	

	unsigned int 			ee_t1;
	unsigned int 			ee_t2;
	unsigned int 			ee_a1;
	unsigned int 			ee_a2;

	unsigned int 			calib_mode;
	unsigned int 			apm_flag;

	unsigned int 			alt_unc;
	unsigned int 			gps_time_unc;
	unsigned int 			glo_time_unc;
	unsigned int 			time_unc;
};

static struct sys_configuration sys_cfg_param;
static void *mutex_hnd = NULL;

struct config_params {
	char                   *field_name;
	enum  sys_config_param  cfg_par;
	char			cfg_type;
	void 		       *store_addr;
	char 			value_type;
};
static FILE *read_fp;

#define ADDR(x) (&sys_cfg_param.x)

static struct config_params cfg_params[] = {

	{MK_STR(internal_test) , cfg_internal_test,  'P', ADDR(int_test), 'I'},
	{MK_STR(uart_bauds_r)  , cfg_baud_rate    ,  'P', ADDR(urt_baud), 'I'},
	{MK_STR(constell_cfg)  , cfg_const_config ,  'P', ADDR(cnst_cfg), 'C'},
	{SIMPLE_SERVER         , cfg_server_mode  ,  'H', ADDR(svr_mode), 'C'},
	{FDIC_MODE             , cfg_fdic_mode    ,  'H', ADDR(fdc_mode), 'C'},
	{MK_STR(gps_alt_hold)  , cfg_alt_mode     ,  'P', ADDR(alt_hold), 'C'},
	{MK_STR(gps_freq_unc)  , cfg_gps_frequn   ,  'P', ADDR(freq_unc), 'S'},
	{MK_STR(max_clk_acc)   , cfg_gps_acc      ,  'P', ADDR(clk_acc) , 'S'},
	{MK_STR(max_user_acc)  , cfg_usr_acc      ,  'P', ADDR(usr_acc) , 'S'},
	{MK_STR(elevation_mask), cfg_ele_mask     ,  'P', ADDR(ele_mask), 'c'},
	{ZERO                  , cfg_pdop_mask    ,  'R', ADDR(pdop_msk), 'C'},
	{FEATURE_CTRL          , cfg_feat_ctrl    ,  'H', ADDR(fea_ctrl), 'C'},
	{ZERO                  , cfg_2pdop_mask   ,  'R', ADDR(d2op_msk), 'C'},
	{MK_STR(time_stamp_evt), cfg_time_event   ,  'P', ADDR(stmp_evt), 'C'},
	{OPER_MODE             , cfg_oper_mode    ,  'H', ADDR(opr_mode), 'C'},
	{ZERO                  , cfg_osc_param    ,  'R', ADDR(os_param), 'O'},
	{MK_STR(time_pol_sel)  , cfg_time_porsel  ,  'P', ADDR(tpol_sel), 'C'},
	{MK_STR(pa_blank_ctrl) , cfg_blank_ctrl   ,  'P', ADDR(blk_ctrl), 'C'},
	{MK_STR(c_by_n_loss)   , cfg_cbyn_loss    ,  'P', ADDR(c_by_n_l), 'F'},
	{WATCHDOG_CTRL         , cfg_wtdog_ctrl   ,  'H', ADDR(wt_ctrl) , 'C'},
	{ZERO                  , cfg_utc_lpsec    ,  'R', ADDR(utc_lsec), 'C'},
	{ZERO                  , cfg_calib_ctrl   ,  'R', ADDR(cal_ctrl), 'C'},
	{MK_STR(pps_out_enbl)  , cfg_pps_opctrl   ,  'P', ADDR(pps_o_en), 'S'},
	{MK_STR(lna_ext_cryst) , cfg_lnaext_cryt  ,  'P', ADDR(ln_e_cry), 'C'},
	{ZERO                  , cfg_sbas_ctrl    ,  'R', ADDR(sbs_ctrl), 'B'},
	{MK_STR(loc_fix_mode)  , cfg_locfix_mode  ,  'P', ADDR(lfx_mode), 'C'},
	{MK_STR(ref_clk_freq)  , cfg_refclk_freq  ,  'P', ADDR(ref_freq), 'I'},
	{ZERO                  , cfg_evt_period   ,  'R', ADDR(evt_per) , 'I'},
	{ZERO                  , cfg_evt_async    ,  'R', ADDR(evt_asyn), 'I'},
	{ZERO                  , cfg_prd_reprate  ,  'R', ADDR(rep_rate), 'S'},
	{ZERO                  , cfg_prd_repsec   ,  'R', ADDR(rep_sec) , 'C'},
	{MK_STR(pos_vel)       , cfg_pos_vel      ,  'P', ADDR(pos_vel) , 'I'},
	{MK_STR(pos_unc_ther)  , cfg_pos_unc      ,  'P', ADDR(pos_unc) , 'I'},
	{MK_STR(gps_alm_age)   , cfg_gps_alm      ,  'P', ADDR(gps_alma), 'I'},
	{MK_STR(gps_eph_age)   , cfg_gps_eph      ,  'P', ADDR(gps_ephe), 'I'},
	{MK_STR(gps_sv_hlth)   , cfg_gps_svhlth   ,  'P', ADDR(gps_hlth), 'I'},
	{MK_STR(down_load_opt) , cfg_dwnld_opt    ,  'P', ADDR(dwld_opt), 'C'},
	{MK_STR(glo_alm_age)   , cfg_glo_alm      ,  'P', ADDR(glo_alma), 'I'},
	{MK_STR(glo_eph_age)   , cfg_glo_eph      ,  'P', ADDR(glo_ephe), 'I'},
	{MK_STR(qop_accuracy)  , cfg_qop_acc      ,  'P', ADDR(qop_acc) , 'I'},
	{MK_STR(qop_timeout)   , cfg_qop_time     ,  'P', ADDR(qop_time), 'I'},
	{MK_STR(patch_path)    , cfg_patch_path   ,  'P', ADDR(pac_path), 'T'},

	{MK_STR(gps_gnss_id)   , cfg_gps_gnss_id   , 'P', ADDR(gps_gnss_id)  , 'C'},
	{MK_STR(gps_ganss_cmn) , cfg_gps_ganss_cmn , 'P', ADDR(gps_ganss_cmn), 'I'},
	{MK_STR(gps_caps_bits) , cfg_gps_caps_bits , 'P', ADDR(gps_caps_bits), 'I'},
	{MK_STR(gps_asst_bits) , cfg_gps_asst_bits , 'P', ADDR(gps_asst_bits), 'I'},
	{MK_STR(gps_sigs_bits) , cfg_gps_sigs_bits , 'P', ADDR(gps_sigs_bits), 'C'},
	{MK_STR(gps_sbas_bits) , cfg_gps_sbas_bits , 'P', ADDR(gps_sbas_bits), 'C'},

	{MK_STR(glo_gnss_id)   , cfg_glo_gnss_id   , 'P', ADDR(glo_gnss_id)  , 'C'},
	{MK_STR(glo_ganss_cmn) , cfg_glo_ganss_cmn , 'P', ADDR(glo_ganss_cmn), 'I'},
	{MK_STR(glo_caps_bits) , cfg_glo_caps_bits , 'P', ADDR(glo_caps_bits), 'I'},
	{MK_STR(glo_asst_bits) , cfg_glo_asst_bits , 'P', ADDR(glo_asst_bits), 'I'},
	{MK_STR(glo_sigs_bits) , cfg_glo_sigs_bits , 'P', ADDR(glo_sigs_bits), 'C'},
	{MK_STR(glo_sbas_bits) , cfg_glo_sbas_bits , 'P', ADDR(glo_sbas_bits), 'C'},

	{MK_STR(pdop_mask_time_out), cfg_pdop_mask_time_out, 'P', ADDR(pdop_mask_time_out) , 'I'},

	{MK_STR(new_log_flag)  , cfg_new_log_flag  , 'P', ADDR(new_log_flag) , 'I'},
	
	{MK_STR(tcxo_short_term_freq_un) , cfg_tcxo_short_term_freq_unc , 'P', ADDR(tcxo_short_term_freq_un), 'I'},
	{MK_STR(tcxo_long_term_freq_un) , cfg_tcxo_long_term_freq_unc , 'P', ADDR(tcxo_long_term_freq_un), 'I'},

	{MK_STR(single_pe_a3) ,			cfg_single_pe_a3 ,			'P',	ADDR(single_pe_a3),			'I'},
	{MK_STR(single_pe_pdop_mask) ,	cfg_single_pe_pdop_mask ,	'P',	ADDR(single_pe_pdop_mask),	'I'},
	
	{MK_STR(tcxo_calib_enable_flag) , 	cfg_calib_enable_flag	, 'P'	, 	ADDR(tcxo_calib_enable_flag)	, 'I'},
	
	{MK_STR(notch_filter_cfg), cfg_notch_filter, 'P', ADDR(notch_filter_cfg), 'I'},

	{MK_STR(pa_blank_polarity) , cfg_pa_blank_polarity , 'P', ADDR(pa_blank_polarity), 'C'},

	{MK_STR(logging_mask)  , cfg_logging_mask  , 'P', ADDR(logging_mask) , 'I'},
	
	{MK_STR(rtc_inj_ctrl), 		cfg_rtc_inj_ctrl, 		'P', ADDR(rtc_inj_ctrl), 		'I'},
	{MK_STR(rtc_time_qual_src), cfg_rtc_time_qual_src,  'P', ADDR(rtc_time_qual_src),   'I'},
	{MK_STR(host_rtc_unc), cfg_host_rtc_unc, 'P', ADDR(host_rtc_unc), 'S'},

	{MK_STR(cold_t1) , cold_t1 , 'P', ADDR(cold_t1), 'I'},
	{MK_STR(cold_t2) , cold_t2 , 'P', ADDR(cold_t2), 'I'},
	{MK_STR(cold_a1) , cold_a1 , 'P', ADDR(cold_a1), 'I'},
	{MK_STR(cold_a2) , cold_a2 , 'P', ADDR(cold_a2), 'I'},
	
	{MK_STR(warm_t1) , warm_t1 , 'P', ADDR(warm_t1), 'I'},
	{MK_STR(warm_t2) , warm_t2 , 'P', ADDR(warm_t2), 'I'},
	{MK_STR(warm_a1) , warm_a1 , 'P', ADDR(warm_a1), 'I'},
	{MK_STR(warm_a2) , warm_a2 , 'P', ADDR(warm_a2), 'I'},
	
	{MK_STR(hot_t1)  , hot_t1 ,  'P', ADDR(hot_t1), 'I'},
	{MK_STR(hot_t2)  , hot_t2 ,  'P', ADDR(hot_t2), 'I'},
	{MK_STR(hot_a1)  , hot_a1 ,  'P', ADDR(hot_a1), 'I'},
	{MK_STR(hot_a2)  , hot_a2,   'P', ADDR(hot_a2), 'I'},

        {MK_STR(ee_t1)  , ee_t1 ,  'P', ADDR(ee_t1), 'I'},
        {MK_STR(ee_t2)  , ee_t2 ,  'P', ADDR(ee_t2), 'I'},
        {MK_STR(ee_a1)  , ee_a1 ,  'P', ADDR(ee_a1), 'I'},
        {MK_STR(ee_a2)  , ee_a2,   'P', ADDR(ee_a2), 'I'},

	{MK_STR(calib_mode)  , cfg_calib_mode,   'P', ADDR(calib_mode), 'I'},

	{MK_STR(apm_flag)  , cfg_apm_flag,   'P', ADDR(apm_flag), 'I'},

	{MK_STR(alt_unc)  , cfg_alt_unc,   'P', ADDR(alt_unc), 'I'},
	{MK_STR(gps_time_unc)  , cfg_nvs_gps_time_unc,   'P', ADDR(gps_time_unc), 'I'},
	{MK_STR(glo_time_unc)  , cfg_nvs_glo_time_unc,   'P', ADDR(glo_time_unc), 'I'},
	{MK_STR(time_unc)  , cfg_time_unc,   'P', ADDR(time_unc), 'I'},
	{NULL                  , cfg_param_end     ,'\0', NULL            , '\0'}
};

/* setup the configuration value of the parameter with the value */
static int setup_cfg_value(struct config_params *param, void **value, int type)
{
	char *src;
	char *dst;
	switch(param->value_type) {

		case 'I':
			*((unsigned int *)param->store_addr) =
				type ? (unsigned int) atoi(*value) : **(unsigned int **)(value);
			break;

		case 'S':
			*((unsigned short *)param->store_addr) =
				type ? (unsigned int) atoi(*value) : **(unsigned short **)(value);
			break;

		case 'C':
			*((unsigned char *)param->store_addr) =
				type ? (unsigned int) atoi(*value) : **(unsigned char **)(value);
			break;

		case 'F':
			*((float *)param->store_addr) =
				type ? (unsigned int)atof(*value) : **(float **)(value);
			break;

		case 'O':
			if (!type)
				*((struct oscillator_parameters *)param->
				  store_addr) =
					**(struct oscillator_parameters **)(value);
			break;

		case 'B':
			if (!type)
				*((struct sbas_control*)param->store_addr) =
					**(struct sbas_control**)(value);
			break;

		case 'i':
			*((signed int *)param->store_addr) =
				type ? (int) atoi(*value) : **(signed int **)(value);
			break;

		case 's':
			*((signed short *)param->store_addr) =
				type ? (short) atoi(*value) : **(signed short **)(value);
			break;

		case 'c':
			*((signed char *)param->store_addr) =
				type ? (char) atoi(*value) : **(signed char **)(value);
			break;

		case 'T':
			src = (char *)(*value);
			dst = param->store_addr;
			while ((*src != '\n') && (*src != '\r') && (*src != '\t') && 
				   (*src != ' ') && (*src != '\0') && (*src != '#'))
				*dst++ = *src++;
			*dst = '\0';
			break;

		default:
			break;
	}

	return cfg_success;
}

static int get_param_name_tok(char *str, char **name)
{
	char c;

	*name = NULL;

	while((c = *str++)) {

		/* check for leading spaces in the string */
		if(((c == ' ') || (c == '\t')) && !(*name))
			continue;

		/* delimiters */
		if((c == '#') || (c == ' ') || (c == '=') || (c == '\n') || (c == '\t'))
			break;

		/* begining of name of parameter */
		if(*name == NULL)
			*name = str - 1;
	}

	return (*name) ? (--str - *name) : 0;
}

static int get_param_value_tok(char *str, char **value)
{
	char c;
	int  eq_flag = 0;

	*value = NULL;

	while((c = *str++)) {

		/* check for leading spaces in string */
		if(((c == ' ') || (c == '\t') ) && !(*value))
			continue;

		/* was '=' already encountered? */
		if((c == '=') && !(eq_flag)){
			eq_flag = 1;
			continue;
		}

		/* delimiters */
		if((c == '#') || (c == ' ') || (c == '=') || (c == '\n') || (c == '\t'))
			break;


		/* begining of value of parameter */
		if(*value == NULL)
			*value = str - 1;
	}

	return (*value) ? (--str - *value) : 0;
}

static unsigned int parse_line(struct config_params *param)
{
	unsigned int name_len  = 0;
	unsigned int value_len = 0;

	char *name, *value;
	char line[200] = {0};

	fseek(read_fp, 0, SEEK_SET);
	/* get the data in the buff from the file */
	while (fgets(line, 200, read_fp) != NULL) {


		name_len = get_param_name_tok(line, &name);
		if (!name_len)
			continue;

		value_len = get_param_value_tok(name + name_len, &value);
		if (!value_len)
			continue;

		if (!strncmp(param->field_name, name, name_len))
			return setup_cfg_value(param, (void**) &value, 1);

	}

	return cfg_success;
}

static struct config_params* find_param(enum sys_config_param sys_param)
{

	struct config_params *param = cfg_params;

	while (param->cfg_par != cfg_param_end) {

		if (param->cfg_par == sys_param)
			return param;

		param++;

	}
	return NULL;
}

int sys_param_get(enum sys_config_param sys_param, void **buffer)
{
	struct config_params *param = find_param(sys_param);

	if (sys_param > cfg_param_end || !param)
		return cfg_error;

	os_mutex_take(mutex_hnd);

	*buffer = (void*)param->store_addr;

	os_mutex_give(mutex_hnd);

	return cfg_success;
}

int sys_param_set(enum sys_config_param sys_param, void **buffer)
{
	int ret_val;
	struct config_params *param = find_param(sys_param);

	if (sys_param > cfg_param_end || !param)
		return cfg_error;

	os_mutex_take(mutex_hnd);

	ret_val = setup_cfg_value(param, buffer, 0);

	os_mutex_give(mutex_hnd);

	return ret_val;
}

int sys_config_init(char *config_file_path)
{
	struct config_params *param = cfg_params;

	if (!mutex_hnd) {
		if (!(mutex_hnd = os_mutex_init())) {
			return cfg_error;
		}
	}

	os_mutex_take(mutex_hnd);

	read_fp = fopen(config_file_path, "r");
	if (!read_fp) {
		os_mutex_give(mutex_hnd);
		return cfg_error;
	}

	while (param->cfg_par < cfg_param_end) {

		if (param->cfg_type == 'P')
			parse_line(param);
		else if (param->cfg_type == 'R' || param->cfg_type == 'H')
			setup_cfg_value(param, (void**) &param->field_name, 1);
		param++;
	}

	fclose (read_fp);

	os_mutex_give(mutex_hnd);

	return cfg_success;
}

void sys_config_exit(void)
{

}

struct ptch_dwld_cfg *get_ptch_dwld_cfg(void)
{
        static struct ptch_dwld_cfg ptch_dwld_cfg[1];
	unsigned int *config = 0;

	if (cfg_success != sys_param_get(cfg_dwnld_opt, (void **)&config)) {
		LOGPRINT(patch, warn, "err read dwnld opt. ptch dwnld skipped");
		ptch_dwld_cfg->dwld_type = 5;
	}

	if (0 == *config) {
		ptch_dwld_cfg->dwld_type = 0x05; /* No brain washing device. */
		LOGPRINT(patch, info, "no patch download");
	} else if (1 == *config) {
		ptch_dwld_cfg->dwld_type = 0x01; /* Brain wash device. */
		LOGPRINT(patch, info, "patch download");
	} else if (2 == *config) {
		ptch_dwld_cfg->dwld_type = 0x03; /* New brain altogether. */
		LOGPRINT(patch, info, "ROM download");
	}

        return ptch_dwld_cfg;
}


struct dev_app_desc *get_app_desc_cfg(void)
{
	static struct dev_app_desc dev_app_desc[1];
	unsigned char *constell_config;
    
	sys_param_get(cfg_const_config, (void **)&constell_config);
	dev_app_desc->gnss_select = *constell_config;
	return dev_app_desc;
}


//: Protect me using semaphore
static unsigned int rx_on_time;
void set_rx_on_time(unsigned int tim)
{
	rx_on_time = tim;
}

unsigned int get_rx_on_time(void)
{
	return rx_on_time;
}

