/*
 * nvs_validate.c
 *
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "logger.h"
#include "config.h"
#include "nvs_validate.h"
#include "nvs_mod_init.h"

#include "utils.h"
#include "os_services.h"
#include "nvs_utils.h"

#define VALID_TIME_UNC          (60 * 1000)
#define C_MAX_VALID_TIME_UNC_MS (60 * 1000)
#define range_check(data, type, member, min, max) ({\
	const typeof(((type*)0)->member) _mem_var = ((type*)data)->member; \
	if ( _mem_var < min || _mem_var > max){ \
		LOGPRINT(nvs, warn, "%s.%s not in range:value [%d]",#type,#member,_mem_var); \
		return -1;  \
	}})

typedef int (*nvs_validate_aid)(void *data);
struct nvs_aid_desc {

	enum  nvs_record       record;
	nvs_validate_aid       validate_aid;
};


static void *mutex_hnd = NULL;

int elapsed_gps_secs(struct dev_gps_time *gps_tim)
{
	struct dev_gps_time gps_time;	
	unsigned int now_secs = os_utc_secs();
	unsigned int* val;

	if(!gps_tim){
		nvs_rec_get(nvs_gps_clk, &gps_time, 1);
		gps_tim = &gps_time;		
	}

	unsigned int rec_secs = nvs_rec_ts_s(nvs_gps_clk, gps_tim);
	unsigned short mantissa = (unsigned short)(gps_time.time_unc >> 5);	
	unsigned char exponent = (unsigned char)(gps_time.time_unc & 0x001F);
	float temp_flt = (float)ldexp(mantissa,exponent);
	sys_param_get(cfg_nvs_gps_time_unc,(void**) &val);

	if(now_secs < rec_secs){
		LOGPRINT(nvs, info, "elapsed_gps_secs: invalid now_secs [%d] < rec_secs [%d]",
			 now_secs, rec_secs);
		return -1;
	}

	if((now_secs - rec_secs) > (*val)){
		LOGPRINT(nvs, info, "elapsed_gps_secs: invalid now_secs [%d] - rec_secs [%d] > [%d]",
			 now_secs, rec_secs, *val);
		return -1;
	}

	if(gps_tim->week_num == GPS_WEEK_UNKNOWN){
		LOGPRINT(nvs, info, "elapsed_gps_secs: invalid gps_tim->week_num == GPS_WEEK_UNKNOWN");
		return -1;
	}

	if((temp_flt * 0.000001f) > C_MAX_VALID_TIME_UNC_MS){
		LOGPRINT(nvs, info, "elapsed_gps_secs: invalid Tunc [%d] > MAX time unc",
			 (temp_flt * 0.000001f ));
		return -1;
	}
#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "elapsed_gps_secs: valid now sec [%d] rec sec [%d]", now_secs, rec_secs);
#endif
	return (now_secs - rec_secs);
}

unsigned int do_gps_secs(struct dev_gps_time *gps_tim)
{
	return  (gps_tim->week_num * MAX_SECS_IN_1WEEK) +
			(gps_tim->time_msec / 1000);
}

void add2dev_gps_time(struct dev_gps_time *gps_tim, unsigned int secs)
{
	/* Converge every thing into seconds */
	unsigned int gps_secs = do_gps_secs(gps_tim); 

	/* Split estimated seconds into week and milli-seconds */        
	struct week_offset wkss = secs2week(gps_secs + secs);

	gps_tim->week_num   = wkss.Wn;
	gps_tim->time_msec  = wkss.ss * 1000;

	return;
}

long long elapsed_gps_msecs(struct dev_gps_time *gps_tim)
{
	struct dev_gps_time gps_time;	
	unsigned int *val;
	unsigned int now_secs;
	unsigned int now_msecs;
	unsigned long long now;
	unsigned long long rec_msecs;
	unsigned short mantissa;
	unsigned char exponent;
	float temp_flt;

	os_time_value(&now_secs, &now_msecs);
	now = (((unsigned long long) now_secs) * 1000ULL) 
		+ ((unsigned long long) now_msecs);

	if(!gps_tim) {
		nvs_rec_get(nvs_gps_clk, &gps_time, 1);
		gps_tim = &gps_time;		
	}

	nvs_rec_ts_ms(nvs_gps_clk, gps_tim, &rec_msecs);
	mantissa = (unsigned short)(gps_time.time_unc >> 5);	
	exponent = (unsigned char)(gps_time.time_unc & 0x001F);
	temp_flt = (float)ldexp(mantissa,exponent);

	sys_param_get(cfg_nvs_gps_time_unc,(void**) &val);

	if(now < rec_msecs) {
		LOGPRINT(nvs, info, "elapsed_gps_secs: gps time invalid now_msecs [%llu] < rec_msecs [%llu]", 
			 now, rec_msecs);
		return -1;
	}

	if ((now - rec_msecs) > (((unsigned long long) (*val)) * 1000ULL)) {
		LOGPRINT(nvs, info, "elapsed_gps_secs: gps time invalid now_msecs [%llu] - rec_msecs [%llu] > [%llu]", 
			 now, rec_msecs, (((unsigned long long) (*val)) * 1000ULL));
		return -1;
	}

	if(gps_tim->week_num == GPS_WEEK_UNKNOWN) {
		LOGPRINT(nvs, info, "elapsed_gps_secs: gps time invalid gps_tim->week_num == GPS_WEEK_UNKNOWN");
		return -1;
	}

	//if((temp_flt * 0.0000001f ) >  C_MAX_VALID_TIME_UNC_MS) {
	if((temp_flt * 0.000001f ) >  C_MAX_VALID_TIME_UNC_MS) {
		LOGPRINT(nvs, info, "elapsed_gps_secs: gps time invalid Tunc [%f] > MAX time unc", 
			 (temp_flt * 0.000001f ));
		return -1;
	}

	LOGPRINT(nvs, info, "elapsed_gps_secs: gps time valid now sec [%llu] rec sec [%llu]", 
		 now, rec_msecs);

	return (now - rec_msecs);
}

unsigned long long do_gps_msecs(struct dev_gps_time *gps_tim)
{
	return (((unsigned long long) gps_tim->week_num) * 
		((unsigned long long) MAX_SECS_IN_1WEEK) * 1000ULL) +
		((unsigned long long) gps_tim->time_msec);
}

void add2dev_gps_time_ms(struct dev_gps_time *gps_tim, unsigned long long msecs)
{
	/* Converge every thing into seconds */
	unsigned long long gps_msecs = do_gps_msecs(gps_tim);

	gps_tim->week_num   = (gps_msecs + msecs) / 
		(((unsigned long long) MAX_SECS_IN_1WEEK) * 1000ULL);
	gps_tim->time_msec  = (gps_msecs + msecs) - 
		(((unsigned long long)gps_tim->week_num) * 
		 ((unsigned long long) MAX_SECS_IN_1WEEK) * 1000ULL);

	return;
}

int elapsed_glo_secs(struct dev_glo_time *glo_time)
{
	struct dev_glo_time time_obj;
	unsigned int now_secs = os_utc_secs();
	unsigned int* val;

	if(!glo_time) {
		nvs_rec_get(nvs_glo_clk, &time_obj, 1);
		glo_time = &time_obj;
	}

	unsigned int rec_secs = nvs_rec_ts_s(nvs_glo_clk, glo_time);
	sys_param_get(cfg_nvs_glo_time_unc,(void**) &val);

	if(now_secs < rec_secs){
		LOGPRINT(nvs, info, "elapsed_glo_secs: invalid now_secs [%d] < rec_secs [%d]", now_secs, rec_secs);
		return -1;
	}

	if((now_secs - rec_secs) > (*val)){
		LOGPRINT(nvs, info, "elapsed_glo_secs: invalid now_secs [%d] - rec_secs [%d] > [%d]",
			 now_secs, rec_secs, (*val));
		return -1;
	}

	if(!(glo_time->time_status & 0x3)){
		LOGPRINT(nvs, info, "elapsed_glo_secs: invalid STATUS is not set");
		return -1;
	}

	if(glo_time->time_unc > VALID_TIME_UNC){
		LOGPRINT(nvs, info, "elapsed_glo_secs: invalid Tunc [%d] > VALID_TIME_UNC", glo_time->time_unc);
		return -1;
	}

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "elapsed_glo_secs: valid now sec [%d] rec sec [%d]", now_secs, rec_secs);
#endif

	return now_secs - rec_secs;
}

unsigned int do_glo_secs(struct dev_glo_time *glo_tim)
{
	struct YMDS_offset ymds;

	ymds.Yn = glo_tim->time_year  - 1996; /* To get Y offset */
	ymds.Mn = glo_tim->time_month - 1;    /* To make 0 based */
	ymds.Dn = glo_tim->time_day   - 1;    /* To make 0 based */
	ymds.ss = glo_tim->time_msec / 1000;  /* To make seconds */

	return YMDS2secs(ymds);
}

void add2dev_glo_time(struct dev_glo_time *glo_tim, unsigned int secs)
{
	unsigned int glo_secs = do_glo_secs(glo_tim); /* Converge into seconds */

	/* Split estimated seconds into Year, Month, Day and milli-seconds */
	struct YMDS_offset ymds = secs2YMDS(glo_secs + secs);            

	glo_tim->time_year  = ymds.Yn + 1996;
	glo_tim->time_month = ymds.Mn + 1;     /* To make 1 based       */
	glo_tim->time_day   = ymds.Dn + 1;     /* To make 1 based       */
	glo_tim->time_msec  = ymds.ss * 1000;  /* To make milli-seconds */
}

long long elapsed_glo_msecs(struct dev_glo_time *glo_time)
{
	struct dev_glo_time time_obj;
	unsigned int* val;
	unsigned int now_secs;
	unsigned int now_msecs;
	unsigned long long now;
	unsigned long long rec_msecs;

	if(!glo_time) {
		nvs_rec_get(nvs_glo_clk, &time_obj, 1);
		glo_time = &time_obj;
	}

	os_time_value(&now_secs, &now_msecs);
	now = (((unsigned long long) now_secs) * 1000ULL) 
		+ ((unsigned long long) now_msecs);

	nvs_rec_ts_ms(nvs_glo_clk, glo_time, &rec_msecs);
	sys_param_get(cfg_nvs_glo_time_unc,(void**) &val);

	if(now < rec_msecs) {
		LOGPRINT(nvs, info, "elapsed_glo_secs: glo time invalid now_msecs [%llu] < rec_msecs [%llu]", 
			 now, rec_msecs);
		return -1;
	}

	if((now - rec_msecs) > (((unsigned long long) (*val)) * 1000ULL)) {
		LOGPRINT(nvs, info, "elapsed_glo_secs: glo time invalid now_msecs [%llu] - rec_msecs [%llu] > [%llu]", 
			 now, rec_msecs, (((unsigned long long) (*val)) * 1000ULL));
		return -1;
	}

	if(!(glo_time->time_status & 0x3)) {
		LOGPRINT(nvs, info, "elapsed_glo_secs: glo time invalid STATUS is not set");
		return -1;
	}

	if((glo_time->time_unc) > VALID_TIME_UNC) {
		LOGPRINT(nvs, info, "elapsed_glo_secs: glo time invalid Tunc [%d] > VALID_TIME_UNC", 
			 glo_time->time_unc);
		return -1;
	}

	LOGPRINT(nvs, info, "elapsed_glo_secs: glo time valid now now_secs [%llu] rec_msecs [%llu]", 
		 now, rec_msecs);

	return now - rec_msecs;
}

unsigned long long do_glo_msecs(struct dev_glo_time *glo_tim)
{
	struct YMDS_offset ymds;

	ymds.Yn = glo_tim->time_year  - 1996; /* To get Y offset */
	ymds.Mn = glo_tim->time_month - 1;    /* To make 0 based */
	ymds.Dn = glo_tim->time_day   - 1;    /* To make 0 based */
	ymds.ss = glo_tim->time_msec / 1000;  /* To make seconds */

	return (((unsigned long long) YMDS2secs(ymds)) * 1000ULL) + 
		(((unsigned long long) glo_tim->time_msec) % 1000ULL);
}

void add2dev_glo_time_ms(struct dev_glo_time *glo_tim, unsigned long long msecs)
{
	unsigned long long glo_msecs = do_glo_msecs(glo_tim); /* Converge into mseconds */

	/* Split estimated seconds into Year, Month, Day and milli-seconds */
	struct YMDS_offset ymds = secs2YMDS((glo_msecs + msecs) / 1000);

	glo_tim->time_year  = ymds.Yn + 1996;
	glo_tim->time_month = ymds.Mn + 1;     /* To make 1 based       */
	glo_tim->time_day   = ymds.Dn + 1;     /* To make 1 based       */
	glo_tim->time_msec  = ymds.ss * 1000 + 
		((glo_msecs + msecs) % 1000ULL);  /* To make milli-seconds */
}

/* Assumes that function is called only on attaining valid conditions */
void fixup_dev_gnss_time(enum nvs_record record, void *data)
{
	if (record == nvs_gps_clk) {
		struct dev_gps_time *gps_tim = data;
		add2dev_gps_time_ms(gps_tim, elapsed_gps_msecs(gps_tim));
		return;
	}
	if (record == nvs_glo_clk) {
		struct dev_glo_time *glo_tim = data;
		add2dev_glo_time_ms(glo_tim, elapsed_glo_msecs(glo_tim));
		return;
	}
}

void fixup_dev_gnss_pos(void *data)
{
	struct dev_pos_info *dev = (struct dev_pos_info *) data;
	unsigned int *pos_vel, pos_secs, uncert = 0;
	unsigned int now_secs = os_utc_secs();

	sys_param_get(cfg_pos_vel, (void**) &pos_vel);

	dev->ell = BUF_OFFSET_ST(dev, struct dev_pos_info,
				 struct dev_ellp_unc);
	dev->utc = BUF_OFFSET_ST(dev->ell, struct dev_ellp_unc, 
				 struct dev_utc_info);

	if (dev->contents & DEV_POS_HAS_ELP) {
			uncert = (int)(10 * (pow(1.1, dev->ell->semi_major) - 1));
			uncert = (int)(uncert * C_RECIP_LSB_POS_SIGMA)/3;
		}else{

		uncert = (dev->unc_east > dev->unc_north) ? dev->unc_east :
								dev->unc_north;
		uncert = (uncert > dev->unc_vertical) ? uncert :
							dev->unc_vertical;
	}

	pos_secs = nvs_rec_ts_s(nvs_gps_pos, data);
	if (!pos_secs){
		LOGPRINT(nvs, warn, "fixup_dev_gnss_pos: invalid, time_stamp is invalid");
		return -1;
	}

	uncert += *pos_vel * ((now_secs - pos_secs)/3600) * 1000 * 10;

	/*Capping the Position uncertainity to 1000m*/	
	if (uncert < 10000)
		uncert = 10000;
	
	dev->contents &= ~DEV_POS_HAS_ELP;
	dev->unc_north = uncert;
	dev->unc_east = 0;
	dev->unc_vertical = 0;

	LOGPRINT(nvs, info, "fixup_dev_gnss_pos: Uncert=%d, Pos_sec=%d, Now_sec=%d",
						uncert, pos_secs, now_secs);
	return;
}

static unsigned int calc_glo_secs()
{
	struct dev_glo_time glo_tim;
	unsigned int glo_secs;
	nvs_rec_get(nvs_glo_clk, &glo_tim, 1);

	glo_secs = do_glo_secs(&glo_tim);

	/* Time elapsed since that report */
	int elapsed = elapsed_glo_secs(&glo_tim);

	LOGPRINT(nvs, info, "calc_glo_secs glo_secs[%d]", glo_secs + elapsed);

	return glo_secs + elapsed;
}

static unsigned int calc_glo_msecs()
{
	struct dev_glo_time glo_tim;
	unsigned long long glo_msecs;
	nvs_rec_get(nvs_glo_clk, &glo_tim, 1);

	glo_msecs = do_glo_msecs(&glo_tim);

	/* Time elapsed since that report */
	unsigned long long elapsed = elapsed_glo_msecs(&glo_tim);

	LOGPRINT(nvs, info, "calc_glo_msecs glo_msecs[%d]", glo_msecs + elapsed);

	return glo_msecs + elapsed;
}

static unsigned short get_gps_week(struct dev_gps_time *gps_tim)
{
       unsigned short mantissa;
       unsigned char exponent;
       float temp_flt;

       mantissa = (unsigned short)(gps_tim->time_unc >> 5);
       exponent = (unsigned char)(gps_tim->time_unc & 0x001F);
       temp_flt = (float)ldexp(mantissa,exponent);

       if( gps_tim->week_num == GPS_WEEK_UNKNOWN || 
		(temp_flt * 0.000001f) >  C_MAX_VALID_TIME_UNC_MS )
			return 0;

	return gps_tim->week_num;
}

static unsigned int calc_gps_secs()
{
	struct dev_gps_time gps_tim;
	unsigned int gps_secs; 
        int elapsed;

	nvs_rec_get(nvs_gps_clk, &gps_tim, 1);

	/* GPS time reported by receiver  */
	gps_secs = do_gps_secs(&gps_tim);

	/* Time elapsed since that report */
	elapsed  = elapsed_gps_secs(NULL);

	LOGPRINT(nvs, info, "calc_gps_secs gps_secs[%d]", gps_secs + elapsed);

	return gps_secs + elapsed;
}

static unsigned int calc_gps_msecs()
{
	struct dev_gps_time gps_tim;
	unsigned long long gps_msecs;
	unsigned long long elapsed;

	nvs_rec_get(nvs_gps_clk, &gps_tim, 1);

	/* GPS time reported by receiver  */
	gps_msecs = do_gps_msecs(&gps_tim);

	/* Time elapsed since that report */
	elapsed  = elapsed_gps_msecs(NULL);

	LOGPRINT(nvs, info, "calc_gps_msecs gps_msecs[%d]", gps_msecs + elapsed);

	return gps_msecs + elapsed;
}

static unsigned short valid_wk(unsigned short wk)
{
	struct dev_gps_time time[1];

	if(0xFFFF != wk) 
		return wk;

	if (nvs_rec_get(nvs_gps_clk, time, 1) > 0)
		return time->week_num;
	else
		return wk;
}

static int validate_gps_eph(void *data)
{
	struct dev_gps_eph *eph = (struct dev_gps_eph*) data;
        int exh_secs;
        int eph_life;
        unsigned int *valid_eph_age;
        unsigned int gps_secs = calc_gps_secs();
        unsigned int eph_secs = 
                valid_wk(eph->Week)*MAX_SECS_IN_1WEEK + eph->Toe*16;

	sys_param_get(cfg_gps_eph, (void**) &valid_eph_age);
        eph_life = (int) *valid_eph_age;

#ifdef VERBOSE_LOGGING
        LOGPRINT(nvs, info, "validate_gps_eph: PRN [%d], MAX_EPH_AGE [%d], gps_seconds [%u] and eph_secs[%u]", 
		 eph->prn, eph_life, gps_secs, eph_secs);
#endif

        exh_secs = abs((int)(gps_secs - eph_secs));
    if(exh_secs > eph_life) {
		LOGPRINT(nvs, err, "validate_gps_eph: invalid PRN [%d] and exh_secs [%d]",
			 eph->prn, exh_secs);
            return -1;
    }
        
	LOGPRINT(nvs, info, "validate_gps_eph: valid PRN[%d] and TTL [%d]", 
		 eph->prn, 
                 (eph_life - ((((int)(eph_secs - gps_secs)) > 0) ? 
                              -exh_secs : exh_secs)));

        return (eph_life - ((((int)(eph_secs - gps_secs)) > 0) ? 
                            -exh_secs : exh_secs));
}

static int validate_gps_alm(void *data)
{
	struct dev_gps_alm *alm = (struct dev_gps_alm*) data;
	struct dev_gps_time gps_time;
	nvs_rec_get(nvs_gps_clk, &gps_time, 1);
	unsigned short alm_week = get_gps_week(&gps_time);
	short exh_week;
	int *valid_alm_age, alm_life;

	if(!alm_week) {
		LOGPRINT(nvs, err, "validate_gps_alm: invalid PRN [%d] and alm_week [%d]",
							alm->prn, alm_week);
			return -1;
	}

	sys_param_get(cfg_gps_alm, (void**)&valid_alm_age);
	alm_life = *valid_alm_age;

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_gps_alm: PRN [%d] and MAX_ALM_AGE [%d]",
		 alm->prn, alm_life);
#endif

	exh_week = abs(alm_week - alm->Week);

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_gps_alm: exh_week [%d] and alm_week [%d]",
		 exh_week, alm->Week);
#endif
	if(exh_week > alm_life) {
		LOGPRINT(nvs, err, "validate_gps_alm: invalid PRN [%d]", alm->prn);
		return -1;
	}

	LOGPRINT(nvs, info, "validate_gps_alm: valid PRN[%d] and TTL [%d]", 
		 alm->prn, 
		 (MAX_SECS_IN_1WEEK * 
		  (alm_life - (((alm->Week - alm_week) > 0) ? -exh_week : exh_week))));

	return (MAX_SECS_IN_1WEEK * 
		(alm_life - (((alm->Week - alm_week) > 0) ? -exh_week : exh_week)));
}

static int validate_sv_health(void *data)
{
	struct dev_sv_hlth *sv_health = (struct dev_sv_hlth*) data;
	int *valid_hlth_age;
	unsigned int gps_secs, exh_secs, hlth_life;

	sys_param_get(cfg_gps_svhlth, (void**) &valid_hlth_age);
	hlth_life = *valid_hlth_age;

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_sv_health: max_health_age [%d]", hlth_life);
#endif

	if (sv_health->update_week == GPS_WEEK_UNKNOWN) {
		LOGPRINT(nvs, err, "validate_sv_health: invalid_sv_health, WEEK [%d]",
			 sv_health->update_week);
		return -1;
	}

	gps_secs = calc_gps_secs();
        exh_secs = gps_secs - 
                (sv_health->update_week*MAX_SECS_IN_1WEEK+ sv_health->update_msecs / 1000);
        
	if(exh_secs > hlth_life){
		LOGPRINT(nvs, err, "validate_sv_health: invalid_sv_health, exh_secs [%d] and gps_secs [%d]",
					     exh_secs, gps_secs);
            return -1;
	}

	LOGPRINT(nvs, info, "validate_sv_health: valid sv_health and TTL [%d]", 
		 hlth_life - exh_secs);
	
	return hlth_life - exh_secs;
}

static int validate_pos_info(void *data)
{
	float         pos_acc;
	signed long   age_secs;
	unsigned int *pos_vel, *pos_unc, pos_secs;

	sys_param_get(cfg_pos_vel, (void**) &pos_vel);
	sys_param_get(cfg_pos_unc, (void**) &pos_unc);

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_pos_info: pos_vel[%d], pos_unc[%d] ",
		 *pos_vel, *pos_unc);
#endif

	pos_secs = nvs_rec_ts_s(nvs_gps_pos, data);
	if (!pos_secs){
		LOGPRINT(nvs, warn, "validate_pos_info: invalid, time_stamp is invalid");
		return -1;
	}

	age_secs =  os_utc_secs() - pos_secs;

	pos_acc  = *pos_vel * age_secs / 3600.0;

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_pos_info: posUnc [%f], posAgeSec[%ld], time_stamp[%d]",
		 pos_acc, age_secs, pos_secs);
#endif

	if (age_secs < 0) {
		LOGPRINT(nvs, warn, "validate_pos_info: invalid, posAgeSec is -ve, reject position");
		return -1;
	}

	if(pos_acc > *pos_unc) {
		LOGPRINT(nvs, warn, "validate_pos_info: invalid, position unc too large [%d]", pos_acc);
		return -1;
	}

	LOGPRINT(nvs, info, "validate_pos_info: valid pos info");

	return 0; // Do we need to return TTL?
}

static int is_valid_gps_clk(void)
{
	return (elapsed_gps_msecs(NULL) < 0LL) ? 0 : 1;
}

static int is_valid_glo_clk(void)
{
	return (elapsed_glo_msecs(NULL) < 0LL) ? 0 : 1;
}

static int validate_gps_time(void *data)
{
	long long ret_val = elapsed_gps_msecs(data);

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_gps_time: elapsed_gps_msecs [%lld] ", ret_val);
#endif

	if (ret_val >= 0) {
		ret_val = 48LL*60LL*60LL*1000LL - ret_val;   //Validate for 48hrs.
	}

	LOGPRINT(nvs, info, "validate_gps_time: %s, GPS TIME TTL [%lld] msec",
		 (ret_val >= 0) ? "valid" : "invalid",
		 ret_val);

	return (ret_val >= 0) ? ((int) (ret_val / 1000LL)) : -1;
}

static int validate_glo_time(void *data)
{
	long long ret_val = elapsed_glo_msecs((struct dev_glo_time*)data);

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_glo_time: elapsed_glo_msecs [%lld] ", ret_val);
#endif

	if(ret_val >= 0) {
		ret_val = 30LL*60LL*1000LL - ret_val;
	}

	LOGPRINT(nvs, info, "validate_glo_time: %s, GLO TIME TTL [%lld] msec",
		 (ret_val >= 0) ? "valid" : "invalid",
		 ret_val);

	return (ret_val >= 0) ? ((int) (ret_val / 1000LL)) : -1;
}

static int validate_gps_iono(void *data)
{
	unsigned int now_secs =  os_utc_secs();
	unsigned int rec_secs =  nvs_rec_ts_s(nvs_gps_iono, data);
	int ret_val = now_secs - rec_secs;

	if ((ret_val >=0) && (ret_val < MAX_SECS_IN_365DY) ) {
		ret_val = MAX_SECS_IN_365DY - ret_val;
		LOGPRINT(nvs, info, "validate_gps_iono: valid, TTL [%d]", ret_val);
	} else {
		LOGPRINT(nvs, err, "validate_gps_iono: invalid, TTL [%d]", ret_val);
		ret_val = -1;
	}

	return ret_val;
}

static int validate_gps_utc(void *data)
{

	unsigned int now_secs =  os_utc_secs();
	unsigned int rec_secs =  nvs_rec_ts_s(nvs_gps_utc, data);

	int ret_val = now_secs - rec_secs;
	if ((ret_val >=0) && (ret_val < MAX_SECS_IN_365DY) ) {
		ret_val = MAX_SECS_IN_365DY - ret_val;
		LOGPRINT(nvs, info, "validate_gps_utc: valid, TTL [%d]", ret_val);
	} else {
		LOGPRINT(nvs, err, "validate_gps_utc: invalid, TTL [%d]", ret_val);
		ret_val = -1;
	}

	return ret_val;
}

static int validate_glo_utc(void *data)
{
	unsigned int now_secs =  os_utc_secs();
	unsigned int rec_secs =  nvs_rec_ts_s(nvs_glo_utc, data);

	int ret_val = now_secs - rec_secs;
	if ((ret_val >=0) && (ret_val < MAX_SECS_IN_365DY)) {
		ret_val = MAX_SECS_IN_365DY - ret_val;
		LOGPRINT(nvs, info, "validate_glo_utc: valid, TTL [%d]", ret_val);
	} else {
		LOGPRINT(nvs, err, "validate_glo_utc: invalid, TTL [%d]", ret_val);
		ret_val = -1;
	}

	return ret_val;
}

static int validate_gps_tcxo(void *data)
{
	UNUSED(data);
	return 0;
}

static int validate_gps_svdr(void *data)
{
	UNUSED(data);
	return 0;
}

static int validate_glo_eph(void *data)
{
	struct dev_glo_eph *eph = (struct dev_glo_eph*) data;
        int eph_life; 
        unsigned int *valid_eph_age; 

	sys_param_get(cfg_glo_eph, (void**) &valid_eph_age);
        eph_life = (int) *valid_eph_age;
	
        unsigned int rec_secs = nvs_rec_ts_s(nvs_glo_eph, eph);
       	unsigned int lpyr_secs = (((rec_secs - UTC_AT_GLO_ORIGIN) /
			                        MAX_SECS_4Y_BLOCK) *
									MAX_SECS_4Y_BLOCK);
															
		unsigned int eph_secs = DO_GLO_NT_SECS(eph->Nt) + 
                                DO_GLO_Tk_SECS(eph->Tk) + 
                                lpyr_secs;

    unsigned int glo_secs = calc_glo_secs();
        int exh_secs = abs((int)(glo_secs - eph_secs));

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_glo_eph: PRN [%d], NT [%d] and TK [%d]",
		 eph->prn, eph->Nt, eph->Tk);
        LOGPRINT(nvs, info, "validate_glo_eph: PRN [%d], MAX_EPH_AGE [%d], glo_seconds [%u], eph_secs[%u] and rec_glo_tim [%u]", 
		 eph->prn, eph_life, glo_secs, eph_secs, rec_secs);
#endif
	
    if(exh_secs > eph_life) {
		LOGPRINT(nvs, warn, "validate_glo_eph: invalid PRN [%d] and exh_secs [%d]",
			 eph->prn, exh_secs);
            return -1;
    }

	LOGPRINT(nvs, info, "validate_glo_eph: valid PRN[%d] and TTL [%d]", 
                 eph->prn, 
                 (eph_life - ((((int)(eph_secs - glo_secs)) > 0) ? 
                              -exh_secs : exh_secs)));

        return (eph_life - ((((int)(eph_secs - glo_secs)) > 0) ? 
                            -exh_secs : exh_secs));
}

static int validate_glo_alm(void *data)
{
	struct dev_glo_alm *alm = (struct dev_glo_alm*) data;
	unsigned int *valid_alm_age, alm_life, exh_week;

	unsigned int alm_secs = DO_GLO_N4_SECS(alm->N4) + DO_GLO_NA_SECS(alm->NA);
	unsigned int glo_secs = calc_glo_secs();

	sys_param_get(cfg_glo_alm, (void**)&valid_alm_age);
	alm_life = *valid_alm_age;

#ifdef VERBOSE_LOGGING
	LOGPRINT(nvs, info, "validate_glo_alm: PRN [%d] and MAX_ALM_AGE [%d]",
		 alm->prn, alm_life);
	LOGPRINT(nvs, info, "validate_glo_alm: PRN [%d], N4 [%d] and NA [%d]",
		 alm->prn, alm->N4, alm->NA);
	LOGPRINT(nvs, info, "validate_glo_alm: PRN [%d], alm_secs [%d] and glo_secs [%d]",
		 alm->prn, alm_secs, glo_secs);
#endif

        exh_week = abs(glo_secs - alm_secs) / (MAX_SECS_IN_1WEEK);
        
        if(exh_week > alm_life) {
		LOGPRINT(nvs, err, "validate_glo_alm: invalid PRN [%d] and exh_week [%d]",
			 alm->prn, exh_week);
                return -1;
        }

	LOGPRINT(nvs, info, "validate_glo_alm: valid PRN[%d] and TTL [%d]", 
		 alm->prn, 
		 ((alm_life - (((alm_secs - glo_secs) > 0) ? -exh_week : exh_week)) 
		  * MAX_SECS_IN_1WEEK));

	return (alm_life - (((alm_secs - glo_secs) > 0) ? -exh_week : exh_week)) 
		* MAX_SECS_IN_1WEEK;
}

static int validate_glo_svdr(void *data)
{
	UNUSED(data); 
	return 0;
}

static int validate_rec_end(void *data)
{
	UNUSED(data);
	return -1;
}

static int validate_sv_slot(void *data)
{
	UNUSED(data);
	return 0;
}

/* Do not change the order of elements in this table. The order ensures correct
   download of aiding data into the device. 
 */
struct nvs_aid_desc nvs_aid_tbl[ ] = {

	{.record = nvs_glo_sv_slot, .validate_aid = validate_sv_slot},
	{.record = nvs_gps_clk, .validate_aid = validate_gps_time},
	{.record = nvs_gps_pos,  .validate_aid = validate_pos_info},
	{.record = nvs_gps_eph,  .validate_aid = validate_gps_eph},
	{.record = nvs_gps_alm,  .validate_aid = validate_gps_alm},
	{.record = nvs_gps_iono, .validate_aid = validate_gps_iono},
	{.record = nvs_gps_utc,  .validate_aid = validate_gps_utc},
	{.record = nvs_hlth,     .validate_aid = validate_sv_health},
	{.record = nvs_gps_tcxo, .validate_aid = validate_gps_tcxo},
	{.record = nvs_glo_clk,  .validate_aid = validate_glo_time},
	{.record = nvs_glo_eph,  .validate_aid = validate_glo_eph},
	{.record = nvs_glo_alm,  .validate_aid = validate_glo_alm},
	{.record = nvs_glo_svdr, .validate_aid = validate_glo_svdr},
	{.record = nvs_glo_utc,  .validate_aid = validate_glo_utc},
	{.record = nvs_gps_svdr, .validate_aid = validate_gps_svdr},
	{.record = nvs_rec_end,  .validate_aid = validate_rec_end}
};

unsigned int nvs_get_num_aid_elem()
{
	return sizeof(nvs_aid_tbl) / sizeof (struct nvs_aid_desc) - 1;	
}

/* find the record descriptor */
static struct nvs_aid_desc* find_aid_desc(enum nvs_record rec)
{
	struct nvs_aid_desc *aid_desc = nvs_aid_tbl;

	while (aid_desc->record != nvs_rec_end) {
		if (aid_desc->record == rec)
			return aid_desc;

		aid_desc++;
	}

	return NULL;
}

static struct nvs_rec_param* find_rec_info(enum nvs_record rec)
{
	struct nvs_rec_info *rec_desc =  get_rec_info_tbl();

	while (rec_desc->record != nvs_rec_end) {
		if (rec_desc->record == rec)
			return rec_desc->param;

		rec_desc++;
	}

	return NULL;
}

static unsigned int can_fetch_aid(enum nvs_record record)
{
	int ret_val = -1;

	switch(record) {

		case nvs_gps_eph:
		case nvs_gps_alm:
		case nvs_gps_iono:
		case nvs_gps_utc:
		case nvs_gps_pos:
		case nvs_gps_clk:
		case nvs_gps_svdr:
		case nvs_hlth:	                
			ret_val = is_valid_gps_clk();
			break;

		case nvs_gps_tcxo:
			ret_val = 1;
			break;

		case nvs_glo_eph:
		case nvs_glo_alm:
		case nvs_glo_svdr:
		case nvs_glo_clk:
		case nvs_glo_utc:
			ret_val = is_valid_glo_clk();
			break;

		case nvs_glo_sv_slot:
			ret_val = 1;
			break;

		default: 
			ret_val = 0;
	};

	LOGPRINT(nvs, info, "can_fetch_aid: record [%d] %s", 
		 record, (ret_val ? "success" : "failure"));

	return ret_val;
}

static char aid_buffer[2048*32];

static int do_nvs_aid_ttl(enum nvs_record record, struct nvs_aid_status *array,
						  int num)
{
	unsigned int record_type_bitmap = 0xFFFFFFFF;
	struct nvs_aid_desc  *aid_desc  = find_aid_desc(record);
	struct nvs_rec_param *rec_param = find_rec_info(record); 
	if(!aid_desc || !rec_param || !aid_desc->validate_aid)
		return -1;

	memset(aid_buffer, 0, sizeof(aid_buffer));

	if ((record == nvs_glo_alm)||(record == nvs_glo_eph)||(record == nvs_glo_sv_slot)) {
		record_type_bitmap = 0x00FFFFFF;
#ifdef VERBOSE_LOGGING
		LOGPRINT(nvs, info, "do_nvs_aid_ttl: record %d and bitmap: 0x%x", 
			 record, record_type_bitmap );
#endif
	}

	int n_obj = nvs_rec_get(record, aid_buffer, record_type_bitmap);
	int count = 0;
	int obj_idx = 0;

	for(obj_idx = 0; obj_idx < num; obj_idx++) {
		array[obj_idx].ttl_ms = 0xFFFFFFFF;
		if (array[obj_idx].obj_id > rec_param->n_objs)
			array[obj_idx].obj_id = 0xFF;
	}

	for(count = 0; count < n_obj; count++) {

		int rec_sz   = rec_param->rec_sz * count;
		int sv_id    = rec_param->get_oid(aid_buffer + rec_sz);
		if (sv_id < 0)
			continue;

		for(obj_idx = 0; obj_idx < num; obj_idx++) {

			if (sv_id == array[obj_idx].obj_id) {
				int ttl_secs = aid_desc->validate_aid(aid_buffer + rec_sz);
				if (ttl_secs > 0 )
					array[obj_idx].ttl_ms = ttl_secs * 1000;
					break;
			}	
		}	
	}

	return num;
}

int nvs_aid_ttl(enum nvs_record record, struct nvs_aid_status *array, 
				int num)
{
	int ret_val = 0;

	if(!mutex_hnd)	// movie to init function
		mutex_hnd = os_mutex_init();

	os_mutex_take(mutex_hnd);

	ret_val = do_nvs_aid_ttl(record, array, num);

	os_mutex_give(mutex_hnd);

	return ret_val;
}

static int do_aid_get(enum nvs_record record,  char *data, int flags)
{
	int n_obj =  0; /* Num of Objects */
	int o_idx =  0; /* Object index   */
	int n_vld =  0; /* Num Valid objs */
	unsigned int obj_ofs = 0;
	struct nvs_aid_desc  *aid_desc  = find_aid_desc(record);
	struct nvs_rec_param *rec_param = find_rec_info(record);

	if(!can_fetch_aid(record))
		return -1;

	memset(aid_buffer, 0, sizeof(aid_buffer));
	n_obj = nvs_rec_get(record, aid_buffer, flags);

	if((n_obj < 0) || !rec_param || !aid_desc || !aid_desc->validate_aid) {
#ifdef VERBOSE_LOGGING
		LOGPRINT(nvs, err, "do_aid_get: record no [%d] failed", 
			 record);
#endif
		return -1;
	}

	for(o_idx = 0; o_idx < n_obj; o_idx++) {
		unsigned int aid_ofs = o_idx * rec_param->rec_sz;
		if(aid_desc->validate_aid(aid_buffer + aid_ofs) < 0)
			continue;

		memcpy(data + obj_ofs, aid_buffer + aid_ofs, rec_param->rec_sz);
		obj_ofs += rec_param->rec_sz;
		n_vld++;
	}

	if(n_vld && 
	   ((nvs_gps_clk == record) ||
	    (nvs_glo_clk == record)))
		fixup_dev_gnss_time(record, data);

	if(n_vld && (nvs_gps_pos == record))
		fixup_dev_gnss_pos(data);

	LOGPRINT(nvs, info, "do_aid_get: record no [%d] and valid n_objs [%d]", 
		 record, n_vld);

	return n_vld == 0 ? -1 : n_vld;
}

static int _do_aid_get(enum nvs_record record,  char *data, int flags)
{
	int n_obj =  0; /* Num of Objects */
	int o_idx =  0; /* Object index   */
	int n_vld =  0; /* Num Valid objs */
	unsigned int obj_ofs = 0;
	struct nvs_aid_desc  *aid_desc  = find_aid_desc(record);
	struct nvs_rec_param *rec_param = find_rec_info(record);

	if(!can_fetch_aid(record))
		return -1;

	memset(aid_buffer, 0, sizeof(aid_buffer));
	n_obj = nvs_rec_get(record, aid_buffer, flags);

	if((n_obj < 0) || !rec_param || !aid_desc || !aid_desc->validate_aid) {
#ifdef VERBOSE_LOGGING
		LOGPRINT(nvs, err, "do_aid_get: record no [%d] failed", 
			 record);
#endif
		return -1;
	}

	for(o_idx = 0; o_idx < n_obj; o_idx++) {
		unsigned int aid_ofs = o_idx * rec_param->rec_sz;
		if(aid_desc->validate_aid(aid_buffer + aid_ofs) < 0)
			continue;

		memcpy(data + obj_ofs, aid_buffer + aid_ofs, rec_param->rec_sz);
		obj_ofs += rec_param->rec_sz;
		n_vld++;
	}

	LOGPRINT(nvs, info, "do_aid_get: record no [%d] and valid n_objs [%d]", 
		 record, n_vld);

	return n_vld == 0 ? -1 : n_vld;
}

int nvs_aid_get(enum nvs_record record, void *data, int flags)
{
	int ret_val = 0;

	if(!mutex_hnd)
		mutex_hnd = os_mutex_init();

	os_mutex_take(mutex_hnd);

	ret_val = do_aid_get(record, data, flags);

	os_mutex_give(mutex_hnd);

	return ret_val;
}

static int do_fetch_aiding(struct nvs_aid_data *aid_data, int num_aid) 
{
	int aid_idx,n;

	for(aid_idx = 0; aid_idx < num_aid; aid_idx++) {

		enum nvs_record record        = aid_data[aid_idx].record;
		aid_data[aid_idx].n_objs      = 0;

		struct nvs_aid_desc *aid_desc = find_aid_desc(record);

		if(!aid_desc) {
#ifdef VERBOSE_LOGGING
			LOGPRINT(nvs, debug, "do_fetch_aiding: record [%d] not found", record);
#endif
			continue;
		}

		if(!can_fetch_aid(record)) {
#ifdef VERBOSE_LOGGING
			LOGPRINT(nvs, warn, "do_fetch_aiding: record [%d] can_fetch_aid failed", record);
#endif
			break; /* Done with fetching, return whatever we have */
		}

		n = do_aid_get(record, aid_data[aid_idx].buffer, 0xFFFFFFFF);
		if(n < 0) {
#ifdef VERBOSE_LOGGING
			LOGPRINT(nvs, warn, "do_fetch_aiding(): do_aid_get failed for %d", record);
#endif
			aid_data[aid_idx].n_objs = 0;
			continue;
		}

		aid_data[aid_idx].n_objs = n;
#ifdef VERBOSE_LOGGING
		LOGPRINT(nvs, debug, "do_fetch_aiding(): success for %d", record);
#endif
	}

	LOGPRINT(nvs, info, "do_fetch_aiding: returning n_aiding [%d]", aid_idx);

	return aid_idx;
}

static int _do_fetch_aiding(struct nvs_aid_data *aid_data, int num_aid)
{
	int aid_idx,n;

	for(aid_idx = 0; aid_idx < num_aid; aid_idx++) {

		enum nvs_record record        = aid_data[aid_idx].record;
		aid_data[aid_idx].n_objs      = 0;

		struct nvs_aid_desc *aid_desc = find_aid_desc(record);

		if(!aid_desc) {
#ifdef VERBOSE_LOGGING
			LOGPRINT(nvs, debug, "do_fetch_aiding: record [%d] not found", record);
#endif
			continue;
		}

		if(!can_fetch_aid(record)) {
#ifdef VERBOSE_LOGGING
			LOGPRINT(nvs, warn, "do_fetch_aiding: record [%d] can_fetch_aid failed", record);
#endif
			break; /* Done with fetching, return whatever we have */
		}

		n = _do_aid_get(record, aid_data[aid_idx].buffer, 0xFFFFFFFF);
		if(n < 0) {
#ifdef VERBOSE_LOGGING
			LOGPRINT(nvs, warn, "do_fetch_aiding(): do_aid_get failed for %d", record);
#endif
			aid_data[aid_idx].n_objs = 0;
			continue;
		}

		aid_data[aid_idx].n_objs = n;
#ifdef VERBOSE_LOGGING
		LOGPRINT(nvs, info, "do_fetch_aiding(): success for %d", record);
#endif
	}

	LOGPRINT(nvs, info, "do_fetch_aiding: returning n_aiding [%d]", aid_idx);

	return aid_idx;
}

int read_nvs_aiding(struct nvs_aid_data *aid_data, int num_aid, int boot_order)
{
	int ret_val = 0, i;
	if(!mutex_hnd)
		mutex_hnd = os_mutex_init();

	if(boot_order) {
		/* Ensures that NVS is downloaded into device in correct order. */
		for(i = 0; i < num_aid; i++) {
			aid_data[i].record = nvs_aid_tbl[i].record;
		}
	}

	os_mutex_take(mutex_hnd);

	ret_val = do_fetch_aiding(aid_data, num_aid);

	os_mutex_give(mutex_hnd);

	LOGPRINT(nvs, info, "read_nvs_aiding: returning n_aiding [%d]", ret_val);

	return ret_val;
}

int _read_nvs_aiding(struct nvs_aid_data *aid_data, int num_aid, int boot_order)
{
	int ret_val = 0, i;
	if(!mutex_hnd)
		mutex_hnd = os_mutex_init();

	if(boot_order) {
		/* Ensures that NVS is downloaded into device in correct order. */
		for(i = 0; i < num_aid; i++) {
			aid_data[i].record = nvs_aid_tbl[i].record;
		}
	}

	os_mutex_take(mutex_hnd);

	ret_val = _do_fetch_aiding(aid_data, num_aid);

	os_mutex_give(mutex_hnd);

	LOGPRINT(nvs, info, "read_nvs_aiding: returning n_aiding [%d]", ret_val);

	return ret_val;
}

/* Public API */
int nvs_gps_secs(unsigned int *gps_secs)
{
        if(elapsed_gps_secs(NULL) < 0)
                return -1;

        *gps_secs = calc_gps_secs();
        return 0;
}

int nvs_glo_secs(unsigned int *glo_secs)
{
        if(elapsed_glo_secs(NULL) < 0)
                return -1;

        *glo_secs = calc_glo_secs();
        return 0;
}

/* Utils.h */ /* To be used by dev-proxy */
int get_gps_secs(unsigned int *gps_secs)
{
        return nvs_gps_secs(gps_secs);
}

int get_glo_secs(unsigned int *glo_secs)
{
        return nvs_glo_secs(glo_secs);
}
