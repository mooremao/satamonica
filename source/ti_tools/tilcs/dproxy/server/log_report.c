#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "dev2host.h"
#include "os_services.h"
#include "rpc.h"
#include "ai2.h"
#include "logger.h"
#include "sequencer.h"
#include "utils.h"
#include "nvs.h"
#include "gnss.h"
#include "log_report.h"
#include "msr_qualify.h"


void print_data(unsigned int rei, void *data)
{
	switch (rei) {
		case REI_DEV_GPS_ALM:
			{
				struct dev_gps_alm *dev_gps_alm_info = (struct dev_gps_alm *) data;
#ifdef VERBOSE_LOGGING
				LOGPRINT(ai2, info, "gps almanac info:");
				LOGPRINT(ai2, info, "prn                  [%10d]", dev_gps_alm_info->prn);
				LOGPRINT(ai2, info, "Health               [%10d]", dev_gps_alm_info->Health);
				LOGPRINT(ai2, info, "Toa                  [%10d]", dev_gps_alm_info->Toa);
				LOGPRINT(ai2, info, "E                    [%10d]", dev_gps_alm_info->E);
				LOGPRINT(ai2, info, "DeltaI               [%10d]", dev_gps_alm_info->DeltaI);
				LOGPRINT(ai2, info, "OmegaDot             [%10d]", dev_gps_alm_info->OmegaDot);
				LOGPRINT(ai2, info, "SqrtA                [%10d]", dev_gps_alm_info->SqrtA);
				LOGPRINT(ai2, info, "OmegaZero            [%10d]", dev_gps_alm_info->OmegaZero);
				LOGPRINT(ai2, info, "Omega                [%10d]", dev_gps_alm_info->Omega);
				LOGPRINT(ai2, info, "MZero                [%10d]", dev_gps_alm_info->MZero);
				LOGPRINT(ai2, info, "Af0                  [%10d]", dev_gps_alm_info->Af0);
				LOGPRINT(ai2, info, "Af1                  [%10d]", dev_gps_alm_info->Af1);
				LOGPRINT(ai2, info, "Week                 [%10d]", dev_gps_alm_info->Week);
				LOGPRINT(ai2, info, "UpdateWeek           [%10d]", dev_gps_alm_info->UpdateWeek);
				LOGPRINT(ai2, info, "UpdateMS             [%10d]", dev_gps_alm_info->UpdateMS);
#else
				LOGPRINT(ai2, info, "gps almanac ai2 message constructed");
#endif
			}
			break;

		case REI_DEV_GLO_ALM:
			{
				struct dev_glo_alm *dev_glo_alm_info = (struct dev_glo_alm *) data;

#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "glonass almanac info received");
				LOGPRINT(ai2, info, "prn                  [%10d]", dev_glo_alm_info->prn);
				LOGPRINT(ai2, info, "TauC                 [%10d]", dev_glo_alm_info->TauC);
				LOGPRINT(ai2, info, "TauGPS               [%10d]", dev_glo_alm_info->TauGPS);
				LOGPRINT(ai2, info, "N4                   [%10d]", dev_glo_alm_info->N4);
				LOGPRINT(ai2, info, "NA                   [%10d]", dev_glo_alm_info->NA);
				LOGPRINT(ai2, info, "nA                   [%10d]", dev_glo_alm_info->nA);
				LOGPRINT(ai2, info, "HnA                  [%10d]", dev_glo_alm_info->HnA);
				LOGPRINT(ai2, info, "Lambda_nA            [%10d]", dev_glo_alm_info->Lambda_nA);
				LOGPRINT(ai2, info, "t_Lambda_nA          [%10d]", dev_glo_alm_info->t_Lambda_nA);
				LOGPRINT(ai2, info, "Delta_i_nAcorrection [%10d]", dev_glo_alm_info->Delta_i_nAcorrection);
				LOGPRINT(ai2, info, "Delta_T_nArate       [%10d]", dev_glo_alm_info->Delta_T_nArate);
				LOGPRINT(ai2, info, "Varepsilon_nA        [%10d]", dev_glo_alm_info->Varepsilon_nA);
				LOGPRINT(ai2, info, "Omega_nA             [%10d]", dev_glo_alm_info->Omega_nA);
				LOGPRINT(ai2, info, "M_nA                 [%10d]", dev_glo_alm_info->M_nA);
				LOGPRINT(ai2, info, "B1                   [%10d]", dev_glo_alm_info->B1);
				LOGPRINT(ai2, info, "B2                   [%10d]", dev_glo_alm_info->B2);
				LOGPRINT(ai2, info, "Kp                   [%10d]", dev_glo_alm_info->Kp);
				LOGPRINT(ai2, info, "Tau_nA               [%10d]", dev_glo_alm_info->Tau_nA);
				LOGPRINT(ai2, info, "C_nA                 [%10d]", dev_glo_alm_info->C_nA);
#else
				LOGPRINT(ai2, info, "glonass almanac ai2 message constructed");
#endif

			}
			break;

		case REI_DEV_GPS_EPH:
			{
				struct dev_gps_eph *dev_gps_eph_info = (struct dev_gps_eph *) data;

#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "gps ephemeris info:");
				LOGPRINT(ai2, info, "prn                  [%10d]", dev_gps_eph_info->prn);
				LOGPRINT(ai2, info, "Code_on_L2           [%10d]", dev_gps_eph_info->Code_on_L2);
				LOGPRINT(ai2, info, "Accuracy             [%10d]", dev_gps_eph_info->Accuracy);
				LOGPRINT(ai2, info, "Health               [%10d]", dev_gps_eph_info->Health);
				LOGPRINT(ai2, info, "Tgd                  [%10d]", dev_gps_eph_info->Tgd);
				LOGPRINT(ai2, info, "IODC                 [%10d]", dev_gps_eph_info->IODC);
				LOGPRINT(ai2, info, "Toc                  [%10d]", dev_gps_eph_info->Toc);
				LOGPRINT(ai2, info, "Af2                  [%10d]", dev_gps_eph_info->Af2);
				LOGPRINT(ai2, info, "Af1                  [%10d]", dev_gps_eph_info->Af1);
				LOGPRINT(ai2, info, "Af0                  [%10d]", dev_gps_eph_info->Af0);
				LOGPRINT(ai2, info, "IODE                 [%10d]", dev_gps_eph_info->IODE);
				LOGPRINT(ai2, info, "Crs                  [%10d]", dev_gps_eph_info->Crs);
				LOGPRINT(ai2, info, "DeltaN               [%10d]", dev_gps_eph_info->DeltaN);
				LOGPRINT(ai2, info, "Mo                   [%10d]", dev_gps_eph_info->Mo);
				LOGPRINT(ai2, info, "Cuc                  [%10d]", dev_gps_eph_info->Cuc);
				LOGPRINT(ai2, info, "E                    [%10d]", dev_gps_eph_info->E);
				LOGPRINT(ai2, info, "Cus                  [%10d]", dev_gps_eph_info->Cus);
				LOGPRINT(ai2, info, "SqrtA                [%u]", dev_gps_eph_info->SqrtA);
				LOGPRINT(ai2, info, "Toe                  [%10d]", dev_gps_eph_info->Toe);
				LOGPRINT(ai2, info, "Cic                  [%10d]", dev_gps_eph_info->Cic);
				LOGPRINT(ai2, info, "Omega0               [%10d]", dev_gps_eph_info->Omega0);
				LOGPRINT(ai2, info, "Cis                  [%10d]", dev_gps_eph_info->Cis);
				LOGPRINT(ai2, info, "Io                   [%10d]", dev_gps_eph_info->Io);
				LOGPRINT(ai2, info, "Crc                  [%10d]", dev_gps_eph_info->Crc);
				LOGPRINT(ai2, info, "Omega                [%10d]", dev_gps_eph_info->Omega);
				LOGPRINT(ai2, info, "OmegaDot             [%10d]", dev_gps_eph_info->OmegaDot);
				LOGPRINT(ai2, info, "Idot                 [%10d]", dev_gps_eph_info->Idot);
				LOGPRINT(ai2, info, "Week                 [%10d]", dev_gps_eph_info->Week);
				LOGPRINT(ai2, info, "UpdateWeek           [%10d]", dev_gps_eph_info->UpdateWeek);
				LOGPRINT(ai2, info, "UpdateMS             [%10d]", dev_gps_eph_info->UpdateMS);
#else
				LOGPRINT(ai2, info, "gps ephemeris ai2 message constructed");
#endif
				
				break;
			}

		case REI_DEV_GLO_EPH:
			{
				struct dev_glo_eph *dev_glo_eph_info = (struct dev_glo_eph *) data;

#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "glonass ephemeris info:");
				LOGPRINT(ai2, info, "prn                  [%10d]", dev_glo_eph_info->prn);
				LOGPRINT(ai2, info, "m                    [%10d]", dev_glo_eph_info->m);
				LOGPRINT(ai2, info, "Tk                   [%10d]", dev_glo_eph_info->Tk);
				LOGPRINT(ai2, info, "Tb                   [%10d]", dev_glo_eph_info->Tb);
				LOGPRINT(ai2, info, "M                    [%10d]", dev_glo_eph_info->M);
				LOGPRINT(ai2, info, "Epsilon              [%10d]", dev_glo_eph_info->Epsilon);
				LOGPRINT(ai2, info, "Tau                  [%10d]", dev_glo_eph_info->Tau);
				LOGPRINT(ai2, info, "Xc                   [%10d]", dev_glo_eph_info->Xc);
				LOGPRINT(ai2, info, "Yc                   [%10d]", dev_glo_eph_info->Yc);
				LOGPRINT(ai2, info, "Zc                   [%10d]", dev_glo_eph_info->Zc);
				LOGPRINT(ai2, info, "Xv                   [%10d]", dev_glo_eph_info->Xv);
				LOGPRINT(ai2, info, "Yv                   [%10d]", dev_glo_eph_info->Yv);
				LOGPRINT(ai2, info, "Zv                   [%10d]", dev_glo_eph_info->Zv);
				LOGPRINT(ai2, info, "Xa                   [%10d]", dev_glo_eph_info->Xa);
				LOGPRINT(ai2, info, "Ya                   [%10d]", dev_glo_eph_info->Ya);
				LOGPRINT(ai2, info, "Za                   [%10d]", dev_glo_eph_info->Za);
				LOGPRINT(ai2, info, "B                    [%10d]", dev_glo_eph_info->B);
				LOGPRINT(ai2, info, "P                    [%10d]", dev_glo_eph_info->P);
				LOGPRINT(ai2, info, "Nt                   [%10d]", dev_glo_eph_info->Nt);
				LOGPRINT(ai2, info, "Ft                   [%10d]", dev_glo_eph_info->Ft);
				LOGPRINT(ai2, info, "n                    [%10d]", dev_glo_eph_info->n);
				LOGPRINT(ai2, info, "DeltaTau             [%10d]", dev_glo_eph_info->DeltaTau);
				LOGPRINT(ai2, info, "En                   [%10d]", dev_glo_eph_info->En);
				LOGPRINT(ai2, info, "P1                   [%10d]", dev_glo_eph_info->P1);
				LOGPRINT(ai2, info, "P2                   [%10d]", dev_glo_eph_info->P2);
				LOGPRINT(ai2, info, "P3                   [%10d]", dev_glo_eph_info->P3);
				LOGPRINT(ai2, info, "P4                   [%10d]", dev_glo_eph_info->P4);
				LOGPRINT(ai2, info, "ln                   [%10d]", dev_glo_eph_info->ln);
				LOGPRINT(ai2, info, "UpdateWeek           [%10d]", dev_glo_eph_info->UpdateWeek);
				LOGPRINT(ai2, info, "UpdateMS             [%10d]", dev_glo_eph_info->UpdateMS);
#else
				LOGPRINT(ai2, info, "glonass ephemeris ai2 message constructed");
#endif

				
			}
			break;

		case REI_DEV_GPS_ION:
		case REI_DEV_GLO_ION:
			{
				struct dev_gps_ion *dev_gps_ion_info = (struct dev_gps_ion *) data;

#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "ion_info:");
				LOGPRINT(ai2, info, "Alpha0               [%10d]", dev_gps_ion_info->Alpha0);
				LOGPRINT(ai2, info, "Alpha1               [%10d]", dev_gps_ion_info->Alpha1);
				LOGPRINT(ai2, info, "Alpha2               [%10d]", dev_gps_ion_info->Alpha2);
				LOGPRINT(ai2, info, "Alpha3               [%10d]", dev_gps_ion_info->Alpha3);
				LOGPRINT(ai2, info, "Beta0                [%10d]", dev_gps_ion_info->Beta0);
				LOGPRINT(ai2, info, "Beta1                [%10d]", dev_gps_ion_info->Beta1);
				LOGPRINT(ai2, info, "Beta2                [%10d]", dev_gps_ion_info->Beta2);
				LOGPRINT(ai2, info, "Beta3                [%10d]", dev_gps_ion_info->Beta3);
#else
				LOGPRINT(ai2, info, "iono ai2 message constructed");
#endif
				
			}
			break;

		case REI_DEV_GPS_UTC:
		case REI_DEV_GLO_UTC:
			{
				struct dev_gps_utc *dev_gps_utc_info = (struct dev_gps_utc *) data;

#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "utc_info:");
				LOGPRINT(ai2, info, "A0                   [%10d]", dev_gps_utc_info->A0);
				LOGPRINT(ai2, info, "A1                   [%10d]", dev_gps_utc_info->A1);
				LOGPRINT(ai2, info, "DeltaTls             [%10d]", dev_gps_utc_info->DeltaTls);
				LOGPRINT(ai2, info, "Tot                  [%10d]", dev_gps_utc_info->Tot);
				LOGPRINT(ai2, info, "WNt                  [%10d]", dev_gps_utc_info->WNt);
				LOGPRINT(ai2, info, "WNlsf                [%10d]", dev_gps_utc_info->WNlsf);
				LOGPRINT(ai2, info, "DN                   [%10d]", dev_gps_utc_info->DN);
				LOGPRINT(ai2, info, "DeltaTlsf            [%10d]", dev_gps_utc_info->DeltaTlsf);
#else
				LOGPRINT(ai2, info, "utc ai2 message constructed");
#endif
				
			}
			break;

		case REI_DEV_GPS_TIM:
			{
				struct dev_gps_time *dev_tim = (struct dev_gps_time *) data;

				LOGPRINT(ai2, info, "gps_clk_info: Tcount:[%8d] week:[%5d] Msec[%8d] TBias:[%5d] TUnc:[%8d] FBias:[%5d] FUnc:[%8d] isUTC:[%2d] UTC:[%3d]",
							dev_tim->timer_count,
							dev_tim->week_num,
							dev_tim->time_msec,
							dev_tim->time_bias,
							dev_tim->time_unc,
							dev_tim->freq_bias,
							dev_tim->freq_unc,
							dev_tim->is_utc_diff,
							dev_tim->utc_diff);
				
			}
			break;

		case REI_DEV_SV_HLT:
			{
				int i;
				struct dev_sv_hlth *health_info = (struct dev_sv_hlth *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "health_info:");
				LOGPRINT(ai2, info, "updated week         [%10d]", health_info->update_week);
				LOGPRINT(ai2, info, "msec                 [%10d]", health_info->update_msecs);
				LOGPRINT(ai2, info, "n_sat                [%10d]", health_info->num_sat);
				for(i = 0; i < health_info->num_sat; i++)
					LOGPRINT(ai2, info, "health_status[%2d]    [%10d]", i, health_info->health_status[i]);
				break;
#else 
				LOGPRINT(ai2, info, "sv health ai2 message constructed");
#endif
			}

		case REI_DEV_SV_DIR:
			{
				struct dev_sv_dir *sv_dir_info = (struct dev_sv_dir *) data;

				LOGPRINT(ai2, info, "sv_dir_info:prn:[%10d] elevation:[%10d] azimuth:[%10d]",
							sv_dir_info->prn,
							sv_dir_info->elevation,
							sv_dir_info->azimuth);

			}
			break;

		case REI_DEV_VERSION:
			{
				struct dev_version *dev_ver = (struct dev_version *) data;
				char nm_buf[128];
				char *env_val;
				FILE *pat_ver_fd;
				char patch_version[128];
#define				PATCH_VER_FILE_PATH 			"/mnt/userdata/gnss/"

				LOGPRINT(ai2, info, "dev_ver_info:");
				LOGPRINT(ai2, info, "chip_id_major        [%d]", dev_ver->chip_id_major);
				LOGPRINT(ai2, info, "chip_id_minor        [%d]", dev_ver->chip_id_minor);
				LOGPRINT(ai2, info, "rom_id_major         [%d]", dev_ver->rom_id_major);
				LOGPRINT(ai2, info, "rom_id_minor         [%d]", dev_ver->rom_id_minor);
				LOGPRINT(ai2, info, "rom_id_sub_minor1    [%d]", dev_ver->rom_id_sub_minor1);
				LOGPRINT(ai2, info, "rom_id_sub_minor2    [%d]", dev_ver->rom_id_sub_minor2);
				LOGPRINT(ai2, info, "rom_day              [%d]", dev_ver->rom_day);
				LOGPRINT(ai2, info, "rom_month            [%d]", dev_ver->rom_month);
				LOGPRINT(ai2, info, "rom_year             [%d]", dev_ver->rom_year);
				LOGPRINT(ai2, info, "patch_major          [%d]", dev_ver->patch_major);
				LOGPRINT(ai2, info, "patch_minor          [%d]", dev_ver->patch_minor);
				LOGPRINT(ai2, info, "patch_day            [%d]", dev_ver->patch_day);
				LOGPRINT(ai2, info, "patch_month          [%d]", dev_ver->patch_month);
				LOGPRINT(ai2, info, "patch_year           [%d]", dev_ver->patch_year);
				snprintf(nm_buf, 128, "%s%s", PATCH_VER_FILE_PATH, "gnss-patch-version");
				pat_ver_fd = fopen(nm_buf,"wb+");
				if(pat_ver_fd == NULL)
					return -1;

				sprintf(patch_version,"Patch Version : %d.%d\tDate YYYY-MM-DD : %d-%d-%d\r\n",  
					dev_ver->patch_minor, dev_ver->patch_major, dev_ver->patch_year, dev_ver->patch_month,dev_ver->patch_day);      
				
				if(fwrite(patch_version, sizeof(char),strlen(patch_version) + 1, pat_ver_fd) ==0)
					return -1;
				fclose(pat_ver_fd);				
}
			break;

		case REI_DEV_STATUS:
			{
					struct dev_gps_status *dev_stat = (struct dev_gps_status*) data;
					
					LOGPRINT(ai2, info, "dev_Status_info:");
					LOGPRINT(ai2, info, "chip_id_major		  [%d]", dev_stat->gps_ver->chip_id_major);
					LOGPRINT(ai2, info, "chip_id_minor		  [%d]", dev_stat->gps_ver->chip_id_minor);
					LOGPRINT(ai2, info, "rom_id_major		  [%d]", dev_stat->gps_ver->rom_id_major);
					LOGPRINT(ai2, info, "rom_id_minor		  [%d]", dev_stat->gps_ver->rom_id_minor);
					LOGPRINT(ai2, info, "rom_id_sub_minor1	  [%d]", dev_stat->gps_ver->rom_id_sub_minor1);
					LOGPRINT(ai2, info, "rom_id_sub_minor2	  [%d]", dev_stat->gps_ver->rom_id_sub_minor2);
					LOGPRINT(ai2, info, "rom_day			  [%d]", dev_stat->gps_ver->rom_day);
					LOGPRINT(ai2, info, "rom_month			  [%d]", dev_stat->gps_ver->rom_month);
					LOGPRINT(ai2, info, "rom_year			  [%d]", dev_stat->gps_ver->rom_year);
					LOGPRINT(ai2, info, "patch_major		  [%d]", dev_stat->gps_ver->patch_major);
					LOGPRINT(ai2, info, "patch_minor		  [%d]", dev_stat->gps_ver->patch_minor);
					LOGPRINT(ai2, info, "patch_day			  [%d]", dev_stat->gps_ver->patch_day);
					LOGPRINT(ai2, info, "patch_month		  [%d]", dev_stat->gps_ver->patch_month);
					LOGPRINT(ai2, info, "patch_year 		  [%d]", dev_stat->gps_ver->patch_year);
			}
			break;
		case REI_DEV_GPS_POS:
		case REI_DEV_GLO_POS:
			{
				struct dev_pos_info *dev_pos = (struct dev_pos_info *) data;
				dev_pos->ell = BUF_OFFSET_ST(dev_pos, struct dev_pos_info,
											 struct dev_ellp_unc);
				dev_pos->utc = BUF_OFFSET_ST(dev_pos->ell, struct dev_ellp_unc,
											 struct dev_utc_info);

#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "pos_report:");
				LOGPRINT(ai2, info, "pos_flags            [%10d]", dev_pos->pos_flags);
				LOGPRINT(ai2, info, "lat_rad              [%10d]", dev_pos->lat_rad);
				LOGPRINT(ai2, info, "lon_rad              [%10d]", dev_pos->lon_rad);
				LOGPRINT(ai2, info, "alt_wgs84            [%10d]", dev_pos->alt_wgs84);
				LOGPRINT(ai2, info, "has_msl              [%10d]", dev_pos->has_msl);
				LOGPRINT(ai2, info, "alt_msl              [%10d]", dev_pos->alt_msl);
				LOGPRINT(ai2, info, "unc_east             [%10d]", dev_pos->unc_east);
				LOGPRINT(ai2, info, "unc_north            [%10d]", dev_pos->unc_north);
				LOGPRINT(ai2, info, "unc_vertical         [%10d]", dev_pos->unc_vertical);
				LOGPRINT(ai2, info, "orientation          [%10d]", dev_pos->ell->orientation);
				LOGPRINT(ai2, info, "semi_major           [%10d]", dev_pos->ell->semi_major);
				LOGPRINT(ai2, info, "semi_minor           [%10d]", dev_pos->ell->semi_minor);
				LOGPRINT(ai2, info, "hours                [%10d]", dev_pos->utc->hours);
				LOGPRINT(ai2, info, "minutes              [%10d]", dev_pos->utc->minutes);
				LOGPRINT(ai2, info, "seconds              [%10d]", dev_pos->utc->seconds);
				LOGPRINT(ai2, info, "sec_1by10            [%10d]", dev_pos->utc->sec_1by10);
#else
				LOGPRINT(pos_rpt, info, "pos_report: Flag:[%4d] Lat:[%10d] Lon:[%10d]",
							dev_pos->pos_flags,
							dev_pos->lat_rad,
							dev_pos->lon_rad);
#endif

			}
			break;

		case REI_REF_POS:
			{
				struct loc_gnss_info *loc_info = (struct loc_gnss_info *) data;
				struct location_desc *loc      = &loc_info->location;

#ifdef VERBOSE_LOGGING
				
				LOGPRINT(ai2, info, "fix_type             [%10d]\n", loc_info->fix_type);
				LOGPRINT(ai2, info, "gnss_tid             [%10d]\n", loc_info->gnss_tid);
				LOGPRINT(ai2, info, "ref_secs             [%10d]\n", loc_info->ref_secs);
				LOGPRINT(ai2, info, "ref_msec             [%10d]\n", loc_info->ref_msec);				
				LOGPRINT(ai2, info, "pos_bits             [%10d]\n", loc_info->pos_bits);
				LOGPRINT(ai2, info, "shape                [%10d]\n", loc->shape);
				LOGPRINT(ai2, info, "lat_sign             [%10d]\n", loc->lat_sign);
				LOGPRINT(ai2, info, "latitude             [%10d]\n", loc->latitude_N);
				LOGPRINT(ai2, info, "longitude            [%10d]\n", loc->longitude_N);
				LOGPRINT(ai2, info, "alt_dir              [%10d]\n", loc->alt_dir);
				LOGPRINT(ai2, info, "altitude             [%10d]\n", loc->altitude_N);
				LOGPRINT(ai2, info, "unc_semi_major       [%10d]\n", loc->unc_semi_maj_K);
				LOGPRINT(ai2, info, "unc_semi_minor       [%10d]\n", loc->unc_semi_min_K);
				LOGPRINT(ai2, info, "orientation          [%10d]\n", loc->orientation);
				LOGPRINT(ai2, info, "unc_altitude         [%10d]\n", loc->unc_altitude_K);
				LOGPRINT(ai2, info, "confidence           [%10d]\n", loc->confidence);
#else
				LOGPRINT(pos_rpt, info, "lat:[%10d] lon:[%10d]", 
								loc->latitude_N,
								loc->longitude_N);

#endif
			}
			break;

		case REI_DEV_POS_STAT:
			{
				struct dev_pos_stat *stat = (struct dev_pos_stat *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "pos_status_report:");
				LOGPRINT(ai2, info, "status               [%10d]", stat->status);
				LOGPRINT(ai2, info, "sigma                [%10d]", stat->sigma);
				LOGPRINT(ai2, info, "PDOP                 [%10d]", stat->PDOP);
				LOGPRINT(ai2, info, "HDOP                 [%10d]", stat->HDOP);
				LOGPRINT(ai2, info, "VDOP                 [%10d]", stat->VDOP);
				LOGPRINT(ai2, info, "fix_status           [%10d]", stat->fix_status);
#endif
			}
			break;

		case REI_DEV_ALM_STAT:
			{
				struct dev_alm_stat *stat = (struct dev_alm_stat *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "alm_status_report:");
				LOGPRINT(ai2, info, "prn                  [%10d]", stat->prn);
				LOGPRINT(ai2, info, "Toa                  [%10d]", stat->toa);
				LOGPRINT(ai2, info, "Week                 [%10d]", stat->week);
#endif
			}
			break;

		case REI_DEV_EPH_STAT:
			{
				struct dev_eph_stat *stat = (struct dev_eph_stat *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "eph_stat_report:");
				LOGPRINT(ai2, info, "prn                  [%10d]", stat->prn);
				LOGPRINT(ai2, info, "iode                 [%10d]", stat->iode);
				LOGPRINT(ai2, info, "toe                  [%10d]", stat->toe);
				LOGPRINT(ai2, info, "week                 [%10d]", stat->week);
#endif
			}
			break;

		case REI_DEV_VEL_INF:
			{
				struct dev_vel_info *dev_vel = (struct dev_vel_info *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "vel_info:");
				LOGPRINT(ai2, info, "east                 [%10d]", dev_vel->east);
				LOGPRINT(ai2, info, "north                [%10d]", dev_vel->north);
				LOGPRINT(ai2, info, "vertical             [%10d]", dev_vel->vertical);
				LOGPRINT(ai2, info, "uncetainty           [%10d]", dev_vel->uncertainty);
#endif
			}
			break;

		case REI_DEV_DOP_INF:
			{
				struct dev_dop_info *dev_dop = (struct dev_dop_info *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "dop_info:");
				LOGPRINT(ai2, info, "position             [%10d]", dev_dop->position);
				LOGPRINT(ai2, info, "horizontal           [%10d]", dev_dop->horizontal);
				LOGPRINT(ai2, info, "vertical             [%10d]", dev_dop->vertical);
				LOGPRINT(ai2, info, "time                 [%10d]", dev_dop->time);
#endif
			}
			break;

		case REI_DEV_HDG_INF:
			{
				struct dev_hdg_info *dev_hdg = (struct dev_hdg_info *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "hdg_info:");
				LOGPRINT(ai2, info, "truthful             [%10d]", dev_hdg->truthful);
				LOGPRINT(ai2, info, "magnetic             [%10d]", dev_hdg->magnetic);
#endif
			}
			break;

		case REI_DEV_SAT_INF:
			{
				struct dev_sat_info *dev_sat = (struct dev_sat_info *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "sat_info:");
				LOGPRINT(ai2, info, "gnss_id              [%10d]", dev_sat->gnss_id);
				LOGPRINT(ai2, info, "svid                 [%10d]", dev_sat->svid);
				LOGPRINT(ai2, info, "iode                 [%10d]", dev_sat->iode);
				LOGPRINT(ai2, info, "msr_residual         [%10d]", dev_sat->msr_residual);
				LOGPRINT(ai2, info, "weigh_in_fix         [%10d]", dev_sat->weigh_in_fix);
#endif
			}
			break;

		case REI_DEV_MSR_GPS:
			{
				struct dev_msr_info_PVT *dev_msr = (struct dev_msr_info_PVT *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "dev_msr_info:");
				LOGPRINT(ai2, info, "svid				  [%10d]", dev_msr->svid);
				LOGPRINT(ai2, info, "gnss_id			  [%10d]", dev_msr->gnss_id);
				LOGPRINT(ai2, info, "snr				  [%10d]", dev_msr->snr);
				LOGPRINT(ai2, info, "cno				  [%10d]", dev_msr->cno);
				LOGPRINT(ai2, info, "latency_ms 		  [%10d]", dev_msr->latency_ms);
				LOGPRINT(ai2, info, "msec				  [%10d]", dev_msr->msec);
				LOGPRINT(ai2, info, "sub_msec			  [%10d]", dev_msr->sub_msec);
				LOGPRINT(ai2, info, "time_unc			  [%10d]", dev_msr->time_unc);
				LOGPRINT(ai2, info, "speed				  [%10d]", dev_msr->speed);
				LOGPRINT(ai2, info, "speed_unc			  [%10d]", dev_msr->speed_unc);
				LOGPRINT(ai2, info, "elevation			  [%10d]", dev_msr->elevation);
				LOGPRINT(ai2, info, "azimuth			  [%10d]", dev_msr->azimuth);
#else
				LOGPRINT(msr_rpt, info, "[%3d] [%3d]",dev_msr->svid, dev_msr->cno);
		
#endif

			}
			break;

		case REI_MSR_GPS:
			{
				struct gps_msr_info *msr = (struct gps_msr_info *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "gps_msr_info:");
				LOGPRINT(ai2, info, "svid                 [%10d]", msr->svid);
				LOGPRINT(ai2, info, "cno                  [%10d]", msr->c_no);
				LOGPRINT(ai2, info, "doppler              [%10d]", msr->doppler);
#endif
			}
			break;

		case REI_MSR_GLO_SG1:
		case REI_MSR_GLO_SG2:
			{
				struct glo_msr_info *msr_info = (struct glo_msr_info *) data;
				struct ganss_msr_info *msr    = &msr_info->msr;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "glo_msr_info:");
				LOGPRINT(ai2, info, "contents             [%10d]", msr->contents);
				LOGPRINT(ai2, info, "svid                 [%10d]", msr->svid);
				LOGPRINT(ai2, info, "c_no                 [%10d]", msr->c_no);
				LOGPRINT(ai2, info, "mpath_det            [%10d]", msr->mpath_det);
				LOGPRINT(ai2, info, "carrier_qual         [%10d]", msr->carrier_qual);
				LOGPRINT(ai2, info, "code_phase           [%10d]", msr->code_phase);
				LOGPRINT(ai2, info, "integer_cp           [%10d]", msr->integer_cp);
				LOGPRINT(ai2, info, "cp_rms_err           [%10d]", msr->cp_rms_err);
				LOGPRINT(ai2, info, "doppler              [%10d]", msr->doppler);
				LOGPRINT(ai2, info, "adr                  [%10d]", msr->adr);
#endif

			}
			break;

		case REI_DEV_GLO_TIM:
			{
				struct dev_glo_time *dev_tim = (struct dev_glo_time *) data;
#ifdef VERBOSE_LOGGING

				LOGPRINT(ai2, info, "glo_clk_info:");
				LOGPRINT(ai2, info, "time_day             [%10d]", dev_tim->time_day);
				LOGPRINT(ai2, info, "time_month           [%10d]", dev_tim->time_month);
				LOGPRINT(ai2, info, "time_year            [%10d]", dev_tim->time_year);
				LOGPRINT(ai2, info, "time_msec            [%10d]", dev_tim->time_msec);
				LOGPRINT(ai2, info, "time_bias            [%10d]", dev_tim->time_bias);
				LOGPRINT(ai2, info, "time_unc             [%10d]", dev_tim->time_unc);
				LOGPRINT(ai2, info, "utc_diff             [%10d]", dev_tim->utc_diff);
				LOGPRINT(ai2, info, "freq_bias            [%10d]", dev_tim->freq_bias);
				LOGPRINT(ai2, info, "freq_unc             [%10d]", dev_tim->freq_unc);
				LOGPRINT(ai2, info, "ggto                 [%10d]", dev_tim->ggto);
				LOGPRINT(ai2, info, "time_status          [%10d]", dev_tim->time_status);
#endif
			}
			break;

		default:
			break;
	}
}

