/*
 * pa_blank.c
 *
 * Power Amplifier blanking
 *
 * management of PA blanking feature in receiver
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include "host2dev.h"
#include "rpc.h"

int h2d_pa_blank_command(struct srv_rei *srv_re, struct ie_desc *ie_adu);