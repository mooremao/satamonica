/*
 * sequencer.h
 *
 * Sequencer functionality for device dialogues
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

/** @file sequencer.h
    This file describes the interface for basic sequencing and synchronisation.

    @note
*/
/*	:
	1. Rename sequencer to something else to avoid confusion.
	2. Data transfer concept.
*/

#ifndef _DPROXY_SEQUENCER_H_
#define _DPROXY_SEQUENCER_H_

#define SEQ_OK 0
#define SEQ_ERR -1

enum seq_id {
	ready,
	wakeup,
	dsleep,
	active,
	deact,
	reports,
	inj_time,
	calib_single_shot,
	calib_periodic,
	calib_undo,
/* add for GNSS recovery ++*/	
	protocol_select,
	/* error trigger test code, to be removed */
	fatal_err_select,
	exception_err_select,
/* add for GNSS recovery --*/		
	/* Add new commands here. */
	inc_clk_unc,
	/*Adding new for cleanup*/
	cleanup,
	ping,
	meas_rep_recovery,
	no_resp_recovery,
	hard_reset_recovery,
	null
};

enum seq_action {
	send,
	recv,
	nop
};

typedef int (*seq_fn)(void *data);

struct seq_step {
	int id;
	enum seq_action action;
	seq_fn trigger;
};

/* Initialize the sequencer module. */
int seq_init(void);

/* Wind up the sequencer module. */
int seq_deinit(void);

/* Reset the sequence. */
int seq_reset();

/* Take out the current sequence. Intenal sequence pointer will be moved to next
   sequence. If no further sequence available, function returns -1. */
int seq_pop_step(struct seq_step *step);

/* Push a new step dynamically inside. Intenal sequence pointer will point to
   this new step. Next pop call will return this new sequence. */
int seq_push_step(struct seq_step *step);

/* Loads and executes the sequence of device transaction. */
int seq_execute(enum seq_id seq_id, void *data);

/* Blocks the caller till device sends out a packet with id mentioned here. */
int seq_wait_for_event(int id, void **data);
int seq_wait_for_timed_event(int id, void **data);

/* Returns SEQ_OK, if a caller is waiting for this id. Else SEQ_ERR is returned.
   This means the 'id' has to be handled by the caller. No other takers. */
int seq_post_event(void *data);


#endif
