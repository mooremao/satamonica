/*
 * pa_blank.c
 *
 * Power Amplifier blanking
 *
 * management of PA blanking feature in receiver
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include "ai2.h"
#include "pa_blank.h"
#include "host2dev.h"
#include "dev_proxy.h"
#include "rpc.h"
#include "logger.h"
#include "utils.h"
#include "config.h"

extern int h2d_ai2_hdl;

/*
* Control 
*	1 - enable
*	0 - disable
*
* Polatity
*	1 - Active high
*	0 - Active low
*
*/
int h2d_pa_blank_ctrl_inj(unsigned char control, unsigned char polority){
#define AI2_PA_BLANK_PKT_SIZE 5
	struct ai2_qop_cfg {
		unsigned int id;
		unsigned int len;
		unsigned int pa_blank;
		unsigned int polarity;
	} pa[1];
	char ai2_pa_blank_sizes[] = {1, 2, 1, 1, 0};
	unsigned char mem[AI2_PA_BLANK_PKT_SIZE];

	pa->id = 0xE1;
	pa->len = 2;

	pa->pa_blank = control;
	pa->polarity = polority;

	d2h_ai2_encode((void *)pa, mem, ai2_pa_blank_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(generic, info, "PA blank control injected");
			return OK;
		}
	}

	LOGPRINT(generic, err, "error sending PA blank control ai2 packet to device");
	return ERR;
}

int h2d_pa_blank_command(struct srv_rei *srv_re, struct ie_desc *ie_adu){
	struct dev_pa_blank *in = (struct dev_pa_blank *)ie_adu->data;
	unsigned char pa_blank_ctrl;
	unsigned char *polarity = 0; // change this so that it is read from the config file
	
	sys_param_get(cfg_pa_blank_polarity,  (void**) &polarity);

	LOGPRINT(generic, info, "polarity = %d",*polarity);
	
	if (in->control == PA_BLANK_ENABLE){
		LOGPRINT(generic, info, "PA Blanking is being enabled");
		pa_blank_ctrl = 1;
	}
	else if (in->control == PA_BLANK_DISABLE){
		LOGPRINT(generic, info, "PA Blanking is being disabled");
		pa_blank_ctrl = 0;
	}
	else{
		LOGPRINT(generic, err, "PA Blanking: Invalid argument, AI2 packet is not injected");
		return ERR;
	}

	return h2d_pa_blank_ctrl_inj(pa_blank_ctrl, *polarity);

}
