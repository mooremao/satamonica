/*
 * clock_report.c
 *
 * Clock report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ai2.h"
#include "gnss.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "dev_proxy.h"
#include "logger.h"
#include "nvs.h"
#include "nvs_utils.h"
#include "log_report.h"
#include "os_services.h"
#include "config.h"

extern int h2d_ai2_hdl;

int clk_gps_rpt_create_clk_ie(struct rpc_msg *rmsg, unsigned short oper_id,
							  struct dev_gps_time *dev_clk, 
							  unsigned int rei_select);

int clk_glo_rpt_create_clk_ie(struct rpc_msg *rmsg, unsigned short oper_id,
							  struct dev_glo_time *dev_clk, 
							  unsigned int rei_select);

static struct ie_desc *mk_ie_adu_4clk_dev_gps(struct rpc_msg *rmsg,
											  unsigned short oper_id, 
											  struct dev_gps_time *dev_clk)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_gps_time);
	struct dev_gps_time *n_clk = malloc(st_size);
	if(!n_clk)
		goto mk_adu_4clk_dev_exit1;

	memcpy(n_clk, dev_clk, st_size);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GPS_TIM, oper_id, st_size, n_clk);
	if(!ie_adu)
		goto mk_adu_4clk_dev_exit2;

	return ie_adu;

mk_adu_4clk_dev_exit2:
	free(n_clk);

mk_adu_4clk_dev_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4clk_gps(struct rpc_msg *rmsg,
										  unsigned short oper_id, 
										  struct dev_gps_time *dev_clk)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(gps_time);
	struct gps_time *gps_tim = malloc(st_size);
	if(!gps_tim)
		goto mk_adu_4clk_gps_exit1;

	gps_tim->tow_unit = milli_secs;
	gps_tim->n_wk_ro  = dev_clk->week_num / 1024;
	gps_tim->week_nr  = dev_clk->week_num % 1024;
	gps_tim->tow      = dev_clk->time_msec;
	
	ie_adu = rpc_ie_attach(rmsg, REI_GPS_TIM, oper_id, st_size, gps_tim);
	if (!ie_adu)
		goto mk_adu_4clk_gps_exit2;

	return ie_adu;

mk_adu_4clk_gps_exit2:
	free(gps_tim);

mk_adu_4clk_gps_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4clk_ref_gps(struct rpc_msg *rmsg,
											  unsigned short oper_id, 
											  struct dev_gps_time *dev_clk)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(gps_ref_time);
	struct gps_ref_time *ref_tim = malloc(st_size);
	struct gps_time *gps_tim;
	float time_unc;
	unsigned short mantissa;
	unsigned char exponent;
#define LIGHT_MSEC              (double)(299792458.0/1000.0)
#define C_SPEED_LIGHT_NSEC (LIGHT_MSEC * 0.001f * 0.001f)

	if(!ref_tim)
		goto mk_adu_4clk_gps_exit1;

	ref_tim->accuracy = fine;
	//time_unc = (((float)dev_clk->time_unc) / (0.0022f * 1000.0f)) + 1.0f;
	//ref_tim->time_unc = (unsigned int) (logf(time_unc) / logf(1.18f));
	mantissa = dev_clk->time_unc >> 5;
	exponent = dev_clk->time_unc & 0x001F;
	ref_tim->time_unc  = (float)(os_service_ldexp((double)(mantissa), (double)(exponent)) / 1000.0f);//TimeUnc in microsec
	ref_tim->freq_unc = dev_clk->freq_unc * 0.1f;

	ref_tim->n_atow   = 0;

	gps_tim           = &ref_tim->time;
	gps_tim->tow_unit = milli_secs;
	gps_tim->n_wk_ro  = dev_clk->week_num / 1024;
	gps_tim->week_nr  = dev_clk->week_num % 1024;
	gps_tim->tow      = dev_clk->time_msec;
	gps_tim->freq_unc = dev_clk->freq_unc * 0.1f;

	if (oper_id == OPER_INF)
		ie_adu = rpc_ie_attach(rmsg, REI_GPS_REF_TIM, oper_id, st_size, ref_tim);
	else
		ie_adu = rpc_ie_attach(rmsg, REI_GPS_TIM, oper_id, st_size, ref_tim);
	if (!ie_adu)
		goto mk_adu_4clk_gps_exit2;

	return ie_adu;

mk_adu_4clk_gps_exit2:
	free(ref_tim);

mk_adu_4clk_gps_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4clk_dev_glo(struct rpc_msg *rmsg,
											  unsigned short oper_id, 
											  struct dev_glo_time *dev_clk)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_glo_time);
	struct dev_glo_time *n_clk = malloc(st_size);
	if(!n_clk)
		goto mk_adu_4clk_dev_exit1;

	memcpy(n_clk, dev_clk, st_size);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GLO_TIM, oper_id, st_size, n_clk);
	if(!ie_adu)
		goto mk_adu_4clk_dev_exit2;

	return ie_adu;

mk_adu_4clk_dev_exit2:
	free(n_clk);

mk_adu_4clk_dev_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4clk_glo(struct rpc_msg *rmsg,
										  unsigned short oper_id, 
										  struct dev_glo_time *dev_clk)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(ganss_ref_time);
	struct ganss_ref_time *ref_tim = malloc(st_size);
	unsigned int glo_secs = do_glo_secs(dev_clk);
	if(!ref_tim)
		goto mk_adu_4clk_glo_exit1;

	ref_tim->tid      = glo_time_id;
	ref_tim->day      = (glo_secs / (60 * 60 * 24)) % 8192;
	ref_tim->day 	 += 1; //Days are 1 based
	ref_tim->tod      = (dev_clk->time_msec / 1000);
	ref_tim->tod_unc  = dev_clk->time_unc;
	ref_tim->accuracy = coarse;

	ie_adu = rpc_ie_attach(rmsg, REI_GLO_TIM, oper_id, st_size, ref_tim);
	if (!ie_adu)
		goto mk_adu_4clk_glo_exit2;

	return ie_adu;

mk_adu_4clk_glo_exit2:
	free(ref_tim);

mk_adu_4clk_glo_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4clk_ref_glo(struct rpc_msg *rmsg,
											  unsigned short oper_id, 
											  struct dev_glo_time *dev_clk)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(ganss_ref_time);
	struct ganss_ref_time *ref_tim = malloc(st_size);
	unsigned int glo_secs = do_glo_secs(dev_clk);
	if(!ref_tim)
		goto mk_adu_4clk_glo_exit1;

	ref_tim->tid      = glo_time_id;
	ref_tim->day      = (glo_secs / (60 * 60 * 24)) % 8192;
	ref_tim->day 	 += 1; //Days are 1 based
	ref_tim->tod      = (dev_clk->time_msec / 1000);
	ref_tim->tod_unc  = dev_clk->time_unc;
	ref_tim->accuracy = coarse;

	if (oper_id == OPER_INF)
		ie_adu = rpc_ie_attach(rmsg, REI_GLO_REF_TIM, oper_id, st_size, ref_tim);
	else
		ie_adu = rpc_ie_attach(rmsg, REI_GLO_TIM, oper_id, st_size, ref_tim);
	if (!ie_adu)
		goto mk_adu_4clk_glo_exit2;

	return ie_adu;

mk_adu_4clk_glo_exit2:
	free(ref_tim);

mk_adu_4clk_glo_exit1:
	return NULL;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int clk_gps_create_clk_ie(struct rpc_msg *rmsg, unsigned short oper_id,
								 struct dev_gps_time *gps_tim, 
								 unsigned rei_select)
{
	struct ie_desc *ie_adu[3];
	int n_adu = 0;

	if(rei_select & POS_RPT_TIM_GPS_NATIVE) {
		ie_adu[n_adu] = mk_ie_adu_4clk_dev_gps(rmsg, oper_id, gps_tim);
		if(!ie_adu[n_adu++])
			goto clk_ie_exit1;
	}

	if(rei_select & POS_RPT_TIM_GPS_IE_SEL) {
		ie_adu[n_adu] = mk_ie_adu_4clk_gps(rmsg, oper_id, gps_tim);
		if(!ie_adu[n_adu++])
			goto clk_ie_exit1;
	}

	ie_adu[n_adu] = mk_ie_adu_4clk_ref_gps(rmsg, oper_id, gps_tim);
	if(!ie_adu[n_adu++])
		goto clk_ie_exit1;

	return n_adu;

clk_ie_exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
static int clk_glo_create_clk_ie(struct rpc_msg *rmsg, unsigned short oper_id,
								 struct dev_glo_time *glo_tim, 
								 unsigned rei_select)
{
	struct ie_desc *ie_adu[3];
	int n_adu = 0;

	if(rei_select & POS_RPT_TIM_GLO_NATIVE) {
		ie_adu[n_adu] = mk_ie_adu_4clk_dev_glo(rmsg, oper_id, glo_tim);
		if(!ie_adu[n_adu++])
			goto clk_ie_exit1;
	}

	if(rei_select & POS_RPT_TIM_GLO_IE_SEL) {
		ie_adu[n_adu] = mk_ie_adu_4clk_glo(rmsg, oper_id, glo_tim);
		if(!ie_adu[n_adu++])
			goto clk_ie_exit1;
	}

	ie_adu[n_adu] = mk_ie_adu_4clk_ref_glo(rmsg, oper_id, glo_tim);
	if(!ie_adu[n_adu++])
		goto clk_ie_exit1;

	return n_adu;

clk_ie_exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int clk_gps_rpt_create_clk_ie(struct rpc_msg *rmsg, unsigned short oper_id,
							  struct dev_gps_time *dev_clk, 
							  unsigned int rei_select)
{
	return clk_gps_create_clk_ie(rmsg, oper_id, dev_clk, rei_select);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int clk_glo_rpt_create_clk_ie(struct rpc_msg *rmsg, unsigned short oper_id,
							  struct dev_glo_time *dev_clk, 
							  unsigned int rei_select)
{
	return clk_glo_create_clk_ie(rmsg, oper_id, dev_clk, rei_select);
}

int clk_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  unsigned int rei_select)
{
	struct dev_gps_time *gps_tim = (struct dev_gps_time *) ai2_d2h->ai2_struct;
	struct dev_glo_time *glo_tim = BUF_OFFSET_ST(gps_tim, struct dev_gps_time,
												 struct dev_glo_time);

	if (d2h_get_msg_id(ai2_d2h) == 0x05) {
		if(-1 == clk_gps_create_clk_ie(rmsg, OPER_INF, gps_tim, rei_select))
			return -1;
	} else {
		if(-1 == clk_glo_create_clk_ie(rmsg, OPER_INF, glo_tim, rei_select))
			return -1;
	}

	return 0;
}

static int dev_gps_tim_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_time *gps_tim = (struct dev_gps_time *) ai2_d2h->ai2_struct;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;
	unsigned short                 time_unc;
	gps_tim->timer_count         = BUF_LE_U4B_RD_INC(ai2_pkt);
	gps_tim->week_num            = BUF_LE_U2B_RD_INC(ai2_pkt);
	gps_tim->time_msec           = BUF_LE_U4B_RD_INC(ai2_pkt);
	gps_tim->time_bias           = BUF_LE_S3B_RD_INC(ai2_pkt);
	gps_tim->time_unc            = BUF_LE_U2B_RD_INC(ai2_pkt);
	gps_tim->is_utc_diff         = 1;
	gps_tim->utc_diff            = BUF_LE_S1B_RD_INC(ai2_pkt);
	gps_tim->freq_bias           = BUF_LE_S4B_RD_INC(ai2_pkt);
	gps_tim->freq_unc            = BUF_LE_U4B_RD_INC(ai2_pkt);

	print_data(REI_DEV_GPS_TIM, gps_tim);

	if (gps_tim->week_num != 65535) {
	if (nvs_success != nvs_rec_add(nvs_gps_clk, gps_tim, 1)) {
		LOGPRINT(tim_rpt, err, "error storing clock report in nvs");
		return -1;
		}
	}
	return (ai2_pkt - ai2_ref);
}

static int dev_glo_tim_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_time *gps_tim = (struct dev_gps_time *) ai2_d2h->ai2_struct;
	struct dev_glo_time *glo_tim = BUF_OFFSET_ST(gps_tim, struct dev_gps_time,
												 struct dev_glo_time);
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;
	unsigned short                 time_unc;
	unsigned short                 mantissa;
	unsigned char                  exponent;

	glo_tim->timer_count         = BUF_LE_U4B_RD_INC(ai2_pkt);
	glo_tim->time_month          = BUF_LE_U1B_RD_INC(ai2_pkt);
	glo_tim->time_day            = BUF_LE_U1B_RD_INC(ai2_pkt);
	glo_tim->time_year           = BUF_LE_U2B_RD_INC(ai2_pkt);
	glo_tim->time_msec           = BUF_LE_U4B_RD_INC(ai2_pkt);
	glo_tim->time_bias           = BUF_LE_S3B_RD_INC(ai2_pkt);
	time_unc                     = BUF_LE_U2B_RD_INC(ai2_pkt);
	mantissa                     = (unsigned short)(time_unc >> 5);
	exponent                     = (unsigned char) (time_unc & 0x001F);
	glo_tim->time_unc            = (unsigned short)
		(os_service_ldexp((double) mantissa, (unsigned int) exponent) 
		 * 0.000001f);
	glo_tim->utc_diff            = BUF_LE_S1B_RD_INC(ai2_pkt);
	glo_tim->freq_bias           = BUF_LE_S4B_RD_INC(ai2_pkt);
	glo_tim->freq_unc            = BUF_LE_U4B_RD_INC(ai2_pkt);
	glo_tim->ggto                = BUF_LE_S2B_RD_INC(ai2_pkt);
	glo_tim->time_status         = BUF_LE_U1B_RD_INC(ai2_pkt);

	print_data(REI_DEV_GLO_TIM, glo_tim);

	return (ai2_pkt - ai2_ref);
}

static int clk_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if (d2h_get_msg_id(ai2_d2h) == 0x05) {
		if(ai2_offset < ai2_pkt_sz)
			ai2_offset += dev_gps_tim_struct(ai2_d2h, ai2_offset);
	} else {
		if(ai2_offset < ai2_pkt_sz)
			ai2_offset += dev_glo_tim_struct(ai2_d2h, ai2_offset);
	}

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int qualify_clk(struct d2h_ai2 *ai2_d2h)
{
#define GLO_CLK_GGTO_VALID 0x01
#define GLO_CLK_DTMS_VALID 0x02

	struct dev_gps_time *gps_tim = (struct dev_gps_time *) ai2_d2h->ai2_struct;
	struct dev_glo_time *glo_tim = BUF_OFFSET_ST(gps_tim, struct dev_gps_time,
							 struct dev_glo_time);

	if (d2h_get_msg_id(ai2_d2h) == 0x05) {
		if (gps_tim->week_num != 65535) {
			LOGPRINT(tim_rpt, info, "gps_clk_report passed");
			return 0;
		} else
			LOGPRINT(tim_rpt, info, "gps_clk_report dropped");
	} else {
		if ((glo_tim->time_status & GLO_CLK_DTMS_VALID) &&
                    (glo_tim->time_month > 0) &&
                    (glo_tim->time_year > 0)) {
			LOGPRINT(nvs, info, "glo_clk_report passed");
			return 0;
		} else
			LOGPRINT(nvs, info, "glo_clk_report dropped S:0x%X M:%d Y:%d",
                                             glo_tim->time_status,
                                             glo_tim->time_month,
                                             glo_tim->time_year);
	}

	return -1;
}

#define CLK_RPT_ST_LEN ST_SIZE(dev_gps_time) + ST_SIZE(dev_glo_time)

static int clk_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0;
	struct dev_gps_time *gps_tim = (struct dev_gps_time *) ai2_d2h->ai2_struct;
	struct dev_glo_time *glo_tim = BUF_OFFSET_ST(gps_tim, struct dev_gps_time,
												 struct dev_glo_time);

	LOGPRINT(tim_rpt, info, "storing clock report in nvs");

	if (d2h_get_msg_id(ai2_d2h) == 0x05)
		wr_suc = nvs_rec_add(nvs_gps_clk, gps_tim, 1);
	else
		wr_suc = nvs_rec_add(nvs_glo_clk, glo_tim, 1);

	if (wr_suc != nvs_success) {
		LOGPRINT(tim_rpt, err, "error storing clock report in nvs");
		return -1;
	}

	return 0;
}

struct d2h_ai2 d2h_clk_rpt = {

	.ai2_st_len = CLK_RPT_ST_LEN,

	.gen_struct = clk_struct,
	.do_ie_4rpc = clk_rpc_ie,
	.nvs_action = clk_nvs_info,
	.assess_qoi = qualify_clk
};

static struct dev_gps_time first_clk_rpt[1];

int h2d_first_clk_rpt(void *data)
{
	char  *ai2_pkt               = (char*) data;

	ai2_pkt += 3;

	first_clk_rpt->timer_count         = BUF_LE_U4B_RD_INC(ai2_pkt);
	first_clk_rpt->week_num            = BUF_LE_U2B_RD_INC(ai2_pkt);
	first_clk_rpt->time_msec           = BUF_LE_U4B_RD_INC(ai2_pkt);
	first_clk_rpt->time_bias           = BUF_LE_S3B_RD_INC(ai2_pkt);
	first_clk_rpt->time_unc            = BUF_LE_U2B_RD_INC(ai2_pkt);
	first_clk_rpt->is_utc_diff         = 1;
	first_clk_rpt->utc_diff            = BUF_LE_S1B_RD_INC(ai2_pkt);
	first_clk_rpt->freq_bias           = BUF_LE_S4B_RD_INC(ai2_pkt);
	first_clk_rpt->freq_unc            = BUF_LE_U4B_RD_INC(ai2_pkt);

	nvs_rec_get(nvs_gps_clk, first_clk_rpt, 0x01);

	LOGPRINT(nvs, info, "h2d_first_clk_rpt: timer_count %d, week_num %d, time_msec %d and time_unc %d", 
		 first_clk_rpt->timer_count, 
		 first_clk_rpt->week_num, 
		 first_clk_rpt->time_msec, 
		 first_clk_rpt->time_bias);

	return 0;
}

/* Function to request a clock report from the device */
int h2d_inj_gps_tim_req(void *data)
{
	struct ai2_inj_gps_time_req {
		unsigned int id;
		unsigned int len;
	} tim_req[1];
	char ai2_inj_gps_time_req_sizes[] = {1, 2, 0};
	unsigned char mem[3];

	tim_req->id = 0x09;
	
	tim_req->len = 0;

	d2h_ai2_encode((void *)tim_req, mem, ai2_inj_gps_time_req_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(tim_inj, info, "gps time req injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(tim_inj, err, "error sending ai2 packet to device");
	return ERR;
}

unsigned short convt_m11e5(float flt);

int del_assist(enum nvs_record rec_nvs, unsigned short elem_id,
               struct dev_aid_del *del_aid, int act);

int h2d_inj_del_gps_time(void *data)
{
#define AI2_DEL_TIME 4

        int ret = ERR;
        struct dev_aid_del aid_del = {0xFFFFFFFF, 0xFFFFFFFF};

        nvs_rec_get(nvs_gps_clk, first_clk_rpt, 0x01);

        if(!del_assist(nvs_gps_clk, REI_GPS_TIM, &aid_del, AI2_DEL_TIME))
                ret = OK;

        UNUSED(data);

        return ret;
}

int h2d_inj_first_clk_rpt(void *data)
{
	int ret = ERR;
	struct dev_gps_time *dev = NULL;
	float unc;
	unsigned int *time_unc;

	LOGPRINT(tim_inj, info, "h2d_inj_first_clk_rpt");

	/* Get some memory. */
	dev = malloc(sizeof(struct dev_gps_time));
	if (NULL == dev) {
		LOGPRINT(tim_inj, err, "malloc failed");
		return ERR;
	}

	memcpy(dev, first_clk_rpt, sizeof(struct dev_gps_time));

	// Retrive from config file
	sys_param_get(cfg_time_unc, (void**) &time_unc);

	unc = (float)(*time_unc);

	dev->time_unc = convt_m11e5(unc * 1e9 );

	if (dev->week_num != 65535 && dev->week_num != 0)
		ret = h2d_inj_inc_gps_time_unc(dev);
	else
		ret = OK;

	free(dev);

	UNUSED(data);

	return ret;
}
