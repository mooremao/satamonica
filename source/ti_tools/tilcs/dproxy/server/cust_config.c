/*
 * cust_config.c
 *
 * Client API procedures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
*/


#include "dev_proxy.h"
#include "inject.h"
#include "ai2.h"
#include "logger.h"
#include "config.h"
#include "utils.h"

extern int h2d_ai2_hdl;


int dpxy_inj_lna_ctrl(void *data)
{
	struct ai2_rtc_inj_ctrl {
		unsigned int id;
		unsigned int len;
		unsigned int sub_pkt_id;
		unsigned int lna_config;
	} lna[1];
	char ai2_lna_ctrl_sizes[] = {1, 2, 1, 1, 0};
	unsigned char mem[5];
	unsigned int *inj_ctrl;

	lna->id = 0xFB;
	lna->len = 2;
	lna->sub_pkt_id = 0x09;

	sys_param_get(cfg_lnaext_cryt,(void**) &inj_ctrl);
	lna->lna_config = 0x01; //default TCXO connected
	
	if (*inj_ctrl)
		lna->lna_config |= 0x02;

	d2h_ai2_encode((void *)lna, mem, ai2_lna_ctrl_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "LNA control injected: cfg_lnaext_ctrl [%d]: inj [0x%X]",
						*inj_ctrl, lna->lna_config);
			return OK;
		}
	}

	UNUSED(data);
	LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}

