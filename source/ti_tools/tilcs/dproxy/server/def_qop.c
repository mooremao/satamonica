

/*
 * def_qop.c
 *
 * Injecting default QOP and PDOP masks.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "dev_proxy.h"
#include "inject.h"
#include "ai2.h"
#include "logger.h"
#include "config.h"
#include "sequencer.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "os_services.h"
#include "nvs.h"
#include "nvs_validate.h"
#include "nvs_utils.h"

extern int h2d_ai2_hdl;


#define COLD_START 1
#define WARM_START 2
#define HOT_START  3

#define MIN_SV_VISIBLE  6
#define MIN_TTL			60
#define VALID 			1
#define INVALID 		0

struct ai2_qop_cfg {
		unsigned int id;
		unsigned int len;
		unsigned int sub_pkt_id;
		unsigned int reserved1;
		unsigned int t1;
		unsigned int a1;
		unsigned int t2;
		unsigned int a2;
		unsigned int a3;
		unsigned int pdop_mask;
		unsigned int reserved2;
		unsigned int reserved3;
		unsigned int reserved4;
};

int dpxy_inj_default_qop_params(void *data)
{
	struct ai2_qop_cfg qop[1];
	struct ai2_qop_cfg *pqop;
	char ai2_qop_cfg_sizes[] = {1, 2, 1, 1, 2, 2, 2, 2, 2, 1, 1, 4, 4, 0};
	
	unsigned char mem[25];
	unsigned int delay = ((unsigned int) os_time_secs() - get_rx_on_time()) * 1000;
	unsigned int *a3;
	unsigned int *pdop_mask;

	qop->id = 0xFB;
	qop->len = 0x16;
	qop->sub_pkt_id = 0x15;
	qop->reserved1 = 0;

	pqop = (struct ai2_qop_cfg*)data;

	/* 0.5 sec/bit encoding.*/
	qop->t1 = pqop->t1*2;
	qop->t2 = pqop->t2*2;

	qop->a1 = pqop->a1;
	qop->a2 = pqop->a2;

	// Get the A3 and PDOP from config file
	sys_param_get(cfg_single_pe_a3,(void**) &a3);
	sys_param_get(cfg_single_pe_pdop_mask,(void**) &pdop_mask);
	
	qop->a3 		= *a3;
	qop->pdop_mask  = *pdop_mask;
	
	qop->reserved2 = 0;
	qop->reserved3 = 0;
	qop->reserved4 = 0;

	LOGPRINT(qop_parm, info, "Default QOPs t1[%d] ,t2[%d], a1[%d], a2[%d] ,a3[%d] ,pdop[%d] ",
						qop->t1/2,qop->t2/2,qop->a1,qop->a2,qop->a3,qop->pdop_mask);

	d2h_ai2_encode((void *)qop, mem, ai2_qop_cfg_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 25)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(qop_parm, info, "qop control injected");
			return OK;
		}
	}

	LOGPRINT(qop_parm, err, "error sending ai2 packet to device");

	UNUSED(data);

	return ERR;
}

void get_def_qops(struct ai2_qop_cfg *pqop, int mode)
{
	unsigned int* val;
	switch(mode)
		{
		case COLD_START:
			sys_param_get(cold_t1,   (void**) &val);
			pqop->t1 = *val;

			sys_param_get(cold_t2,   (void**) &val);
			pqop->t2 = *val;

			sys_param_get(cold_a1,	 (void**) &val);
			pqop->a1 = *val;
			
			sys_param_get(cold_a2,	 (void**) &val);
			pqop->a2 = *val;

			LOGPRINT(qop_parm, info, "COLD a1 %d, a2 %d, t1 %d, t2 %d,",
						pqop->a1, pqop->a2, pqop->t1, pqop->t2);
			break;

		case WARM_START:
			sys_param_get(warm_t1,	 (void**) &val);
			pqop->t1 = *val;
			
			sys_param_get(warm_t2,	 (void**) &val);
			pqop->t2 = *val;
			
			sys_param_get(warm_a1,	 (void**) &val);
			pqop->a1 = *val;
			
			sys_param_get(warm_a2,	 (void**) &val);
			pqop->a2 = *val;

			LOGPRINT(qop_parm, info, "WARM a1 %d, a2 %d, t1 %d, t2 %d,",
						pqop->a1, pqop->a2, pqop->t1, pqop->t2);
			
			break;

		case HOT_START:
			sys_param_get(hot_t1,	 (void**) &val);
			pqop->t1 = *val;
			
			sys_param_get(hot_t2,	 (void**) &val);
			pqop->t2 = *val;
			
			sys_param_get(hot_a1,   (void**) &val);
			pqop->a1 = *val;
			
			sys_param_get(hot_a2,   (void**) &val);
			pqop->a2 = *val;

			LOGPRINT(qop_parm, info, "HOT a1 %d, a2 %d, t1 %d, t2 %d,",
						pqop->a1, pqop->a2, pqop->t1, pqop->t2);

			break;
		}

		
}

int is_gps_time_valid()
{
	static struct dev_gps_time gps_time;
	
	nvs_rec_get(nvs_gps_clk, &gps_time, 1);
	//print_data(REI_DEV_GPS_TIM, &gps_time);

	LOGPRINT(nvs, info, "gps_clk_info to inject QOP: Tcount:[%8d] week:[%5d] Msec[%8d] TBias:[%5d] TUnc:[%8d] FBias:[%5d] FUnc:[%8d] isUTC:[%2d] UTC:[%3d]",
				gps_time.timer_count,
				gps_time.week_num,
				gps_time.time_msec,
				gps_time.time_bias,
				gps_time.time_unc,
				gps_time.freq_bias,
				gps_time.freq_unc,
				gps_time.is_utc_diff,
				gps_time.utc_diff);

	unsigned short mantissa = (unsigned short)(gps_time.time_unc >> 5);	
	unsigned char exponent = (unsigned char)(gps_time.time_unc & 0x001F);
	float temp_flt = (float)ldexp(mantissa,exponent);
	if( (temp_flt * 0.000000001f ) < 30 )	
		return VALID;
	else
		return INVALID;	
}

int is_eph_valid()
{
	static struct nvs_aid_status status[32];
	int count,index,valid_eph = 0;

	memset(status, 0, 32 * sizeof(struct nvs_aid_status));

	for (count = 0; count < 32; count++)
		status[count].obj_id = count + 1;
	
	count = get_assist_status(a_GPS_EPH, status);

	for (index = 0; index < count; index++) {	

		if (status[index].ttl_ms > 0 ) 
				valid_eph++;
	}

	LOGPRINT(qop_parm, info, "QOP: Valid EPH Count: %d",  valid_eph);
	
	if(valid_eph >= MIN_SV_VISIBLE)
		return VALID;
	else
		return INVALID;

}

int is_alm_valid()
{
	static struct nvs_aid_status status[32];
	int index,alm_cnt,valid_alm = 0 , count;

	for (count = 0; count < 32; count++)
		status[count].obj_id = count + 1;
	
	memset(status, 0, 32 * sizeof(struct nvs_aid_status));
	alm_cnt  = get_assist_status(a_GPS_ALM, status);
	for (index = 0; index < alm_cnt; index++) 
	{		
		//LOGPRINT(qop_parm,info,"QOP: Alm TTL MS: %d", status[index].ttl_ms);
		if( status[index].ttl_ms > 0 ) 
			valid_alm++;
	}
	LOGPRINT(qop_parm, info, "QOP: Valid ALM Count: %d",  valid_alm);
	if(valid_alm >= 24)
		return VALID;
	else
		return INVALID;
		
}

int is_pos_valid()
{
	static struct nvs_aid_status status[32];
	int pos_cnt,pos_secs,valid_pos;

	memset(status, 0, 32 * sizeof(struct nvs_aid_status));
	pos_cnt  = get_assist_status(a_REF_POS, status);
	pos_secs = status[0].ttl_ms/1000;

	if(pos_cnt == -1)
		return INVALID;
	else
		return VALID;
}

int getmode()
{		
	int valid_eph = 0,valid_pos = 0,valid_time = 0,valid_alm = 0;

	valid_time = is_gps_time_valid();
	
	if( valid_time )
	{
		valid_eph = is_eph_valid();

		valid_alm = is_alm_valid();		
		
		valid_pos = is_pos_valid();
	}

	LOGPRINT(qop_parm, info, "QOP: EPH [%d], POS [%d], TIME [%d], ALM [%d]", 
						 valid_eph, valid_pos, valid_time, valid_alm);

	if(valid_time == INVALID)
	{
		LOGPRINT(qop_parm, info,"QOP:Its COLD START");
		return COLD_START;
	}
	else if((valid_eph == VALID) && (valid_pos == VALID)) 
	{
		LOGPRINT(qop_parm, info,"QOP:Its HOT START");
		return HOT_START;
	}
	else if((valid_pos == VALID) && ( valid_alm == VALID ) ) 
	{
		LOGPRINT(qop_parm, info,"QOP: Its WARM START");
		return WARM_START;
	}
	else
	{
		LOGPRINT(qop_parm, info,"QOP: Its Default COLD START");
		return COLD_START;
	}
	
}


int h2d_dproxy_sleep(void *data)
{
	LOGPRINT(qop_parm, info,"h2d_dproxy_sleep: Start: %u", os_time_msecs());
	sleep(1);
	LOGPRINT(qop_parm, info,"h2d_dproxy_sleep: Stop: %u", os_time_msecs());
	return 0;
}

int dpxy_inj_def_qop(void *data)
{
	int mode;
	struct ai2_qop_cfg qop;
	mode = getmode();

	LOGPRINT(qop_parm, info, "dpxy_inj_def_qop(): Mode [%d]", mode);

	get_def_qops(&qop, mode);

	return (dpxy_inj_default_qop_params(&qop) );
}

int dpxy_inj_def_EE_qop(void)
{
        struct ai2_qop_cfg qop, *pqop;
        unsigned int* val;

        pqop = &qop;

        sys_param_get(ee_t1,   (void**) &val);
        pqop->t1 = *val;

        sys_param_get(ee_t2,   (void**) &val);
        pqop->t2 = *val;

        sys_param_get(ee_a1,   (void**) &val);
        pqop->a1 = *val;

        sys_param_get(ee_a2,   (void**) &val);
        pqop->a2 = *val;

        LOGPRINT(qop_parm, info, "EE QOP a1 %d, a2 %d, t1 %d, t2 %d,",
        pqop->a1, pqop->a2, pqop->t1, pqop->t2);

        dpxy_inj_default_qop_params(pqop);

        return 1;
}

