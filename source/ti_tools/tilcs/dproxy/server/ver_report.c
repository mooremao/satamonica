/*
 * ver_report.c
 *
 * Version report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "log_report.h"

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int ver_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
						struct dev_version *dev_ver);

static struct ie_desc *mk_ie_adu_4ver_dev(struct rpc_msg *rmsg,
			unsigned short oper_id, struct dev_version *dev_ver)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_version);
	struct dev_version *new_ver = (struct dev_version *)malloc(st_size);
	if(!new_ver)
		goto exit1;

	memcpy(new_ver, dev_ver, st_size);

	ie_adu = rpc_ie_attach(rmsg, oper_id, OPER_INF, st_size, new_ver);
	if(!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_ver);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int ver_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
						struct dev_version *dev_ver)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4ver_dev(rmsg, oper_id, dev_ver);
	if(!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int ver_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
						struct dev_version *dev_ver)
{
	return ver_create_dev_ie(rmsg, oper_id, dev_ver);
}

static int ver_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
							unsigned int rei_select)
{
	struct dev_version *dev_ver = (struct dev_version *)ai2_d2h->ai2_struct;

	UNUSED(rei_select);

	if(-1 == ver_create_dev_ie(rmsg, REI_DEV_VERSION, dev_ver))
		return -1;

	return 0;
}

static int dev_ver_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_version *dev_ver  = (struct dev_version *)ai2_d2h->ai2_struct;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;
	unsigned char chip_id;

	chip_id                      = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->chip_id_major       = (chip_id >> 4);
	dev_ver->chip_id_minor       = (chip_id & 0x0F);

	/* Ignore system config */
	ai2_pkt++;

	dev_ver->rom_id_sub_minor2   = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->rom_id_sub_minor1   = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->rom_id_minor		 = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->rom_id_major		 = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->rom_month           = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->rom_day             = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->rom_year            = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_ver->patch_major         = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->patch_minor         = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->patch_month         = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->patch_day           = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_ver->patch_year          = BUF_LE_U2B_RD_INC(ai2_pkt);

	print_data(REI_DEV_VERSION, dev_ver);

	return (ai2_pkt - ai2_ref);
}

static int ver_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_ver_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

#define EXT_PVT_ST_LEN ST_SIZE(dev_version)

struct d2h_ai2 d2h_dev_version = {

	.ai2_st_len = EXT_PVT_ST_LEN,
	.gen_struct = ver_ext_struct,
	.do_ie_4rpc = ver_rpc_ie
};

