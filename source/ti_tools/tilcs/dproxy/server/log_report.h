/*
 * log_report.h
 *
 * Debug print of any data requested. Accumulation code.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/


#ifndef _LOG_REPORT_H_
#define _LOG_REPORT_H_

void print_data(unsigned int rei, void *data);

#endif /* _LOG_REPORT_H_ */

