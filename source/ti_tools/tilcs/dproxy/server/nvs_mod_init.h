#ifndef _NVS_MOD_INIT_H_
#define _NVS_MOD_INIT_H_

#include "nvs.h"

struct nvs_rec_info {

	enum nvs_record       record;
	struct nvs_rec_param *param;
};

/** Get the record info table for the modules to 
      intialise all the records.
  @return struct nvs_rec_info*
 */
struct nvs_rec_info* get_rec_info_tbl(void);
void nvs_display_all(void);

#endif