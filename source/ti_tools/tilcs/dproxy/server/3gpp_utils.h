/*
 * 3gpp_utils.h
 *
 *
 * Copyright (C) {2012} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#ifndef _3GPP_UTILS_H_
#define _3GPP_UTILS_H_

#include <math.h>
#include <stdlib.h>

#include "gnss.h"
#include "gnss_utils.h"

/* Refer to section 6.2 and 6.4 of 3GPP spec 23.032 for details about evaluating
   the number K to specify the uncertainity (in meters). In general terms,
   unc (in meters) = C((1 + x)^K -1) wherer C and x are specified constants.

   Thus, K = (unsigned char)(log10(unc_m / C) + 1f) / log10 (1f + x))
 */
#define UNC_MAP_TO_K(unc_m, C, x)  \
			(unsigned char)(log10(((unc_m*1.0) / (C*1.0)) + 1.0) / log10(1.0 + x))

/* Place holder for GLO EPH ICD information */
struct glo_eph_info {

	unsigned int    flags;         /* Implementation specific */
	unsigned char   n_sat;         /* Number of satellites    */ 

	struct glo_eph  eph[MAX_SAT];  /* ICD Ephemeris Data      */  
};

/* Place holder for GLO ALM ICD informaiton */
struct glo_alm_info {

	unsigned int    flags;         /* Implementation specific */
	unsigned char   n_sat;         /* Number of satellites    */

	struct glo_alm  alm[MAX_SAT];  /* ICD Alamanac Data       */
};

/* Include Navigation Model information from Network into GLO EPH ICD info
 * 
 * [inout] eph_info: EPH ICD Place holder
 * [in]    utc_secs: UTC at function call
 * [in]    nav_mdl:  Array of 3GPP GLO Navigation Model
 * [in]    num_nav:  Number of elements in NAV array
 *
 * Returns 0 on success otherwise -1 on error
 */
int add2glo_icd_eph(struct glo_eph_info* eph_info, unsigned int utc_secs,
					const struct glo_nav_model* nav_mdl, unsigned char num_nav);

/* Does place holder has all required information? 
 * 
 * [inout]  eph_info: EPH ICD Place holder
 * 
 *
 * Returns true or false
 */
unsigned int is_glo_icd_eph(const struct glo_eph_info* eph_info);

/* Include 3GPP ALM Keplerien Model from Network into GLO ALM ICD info
 * 
 * [inout] alm_info: ALM ICD Place holder
 * [in]    utc_secs: UTC at function call
 * [in]    kp_set:   Array of 3GPP GLO ALM Model (KP)
 * [in]    num_set:  Number of elements in ALM array (KP)
 *
 * Returns 0 on success otherwise -1 on error
 */
int add_kp2glo_icd_alm(struct glo_alm_info* alm_info, unsigned int utc_secs,
					   const struct glo_alm_kp_set *kp_set, unsigned char num_set);

/* Include 3GPP GLO UTC Model from Network into GLO ALM ICD info
 * 
 * [inout] alm_info: ALM ICD Place holder
 * [in]    utc_mdl:  UTC Mdl from network
 *
 * Returns 0 on success otherwise -1 on error
 */
int add_utc2glo_icd_alm(struct glo_alm_info* alm_info, 
						const struct glo_utc_model* utc_mdl);

/* Include 3GPP GLO UTC Model from Network into GLO ALM ICD info
 * 
 * [inout] eph_info: ALM ICD Place holder
 * [in]    time_mdl: UTC Model from network
 *
 * Returns 0 on success otherwise -1 on error
 */
int add2glo_icd_alm(struct glo_alm_info* alm_info, 
					const struct ganss_ref_time* glo_time,
					const struct ganss_time_model* time_mdl);

/* Does place holder has all required information? 
 * 
 * [inout]  alm_info: ALM ICD Place holder
 * 
 *
 * Returns true or false
 */
unsigned int is_glo_icd_alm(const struct glo_alm_info* alm_info);

/* Reset GLO ICD information place holders. 
 *
 * Must be called before start and after completion of every 
 * 3GPP-to-ICD translation
 * 
 * [inout]  eph_info: EPH ICD Place holder
 * [inout]  alm_info: ALM ICD Place holder
 * 
 *
 */
void reset_glo_icd_data(struct glo_eph_info* eph_info, 
						struct glo_alm_info* alm_info);

int add_gps2glo_icd_alm(struct glo_alm_info* alm_info, 
						const struct ganss_ref_time* glo_time, 
						const struct ganss_time_model* time_mdl);

static inline float alt_unc_3gpp_KtoM(unsigned char K)
{
	return 45*(pow((1 + 0.025), K) - 1);
}

static inline unsigned char alt_unc_3gpp_MtoK(float unc)
{
	return ceil(log((unc / 45) + 1) / log(1 + 0.025));
}

static inline float hor_unc_3gpp_KtoM(unsigned char K)
{
	return 10*(pow((1 + 0.100), K) - 1);
}

static inline unsigned char hor_unc_3gpp_MtoK(float unc)
{
	return ceil(log((unc / 10) + 1) / log(1 + 0.100));
}

static inline float lat_3gpp_NtoDeg(unsigned int N)
{
	return (N*90)  / pow(2, 23);
}

static inline unsigned int lat_3gpp_DegtoN(float deg)
{
	return ceil((abs(deg) / 90) * pow(2, 23));
}

static inline int lon_3gpp_DegtoN(float deg)
{
	return ceil((deg / 180) * pow(2, 24));
}

static inline float lon_3gpp_NtoDeg(int N)
{
	return (N*360) / pow(2, 24);
}

#endif

