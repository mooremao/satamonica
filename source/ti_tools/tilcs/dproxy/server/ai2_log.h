/*
 * ai2_log.h
 *
 * Dumps the data to and from GNSS device as seen by the serial interface.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _AI2_LOG_H_
#define _AI2_LOG_H_

#define MAX_FILE_SIZE 150

#define AI2_ASCII_BUF_LIMIT 2048
#define AI2_LOG_MODE_ASCII     0x00000001
#define AI2_LOG_MODE_TIMESTAMP 0x00000002
#define AI2_LOG_ENABLE 8

struct ai2_log_cfg {
	char log_name[MAX_FILE_SIZE];
	unsigned int modes;
};

enum ai2_dir {
	ai2_d2h,
	ai2_h2d
};

int ai2_log_init(struct ai2_log_cfg *);
void ai2_log_exit(void);
void ai2_log_reset(void);
int ai2_log_pkt_ascii(enum ai2_dir dir, char *pkt_name, void *ai2_pkt,
                       int pkt_len);
int ai2_log_bin(enum ai2_dir dir, void *ai2_buf, unsigned int ai2_len);

#endif
