/*
 * ai2_test.c
 *
 * Unit testing for AI2 module
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdio.h>
#include "ai2.h"

/*
 * : Move this to test definitions to configuration file.
 */
#define AI2_UNIT_TEST_NO 100

typedef void (*ai2_tst_action)(void);

struct ai2_unit_test {
	ai2_tst_action setup;
	ai2_tst_action test;
	ai2_tst_action cleanup;
};

static struct ai2_unit_test ai2_test_suite[AI2_UNIT_TEST_NO];
static unsigned int ai2_test_cnt;

/* 1. Inject AI2 communication protocol. */
void w01_ai2_comm_protocol(void)
{
        unsigned char wmsg1[4] = {0xF5,0x01,0x00,0x01};
        unsigned char wmsg2[4] = {0xF1,0x01,0x00,0x05};
        unsigned char rmsg[AI2_READ_BUF_SIZE];
	int hdl;
	int ret = ai2_ok;

	hdl = ai2_open(ai2_op_write);
	if (hdl < ai2_ok ) {
		printf ("Error: ai2_open() returned %d\n", hdl);
	}

        ret = ai2_write(hdl, wmsg1, 4);
        if (ret != ai2_ok)
                printf ("Error: ai2_write returned %d\n", ret);
        ret = ai2_flush(hdl);
        if (ret != ai2_ok)
                printf ("Error: ai2_flush returned %d\n", ret);
/*
 *         ret = ai2_write(hdl, wmsg2, 4);
 *         if (ret != ai2_ok)
 *                 printf ("Error: ai2_write returned %d\n", ret);
 *         ret = ai2_flush(hdl);
 *         if (ret != ai2_ok)
 *                 printf ("Error: ai2_flush returned %d\n", ret);
 */

        hdl = ai2_open(ai2_op_read);
        ret = ai2_read(hdl, rmsg, AI2_READ_BUF_SIZE);
        if (ret <= ai2_ok)
                printf ("Error: ai2_read returned %d\n", ret);
        else
                printf("Pass: Read %d bytes\n", ret);

	ai2_close(hdl);
}
/* 2. Request version report. */
void w02_ai2_req_version(void)
{
        unsigned char msg[3] = {0xF0, 0x00, 0x00};
	int hdl;
	int ret = ai2_ok;

	hdl = ai2_open(ai2_op_write);
	if (hdl < ai2_ok ) {
		printf ("Error: ai2_open() returned %d\n", hdl);
	}

        ret = ai2_write(hdl, msg, 3);
        if (ret != ai2_ok)
                printf ("Error: ai2_write returned %d\n", ret);
        ai2_flush(hdl);

	ai2_close(hdl);
}
/* 3. Read test 1 - Proper Packet. */
void r01_ai2_read_full_pkt(void)
{
	unsigned char msg[AI2_READ_BUF_SIZE];
	int hdl;
	int ret = ai2_ok;

	hdl = ai2_open(ai2_op_read);
	if (hdl < ai2_ok ) {
		printf ("Error: ai2_open() returned %d\n", hdl);
	}

        ret = ai2_read(hdl, msg, AI2_READ_BUF_SIZE);
        if (ret <= ai2_ok)
                printf ("Error: ai2_read returned %d\n", ret);
        else
                printf("Pass: Read %d bytes\n", ret);

	ai2_close(hdl);
}
/* 4. Read test 2 - Double Packet. */
void r02_ai2_read_dbl_pkt(void)
{
        unsigned char msg[AI2_READ_BUF_SIZE];
	int hdl;
	int ret = ai2_ok;

	hdl = ai2_open(ai2_op_read);
	if (hdl < ai2_ok ) {
		printf ("Error: ai2_open() returned %d\n", hdl);
	}

        ret = ai2_read(hdl, msg, AI2_READ_BUF_SIZE);
        if (ret <= ai2_ok)
                printf ("Error: ai2_read returned %d\n", ret);

        ret = ai2_read(hdl, msg, AI2_READ_BUF_SIZE);
        if (ret <= ai2_ok)
                printf ("Error: ai2_read returned %d\n", ret);
        else
                printf("Pass: Read %d bytes\n", ret);

	ai2_close(hdl);
}

void load_test_cases(void)
{
	ai2_test_suite[ai2_test_cnt].setup = 0;
        ai2_test_suite[ai2_test_cnt].test = w01_ai2_comm_protocol;
	ai2_test_suite[ai2_test_cnt].cleanup = 0;
	ai2_test_cnt++;

	ai2_test_suite[ai2_test_cnt].setup = 0;
        ai2_test_suite[ai2_test_cnt].test = w02_ai2_req_version;
	ai2_test_suite[ai2_test_cnt].cleanup = 0;
	ai2_test_cnt++;

	ai2_test_suite[ai2_test_cnt].setup = 0;
        ai2_test_suite[ai2_test_cnt].test = r01_ai2_read_full_pkt;
	ai2_test_suite[ai2_test_cnt].cleanup = 0;
	ai2_test_cnt++;

	ai2_test_suite[ai2_test_cnt].setup = 0;
        ai2_test_suite[ai2_test_cnt].test = r02_ai2_read_dbl_pkt;
	ai2_test_suite[ai2_test_cnt].cleanup = 0;
	ai2_test_cnt++;
}

int main()
{
	/* Load test cases. */
        load_test_cases();

        /* : Implement test order mechanism. */
        ai2_test_suite[0].test();

	return 0;
}
