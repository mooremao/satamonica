/*
 * msc_inject.c
 *
 * Misc., injections to the device.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/
#include <time.h>
#include <math.h>
#include <sys/time.h>

#include "ai2.h"
#include "config.h"
#include "logger.h"
#include "device.h"
#include "utils.h"
#include "inject.h"
#include "gnss.h"
#include "dev_proxy.h"
#include "os_services.h"

#define OK 0
#define ERR -1

extern int h2d_ai2_hdl;

int h2d_dev_ping_req(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	unsigned char mem[3];
	unsigned char *buf = mem;

	buf = BUF_LE_U1B_WR_INC(buf, 0x00);
	buf = BUF_LE_U2B_WR_INC(buf, 0);


	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(ver, info, "version req injected");
			return OK;
		}
	}

	UNUSED(srv_re);
	UNUSED(ie_adu);
	LOGPRINT(ver, err, "error sending ai2 packet to device");
	return ERR;
}

int h2d_dev_ver_req(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	unsigned char mem[3];
	unsigned char *buf = mem;

	buf = BUF_LE_U1B_WR_INC(buf, 0xF0);
	buf = BUF_LE_U2B_WR_INC(buf, 0);


	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(ver, info, "version req injected");
			return OK;
		}
	}

	UNUSED(srv_re);
	UNUSED(ie_adu);
	LOGPRINT(ver, err, "error sending ai2 packet to device");
	return ERR;
}

int h2d_dev_nav_sel(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	struct dev_app_desc *desc = get_app_desc_cfg();

	*desc = *((struct dev_app_desc *)ie_adu->data);

	UNUSED(srv_re);

	return OK;
}


int h2d_qop_command(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
#define QOP_ACC_BACK_OFF 10
	struct ai2_qop_cfg {
		unsigned int id;
		unsigned int len;
		unsigned int sub_pkt_id;
		unsigned int reserved1;
		unsigned int t1;
		unsigned int a1;
		unsigned int t2;
		unsigned int a2;
		unsigned int a3;
		unsigned int pdop_mask;
		unsigned int reserved2;
		unsigned int reserved3;
		unsigned int reserved4;
	} qop[1];
	char ai2_qop_cfg_sizes[] = {1, 2, 1, 1, 2, 2, 2, 2, 2, 1, 1, 4, 4, 0};
	unsigned char mem[25];
	struct dev_qop_specifier *in = (struct dev_qop_specifier *)ie_adu->data;
	unsigned int delay_msec = 0;
	unsigned int *a3;
	unsigned int *pdop_mask;
	qop->id = 0xFB;
	qop->len = 22;
	qop->sub_pkt_id = 0x15;
	qop->reserved1 = 0;

	if (0 != get_rx_on_time())
		delay_msec = (unsigned int)(os_time_msecs() - get_rx_on_time());
	
	LOGPRINT(qop_parm, info, "qop delay msec [%u]", delay_msec);

	/* 0.5 sec/bit encoding.*/
	qop->t1 = (unsigned short)ceil(((in->max_ttf_ms + delay_msec) / (2 * 500.0)));
	qop->t2 = (unsigned short)floor(((in->max_ttf_ms + delay_msec) / 500.0));
	
	LOGPRINT(qop_parm, info, "qop t1 [%f] s", (float)qop->t1/2);
	LOGPRINT(qop_parm, info, "qop t2 [%f] s", (float)qop->t2/2);

	qop->a1 = in->accuracy_m;
	qop->a2 = in->accuracy_m * 2;
	if( in->accuracy_m > QOP_ACC_BACK_OFF ) 
		qop->a2 -= QOP_ACC_BACK_OFF;
	
	LOGPRINT(qop_parm, info, "qop a1 [%u] m", qop->a1);
	LOGPRINT(qop_parm, info, "qop a2 [%u] m", qop->a2);

	// Get the A3 and PDOP from config file
	sys_param_get(cfg_single_pe_a3,(void**) &a3);

	sys_param_get(cfg_single_pe_pdop_mask,(void**) &pdop_mask);

	qop->a3 = *a3;
	qop->pdop_mask = *pdop_mask;

	LOGPRINT(qop_parm, info, "qop a3 [%u]", qop->a3);
	LOGPRINT(qop_parm, info, "qop pdop mask [%u]", qop->pdop_mask);

	qop->reserved2 = 0;
	qop->reserved3 = 0;
	qop->reserved4 = 0;

	d2h_ai2_encode((void *)qop, mem, ai2_qop_cfg_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(qop_parm, info, "qop control injected");
			return OK;
		}
	}

	LOGPRINT(qop_parm, err, "error sending ai2 packet to device");
	return ERR;
}
static int h2d_cw_test(struct plt_param *plt)
{
	struct ai2_inj_cw_test {
		unsigned int id;
		unsigned int len;
		unsigned int tests;
		unsigned int options;
		unsigned int num_wbp;
		unsigned int wbp_samples;
		unsigned int num_nbp;
		unsigned int nb_cenfreq;
		unsigned int nbp_samples;
		unsigned int dec_fac;
		unsigned int toff_pts;
		unsigned int glo_slot;
		unsigned int num_fft_avg;
		unsigned int noise_fig_correction;
		unsigned int in_tone_lvl;
	} cw_test[1];
	char ai2_inj_cw_test_sizes[] = {1,2,2,2,1,1,1,4,1,1,1,1,1,1,2,0};
	unsigned char mem[22];

	cw_test->id = 0xC6;
	cw_test->len = 0x13;
	
	LOGPRINT(seq, info , "CW Test Type :%d ", plt->cw_test.tests);
	cw_test->tests = plt->cw_test.tests;
	cw_test->options = plt->cw_test.options;
	cw_test->num_wbp = plt->cw_test.num_wb_peaks;
	cw_test->wbp_samples = plt->cw_test.wb_peak_samples;
	cw_test->num_nbp = plt->cw_test.num_nb_peaks;
	cw_test->nb_cenfreq = plt->cw_test.nb_center_freq;
	cw_test->nbp_samples = plt->cw_test.nb_peak_samples;
	cw_test->dec_fac = plt->cw_test.decim_factor;
	cw_test->toff_pts = plt->cw_test.tap_off_pts;
	cw_test->glo_slot = plt->cw_test.glonass_slot;
	cw_test->num_fft_avg = plt->cw_test.num_fft_avg;
	cw_test->noise_fig_correction = plt->cw_test.noise_fig_corr_factor;
	cw_test->in_tone_lvl = plt->cw_test.in_tone_lvl;

	
	d2h_ai2_encode((void *)cw_test, mem, ai2_inj_cw_test_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 22)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "CW Test injected");
			return OK;
		}
	}
	LOGPRINT(seq, err, "CW Test inject failure");

	return ERR;


	
}

static int h2d_sigAcq_test(struct plt_param *plt)
{
	struct ai2_inj_siqacq_test {
		unsigned int id;
		unsigned int len;
		unsigned int timeout;
		unsigned int svid;
	} sigacq_test[1];
	char ai2_inj_sigacq_test_sizes[] = {1,2,1,1,0};
	unsigned char mem[5];

	sigacq_test->id = 0xCA;
	sigacq_test->len = 0x02;
	
	LOGPRINT(seq, info , "Sig Acq Test Inject SVID:%d timeout:%d", plt->svid, plt->timeout);
	sigacq_test->timeout = plt->timeout;
	sigacq_test->svid = plt->svid;
	
	d2h_ai2_encode((void *)sigacq_test, mem, ai2_inj_sigacq_test_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 5)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "Sig Acq Test injected");
			return OK;
		}
	}
	LOGPRINT(seq, err, "Sig Acq Test inject failure");

	return ERR;


	
}



int h2d_dev_plt(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	struct plt_param *plt= (struct plt_param*)ie_adu->data;


	LOGPRINT(h2d, info, "h2d_dev_plt: Entering %d ", plt->test_type);
	
	switch(plt->test_type)
	{
		case plt_rtc_osctest:
		case plt_gps_osctest:
		case plt_gpiotest:
		case plt_synctest:
		case plt_selftest:
		case plt_ram_chksumtest:
			break;
		case plt_cw_test:
			h2d_cw_test(plt);
			break;
		case plt_sigacqtest:
			h2d_sigAcq_test(plt);	
		break;
		default:
			break;
	}
	LOGPRINT(h2d, info, "h2d_dev_plt: Exiting");

	return 	OK;


}

