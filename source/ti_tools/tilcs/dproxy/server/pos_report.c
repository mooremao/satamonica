/*
 * pos_report.c
 *
 * Position report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ai2.h"
#include "device.h"
#include "dev2host.h"
#include "gnss.h"
#include "rpc.h"
#include "dev_proxy.h"
#include "utils.h"
#include "report.h"
#include "logger.h"
#include "nvs.h"
#include "log_report.h"
#include "nvs_validate.h"
#include "heading_filter.h"
unsigned int g_refTow;
unsigned char g_pps_state;

struct dev_PVT_info {

	/* The macros DEV_PVT_INC_XXX informs app about availability of data  */

#define DEV_PVT_HAS_DOP   0x0001    /**< Solicited @see struct dev_dop_info  */
#define DEV_PVT_HAS_VEL   0x0002    /**< Solicited @see struct dev_sat_info  */
#define DEV_PVT_HAS_SAT   0x0004    /**< Solicited @see struct dev_sat_info  */

	unsigned int         contents;

	struct dev_pos_info *pos_info;
	struct dev_dop_info *dop_info;
	struct dev_vel_info *vel_info;
	struct dev_hdg_info *hdg_info;

	short                  n_of_sat;
	struct dev_sat_info *sat_info;

};

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int pos_rpt_create_vel_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_vel_info *dev_vel, unsigned int rei_select);
int pos_rpt_create_loc_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_pos_info *dev_pos, unsigned int rei_select);
int pos_rpt_create_dop_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_dop_info *dev_dop, unsigned int rei_select);
int pos_rpt_create_sat_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_sat_info *dev_sat, unsigned int rei_sel,
						  unsigned int no_sat);

/*------------------------------------------------------------------------------
 * 3GPP Position / Location information calculation as per specification 23.032
 *------------------------------------------------------------------------------
 */

/* Latitude (degrees): Coded as per section 6.1 of 3GPP specification 23.032 */
static inline void setup_3gpp_lat(struct location_desc *loc,
				  struct dev_pos_info *dev_pos)
{
	/* X (deg) = (lat_rad) * (PI / 2^32) * (180 / PI) * (2^23 / 90) where,
	   unit of lat_rad is (PI / 2^32) radians per bit,
	   (180  / PI) converts radians to degrees,
	   (2^23 / 90) is a factor defined by 3GPP

	   Thus, X (deg) = lat_rad / (2^8)
	*/
	loc->lat_sign   = (dev_pos->lat_rad < 0) ? south : north;
	loc->latitude_N = (loc->lat_sign == south) ?
		(~(dev_pos->lat_rad >> 8) + 1) : (dev_pos->lat_rad >> 8);

        loc->lat_deg_by_2p8 = dev_pos->lat_rad;

	return;
}

/* Longitude (degrees): Coded as per section 6.1 of 3GPP specification 23.032 */
static inline void setup_3gpp_lon(struct location_desc *loc,
				  struct dev_pos_info *dev_pos)
{
	/* X (deg) = (lon_rad) * (PI / 2^31) * (180 / PI) * (2^24 / 360) where,
	   unit of lon_rad is (PI / 2^31) radians per bit
	   (180  / PI)  converts radians to degrees,
	   (2^24 / 360) is a factor defined by 3GPP

	   Thus, X (deg) = (lon_rad / (2^8)
	*/
	loc->longitude_N    = dev_pos->lon_rad >> 8;
        loc->lon_deg_by_2p8 = dev_pos->lon_rad; 
	return;
}

/* Altitude (in meters): Coded as per section 6.3 of 3GPP specification 23.032*/
static inline void setup_3gpp_alt(struct location_desc *loc,
								  struct dev_pos_info *dev_pos)
{
	/* alt (meters)      = alt_wgs84 (in 0.5 meters) * (0.5)
	   Thus, alt (meters)= alt_wgs84 / 2^1
	 */
	loc->alt_dir    = (dev_pos->alt_wgs84 < 0)? depth : height;
	loc->altitude_N   = (loc->alt_dir == depth) ?
		((~dev_pos->alt_wgs84 + 1) >> 1) : (dev_pos->alt_wgs84 >> 1);
	loc->altitude_MSL = (dev_pos->has_msl) ?
				 (dev_pos->alt_msl >> 1) : 0;

	return;
}

/* Refer to section 6.2 and 6.4 of 3GPP spec 23.032 for details about evaluating
   the number K to specify the uncertainity (in meters). In general terms,
   unc (in meters) = C((1 + x)^K -1) wherer C and x are specified constants.

   Thus, K = (unsigned char)(log10(unc_m / C) + 1f) / log10 (1f + x))
 */
#define UNC_MAP_TO_K(unc_m, C, x)  \
			(unsigned char)(log10(((unc_m*1.0) / (C*1.0)) + 1.0) / log10(1.0 + x))


/* (Ellipse) Unc: Coded as per section 6.2 of 3GPP specification 23.032 */
static inline void setup_3gpp_eval_ell_unc(struct location_desc *loc,
										   struct dev_pos_info *dev_pos)
{
	/* Calculate K using C = 10 & x = 0.1, as recommended in spec, 23.032.
	   (Input) unc (in meters) = unc_ai2 * (0.1) = unc_ai2 / 10f
	 */
	unsigned char E_K = UNC_MAP_TO_K(dev_pos->unc_east/10,  10, 0.1F);
	unsigned char N_K = UNC_MAP_TO_K(dev_pos->unc_north/10, 10, 0.1F);

	if(E_K > N_K) {
		loc->unc_semi_maj_K = E_K;
		loc->unc_semi_min_K = N_K;
		loc->orientation    = 89;
	} else {
		loc->unc_semi_maj_K = N_K;
		loc->unc_semi_min_K = E_K;
		loc->orientation    = 0;
	}

	return;
}

/* (ELP) Uncertainity: Coded as per sections 6.2 & 6.7 of 3GPP spec 23.032 */
static inline void setup_3gpp_ell_unc(struct location_desc *loc,
									  struct dev_ellp_unc *ell_unc)
{
	/* Calculate K using C = 10 & x = 0.1, as recommended in spec, 23.032.
	   (Input) unc (in meters) = unc_ai2 * (0.1) = unc_ai2 / 10f

	   angle (in degrees) = angle_ai2 * (PI / 2^15) * (180 / PI) * (1 / 2)
	   where, unit of angle_ai2 is radians per 2^15 and
	   (180 / PI) coverts radians to degrees
	   (1 / 2) is (3GPP) factor to present angle with 2 degree resolution

	   Thus,
	   angle (in degrees) = angle_ai2 * 45 / 2^14
	 */
	unsigned char major = UNC_MAP_TO_K(ell_unc->semi_major/10, 10, 0.1F);
	unsigned char minor = UNC_MAP_TO_K(ell_unc->semi_minor/10, 10, 0.1F);
	unsigned char angle = (ell_unc->orientation * 45) >> 14;

	loc->unc_semi_maj_K = major;
	loc->unc_semi_min_K = minor;
	loc->orientation    = angle;

	return;
}

/* Altitude uncertainity: Coded as per section 6.4, 3GPP specification 23.032 */
static inline void setup_3gpp_alt_unc(struct location_desc *loc,
									  struct dev_pos_info *dev_pos)
{
	/* Calculate K using C = 45 & x = 0.025, as recommended in spec, 23.032.
	   (Input) alt unc (in meters) = unc_ai2 * (0.5) = unc_ai2 / (2^1).
Note: 3GPP specifies Max alt unc of 990.5M as against 32767.5M (dev)
	 */
	unsigned short alt_unc = dev_pos->unc_vertical >> 1; /* Now, in mts */
	loc->unc_altitude_K = UNC_MAP_TO_K((alt_unc > 990.5)? 990.5 : alt_unc,
									   45, 0.025);

	return;
}

/*------------------------------------------------------------------------------
 * 3GPP Velocity information calculation as per specification 23.032
 *------------------------------------------------------------------------------
 */

/* Horizontal velocity: Coded as per section 8.7 and 8.8 of 3GPP spec 23.032
   Hor speed in Kms/hr & bearing in degrees measured clockwise from North */
static inline void setup_3gpp_hor_vel(struct vel_gnss_info *vel,
									  struct dev_vel_info *dev_vel)
{
	/* Hor speed (Kms/hr) = (((Veast^2) + (Vnorth^2)) ^ (1 / 2)) *
	   (((V_E^2) + (V_N^2))^(1 / 2)) * (1000 / 65535) * (3600 / 1000)
	   where, unit of V_E and V_N is (1000 / 65535) m/sec per bit,
	   (3600 / 1000) converts m/sec to Kms/hr
	   Thus,
	   Hor speed (Kms/hr) = (((V_E^2) + (V_N^2)) ^ (1/2)) * (720 / 13107)
	 */
	vel->h_speed = sqrt(pow(dev_vel->east, 2) + pow(dev_vel->north, 2))
		* 720 / 13107;

	/* Bearing (degrees) = hdg * (PI / 2^15) * (180 / PI) where,
	   unit of mag hdg is (PI / 2^15) rad per bit, and
	   (180 / PI) converts radians to degrees

	   Thus, Bearing (degrees) = (true heading) * 45 / 2^13
	 */
	vel->bearing = (dev_vel->hdg_info->truthful * 45) >> 13;

	return;
}

/* Vertical velocity: Coded as per section 8.9 and 8.10 of 3GPP spec 23.032
   Ver speed in Kms/hr & Ver speed dir using 1 bit (0 --> up and 1 --> down)
 */
static inline void setup_3gpp_ver_vel(struct vel_gnss_info *vel,
									  struct dev_vel_info *dev_vel)
{
	/* Ver speed (Kms/hr) = vertical * (1000 / 65535) * (3600 / 1000)
	   where,
	   unit of vertical is (1000 / 65535) m/sec per bit,
	   (3600 / 1000) converts m/sec to Kms/hr

	   Thus, Ver speed (Kms/hr) = vertical * (720 / 13107)
	 */
	vel->v_speed = abs(dev_vel->vertical) * 720 / 13107;

	/* Ver dir (0-up / 1-down) */
	vel->ver_dir = (dev_vel->vertical > 0) ? 0 : 1;
	vel->v_unc = (dev_vel->uncertainty ) ;

	vel->est_unc = (dev_vel->east_unc) ;
	vel->nth_unc = (dev_vel->north_unc) ;
	vel->vup_unc = (dev_vel->velocityUp_unc) ;

	return;
}

/* Horizontol speed Uncertainty: Coded as per section 8.11 of 3GPP spec 23.032
   Uncertainty in Kms / hr
 */
static inline void setup_3gpp_hor_vel_unc(struct vel_gnss_info *vel,
										  struct dev_vel_info *dev_vel)
{
	/* Unc (Kms/hr) = uncertainty * (1000 / 65535) * (3600 / 1000)
	   where,
	   unit of uncertainty is (1000 / 65535) m/sec per bit,
	   (3600 / 1000) converts m/sec to Kms/hr

	   Thus, Unc (Kms/hr in 8 bits) = uncertainty * (720 / 13107)
	 */


	UNUSED(vel);
	UNUSED(dev_vel);
	return;
}

/* Vertical speed Uncertainty: Coded as per section 8.11 of 3GPP spec 23.032
   Uncertainty in Kms / hr
 */
static inline void setup_3gpp_ver_vel_unc(struct vel_gnss_info *vel,
										  struct dev_vel_info *dev_vel)
{
	/* Not available from the device */
	UNUSED(vel);
	UNUSED(dev_vel);
	return;
}

/******** Place holder for more 3GPP specific functions *****************/

/*------------------------------------------------------------------------------
 * Processing of POS Report from device and support of auxilliary functions.
 *------------------------------------------------------------------------------
 */

static struct ie_desc *mk_ie_adu_4loc_dev(struct rpc_msg *rmsg,
										  unsigned char oper_id, 
										  struct dev_PVT_info *info)
{
	struct dev_pos_info *dev_pos = info->pos_info;
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_pos_info) + ST_SIZE(dev_ellp_unc) +
		ST_SIZE(dev_utc_info);
	struct dev_pos_info *n_pos;

	n_pos = malloc(st_size);
	if(!n_pos)
		goto mk_loc_dev_exit1;

	n_pos->ell = BUF_OFFSET_ST(n_pos, struct dev_pos_info,
							   struct dev_ellp_unc);
	n_pos->utc = BUF_OFFSET_ST(n_pos->ell, struct dev_ellp_unc,
							   struct dev_utc_info);

	memcpy(n_pos, dev_pos, ST_SIZE(dev_pos_info));
	memcpy(n_pos->ell, dev_pos->ell, ST_SIZE(dev_ellp_unc));
	memcpy(n_pos->utc, dev_pos->utc, ST_SIZE(dev_utc_info));

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_EXL_POS, oper_id, st_size, n_pos);
	if(!ie_adu)
		goto mk_loc_dev_exit2;

	return ie_adu;

mk_loc_dev_exit2:
	free(n_pos);

mk_loc_dev_exit1:
	return NULL;
}

/* Following function is not suited for position report (w/ buffered location
   fixes).
 */
static int setup_loc_desc(struct location_desc *loc,
			  struct dev_pos_info *dev_pos)
{
	if(!(dev_pos->pos_flags & 0x3))
		return -1;

	if(dev_pos->pos_flags & 0x2)
		loc->shape = ellipsoid_with_v_alt_and_e_unc; /* 3D Fix */
	else
		loc->shape = ellipsoid_with_e_unc;           /* 2D Fix */

	setup_3gpp_lat(loc, dev_pos);      /* Latitude  in degrees     */
	setup_3gpp_lon(loc, dev_pos);      /* Longitude in degress     */

	if(loc->shape == ellipsoid_with_v_alt_and_e_unc) {
		/* Include altitude information only for 3D Fix */
		setup_3gpp_alt(loc, dev_pos);      /* Alitude   in meters     */
		setup_3gpp_alt_unc(loc, dev_pos);  /* Altitude uncertainity   */
	}

	if(dev_pos->contents & DEV_POS_HAS_ELP)
		setup_3gpp_ell_unc(loc, dev_pos->ell);/* Ellipse uncertainity */

	loc->confidence = 0;  /* Not supported, set to 0  */

	loc->east_unc  = dev_pos->unc_east * 0.1f;
	loc->north_unc = dev_pos->unc_north * 0.1f ;
//	loc->vert_unc  = dev_pos->unc_vertical >> 1;
	loc->vert_unc  = dev_pos->unc_vertical * 0.5f;
	/*Fix for the issue where Verical Pos unc is reported as 0.0 to appplication */
	if(loc->vert_unc <= 0)
		loc->vert_unc  = 0.5f;	
	return 0;
}

static struct ie_desc *mk_ie_adu_4loc_3gpp(struct rpc_msg *rmsg,
					   unsigned char oper_id, 
					   struct dev_PVT_info *pinfo)
{
	struct timeval timestp;
	/** Number of seconds between the dates
	 ** 1st Jan 1970 to 4th Jan 1980
	 **/
#define GPS2UTC 315964800
#define DAY_SECS 86400
#define DAY_MSEC 86400000

	struct dev_pos_info *dev_pos = pinfo->pos_info;
	struct dev_utc_info *dev_utc = dev_pos->utc;
	struct dev_sat_info *sat_info = pinfo->sat_info;
	int n_of_sat = pinfo->n_of_sat;
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(loc_gnss_info);
	struct loc_gnss_info *loc_gps = malloc(st_size);
	struct dev_gps_time dev_time[1];
	int osc_bias = 0;
	if(!loc_gps)
		goto mk_loc_gps_exit1;

	if(setup_loc_desc(&loc_gps->location, dev_pos))
		goto mk_loc_gps_exit2;

	nvs_rec_get(nvs_gps_clk, dev_time, 0x01);

        // Initialize a bit map to zero -- KW Error
	loc_gps->pos_bits = 0;
	if((dev_time->week_num == 0xFFFF)||
		(dev_time->week_num == 0)){
	
		loc_gps->utc_secs = 0xFFFFFFFF;
		loc_gps->utc_msec = 0xFFFF;
                loc_gps->ref_msec = 0xFFFF;
                loc_gps->ref_secs = 0xFFFF;
				loc_gps->osc_bias = 0xFFFFFFFF;
				loc_gps->osc_freq = 0xFFFFFFFF;
				loc_gps->utcOffset = 0xFF;				
				loc_gps->week_num  = 0xFFFF;				
	
		LOGPRINT(pos_rpt,info,"unknown week, unable to compute UTC");

	}else{

		loc_gps->gnss_tid = gps_time_id;
		loc_gps->ref_msec = dev_time->time_msec;
		gettimeofday(&timestp, NULL);
		loc_gps->ref_secs = (dev_time->week_num * WEEK_SECS) +
					(dev_time->time_msec / 1000);

		loc_gps->utc_secs =(dev_time->week_num * WEEK_SECS) +
		                ((dev_time->time_msec/DAY_MSEC)*DAY_SECS) +
		                (dev_utc->hours * 3600) +
		                (dev_utc->minutes* 60)+
		                (dev_utc->seconds);

		/* detect if GPS has rolled over */
		/* substract one day */
		if(loc_gps->utc_secs>loc_gps->ref_secs)
			loc_gps->utc_secs -= (24 * 60 *60);

		loc_gps->utc_secs += GPS2UTC;

		loc_gps->utc_msec = dev_utc->sec_1by10 * 100;

		osc_bias = (int)((dev_time->time_bias & 0x800000)?(0xFF000000 | dev_time->time_bias): dev_time->time_bias) ;
		LOGPRINT(pos_rpt, info," devMS = %d , dev_time->time_bias   %d  , osc_bias %d\n",dev_time->time_msec, dev_time->time_bias , osc_bias);
//		loc_gps->osc_bias = (double)((double)(osc_bias) * 299792458.0 );
		loc_gps->osc_bias = (double)osc_bias ;
		loc_gps->osc_bias = loc_gps->osc_bias * (1.0f / 16777216.0f) *( 299792.458 );
		loc_gps->osc_freq = dev_time->freq_bias * 0.1;
		loc_gps->utcOffset = 16; /*LEAP SEC value*/				
		loc_gps->week_num  = dev_time->week_num;				
		LOGPRINT(pos_rpt,info,"utc_secs %d, utc_msec %d,",
				loc_gps->utc_secs,loc_gps->utc_msec);

	}

	while (n_of_sat--) {
		if (sat_info->gnss_id == e_gnss_gps)
			loc_gps->pos_bits |= 0x00000002;
		else if (sat_info->gnss_id == e_gnss_glo)
			loc_gps->pos_bits |= 0x00000040;
		else if (sat_info->gnss_id == e_gnss_qzs)
			loc_gps->pos_bits |= 0x00000020;
		else if (sat_info->gnss_id == e_gnss_sbs)
			loc_gps->pos_bits |= 0x00000008;

		sat_info++;
	}
	LOGPRINT(pos_rpt,info,"ref_msec: %u", loc_gps->ref_msec);
	if(dev_pos->pos_flags  & 0x2){
			loc_gps->fix_type = fix_3d;
		if((dev_pos->pos_flags  & 0x2) && (dev_pos->pos_flags  & 0x0800))
			loc_gps->fix_type = fix_3d_diff;
	}
	 if((dev_pos->pos_flags  & 0x1) ){
			loc_gps->fix_type = fix_2d;
	 if((dev_pos->pos_flags  & 0x1) && (dev_pos->pos_flags  & 0x0800))
			loc_gps->fix_type = fix_2d_diff;
	 	}
	/*SRK: Added to check the POS Fcount*/
	loc_gps->TCount = dev_pos->TCount;
	loc_gps->pps_state = dev_pos->pps_state;
	loc_gps->pos_flag = dev_pos->pos_flags;
	
	LOGPRINT(pos_rpt,info,"fix_type: %d [fix_2d=0, fix_3d=1]", loc_gps->fix_type);
	g_refTow = loc_gps->ref_msec;
	loc_gps->timeTag_sec = timestp.tv_sec;
	loc_gps->timetag_usec = timestp.tv_usec;

	ie_adu = rpc_ie_attach(rmsg, REI_LOC_GPS, oper_id, st_size, loc_gps);
	if(!ie_adu)
		goto mk_loc_gps_exit2;

	return ie_adu;

mk_loc_gps_exit2:
	free(loc_gps);

mk_loc_gps_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4pos_ref(struct rpc_msg *rmsg,
					  unsigned char oper_id, 
					  struct dev_PVT_info *pinfo)
{
	struct dev_pos_info *dev_pos = pinfo->pos_info;
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(location_desc);
	struct location_desc *ref_pos = malloc(st_size);

	if(!ref_pos)
		goto mk_pos_ref_exit1;

	if(setup_loc_desc(ref_pos, dev_pos))
		goto mk_pos_ref_exit2;

	LOGPRINT(pos_rpt, info, "mk_ie_adu_4pos_ref");

	ie_adu = rpc_ie_attach(rmsg, REI_REF_POS, oper_id, st_size, ref_pos);
	if(!ie_adu)
		goto mk_pos_ref_exit2;

	return ie_adu;

mk_pos_ref_exit2:
	free(ref_pos);

mk_pos_ref_exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int pos_ext_create_loc_ie(struct rpc_msg *rmsg, unsigned char oper_id,
				 struct dev_PVT_info *info, 
				 unsigned int rei_select)
{
	int n_adu = 0;
	struct ie_desc *ie_adu[4];

	if(rei_select & POS_RPT_LOC_NATIVE) {
		ie_adu[n_adu] = mk_ie_adu_4loc_dev(rmsg, oper_id, info);
		if(!ie_adu[n_adu++])
			goto loc_rei_exit1;
	}

	if((rei_select & POS_RPT_GPS_IE_SEL) || (rei_select & POS_RPT_GLO_IE_SEL)) {
		ie_adu[n_adu] = mk_ie_adu_4loc_3gpp(rmsg, oper_id, info);
		if(!ie_adu[n_adu++])
			goto loc_rei_exit1;
	}

	ie_adu[n_adu] = mk_ie_adu_4pos_ref(rmsg, oper_id, info);
	if(!ie_adu[n_adu++])
		goto loc_rei_exit1;

	return n_adu;

loc_rei_exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int pos_rpt_create_loc_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_pos_info *dev_pos, unsigned int rei_select)
{
	struct dev_PVT_info info = {
		.pos_info = dev_pos
	};

	return pos_ext_create_loc_ie(rmsg, oper_id, &info, rei_select);
}

/*------------------------------------------------------------------------------
 * Processing of DOP Report from device and support of auxilliary functions.
 *------------------------------------------------------------------------------
 */
static struct ie_desc *mk_ie_adu_4dop(struct rpc_msg *rmsg,
									  unsigned char oper_id, 
									  struct dev_dop_info *dev_dop)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_dop_info);
	struct dev_dop_info *n_dop;

	n_dop = malloc(st_size);
	if(!n_dop)
		goto mk_dop_exit1;

	memcpy(n_dop, dev_dop, st_size);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_DOP_INF, oper_id, st_size, n_dop);
	if(!ie_adu)
		goto mk_dop_exit2;

	return ie_adu;

mk_dop_exit2:
	free(n_dop);

mk_dop_exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int pos_ext_create_dop_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_dop_info *dev_dop, 
								 unsigned int rei_select)
{
	struct ie_desc *ie_adu[1];
	int n_adu = 0;

	UNUSED(rei_select);

	ie_adu[n_adu] = mk_ie_adu_4dop(rmsg, oper_id, dev_dop);
	if(!ie_adu[n_adu++])
		goto dop_rei_exit1;

	return n_adu;

dop_rei_exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int pos_rpt_create_dop_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_dop_info *dev_dop, unsigned int rei_select)
{
	return pos_ext_create_dop_ie(rmsg, oper_id, dev_dop, rei_select);
}

/*------------------------------------------------------------------------------
 * Processing of SAT Report from device and support of auxilliary functions.
 *------------------------------------------------------------------------------
 */
static struct ie_desc *mk_ie_adu_4sat(struct rpc_msg *rmsg, unsigned char op_id,
									  struct dev_sat_info *dev_sat, 
									  unsigned int n_of_sat)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_sat_info) * n_of_sat;
	struct dev_sat_info *new_sat;

	new_sat = malloc(st_size);
	if(!new_sat)
		goto mk_sat_exit1;

	memcpy(new_sat, dev_sat, st_size);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_SAT_INF, op_id, st_size, new_sat);
	if(!ie_adu)
		goto mk_sat_exit2;

	return ie_adu;

mk_sat_exit2:
	free(new_sat);

mk_sat_exit1:
	return NULL;
}

static int pos_ext_create_sat_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_sat_info *dev_sat, 
								 unsigned int rei_select, unsigned int n_of_sat)
{
	struct ie_desc *ie_adu[1];
	int n_adu = 0;

	UNUSED(rei_select);

	ie_adu[n_adu] = mk_ie_adu_4sat(rmsg, oper_id, dev_sat, n_of_sat);
	if(!ie_adu[n_adu++])
		goto sat_rei_exit1;

	return n_adu;

sat_rei_exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int pos_rpt_create_sat_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_sat_info *dev_sat, unsigned int rei_sel, 
						  unsigned int no_sat)
{
	return pos_ext_create_sat_ie(rmsg, oper_id, dev_sat, rei_sel, no_sat);
}

/*------------------------------------------------------------------------------
 * Processing of SAT Report from device and support of auxilliary functions.
 *------------------------------------------------------------------------------
 */
static void setup_vel_gnss(struct vel_gnss_info *vel,
						  struct dev_vel_info *dev_vel)
{
	/* There is always horizontol velocity */
	setup_3gpp_hor_vel(vel, dev_vel);

	if((vel->type == h_vel_unc) || (vel->type == h_v_vel_unc))
		setup_3gpp_hor_vel_unc(vel, dev_vel);  /* Horizontol vel unc  */

	if((vel->type == h_v_vel) || (vel->type ==  h_v_vel_unc))
		setup_3gpp_ver_vel(vel, dev_vel);      /* Vertical   velocity */

	if(vel->type == h_v_vel_unc)
		setup_3gpp_ver_vel_unc(vel, dev_vel);  /* Vertical vel unc    */
}

static struct ie_desc *mk_ie_adu_4vel_gnss(struct rpc_msg *rmsg,
										   unsigned char oper_id, 
										   struct dev_vel_info *dev_vel)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(vel_gnss_info);
	struct vel_gnss_info *vel_gnss = malloc(st_size);

	if(!vel_gnss)
		goto mk_vel_gnss_exit1;

	/* As of now, only horizontol and vertical velocities are reported */
	vel_gnss->type = h_v_vel;

	setup_vel_gnss(vel_gnss, dev_vel);

	ie_adu = rpc_ie_attach(rmsg, REI_VEL_GNS, oper_id, st_size, vel_gnss);
	if(!ie_adu)
		goto mk_vel_gnss_exit2;

	return ie_adu;

mk_vel_gnss_exit2:
	free(vel_gnss);

mk_vel_gnss_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4vel_dev(struct rpc_msg *rmsg,
										  unsigned char oper_id, 
										  struct dev_vel_info *dev_vel)
{
	struct ie_desc *ie_adu;
	int st_size = ST_SIZE(dev_vel_info) + ST_SIZE(dev_hdg_info);
	struct dev_vel_info *vel;
	struct dev_hdg_info *hdg_info;

	vel = malloc(st_size);
	if(!vel)
		goto mk_vel_dev_exit1;

	vel->contents = dev_vel->contents;
	vel->east = dev_vel->east;
	vel->north = dev_vel->north;
	vel->vertical = dev_vel->vertical;
	vel->uncertainty = dev_vel->uncertainty;
	vel->east_unc= dev_vel->east_unc;
	vel->north_unc= dev_vel->north_unc;
	vel->velocityUp_unc= dev_vel->velocityUp_unc;

	vel->hdg_info = BUF_OFFSET_ST(vel, struct dev_vel_info,
								  struct dev_hdg_info);
	memcpy(vel->hdg_info, dev_vel->hdg_info, ST_SIZE(dev_hdg_info));

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_VEL_INF, oper_id, st_size, vel);
	if(!ie_adu)
		goto mk_vel_dev_exit2;

	UNUSED(hdg_info);
	return ie_adu;

mk_vel_dev_exit2:
	free(vel);

mk_vel_dev_exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int pos_ext_create_vel_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_vel_info *dev_vel, 
								 unsigned int rei_select)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	if(rei_select & POS_RPT_VEL_NATIVE) {
		ie_adu[n_adu] = mk_ie_adu_4vel_dev(rmsg, oper_id, dev_vel);
		if(!ie_adu[n_adu++])
			goto loc_rei_exit1;
	}

	if(rei_select & POS_RPT_VEL_IE_SEL) {
		ie_adu[n_adu] = mk_ie_adu_4vel_gnss(rmsg, oper_id, dev_vel);
		if(!ie_adu[n_adu++])
			goto loc_rei_exit1;
	}

	return n_adu;

loc_rei_exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int pos_rpt_create_vel_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						  struct dev_vel_info *dev_vel, unsigned int rei_select)
{
	return pos_ext_create_vel_ie(rmsg, oper_id, dev_vel, rei_select);
}

/*------------------------------------------------------------------------------
 * Position report extended: D2H core and support of auxilliary functions.
 *------------------------------------------------------------------------------
 */
static int pos_ext_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
						  unsigned int rei_select)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;

	if(-1 == pos_ext_create_loc_ie(rmsg, OPER_INF, dev_PVT, rei_select))
		return -1;

	if(dev_PVT->contents & DEV_PVT_HAS_DOP)
		if(-1 == pos_ext_create_dop_ie(rmsg, OPER_INF,
									   dev_PVT->dop_info, rei_select))
			return -1;

	if(dev_PVT->contents & DEV_PVT_HAS_VEL)
		if(-1 == pos_ext_create_vel_ie(rmsg, OPER_INF,
									   dev_PVT->vel_info, rei_select))
			return -1;

	if(dev_PVT->contents & DEV_PVT_HAS_SAT)
		if(-1 == pos_ext_create_sat_ie(rmsg, OPER_INF,
									   dev_PVT->sat_info, rei_select, 
									   dev_PVT->n_of_sat))
			return -1;

	return 0;
}

static int dev_pos_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_pos_info *dev_pos = dev_PVT->pos_info;
	char  *ai2_pkt               = (char* )ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;

	dev_pos->TCount              = BUF_LE_U4B_RD_INC(ai2_pkt);
	dev_pos->pos_flags           = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_pos->lat_rad             = BUF_LE_S4B_RD_INC(ai2_pkt);
	dev_pos->lon_rad             = BUF_LE_S4B_RD_INC(ai2_pkt);
	dev_pos->alt_wgs84           = BUF_LE_S2B_RD_INC(ai2_pkt);

	if (d2h_get_msg_id(ai2_d2h) == 0xD5) {
		dev_pos->has_msl             = 1;
		dev_pos->alt_msl             = BUF_LE_S2B_RD_INC(ai2_pkt);
	} else {
		dev_pos->has_msl             = 0;
		dev_pos->alt_msl             = 0;
	}

	dev_pos->unc_east            = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_pos->unc_north           = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_pos->unc_vertical        = BUF_LE_U2B_RD_INC(ai2_pkt);

	LOGPRINT(pos_rpt,info,"position report extracted with FCount: %d", dev_pos->TCount);
	return (ai2_pkt - ai2_ref);
}

static int dev_vel_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_vel_info *dev_vel = dev_PVT->vel_info;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;

	dev_PVT->contents           |= DEV_PVT_HAS_VEL;

	dev_vel->east                = BUF_LE_S2B_RD_INC(ai2_pkt);
	dev_vel->north               = BUF_LE_S2B_RD_INC(ai2_pkt);
	dev_vel->vertical            = BUF_LE_S2B_RD_INC(ai2_pkt);

	if (d2h_get_msg_id(ai2_d2h) == 0xD5){
	//	printf("dev_vel_struct Entering: new AI2 parsing\n ");
		dev_vel->uncertainty         = BUF_LE_U2B_RD_INC(ai2_pkt);
		ai2_pkt                      = ai2_pkt + 21; //Work over AI2
		dev_vel->east_unc = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_vel->north_unc = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_vel->velocityUp_unc = BUF_LE_U2B_RD_INC(ai2_pkt);
		ai2_pkt                      = ai2_pkt - 27;    //Reset
		
		}
	else
		dev_vel->uncertainty         = 0;

	print_data(REI_DEV_VEL_INF, dev_vel);

	return (ai2_pkt - ai2_ref);
}

static int dev_dop_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_dop_info *dev_dop = dev_PVT->dop_info;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;

	dev_PVT->contents           |= DEV_PVT_HAS_DOP;

	dev_dop->position            = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_dop->horizontal          = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_dop->vertical            = BUF_LE_U1B_RD_INC(ai2_pkt);

	/*********************************************************/
	/*  TDOP KLUDGE: To work over AI2 packet pattern         */
	/*********************************************************/
	ai2_pkt                      = ai2_pkt + 14;
	dev_dop->time                = BUF_LE_U1B_RD_INC(ai2_pkt);
	ai2_pkt                      = ai2_pkt - 15; /* Reset */

	print_data(REI_DEV_DOP_INF, dev_dop);

	return (ai2_pkt - ai2_ref);
}

static int dev_hdg_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	unsigned short truthful;
	unsigned short magnetic;
	struct dev_vel_info *dev_vel = (struct dev_vel_info *)dev_PVT->vel_info;
//	struct dev_hdg_info *dev_hdg = (struct dev_hdg_info *)dev_vel->hdg_info;
	struct dev_hdg_info *dev_hdg = (struct dev_hdg_info *)dev_PVT->hdg_info;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;

	dev_vel->contents           |= DEV_POS_HAS_HDG;

	dev_PVT->hdg_info->truthful            = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_PVT->hdg_info->magnetic            = BUF_LE_U2B_RD_INC(ai2_pkt);


	return (ai2_pkt - ai2_ref);
}

static int dev_ell_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_pos_info *dev_pos = dev_PVT->pos_info;
	struct dev_ellp_unc *dev_ell = dev_pos->ell;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;

	dev_pos->contents           |= DEV_POS_HAS_ELP;

	dev_ell->orientation         = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_ell->semi_major          = BUF_LE_U2B_RD_INC(ai2_pkt);
	dev_ell->semi_minor          = BUF_LE_U2B_RD_INC(ai2_pkt);

	return (ai2_pkt - ai2_ref);
}

static int dev_utc_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_pos_info *dev_pos = dev_PVT->pos_info;
	struct dev_utc_info *dev_utc = dev_pos->utc;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;

	dev_pos->contents           |= DEV_POS_HAS_UTC;

	dev_utc->hours               = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_utc->minutes             = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_utc->seconds             = BUF_LE_U1B_RD_INC(ai2_pkt);
	dev_utc->sec_1by10           = BUF_LE_U1B_RD_INC(ai2_pkt);

	return (ai2_pkt - ai2_ref);
}

static int dev_sat_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_sat_info *dev_sat = dev_PVT->sat_info;
	char  *ai2_pkt               = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref               = ai2_pkt;

	int    n_sat                 = (ai2_d2h->ai2_pkt_sz - ai2_offset) / 6;

	dev_PVT->n_of_sat            = n_sat;
	if(n_sat)
		dev_PVT->contents       |= DEV_PVT_HAS_SAT;

	while(n_sat--) {

		dev_sat->svid            = BUF_LE_U1B_RD_INC(ai2_pkt);

		if (dev_sat->svid >= 1 && dev_sat->svid <= 32)
			dev_sat->gnss_id          = e_gnss_gps;
		else if (dev_sat->svid >= 65 && dev_sat->svid <= 88)
			dev_sat->gnss_id          = e_gnss_glo;
		else if  (dev_sat->svid >= 120 && dev_sat->svid <= 138)
			dev_sat->gnss_id          = e_gnss_sbs;
		else if (dev_sat->svid >= 193 && dev_sat->svid <= 202)
			dev_sat->gnss_id          = e_gnss_qzs;
		else
			dev_sat->gnss_id          = e_gnss_und;

		dev_sat->iode            = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_sat->msr_residual    = BUF_LE_S2B_RD_INC(ai2_pkt);
		dev_sat->weigh_in_fix    = BUF_LE_U2B_RD_INC(ai2_pkt);

		print_data(REI_DEV_SAT_INF, dev_sat);

		dev_sat++;
	}

	return (ai2_pkt - ai2_ref);
}

#define PVT_VEL_OFFSET  24
#define PVT_DOP_OFFSET  32
#define PVT_HDG_OFFSET  35
#define PVT_UTC_OFFSET  45
#define PVT_SAT_OFFSET  61

static int pos_ext_setmem(struct d2h_ai2 *ai2_d2h)
{
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_pos_info *dev_pos;
	struct dev_vel_info *dev_vel;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	/* Set up memory for contents of position information */
	dev_pos = dev_PVT->pos_info = BUF_OFFSET_ST(dev_PVT,
												struct dev_PVT_info, 
												struct dev_pos_info);

	dev_pos->ell = BUF_OFFSET_ST(dev_pos, struct dev_pos_info,
								 struct dev_ellp_unc);

	dev_pos->utc = BUF_OFFSET_ST(dev_pos->ell, struct dev_ellp_unc,
								 struct dev_utc_info);

	/* Set up memory for contents of dilution of precision information */
	dev_PVT->dop_info = BUF_OFFSET_ST(dev_pos->utc, struct dev_utc_info,
									  struct dev_dop_info);

	/* Set up memory for contents of velocity information */
	dev_PVT->vel_info = dev_vel = BUF_OFFSET_ST(dev_PVT->dop_info,
												struct dev_dop_info, 
												struct dev_vel_info);
	dev_PVT->hdg_info = dev_vel->hdg_info = BUF_OFFSET_ST(dev_PVT->vel_info,
														  struct dev_vel_info, 
														  struct dev_hdg_info);

	/* Set up memory for contents of satellite information */
	dev_PVT->sat_info = BUF_OFFSET_ST(dev_vel->hdg_info,
									  struct dev_hdg_info, struct dev_sat_info);

	return 0;
}

static int pos_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_pos_struct(ai2_d2h, ai2_offset);

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_vel_struct(ai2_d2h, ai2_offset);

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_dop_struct(ai2_d2h, ai2_offset);

	if (d2h_get_msg_id(ai2_d2h) == 0xD5) {
		if(ai2_offset < ai2_pkt_sz)
			ai2_offset += dev_hdg_struct(ai2_d2h, ai2_offset);

		if(ai2_offset < ai2_pkt_sz)
			ai2_offset += dev_ell_struct(ai2_d2h, ai2_offset);

		if(ai2_offset < ai2_pkt_sz)
			ai2_offset += dev_utc_struct(ai2_d2h, ai2_offset);

		ai2_offset += 1;  /* Skip over TDOP, handled before in DOP */

		ai2_offset += 11; /* Adjust for 11 reserved bytes   */
	}

	ai2_offset += dev_sat_struct(ai2_d2h, ai2_offset);

	print_data(REI_DEV_GPS_POS,
			   ((struct dev_PVT_info *)ai2_d2h->ai2_struct)->pos_info);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

#define EXT_PVT_ST_LEN                                            \
	ST_SIZE(dev_PVT_info) +                                       	  \
ST_SIZE(dev_pos_info) +                                           \
ST_SIZE(dev_ellp_unc) +                                           \
ST_SIZE(dev_utc_info) +                                           \
ST_SIZE(dev_dop_info) +                                           \
ST_SIZE(dev_vel_info) +                                           \
ST_SIZE(dev_hdg_info) +                                           \
(ST_SIZE(dev_sat_info) * 200)

static int pos_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0;
	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	struct dev_pos_info *dev_pos = dev_PVT->pos_info;
	struct dev_dop_info *dev_dop = dev_PVT->dop_info;

	LOGPRINT(pos_rpt, info, "storing pos in nvs");
	/* Don't save position aiging in case of below condition */
	if( ((dev_pos->unc_vertical * 0.5f) > 100.0 ) || ((dev_dop->horizontal * 0.1) > 5.0) || ((dev_pos->alt_wgs84 * 0.5f) < 0)
			|| ((dev_pos->pos_flags & 0x2) != 0x2) )
		return 0;
	
	wr_suc = nvs_rec_add(nvs_gps_pos, dev_pos, 1);
	if (wr_suc != nvs_success) {
		LOGPRINT(pos_rpt, err, "error storing pos in nvs");
		return -1;
	}

	return 0;
}

static void filter_pos_heading(struct d2h_ai2 *ai2_d2h)
{

	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;

	struct dev_pos_info *dev_pos = dev_PVT->pos_info;
	struct dev_vel_info *dev_vel = dev_PVT->vel_info;
	struct dev_hdg_info *dev_hdg = dev_PVT->hdg_info;

	struct VelocityVar CurrentVelocityVar;
	struct VelocityVar FilteredVelocityVar;

	CurrentVelocityVar.TCount = dev_pos->TCount;
	CurrentVelocityVar.Heading = (float)(dev_hdg->truthful * 180.0 / ((float)(pow(2.0,15))));
	CurrentVelocityVar.VelEast = (float)(dev_vel->east * 1000.0 / 65535.0);
	CurrentVelocityVar.VelNorth = (float)(dev_vel->north * 1000.0 / 65535.0);

	HdgFilter(&CurrentVelocityVar, &FilteredVelocityVar);

	dev_hdg->truthful = (unsigned short)(FilteredVelocityVar.Heading * pow(2.0,15)/180.0);
	dev_vel->east = (short)(FilteredVelocityVar.VelEast * 65535.0/1000.0);
	dev_vel->north = (short)(FilteredVelocityVar.VelNorth * 65535.0/1000.0);

}


static int qualify_pos(struct d2h_ai2 *ai2_d2h)
{
#define POS_RPT_A_2D_FIX 0x0001
#define POS_RPT_A_3D_FIX 0x0002
#define POS_RPT_OVRDETER 0x0004
#define POS_RPT_FEW_SATS 0x0008
#define POS_RPT_DIFFTIAL 0x0010
#define POS_RPT_NO_UPDAT 0x0020
#define POS_RPT_EXTERNAL 0x0040
#define POS_RPT_VELOCITY 0x0080
#define POS_RPT_ITAR_VIO 0x0100
#define POS_RPT_TIME_USE 0x0200
#define POS_RPT_PROPGATE 0x0400
#define POS_RPT_SBAS_COR 0x0800
#define POS_RPT_KALMAN_F 0x1000

	struct dev_PVT_info *dev_PVT = (struct dev_PVT_info*)
		ai2_d2h->ai2_struct;
	unsigned short pos_flags = dev_PVT->pos_info->pos_flags;
	struct dev_pos_info *dev_pos = dev_PVT->pos_info;

	/* Rules for filtering POS reports: 
	 * a) Should not be a copy of POS aided/assisted information given to UE
	 * b) Should be either a 2D fix or 3D fix.
	 * c) POS_RPT_DIFFTIAL added as per Trimble requirement
	 */
	if((pos_flags & (POS_RPT_DIFFTIAL |POS_RPT_NO_UPDAT | POS_RPT_EXTERNAL | POS_RPT_ITAR_VIO)) ||
	   !((pos_flags & POS_RPT_A_2D_FIX) | (pos_flags & POS_RPT_A_3D_FIX))) {
		/* Drop packet */
		LOGPRINT(pos_rpt, info, "qualify_pos: dropped");
		return 1;
	}

	filter_pos_heading(ai2_d2h);
	dev_pos->pps_state = g_pps_state;

	LOGPRINT(pos_rpt, info, "qualify_pos: passed");

	return 0;
}

struct d2h_ai2 d2h_pos_rpt = {
	.ai2_st_len = EXT_PVT_ST_LEN,
	.st_set_mem = pos_ext_setmem,
	.gen_struct = pos_ext_struct,
	.do_ie_4rpc = pos_ext_rpc_ie,
	.nvs_action = pos_nvs_info,
	.assess_qoi = qualify_pos
};


/*------------------------------------------------------------------------------
 * Add code to manage position report for buffered location fixes etc.
 *------------------------------------------------------------------------------
 */
/* 0x13 Report Diag String */
static int dpxy_decode_report_diag_str(struct d2h_ai2 *d2h_ai2)
{
	UNUSED(d2h_ai2);
	return 0;
}


	


static int rep_diag_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg, 
							unsigned int rei_select)
{
	UNUSED(rei_select);

	if(strcmp(PPS_ON_STRING,(char*) ai2_d2h->ai2_packet + 3) == 0){
			printf("PPS_ON_STRING %s\n",(char*) ai2_d2h->ai2_packet + 3 );
			g_pps_state = PPS_ON;
		}
	else if(strcmp(PPS_OFF_STRING,(char*) ai2_d2h->ai2_packet + 3) == 0){

			printf("PPS_OFF_STRING %s\n",(char*) ai2_d2h->ai2_packet + 3 );
			g_pps_state = PPS_OFF;
	}
	else if(strcmp(PPS_THRES_UNC_STRING,(char*) ai2_d2h->ai2_packet + 3) == 0){
			printf("PPS_THRES_UNC_STRING  %s\n", (char*) ai2_d2h->ai2_packet + 3);
			g_pps_state = PPS_THRES_UNC;
	}

	return 0;
}




struct d2h_ai2 d2h_rep_diag_str = {
	
	.ai2_st_len = 0,
	.gen_struct = dpxy_decode_report_diag_str,
	.do_ie_4rpc = rep_diag_rpc_ie
};

