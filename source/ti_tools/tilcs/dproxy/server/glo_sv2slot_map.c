/*
 * glo_sv2slot_map.c
 *
 * GLONASS Sv to slot mapping
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "device.h"
#include "ai2.h"
#include "config.h"
#include "logger.h"
#include "utils.h"
#include "inject.h"
#include "gnss.h"
#include "dev_proxy.h"
#include "glo_sv2slot_map.h"
#include "nvs.h"

extern int h2d_ai2_hdl;

int h2d_inj_glo_sv2slot_map(void *data, int num)
{
#define MAX_AI2_SV_SLOT_SIZE 53

	struct ai2_inj_glo_sv_slot_map_hrd {
		unsigned int id;
		unsigned int len;
		unsigned int subpkt_id;
		unsigned int nsvs;
	} sv_slot_hdr;

	struct ai2_inj_glo_sv_slot_map_byd {
		unsigned int svid;
		unsigned int slot;
	} sv_slot_bdy;

	char ai2_inj_glo_sv_slot_map_hdr_sizes[] = { 1, 2, 1, 1, 0};	
	char ai2_inj_glo_sv_slot_map_bdy_sizes[] = { 1, 1, 0};	

	struct dev_sv_slot_map sv_slot[24];
	unsigned char mem[MAX_AI2_SV_SLOT_SIZE];
	unsigned char *cpTemp = mem;
	int svs = 0,bufsize = 5, obj_count;

	LOGPRINT(generic, info, "h2d_inj_glo_sv2slot_map",svs);

	obj_count = nvs_rec_get(nvs_glo_sv_slot,sv_slot,0x00FFFFFF );
	if( obj_count != 24 ){
		LOGPRINT(generic, info, "No of svs [%d], Invalid", obj_count);
		goto ERROR;
	}

	sv_slot_hdr.id        = 0xFB;
	sv_slot_hdr.len       = (obj_count*2) + 2;
	sv_slot_hdr.subpkt_id = 0x2A;
	sv_slot_hdr.nsvs      = obj_count;

	if(d2h_ai2_encode((void*)&sv_slot_hdr, 	cpTemp,
			  ai2_inj_glo_sv_slot_map_hdr_sizes) != ai2_ok ){
		LOGPRINT(generic, info, "sv_slot map:Error in Header construction");
		goto ERROR;
	}

	cpTemp += 5;

	for(svs = 0; svs < obj_count; svs++ ){

		sv_slot_bdy.svid = sv_slot[svs].svid;
		sv_slot_bdy.slot = sv_slot[svs].slot;

		if(d2h_ai2_encode((void*)&sv_slot_bdy, cpTemp,
				  ai2_inj_glo_sv_slot_map_bdy_sizes) != ai2_ok ){

			LOGPRINT(generic, info, "sv_slot map:Error in Body construction %d",svs);
			goto ERROR;
		}
		bufsize += 2;
		cpTemp += 2;
	}	

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, bufsize)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(generic, info, "glo sv slot injected");
			return OK;
		}
	}

ERROR:
	LOGPRINT(generic, err, "error in sending ai2 pkt");
	return ERR;
}

int h2d_inj_req_glo_sv2slot_map(void *data, int num){

	struct ai2_req_glo_sv_slot_map {
		unsigned int id;
		unsigned int len;
		unsigned int subpkt_id;	
	} sv_slot[1];
	char ai2_req_glo_sv_slot_map_sizes[] = { 1, 2, 1, 0};
	unsigned char mem[4];
	
	sv_slot->id         =	0xFB;
	sv_slot->len        =	1;	
	sv_slot->subpkt_id	= 	0x2B;
	
	LOGPRINT(generic, info, "glo req sv2 slot mapping");
	
	d2h_ai2_encode((void *)sv_slot, mem, ai2_req_glo_sv_slot_map_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(generic, info, "glo sv slot requested");
			return OK;
		}
	}

	LOGPRINT(generic, err, "error sending ai2 packet to device");
	return ERR;

}
