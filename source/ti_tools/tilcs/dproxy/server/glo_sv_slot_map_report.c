/*
 * glo_sv_slot_map_report.c
 *
 * GLO SV to SLOT Map report
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
 */
#include "nvs.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "utils.h"
#include "ai2.h"
#include "glo_sv_slot_map_report.h"

#define PKT_LEN 73

int sv_slot_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0, index;
	struct dev_sv_slot_info *dev_sv_slot = (struct dev_sv_slot_info*) 
		ai2_d2h->ai2_struct;	
	struct dev_sv_slot_map glo_sv_map;

	LOGPRINT(generic, info, "storing sv slot map report in nvs");
	
	for (index = 0; index < 24; index++) {
		 glo_sv_map.svid   = dev_sv_slot[index].svid;
		 glo_sv_map.slot   = dev_sv_slot[index].slot;
		 glo_sv_map.status = dev_sv_slot[index].status;

		 LOGPRINT(generic, info, "sv_slot_nvs_info(): svid-%d, slot-%d, status-%d", 
			  glo_sv_map.svid, glo_sv_map.slot, glo_sv_map.status);

		wr_suc = nvs_rec_add(nvs_glo_sv_slot, &glo_sv_map, 1);

		if (wr_suc != nvs_success) {
			LOGPRINT(generic, err, "error storing sv slot info in nvs");
			return -1;
		}
	}

	return 0;
}

int sv_gen_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 1, index;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;
	struct dev_sv_slot_info *dev_sv_slot = (struct dev_sv_slot_info*) 
		ai2_d2h->ai2_struct;
	char  *ai2_pkt               = (char* )ai2_d2h->ai2_packet + ai2_offset;

	ai2_d2h->ai2_st_len = BUF_LE_U2B_RD_INC(ai2_pkt);
	
	if (ai2_d2h->ai2_st_len != 73) {
		LOGPRINT(generic, err, "unexpected pkt len %d, but expected is %d", 
			 ai2_d2h->ai2_st_len, PKT_LEN);
		return -1;
	}
	
	ai2_pkt++;
	
	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	LOGPRINT(generic, info, "sv slot: ai2 st len = %d",ai2_d2h->ai2_st_len);

	for (index = 0;index<24;index++) {
		dev_sv_slot[index].svid   = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_sv_slot[index].slot   = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_sv_slot[index].status = BUF_LE_U1B_RD_INC(ai2_pkt);
	}

	LOGPRINT(generic, info, "sv_gen_struct(): ai2 offset = %d",ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

#define GLO_SV_SLOT_RPT_ST_LEN (24*ST_SIZE(dev_sv_slot_map))

struct d2h_ai2 d2h_sv_slot_rpt = {

	.ai2_st_len = GLO_SV_SLOT_RPT_ST_LEN,

	.gen_struct = sv_gen_struct,
	.do_ie_4rpc = 0,
	.nvs_action = sv_slot_nvs_info,
	.assess_qoi = 0
};

int sv_glo_report(void *data)
{
	int wr_suc = 0, index;
	char *ai2_pkt = ((char *)data) + 4;
	struct dev_sv_slot_info dev_sv_slot[1];

	for(index = 0; index < 24; index++) {

		dev_sv_slot->svid   = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_sv_slot->slot   = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_sv_slot->status = BUF_LE_U1B_RD_INC(ai2_pkt);	

		LOGPRINT(generic, info, "sv_glo_report(): svid-%d, slot-%d, status-%d", 
			 dev_sv_slot->svid, dev_sv_slot->slot, 
			 dev_sv_slot->status);

		wr_suc = nvs_rec_add(nvs_glo_sv_slot, dev_sv_slot, 1);
		if (wr_suc != nvs_success) {
			LOGPRINT(generic, err, "error storing sv slot info in nvs");
			return -1;
		}
	}

	return 0;
}

