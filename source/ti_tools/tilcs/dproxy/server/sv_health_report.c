/*
 * sv_health_report.c
 *
 * Satellite health report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "nvs.h"
#include "log_report.h"

/* Functions that are offering services outside. */
int health_stat_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  unsigned short num_sat, 
								  struct dev_sv_hlth *sv_hlth);

static struct ie_desc *mk_ie_adu_4health_stat_dev(struct rpc_msg *rmsg,
												  unsigned char oper_id, 
												  struct dev_sv_hlth *sv_hlth)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_sv_hlth);

	struct dev_sv_hlth *n_sv_hlth = (struct dev_sv_hlth *)malloc(st_sz);
	if(!n_sv_hlth)
		goto exit1;

	memcpy(n_sv_hlth, sv_hlth, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_SV_HLT, oper_id, st_sz, n_sv_hlth);
	if(!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(n_sv_hlth);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int health_stat_create_dev_ie(struct rpc_msg *rmsg,
									 unsigned char oper_id,
									 struct dev_sv_hlth *sv_hlth)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4health_stat_dev(rmsg, oper_id, sv_hlth);
	if(!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while(--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int health_stat_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  unsigned short num_sat, 
								  struct dev_sv_hlth *sv_hlth)
{
	UNUSED(num_sat);
	return health_stat_create_dev_ie(rmsg, oper_id, sv_hlth);
}

static int health_stat_rpc_ie(struct d2h_ai2 *ai2_d2h,
				struct rpc_msg *rmsg, unsigned int rei_select)
{
	struct dev_sv_hlth *sv_hlth =
		(struct dev_sv_hlth*) ai2_d2h->ai2_struct;

	if(-1 == health_stat_create_dev_ie(rmsg, OPER_INF, sv_hlth))
		return -1;

	UNUSED(rei_select);

	return 0;
}

static int hlth_stat_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_sv_hlth *sv_hlth = (struct dev_sv_hlth *) ai2_d2h->ai2_struct;
	char *ai2_pkt = (char *) ai2_d2h->ai2_packet + ai2_offset;
	char *ai2_ref = ai2_pkt;
	unsigned short update_week = BUF_LE_U2B_RD_INC(ai2_pkt);
	unsigned int update_msecs = BUF_LE_U4B_RD_INC(ai2_pkt);
	unsigned short dev_msr_sz = 1;
	int num_sat = (ai2_d2h->ai2_pkt_sz - (ai2_offset + 6)) / dev_msr_sz;
	unsigned char *sv_hlth_stat = sv_hlth->health_status;

	sv_hlth->update_week = update_week;
	sv_hlth->update_msecs = update_msecs;
	sv_hlth->num_sat = num_sat;

	while(num_sat--) {

		*sv_hlth_stat = BUF_LE_U1B_RD_INC(ai2_pkt);
		sv_hlth_stat++;
	}

	print_data(REI_DEV_SV_HLT, sv_hlth);

	return (ai2_pkt - ai2_ref);
}

static int health_stat_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += hlth_stat_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int health_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0;
	struct dev_sv_hlth *sv_hlth = (struct dev_sv_hlth *)ai2_d2h->ai2_struct;

	LOGPRINT(hlt_rpt, info, "storing sv health to nvs");

	wr_suc = nvs_rec_add(nvs_hlth, sv_hlth, 1);
	if (wr_suc != nvs_success) {
		LOGPRINT(hlt_rpt, err, "error storing sv health in nvs");
		return -1;
	}

	return 0;
}

#define EXT_PVT_ST_LEN  ST_SIZE(dev_sv_hlth)

struct d2h_ai2 d2h_health_stat_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.gen_struct = health_stat_ext_struct,
//	.do_ie_4rpc = health_stat_rpc_ie,
	.do_ie_4rpc = 0,
	.nvs_action = health_nvs_info
};

