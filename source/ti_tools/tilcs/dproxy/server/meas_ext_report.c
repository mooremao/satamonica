/*
 * meas_ext_report.c
 *
 * Extended measurement report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#include <stdlib.h>
#include <string.h>

#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "dev_proxy.h"
#include "ai2.h"
#include "logger.h"
#include "log_report.h"
#include "os_services.h"
#include "nvs.h"
#include "nvs_validate.h"
#include "msr_qualify.h"
#include "device.h"
#include "config.h"
#include "nvs_utils.h"

#define FLOAT(x)  ((float) x)
#define DOUBLE(x) ((double) x)

extern unsigned char g_pps_state;
struct dev_sat_meas {

	unsigned short       num_sat;
	struct dev_msr_info_PVT *dev_msr;
};

struct dev_meas_rpt {

	struct dev_gps_time  time;
	struct dev_sat_meas  sat_meas;
};

/* Extended type. */
struct d2h_ai2_msr
{
	struct d2h_ai2 base;
	void *private_field;
};
 int clk_gps_create_clk_ie(struct rpc_msg *rmsg, unsigned short oper_id,
								 struct dev_gps_time *gps_tim, 
								 unsigned rei_select);


static int setup_rssi_val(struct dev_msr_info *new_msr,struct dev_msr_info_PVT *msr )
{
	/*Added for Spur detection*/
#define RF_GAIN_INT 91.02
#define RF_GAIN_EXT 83.49 
	unsigned int *inj_ctrl;		
	signed char rssi;
	float  rf_gain;


	LOGPRINT(msr_rpt, info,"setup_rssi_val	 %x\n", msr->rf_reg_val );
		
	if(msr->rf_reg_val > 127)
		rssi = msr->rf_reg_val  -255;
	else 
		rssi = msr->rf_reg_val ;

	
	rf_gain = RF_GAIN_INT;

	sys_param_get(cfg_lnaext_cryt,(void**) &inj_ctrl);

	if(*inj_ctrl)
		rf_gain = RF_GAIN_EXT;
		
	new_msr->rssi_dbm = 10 * log10(pow(2, (rssi/8.0))) - rf_gain; 

	return 0;

}
/*------------------------------------------------------------------------------
 * Processing of SAT Report from device and support of auxilliary functions.
 *------------------------------------------------------------------------------
 */
static struct ie_desc *mk_ie_adu_4msr_dev_gps(struct rpc_msg *rmsg, 
											  unsigned short oper_id, 
											  struct dev_sat_meas *sat_meas,
											  struct dev_gps_time *gps_tim )
{
	struct ie_desc *ie_adu;
	int num_sat = 0;
	int st_sz = 0;
	int count;
	struct dev_msr_info_PVT *msr = sat_meas->dev_msr;
	struct dev_msr_info *new_msr, *new_msr_base;

	for (count = 0; count < sat_meas->num_sat; count++) {
		if (msr->svid >= 1 && msr->svid <= 32)
			num_sat++;

		msr++;
	}

	st_sz = ST_SIZE(dev_msr_info) * num_sat;
	msr = sat_meas->dev_msr;

	new_msr_base = new_msr = (struct dev_msr_info *) malloc(st_sz);
	if(!new_msr)
		goto mk_msr_dev_exit1;

	for (count = 0; count < sat_meas->num_sat; count++) {
		if (msr->svid >= 1 && msr->svid <= 32) {

			new_msr->gnss_id = msr->gnss_id;
			new_msr->svid = msr->svid;
			new_msr->snr = msr->snr;
			new_msr->cno = msr->cno;
			new_msr->latency_ms = msr->latency_ms;
			new_msr->msec = msr->msec;
			new_msr->sub_msec = msr->sub_msec;
			new_msr->time_unc = msr->time_unc;
			new_msr->speed = msr->speed;
			new_msr->speed_unc = msr->speed_unc;
			new_msr->sv_slot = msr->sv_slot;
			new_msr->elevation = msr->elevation;
			new_msr->azimuth = msr->azimuth;
			new_msr->meas_flags = msr->msr_flags;
			new_msr->meas_qual = msr->ch_states;
			setup_rssi_val(new_msr, msr);

			new_msr++;
		}

		msr++;
	}

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_MSR_GPS, oper_id, st_sz, new_msr_base);
	if(!ie_adu)
		goto mk_msr_dev_exit2;

	return ie_adu;

mk_msr_dev_exit2:
	free(new_msr);

mk_msr_dev_exit1:
	return NULL;
}

static unsigned char pseudoRMS_formatter(float rms_meters)
{
	float values[64];
	unsigned char cnt, mantissa, exponent;
	unsigned short word;
	unsigned char ret_val = 0;

	for (cnt = 0; cnt < 64; cnt++) {

		mantissa = (unsigned char)(cnt & 0x07);
		exponent = (unsigned char)((cnt >> 3) & 0x07);

		word = (unsigned short)mantissa;

		word <<= 5;
		word += 0x0100;
		word <<= exponent;
		word >>= 1;

		values[cnt] = (float)(word >> 8) + 
			(float)(word & 0xFF) * 0.00390625f ;
	}

	if (rms_meters < values[0])
		ret_val = 0;
	else if (rms_meters >= values[62])
		ret_val = 63;
	else {
		for (cnt = 1; cnt < 63; cnt++) {
			if ((rms_meters < values[cnt]) &&
				(rms_meters >= values[cnt - 1])) {
				ret_val = cnt;
				break;
			}
		}
	}

	return ret_val;
}

static unsigned short setup_3gpp_gps_msr(struct gps_msr_info *tgt, struct dev_sat_meas *src, struct dev_gps_time *gps_tim )
{
#define CA_CHIPS_MSEC      (1023.0f)
#define CA_FREQ            (1000.0f * CA_CHIPS_MSEC)
#define LIGHT_SEC          (299792458.0f)
#define C_SPEED_LIGHT_NSEC (LIGHT_MSEC * 0.001f * 0.001f)
#define L1_FREQ            (1540.0f * CA_FREQ)
#define CA_WAVELENGTH      (LIGHT_SEC / CA_FREQ)
#define L1_WAVELENGTH      (LIGHT_SEC / L1_FREQ)
#define C_MSEC_PER_CHIP    (1.0f / 1023.0f)
#define C_MSEC_FRAC_CHIP   (C_MSEC_PER_CHIP / 1024.0f)
#define C_LSB_SV_SUBMS     (double)(1.0f / 16777216.0f)
#define  MEAS_STATUS_NULL                                    0
            /* The don't know anything state */
#define  MEAS_STATUS_SM_VALID               (1<<0)
            /* TRUE -> f_SatSubMs is valid */
#define  MEAS_STATUS_SB_VALID                (1<<1)
            /* TRUE -> sub bit time known */
#define  MEAS_STATUS_MS_VALID               (1<<2)
            /* TRUE -> satellite time is known */
#define  MEAS_STATUS_BE_CONFIRM          (1<<3) 
            /* TRUE -> bit edge confirmed from signal */
#define  MEAS_STATUS_VE_VALID                (1<<4)
            /* TRUE -> measured velocity */
#define  MEAS_STATUS_VE_FINE                   (1<<5)
            /* TRUE/FALSE -> fine/coarse velocity */
#define  MEAS_STATUS_CONT_LOCK                        (1<<6)
            /* TRUE -> Channel has continous lock */
#define  MEAS_STATUS_HEALTHY                 (1<<7)
            /* TRUE -> Satellite is healthy */
#define  MEAS_STATUS_GOOD_DATA               (1<<8)
            /* TRUE -> Good satellite data parity */
#define  MEAS_STATUS_FROM_DIFF                         (1<<9)
            /* TRUE -> Last update from difference, not meas */
#define  MEAS_STATUS_UNCERTAIN                         (1<<10)
            /* TRUE -> Weak SV but can't do definite XCORR test */
#define  MEAS_STATUS_XCORR                                 (1<<11)
            /* TRUE -> Strong indication of cross correlation */
#define  MEAS_STATUS_MULTIPATH                         (1<<12)
            /* TRUE -> Had multipath indicators */
#define  MEAS_STATUS_DONT_USE              (1<<13)
            /* TRUE -> Dubious measurement, wait for another dwell */

            struct dev_msr_info_PVT *dev_msr = src->dev_msr;
            double sub_msec;
            struct dev_gps_time dev_clk[1];
            unsigned short mantissa;
            unsigned char exponent;
            float freq_num;
            int count;
            float init_time;
            int latency;
			unsigned short sv_cnt = 0;
			float fdoppler;
			double fwholechips;
			double ffracchips;
			double svTime = 0;
			int svSpeed = 0;
			int svTimbias = 0;
			double svTime_conv ;
			
			nvs_rec_get(nvs_gps_clk, dev_clk, 0x01);
			LOGPRINT(msr_rpt, info, "setup_3gpp_gps_msr:");
			
#ifdef VERBOSE_LOGGING
			LOGPRINT(msr_rpt, info, "freq_bias = %d\t time_bias = %d", dev_clk->freq_bias, dev_clk->time_bias);
#endif
			tgt->TCount = dev_msr->Tcount;
			LOGPRINT(msr_rpt, info,"MEAS_REPORT TCOUNT : %d \n", dev_msr->Tcount);
            for (count = 0; count < src->num_sat; count++) {
#ifdef VERBOSE_LOGGING
			LOGPRINT(msr_rpt, info, "svid:",dev_msr->svid);	
#endif
				if (dev_msr->svid >= 1 && dev_msr->svid <= 32) {

                    init_time = dev_msr->post_int * dev_msr->pre_int;

                    /* compute latency */
                    if(init_time <= 4100)
                                latency = 5001;
                    else if  (init_time <= 17000)
                                latency = 17001; /* 16s integration */
                    else      
                                latency = 32001;  /*int gr8 er than 16s*/

#ifdef VERBOSE_LOGGING
                    LOGPRINT(msr_rpt, info, "before cn0 [%d]", dev_msr->cno);
					LOGPRINT(msr_rpt, info, "smvalid: %u diff: %u use: %u xcorr: %u latency: %d", 
								(dev_msr->msr_flags & MEAS_STATUS_SM_VALID),
								(~dev_msr->msr_flags & MEAS_STATUS_FROM_DIFF),
                                (~dev_msr->msr_flags & MEAS_STATUS_DONT_USE),
                                (~dev_msr->msr_flags & MEAS_STATUS_XCORR),
                                (latency));
#endif		
                    /* check if it is good observation */
                    if ((dev_msr->msr_flags & MEAS_STATUS_MS_VALID) &&
                                (~dev_msr->msr_flags & MEAS_STATUS_FROM_DIFF) &&
                                (~dev_msr->msr_flags & MEAS_STATUS_DONT_USE) &&
                                (~dev_msr->msr_flags & MEAS_STATUS_XCORR) &&
                                (dev_msr->latency_ms < latency) &&
                                (dev_msr->cno > 50)) {

                                tgt->svid = dev_msr->svid -1;
                                tgt->c_no = (dev_msr->cno + 5) / 10;
#ifdef VERBOSE_LOGGING
                                LOGPRINT(msr_rpt, info, "after cn0 [%d]", tgt->c_no);
#endif
								fdoppler = (((FLOAT(dev_msr->speed) * 0.01f) - FLOAT(dev_clk->freq_bias)) 
                                                * (1.0f / L1_WAVELENGTH) * (-1.0f));
												
								tgt->doppler = 	(short)(fdoppler * 5.0f);
								/*Signed Ext : KNS*/
								svSpeed = (int)((dev_msr->speed & 0x800000)?(0xFF000000 | dev_msr->speed): dev_msr->speed) ;
								tgt->doppler = ((float)svSpeed * 0.01f);

                                sub_msec = (DOUBLE((dev_msr->sub_msec) + (dev_clk->time_bias)) * C_LSB_SV_SUBMS );
#ifdef VERBOSE_LOGGING
								LOGPRINT(msr_rpt, info, "dev_msr->sub_msec = %u sub_msec = %ld", dev_msr->sub_msec, sub_msec);
#endif		
                                if (sub_msec > 1.0f)
                                            sub_msec -= 1.0f;
                                else if (sub_msec < 0)
                                            sub_msec = 1.0f + sub_msec;

                                sub_msec = 1.0f - sub_msec;
#ifdef VERBOSE_LOGGING
								LOGPRINT(msr_rpt, info, "sub_msec = %ld ", sub_msec);
#endif
								mantissa = dev_msr->time_unc >> 5;
								exponent = dev_msr->time_unc & 0x001F;
								freq_num = os_service_ldexp(DOUBLE(mantissa), DOUBLE(exponent)) 
											* C_SPEED_LIGHT_NSEC;
							

								/*SRK: Added for DR intr*/
								svTime = DOUBLE(dev_msr->msec) + DOUBLE(dev_msr->sub_msec * C_LSB_SV_SUBMS);
								LOGPRINT(msr_rpt, info, "dev_msr->msec %10d \n",dev_msr->msec );
								LOGPRINT(msr_rpt, info, " dev_msr->sub_msec= %10d\n", dev_msr->sub_msec);
								LOGPRINT(msr_rpt, info, " converted dev_msr->sub_msec= %f\n",DOUBLE(dev_msr->sub_msec * C_LSB_SV_SUBMS));
								LOGPRINT(msr_rpt, info, " converted dev_msr->sub_msec= %f\n",DOUBLE(dev_msr->sub_msec)/ pow(2,24));
								LOGPRINT(msr_rpt, info, "converted SvTime %f \n",svTime);
						

								/*KNS: Subtract time bias*/
								
								svTimbias = (int)((gps_tim->time_bias & 0x800000)?(0xFF000000 | gps_tim->time_bias):gps_tim->time_bias);
								
								if(gps_tim->time_bias > 8388607 )
								{
									svTimbias = DOUBLE(gps_tim->time_bias - 16777216.0f);
								}
								LOGPRINT(msr_rpt, info, "gps_tim->time_bias non converted %10d \n",gps_tim->time_bias );
								
								LOGPRINT(msr_rpt, info, "svTimeBias %d\n", svTimbias);
								
								LOGPRINT(msr_rpt, info, "gps_tim->time_msec %d\n", gps_tim->time_msec);
								
								LOGPRINT(msr_rpt, info, "svTimeBias converted %f\n", (DOUBLE(svTimbias) *C_LSB_SV_SUBMS ));
								svTime_conv = (DOUBLE(svTimbias) *C_LSB_SV_SUBMS );

								LOGPRINT(msr_rpt, info, "Pseudo range in m/s %f\n", DOUBLE(( (gps_tim->time_msec - svTime	)- svTime_conv) ));

								tgt->pseudo_msr = DOUBLE(( (gps_tim->time_msec - svTime	)- svTime_conv) * LIGHT_MSEC);
								
								LOGPRINT(msr_rpt, info, "##################### Pseudo Range calculations ####################################\n");
								LOGPRINT(msr_rpt, info, "Pseudo range in  meters%f\n", tgt->pseudo_msr);

								tgt->pseudo_msr_unc = freq_num;
								tgt->doppler_unc = (dev_msr->speed_unc * 0.01);
								
								tgt->meas_flags = dev_msr->msr_flags;
								tgt->msr_qual = dev_msr->ch_states;
								tgt->snr = dev_msr->snr * 0.1;
								tgt->pseudo_range_residuals = dev_msr->pos_residuals * 0.1f;
								tgt->doppler_residuals = dev_msr->velocity_residuals * 0.1f;

								
								tgt->azimuth_deg = 
											ceilf(((float)dev_msr->azimuth) * 45.0f / 32.0f * 100.0f) / 100;
								tgt->elevate_deg = 
											ceilf(((float)dev_msr->elevation) * 45.0f / 64.0f * 100.0f) / 100;
                                print_data(REI_MSR_GPS, tgt);
								if(tgt->msr_qual == 4){
	                                tgt++;
									sv_cnt++;
								}
                    	}
				}
            	dev_msr++;
           	}
	return sv_cnt;
}

static struct ie_desc *mk_ie_adu_4msr_gps(struct rpc_msg *rmsg, 
										  unsigned short oper_id, 
										  struct dev_sat_meas *sat_meas,
										  struct dev_gps_time *gps_tim )
{
	struct ie_desc *ie_adu;
	int st_sz;
	struct dev_msr_info_PVT *msr = sat_meas->dev_msr;
	struct gps_msr_info *new_msr;
	int num_sat = 0;
	int count;
	unsigned short sv_cnt;

	for (count = 0; count < sat_meas->num_sat; count++) {
		if (msr->svid >= 1 && msr->svid <= 32)
			num_sat++;

		msr++;
	}

	st_sz = ST_SIZE(gps_msr_info) * num_sat;
	msr = sat_meas->dev_msr;

	new_msr = (struct gps_msr_info *) malloc(st_sz);
	if(!new_msr)
		goto mk_msr_dev_exit1;

	sv_cnt = setup_3gpp_gps_msr(new_msr, sat_meas, gps_tim );	
	st_sz = ST_SIZE(gps_msr_info) * sv_cnt;

	LOGPRINT(msr_rpt,info,"sv_cnt = %d, st_sz = %d", sv_cnt, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_MSR_GPS, oper_id, st_sz, new_msr);
	if(!ie_adu)
		goto mk_msr_dev_exit2;

	return ie_adu;

mk_msr_dev_exit2:
	free(new_msr);

mk_msr_dev_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4msr_dev_glo(struct rpc_msg *rmsg, 
											  unsigned short oper_id, 
											  struct dev_sat_meas *sat_meas,
											   struct dev_gps_time *gps_tim )
{
	struct ie_desc *ie_adu;
	int num_sat = 0;
	int st_sz = 0;
	struct dev_msr_info_PVT *msr = sat_meas->dev_msr;
	struct dev_msr_info *new_msr, *new_msr_base;
	int count;

	for (count = 0; count < sat_meas->num_sat; count++) {
		if( (msr->svid >= 65 && msr->svid <= 88 ) ||  (msr->svid >= 120 && msr->svid <= 138 ) || (msr->svid >= 193 && msr->svid <= 202 )){
			num_sat++;
		}
		msr++;
	}

	st_sz = ST_SIZE(dev_msr_info) * num_sat;
	msr = sat_meas->dev_msr;

	new_msr_base = new_msr = (struct dev_msr_info *) malloc(st_sz);
	if(!new_msr)
		goto mk_msr_dev_exit1;

	for (count = 0; count < sat_meas->num_sat; count++) {
		if( (msr->svid >= 65 && msr->svid <= 88)||  (msr->svid >= 120 && msr->svid <= 138 ) || (msr->svid >= 193 && msr->svid <= 202 )) {

			new_msr->gnss_id = msr->gnss_id;
			new_msr->svid = msr->svid;
			new_msr->snr = msr->snr;
			new_msr->cno = msr->cno;
			new_msr->latency_ms = msr->latency_ms;
			new_msr->msec = msr->msec;
			new_msr->sub_msec = msr->sub_msec;
			new_msr->time_unc = msr->time_unc;
			new_msr->speed = msr->speed;
			new_msr->speed_unc = msr->speed_unc;
			new_msr->sv_slot = msr->sv_slot;
			new_msr->elevation = msr->elevation;
			new_msr->azimuth = msr->azimuth;
			new_msr->meas_flags=msr->msr_flags;
			new_msr->meas_qual = msr->ch_states;
			new_msr++;
		}

		msr++;
	}

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_MSR_GLO, oper_id, st_sz, new_msr_base);
	if(!ie_adu)
		goto mk_msr_dev_exit2;

	return ie_adu;

mk_msr_dev_exit2:
	free(new_msr);

mk_msr_dev_exit1:
	return NULL;
}

static unsigned short setup_3gpp_glo_msr(struct glo_msr_info *tgt, struct dev_sat_meas *src,  struct dev_gps_time *gps_tim )
{
	int sv_cnt = 0;
	struct dev_msr_info_PVT *dev_msr = src->dev_msr;
	struct ganss_msr_info *msr;
	int count;
	unsigned short mantissa;
	unsigned char exponent;
	float freq_num;
	struct dev_glo_time dev_clk[1];
	double svTime = 0;
	int svSpeed = 0;
	int svTimbias = 0;
	double svTime_conv ;
	double sub_msec;
	#define C_LSB_SV_SUBMS     (double)(1.0f / 16777216.0f)
	#define LIGHT_SEC          (299792458.0f)
	#define C_SPEED_LIGHT_NSEC (LIGHT_MSEC * 0.001f * 0.001f)

	nvs_rec_get(nvs_glo_clk, dev_clk, 0);

	for (count  = 0; count < src->num_sat; count++) {

		msr = &tgt->msr;

		if ((dev_msr->svid >= 65 && dev_msr->svid <= 88)||  (dev_msr->svid >= 120 && dev_msr->svid <= 138 ) || (dev_msr->svid >= 193 && dev_msr->svid <= 202 ))
			{

			msr->svid = dev_msr->svid;
			//msr->c_no = dev_msr->cno;
			msr->c_no = (dev_msr->cno + 5) / 10;
			mantissa = dev_msr->time_unc >> 5;
			exponent = dev_msr->time_unc & 0x001F;
			freq_num = os_service_ldexp(DOUBLE(mantissa), DOUBLE(exponent)) 
				* C_SPEED_LIGHT_NSEC;


			//msr->carrier_qual = dev_msr->;
			//msr->code_phase = dev_msr->;
			//msr->integer_cp = dev_msr->;
			//msr->cp_rms_err = pseudoRMS_formatter(freq_num);

			

			msr->doppler = (short)(((FLOAT(dev_msr->speed) * 0.01f) 
									- FLOAT(dev_clk->freq_bias)) 
								   * (1.0f / L1_WAVELENGTH) * (-1.0f) * 5.0f);

			/*Doppler calc for DR*/
			svSpeed = (int)((dev_msr->speed & 0x800000)?(0xFF000000 | dev_msr->speed): dev_msr->speed) ;
			msr->doppler = ((float)svSpeed * 0.01f);
											
			sub_msec = (DOUBLE((dev_msr->sub_msec) + (dev_clk->time_bias)) * C_LSB_SV_SUBMS );
#ifdef VERBOSE_LOGGING
			LOGPRINT(msr_rpt, info, "dev_msr->sub_msec = %u sub_msec = %ld", dev_msr->sub_msec, sub_msec);
#endif		
			if (sub_msec > 1.0f)
							sub_msec -= 1.0f;
			else if (sub_msec < 0)
						sub_msec = 1.0f + sub_msec;

			sub_msec = 1.0f - sub_msec;
#ifdef VERBOSE_LOGGING
			LOGPRINT(msr_rpt, info, "sub_msec = %ld ", sub_msec);
#endif
			/*SRK: Added for DR intr*/
			svTime = DOUBLE(dev_msr->msec) + DOUBLE(dev_msr->sub_msec * C_LSB_SV_SUBMS);
			LOGPRINT(msr_rpt, info, "dev_msr->msec %10d \n",dev_msr->msec );
			LOGPRINT(msr_rpt, info, " dev_msr->sub_msec= %10d\n", dev_msr->sub_msec);
			LOGPRINT(msr_rpt, info, " converted dev_msr->sub_msec= %f\n",DOUBLE(dev_msr->sub_msec * C_LSB_SV_SUBMS));
			LOGPRINT(msr_rpt, info, " converted dev_msr->sub_msec= %f\n",DOUBLE(dev_msr->sub_msec)/ pow(2,24));
			LOGPRINT(msr_rpt, info, "converted SvTime %f \n",svTime);
	

			/* Subtract time bias*/
			
			svTimbias = (int)((gps_tim->time_bias & 0x800000)?(0xFF000000 | gps_tim->time_bias):gps_tim->time_bias);
			
			if(gps_tim->time_bias > 8388607 )
			{
				svTimbias = DOUBLE(gps_tim->time_bias - 16777216.0f);
			}
			LOGPRINT(msr_rpt, info, "gps_tim->time_bias non converted %10d \n",gps_tim->time_bias );
			
			LOGPRINT(msr_rpt, info, "svTimeBias %d\n", svTimbias);
			
			LOGPRINT(msr_rpt, info, "gps_tim->time_msec %d\n", gps_tim->time_msec);
			
			LOGPRINT(msr_rpt, info, "svTimeBias converted %f\n", (DOUBLE(svTimbias) *C_LSB_SV_SUBMS ));
			svTime_conv = (DOUBLE(svTimbias) *C_LSB_SV_SUBMS );

			LOGPRINT(msr_rpt, info, "Pseudo range in m/s %f\n", DOUBLE(( (gps_tim->time_msec - svTime	)- svTime_conv) ));

			msr->pseudo_msr = DOUBLE(( (gps_tim->time_msec - svTime )- svTime_conv) * LIGHT_MSEC);
			
			LOGPRINT(msr_rpt, info, "##################### Pseudo Range calculations ####################################\n");
			LOGPRINT(msr_rpt, info, "Pseudo range in  meters%f\n", msr->pseudo_msr);

			msr->pseudo_msr_unc = freq_num;
			msr->doppler_unc = (dev_msr->speed_unc * 0.01);
			
			msr->meas_flags = dev_msr->msr_flags;
			msr->msr_qual = dev_msr->ch_states;
			msr->snr = dev_msr->snr * 0.1;
			msr->pseudo_range_residuals = dev_msr->pos_residuals * 0.1f;
			msr->doppler_residuals = dev_msr->velocity_residuals * 0.1f;
			
			msr->azimuth_deg = 
						ceilf(((float)dev_msr->azimuth) * 45.0f / 32.0f * 100.0f) / 100;
			msr->elevate_deg = 
						ceilf(((float)dev_msr->elevation) * 45.0f / 64.0f * 100.0f) / 100;

			//msr->adr = dev_msr->;

			print_data(REI_MSR_GANSS, tgt);
			if(msr->msr_qual == 4){
				tgt++;
				sv_cnt++;
			}
		}

		dev_msr++;
	}
	return sv_cnt;
}

static struct ie_desc *mk_ie_adu_4msr_glo(struct rpc_msg *rmsg, 
										  unsigned short oper_id, 
										  struct dev_sat_meas *sat_meas, 
										   struct dev_gps_time *gps_tim )
{
	struct ie_desc *ie_adu;
	int st_sz;
	struct dev_msr_info_PVT *msr = sat_meas->dev_msr;
	struct glo_msr_info *new_msr;
	int num_sat = 0;
	int count;
	unsigned short sv_cnt;

	for (count = 0; count < sat_meas->num_sat; count++) {
		if ((msr->svid >= 65 && msr->svid <= 88) ||  (msr->svid >= 120 && msr->svid <= 138 ) || (msr->svid >= 193 && msr->svid <= 202 ))
			num_sat++;

		msr++;
	}

	st_sz = ST_SIZE(glo_msr_info) * num_sat;
	msr = sat_meas->dev_msr;

	new_msr = (struct glo_msr_info *) malloc(st_sz);
	if(!new_msr)
		goto mk_msr_dev_exit1;

	sv_cnt = setup_3gpp_glo_msr(new_msr, sat_meas, gps_tim);
	st_sz = ST_SIZE(ganss_msr_info) * sv_cnt;

	//ie_adu = rpc_ie_attach(rmsg, REI_MSR_GLO_SG1, oper_id, st_sz, new_msr);
	ie_adu = rpc_ie_attach(rmsg, REI_MSR_GANSS, oper_id, st_sz, new_msr);
	if(!ie_adu)
		goto mk_msr_dev_exit2;

	return ie_adu;

mk_msr_dev_exit2:
	free(new_msr);

mk_msr_dev_exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4msr_rslt_gnss(struct rpc_msg *rmsg, 
												unsigned short oper_id, 
												struct dev_msr_result *msr_rpt_results)
{
	struct ie_desc *ie_adu;
	struct dev_msr_result * new_msr_rpt_results;
	int st_sz = ST_SIZE(dev_msr_result);

	new_msr_rpt_results = (struct dev_msr_result *) malloc(st_sz);
	if(!new_msr_rpt_results)
		goto exit1;

	memcpy(new_msr_rpt_results, msr_rpt_results, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_MSR_RSLT, oper_id, st_sz, 
						   new_msr_rpt_results);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_msr_rpt_results);

exit1:
	return NULL;

}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error.*/
static int ext_msr_create_msr_ie(struct rpc_msg *rmsg, unsigned short oper_id,
								 struct dev_sat_meas *sat_meas, 
								 struct dev_gps_time *gps_tim ,
								 unsigned int rei_select)
{
	struct ie_desc *ie_adu[4];
	int n_adu = 0;

	if(rei_select & MSR_RPT_GPS_NATIVE) {
		ie_adu[n_adu] = mk_ie_adu_4msr_dev_gps(rmsg, oper_id, sat_meas, gps_tim);
		if(!ie_adu[n_adu++])
			goto exit1;
	}

	if(rei_select & MSR_RPT_GPS_IE_SEL) {
		ie_adu[n_adu] = mk_ie_adu_4msr_gps(rmsg, oper_id, sat_meas, gps_tim);
		if(!ie_adu[n_adu++])
			goto exit1;
	}

	if(rei_select & MSR_RPT_GLO_NATIVE) {
		ie_adu[n_adu] = mk_ie_adu_4msr_dev_glo(rmsg, oper_id, sat_meas, gps_tim);
		if(!ie_adu[n_adu++])
			goto exit1;
	}

	if(rei_select & MSR_RPT_GLO_IE_SEL) {
		ie_adu[n_adu] = mk_ie_adu_4msr_glo(rmsg, oper_id, sat_meas, gps_tim);
		if(!ie_adu[n_adu++])
			goto exit1;
	}

	return n_adu;

exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error.*/
static int ext_msr_result_create_msr_ie(struct rpc_msg *rmsg, unsigned short oper_id,
					struct d2h_ai2_msr *msr, unsigned int rei_select)
{
	struct d2h_ai2 *ai2_d2h = &msr->base;
	struct dev_msr_result *msr_rpt_results = (struct dev_msr_result *)msr->private_field;
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt*) ai2_d2h->ai2_struct;
	struct dev_gps_time *gps_tim = &rpt->time;
	struct ie_desc *ie_adu[1];
	int n_adu = 0;

	msr_rpt_results->gps_ref_secs = gps_tim->week_num * MAX_SECS_IN_1WEEK;
	msr_rpt_results->gps_ref_msec = gps_tim->time_msec;

	//msr_rpt_results->glo_ref_secs = ;
	//msr_rpt_results->glo_ref_msec = ;

	if(rei_select & MSR_RPT_DEV_RESULT) {
		ie_adu[n_adu] = mk_ie_adu_4msr_rslt_gnss(rmsg, oper_id, 
							 msr_rpt_results);
		if(!ie_adu[n_adu++])
			goto exit1;
	}

	return n_adu;

exit1:
	return 0;
}


int meas_clk_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  unsigned int rei_select)
{
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt*) ai2_d2h->ai2_struct;
	struct dev_gps_time *gps_tim = &rpt->time;
	
	rei_select = POS_RPT_TIM_GPS_NATIVE | POS_RPT_TIM_GPS_IE_SEL;

	if (gps_tim->week_num != 65535) {
		LOGPRINT(tim_rpt, info, "meas_ext_report gps clock passed");
	} else {
		LOGPRINT(tim_rpt, info, "meas_ext_report gps clock dropped");
		return 0;
	}

	if(-1 == clk_gps_create_clk_ie(rmsg, OPER_INF, gps_tim, rei_select))
			return -1;

	return 0;
}

/*------------------------------------------------------------------------------
 * Processing of MSR Report from device and handlers required by D2H construct.
 *------------------------------------------------------------------------------
 */
static int ext_msr_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
						  unsigned int rei_select)
{
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt*) ai2_d2h->ai2_struct;
	struct d2h_ai2_msr *msr = (struct d2h_ai2_msr *) ai2_d2h;

	if (-1 == meas_clk_rpc_ie(ai2_d2h, rmsg, rei_select)){
		return -1;
	}

	/* MSR Report filtered results */
	if(-1 == ext_msr_result_create_msr_ie(rmsg, OPER_INF, msr,
										  rei_select)) {
		return -1;
	}

	/* MSR Reports extraced from the ai2 structs */
	if(-1 == ext_msr_create_msr_ie(rmsg, OPER_INF, &rpt->sat_meas, &rpt->time,
								   rei_select)) {
		return -1;
	}

	return 0;
}

static int dev_tim_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_meas_rpt *rpt = ai2_d2h->ai2_struct;
	struct dev_gps_time *tim_gps = &rpt->time;
	char  *ai2_pkt             = (char *) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref             = ai2_pkt;

	tim_gps->timer_count         = BUF_LE_U4B_RD_INC(ai2_pkt);
	tim_gps->week_num            = BUF_LE_U2B_RD_INC(ai2_pkt);
	tim_gps->time_msec           = BUF_LE_U4B_RD_INC(ai2_pkt);
	tim_gps->time_bias           = BUF_LE_S3B_RD_INC(ai2_pkt);
	tim_gps->time_unc            = BUF_LE_U2B_RD_INC(ai2_pkt);
	tim_gps->is_utc_diff         = 0;
	tim_gps->utc_diff            = 0;
	tim_gps->freq_bias           = BUF_LE_S4B_RD_INC(ai2_pkt);
	tim_gps->freq_unc            = BUF_LE_U4B_RD_INC(ai2_pkt);

	return (ai2_pkt - ai2_ref);
}

static int dev_msr_ext_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt*) ai2_d2h->ai2_struct;
	struct dev_gps_time *tim_gps = &rpt->time;
	struct dev_sat_meas *sat_meas = &rpt->sat_meas;
	struct dev_msr_info_PVT *dev_msr  = sat_meas->dev_msr;
	char  *ai2_pkt            = (char *) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref            = ai2_pkt;

	unsigned short dev_msr_sz     = BUF_LE_U2B_RD_INC(ai2_pkt);
	int num_sat                   = (ai2_d2h->ai2_pkt_sz - (ai2_offset + 2))
		/ dev_msr_sz;
	sat_meas->num_sat             = num_sat;
	dev_msr->Tcount = tim_gps->timer_count;
	
//	LOGPRINT(msr_rpt, info, "Svid    CNo      Flags   State  Msec         SubMs        TUnc      Latency   Speed       SpeedUnc     GOC     TOC");
	LOGPRINT(msr_rpt, info, "Svid	 Cno");

	while(num_sat--) {

		dev_msr->svid = BUF_LE_U1B_RD_INC(ai2_pkt);

		if (dev_msr->svid >= 1 && dev_msr->svid <= 32)
			dev_msr->gnss_id = e_gnss_gps;
		else if (dev_msr->svid >= 65 && dev_msr->svid <= 88)
			dev_msr->gnss_id = e_gnss_glo;
		else if (dev_msr->svid >= 120 && dev_msr->svid <= 138)
			dev_msr->gnss_id = e_gnss_sbs;
		else if (dev_msr->svid >= 193 && dev_msr->svid <= 202)
			dev_msr->gnss_id = e_gnss_qzs;
		else
			dev_msr->gnss_id = e_gnss_und;

		dev_msr->cno              = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->latency_ms       = BUF_LE_S2B_RD_INC(ai2_pkt);
		dev_msr->pre_int          = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->post_int         = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->msec             = BUF_LE_U4B_RD_INC(ai2_pkt);
		dev_msr->sub_msec         = BUF_LE_U3B_RD_INC(ai2_pkt);
		dev_msr->time_unc         = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->speed            = BUF_LE_S3B_RD_INC(ai2_pkt);
		dev_msr->speed_unc        = BUF_LE_U3B_RD_INC(ai2_pkt);
		dev_msr->msr_flags        = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->ch_states        = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->accum_cp         = BUF_LE_S4B_RD_INC(ai2_pkt);
//		dev_msr->carrier_vel      = BUF_LE_S4B_RD_INC(ai2_pkt);
		g_pps_state     = BUF_LE_U1B_RD_INC(ai2_pkt);
		ai2_pkt +=3;
		/*Added in ver0.166 patch*/
		dev_msr->pos_residuals	  = BUF_LE_S2B_RD_INC(ai2_pkt);
		dev_msr->rf_reg_val		  = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->snr              = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->good_obv_cnt     = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->total_obv_cnt    = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->sv_slot          = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->diff_sv          = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->early_term       = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->elevation        = BUF_LE_S1B_RD_INC(ai2_pkt);
		dev_msr->azimuth          = BUF_LE_U1B_RD_INC(ai2_pkt);

		// : Define the following numbers
		// : Backward compatibility warning for 128x

		/* Reserved */
		ai2_pkt += 7;

		dev_msr->velocity_residuals = BUF_LE_S2B_RD_INC(ai2_pkt); 
		
		print_data(REI_DEV_MSR_GPS, (void*)dev_msr);

		dev_msr++;
	}

	return (ai2_pkt - ai2_ref);
}

static int dev_msr_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt*) ai2_d2h->ai2_struct;
	struct dev_sat_meas *sat_meas = &rpt->sat_meas;
	struct dev_msr_info_PVT *dev_msr = sat_meas->dev_msr;
	char  *ai2_pkt = (char *) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref = ai2_pkt;
	unsigned short dev_msr_sz = 28;
	int num_sat = (ai2_d2h->ai2_pkt_sz - ai2_offset) / dev_msr_sz;

	sat_meas->num_sat = num_sat;

	while(num_sat--) {

		dev_msr->svid = BUF_LE_U1B_RD_INC(ai2_pkt);

		if (dev_msr->svid >= 1 && dev_msr->svid <= 32)
			dev_msr->gnss_id = e_gnss_gps;
		else if (dev_msr->svid >= 65 && dev_msr->svid <= 88)
			dev_msr->gnss_id = e_gnss_glo;
		else if (dev_msr->svid >= 120 && dev_msr->svid <= 138)
			dev_msr->gnss_id = e_gnss_sbs;
		else if (dev_msr->svid >= 193 && dev_msr->svid <= 202)
			dev_msr->gnss_id = e_gnss_qzs;
		else
			dev_msr->gnss_id = e_gnss_und;

		dev_msr->snr              = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->cno              = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->latency_ms       = BUF_LE_S2B_RD_INC(ai2_pkt);
		dev_msr->pre_int          = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->post_int         = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->msec             = BUF_LE_U4B_RD_INC(ai2_pkt);
		dev_msr->sub_msec         = BUF_LE_U3B_RD_INC(ai2_pkt);
		dev_msr->time_unc         = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->speed            = BUF_LE_S3B_RD_INC(ai2_pkt);
		dev_msr->speed_unc        = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->msr_flags        = BUF_LE_U2B_RD_INC(ai2_pkt);

		/* Reserved */
		ai2_pkt += 2;

		print_data(REI_DEV_MSR_GPS, dev_msr);

		dev_msr++;
	}

	return (ai2_pkt - ai2_ref);
}

static int ext_msr_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	if (d2h_get_msg_id(ai2_d2h) == 0xF7) {

		ai2_offset = 4;

		ai2_offset += dev_tim_struct(ai2_d2h, ai2_offset);

		ai2_offset += dev_msr_ext_struct(ai2_d2h, ai2_offset);
	} else {

		ai2_offset = 7;

		ai2_offset += dev_msr_struct(ai2_d2h, ai2_offset);
	}

	return (ai2_offset == ai2_pkt_sz) ? 0 : -1;
}

static int ext_msr_setmem(struct d2h_ai2 *ai2_d2h)
{
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt *) ai2_d2h->ai2_struct;
	struct dev_sat_meas *sat_meas = &rpt->sat_meas;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	sat_meas->dev_msr = BUF_OFFSET_ST(sat_meas, struct dev_sat_meas,
									  struct dev_msr_info_PVT);
	return 0;
}                     

static int qualify_msr(struct d2h_ai2 *ai2_d2h)
{
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt*) ai2_d2h->ai2_struct;
	struct dev_sat_meas *sat_meas = &rpt->sat_meas;
	struct dev_msr_info_PVT *msr_info = sat_meas->dev_msr;
	struct d2h_ai2_msr *msr = (struct d2h_ai2_msr *) ai2_d2h;
	struct dev_msr_result *msr_rpt_results = (struct dev_msr_result *)
		(((unsigned char *) ai2_d2h->ai2_struct) + ST_SIZE(dev_meas_rpt) 
		+ (ST_SIZE(dev_msr_info_PVT) * 200));

	memset(msr_rpt_results, 0, ST_SIZE(dev_msr_result));

	msr->private_field = msr_rpt_results;

	/* Check for msr qualification */
	return msr_filter_report(msr_info, sat_meas->num_sat,msr_rpt_results);
}

/* Store the freq unc into a NVS File */
static int store_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	struct dev_meas_rpt *rpt = (struct dev_meas_rpt*) ai2_d2h->ai2_struct;
	struct dev_gps_time *tim_gps = &rpt->time;	
	struct dev_gps_tcxo gps_tcxo;
	unsigned int *short_term_freq_unc;
	unsigned int temp_unc;
	int wr_suc = 0;

	// Store Time
	LOGPRINT(tim_rpt, info, "gps_clk_info: Tcount:[%8d] week:[%5d] Msec[%8d] TBias:[%5d] TUnc:[%8d] FBias:[%5d] FUnc:[%8d] isUTC:[%2d] UTC:[%3d]",
				tim_gps->timer_count,
				tim_gps->week_num,
				tim_gps->time_msec,
				tim_gps->time_bias,
				tim_gps->time_unc,
				tim_gps->freq_bias,
				tim_gps->freq_unc,
				tim_gps->is_utc_diff,
				tim_gps->utc_diff);

	if (tim_gps->week_num != 65535) {
	LOGPRINT(tim_rpt, info, "storing clock report from msr in nvs");
	wr_suc = nvs_rec_add(nvs_gps_clk, tim_gps, 1);
	if (wr_suc != nvs_success) {
			LOGPRINT(tim_rpt, err, "Error storing clock report from msr in nvs");
			return -1;
		}
		}

	sys_param_get(cfg_tcxo_short_term_freq_unc,(void**) &short_term_freq_unc);

	temp_unc = ((*short_term_freq_unc) * LIGHT_SEC * 1e-6);

	if( temp_unc > tim_gps->freq_unc ){
		
		gps_tcxo.freq_bias = tim_gps->freq_bias;
		gps_tcxo.freq_unc  = tim_gps->freq_unc;

		LOGPRINT(tim_rpt, info, "storing TCXO in nvs");
			
		wr_suc = nvs_rec_add(nvs_gps_tcxo, &gps_tcxo, 1);
		if (wr_suc != nvs_success) {
			LOGPRINT(tim_rpt, err, "Error storing TCXO in nvs");
			return -1;
		}
	}

	
	return 0;
}


/* 56 = 32 (GPS) + 24 (GLO) */
#define EXT_MSR_ST_LEN (ST_SIZE(dev_meas_rpt) \
	+ (ST_SIZE(dev_msr_info_PVT) * 200 )            \
	+ ST_SIZE(dev_msr_result))

struct d2h_ai2_msr d2h_msr_rpt = {

	.base = {

		.ai2_st_len = EXT_MSR_ST_LEN,
		.st_set_mem = ext_msr_setmem,
		.gen_struct = ext_msr_struct,
		.do_ie_4rpc = ext_msr_rpc_ie,
		.assess_qoi = qualify_msr,
		.nvs_action = store_nvs_info
	},

	.private_field = (void*)NULL
};
