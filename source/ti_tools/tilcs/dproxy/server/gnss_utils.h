/*
 * gnss_utils.h
 *
 *
 * Copyright (C) {2012} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#ifndef _GNSS_UTILS_H_
#define _GNSS_UTILS_H_

#include "nvs_utils.h"

// Define UTC as on 01 Jan 1996; note that UTC started on 01 Jan 1970 
#define UTC_Y1996_M01_D01  ( \
        365*MAX_SECS_IN_A_DAY + /* Y1970 */ \
        365*MAX_SECS_IN_A_DAY + /* Y1971 */ \
          6*MAX_SECS_4Y_BLOCK  /* 6 groups of 4 years, i.e. 1972 to 1996 */  \
          )

#define UTC_AT_GLO_ORIGIN                   \
	(                                       \
	365 *24*60*60 +     /* Y1970 */         \
	365 *24*60*60 +     /* Y1971 */         \
	1461*24*60*60 * 6  /* 6 groups of 4 years, i.e. 1972 to 1996 */ \
	)

#define UTC_AT_GPS_ORIGIN                   \
	(                                       \
	365 *24*60*60 +     /* Y1970 */         \
	365 *24*60*60 +     /* Y1971 */         \
	1461*24*60*60 * 6  /* 6 groups of 4 years, i.e. 1972 to 1996 */ \
	)

		
#define DO_GLO_NT_SECS(NT) ((NT - 1) * MAX_SECS_IN_A_DAY)

#define DO_GLO_Tk_SECS(Tk)                               \
	(                                                    \
	(((Tk >> 7) & 0x1F) * MAX_SECS_IN_1HOUR) +  /* HH */ \
	(((Tk >> 1) & 0x3F) * 60)                +  /* MM */ \
	(((Tk << 0) & 0x01) ? 30 : 0)               /* SS */ \
	)
	
#define DO_GLO_N4_SECS(N4)          ((N4 - 1) * MAX_SECS_4Y_BLOCK)
#define DO_GLO_NA_SECS(NA)          ((NA - 1) * MAX_SECS_IN_A_DAY)

// Define GLO UTC and local that originated at 03.00.00 hrs UTC on 01 Jan 1996 */ 
#define GLO_START_RU_TIME ((3 * MAX_SECS_IN_1HOUR) + UTC_AT_GLO_ORIGIN)

static inline unsigned int glo2utc_time(unsigned int glo_secs)
{
	return glo_secs - 3 * MAX_SECS_IN_1HOUR;
}

static inline unsigned int utc2glo_time(unsigned int utc_secs)
{
	return utc_secs + 3 * MAX_SECS_IN_1HOUR;
}

unsigned int gps_time_now(unsigned short nr_week, unsigned int wk_secs);

unsigned int glo_time_now(unsigned short day, unsigned int tod);

unsigned char get_glo_N4_param(unsigned int glo_now);

unsigned char utc2glo_N4_param(unsigned int utc_now);

unsigned short utc2glo_NT_param(unsigned int utc_now);

unsigned short get_glo_NT_param(unsigned int glo_now);

#endif
