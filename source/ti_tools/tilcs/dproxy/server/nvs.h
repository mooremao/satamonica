/*
 * nvs.h
 *
 * This file describes the storage Mechanism of GNSS data in
 * the Ram as well as in the NVS.
 *
 * This file gives the interface to add, get and delete a
 * particular object ID in the record .
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#ifndef _NVS_H_
#define _NVS_H_
#include "device.h"
#include <time.h>

#define MAX_FILE_SIZE 150

/** Gives the object ID within the record

  The record doing the operation need to register this
  as callback

  @param[in] obj_data is the data of response ID.
  @return obj_id within the record.
 */
typedef unsigned int (*nvs_rec_find_oid)(void *obj_data);

/** @defgroup nvs_mcache_group NVS storage method
  defines the nvs storage type

  @{
 */
enum nvs_mcache {

	write_back,
	write_thru
};

/* New struct  */
/** record description */
struct nvs_rec_param {

	enum nvs_mcache      mcache;  /**< Caching method          */
	nvs_rec_find_oid     get_oid; /**< Object Id info 	   */
	int                  rec_sz;  /**< Record size             */
	int                  n_objs;  /**< No of Objects in record */

};

/** defgroup nvs_record_group record type
  defines various record types

  @{
 */
enum nvs_record {

	nvs_gps_clk,
	nvs_gps_pos,
	nvs_gps_eph,
	nvs_gps_alm,
	nvs_hlth,
	nvs_gps_iono,
	nvs_gps_utc,
	nvs_gps_tcxo,
	nvs_gps_svdr,
	nvs_glo_clk,
	nvs_glo_eph,
	nvs_glo_alm,
	nvs_glo_svdr,
	nvs_glo_utc,
	nvs_glo_sv_slot,
	nvs_rec_end
};

/** results of the operations */
enum nvs_status {

	nvs_error   = -1,
	nvs_success = 0
};

/** configuration information */
struct nvs_config {

	char path[MAX_FILE_SIZE];
};

/** Intialises and opens the file for each record for
  reading, writing and deleting the data.
  @param[in] configuration data
  @return 0 on success -1 for error
 */
int nvs_init(struct nvs_config *cfg);

/** Exits and closes all the files */
void nvs_exit(void);  /* Flush all write-back caches */

/** Add data to object IDs of a record
  @param[in] rec is record type of nvs_record_group
  @param[in] data is response id data
  @param[in] num is number of object ids
  @return number of object ids successfully added
 */
int nvs_rec_add(enum nvs_record rec, void *data, unsigned int num);

/** Get data from the objects IDs of a record
  @param[in] rec is record type of nvs_record_group
  @param[in] data is response id data
  @param[in] flags is object ids to be read
  @return number of object ids successfully read
 */
int nvs_rec_get(enum nvs_record rec, void *data, unsigned int flags);

/** Delete data from the objects IDs of a record
  @param[in] rec is record type of nvs_record_group
  @param[in] flags is object ids to be deleted
  @return number of object ids successfully deleted
 */
int nvs_rec_del(enum nvs_record rec, unsigned int flags);

/** Sets up the record information
  @param[in] rec is record type of nvs_record_group
  @param[in] param is parameter information
  @return 0 on success -1 for errors
 */
int nvs_rec_setup(enum nvs_record rec, struct nvs_rec_param *param);

/** set the ttl setting
  @param[in] rec is record type of nvs_record_group
  @param[in] val is to set or reset
  @return 0 on success -1 for errors
 */
int nvs_rec_setttl(enum nvs_record rec, int val);

/** get the rec_timestamp
    @param[in] rec is record type of nvs_record_group
    @param[in] obj_idx is the object id
    @return timestamp of the record
*/
unsigned long nvs_rec_ts_s(enum nvs_record rec, void *rec_data);

int nvs_rec_ts_ms(enum nvs_record rec, void *rec_data, unsigned long long *data);

void nvs_flush(void);


#endif
