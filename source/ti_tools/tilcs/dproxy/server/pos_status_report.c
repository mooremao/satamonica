/*
 * pos_status_report.c
 *
 * Position status report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "log_report.h"

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int pos_stat_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						struct dev_pos_stat *stat);

static struct ie_desc *mk_ie_adu_4pos_stat_dev(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_pos_stat *stat)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_pos_stat);

	struct dev_pos_stat *new_stat = (struct dev_pos_stat *)malloc(st_sz);
	if(!new_stat)
		goto exit1;

	memcpy(new_stat, stat, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_POS_STAT, oper_id, st_sz, new_stat);
	if(!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_stat);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int pos_stat_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						struct dev_pos_stat *stat)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4pos_stat_dev(rmsg, oper_id, stat);
	if(!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while(--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int pos_stat_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						struct dev_pos_stat *stat)
{
	return pos_stat_create_dev_ie(rmsg, oper_id, stat);
}

static int pos_stat_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
							unsigned int rei_select)
{
	struct dev_pos_stat *stat = (struct dev_pos_stat *) ai2_d2h->ai2_struct;

	if (-1 == pos_stat_create_dev_ie(rmsg, OPER_INF, stat))
		return -1;

	UNUSED(rei_select);

	return 0;
}

static int pos_stat_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_pos_stat *stat = (struct dev_pos_stat *) ai2_d2h->ai2_struct;
	char *ai2_pkt = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char *ai2_ref = ai2_pkt;

	int ref_count 	    = BUF_LE_U4B_RD_INC(ai2_pkt);
	stat->status        = BUF_LE_U1B_RD_INC(ai2_pkt);
	stat->sigma         = BUF_LE_U2B_RD_INC(ai2_pkt);
	stat->PDOP          = BUF_LE_U1B_RD_INC(ai2_pkt);
	stat->HDOP          = BUF_LE_U1B_RD_INC(ai2_pkt);
	stat->VDOP          = BUF_LE_U1B_RD_INC(ai2_pkt);
	stat->fix_status    = BUF_LE_U1B_RD_INC(ai2_pkt);

	/* Dump the decoded data to logger. */
	print_data(REI_DEV_POS_STAT, stat);

	UNUSED(ref_count);
	return (ai2_pkt - ai2_ref);
}

static int pos_stat_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += pos_stat_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

#define EXT_PVT_ST_LEN ST_SIZE(dev_pos_stat)

struct d2h_ai2 d2h_pos_stat_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.gen_struct = pos_stat_ext_struct,
	.do_ie_4rpc = pos_stat_rpc_ie
};

