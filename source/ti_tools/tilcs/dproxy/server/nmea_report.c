/*
 * nmea_report.c
 *
 * NMEA report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"

struct nmea_rpc {
	char           *sn_hdr;
	unsigned short  hdr_sz;
	unsigned short  rpc_id;
};

static struct nmea_rpc rpc_tbl [] = {

	{"$GPGGA", 6, REI_NMEA_GPGGA},
	{"$GPGLL", 6, REI_NMEA_GPGLL},
	{"$GPGSA", 6, REI_NMEA_GPGSA},
	{"$GPGSV", 6, REI_NMEA_GPGSV},
	{"$GPRMC", 6, REI_NMEA_GPRMC},
	{"$GPVTG", 6, REI_NMEA_GPVTG},
	{"$GPGRS", 6, REI_NMEA_GPGRS},
	{"$GPGST", 6, REI_NMEA_GPGST},
	{"$GLGGA", 6, REI_NMEA_GLGGA},
	{"$GLGLL", 6, REI_NMEA_GLGLL},
	{"$GLGSA", 6, REI_NMEA_GLGSA},
	{"$GLGSV", 6, REI_NMEA_GLGSV},
	{"$GLRMC", 6, REI_NMEA_GLRMC},
	{"$GLVTG", 6, REI_NMEA_GLVTG},
	{"$QZGSV", 6, REI_NMEA_QZGSV},
	{"$WSGSV", 6, REI_NMEA_WSGSV},
	{"$GNGLL", 6, REI_NMEA_GNGLL},
	{"$GNVTG", 6, REI_NMEA_GNVTG},
	{"$GNGSA", 6, REI_NMEA_GNGSA},
	{"$GNGNS", 6, REI_NMEA_GNGNS},
	{"$GNGRS", 6, REI_NMEA_GNGRS},

	/*Add more */
	{NULL,    0, REI_NOTDEF}
};

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int nmea_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
					unsigned int sn_size, char *dev_nmea);

static struct nmea_rpc *nmea_sn_find_rpc_id(char *dev_nmea)
{
	struct nmea_rpc *rpc = rpc_tbl;

	while(rpc->sn_hdr) {
		if(0 == strncmp(rpc->sn_hdr, dev_nmea, rpc->hdr_sz))
			return rpc;

		rpc++;
	}

	return NULL;
}

	static struct ie_desc*
mk_ie_adu_4nmea_dev(struct rpc_msg *rmsg, unsigned short oper_id,
					unsigned int sn_size, char *dev_nmea)
{
	struct ie_desc *ie_adu;
	struct nmea_rpc *rpc;
	char  *nmea_buf = NULL;

	rpc = nmea_sn_find_rpc_id(dev_nmea);
	if(!rpc)
		goto nmea_mkadu_exit1;

	nmea_buf = malloc(sn_size);
	if(!nmea_buf)
		goto nmea_mkadu_exit1;

	memcpy(nmea_buf, dev_nmea, sn_size);

	ie_adu = rpc_ie_attach(rmsg, rpc->rpc_id, oper_id, sn_size, nmea_buf);
	if(!ie_adu)
		goto nmea_mkadu_exit2;

	return ie_adu;

nmea_mkadu_exit2:
	free(nmea_buf);

nmea_mkadu_exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int nmea_sn_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
					unsigned int sn_size, char *dev_nmea)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4nmea_dev(rmsg, oper_id, sn_size, dev_nmea);
	if(!ie_adu[n_adu++])
		goto nmea_create_exit1;

	return n_adu;

nmea_create_exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* 
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int nmea_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
					unsigned int sn_size, char *dev_nmea)
{
	return nmea_sn_create_dev_ie(rmsg, oper_id, sn_size, dev_nmea);
}

static int nmea_sn_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg, 
							unsigned int rei_select)
{
	UNUSED(rei_select);

	if(-1 == nmea_sn_create_dev_ie(rmsg, OPER_INF, ai2_d2h->ai2_pkt_sz - 7,
						(char*)ai2_d2h->ai2_packet + 7))
		return -1;

	return 0;
}

static int nmea_sn_struct(struct d2h_ai2 *ai2_d2h)
{
	UNUSED(ai2_d2h);
	return 0;
}

struct d2h_ai2 d2h_nmea = {

	.ai2_st_len = 0,

	.gen_struct = nmea_sn_struct,
	.do_ie_4rpc = nmea_sn_rpc_ie
};

