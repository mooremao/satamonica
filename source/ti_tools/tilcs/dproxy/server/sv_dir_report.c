/*
 * sv_dir_report.c
 *
 * Satellite direction status report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "log_report.h"
#include "nvs.h"

struct dev_dir_rpt {

	unsigned short num_sat;
	struct dev_sv_dir *dir;
};

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int sv_dir_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
				unsigned short num_sat, struct dev_sv_dir *dir);

static struct ie_desc *mk_ie_adu_4sv_dir_dev(struct rpc_msg *rmsg,
			unsigned char oper_id, struct dev_dir_rpt *sv_dir_rpt)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_sv_dir) * sv_dir_rpt->num_sat;

	struct dev_sv_dir *dir = sv_dir_rpt->dir;
	struct dev_sv_dir *new_dir = (struct dev_sv_dir *)malloc(st_sz);
	if(!new_dir)
		goto exit1;

	memcpy(new_dir, dir, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_SV_DIR, oper_id, st_sz, new_dir);
	if(!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_dir);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int sv_dir_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
						struct dev_dir_rpt *sv_dir_rpt)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4sv_dir_dev(rmsg, oper_id, sv_dir_rpt);
	if(!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while(--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int sv_dir_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
				unsigned short num_sat, struct dev_sv_dir *dir)
{
	struct dev_dir_rpt rpt = {num_sat, dir};
	return sv_dir_create_dev_ie(rmsg, oper_id, &rpt);
}


static int sv_dir_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
						 unsigned int rei_select)
{
	struct dev_dir_rpt *rpt = (struct dev_dir_rpt *) ai2_d2h->ai2_struct;

	if(-1 == sv_dir_create_dev_ie(rmsg, OPER_INF, rpt))
		return -1;

	UNUSED(rei_select);

	return 0;
}

static int dev_sv_dir_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_dir_rpt *rpt = (struct dev_dir_rpt*) ai2_d2h->ai2_struct;
	struct dev_sv_dir *dir = rpt->dir;
	char *ai2_pkt = (char *) ai2_d2h->ai2_packet + ai2_offset;
	char *ai2_ref = ai2_pkt;
	unsigned short dev_msr_sz = 3;
	int num_sat = (ai2_d2h->ai2_pkt_sz - ai2_offset) / dev_msr_sz;

	rpt->num_sat = num_sat;

	while(num_sat--) {
		dir->prn             = BUF_LE_U1B_RD_INC(ai2_pkt);
		dir->elevation       = BUF_LE_S1B_RD_INC(ai2_pkt);
		dir->azimuth         = BUF_LE_U1B_RD_INC(ai2_pkt);

		print_data(REI_DEV_SV_DIR, dir);

		dir++;
	}

	return (ai2_pkt - ai2_ref);
}

static int sv_dir_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	ai2_offset += dev_sv_dir_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int sv_dir_ext_setmem(struct d2h_ai2 *ai2_d2h)
{
	struct dev_dir_rpt *rpt = (struct dev_dir_rpt *) ai2_d2h->ai2_struct;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	rpt->dir = BUF_OFFSET_ST(rpt, struct dev_dir_rpt, struct dev_sv_dir);

	return 0;
}

static int sv_dir_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0;
	struct dev_dir_rpt *rpt = (struct dev_dir_rpt *) ai2_d2h->ai2_struct;
	struct dev_sv_dir *dir = rpt->dir;

	int num_sat = rpt->num_sat;

	while(num_sat--) {

		if (dir->prn >= 1 && dir->prn <= 32) {

			wr_suc = nvs_rec_add(nvs_gps_svdr, dir, 1);
			if (wr_suc != nvs_success) {
				LOGPRINT(svdir_rpt, err, "error storing sv-dir in nvs");
				return -1;
			}
		} else if (dir->prn >= 65 && dir->prn <= 88) {

			wr_suc = nvs_rec_add(nvs_glo_svdr, dir, 1);
			if (wr_suc != nvs_success) {
				LOGPRINT(svdir_rpt, err, "error storing sv-dir in nvs");
				return -1;
			}
		}

		dir++;
	}

	LOGPRINT(svdir_rpt, err, "sv-dir is stored in nvs");

	return 0;
}

#define EXT_PVT_ST_LEN  ST_SIZE(dev_dir_rpt) + (200 * ST_SIZE(dev_sv_dir))

struct d2h_ai2 d2h_sv_dir_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.st_set_mem = sv_dir_ext_setmem,
	.gen_struct = sv_dir_ext_struct,
	.do_ie_4rpc = sv_dir_rpc_ie,
	.nvs_action = sv_dir_nvs_info
};

