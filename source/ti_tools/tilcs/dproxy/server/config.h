/*
 * config.h
 *
 *  Internal server configuration code. This abstracts the following
 *   1. solution level configuration
 *    2. local server-only configuration
 *    3. device specific configurations
 *    4. configurations set by clients
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _DPROXY_CONFIG_H_
#define _DPROXY_CONFIG_H_

#define AI2_GPS_BASE 0
#define AI2_GLO_BASE 64

/* NVS Details. */
#define NVS_FILE_PATH "/mnt/userdata/gnss/nvs/"

/* Default value for AI2 force flag fields. 
 * Override at the specific implementation, if needed. 
 */
#define AI2_FORCE_FLAG_DFLT 1
#define AI2_DONOT_FORCE_FLAG 0

/* Device communication. */
#define AI2_WRITE_BUF_SIZE (800)
#define AI2_READ_BUF_SIZE (4 * 1024)
#define AI2_REQUIRE_ACK 0
#define AI2_ACK_TIMEOUT 3 /* Seconds. Ignored, if AI2_REQUIRE_ACK != 1. */
#define AI2_RETRY_COUNT 3 /* Ignored, if AI2_REQUIRE_ACK != 1. */
#define AI2_RETRY_DELAY 2 /* Seconds. Ignored, if AI2_REQUIRE_ACK != 1. */

#define AI2_COMM_DEV_NAME "/dev/tigps"

/* Internal FIFO details. */
#define CLIENT_FIFO "/tmp/dproxy.client"
#define SERVER_FIFO "/tmp/dproxy.server"

/* Lock file details. */
#define LOCK_FILE "/tmp/dproxy.lock"

/* Patch related details. */
#ifdef ANDROID
#define PATCH_FILE_NAME "/system/etc/gnss/patch/dproxy.patch"
#else
#define PATCH_FILE_NAME "/lib/firmware/ti-gnss-patch/dproxy.patch"
#endif

/* Sensor logs. */
#define AI2_LOG_FILE_PREFIX  "/mnt/userdata/gnss/logs/"

/* Running directory. */
#define RUNNING_DIR "/mnt/userdata/gnss/logs/"

/* Sequencer details. */
#define SEQ_STACK_SIZE 30
#define SEQ_PUSH_WINDOW 5

/* CEP. */
#define ACCU_VARIATION 10

/* Misc. */
#define SV_SELECT_ALGO_NOW 0


/*4 SVs Almanac buffer size*/
#define AI2_4SV_ALMANAC_BUF_SIZE (4 * 30)
/*2 SVs Ephemeris buffer size*/
#define AI2_2SV_EPHEMERIS_BUF_SIZE (2 * 70)


/* Dynamic Configurations. */
struct ptch_dwld_cfg {
	unsigned short dwld_type;
};
struct ptch_dwld_cfg *get_ptch_dwld_cfg(void);

struct dev_app_desc;
struct dev_app_desc *get_app_desc_cfg(void);

struct oscillator_parameters {

	unsigned char  ctrl;
	unsigned short osc_quality;
	unsigned int   osc_freq;
};

struct sbas_control {

	unsigned char sbas_ctrl;
	unsigned int  sbas_prn_mask;
	unsigned char reserved[10];
};

struct time_stamp_period {
	unsigned short calib_period;
	unsigned short time_unc;
};


enum sys_config_param {

	cfg_internal_test,
	cfg_baud_rate,
	cfg_const_config,
	cfg_server_mode,
	cfg_fdic_mode,
	cfg_alt_mode,
	cfg_gps_frequn,
	cfg_gps_acc,
	cfg_usr_acc,
	cfg_ele_mask,
	cfg_pdop_mask,
	cfg_feat_ctrl,
	cfg_2pdop_mask,
	cfg_time_event,
	cfg_oper_mode,
	cfg_osc_param,
	cfg_time_porsel,
	cfg_blank_ctrl,
	cfg_cbyn_loss,
	cfg_wtdog_ctrl,
	cfg_utc_lpsec,
	cfg_calib_ctrl,
	cfg_pps_opctrl,
	cfg_lnaext_cryt,
	cfg_sbas_ctrl,
	cfg_locfix_mode,
	cfg_refclk_freq,
	cfg_evt_period,
	cfg_evt_async,
	cfg_prd_reprate,
	cfg_prd_repsec,
	cfg_pos_vel,
	cfg_pos_unc,
	cfg_gps_alm,
	cfg_gps_eph,
	cfg_gps_svhlth,
	cfg_dwnld_opt,
	cfg_glo_alm,
	cfg_glo_eph,
	cfg_qop_acc,
	cfg_qop_time,
	cfg_patch_path,
	cfg_gps_gnss_id,
	cfg_gps_ganss_cmn,
	cfg_gps_caps_bits,
	cfg_gps_asst_bits,
	cfg_gps_sigs_bits,
	cfg_gps_sbas_bits,	
	cfg_glo_gnss_id,
	cfg_glo_ganss_cmn,
	cfg_glo_caps_bits,
	cfg_glo_asst_bits,
	cfg_glo_sigs_bits,
	cfg_glo_sbas_bits,	
	cfg_pdop_mask_time_out,
	cfg_new_log_flag,
	cfg_tcxo_short_term_freq_unc,
	cfg_tcxo_long_term_freq_unc,
	cfg_single_pe_a3,
	cfg_single_pe_pdop_mask,
	cfg_calib_enable_flag,
	cfg_notch_filter,	
	cfg_pa_blank_polarity,
	cfg_logging_mask,
	cfg_rtc_inj_ctrl,
	cfg_rtc_time_qual_src,
	cfg_host_rtc_unc,
	cold_t1,
	cold_t2,
	cold_a1,
	cold_a2,
	warm_t1,
	warm_t2,
	warm_a1,
	warm_a2,
	hot_t1,
	hot_t2,
	hot_a1,
	hot_a2,
        ee_t1,
        ee_t2,
        ee_a1,
        ee_a2,
	cfg_calib_mode,
	cfg_apm_flag,
	cfg_alt_unc,
	cfg_nvs_gps_time_unc,
	cfg_nvs_glo_time_unc,
	cfg_time_unc,
	cfg_param_end
};

/** results of the operations */
enum cfg_status {

	cfg_success =  0,
	cfg_error   = -1,
};

/** intailises the configurable parameters 
    @param[in] path of the platform configuration file
    @return cfg_success on success or cfg_error on error
*/
int sys_config_init(char *config_file_path);

/** get the system config parameter
    @param[in] cfg_param is config parameter of the sys_config_param_group
    @param[in] buffer is the address of the parameter
    @param[in] type is the value type of the parameter
    @return cfg_success on success or cfg_error on error
*/
int sys_param_get(enum sys_config_param cfg_param, void **buffer);

/** get the system config parameter
    @param[in] cfg_param is config parameter of the sys_config_param_group
    @param[in] buffer is the address of the parameter
    @return cfg_success on success or cfg_error on error
*/
int sys_param_set(enum sys_config_param cfg_param, void **buffer);

/** exit*/
void sys_config_exit(void);
void set_rx_on_time(unsigned int tim);
unsigned int get_rx_on_time(void);
// void print_config_params();

#endif
