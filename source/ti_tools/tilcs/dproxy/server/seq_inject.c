/*
 * seq_inject.c
 *
 * Sequencers related injection functions.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "dev_proxy.h"
#include "inject.h"
#include "ai2.h"
#include "logger.h"
#include "config.h"
#include "sequencer.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "os_services.h"
#include "nvs.h"
#include "nvs_validate.h"
#include "nvs_utils.h"
#include "release.h"

extern int h2d_ai2_hdl;

struct timeval patch_time_start[1];
struct timeval patch_time_end[1];
struct timeval patch_rec_time_start[1];
struct timeval patch_rec_time_end[1];

unsigned int pdop_mask = 0x0C;
unsigned int pdop_2d_mask = 0x08;


int dpxy_inj_comm_prot(void *data)
{
	struct ai2_inj_com_prot {
		unsigned int id;
		unsigned int len;
		unsigned int protocol;
	} prot[1];
	char ai2_inj_com_prot_sizes[] = {1, 2, 1, 0};
	unsigned char mem[4];
	unsigned int select_retry = 0;
	prot->id = 0xF5;
	prot->len = 1;
	prot->protocol = 1;

	d2h_ai2_encode((void *)prot, mem, ai2_inj_com_prot_sizes);
	do{
	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "communication protocol injected");
			return OK;
		}
	}
	  LOGPRINT(seq, critl, "communication protocol retry");
		select_retry++;
	}while(select_retry != 5);

        UNUSED(data);
        LOGPRINT(seq, err, "error sending ai2 packet to device");
	return ERR;
}

int dpxy_get_gps_stat(void *data)
{
        UNUSED(data);
	return OK;
}

int dpxy_get_gps_ver(void *data)
{
	unsigned char mem[3];

	mem[0] = 0xF0;
	mem[1] = 0;
	mem[2] = 0;

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "dpxy_get_gps_ver injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(cfg, err, "error sending dpxy_get_gps_ver ai2 packet to device");
	return ERR;
}


/*------------------------------------------------------------------------------
 * Patch download section
 *----------------------------------------------------------------------------*/
#define PATCH_HEADER_SIZE 9
#define PATCH_MAX_REC_SIZE 1120

enum ptch_state {
	begin,
	middle,
	end,
	nopatch
};
struct ptch_dwld_ctrl {
	enum ptch_state state;
	unsigned short rec_size;
	unsigned short rec_cnt;
	unsigned short last_rec_size;
	unsigned short cur_rec;
	FILE *fp;
};
static struct ptch_dwld_ctrl seq_ptch_ctrl[1];

static int dpxy_inj_ptch_dwld_begin(void *data)
{
	struct ai2_inj_ptch_dwld_ctrl {
		unsigned int id;
		unsigned int len;
		unsigned int dwld_type;
	} dwld_ctrl[1];
	char ai2_inj_ptch_dwld_ctrl_sizes[] = {1, 2, 1, 0};
	unsigned char mem[4];
	struct ptch_dwld_cfg *cfg = get_ptch_dwld_cfg();
	struct ptch_dwld_ctrl *ctrl = (struct ptch_dwld_ctrl *) data;

	/* If no download, just pop the next instruction from sequencer. */
	if (cfg->dwld_type == 0x05) {
		seq_pop_step(NULL);
		ctrl->state = nopatch;
	}
	else {
		ctrl->state = middle;
		ctrl->cur_rec = 0;
	}

	dwld_ctrl->id = 0xF1;
	dwld_ctrl->len = 1;
	dwld_ctrl->dwld_type = cfg->dwld_type;

	d2h_ai2_encode((void *)dwld_ctrl, mem, ai2_inj_ptch_dwld_ctrl_sizes);

	gettimeofday(patch_time_start, NULL);
	gettimeofday(patch_rec_time_start, NULL);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(patch, info, "patch download control injected");
			return OK;
		}
	}

        LOGPRINT(patch, err, "error sending ai2 packet to device");
	return ERR;
}

/* If 'size' is '0', then file will be closed. */
static int ptch_file_read (void *data, unsigned char *buf, unsigned int size)
{
	struct ptch_dwld_ctrl *ctrl = (struct ptch_dwld_ctrl *) data;
	//char *patch_file_name ; 
	char patch_file_name[100];
	/* no-op on size == 0. */
	if (0 == size) {
		LOGPRINT(patch, warn, "invalid buf size, abrt patch read");
		return ERR;
	}

	sys_param_get(cfg_patch_path, (void **)&patch_file_name);
	sprintf(patch_file_name,"%s",PATCH_FILE_NAME);
	strncat(patch_file_name, rom_version, sizeof(rom_version));
	
	/* Open file if it is not opened already. */
	if (!ctrl->fp) {
		if (NULL == (ctrl->fp = fopen(patch_file_name, "r"))) {
			LOGPRINT(patch, err, "error opening patch file - %s", patch_file_name);
			return ERR;
		}
	}

	LOGPRINT(patch, info, "Going to read patch file. ");

	/* Read. */
	if (1 != fread(buf, size, 1, ctrl->fp)) {
		LOGPRINT(patch, err, "error reading patch file");
		return ERR;
	}

	LOGPRINT(patch, info, "Finished reading patch file. ");

	return OK;
}

static int dpxy_inj_ptch_record(void *data, unsigned char *buf, unsigned int len)
{
	unsigned char ai2_pkt_header[5];
	unsigned char *ptr = ai2_pkt_header;
	struct ptch_dwld_ctrl *ctrl = (struct ptch_dwld_ctrl *) data;

	/* Dump to device. */
	ptr = BUF_LE_S1B_WR_INC(ptr, 0xF2);
	ptr = BUF_LE_S2B_WR_INC(ptr, len + 2);

	/* Special case: MSB first. */
	ptr = BUF_LE_S1B_WR_INC(ptr, (ctrl->cur_rec >> 8) & 0xFF);
	ptr = BUF_LE_S1B_WR_INC(ptr, ctrl->cur_rec & 0xFF);

	gettimeofday(patch_rec_time_start, NULL);

	if (ai2_ok != ai2_write(h2d_ai2_hdl, ai2_pkt_header, 5))
		goto error;

	if (ai2_ok != ai2_write(h2d_ai2_hdl, buf, len))
		goto error;

	if (ai2_ok != ai2_flush(h2d_ai2_hdl))
		goto error;

	return OK;

error:
	LOGPRINT(patch, err, "error injecting patch records");
	return ERR;
}

static int dpxy_inj_ptch_dwld_middle(void *data)
{
	unsigned char buf[PATCH_MAX_REC_SIZE];
	struct ptch_dwld_ctrl *ctrl = (struct ptch_dwld_ctrl *) data;
	struct ptch_dwld_cfg *cfg = get_ptch_dwld_cfg();
	int patch_first_rec_size;

	if (cfg->dwld_type == 0x03)
		patch_first_rec_size = 1120;
	else
		patch_first_rec_size = 128;

	if (0 == ctrl->cur_rec) {
		/* Update record details from patch header. */
		ptch_file_read(data, buf, PATCH_HEADER_SIZE);

		ctrl->rec_size = (buf[5] << 8) | buf[6];
		ctrl->rec_cnt = (buf[3] << 8) | buf[4];
		ctrl->last_rec_size = (buf[7] << 8) | buf[8];

		/* First record transmission. */
		ptch_file_read(data, buf, patch_first_rec_size);
		dpxy_inj_ptch_record(ctrl, buf, patch_first_rec_size);
		LOGPRINT(patch, info, "1st patch record injected");
		ctrl->cur_rec++;
	} else if (ctrl->cur_rec == ctrl->rec_cnt - 1) {
		/* Last record transmission. */
		ptch_file_read(data, buf, ctrl->last_rec_size);
		dpxy_inj_ptch_record(ctrl, buf, ctrl->last_rec_size);
		LOGPRINT(patch, info, "last patch record injected");
		ctrl->state = end;
	} else {
		/* Middle records transmission. */
		ptch_file_read(data, buf, ctrl->rec_size);
		dpxy_inj_ptch_record(ctrl, buf, ctrl->rec_size);
		ctrl->cur_rec++;
		LOGPRINT(patch, info, "%d patch record injected", ctrl->cur_rec);
	}

	return OK;
}

static int dpxy_inj_ptch_dwld_end(void *data)
{
	struct ai2_inj_ptch_dwld_end {
		unsigned int id;
		unsigned int len;
		unsigned int stopind;
	} end_msg[1];
	char ai2_inj_ptch_dwld_end_sizes[] = {1, 2, 3, 0};
	unsigned char mem[6];

	struct ptch_dwld_ctrl *ctrl = (struct ptch_dwld_ctrl *) data;
	/*
	 * Pop the 0xF3 recv command which was injected by default in the
	 * 'dpxy_get_dwld_stat'. For this download end indicator gnss
	 * device will send 0xF4.
	 */
	seq_pop_step(NULL);

	end_msg->id = 0xF2;
	end_msg->len = 3;
	end_msg->stopind = 0xFFFFFFFF;

	d2h_ai2_encode((void *)end_msg, mem, ai2_inj_ptch_dwld_end_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 6)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(patch, info, "patch download end injected");
						gettimeofday(patch_time_end, NULL);

						LOGPRINT(patch, info, "Patch total time[%u]", 
								 ((patch_time_end->tv_sec - patch_time_start->tv_sec) * 1000) 
								 + ((patch_time_end->tv_usec - patch_time_start->tv_usec) / 1000));

						ctrl->state =begin;  //Reset the state to begin
						fclose(ctrl->fp);
						ctrl->fp = NULL;
						ctrl->cur_rec =0;
						ctrl->last_rec_size = 0;
						ctrl->rec_cnt = 0;
			return OK;
		}
	}

        LOGPRINT(patch, err, "error sending ai2 packet to device");
	return ERR;
}
int dpxy_inj_ptch_dwld(void *data)
{
	int ret = OK;

	switch(seq_ptch_ctrl->state) {
		case begin:
		case nopatch:
			ret = dpxy_inj_ptch_dwld_begin(seq_ptch_ctrl);
			break;
		case middle:
			ret = dpxy_inj_ptch_dwld_middle(seq_ptch_ctrl);
			break;
		case end:
			ret = dpxy_inj_ptch_dwld_end(seq_ptch_ctrl);
			break;
		default:
			break;
	}

	UNUSED(data);

	return ret;
}

int dpxy_get_dwld_stat(void *data)
{
	struct seq_step step[1];

	gettimeofday(patch_rec_time_end, NULL);

	LOGPRINT(patch, info, "Patch record time[%u] and record no. [%d]", 
			 ((patch_rec_time_end->tv_sec - patch_rec_time_start->tv_sec) * 1000) 
			 + ((patch_rec_time_end->tv_usec - patch_rec_time_start->tv_usec) / 1000), 
			 seq_ptch_ctrl->cur_rec);

	/* Push the patch download status handler. */
	step->id = 0xF3;
	step->action = recv;
	step->trigger = dpxy_get_dwld_stat;

	if (SEQ_OK != seq_push_step(step))
		LOGPRINT(patch, err, "error pushing next sequence");

	/* Push the patch download next step. */
	step->id = 0xF2;
	step->action = send;
	step->trigger = dpxy_inj_ptch_dwld;

	if (SEQ_OK != seq_push_step(step))
		LOGPRINT(patch, err, "error pushing next sequence");

        UNUSED(data);
	return OK;
}

/*------------------------------------------------------------------------------
 * Configurations
 *----------------------------------------------------------------------------*/
int dpxy_get_cust_cfg(void *data)
{
        UNUSED(data);
	return OK;
}

int dpxy_inj_cust_cfg(void *data)
{
 	unsigned char mem[84];
	unsigned char *buf = mem;

	buf = BUF_LE_U1B_WR_INC(buf, 0xDD);
	buf = BUF_LE_U2B_WR_INC(buf, 81);
	/* : Complement me. */

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 84)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "custom config injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}

struct ai2_recv_cfg {
	unsigned int id;
	unsigned int len;
	unsigned int constellation;
	unsigned int dgps_correction;
	unsigned int max_interval;
	unsigned int server_mode;
	unsigned int reserved1;
	unsigned int fdic_mode;
	unsigned int alt_hold_mode;
	unsigned int max_clk_freq_uncert;
	unsigned int max_clk_accel;
	unsigned int max_usr_accel;
	int elev_mask;
	unsigned int pdop_mask;
	unsigned int feature_mask;
	unsigned int pdop_2d_mask;
	unsigned int timestamp_evt_edge;
	unsigned int reserved2;
	unsigned int op_mode_reserved;
	unsigned int reserved3;
};
char ai2_recv_cfg_sizes[] = {1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1,
1, 1, 1, 4, 1, 1, 0};

int dpxy_inj_req_recv_cfg(void *data)
{
	unsigned char mem[3];

	mem[0] = 0x07;
	mem[1] = 0;
	mem[2] = 0;

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "receiver config request injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}

struct ai2_notch_filter_cfg {
	unsigned int id;
	unsigned int len;
	unsigned int sub_pkt_id;
	unsigned int request;
	unsigned int pre_def_freq1;
	unsigned int pre_def_freq2;
	unsigned int pre_def_freq3;
	unsigned int pre_def_freq4;
	unsigned int notch_enable;
	unsigned int data_log_ctrl;
	unsigned int coarse_sniff_cnt;
	unsigned int notch_filter_bw;
	unsigned int sleep_time;
};

char ai2_notch_filter_cfg_sizes[] = {1, 2, 1, 1, 4, 4, 4, 4, 1, 1, 1, 1, 1};

int dpxy_inj_enable_notch_filter(void *data)
{
	struct ai2_notch_filter_cfg recv_cfg[1];
	unsigned char mem[26];
	unsigned int *notch_config;

	if (cfg_success != sys_param_get(cfg_notch_filter, 
					 (void **)&notch_config)) {
		LOGPRINT(cfg, warn, "err read notch filter enable");
		return OK;
	}

	LOGPRINT(cfg, info, "notch filter enable status [%d]", *notch_config);

	if (*notch_config) {
		usleep(100000);

		recv_cfg->id = 0xFB;
		recv_cfg->len = 0x0017;
		recv_cfg->sub_pkt_id = 0x27;
		recv_cfg->request = 0x00;
		recv_cfg->pre_def_freq1 = 0x5F4ECDC0;
		recv_cfg->pre_def_freq2 = 0x5F5F5585;
		recv_cfg->pre_def_freq3 = 0x00000000;
		recv_cfg->pre_def_freq4 = 0x00000000;
		recv_cfg->notch_enable = 0x03;
		recv_cfg->data_log_ctrl = 0x00;
		recv_cfg->coarse_sniff_cnt = 0x00;
		recv_cfg->notch_filter_bw = 0x09;
		recv_cfg->sleep_time = 0x05;

		d2h_ai2_encode((void *)recv_cfg, mem, 
			       ai2_notch_filter_cfg_sizes);

		if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 26)) {
			if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
				LOGPRINT(cfg, info, "notch filter config injected");
				return OK;
			}
		}

		UNUSED(data);
		LOGPRINT(cfg, err, "error sending ai2 packet to device");
		return ERR;
	}

	return OK;
}

int dpxy_inj_recv_cfg(void *data)
{
#define LIGHT_SEC      (299792458.0)         /* m/s */

	struct ai2_recv_cfg recv_cfg[1];
	unsigned char mem[27];
	unsigned char *constellation, *alt, *time_evt ;
	char *elevation;
	unsigned int *freq;
	unsigned short *clock_acc, *user_acc;

	sys_param_get(cfg_const_config,(void**) &constellation);
	LOGPRINT(cfg, info, "cfg_const_config [%d]", *constellation);
	sys_param_get(cfg_alt_mode, (void**) &alt);
	LOGPRINT(cfg, info, "cfg_alt_mode [%d]", *alt);
	sys_param_get(cfg_tcxo_long_term_freq_unc, (void**) &freq);
	LOGPRINT(cfg, info, "cfg_gps_frequn [%d]", *freq);
	sys_param_get(cfg_gps_acc, (void**) &clock_acc);
	LOGPRINT(cfg, info, "cfg_gps_acc [%d]", *clock_acc);
	sys_param_get(cfg_usr_acc, (void**) &user_acc);
	LOGPRINT(cfg, info, "cfg_usr_acc [%d]", *user_acc);
	sys_param_get(cfg_ele_mask, (void**) &elevation);
	LOGPRINT(cfg, info, "cfg_ele_mask [%d]", *elevation);
	sys_param_get(cfg_time_event, (void**) &time_evt);
	LOGPRINT(cfg, info, "cfg_time_event [%d]", *time_evt);
	
	recv_cfg->id = 0x08;
	recv_cfg->len = 0x0018;
	recv_cfg->constellation = *constellation;
	recv_cfg->dgps_correction = 0x01;
	recv_cfg->max_interval = 0x3C;
	recv_cfg->server_mode = 0x01;
	recv_cfg->reserved1 = 0x00;
	recv_cfg->fdic_mode = 0x01;
	recv_cfg->alt_hold_mode = *alt;
	recv_cfg->max_clk_freq_uncert = ((*freq) * LIGHT_SEC * 1e-6);
	recv_cfg->max_clk_accel = *clock_acc;
	recv_cfg->max_usr_accel = *user_acc;
	recv_cfg->elev_mask = *elevation;
	recv_cfg->pdop_mask = pdop_mask;
	recv_cfg->feature_mask = 0x0E;
	recv_cfg->pdop_2d_mask = pdop_2d_mask;
	recv_cfg->timestamp_evt_edge = *time_evt;
	recv_cfg->reserved2 = 0x00000000;
	recv_cfg->op_mode_reserved = 0x01;
	recv_cfg->reserved3 = 0x00;

	d2h_ai2_encode((void *)recv_cfg, mem, ai2_recv_cfg_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 27)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(cfg, info, "receiver config injected");
			return OK;
		}
	}

	UNUSED(data);
	LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}


int dpxy_inj_rtc_inj_ctrl(void *data)
{
	struct ai2_rtc_inj_ctrl {
		unsigned int id;
		unsigned int len;
		unsigned int sub_pkt_id;
		unsigned int rtc_inj_ctrl;
		unsigned int rtc_time_qual_src;
		unsigned int rtc_qual;
	} rtc[1];
	char ai2_rtc_inj_ctrl_sizes[] = {1, 2, 1, 1, 1, 2, 0};
	unsigned char mem[8];
	unsigned int *inj_ctrl;
	unsigned int *tim_qual_src;
	unsigned int *rtc_unc;

	rtc->id = 0xFB;
	rtc->len = 5;
	rtc->sub_pkt_id = 0;

	sys_param_get(cfg_rtc_inj_ctrl,(void**) &inj_ctrl);
	LOGPRINT(cfg, info, "cfg_rtc_inj_ctrl [%d]", *inj_ctrl);
	rtc->rtc_inj_ctrl = *inj_ctrl;

	sys_param_get(cfg_rtc_time_qual_src,(void**) &tim_qual_src);
	LOGPRINT(cfg, info, "cfg_rtc_time_qual_src [%d]", *tim_qual_src);
	rtc->rtc_time_qual_src = *tim_qual_src;

	sys_param_get(cfg_host_rtc_unc ,(void**) &rtc_unc);
	LOGPRINT(cfg, info, "cfg_rtc_unc [%d]", *rtc_unc);
	rtc->rtc_qual = (*rtc_unc);

	d2h_ai2_encode((void *)rtc, mem, ai2_rtc_inj_ctrl_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 8)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "rtc control injected");
			return OK;
		}
	}

    UNUSED(data);
    LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}

#define SEQ_BASIC_DEVICE_EVT 0x00000040
struct ai2_evt_cfg {
	unsigned int id;
	unsigned int len;
	unsigned int port_no;
	unsigned int periodic_rpts;
	unsigned int async_evt_rpts;
	unsigned int periodic_rept_rate;
	unsigned int sub_sec_rate;
	unsigned int reserved;
};
char ai2_evt_cfg_sizes[] = {1, 2, 1, 4, 4, 2, 1, 1, 0};


int dpxy_inj_init_evt_cfg(void *data)
{
	struct ai2_evt_cfg evt[1];
	unsigned char mem[16];

	evt->id = 0x06;
	evt->len = 13;
	evt->port_no = 1;
	evt->periodic_rpts = 0;
	evt->async_evt_rpts = SEQ_BASIC_DEVICE_EVT; /* Engine events */
	evt->periodic_rept_rate = 0;
	evt->sub_sec_rate = 0;
	evt->reserved = 0;

	d2h_ai2_encode((void *)evt, mem, ai2_evt_cfg_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 16)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "initial event config injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}

int dpxy_get_evt_cfg(void *data)
{
        UNUSED(data);
	return OK;
}

int dpxy_evt_confg(void *data)
{
	struct ai2_evt_cfg {
		unsigned int id;
		unsigned int len;
		unsigned int prt_num;
	} evt_cfg[1];
	char ai2_evt_cfg_sizes[] = {1, 2, 1,0};
	unsigned char mem[4];

	evt_cfg->id = 0x05;
	evt_cfg->len = 1;
	evt_cfg->prt_num = 1;

	d2h_ai2_encode((void *)evt_cfg, mem, ai2_evt_cfg_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "dpxy_evt_confg");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(seq, err, "dpxy_evt_confg error");
	return ERR;

}

int dpxy_get_recvr_cfg(void* data)
{
	struct ai2_recvr_cfg {
		unsigned int id;
		unsigned int len;
	} recvr_cfg[1];
	char ai2_recvr_cfg_sizes[] = {1, 2, 0};
	unsigned char mem[3];

	recvr_cfg->id = 0x07;
	recvr_cfg->len = 0;

	d2h_ai2_encode((void *)recvr_cfg, mem, ai2_recvr_cfg_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "dpxy_get_recvr_cfg");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(seq, err, "dpxy_get_recvr_cfg error");
	return ERR;
}

int dpxy_get_pos_ext(void* data)
{
	struct ai2_pos_ext {
		unsigned int id;
		unsigned int len;
	} pos_ext[1];
	char ai2_pos_ext_sizes[] = {1, 2, 0};
	unsigned char mem[3];

	pos_ext->id = 0xF4;
	pos_ext->len = 0;

	d2h_ai2_encode((void *)pos_ext, mem, ai2_pos_ext_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "dpxy_get_pos_ext");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(seq, err, "dpxy_get_pos_ext error");
	return ERR;
}

int dpxy_get_meas(void* data)
{
	struct ai2_meas {
		unsigned int id;
		unsigned int len;
	} meas[1];
	char ai2_meas_sizes[] = {1, 2, 0};
	unsigned char mem[3];

	meas->id = 0x0E;
	meas->len = 0;

	d2h_ai2_encode((void *)meas, mem, ai2_meas_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "dpxy_get_meas");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(seq, err, "dpxy_get_meas error");
	return ERR;
}

int dpxy_get_meas_status(void* data)
{
	struct ai2_meas_status {
		unsigned int id;
		unsigned int len;
	} meas_stat[1];
	char ai2_meas_stat_sizes[] = {1, 2, 0};
	unsigned char mem[3];

	meas_stat->id = 0x0F;
	meas_stat->len = 0;

	d2h_ai2_encode((void *)meas_stat, mem, ai2_meas_stat_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "dpxy_get_meas_status");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(seq, err, "dpxy_get_meas_status error");
	return ERR;
}

int dpxy_ping(void* data)
{
	struct ai2_ping {
		unsigned int id;
		unsigned int len;
	} ping[1];
	char ai2_ping_sizes[] = {1, 2, 0};
	unsigned char mem[3];

	ping->id = 0x00;
	ping->len = 0;

	d2h_ai2_encode((void *)ping, mem, ai2_ping_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 3)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(seq, info, "dpxy_ping");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(seq, err, "dpxy_ping error");
	return ERR;
}
int dpxy_fs_flush(void *data)
{
	LOGPRINT(generic, info, "dpxy_fs_flush: enter: %d", os_time_msecs());

	ai2_log_flush();

	nvs_flush();

	LOGPRINT(generic, info, "dpxy_fs_flush: exit: %d", os_time_msecs());
	UNUSED(data);
	return OK;
}

static int dpxy_set_msr(int flag)
{
	/* 3GPP. */
	d2h_set_report_format(d2h_msr, flag);

	if (flag)
		return 0x40000000;

	return 0;
}

static int dpxy_set_pos(int flag)
{
	int ai2_flag = 0;

	/* 3GPP. */
	d2h_set_report_format(d2h_pos, flag);

	d2h_set_report_format(d2h_clk, flag);

	/* GPS Time. */
	if ((flag & POS_RPT_TIM_GPS_NATIVE) ||
		(flag & POS_RPT_TIM_GPS_IE_SEL))
		ai2_flag |= 0x00000000;

	/* GLO Time. */
	if ((flag & POS_RPT_TIM_GLO_NATIVE) ||
		(flag & POS_RPT_TIM_GLO_IE_SEL))
		ai2_flag |= 0x00000040;

	/* Extended Pos. */
	if ((flag & POS_RPT_LOC_NATIVE) ||
		(flag & POS_RPT_GPS_IE_SEL) ||
		(flag & POS_RPT_GLO_IE_SEL) ||
		(flag & POS_RPT_SAT_NATIVE) || 
		(flag & POS_RPT_DOP_NATIVE))
		ai2_flag |= 0x80000000;

	return ai2_flag;
}

static int dpxy_set_async(int flag)
{
	int ai2_flag = 0;

	/* add for GNSS recovery ++*/		
	ai2_flag |= ((flag & 0x00000001)? (1 << 0) : 0);	
	ai2_flag |= ((flag & 0x00000002)? (1 << 1) : 0);	
	ai2_flag |= ((flag & 0x00000004)? (1 << 2) : 0);	
	ai2_flag |= ((flag & 0x00000008)? (1 << 3) : 0);
	/* add for GNSS recovery --*/
	ai2_flag |= ((flag & 0x00000010)? (1 << 4) : 0);
	ai2_flag |= ((flag & 0x00000020)? (1 << 5) : 0);
	ai2_flag |= ((flag & 0x00000040)? (1 << 6) : 0);
	ai2_flag |= ((flag & 0x00000080)? (1 << 7) : 0);
	ai2_flag |= ((flag & 0x00000100)? (1 << 8) : 0);

	ai2_flag |= ((flag & 0x00000200)? (1 << 9) : 0);/* add for GNSS recovery ++*/
	return SEQ_BASIC_DEVICE_EVT | ai2_flag;
}

static int dpxy_set_bcast(int flag)
{
	int ai2_flag = 0;

	ai2_flag |= ((flag & 0x00000001)? (1 << 0) : 0);
	ai2_flag |= ((flag & 0x00000002)? (1 << 1) : 0);
	ai2_flag |= ((flag & 0x00000004)? (1 << 2) : 0);
	ai2_flag |= ((flag & 0x00000008)? (1 << 3) : 0);
	ai2_flag |= ((flag & 0x00000200)? (1 << 9) : 0);

	return ai2_flag;
}

int dpxy_send_host_ver(void *data)
{
/* this function is used to send the host sw release verison to the Chip
* this info is of no use to the chip, but sending this info as an 
* AI2 packet allows for recording the version in the sensor log files
* this will be helpful during debug activities.
* this particular packet is an unused custom packet and 
* is expected not to interfere with the operations of the chip
*/
struct ai2_ver {
	unsigned int id;
	unsigned int len;
	unsigned int sub_id;
	unsigned int major;
	unsigned int minor;
	unsigned int patch;
	unsigned int build;
};
	char ai2_ver_sizes[] = {1, 2, 1, 1, 1, 1, 1, 0};
	struct ai2_ver version[1];

	unsigned char mem[8];

	version->id = 0xFB;
	version->len = 5;
	version->sub_id = 0xD1;

	version->major = MAJOR;
	version->minor = MINOR;
	version->patch = PATCH;
	version->build = BUILD;

	d2h_ai2_encode((void *)version, mem, ai2_ver_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, sizeof(mem))) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(ver, info, "host version injected to the Chip");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(ver, err, "error sending ai2 packet to device");
	return ERR;

	

}

int dpxy_inj_evt_cfg(void *data)
{
	struct ai2_evt_cfg evt[1];
	unsigned char mem[16];

	struct dev_rpt_sel *rpt = (struct dev_rpt_sel *) data;

	evt->id = 0x06;
	evt->len = 13;
	evt->port_no = 1;
	evt->periodic_rpts = 0x00000021 | dpxy_set_msr(rpt->msr_reports) |dpxy_set_pos(rpt->pos_reports);
	//evt->periodic_rpts = 0x00000020 | dpxy_set_msr(rpt->msr_reports) |dpxy_set_pos(rpt->pos_reports);
	evt->async_evt_rpts = dpxy_set_async(rpt->async_event);
	evt->async_evt_rpts |= dpxy_set_bcast(rpt->bcast_agnss);
	evt->periodic_rept_rate = rpt->update_secs;
	evt->sub_sec_rate = 0;
	evt->reserved = 0;

	d2h_ai2_encode((void *)evt, mem, ai2_evt_cfg_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 16)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "event config injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}

extern char ai2_rec_action_sizes[];
int dpxy_inj_dev_off(void *data)
{
	struct ai2_rec_action action[1];
	unsigned char mem[4];

	action->id = 0x02;
	action->len = 1;
	action->action_type = 1;

	d2h_ai2_encode((void *)action, mem, ai2_rec_action_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(rx_off, info, "receiver off injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(rx_off, err, "error sending ai2 packet to device");
	return ERR;
}

int dpxy_inj_dev_idle(void *data)
{
	struct ai2_rec_action action[1];
	unsigned char mem[4];

	action->id = 0x02;
	action->len = 1;
	action->action_type = 2;

	d2h_ai2_encode((void *)action, mem, ai2_rec_action_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(rx_idle, info, "receiver idle injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(rx_idle, err, "error sending ai2 packet to device");
	return ERR;
}

int dpxy_inj_dev_on(void *data)
{
	struct ai2_rec_action action[1];
	unsigned char mem[4];

	action->id = 0x02;
	action->len = 1;
	action->action_type = 3;

	d2h_ai2_encode((void *)action, mem, ai2_rec_action_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(rx_on, info, "receiver on injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(rx_on, err, "error sending ai2 packet to device");
	return ERR;
}

int dpxy_inj_dev_reset(void *data)
{
	struct ai2_rec_action action[1];
	unsigned char mem[4];

	action->id = 0x02;
	action->len = 1;
	action->action_type = 0;

	d2h_ai2_encode((void *)action, mem, ai2_rec_action_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(rx_idle, info, "receiver reset injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(rx_idle, err, "error sending ai2 packet to device");
	return ERR;
}
struct ai2_rec_action_wakeup {
	unsigned int id;
	unsigned int len;
	unsigned int sub_pkt_id;
	unsigned int region_num;
};

static char ai2_rec_action_wakeup_size[] = {1, 2, 1, 2, 0};

int dpxy_inj_dev_wakeup(void *data)
{
	struct ai2_rec_action_wakeup action[1];
	unsigned char mem[6];

	action->id = 0xFB;
	action->len = 3;
	action->sub_pkt_id = 0x19;
	action->region_num = 0;

	d2h_ai2_encode((void *)action, mem, ai2_rec_action_wakeup_size);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 6)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(rx_wake, info, "receiver wakeup injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(rx_wake, err, "error sending ai2 packet to device");
	return ERR;
}

int dpxy_inj_nmea_out_ctrl(void *data)
{
	struct ai2_nmea_out_ctrl {
		unsigned int id;
		unsigned int len;
		unsigned int ser_1_nmea_ctrl;
		unsigned int ser_2_nmea_ctrl;
	} ctrl[1];
	char ai2_nmea_out_ctrl_sizes[] = {1, 2, 2, 2, 0};
	unsigned char mem[7];

	ctrl->id = 0xE5;
	ctrl->len = 4;
	ctrl->ser_1_nmea_ctrl = ((struct dev_rpt_sel *)data)->nmea_select;
	ctrl->ser_2_nmea_ctrl = 0;

	d2h_ai2_encode((void *)ctrl, mem, ai2_nmea_out_ctrl_sizes);
	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 7)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
                        LOGPRINT(cfg, info, "nmea out control injected");
			return OK;
		}
	}

        UNUSED(data);
        LOGPRINT(cfg, err, "error sending ai2 packet to device");
	return ERR;
}

struct ai2_gps_shutdown{
	unsigned int id;
	unsigned int len;
	unsigned int sub_pkt_id;
	unsigned int reserved;
	unsigned int shutdwn_ctrl;
};

static char ai2_gps_shutdown_size[] = {1,2,1,1,1,0};

int dpxy_inj_gps_shutdwn_ctrl(void* data)
{
	struct ai2_gps_shutdown shutdwn[1];
	unsigned char mem[6];
	
	shutdwn->id = 0xFB;
	shutdwn->len = 3;
	shutdwn->sub_pkt_id = 0x17;
	shutdwn->reserved = 0;
	shutdwn->shutdwn_ctrl = 0;

	d2h_ai2_encode((void *)shutdwn, mem, ai2_gps_shutdown_size);
	
	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 6)) {
			if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
							LOGPRINT(cfg, info, "GPS shutdown control injected");
				return OK;
			}
		}
	
			UNUSED(data);
			LOGPRINT(cfg, err, "error sending ai2 packet to device");
		return ERR;
}


struct ai2_pps_out_ctrl{
	unsigned int id;
	unsigned int len;
	unsigned int pps_out;
	unsigned int pps_polarity;
};

static char ai2_pps_out_size[] = {1,2,1,1,0};

int dpxy_inj_pps_out_ctrl(void* data)
{
	struct ai2_pps_out_ctrl ppsout[1];
	unsigned char mem[5];
	
	ppsout->id = 0xEE;
	ppsout->len = 2;
	ppsout->pps_out = 0x01; // Enabled
	ppsout->pps_polarity = 0x00; //Rising edge 

	d2h_ai2_encode((void *)ppsout, mem, ai2_pps_out_size);
	
	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 5)) {

			if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
							LOGPRINT(cfg, info, "PPS out control injected");
				return OK;
			}
		}
	
			UNUSED(data);
			LOGPRINT(cfg, err, "error sending ai2 packet to device");
		return ERR;
}


struct ai2_pinmux_cfg{
	unsigned int id;
	unsigned int len;
	unsigned int gps_ext_lna_en;
	unsigned int gps_time_stamp;
	unsigned int gps_sense_i2c_scl;
	unsigned int gps_sense_i2c_sda;
	unsigned int gps_ant_select;
	unsigned int gps_pps_out;
	unsigned int gps_pa_en;
	unsigned int gps_sens_pwm;
	unsigned int gnss_pfix;	
	unsigned int tcxo_temp_sens_in;		
};

static char ai2_pinmux_cfg_size[] = {1,2,3,3,3,3,3,3,3,3,3,3,0};

int dpxy_inj_pinmux_cfg(void* data)
{
	struct ai2_pinmux_cfg pinmux[1];
	unsigned char mem[33];

	pinmux->id = 0x39;
	pinmux->len = 0x1E;
	pinmux->gps_ext_lna_en = 0x00;
	pinmux->gps_time_stamp = 0x00;
	pinmux->gps_sense_i2c_scl = 0x00;
	pinmux->gps_sense_i2c_sda = 0x00;
	pinmux->gps_ant_select = 0x00;
	pinmux->gps_pps_out = 0x04; // BT_FUNC7
	pinmux->gps_pa_en =0x00;
	pinmux->gps_sens_pwm = 0x00;
	pinmux->gnss_pfix = 0x00;
	pinmux->tcxo_temp_sens_in = 0x00;
	
	d2h_ai2_encode((void *)pinmux, mem, ai2_pinmux_cfg_size);
	
	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 33)) {
			if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
							LOGPRINT(cfg, info, "pin muc config control injected");
				return OK;
			}
		}
	
			UNUSED(data);
			LOGPRINT(cfg, err, "error sending ai2 packet to device");
		return ERR;
}


int h2d_dev_maximize_pdop(struct srv_rei *srv_re, struct ie_desc *ie_adu)
{
	int ret_val = ERR;
	unsigned int pdop_mask_temp = pdop_mask;
	unsigned int pdop_2d_mask_temp = pdop_2d_mask;
	unsigned int *pdop_mask_time_out;

	sys_param_get(cfg_pdop_mask_time_out,(void**) &pdop_mask_time_out);
	LOGPRINT(cfg, info, "cfg_pdop_mask_time_out [%d]", *pdop_mask_time_out);
	UNUSED(srv_re);

	pdop_mask = *pdop_mask_time_out;
	pdop_2d_mask = *pdop_mask_time_out;

	ret_val = dpxy_inj_recv_cfg(ie_adu->data);

	pdop_mask = pdop_mask_temp;
	pdop_2d_mask = pdop_2d_mask_temp;

	return ret_val;
}

/* Read the GPS Freq Unc from NVS File & Inject the Freq  Estimate */
int dpxy_inj_freq_est(void *data)
{
#define LIGHT_SEC          (299792458.0f)

	struct dev_gps_tcxo tcxo[1];
	struct dev_gps_tcxo tcxo1;
	unsigned int *longterm_freq_unc, *shortterm_freq_unc;
	
	sys_param_get(cfg_tcxo_long_term_freq_unc,(void**) &longterm_freq_unc);
	sys_param_get(cfg_tcxo_short_term_freq_unc,(void**) &shortterm_freq_unc);
	
	if( nvs_aid_get(nvs_gps_tcxo, &tcxo[0], 1) > 0 ){
		LOGPRINT(cfg, info, "using short term unc");
		tcxo[0].freq_unc = ((*shortterm_freq_unc) * LIGHT_SEC * 1e-6);
	}
	else{
		LOGPRINT(cfg, info, "using long term unc");
		tcxo[0].freq_bias = 0;
		tcxo[0].freq_unc = ((*longterm_freq_unc) * LIGHT_SEC * 1e-6);
	}
	
	return (h2d_inj_dev_freq_est(&tcxo[0],1));
}


int h2d_dproxy_gnss_core_off(void *data)
{
	int ret_val = 1;
	char gnss_core_off[] = {0x01, 0xc0, 0xff, 0x01, 0x00};

	unsigned int len = 5;


	return ret_val;
}


int h2d_dproxy_gnss_core_on(void *data)
{
	int ret_val = 1;
	char gnss_core_on[] = {0x01, 0xc0, 0xff, 0x01, 0x01};

	unsigned int len = 5;


	return ret_val;
}


int h2d_dproxy_hard_reset(void *data)
{
	int ret_val=1;


	return ret_val;
}
