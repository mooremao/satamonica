
/*
 * calib.h
 *
 * Calibeartion of TCXO
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/


int dpxy_inj_osc_freq(void *data );

int inj_calib_ctrl(unsigned char flag);
int dpxy_inj_calib_ctrl_single_shot(void *data);
int dpxy_inj_calib_ctrl_periodic(void *data);
int dpxy_get_calib_resp(void *data);
int dpxy_undo_freq_est(void *data);



