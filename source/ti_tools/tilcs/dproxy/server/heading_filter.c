/*
 * heading_filter.c
 *
 * Position report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#include <math.h>
#include <stdio.h>
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include "utils.h"
#include "logger.h"
#include "heading_filter.h"


/** LOCAL **/
static struct HdgFiltStateVar HdgFiltVar= {50.0,0,0,1,1};


void HdgFiltInit(void)
{
	HdgFiltVar.StateNoise  = 50.0;
	HdgFiltVar.StateNew    = 0;
	HdgFiltVar.StatePrev   = 0;
	HdgFiltVar.FilterStart = 1;

}

int init_Heading_Filter (void *data)
{
	HdgFiltInit();
	UNUSED(data);
	return OK;
}

void HdgFilter(struct VelocityVar* CurrentVelocityVar, 
				struct VelocityVar* FilteredVelocityVar)
{
	float tempHdg = 0, DeltaHdg = 0;
	double AbsVel = 0, RNoise = 0, QNoise = 0, KState = 0;
	
	LOGPRINT(pos_rpt,info,"HdgFilter: In: TC[%u]VEast[%f] VNorth[%f] Head[%f]",
				CurrentVelocityVar->TCount,
				CurrentVelocityVar->VelEast,
				CurrentVelocityVar->VelNorth,
				CurrentVelocityVar->Heading);

	LOGPRINT(pos_rpt,info,"HdgFilter: StateNoise[%f] StateNew[%f] StatePrev[%f] FilterStart[%d]",
				HdgFiltVar.StateNoise,
				HdgFiltVar.StateNew,
				HdgFiltVar.StatePrev,
				HdgFiltVar.FilterStart);

	// find the absolute horrizontal velocity using the N and E velocity components
	AbsVel = sqrt(pow(CurrentVelocityVar->VelEast,2) + pow(CurrentVelocityVar->VelNorth, 2));

	// maintain a copy of the current heading
	tempHdg = CurrentVelocityVar->Heading;

	// Check if this is the first sample in this session
	// If yes initialize the heading filter variables
	// Also we may need to include a check based on f_count to
	// detrmine if we need to reset the filter as there is a discontinuity
	// in received fixes

	if (HdgFiltVar.FilterStart == 1){
		LOGPRINT(pos_rpt,info,"HdgFilter: FilterStart == 1");
		HdgFiltInit();
		HdgFiltVar.FilterStart ++;
	}

	// if the velocity is less than 0.5m/s propagate the previous heading
	// retain the current velocity

	if (AbsVel < 0.5){
		LOGPRINT(pos_rpt,info,"HdgFilter: AbsVel < 0.5 [%f]",AbsVel);
		FilteredVelocityVar->Heading = (float)HdgFiltVar.StatePrev;
		FilteredVelocityVar->VelEast = CurrentVelocityVar->VelEast;
		FilteredVelocityVar->VelNorth = CurrentVelocityVar->VelNorth;
	}
	else{
		LOGPRINT(pos_rpt,info,"HdgFilter: AbsVel >= 0.5 [%f]",AbsVel);
		DeltaHdg = tempHdg - (float)HdgFiltVar.StatePrev;

		if(DeltaHdg < -180.0){
			DeltaHdg += 360.0;
		}
		else if (DeltaHdg > 180.0){
			DeltaHdg -= 360.0;
		}

		RNoise = 4.0 / pow(AbsVel,2);

		/**** Kalman Filter Starts Here ********/
		QNoise = (AbsVel < 2.0) ? QNOISE_FACT1 : QNOISE_FACT2;

		HdgFiltVar.StateNoise += QNoise;
		KState = HdgFiltVar.StateNoise /(RNoise + HdgFiltVar.StateNoise);
		HdgFiltVar.StateNew = HdgFiltVar.StatePrev + (KState * DeltaHdg);
		HdgFiltVar.StateNoise = (pow((1 - KState),2)* HdgFiltVar.StateNoise) + (pow(KState,2) * RNoise);

		HdgFiltVar.StatePrev = HdgFiltVar.StateNew;

		FilteredVelocityVar->Heading = (float)HdgFiltVar.StateNew;

		// compute new velocity based on filtered heading
		FilteredVelocityVar->VelEast = (float)((double)AbsVel * sin(FilteredVelocityVar->Heading * (PI/180.0)));
		FilteredVelocityVar->VelNorth = (float)((double)AbsVel * cos(FilteredVelocityVar->Heading * (PI/180.0)));
	}

	if (FilteredVelocityVar->Heading < 0)
		FilteredVelocityVar->Heading += 360;
	else if (FilteredVelocityVar->Heading >= 360.0)
		FilteredVelocityVar->Heading -= 360;

	HdgFiltVar.TCount = CurrentVelocityVar->TCount;

	LOGPRINT(pos_rpt,info,"HdgFilter: Out: TC[%u]VEast[%f] VNorth[%f] Head[%f]",
				CurrentVelocityVar->TCount,
				FilteredVelocityVar->VelEast,
				FilteredVelocityVar->VelNorth,
				FilteredVelocityVar->Heading);

	LOGPRINT(pos_rpt,info,"HdgFilter: StateNoise[%f] StateNew[%f] StatePrev[%f] FilterStart[%d]",
				HdgFiltVar.StateNoise,
				HdgFiltVar.StateNew,
				HdgFiltVar.StatePrev,
				HdgFiltVar.FilterStart);
}

