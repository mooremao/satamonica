/*
 * dev2host.h
 *
 * Framework to handle the data from device towards host.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _DEV2HOST_H_
#define _DEV2HOST_H_

#include "rpc.h"

struct d2h_ai2; /* Forward declaration */

typedef int (*d2h_process_fn)(struct d2h_ai2 *ai2_d2h);
typedef int (*d2h_ie_4rpc_fn)(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
						unsigned int rei_select);

struct d2h_ai2
{

	unsigned int    ai2_pkt_sz;   /* Size of Raw AI2 packet from device */
	unsigned char   *ai2_packet;   /* Raw AI2 pkt received from the dev  */

	void           *ai2_struct;   /* Translated C struct of dev AI2 msg */
	unsigned int    ai2_st_len;   /* Size of translated C msg structure */

	d2h_process_fn  init_fixup;   /* Set d2h before start of processing */
	d2h_process_fn  st_set_mem;   /* Setup internal memory for C struct */
	d2h_process_fn  gen_struct;   /* Generate C structure from RAW data */
	d2h_process_fn  first_prep;   /* First chance of fixing up C struct */
	d2h_process_fn  assess_qoi;   /* Assess quality of info in C struct */
	d2h_process_fn  nvs_action;   /* NVS action: Read or write into NVS */
	d2h_process_fn  final_prep;   /* Final chance of fixing up C struct */

	d2h_ie_4rpc_fn  do_ie_4rpc;   /* Translate C struct to intended IEs */

	unsigned int    rei_select;   /* Identifies RPC(s) to be originated */
};

void *d2h_thread(void *arg);

unsigned char d2h_get_msg_id(struct d2h_ai2 *ai2_d2h);


enum d2h_report {

	d2h_pos,
	d2h_msr,
	d2h_eph,
	d2h_alm,
	d2h_ion,
	d2h_utc,
	d2h_clk

		/* Add more */
		/* d2h_blf --> later */
};

void d2h_set_report_format(enum d2h_report rpt_id, unsigned int report_flags);
unsigned int d2h_get_report_format(struct d2h_ai2 *ai2_d2h);
unsigned short d2h_ai2_key_id(unsigned char pkt_id,	unsigned char sub_id);

void d2h_sync_flg_update(void);

#endif
