/*
 * meas_report.c
 *
 * Measurement report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"

#define EXT_PVT_ST_LEN (32 * ST_SIZE(dev_msr_info))

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int meas_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
unsigned int st_size, struct dev_msr_info *dev_msr, unsigned int rei_select);

static struct ie_desc *mk_ie_adu_4meas_dev(struct rpc_msg *rmsg,
unsigned short oper_id, unsigned int st_size, struct dev_msr_info *dev_msr)
{
	struct ie_desc *ie_adu;

	ie_adu = rpc_ie_attach(rmsg, oper_id, OPER_INF, st_size, dev_msr);
	if(!ie_adu)
		goto exit1;

	return ie_adu;

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg
   -1 --> Error
 */
static int meas_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
unsigned int st_size, struct dev_msr_info *dev_msr, unsigned int rei_select)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	UNUSED(rei_select);

	ie_adu[n_adu] = mk_ie_adu_4meas_dev(rmsg, oper_id, st_size, dev_msr);
	if(!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while(--n_adu) {
		rpc_ie_detach(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int meas_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned short oper_id,
unsigned int st_size, struct dev_msr_info *dev_msr, unsigned int rei_select)
{
	return meas_create_dev_ie(rmsg, oper_id, st_size, dev_msr, rei_select);
}

static int meas_rpc_ie(struct d2h_ai2 *d2h_ai2, struct rpc_msg *rmsg,
					   unsigned int rei_select)
{
	struct dev_msr_info *dev_msr = (struct dev_msr_info*)d2h_ai2->ai2_struct;

	if(-1 == meas_create_dev_ie(rmsg, REI_DEV_MSR_GPS, d2h_ai2->ai2_st_len,
							dev_msr, rei_select))
		return -1;

	return 0;
}

static int dev_msr_struct(struct d2h_ai2 *d2h_ai2, int ai2_offset)
{
	struct dev_msr_info *dev_msr = (struct dev_msr_info *) 
							d2h_ai2->ai2_struct;
	unsigned char *ai2_pkt = (unsigned char *) d2h_ai2->ai2_packet + 
								ai2_offset;
	unsigned char *ai2_ref = ai2_pkt;
	unsigned short dev_msr_sz = 28;
	int n_sat = (d2h_ai2->ai2_pkt_sz - ai2_offset) / dev_msr_sz;
	
	d2h_ai2->ai2_st_len = n_sat * ST_SIZE(dev_msr_info);

//	int timer_count              = BUF_LE_U4B_RD_INC(ai2_pkt);

	while(n_sat--) {

		dev_msr->svid            = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->snr             = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->cno             = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->latency_ms      = BUF_LE_S2B_RD_INC(ai2_pkt);
		dev_msr->pre_int         = BUF_LE_U1B_RD_INC(ai2_pkt);
		dev_msr->post_int        = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->msec            = BUF_LE_U4B_RD_INC(ai2_pkt);
		dev_msr->sub_msec        = BUF_LE_U4B_RD_INC(ai2_pkt);
		dev_msr->time_unc        = BUF_LE_U2B_RD_INC(ai2_pkt);
		dev_msr->speed           = BUF_LE_S4B_RD_INC(ai2_pkt);
		dev_msr->speed_unc       = BUF_LE_U4B_RD_INC(ai2_pkt);
		dev_msr->msr_flags       = BUF_LE_U2B_RD_INC(ai2_pkt);

		/* Not present */
		dev_msr->ch_states       = 0;
		dev_msr->accum_cp        = 0;
		dev_msr->carrier_vel     = 0;
		dev_msr->carrier_acc     = 0;
		dev_msr->loss_lock_ind   = 0;
		dev_msr->good_obv_cnt    = 0;
		dev_msr->total_obv_cnt   = 0;
		dev_msr->sv_slot         = 0;
		dev_msr->diff_sv         = 0;
		dev_msr->early_term      = 0;
		dev_msr->elevation       = 0;
		dev_msr->azimuth         = 0;

		/* Reserved */
		ai2_pkt += 2;

		dev_msr++;
	}

	return (ai2_pkt - ai2_ref);
}

static int meas_struct(struct d2h_ai2 *d2h_ai2)
{
	int ai2_offset = 0;
	int ai2_pkt_sz = d2h_ai2->ai2_pkt_sz;

	if(ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_msr_struct(d2h_ai2, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

struct d2h_ai2 d2h_meas = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.gen_struct = meas_struct,
	.do_ie_4rpc = meas_rpc_ie
};

