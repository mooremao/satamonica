/*
 * glo_sv2slot_map.h
 *
 * GLO sv to slot mapping.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

int h2d_inj_glo_sv2slot_map(void *data, int num);

int h2d_inj_req_glo_sv2slot_map(void *data, int num);


