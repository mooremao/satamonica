/*
 * ai2_comm.c
 *
 * This module takes care abstract the transport mechanism between host and
 * GNSS device.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <strings.h>
#include <errno.h>

#include "ai2.h"
#include "config.h"
#include "ai2_comm.h"
#include "logger.h"
#include "utils.h"

static int ai2_comm_hdl = -1;

static void ai2_comm_print_err(int error_num)
{
	switch(error_num) {
	case EINTR:
		LOGPRINT(ai2, err, "ai2_comm select() returned EINTR");
		break;
	case EBADF:
		LOGPRINT(ai2, err, "ai2_comm select() returned EBADF");
		break;
	case EINVAL:
		LOGPRINT(ai2, err, "ai2_comm select() returned EINVAL");
		break;
	case ENOMEM:
		LOGPRINT(ai2, err, "ai2_comm select() returned ENOMEM");
		break;
	default:
		LOGPRINT(ai2, err, "ai2_comm select() returned %d", error_num);
	}
}

int ai2_comm_open(void)
{
	struct termios tio;

	if (ai2_comm_hdl != -1)
		return ai2_ok;

	if ((ai2_comm_hdl = open(AI2_COMM_DEV_NAME, O_RDWR)) < 0) {
		LOGPRINT(ai2,err,"ai2_comm_open Err %d", errno);
		ai2_comm_hdl = -1;
		return ai2_comm_error;
	}

	UNUSED(tio);
	return ai2_ok;
}

int ai2_comm_tx(unsigned const char *buf, unsigned int len)
{
	int sent_bytes = 0;

	while (1) {
		sent_bytes = write(ai2_comm_hdl, buf, len);
		if ((unsigned int)sent_bytes == len) {
			break;
		}
		else if (sent_bytes < 0) {
			return ai2_comm_error;
		}
		else {
			buf += sent_bytes;
			len -= sent_bytes;
		}
	}

	return ai2_ok;
}

int ai2_comm_rx(unsigned char *buf, unsigned int *len)
{
	int recd_bytes = 0;
	int error = 0;
	struct timeval timeout = {1, 0}; 
	fd_set rdset;

	FD_ZERO(&rdset);
	FD_SET(ai2_comm_hdl, &rdset);

	//if (select(FD_SETSIZE, &rdset, NULL, NULL, &timeout) >=  0) {
	if (select(FD_SETSIZE, &rdset, NULL, NULL, NULL) >=  0) {
		
		ioctl(ai2_comm_hdl, FIONREAD, &recd_bytes);
		if ((recd_bytes = read(ai2_comm_hdl, buf, recd_bytes)) < 0)
			return ai2_comm_error;
		
		*len = recd_bytes;
		return ai2_ok;
	}
	else {
		error = errno;
		ai2_comm_print_err(error);
		return ai2_comm_error;
	}

}

int ai2_comm_close(void)
{
	if (ai2_comm_hdl >= 0){

		if(close(ai2_comm_hdl)!= 0)
		{
			LOGPRINT(ai2,info,"ai2_comm_close : Error %d", errno);				
		}
	}

	ai2_comm_hdl = -1;
	return ai2_ok;
}


int wl8_device_reset(void)
{
	LOGPRINT(ai2, info, "wl8_device_reset : Reset to the device sent");
	return ai2_ok;
}
