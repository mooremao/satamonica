#include "nvs_utils.h"

struct MDS1Y_offset secs2MDS1Y(unsigned int secs, unsigned char leap_yr)
{
	unsigned char i = 0, *Mn_Dn_map;
	struct MDS1Y_offset mds1y;
	unsigned short days;

	days = secs / MAX_SECS_IN_A_DAY;
	secs = secs - days * MAX_SECS_IN_A_DAY;

	Mn_Dn_map = leap_yr ? Mn_Dn_map_leap_yr : Mn_Dn_map_no_leap;

	for(i = 0; i < 12; i++) {
		if(days < Mn_Dn_map[i])
			break;

		days -= Mn_Dn_map[i];
	}

	mds1y.Mn =  i;
	mds1y.Dn = days;
	mds1y.ss = secs;

	return mds1y;
}

unsigned char days2months_1Y(unsigned short days, unsigned char leap_yr)
{
	unsigned char i = 0, *Mn_Dn_map;

	Mn_Dn_map = leap_yr ? Mn_Dn_map_leap_yr : Mn_Dn_map_no_leap;

	for(i = 0; i < 12; i++) {
		if(days < Mn_Dn_map[i])
			break;

		days -= Mn_Dn_map[i];
	}

	return i + 1;
}

unsigned short months2days_1Y(unsigned char months, unsigned char leap_yr)
{
	unsigned char i = 0, *Mn_Dn_map;
	unsigned short days = 0;

	Mn_Dn_map = leap_yr ? Mn_Dn_map_leap_yr : Mn_Dn_map_no_leap;

	for(i = 0; i < months; i++) {
		days += Mn_Dn_map[i];
	}

	return days; 
}

struct YMDS_offset secs2YMDS(unsigned int secs)
{
	unsigned char  Y4, Yr;
	struct YMDS_offset ymds;
	struct MDS1Y_offset mds1y;

	Y4   = secs / MAX_SECS_4Y_BLOCK;

	secs = secs - Y4 * MAX_SECS_4Y_BLOCK;
	Yr   = secs / MAX_SECS_IN_365DY;

	secs = secs - Yr * MAX_SECS_IN_365DY;

	mds1y = secs2MDS1Y(secs, Y4 ? !Yr : 0); 

	ymds.Yn = Y4 * 4 + Yr;
	ymds.Mn = mds1y.Mn;
	ymds.Dn = mds1y.Dn;
	ymds.ss = mds1y.ss;

	return ymds;
}

unsigned int YMDS2secs(struct YMDS_offset ymds)
{
	unsigned char  Y4, Yr;
	unsigned short Dn;
	unsigned int secs = 0;

	Y4 = ymds.Yn / 4;
	Yr = ymds.Yn - Y4 * 4;
	Dn = ymds.Dn + months2days_1Y(ymds.Mn, Y4 ? !Yr : 0); 

	secs += Y4 * MAX_SECS_4Y_BLOCK + Yr * MAX_SECS_IN_365DY;
	secs += Dn * MAX_SECS_IN_A_DAY;
	secs += ymds.ss;

	return secs;
}

struct week_offset secs2week(unsigned int secs)
{
	struct week_offset wkss;

	wkss.Wn = secs / MAX_SECS_IN_1WEEK;
	wkss.ss = secs - (wkss.Wn * MAX_SECS_IN_1WEEK);

	return wkss;
}
