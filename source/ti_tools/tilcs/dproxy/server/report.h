/*
 * report.h
 *
 *   This file contains all the low level function that can be used to inject
 *   data to the GNSS device.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#ifndef _DPROXY_REPORT_H_
#define _DPROXY_REPORT_H_


struct dev_gps_time;
void get_clk_rpt(struct dev_gps_time *dev_clk);

/* Default fillers. */
struct d2h_ai2;
struct no_object {
};
int no_function(struct d2h_ai2 *d2h_ai2);

#define PPS_OFF			0
#define PPS_ON			1
#define PPS_THRES_UNC	2


#define PPS_ON_STRING	"#PPS2,2\r\n"
#define PPS_OFF_STRING	"#PPS0,0\r\n"
#define PPS_THRES_UNC_STRING	"#PPS1,1\r\n"
#endif
