/*
 * eph_report.c
 *
 * Ephemeris report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "nvs.h"
#include "log_report.h"
#include "config.h"

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int eph_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_eph *dev_eph, unsigned int count);

int eph_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_eph *dev_eph, unsigned int count);

int eph_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_eph *dev_eph, unsigned int count);

int eph_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_eph *dev_eph, unsigned int count);

static struct ie_desc *mk_ie_adu_4eph_gps_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_gps_eph *dev_eph, 
											  unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_gps_eph) * count;
	struct dev_gps_eph *new_eph = (struct dev_gps_eph *)malloc(st_sz);
	if (!new_eph)
		goto exit1;

	memcpy(new_eph, dev_eph, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GPS_EPH, oper_id, st_sz, new_eph);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_eph);

exit1:
	return NULL;
}

static void setup_3gpp_gps_eph(struct gps_eph *tgt, struct dev_gps_eph *src, 
							  unsigned int count)
{
	while (count--) {
		tgt->svid            = src->prn - 1;
		tgt->is_bcast        = 1;
		//tgt->status          = src->;
		tgt->Code_on_L2      = src->Code_on_L2;
		tgt->Ura             = src->Accuracy;

		if (src->Accuracy & 0x80)
			tgt->FitIntervalFlag = 1;

		tgt->Health          = src->Health;
		tgt->IODC            = src->IODC;
		tgt->Tgd             = src->Tgd;
		tgt->Toc             = src->Toc;
		tgt->Af2             = src->Af2;
		tgt->Af1             = src->Af1;
		tgt->Af0             = src->Af0;
		tgt->Crs             = src->Crs;
		tgt->DeltaN          = src->DeltaN;
		tgt->Mo              = src->Mo;
		tgt->Cuc             = src->Cuc;
		tgt->E               = src->E;
		tgt->Cus             = src->Cus;
		tgt->SqrtA           = src->SqrtA;
		tgt->Toe             = src->Toe;
		tgt->Cic             = src->Cic;
		tgt->Omega0          = src->Omega0;
		tgt->Cis             = src->Cis;
		tgt->Io              = src->Io;
		tgt->Crc             = src->Crc;
		tgt->Omega           = src->Omega;
		tgt->OmegaDot        = src->OmegaDot;
		tgt->Idot            = src->Idot;
		LOGPRINT(eph_rpt, info, "setup_3gpp_gps_eph: svid [%d], Toe [%d]", tgt->svid, tgt->Toe);

		tgt++;
		src++;
	}
}

static struct ie_desc *mk_ie_adu_4eph_gps_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_gps_eph *dev_eph, 
											   unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(gps_eph) * count;
	struct gps_eph *new_eph = (struct gps_eph *)malloc(st_sz);
	if (!new_eph)
		goto exit1;

	setup_3gpp_gps_eph(new_eph, dev_eph, count);

	ie_adu = rpc_ie_attach(rmsg, REI_GPS_EPH, oper_id, st_sz, new_eph);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_eph);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4eph_glo_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_glo_eph *dev_eph, 
											  unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_glo_eph) * count;
	struct dev_glo_eph *new_eph = (struct dev_glo_eph *)malloc(st_sz);
	if (!new_eph)
		goto exit1;

	memcpy(new_eph, dev_eph, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GLO_EPH, oper_id, st_sz, new_eph);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_eph);

exit1:
	return NULL;
}

static  void setup_3gpp_glo_eph(struct glo_eph *tgt,struct dev_glo_eph *src, 
							  unsigned int count)
{
	while (count--) {

		tgt->the_svid = src->prn - AI2_GLO_BASE - 1;
		tgt->is_bcast = 1;
		tgt->m        = src->m;
		tgt->tk       = src->Tk;
		tgt->tb       = src->Tb;
		tgt->M        = src->M;
		tgt->Gamma    = src->Epsilon;
		tgt->Tau      = src->Tau;
		tgt->Xc       = src->Xc;
		tgt->Yc       = src->Yc;
		tgt->Zc       = src->Zc;
		tgt->Xv       = src->Xv;
		tgt->Yv       = src->Yv;
		tgt->Zv       = src->Zv;
		tgt->Xa       = src->Xa;
		tgt->Ya       = src->Ya;
		tgt->Za       = src->Za;
		tgt->P        = src->P;
		tgt->NT       = src->Nt;
		tgt->n        = src->n;
		tgt->FT       = src->Ft;
		tgt->En       = src->En;
		tgt->Bn       = src->B;
		tgt->P1       = src->P1;
		tgt->P2       = src->P2;
		tgt->P3       = src->P3;
		tgt->P4       = src->P4;
		tgt->Delta_Tau = src->DeltaTau;
		tgt->ln       = src->ln;

		LOGPRINT(eph_rpt, info, "setup_3gpp_glo_eph: the_svid [%d], tk [%d], NT [%d]", tgt->the_svid, tgt->tk, tgt->NT);
		tgt++;
		src++;
	}
}

static struct ie_desc *mk_ie_adu_4eph_glo_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_glo_eph *dev_eph, 
											   unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(glo_eph) * count;
	struct glo_eph *new_eph = (struct glo_eph *)malloc(st_sz);
	if (!new_eph)
		goto exit1;

	setup_3gpp_glo_eph(new_eph, dev_eph, count);
	

	ie_adu = rpc_ie_attach(rmsg, REI_GLO_EPH, oper_id, st_sz, new_eph);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_eph);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int eph_gps_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_gps_eph *dev_eph, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4eph_gps_dev(rmsg, oper_id, dev_eph, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int eph_gps_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_gps_eph *dev_eph, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4eph_gps_3gpp(rmsg, oper_id, dev_eph, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int eph_glo_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_glo_eph *dev_eph, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4eph_glo_dev(rmsg, oper_id, dev_eph, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int eph_glo_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_glo_eph *dev_eph, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4eph_glo_3gpp(rmsg, oper_id, dev_eph, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int eph_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_eph *dev_eph, unsigned int count)
{
	return eph_gps_create_dev_ie(rmsg, oper_id, dev_eph, count);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int eph_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_eph *dev_eph, unsigned int count)
{
	return eph_glo_create_dev_ie(rmsg, oper_id, dev_eph, count);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int eph_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_eph *dev_eph, unsigned int count)
{
	return eph_gps_create_3gpp_ie(rmsg, oper_id, dev_eph, count);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int eph_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_eph *dev_eph, unsigned int count)
{
	return eph_glo_create_3gpp_ie(rmsg, oper_id, dev_eph, count);
}

static int eph_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  unsigned int rei_select)
{
	struct dev_gps_eph *eph_gps = (struct dev_gps_eph *)ai2_d2h->ai2_struct;
	struct dev_glo_eph *eph_glo = BUF_OFFSET_ST(eph_gps, struct dev_gps_eph,
												struct dev_glo_eph);

	if ((eph_gps->prn >= 1) && (eph_gps->prn <= 32)) {
		if(rei_select & NEW_EPH_GPS_NATIVE) {
			if(-1 == eph_gps_create_dev_ie(rmsg, OPER_INF, eph_gps, 1))
				return -1;
		} else if(rei_select & NEW_EPH_GPS_IE_IND) {
			if(-1 == eph_gps_create_3gpp_ie(rmsg, OPER_INF, eph_gps, 1))
				return -1;
		}
	} else if ((eph_glo->prn >= 65) && (eph_glo->prn <= 88)) {
		if (rei_select & NEW_EPH_GLO_NATIVE) {
			if (-1 == eph_glo_create_dev_ie(rmsg, OPER_INF, eph_glo, 1))
				return -1;
		} else if (rei_select & NEW_EPH_GLO_IE_IND) {
			if (-1 == eph_glo_create_3gpp_ie(rmsg, OPER_INF, eph_glo, 1))
				return -1;
		}
	}

	return 0;
}

static int dev_gps_eph_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_eph *eph_gps = (struct dev_gps_eph *) ai2_d2h->ai2_struct;
	char  *ai2_pkt                = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref                = ai2_pkt;

	eph_gps->Code_on_L2  = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_gps->Accuracy    = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_gps->Health      = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_gps->Tgd         = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_gps->IODC        = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_gps->Toc         = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_gps->Af2         = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_gps->Af1         = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_gps->Af0         = BUF_LE_U3B_RD_INC(ai2_pkt);
	eph_gps->IODE        = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_gps->Crs         = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->DeltaN      = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->Mo          = BUF_LE_S4B_RD_INC(ai2_pkt);
	eph_gps->Cuc         = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->E           = BUF_LE_U4B_RD_INC(ai2_pkt);
	eph_gps->Cus         = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->SqrtA       = BUF_LE_U4B_RD_INC(ai2_pkt);
	eph_gps->Toe         = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_gps->Cic         = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->Omega0      = BUF_LE_S4B_RD_INC(ai2_pkt);
	eph_gps->Cis         = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->Io          = BUF_LE_S4B_RD_INC(ai2_pkt);
	eph_gps->Crc         = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->Omega       = BUF_LE_S4B_RD_INC(ai2_pkt);
	eph_gps->OmegaDot    = BUF_LE_S3B_RD_INC(ai2_pkt);
	eph_gps->Idot        = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_gps->Week        = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_gps->UpdateWeek  = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_gps->UpdateMS    = BUF_LE_U4B_RD_INC(ai2_pkt);
	eph_gps->is_bcast	 = 1;

	print_data(REI_DEV_GPS_EPH, eph_gps);

	return (ai2_pkt - ai2_ref);
}

static int dev_glo_eph_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_glo_eph *eph_glo = BUF_OFFSET_ST(ai2_d2h->ai2_struct,
												struct dev_gps_eph, 
												struct dev_glo_eph);
	char  *ai2_pkt         = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref        = ai2_pkt;

	eph_glo->m           = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->Tk          = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_glo->Tb          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->M           = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->Epsilon     = BUF_LE_S2B_RD_INC(ai2_pkt);
	eph_glo->Tau         = BUF_LE_S3B_RD_INC(ai2_pkt);
	eph_glo->Xc          = BUF_LE_S4B_RD_INC(ai2_pkt);
	eph_glo->Yc          = BUF_LE_S4B_RD_INC(ai2_pkt);
	eph_glo->Zc          = BUF_LE_S4B_RD_INC(ai2_pkt);
	eph_glo->Xv          = BUF_LE_S3B_RD_INC(ai2_pkt);
	eph_glo->Yv          = BUF_LE_S3B_RD_INC(ai2_pkt);
	eph_glo->Zv          = BUF_LE_S3B_RD_INC(ai2_pkt);
	eph_glo->Xa          = BUF_LE_S1B_RD_INC(ai2_pkt);
	eph_glo->Ya          = BUF_LE_S1B_RD_INC(ai2_pkt);
	eph_glo->Za          = BUF_LE_S1B_RD_INC(ai2_pkt);
	eph_glo->B           = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->P           = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->Nt          = BUF_LE_U2B_RD_INC(ai2_pkt);
	eph_glo->Ft          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->n           = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->DeltaTau    = BUF_LE_S1B_RD_INC(ai2_pkt);
	eph_glo->En          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->P1          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->P2          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->P3          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->P4          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->ln          = BUF_LE_U1B_RD_INC(ai2_pkt);
	eph_glo->is_bcast    = 1;

	print_data(REI_DEV_GLO_EPH, eph_glo);

	return (ai2_pkt - ai2_ref);
}

static int dev_eph_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_eph *eph_gps = (struct dev_gps_eph *)ai2_d2h->ai2_struct;
	struct dev_glo_eph *eph_glo = BUF_OFFSET_ST(eph_gps, struct dev_gps_eph,
												struct dev_glo_eph);
	char  *ai2_pkt        = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref        = ai2_pkt;
	int ai2_pkt_sz        = ai2_d2h->ai2_pkt_sz;

	eph_gps->prn = eph_glo->prn = BUF_LE_U1B_RD_INC(ai2_pkt);
	ai2_offset += 1;

	if ((eph_gps->prn >= 1) && (eph_gps->prn <= 32)
		&& (ai2_offset < ai2_pkt_sz)) {
		ai2_pkt += dev_gps_eph_struct(ai2_d2h, ai2_offset);
		eph_glo->prn = 255;
	} else if ((eph_glo->prn) >= 65 && (eph_glo->prn <= 88)
			   && (ai2_offset < ai2_pkt_sz)) {
		ai2_pkt += dev_glo_eph_struct(ai2_d2h, ai2_offset);
		eph_gps->prn = 255;
	} else
		LOGPRINT(eph_rpt, info, "No eph data for prn [%d]", eph_gps->prn);

	return (ai2_pkt - ai2_ref);
}

static int eph_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if (ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_eph_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int eph_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0;
	struct dev_gps_eph *eph_gps = (struct dev_gps_eph *)ai2_d2h->ai2_struct;
	struct dev_glo_eph *eph_glo =
		BUF_OFFSET_ST(eph_gps, struct dev_gps_eph, struct dev_glo_eph);

	LOGPRINT(eph_rpt, info, "storing eph to nvs");

	if ((eph_gps->prn >= 1) && (eph_gps->prn <= 32)) {

		wr_suc = nvs_rec_add(nvs_gps_eph, eph_gps, 1);
		if (wr_suc != nvs_success) {
			LOGPRINT(eph_rpt, err, "error storing ephemeris in nvs");
			return -1;
		}
	} else if ((eph_glo->prn >= 65) && (eph_glo->prn <= 88)) {

		wr_suc = nvs_rec_add(nvs_glo_eph, eph_glo, 1);
		if (wr_suc != nvs_success) {
			LOGPRINT(eph_rpt, err, "error storing ephemeris in nvs");
			return -1;
		}
	}


	return 0;
}

#define EXT_PVT_ST_LEN (ST_SIZE(dev_gps_eph) + ST_SIZE(dev_glo_eph))

struct d2h_ai2 d2h_eph_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.rei_select = (NEW_EPH_GPS_IE_IND | NEW_EPH_GLO_IE_IND),

	.gen_struct = eph_ext_struct,
	.do_ie_4rpc = eph_rpc_ie,
	.nvs_action = eph_nvs_info
};

