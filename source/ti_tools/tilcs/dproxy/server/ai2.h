/*
 * ai2.h
 *
 *  This file describes the interface for communication with GPS device.
 *  Interface takes AI2 data stream as defined by AI2 ICD. At any instance, only
 *  one operation of a particular type can happen.
 *
 *  The implementation of this interface will hide protocol intricacies like
 *  sync byte detection, wraping and unwrapping payload, CRC validation,
 *  acknowledgements, payload size management, timeouts, retransmission and
 *  sync byte escaping.

 *  Note: Any calls to open AI2 for second and subsequent usage of particular
 *  operation will be rejected with 'ai2_busy'.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _AI2_H_
#define _AI2_H_

/** @defgroup dev_proxy_ai2 AI2 Protocol Library
    Transmits and receives AI2 data stream.

    @{
*/

enum ai2_op {
        ai2_op_read,
        ai2_op_write
};


/** Positive results are context sensitive answers */
enum ai2_res {
        ai2_ok                  = 0,
        ai2_error               = -1,
        ai2_busy                = -2,
        ai2_read_buf_full       = -3,
        ai2_buf_size_err        = -4,
        ai2_incomplete_stream   = -5,
        ai2_chksm_err   	= -6,
        ai2_op_invalid          = -7,
        ai2_comm_error	        = -8 
 };

/** Configuration override */
struct ai2_cfg {
        unsigned int 	read_time_out;
        unsigned int    read_buf_size;

	int             tx_req_ack;
	unsigned int	tx_retry_count;
	unsigned int	tx_retry_delay;
	unsigned int    write_buf_size;
};

/** Set or get the configuration for AI2 module. To modify any default value,
    read the full configuration and change the corresponding parameter. The
    configuration cannot be set after ai2_open() is called.
    @param[in] op is the ai2 operation
    @param[in/out] config is the configuration
    @return 0  on success or errorcode
    @see enum ai2_res
*/
int ai2_config(struct ai2_cfg *config);

/** Open ai2 channel for reading or writing. Parallel reading and writing can
    happen and is guarented to be thread safe.
    @param[in] op is the ai2 operation
    @return handle on success or errorcode
    @see enum ai2_res
*/
int ai2_open(enum ai2_op op);

/** Write AI2 field(s) to transmission buffer. The writen data is accumulated in
    an internal buffer. The amount of data that can be written at one cannot
    exceed AI2_WRITE_BUF_SIZE. Once the size of the accumulated buffer reaches
    an threshold, which is a value less than AI2_WRITE_BUF_SIZE, transmission
    will happen. The caller can continue writing the subsequent data. In case,
    a data transfer is needed before the threshold is reached, the caller should
    call ai2_flush().
    @param[in] hdl is the ai2 handle from ai2_open()
    @param[in] buf is one or more AI2 fields
    @param[in] buflen is the length of data in the 'buf'
    @return 0  on success or errorcode
    @see enum ai2_res
*/
int ai2_write(const int hdl, const unsigned char *buf, unsigned int buflen);

/** Returns the current size of internal transmission buffer.
    @param[in] hdl is the ai2 handle from ai2_open()
    @return size on success or errorcode
    @see enum ai2_res
*/
int ai2_tell(const int hdl);

/** Prepares the final AI2 packet as per AI2 protocol specification and sends it
    to the device through the configured transport mechanism. After this call,
    the internal buffer is flushed and any futher call to ai2_write() will be
    used to prepare new AI2 packet. The call to this api will block and return
    only after transmission success is confirmed and optionally, ACK is received
    from the device.
    @param[in] hdl is the ai2 handle from ai2_open()
    @param[in] res_cb is the call back to notify success of this call
    @return size of the constructed buffer or errorcode
    @see enum ai2_res
*/
int ai2_flush(const int hdl);


/** Reset the context associated with the handle. After this call, the buffer
    is discarded and any futher AI2 calls starts anew.
    @param[in] hdl is the ai2 handle from ai2_open()
    @return 0 on success or errorcode
*/
int ai2_reset(const int hdl);

/** Read a complete AI2 packet from the device using configured transport
    mechanism. The call to this api will block and return only after
    reading a full AI2 packet from the device.
    @param[in] hdl is the ai2 codec handle from ai2_open()
    @param[in] buf is the AI2 stream from the device to be decoded
    @return remaining buffer length on success or errorcode
    @see enum ai2_res
*/
int ai2_read(const int hdl, unsigned char **buf);


/** Closes the channel assosicated with the handle.
    @param[in] hdl is the ai2 handle from ai2_open()
    @return 0  on success or errorcode
    @see enum ai2_res
*/
int ai2_close(const int hdl);




int ai2_almanac_flush(const int hdl, const unsigned char *buf, unsigned int buflen);


/*
 *  ============================================================================
 *  Macros for reading/writing signed/unsigned data of specified size and update
 *  buffer for next read
 *
 *  LE  --> Little Endian
 *  Sn  --> Signed n Bytes
 *  Un  --> Unsigned n Bytes
 *  BE  --> Big Endian
 *  RD  --> Read Data
 *  WR	--> Write Data
 *  INC --> Increment
 *
 *  ============================================================================
 */

#define BUF_LE_S4B_RD_INC(buf) (int)  ai2_get_nbytes((unsigned char **)&buf,  \
		sizeof(int))
#define BUF_LE_S2B_RD_INC(buf) (short) ai2_get_nbytes((unsigned char **)&buf, \
		sizeof(short))
#define BUF_LE_S1B_RD_INC(buf) (char) ai2_get_nbytes((unsigned char **)&buf,  \
		sizeof(char))
#define BUF_LE_U4B_RD_INC(buf) (unsigned int) ai2_get_nbytes(                 \
		(unsigned char **)&buf, sizeof(unsigned int))
#define BUF_LE_U2B_RD_INC(buf) (unsigned short)ai2_get_nbytes(                \
		(unsigned char **)&buf, sizeof(unsigned short))
#define BUF_LE_U1B_RD_INC(buf) (unsigned char) ai2_get_nbytes(                \
		(unsigned char **)&buf, sizeof(unsigned char))
#define BUF_LE_3B_RD_INC(buf) ai2_get_nbytes((unsigned char **)&buf, 3)
#define BUF_LE_U3B_RD_INC(buf) (unsigned int) ai2_get_nbytes((unsigned char **)&buf, 3)
#define BUF_LE_S3B_RD_INC(buf) (int) ai2_get_nbytes((unsigned char **)&buf, 3)

#define BUF_LE_S4B_WR_INC(buf, bytes) ai2_put_nbytes(bytes, buf, sizeof(int))
#define BUF_LE_S2B_WR_INC(buf, bytes) ai2_put_nbytes(bytes, buf, sizeof(short))
#define BUF_LE_S1B_WR_INC(buf, bytes) ai2_put_nbytes(bytes, buf, sizeof(char))
#define BUF_LE_U4B_WR_INC(buf, bytes) 				\
	ai2_put_nbytes(bytes, buf, sizeof(unsigned int))
#define BUF_LE_U2B_WR_INC(buf, bytes)				\
        ai2_put_nbytes(bytes, buf, sizeof(unsigned short))
#define BUF_LE_U1B_WR_INC(buf, bytes)				\
        ai2_put_nbytes(bytes, buf, sizeof(unsigned char))
#define BUF_LE_3B_WR_INC(buf, bytes) ai2_put_nbytes(bytes, buf, 3)

/** Returns n decoded bytes from buffer address and increments it as well.
    @param[in/out] buf Pointer to pointer to data.
    @param[in] num_bytes Number of bytes to return.
    @return n decoded bytes, can be typecasted to needed value.
*/
unsigned int ai2_get_nbytes(unsigned char **buf, unsigned char num_bytes);

/** Encodes n bytes to the buffer address and returns new pointer on the buffer.
    @param[in] bytes The data to be encoded.
    @param[in] buf Buffer to store the encoded data.
    @param[in] num_bytes Number of bytes to encode from 'bytes'.
    @return location further to buf at which further encoding can be done.
*/
unsigned char *ai2_put_nbytes(unsigned int bytes, unsigned char *buf,
						unsigned char num_bytes);


int d2h_ai2_encode(unsigned int *data, unsigned char *buf, char *sizes);

/** @} */

#endif
