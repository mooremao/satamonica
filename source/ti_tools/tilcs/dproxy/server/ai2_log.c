/*
 * ai2_log.c
 *
 * Dumps the data to and from GNSS device as seen by the serial interface.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ai2_log.h"
#include "utils.h"
#include "os_services.h"
#include "logger.h"
#include "config.h"

struct ai2_log_desc {

	FILE *d2h_bin_fd;
	FILE *h2d_bin_fd;
	FILE *bin_ts_fd;
	FILE *asc_log_fd;

	int flags;

	void *mutex_hnd;
};

static struct ai2_log_desc OBJ_PTR(log_desc);
static char ascii_buf[AI2_ASCII_BUF_LIMIT];

static int log_time(char *buf)
{
	int len = 0;
	struct timeval tv;
	struct tm *timeinfo = NULL;
	char local_buf[50];

	gettimeofday(&tv, NULL);

	timeinfo = localtime(&tv.tv_sec);

	if (timeinfo == NULL)
		return 0;
	
	memset(local_buf, 0 , sizeof(local_buf));
	strftime(local_buf , 50, "%Y-%m-%d-%H-%M-%S", timeinfo);
	len = snprintf(buf ,50,"%s-%ld", local_buf, (long int)tv.tv_usec);

	return len;
}

static unsigned char digit_ascii[] = {'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

static int form_ascii_lines(char *log_str, int log_max, void *ai2_buf,
							int ai2_len, int line_len, int hex_bytes)
{
	char *ai2_arr = (char*) ai2_buf;
	int  hex_len = (int) hex_bytes;
	int  col_len = line_len  , log_len = 0;

	hex_len += hex_len;   /* Display: 1 binary byte -> 2 ascii bytes */

	/* Estimate usable length of line or column length */
	col_len = col_len / (hex_len + 1);  /* Includes a byte for ' ' */
	col_len = col_len * (hex_len + 1);  /* Includes a byte for ' ' */

	while(log_max > (log_len + col_len)) {
		int num_gap = 0;              /* Num of ' ' in line */
		int log_ref = log_len;

		/* Prepare a log line */
		while(((log_len - log_ref) < col_len) && ai2_len) {

			log_str[log_len++] = digit_ascii[(*ai2_arr & 0xF0) >> 4];
			log_str[log_len++] = digit_ascii[(*ai2_arr & 0x0F)];
			ai2_arr++;
			ai2_len--;

			/* Introduce a gap, if required */
			if(!((log_len - log_ref - num_gap) & (hex_len - 1))) {
				log_str[log_len++] = ' ';
				num_gap++;
			}
		}

		/* Set up end of line */
		if((log_len + 2) < log_max) {
			log_str[log_len++] = '\r';
			log_str[log_len++] = '\n';
		}

		if(!ai2_len)
			break; /* Done, so quit */

		/* Prepare and loop for next line */
	}

	return log_len;
}

void ai2_log_flush(void)
{

	fflush(log_desc->h2d_bin_fd);
	fflush(log_desc->d2h_bin_fd);

}

int ai2_log_bin(enum ai2_dir dir, void *ai2_buf, unsigned int ai2_len)
{
	FILE * bin_fd;
	char ts_buf[50];   /* Time stamp ascii_buf */
	unsigned int ts_len = 0;       /* Time stamp length */
	int  ret_val = -1;
	unsigned int *logging_mask;

	os_mutex_take(log_desc->mutex_hnd);

	if(log_desc->flags & AI2_LOG_MODE_TIMESTAMP) {
		ts_len += log_time(ts_buf + 0);
		ts_len += sprintf(ts_buf + ts_len, " %s\n",
						  dir == ai2_h2d?  "H2D" : "D2H");

		if (log_desc->bin_ts_fd == NULL)
			goto log_bin_exit1;

	if (cfg_success == sys_param_get(cfg_logging_mask,(void**) &logging_mask)){
		update_logmask(*logging_mask);		
	}
	if(*logging_mask & AI2_LOG_ENABLE){
		if(fwrite(ts_buf, sizeof(char),ts_len, log_desc->bin_ts_fd) != ts_len){
			fflush(log_desc->bin_ts_fd);
			goto log_bin_exit1;
		}
	  }
	}
	bin_fd = (dir == ai2_h2d) ? log_desc->h2d_bin_fd : log_desc->d2h_bin_fd;

	if (bin_fd == NULL)
		goto log_bin_exit1;

	if(*logging_mask & AI2_LOG_ENABLE){
		if(fwrite(ai2_buf, sizeof(char),ai2_len, bin_fd) !=  ai2_len){
			fflush(log_desc->bin_ts_fd);
			goto log_bin_exit1;
		}

	}

	ret_val = ai2_len;

log_bin_exit1:
	os_mutex_give(log_desc->mutex_hnd);

	return ret_val;
}

int ai2_log_pkt_ascii(enum ai2_dir dir, char *pkt_name, void *ai2_pkt,
					  int pkt_len)
{
	int ret_val = -1;
	unsigned int ascii_len = 0;
	unsigned int ascii_max = sizeof(ascii_buf);
	unsigned int *logging_mask;

	if(log_desc->flags & AI2_LOG_MODE_ASCII) {
		os_mutex_take(log_desc->mutex_hnd);

		ascii_len += log_time(ascii_buf + 0);
		ascii_len += sprintf(ascii_buf + ascii_len," %s --> pkt: %s\n",
							 dir == ai2_h2d?  "H2D" : "D2H", pkt_name);

		ascii_len += form_ascii_lines(ascii_buf + ascii_len,
									  ascii_max - ascii_len,
									  ai2_pkt, pkt_len, 80, 2);

		if (log_desc->asc_log_fd == NULL)
			goto log_pkt_exit1;

		if (cfg_success == sys_param_get(cfg_logging_mask,(void**) &logging_mask)) {
			update_logmask(*logging_mask);
		}
		if(*logging_mask & AI2_LOG_ENABLE){
		if(fwrite(ascii_buf, 
				 ascii_len, sizeof(char),log_desc->asc_log_fd) != ascii_len)
			goto log_pkt_exit1;
		}

		ret_val = ascii_len;

log_pkt_exit1:
		os_mutex_give(log_desc->mutex_hnd);
	}
	return ret_val;
}

void ai2_log_exit(void)
{
	os_mutex_take(log_desc->mutex_hnd);

	fclose(log_desc->d2h_bin_fd);
	log_desc->d2h_bin_fd = NULL;

	fclose(log_desc->h2d_bin_fd);
	log_desc->h2d_bin_fd = NULL;

	if(log_desc->flags & AI2_LOG_MODE_ASCII) {
		fclose(log_desc->asc_log_fd);
		log_desc->asc_log_fd = NULL;
	}

	if(log_desc->flags & AI2_LOG_MODE_TIMESTAMP) {
		fclose(log_desc->bin_ts_fd);
		log_desc->bin_ts_fd = NULL;
	}

	os_mutex_give(log_desc->mutex_hnd);

	os_mutex_exit(log_desc->mutex_hnd);
	log_desc->mutex_hnd = -1;
}

int ai2_log_init(struct ai2_log_cfg *cfg)
{
	char time_buf[50];

	log_time(time_buf);
	snprintf(ascii_buf, 2048, "%s%s-%s.bin", cfg->log_name, "ToSensor" , time_buf);

	log_desc->h2d_bin_fd = fopen(ascii_buf,"wb+");
	if(log_desc->h2d_bin_fd == NULL)
		return -1;

	snprintf(ascii_buf, 2048, "%s%s-%s.bin", cfg->log_name, "FromSensor" , time_buf);
	log_desc->d2h_bin_fd = fopen(ascii_buf, "wb+");
	if(log_desc->d2h_bin_fd == NULL)
		return -1;

	log_desc->flags = cfg->modes;

	if(log_desc->flags & AI2_LOG_MODE_TIMESTAMP) {
		snprintf(ascii_buf, 2048, "%s%s-%s", cfg->log_name, 
				 "TIMESTAMP", time_buf);
		log_desc->bin_ts_fd = fopen(ascii_buf, "w");
		if(log_desc->bin_ts_fd == NULL)
			return -1;
	}

	if(log_desc->flags & AI2_LOG_MODE_ASCII) {
		snprintf(ascii_buf, 2048, "%s-%s-%s", cfg->log_name, "ASCII",
				 time_buf);
		log_desc->asc_log_fd = fopen(ascii_buf, "w");
		if(log_desc->asc_log_fd == NULL)
			return -1;
	}

	if (!log_desc->mutex_hnd) {
		if (!(log_desc->mutex_hnd = os_mutex_init())) {
			return -1;
		}
	}

	return 0;
}

void ai2_log_reset(void)
{
	os_mutex_take(log_desc->mutex_hnd);

	fclose(log_desc->d2h_bin_fd);
	log_desc->d2h_bin_fd = NULL;

	fclose(log_desc->h2d_bin_fd);
	log_desc->h2d_bin_fd = NULL;

	if(log_desc->flags & AI2_LOG_MODE_ASCII) {
		fclose(log_desc->asc_log_fd);
		log_desc->asc_log_fd = NULL;
	}
	if(log_desc->flags & AI2_LOG_MODE_TIMESTAMP) {
		fclose(log_desc->bin_ts_fd);
		log_desc->bin_ts_fd = NULL;
	}

	os_mutex_give(log_desc->mutex_hnd);
}

