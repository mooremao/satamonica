/*
 * heading_filter.c
 *
 * Position report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#define QNOISE_FACT1 (1.0/16.0)
#define QNOISE_FACT2 (1.0)
#define PI (3.14159265359)


struct HdgFiltStateVar{

	double StateNoise;   //Kalman state variable - process noise
	double StateNew;     //Kalman state variable
	double StatePrev;    //previous filtere output
	unsigned char  FilterStart;  //Flag to indicate if teh filter is already active
	unsigned int TCount;		//FCount corresponding to the last filtered heading
};

struct VelocityVar {
	float VelEast;
	float VelNorth;
	float Heading;
	unsigned int TCount;
};

int init_Heading_Filter (void *data);

void HdgFiltInit(void);

void HdgFilter(struct VelocityVar* CurrentVelocityVar, 
				struct VelocityVar* FilteredVelocityVar);

