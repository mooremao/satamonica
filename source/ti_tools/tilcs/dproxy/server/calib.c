/*
 * calib.c
 *
 * Calibeation of TCXO related injection functions.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#include "dev_proxy.h"
#include "inject.h"
#include "ai2.h"
#include "logger.h"
#include "config.h"
#include "sequencer.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "nvs.h"
#include "nvs_validate.h"

extern int h2d_ai2_hdl;

int h2d_inj_osc_freq(unsigned int clk_freq,unsigned short clk_quality){
	struct ai2_osc_freq {
		unsigned int id;
		unsigned int len;
		unsigned int ctrl;
		unsigned int osc_qualty;
		unsigned int osc_freq;		
	} osc_freq[1];
	char ai2_osc_freq_sizes[] = {1, 2, 1, 2, 4, 0};
	unsigned char mem[10];

	osc_freq->id = 0x25;
	osc_freq->len = 7;
	osc_freq->ctrl = 0;
	osc_freq->osc_qualty = clk_quality;
	osc_freq->osc_freq = clk_freq;	

	d2h_ai2_encode((void *)osc_freq, mem, ai2_osc_freq_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 10)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(generic, info, "osc freq injected- %d,%d,%d",
					osc_freq->ctrl,	osc_freq->osc_freq, osc_freq->osc_qualty);
			return OK;
		}
	}

	LOGPRINT(generic, err, "error sending ai2 packet to device");

	return ERR;

}

int dpxy_inj_osc_freq(void *data ){

	struct dev_clk_calibrate *calib = (struct dev_clk_calibrate *)data;

	UNUSED(data);
	
	if(calib->flags == DEV_CALIB_AUTO ){		

		unsigned int* ref_clk_freq;
		unsigned short* ref_clk_freq_unc;
		
		sys_param_get(cfg_refclk_freq,(void**) &ref_clk_freq);

		sys_param_get(cfg_gps_frequn,(void**) &ref_clk_freq_unc);
		
		return(h2d_inj_osc_freq(*ref_clk_freq,*ref_clk_freq_unc));
	}
	else
		return(h2d_inj_osc_freq(calib->ref_clk_freq,calib->ref_clk_quality));
	
}


int inj_calib_ctrl(unsigned char flag){
	struct ai2_calib_ctrl {
		unsigned int id;
		unsigned int len;
		unsigned int calb_ctrl;				
	} calib_ctrl[1];
	char ai2_calib_ctrl_sizes[] = {1, 2, 1, 0};
	unsigned char mem[4];

	calib_ctrl->id = 0xED;
	calib_ctrl->len = 1;
	
	calib_ctrl->calb_ctrl = flag;

	d2h_ai2_encode((void *)calib_ctrl, mem, ai2_calib_ctrl_sizes);

	if (ai2_ok == ai2_write(h2d_ai2_hdl, mem, 4)) {
		if (ai2_ok == ai2_flush(h2d_ai2_hdl)) {
			LOGPRINT(generic, info, "calib ctrl injected");
			return OK;
		}
	}

	LOGPRINT(generic, err, "error sending ai2 packet to device");
	return ERR;

}

int dpxy_inj_calib_ctrl_periodic(void *data){

        int iRet = 0;
        
        iRet = inj_calib_ctrl(DEV_CALIB_ENABLE_REF_CLCK_PERIODIC);

        return iRet;
}

int dpxy_inj_calib_ctrl_single_shot(void *data){
	
	int iRet = 0;

	iRet = inj_calib_ctrl(DEV_CALIB_ENABLE_REF_CLCK_SINGLE_SHOT);

	return iRet;
}

int dpxy_get_calib_resp(void *data){
	
	struct dev_calib_resp calib[1];
	char  *ai2_pkt               = (char*) data;
	ai2_pkt += 4;

	calib->calib_resp = BUF_LE_U1B_RD_INC(ai2_pkt);

	if(calib->calib_resp == 0){
		LOGPRINT(generic, info, "%d, calib did not complete", calib->calib_resp);
	}
	else if (calib->calib_resp == 1){
		LOGPRINT(generic, info, "%d, calib complete", calib->calib_resp);
	}
	else{
		LOGPRINT(generic, info, "%d, unknown calib response", calib->calib_resp);		
	}
	
	return calib->calib_resp;
}

/* Read the GPS Freq Unc from NVS File & Inject the Freq  Estimate */
int dpxy_undo_freq_est(void *data)
{
#define LIGHT_SEC          (299792458.0f)

	struct dev_gps_tcxo tcxo[1];
	struct dev_gps_tcxo tcxo1;
	unsigned int *longterm_freq_unc, *shortterm_freq_unc;
	
	sys_param_get(cfg_tcxo_long_term_freq_unc,(void**) &longterm_freq_unc);
	sys_param_get(cfg_tcxo_short_term_freq_unc,(void**) &shortterm_freq_unc);
	
	LOGPRINT(generic, info, "injecting long term unc");
	tcxo[0].freq_bias = 0;
	tcxo[0].freq_unc = ((*longterm_freq_unc) * LIGHT_SEC * 1e-6);
	
	return (h2d_inj_dev_freq_est(&tcxo[0],1));
}


