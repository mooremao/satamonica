/*
 * sequencer.c
 *
 * Sequencer functionality for device dialogues
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <string.h>
#include <pthread.h>
#include "sequencer.h"
#include "ai2.h"
#include "inject.h"
#include "os_services.h"
#include "logger.h"
#include "config.h"
#include "calib.h"
#include "glo_sv2slot_map.h"
#include "cust_config.h"
#include "apm.h"
#include "heading_filter.h"

/* This value should be more than the longest number of steps in any of the
 * following sequences
 */
static struct seq_step seq_stack[SEQ_STACK_SIZE];
static int seq_ptr = -1;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_condattr_t attr;
pthread_mutex_t pthr_mutex = PTHREAD_MUTEX_INITIALIZER;

struct evt_sync {
	void *seq_mutex;
	void *evt_mutex;
	void *evt_sem;
	void *data;

	int id;
};
static struct evt_sync evt_sync;

/* AI2 write handle. */
extern int h2d_ai2_hdl;

int h2d_first_clk_rpt(void *data);
int h2d_inj_gps_tim_req(void *data);
int sv_glo_report(void *data);
int dpxy_send_host_ver(void *data);
int dpxy_fs_flush(void *data);
int h2d_inj_first_clk_rpt(void *data);
int h2d_inj_del_gps_time(void *data);

/*
 * Code Group 1: Sequences and functions. Please add new sequences below. The
 * function signature and implementation will be in *inject*.? files.
 */
/* Ready. */
#define READY_SEQ	\
{	\
	{0x00F5, send, dpxy_inj_comm_prot},	\
	{0x00F1, recv, dpxy_get_gps_stat},	\
	{0x00F1, send, dpxy_inj_ptch_dwld},	\
	{0x00F2, recv, dpxy_get_dwld_stat},	\
	{0x00F0, recv, dpxy_get_gps_ver},	\
	{0xD1FB, send, dpxy_send_host_ver},		\
	{0x00FB, send, dpxy_inj_rtc_inj_ctrl},	\
	{0x09FB, send, dpxy_inj_lna_ctrl},	\
	{0x0006, send, dpxy_inj_init_evt_cfg},	\
	{0x0006, send, dpxy_inj_nvs_aiding},	\
	{0x0039, send, dpxy_inj_pinmux_cfg},  \
	{0x00EE, send, dpxy_inj_pps_out_ctrl},   /*Both of the pps out to be enabled and tested */ \
	{0x0002, send, dpxy_inj_dev_idle},	\
	{-1, nop, 0}				\
}

/* Wakeup. */
#define WAKEUP_SEQ	\
{	\
	{0x00FB, send, dpxy_inj_dev_wakeup},	\
	{0x0D80, recv, dpxy_get_evt_cfg},	\
	{0x0002, send, dpxy_inj_dev_idle},	\
	{0x0780, recv, dpxy_get_evt_cfg},	\
	{-1, nop, 0}				\
}

/* Sleep. */
#define SLEEP_SEQ	\
{	\
	{0x0002, send, dpxy_inj_dev_off},	\
	{0x0180, recv, dpxy_get_evt_cfg},	\
	{-1, nop, 0}				\
}

/* Active. */
#define ACTIVE_SEQ	\
{	\
	{0x0008, send, dpxy_inj_recv_cfg},	\
	{0x0000, send, init_Heading_Filter},	\
	{0x0CFB, send, h2d_inj_apm_ctrl_from_config},	\
	{0x0020, send, dpxy_inj_freq_est},	\
	{0x0002, send, dpxy_inj_dev_on},	\
	{0x0080, recv, dpxy_get_evt_cfg},	\
	{0x0009, send, h2d_inj_gps_tim_req},	\
	{0x0005, recv, h2d_first_clk_rpt},	\
	{0xFB22, send, dpxy_inj_def_qop},	\
	{-1, nop, 0}				\
}

/* Deact. */
#define DEACT_SEQ	\
{	\
	{0x0CFB, send, h2d_inj_apm_disable}, 	\   
	{0x2BFB, send, h2d_inj_req_glo_sv2slot_map}, \
	{0x11F7, recv, sv_glo_report},    \
	{0x0002, send, dpxy_inj_dev_idle},	\
	{0x0780, recv, dpxy_get_evt_cfg},	\
	{0x0000, send, dpxy_fs_flush},	\
	{-1, nop, 0}				\
}


/* Deact. */
#define CLEANUP_SEQ	\
{	\
	{0x00FB, send, dpxy_inj_gps_shutdwn_ctrl}, \
	{-1, nop, 0}				\
}


/* Reports. */
#define REPORTS_SEQ	\
{	\
	{0x0006, send, dpxy_inj_evt_cfg},	\
	{0x00E5, send, dpxy_inj_nmea_out_ctrl},	\
	{-1, nop, 0}				\
}

/* Inject time. */
#define INJ_TIME_SEQ	\
{	\
	{0x000A, send, h2d_inj_gps_tim},	\
	{0x0009, send, h2d_inj_gps_tim_req},	\
	{0x0005, recv, h2d_first_clk_rpt},	\
	{-1, nop, 0}				\
}

#define SINGLESHOT_CALIB_SEQ	\
{	\
	{0x0025, send, dpxy_inj_osc_freq},	\
	{0x00ED, send, dpxy_inj_calib_ctrl_single_shot},\
	{0x02F7, recv, dpxy_get_calib_resp},	\
	{-1, nop, 0}				\
}

#define PERIODIC_CALIB_SEQ	\
{	\
	{0x0025, send, dpxy_inj_osc_freq},	\
	{0x00ED, send, dpxy_inj_calib_ctrl_periodic},\
	{-1, nop, 0}				\
}

#define UNDO_CALIB_SEQ	\
{	\
	{0x0025, send, dpxy_undo_freq_est},	\
	{-1, nop, 0}				\
}

#define INCREASE_CLK_UNC                        \
{                                               \
	{0x0009, send, h2d_inj_gps_tim_req},	\
	{0x0005, recv, h2d_first_clk_rpt},	\
	{0x0002, send, h2d_inj_del_gps_time},	\
	{0x000A, send, h2d_inj_first_clk_rpt},	\
	{-1, nop, 0}                            \
}

/* add for GNSS recovery ++*/	
#define ERR_READY_SEQ	\
{	\
	{0x0009, send, h2d_dproxy_sleep},	\
	{0x00F5, send, dpxy_inj_comm_prot},	\
	{0x0009, send, h2d_dproxy_sleep},	\	
	{0x00FB, send, dpxy_inj_rtc_inj_ctrl},	\
	{0x09FB, send, dpxy_inj_lna_ctrl},	\
	{0x0006, send, dpxy_inj_init_evt_cfg},	\
	{0x0006, send, dpxy_inj_nvs_aiding},	\
	{0x0039, send, dpxy_inj_pinmux_cfg},  \
	{0x00EE, send, dpxy_inj_pps_out_ctrl},   /*Both of the pps out to be enabled and tested */ \
	{0x0002, send, dpxy_inj_dev_idle},	\
	{0x0780, recv, dpxy_get_evt_cfg}, \
	{-1, nop, 0}				\	
}
/* error trigger test code, to be removed */
#define FATAL_ERR_SEQ	\
{	\
	{0x00FB, send, dpxy_inj_fatal_pkt1},	\
	{0x00FB, send, dpxy_inj_fatal_pkt2},	\	
	{-1, nop, 0}				\
}								
/* error trigger test code, to be removed */
#define EXCEPTION_ERR_SEQ	\
{	\
	{0x00FB, send, dpxy_inj_exception_pkt},	\
	{-1, nop, 0}				\
}
#define MEAS_REP_RECOVERY_SEQ \
{ 	\
	{0x00F0, send, dpxy_get_gps_ver},	\
	{0x00F0, recv, dpxy_get_gps_stat},	\
	{0x0002, send, dpxy_inj_dev_reset},	\
	{0xD1FB, send, dpxy_send_host_ver},		\
	{0x00FB, send, dpxy_inj_rtc_inj_ctrl},	\
	{0x09FB, send, dpxy_inj_lna_ctrl},	\
	{0x0006, send, dpxy_inj_init_evt_cfg},	\
	{0x0009, send, h2d_dproxy_sleep},	\
	{0x0039, send, dpxy_inj_pinmux_cfg},  \
	{0x00EE, send, dpxy_inj_pps_out_ctrl},   \
	{0x0002, send, dpxy_inj_dev_idle},	\
	{-1, nop, 0}				\
}

#define NO_RESP_RECOVERY_SEQ \
{ \
	{0x00F5, send, dpxy_inj_comm_prot},	\
	{0x00F1, recv, dpxy_get_gps_stat},	\
	{-1, nop, 0}				\
}

#define HARD_RESET_RECOVERY_SEQ \
{ \
	{0x0009,send, h2d_dproxy_hard_reset }, \
	{0x0009, send, h2d_dproxy_sleep},	\
	{-1, nop, 0} \
}
/*
 * Code Group 2: Sequencer engine.
 */
static int seq_load(enum seq_id seq_id)
{
	struct seq_step *dest = &seq_stack[SEQ_PUSH_WINDOW];
	switch(seq_id) {
		case ready: {
		        struct seq_step src[] = READY_SEQ;
		        memcpy(dest, src, sizeof(src));
                        LOGPRINT(seq, info, "ready sequence loaded");
			break;
		}

		case wakeup: {
		        struct seq_step src[] = WAKEUP_SEQ;
		        memcpy(dest, src, sizeof(src));
                        LOGPRINT(seq, info, "wakeup sequence loaded");
			break;
                }
		case dsleep: {
		        struct seq_step src[] = SLEEP_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "sleep sequence loaded");
			break;
                }
		case active: {
		        struct seq_step src[] = ACTIVE_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "active sequence loaded");
			break;
                }
		case deact: {
		        struct seq_step src[] = DEACT_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "deact sequence loaded");
			break;
                }
		case reports: {
		        struct seq_step src[] = REPORTS_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "reports sequence loaded");
			break;
                }
		case inj_time: {
		        struct seq_step src[] = INJ_TIME_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "inj time sequence loaded");
			break;
                }
		case calib_single_shot: {
		        struct seq_step src[] = SINGLESHOT_CALIB_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "singleshot calib sequence loaded");
			break;
				}
		case calib_periodic: {
		        struct seq_step src[] = PERIODIC_CALIB_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "periodic calib sequence loaded");
			break;
                }
		case calib_undo: {
		        struct seq_step src[] = UNDO_CALIB_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "undo calib sequence loaded");
			break;
                }
		/* add for GNSS recovery ++*/	
		case protocol_select: {
				struct seq_step src[] = ERR_READY_SEQ;
				memcpy(dest, src, sizeof(src));
				LOGPRINT(seq, info, "err_seq: error recovery sequence loaded");
			break;
				}

		/* error trigger test code, to be removed */				
		case fatal_err_select: {
				struct seq_step src[] = FATAL_ERR_SEQ;
				memcpy(dest, src, sizeof(src));
				LOGPRINT(seq, info, "err_seq: fatal error sequence loaded");
			break;
				}
				
		/* error trigger test code, to be removed */				
		case exception_err_select: {
				struct seq_step src[] = EXCEPTION_ERR_SEQ;
				memcpy(dest, src, sizeof(src));
				LOGPRINT(seq, info, "err_seq: exception sequence loaded");
			break;
				}
		/* add for GNSS recovery --*/	
		case meas_rep_recovery: {
				struct seq_step src[] = MEAS_REP_RECOVERY_SEQ;
				memcpy(dest, src, sizeof(src));
				LOGPRINT(seq, info, "err_seq: measurement report recovery sequence  loaded");
			break;
				}
		case no_resp_recovery: {
				struct seq_step src[] = NO_RESP_RECOVERY_SEQ;
				memcpy(dest, src, sizeof(src));
				LOGPRINT(seq, info, "err_seq: no response recovery sequence loaded");
			break;
				}
		case hard_reset_recovery:{
				struct seq_step src[] = HARD_RESET_RECOVERY_SEQ;
				memcpy(dest, src, sizeof(src));
				LOGPRINT(seq, info, "err_seq: HARD RESET recovery sequence loaded");
			break;
			}
		case inc_clk_unc: {
		        struct seq_step src[] = INCREASE_CLK_UNC;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "increase clk unc sequence loaded");
			break;
                }
		case cleanup : {
				struct seq_step src[] = CLEANUP_SEQ;
		        memcpy(dest, src, sizeof(src));
		        LOGPRINT(seq, info, "Cleanup sequence loaded");
			break;
			}
		
                case null:
                default:
                        return SEQ_ERR;
	};

	seq_ptr = SEQ_PUSH_WINDOW;
	return SEQ_OK;
}

int seq_reset()
{
	seq_ptr = -1;
        LOGPRINT(seq, info, "sequencer reset");
	return SEQ_OK;
}

int seq_pop_step(struct seq_step *step)
{
	/* Just pop. */
	if (!step) {
		seq_ptr++;
		return SEQ_OK;
	}

	/* Pop and get. */
	if (-1 == seq_stack[seq_ptr].id)
		return SEQ_ERR;

	*step = seq_stack[seq_ptr++];
	return SEQ_OK;
}

int seq_peek_step(struct seq_step *step)
{
	if (!step)
		return SEQ_ERR;

	/* Pop and get. */
	if (-1 == seq_stack[seq_ptr].id)
		return SEQ_ERR;

	*step = seq_stack[seq_ptr];
	return SEQ_OK;
}

int seq_push_step(struct seq_step *step)
{
	if (!step || seq_ptr <= 0)
		return SEQ_ERR;

	seq_stack[--seq_ptr] = *step;
	LOGPRINT(seq, info, "new sequence pushed with id %X", step->id);
	return SEQ_OK;
}

void remove_event(int id)
{
	os_mutex_take(evt_sync.evt_mutex);

	evt_sync.id = -1;
	evt_sync.data = NULL;

	os_mutex_give(evt_sync.evt_mutex);
	#ifndef ANDROID
		/*Signal the event*/
		pthread_cond_signal( &cond );
	#endif
}
int setup_wait_for_event(int id)
{
	os_mutex_take(evt_sync.evt_mutex);

	/* Safely set the event id. */
	evt_sync.id = id;

	os_mutex_give(evt_sync.evt_mutex);

	return SEQ_OK;
}

int seq_wait_for_event(int id, void **data)
{
	int ret;
	struct timespec to;
	int retval;	
	clock_gettime(CLOCK_MONOTONIC, &to);
		to.tv_sec += 5;
	
	os_mutex_take(evt_sync.evt_mutex);

	/* Safely set the event id. */
	evt_sync.id = id;

	os_mutex_give(evt_sync.evt_mutex);

	if ((retval = pthread_mutex_lock(&pthr_mutex))) {
				LOGPRINT(seq, err,"pthread_mutex_lock %s\n", 
					strerror(retval));
	
			return SEQ_ERR;
		}
	
	if ((retval = pthread_cond_timedwait(&cond, &pthr_mutex, &to)))
	   {
		   LOGPRINT(seq, err, "pthread_cond_timedwait %s\n",
				   strerror(retval));

			/*Release the lock and exit*/	
			if ((retval = pthread_mutex_unlock(&pthr_mutex))) {
				   LOGPRINT(seq, critl, "pthread_mutex_unlock %s\n", 
							strerror(retval));
			
					return SEQ_ERR;
				}
		   return SEQ_ERR;
	   }

	
	if ((retval = pthread_mutex_unlock(&pthr_mutex))) {
		   LOGPRINT(seq, err, "pthread_mutex_unlock %s\n", 
					strerror(retval));
	
			return SEQ_ERR;
		}
	os_mutex_take(evt_sync.evt_mutex);
	*data = evt_sync.data;
	evt_sync.id = -1;
	os_mutex_give(evt_sync.evt_mutex);

	LOGPRINT(seq, info, "received event %X", id);

	return SEQ_OK;
}

int seq_wait_for_timed_event(int id, void **data)
{
	int ret;
	os_mutex_take(evt_sync.evt_mutex);

	/* Safely set the event id. */
	evt_sync.id = id;

	os_mutex_give(evt_sync.evt_mutex);


	ret = os_sem_timed_take(evt_sync.evt_sem, 6);
	LOGPRINT(seq, info, "os_sem_timed_take return	%X", ret );
	if(ret == -1){
#if 0
		os_mutex_take(evt_sync.evt_mutex);
			evt_sync.id = -1;
			evt_sync.data = NULL;
		os_mutex_give(evt_sync.evt_mutex);
#endif
		return SEQ_ERR;
	}

	os_mutex_take(evt_sync.evt_mutex);
	*data = evt_sync.data;
	evt_sync.id = -1;
	os_mutex_give(evt_sync.evt_mutex);

	LOGPRINT(seq, info, "received event %X", id);

	return SEQ_OK;
}

int seq_post_event(void *data)
{
	unsigned char *ai2_pkt = (unsigned char *) data;
	int ret = 0;
	int id = d2h_ai2_key_id(ai2_pkt[0], ai2_pkt[3]);

	os_mutex_take(evt_sync.evt_mutex);

	/* Check for a match. */
	ret = (evt_sync.id == id || id == 0xF5 /*Invalid packet*/);
	evt_sync.data = data;
	os_mutex_give(evt_sync.evt_mutex);

	/* Signal, if match. */
	if (ret) {
		LOGPRINT(seq, info, "signal H2D thrd waiting on evnt %X", id);
		
	#ifndef ANDROID	
		pthread_cond_signal( &cond );
	#else
		os_sem_give(evt_sync.evt_sem);
	#endif
	
		if(id == 0xF5)
			return SEQ_ERR;
		else	
		return SEQ_OK;
	}

	return SEQ_ERR;
}

int seq_execute(enum seq_id seq_id, void *to_data)
{
	struct seq_step step;
	struct seq_step step_nxt;
	int ret = SEQ_OK;
	int ret_val = SEQ_OK;
	void *data;

	//os_mutex_take(evt_sync.seq_mutex);
	if (SEQ_OK != seq_load(seq_id)) {
		//os_mutex_give(evt_sync.seq_mutex);
		return SEQ_ERR;
	}

	LOGPRINT(seq, info, "executing sequence");

	while(SEQ_ERR != seq_pop_step(&step)) {
		if (send == step.action) {

			if (SEQ_ERR != seq_peek_step(&step_nxt)) {

				if (recv == step_nxt.action)
					setup_wait_for_event(step_nxt.id);
			}

			if (step.trigger){
				ret = step.trigger(to_data);
				if (ret != SEQ_OK) {
					//LOGPRINT(seq, err, "seq_execute failed(%d) at id(%d)", ret, step.id);
					printf("seq_execute failed(%d) at id(%d) step trigger", ret, step.id);
					remove_event(step_nxt.id);
					//return SEQ_ERR;
					ret = SEQ_ERR;
					break;
				}
			}
		}
		else if (recv == step.action) {
			#ifndef ANDROID
				ret_val = seq_wait_for_event(step.id, &data);
			#else
				ret_val = seq_wait_for_timed_event(step.id, &data);
			#endif
			if(ret_val != SEQ_OK){
					LOGPRINT(seq, err, "seq_wait_for_event Error");
				ret = SEQ_ERR;
				break;
			}
			if (step.trigger){
				ret = step.trigger(data);
				if (ret != SEQ_OK) {
					//LOGPRINT(seq, err, "seq_execute failed(%d) at id(%d)", ret, step.id);
					printf("seq_execute failed(%d) at id(%d)", ret, step.id);
					remove_event(step_nxt.id);
						//return SEQ_ERR;
						ret = SEQ_ERR;
						break;
				}
			}
		}
	}
	

	LOGPRINT(seq, info, " Out of Sequence load %d, ret =%d", seq_id, ret);
	return ret;
}

int seq_init(void)
{
        LOGPRINT(seq, info, "initializing sequencer");
	evt_sync.evt_mutex = os_mutex_init();
	evt_sync.evt_sem = os_sem_init();
	evt_sync.id = -1;
	pthread_condattr_init( &attr);
	#ifndef ANDROID
		pthread_condattr_setclock( &attr, CLOCK_MONOTONIC);
	#endif
	pthread_cond_init( &cond, &attr);

	evt_sync.seq_mutex = os_mutex_init();
	return SEQ_OK;
}

int seq_deinit(void)
{
        LOGPRINT(seq, info, "deinitializing sequencer");
	os_sem_exit(evt_sync.evt_sem);
	os_mutex_exit(evt_sync.evt_mutex);

	os_mutex_exit(evt_sync.seq_mutex);
	return SEQ_OK;
}
