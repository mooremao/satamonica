
	/*
	 * glo_sv_slot_map_report.h
	 *
	 * GLO SV to SLOT Mapping report
	 *
	 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
	 * ALL RIGHTS RESERVED
	 *
	*/
	#include "dev2host.h"

	
	struct dev_sv_slot_info {
		unsigned char svid;
		unsigned char slot;
		unsigned char status;
	};
	
	int sv_slot_nvs_info(struct d2h_ai2 *ai2_d2h);
	


