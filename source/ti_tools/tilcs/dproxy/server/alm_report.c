/*
 * alm_report.c
 *
 * Alamanac report parser
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdlib.h>
#include <string.h>

#include "ai2.h"
#include "rpc.h"
#include "utils.h"
#include "device.h"
#include "dev2host.h"
#include "logger.h"
#include "dev_proxy.h"
#include "nvs.h"
#include "log_report.h"

/* : Mode to a header file, as required */
/* Functions that are offering services outside. */
int alm_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_alm *dev_alm, unsigned int count);

int alm_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_alm *dev_alm, unsigned int count);

int alm_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_alm *dev_alm, unsigned int count);

int alm_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_alm *dev_alm, unsigned int count);

int alm_qzs_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_alm *dev_alm, unsigned int count);

static struct ie_desc *mk_ie_adu_4alm_qzs_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_gps_alm *dev_alm, 
											  unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_gps_alm) * count;
	struct dev_gps_alm *new_alm = (struct dev_gps_alm *)malloc(st_sz);
	if (!new_alm)
		goto exit1;

	memcpy(new_alm, dev_alm, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_QZS_ALM, oper_id, st_sz, new_alm);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_alm);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4alm_gps_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_gps_alm *dev_alm, 
											  unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_gps_alm) * count;
	struct dev_gps_alm *new_alm = (struct dev_gps_alm *)malloc(st_sz);
	if (!new_alm)
		goto exit1;

	memcpy(new_alm, dev_alm, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GPS_ALM, oper_id, st_sz, new_alm);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_alm);

exit1:
	return NULL;
}

static void  setup_3gpp_gps_alm(struct gps_alm *tgt, struct dev_gps_alm *src, 
							  unsigned int count)
{
	while (count--) {
		tgt->svid      = src->prn - 1;
		//tgt->dataId    = src->;
		tgt->E         = src->E;
		tgt->Toa       = src->Toa;
		tgt->DeltaI    = src->DeltaI;
		tgt->OmegaDot  = src->OmegaDot;
		tgt->Health    = src->Health;
		tgt->SqrtA     = src->SqrtA;
		tgt->OmegaZero = src->OmegaZero;
		tgt->Omega     = src->Omega;
		tgt->Mzero     = src->MZero;
		tgt->Af0       = src->Af0;
		tgt->Af1       = src->Af1;
		tgt->wna       = src->Week;
		LOGPRINT(alm_rpt, info, "setup_3gpp_gps_alm: svid [%d], wna [%d], Toa [%d]", tgt->svid, tgt->wna, tgt->Toa);
		tgt++;
		src++;
	}
}

static struct ie_desc *mk_ie_adu_4alm_gps_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_gps_alm *dev_alm, 
											   unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(gps_alm) * count;
	struct gps_alm *new_alm = (struct gps_alm *)malloc(st_sz);
	if (!new_alm)
		goto exit1;

    setup_3gpp_gps_alm(new_alm, dev_alm, count);

	ie_adu = rpc_ie_attach(rmsg, REI_GPS_ALM, oper_id, st_sz, new_alm);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_alm);

exit1:
	return NULL;
}

static struct ie_desc *mk_ie_adu_4alm_glo_dev(struct rpc_msg *rmsg,
											  unsigned char oper_id, 
											  struct dev_glo_alm *dev_alm, 
											  unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(dev_glo_alm) * count;

	struct dev_glo_alm *new_alm = (struct dev_glo_alm *)malloc(st_sz);
	if (!new_alm)
		goto exit1;

	memcpy(new_alm, dev_alm, st_sz);

	ie_adu = rpc_ie_attach(rmsg, REI_DEV_GLO_ALM, oper_id, st_sz, new_alm);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_alm);

exit1:
	return NULL;
}

static void setup_3gpp_glo_alm(struct glo_alm *tgt,
							  struct dev_glo_alm *src, 
							  unsigned int count)
{
	while (count--) {

		tgt->TauC           = src->TauC;
		tgt->N4             = src->N4;
		tgt->TauGPS         = src->TauGPS;
		tgt->NA             = src->NA;
		tgt->nA             = src->nA;
		tgt->HnA            = src->HnA;
		tgt->Lambda_nA      = src->Lambda_nA;
		tgt->t_Lambda_nA    = src->t_Lambda_nA;
		tgt->Delta_i_nA     = src->Delta_i_nAcorrection;
		tgt->Delta_T_nA     = src->Delta_T_nArate;
		tgt->Delta_T_dot_nA = src->Delta_T_nArate_Dot;
		tgt->Epsilon_nA     = src->Varepsilon_nA;
		tgt->Omega_nA       = src->Omega_nA;
		tgt->M_nA           = src->M_nA;
		tgt->B1             = src->B1;
		tgt->B2             = src->B2;
		tgt->KP             = src->Kp;
		tgt->Tau_nA         = src->Tau_nA;
		tgt->C_nA           = src->C_nA;
		LOGPRINT(alm_rpt, info, "setup_3gpp_glo_alm: nA [%d], N4 [%d], NA [%d]", tgt->nA, tgt->N4, tgt->NA);
		tgt++;
		src++;
	}
}

static struct ie_desc *mk_ie_adu_4alm_glo_3gpp(struct rpc_msg *rmsg,
											   unsigned char oper_id, 
											   struct dev_glo_alm *dev_alm, 
											   unsigned int count)
{
	struct ie_desc *ie_adu;
	int st_sz = ST_SIZE(glo_alm) * count;
	struct glo_alm *new_alm = (struct glo_alm *)malloc(st_sz);
	if (!new_alm)
		goto exit1;

	setup_3gpp_glo_alm(new_alm, dev_alm, count);	

	ie_adu = rpc_ie_attach(rmsg, REI_GLO_ALM, oper_id, st_sz, new_alm);
	if (!ie_adu)
		goto exit2;

	return ie_adu;

exit2:
	free(new_alm);

exit1:
	return NULL;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int alm_qzs_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_gps_alm *dev_alm, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4alm_qzs_dev(rmsg, oper_id, dev_alm, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

static int alm_gps_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_gps_alm *dev_alm, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4alm_gps_dev(rmsg, oper_id, dev_alm, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int alm_gps_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_gps_alm *dev_alm, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4alm_gps_3gpp(rmsg, oper_id, dev_alm, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int alm_glo_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								 struct dev_glo_alm *dev_alm, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4alm_glo_dev(rmsg, oper_id, dev_alm, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/* Returns number of IE_ADU added to rpc_msg or -1 in case of error. */
static int alm_glo_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
								  struct dev_glo_alm *dev_alm, unsigned int count)
{
	struct ie_desc *ie_adu[2];
	int n_adu = 0;

	ie_adu[n_adu] = mk_ie_adu_4alm_glo_3gpp(rmsg, oper_id, dev_alm, count);
	if (!ie_adu[n_adu++])
		goto exit1;

	return n_adu;

exit1:
	while (--n_adu) {
		rpc_ie_freeup(rmsg, ie_adu[n_adu - 1]);
	}

	return -1;
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int alm_gps_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_alm *dev_alm, unsigned int count)
{
	return alm_gps_create_dev_ie(rmsg, oper_id, dev_alm, count);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int alm_glo_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_glo_alm *dev_alm, unsigned int count)
{
	return alm_glo_create_dev_ie(rmsg, oper_id, dev_alm, count);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int alm_gps_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_gps_alm *dev_alm, unsigned int count)
{
	return alm_gps_create_3gpp_ie(rmsg, oper_id, dev_alm, count);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int alm_glo_rpt_create_3gpp_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							   struct dev_glo_alm *dev_alm, unsigned int count)
{
	return alm_glo_create_3gpp_ie(rmsg, oper_id, dev_alm, count);
}

/*
 * Returns number of IE_ADU added to rpc_msg or -1 in case of error.
 * Public function to be called by other module, typically, H2D.
 */
int alm_qzs_rpt_create_dev_ie(struct rpc_msg *rmsg, unsigned char oper_id,
							  struct dev_gps_alm *dev_alm, unsigned int count)
{
	return alm_qzs_create_dev_ie(rmsg, oper_id, dev_alm, count);
}

static int alm_rpc_ie(struct d2h_ai2 *ai2_d2h, struct rpc_msg *rmsg,
					  unsigned int rei_select)
{
	struct dev_gps_alm *alm_gps = (struct dev_gps_alm *)ai2_d2h->ai2_struct;
	struct dev_glo_alm *alm_glo =
		BUF_OFFSET_ST(alm_gps, struct dev_gps_alm, struct dev_glo_alm);

	if ((alm_gps->prn >= 1) && (alm_gps->prn <= 32)) {
		if(rei_select & NEW_ALM_GPS_NATIVE) {
			if(-1 == alm_gps_create_dev_ie(rmsg, OPER_INF, alm_gps, 1))
				return -1;
		} else if(rei_select & NEW_ALM_GPS_IE_IND) {
			if(-1 == alm_gps_create_3gpp_ie(rmsg, OPER_INF, alm_gps, 1))
				return -1;
		}
	} else if ((alm_glo->prn >= 65) && (alm_glo->prn <= 88)) {
		if (rei_select & NEW_ALM_GLO_NATIVE) {
			if (-1 == alm_glo_create_dev_ie(rmsg, OPER_INF, alm_glo, 1))
				return -1;
		} else if (rei_select & NEW_ALM_GLO_IE_IND) {
			if (-1 == alm_glo_create_3gpp_ie(rmsg, OPER_INF, alm_glo, 1))
				return -1;
		}
	} else if ((alm_gps->prn >= 193) && (alm_gps->prn <= 202)) {
		if (-1 == alm_qzs_create_dev_ie(rmsg, OPER_INF, alm_gps, 1))
			return -1;
	}

	return 0;
}

static int dev_gps_alm_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_alm *alm_gps = (struct dev_gps_alm *)ai2_d2h->ai2_struct;
	char  *ai2_pkt		= (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref		= ai2_pkt;

	alm_gps->Health		= BUF_LE_U1B_RD_INC(ai2_pkt);
	alm_gps->Toa		= BUF_LE_U1B_RD_INC(ai2_pkt);
	alm_gps->E		= BUF_LE_U2B_RD_INC(ai2_pkt);
	alm_gps->DeltaI		= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_gps->OmegaDot	= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_gps->SqrtA		= BUF_LE_U3B_RD_INC(ai2_pkt);
	alm_gps->OmegaZero	= BUF_LE_S3B_RD_INC(ai2_pkt);
	alm_gps->Omega		= BUF_LE_S3B_RD_INC(ai2_pkt);
	alm_gps->MZero		= BUF_LE_S3B_RD_INC(ai2_pkt);
	alm_gps->Af0		= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_gps->Af1		= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_gps->Week		= BUF_LE_U2B_RD_INC(ai2_pkt);
	alm_gps->UpdateWeek	= BUF_LE_U2B_RD_INC(ai2_pkt);
	alm_gps->UpdateMS	= BUF_LE_U4B_RD_INC(ai2_pkt);

	/* Dump the decoded data to logger. */
	print_data(REI_DEV_GPS_ALM, alm_gps);

	return (ai2_pkt - ai2_ref);
}

static int dev_glo_alm_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_glo_alm *alm_glo =
		BUF_OFFSET_ST(ai2_d2h->ai2_struct, struct dev_gps_alm,
					  struct dev_glo_alm);
	char  *ai2_pkt		= (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref		= ai2_pkt;

	alm_glo->TauC 		= BUF_LE_S4B_RD_INC(ai2_pkt);
	alm_glo->TauGPS	 	= BUF_LE_S3B_RD_INC(ai2_pkt);
	alm_glo->N4 		= BUF_LE_U1B_RD_INC(ai2_pkt);
	alm_glo->NA 		= BUF_LE_U2B_RD_INC(ai2_pkt);
	alm_glo->nA 		= BUF_LE_U2B_RD_INC(ai2_pkt);
	alm_glo->HnA 		= BUF_LE_U1B_RD_INC(ai2_pkt);
	alm_glo->Lambda_nA 	= BUF_LE_S3B_RD_INC(ai2_pkt);
	alm_glo->t_Lambda_nA 	= BUF_LE_U3B_RD_INC(ai2_pkt);
	alm_glo->Delta_i_nAcorrection = BUF_LE_S3B_RD_INC(ai2_pkt);
	alm_glo->Delta_T_nArate = BUF_LE_S3B_RD_INC(ai2_pkt);
	//: Check with FW team
	//alm_glo->Delta_T_nArate_Dot = BUF_LE_S1B_RD_INC(ai2_pkt);
	alm_glo->Varepsilon_nA 	= BUF_LE_U2B_RD_INC(ai2_pkt);
	alm_glo->Omega_nA 	= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_glo->M_nA 		= BUF_LE_U1B_RD_INC(ai2_pkt);
	alm_glo->B1 		= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_glo->B2 		= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_glo->Kp 		= BUF_LE_U1B_RD_INC(ai2_pkt);
	alm_glo->Tau_nA 	= BUF_LE_S2B_RD_INC(ai2_pkt);
	alm_glo->C_nA 		= BUF_LE_U1B_RD_INC(ai2_pkt);

	/* Dump the decoded data to logger. */
	print_data(REI_DEV_GLO_ALM, alm_glo);

	return (ai2_pkt - ai2_ref);
}

static int dev_alm_struct(struct d2h_ai2 *ai2_d2h, int ai2_offset)
{
	struct dev_gps_alm *alm_gps = (struct dev_gps_alm *)ai2_d2h->ai2_struct;
	struct dev_glo_alm *alm_glo =
		BUF_OFFSET_ST(alm_gps, struct dev_gps_alm,
					  struct dev_glo_alm);
	char *ai2_pkt  = (char*) ai2_d2h->ai2_packet + ai2_offset;
	char  *ai2_ref = ai2_pkt;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	alm_gps->prn = alm_glo->prn = BUF_LE_U1B_RD_INC(ai2_pkt);
	ai2_offset += 1;

	if ((alm_gps->prn >= 1) && (alm_gps->prn <= 32)
		&& (ai2_offset < ai2_pkt_sz)) {
		ai2_pkt += dev_gps_alm_struct(ai2_d2h, ai2_offset);
		alm_glo->prn = 255;
	} else if ((alm_gps->prn >= 65) && (alm_gps->prn <= 88)
			   && (ai2_offset < ai2_pkt_sz)) {
		ai2_pkt += dev_glo_alm_struct(ai2_d2h, ai2_offset);
		alm_gps->prn = 255;
	} else if ((alm_gps->prn >= 193) && (alm_gps->prn <= 202)
			   && (ai2_offset < ai2_pkt_sz)) {
		ai2_pkt += dev_gps_alm_struct(ai2_d2h, ai2_offset);
		alm_glo->prn = 255;
	} else
		LOGPRINT(alm_rpt, info, "no alm data for prn [%d]", alm_gps->prn);

	return (ai2_pkt - ai2_ref);
}

static int alm_ext_struct(struct d2h_ai2 *ai2_d2h)
{
	int ai2_offset = 3;
	int ai2_pkt_sz = ai2_d2h->ai2_pkt_sz;

	memset(ai2_d2h->ai2_struct, 0, ai2_d2h->ai2_st_len);

	if (ai2_offset < ai2_pkt_sz)
		ai2_offset += dev_alm_struct(ai2_d2h, ai2_offset);

	return (ai2_offset <= ai2_pkt_sz) ? 0 : -1;
}

static int alm_nvs_info(struct d2h_ai2 *ai2_d2h)
{
	int wr_suc = 0;
	struct dev_gps_alm *alm_gps = (struct dev_gps_alm *)ai2_d2h->ai2_struct;
	struct dev_glo_alm *alm_glo =
		BUF_OFFSET_ST(alm_gps, struct dev_gps_alm, struct dev_glo_alm);

	LOGPRINT(alm_rpt, info, "storing almanac in nvs");

	/* Based on the range store the almanac to nvs. */
	if ((alm_gps->prn >= 1) && (alm_gps->prn <= 32)) {

		wr_suc = nvs_rec_add(nvs_gps_alm, alm_gps, 1);
		if (wr_suc != nvs_success) {
			LOGPRINT(alm_rpt, err, "error storing almanac in nvs");
			return -1;
		}
	} else if ((alm_glo->prn >= 65) && (alm_glo->prn <= 88)) {

		wr_suc = nvs_rec_add(nvs_glo_alm, alm_glo, 1);
		if (wr_suc != nvs_success) {
			LOGPRINT(alm_rpt, err, "error storing almanac in nvs");
			return -1;
		}
	}

	return 0;
}

#define EXT_PVT_ST_LEN (ST_SIZE(dev_gps_alm) + ST_SIZE(dev_glo_alm))

struct d2h_ai2 d2h_alm_rpt = {

	.ai2_st_len = EXT_PVT_ST_LEN,

	.rei_select = (NEW_ALM_GPS_IE_IND | NEW_ALM_GLO_IE_IND),

	.gen_struct = alm_ext_struct,
	.do_ie_4rpc = alm_rpc_ie,
	.nvs_action = alm_nvs_info
};
