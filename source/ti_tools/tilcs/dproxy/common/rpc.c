/*
 * rpc.c
 *
 * RPC procedures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/uio.h>

#include "rpc.h"
#include "os_services.h"
#include "logger.h"
#include "utils.h"
#include "device.h"


int wq_ie_add_to(unsigned short elem_id, unsigned char oper_id, 
				 unsigned int xact_id);
int msg_send(struct rpc_msg *rmsg);

enum rpc_dest {

	rpc_to_app,
	rpc_to_svr
};

/* Move this to utils file */
static inline int util_iovec_set(struct iovec *vec, void *base, int len)
{
	vec->iov_base = base;
	vec->iov_len  = len;

	return len;
}

static void msg_free(struct rpc_msg *rmsg)
{
	struct rm_head *rm_hdr = RM_HEADER(rmsg);
	int n_ie = 0;

	for(n_ie = 0; n_ie < MAX_IE; n_ie++) {
		struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
		struct ie_head *ie_hdr = IE_HEADER(ie_adu);
		ie_hdr->tot_len = 0;
		ie_hdr->elem_id = REI_NOTDEF;
		if(ie_adu->data) {
			free(ie_adu->data);
			ie_adu->data = NULL;
		}
	}

	free(rmsg);
}

void rpc_msg_free(struct rpc_msg *rmsg)
{
	msg_free(rmsg);
}

static struct rpc_msg *msg_alloc(void)
{
	struct rpc_msg *rmsg = (struct rpc_msg *)malloc(sizeof(struct rpc_msg));
	struct rm_head *rm_hdr;
	int n_ie = 0;

	if(!rmsg)
		goto rmalloc_exit1;

	rm_hdr         = RM_HEADER(rmsg);
	rm_hdr->length = 0;
	rm_hdr->num_ie = 0;

	for(n_ie = 0; n_ie < MAX_IE; n_ie++) {
		struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
		struct ie_head *ie_hdr = IE_HEADER(ie_adu);

		ie_hdr->elem_id = REI_NOTDEF;
		ie_hdr->tot_len = 0;

		ie_adu->data    = NULL;
		ie_adu->objs    = 0;
	}

rmalloc_exit1:

	return rmsg;
}

struct rpc_msg *rpc_msg_alloc(void)
{
	return msg_alloc();
}

/* Forward declarations */
struct rm_xact;
struct ie_xact;

enum rpc_node {

	server,
	client
};

struct rpc_desc {

	char             name[24];      /* Optional           */
	enum rpc_node    c_or_s;        /* Client or Server   */

	struct rm_xact  *wq_rmsg;       /* Wait queue, client */
	struct ie_xact  *wq_elem;       /* Wait queue, server */

	int              fd;            /* FIFO file desc     */

	unsigned int     seq_num;       /* Last xaction ID    */

	void            *wr_mutex_hnd;
	void            *list_mutex_hnd;
};

static struct rpc_desc rpc_desc[1];

/*
   Bit 31-30 --> Transaction type (b00:ACK, b01:CMD/IND, b10:REQ/RSP, b11 ERR)
   Bit    29 --> Client or Server (b1: Client, b0: Server)
   Bit    28 --> Reserved Bit
   Bit 24-27 --> Operation ID
   Bit 20-23 --> Reserved Bit
   Bit    19 --> Sequence owner   (b1: Client, b0: Server)
   Bit  0-18 --> Sequence number  (0x00000 - 0x7FFFF)

 */

#define CTW_ASSEMBLE(xact_ty, c_or_s, oper_id, xact_id)                 \
	((xact_ty << 30) | (c_or_s << 29) | (oper_id << 24) | (xact_id))

/* Assemble a control word for a given transaction type and transaction ID */
static void adu_ct_word_make(struct ie_head *ie_hdr, enum rpc_xact xtype,
							 unsigned int xact_id)
{
	unsigned char oper_id = CTW_2_OPER_ID(ie_hdr->ct_word);
#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "adu_ct_word_make: using old xact id [0x%x]", xact_id);
#endif
	ie_hdr->ct_word = CTW_ASSEMBLE(xtype, rpc_desc->c_or_s, oper_id, xact_id);
}

/* 20 bit transaction ID --> 19: sequence owner (so), 0-18: sequence number */
#define RPC_NEXT_XACT_ID(so, seq_num) ((so << 19) | (++seq_num & 0x7ffff))

/* Generate a control word with next transaction ID */
static void adu_ct_word_next(struct ie_head *ie_hdr, enum rpc_xact xtype)
{
	unsigned int  xact_id = RPC_NEXT_XACT_ID(rpc_desc->c_or_s, rpc_desc->seq_num);
	unsigned char oper_id = CTW_2_OPER_ID(ie_hdr->ct_word);

#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "adu_ct_word_next: using new xact id [0x%x]", xact_id);
#endif
	ie_hdr->ct_word = CTW_ASSEMBLE(xtype, rpc_desc->c_or_s, oper_id, xact_id);
}

/* Need a revisit */
enum rpc_result {

	false,
	true = 3
};

/* Association of RPC message and transaction, typically used by client */
struct rm_xact {

	struct rpc_msg   *rmsg;      /* RPC Message                     */
	unsigned int      nr_x;      /* Count of transaction in message */
	struct rm_xact   *next;

	/* include semaphore */
	void *sem_hnd;
};

#define MAX_RM_XACT 3  /* Indicative of number of threads in client app */

static struct rm_xact *rm_xact_free_list = NULL;
static struct rm_xact rm_xact_array[MAX_RM_XACT];

static int rm_xact_init(void)
{
	struct rm_xact *xact = NULL;
	int i = 0;

	for(i = 0; i < MAX_RM_XACT; i++) {
		xact = rm_xact_array + i;

		/* Allocate a semaphore i.e. xact->sem = <OS specific > */
		xact->sem_hnd = os_sem_init();

		ADD_LIST_ATOMIC(rpc_desc->list_mutex_hnd, xact, rm_xact_free_list);
	}

	return 0;
}

static void rm_xact_exit(void)
{
	struct rm_xact *xact = NULL;
	int i = 0;

	for(i = 0; i < MAX_RM_XACT; i++) {
		xact = rm_xact_free_list;

		/* ASSERT(xact) */ /* This should not happen */

		/* Free the semaphores */
		os_sem_exit(xact->sem_hnd);

		rm_xact_free_list = xact->next;
	}
}

static struct rm_xact *rm_xact_alloc(void)
{
	struct rm_xact *xact = NULL;

	POP_LIST_ATOMIC(rpc_desc->list_mutex_hnd, rm_xact_free_list, xact);

	return xact;
}

static void rm_xact_free(struct rm_xact *xact)
{
	ADD_LIST(rpc_desc->list_mutex_hnd, xact, rm_xact_free_list);

	return;
}

/* Wait for transaction completion */
static struct rm_xact * wq_xact_cmpl_wait(struct rpc_msg *rmsg)
{
	struct rm_xact *xact   = rm_xact_alloc();
	struct rm_head *rm_hdr = RM_HEADER(rmsg);
	struct ie_desc *ie_adu = rmsg->info_elem;
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);

	if(!xact) {
		LOGPRINT(rpc, err, "failed to allocate semaphore");
		return NULL;
	}

	xact->rmsg    = rmsg;
	xact->nr_x    = rm_hdr->num_ie;

#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "adding rmsg with elem id [0x%x] and xact_id [0x%x] in client rmsg queue", 
		 ie_hdr->elem_id, CTW_2_XACT_ID(ie_hdr->ct_word));
#endif

	ADD_LIST_ATOMIC(rpc_desc->list_mutex_hnd, xact, rpc_desc->wq_rmsg);

	return xact;
}

static void _wq_xact_update(struct rm_xact *xact, struct ie_desc *ps_adu)
{
	struct rpc_msg *rmsg = xact->rmsg;
	struct rm_head *rm_hdr = RM_HEADER(rmsg);
	struct ie_head *ie_hdr = IE_HEADER(ps_adu);

	rm_hdr->num_ie++;
	rm_hdr->length += IE_HDR_SZ + ie_hdr->tot_len;

	if (xact->nr_x)
		--xact->nr_x;

	return;
}

static void wq_xact_update(struct rm_xact *xact, struct ie_desc *ps_adu)
{
	os_mutex_take(rpc_desc->list_mutex_hnd);
	_wq_xact_update(xact, ps_adu);
	os_mutex_give(rpc_desc->list_mutex_hnd);
}

static void _wq_xact_signal(struct rm_xact *xact)
{
	struct rm_xact *last = NULL, *this = rpc_desc->wq_rmsg;
	struct rpc_msg *rmsg = xact->rmsg;

	if(xact->nr_x)
		return;  /* Still more ADU awaited in queued RPC MSG */

	/* All ADU(s) received, wakeup waiting RPC MSG thread */
	while(this) {
		if(this == xact)
			goto wqx_found;

		last = this;
		this = xact->next;
	}

	return;

wqx_found:
	DEL_LIST(rpc_desc->list_mutex_hnd, last, this, rpc_desc->wq_rmsg);

	return;
}

static struct rm_xact* wq_xact_find_get(unsigned int xact_id, 
					struct ie_desc **ie_adu_o);

void wq_xact_signal(struct ie_head *ie_hdr)
{
	struct rm_xact *xact;
	struct ie_desc *ps_adu;

	xact = wq_xact_find_get(CTW_2_XACT_ID(ie_hdr->ct_word), &ps_adu);
	if(!xact)
		return;

	os_mutex_take(rpc_desc->list_mutex_hnd);

	_wq_xact_signal(xact);
	rm_xact_free(xact);

	os_sem_give(xact->sem_hnd); /* Change as per OS requirements */

#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "found rmsg with elem id [0x%x] and xact_id [0x%x] in client rmsg queue", 
		 ie_hdr->elem_id, CTW_2_XACT_ID(ie_hdr->ct_word));
#endif

	os_mutex_give(rpc_desc->list_mutex_hnd);

	return;
}

/* Client: finds a transaction in the wait-queue and returns reference to the
   waiting message. At the same time, provides access to related ADU in the
   wait queue.
 */
static struct rm_xact* _wq_xact_find_get(unsigned int xact_id,
										struct ie_desc **ie_adu_o)
{
	struct rm_xact *xact = rpc_desc->wq_rmsg;
	int n_ie = 0;

	while(xact) {
		struct rpc_msg *rmsg = xact->rmsg;
		struct rm_head *rm_hdr = RM_HEADER(rmsg);

		for(n_ie = 0; n_ie < rm_hdr->num_ie; n_ie++) {

			struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
			struct ie_head *ie_hdr = IE_HEADER(ie_adu);

			if(CTW_2_XACT_ID(ie_hdr->ct_word) == xact_id) {
				*ie_adu_o = ie_adu;
				goto wqx_fg_exit1;
			}
		}

		xact = xact->next;
	}

wqx_fg_exit1:
	return xact;
}

static struct rm_xact* wq_xact_find_get(unsigned int xact_id, 
					struct ie_desc **ie_adu_o)
{
	struct rm_xact *xact;
	os_mutex_take(rpc_desc->list_mutex_hnd);
	xact = _wq_xact_find_get(xact_id, ie_adu_o);
	os_mutex_give(rpc_desc->list_mutex_hnd);

	return xact;
}

#define MAX_IE_XACT 6  /* Indicative of number of threads in client app */

struct ie_xact {

	unsigned int   xact_id;  /* Transaction ID */
	unsigned short elem_id;  /* Element ID     */
	unsigned char  oper_id;  /* Operation ID   */
	struct ie_xact *next;

};

static struct ie_xact *ie_xact_free_list = NULL;
static struct ie_xact ie_xact_array[MAX_IE_XACT];

static int wq_ie_xact_init(void)
{
	int i = 0;
	struct ie_xact *xact;

	for(i = 0; i < MAX_IE_XACT; i++) {

		xact = ie_xact_array + i;
		ADD_LIST_ATOMIC(rpc_desc->list_mutex_hnd, xact, ie_xact_free_list);
	}

	return 1;
}

static void wq_ie_xact_exit(void)
{
	int i = 0;
	struct ie_xact *xact;

	for(i = 0; i < MAX_IE_XACT; i++) {
		xact = ie_xact_free_list;

		/* ASSERT(xact) */ /* This should not happen */

		ie_xact_free_list = xact->next;
	}
}

static void wq_ie_xact_free(struct ie_xact *xact)
{
	ADD_LIST_ATOMIC(rpc_desc->list_mutex_hnd, xact, ie_xact_free_list);
}

static struct ie_xact *wq_ie_xact_alloc(void)
{
	struct ie_xact *xact = NULL;

	POP_LIST_ATOMIC(rpc_desc->list_mutex_hnd, ie_xact_free_list, xact);

	return xact;
}

int wq_ie_add_to(unsigned short elem_id, unsigned char oper_id, 
				 unsigned int xact_id)
{
	struct ie_xact *xact = wq_ie_xact_alloc();

	if(!xact)
		return 0;

#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "Adding elem id [0x%x], oper_id [0x%x] and xact id [0x%x] in server ie queue", 
		 elem_id, oper_id, xact_id);
#endif

	xact->elem_id = elem_id;
	xact->xact_id = xact_id;
	xact->oper_id = oper_id;

	ADD_LIST_ATOMIC(rpc_desc->list_mutex_hnd, xact, rpc_desc->wq_elem);

	return 1;
}

/* Server: Find waiting elem, if any. Element specified by elem_id.
Return: 1 for a match or 0 for a no match
 */
static int _wq_ie_find_get(unsigned short elem_id, unsigned char oper_id, 
						  unsigned int *xact_id)
{
	struct ie_xact *xact = rpc_desc->wq_elem, *last = NULL;

	while(xact) {
		if((xact->elem_id == elem_id) && 
		   (xact->oper_id == oper_id))
			break;

		last = xact;
		xact = xact->next;
	}

	if(!xact)
		return 0;

	*xact_id = xact->xact_id;

#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "Found elem id [0x%x], oper_id [0x%x] and xact id [0x%x] in server ie queue", 
		 elem_id, oper_id, *xact_id);
#endif

	DEL_LIST(rpc_desc->list_mutex_hnd, last, xact, rpc_desc->wq_elem);
	ADD_LIST(rpc_desc->list_mutex_hnd, xact, ie_xact_free_list);

	return 1;
}

static int wq_ie_find_get(unsigned short elem_id, unsigned char oper_id, 
						  unsigned int *xact_id)
{
	int ret_val;

	os_mutex_take(rpc_desc->list_mutex_hnd);
	ret_val = _wq_ie_find_get(elem_id, oper_id, xact_id);
	os_mutex_give(rpc_desc->list_mutex_hnd);

	return ret_val;
}

static int adu_read(struct ie_desc *ie_adu)
{
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);
	int dlen;

	dlen = os_fifo_read(rpc_desc->fd, ie_adu->data, ie_hdr->tot_len);
	if(dlen < 0) {
		return -1;
	}

	ie_hdr->tot_len = dlen;

	return dlen;
}

#define MIN(a, b) (a < b)? a : b

/* Read IE data into buffer that has been previously allocated.
   rx_hdr refers to header of data already available in IPC.

return: -1 on error otherwise size of data read from IPC
 */
static int adu_read_preset(struct ie_desc *ps_adu, struct ie_head *rx_hdr)
{
	struct ie_head *ps_hdr = IE_HEADER(ps_adu);  /* Preset ADU */
	struct ie_desc  ie_adu[1];                   /* Local  ADU */
	int ret_val = -1;

	ps_hdr->elem_id         = rx_hdr->elem_id;
	ps_hdr->ct_word         = rx_hdr->ct_word;

	if(ps_hdr->tot_len >= rx_hdr->tot_len) {
		/* Read in, if there is sufficient buffer for availalbe data */
		ps_hdr->tot_len = rx_hdr->tot_len;

		ret_val = adu_read(ps_adu);

		goto adu_rd_ps_exit1;
	}

	/* Set up a local buffer to read in data from RPC */
	ie_adu->head.elem_id = rx_hdr->elem_id;
	ie_adu->head.ct_word = rx_hdr->ct_word;
	ie_adu->head.tot_len = rx_hdr->tot_len;

	ie_adu->data = malloc(rx_hdr->tot_len);
	if(!ie_adu->data)
		goto adu_rd_ps_exit1;

	if(adu_read(ie_adu) < 0)
		goto adu_rd_ps_exit2;

	/* Populate preset ADU with actual length and data */
	ps_hdr->tot_len = MIN(ps_hdr->tot_len, rx_hdr->tot_len);
	memcpy(ps_adu->data, ie_adu->data, ps_hdr->tot_len);
	ret_val = ps_hdr->tot_len;

adu_rd_ps_exit2:
	if (ie_adu->data) {
	free(ie_adu->data);
	ie_adu->data = NULL;
	}

adu_rd_ps_exit1:
	return ret_val;
}

typedef int (*awaited_adu_fn)(struct ie_head*);

#define HAS_IE_GOT_DATA(elem_hd) (elem_hd->tot_len != 0)

#define IS_CTW_ACK_XACT(ct_word) /* To be defined */

/* Client: process an ADU that was received as well as awaited
Return: 0 for not an awaited ADU
1 for an awaited ADU
 */
static int cli_rx_awaited(struct ie_head *rx_hdr)
{
	unsigned int ct_word = rx_hdr->ct_word;
	struct rm_xact *xact;
	struct ie_desc *ps_adu;  /* Preset ADU */
	struct ie_head *ps_hdr;

	xact = wq_xact_find_get(CTW_2_XACT_ID(ct_word), &ps_adu);
	if(!xact)
		return 0;        /* Not an awaited ADU */

	if(HAS_IE_GOT_DATA(rx_hdr)) {
		/* Transaction has data so, let us process further */
		if(adu_read_preset(ps_adu, rx_hdr) < 0)
			return -1;
	} else {
		ps_hdr = IE_HEADER(ps_adu);
		ps_hdr->elem_id         = rx_hdr->elem_id;
		ps_hdr->ct_word         = rx_hdr->ct_word;
		ps_hdr->tot_len         = 0;
#ifdef VERBOSE_LOGGING
		LOGPRINT(rpc, info, "cli_rx_awaited: empty adu, signal xact");
#endif
	}

	/* If last transaction in waiting RPC MSG then, signal thead */
	wq_xact_update(xact, ps_adu);

	return 1;
}

static int adu_recv(struct ie_desc *ie_adu, awaited_adu_fn awaited_adu_rx,
					int *awaited)
{
	struct ie_head *ie_hdr = IE_HEADER(ie_adu);
	*awaited = 0;

	if(os_fifo_read(rpc_desc->fd, ie_hdr, IE_HDR_SZ) != IE_HDR_SZ)
		goto adu_rcv_exit1;   /* Error in reading IE header  */

	if(awaited_adu_rx) {
		*awaited = awaited_adu_rx(ie_hdr);
		if(*awaited < 0)
			goto adu_rcv_exit1;  /* Error in preset read */

		if(*awaited > 0) {
			goto adu_rcv_exit2;  /* Success in preset rd */
		}
	}

	/* No preset read i.e. *awaited is 0, contine w/ processing */

	if(HAS_IE_GOT_DATA(ie_hdr)) {
		/* Transaction has data so, let us process further */
		if(!(ie_adu->data = malloc(ie_hdr->tot_len)))
			goto adu_rcv_exit1;  /* No memory to alloc   */

		if(adu_read(ie_adu) < 0) {
			if (ie_adu->data) {
			free(ie_adu->data);
			ie_adu->data = NULL;
			}
			goto adu_rcv_exit1;  /* IE data read error   */
		}
	}

adu_rcv_exit2:
	return (ie_hdr->tot_len + IE_HDR_SZ);

adu_rcv_exit1:
	return -1;
}

struct frame_delimit {
	unsigned int   magic1;
};

#define MAGIC_AA 0xAAAAAAAA
#define MAGIC_55 0x55555555

#define SOF_SIZE sizeof(struct frame_delimit)
#define EOF_SIZE sizeof(struct frame_delimit)

struct frame_delimit sof_sign[1] = {{MAGIC_AA}};
struct frame_delimit eof_sign[1] = {{MAGIC_55}};

static int rm_head_recv(struct rpc_msg *rmsg)
{
	struct frame_delimit sof[1] = {{MAGIC_AA}};
	struct iovec rd_vec[2], *rd;
	int rd_len = 0;

	rd = rd_vec;

	rd_len += util_iovec_set(rd++, sof, SOF_SIZE); /* Start of Frame */

	rd_len += util_iovec_set(rd++, RM_HEADER(rmsg), RM_HDR_SZ);/* Msg HDR */

	if(os_fifo_readv(rpc_desc->fd, rd_vec, rd - rd_vec) != rd_len)
		return -1;

	if(sof->magic1 != MAGIC_AA)
		return -1;

	return 0;
}

static int rm_tail_recv(void)
{
	struct frame_delimit eof[1] = {{MAGIC_55}};
	struct iovec rd_vec[1], *rd;
	int rd_len = 0;

	rd = rd_vec;

	rd_len += util_iovec_set(rd++, eof, EOF_SIZE); /* End of Frame */

	if(os_fifo_readv(rpc_desc->fd, rd_vec, rd - rd_vec) != rd_len)
		return -1;

	if(eof->magic1 != MAGIC_55)
		return -1;

	return 0;
}

/* Receive a message from FIFO */
static struct rpc_msg *msg_recv(awaited_adu_fn awaited_adu_rx)
{
	struct rpc_msg *rmsg = msg_alloc();
	struct rm_head *rm_hdr;
	int n_ie;

	if(!rmsg)
		goto mrcv_exit1;

	if(rm_head_recv(rmsg) < 0)
		goto mrcv_exit2;

	rm_hdr = RM_HEADER(rmsg);   /* Has info about available msg */

	for(n_ie = 0; n_ie < rm_hdr->num_ie; n_ie++) {
		struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
		struct ie_head *ie_hdr = IE_HEADER(ie_adu);
		int awaited = 0;    /* Indicates an awaited ADU     */

		/* Receive Application Data Unit */
		if(adu_recv(ie_adu, awaited_adu_rx, &awaited) < 0)
			goto mrcv_exit2;  /* Error */

		if(!awaited)
			continue; /* Skip; not an awaited ADU */

		/* An awaited ADU, used up, make reqd adjustments */
		rm_hdr->length -= sizeof(struct ie_head);
		rm_hdr->length -= ie_hdr->tot_len;

		ie_adu->data    = NULL;
		ie_hdr->tot_len = 0;
		ie_hdr->elem_id = REI_NOTDEF;
	}

	if(rm_tail_recv() < 0)
		goto mrcv_exit2;

	return rmsg;

mrcv_exit2:
	msg_free(rmsg);

mrcv_exit1:
	return NULL;
}

/* Client: receive a message from server */
struct rpc_msg *rpc_cli_recv(void)
{
	return msg_recv(cli_rx_awaited);
}

/* Server: receive a message from client */
struct rpc_msg *rpc_svr_recv(void)
{
	awaited_adu_fn svr_rx_awaited = NULL;  /* No preset ADU awaited */

	return msg_recv(svr_rx_awaited);
}

static unsigned int ie_xact_id_find(unsigned int elem_id)
{
	UNUSED(elem_id);
	return 0;
}

/* Queue IE to the RPC */
struct ie_desc* rpc_ie_attach(struct rpc_msg *rmsg,  unsigned short elem_id,
							  unsigned char oper_id, unsigned short tot_len,
							  void *data)
{
	struct rm_head *rm_hdr = NULL;
	struct ie_head *ie_hdr = NULL;
	struct ie_desc *ie_adu = NULL;
	int n_ie = 0;

	rm_hdr = RM_HEADER(rmsg);   /* Get header to RPC Message */

	for(n_ie = 0; n_ie < MAX_IE; n_ie++) {

		ie_adu = rmsg->info_elem + n_ie;
		ie_hdr = IE_HEADER(ie_adu);

		if(!ie_adu->data) { /* Free IE (app data unit)   */

			ie_hdr->elem_id = elem_id;
			ie_hdr->tot_len = tot_len;
			ie_hdr->ct_word = OPER_ID_2_CTW(oper_id);

			ie_adu->data    = data;

			rm_hdr->num_ie++;
			rm_hdr->length += (IE_HDR_SZ + tot_len);

			break;
		}
	}

	return ie_adu;
}

/* Detach and free (i.e. delete) data, if flagged to do so */
static void msg_ie_cancel(struct rpc_msg *rmsg, struct ie_desc *ie_adu,
						  int del_flag)
{
	struct rm_head *rm_hdr = RM_HEADER(rmsg);
	int n_ie = 0;

	for(n_ie = 0; n_ie < MAX_IE; n_ie++) {

		if((rmsg->info_elem + n_ie) == ie_adu) {
			struct ie_head *ie_hdr = IE_HEADER(ie_adu);

			rm_hdr->num_ie--;
			rm_hdr->length -= (IE_HDR_SZ + ie_hdr->tot_len);

			ie_hdr->elem_id = REI_NOTDEF;
			ie_hdr->tot_len = 0;

			if(del_flag && ie_adu->data) 
				free(ie_adu->data);
			ie_adu->data = NULL;

			break;
		}
	}

	return;

}

void rpc_ie_detach(struct rpc_msg *rmsg, struct ie_desc *ie_adu)
{
	return msg_ie_cancel(rmsg, ie_adu, 0);
}

void rpc_ie_freeup(struct rpc_msg *rmsg, struct ie_desc *ie_adu)
{
	return msg_ie_cancel(rmsg, ie_adu, 1);
}

/* Completes control word for each of IE contained in MSG. Specifically, checks
   whether IE was previously requested or whether it is an async indiaction.
 */
static void svr_send_prep(struct rpc_msg *rmsg, enum rpc_xact xtype)
{
	struct rm_head *rm_hdr = RM_HEADER(rmsg);
	int n_ie = 0;

	for(n_ie = 0; n_ie < rm_hdr->num_ie; n_ie++) {

		struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
		struct ie_head *ie_hdr = IE_HEADER(ie_adu);
		unsigned int xact_id;

		if (CTW_2_OPER_ID(ie_hdr->ct_word) != OPER_INF) {
			if(wq_ie_find_get(ie_hdr->elem_id, 
							  CTW_2_OPER_ID(ie_hdr->ct_word), 
							  &xact_id))
				adu_ct_word_make(ie_hdr, rpc_rsp, xact_id);
			else
				adu_ct_word_next(ie_hdr, rpc_ind);
		} else
			adu_ct_word_next(ie_hdr, rpc_ind);
#ifdef VERBOSE_LOGGING
		LOGPRINT(rpc, info, "svr_send_prep: using xact id [0x%x] and elem_id [0x%x]",
				 CTW_2_XACT_ID(ie_hdr->ct_word), ie_hdr->elem_id);
#endif
	}

	UNUSED(xtype);
}

/* Write IO_VECS: 1 (sof) + 1 (msg) + MAX_IE (ie_hdr) + MAX_IE (data) + 1 (eof) */
#define WR_VEC_COUNT       (1 + 1 + MAX_IE + MAX_IE + 1)

/* Send message out on FIFO */
int msg_send(struct rpc_msg *rmsg)
{
	struct iovec wr_vec[WR_VEC_COUNT], *io, *wr;
	struct rm_head *rm_hdr = NULL;
	int n_ie = 0, wr_len = 0;

	wr = io = wr_vec + 0;

	wr_len += util_iovec_set(wr++, sof_sign, SOF_SIZE); /* Start of Frame */

	rm_hdr = RM_HEADER(rmsg);
	wr_len += util_iovec_set(wr++, rm_hdr, RM_HDR_SZ);  /* Message Header */

	for(n_ie = 0; n_ie < rm_hdr->num_ie; n_ie++) {
		struct ie_desc *ie_adu = rmsg->info_elem + n_ie;
		struct ie_head *ie_hdr = IE_HEADER(ie_adu);

		wr_len += util_iovec_set(wr++, ie_hdr, IE_HDR_SZ); /* IE HDR  */

		if(!HAS_IE_GOT_DATA(ie_hdr))
			continue; /* Skip following  */

		/* ADU contents */
		wr_len += util_iovec_set(wr++, ie_adu->data, ie_hdr->tot_len);
	}

	wr_len += util_iovec_set(wr++, eof_sign, EOF_SIZE);  /* End of frame  */

	os_mutex_take(rpc_desc->wr_mutex_hnd);

	if(os_fifo_writev(rpc_desc->fd, io, wr - io) == wr_len) {
		os_mutex_give(rpc_desc->wr_mutex_hnd);
		return rm_hdr->length;
	}

	os_mutex_give(rpc_desc->wr_mutex_hnd);

	return -1;
}

int rpc_svr_send(struct rpc_msg *rmsg)
{
	/* Add error checking and other handling */

	svr_send_prep(rmsg, rpc_rsp);

	return msg_send(rmsg);
}

static int msg_send_ct_word(enum rpc_xact xtype, unsigned short oper_id,
							unsigned int xact_id)
{
	struct rpc_msg rmsg[1];
	struct rm_head *rm_hdr;
	struct ie_desc *ie_adu;
	struct ie_head *ie_hdr;

	rm_hdr          = RM_HEADER(rmsg);
	rm_hdr->num_ie  = 1;

	ie_adu          = rmsg->info_elem + 0;

	ie_hdr          = IE_HEADER(ie_adu);
	ie_hdr->ct_word = OPER_ID_2_CTW(oper_id);
	ie_hdr->tot_len = 0;
	ie_hdr->elem_id = 0;

	adu_ct_word_make(ie_hdr, xtype, xact_id);

	rm_hdr->length  = IE_HDR_SZ;

	return msg_send(rmsg);
}

int rpc_send_ack(unsigned short oper_id, unsigned int xact_id)
{
#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "sending ack for xact id [0x%x]", xact_id);
#endif
	return msg_send_ct_word(rpc_ack, oper_id, xact_id);
}

int rpc_send_err(unsigned short oper_id, unsigned int xact_id)
{
#ifdef VERBOSE_LOGGING
	LOGPRINT(rpc, info, "sending err for xact id [0x%x]", xact_id);
#endif
	return msg_send_ct_word(rpc_err, oper_id, xact_id);
}

/* Prepares message from client to server */
static int cli_send_prep(struct rpc_msg *rmsg_i, struct rpc_msg *rmsg_o)
{
	struct rm_head *rm_hdr_i = RM_HEADER(rmsg_i);
	int n_ie = 0;
	enum rpc_xact xtype = rpc_cmd;

	if(rmsg_o) {
		struct rm_head *rm_hdr_o;
		xtype = rpc_req;  /* Changing transaction to requrest */

		/* Data contents expected from server; do basic tests */
		rm_hdr_o = RM_HEADER(rmsg_o);
		if(rm_hdr_i->num_ie != rm_hdr_o->num_ie)
			return -1;
	}

	for(n_ie = 0; n_ie < rm_hdr_i->num_ie; n_ie++) {
		/* Support to setup expected transactions from server */
		struct ie_desc *ie_adu_o;
		struct ie_head *ie_hdr_o;

		/* Create control word for next transaction to server */
		struct ie_desc *ie_adu_i = rmsg_i->info_elem + n_ie;
		struct ie_head *ie_hdr_i = IE_HEADER(ie_adu_i);

		adu_ct_word_next(ie_hdr_i, xtype);

		if(!rmsg_o)
			continue; /* Skip, no IE expected from server */

		/* IE expected from server; save transaction to match */
		ie_adu_o = rmsg_o->info_elem + n_ie;
		ie_hdr_o = IE_HEADER(ie_adu_o);
		ie_hdr_o->ct_word = CTW_2_XACT_ID(ie_hdr_i->ct_word);
#ifdef VERBOSE_LOGGING
		LOGPRINT(rpc, info, "cli_msg_prep: using xact id [0x%x]",
				 CTW_2_XACT_ID(ie_hdr_i->ct_word));
#endif
	}

	return 0;
}

int rpc_cli_send(struct rpc_msg *rmsg)
{
	struct rm_xact *xact;
	/* Add error checking and other handling */

	cli_send_prep(rmsg, NULL);    /* Add transaction ID */

	xact = wq_xact_cmpl_wait(rmsg);      /* Blocking call      */
	if (!xact)
		return -1;

	if(msg_send(rmsg) < 0)
		return -1;

	/* Block here for xact->sem_hnd */
	os_sem_take(xact->sem_hnd); /* Change as per OS requirements */

	return 0;
}

int rpc_cli_read(struct rpc_msg *rmsg_i, struct rpc_msg *rmsg_o)
{
	struct rm_xact *xact;
	/* Add error checking and other handling */

	cli_send_prep(rmsg_i, rmsg_o); /* Add transaction ID */

	xact = wq_xact_cmpl_wait(rmsg_o);    /* Blocking call      */
	if (!xact)
		return -1;	

	if(msg_send(rmsg_i) < 0)
		return -1;

	/* Block here for xact->sem_hnd */
	os_sem_take(xact->sem_hnd); /* Change as per OS requirements */

	return 0;
}

int rpc_cli_init(char *rd_fifo, char *wr_fifo)
{
	os_services_init();

	strcpy(rpc_desc->name, "proxy-cli");
	rpc_desc->c_or_s = client;
	rpc_desc->wq_elem = NULL;
	rpc_desc->seq_num = 0;

	if ((rpc_desc->fd = os_fifo_init(rd_fifo, wr_fifo)) < 0)
		goto rpc_cli_init_err1;

	if (!(rpc_desc->wr_mutex_hnd = os_mutex_init()))
		goto rpc_cli_init_err2;

	if (!(rpc_desc->list_mutex_hnd = os_mutex_init()))
		goto rpc_cli_init_err3;

	rm_xact_init();

	return 0;

rpc_cli_init_err3:
	os_mutex_exit(rpc_desc->wr_mutex_hnd);

rpc_cli_init_err2:
	os_fifo_exit(rpc_desc->fd);

rpc_cli_init_err1:
	os_services_exit();

	return -1;
}

int rpc_cli_exit(void)
{
	rm_xact_exit();

	os_mutex_exit(rpc_desc->list_mutex_hnd);

	os_mutex_exit(rpc_desc->wr_mutex_hnd);

	os_fifo_exit(rpc_desc->fd);

	os_services_exit();

	return 0;
}

int rpc_svr_init(char *rd_fifo, char *wr_fifo)
{
	strcpy(rpc_desc->name, "proxy-svr");
	rpc_desc->c_or_s = server;
	rpc_desc->wq_rmsg = NULL;
	rpc_desc->seq_num = 0;

	if ((rpc_desc->fd = os_fifo_init(rd_fifo, wr_fifo)) < 0)
		goto rpc_svr_init_err1;

	if (!(rpc_desc->wr_mutex_hnd = os_mutex_init()))
		goto rpc_svr_init_err2;

	if (!(rpc_desc->list_mutex_hnd = os_mutex_init()))
		goto rpc_svr_init_err3;

	wq_ie_xact_init();

	return 0;

rpc_svr_init_err3:
	os_mutex_exit(rpc_desc->wr_mutex_hnd);

rpc_svr_init_err2:
	os_fifo_exit(rpc_desc->fd);

rpc_svr_init_err1:
	os_services_exit();

	return -1;
}

int rpc_svr_exit(void)
{
	wq_ie_xact_exit();

	os_mutex_exit(rpc_desc->list_mutex_hnd);

	os_mutex_exit(rpc_desc->wr_mutex_hnd);

	os_fifo_exit(rpc_desc->fd);

	return 0;
}
