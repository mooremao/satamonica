/*
 * logger.c
 *
 * Logging procedures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdio.h>
#include <stdarg.h>
#include <sys/time.h>
#include <time.h>

#include "logger.h"

#include "ti_log.h"
#ifdef ANDROID
#include <utils/Log.h>

#define LOGD ALOGD
#else
#define LOGD printf("\n"); printf
	
#endif

/* CAUTION !!!! Do NOT change the order from enum mod_id in logger.h. 
                keep appending additional log strings and maintain the s
                same order in enum mod_id logger.h */
static char *mod_id_str[] = {
	"",
	"D2H",                 	
	"H2D",                 	
	"SEQ",                 	
	"API",                 	
	"RPC",                 	
	"OS",                  	
	"AI2",                 	
	"NVS",                 	
	"CONFIG",          		          
	"VER",                  
	"ASYNC_EVT",  			
	"RX_ERR",           	
	"RX_OFF",           	
	"RX_ON",             	
	"RX_IDLE",           	
	"RX_WAKE",      		
	"DEL_ASSIST",   		
	"QOP",                 	
	"PATCH",             	
	"MSR_FILTER",  			
	"FREQ_EST",      		
	"TIME_UNC",    			
	"TIME_INJ",         		
	"POS_INJ",         		
	"EPH_INJ",          	
	"ALM_INJ",        		
	"ACQ_INJ",         		
	"SVDIR_INJ",     		
	"HLT_INJ",          	
	"ION_INJ",          	
	"UTC_INJ",         		
	"RTI_INJ",           	
	"DGPS_INJ",      		
	"TIME_RPT",      		
	"EPH_RPT",        		
	"ALM_RPT",       		
	"POS_RPT",       		
	"MSR_RPT",       		
	"HLT_REP",         		
	"ION_REP",        		
	"UTC_RPT",        		
	"SVDIR_RPT"     		
/* add here */
};

static char *log_cat_str[] = {
    "info",
	"warn",                
	"err"
};

static unsigned int log_mask = 0x0000000F; /* by defauld enable all */

#define DPROXY_LOG_INFO		1
#define DPROXY_LOG_WARN		2
#define DPROXY_LOG_ERR		4
#define DPROXY_LOG_CRITICAL 16


/* : Implement syslog calls. Timebeing it is just printf. */
void logprint(enum mod_id mod_id, enum log_cat log_cat, const char *file,
					const int line, const char *fmt, ...)
{
	char buf[LOG_BUFFER_SIZE];
	va_list ap;
	char outstr[200];
	time_t t;
	struct tm *tmp;

	t = time(NULL);
	tmp = localtime(&t);

	if(tmp!=NULL)
		strftime(outstr, sizeof(outstr), "%d:%m:%Y-%H:%M:%S", tmp);

	va_start(ap, fmt);
	vsnprintf(buf, sizeof(buf), fmt, ap);

	if ((log_cat == info) && (log_mask & DPROXY_LOG_INFO)) {

		if (mod_id != generic) {
			//TI_INFO("[%s][%s]%s", outstr,mod_id_str[mod_id], buf);
			LOGD("[%s][%s]%s", outstr,mod_id_str[mod_id], buf);
		}
		else {

			//TI_INFO("[%s]%s", outstr, buf);			
			LOGD("[%s]%s", outstr, buf);			
		}

	} else if ( (log_cat == warn)  && (log_mask & DPROXY_LOG_WARN)) {


		if (mod_id != generic) {
			//TI_WARN("[%s][%s][Warn]%s", outstr,mod_id_str[mod_id], buf); 
			LOGD("[%s][%s][Warn]%s", outstr,mod_id_str[mod_id], buf); 
		}
		else {

			//TI_WARN("[%s][Warn]%s", outstr, buf); 
			LOGD("[%s][Warn]%s", outstr, buf); 
		}


	} else if (((log_cat == err)  && (log_mask & DPROXY_LOG_ERR))){


		if (mod_id != generic) {
			//TI_ERRx("[%s][%s][Error]%s (%s, %d)", outstr, 
			//	mod_id_str[mod_id], buf, file, line);
			LOGD("[%s][%s][Error]%s (%s, %d)", outstr, 
				mod_id_str[mod_id], buf, file, line);
		}
		else {

			//TI_ERRx("[%s][Error]%s (%s, %d)", outstr, buf, file, line);
			LOGD("[%s][Error]%s (%s, %d)", outstr, buf, file, line);
		}

	
	} else {
		/* Do Nothing */
	}
}

void update_logmask(unsigned int logging_mask)
{
	log_mask = logging_mask;
}
