/*
 * os_services.c
 *
 * OS specific procedures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <math.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#ifdef ANDROID
#include <sys/time.h>
#else
#include <time.h>

#endif
#include "os_services.h"
#include "utils.h"
#include "logger.h"

#define MAX_OS_TIMER 4

static struct os_timer_desc  os_timer_array2use[MAX_OS_TIMER];
static struct os_timer_desc *free_os_timer_list = NULL;

static pthread_mutex_t os_timer_mtx;
struct os_mutex {

	pthread_mutex_t  mutex;
	struct os_mutex *next;
};

#define MAX_OS_MUTEXES 32

static struct os_mutex *os_mutex_free_list = NULL;
static struct os_mutex os_mutex_array[MAX_OS_MUTEXES] = {
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL}
};

//static void *osal_timer_sched(unsigned int tout_secs, unsigned int intv_secs, os_timer_cb cb_fn, void *param);

static void  osal_timer_abort(void *timer_hnd);

static void os_timer_handler_thread(union sigval val);

static void os_mutex_list_init(void)
{
	struct os_mutex *mutex = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_MUTEXES; i++) {
		mutex = os_mutex_array + i;

		ADD_LIST_ATOMIC(NULL, mutex, os_mutex_free_list);
	}
}

void* os_mutex_init(void)
{
	struct os_mutex *mutex = NULL;

	POP_LIST_ATOMIC(NULL, os_mutex_free_list, mutex);

	return mutex;
}

int os_mutex_take(void *mtx_hnd)
{
	int ret_val;
	struct os_mutex *mutex = (struct os_mutex *)mtx_hnd;

	if (mutex) {
		ret_val = pthread_mutex_lock(&mutex->mutex);
		if (ret_val != 0) {
			LOGPRINT(os, err, "pthread_mutex_lock() failed: %s",
							strerror(ret_val));
			exit(EXIT_FAILURE);
		}

		return 0;
	} else {
		LOGPRINT(os, err, "invalid pointer");
		exit(EXIT_FAILURE);
	}
}

int os_mutex_give(void *mtx_hnd)
{
	int ret_val;
	struct os_mutex *mutex = (struct os_mutex *)mtx_hnd;

	if (mutex) {
		ret_val = pthread_mutex_unlock(&mutex->mutex);
		if (ret_val != 0) {
			LOGPRINT(os, err, "pthread_mutex_unlock() failed: %s",
							strerror(ret_val));
			exit(EXIT_FAILURE);
		}

		return 0;
	} else{
		LOGPRINT(os, err, "invalid pointer");
		exit(EXIT_FAILURE);
	}
}

void os_mutex_exit(void *mtx_hnd)
{
	struct os_mutex *mutex = (struct os_mutex *)mtx_hnd;

	ADD_LIST_ATOMIC(NULL, mutex, os_mutex_free_list);
}

static void os_mutex_list_exit(void)
{
	struct os_mutex *mutex = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_MUTEXES && os_mutex_free_list; i++) {

		mutex = os_mutex_free_list;
		os_mutex_free_list = os_mutex_free_list->next;
		mutex->next = NULL;
	}
}

struct os_sem {

	sem_t          sem;
	struct os_sem *next;
};

#define MAX_OS_SEMAPHORES 20

static struct os_sem *os_sem_free_list = NULL;
static struct os_sem os_sem_array[MAX_OS_SEMAPHORES];

static void os_sem_list_init(void)
{
	struct os_sem *sem = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_SEMAPHORES; i++) {
		sem = os_sem_array + i;

		sem_init(&sem->sem, 0, 0);

		ADD_LIST_ATOMIC(NULL, sem, os_sem_free_list);
	}
}

void* os_sem_init(void)
{
	struct os_sem *sem = NULL;

	POP_LIST_ATOMIC(NULL, os_sem_free_list, sem);

	return sem;
}

int os_sem_take(void *sem_hnd)
{
	int ret_val;
	struct os_sem *sem = (struct os_sem *)sem_hnd;

	if (sem) {
		ret_val = sem_wait(&sem->sem);
		if (ret_val != 0) {
			LOGPRINT(os, err, "sem_wait() failed %s",
							strerror(ret_val));
			exit(EXIT_FAILURE);
		}

		return 0;
	} else {
		LOGPRINT(os, err, "invalid pointer");
		exit(EXIT_FAILURE);
	}
}

int os_sem_give(void *sem_hnd)
{
	int ret_val;
	struct os_sem *sem = (struct os_sem *)sem_hnd;

	if (sem) {
		ret_val = sem_post(&sem->sem);
		if (ret_val != 0) {
			LOGPRINT(os, err, "sem_post() failed %s",
							strerror(ret_val));
			exit(EXIT_FAILURE);
		}

		return 0;
	} else {
		LOGPRINT(os, err, "invalid pointer");
		exit(EXIT_FAILURE);
	}
}

void os_sem_exit(void *sem_hnd)
{
	struct os_sem *sem = (struct os_sem *)sem_hnd;

	ADD_LIST_ATOMIC(NULL, sem, os_sem_free_list);
}

static void os_sem_list_exit(void)
{
	struct os_sem *sem = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_SEMAPHORES && os_sem_free_list; i++) {

		sem = os_sem_free_list;
		os_sem_free_list = os_sem_free_list->next;
		sem->next = NULL;
	}
}

struct os_fifo_desc {
	char *rd_path;
	int rd_fd;

	char *wr_path;
	int wr_fd;

	struct os_fifo_desc *next;
};

#define MAX_OS_FIFOS 20

static struct os_fifo_desc *os_fifo_free_list = NULL;
static struct os_fifo_desc os_fifo_array[MAX_OS_FIFOS];

static void os_fifo_list_init(void)
{
	struct os_fifo_desc *fifo = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_FIFOS; i++) {
		fifo = os_fifo_array + i;

		ADD_LIST_ATOMIC(NULL, fifo, os_fifo_free_list);
	}
}

int os_fifo_init(char *rd_name, char *wr_name)
{
	int ret_val;
	struct os_fifo_desc *fifo;

	if (!rd_name || !wr_name)
		goto os_fifo_init_err1;

	POP_LIST_ATOMIC(NULL, os_fifo_free_list, fifo);
	if (!fifo)
		goto os_fifo_init_err1;

	fifo->rd_path = (char *)malloc(strlen(rd_name) + 1);
	if(!fifo->rd_path)
		goto os_fifo_init_err2;

	strcpy(fifo->rd_path, rd_name);

	fifo->wr_path = (char *)malloc(strlen(wr_name) + 1);
	if(!fifo->wr_path)
		goto os_fifo_init_err3;

	strcpy(fifo->wr_path, wr_name);

	ret_val = mkfifo(rd_name, 0666);
	if ((ret_val == -1) && (errno != EEXIST)) {
		LOGPRINT(os, err, "error creating named pipe [%s]", rd_name);
		goto os_fifo_init_err4;
	}

	ret_val = mkfifo(wr_name, 0666);
	if ((ret_val == -1) && (errno != EEXIST)) {
		LOGPRINT(os, err, "error creating named pipe [%s]", wr_name);
		goto os_fifo_init_err4;
	}

	if((fifo->rd_fd = open(rd_name, O_RDWR)) < 0) {
		LOGPRINT(os, err, "error opening read fifo [%s]", rd_name);
		goto os_fifo_init_err4;
	}

	if((fifo->wr_fd = open(wr_name, O_RDWR)) < 0) {
		LOGPRINT(os, err, "error opening write fifo [%s]", wr_name);
		goto os_fifo_init_err5;
	}

	return (fifo - os_fifo_array);

os_fifo_init_err5:
	close(fifo->rd_fd);

os_fifo_init_err4:
	free(fifo->wr_path);

os_fifo_init_err3:
	free(fifo->rd_path);

os_fifo_init_err2:
	ADD_LIST_ATOMIC(NULL, os_fifo_free_list, fifo);

os_fifo_init_err1:
	return -1;
}

ssize_t os_fifo_writev(int fd, const struct iovec *iov, int iovcnt)
{
	struct os_fifo_desc *fifo = os_fifo_array + fd;

	return writev(fifo->wr_fd, iov, iovcnt);
}

ssize_t os_fifo_readv(int fd, const struct iovec *iov, int iovcnt)
{
	struct os_fifo_desc *fifo = os_fifo_array + fd;

	return readv(fifo->rd_fd, iov, iovcnt);
}

ssize_t os_fifo_write(int fd, const void *buf, size_t count)
{
	struct os_fifo_desc *fifo = os_fifo_array + fd;

	return write(fifo->wr_fd, buf, count);
}

ssize_t os_fifo_read(int fd, void *buf, size_t count)
{
	struct os_fifo_desc *fifo = os_fifo_array + fd;

	return read(fifo->rd_fd, buf, count);
}

void os_fifo_exit(int fd)
{
	struct os_fifo_desc *fifo = os_fifo_array + fd;

	close(fifo->wr_fd);

	close(fifo->rd_fd);

	free(fifo->wr_path);

	free(fifo->rd_path);

	ADD_LIST_ATOMIC(NULL, os_fifo_free_list, fifo);
}

static void os_fifo_list_exit(void)
{
	struct os_fifo_desc *fifo = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_FIFOS && os_fifo_free_list; i++) {

		fifo = os_fifo_free_list;
		os_fifo_free_list = os_fifo_free_list->next;
		fifo->next = NULL;
	}
}

void *os_create_thread(thread_fn function, void *thread_arg, char *thr_name)
{
	pthread_t *thread_id;

	thread_id = (pthread_t *)malloc(sizeof(pthread_t));
	if (!thread_id) {
		LOGPRINT(os, err, "malloc failed");
		exit(EXIT_FAILURE);
	}

	if (pthread_create(thread_id, NULL, function, thread_arg) != 0) {
		LOGPRINT(os, err, "pthread_create failed");
		free(thread_id);
		exit(EXIT_FAILURE);
	}
	if(pthread_setname_np(*thread_id, thr_name) != 0){
		LOGPRINT(os, err, "pthread_setname_np failed");
	}
	LOGPRINT(os, info, "thread [0x%x] started successfully",
						(unsigned int)*thread_id);
	return (void *)thread_id;
}

void os_destroy_thread(void *thrd_hnd)
{
	pthread_t *thread_id = (pthread_t *)thrd_hnd;

	//pthread_cancel(*thread_id);

	pthread_join(*thread_id, NULL);

	LOGPRINT(os, info, "thread [0x%x] destroyed successfully",
						(unsigned int)*thread_id);
	free(thread_id);
}

void os_exit_thread(void *val)
{
	pthread_exit(val);
}

void os_services_init(void)
{
	os_fifo_list_init();

	os_mutex_list_init();

	os_sem_list_init();
}

void os_services_exit(void)
{
	os_sem_list_exit();

	os_mutex_list_exit();

	os_fifo_list_exit();
}

int os_add_sig_handler(int signum, sig_handler handler)
{
	struct sigaction sa;
	
	if (handler) {	
		sa.sa_handler = handler;
		return sigaction(signum, &sa, NULL);
	} else {
		sa.sa_handler = SIG_IGN;
		return sigaction(signum, &sa, NULL);
	}
}

int os_rem_sig_handler(int signum)
{
	struct sigaction sa;

	sa.sa_handler = SIG_DFL;
	return sigaction(signum, &sa, NULL);
}

unsigned int os_utc_secs()
{
        return os_time_secs();
}

int os_is_file_exists(char *file_name)
{
	return access(file_name, F_OK);
}

void *os_get_stat() 
{
	return malloc(sizeof(struct stat));
}

int os_stat_file(char *file_name, void *f_stat)
{
	return stat(file_name, (struct stat*)f_stat);
}

int os_file_size(void *f_stat)
{
 	return ((struct stat*)f_stat)->st_size;
}

void os_free_stat(void *f_stat)
{
	free(f_stat);
}

double os_service_log(double d_value)
{
	return log10(d_value);
}

double os_service_sqrt(double d_value)
{
	return sqrt(d_value);
}

double os_service_ldexp(double mantissa, unsigned int exponent)
{
	return ldexp(mantissa, exponent);
}

double os_service_floor(double d_value)
{
	return floor(d_value);
}

double os_service_fabs(double d_value)
{
	return fabs(d_value);
}

unsigned int os_time_value(unsigned int *secs, unsigned int *msecs)
{
	struct timeval tv;

	if(gettimeofday(&tv, NULL) == -1){
		//exit(-1);
		LOGPRINT(os,err,"os_time_value Failure");
		exit(EXIT_FAILURE);
	}

	*secs = tv.tv_sec;
	*msecs = tv.tv_usec / 1000;

	return 0;
}

unsigned int os_time_secs()
{
	struct timeval tv;

	if(gettimeofday(&tv, NULL) == -1){	
		//		exit(-1);
		LOGPRINT(os,err, "os_time_secs Failure");
		exit(EXIT_FAILURE);
	}

	return tv.tv_sec + ((tv.tv_usec > 50000000L)? 1 : 0); 
}

unsigned int os_time_msecs()
{
	struct timeval tv;

	if(gettimeofday(&tv, NULL) == -1){
//		exit(-1);
		LOGPRINT(os, err, "os_time_msecs Failure");
		exit(EXIT_FAILURE);
	}


	return ((tv.tv_sec * 1000) + (tv.tv_usec/1000)); 
}

unsigned int os_monotonic_secs()
{
	struct timespec tv;

	if(clock_gettime(CLOCK_MONOTONIC, &tv) == -1){
	//		exit(-1);
		LOGPRINT(os,err,"os_monotonic_secs Failure" );
		exit(EXIT_FAILURE);
	}

	return tv.tv_sec + ((tv.tv_nsec > 50000000000L)? 1 : 0); 
}

inline void os_timer_mtx_lockup(void)
{
	pthread_mutex_lock(&os_timer_mtx);
}

inline void os_timer_mtx_unlock(void)
{
	pthread_mutex_unlock(&os_timer_mtx);
}

static struct os_timer_desc* get_free_os_timer(void)
{
	struct os_timer_desc *os_timer;

	os_timer_mtx_lockup();

	os_timer = free_os_timer_list;
	if(NULL != os_timer) {
		free_os_timer_list = os_timer->next;
		os_timer->next = NULL;
	}

	os_timer_mtx_unlock();

	return os_timer;
}

static void _put_free_os_timer(struct os_timer_desc *os_timer)
{
	os_timer->cb_funct = NULL;
	os_timer->cb_param = NULL;
	os_timer->is_inuse = 0;
	os_timer->is_1shot = 0;

	os_timer->next     = free_os_timer_list;
	free_os_timer_list = os_timer;

	return;
}

static void put_free_os_timer(struct os_timer_desc *os_timer)
{
	os_timer_mtx_lockup();
	_put_free_os_timer(os_timer);
	os_timer_mtx_unlock();

	return;
}

/* Common handler for all timers that are managed by this implementation */
static void os_timer_handler_thread(union sigval val)
{
	struct os_timer_desc *os_timer = (struct os_timer_desc*) val.sival_ptr;
	printf("Timer handler thread %x.\n", (unsigned int)os_timer);

	os_timer_mtx_lockup();

	if(1 != os_timer->is_inuse) {
		/* Already aborted by app so, no processing required */
		goto os_timer_handler_thread_exit1;
	}

	/* Should be a very simple implementation */
	if(NULL != os_timer->cb_funct)
		os_timer->cb_funct(os_timer->cb_param);

	if(1 != os_timer->is_1shot) {
		/* A periodic timer - let it run for follow up CB(s) */
		goto os_timer_handler_thread_exit1;
	}

	/* 1 Shot timer - done with the job so, free it */
	_put_free_os_timer(os_timer);

os_timer_handler_thread_exit1:
	os_timer_mtx_unlock();
}

static void  osal_timer_abort(void *timer_hnd)
{
	struct os_timer_desc *os_timer = (struct os_timer_desc*) timer_hnd;
	struct itimerspec expiry;

	os_timer_mtx_lockup();

	if(1 != os_timer->is_inuse) {
		perror("Timer expired & already stopped, ignoring.\n");
		goto osal_timer_abort_err1;
	}

	/* Set up it_value as 0 to request OS to disarm a timer, if running */
	expiry.it_value.tv_sec     = 0;
	expiry.it_value.tv_nsec    = 0;
	expiry.it_interval.tv_sec  = 0;
	expiry.it_interval.tv_nsec = 0;

	if(0 != timer_settime(os_timer->timer_id, 0, &expiry, NULL)) {
		perror("Failed to stop the POSIX timer instance.\n");
	}

	_put_free_os_timer(os_timer);

osal_timer_abort_err1:

	os_timer_mtx_unlock();

	return;
}


void *osal_timer_sched(unsigned int tout_secs, unsigned int intv_secs,
			      os_timer_cb cb_fn, void *param)
{
	struct itimerspec expiry;
	struct os_timer_desc *os_timer = NULL;

	expiry.it_value.tv_sec     = tout_secs;
	expiry.it_value.tv_nsec    = 0;
	expiry.it_interval.tv_sec  = intv_secs;
	expiry.it_interval.tv_nsec = 0;

	os_timer = get_free_os_timer();
	if(NULL == os_timer) {
		perror("Failed to find a free OS timer instance.\n");
		goto osal_timer_sched_err1;
	}

	os_timer->cb_funct = cb_fn;
	os_timer->cb_param = param;
	os_timer->is_1shot = (0 == intv_secs)? 1 : 0;
	os_timer->is_inuse = 1;

	if(0 != timer_settime(os_timer->timer_id, 0, &expiry, NULL)) {
		perror("Failed to start the POSIX timer instance.\n");
		put_free_os_timer(os_timer);
		os_timer = NULL;
	}

osal_timer_sched_err1:

	return (void*) os_timer;
}

void osal_timer_module_exit(void)
{
	while(free_os_timer_list) {
		struct os_timer_desc *os_timer = free_os_timer_list;
		timer_delete(os_timer->timer_id);
		free_os_timer_list = os_timer->next;
	}
}

void* os_timer_1shot(unsigned int tout_secs, os_timer_cb timer_fn,
		     void *cb_param)
{
	return osal_timer_sched(tout_secs, 0, timer_fn, cb_param);
}

void os_timer_stop(void *timer)
{
	return osal_timer_abort(timer);
}

void os_timer_module_exit(void)
{
	return osal_timer_module_exit();
}

int os_timer_module_init(void)
{
	int idx = 0;
	struct sigevent sigev;

	sigev.sigev_notify            = SIGEV_THREAD;
	sigev.sigev_notify_function   = os_timer_handler_thread;
	sigev.sigev_notify_attributes = NULL;

	pthread_mutex_init(&os_timer_mtx, NULL);

	for(idx = 0; idx < MAX_OS_TIMER; idx++) {

		struct os_timer_desc *os_timer = os_timer_array2use + idx;
		sigev.sigev_value.sival_ptr = os_timer;

		if(0 !=
		   timer_create(CLOCK_MONOTONIC, &sigev, &os_timer->timer_id)) {
			perror("Failed to create a POSIX timer instance.\n");
			goto os_timer_module_init_err1;
		}

		put_free_os_timer(os_timer);
	}

	return 0;

os_timer_module_init_err1:

	/* Run through list of timers already created */
	osal_timer_module_exit();

	return -1;
}



int os_sock_init(const char* sock_path)
{
	int ret_val;

	int sock_fd;
	socklen_t sock_len;
	struct sockaddr_un sock_addr;
	memset(&sock_addr,0,sizeof(sock_addr));

	/* Create a Unix Domain Socket */
	sock_fd = socket(AF_UNIX, SOCK_DGRAM,0);
	if (sock_fd  == -1) {
		perror("Failed to create socket\n");
		return -1;
	}
	
	sock_addr.sun_family = AF_UNIX; /* Internet IP */
	if((strlen(sock_path)+1) < 108){
		strcpy(sock_addr.sun_path, sock_path);
	}else{
		close(sock_fd);
		return -1;
	}
	
	unlink(sock_addr.sun_path);
	sock_len = strlen(sock_addr.sun_path) + sizeof(sock_addr.sun_family);

	ret_val = bind(sock_fd, (struct sockaddr *)&sock_addr, sock_len);
	if(ret_val < 0){
		perror("Failed to bind server socket on path \n");
		close(sock_fd);
		return -1;
	}

	if(chmod(sock_path, 0777) < 0) {
		perror("Failed to change permission socket on path\n");
	}


	return sock_fd;
}

int os_sock_exit(int fd)
{

	if(close(fd) < 0){
		perror("ERROR closing\n");
		return -1;
	}

	return 0;
}
int os_sem_timed_take(void *sem_hnd, int seconds)
{
	int ret_val = -1;
	struct os_sem *sem = (struct os_sem *)sem_hnd;
    struct timespec tm;

	//gettimeofday(&tm, NULL);
	//tm.tv_sec += seconds;
	/* Calculate relative interval as current time plus
       number of seconds given argv[2] */

   if (clock_gettime(CLOCK_REALTIME, &tm) == -1)
        LOGPRINT(os, info, "Error in clock_gettime" );
   
   tm.tv_sec += seconds;

    if (sem) {
	    LOGPRINT(os, info, "waiting for %d seconds", seconds);
	    ret_val = sem_timedwait(&sem->sem, &tm);
	    if (ret_val != 0 && errno != ETIMEDOUT) {
	        LOGPRINT(os, err, "sem_wait() failed %s",
	        strerror(ret_val));
	        //return ret_val;
	    } else if (errno == ETIMEDOUT) {
	       	LOGPRINT(os, err, "semaphore timedout(%d seconds)", seconds);
			//return ret_val;
		}

    	//return 0;
    } else {
		LOGPRINT(os, err, "invalid pointer");
		//return -1;
	}
		return ret_val;
}


int os_sock_send(int fd, const char *dst_path, char *data, int len)
{

	struct sockaddr_un sock_addr;
	int length = 0;
	memset(&sock_addr,0,sizeof(sock_addr));

	sock_addr.sun_family = AF_UNIX; /* Unix domain socket */
	if((strlen(dst_path)+1) < 108)
		strcpy(sock_addr.sun_path, dst_path);
	else
		return -1;
        
        
	/* send over unix path */
	if((length = sendto(fd, data, len, 0, (struct sockaddr *)       
                            &sock_addr, sizeof(sock_addr))) < 0)  {
		perror("Failed to send message: length ");
		return -1;
	}
        
	return length;
        
}

int os_sock_recv(int fd, char *data, int len)
{
        
	struct sockaddr_un sock_dst_addr;
	int length=0;
	socklen_t addr_len = sizeof(sock_dst_addr);
        
	/* Receive over Unix path */
	if((length = recvfrom(fd, data, len, 0, (struct sockaddr*)&sock_dst_addr,
                               &addr_len)) < 0)  {
		perror("Failed to receive message: length \n");
		return -1;
	}
        
	return length;
}
int os_log_time(char *buf)
{
	int len = 0;
	struct timeval tv;
	struct tm *timeinfo = NULL;
	char local_buf[50];

	gettimeofday(&tv, NULL);

	timeinfo = localtime(&tv.tv_sec);

	if (timeinfo == NULL)
		return 0;
	
	memset(local_buf, 0 , sizeof(local_buf));
	strftime(local_buf , 50, "%Y-%m-%d-%H-%M-%S", timeinfo);
	len = snprintf(buf ,50,"%s-%ld", local_buf, (long int)tv.tv_usec);

	return len;
}




