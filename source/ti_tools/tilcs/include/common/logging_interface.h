/*
 * logger.h
 *
 * Logging procedure prototypes
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _DPROXY_LOGGER_
#define _DPROXY_LOGGER_


/** @file ai2.h
    This file describes the interface for logging.
    This interface is undefined in release mode builds.

    @note Do NOT include any code in the logging macro that can affect the rest
    of the code. Logging macro WILL NOT be available in release mode.
*/

#define LOG_BUFFER_SIZE 	1024

#define LOG_MASK_MOD_ID		0x00000001
#define LOG_MASK_CAT_ID		0x00000002
#define LOG_MASK_CLASS_ID	0x00000004

#define LOG_MASK_ALL		0xFFFFFFFF

#define MAX_LOG_MSG_LEN		1024

#define LOG_INFRA_NW_PATH	"/mnt/userdata/gnss/log_sock_file"
#define LOG_3GPP_FILENAME	"/mnt/userdata/gnss/3gpp_log.txt"
#define LOG_CMCC_FILENAME	"/mnt/userdata/gnss/cmcc_log.txt"

#define LOGD ALOGD

struct log_mask_ids{

	unsigned int log_mask_mod_id;
	unsigned int log_mask_cat_id;
	unsigned int log_mask_cls_id;
};

enum class_id{
	class_internal,
	log_3gpp,
	oper_sess
};

enum mod_id {
	d2h,
	h2d,
	seq,
	api,
	rpc,
	os,
	ai2,
	comm,
	nvs,
	supl_client,
	sensor
};

enum log_cat {
	crit,
	err,
	warn,
	info,
	debug,
	dump
};

struct log_msg{
	unsigned char log_class;
	unsigned char log_module;
	unsigned char log_category;
	unsigned short log_msg_len;
	char  log_msg[MAX_LOG_MSG_LEN];
};

void get_time_usec(unsigned int *secs, unsigned int * usec);

void logd_print(enum class_id cl_id, enum mod_id mod_id, enum log_cat log_cat,
							const char *fmt, ...);

void logprint(enum mod_id mod_id, enum log_cat log_cat, const char *file,
					const int line, const char *fmt, ...);

#define LOGPRINT(mod_id, log_cat, ...) \
	logprint(mod_id, log_cat, __FILE__, __LINE__, __VA_ARGS__);

#define LOGDPRINT(class_id, mod_id, log_cat, ...) \
	logd_print(class_id, mod_id, log_cat,  __VA_ARGS__);

void log_debug_print(enum class_id cl_id, enum mod_id mod_id,
					enum log_cat log_cat, const char *buf);

void log_c_print(enum class_id cl_id, enum mod_id mod_id, enum log_cat log_cat,
			const char *file, const int line, const char *buf);

void update_logmask(unsigned int logging_mask);

int init_log();

int exit_log();


#endif


