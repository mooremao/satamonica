/*
 * gnss_utils.h
 *
 *
 * Copyright (C) {2012} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#ifndef _GNSS_UTILS_H_
#define _GNSS_UTILS_H_


#define MAX_SECS_IN_A_MIN     	(60)
#define MAX_SECS_IN_1HOUR     	(60   * MAX_SECS_IN_A_MIN)
#define MAX_SECS_IN_A_DAY     	(24   * MAX_SECS_IN_1HOUR)
#define MAX_SECS_IN_1WEEK     	(7    * MAX_SECS_IN_A_DAY)
#define MAX_SECS_IN_365DY     	(365  * MAX_SECS_IN_A_DAY)
#define MAX_SECS_4Y_BLOCK     	(1461 * MAX_SECS_IN_A_DAY)

#define MAX_MINS_IN_1HOUR	  		(60)
#define MAX_MINS_IN_A_DAY	  		(24   * MAX_MINS_IN_1HOUR)
#define MAX_MINS_IN_GLO2UTC_DIFF 	(3	  * MAX_MINS_IN_1HOUR)


// Calc UTC as on 01 Jan 1996; note that UTC started on 01 Jan 1970 
#define UTC_Y1996_M01_D01                                               \
        (                                                               \
         365 * MAX_SECS_IN_A_DAY + /* Y1970 */                          \
         365 * MAX_SECS_IN_A_DAY + /* Y1971 */                          \
         6   * MAX_SECS_4Y_BLOCK   /* 6 block of 4Y, i.e. 1972-1996 */  \
         )

#define UTC_AT_GLO_ORIGIN   /* i.e. start of 01 Jan 1996 */             \
	(                                                               \
         365 * MAX_SECS_IN_A_DAY + /* Y1970 */                          \
         365 * MAX_SECS_IN_A_DAY + /* Y1971 */                          \
         6   * MAX_SECS_4Y_BLOCK   /* 6 block of 4Y, i.e. 1972-1996 */  \
         )

#define UTC_AT_GPS_ORIGIN   /* i.e. start of 06 Jan 1980 */             \
	(                                                               \
         365 * MAX_SECS_IN_A_DAY + /* Y1970 */                          \
         365 * MAX_SECS_IN_A_DAY + /* Y1971 */                          \
         2   * MAX_SECS_4Y_BLOCK + /* 6 block of 4Y, i.e. 1972-1980 */  \
         5   * MAX_SECS_IN_A_DAY   /* 1 to 5 Jan, 1980 */               \
         )

#define GLO2UTC_OFST_SECS                                               \
        (                                                               \
         3 * MAX_SECS_IN_1HOUR /* GLO Time is 3 Hrs ahead of UTC */     \
         )

#define DO_GLO_NT_SECS(NT) ((NT - 1) * MAX_SECS_IN_A_DAY)

#define DO_GLO_Tk_SECS(Tk)                                   \
	(                                                    \
         (((Tk >> 7) & 0x1F) * MAX_SECS_IN_1HOUR) +  /* HH */   \
         (((Tk >> 1) & 0x3F) * 60)                +  /* MM */   \
         (((Tk << 0) & 0x01) ? 30 : 0)               /* SS */   \
         )
	
#define DO_GLO_N4_SECS(N4)          (N4 - 1)*MAX_SECS_4Y_BLOCK
#define DO_GLO_NA_SECS(NA)          (NA - 1)*MAX_SECS_IN_A_DAY

/* Local time at GLO origin (i.e. 03.00.00 hrs UTC on 01 Jan 1996) */ 
#define GLO_START_RU_TIME (GLO2UTC_OFST_SECS + UTC_AT_GLO_ORIGIN)

static inline unsigned int calc_min_secs(unsigned int min)
{
	return min * MAX_SECS_IN_A_MIN;
}

static inline unsigned int calc_hour_secs(unsigned int hrs)
{
	return hrs * MAX_SECS_IN_1HOUR;
}


static inline unsigned int calc_week_secs(unsigned int week)
{
	return week * MAX_SECS_IN_1WEEK;
}

// Move to C file 
static inline unsigned int get_week_secs(unsigned int week)
{
	return calc_week_secs(week);
}

// Move to C file 
static inline unsigned int wk2secs(unsigned int week)
{
	return calc_week_secs(week);
}

static inline unsigned int calc_day_secs(unsigned int day)
{
	return day * MAX_SECS_IN_A_DAY;
}

// Move to C file 
static inline unsigned int get_day_secs(unsigned int day)
{
	return calc_day_secs(day);
}

// Move to C file 
static inline unsigned int day2secs(unsigned int day)
{
	return calc_day_secs(day);
}

static inline unsigned int calc_year_secs(unsigned int yrs)
{
	return yrs * MAX_SECS_IN_365DY;
}

static inline unsigned int calc_4year_secs(unsigned int N4_year)
{
	return N4_year * MAX_SECS_4Y_BLOCK;
}

// Move to C file 
static inline unsigned int get_gps_eph_secs(unsigned char week, unsigned char toe)
{
	return calc_week_secs(week) + toe*16;
}

// Move to C file 
static inline unsigned int get_glo_eph_secs_N4(unsigned char N4, unsigned short NT, 
                                               unsigned short Tk)
{
	return  calc_4year_secs(N4 - 1)          + /* 4Y */
		calc_day_secs(NT - 1)            + /* DD */
		calc_hour_secs((Tk >> 7) & 0x1F) + /* HH */                     
		calc_min_secs((Tk >> 1) & 0x3F)  + /* MM */ 
		((Tk << 0) & 0x01)? 30 : 0;        /* SS */
}

// Move to C file 
static 
inline unsigned int get_glo_eph_secs_NT(unsigned short NT, unsigned short Tk)
{
	return  calc_year_secs(NT - 1)           + /* 4Y */
		calc_hour_secs((Tk >> 7) & 0x1F) + /* HH */                     
		calc_min_secs((Tk >> 1) & 0x3F)  + /* MM */ 
		((Tk << 0) & 0x01)? 30 : 0;        /* SS */
}

// Move to C file 
static 
inline unsigned int get_glo_alm_secs_N4(unsigned char N4, unsigned short NA)
{
	return calc_4year_secs(N4 - 1) + calc_day_secs(NA - 1);
} 

static inline unsigned int glo2utc_time(unsigned int glo_now)
{
	return glo_now - GLO2UTC_OFST_SECS;;
}

static inline unsigned int utc2glo_time(unsigned int utc_now)
{
	return utc_now + GLO2UTC_OFST_SECS;
}

struct MDS1Y_offset {

	unsigned char   Mn; /* 0 based, 0 to 11    */ 
	unsigned short  Dn; /* 0 based, 0 to 30    */
	unsigned int    ss; /* 0 based, 0 to 86399 */
};

struct YMDS_offset {

	unsigned char   Yn;   /* 0 based, 0O to 99    */
	unsigned char   Mn;   /* 0 based, 0  to 11    */
	unsigned short  Dn;   /* 0 based, 0  to 30    */
	unsigned int    ss;   /* 0 based, 0  to 86399 */
};

struct week_offset {

	unsigned short Wn;  /* 1 Based */
	unsigned int   ss;  /* 0 Based */
};

struct MDS1Y_offset secs2MDS1Y(unsigned int secs, unsigned char leap_yr);

unsigned char days2months_1Y(unsigned short days, unsigned char leap_yr);

unsigned short months2days_1Y(unsigned char months, unsigned char leap_yr);

struct YMDS_offset secs2YMDS(unsigned int secs);

unsigned int YMDS2secs(struct YMDS_offset ymds);

struct week_offset secs2week(unsigned int secs);

unsigned int gps_time_now(unsigned short nr_week, unsigned int wk_ro, unsigned int wk_secs);

unsigned int glo_time_now(unsigned short day, unsigned int tod);

unsigned char get_glo_N4_param(unsigned int glo_now);

unsigned char utc2glo_N4_param(unsigned int utc_now);

unsigned short utc2glo_NT_param(unsigned int utc_now);

unsigned short get_glo_NT_param(unsigned int glo_now);

unsigned short utc2glo_tk_param(unsigned int utc_now);

unsigned short get_glo_tk_param(unsigned int glo_now);



#endif
