/*
 * utils.h
 *
 * Utilities
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef __UTILS_H__
#define __UTILS_H__

#define OK 0
#define ERR -1

/* Defines. */
#define C_PI	3.1415926535898
#define C_90_OVER_2_23    (double)((double)90.0/(double)8388608.0)
#define C_360_OVER_2_24   (double)((double)360.0/(double)16777216.0)
#define C_RAD_TO_DEG      (180.0 / C_PI)
#define C_DEG_TO_RAD      (C_PI / 180.0)
#define C_LSB_LAT_REP	  (65536.0 * 65536.0 / C_PI)
#define C_LSB_LON_REP	  (65536.0 * 32768.0 / C_PI)
#define C_LSB_HT_REP	  2
#define C_RECIP_LSB_POS_SIGMA 	10.0

#define WEEK			604800
#define HALFWEEK		302400
#define OMEGA_DOT_E		7.2921151467E-5
#define MU			3.986005E14

#define WGS84_A		6378137.000		/* Semi-major axis (m). (to equator)*/
#define WGS84_B		6356752.314		/* Semi-minor axis (m). (to poles)  */

#define  GPS_WEEK_UNKNOWN  0xFFFF


/* Earth's obliquity and flatness terms. */
#define WGS84_F		(WGS84_A - WGS84_B) / WGS84_A
#define WGS84_1MF	(1.0 - WGS84_F)
#define WGS84_R2	((WGS84_B * WGS84_B) / (WGS84_A * WGS84_A))
#define WGS84_E2	(1.0 - (WGS84_B * WGS84_B) / (WGS84_A * WGS84_A))

#define		TWO_TO_4		16.0
#define		TWO_TO_5		32.0
#define		TWO_TO_10		1024.0
#define		TWO_TO_11		2048.0
#define		TWO_TO_12		4096.0
#define		TWO_TO_14		16384.0
#define		TWO_TO_16		65536.0
#define		TWO_TO_19		524288.0
#define		TWO_TO_20		1048576.0
#define		TWO_TO_21		2097152.0
#define		TWO_TO_23		8388608.0
#define		TWO_TO_24		16777216.0
#define		TWO_TO_27		134217728.0
#define		TWO_TO_29		536870912.0
#define		TWO_TO_30		1073741824.0
#define		TWO_TO_31		2147483648.0
#define		TWO_TO_33		8589934592.0
#define		TWO_TO_38		274877906944.0
#define		TWO_TO_41		2199023255552.0
#define		TWO_TO_43		8796093022208.0
#define		TWO_TO_50		1.12589990684E+15
#define		TWO_TO_55		3.602879701896E+16

#define GPS_WEEK_UNKNOWN 	0xFFFF
#define WEEK_HRS 		(7U*24U)
#define WEEK_SECS		(WEEK_HRS*3600U)
#define WEEK_MSECS		(WEEK_SECS*1000U)
#define WEEK_ZCOUNTS		(WEEK_SECS/6U)

#define C_N_PARITY  26  /* Number of bits that affect parity */

#define SV_DTIME_STATUS_SM_VALID (1<<0)
#define SV_DTIME_STATUS_SB_VALID (1<<1)
#define SV_DTIME_STATUS_MS_VALID (1<<2)

/* Generate an offset for a given structure & typecast result for assignment */
#define BUF_OFFSET_ST(buf, offset_st_name, ptr_st_name)                    \
	(ptr_st_name *)((char*)buf + sizeof(offset_st_name))

/* Structure pointer */
#define ST_PTR(buf, st_name) (struct st_name*)(buf)

#define ST_SIZE(st_name) sizeof(struct st_name)

//#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define UNUSED(x) ((void)(&x))

#define OBJ_PTR(x) x[1]

/*
   container_of - cast a member of a structure out to the containing structure
   @ptr:    the pointer to the member.
   @type:   the type of the container struct this is embedded in.
   @member: the name of the member within the struct.
 */
#define container_of(ptr, type, member) ({                                 \
		typeof( ((type *)0)->member ) *__mptr = (ptr);                     \
		(type *)( (char *)__mptr - offsetof(type,member) );})

#endif /* __UTILS_H__ */
