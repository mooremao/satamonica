/*
 * gnss.h
 *
 * GNSS Data structures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
*/

#ifndef _GNSS_H_
#define _GNSS_H_

#ifdef __cplusplus
	extern "C" {
#endif 
#define MAX_GANSS  8
#ifdef ANDROID
#define MAX_SAT    32
#else
#define MAX_SAT    32

#endif
#define MAX_SIG    1


/*----------------------------------------------------------------------------*/
/**  GPS specific structures                                                  */
/*----------------------------------------------------------------------------*/

/** GPS Almanac Model. Refer to Table 20-VI of ICD-GPS-200 and section 10.3.7.89 
    of 3GPP specification 25.33.1
*/
struct gps_alm {

	unsigned char  wna;              /**< RRC:  8 bits and RRLP:  8 bits */

	unsigned char  dataId ;          /**< RRC: 2 bits and RRLP: NA*/
	unsigned char  svid;             /**< RRC: 6 bits and RRLP: 6 bits */

	unsigned short E;                /**< RRC: 16 bits and RRLP: 16 bits */
	unsigned char  Toa;              /**< RRC:  8 bits and RRLP:  8 bits */
	short          DeltaI;           /**< RRC: 16 bits and RRLP: 16 bits */
	short          OmegaDot;         /**< RRC: 16 bits and RRLP: 16 bits */
	unsigned char  Health;           /**< RRC:  8 bits and RRLP:  8 bits */
	unsigned int   SqrtA;            /**< RRC: 24 bits and RRLP: 24 bits */
	int            OmegaZero;        /**< RRC: 24 bits and RRLP: 24 bits */
	int            Mzero;            /**< RRC: 24 bits and RRLP: 24 bits */
	int            Omega;            /**< RRC: 24 bits and RRLP: 24 bits */
	short          Af0;              /**< RRC: 11 bits and RRLP: 11 bits */
	short          Af1;              /**< RRC: 11 bits and RRLP: 11 bits */
};

/** GPS Ephemeris Model. Refer to Table 20-III of ICD-GPS-200, section 10.3.7.94
    and 10.3.7.91a of 3GPP specification 25.331
*/
enum gps_eph_sat_status {

        NS_NN  = 0x0,   /* New Satellite,       New Navigation */
        ES_SN  = 0x1,   /* Existing Satellite, Same Navigation */
        ES_NN  = 0x2,   /* Existing Satellite,  New Navigation */
        RESVD  = 0x3    /* Reserved */
};

struct gps_eph {

	unsigned char  svid;             /**< RRC:  6 bits and RRLP:  6 bits */
	unsigned char  is_bcast;   /* Is it broadcast or predicted? */
        
        enum gps_eph_sat_status status;  /**< RRC:  2 bits and RRLP:  2 bits */

	unsigned char  Code_on_L2;       /**< RRC:  2 bits and RRLP:  2 bits */
	unsigned char  Ura;              /**< RRC:  4 bits and RRLP:  4 bits */
	unsigned char  Health;           /**< RRC:  6 bits and RRLP:  6 bits */
	unsigned short IODC;             /**< RRC: 10 bits and RRLP: 10 bits */

	/* RRC: Reserved 88 bits and RRLP: Reserved 88 bits */

	unsigned char  Tgd;              /**< RRC:  8 bits and RRLP:  8 bits */
	unsigned short Toc;              /**< RRC: 16 bits and RRLP: 16 bits */
	unsigned char  Af2;              /**< RRC:  8 bits and RRLP:  8 bits */
	unsigned short Af1;              /**< RRC: 16 bits and RRLP: 16 bits */
	unsigned int   Af0;              /**< RRC: 22 bits and RRLP: 22 bits */
	short          Crs;              /**< RRC: 16 bits and RRLP: 16 bits */
	short          DeltaN;           /**< RRC: 16 bits and RRLP: 16 bits */
	int            Mo;               /**< RRC: 32 bits and RRLP: 32 bits */
	short          Cuc;              /**< RRC: 16 bits and RRLP: 16 bits */
	unsigned int   E;                /**< RRC: 32 bits and RRLP: 32 bits */
	short          Cus;              /**< RRC: 16 bits and RRLP: 16 bits */
	unsigned int   SqrtA;            /**< RRC: 32 bits and RRLP: 32 bits */
	unsigned short Toe;              /**< RRC: 16 bits and RRLP: 16 bits */

	unsigned char  FitIntervalFlag;  /**< RRC:  1 bits and RRLP:  1 bits */
	unsigned char  Aodo;             /**< RRC:  5 bits and RRLP: NA bits */
	short          Cic;              /**< RRC: 16 bits and RRLP: 16 bits */
	int            Omega0;           /**< RRC: 32 bits and RRLP: 32 bits */
	short          Cis;              /**< RRC: 16 bits and RRLP: 16 bits */
	int            Io;               /**< RRC: 32 bits and RRLP: 32 bits */
	short          Crc;              /**< RRC: 16 bits and RRLP: 16 bits */
	int            Omega;            /**< RRC: 32 bits and RRLP: 32 bits */
	int            OmegaDot;         /**< RRC: 24 bits and RRLP: 24 bits */
	short          Idot;             /**< RRC: 14 bits and RRLP: 14 bits */
};

/** GPS Ionospheric Model. Refer to Table 20-X of ICD-GPS-200 and section 10.3.7.92 
    of 3GPP specfication 25.33.1
*/
struct gps_ion {

	char  Alpha0;                    /**< RRC: 8 bits and RRLP: 8 bits */
	char  Alpha1;                    /**< RRC: 8 bits and RRLP: 8 bits */
	char  Alpha2;                    /**< RRC: 8 bits and RRLP: 8 bits */
	char  Alpha3;                    /**< RRC: 8 bits and RRLP: 8 bits */
	char  Beta0;                     /**< RRC: 8 bits and RRLP: 8 bits */
	char  Beta1;                     /**< RRC: 8 bits and RRLP: 8 bits */
	char  Beta2;                     /**< RRC: 8 bits and RRLP: 8 bits */
	char  Beta3;                     /**< RRC: 8 bits and RRLP: 8 bits */
};

/** GPS UTC Model. Refer to Table 20-III of ICD-GPS-200 and section 10.3.7.97 of 
    3GPP specfication 25.33.1
*/
struct gps_utc {

	int            A1;               /**< RRC: 24 bits and RRLP: 24 bits */
	int            A0;               /**< RRC: 32 bits and RRLP: 32 bits */
	unsigned char  Tot;              /**< RRC:  8 bits and RRLP:  8 bits */
	unsigned char  WNt;              /**< RRC:  8 bits and RRLP:  8 bits */
	char           DeltaTls;         /**< RRC:  8 bits and RRLP:  8 bits */
	unsigned char  WNlsf;            /**< RRC:  8 bits and RRLP:  8 bits */
	unsigned char  DN;               /**< RRC:  8 bits and RRLP:  8 bits */
	char           DeltaTlsf;        /**< RRC:  8 bits and RRLP:  8 bits */
};

/** GPS Time-of-Week assist. Refer to section 10.3.7.96 of 3GPP specfication
    25.331 and section A.4.2.4 of 3GPP specification 44.031
 */
struct gps_atow {

	unsigned char  svid;             /**< RRC:  6 bits and RRLP:  6 bits */
	unsigned short tlm_word;         /**< RRC: 14 bits and RRLP: 14 bits */
	unsigned char  tlm_rsvd;         /**< RRC:  2 bits and RRLP:  2 bits */
	unsigned char  alert_flag;       /**< RRC:  1 bits and RRLP:  1 bits */
	unsigned char  spoof_flag;       /**< RRC:  1 bits and RRLP:  1 bits */
};

enum tow_scale {

        sec_8by100,
        milli_secs,
        clear_secs
};

/** GPS Time Information */
struct gps_time {

        unsigned char  n_wk_ro;          /**< RRC: 3 bits and RRLP: NA */
        unsigned short week_nr;          /**< RRC: 10 bits, week number: GPS week 0 
                                            began at the beginning of the GPS Time 
                                            Scale (1/5/1980 to 1/6/1980 midnight)
                                            and RRLP: 10 bits
                                         */

        enum tow_scale tow_unit;
        unsigned int   tow;              /**< RRC: 30 bits, ToW: 
                                            range 0-604799999, 
                                            30-bit presentation, 
                                            no. of milli-seconds since beginning of 
                                            week i.e. Sunday
                                            and RRLP: 23 bits
                                         */
   	  	float 	    freq_unc;
};

/** Quality of time double check */
enum time_qual { 
        
	coarse,                          /**< 2 to 3 second approximation */
	fine                             /**< usec to msec  approximation */
};

/** GPS Reference Time. Refer to section 10.3.7.96 of 3GPP specification 25.331
    and section A.4.2.4 of 3GPP specification 44.031
*/
struct gps_ref_time {

        struct gps_time   time;
        enum time_qual    accuracy;
        //unsigned int      time_unc;
        float      time_unc;
	  	float 	    freq_unc;
        unsigned char     n_atow;         /**< RRC:  4 bits and RRLP:  4 bits */
        struct gps_atow   atow[MAX_SAT];  /**< GPS TOW assist */
};

/** 
    GPS Real-Time-Integrity. Refer to section 10.3.7.95 of 3GPP specification
    25.331
*/
struct gps_rti {
        
	unsigned char     bad_svid;      /**< RRC:  6 bits and RRLP:  6 bits */
};

/** 
    DGPS Corrections. Refer to section 10.3.7.91 of 3GPP specifications 25.331
*/

struct dgps_sat {
        
        enum tow_scale tow_unit;
        unsigned int   gps_tow;          /**< RRC: 20 bits and RRLP: 20 bits */
        unsigned char  health;           /**< RRC:  3 bits and RRLP:  3 bits */

        unsigned char  svid;             /**< RRC:  6 bits and RRLP:  6 bits */
        unsigned short iod;              /**< RRC:  8 bits and RRLP:  8 bits */
        unsigned char  udre;             /**< RRC:  2 bits and RRLP:  2 bits */
        short          prc;              /**< RRC: 12 bits and RRLP: 12 bits */
        char           rrc;              /**< RRC:  8 bits and RRLP:  8 bits */

        char           dprc2;            /**< RRC:  8 bits (If not used set it 0) and RRLP:  8 bits */
        char           drrc2;            /**< RRC:  4 bits (If not used set it 0) and RRLP:  4 bits */
        char           dprc3;            /**< RRC:  8 bits (If not used set it 0) and RRLP:  8 bits */
        char           drrc3;            /**< RRC:  4 bits (If not used set it 0) and RRLP:  4 bits */
};

/** GPS Acquisition Assistance Data. Refer to section 10.3.7.88 of 3GPP
    specification 25.331 and A.24 / A.25 of 3GPP specification 44.031
*/
#define GPS_ACQ_FLAG_NEW                0x0001  /** New Acq Assist */
#define GPS_ACQ_FLAG_OLD                0x0002  /** Buffered Acq Assist */
#define GPS_ACQ_FLAG_INV                0xFFFF  /** Invalid. Ignore flag info */
struct gps_acq {
        
        enum tow_scale tow_unit;
        unsigned int   gps_tow;      /**< RRC: 30 bits and RRLP: 23 bits */
                
        unsigned char  svid;         /**< RRC:  6 bits and RRLP:  6 bits */
        short          Doppler0;     /**< RRC: 12 bits and RRLP: 12 bits */
        unsigned char  Doppler1;     /**< RRC:  6 bits and RRLP:  6 bits */
        unsigned char  DopplerUnc;   /**< RRC:  3 bits and RRLP:  3 bits */
        unsigned short CodePhase;    /**< RRC: 10 bits and RRLP: 10 bits */
        unsigned char  IntCodePhase; /**< RRC:  5 bits and RRLP:  5 bits */
        unsigned char  GPSBitNum;    /**< RRC:  2 bits and RRLP:  2 bits */
        unsigned char  SrchWin;      /**< RRC:  4 bits and RRLP:  4 bits */
        unsigned char  Azimuth;      /**< RRC:  5 bits and RRLP:  5 bits */
        unsigned char  Elevation;    /**< RRC:  3 bits and RRLP:  3 bits */

	unsigned short use_flag;     /** GPS_ACQ_FLAG_* to indicate freshness */
};

/*----------------------------------------------------------------------------*/
/*  GLONASS specific structures                                               */
/*----------------------------------------------------------------------------*/


/** GLONASS Almanac Model 5 using Keplerian Parameters. Refer to section 
    10.3.7.89a of 3GPP specification 25.331 and table A.54 of 3GPP 
    specificaiton 44.031
 */
struct glo_alm_kp_set {

        unsigned char  wna;

        unsigned char  is_toa;/**< Is there a valid TOA?                       */
        unsigned char  toa;   /**< Optional in 44.031 & not included in 25.331 */
        unsigned char  ioda;  /**< Optional in 44.031 & not included in 25.331 */
        
        unsigned short NA;
        unsigned char  nA;
        unsigned char  HnA;
        int            Lambda_nA;
        unsigned int   t_Lambda_nA;
        int            Delta_i_nA;
        int            Delta_T_nA;
        char           Delta_T_dot_nA;
        unsigned short Epsilon_nA;
        short          Omega_nA;
        short          Tau_nA;
        unsigned char  C_nA;
        unsigned char  M_nA;
};


/** GLONASS Additonal UTC Model 3. Refer to section 10.3.7.97d of 3GPP
    specification 25.331 and Table A.55.17 of 3GPP specification 44.031
*/
struct glo_utc_model {

        unsigned short NA;
        int            TauC;
        short          B1;
        short          B2;
        
        unsigned char  KP;
};

/* Following is Clock Model 4. Refer to section 10.3.7.91f of 3GPP specification
   25.331 and table A.49.1 of 3GPP specification 44.031 
*/
struct glo_clk_model {

	int   Tau;
	short Gamma;
	char  Delta_Tau;
};

/* Following is Orbit Model 4 using Earth Centric Earth Fixed Parameters. 
   Refer to section 10.3.7.91e of 3GPP specification 25.331 and table A.49.2 of 
   3GPP specification 44.031.
*/
struct glo_orb_model {

	unsigned char  En;
	unsigned char  P1;
	unsigned char  P2;
	unsigned char  M;
	int            X;
	int            X_dot;
	char           X_dot_dot;
	int            Y;
	int            Y_dot;
	char           Y_dot_dot;
	int            Z;
	int            Z_dot;
	char           Z_dot_dot;
};

/** GLONASS (Ephemeris) Navigation Model. Refer to section 10.3.7.94b of 3GPP 
    specification 25.331 and table A.46 of 3GPP specification 44.031.

    Macro definitions are based on statements available in Note 1 & 2 of section
    10.3.7.94b of 3GPP specification 25.331 and table A.48.1 and A.48.2 of 3GPP
    specification 44.031. Bit selection are based on ASN.1 BIT STRING definition.
*/
#define GANSS2GLO_EPH_FT(sv_health) (sv_health & 0x1E) /* Bit#  4 - 1 */
#define GANSS2GLO_EPH_Bn(sv_health) (sv_health & 0x20) /* Bit#  5(MSB)*/
#define GANSS2GLO_EPH_tb(iod)       (iod       & 0x7F) /* Bit#  0 - 6 */

struct glo_nav_model {

	unsigned char        the_svid;
        unsigned char        is_bcast;

        unsigned char        Bn;  /**< From SV health of GANSS Nav Model */
        unsigned char        FT;  /**< From SV health of GANSS Nav Model */
 
        unsigned char        tb;  /**< From IOD of GANSS Nav Model       */

	struct glo_clk_model clock;
	struct glo_orb_model orbit;
};


/** 
    GLONASS Ephemeris (Native). Refer to table 4.5 of GLONASS ICD version 5.1 
    - 2008
*/
struct glo_eph {

        unsigned char  the_svid;
        unsigned char  is_bcast;   /* Is it broadcast or predicted? */

	unsigned char  m;
	unsigned short tk;
	unsigned char  tb;          /* From IOD of ganns_nav_model  */ 
	unsigned char  M;           /* From glo_orb_model */ 
	short          Gamma;       /* From glo_clk_model */
	int            Tau;         /* From glo_clk_model */

	int            Xc;          /* From glo_orb_model */ 
	int            Yc;          /* From glo_orb_model */ 
	int            Zc;          /* From glo_orb_model */ 
	int            Xv;          /* From glo_orb_model */ 
	int            Yv;          /* From glo_orb_model */ 
	int            Zv;          /* From glo_orb_model */ 
	char           Xa;          /* From glo_orb_model */ 
	char           Ya;          /* From glo_orb_model */ 
	char           Za;          /* From glo_orb_model */ 

	unsigned char  P;
	unsigned short NT;
	unsigned char  n;

	unsigned char  FT;          /* From SV Health param of ganss_nav_model */

	unsigned char  En;          /* From glo_orb_model */ 
	unsigned char  Bn;          /* From SV Health param of ganss_nav_model */


	unsigned char  P1;          /* From glo_orb_model */ 
	unsigned char  P2;	    /* From glo_orb_model */ 	
	unsigned char  P3;
	unsigned char  P4;

	char           Delta_Tau;   /* From glo_clk_model */
	unsigned char  ln;

};

/** 
    GLONASS Almanac (native). Refer to table 4.9 of GLONASS ICD version 5.1 
    - 2008
*/
struct glo_alm {

	int            TauC;                  /* From glo_utc_model  */

	int            TauGPS;

	unsigned char  N4;

	unsigned short NA;                    /* From glo_alm_kp_set */

	unsigned char  nA;                    /* From glo_alm_kp_set */  
	unsigned char  HnA;                   /* From glo_alm_kp_set */  
	int            Lambda_nA;             /* From glo_alm_kp_set */    
	unsigned int   t_Lambda_nA;           /* From glo_alm_kp_set */  
	int            Delta_i_nA;            /* From glo_alm_kp_set */  
	int            Delta_T_nA;            /* From glo_alm_kp_set */  
	char           Delta_T_dot_nA;        /* From glo_alm_kp_set */  
	unsigned short Epsilon_nA;            /* From glo_alm_kp_set */  
	short          Omega_nA;              /* From glo_alm_kp_set */  
	unsigned char  M_nA;                  /* From glo_alm_kp_set */  
	short          B1;                    /* From glo_utc_model  */
	short          B2;                    /* From glo_utc_model  */
	unsigned char  KP;                    /* From glo_utc_model  */
	short          Tau_nA;                /* From glo_alm_kp_set */  
	unsigned char  C_nA;                  /* From glo_alm_kp_set */  

};

/*----------------------------------------------------------------------------*/
/*  GANSS specific structures                                                 */
/*----------------------------------------------------------------------------*/

/** GANSS Ionospheric Model. Refer to section 10.3.7.92a of 3GPP specification
    25.331 and table A.35 of 3GPP specification 44.031
*/
struct ganss_ion_model {

	unsigned short a0;
	unsigned short a1;
	unsigned short a2;
};

/** GANSS Additonal Ionospheric Model. Refer to section 10.3.7.92b of 3GPP
    specification 25.331 and table A.35.a of 3GPP specification of 44.031
*/
struct ganss_addl_iono {

	unsigned char    data_id;

	unsigned char    alfa0;
	unsigned char    alfa1;
	unsigned char    alfa2;
	unsigned char    alfa3;
	unsigned char    beta0;
	unsigned char    beta1;
	unsigned char    beta2;
	unsigned char    beta3;
};

/** GANSS Reference Measurement. Refer to section 10.3.7.88b of 3GPP
    specification 25.331 and table A.52 of 3GPP specification 44.031
*/
struct ganss_ref_msr {
        
        unsigned char  svid;
        unsigned short doppler0;
        char           doppler1;
        unsigned char  doppler_unc;
        unsigned short code_phase;
        unsigned char  int_code_phase;
        unsigned char  cp_search_win;
        unsigned char  azimuth;
        unsigned char  elevation;
};

/** GANSS Time model. Refer to section 10.3.7.97a of 3GPP specification 25.331
    and table A.41 of 3GPP specification 44.031
*/
struct ganss_time_model {

	unsigned char  ganss_id;    /* To be removed */
	unsigned short ref_time16s; /**< In seconds */

	int            t_A0;
	int            t_A1;
	char           t_A2;

	unsigned char  gnss_TO_id;

	unsigned short ref_week;
};



enum gnss_time_id { 

	gll_time_id  =  8, /* Default */
	gps_time_id  =  0,
	qzss_time_id =  1,
	glo_time_id  =  2
};

/** GANSS Reference Time. Refer to section 10.3.7.96o of 3GPP specification 
    25.331 and table A.33 of GPP specification 44.031
 */
struct ganss_ref_time {

	enum gnss_time_id    tid;      /* Time ID */
	unsigned short       day;
	unsigned int         tod;
	unsigned char        tod_unc;
	enum time_qual       accuracy;
};

/** GLONASS Real-Time-Integrity. Refer to section 10.3.7.95b of 3GPP specification
    25.331 and table A.50 of 3GPP specification 44.031
*/
struct ganss_rti {

        unsigned char      ganss_id;  /* Aux */

	unsigned char      bad_svid;  /**< Id of bad Satellite */
	unsigned char      sig_bits;  /**< Bit Map of bad sigs */
};

/** DGANSS Corrections. Refer to section 10.3.7.91b of 3GPP specifications 
    25.331 and table and A.43 of 3GPP specification 44.031
 */
struct dganss_sat {
        
        unsigned char  ganss_id;         /* Aux */
        
        unsigned char  ref_time;         /* GANSS specific */
        unsigned char  health;           /**< RRC:  3 bits and RRLP:  3 bits */
        
        unsigned char  svid;             /**< RRC:  6 bits and RRLP:  6 bits */
        unsigned short iod;              /**< RRC:  8 bits and RRLP:  8 bits */
        unsigned char  udre;             /**< RRC:  2 bits and RRLP:  2 bits */
        short          prc;              /**< RRC: 12 bits and RRLP: 12 bits */
        char           rrc;              /**< RRC:  8 bits and RRLP:  8 bits */
        

};

/*----------------------------------------------
 * GANSS Measurement Information - Response
 *--------------------------------------------*/
enum mpath_ind {

	not_measured,
	low,
	medium,
	high
};

/* GPS Measurement parameters. Refer to section 10.3.7.93 of 3GPP 
   specification 25.331 and table A.8 of 3GPP specification 44.031*/
struct gps_msr_info
{
	unsigned char   svid;         /**< RRC:  6 bits and RRLP:  6 bits */
	unsigned char   c_no;         /**< RRC:  6 bits and RRLP:  6 bits */
	float           doppler;      /**< RRC: 16 bits and RRLP: 16 bits */
	/*SRK Added for DR intr*/
	double pseudo_msr; /*Pseudo Range Measurements */
	float pseudo_msr_unc; /*Pseudo Range Measurements uncertainity*/
	float doppler_unc; /*Pseudo Range Measurements uncertainity*/	
	unsigned char msr_qual; /*Pseudo Range Measurements uncertainity*/		
	unsigned char snr;
	unsigned short meas_flags;
	unsigned int TCount;
	float pseudo_range_residuals; /*Pseudo range residuals*/
	float doppler_residuals;/* Doppler residuals  */

	unsigned short  azimuth_deg;     /* In degrees       */
	short           elevate_deg;     /* In degrees       */

};

/* GANSS Measurement parameters. Refer to section 10.3.7.93a of 3GPP
   specification 25.331 and table A.10.13 of 3GPP specification 44.031
*/
struct ganss_msr_info {

        unsigned char   contents;   /* TBD */

	unsigned char   svid;
	unsigned char   c_no;
	/*SRK Added for DR intr*/
	double pseudo_msr; /*Pseudo Range Measurements */
	float pseudo_msr_unc; /*Pseudo Range Measurements uncertainity*/
	float doppler_unc; /*Pseudo Range Measurements uncertainity*/	
	unsigned char msr_qual; /*Pseudo Range Measurements uncertainity*/		
	unsigned char snr;
	unsigned short meas_flags;
	float           doppler;      /**< RRC: 16 bits and RRLP: 16 bits */
	float pseudo_range_residuals;
	float doppler_residuals;

	unsigned short  azimuth_deg;     /* In degrees       */
	short           elevate_deg;     /* In degrees       */
};

/* Measured parameters for GLONASS */
struct glo_msr_info {
        struct ganss_msr_info msr;
        /* Do not define new members at this time */
};

#define GLO_MSR_PTR(ptr) ((struct ganss_msr_info*) ptr)
#define GLO_MSR(st_var)  (st_var.msr)

/**
   GANSS Signal Measurement. Refer to section 10.3.7.93a of 3GPP
   specification 25.331 and table A.10.12 of 3GPP specification 44.031
*/

struct ganss_sig_info {

        unsigned char       ganss_id; /* Aux */

        unsigned char       signal_id;
        unsigned char       contents;
        unsigned char       ambiguity;

        unsigned char           n_sat;
        struct ganss_msr_info   sats[MAX_SAT];
};

/* Signal detected for GLONASS */
struct glo_sig_info {
        struct ganss_sig_info signal;
        /* Do not define new members at this time */
};

#define GLO_SIG_PTR(ptr) ((struct ganss_sig_info*) ptr)
#define GLO_SIG(st_var)  (st_var.signal)


/**
   GANSS Generic MSR Info. Refer to section 10.3.7.93a of 3GPP specification
   25.331 and table A.10.11 of 3GPP specification 44.031
*/
struct ganss_msr_desc {
        
        unsigned char           ganss_id;
        unsigned char           n_signal;
        struct ganss_sig_info   signals[MAX_SIG];
};


/*-------------------------------------------------
 * Position Information - Response
 *-----------------------------------------------*/
enum loc_fix_type {
	fix_none,
	fix_est,
	fix_2d,
	fix_2d_diff,
	fix_3d,
	fix_3d_diff
};

enum location_shape {

	ellipsoid,                      /**< Ellipsoid point                */
	ellipsoid_with_c_unc,           /**< Ellipsoid point w/ circle unc  */
	ellipsoid_with_e_unc = 3,       /**< Ellipsoid point w/ ellipse unc */
	polygon              = 5,
	ellipsoid_with_v_alt = 8,       /**< Ellipsoid point w/ altitude    */
	ellipsoid_with_v_alt_and_e_unc,
	ellipsoid_with_arc
};

/** Ellipsoid point information with altitude and uncertainty ellipsoid. Refer
    to 3GPP specification 23.032
*/

/** GANSS Reference Location, 44.031, table A.31 */
struct location_desc {

	enum location_shape  shape;

	enum latitude_sign {
		north,
		south
	} lat_sign;

	unsigned int  latitude_N;
	int           longitude_N;

        int           lat_deg_by_2p8;/* Latitude  degree; scale factor 1/(2^8)*/
        int           lon_deg_by_2p8;/* Longitude degree; scale factor 1/(2^8)*/

	enum altitude_direction {
		height,
		depth

	} alt_dir;

	unsigned short altitude_N;

	unsigned char  unc_semi_maj_K;
	unsigned char  unc_semi_min_K;
	unsigned char  orientation;

	unsigned char  unc_altitude_K;
	double altitude_MSL;
	float east_unc;
	float north_unc;
	float vert_unc;
	unsigned char  confidence;
};

/** GPS location information. Refer to section 10.3.7.109 of 3GPP specification 
  25.331 and section A.10.1 of 3GPP specification 44.031 
 */
//#define LOC_HAS_GPS_TIME    0x00000002
struct loc_gnss_info {

        unsigned int    TCount;
        enum gnss_time_id      gnss_tid;  /** GNSS reference time ID          */

        unsigned int           ref_secs;  /**< GPS: Week and GLO: Day (secs)  */
        unsigned int           ref_msec;  /**< GPS: ToW  and GLO: ToD (msec)  */

		unsigned int 		utc_secs; /** Current utc sec elapsed since January 1, 1970 */
		unsigned short		utc_msec; /** Mili Seconds elapsed in the currect second */
        unsigned char  utcOffset;
		unsigned short week_num;
		double 	       osc_bias;   
		float 		   osc_freq;		
		
        enum loc_fix_type      fix_type;

	unsigned int           pos_bits;  /** GNSS(es) used in pos estimation */
	unsigned char 			pps_state;
	struct location_desc   location;  /** position estimate               */ 
	unsigned int timeTag_sec;
	unsigned int timetag_usec;
	unsigned short pos_flag;
};

enum vel_type {

	h_vel,         /**< Horizontal velocity                               */
	h_v_vel,       /**< Horizontal with vertical velocity                 */ 
	h_vel_unc,     /**< Horizontal velocity with uncertainty              */
	h_v_vel_unc    /**< Horizontal with vertical velocity and uncertainty */
};

/** GPS & GANSS velocity information. Described in details in section 8 of 3GPP
  specification 23.032. Refer to section 10.3.7.109 of 3GPP specification
  25.331 and section A.2.6.b of 3GPP specification 44.031
 */
struct vel_gnss_info {

	enum vel_type   type;

	float  bearing;
	float  			h_speed;
	unsigned char   ver_dir;
	float		   v_speed;
	unsigned char   h_unc;
	unsigned short v_unc;
	unsigned short est_unc;
	unsigned short nth_unc;
	unsigned short vup_unc;
};

/** Dillution of precision         */
struct dop_gnss_info {

        unsigned char  position;
        unsigned char  horizontal;
        unsigned char  vertical;
        unsigned char  time;
};
struct hdg_gnss_info {

        unsigned short truthful;     /**< True     head, pi/2^15 rad resolution   */
		unsigned short magnetic;     /**< Magnetic head, pi/2^15 rad resolution   */
		long allign_1;
		long allign_2;
		long allign_3;		

};


/** UE GANSS Positioning capabilities. Refer to section 10.3.3.45 of 3GPP
    specification 25.331 and A.8.2.1 of 3GPP specification 44.031
*/

#define UE_LCS_CAP_GNSS_AUTO     0x00000001
#define UE_LCS_CAP_AGNSS_POS     0x00000002
#define UE_LCS_CAP_AGNSS_MSR     0x00000004

/*------------------------
 * GANSS --> GLONASS
 *-----------------------*/
#define UE_LCS_CAP_GLO_AUTO      UE_LCS_CAP_GNSS_AUTO
#define UE_LCS_CAP_AGLO_MSR      UE_LCS_CAP_AGNSS_MSR
#define UE_LCS_CAP_AGLO_POS      UE_LCS_CAP_AGNSS_POS
#define UE_LCS_CAP_GLO_SIG_G1    0x01
#define UE_LCS_CAP_GLO_SIG_G2    0x02
#define UE_LCS_CAP_GLO_SIG_G3    0x04

/*------------------------
 * SBAS for each GANSS
 *-----------------------*/
#define UE_LCS_CAP_SBAS_WAAS     0x01
#define UE_LCS_CAP_SBAS_EGNOS    0x02
#define UE_LCS_CAP_SBAS_MSAS     0x04
#define UE_LCS_CAP_SBAS_GAGAN    0x08

struct ganss_ue_lcs_caps {

        unsigned char            n_ganss;    /**< # of GANSS entries     */

        struct ganss_lcs_caps {
                
                unsigned char    ganss_id;

                unsigned int     caps_bits;  /**< POS / MSR capabilities */
                unsigned char    sigs_bits;  /**< GANSS Signal bits      */
                unsigned char    sbas_bits;  /**< GANSS SBAS bits        */
                
        }                        lcs_caps[MAX_GANSS];

};

/** GANSS Additional Assistance Data Choices. Refer to section A.8.2.2 / 
    table A.61 of 3GPP specification 44.031 and section 10.3.7.88c of 
    3GPP specification 25.331. 
*/
struct ganss_data_model {

        unsigned char orb_model;  /**< ID of non-native Orbit Model */
        unsigned char clk_model;  /**< ID of non-native Clock Model */
        unsigned char alm_model;  /**< ID of non-native ALM   Model */
        unsigned char utc_model;  /**< ID of non-native UTC   Model */

};

/*------------------------
 * Common to all GANSS
 *-----------------------*/

#define UE_CAP_AGANSS_TIM    0x00000001
#define UE_CAP_AGANSS_POS    0x00000002
#define UE_CAP_AGANSS_ION    0x00000004
#define UE_CAP_AGANSS_EOP    0x00000008

#define UE_ASSIST_CLK_MDL    0x00
#define UE_ASSIST_ORB_MDL    0x01
#define UE_ASSIST_ALM_MDL    0x02
#define UE_ASSIST_UTC_MDL    0x03

#define UE_SEL_BIT_MODEL1    0x01
#define UE_SEL_BIT_MODEL2    0x02
#define UE_SEL_BIT_MODEL3    0x03
#define UE_SEL_BIT_MODEL4    0x04
#define UE_SEL_BIT_MODEL5    0x05
#define UE_SEL_BIT_MODEL6    0x06
#define UE_SEL_BIT_MODEL7    0x07
#define UE_SEL_BIT_MODEL8    0x08

/*------------------------
 * GANSS specific capabilities
 *-----------------------*/

#define UE_CAP_AGANSS_RTI    0x00000100    /**< Real time integrity    */
#define UE_CAP_AGANSS_DGA    0x00000200    /**< DGANSS corrections     */
#define UE_CAP_AGANSS_ALM    0x00000400    /**< Almanac assistance     */
#define UE_CAP_AGANSS_MSR    0x00000800    /**< Satellite ACQ assist   */
#define UE_CAP_AGANSS_NAV    0x00001000    /**< Ephemeris assistance   */  

#define UE_CAP_AGANSS_UTC    0x00004000    /**< UTC time model         */
#define UE_CAP_AGANSS_GGT    0x00008000    /**< GNSS GNSS time model   */

#define UE_CAP_AGANSS_MDL    0x00040000    /**< Non native data models */  

struct ganss_ue_assist_caps {
        
        unsigned int                 common_bits;    /**< Common capabilities */
        unsigned int                 n_ganss;        /**< # of GANSS entries  */
        
        struct ganss_assist_caps {
                
                unsigned char        id_of_ganss;   
                
                unsigned int         caps_bitmap;    /**< GANSS specific caps */
                
                /** Non-native data     */
                struct ganss_data_model data_mdl;
                
	} ganss_caps[MAX_GANSS];
};

/** GNSS Navigation model. Refer to section 10.3.7.88a and 10.3.7.88c for 3GPP 
    specification 25.331 and section A.8.2.3 of 3GPP specification 44.031 (in
    conjunction with section 10.31 of 3GPP 49.031)
*/
struct sat_nav_req_info {

        unsigned int     ref_secs;  /* GPS: week_nr, GLO: day in NT */
        unsigned char    gnss_toe;
        unsigned char    ttoe_age;

        unsigned char    n_of_sat;

        struct sat_nav {

                unsigned char  sat;  /**< Satellite ID  */
                unsigned short iod;  /**< Issue of data */

        }               sat_info[MAX_SAT];
};

/** GPS assistance request.   Refer to section 10.3.7.90 for 3GPP sepcification
    25.331 and section A.8.2.3 of 3GPP specification 44.031 (in conjuction with
    section 10.30 of 3GPP 49.031)
*/

#define A_GPS_ALM_REQ            0x00000001      /**< GPS Almanac            */
#define A_GPS_UTC_REQ            0x00000002      /**< GPS UTC REQ            */
#define A_GPS_ION_REQ            0x00000004      /**< GPS Ionosphere REQ     */
#define A_GPS_NAV_REQ            0x00000008      /**< GPS Ephemeris  REQ     */
#define A_GPS_DGP_REQ            0x00000010      /**< GPS Diff-GPS REQ       */
#define A_GPS_POS_REQ            0x00000020      /**< GPS Reference POS      */
#define A_GPS_TIM_REQ            0x00000040      /**< GPS Reference Time     */ 
#define A_GPS_ACQ_REQ            0x00000080      /**< GPS SAT Acquisition    */
#define A_GPS_RTI_REQ            0x00000100      /**< GPS Real Time Integrity*/ 

struct gps_assist_req {

        unsigned int             assist_req_map; /**< Specifies assist needed */
        struct sat_nav_req_info  nav_assist_req; /**< Valid for A_GPS_NAV_REQ */
};

#define A_GANSS_RTI_REQ          0x00000100  /**< Real time integrity     */ 
#define A_GANSS_DGA_REQ          0x00000200  /**< DGANSS corrections      */
#define A_GANSS_ALM_REQ          0x00000400  /**< Almanac assistance      */
#define A_GANSS_MSR_REQ          0x00000800  /**< Satellite ACQ assist    */
#define A_GANSS_NAV_REQ          0x00001000  /**< Ephermeris assistance   */

#define A_GANSS_UTC_REQ          0x00004000  /**< UTC time model          */
#define A_GANSS_GGT_REQ          0x00008000  /**< GNSS-GNSS Time model, Is it to be used? */
#define A_GANSS_MDL_REQ          0x00040000  /**< Non native data models  */


struct ganss_req_info {

        unsigned char            id_of_ganss;

        /** Bitmap of GANSS assistance requested by UE to perform LOC */
        unsigned int             req_bitmap;

        /** Specifies signals for which DGANSS corrections are requested */
        unsigned char            dganss_req; /**< Valid with A_GANSS_DGC_REQ */
        
        /** Enumerates EPH info for NAV assistance request */
        struct sat_nav_req_info  nav_assist; /**< Valid with A_GANSS_NAV_REQ */
        
        /** Describe data models for requested assistance */
        struct ganss_data_model  data_model; /**< Valid with A_GANSS_MDL_REQ */
};

/** GANSS assistance request. Refer to section 10.3.7.90b for 3GPP sepcification
    25.331 and section A.8.2.3 of 3GPP specification 44.031 (in conjuction with
    section 10.31 of 3GPP 49.031)
*/

#define A_GANSS_TIM_REQ          0x00000001      /**< GANSS Ref Time REQ      */
#define A_GANSS_POS_REQ          0x00000002      /**< GANSS Ref POS  REQ      */
#define A_GANSS_ION_REQ          0x00000004      /**< GANSS Ref ION  REQ      */

#define A_GANSS_EOP_REQ          0x00000020      /**< Earth Oriented Params   */ /* Not Supported */

struct ganss_assist_req {


        unsigned int             common_req_map; /**< Common to all GANSS(es) */
        unsigned int             n_of_ganss_req; /**< Number of needed A-GNSS */
        struct ganss_req_info    ganss_req_data[MAX_GANSS];
};

/** UE Positioning Error. Refer to section 10.3.7.87 of 3GPP specification 25.331
    and section A.3.2.6 of 3GPP specification 44.031
*/
#define ERR_HAS_GPS_ASSIST_REQ   0x00000001
#define ERR_HAS_GANSS_ASSIST_REQ 0x00000002

enum lc_err_id {

        undefined              =  0,
        not_enough_gps_sv      =  2,
        no_gps_assist_data     =  6,
        not_enough_ganss_sv    = 11,
        no_ganss_assist_data   = 12

	/* Need to add more */
};

struct gnss_loc_err {
        
        enum lc_err_id           error_id; // Need to change enum ue_err_id to enum loc_err_id

        unsigned int             contents; /**< Flags outlining info presence  */     

        struct gps_assist_req    gps_req;  /**< Valid: ERR_HAS_GPS_ASSIST_REQ  */
        struct ganss_assist_req  ganss_req;/**< Valid: ERR_HAS_GANSS_ASSIST_REQ*/
};

/** UE Measured Results (GPS MSR). Refer to section 10.3.7.99 & 10.3.7.93 of 3GPP
    specification 25.331 and section A.3.1 of 3GPP specification 44.031
*/
#define GPS_MSR_ERR_REPORTED  0x00000001
#define GPS_MSR_NOT_REPORTED  0x00000002

struct gps_msr_result {

        unsigned int          contents; /**< Flags outlining info presence     */

        struct gnss_loc_err   error;    /**< If GPS_MSR_ERR_REPORTED is set    */
	unsigned char         h_acc_K;  /**< Horizontal accuracy               */
	unsigned char         v_acc_K;  /**< Vertical   accuracy               */

        unsigned int          ref_secs; /**< GPS: Week (secs); GLO: Day (secs) */
        unsigned int          ref_msec; /**< GPS: ToW  (msec); GLO: ToD (msec) */

        unsigned char         n_sat;  /**< zero if GPS_MSR_ERR_REPORTED is set */
};

/** UE Measured Results (GANSS MSR). Refer to section 10.3.7.99 & 10.3.7.93a of 
    3GPP specification 25.331 and section A.3.1 of 3GPP specification 44.031
*/
#define GANSS_MSR_ERR_REPORTED  0x00000001
#define GANSS_MSR_NOT_REPORTED  0x00000002

struct ganss_msr_result {

        unsigned int         contents; /**< Flags outlining info presence     */

        struct gnss_loc_err  error;    /**<Valid for GANSS_MSR_ERR_REPORTED   */

	unsigned char        h_acc_K;  /**< Horizontal accuracy */
	unsigned char        v_acc_K;  /**< Vertical   accuracy */

        unsigned int         ref_secs; /**< GPS: Week (secs); GLO: Day (secs) */
        unsigned int         ref_msec; /**< GPS: ToW  (msec); GLO: ToD (msec) */

        /**< Num of struct ganss_msr_desc; if GANSS_MSR_ERR_REPORTED then 0   */
        unsigned char        n_ganss;  
};


/** UE Measured Results (POS Estimate). Refer to section 10.3.7.99 & 10.3.7.109 
    of 3GPP specification 25.331 and section A.3.1 of 3GPP specification 44.031
*/
#define POS_ERR_REPORTED    0x00000001
#define POS_NOT_REPORTED    0x00000002
/*SRK: Added to report the DOP*/
#define POS_HAS_DOP		    0x00000004
#define POS_HAS_VELOCITY    0x00000008
/*SRK: Added to report the Heading*/
#define POS_HAS_HDG		    0x00000010
#define POS_HAS_TIME        0x00000020

struct gnss_pos_result {

        unsigned int         contents; /**< Flags outlining info presence      */
       	unsigned int 		 Tcount;   /*Fcount from the device*/
        struct gnss_loc_err  lc_error; /**< Valid if POS_ERR_REPORTED is set   */

        enum gnss_time_id    gnss_tid; /**< ID of GNSS Reference Time          */ 
        unsigned int         ref_secs; /**< GPS: Week (secs); GLO: Day (secs)  */
        unsigned int         ref_msec; /**< GPS: ToW  (msec); GLO: ToD (msec)  */

		unsigned int 		utc_secs; /** Current utc sec elapsed since January 1, 1970 */
		unsigned short		utc_msec; /** Mili Seconds elapsed in the currect second */

        enum loc_fix_type    fix_type;
        
        unsigned int         pos_bits; /**< Indicates GNSS used in UE POS      */
		unsigned char 		pps_state;
        unsigned char  utcOffset;
		unsigned short week_num;
		double 	       osc_bias;   
		float 		   osc_freq;		
        struct location_desc location; /**< The Location information of UE     */
        struct vel_gnss_info velocity; /**< The Velocity informaiton of UE     */
		struct dop_gnss_info dopinfo;
		unsigned int  time_sec;
		unsigned int  time_usec;
		unsigned short pos_flags;
};

//Private enum to indicate gnss_id
enum gnss_pv_id {

	e_gnss_gps = 0,
	e_gnss_glo,
	e_gnss_sbs,
	e_gnss_qzs,
	e_gnss_und
};

/*List of the Product line test*/
enum plt_test_modes
{
  plt_rtc_osctest,           
  plt_gps_osctest,           
  plt_sigacqtest,           
  plt_cw_test,           
  plt_gpiotest,           
  plt_synctest,           
  plt_selftest,           
  plt_ram_chksumtest,            
};


struct cw_test_param{
	unsigned int 	nb_center_freq;
	unsigned short 	tests;
	unsigned short 	options;
	unsigned char	num_wb_peaks;
	unsigned char	wb_peak_samples;
	unsigned char	num_nb_peaks;
	unsigned char	nb_peak_samples;
	unsigned char	decim_factor;
	unsigned char	tap_off_pts;
	unsigned char	glonass_slot;
	unsigned char	num_fft_avg;
	unsigned char	noise_fig_corr_factor;
	unsigned char 	zzz_align;
	unsigned short	in_tone_lvl;
};



struct plt_param {
	unsigned int 			test_type;
	unsigned int 			timeout;
	unsigned char 			svid;
	unsigned char 			term_evt;
	unsigned short 			zzz_align;
	struct cw_test_param	cw_test;
};
 

 
#define MAX_SIGACQ_SV 13

#define MAX_PEAK 10
struct wbnd_snr {
	unsigned char peaks;
	signed short wb_peak_index[MAX_PEAK];
	float wb_peak_SNR[MAX_PEAK];
};

struct nbnd_snr {
	unsigned char peaks;
	signed int c_freq;
	signed short nb_peak_index[MAX_PEAK];
	float nb_peak_SNR[MAX_PEAK];
};

enum plt_rep_type{
	plt_sigacq_test_rep,
	plt_cw_test_rept
};

struct cw_test_report{
	unsigned char resp_type;
	unsigned short tot_packts;
	unsigned short pack_num;
	struct wbnd_snr wb_snr;
	struct nbnd_snr nb_snr;
	float tcxo_offset;
	float noise_figure;
	/*For signal Acquisition test*/
	enum plt_rep_type rep_type;
	unsigned char svs_acquired;
	unsigned char prn[MAX_SIGACQ_SV];
	float cno[MAX_SIGACQ_SV];
	
};
#ifdef __cplusplus
	}
#endif 
#endif
