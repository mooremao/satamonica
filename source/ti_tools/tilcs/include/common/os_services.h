/*
 * os_services.h
 *
 * OS specific procedure prototypes
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef __OS_SERVICES_H__
#define __OS_SERVICES_H__

#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>

#ifdef __cplusplus
	extern "C" {
#endif
void* os_mutex_init(void);

int os_mutex_take(void *mtx_hnd);

int os_mutex_give(void *mtx_hnd);

void os_mutex_exit(void *mtx_hnd);

void* os_sem_init(void);

int os_sem_take(void *sem_hnd);

int os_sem_give(void *sem_hnd);

void os_sem_exit(void *sem_hnd);

int os_fifo_init(char *rd_name, char *wr_name);

ssize_t os_fifo_writev(int fd, const struct iovec *iov, int iovcnt);

ssize_t os_fifo_readv(int fd, const struct iovec *iov, int iovcnt);

ssize_t os_fifo_write(int fd, const void *buf, size_t count);

ssize_t os_fifo_read(int fd, void *buf, size_t count);

void os_fifo_exit(int fd);

typedef void *thread_fn(void *arg);

void *os_create_thread(thread_fn function, void *thread_arg, char *thr_name);

void os_destroy_thread(void *thrd_hnd);

void os_exit_thread(void *val);

void os_services_init(void);

void os_services_exit(void);

unsigned int os_utc_secs();

int os_is_file_exists(char *file_name);

void *os_get_stat() ;

int os_stat_file(char *file_name, void *stat);

int os_file_size(void *stat);

void os_free_stat(void *stat);

#define POP_LIST_ATOMIC(mutex, list, entity)          \
{                                                     \
    if (mutex)                                        \
        os_mutex_take(mutex);                         \
                                                      \
    entity = list;                                    \
                                                      \
    if(entity) {                                      \
        list = entity->next;                          \
        entity->next = NULL;                          \
    }                                                 \
                                                      \
    if (mutex)                                        \
        os_mutex_give(mutex);                         \
}

#define ADD_LIST_ATOMIC(mutex, entity, list)          \
{                                                     \
    if (mutex)                                        \
        os_mutex_take(mutex);                         \
                                                      \
    if(entity) {                                      \
        entity->next = list;                          \
        list = entity;                                \
    }                                                 \
                                                      \
    if (mutex)                                        \
        os_mutex_give(mutex);                         \
}

#define ADD_LIST(mutex, entity, list)                 \
{                                                     \
    if(entity) {                                      \
        entity->next = list;                          \
        list = entity;                                \
    }                                                 \
}

#define DEL_LIST_ATOMIC(mutex, last, entity, list)    \
{                                                     \
    if (mutex)                                        \
        os_mutex_take(mutex);                         \
                                                      \
    if(last) {                                        \
        last->next = entity->next;                    \
    } else {                                          \
        list = entity->next;                          \
        entity->next = NULL;                          \
    }                                                 \
                                                      \
    if (mutex)                                        \
        os_mutex_give(mutex);                         \
}

#define DEL_LIST(mutex, last, entity, list)           \
{                                                     \
    if(last) {                                        \
        last->next = entity->next;                    \
    } else {                                          \
        list = entity->next;                          \
        entity->next = NULL;                          \
    }                                                 \
}

typedef void (*sig_handler)(int);

int os_add_sig_handler(int signum, sig_handler handler);

int os_rem_sig_handler(int signum);

double os_service_log(double d_value);

double os_service_sqrt(double d_value);

double os_service_ldexp(double mantissa, unsigned int exponent);

double os_service_floor(double d_value);

double os_service_fabs(double d_value);

unsigned int os_time_secs();

unsigned int os_monotonic_secs();

#define SIGTIMER SIGRTMAX
#define OS_SIG_TIMER SIGTIMER

typedef void (*os_timer_cb)( void *);

struct os_timer_desc {

        timer_t        timer_id;
        os_timer_cb    cb_funct;
	void*                 cb_param;

	unsigned char         is_inuse;
	unsigned char         is_1shot;
	struct os_timer_desc *next;
};

int os_timer_module_init(void);

void os_timer_module_exit(void);

void os_timer_stop(void *timer);
void* os_timer_1shot(unsigned int tout_secs, os_timer_cb timer_fn, void *cb_param);

void osal_timer_module_exit(void);

void *osal_timer_sched(unsigned int tout_secs, unsigned int intv_secs, os_timer_cb cb_fn, void *param);
int os_sock_recv(int fd, char *data, int len);
int os_sock_send(int fd, const char *dst_path, char *data, int len);
int os_sock_exit(int fd);
int os_sock_init(const char* sock_path);




#ifdef __cplusplus
}
#endif
#endif /* __OS_SERVICES_H__ */

