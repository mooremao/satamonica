/*
 * 3GPP_Utils.h
 *
 * Utilities to manage 3GPP specific data structures.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
 */

#ifndef _3GPP_UTILS_H_
#define _3GPP_UTILS_H_

#include "gnss.h"

/* Place holder for GLO EPH ICD information */
struct glo_eph_info {

	unsigned int    flags;         /* Implementation specific */
	unsigned char   n_sat;         /* Number of satellites    */ 

	struct glo_eph  eph[MAX_SAT];  /* ICD Ephemeris Data      */  
};

/* Place holder for GLO ALM ICD informaiton */
struct glo_alm_info {

	unsigned int    flags;         /* Implementation specific */
	unsigned char   n_sat;         /* Number of satellites    */

	struct glo_alm  alm[MAX_SAT];  /* ICD Alamanac Data       */
};

/* Include Navigation Model information from Network into GLO EPH ICD info
 * 
 * [inout] eph_info: EPH ICD Place holder
 * [in]    utc_secs: UTC at function call
 * [in]    nav_mdl:  Array of 3GPP GLO Navigation Model
 * [in]    num_nav:  Number of elements in NAV array
 *
 * Returns 0 on success otherwise -1 on error
 */
int add2glo_icd_eph(struct glo_eph_info* eph_info, unsigned int utc_now,
						const struct glo_nav_model* nav_mdl, unsigned char num_nav);

/* Does place holder has all required information? 
 * 
 * [inout]  eph_info: EPH ICD Place holder
 * 
 *
 * Returns true or false
 */
unsigned int is_glo_icd_eph(const struct glo_eph_info* eph_info);


/* Include 3GPP ALM Keplerien Model from Network into GLO ALM ICD info
 * 
 * [inout] alm_info: ALM ICD Place holder
 * [in]    utc_secs: UTC at function call
 * [in]    kp_set:   Array of 3GPP GLO ALM Model (KP)
 * [in]    num_set:  Number of elements in ALM array (KP)
 *
 * Returns 0 on success otherwise -1 on error
 */
int add_kp2glo_icd_alm(struct glo_alm_info *alm_info, unsigned int utc_secs,
					const struct glo_alm_kp_set *kp_set, 
					unsigned char num_set);

/* Include 3GPP GLO UTC Model from Network into GLO ALM ICD info
 * 
 * [inout] alm_info: ALM ICD Place holder
 * [in]    utc_mdl:  UTC Mdl from network
 *
 * Returns 0 on success otherwise -1 on error
 */
int add_utc2glo_icd_alm(struct glo_alm_info* alm_info, 
							const struct glo_utc_model* utc_mdl);

/* Include 3GPP GLO UTC Model from Network into GLO ALM ICD info
 * 
 * [inout] eph_info: ALM ICD Place holder
 * [in]    time_mdl: UTC Model from network
 *
 * Returns 0 on success otherwise -1 on error
 */
int add_time2glo_icd_alm(struct glo_alm_info* alm_info, 
                        const struct ganss_ref_time* glo_time, 
                        const struct ganss_time_model* time_mdl);


/* Does place holder has all required information? 
 * 
 * [inout]  alm_info: ALM ICD Place holder
 * 
 *
 * Returns true or false
 */
unsigned int is_glo_icd_alm(const struct glo_alm_info* alm_info);


/* Reset GLO ICD information place holders. 
 *
 * Must be called before start and after completion of every 
 * 3GPP-to-ICD translation
 * 
 * [inout]  eph_info: EPH ICD Place holder
 * [inout]  alm_info: ALM ICD Place holder
 * 
 *
 */
void reset_glo_icd_data(struct glo_eph_info *eph_info, 
							struct glo_alm_info *alm_info);

#endif
