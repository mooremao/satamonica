/*
 * device.h
 *
 * Device specific structures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#ifndef _DEVICE_H_
#define _DEVICE_H_
/*
enum gnss_sv_type {

	gnss_gps = 0,
	gnss_glo,
	gnss_sbs,
	gnss_qzs,
	gnss_und
};*/

/** Dillution of precision         */
struct dev_dop_info {

        unsigned char  position;
        unsigned char  horizontal;
        unsigned char  vertical;
        unsigned char  time;
};

/** Device location ellipse (orientation) uncertainty information
    @note Tend to match with section 5.3 of 3GPP specification 23.032
*/
struct dev_ellp_unc {

        unsigned short  orientation; /**< Unc (axis), pi/2^15 rad resolution    */
        unsigned short  semi_major;  /**< Unc on axis, 0.1 M resolution         */
        unsigned short  semi_minor;  /**< Unc perpendicular, 0.1 M resolution   */
};

/** UTC information */
struct dev_utc_info {

        unsigned char  hours;
        unsigned char  minutes;
        unsigned char  seconds;
        unsigned char  sec_1by10;
};

/** Satellite used for position fix */
struct dev_sat_info {

        unsigned char  gnss_id;

        unsigned char  svid;
        unsigned char  iode;
        short          msr_residual;
        short          weigh_in_fix;
};

/** Device position information in native format */
struct dev_pos_info
{
        /** The macros DEV_POS_HAS_XXX informs app about addl data with struct */

#define DEV_POS_HAS_UTC   0x0100      /**< Contains  @see struct dev_utc_info  */
#define DEV_POS_HAS_ELP   0x0200      /**< Contains  @see struct dev_ellp_unc  */
        unsigned short  contents;

        unsigned int    TCount;
        unsigned short  pos_flags;      /**< Flags to indicate pos info contents */
        int             lat_rad;      /**< Latitude,  pi/2^32 rad resolution   */
        int             lon_rad;      /**< Longitude, pi/2^31 rad resolution   */
        short           alt_wgs84;    /**< WGS-84 altitude, 0.5 M resolution   */
        short           has_msl;      /**< Has a valid MSL; 1 - yes, 0 - no    */
        short           alt_msl;      /**< MSL altitude, 0.5 M resolution      */
        unsigned short  unc_east;     /**< East uncertainty, 0.1 M resolution  */
        unsigned short  unc_north;    /**< North uncertainty, 0.1 M resolution */
        unsigned short  unc_vertical; /**< Vertical unc, 0.1 M resolution      */
		unsigned int 	pos_bits;	/*SVs used in fix bitmap*/
		unsigned char pps_state; /*Added to update the pps state*/

        struct dev_ellp_unc *ell;     /**< Ellipse uncertainty                 */
        struct dev_utc_info *utc;     /**< UTC information                     */

        /** Information about Velocity, DOP, Satellites are provided through 
            other structures and different enums @see enum ue_pos_id
        */
};

/** Device heading information */
struct dev_hdg_info {

        unsigned short truthful;     /**< True     head, pi/2^15 rad resolution   */
		unsigned short magnetic;     /**< Magnetic head, pi/2^15 rad resolution   */
		long allign_1;
		long allign_2;
		long allign_3;		
};

struct dev_vel_info {

#define DEV_POS_HAS_HDG   0x0001     /**< Contains heading information        */

        unsigned short contents;     /**< Flags to indicate vel info contents */

        short          east;
        short          north;
        short          vertical;
        unsigned short uncertainty;
		unsigned short east_unc;
		unsigned short north_unc;
		unsigned short velocityUp_unc;
        struct dev_hdg_info *hdg_info;
};


struct dev_msr_info {

        unsigned char  gnss_id;       /**< GPS, GLONASS, refer 3GPP spec     */
        unsigned char  svid;          /**< Space Vehicle ID,                 */ 

        unsigned short snr;           /**< SNR, 0.1 dB resolution            */
        unsigned short cno;           /**< C/No 0.1 dB resolution            */
        short          latency_ms;    /**< Latency                           */
        unsigned int   msec;          /**< Time msec, 1 msec resolution      */
        unsigned int   sub_msec;      /**< Sub  msec, 1/2^24 msec resolution */
        unsigned short time_unc;      /**< Time unc, 1 resolution            */
        int            speed;         /**< Speed, 0.01 meters/sec resolution */
        unsigned int   speed_unc;     /**< Unc,   0.01 meters/sec resolution */
        unsigned char  sv_slot;       /**< description? -7 to 13             */
        char           elevation;     /**< resolution ?                      */
        unsigned char  azimuth;       /**< resolution ?                      */
		/*Added for RSSI value*/
		float  rssi_dbm;
		unsigned char meas_qual;
		unsigned short meas_flags;
};


/** Device GPS Time information (native format) */
struct dev_gps_time {

        unsigned short week_num;     /**< GPS week number                  */
        unsigned int   time_msec;    /**< GPS milliseconds                 */
        int            time_bias;    /**< Resolution 1 ms, 24 bit value    */
        unsigned short time_unc;     /**< Resolution 1ns                   */
        int 	       freq_bias;    /**< 0.1 M per sec                    */
        int 	       freq_unc;     /**< 0.1 M per sec                    */
        unsigned char  is_utc_diff;
        unsigned char  utc_diff;
        unsigned int   timer_count;
};

struct dev_glo_time {

        unsigned int   timer_count;
        unsigned char  time_month;
        unsigned char  time_day;
        unsigned short time_year;
        unsigned int   time_msec;
        int            time_bias;    /**< Resolution 1 ms, 24 bit value    */
        unsigned short time_unc;     /**< Resolution 1ns                   */
        char           utc_diff;
        int 	       freq_bias;    /**< 0.1 M per sec                    */
        unsigned int   freq_unc;     /**< 0.1 M per sec                    */
		short          ggto;
		unsigned char  time_status;
};

struct dev_version
{
        unsigned char   chip_id_major;
        unsigned char   chip_id_minor;
        unsigned char   rom_id_major;
        unsigned char   rom_id_minor;
        unsigned char   rom_id_sub_minor1;
        unsigned char   rom_id_sub_minor2;
        unsigned char   rom_day;
        unsigned char   rom_month;
        unsigned char   rom_year;
        unsigned char   patch_major;
        unsigned char   patch_minor;
        unsigned char   patch_day;
        unsigned char   patch_month;
        unsigned short  patch_year;
};

struct dev_gps_status
{
	unsigned short status_word;
	struct dev_version gps_ver[1];
	unsigned int 	ref_clk;
};
/* Select device reports; not to be used in dev_proxy.h */
struct dev_rpt_sel
{
        unsigned int   async_event;
        unsigned int   bcast_agnss;
        unsigned int   nmea_select;
        unsigned int   msr_reports;
        unsigned int   pos_reports;
        unsigned short update_secs;
};

/* Describe application; not to be used in dev_proxy.h */
struct dev_app_desc
{
        unsigned int   use_cases;
        unsigned int   gnss_select;
};

/* Delete device assist; not to be used in dev_proxy.h */
struct dev_aid_del
{
        unsigned int sv_id_map;
        unsigned int mem_flags;
};

struct dev_gps_alm {

	unsigned char  prn;
	unsigned char  Health;
	unsigned char  Toa;
	unsigned short E;
	short          DeltaI;
	short          OmegaDot;
	unsigned int   SqrtA; 	
	int            OmegaZero;
	int            Omega;
	int            MZero;
	short          Af0;
	short          Af1;
	unsigned short Week;
	unsigned short UpdateWeek;
	unsigned int   UpdateMS;

	double         FullToa; 
	double         UpdateTime; 
};

struct dev_glo_alm {

	unsigned char  prn;
	int            TauC;
	int            TauGPS;
	unsigned char  N4;
	unsigned short NA;
	unsigned short nA;
	unsigned char  HnA;
	int            Lambda_nA; 			
	unsigned int   t_Lambda_nA;
	int            Delta_i_nAcorrection;
	int            Delta_T_nArate;
	char           Delta_T_nArate_Dot;
	unsigned short Varepsilon_nA;
	short          Omega_nA; 			
	unsigned char  M_nA;
	short          B1;
	short          B2;
	unsigned char  Kp;
	short          Tau_nA;
	unsigned char  C_nA;

};

struct dev_gps_eph {

	unsigned char  prn;
	unsigned char  Code_on_L2;
	unsigned char  Accuracy;
	unsigned char  Health;
	unsigned char  Tgd;
	unsigned short IODC; 	
	unsigned short Toc;
	unsigned char  Af2;
	unsigned short Af1;
	unsigned int   Af0;

	unsigned char  IODE;
	short          Crs;
	short          DeltaN;
	int            Mo; 		
	short          Cuc;
	unsigned int   E;
	short          Cus;
	unsigned int   SqrtA;
	unsigned short Toe;
	short          Cic;
	int            Omega0; 	
	short          Cis;
	int            Io; 		
	short          Crc;
	int            Omega;
	int            OmegaDot;
	short          Idot;
	unsigned short Week;

	unsigned char  is_bcast;

	unsigned short UpdateWeek;
	unsigned int   UpdateMS;

	double         UpdateTime;	 
};

struct dev_glo_eph {

	unsigned char  prn;
	unsigned char  m; 		
	unsigned short Tk;
	unsigned char  Tb;
	unsigned char  M;
	short          Epsilon;
	int            Tau;
	int            Xc; 		
	int            Yc; 		
	int            Zc; 		
	int            Xv; 		
	int            Yv; 		
	int            Zv; 		
	char           Xa;
	char           Ya;
	char           Za;
	unsigned char  B; 		
	unsigned char  P; 		
	unsigned short Nt;
	unsigned char  Ft;
	unsigned char  n; 	
	char           DeltaTau;
	unsigned char  En;
	unsigned char  P1;
	unsigned char  P2;
	unsigned char  P3;
	unsigned char  P4;
	unsigned char  ln;

	unsigned char  is_bcast;

	unsigned short UpdateWeek;
	unsigned int   UpdateMS;

	double         UpdateTime;	 
};

struct dev_gps_ion {

	char           Alpha0;
	char           Alpha1;
	char           Alpha2;
	char           Alpha3;
	char           Beta0;
	char           Beta1;
	char           Beta2;
	char           Beta3;
};

struct dev_glo_ion {

	char           Alpha0;
	char           Alpha1;
	char           Alpha2;
	char           Alpha3;
	char           Beta0;
	char           Beta1;
	char           Beta2;
	char           Beta3;
};

struct dev_gps_utc {

	int            A0;
	int            A1;
	char           DeltaTls;
	unsigned char  Tot;
	unsigned char  WNt;
	unsigned char  WNlsf;
	unsigned char  DN;
	char           DeltaTlsf;
};

struct dev_glo_utc {

	unsigned short B1;
	unsigned short B2;
	unsigned char KP;
};

struct dev_pos_stat {

	unsigned char  status;
	unsigned short sigma;
	unsigned char  PDOP;
	unsigned char  HDOP;
	unsigned char  VDOP;
	unsigned char  fix_status;
};

#define DEV_MSR_HAS_GPS_RSLT    0x01   /* Horizontal */
#define DEV_MSR_HAS_GLO_RSLT    0x02   /* Horizontal */

struct dev_msr_result {

	unsigned int  contents;        /* See above flags */
	unsigned char gps_unc_K;       /* coded as per section 6.2 of 3GPP specification 23.032 */
	unsigned char glo_unc_K;       /* coded as per section 6.2 of 3GPP specification 23.032 */
	unsigned char msr_qual_status; /* flag to indicate  msr status */

	unsigned int  gps_ref_secs;    /* Week # */
	unsigned int  gps_ref_msec;    /* ToW    */

	unsigned int  glo_ref_secs;
	unsigned int  glo_ref_msec;

};

struct dev_alm_stat {

	unsigned char  prn;
	unsigned char  toa;
	unsigned short week;
};

struct dev_eph_stat {

	unsigned char  prn;
	unsigned char  iode;
	unsigned short toe;
	unsigned short week;
};

struct dev_sv_hlth {

	unsigned short update_week;
	unsigned int   update_msecs;

	unsigned short num_sat;
	unsigned char  health_status[200];
};

struct dev_sv_dir {
	unsigned char  prn;
	char           elevation;
	unsigned char  azimuth;
};

struct dev_gps_tcxo{
	
	int freq_bias;
	unsigned int freq_unc;
};

struct dev_calib_resp{
	
	unsigned char calib_resp;
};

struct dev_sv_slot_map {
	unsigned char svid;
	unsigned char slot;	
	unsigned char status; 
};



struct dev_cw_test_param {
		unsigned int tests;
		unsigned int options;
		unsigned int num_wbp;
		unsigned int wbp_samples;
		unsigned int num_nbp;
		unsigned int nb_cenfreq;
		unsigned int nbp_samples;
		unsigned int dec_fac;
		unsigned int toff_pts;
		unsigned int glo_slot;
		unsigned int num_fft_avg;
		unsigned int noise_fig_correction;
		unsigned int in_tone_lvl;
} ;

struct dev_plt_param {
	unsigned int 			test_type;
	unsigned int 			timeout;
	unsigned char 			svid;
	unsigned char 			term_evt;
	unsigned short 			zzz_align;
	struct dev_cw_test_param	cw_test;
//	struct gpio_test_param	gpio_test;
	
};
enum plt_test_rep_type{
	sigacq_test_rep,
	cw_test_rept
};


#define MAX_SIGACQ_SV 13

#define MAX_PEAK 10
struct widebnd_snr {
	unsigned char peaks;
	signed short wb_peak_index[MAX_PEAK];
	unsigned short wb_peak_SNR[MAX_PEAK];
};

struct narrowbnd_snr {
	unsigned char peaks;
	signed int center_freq;
	signed short nb_peak_index[MAX_PEAK];
	unsigned short nb_peak_SNR[MAX_PEAK];
};



struct dev_cw_test_report{
	unsigned char response_type;
	unsigned short total_packts;
	unsigned short pack_num;
	struct widebnd_snr wb_snr;
	struct narrowbnd_snr nb_snr;
	signed int tcxo_offset;
	unsigned short noise_figure;
	/*For signal Acquisition test*/
	enum plt_test_rep_type rep_type;
	unsigned char svs_acquired;
	unsigned char prn[MAX_SIGACQ_SV];
	unsigned short cno[MAX_SIGACQ_SV];
	
};

#endif
