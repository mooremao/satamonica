/*
 * dev_proxy.h
 *
 * Client API procedure prototypes
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

/** @file dev_proxy.h
    This file describes the interface of the proxy-server for the GNSS device
    to be used by a client app.

    The proxy-server is the SW agent or representative of the real GNSS device
    that provisions the services and features of the positioning device on the
    general purpose processor (GPP) a.k.a host platform. The proxy-server acts
    as the native C user space driver and plugs-in the GNSS receiver in to the
    platform specific application framework as the location provider.

    The key motivation that drove the design of the device proxy-server was to
    abstract the low level details of message transactions with the device and
    yet, provide rudimentary API(s), such that, the capabilities & features of
    the device are availalbe to be utilized by existing and new applications.

    It has been a deliberate decision to keep the interface API(s) rudimentaty
    and not get into the practice of defining policies and rules. Specifically,
    by provisioning the rudimentary API(s), the device proxy-server offers the
    client app the utmost flexibility to build the policies and rules that are
    best suited for the targeted platforms & requirements. Alternatively, such
    an arrangement of exporting essential & rudimentary API(s) facilitates the
    un-restrictive use of the GNSS device in a wide range of known and unknown
    platforms.

    Yet another objective of provisioning such rudimentary API(s) is to enable
    a sufficient and yet, simple design and easy-to-maintain implementation of
    the proxy-server. The maintenance and support aspect of the implementation
    is of a paramount importance. As a follow up, an unjustified state machine
    is not part of the design. Alternatively, the implementation of the proxy-
    server remains by-and-large state (machine) less.

    Any future extensions & development must follow the following guidelines.

    A new API or an extension to the existing API
    a) must be rudimentary
    b) must not imply a rule or policy (including a state machine)
    b) must ensure simple design and implementation

    @note The proxy-server at any given point of time supports only one client
    application.

    @note The comments in this file has been rendered with key-words & tags to
    facilitate generation of documentation by using doxygen (www.doxygen.org).
    Please do not remove any key-words & tags.
*/

#ifndef _DEV_PROXY_H_
#define _DEV_PROXY_H_

#include "device.h"
#include "gnss.h"

/** @defgroup ue_pos_group The UE POS information
    Defines and identifies various flavors of measurement & position reports

    @warning GLONASS Measurement report is not supported by the GNSS device

    @note DOP means dilution of precision

    @{
*/
enum ue_pos_id {

        loc_exl_native, /**< location w/o aux info @see struct dev_pos_info   */
        loc_sat_native, /**< location w/  sat info @see struct dev_sat_info   */
        loc_dop_native, /**< location w/  DOP info @see struct dev_dop_info   */
        loc_all_native, /**< location + DOP + satellite info                  */
        loc_gnss_ie,     /**< 3GPP, GPS/GLO loc IE @see struct loc_gnss_info  */

        vel_native,     /**< velocity information  @see struct dev_vel_info   */
        vel_gnss_ie,    /**< 3GPP, Velocity IE @see struct vel_gnss_info      */

        tim_gps_native,
        tim_glo_native,
        tim_gps_ie,
        tim_glo_ie,
        hdg_native,   /**SRK: Added to report the heading to the device */
};

/** Publishes the GNSS specific position information that is established by the
    device. The format and contents of the reported location is indicated by the
    report ID.

    Client app must configure & select a set of position reports that the GNSS
    device, on activation, should update periodically.

    @param[in] id identifies the information being reported @see ue_pos_id
    @param[in] data_array is the placeholder for the MSR info, depends on id
    @param[in] num is number of elements in the data array
    @return -1 on error otherwise 0

    @see ti_dev_config_reports
    @see ti_dev_exec_action
*/
typedef int (*ti_ue_pos_report)(enum ue_pos_id, void *data_array, int num);

/** @} */

/** @defgroup ue_msr_group The UE Satellite Measurement Information
    Refer to section 10.3.7.88d of 3GPP specification 25.331 and table A.59 of
    3GPP specification 44.031 fog GANSS specific details.
*/

enum ue_msr_id {

        msr_gps_native, /**< Device GPS MSR rpt @ref struct dev_gps_msr       */
        msr_gps_rpt_ie, /**< 3GPP GPS MSR IE @ref struct msr_gps_info         */

        msr_glo_native, /**< Native & device specific GLONASS measurements    */
        msr_glo_signal, /**< Refer to GLO signal @ref struct signal_glo_info  */
        msr_glo_sig_G1, /**< 3GPP, GLONASS signal G1 @ref struct msr_glo_info */
        msr_glo_sig_G2,  /**< 3GPP, GLONASS signal G2 @ref struct msr_glo_info */
        msr_device_result,
        msr_ganss_rpt_ie /*3GPP Glonass MSR IE*/
};

/** Publishes the GNSS specific satellite measurement information as detected by
    the device. The format amd contents of the reported measurement is indicated
    by the report ID.

    Client app must configure & select a set of measurement reports that the GNSS
    device, on activation, should update periodically.

    @param[in] id identifies the information being reported @see ue_msr_id
    @param[in] data_array is the placeholder for the MSR info, depends on id
    @param[in] num is number of elements in the data array
    @return -1 on error otherwise 0

    @see ti_dev_config_reports
    @see ti_dev_exec_action
*/
typedef int (*ti_ue_msr_report)(enum ue_msr_id id, void *data_array, int num);

/** @} */

/** @defgroup ue_err_group 3GPP location errors
    Location errors defined as per 3GPP standards

    @{
*/
enum lc_evt_id {

	evt_nf_off = 0,
	evt_nf_not_enough_svs,
	evt_nf_not_enough_eph, 
	evt_nf_dop_negative,
	evt_nf_vel_unc_greaterthan100, 
	evt_nf_meas_not_used_cno_lessthan100, 
	evt_nf_pdop_greater_than400, 
	evt_nf_equal_weight_inversion_fail, 
	evt_nf_2d_fail, 
	evt_nf_no_posinfo_or_posupdate, 
	evt_nf_vel_itar_fail, 
	evt_nf_pos_itar_fail
};

/** Reports the error encountered by the GNSS device whilst establishing the
    measurement and / or position (location) information. This is in response
    to MSR-POS request made by the client app by activating the GNSS device.
*/
typedef int (*ti_ue_loc_event)(enum lc_evt_id id);

/** @} */

/** defgroup nmea_group NMEA sentences
    Identifies various NMEA messages published by the GNSS device

    @{
*/
enum nmea_sn_id {

        /** GPS specific sentences */
        gpgga,
        gpgll,
        gpgsa,
        gpgsv,
        gprmc,
        gpvtg,
		gpgrs,
		gpgst,

        /** GLONASS specific sentences; not supported as of now */
        glgga,
        glgll,
        glgsa,
        glgsv,
        glrmc,
        glvtg,

		qzgsv,

		wsgsv,

		gngrs,
		gngll,
		gngns,
		gngsa,
		gnvtg
};

/** Publishes the NMEA sentences reported by the GNSS device. Depending on the
    data type (i.e. sentence id), it is possible that the data content is split
    into mutliple number (num) sentences (i.e. character array).

    Client app must configure & select a set of NMEA messages for reporting.
    The selected NMEA messages are generated periodically by the GNSS device
    on activation.

    @param[in] nmea identifies the NMEA sentences @see nmea_sn_id
    @param[in] data is the null terminated NMEA character array
    @param[in] num specifies the number of NMEA sentences (*data)
    @return -1 on error otherwise number of NMEA sentences consumed

    @see ti_dev_config_reports
    @see ti_dev_exec_action
*/
typedef int (*ti_nmea_sentence)(enum nmea_sn_id nmea, char *data, int len);

/** @} */

/** List of assistance elements provided the network to the proxy-server of MS */
enum loc_assist {

        a_REF_POS,   /**< Reference location, struct location_desc */

        /** GPS assist */
        a_GPS_TIM,   /**< Reference time, struct gps_ref_time      */
        a_GPS_DGP,   /**< DGPS corrections, struct dgps_sat        */        
        a_GPS_EPH,   /**< Ephemeris, struct gps_eph                */
        a_GPS_ION,   /**< Iono, struct gps_ion                     */
        a_GPS_UTC,   /**< UTC, struct gps_utc                      */
        a_GPS_ALM,   /**< Almanac, struct gps_alm                  */
        a_GPS_ACQ,   /**< Acquisition assist, struct gps_acq       */
        a_GPS_RTI,   /**< Real time integrity, struct gps_rti      */

        /** GLONASS assist */
        a_GLO_EPH,   /**< Ephermeris, struct glo_eph               */
        a_GLO_ALM,   /**< Almanac, struct glo_alm                  */
        a_GLO_UTC,   /**< UTC, struct glo_utc, Mayn't be supported */
        a_GLO_ION,   /**< Ionosphere, struct ganss_ion_model       */
        a_GLO_TIM,   /** Reference time, ganss_ref_time */

        a_GLO_ACQ_G1, /** Reference MSR, signal G1, ganss_ref_msr  */
        a_GLO_ACQ_G2, /** Reference MSR, signal G2, ganss_ref_msr  */

        a_GLO_RTI,    /** Real time integrity, struct ganss_rti    */

        a_GLO_DGC_G1, /** DGANSS corrections, struct dganss_sat    */
        a_GLO_DGC_G2, /** DGANSS corrections, struct dganss_sat    */

        /* Add other information for GLONASS */
};

/** Notifies the assistance data that has been either decoded and / or vetted by
    the GNSS receiver.

    Depending on the assistance type, it may be possbile that server notifies a
    collection (array) of data, such that, each element of the array represents
    informaton about a satellite or signal.

    Client app must configure & select a set of assistance reports that receiver
    should generate. The selected assistance data are reported asynchronously by
    the device on activation.

    @param[in] assist is the (type of) assistance information for a GNSS
    @param[in out] assist_array is a set of info about satellites or signals
    @param[in] num is the number of information elements in the array

    @see ti_dev_config_reports
    @see ti_dev_exec_action
*/
typedef int (*ti_dev_agnss_dec)(enum loc_assist assist, void *assist_array,
                                int num);

enum dev_evt_id {

        evt_dev_ver     /**< unsolicited device version information */
};

/** Indicates events from the device - TBD */
typedef int (*ti_dev_event_ind)(enum dev_evt_id, void *data_array, int num);

enum dev_err_id {
	/* add for GNSS recovery ++*/
	/* posible Error Types */
	gnss_no_err,
	gnss_fatal_err,
	gnss_invalid_msg_err,
	gnss_dev_exception_err,
	gnss_interval_mismatch_err,
	gnss_no_meas_rep_err,
	gnss_no_resp_err  ,           // This error is when the device does not respond to recovery messages
	gnss_hard_reset_err
	/* add for GNSS recovery --*/

};

enum dev_reset_ind{
	gnss_no_meas_resp_ind, 	/* No measurement response from the device for 5sec*/
	gnss_fw_fatal_ind, 	  	/*Fatal error in GNSS Firmware*/
	gnss_no_reset_resp_ind, /* No response for the multiple reset command*/
	gnss_hard_reset_ind		/*Hard reset indication*/			
};

/** Indicates errors from the device - TBD */
typedef int (*ti_dev_error_ind)(enum dev_err_id);

enum dev_cw_test {

	ecw_test
};



typedef int (*ti_ue_cwt_test_results)(enum dev_cw_test id, void *data_array, int num);
/** Various user application specific updates */
enum app_rpt_id {

        app_pos_old, /**< There is no new position to report                     */
        app_blf_out, /**< Buffered location fixes now available; no data content */
        app_gfc_ind  /**< Geo-fencing trigger @see struct geofence_status        */
};

/** Indication about various application specific information
    @param[in] id specifies the information type being indicated
    @param[in] data depending on the information type is optional
    @return -1 on error otherwise, 0 on success

    @see ti_dev_config_reports
    @see ti_dev_exec_action
*/
typedef int (*ti_app_ucase_ind)(enum app_rpt_id id, void *data);

/** Following is the collection of callback functions that will be invoked into
    the client application by the proxy-server. A client application can choose
    not to implement certain callback functions.

    Refer to defintion of individual callback functions for details.
*/
struct ti_dev_callbacks {

        ti_ue_pos_report  location_rpt;  /**< Device location info        */
        ti_ue_msr_report  measure_info;  /**< SV signal Measurement info  */
        ti_ue_loc_event   location_evt;  /**< Location Error              */
        ti_nmea_sentence  nmea_message;  /**< Report NMEA message         */
        ti_dev_agnss_dec  device_agnss;  /**< Assist info decoded by dev  */
        ti_dev_error_ind  device_error;  /**< Notify multiple errors      */
        ti_dev_event_ind  device_event;  /**< Events from device          */
        ti_app_ucase_ind  ucase_notify;  /**< Usecase indication from dev */
        ti_ue_cwt_test_results  cw_test_rpt;  /**< CW test report from dev */

};

struct ti_dev_proxy_params {

        unsigned int fn_call_timeout_ms; /**< Timeout for communication w/ dev */

};

/** Enables a client app to establish a connection with the proxy-server that
    acts as an agent for the real GNSS device or receiver.

    This must be the first function called by the client application.

    @param[in] callbacks is the pointer to set of functions implemented by app
    @param[in] params specifies the operation constraints for proxy-server
    @return -1 on error otherwise 0
*/
int ti_dev_proxy_connect(struct ti_dev_callbacks *callbacks,
                         struct ti_dev_proxy_params *params);

/** Client application closes the previously established connection with the
    proxy-server.

    This must be the last function called by the client application whilst
    shutting down. Following this function call, any other client app can set
    up a new connection with proxy-server.

*/
void ti_dev_proxy_release(void);

/** This structure describes the version information about the proxy server    */
struct dev_proxy_version {

        unsigned char  major;  /** MM: major version #                         */
        unsigned char  minor;  /** mm: minor version #; unique within MM       */
        unsigned short patch;  /** pp: bug-fix level #; unique within MM.mm    */
        unsigned short build;  /** bb: build iterate in lifetime of package    */

        unsigned char  exten;  /** a character extension to ver#, rarely used  */
        unsigned char  pad;

        unsigned char  qfier[12]; /** Quality: alpha<n>, beta<n>......         */


        /** Date of proxy-server compilation */
        unsigned char  day;    /** dd fomrat */
        unsigned char  month;  /** mm format, 0 to 12 */
        unsigned short year;   /** yyyy format */

};

/** Fetches version information of dev-proxy server
   @param[in out] version contains the retrieved information.
   @return -1 on error otherwise zero
*/
int ti_dev_proxy_version(struct dev_proxy_version *version);

struct dev_nvs_aid_config {

        enum nvs_aid_qual {

                aid_dont_inj,  /**< Do not use any aid from NVS (cold start) */
                aid_push_all,  /**< Inject all aid info, unchecked, into dev */
                aid_sys_time,  /**< Epoch ref: January 1, 1970; 00:00:00 UTC */
                aid_gps_time,  /**< Epoch ref: January 6, 1980; 00:00:00 UTC */
                aid_glo_time   /**< Epoch ref: January 1, 1996; 03:00:00 UTC */

        } aid_config;


        /**
           @note sys_time must relate to RTC available on platform; dev-proxy
           would use platform RTC as reference for subsequent aid info on NVS.
        */
        unsigned int epoch_secs;  /**< Elapsed epoch (secs) from reference */
};

struct dev_start_up_info {

        struct dev_nvs_aid_config  nvs_aid_cfg;

        char                       sevice_pack[60];
        char                       config_file[60];
};


/** Connect and intialize the GNSS device. Also, allocate resources required
    to establish and maintain the communication channel with the device.
    @param[in] patch_file is the name of the GNSS device service pack
    @param[in] config_file is the name of the file to configure the device
    @return -1 on error otherwise 0
*/
int ti_dev_connect(struct dev_start_up_info *start_info);

/** Release the resources used to communicate with the GNSS device and power-off
    the device.

    Following this function call, the client app can again initialize the device
    with service pack and configuration.
*/
void ti_dev_release(void);

/** Fetch version information of the device
    @param[in] version contains the retrieved information.
    @return -1 on error otherwise 0
*/
int ti_dev_version(struct dev_version *version);

/** @defgroup devact_group Defines atomic actions to manage device states.
    Lists the various device specific (power) management actions to move it
    from one operation state to another.

    dev_active: idle to active (SV acquisition & tracking) state for MSR-POS
    dev_de_act: active to idle state (MSR-POS stops)
    dev_dsleep: idle to deep sleep state
    dev_wakeup: deep sleep to idle state

    Typical state transition of the device:
    init --> idle --> sleep --> idle --> active --> idle --> sleep

    @{
*/
enum dev_action {

        dev_active,  /**< Start MSR-POS @see ti_dev_config_reports */
        dev_de_act,  /**< Stop  MSR-POS @see ti_dev_config_reports */
        dev_dsleep,  /**< Put device to deep sleep from idle       */
        dev_wakeup   /**< Wakeup device from sleep to   idle       */
};

/** Execute the specified device power management action.
    @param act identifies the action to be executed @see enum dev_action
    @return -1 on error otherwise 0
*/
int ti_dev_exec_action(enum dev_action act);

/** @} */

int ti_dev_is_alive(void); /* Ping Request */

/*--------------------------------------------------------------------*/
/** Select one or more of following async events /info from device    */
/*--------------------------------------------------------------------*/

//enum dev_evt_id {

        /* TBD */
//};

#define SET_BIT(x) (1 << x) /**< set the specified bit */

/** Enable indication of new power state of device - TBD*/
#define NEW_DEV_PWR_IND_EN   0x00100000

#define RPT_MSR_GPS_NATIVE  SET_BIT(gps_msr_native) /**< Native GPS MSR report */
#define RPT_MSR_GLO_NATIVE  SET_BIT(gps_msr_native) /**< Native GLO MSR report */
#define RPT_MSR_GPS_IE_SEL  SET_BIT(gps_msr_ie)
#define RPT_MSR_GLO_IE_SEL  SET_BIT(glo_msr_ie)

/** @} */

/** defgroup app_group Defines application profile
    Selects the essence of use-cases and GNSS(es) to be utilized on the device
    to establish a position fix
    @param[in] use_cases select the intended applications of dev
    @param[in] gnss_select is the set of the GNSS(es) to be used
    @return -1 on error otherwise 0
    @{
 */
#define DEV_UCASE_NO_NAV   0x00000001  /**< Disable periodic reports  */
#define DEV_UCASE_BLF_EN   0x00000003  /**< Enable buffered locations */
#define DEV_UCASE_GFC_EN   0x00000004  /**< Enable geo-fencing        */

#define DEV_GNSS_GPS_EN    0x00000001  /**< GPS                       */
#define DEV_GNSS_GLO_EN    0x00000002  /**< GLONASS                   */

int ti_dev_set_app_profile(unsigned int use_cases, unsigned int gnss_select);

/** @} */

/*----------------------------------------------------------------------------*/
/** Select events, agnss info, NMEA, MSR info and POS info from dev           */
/*----------------------------------------------------------------------------*/


/** @defgroup dev_agnss_group Select decoded agnss (async) from device
    @{
*/
/** Enable indication of event about new decoded EPH information */
#define NEW_EPH_GPS_NATIVE  0x00000001 /**< Native @see struct dev_gps_eph   */
#define NEW_EPH_GPS_IE_IND  0x00000002 /**< 3GPP @ see struct gps_eph        */
#define NEW_EPH_GLO_NATIVE  0x00000004 /**< Native @see struct dev_glo_eph   */
#define NEW_EPH_GLO_IE_IND  0x00000008 /**< 3GPP @see struct glo_eph_kp_set  */

/** Enable indication of event about new decoded ALM information */
#define NEW_ALM_GPS_NATIVE  0x00000010 /**< Native @see struct dev_gps_alm   */
#define NEW_ALM_GPS_IE_IND  0x00000020 /**< 3GPP @see struct gps_alm         */
#define NEW_ALM_GLO_NATIVE  0x00000040 /**< Native @see struct dev_glo_alm   */
#define NEW_ALM_GLO_IE_IND  0x00000080 /**< 3GPP @see struct glo_alm_kp_set  */

/** Enable indication of event about new decoded IONO information */
#define NEW_ION_GPS_NATIVE  0x00000100 /**< Native @see struct dev_gps_iono  */
#define NEW_ION_GPS_IE_IND  0x00000200 /**< 3GPP @ struct gps_iono           */
#define NEW_ION_GLO_NATIVE  0x00000400 /**< Native @see struct dev_glo_iono  */
#define NEW_ION_GLO_IE_IND  0x00000800 /**< 3GPP @ struct glo_iono           */

/** Enable indication of new UTC */
#define NEW_UTC_GPS_NATIVE  0x00001000
#define NEW_UTC_GPS_IE_IND  0x00002000  /** GPS UTC - 3GPP format            */
#define NEW_UTC_GLO_NATIVE  0x00004000
#define NEW_UTC_GLO_IE_IND  0x00008000  /** Not supported by device          */

/** @} */

/** @addtogroup nmea_group Selects NMEA messages from device
    @{
*/

/** GPS NMEA sentences supported by device
    @{
 */
#define NMEA_GP_GGA_EN       SET_BIT(gpgga)
#define NMEA_GP_GLL_EN       SET_BIT(gpgll)
#define NMEA_GP_GSA_EN       SET_BIT(gpgsa)
#define NMEA_GP_GSV_EN       SET_BIT(gpgsv)
#define NMEA_GP_RMC_EN       SET_BIT(gprmc)
#define NMEA_GP_VTG_EN       SET_BIT(gpvtg)
#define NMEA_GP_GRS_EN       SET_BIT(gpgrs)
#define NMEA_GP_GST_EN       SET_BIT(gpgst)
/** @} */

/** GLO (i.e. GLONASS) NMEA sentences
    @note not supported as of now
    @{
*/
#define NMEA_GL_GGA_EN       SET_BIT(glgga)
#define NMEA_GL_GLL_EN       SET_BIT(glgll)
#define NMEA_GL_GSA_EN       SET_BIT(glgsa)
#define NMEA_GL_GSV_EN       SET_BIT(glgsv)
#define NMEA_GL_RMC_EN       SET_BIT(glrmc)
#define NMEA_GL_VTG_EN       SET_BIT(glvtg)
#define NMEA_QZ_GSV_EN       SET_BIT(qzgsv)
#define NMEA_WS_GSV_EN       SET_BIT(wsgsv)
#define NMEA_GN_GRS_EN       SET_BIT(gngrs)
#define NMEA_GN_GLL_EN       SET_BIT(gngll)
#define NMEA_GN_GNS_EN       SET_BIT(gngns)
#define NMEA_GN_GSA_EN       SET_BIT(gngsa)
#define NMEA_GN_VTG_EN       SET_BIT(gnvtg)
/** @} */

/** @} */ /* end of addtogroup */

/**< @addtogroup msr_group Measurement Report Selection Flags
   @{
*/
#define MSR_RPT_GPS_NATIVE  SET_BIT(msr_gps_native) /**< GPS MSR native form */
#define MSR_RPT_GPS_IE_SEL  SET_BIT(msr_gps_rpt_ie) /**< GPS MSR protocol IE */
#define MSR_RPT_GLO_NATIVE  SET_BIT(msr_glo_native) /**< GLO MSR native form */
#define MSR_RPT_GLO_IE_SEL  SET_BIT(msr_glo_signal) /**< GLO MSR protocol IE */
#define MSR_RPT_DEV_RESULT  SET_BIT(msr_device_result) /**< Device MSR Results */
#define MSR_RPT_GANSS_IE_SEL  SET_BIT(msr_ganss_rpt_ie) /**< GANSS MSR protocol IE */

/** @} */

/**< @addtogroup pos_group Position Report Selection Flags
   @{
*/
#define POS_RPT_LOC_NATIVE      SET_BIT(loc_exl_native) /**< LOC DEV native  */
#define POS_RPT_SAT_NATIVE      SET_BIT(loc_sat_native) /**< SAT DEV native  */
#define POS_RPT_DOP_NATIVE      SET_BIT(loc_dop_native) /**< DOP DEV native  */

#define POS_RPT_NATIVE_ALL      (POS_RPT_LOC_NATIVE |   \
                                 POS_RPT_SAT_NATIVE |   \
                                 POS_RPT_DOP_NATIVE)    /**< All POS native  */

#define POS_RPT_GPS_IE_SEL      SET_BIT(loc_gnss_ie)   /**< GPS POS 3GPP IE */
#define POS_RPT_GLO_IE_SEL      SET_BIT(loc_gnss_ie)   /**< GLO POS 3GPP IE */

#define POS_RPT_VEL_NATIVE      SET_BIT(vel_native)     /**< VEL DEV NATIVE  */
#define POS_RPT_VEL_IE_SEL      SET_BIT(vel_gnss_ie)    /**< GNS VEL 3GPP IE */
#define POS_RPT_HDG_NATIVE      SET_BIT(hdg_native)     /**< VEL DEV NATIVE  */

#define POS_RPT_TIM_GPS_NATIVE  SET_BIT(tim_gps_native) /**< GPS TIM NATIVE  */
#define POS_RPT_TIM_GLO_NATIVE  SET_BIT(tim_glo_native) /**< GLO TIM NATIVE  */
#define POS_RPT_TIM_GPS_IE_SEL  SET_BIT(tim_gps_ie)     /**< GPS TIM 3GPP IE */
#define POS_RPT_TIM_GLO_IE_SEL  SET_BIT(tim_glo_ie)     /**< GLO TIM 3GPP IE */

/** @} */

/**< @addtogroup dev_event_group Asynchronous Event Report Selection Flags
   @{
*/
#define ASYC_EVT_NEW_EPH        SET_BIT(0)
#define ASYC_EVT_NEW_ALM        SET_BIT(1)
#define ASYC_EVT_NEW_ION        SET_BIT(2)
#define ASYC_EVT_NEW_HLT        SET_BIT(3)

#define ASYC_EXT_HW_EVT   		SET_BIT(4)
#define ASYC_RX_DIAG_EVT 		SET_BIT(5)
#define ASYC_RX_STATE_EVT 		SET_BIT(6)
#define ASYC_RX_PWR_EVT    		SET_BIT(7)
#define ASYC_FIRST_FIX_EVT       	SET_BIT(8)

#define ASYC_EVT_NEW_UTC        SET_BIT(9)

/** @} */

/** Configure proxy server for a select set of periodic reports & NMEA messages,
    the position update interval and a select set of asynchronous events / info.

    The selected reports & NMEA messages are re-freshed by the GNSS receiver at
    the specified interval. The selected events are indicated asynchronously as
    and when they occur.

    The server provides the periodic updates & async events to the client using
    the callback methods. @see ti_dev_callbacks

    @param[in] async_events solicit async device events    @ref dev_event_group
    @param[in] dev_agnss selects the decoded assistance    @ref dev_agnss_group
    @param[in] nmea_flags select the periodic NMEA message @ref nmea_group
    @param[in] msr_reports select the periodic MSR reports @ref ue_msr_group
    @param[in] pos_reports select the periodic POS reports @ref ue_pos_group
    @param[in] interval is the time period (sec) to refresh MSR, POS & NMEA info
    @return -1 on error otherwise 0 on success

    @see ti_dev_callbacks
*/
int ti_dev_config_reports(unsigned int async_events, unsigned int dev_agnss,
                          unsigned int nmea_flags, unsigned int msr_reports,
                          unsigned int pos_reports, unsigned short interval);

/** Delete information about the specified assistance for the flagged satellites
    from the indicated memory storages controlled by the proxy server. */

#define MEM_SRV_RAM   0x01  /**< Data RAM (on host) used by proxy-server  */
#define MEM_SRV_NVM   0x02  /**< NVM file (on host) used by proxy-server  */
#define MEM_DEV_RAM   0x04  /**< Data RAM (on chip) used by GNSS firmware */

/** Delete assist information for a set of select (applicable) satellites from
    the memory storages.

    @param[in] assist is the (type of) assistance information for a GNSS
    @param[in] sv_id_map is the bit map to indicate SV ID, bit 0 is SV ID 1
    @param[in] mem_flags refers to the storages to be delete info from
    @return -1 on error otherwise number of deletions (= num sat x num mem)
    @see enum loc_assist

    @note If no satellites are associated with the assistance type then, 0
    must be assigned to sv_id_map.
*/
int ti_dev_loc_aiding_del(enum loc_assist assist, unsigned int sv_id_map,
                          unsigned int mem_flags);

/** Provisions an assistance data for an on-going and as well as for subsequent
    location requests.

    The proxy-server utilizes the information available in each element of the
    array of the specified assistance type for establishing the a location fix
    and / or measurement information.

    Based on the type of assistance information, the content in each element of
    the array may represent the data for a satellite or a signal in that GNSS.
    The satellite ID or signal ID shall be a consituent of the array element.

    @param[in] assist is the (type of) assistance information for a GNSS
    @param[in] assist_array is a set of info about satellites or signals
    @param[in] num is the number of information elements in the array
    @return -1 on error otherwise, the number of elements successfully added
    @see enum loc_assist

    @note If assist info data does not involve an array then num = 1
*/
int ti_dev_loc_aiding_add(enum loc_assist assist, void *assist_array, int num);

/** Fetch the information about the specified assistance from the proxy-server.
    The retrieved data contains assistance information that was or is being
    utilized to establish either location fix or measurement infomation.

    Depending up on the type of the assistance, it may be required to fetch an
    array or set of data from the server. Each element of the array or set may
    represent data for a satellite or signal in that GNSS. The satellite ID or
    signal ID shall be specified in the array element by the caller as part of
    provisioning of memory to store the retrieved information.

    @param[in] assist is the (type of) assistance information for a GNSS
    @param[in out] assist_array is a set of info about satellites or signals
    @param[in] num is the number of information elements in the array
    @return -1 on error otherwise, the number of elements successfully fetched
    @see enum loc_assist

    @note If assist info data does not involve an array then num = 1
*/
int ti_dev_loc_aiding_get(enum loc_assist assist, void *assist_array, int num);

struct aiding_status {

        unsigned char obj_id;
        int           ttl_ms;
};

/** Fetch the remaining valid time (in msecs) of the specified assistance type
    from the proxy-server. The retrieved status relates to the assistance data
    that was or is being utilized to establish MSR-POS information.

    Depending up on the type of the assistance, a set of data or array may be
    relevant. Each element of the array or set may represent data for a sat-
    ellite or signal in that GNSS. The satellite ID or signal ID is specified
    in the obj_id parameter..

    The caller provisions the storage space for the status information

    @param[in] assist is the (type of) assistance information for a GNSS
    @param[in out] assist_array is a set of info about satellites or signals
    @param[in] num is the number of information elements in the array
    @return -1 on error otherwise number of elements successfully fetched
    @see enum loc_assist

    @note If assist info data does not involve an array then num = 1
    @note Only a select types of @see enum loc_assist can be queried for status
*/
int ti_dev_loc_aiding_status(enum loc_assist assist, struct aiding_status *array,
                             int num);


/*---------------------------
 * Device feature operation
 *---------------------------
 */
struct geofence_region {

        unsigned short region_id;

#define DEV_GFENCE_NO_CONFIG  0x0000
#define DEV_GFENCE_REGION_EN  0x0001 /** Can this macro be removed */
#define DEV_GFENCE_TRACK_POS (0x0002 | DEV_GFENCE_REGION_EN)
#define DEV_GFENCE_TRACK_VEL (0x0004 | DEV_GFENCE_REGION_EN)

#define DEV_GFENCE_MV_IN_POS (0x0000 | DEV_GFENCE_TRACK_POS)
#define DEV_GFENCE_MVOUT_POS (0x0008 | DEV_GFENCE_TRACK_POS)
#define DEV_GFENCE_BELOW_VEL (0x0000 | DEV_GFENCE_TRACK_VEL)
#define DEV_GFENCE_ABOVE_VEL (0x0010 | DEV_GFENCE_TRACK_VEL)
#define DEV_GFENCE_FIX_EXIST (0x0020 | DEV_GFENCE_REGION_EN)
#define DEV_GFENCE_3D_RPT_EN (0x0040 | DEV_GFENCE_REGION_EN)

        unsigned short cfg_flags;

        unsigned short thresh_vel_mpsec;
        short thresh_alt_m;
        short mean_sea_level_m;

        enum geometry {
                gfc_polygon,
                gfc_circle
        } shape;                              /**< shape of region            */

        /* Definition of region vertex   */
        struct fence_mark {               /**< fence co-ordinate / vertex */
                int lat;
                int lon;

                struct fence_mark *next;      /**< clockwise, NULL if none    */
        } m1;

        /* Definition of circular region */
        struct fence_circular {               /**< circular fence             */
                struct fence_mark center;
                unsigned int      radius;
        } c1;

        union  fence_boundary {
                struct fence_mark     vertice;
                struct fence_circular acircle;

        } fence;                              /**< fence description          */
};

struct geofencing_params {

        unsigned short       n_regions;       /**< Max = 25                  */
        struct geofence_region *region;       /**< array of n_regions size   */

};

/** Configuration for buffered location fixes */
struct buffered_location {

        /*
           The selection of msr_pos_reports defines the information that are
           published when fix threshold is reached.
        */

        unsigned short int num_fixes; /**< Threshold */
};

/** Information about the HW transaction on the timestamp line to the device */
struct dev_ts_pulse_info {

        unsigned short duration_ms;
        unsigned short assert_qual;
};

/** Information about the HW reference clock provided to the GNSS device */
struct dev_ref_clk_param {

        unsigned int   freq_hz;
        unsigned short quality;
};




#define DEV_CALIB_ENABLE 		0x01
#define DEV_CALIB_DISABLE  		0x00
#define DEV_CALIB_SINGLE_SHOT		0x02
#define DEV_CALIB_PERIODIC		0x00
#define DEV_CALIB_REF_CLOCK		0x00
#define DEV_CALIB_TIME_STAMP		0x04
#define DEV_CALIB_AUTO 			0xff
#define DEV_CALIB_UNDO			0xfe


#define DEV_CALIB_ENABLE_REF_CLCK_SINGLE_SHOT	\
	(DEV_CALIB_ENABLE | DEV_CALIB_SINGLE_SHOT | DEV_CALIB_REF_CLOCK)
	
#define DEV_CALIB_ENABLE_REF_CLCK_PERIODIC	\
	(DEV_CALIB_ENABLE | DEV_CALIB_PERIODIC | DEV_CALIB_REF_CLOCK)

#define DEV_CALIB_ENABLE_TIME_STAMP_SINGLE_SHOT	\
	(DEV_CALIB_ENABLE | DEV_CALIB_SINGLE_SHOT | DEV_CALIB_TIME_STAMP)
	
#define DEV_CALIB_ENABLE_TIME_STAMP_PERIODIC	\
	(DEV_CALIB_ENABLE | DEV_CALIB_PERIODIC | DEV_CALIB_TIME_STAMP)



/** Information to calibrate the GNSS oscillator*/
struct dev_clk_calibrate {

        unsigned char flags;
		unsigned int ref_clk_freq;	/* 1 Hz per bit */
		unsigned short ref_clk_quality; /*0.01 ppm per bit */
};

/** Expected Quality of Position
    @see ti_dev_config_reports()
    @see ti_dev_exec_action()
*/
struct dev_qop_specifier {

        unsigned int accuracy_m;  /**< position accuracy in meters */
        unsigned int max_ttf_ms;  /**< maximal time to fix in msec */
};


/* struct for defination of APM control in receiver */
#define APM_DISABLE 	0 	/* Accuracy: MAX Power: MAX */
#define APM_LEVEL_1 	1 	/* Fast APM */
#define APM_LEVEL_2 	2 	/* Legacy APM */
#define APM_LEVEL_3 	3 	/* F+L APM */  /*Power: MIN Accuracy:MIN*/

struct dev_apm_ctrl {
	unsigned char control;
};
	
/** control to enable/disable the PA blanking feature in the receiver */
#define PA_BLANK_ENABLE 1
#define PA_BLANK_DISABLE 0
struct dev_pa_blank{
	unsigned char control;
};

enum oper_param {

        ts_pulse_info, /**< @see struct dev_ts_pulse_info */
        dev_ref_clock, /**< @see struct dev_ref_clk_param */
        clk_calibrate, /**< @see struct dev_clk_calibrate */
        area_geofence, /**< @see struct geofencing_params & @ref appuser_group */
        buf_loc_fixes, /**< @see struct buffered_location & @ref appuser_group */
        qop_specifier,  /**< @see struct dev_qop_specifier */
        maximize_pdop,
        apm_ctrl, /**< @see struct dev_apm_ctrl */
        pa_blank,      /**< @see struct dev_pa_blank */
        err_recovery,// add for GNSS recovery ++
		/* error trigger test code, to be removed */		
        fatal_err,
        exception_err, // add for GNSS recovery --
        gnss_meas_rep_recovery,
        gnss_no_resp_recovery,
        gnss_hard_reset_recovery,
};

/** Sets up various operational parameters in the device to facilitate services
  and fine tuning of performance
  @param[in] id specifies the parameter that is being configured
  @param[in] value specifies the contents of the parameter being set
 */
int ti_dev_setup_oper_param(const enum oper_param id, void *value);

struct dev_caps {

	unsigned char   gnss_id;     /* GNSS ID */
	unsigned int    ganss_cmn;   /* GANSS Common, not valid for GPS */
	unsigned int    caps_bits;   /* MSA, MSB etc.. */
	unsigned int    asst_bits;   /* Assistance capabilities */
	unsigned char   sigs_bits;   /* Signal capabilities */
	unsigned char   sbas_bits;   /* SBAS capabilities */
};

/* Returns -1 on error otherwise number of elements fetched.
Input: Array and number of elements in the array
 */
int ti_get_dev_caps(struct dev_caps *array, int num);

/*Product line test*/
int ti_dev_plt(const struct dev_plt_param* plt_test);

#endif
