
#ifndef _TI_AGNSS_LCS_H
#define _TI_AGNSS_LCS_H
#ifdef __cplusplus
extern "C" {
#endif

#include "gnss_lcs_common.h" 

/**
   For communication purposes, identifies applications that make use of Location
   Services (LCS) provided by TI's A-GNSS solution
*/
enum ti_lcs_app_enum {

        pform,           /* Platform app  */
        supl2,           /* SUPL  client  */
        cplane           /* Control plane */
};

/** The following construct describes the services (interface) that are offered
    by the TI's AGNSS solution to a source that can provide assistance data  to
    the TI's GNSS device for fast satellite acquisition. 

    In this version of software, the construct does not introduce any property
    in addition to the ones that are inherited. 

    @refer struct agnss_lcs_pvdr_ops
*/
struct ti_agnss_lcs_if : public agnss_lcs_pvdr_ops
{
        /* Refer to parent structure for details */
};

/** The following construct describes the service responses that are presented
    by the TI's AGNSS solution to a GNSS assist source. 

    In this version of software, the construct does not introduce any property
    in additon to the ones that are inherited.

    @refer struct agnss_lcs_user_ops
*/
struct ti_agnss_lcs_cb : public agnss_lcs_user_ops
{
        /* Refer to parent construct for details */
};

/** 
    Initialize and provision TI's AGNSS Location services (LCS).

    @param[in]  app       identifies app that uses LCS
    @param[out] if_holder a placeholder to upload the requested interface
    @param[in]  callbacks a set of functions to be invoked on app by LCS
    
    @return A valid handle to TI's AGNSS Location services or NULL on error

    @note The callee is required to invoke the LCS through the operations 
    uploaded in the interface (IF) place holder.

    @warning No more than one set of services supported by TI's A-GNSS solution
    must be invoked from the context a single process. In particular, TI A-GNSS 
    LCS works independent of other services provided by TI's A-GNSS solution.

*/
void *ti_agnss_lcs_init(ti_lcs_app_enum app, struct ti_agnss_lcs_if& if_holder,
                        const struct ti_agnss_lcs_cb& callbacks);

/** 
    Work in loop to process callbacks provided by TI's AGNSS Location services
    (LCS).

    @param[in] hnd Handle returned by ti_agnss_lcs_init()
    
    @return -1 for errors otherwise 0 for successful halt of processing

    @note Must be invoked from an independent worker thread of the caller. This
    is a blocking call and must be broken by invoking ti_agnss_lcs_halt().

    @see ti_agnss_lcs_halt()

*/

int  ti_agnss_lcs_work(void *hnd);

/** 
    Halt Work Loop that processes the callbacks provided by TI's AGNSS Location
    services (LCS).

    @param[in] hnd Handle that was returned by ti_agnss_lcs_init()

    @return -1 for errors otherwise 0 for successful halt for processing

    @note Must be invoked by the caller to break the work loop.

    @see ti_agnss_lcs_work()

*/

int  ti_agnss_lcs_halt(void *hnd);

/**
   Close and Exit TI's AGNSS Location services (LCS).

   @param[in] hnd Handle that was returned by LCS at initialization

   @return None
*/

void ti_agnss_lcs_exit(void *hnd);


/*------------------------------------------------------------------------------
 *  LOCATION ASSIST SOURCE (standalone)
 *----------------------------------------------------------------------------*/

/**
   Identifies the source of assistance (local and standalone) that are available
   to TI's GNSS device 
*/
enum ti_assist_src_enum {
        
        sa_or_pgps,    /* SA-GPS or PGPS */
};

struct ti_lc_assist_if : public lc_assist_user_ops {

};

struct ti_lc_assist_cb : public lc_assist_pvdr_ops {

};

/**
   Initialize services to facilitate TI's device with external GNSS location (LC)
   assistance data.

   @param[in]  src       identifies the (local) source of assistance 
   @param[out] if_holder a placeholder to upload the requested interface
   @param[in]  callbacks a set of functions to be invoked on assist source
   
   @return A valid handle to TI's GNSS assist receipient or NULL on error
   
   @note The callee is required to invoke the LCS through the operations 
   uploaded in the interface (IF) place holder.
   
   @warning No more than one set of services supported by TI's A-GNSS solution
   must be invoked from the context  a single process. TI GNSS device can work 
   with a GNSS assist source independently of other services provided by TI's 
   A-GNSS solution.
*/
void*
ti_assisted_ue_init(ti_assist_src_enum src, struct ti_lc_assist_if& if_holder,
                    const struct ti_lc_assist_cb& callbacks);
        
/** 
    Work in loop to process callbacks provided by TI's GNSS device to external
    source of assistance data. 

    @param[in] hnd Handle returned by ti_assisted_ue_init()
    
    @return -1 for errors otherwise 0 for successful halt of processing

    @note Must be invoked from an independent worker thread of the caller. This
    is a blocking call and must be broken by invoking ti_assisted_ue_halt().

    @see ti_assisted_ue_halt()
*/
int  ti_assisted_ue_work(void *hnd);

/**
    Halt Work Loop that processes the callbacks provided by TI's GNSS device to
    external source of assistance data.

    @param[in] hnd Handle returned by ti_assisted_ue_init()

    @return -1 for errors otherwise 0 for successful halt for processing

    @note Must be invoked by the caller to break the work loop.

    @see ti_assisted_ue_work()
*/
int ti_assisted_ue_halt(void *hnd);

/**
   Close and Exit communication between TI's GNSS device and external assist 
   source.

   @param[in] hnd the handler that was returned by LCS at initialization
   
   @return None
*/
void ti_assisted_ue_exit(void *hnd);



#ifdef __cplusplus
}
#endif
#endif
