/*
 * File: gnss_lcs_common.h
 *
 * Common constructs between TI A-GNSS framework and external entities.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
 */
#ifndef _GNSS_LCS_COMMON_H_
#define _GNSS_LCS_COMMON_H_

extern "C" {
#include "dev_proxy.h"   // to be removed after re-aligning data structures in gnss.h
}
#include "gnss.h"
#define UNUSED(x) ((void)(&x))

/** 
    This file lists constructs that are common across services of A-GNSS Connect
    and can be utilized to define required interface of associated service. 
*/

/**
    Get cabilities of UE to perform LOC (MSR / POS). 
    
    @param[in]    hnd: abstract handle to object that implements needed behavior  
    @param[out]   gps_caps_bits: bitmap, UE outlines supported GPS capabilities
    @param[inout] ganss_caps: array, UE outlines supported GANSS capabilities

    @return 0 on success else -1 on error

    @note as input struct ganss_ue_lcs_caps and nested members carries the number
    (max) of elements in corresponding array. Where as, as output, implementation
    must specify the actual number of populated elements in respective arrays.
*/

#define UE_LCS_CAP_GPS_AUTO    UE_LCS_CAP_GNSS_AUTO   /* From gnss.h */
#define UE_LCS_CAP_AGPS_MSR    UE_LCS_CAP_AGNSS_MSR   /* From gnss.h */
#define UE_LCS_CAP_AGPS_POS    UE_LCS_CAP_AGNSS_POS   /* From gnss.h */

typedef int 
(*ti_gnfn_get_ue_loc_caps)(void *hnd, unsigned int& gps_caps_bits, 
                           struct ganss_ue_lcs_caps *ganss_caps); 

        
/**
   Get capabilities of UE to interwork Assisted-GNSS.
   
   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  gps_caps_bits: bitmap, UE outlines supported A-GPS capabilities
   @param[inout] ganss_caps: array, UE outlines supported A-GANSS capabilities

   @return 0 on success else -1 on error

   @note as input struct ganss_ue_lcs_caps and nested members carries the number
   (max) of elements in corresponding array. Where as, as output, implementation
   must specify the actual number of populated elements in respective arrays.
*/
typedef int 
(*ti_gnfn_get_ue_agnss_caps)(void *hnd, unsigned int& gps_caps_bits, 
                             struct ganss_ue_assist_caps *ganss_caps); 

/**
   Set reporting interval in UE for LOC (MSR / POS).

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]     interval_secs: LOC reporting interval in seconds.

   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_set_loc_interval)(void *hnd, unsigned int interval_secs);


/* API(s) to support external assist and manage device aiding */

enum assist_type_select {

        NTV_ICD,
        NNATIVE
        
};

enum nnative_ie {

        glo_3gpp_eph,
        glo_3gpp_alm,
        glo_3gpp_utc

};

struct nw_assist_id {
        
        enum assist_type_select         type_select;
        
        union {
                enum loc_assist         gnss_icd_ie;
                enum nnative_ie         n_native_ie;
        };
};

enum cw_test_rep {
	ecwt
};
/**
   Add stated (external) assistance to UE.

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  assist: identifies the type of GNSS assistance 
   @param[in] assist_array: array of information for specified GNSS assistance
   @param[in] num: number of elements in the array of GNSS assistance information

   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_ue_add_assist)(void *hnd, const struct nw_assist_id& id,
                         const void *assist_array, int num); 

/**
   Delete stated aiding information from UE.

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  assist: identifies the type of GNSS aiding / assistance
   @param[in] sv_id_map: bitmap to ID satellites for the specified GNSS aiding
 
   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_ue_del_aiding)(void *hnd, enum loc_assist assist, 
                         unsigned int sv_id_map,
                         unsigned int mem_flags);   

/**
   Get specified aiding information from UE.
   
   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  assist: identifies the type of GNSS aiding / assistance
   @param[inout] assist_array: array of information for specified GNSS aiding
   @param[in] num: number of elements in the array of GNSS aiding information

   @return 0 on success else -1 on error

   @note as input assist_array provides place holder to implementation to upload
   aiding data.
*/
typedef int 
(*ti_gnfn_ue_get_aiding)(void *hnd, enum loc_assist assist, 
                         void *assist_array, int num);

/**
   Start operation of device for LOC.
   
   @param[in]   hnd: abstract hadnle to object that implements needed behavior
   
   @return 0 on success else -1 on error
*/
typedef int (*ti_gnfn_loc_start)(void *hnd);

/**
   Stop LOC operation 
   
   @param[in]   hnd: abstract hadnle to object that implements needed behavior
   
   @return 0 on success else -1 on error
*/ 
typedef int (*ti_gnfn_loc_stop)(void *hnd); 

/**
   Get information about needed assistance from UE.
   
   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[out]  gps_assist: UE outlines needed GPS assistance information
   @param[inout] ganss_assist: array, UE outlines needed GANSS assistance info
   @param[in]    max_ganss: indicates num of GANSS(es) (elemets) in ganss_assist
   
   @return 0 on success else -1 on error

   @note as input struct ganss_assist_req and nested members carries the number
   (max) of elements in corresponding array. Where as, as output, implementation
   must specify the actual number of populated elements in respective arrays.
*/
typedef int 
(*ti_gnfn_get_ue_req4assist)(void *hnd, struct gps_assist_req& gps_assist,
                             struct ganss_assist_req *ganss_assist);


/**
   Delivers LOC results for the previous GNSS interval
   
   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[out] pos_result: POS information as established by UE
   @param[out] gps_result: GPS MSR Result as established by UE
   @param[out] gps_msr: GPS MSR Info as detected by UE for valid gps_result
   @param[out] ganss_result: GANSS MSR Result as established by UE
   @param[out] ganss_msr: GANSS MSR Info as detected by UE for valid ganss_result
   
   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_ue_loc_results)(void *hnd, const struct gnss_pos_result& pos_result,
                          const struct gps_msr_result&    gps_result, 
                          const struct gps_msr_info      *gps_msr,
                          const struct ganss_msr_result&  ganss_result,
                          const struct ganss_msr_info    *ganss_msr);

/**struct assist_requester_info gnss_lcs_common.h
   Assist Requester Information

   This contruct is primarily relevant for entities that can act as both 
   a LOC requester and a assistance provider. Specifically, a  requester
   is identified in terms of "self" or "others". An assertion of "Others"
   would mean that a request for assist has been invoked on this enitity
   as a follow up to LOC REQ from some "other" entity.
*/

struct assist_reference {
        
        /* Requester is a client other than the associated one. */
	bool is_client_others; 
};

/**
   Provides LOC results established by Network enitity in an UE assisted case 

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  pos_result: POS information as established by network
   
   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_nw_loc_results)(void *hnd, 
			const struct gnss_pos_result& pos_result,
			const struct assist_reference& assist_ref);

/**
   Indicates that UE needs specified assistance to perform LOC

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  gps_assist: UE outlines needed GPS assistance information
   @param[in] ganss_assist: UE outlines needed GANSS assistance info

   @return 0 on success else -1 on error
*/
/*  Need to include QoP and Reference POS */
typedef int 
(*ti_gnfn_ue_need_assist)(void *hnd, 
                          const struct gps_assist_req& gps_assist,
                          const struct ganss_assist_req& ganss_assist,
                          const struct assist_reference& assist_ref);

/**
   UE publishes NMEA reports for the previous GNSS interval

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  nmea: Identifies the NMEA sentence
   @param[in]  data: The NMEA data
   @param[in]   len: Length of NMEA data
   
   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_ue_nmea_report)(void *hnd, enum nmea_sn_id nmea, 
                          const char *data, int len);

/**
   UE publishes aiding information decoded by GNSS device

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]  assist: identifies the type of GNSS aiding / assistance
   @param[inout] assist_array: array of information for specified GNSS aiding
   @param[in] num: number of elements in the array of GNSS aiding information

   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_ue_decoded_aid)(void *hnd, enum loc_assist assist, 
                          const void *assist_array, int num);

 /**
	UE publishes the production line test results
 
	@param[in]	   hnd: abstract handle to object that implements needed behavior
	@param[in]	cwt: identifies the cw test event
	@param[inout] cw_rep : test results
	@param[in] num: num of elements 
 
	@return 0 on success else -1 on error
 */
 typedef int (*ti_gnfn_ue_cw_test_results)(void *hnd, enum cw_test_rep 	cwt, 
						   const struct cw_test_report *cw_rep, int num);



 /**
	UE publishes the production line test results
 
	@param[in]	   hnd: abstract handle to object that implements needed behavior
	@param[in]	err_ind: identifies the type of error recovery
	@param[in] num: num of elements 
 
	@return 0 on success else -1 on error
 */

 typedef int (*ti_gnfn_ue_recovery_ind)(void *hnd, enum dev_reset_ind err_ind, 
						   int num);


 /**
Init operation 
   
   @param[in]   hnd: abstract hadnle to object that implements needed behavior
   
   @return 0 on success else -1 on error
*/ 
typedef int (*ti_gnfn_devinit)(void *hnd); 

/**
   Cleanup operation 
   
   @param[in]   hnd: abstract hadnle to object that implements needed behavior
   
   @return 0 on success else -1 on error
*/ 
typedef int (*ti_gnfn_cleanup)(void *hnd); 
 


/**
Product line test  operation 
	
	@param[in]	 hnd: abstract hadnle to object that implements needed behavior
	@param[in]	 plt_test:  product line test parameters.
	
	@return 0 on success else -1 on error
 */ 
typedef int (*ti_gnfn_devplt)(void *hnd, const struct plt_param& plt_test); 



enum acc_type {  /* Accuracy type */

        no_acc,
        acc_2d,
        acc_3d
};

/* Move this structure gnss.h */
struct gnss_qop {
        
        enum acc_type  acc_sel;      /* Accuracy type      */

        unsigned int   h_acc_M;      /* Hor Acc in Meters  */
        unsigned int   v_acc_M;      /* Ver Acc in Meters  */

        unsigned short max_age;      /* POS age in Seconds */
        
	unsigned short rsp_secs;     /* Response time  */
};

/**
   Inform UE about app's intended or desired Quality of Position

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in] qop_req: intended quality of position (may or may not be acheived)
   
   @return 0 on success else -1 on error
*/
typedef int
(*ti_gnfn_set_intended_qop)(void *hnd, const struct gnss_qop& qop_req);

/* Type flags */
#define UE_GNSS_AUTO  UE_LCS_CAP_GNSS_AUTO  /* From gnss.h */
#define UE_AGNSS_POS  UE_LCS_CAP_AGNSS_POS  /* From gnss.h */
#define UE_AGNSS_MSR  UE_LCS_CAP_AGNSS_MSR  /* From gnss.h */

/* Definition as per 3GPP specifications 25.331 and 44.031 (Table: A.40)   */
#define GANSS_ID_SBAS   0x00
#define GANSS_ID_MGPS   0x01
#define GANSS_ID_QZSS   0x02
#define GANSS_ID_GLO    0x03
#define GANSS_ID_NONE   0xFF     /* Private defintion for implemementation */

/* LOC / POS Methods -->  3GPP specifications 25.331 and 44.031 (A.2.2.1a) */
#define GNSS_GPS_PMTHD_BIT   0x0001
#define GNSS_GLO_PMTHD_BIT   0x0020

#define GNSS_ERR_PMTHD_BIT   0x0000 /* Error */

struct loc_instruct {

        unsigned int     type_flags;  // Refer to type flags above

        //enum   loc_type  typeof_loc;  // MSA or MSB, validate against capabilities
        struct gnss_qop  qop_intent;  // QOP Desired; from gnss.h
        unsigned short   lc_methods;  // GNSS, validate against capabilities
        
        bool             vel_needed;  // Velocity needed, validate against capabilities
};

/**
   Statement of instruction from app specifying target properties for LOC output

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]     req: intended instruction (may or may not be acheived) for LOC
   
   @return 0 on success else -1 on error
*/
typedef int
(*ti_gnfn_set_loc_instruct)(void *hnd, const struct loc_instruct& req);


struct rpt_criteria {

        unsigned int  interval;  /* Interval in seconds */
};

/**
   Statement of instruction from app specifying target properties for UR Reports

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]     rpt: intended instruction (may or may not be acheived) for RPT 
   
   @return 0 on success else -1 on error
*/
typedef int
(*ti_gnfn_set_rpt_criteria)(void *hnd, const struct rpt_criteria& rpt);

struct dev_gnss_sat_aux_desc {

        unsigned char      num_sats;     /* Number of sats   */

        unsigned int       eph_bits;     /* SV ID Mask EPH   */
        unsigned int       alm_bits;     /* SV ID Mask ALM   */
        unsigned int       pos_bits;     /* SV ID Mask POS   */
		unsigned char 	   num_sbas_sats;	
};

struct dev_gnss_sat_aux_info {

        int             prn;
        unsigned short  snr_db_by10;     /* SNR: unit 0.1 db */
        unsigned short  azimuth_deg;     /* In degrees       */
        short           elevate_deg;     /* In degrees       */
		/*Added for the spur detection*/
		float			rssi_dbm;
		unsigned char meas_qual;
		unsigned short meas_flags;
};

/**
   Device provides auxilliary information about satellites to user application

   @param[in]      hnd: abstract handle to object that implements needed behavior
   @param[in] sat_desc: description of sat info provided in accompanying contruct
   @param[in] sat_info: array of sat info; size of array defined in descriptor
   
   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_ue_aux_info4sv)(void *hnd,
                          const struct dev_gnss_sat_aux_desc&  gps_desc,
                          const struct dev_gnss_sat_aux_info  *gps_info,
                          const struct dev_gnss_sat_aux_desc&  glo_desc,
                          const struct dev_gnss_sat_aux_info  *glo_info);


/** 
    Indicates that source has completed provisioning of assist information

    @param[in]     hnd: abstract handle to object that implements need behavior

    @return 0 on success else -1 on error
*/
typedef int (*ti_gnfn_assist2ue_done)(void *hnd, 
                                      const struct assist_reference& assist_ref);

enum ue_service_ind {
        
        e_ue_service_resume,      /**< UE now restores its suspended services*/
        e_ue_service_cancel,      /**< UE has cancelled & suspended services */

        e_assist_need_ended,      /**< Device has accumulated needed assists */
        e_assist_need_abort       /**< LOC Request has been aborted  mid-way */
};

/**
   Commands source to move its state and / or update about assistance in device

   @param[in]      hnd: abstract handle to object that implements needed behavior
   @param[in]      cmd: command to source of assistance

   @return 0 on success else -1 on error
*/

typedef int (*ti_gnfn_ue_service_ctl)(void *hnd, enum ue_service_ind ind,  
                                      const struct assist_reference& assist_ref);

struct ue_tcxo_state_desc {

        enum tcxo_state {

                e_status_rpt,
                e_need_calib

        } state;
};

struct ue_tcxo_calib_desc {

        enum calib_oper {
                e_calib_enbl,       /* Enable or start */
                e_calib_dsbl,       /* Disabe or stop  */
                e_calib_undo        /* Undo prev calib */
        } oper;

        enum calib_mode {
                e_one_shot,
                e_periodic
        } mode;

        enum calib_base {
                e_ref_clk,           /* Use Ref  Clock */
                e_ts_info            /* Use Time stamp */
        } base;

        union {           /* Selection depends on base */
                struct ts_pulse_info {
                        unsigned short duration_ms;
                        unsigned short assert_qual;
                } pulse;

                struct ref_clk_param {
                        unsigned int   freq_hz;
                        unsigned short quality;
                } clock;
        } data;
};

enum ue_dev_param_id {

      e_ue_tcxo_calib, /**< Refer to struct ue_tcxo_calib_desc; into dev */
      e_ue_tcxo_state,  /**< Refer to struct ue_tcxo_state_desc; from dev*/
	  e_ue_pos_residuals,  /*< Added this to report the position residuals*/
	  e_ue_reset_err_ind  /*<Indicate to application that there was a reset*/
};

typedef int (*ti_gnfn_ue_dev_oper_param)(void *hnd, enum ue_dev_param_id id,
                                         void *param_desc, int num);

/**
   Declares device specific information or indication or condition

   @param[in]     hnd: abstract handle to object that implements needed behavior
   @param[in]      id: identifies the paramter
   @param[in] param_desc: data related to parameter (can be array)
   @param[in]     num: size of elems in the param_desc array

   @return 0 on success else -1 on error
*/
typedef int 
(*ti_gnfn_decl_dev_param)(void *hnd, enum ue_dev_param_id id, 
                          void *param_desc,          int num);


/**\struct agnss_lcs_pvdr_ops "gnss_lcs_common.h"

   This construct defines the set of operations that can be invoked on provider
   of location service (LCS) to get Position and Measurement information.
*/
struct agnss_lcs_pvdr_ops {

        /** Handle provided by "TI A-GNSS Connect" to be included in operation */
        void                      *hnd;

        ti_gnfn_loc_start          loc_start;        /**< Start LOC operation  */
        ti_gnfn_loc_stop           loc_stop;         /**< Stop  LOC operation  */

        ti_gnfn_get_ue_loc_caps    get_ue_loc_caps;  /**< Get UE LOC abilities */

        /** Get capabilities of UE to support external GNSS assistance */
        ti_gnfn_get_ue_agnss_caps  get_ue_agnss_caps;/**< Assist handled by UE */ 

        /** API(s) to support LOC configuration from applications */
        ti_gnfn_set_loc_instruct   set_loc_instruct; /**< Spell LOC properties */
        ti_gnfn_set_rpt_criteria   set_rpt_criteria; /**< Spell RPT properties */

        /* API(s) to support external assist and manage device aiding */
        ti_gnfn_ue_add_assist      ue_add_assist;    /**< Add assist from NW   */
        ti_gnfn_ue_del_aiding      ue_del_aiding;    /**< Del aiding from UE   */ 
        ti_gnfn_ue_get_aiding      ue_get_aiding;    /**< Get aiding from UE   */

        ti_gnfn_assist2ue_done     assist2ue_done;   /**< Completed assist2UE  */
        
        ti_gnfn_get_ue_req4assist  get_ue_req4assist;/**< Identify reqd assist */

        //ti_gnfn_nw_pos_request     nw_pos_request;   /**< UE MSR: POS from Net */
        ti_gnfn_nw_loc_results     nw_loc_results;   /**< POS estbl by Network */
		ti_gnfn_ue_dev_oper_param  oper_ue_dev_param;
	 	ti_gnfn_devinit				dev_init;
		ti_gnfn_cleanup 			ue_cleanup;
	 	ti_gnfn_devplt				dev_plt;
 };

/**\struct agnss_lcs_user_ops "gnss_lcs_common.h"

   This construct defines the set of operations that can be invoked on user of
   location service (LCS) to report Position and Measurement information.
*/
struct agnss_lcs_user_ops {

        /** Handle provided by "TI A-GNSS Connect" to be included in operation */
        void                      *hnd;

        ti_gnfn_ue_need_assist     ue_need_assist; /**< UE needs spelt assist */
        ti_gnfn_ue_decoded_aid     ue_decoded_aid; /**< UE decoded LOC aiding */
        ti_gnfn_ue_loc_results     ue_loc_results; /**< LOC info worked by UE */
        ti_gnfn_ue_nmea_report     ue_nmea_report; /**< NMEA sentence from UE */

        ti_gnfn_ue_aux_info4sv     ue_aux_info4sv; /**< UE given Aux SAT info */
        ti_gnfn_ue_service_ctl      ue_service_ctl; /**< TBD */
        ti_gnfn_decl_dev_param     decl_dev_param; /**< Declares device param */
		ti_gnfn_ue_cw_test_results cw_test_results;
		ti_gnfn_ue_recovery_ind    ue_recovery_ind;
};

/**\struct lc_assist_user_ops "gnss_lcs_common.h"

   This construct defines the set of operations that can be invoked on user of
   GNSS location (LC) assist to fetch assistance data for fast SAT acquisition.
*/
struct lc_assist_user_ops {

        /** Handle provided by TI "A-GNSS Connect" to be included in operation */
        void                      *hnd;

        /* API(s) to support external assist and manage aiding in GNSS device  */
        ti_gnfn_ue_add_assist      ue_add_assist;    /**< Add assist from NW   */
        ti_gnfn_ue_del_aiding      ue_del_aiding;    /**< Del aiding from UE   */ 
        ti_gnfn_ue_get_aiding      ue_get_aiding;    /**< Get aiding from UE   */

        ti_gnfn_assist2ue_done     assist2ue_done;   /**< Completed assist2UE  */
};

/**\struct lc_assist_pvdr_ops "gnss_lcs_common.h"

   This construct defines the set of operations that can be invoked on provider
   or source of GNSS location (LC) assist.
*/
struct lc_assist_pvdr_ops {

        /** Handle provided by TI "A-GNSS Connect" to be included in operation */
        void                      *hnd;

        ti_gnfn_ue_need_assist     ue_need_assist; /**< UE needs spelt assist */
        ti_gnfn_ue_decoded_aid     ue_decoded_aid; /**< UE decoded LOC aiding */
        ti_gnfn_ue_loc_results     ue_loc_results; /**< LOC info worked by UE */

        ti_gnfn_ue_service_ctl     ue_service_ctl; /**< TBD */
        ti_gnfn_decl_dev_param     decl_dev_param; /**< Declares device param */
		ti_gnfn_ue_cw_test_results cw_test_results;
		ti_gnfn_ue_recovery_ind	   ue_recovery_ind;
};

/** Add documentation */
typedef int (*gnss_adapter_load_fn)(void*);

#endif
