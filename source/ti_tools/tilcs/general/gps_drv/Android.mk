LOCAL_PATH:= $(call my-dir)

# To build  gps_drv.ko
# create a fake dependency and build it locally thru the script file
include $(CLEAR_VARS)

LOCAL_MODULE := ti_gnss_ko
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(PRODUCT_OUT)
LOCAL_UNSTRIPPED_PATH := $(PRODUCT_OUT)

ifneq ($(strip $(LOCAL_SRC_FILES)),)
$(error LOCAL_SRC_FILES are not allowed for phony packages)
endif

LOCAL_MODULE_CLASS := FAKE
LOCAL_MODULE_SUFFIX := -timestamp

include $(BUILD_SYSTEM)/base_rules.mk

$(LOCAL_BUILT_MODULE):
	export ARCH=arm
	$(hide) echo "Fake: $@"
	cd $(MYDROID)/external/location/tilcs/general/gps_drv; ./tignss.sh;
	mkdir -p $(PRODUCT_OUT)/system/lib/modules
	cp $(MYDROID)/external/location/tilcs/general/gps_drv/gps_drv.ko $(PRODUCT_OUT)/system/lib/modules
