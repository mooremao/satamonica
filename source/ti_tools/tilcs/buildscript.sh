#!/bin/sh
export ARCH=arm
export CROSS_COMPILE=arm-arago-linux-gnueabi-
export KERNEL_PATH=$PWD/../ipnc_psp_arago/kernel/
export PATH=$PATH:$PWD/../linux_devkit/bin
mkdir -p libs
mkdir -p bins
mkdir -p data/gnss
cd ./data/gnss
mkdir -p config
mkdir -p nvs
mkdir -p logs
mkdir -p patch
cd -
echo "************************************************************"
echo "* Please set the CROSS_COMPILE environment variable        *"
echo "* Please set the KERNEL_PATH   environment variable        *"
echo "* Please set the cross compiler path before building source*"
echo "*                                                          *"
echo "*                                                          *"
echo "*         Press <ENTER> to confirm....                     *"
echo "************************************************************"
read junk

rm -rf ./libs/*
rm -rf ./bins/*

cd ./infrastructure/log/
make clean;make 
cd -
cd ./dproxy/
make clean;make all
cd -
cd ./connect/
make clean;make all
cd -

# gps_drv now built by whole ipnc lsp build
#cd ./general/gps_drv/
#make clean;make
#cd -

cd ./ti-bt-uim/
make clean;make
cd -

cp -rf ./general/config/Connect_Config.txt ./data/gnss/config
cp -rf ./general/config/dproxy_linux.conf ./data/gnss/config/dproxy.conf
cp -rf ./general/devicepack/patch/* ./data/gnss/patch
#cp -rf ./general/gps_drv/gps_drv.ko ./bins
cp -rf ./ti-bt-uim/uim ./bins


