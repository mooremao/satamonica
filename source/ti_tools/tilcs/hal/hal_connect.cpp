/*
   Not compiled and for illustration only.

   This file attempts to summarize the function blocks that are required
   to integrate TI GPS driver into the framework
 */


#include <stdint.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <properties.h>

#include <sched.h>

#include <gps.h>
#include <device.h>
#include <gnss.h>
#include <dev_proxy.h>
#include <debug.h>
#include <Ftrace.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/wait.h>

#include <utils/Log.h>
#include <sys/time.h>

#include <ti_agnss_lcs.h>

#include <cutils/properties.h>
#include "a-gnss_xact.h"
#include <log_socket.h>

#include <gnss_utils.h>
#include <os_services.h>

#include <sys/poll.h>

#include <ctype.h>

#ifdef TI_PPS_ENABLE
#include <timepps.h>

#endif

#ifdef TI_OBDII_ENABLE
//#include <ftdi.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>

#endif


#ifdef TI_DR_ENABLE
#include <a3g4250d.h>
#include <i2c-dev.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <fcntl.h>


#include <gnssIface.h>
#include <drIface.h>


#endif

#ifdef TI_CAN_ENABLE

#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <sys/uio.h>

extern "C"
{
	#include "terminal.h"

	#include <lib.h>
}

#define MAXSOCK 16    /* max. number of CAN interfaces given on the cmdline */
#define MAXIFNAMES 30 /* size of receive name index to omit ioctls */
#define MAXCOL 6      /* number of different colors for colorized output */
#define ANYDEV "any"  /* name of interface to receive from any CAN interface */
#define ANL "\r\n"    /* newline in ASC mode */

#define SILENT_INI 42 /* detect user setting on commandline */
#define SILENT_OFF 0  /* no silent mode */
#define SILENT_ANI 1  /* silent mode with animation */
#define SILENT_ON  2  /* silent mode (completely silent) */

#define BOLD    ATTBOLD
#define RED     ATTBOLD FGRED
#define GREEN   ATTBOLD FGGREEN
#define YELLOW  ATTBOLD FGYELLOW
#define BLUE    ATTBOLD FGBLUE
#define MAGENTA ATTBOLD FGMAGENTA
#define CYAN    ATTBOLD FGCYAN

const char col_on [MAXCOL][19] = {BLUE, RED, GREEN, BOLD, MAGENTA, CYAN};
const char col_off [] = ATTRESET;

static char *cmdlinename[MAXSOCK];
static __u32 dropcnt[MAXSOCK];
static __u32 last_dropcnt[MAXSOCK];
static char devname[MAXIFNAMES][IFNAMSIZ+1];
static int  dindex[MAXIFNAMES];
static int  max_devname_len; /* to prevent frazzled device name output */
const int canfd_on = 1;

#define MAXANI 4
const char anichar[MAXANI] = {'|', '/', '-', '\\'};
const char extra_m_info[4][4] = {"- -", "B -", "- E", "B E"};

extern int optind, opterr, optopt;

static volatile int running = 1;

#define CAN_RESPONSE  0x41    // This is the vehicle speed query 0x41
#define CAN_VEHICLE_SPEED 0x0D

static int CanIf_GetVehicleSpeed(void);
int required_mtu;
struct canfd_frame frame;
int can_sock; /* can raw socket */
static int CanIf_Init(void);

struct sigaction sa_can;
struct sigevent sev_can;
struct itimerspec can_timer;
struct itimerspec can_oitval;
timer_t timerid_can;
static int hal_CAN_timer_start();

int hal_CAN_timer_stop();
void hal_CAN_timer_handler(int );

#endif


#ifdef TI_OBDII_ENABLE


void *elm_327_init(void *param);
/* line properties */


#define VEHICLE_WHEEL_TICK_RATE_HZ  5  // Hz

#define SPEED_CONV_MPH_MS(x)  (float)(x * 0.44704)

#define DPP_CALIB	4.0  // 4cm typically

int obd_fd;
FILE *elm_log_fd;
#define ELM_LOG(...) fprintf(elm_log_fd, __VA_ARGS__)
#define MODEMDEVICE "/dev/ttyO4"
#define BAUDRATE B115200

struct termios oldtio,newtio;



struct sigaction sa_obd;
struct sigevent sev_obd;
struct itimerspec obd_timer;
struct itimerspec obd_oitval;
timer_t timerid_obd;
static int hal_obd2_timer_start();

int hal_obd2_timer_stop();
void hal_obd2_timer_handler(int );
#define MAX_OBD_CMD_LEN 128
#define MAX_OBD_RSP_LEN 128


unsigned char tx[MAX_OBD_CMD_LEN];
unsigned char rx[MAX_OBD_RSP_LEN];
#define VEHICLE_SPEED 0x0D
#define VEHICLE_SPEED_RESPONSE_LENGTH	9


#endif



/*------------------------------------------------------------------------------
 *  Declaration of TI LOGGING constructs
 *----------------------------------------------------------------------------*/
#include "ti_log.h"
#include "log_id.h"

#define PF_OPER_BIN(id, n_objs, data)   TI_LOG_BIN("OPER",  7, id, n_objs, data)
//#define LOGD TI_LOG1

/*------------------------------------------------------------------------------
 *  Declaration of Global HAL constructs
 *----------------------------------------------------------------------------*/
GpsUtcTime g_utc_timestamp;
GpsLocation 		  g_gpsLocation;

static int g_gps_log_ver = 0;

/* 1 km/hr = 0.277778 m/s */
#define SPEED_CONV_KMPH_MS(x) (float)(x * 0.277778)


#ifdef LOGD
#undef LOGD
  #define LOGD  if(g_gps_log_ver)ALOGD
#endif
#define LOGE  if(g_gps_log_ver)ALOGE


#define WEEK_HRS	(7U*24U)
#define WEEK_SECS       (WEEK_HRS*3600U)        /* # of secs in week */
#define WEEK_BITS       (WEEK_SECS*50U)       /* # of bits in week */

#define GPS2UTC 315964800000LLU
#define LEAP_SECS 15000
#define SEC_MSEC 1000

#define VERBOSE_LOGGING 0
#define MAX_CONNECTIONS	      100
#define SOC_NAME_6121		  "/data/gnss/gps6121"
#define SUPL_SOCKT_NAME           "/data/gnss/gps7121"

#ifdef LOG_TAG
  #undef LOG_TAG
  #define LOG_TAG "GNSS_HAL"
#endif

void *ti_hnd;
GpsNiCallbacks  *p_niCallback;
typedef enum {
	NI_RESPONSE_ACCEPT = 1,
	NI_RESPONSE_DENY = 2,
	NI_RESPONSE_NORESP = 3,
}eHAL_NIResp;

typedef struct
{
	eHAL_NIResp resp;
	int client_session_id;
}hal_ni_client_response;
#define MASK_NI_SOCK_FD 0xFFFF0000
#define MASK_NI_SESSION_FD 0x0000FFFF
struct hal_descriptor {

	pthread_t 	  th;
	pthread_t 	  ni_msg_th;

	GpsCallbacks     *gps_callbacks;
	AGpsCallbacks    *agps_callbacks;
	AGpsRilCallbacks *ril_callbacks;
#ifdef TI_DR_ENABLE
	/*Gyro Thread and struct*/

	pthread_t gyro_th;
	pthread_t tmr_th;
	pthread_t DR_th;
	pthread_t hip_th;
	pthread_t obd_th;
#endif

#ifdef TI_CAN_ENABLE
	pthread_t can_th;
#endif

#ifdef TI_PPS_ENABLE
	pthread_t pps_th;
	pthread_t pps_rd_th;

#endif


};

static struct hal_descriptor *hal_desc;

/*---------------------------------------------------------------------------
 *           Support fof the Gyro sensor
 *---------------------------------------------------------------------------*/
#ifdef TI_DR_ENABLE
int gyro_fd;
void *gyro_wait_sem;
static void *hal_read_gyro(void *param);
static bool gyro_read_terminate;   //Read thread terminate
static bool DR_terminate; // Read the samples every 200ms terminate
static signed long cum_gyro_X, cum_gyro_Y, cum_gyro_Z; // cummulative/avg sample 20 samples
static void *hal_DR_timer_start(void *);
void hal_DR_timer_stop();
void hal_DR_timer_handler(int );

void*  hal_DR_caller(void* );
FILE* DR_nvsoutFd;

static unsigned int runn_translvrpos_cnt;


static float g_speed_val_ms;

FILE* fd ;
FILE* hippo_fd;
static int num_samples ;
timer_t timerid;
struct sigaction sa;
struct sigevent sev;

struct itimerval timer;

struct timeval g_oldtime;
struct timeval g_newtime;
int read_time_inSec= 0;
int read_time_inuSec= 0;
unsigned int prev_time = 0;
unsigned int curr_time = 0;
unsigned int curr_dr_process_time = 0;
unsigned int prev_dr_process_time = 0;


static bool g_gyro_timer_running = false;
static bool timer_terminate = false;

void *sig_dr_caller;
void *sig_DR_timer;


void *drdata_mutex_hnd;
void *gnssdata_mutex_hnd;

npEphType   		g_ephtype_gps[NUM_GPS_SVS];
npIonType   		g_ionotype[1];
npTrkMeasType 		g_trkMeas[1];
npChMeasType		g_chMeastype[NUM_CH];
npTimeType		g_gpsTime[1];
npFixType		g_CurrFixType[1];
static unsigned int g_gps_svs_usedInFix;
static unsigned int g_glo_svs_usedInFix;
static unsigned char g_tot_num_SVs;

//npRtcTimeType rtc;
npDrLibInitType   tInit;
npDrDataType      tDrData;
npGnssFixDataType tGnssData;
npMapMatchInputType   tMM;
npDrOutputType    tDrOutput;
npRawNvramType    tDrNVROut;

/* These structures are populated from the ones above before the DR call */
npDrDataType      CL_tDrData;
npGnssFixDataType CL_tGnssData;
npRawNvramType    CL_tDrNVROut;

npVerType         tVersion;

#define DR_VALID (HIPST_POS | HIPST_HDG | HIPST_ZRO | HIPST_DPP | HIPST_GOODDHDG | HIPST_GOODTACH | HIPST_GSF)
#define GYRO_CALIBRATED (HIPST_ZRO | HIPST_GSF)

#define  BUF_SIZE  2048

int hippo_socket_fd = 0;
#define HIPPO_SOCK_PATH 	"/data/gnss/log_MD/hippo_sock"
#define HIPPO_BUF_SZ	8192
char hippo_snd_buf[HIPPO_BUF_SZ];
char hippo_recv_buf[HIPPO_BUF_SZ];

static bool hippo_logger_terminate = false;


const char portNumber[] = "2525";
int  hippo_cli_socket_fd = 0;

static int hippo_thread_exit = 0;




#endif

#ifdef TI_PPS_ENABLE
#define PPS_DEVICE_NAME  "/dev/pps0"
int pps_fd;
void *pps_sig;
void *sig_PPS_timer;


int PPS_init(const char *PPSfilename);
/*PPS device handle returned by time_pps_create()*/
pps_handle_t pps_handle;	  /* handle for PPS sources */
struct timespec pps_timeout;  /* waiting time */
pps_info_t pps_infobuf ;
int getPPS_timestamp(pps_info_t *infobuf);
timer_t pps_timerid;
struct sigaction pps_sa;
struct itimerspec pps_timer;
struct itimerspec pps_oitval;

//static void *hal_PPS_timer_start(void *);
static int hal_PPS_timer_start();

int hal_PPS_timer_stop();
void hal_PPS_timer_handler(int );
static bool pps_timer_terminate =false;
static int PPS_timer_th = -1;
static int PPS_read_th = -1;
void* hal_PPS_Read(void* param);

#endif




/* Add support for remaining functions in GpsCallbacks */

/*------------------------------------------------------------------------------
 *  GPS Framework Xtra Callbacks i.e. GpsXtraCallbacks
 *----------------------------------------------------------------------------*/

/* Add */

/*------------------------------------------------------------------------------
 *  A-GPS Framework Callbacks i.e. AGpsCallbacks
 *----------------------------------------------------------------------------*/

/* Add */

/*------------------------------------------------------------------------------
 *  NI Framework Callbacks i.e. GpsNiCallbacks
 *----------------------------------------------------------------------------*/

/* Add */

/*------------------------------------------------------------------------------
 *  AGPS RIL Framework Callbacks i.e. AGpsRilCallbacks
 *----------------------------------------------------------------------------*/

#define MAX_NI_MSG_SIZE         5120 /* 1024*5 */
#define SUPL_NI_MSG             0x00000001
#define SUPL_SI_TRG_MSG         0x00000002
#define SUPL_SI_REQ3PRTY_MSG    0x00000003
#define SUPL_SI_SND3PRTY_MSG    0x00000004

void ril_init(AGpsRilCallbacks* callbacks);
void ril_set_ref_location(const AGpsRefLocation *agps_reflocation, size_t sz_struct);
void ril_set_set_id (AGpsSetIDType type, const char* setid);
void ril_ni_message (uint8_t *msg, size_t len);
void ril_update_network_state (int connected, int type, int roaming, const char* extra_info);
void ril_update_network_availability(int avaiable, const char* apn);

static int ni_msg_sockt_fd = -1;
static void hal_crt_ni_msg_sockt(void *msg);
void hal_send_NI_message(unsigned char *msg,size_t len);
static void hal_ni_notification_handler(void* argument);

/* Add */
AGpsRilInterface  agps_rilInterface = {
    sizeof(AGpsRilInterface),
    ril_init,
    ril_set_ref_location,
    ril_set_set_id,
    ril_ni_message,
    ril_update_network_state,
    ril_update_network_availability,
};

typedef struct {
    int ni_msg_type;
    unsigned int msg_len;
    unsigned char ni_msg[MAX_NI_MSG_SIZE];
}supl_ni_msg_t;

/*------------------------------------------------------------------------------
 *  Connect device callbacks / indications to Framework HAL callbacks
 *----------------------------------------------------------------------------*/
//#endif
void thread_rx(void *arguement)
{
        ti_agnss_lcs_work(ti_hnd);
}

/*------------------------------------------------------------------------------
 * Core callbacks from device for GPS feature
 *----------------------------------------------------------------------------*/

/*
  Housekeeping for SV Status
*/
struct gps_sat_used_in_fix {

        unsigned int id_flags;
        unsigned int is_valid;
};

static struct gps_sat_used_in_fix gps_sv_used_in_fix = { .id_flags = 0, .is_valid = 0};

static struct gps_sat_used_in_fix *sv_used_in_fix;

unsigned int get_time_msecs();

static pid_t logcat_pid = 0;
unsigned int iteration = 0;
#define LOG_PATH "/data/gnss/logs"
#define LOG_ARCHIVE_PATH "/data/gnss/archive"

void start_logcat()
{	
	pid_t pid;

	pid = fork();
	if (pid == 0) {
		if (execlp("logcat", "logcat", "-f", LOG_PATH"/android.logcat", NULL) < 0)
			ALOGE("Failed to start logcat process with execlp, errno %d", errno);
	} else {
		ALOGD("Child logcat process PID is %d\n", pid);
		logcat_pid = pid;
	}
}

void stop_logcat()
{
	int status;

	ALOGD("Stopping logcat process %d\n", logcat_pid);
	kill(logcat_pid, SIGTERM);
	wait(&status);
	/* erase previous history */
	system("logcat -c");
}

static int log_time(char *buf)
{
	int len = 0;
	struct timeval tv;
	struct tm *timeinfo = NULL;
	char local_buf[50];

	gettimeofday(&tv, NULL);

	timeinfo = localtime(&tv.tv_sec);

	if (timeinfo == NULL)
		return 0;

	memset(local_buf, 0 , sizeof(local_buf));
	strftime(local_buf , 50, "%Y-%m-%d-%H-%M-%s", timeinfo);
	len = snprintf(buf ,50,"%s-%ld", local_buf, (long int)tv.tv_usec);

	return len;
}

static int decoded_aid(void *hnd, enum loc_assist assist,
                       const void *assist_array, int num)
{

        //      unsigned char *wr_data;

    LOGD("decoded_aid: entering");
    unsigned char index = 0;

        switch (assist) {
                case a_GPS_EPH:
                        {

                                struct gps_eph *eph = (struct gps_eph *)assist_array;

								if(g_gps_log_ver){
                                LOGD("###################Decoded GPS Ephemeris ######################## \n");
                                LOGD("decoded_aid:\n gps eph svid [%d]\n, is_bcast [%d]\n, status [%d]\n, Code_on_L2 [%d]\n, Ura [%d]\n, Health [%d]\n, IODC [%d]\n, Tgd [%d]\n, Toc [%d]\n, Af2 [%d]\n, Af1 [%d]\n, Af0 [%d]\n, Crs [%d]\n, DeltaN [%d]\n, Mo [%d]\n, Cuc [%d]\n, E [%d]\n, Cus [%d]\n, SqrtA [%d]\n, Toe [%d]\n, FitIntervalFlag [%d]\n, Aodo [%d]\n, Cic [%d]\n, Omega0 [%d]\n, Cis [%d]\n, Io [%d]\n, Crc [%d]\n, Omega [%d]\n, OmegaDot [%d]\n, Idot [%d]\n",
									eph->svid+1, eph->is_bcast, eph->status, eph->Code_on_L2, eph->Ura, eph->Health, eph->IODC, eph->Tgd, eph->Toc, eph->Af2, eph->Af1, eph->Af0, eph->Crs, eph->DeltaN, eph->Mo, eph->Cuc, eph->E, eph->Cus, eph->SqrtA, eph->Toe, eph->FitIntervalFlag, eph->Aodo, eph->Cic, eph->Omega0, eph->Cis, eph->Io, eph->Crc, eph->Omega, eph->OmegaDot, eph->Idot);
									}
				index =  eph->svid;

#ifdef TI_DR_ENABLE
				g_trkMeas->ucTowStat 			= TIME_FROM_SV;

				os_mutex_take(gnssdata_mutex_hnd);
				tGnssData.atEph[index].ucSvId 		= eph->svid + 1;
				tGnssData.atEph[index].ucCodeL2 		= eph->Code_on_L2;
				tGnssData.atEph[index].ucL2Pdata 		= 0;  //Hardcoding to zero
				tGnssData.atEph[index].ucURA 			= eph->Ura;
				tGnssData.atEph[index].ucSvHealth 	= eph->Health;
				tGnssData.atEph[index].cTgd 			= eph->Tgd;
				tGnssData.atEph[index].cAf2 			= eph->Af2;
				tGnssData.atEph[index].ucFitInterval 	= 0; //Hardcoding to zero
				tGnssData.atEph[index].usWeekNo 		= g_gpsTime->usWeek;
				tGnssData.atEph[index].usToc 			= eph->Toc;
				tGnssData.atEph[index].usToe 			= eph->Toe;
				tGnssData.atEph[index].sAf1 			= eph->Af1;
				tGnssData.atEph[index].sDeltaN 		= eph->DeltaN;
				tGnssData.atEph[index].sCrs 			= eph->Crs;
				tGnssData.atEph[index].sCrc 			= eph->Crc;
				tGnssData.atEph[index].sCus 			= eph->Cus;
				tGnssData.atEph[index].sCuc 			= eph->Cuc;
				tGnssData.atEph[index].sCis 			= eph->Cis;
				tGnssData.atEph[index].sCic 			= eph->Cic;
				tGnssData.atEph[index].sIdot 			= eph->Idot;
				tGnssData.atEph[index].lAf0 			= eph->Af0;
				tGnssData.atEph[index].lM0 			= eph->Mo;
				tGnssData.atEph[index].ulE 			= eph->E;
				tGnssData.atEph[index].ulSqrtA 		= eph->SqrtA;
				tGnssData.atEph[index].lOmega0 		= eph->Omega0;
				tGnssData.atEph[index].li0 			= eph->Io;
				tGnssData.atEph[index].lOmega 		= eph->Omega;
				tGnssData.atEph[index].lOmegaDot 		= eph->OmegaDot;
				//tGnssData.tMeas.usChOrbFlag = (tGnssData.tMeas.usChOrbFlag) | CHST_SORB;
				os_mutex_give(gnssdata_mutex_hnd);
#endif
//                                LOGD("###################Decoded GPS Ephemeris ######################## \n");

                                }
                        break;

                case a_GPS_TIM:
                        {


                                struct gps_ref_time *tim = (struct gps_ref_time *)assist_array;


#ifdef TI_DR_ENABLE
				g_gpsTime->ulTowMs = tim->time.tow;
				g_gpsTime->usWeek= tim->time.week_nr + (1024 * tim->time.n_wk_ro);
				g_gpsTime->fltUnc   = tim->time_unc / 1000.0f;   //Value to be in millisec for DR
				//g_gpsTime->ulTimeTick =  (U32)get_time_msecs();
				g_gpsTime->ucTStatus = TIME_FROM_SV;
				os_mutex_take(gnssdata_mutex_hnd);
				if(	tGnssData.tMeas.ucTowStat < TIME_FROM_FIX){
					tGnssData.tMeas.ucTowStat = TIME_FROM_SV;
					}

				os_mutex_give(gnssdata_mutex_hnd);
#endif
							if(g_gps_log_ver){

								LOGD("###################Decoded GPS TIME  ######################## \n");
								LOGD("decoded_aid: gps time week_nr [%d] : msec [%d]  \n",
										  tim->time.week_nr + (1024 * tim->time.n_wk_ro),
										  tim->time.tow);

								LOGD("decoded_aid: gps time n_wk_ro [%d]  \n", (1024 * tim->time.n_wk_ro));
								LOGD("###################Decoded GPS TIME  ######################## \n");
							}


                        }
                        break;

                case a_GLO_EPH:
                        {

                                struct glo_eph *eph = (struct glo_eph *)assist_array;

								if(g_gps_log_ver){
	                                LOGD("###################Decoded GLONASS Ephemeris  ######################## \n");
	                                LOGD("decoded_aid: glo eph the_svid [%d]\n, is_bcast [%d]\n, m [%d]\n, tk [%d]\n, tb [%d]\n, M [%d]\n, Gamma [%d]\n, Tau [%d]\n, Xc [%d]\n, Yc [%d]\n, Zc [%d]\n, Xv [%d]\n, Yv [%d]\n, Zv [%d]\n, Xa [%d]\n, Ya [%d]\n, Za [%d]\n, P [%d]\n, NT [%d]\n, n [%d]\n, FT [%d]\n, En [%d]\n, Bn [%d]\n, P1 [%d]\n, P2 [%d]\n, P3 [%d]\n, P4 [%d]\n, Delta_Tau [%d]\n, ln [%d]\n",
										eph->the_svid, eph->is_bcast, eph->m, eph->tk, eph->tb, eph->M, eph->Gamma, eph->Tau, eph->Xc, eph->Yc, eph->Zc, eph->Xv, eph->Yv, eph->Zv, eph->Xa, eph->Ya, eph->Za, eph->P, eph->NT, eph->n, eph->FT, eph->En, eph->Bn, eph->P1, eph->P2, eph->P3, eph->P4, eph->Delta_Tau, eph->ln);
	                                LOGD("###################Decoded GLONASS Ephemeris  ######################## \n");
								}
                        }
                        break;
				case a_GPS_ION:
					   {

							   struct gps_ion *ion = (struct gps_ion*)assist_array;
							   if(g_gps_log_ver){
								   LOGD("###################Decoded GPS Iono  ######################## \n");
								   LOGD("decoded_aid: iono : Alpha0 [%X]\n Alpha1 [%X]\n Alpha2 [%X]\n Alpha3 [%X]\n Beta0 [%X]\n Beta1 [%X]\n Beta2 [%X]\n Beta3 [%X]\n", ion->Alpha0,ion->Alpha1,ion->Alpha2,ion->Alpha3, ion->Beta0, ion->Beta1, ion->Beta2 ,ion->Beta3);

								   LOGD("###################Decoded GPS IONO  ######################## \n");
							   	}
#ifdef TI_DR_ENABLE
				os_mutex_take(gnssdata_mutex_hnd);
				tGnssData.tIono.cAlpha0 	= ion->Alpha0;
				tGnssData.tIono.cAlpha1 	= ion->Alpha1;
				tGnssData.tIono.cAlpha2 	= ion->Alpha2;
				tGnssData.tIono.cAlpha3 	= ion->Alpha3;
				tGnssData.tIono.cBeta0 	= ion->Beta0;
				tGnssData.tIono.cBeta1 	= ion->Beta1;
				tGnssData.tIono.cBeta2 	= ion->Beta2;
				tGnssData.tIono.cBeta3 	= ion->Beta3;
				os_mutex_give(gnssdata_mutex_hnd);
#endif
					   }
					   break;

                default:
                        break;
        }


        return 0;
}

static int dev_ue_recovery_ind(void *hnd, enum dev_reset_ind rstind, int num)
{

	LOGD("dev_ue_recovery_ind: Entering!");
	switch(rstind){
		case gnss_no_meas_resp_ind :
				LOGD("gnss_no_meas_resp_ind ");
			break;
		
		case gnss_no_reset_resp_ind:
				LOGD("gnss_no_reset_resp_ind ");
			break;

	case gnss_hard_reset_ind:
			LOGD("gnss_hard_reset_ind");
		break;

	case gnss_fw_fatal_ind:
			LOGD("gnss_fw_fatal_ind ");
		break;
		default:
			break;
	}
	return 0;
}


/*   ti_ue_pos_report  location_rpt */
static int dev_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
                           const struct gps_msr_result&    gps_result,
                           const struct gps_msr_info      *gps_msr,
                           const struct ganss_msr_result&  ganss_result,
                           const struct ganss_msr_info    *ganss_msr)
{
	unsigned int index = 0;
//	unsigned long pps_msec = 0;
	// LOGD(" dev_loc_results: Entering \n");
       GpsLocation           gpsLocation;
	GpsLocation *h_loc = &gpsLocation;
	#define PYTHAGORAS_VAL(x,y) sqrt((x*x) + (y*y))
	#define EAST_ER 	      (0.1)
	#define NORTH_ER	      (0.1)
	unsigned int flags = POS_ERR_REPORTED | POS_NOT_REPORTED;

	/*if(pos_result.contents & POS_ERR_REPORTED){
		LOGD("dev_loc_results: ERROR  RPT!!!\n");
		return 0;
	 }*/

	if( pos_result.contents & flags){
		LOGD("dev_loc_results: ERROR  RPT!!!\n");
		return 0;
	 }
        const struct location_desc *location = &pos_result.location;

	 h_loc->size = sizeof(GpsLocation);
         if((location->lat_deg_by_2p8 == 0)&&(location->lon_deg_by_2p8 == 0)){

	 h_loc->latitude   = (double)location->latitude_N * 90.0f/8388608.0f;

	 if(location->lat_sign == 1 )
	 	h_loc->latitude = -h_loc->latitude;

	 h_loc->longitude  = (double)location->longitude_N * 360.0f/16777216.0f;
        h_loc->flags     |= GPS_LOCATION_HAS_LAT_LONG;
        h_loc->altitude   = (double)0;

        }else{
	h_loc->latitude = (double) location->lat_deg_by_2p8 * (180 / pow(2,32));
	h_loc->longitude = (double) location->lon_deg_by_2p8 * (180 / pow(2,31));

	h_loc->flags	 |= GPS_LOCATION_HAS_LAT_LONG;

	h_loc->altitude   = (double)0;
        }
#if 0
#define UNC_MAP_K_TO_R(unc_k, C, x) (double)(C * (pow(1+x, unc_k) - 1))

	double unc_east = UNC_MAP_K_TO_R(location->unc_semi_maj_K, 10.0f, 0.1f);
	double unc_north = UNC_MAP_K_TO_R(location->unc_semi_min_K,10.0f, 0.1f);
	h_loc->accuracy = (float) PYTHAGORAS_VAL(unc_east, unc_north);
#endif
	double unc_east = location->east_unc ;
	double unc_north = location->north_unc;
	h_loc->accuracy = (float) PYTHAGORAS_VAL(unc_east, unc_north);
	h_loc->flags |= GPS_LOCATION_HAS_ACCURACY;
	
        if(pos_result.location.shape == ellipsoid_with_v_alt ||
			pos_result.location.shape == ellipsoid_with_v_alt_and_e_unc) {
                h_loc->altitude  = (double)location->altitude_N;
                h_loc->flags    |= GPS_LOCATION_HAS_ALTITUDE;
        }

        /* Include other fields as well. If done, remove this comment */

	#define PYTHAGORAS_VAL(x,y) sqrt((x*x) + (y*y))

        const struct vel_gnss_info *velocity = &pos_result.velocity;

        h_loc->speed    = (float) PYTHAGORAS_VAL(velocity->h_speed,
                                                 velocity->v_speed);
        h_loc->speed	= (h_loc->speed/3600)*1000;
        h_loc->flags   |= GPS_LOCATION_HAS_SPEED;

        h_loc->bearing  = velocity->bearing;
        h_loc->flags   |= GPS_LOCATION_HAS_BEARING;

        //h_loc->accuracy = (float) PYTHAGORAS_VAL(velocity->h_unc, velocity->v_unc);
        //h_loc->flags   |= GPS_LOCATION_HAS_ACCURACY;

	/* Include other fields as well. If done, remove this comment */
if(g_gps_log_ver){
	LOGD("dev_loc_results: ref_secs  [%d]", pos_result.ref_secs);
	LOGD("dev_loc_results: ref_msec  [%d]", pos_result.ref_msec);
	LOGD("dev_loc_results: Latitude  [%lf]", h_loc->latitude);
	LOGD("dev_loc_results: Longitude [%lf]",h_loc->longitude);
	LOGD("dev_loc_results: Altitude  [%lf]", h_loc->altitude);
	LOGD("dev_loc_results: speed  	[%f]", h_loc->speed);
	LOGD("dev_loc_results: bearing  	[%f]", h_loc->bearing);
	LOGD("dev_loc_results: accuracy  [%f]", h_loc->accuracy);
	LOGD("dev_loc_results: accuracy  [%d]", pos_result.fix_type);


	LOGD("/************************Measurement results ************/\n");
}
        int nsat= gps_result.n_sat;
        while(nsat--)
        {

		if(g_gps_log_ver){
             LOGD("svid                      [%10d]\n", gps_msr->svid + 1); // Added 1 because the devproxy has -1 for indexing in array.
             LOGD(" ->c_no                   [%10d]\n", gps_msr->c_no);
             LOGD(" ->snr                    [%10d]\n", gps_msr->snr);
             LOGD(" ->Meas Quality   [%10d]\n", gps_msr->msr_qual);
             LOGD(" ->psedo range    [%10lf]\n", gps_msr->pseudo_msr);
             LOGD(" ->doppler                [%10f]\n", gps_msr->doppler);
             LOGD(" ->psedo range unc[%10f]\n", gps_msr->pseudo_msr_unc);
             LOGD(" ->doppler UNC    [%10f]\n", gps_msr->doppler_unc);
             LOGD(" ->doppler resid  [%10f]\n", gps_msr->doppler_residuals);
             LOGD(" ->pseudo resid   [%10f]\n", gps_msr->pseudo_range_residuals);
             LOGD(" ->Azimuth                [%10d]\n", gps_msr->azimuth_deg);
             LOGD(" ->Elevation      [%10d]\n", gps_msr->elevate_deg);

			 LOGD("/************************Measurement results ************/\n");
			}
#ifdef TI_DR_ENABLE
	os_mutex_take(gnssdata_mutex_hnd);
	if(IS_VALID_GPS_SV(gps_msr->svid + 1)){
		tGnssData.tMeas.atCh[index].ucSvId		= gps_msr->svid + 1;
		tGnssData.tMeas.atCh[index].ucSNR		= gps_msr->snr;
		tGnssData.tMeas.atCh[index].ucCNO		= gps_msr->c_no;
		tGnssData.tMeas.atCh[index].dblPR		= gps_msr->pseudo_msr;
		tGnssData.tMeas.atCh[index].fltPrUnc		= gps_msr->pseudo_msr_unc;
		tGnssData.tMeas.atCh[index].fltDopp		= gps_msr->doppler;
		tGnssData.tMeas.atCh[index].fltDrUnc		= gps_msr->doppler_unc;
		tGnssData.tMeas.atCh[index].ucQual		= (gps_msr->msr_qual == 4) ? GNSS_MEAS_VALID : gps_msr->msr_qual;
		tGnssData.tMeas.atCh[index].usChStatusFlag    = gps_msr->meas_flags;
		tGnssData.tMeas.atCh[index].dblPRResid	= gps_msr->pseudo_range_residuals;
		tGnssData.tMeas.atCh[index].fltDoppResid	= gps_msr->doppler_residuals;
		tGnssData.tMeas.atCh[index].fltAzim		= gps_msr->azimuth_deg;
		tGnssData.tMeas.atCh[index].fltElev		= gps_msr->elevate_deg;
	}
	/*
	tGnssData.tMeas.atCh[index].fltLOS[0]		= sin(gps_msr->azimuth_deg) * cos( gps_msr->elevate_deg);
	tGnssData.tMeas.atCh[index].fltLOS[1]		= cos(gps_msr->azimuth_deg) * sin(gps_msr->elevate_deg);
	tGnssData.tMeas.atCh[index].fltLOS[2]		= sin(gps_msr->elevate_deg);
	tGnssData.tMeas.atCh[index].fltLOS[3]		= 1.0;
	*/
	tGnssData.tMeas.usChOrbFlag = CHST_AZ_EL | CHST_REFPOS;
	tGnssData.tMeas.usChDataFlag =CHST_PR_RESID | CHST_PR_IONO | CHST_PR_TROPO;
	os_mutex_give(gnssdata_mutex_hnd);


		index++;
#endif
             gps_msr++;
         }

         nsat =0;
         //LOGD("ganss_msr n_ganss = %d\n", ganss_result.n_ganss);
         nsat = ganss_result.n_ganss;
         while(nsat--)
          {

		  if(g_gps_log_ver){
	     LOGD("svid                      [%10d]\n", ganss_msr->svid );
             LOGD(" ->c_no                   [%10d]\n", ganss_msr->c_no);
             LOGD(" ->snr                    [%10d]\n", ganss_msr->snr);
             LOGD(" ->Meas Quality   [%10d]\n", ganss_msr->msr_qual);
             LOGD(" ->psedo range    [%10lf]\n", ganss_msr->pseudo_msr);
             LOGD(" ->doppler                [%10f]\n", ganss_msr->doppler);
             LOGD(" ->psedo range unc[%10f]\n", ganss_msr->pseudo_msr_unc);
             LOGD(" ->doppler UNC    [%10f]\n", ganss_msr->doppler_unc);
             LOGD(" ->doppler resid  [%10f]\n", ganss_msr->doppler_residuals);
             LOGD(" ->pseudo resid   [%10f]\n", ganss_msr->pseudo_range_residuals);
             LOGD(" ->Azimuth                [%10d]\n", ganss_msr->azimuth_deg);
             LOGD(" ->Elevation      [%10d]\n", ganss_msr->elevate_deg);
		  	}
#ifdef TI_DR_ENABLE
		os_mutex_take(gnssdata_mutex_hnd);
		if(IS_VALID_GLO_SV(ganss_msr->svid)){
			tGnssData.tMeas.atCh[index].ucSvId            = ganss_msr->svid ;
	        tGnssData.tMeas.atCh[index].ucSNR             = ganss_msr->snr;
	    	tGnssData.tMeas.atCh[index].ucCNO             = ganss_msr->c_no;
	        tGnssData.tMeas.atCh[index].dblPR             = ganss_msr->pseudo_msr;
	    	tGnssData.tMeas.atCh[index].fltPrUnc          = ganss_msr->pseudo_msr_unc;
	        tGnssData.tMeas.atCh[index].fltDopp           = ganss_msr->doppler;
	    	tGnssData.tMeas.atCh[index].fltDrUnc          = ganss_msr->doppler_unc;
	        tGnssData.tMeas.atCh[index].ucQual            = (ganss_msr->msr_qual == 4) ? GNSS_MEAS_VALID : ganss_msr->msr_qual ;
	    	tGnssData.tMeas.atCh[index].usChStatusFlag    = ganss_msr->meas_flags;
	        tGnssData.tMeas.atCh[index].dblPRResid        = ganss_msr->pseudo_range_residuals;
	    	tGnssData.tMeas.atCh[index].fltDoppResid      = ganss_msr->doppler_residuals;
	        tGnssData.tMeas.atCh[index].fltAzim           = ganss_msr->azimuth_deg;
	    	tGnssData.tMeas.atCh[index].fltElev           = ganss_msr->elevate_deg;
		}
		/*
        tGnssData.tMeas.atCh[index].fltLOS[0]         = sin(ganss_msr->azimuth_deg) * cos( ganss_msr->elevate_deg);
    	tGnssData.tMeas.atCh[index].fltLOS[1]         = cos(ganss_msr->azimuth_deg) * sin(ganss_msr->elevate_deg);
        tGnssData.tMeas.atCh[index].fltLOS[2]         = sin(ganss_msr->elevate_deg);
    	tGnssData.tMeas.atCh[index].fltLOS[3]         = 1.0;
    	*/
		tGnssData.tMeas.usChOrbFlag = CHST_AZ_EL | CHST_REFPOS;
		tGnssData.tMeas.usChDataFlag = CHST_PR_RESID | CHST_PR_IONO | CHST_PR_TROPO;
		os_mutex_give(gnssdata_mutex_hnd);
		index++;
#endif
             	ganss_msr++;
           }

//#ifdef TI_PPS_ENABLE
//	getPPS_timestamp(&pps_infobuf);
//#endif


#ifdef TI_DR_ENABLE
	os_mutex_take(gnssdata_mutex_hnd);
	tGnssData.tFix.tTime.ulTowMs   = pos_result.ref_msec;
	tGnssData.tFix.tTime.usWeek    = pos_result.week_num;
	tGnssData.tFix.tTime.ucTStatus = TIME_FROM_FIX;
	tGnssData.tFix.tTime.fltUnc	   = g_gpsTime->fltUnc;
	tGnssData.tFix.tTime.ulTimeTick =   (U32)get_time_msecs();

	tGnssData.tFix.ucUtcOffset 	= pos_result.utcOffset;
//	tGnssData.tFix.ucFixStatus 	= 0; 				//Not define need to check with trimble
	tGnssData.tFix.ucFixType 		= pos_result.fix_type ;/*(pos_result.fix_type == 1)? FIX_TYPE_3D : FIX_TYPE_2D;*/
	tGnssData.tFix.dblLat 		= h_loc->latitude;
	tGnssData.tFix.dblLon 		= h_loc->longitude;
	tGnssData.tFix.dblAlt 		= h_loc->altitude;
	tGnssData.tFix.dblAltMsl 		= pos_result.location.altitude_MSL;
	tGnssData.tFix.dblOscBias 	= pos_result.osc_bias;
	tGnssData.tFix.fltHorizSpeed 	= SPEED_CONV_KMPH_MS(velocity->h_speed);
	tGnssData.tFix.fltHeading 	= h_loc->bearing;
	tGnssData.tFix.fltVertSpeed 	= SPEED_CONV_KMPH_MS(velocity->v_speed);
	tGnssData.tFix.fltOscFreq 	= pos_result.osc_freq;
	tGnssData.tFix.ulFixFlags = pos_result.pos_flags;

	tGnssData.tFix.tUncPos.fltEast 	= pos_result.location.east_unc;
	tGnssData.tFix.tUncPos.fltNorth = pos_result.location.north_unc ;
	tGnssData.tFix.tUncPos.fltVert 	= pos_result.location.vert_unc;

	tGnssData.tFix.tUncVel.fltEast 	= pos_result.velocity.est_unc * 100.0/65535.0;
	tGnssData.tFix.tUncVel.fltNorth = pos_result.velocity.nth_unc* 100.0/65535.0;
	tGnssData.tFix.tUncVel.fltVert 	= pos_result.velocity.vup_unc* 100.0/65535.0;

	tGnssData.tFix.tDop.fltHDOP = (float)(pos_result.dopinfo.horizontal* 1.0/10.0);
	tGnssData.tFix.tDop.fltVDOP = (float)(pos_result.dopinfo.vertical* 1.0/10.0);
	tGnssData.tFix.tDop.fltPDOP = (float)(pos_result.dopinfo.position* 1.0/10.0);
	tGnssData.tFix.tDop.fltTDOP = (float)(pos_result.dopinfo.time* 1.0/10.0);

	tGnssData.tMeas.adblRefPos[0] = h_loc->latitude;
	tGnssData.tMeas.adblRefPos[1] = h_loc->longitude;
	tGnssData.tMeas.adblRefPos[2] = h_loc->altitude;
	tGnssData.tMeas.adblRefPos[3] = pos_result.osc_bias;

	tGnssData.tFix.ulGpsSvsUsed 	= g_gps_svs_usedInFix;
	tGnssData.tFix.ulGloSvsUsed 	= g_glo_svs_usedInFix;

	tGnssData.tMeas.afltRefVel[0] = 0;
	tGnssData.tMeas.afltRefVel[1] = 0;
	tGnssData.tMeas.afltRefVel[2] = 0;
	tGnssData.tMeas.afltRefVel[3] = 0;


	tGnssData.tMeas.ucTowStat = TIME_FROM_FIX;
	tGnssData.tMeas.ulTowMeas =  pos_result.ref_msec;

	tGnssData.tMeas.ucNumSvs = g_tot_num_SVs;

	os_mutex_give(gnssdata_mutex_hnd);


#endif

	if((pos_result.utc_secs == 0xFFFFFFFF)&&(pos_result.utc_msec == 0xFFFF)){
		LOGD("dev_loc_results: UTC time information not available - dropping fix");
	}
	else{
		h_loc->timestamp = g_utc_timestamp = (GpsUtcTime)(((GpsUtcTime)pos_result.utc_secs* (GpsUtcTime)1000)
							+ (GpsUtcTime)pos_result.utc_msec);

		LOGD("dev_loc_results: timestamp [%llu]", h_loc->timestamp);
		LOGD("dev_loc_results: UTC secs [%d],msecs %d",
		pos_result.utc_secs,pos_result.utc_msec);

		PF_OPER_BIN(PFM_APP_LOGB_ID_POS_RESULT, 1, (void*)&pos_result);
#ifndef TIDR_FEATURE
		 hal_desc->gps_callbacks->location_cb(&gpsLocation);
#endif

	}


	return 0;
}

static
int dev_aux_info4sv(void *hnd,
			const struct dev_gnss_sat_aux_desc&  gps_desc,
			const struct dev_gnss_sat_aux_info	 *gps_info,
			const struct dev_gnss_sat_aux_desc&  glo_desc,
			const struct dev_gnss_sat_aux_info	 *glo_info)

{
	GpsSvStatus h_sv[1];

	unsigned char n_sat;
	unsigned char i;
	if(g_gps_log_ver){
		LOGD("dev_aux_info4sv: gps num_sats	[%d]\n", gps_desc.num_sats);
		LOGD("dev_aux_info4sv: glo num_sats [%d]\n",glo_desc.num_sats);
	}
	h_sv->size = sizeof(GpsSvStatus);
	h_sv->num_svs = 0;
	h_sv->almanac_mask = 0;
	h_sv->ephemeris_mask = 0;
	h_sv->used_in_fix_mask = 0;

	n_sat = gps_desc.num_sats;


	for(i = 0; i < n_sat; i++) {

		h_sv->sv_list[i].size		 = sizeof(GpsSvStatus);

		h_sv->sv_list[i].prn	   = gps_info[i].prn;
		h_sv->sv_list[i].snr	   = (float)gps_info[i].snr_db_by10 / 10.0f;
		h_sv->sv_list[i].elevation = gps_info[i].elevate_deg;
		h_sv->sv_list[i].azimuth   = gps_info[i].azimuth_deg;
		if(g_gps_log_ver){

		LOGD("[%3d]	 [%3f]	 [%4f]	 [%2f]\n",
				h_sv->sv_list[i].prn,
				h_sv->sv_list[i].snr ,
				h_sv->sv_list[i].elevation,
				h_sv->sv_list[i].azimuth);
			}
		h_sv->num_svs++;
	}
	h_sv->almanac_mask     = gps_desc.alm_bits;
	h_sv->ephemeris_mask   = gps_desc.eph_bits;
	h_sv->used_in_fix_mask = gps_desc.pos_bits;

	n_sat = glo_desc.num_sats;

	for(unsigned char j = 0; ((i + j) < GPS_MAX_SVS) && (j < n_sat); j++) {

		h_sv->sv_list[i+j].size 	 = sizeof(GpsSvStatus);

		h_sv->sv_list[i+j].prn		 = glo_info[j].prn;
		h_sv->sv_list[i+j].snr		 = (float)glo_info[j].snr_db_by10 / 10.0f;
		h_sv->sv_list[i+j].elevation = glo_info[j].elevate_deg;
		h_sv->sv_list[i+j].azimuth	 = glo_info[j].azimuth_deg;
		LOGD("[%3d]	 [%3f]	 [%4f]	 [%2f]\n",
				h_sv->sv_list[i+j].prn,
				h_sv->sv_list[i+j].snr ,
				h_sv->sv_list[i+j].elevation,
				h_sv->sv_list[i+j].azimuth);

		h_sv->num_svs ++;
	}


	hal_desc->gps_callbacks->sv_status_cb(h_sv);
#ifdef TI_DR_ENABLE
	g_glo_svs_usedInFix = glo_desc.pos_bits;
	g_gps_svs_usedInFix = gps_desc.pos_bits;
	g_tot_num_SVs = h_sv->num_svs;
#endif
	if(g_gps_log_ver){

	LOGD("h_sv: GPS[%d]+GLO[%d]: num[%d] alm[%X] eph[%X] pos[%X] gps_pos[%X]  glo_pos[%X]\n",
			gps_desc.num_sats,
			glo_desc.num_sats,
			h_sv->num_svs,
			h_sv->almanac_mask,
			h_sv->ephemeris_mask,
			h_sv->used_in_fix_mask,
			gps_desc.pos_bits,
			glo_desc.pos_bits);
		}
	return 0;
}


/* ti_nmea_sentence  nmea_message */
int dev_nmea_rpt(void *hnd, enum nmea_sn_id nmea, const char *cb_data, int length)
{
	//LOGD("dev_nmea_rpt: Entering \n");
	struct timeval tv;
	gettimeofday(&tv, NULL);
	GpsUtcTime timestamp = tv.tv_sec*1000+tv.tv_usec/1000;//

        hal_desc->gps_callbacks->nmea_cb(timestamp, cb_data, length);

	return 0;
}



void set_log_server(int state)
{
	if (state == 1) {
		if (property_set("ctl.start", "Log_MD") < 0) {
			LOGD("Failed to start Log_MD\n");
			return;
		}

		/* Allow Navl server to init */
		sleep(1);
		LOGD("Log_MD START \n");
	} else if (state == 0) {
		LOGD("Log_MD STOP \n");
		property_set("ctl.stop", "Log_MD");
	}
    return;
}
void set_devproxy_server(int state)
{
	char value[PROPERTY_VALUE_MAX];
	int loop = 5;
	if (state == 1) {
		property_get("init.svc.devproxy", value, "");
//		LOGD("devproxy current service sattus %s RG: new state %d", value, state);
		while(loop--)
		{
			if(!strncmp(value, "running", 7)||!strncmp(value, "stopping", 8)) {
//				LOGD("devproxy current service status is %s RG: new state %d\n", value, state);
				usleep(5*1000*1000);
				continue;
			}
			if (property_set("ctl.start", "devproxy") < 0) {
				LOGD("Failed to start devproxy\n");
				return;
			}
		}
		if(0 == loop) {
			LOGE("Not able to start devproxy \n");
			return;
		}
		/* Allow Navl server to init */
		sleep(2);
		LOGD("devproxy START \n");
	} else if (state == 0) {
		if (property_set("ctl.stop", "devproxy") < 0) {
		LOGD("Failed to stop devproxy\n");
			return;
		}
		sleep(4);
		LOGD("devproxy STOP \n");
	}
        return;
}

void set_connect_server(int state)
{
	if (state == 1) {
		if (property_set("ctl.start", "agnss_connect") < 0) {
			LOGD("Failed to start agnss_connect\n");
			return;
		}

		/* Allow Navl server to init */
		sleep(2);
		LOGD("agnss_connect START \n");
	} else if (state == 0) {
		LOGD("agnss_connect STOP \n");
		property_set("ctl.stop", "agnss_connect");
	}
        return;
}

void set_pgps_server(int state)
{
	if (state == 1) {
		if (property_set("ctl.start", "RXN_IntApp") < 0) {
			LOGD("Failed to start RXN_IntApp\n");
			return;
		}

		/* Allow Navl server to init */
		sleep(2);
		LOGD("RXN_IntApp START \n");
	} else if (state == 0) {
		LOGD("RXN_IntApp STOP \n");
	}
        return;
}
void set_cpm (int state)
{
	switch (state)
	{
		case 1:
			if (property_set ("ctl.start", "cplc_main") < 0) {
				LOGD("Failed to start CPM!");
				return;
			}
			LOGD("CPM Start Success...");
			break;
		case 0:
			if (property_set ("ctl.stop", "cplc_main") < 0) {
				LOGD("Failed to stop CPM!");
				return;
			}
			LOGD("CPM Stop Success...");
			break;
		default:
			LOGD("Invalid Option: %d", state);
	}
}

unsigned int get_time_msecs()
{
	struct timeval tv;

	if(gettimeofday(&tv, NULL) == -1)
		exit(-1);


	return ((tv.tv_sec * 1000) + (tv.tv_usec/1000));
}


#ifdef TI_PPS_ENABLE
int PPS_init(const char *PPSfilename)
{
		pps_params_t params;	  /* selection of events, options */
		int mode;				  /* supported mode bits */
		int status; 	  /* status bits */
		struct stat st;
		int capturemode;
		u_long assert_seq, clear_seq;
		int first;

		if (stat(PPSfilename, &st) < 0) {
			LOGD("ppstest: stat'ing device special file");
			exit(-1);
		}
		if ((st.st_mode & S_IFMT) != S_IFCHR) {
			LOGD("ppstest: %s is not a device special file\n",PPSfilename);
			return -1;
			}

		/* Open a file descriptor and enable PPS on rising edges */
		pps_fd = open(PPSfilename, O_RDWR, 0);
		if (pps_fd< 0) {
			ALOGD("cannot open %s\n", PPSfilename);
			return -1;
		}

		status = time_pps_create(pps_fd, &pps_handle);
		if (status < 0) {
			ALOGD("ppstest: time_pps_create()");
			return -1;
		}

	return 1;
}



int getPPS_timestamp(pps_info_t *infobuf)
{
	int status; 	  /* status bits */
	status = time_pps_fetch(pps_handle, PPS_TSFMT_TSPEC, infobuf, &pps_timeout);
	//	ALOGD("getPPS_timestamp : nSec =%ld Sec =%ld",  infobuf->assert_tu.tspec.tv_nsec,  infobuf->assert_tu.tspec.tv_sec);
	if (status < 0) {
		LOGD("ppstest setup: time_pps_fetch() failed");
		return -1;
	}
	return 1;
}

#endif


#ifdef TI_OBDII_ENABLE

#define GET_S1B_RD(buf) (char ) get_bytes((unsigned char **)&buf, sizeof(char))

unsigned int get_bytes(unsigned char **buf, unsigned char num_bytes)
{
	unsigned int result = 0;
	unsigned char i;

	if (num_bytes > 4)
		num_bytes = 4;

	*buf += (num_bytes - 1);

	for(i = num_bytes; i; i--) {
		result <<= 8;
		result |= (unsigned int) *((*buf)--);
	}

	*buf += (num_bytes + 1);
	return result;
}

unsigned char ascii2nibble(char c) {

	if ((c >= '0') && (c <= '9'))
		return c - '0';

	if ((c >= 'A') && (c <= 'F'))
		return c - 'A' + 10;

	if ((c >= 'a') && (c <= 'f'))
		return c - 'a' + 10;

	return 16; /* error */
}

static void hexdump (const void *data, int len)
{
	int n = 0, i = 0;
	const unsigned char *p = (const unsigned char *)data;

	ALOGD("NVRAM DUMP");
	while (n < len) {
		ALOGD("%x", *p);
		p++; n++;

	}
}

static void elm_print_data (const unsigned char *data, int len)
{
	int i;
	//ELM_LOG("len = %d, data = ", len);
	/* print in ascii hex */
	ELM_LOG("[%d] ", len);
	for (i = 0; i < len; i++)
		ELM_LOG("%02x ", data[i]);
	
	/* now pretty print */
	ELM_LOG("(");
	for (i = 0; i < len; i++) {
		if (data[i] == '\r')
			ELM_LOG("\\r");
		else if (data[i] == '\n')
			ELM_LOG("\\n");
		else if (isprint(data[i]))
			ELM_LOG("%c", data[i]);
	}
	ELM_LOG(")");

	ELM_LOG("\n");
}

static int find_caret(unsigned char *buf, int len)
{
	int i;

	for (i = 0; i < len; i++)
		if (buf[i] == '>')
			return 1;

	return 0;
}

#define OBD2_SPEED_TIMEOUT 20 /* ms */

void send_elm_speed_cmd(const unsigned char* tx_buf, int len)
{
	int ret = 0;
	int i, n, rx_bytes = 0;
	unsigned char vh_sp_opcode;
	unsigned char speed_val;
	static int dump_response = 1;
	static unsigned int runn_speed_cnt = 0;
	struct pollfd p;
	//time_t start, end;

	//memset(&tx, 0x0, MAX_OBD_CMD_LEN);

	/*  before we send the next speed request, flush any bytes that may 
	 * be in the buffer from a previous failed attempt. */
	//read(obd_fd, rx, MAX_OBD_RSP_LEN);
	//ioctl(obd_fd, FDFLUSH, 0);
	//tcflush(obd_fd, 2);

	memset(&rx, 0x0, MAX_OBD_RSP_LEN);
	// memset(response, 0x0, 64);

	//memcpy(&tx, tx_buf, len);

	//ELM_LOG("OBD2 command write\n");
	ELM_LOG("< ");
	elm_print_data(tx_buf, len);

  	if (write(obd_fd, tx_buf, len) < 0) {
		ELM_LOG("write failed\n");
		return;
	}

	while (1) {
		p.fd = obd_fd;
		p.events = POLLIN;
		p.revents = 0;

		ret = poll(&p, 1, OBD2_SPEED_TIMEOUT);

		if (ret < 0) {
			ELM_LOG("poll failed\n");
			return;
		} else if (ret == 0) {
			ELM_LOG("Incomplete or no response\n");
			/* if we got anything at all prior, print it */
			if (rx_bytes) {
				ELM_LOG("> ");
				elm_print_data(rx, rx_bytes);
			}
			return;
		}

		if ((n = read(obd_fd, rx + rx_bytes, MAX_OBD_RSP_LEN)) < 0) {
			ELM_LOG("read failed\n");
			return;
		}
		
		rx_bytes += n;

		if (find_caret(rx, rx_bytes)) {

			//print_data(rx, rx_bytes);
		
			/* check the total length, and then extract nibbles 4+5 for the speed */
			if (rx_bytes == VEHICLE_SPEED_RESPONSE_LENGTH) {

				vh_sp_opcode = (ascii2nibble(rx[2]) << 4 ) | ( ascii2nibble(rx[3]));
				if (vh_sp_opcode != VEHICLE_SPEED) {
					ELM_LOG("Unexpected response for vehicle speed (code)\n");
					ELM_LOG("> ");
					elm_print_data(rx, rx_bytes);
					return;
				}

				speed_val = (ascii2nibble(rx[4]) << 4 ) | ( ascii2nibble(rx[5]));
				g_speed_val_ms = SPEED_CONV_KMPH_MS(speed_val);

				os_mutex_take(drdata_mutex_hnd);

				runn_speed_cnt++;
				tDrData.tSpeed.ulNumRpt = runn_speed_cnt;
				tDrData.tSpeed.ulSysTick = (U32)get_time_msecs();
				tDrData.tSpeed.bValid = true;
				tDrData.tSpeed.fltValue = SPEED_CONV_KMPH_MS(speed_val);

				LOGD("Vehicle Speed: km/hr %d, m/s %2.2f", speed_val, g_speed_val_ms);

				os_mutex_give(drdata_mutex_hnd);
			} else {
				ELM_LOG("Unexpected response for vehicle speed (length)\n");
			}

			ELM_LOG("> ");
			elm_print_data(rx, rx_bytes);

			return;
		}
	}
}



void update_DWT()
{
	//float vh_sp_in_ms;
	float cm_per_call;
	unsigned int wheelticks;
	static unsigned int runn_wheeltick_cnt = 0;
	static unsigned int cumm_wheelticks = 0;
	float systick_hertz;

	os_mutex_take(drdata_mutex_hnd);

	//vh_sp_in_ms = SPEED_CONV_KMPH_MS(g_speed_val_kmph);
	systick_hertz = (prev_dr_process_time) ? 1000.0 / ((float)(get_time_msecs() - prev_dr_process_time)) : VEHICLE_WHEEL_TICK_RATE_HZ;
	
	//ALOGD("systick_hertz is %2.2f HZ", systick_hertz);
	//cm_per_call = (g_speed_val_ms/VEHICLE_WHEEL_TICK_RATE_HZ) * 100.0 ; // In centimeters
	cm_per_call = (g_speed_val_ms/systick_hertz) * 100.0 ; // In centimeters

	/* take the ceiling of any fractional wheel ticks calculated */
	wheelticks = (unsigned int) ceilf((cm_per_call / DPP_CALIB));
	ALOGD("Vehicle speed (m/s) = %2.2f, distance (cm) = %2.2f, wheelticks = %u, systick_hertz = %2.2f HZ", g_speed_val_ms, cm_per_call, wheelticks, systick_hertz);

	cumm_wheelticks += wheelticks;
	runn_wheeltick_cnt += 10; /* pretend like we read 10 tick samples per DR call */

	tDrData.eIgnSwState = IGN_SW_ON; /* ignition switch is on */
	
	tDrData.tWheelTick.tWTLeftFront.ucSensorValid = SENSOR_DATA_VALID;
	tDrData.tWheelTick.tWTLeftFront.ulCum = cumm_wheelticks;

	tDrData.tWheelTick.tWTLeftRear.ucSensorValid = SENSOR_DATA_VALID;
	tDrData.tWheelTick.tWTLeftRear.ulCum = cumm_wheelticks;

	tDrData.tWheelTick.tWTRightFront.ucSensorValid = SENSOR_DATA_VALID;
	tDrData.tWheelTick.tWTRightFront.ulCum = cumm_wheelticks;

	tDrData.tWheelTick.tWTRightRear.ucSensorValid = SENSOR_DATA_VALID;
	tDrData.tWheelTick.tWTRightRear.ulCum = cumm_wheelticks;

	
	tDrData.tWheelTick.ulSysTick = (U32)get_time_msecs();
	tDrData.tWheelTick.ulNumMsg = runn_wheeltick_cnt;

	os_mutex_give(drdata_mutex_hnd);
}

/* timeout is large since ATZ can take a while to complete */
#define ELM_CMD_TIMEOUT 2000

void send_elm_command(const char* tx_buf, int len)
{
	int ret;
	int i, n, rx_bytes = 0;
	struct pollfd p;
	memset(&tx, 0x0, MAX_OBD_CMD_LEN);
	memset(&rx, 0x0, MAX_OBD_RSP_LEN);

	memcpy(&tx, tx_buf, len);
	tx[len] = '\0'; /* null terminate so we can print it */

	ALOGD("%s", tx);
	ELM_LOG("< ");
	elm_print_data(tx, len);

	if (write(obd_fd, tx, len) < 0) {
		ELM_LOG("write failed\n");
		ALOGD("Failed, check log");
		return ;
	}

	while (1) {
		p.fd = obd_fd;
		p.events = POLLIN;
		p.revents = 0;

		ret = poll(&p, 1, ELM_CMD_TIMEOUT);

		if (ret < 0) {
			ELM_LOG("poll failed\n");
			ALOGD("Failed, check log");
			return;
		} else if (ret == 0) {
			ELM_LOG("Incomplete or no response\n");
			ALOGD("Failed, check log");
			if (rx_bytes) {
				ELM_LOG("> ");
				elm_print_data(rx, rx_bytes);
			}
			return;
		}

		if ((n = read(obd_fd, rx + rx_bytes, MAX_OBD_RSP_LEN)) < 0) {
			ELM_LOG("read failed\n");
			ALOGD("Failed, check log");
			return;
		}
		
		rx_bytes += n;

		if (find_caret(rx, rx_bytes)) {
			/* assume for now the response was 'OK' */
			ALOGD("OK");
			ELM_LOG("> ");
			elm_print_data(rx, rx_bytes);
			return;
		}
	}
}

void auto_protocol_select(const char* tx_buf, int len)
{
	int ret;
	int i;
	int count = 0;
	struct timeval timeout = {5, 0};
	fd_set rdset;
	memset(&tx, 0x0, MAX_OBD_CMD_LEN);
	memset(&rx, 0x0, MAX_OBD_RSP_LEN);

	memcpy(&tx, tx_buf, len);
	ALOGD("auto_protocol_select : %s", tx_buf);

	if (write(obd_fd, tx, sizeof(unsigned char) * len) < 0) {
		LOGD( "unable to send ftdi device data: %d \n", ret);
		return ;
	  }
		 while(1){
		  ret = read(obd_fd, rx,MAX_OBD_RSP_LEN );
		usleep(10000);
		if(ret <= 0)
			continue;
		  else{

				ALOGD("Read %d bytes of data\n", ret);
			for (i = 0; i < ret; i++) ALOGD("r[%d] %c",i, rx[i]);
		  	if(strncmp((const char *)&rx[8],"OK", 2)==0){
				if (write(obd_fd, "01 00\r", sizeof(unsigned char) * 6) < 0) {
					LOGD( "unable to send ftdi device data: %d \n", ret);
					return ;
				  }
				memset(rx, 0, MAX_OBD_RSP_LEN);
				 while(1){
					   FD_ZERO(&rdset);
					   FD_SET(obd_fd, &rdset);
					   if (select(obd_fd +1, &rdset, NULL, NULL, &timeout) >=	0) {
					   							LOGD("OUT OF  SELECT \n");
						  ioctl(obd_fd, FIONREAD, &ret);
						  ret = read(obd_fd, rx,ret );
					   }else{

							break;
					   }

						usleep(10000);
						if(ret <= 0){
						break;
						}
				  		else{

							for (i = 0; i < ret; i++) ALOGD("r[%d] %c",i, rx[i]);
							count++;
							LOGD("Read %d bytes of data select %d\n", ret, count );
			  			}
				 	}

			LOGD("Read %d bytes of data strcmp\n", ret);
			for (i = 0; i < ret; i++) ALOGD("r[%d] %c",i, rx[i]);
			LOGD(")\n");

		break;
		}//strncmp
		  	}//else
		}// WHILE


	return ;
}

int elm_327_Deinit()
{
	int ret;

	fclose(elm_log_fd);
	if ((ret = close(obd_fd)) < 0) {
		LOGD("unable to close ftdi device: %d (%d)\n", ret, errno);
	   	return -1;
	 }

	// ftdi_deinit(obd_fd);
	return 0;
}


//int elm_327_init()
void *elm_327_init(void *param)
{
	char time_buf[50];
	char elm_log_file[256];

  int ret;

  int i,j; /* general purpose indices */


		obd_fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY); /* O_NONBLOCK */
	   if (obd_fd<0) {perror(MODEMDEVICE); exit(1); }


	   tcgetattr(obd_fd,&oldtio); /* save current port settings */

	   bzero(&newtio, sizeof(newtio));
	   cfsetispeed(&newtio, B115200);
	   cfsetospeed(&newtio, B115200);
	   newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
	   newtio.c_iflag = IGNPAR | IGNBRK;
	   newtio.c_oflag = 0;

 /* set input mode (non-canonical, no echo,...) */
	   newtio.c_lflag = 0;
	   newtio.c_cc[VTIME]	 = 0;	/* inter-character timer unused */
	   newtio.c_cc[VMIN]	 = 1;	/* blocking read until 1 chars received */

	   tcflush(obd_fd, TCIFLUSH);
	   tcsetattr(obd_fd,TCSANOW,&newtio);


	log_time(time_buf);
	sprintf(elm_log_file, LOG_PATH"/elm-%s.log", time_buf);
	elm_log_fd = fopen(elm_log_file, "w+");

	ALOGD("ELM log file is %s", elm_log_file);

	//ELM_LOG("ELM LOG %s\n", ctime(time(NULL)));
	ELM_LOG("ELM LOG\n");
	//ELM_LOG("UTC time is %ld\n", time(NULL));
	ELM_LOG("Opened %s\n", MODEMDEVICE);
	/*Sending Reset Command: ATZ*/
   	send_elm_command("ATZ\r", 4);

	/*Sending ECHO OFF Command: ATE0*/
   	send_elm_command("ATE0\r", 5);

	/*Sending LINEFEEDS OFF Command: ATL1 */
   	/* send_elm_command("ATL1\r", 5); */
   	send_elm_command("ATL0\r", 5);

	/*ScanTool version :STI*/
	/* send_elm_command("STI\r", 4); */

	/*Trying Protocol: ISO 14230-4 KPW  This need to be modified to set based on the a config file */
	//send_elm_command("ATSP 5\r", 8);
	/*Auto Protocol Detect*/
	// auto_protocol_select("ATSP 0\r", 7);

	/* ISO 15765-4 CAN (11 bit ID, 500 Kbaud) */
	send_elm_command("ATSP 6\r", 7);


	/*Sending HEADERS ON Command: ATH1*/
	/* send_elm_command("ATH1\r", 5); */
	send_elm_command("ATH0\r", 5);

	/* No spaces in responses */
   	send_elm_command("ATS0\r", 5);

	/* Set cAN header to 00 07 DF */
	send_elm_command("ATSH 7DF\r", 9);
	/* Set filter to 7E8 */
	send_elm_command("ATCRA 7E8\r", 10);

	fflush(elm_log_fd);

	/*OBDLInnk version*/
	/* send_elm_command("STDI\r", 5); */

	return 0;
}



 int  hal_obd2_timer_start()

{
	sa_obd.sa_sigaction = (void (*) (int, siginfo_t*, void *))hal_obd2_timer_handler;
	sa_obd.sa_handler = hal_obd2_timer_handler;

	sigemptyset(&sa_obd.sa_mask);
	if (sigaction(SIGRTMAX-2, &sa_obd, NULL) == -1)
	LOGD("error sigaction failed");

	sev_obd.sigev_notify = SIGEV_SIGNAL;
	sev_obd.sigev_signo = SIGRTMAX-2;
	sev_obd.sigev_value.sival_ptr = &timerid_obd;
	/* Create the timer */
	if (timer_create(CLOCK_REALTIME, &sev_obd, &timerid_obd) == -1)
		LOGD("OBD error timer_create failed");

	obd_timer.it_value.tv_sec = 0 /* NW delay */;
	obd_timer.it_value.tv_nsec = 200000000;
	obd_timer.it_interval.tv_sec = 0;
	obd_timer.it_interval.tv_nsec = 0;

	/* Start the timer */
	if (timer_settime(timerid_obd, 0, &obd_timer, &obd_oitval) == -1)
		LOGD(" OBD error timer_settime failed");


	return 0;
}


int hal_obd2_timer_stop()
{
	LOGD("hal_obd2_timer_stop Entering ++");
	int ret;
	ret = timer_delete(timerid_obd);
	LOGD("hal_obd2_timer_stop Exiting --");

	return ret;

}


void hal_obd2_timer_handler(int num)
{
	hal_obd2_timer_stop();


	/*Place holder to get the speed info*/
	send_elm_speed_cmd((const unsigned char *)"010D\r", 5);
	update_DWT();
	//usleep(5);
	hal_obd2_timer_start();

	/*Added for testing the stability*/
	//ftdi_poll_modem_status(obd_fd, &modem_status);
	//LOGD("R232 status: 0x%x\n", modem_status);
}
#endif





#ifdef TI_CAN_ENABLE

#define CAN_S1B_RD(buf) (char ) get_can_bytes((unsigned char **)&buf, sizeof(char))

unsigned int get_can_bytes(unsigned char **buf, unsigned char num_bytes)
{
	unsigned int result = 0;
	unsigned char i;

	if (num_bytes > 4)
		num_bytes = 4;

	*buf += (num_bytes - 1);

	for(i = num_bytes; i; i--) {
		result <<= 8;
		result |= (unsigned int) *((*buf)--);
	}

	*buf += (num_bytes + 1);
	return result;
}

void sigterm(int signo)
{
	running = 0;
}

int idx2dindex(int ifidx, int socket) {

	int i;
	struct ifreq ifr;

	for (i=0; i < MAXIFNAMES; i++) {
		if (dindex[i] == ifidx)
			return i;
	}

	/* create new interface index cache entry */

	/* remove index cache zombies first */
	for (i=0; i < MAXIFNAMES; i++) {
		if (dindex[i]) {
			ifr.ifr_ifindex = dindex[i];
			if (ioctl(socket, SIOCGIFNAME, &ifr) < 0)
				dindex[i] = 0;
		}
	}

	for (i=0; i < MAXIFNAMES; i++)
		if (!dindex[i]) /* free entry */
			break;

	if (i == MAXIFNAMES) {
		LOGD( "Interface index cache only supports %d interfaces.\n",
		       MAXIFNAMES);
		exit(1);
	}

	dindex[i] = ifidx;

	ifr.ifr_ifindex = ifidx;
	if (ioctl(socket, SIOCGIFNAME, &ifr) < 0)
		perror("SIOCGIFNAME");

	if (max_devname_len < strlen(ifr.ifr_name))
		max_devname_len = strlen(ifr.ifr_name);

	strcpy(devname[i], ifr.ifr_name);

#ifdef DEBUG
	printf("new index %d (%s)\n", i, devname[i]);
#endif

	return i;
}

static void *CAN_Msgrecv(void* param)
{
	fd_set rdfs;
	int s[MAXSOCK];
	int bridge = 0;
	useconds_t bridge_delay = 0;
	unsigned char timestamp = 0;
	unsigned char dropmonitor = 0;
	unsigned char extra_msg_info = 0;
	unsigned char silent = SILENT_INI;
	unsigned char silentani = 0;
	unsigned char color = 0;
	unsigned char view = 0;
	unsigned char log = 0;
	unsigned char logfrmt = 0;
	int count = 0;
	int rcvbuf_size = 0;
	int opt, ret;
	int currmax, numfilter;
	char *ptr, *nptr;
	char can_if[] = "can0,7E8:ffff";
	struct sockaddr_can addr;
	char ctrlmsg[CMSG_SPACE(sizeof(struct timeval)) + CMSG_SPACE(sizeof(__u32))];
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	struct can_filter *rfilter;
	can_err_mask_t err_mask;
	struct canfd_frame frame;
	int nbytes, i, maxdlen;
	struct ifreq ifr;
	struct timeval tv, last_tv;
	struct timeval timeout, timeout_config = { 0, 0 }, *timeout_current = NULL;
	FILE *logfile = NULL;



	LOGD("CAN_Msgrecv Entering ++");

	signal(SIGTERM, sigterm);
	signal(SIGHUP, sigterm);
	signal(SIGINT, sigterm);

	last_tv.tv_sec  = 0;
	last_tv.tv_usec = 0;
	silent = SILENT_ON; /* disable output on stdout */

	currmax = 1; /* find real number of CAN devices */

	if (currmax > MAXSOCK) {
		LOGD( "More than %d CAN devices given on commandline!\n", MAXSOCK);

		LOGD("More than %d CAN devices given on commandline!\n", MAXSOCK);
		return NULL;
	}

	for (i=0; i < currmax; i++) {

		ptr = can_if;
		nptr = strchr(ptr, ',');

//#ifdef DEBUG
		LOGD("open %d '%s'.\n", i, ptr);
//#endif

		s[i] = socket(PF_CAN, SOCK_RAW, CAN_RAW);
		if (s[i] < 0) {
			perror("socket");
			LOGD("CAN recv socket error");
			return NULL;
		}

		cmdlinename[i] = ptr; /* save pointer to cmdline name of this socket */

		if (nptr)
			nbytes = nptr - ptr;  /* interface name is up the first ',' */
		else
			nbytes = strlen(ptr); /* no ',' found => no filter definitions */

		if (nbytes >= IFNAMSIZ) {
			LOGD( "name of CAN device '%s' is too long!\n", ptr);
			LOGD("name of CAN device '%s' is too long!\n", ptr);

			return NULL;
		}

		if (nbytes > max_devname_len)
			max_devname_len = nbytes; /* for nice printing */

		addr.can_family = AF_CAN;

		memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
		strncpy(ifr.ifr_name, ptr, nbytes);

//#ifdef DEBUG
		LOGD("using interface name '%s'.\n", ifr.ifr_name);
//#endif

		if (strcmp(ANYDEV, ifr.ifr_name)) {
			if (ioctl(s[i], SIOCGIFINDEX, &ifr) < 0) {
				perror("SIOCGIFINDEX");
				LOGD("CAN SOCKET SIOCGIFINDEX Err");
				exit(1);
			}
			addr.can_ifindex = ifr.ifr_ifindex;
		} else
			addr.can_ifindex = 0; /* any can interface */

		if (nptr) {

			/* found a ',' after the interface name => check for filters */

			/* determine number of filters to alloc the filter space */
			numfilter = 0;
			ptr = nptr;
			while (ptr) {
				numfilter++;
				ptr++; /* hop behind the ',' */
				ptr = strchr(ptr, ','); /* exit condition */
			}

			rfilter = (struct can_filter*)malloc(sizeof(struct can_filter) * numfilter);
			if (!rfilter) {
				LOGD( "Failed to create filter space!\n");
				LOGD("Failed to create filter space!\n");

				return NULL;
			}

			numfilter = 0;
			err_mask = 0;

			while (nptr) {

				ptr = nptr+1; /* hop behind the ',' */
				nptr = strchr(ptr, ','); /* update exit condition */

				if (sscanf(ptr, "%x:%x",
					   &rfilter[numfilter].can_id,
					   &rfilter[numfilter].can_mask) == 2) {
 					rfilter[numfilter].can_mask &= ~CAN_ERR_FLAG;
					numfilter++;
				} else if (sscanf(ptr, "%x~%x",
						  &rfilter[numfilter].can_id,
						  &rfilter[numfilter].can_mask) == 2) {
 					rfilter[numfilter].can_id |= CAN_INV_FILTER;
 					rfilter[numfilter].can_mask &= ~CAN_ERR_FLAG;
					numfilter++;
				} else if (sscanf(ptr, "#%x", &err_mask) != 1) {
					LOGD( "Error in filter option parsing: '%s'\n", ptr);
					LOGD( "Error in filter option parsing: '%s'\n", ptr);
					free(rfilter);
					return NULL;
				}
			}

			if (err_mask)
				setsockopt(s[i], SOL_CAN_RAW, CAN_RAW_ERR_FILTER,
					   &err_mask, sizeof(err_mask));

			if (numfilter)
				setsockopt(s[i], SOL_CAN_RAW, CAN_RAW_FILTER,
					   rfilter, numfilter * sizeof(struct can_filter));

			free(rfilter);

		} /* if (nptr) */

		/* try to switch the socket into CAN FD mode */
		setsockopt(s[i], SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &canfd_on, sizeof(canfd_on));

		if (rcvbuf_size) {

			int curr_rcvbuf_size;
			socklen_t curr_rcvbuf_size_len = sizeof(curr_rcvbuf_size);

			/* try SO_RCVBUFFORCE first, if we run with CAP_NET_ADMIN */
			if (setsockopt(s[i], SOL_SOCKET, SO_RCVBUFFORCE,
				       &rcvbuf_size, sizeof(rcvbuf_size)) < 0) {
//#ifdef DEBUG
				LOGD("SO_RCVBUFFORCE failed so try SO_RCVBUF ...\n");
//#endif
				if (setsockopt(s[i], SOL_SOCKET, SO_RCVBUF,
					       &rcvbuf_size, sizeof(rcvbuf_size)) < 0) {
					perror("setsockopt SO_RCVBUF");
					LOGD("setsockopt SO_RCVBUF: ");
					return NULL;
				}

				if (getsockopt(s[i], SOL_SOCKET, SO_RCVBUF,
					       &curr_rcvbuf_size, &curr_rcvbuf_size_len) < 0) {
					perror("getsockopt SO_RCVBUF");
										LOGD("getsockopt SO_RCVBUF: ");
					return NULL;
				}

				/* Only print a warning the first time we detect the adjustment */
				/* n.b.: The wanted size is doubled in Linux in net/sore/sock.c */
				if (!i && curr_rcvbuf_size < rcvbuf_size*2)
					LOGD( "The socket receive buffer size was "
						"adjusted due to /proc/sys/net/core/rmem_max.\n");

				LOGD("The socket receive buffer size was "
					"adjusted due to /proc/sys/net/core/rmem_max.\n");
			}
		}

		if (timestamp || log || logfrmt) {

			const int timestamp_on = 1;

			if (setsockopt(s[i], SOL_SOCKET, SO_TIMESTAMP,
				       &timestamp_on, sizeof(timestamp_on)) < 0) {
				perror("setsockopt SO_TIMESTAMP");
				LOGD("setsockopt SO_TIMESTAMP");
				return NULL;
			}
		}

		if (dropmonitor) {

			const int dropmonitor_on = 1;

			if (setsockopt(s[i], SOL_SOCKET, SO_RXQ_OVFL,
				       &dropmonitor_on, sizeof(dropmonitor_on)) < 0) {
				perror("setsockopt SO_RXQ_OVFL not supported by your Linux Kernel");
				LOGD("setsockopt SO_RXQ_OVFL not supported by your Linux Kernel");
				return NULL;
			}
		}

		if (bind(s[i], (struct sockaddr *)&addr, sizeof(addr)) < 0) {
			perror("bind");
			LOGD("bind Err");
			return NULL;
		}
	}

	if (log) {
		time_t currtime;
		struct tm now;
		char fname[sizeof("candump-2006-11-20_202026.log")+1];

		if (time(&currtime) == (time_t)-1) {
			perror("time");
			LOGD("Time Err");
			return NULL;
		}

		localtime_r(&currtime, &now);

		sprintf(fname, "candump-%04d-%02d-%02d_%02d%02d%02d.log",
			now.tm_year + 1900,
			now.tm_mon + 1,
			now.tm_mday,
			now.tm_hour,
			now.tm_min,
			now.tm_sec);

		if (silent != SILENT_ON)
			LOGD("\nWarning: console output active while logging!");

		LOGD( "\nEnabling Logfile '%s'\n\n", fname);


		LOGD("\nEnabling Logfile '%s'\n\n", fname);
		logfile = fopen(fname, "w");
		if (!logfile) {
			perror("logfile");
			return NULL;
		}
	}

	/* these settings are static and can be held out of the hot path */
	iov.iov_base = &frame;
	msg.msg_name = &addr;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = &ctrlmsg;
	LOGD("Entering while running ++");
	while (running) {
#ifdef DEBUG
		LOGD("Value of curmax =%d", currmax);
#endif
		FD_ZERO(&rdfs);
		for (i=0; i<currmax; i++)
			FD_SET(s[i], &rdfs);

		if (timeout_current)
			*timeout_current = timeout_config;

		if ((ret = select(s[currmax-1]+1, &rdfs, NULL, NULL, timeout_current)) <= 0) {
			//perror("select");

			LOGD("Select Error %d", errno);
			running = 0;
			continue;
		}

		for (i=0; i<currmax; i++) {  /* check all CAN RAW sockets */

			if (FD_ISSET(s[i], &rdfs)) {

				int idx;

				/* these settings may be modified by recvmsg() */
				iov.iov_len = sizeof(frame);
				msg.msg_namelen = sizeof(addr);
				msg.msg_controllen = sizeof(ctrlmsg);
				msg.msg_flags = 0;

				nbytes = recvmsg(s[i], &msg, 0);
				if (nbytes < 0) {
					perror("read");
					LOGD("CAN SOC Read err");
					return NULL;
				}
#ifdef DEBUG
				LOGD("Msg Recvd %d", nbytes);
#endif
				if ((size_t)nbytes == CAN_MTU)
					maxdlen = CAN_MAX_DLEN;
				else if ((size_t)nbytes == CANFD_MTU)
					maxdlen = CANFD_MAX_DLEN;
				else {
					LOGD( "read: incomplete CAN frame\n");
					LOGD("read: incomplete CAN frame\n");
					return NULL;
				}

				if (count && (--count == 0))
					running = 0;
#ifdef DEBUG

				LOGD("Bridge %d", bridge);
#endif
				if (bridge) {
					if (bridge_delay)
						usleep(bridge_delay);

					nbytes = write(bridge, &frame, nbytes);
					if (nbytes < 0) {
						perror("bridge write");
		//				LOGD("bridge write");
						return NULL;
					} else if ((size_t)nbytes != CAN_MTU && (size_t)nbytes != CANFD_MTU) {
						LOGD("bridge write: incomplete CAN frame\n");
			//			LOGD("bridge write: incomplete CAN frame\n");
						return NULL;
					}
				}

				for (cmsg = CMSG_FIRSTHDR(&msg);
				     cmsg && (cmsg->cmsg_level == SOL_SOCKET);
				     cmsg = CMSG_NXTHDR(&msg,cmsg)) {
					if (cmsg->cmsg_type == SO_TIMESTAMP)
						tv = *(struct timeval *)CMSG_DATA(cmsg);
					else if (cmsg->cmsg_type == SO_RXQ_OVFL)
						dropcnt[i] = *(__u32 *)CMSG_DATA(cmsg);
				}


				/* check for (unlikely) dropped frames on this specific socket */
				if (dropcnt[i] != last_dropcnt[i]) {

					__u32 frames;

					if (dropcnt[i] > last_dropcnt[i])
						frames = dropcnt[i] - last_dropcnt[i];
					else
						frames = 4294967295U - last_dropcnt[i] + dropcnt[i]; /* 4294967295U == UINT32_MAX */

					if (silent != SILENT_ON)
						LOGD("DROPCOUNT: dropped %d CAN frame%s on '%s' socket (total drops %d)\n",
						       frames, (frames > 1)?"s":"", cmdlinename[i], dropcnt[i]);

					if (log){
						fprintf(logfile, "DROPCOUNT: dropped %d CAN frame%s on '%s' socket (total drops %d)\n",
							frames, (frames > 1)?"s":"", cmdlinename[i], dropcnt[i]);
						LOGD("DROPCOUNT: dropped %d CAN frame%s on '%s' socket (total drops %d)\n",
							frames, (frames > 1)?"s":"", cmdlinename[i], dropcnt[i]);
						}
					last_dropcnt[i] = dropcnt[i];
				}
#ifdef DEBUG

				LOGD(" dropped frames chk done");
#endif
				idx = idx2dindex(addr.can_ifindex, s[i]);
#ifdef DEBUG

				LOGD(" FrameCANID = %x , maxdlen = %d", frame.can_id, maxdlen);
#endif
				/*Parsing logic*/
				char buf[CL_CFSZ];
				char *tokbuf;
				char resp_len;
				char can_resp;
				char vh_sp_opcode;
				char speed_val;

				sprint_canframe(buf, &frame, 0, maxdlen);
				ALOGD("Can Msg recvd :%s", buf);
				tokbuf = strtok(buf, "#");
				tokbuf = strtok(NULL, "");
#ifdef DEBUG
				LOGD("Recvd Frame after the delimiter #  %s", tokbuf);
#endif
				resp_len = (asc2nibble(CAN_S1B_RD(tokbuf)) << 4 ) |( asc2nibble(CAN_S1B_RD(tokbuf)));
#ifdef DEBUG

				LOGD("Resp Len = %x", resp_len);
#endif
				can_resp = (asc2nibble(CAN_S1B_RD(tokbuf)) << 4 ) |( asc2nibble(CAN_S1B_RD(tokbuf)));
#ifdef DEBUG

				LOGD("Can resp = %x", can_resp);
#endif
				if(can_resp == CAN_RESPONSE)
				{
					vh_sp_opcode = (asc2nibble(CAN_S1B_RD(tokbuf)) << 4 ) |( asc2nibble(CAN_S1B_RD(tokbuf)));
#ifdef DEBUG
					LOGD("vh_sp_opcode %c , Hex %x  Dec %d", vh_sp_opcode, vh_sp_opcode, vh_sp_opcode);
#endif
					if(vh_sp_opcode == CAN_VEHICLE_SPEED)
					{
					os_mutex_take(drdata_mutex_hnd);
						//tDrData.ulSpeedSysTime = time(NULL);
						tDrData.ulSpeedSysTime= (U32)get_time_msecs();

						tDrData.bVehSpdAv_Valid = true;
						speed_val = (asc2nibble(CAN_S1B_RD(tokbuf)) << 4 ) |( asc2nibble(CAN_S1B_RD(tokbuf)));
#ifdef DEBUG
						LOGD("Speed value %x", speed_val);
#endif
						tDrData.ulVehSpdAv = (U32)speed_val;
					os_mutex_give(drdata_mutex_hnd);

					}

				}

				//LOGD("Speed = %d", atoi(tokbuf[3]));
				/* once we detected a EFF frame indent SFF frames accordingly */
				if (frame.can_id & CAN_EFF_FLAG)
					view |= CANLIB_VIEW_INDENT_SFF;


				if (log) {
					/* log CAN frame with absolute timestamp & device */
					fprintf(logfile, "(%010ld.%06ld) ", tv.tv_sec, tv.tv_usec);
					fprintf(logfile, "%*s ", max_devname_len, devname[idx]);
					/* without separator as logfile use-case is parsing */
					fprint_canframe(logfile, &frame, "\n", 0, maxdlen);
				}

				if (logfrmt) {
					/* print CAN frame in log file style to stdout */
					LOGD("(%010ld.%06ld) ", tv.tv_sec, tv.tv_usec);
					LOGD("%*s ", max_devname_len, devname[idx]);
					fprint_canframe(stdout, &frame, "\n", 0, maxdlen);
					goto out_fflush; /* no other output to stdout */
				}

				if (silent != SILENT_OFF){
					if (silent == SILENT_ANI) {
						LOGD("%c\b", anichar[silentani%=MAXANI]);
						silentani++;
					}
					goto out_fflush; /* no other output to stdout */
				}


				LOGD(" %s", (color>2)?col_on[idx%MAXCOL]:"");

				switch (timestamp) {

				case 'a': /* absolute with timestamp */
					LOGD("(%010ld.%06ld) ", tv.tv_sec, tv.tv_usec);
					break;

				case 'A': /* absolute with date */
				{
					struct tm tm;
					char timestring[25];

					tm = *localtime(&tv.tv_sec);
					strftime(timestring, 24, "%Y-%m-%d %H:%M:%S", &tm);

					LOGD("(%s.%06ld) ", timestring, tv.tv_usec);
				}
				break;

				case 'd': /* delta */
				case 'z': /* starting with zero */
				{
					struct timeval diff;

					if (last_tv.tv_sec == 0)   /* first init */
						last_tv = tv;
					diff.tv_sec  = tv.tv_sec  - last_tv.tv_sec;
					diff.tv_usec = tv.tv_usec - last_tv.tv_usec;
					if (diff.tv_usec < 0)
						diff.tv_sec--, diff.tv_usec += 1000000;
					if (diff.tv_sec < 0)
						diff.tv_sec = diff.tv_usec = 0;
					LOGD("(%03ld.%06ld) ", diff.tv_sec, diff.tv_usec);

					if (timestamp == 'd')
						last_tv = tv; /* update for delta calculation */
				}
				break;

				default: /* no timestamp output */
					break;
				}

				LOGD(" %s", (color && (color<3))?col_on[idx%MAXCOL]:"");
				LOGD("%*s", max_devname_len, devname[idx]);

				if (extra_msg_info) {

					if (msg.msg_flags & MSG_DONTROUTE)
						LOGD ("  TX %s", extra_m_info[frame.flags & 3]);
					else
						LOGD ("  RX %s", extra_m_info[frame.flags & 3]);
				}

				LOGD("%s  ", (color==1)?col_off:"");

				fprint_long_canframe(stdout, &frame, NULL, view, maxdlen);

				LOGD("%s", (color>1)?col_off:"");
				LOGD("\n");
			}

		out_fflush:
			fflush(stdout);
		}

		usleep(50000);
	}

	for (i=0; i<currmax; i++)
		close(s[i]);

	if (bridge)
		close(bridge);

	if (log)
		fclose(logfile);

	LOGD("CAN_Msgrecv Exiting --");


	return 0;
}



int CanIf_Init(void)
{
	int mtu;
	int enable_canfd = 1;
	struct sockaddr_can addr;
	struct ifreq ifr;
	/*Hardcoded as we are using the CAN to get only Vehicle speeds*/
//	char Vh_speed[] = "7df#02010d.0000000000";
	char Vh_speed[] = "7df#02010d";

	ALOGD("CanIf_Init : Entering ++");
	/* parse CAN frame */
	required_mtu = parse_canframe(Vh_speed, &frame);
	if (!required_mtu){
		LOGD( "\nWrong CAN-frame format! Try:\n\n");
		LOGD( "    <can_id>#{R|data}          for CAN 2.0 frames\n");
		LOGD( "    <can_id>##<flags>{data}    for CAN FD frames\n\n");
		LOGD( "<can_id> can have 3 (SFF) or 8 (EFF) hex chars\n");
		LOGD( "{data} has 0..8 (0..64 CAN FD) ASCII hex-values (optionally");
		LOGD( " separated by '.')\n");
		LOGD( "<flags> a single ASCII Hex value (0 .. F) which defines");
		LOGD( " canfd_frame.flags\n\n");
		LOGD( "e.g. 5A1#11.2233.44556677.88 / 123#DEADBEEF / 5AA# / ");
		LOGD( "123##1 / 213##311\n     1F334455#1122334455667788 / 123#R ");
		LOGD( "for remote transmission request.\n\n");
		return 1;
	}

	/* open socket */
	if ((can_sock= socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
		return 1;
	}
//	LOGD("CanIf_GetVehicleSpeed : Socket Created");

	addr.can_family = AF_CAN;

	strcpy(ifr.ifr_name, "can0");
	if (ioctl(can_sock, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		return 1;
	}
	addr.can_ifindex = ifr.ifr_ifindex;

	if (required_mtu > CAN_MTU) {

		/* check if the frame fits into the CAN netdevice */
		if (ioctl(can_sock, SIOCGIFMTU, &ifr) < 0) {
			perror("SIOCGIFMTU");
			return 1;
		}
		mtu = ifr.ifr_mtu;

		if (mtu != CANFD_MTU) {
			LOGD("CAN interface ist not CAN FD capable - sorry.\n");
			return 1;
		}

		/* interface is ok - try to switch the socket into CAN FD mode */
		if (setsockopt(can_sock, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
			       &enable_canfd, sizeof(enable_canfd))){
			LOGD("error when enabling CAN FD support\n");
			return 1;
		}

		/* ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64 */
		frame.len = can_dlc2len(can_len2dlc(frame.len));
	}

	/* disable default receive filter on this RAW socket */
	/* This is obsolete as we do not read from the socket at all, but for */
	/* this reason we can remove the receive list in the Kernel to save a */
	/* little (really a very little!) CPU usage.                          */
	setsockopt(can_sock, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

	if (bind(can_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}

	//LOGD("CanIf_GetVehicleSpeed : Socket bind done");


	ALOGD("CanIf_Init : Exiting --");
	return 0;
}

int CanIf_GetVehicleSpeed(void)
{
	int required_mtu;
	struct canfd_frame frame;
	int can_sock; /* can raw socket */

	int mtu;
	int enable_canfd = 1;
	struct sockaddr_can addr;
	struct ifreq ifr;
	/*Hardcoded as we are using the CAN to get only Vehicle speeds*/
	char Vh_speed[] = "7df#02010d.0000000000";

	ALOGD("CanIf_GetVehicleSpeed : Entering ++");
	/* parse CAN frame */
	required_mtu = parse_canframe(Vh_speed, &frame);
	if (!required_mtu){
		LOGD( "\nWrong CAN-frame format! Try:\n\n");
		LOGD( "    <can_id>#{R|data}          for CAN 2.0 frames\n");
		LOGD( "    <can_id>##<flags>{data}    for CAN FD frames\n\n");
		LOGD( "<can_id> can have 3 (SFF) or 8 (EFF) hex chars\n");
		LOGD( "{data} has 0..8 (0..64 CAN FD) ASCII hex-values (optionally");
		LOGD( " separated by '.')\n");
		LOGD( "<flags> a single ASCII Hex value (0 .. F) which defines");
		LOGD( " canfd_frame.flags\n\n");
		LOGD( "e.g. 5A1#11.2233.44556677.88 / 123#DEADBEEF / 5AA# / ");
		LOGD( "123##1 / 213##311\n     1F334455#1122334455667788 / 123#R ");
		LOGD( "for remote transmission request.\n\n");
		return 1;
	}

	/* open socket */
	if ((can_sock= socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
		return 1;
	}
//	LOGD("CanIf_GetVehicleSpeed : Socket Created");

	addr.can_family = AF_CAN;

	strcpy(ifr.ifr_name, "can0");
	if (ioctl(can_sock, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		return 1;
	}
	addr.can_ifindex = ifr.ifr_ifindex;

	if (required_mtu > CAN_MTU) {

		/* check if the frame fits into the CAN netdevice */
		if (ioctl(can_sock, SIOCGIFMTU, &ifr) < 0) {
			perror("SIOCGIFMTU");
			return 1;
		}
		mtu = ifr.ifr_mtu;

		if (mtu != CANFD_MTU) {
			LOGD("CAN interface ist not CAN FD capable - sorry.\n");
			return 1;
		}

		/* interface is ok - try to switch the socket into CAN FD mode */
		if (setsockopt(can_sock, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
			       &enable_canfd, sizeof(enable_canfd))){
			LOGD("error when enabling CAN FD support\n");
			return 1;
		}

		/* ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64 */
		frame.len = can_dlc2len(can_len2dlc(frame.len));
	}

	/* disable default receive filter on this RAW socket */
	/* This is obsolete as we do not read from the socket at all, but for */
	/* this reason we can remove the receive list in the Kernel to save a */
	/* little (really a very little!) CPU usage.                          */
	setsockopt(can_sock, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

	if (bind(can_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}

	//LOGD("CanIf_GetVehicleSpeed : Socket bind done");

	/* send frame */
	if (write(can_sock, &frame, required_mtu) != required_mtu) {
		perror("write");

		close(can_sock);
		return 1;
	}
	//LOGD("CanIf_GetVehicleSpeed : Socket write complete");

	close(can_sock);

	ALOGD("CanIf_GetVehicleSpeed : Exiting --");
	return 0;
}


 int  hal_CAN_timer_start()

{
	sa_can.sa_sigaction = (void (*) (int, siginfo_t*, void *))hal_CAN_timer_handler;
		sa_can.sa_handler = hal_CAN_timer_handler;

		sigemptyset(&sa_can.sa_mask);
		if (sigaction(SIGRTMAX-2, &sa_can, NULL) == -1)
		LOGD("error sigaction failed");

		sev_can.sigev_notify = SIGEV_SIGNAL;
		sev_can.sigev_signo = SIGRTMAX-2;
		sev_can.sigev_value.sival_ptr = &timerid_can;
		/* Create the timer */
		if (timer_create(CLOCK_REALTIME, &sev_can, &timerid_can) == -1)
			LOGD("CAN error timer_create failed");

		can_timer.it_value.tv_sec = 0 /* NW delay */;
		can_timer.it_value.tv_nsec = 500000000;
		can_timer.it_interval.tv_sec = 0;
		can_timer.it_interval.tv_nsec = 0;

		/* Start the timer */
		if (timer_settime(timerid_can, 0, &can_timer, &can_oitval) == -1)
			LOGD(" CAN error timer_settime failed");

	//	LOGD("timer created, ID is 0x%lx\n", (long) timerid);
	return 0;
}


int hal_CAN_timer_stop()
{
	LOGD("hal_CAN_timer_stop Entering ++");
	int ret;
	ret = timer_delete(timerid_can);
	LOGD("hal_CAN_timer_stop Exiting --");

	return ret;

}


void hal_CAN_timer_handler(int num)
{
	hal_CAN_timer_stop();


#ifdef TI_CAN_ENABLE
		/*Get the Vehicle speed from CAN*/

		//	CanIf_GetVehicleSpeed();
#endif

	usleep(5);
	hal_CAN_timer_start();
}


#endif

#ifdef TI_PPS_ENABLE


//void *hal_PPS_timer_start(void *param)
 int  hal_PPS_timer_start()

{
	sa.sa_sigaction = (void (*) (int, siginfo_t*, void *))hal_PPS_timer_handler;
		sa.sa_handler = hal_PPS_timer_handler;

		sigemptyset(&sa.sa_mask);
		if (sigaction(SIGRTMAX-1, &sa, NULL) == -1)
		LOGD("error sigaction failed");

		sev.sigev_notify = SIGEV_SIGNAL;
		sev.sigev_signo = SIGRTMAX-1;
		sev.sigev_value.sival_ptr = &timerid;
		/* Create the timer */
		if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1)
			LOGD("error timer_create failed");

		/* Start the timer */
		if (timer_settime(timerid, 0, &pps_timer, &pps_oitval) == -1)
			LOGD("error timer_settime failed");

	//	LOGD("timer created, ID is 0x%lx\n", (long) timerid);
	return 0;
}


int hal_PPS_timer_stop()
{
	LOGD("hal_PPS_timer_stop Entering ++");
	int ret;
	ret = timer_delete(timerid);
	LOGD("hal_PPS_timer_stop Exiting --");

	return ret;

}


void hal_PPS_timer_handler(int num)
{
	hal_PPS_timer_stop();
	unsigned long start, end;

	//start = (U32)get_time_msecs();
	if(g_gpsLocation.flags & (GPS_LOCATION_HAS_LAT_LONG | GPS_LOCATION_HAS_ALTITUDE) ){
		LOGD("Calling GPS CALLBACK to update position");
		hal_desc->gps_callbacks->location_cb(&g_gpsLocation);
	}
	//end = (U32)get_time_msecs();

	//ALOGD("Calling location_cb took %lums", end - start);

	os_sem_give(pps_sig);
}

#define KERNEL_PPS_DELAY 20 /* this delay is to try and ensure the PPS timer below
			       does not fire before the kernel PPS interrupt */
void* hal_PPS_Read(void* param)
{
	unsigned long pps_msec = 0;
//	static int pps_found = false;
	unsigned long timer_msec;
	unsigned long delta_time;

	while(!pps_timer_terminate)
		{

			LOGD("hal_PPS_READ Wait ....");
			os_sem_take(pps_sig);

			if(pps_timer_terminate != false)
				break;

			getPPS_timestamp(&pps_infobuf);
			pps_msec = (pps_infobuf.assert_tu.tspec.tv_nsec /1000000L) ;
			if(pps_infobuf.assert_tu.tspec.tv_sec > 0 ){ /* if valid PPS returned from the kernel */


					os_mutex_take(drdata_mutex_hnd);
					tDrData.ulSysTickPps = (pps_infobuf.assert_tu.tspec.tv_sec *1000) + pps_msec ;
					LOGD("time_pps_fetch : nSec =%ld Sec =%ld",  pps_infobuf.assert_tu.tspec.tv_nsec,  pps_infobuf.assert_tu.tspec.tv_sec);
					os_mutex_give(drdata_mutex_hnd);

					delta_time = (U32)get_time_msecs() - tDrData.ulSysTickPps;
					ALOGD("get_time_msecs()\t%lu\ttDrData.ulSysTickPps\t%lu\tdelta\t%lu", (U32)get_time_msecs(), tDrData.ulSysTickPps, delta_time);
					if (delta_time > 1500) /* situation where PPS did not arrive. depending on how long it has been,
								  set timer accordingly */
						goto just_set_1sec; /* give up */
					else if (delta_time > 1000)
						timer_msec = 100; /* try again shortly to see if it arrived */
					/* align the read timer so that it fires roughly KERNEL_PPS_DELAY seconds after the PPS kernel event.
					 * Otherwise the timer will slowly fall behind. */
					else
						timer_msec = (1000 - delta_time) + KERNEL_PPS_DELAY;
					if (timer_msec > 999) /* timer_msec can end up greater than 1 second if delta_time < KERNEL_PPS_DELAY
								 (e.x. delta_time = 17).  In this case just set 1 second - otherwise the time has to
								 be split between sec and nsec which is a  */
						goto just_set_1sec;

					ALOGD("set timer for %lu ms", timer_msec);
					pps_timer.it_value.tv_sec = 0;
					pps_timer.it_value.tv_nsec = timer_msec * 1000000L;
					pps_timer.it_interval.tv_sec = 0;
					pps_timer.it_interval.tv_nsec = 0;

					ALOGD("Next PPS read will be in %lu ms", timer_msec);
			} else {
just_set_1sec:
					/* keep polling for PPS, set it for one second */
					pps_timer.it_value.tv_sec = 1;
					pps_timer.it_value.tv_nsec = 0;
					pps_timer.it_interval.tv_sec = 0;
					pps_timer.it_interval.tv_nsec = 0;
			}
			usleep(5);
			hal_PPS_timer_start();
		}
	return 0;
}





#endif


#ifdef TI_DR_ENABLE

#define TI_DR_GYRO_ADDR   0x69

void *hal_DR_timer_start(void *param)
{
	 int ret;




	/* Establish handler for timer signal */
	LOGD("hal_DR_timer_start Entering ++");
	  /* Install timer_handler as the signal handler for SIGVTALRM.  */
	  memset (&sa, 0, sizeof (sa));
	  sa.sa_handler = &hal_DR_timer_handler;
	 // sigaction (SIGVTALRM, &sa, NULL);
	 sigaction (SIGALRM, &sa, NULL);
 	  //sigaction (SIGPROF, &sa, NULL);

	 /* Configure the timer to expire after 200 msec...  */
	  timer.it_value.tv_sec = 0;
	  timer.it_value.tv_usec = 0;
	  /* ... and every 200 msec after that.  */
	  timer.it_interval.tv_sec = 0;
	  timer.it_interval.tv_usec = 0;
	  /* Start a virtual timer. It counts down whenever this process is
    	 executing.  */
	//  setitimer (ITIMER_VIRTUAL, &timer, NULL);
	  setitimer (ITIMER_REAL, &timer, NULL);
	  //setitimer (ITIMER_PROF, &timer, NULL);

	  prev_time= get_time_msecs();

		gettimeofday(&g_oldtime, NULL);
	 while(!timer_terminate)
	 	os_sem_take( sig_DR_timer );

	LOGD("hal_DR_timer_start Exiting --");
	return 0;
}


void hal_DR_timer_stop()
{
	LOGD("hal_DR_timer_stop Entering ++");

         /* Configure the timer to expire after 250 msec...  */
          timer.it_value.tv_sec = 0;
          timer.it_value.tv_usec = 0;
          /* ... and every 250 msec after that.  */
          timer.it_interval.tv_sec = 0;
          timer.it_interval.tv_usec = 0;
          /* Start a virtual timer. It counts down whenever this process is
         executing.  */
          setitimer (ITIMER_REAL, &timer, NULL);
		  //setitimer (ITIMER_PROF, &timer, NULL);
	//	  setitimer (ITIMER_VIRTUAL, &timer, NULL);
	LOGD("hal_DR_timer_stop Exiting --");
}

void hal_DR_timer_Reload()
{
	LOGD("hal_DR_timer_Reload Entering ++");
         /* Configure the timer to expire after 200 msec...  */
          timer.it_value.tv_sec = 0;
          timer.it_value.tv_usec = 199000; /* 200000 */
          /* ... and every 200 msec after that.  */
          timer.it_interval.tv_sec = 0;
          timer.it_value.tv_usec = 199000; /* 200000 */
          /* Start a virtual timer. It counts down whenever this process is
         executing.  */
          setitimer (ITIMER_REAL, &timer, NULL);
		  //setitimer (ITIMER_PROF, &timer, NULL);
	//	  setitimer (ITIMER_VIRTUAL, &timer, NULL);
	LOGD("hal_DR_timer_Reload Exiting --");
}

void hal_DR_timer_handler(int num)
{
	gettimeofday(&g_newtime, NULL);
	read_time_inSec=g_newtime.tv_sec - g_oldtime.tv_sec;
	read_time_inuSec = g_newtime.tv_usec - g_oldtime.tv_usec;
	curr_time = get_time_msecs();
	//ALOGD("DR  Process Msec =%d\n ", curr_dr_process_time - prev_dr_process_time);
	hal_DR_timer_stop();



	//LOGD("Sec %d    USec %d\n",read_time_inSec, read_time_inuSec );
	//ALOGD("Msec =%d\n ", curr_time - prev_time);
		os_sem_give(sig_dr_caller);



}

void* hal_DR_caller(void* param)
{
	unsigned int input;

	while(!timer_terminate)
	{

		LOGD("hal_DR_caller Wait ....");
		os_sem_take(sig_dr_caller);

		if(timer_terminate != false)
			break;


	/* just before the DR call, grab the vehicle speed and wheel ticks. mutex is obtained inside these calls */
	send_elm_speed_cmd((const unsigned char *)"010D\r", 5);
	update_DWT();

	os_mutex_take(drdata_mutex_hnd);

	runn_translvrpos_cnt++;
	tDrData.ulSysTick = (U32)get_time_msecs();
	tDrData.tTranShftLvrPos.bValid = true;
	tDrData.tTranShftLvrPos.ulSysTick = (U32)get_time_msecs();
	tDrData.tTranShftLvrPos.ulNumRpt = runn_translvrpos_cnt;
	tDrData.tTranShftLvrPos.ucValue = DRLIB_TRANS_FORWARD;  //Now defaulted to Forward needs to get realtime value.

	//update_DWT();

	memcpy(&CL_tDrData ,&tDrData, sizeof(npDrDataType));


	os_mutex_give(drdata_mutex_hnd);

	os_mutex_take(gnssdata_mutex_hnd);

	memcpy(&CL_tGnssData,&tGnssData, sizeof(npGnssFixDataType));

	os_mutex_give(gnssdata_mutex_hnd);
	prev_dr_process_time= get_time_msecs();

	input = npTnlDrLib(&CL_tDrData, &CL_tGnssData, NULL, &tDrOutput, &CL_tDrNVROut);
	ALOGD("npTnlDrLib return val =%d\n", input);
	 ALOGD("npTnlDrLib  flags from ulHIPStatus = %s%s%s%s%s%s%s\n",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_POS) ? "HIPST_POS " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_HDG) ? "HIPST_HDG " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_ZRO) ? "HIPST_ZRO " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_DPP) ? "HIPST_DPP " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_GOODDHDG) ? "HIPST_GOODDHDG " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_GOODTACH) ? "HIPST_GOODTACH " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_GSF) ? "HIPST_GSF" : "");
	/* ALOGD("npTnDrLib returned %u\n", input); */
	switch (input)
		{
			case INPUT_SUCCESS:
			    //ALOGD("ulHIPStatus = 0x%x, DR_VALID = 0x%x\n", tDrOutput.tDrKF.ulHIPStatus, DR_VALID);

			    LOGD("DR_VALID flags from ulHIPStatus = %s%s%s%s%s%s%s\n",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_POS) ? "HIPST_POS " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_HDG) ? "HIPST_HDG " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_ZRO) ? "HIPST_ZRO " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_DPP) ? "HIPST_DPP " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_GOODDHDG) ? "HIPST_GOODDHDG " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_GOODTACH) ? "HIPST_GOODTACH " : "",
			    (tDrOutput.tDrKF.ulHIPStatus & HIPST_GSF) ? "HIPST_GSF" : "");
			    if ((tDrOutput.tDrKF.ulHIPStatus & DR_VALID) == DR_VALID)
			    {
			        // If got to this point, the DR output structure contains
			        // data which can be passed to the application (displayed on map, etc).
			    	ALOGD ("DR position:\n");
			    	ALOGD("  Latitude (rad):    %lf\n",tDrOutput.adblLLAFF[0]);
			    	ALOGD("  Longitude (rad):   %lf\n",tDrOutput.adblLLAFF[1]);
			    	ALOGD("  Altitude (m, MSL): %lf\n",tDrOutput.adblLLAFF[2] - tDrOutput.tOneHz.fltMslOffset);
			    	ALOGD("  Heading (rad):     %lf\n",tDrOutput.fltHdgFF);


					GpsLocation *h_loc = &g_gpsLocation;
#define PI (3.14159265359)

					 h_loc->size = sizeof(GpsLocation);

					 h_loc->latitude   = (double)tDrOutput.adblLLAFF[0] * 180/PI;
					 h_loc->longitude  = (double)tDrOutput.adblLLAFF[1] * 180/PI;
					 h_loc->flags	  |= GPS_LOCATION_HAS_LAT_LONG;
					 h_loc->flags	 |= GPS_LOCATION_HAS_ALTITUDE;
					 h_loc->altitude   = (double)(tDrOutput.adblLLAFF[2] - tDrOutput.tOneHz.fltMslOffset);
					 h_loc->speed	 = tDrOutput.tOneHz.fltSpeed;
					 h_loc->flags	|= GPS_LOCATION_HAS_SPEED;
					 h_loc->bearing  = tDrOutput.fltHdgFF * 180/PI;
					 h_loc->flags	|= GPS_LOCATION_HAS_BEARING;
					if((tDrOutput.tDrKF.ulHIPStatus & HIPST_ACCY) == HIPST_ACCY){
						 h_loc->accuracy = tDrOutput.tDrKF.tAccy.fltEstHorizPosErr67 ;
						 h_loc->flags	|= GPS_LOCATION_HAS_ACCURACY;
					}
					 if(g_utc_timestamp != 0xFFFFFFFF)
						h_loc->timestamp = g_utc_timestamp;




			    }
			    else
			    {
			        // Here you need to decide whether you want to use the GPS solution or
			        // do something else here. Bottom line, if you're here, you cannot use
			        // the lat/long/alt/heading from the DR output structure.
			    }

			    if ((tDrOutput.tDrKF.ulHIPStatus& GYRO_CALIBRATED) == GYRO_CALIBRATED)
			    {
			        // This indicates the gyro is calibrated (i.e. the DR library has
			        // calibrated the gyro from GPS and is using the gyro data to
			        // propagate DR solution.
			    	LOGD("Gyro is calibrated.");
			    }

			    if (tDrOutput.tDrKF.ulHIPStatus& HIPST_DPP)
			    {
			        // This indicates the tacho/wheel ticks are calibrated (i.e. the DR
			        // library has calibrated the tacho/wheel ticks and is using the
			        // speedometer data to propagate DR solution.
			    	LOGD("Tacho/speedometer is calibrated.");
			    }
				break;

			default:
				//LOGD("Failure. %x", tDrOutput.tTGS0.ulHIPStatus );
				break;
		}

		LOGD("hal_DR_caller Exiting  ");
		 prev_time= get_time_msecs();
				gettimeofday(&g_oldtime, NULL);
				curr_dr_process_time= get_time_msecs();

				usleep(5);
		hal_DR_timer_Reload();

	}

	return 0 ;
}

static int hal_gyro_init(void)
{
    char filename[40];
    const char *buffer;
    unsigned char value;

	LOGD("Entering hal_gyro_init ++");
    /*On J6 the Sensor is connected on the i2c BUS-3*/
    sprintf(filename,"/dev/i2c-2");
    if ((gyro_fd = open(filename,O_RDWR)) < 0) {
        LOGD("Failed to open the bus.");
        return -1;
    }

    if (ioctl(gyro_fd,I2C_SLAVE,TI_DR_GYRO_ADDR) < 0) {
        LOGD("Failed to acquire bus access and/or talk to slave.\n");
        return -1;
    }

    //power on device, set pwm enable
    value = i2c_smbus_read_byte_data(gyro_fd,WHO_AM_I);
    LOGD("Read from Register: 0x0F, Value = %x \n", value);
    if ( value == 0xd3 ) {
	LOGD("A3G4250D Detected \n");
    } else {
	LOGD("No Device Detected \n");
    }

   /*Need the optimum init details from Trimble*/
    i2c_smbus_write_byte_data(gyro_fd,CTRL_REG1,0x9F);
    i2c_smbus_write_byte_data(gyro_fd,CTRL_REG2,0x00);
    i2c_smbus_write_byte_data(gyro_fd,CTRL_REG5,0x00);


	gyro_wait_sem	= os_sem_init();

	LOGD("Exiting hal_gyro_init --");
    return 0;
}

static void *hal_read_gyro(void *param)
{
	signed short x_axis =0;
	signed short y_axis =0;
	signed short z_axis =0;

	signed char temp_value;
	unsigned char temp1;
	unsigned char temp2;
	unsigned char chk_zyxda;
	int first_100_samples = 150;
	unsigned char buf[50];

	//struct timespec sem_timeout = {0, 9000000000};
	 do{


		temp2 = i2c_smbus_read_byte_data(gyro_fd,OUT_Z_H);
                temp1 = i2c_smbus_read_byte_data(gyro_fd,OUT_Z_L);
                if (temp2 == 0xff) temp2=0;
                z_axis = (signed long )((temp2 << 8) & 0xff00) | (temp1 & 0xff);

                temp2 = i2c_smbus_read_byte_data(gyro_fd,OUT_Y_H);
                temp1 = i2c_smbus_read_byte_data(gyro_fd,OUT_Y_L);
                y_axis = (signed long )((temp2 << 8) & 0xff00) | (temp1 & 0xff);

                temp2 = i2c_smbus_read_byte_data(gyro_fd,OUT_X_H);
                temp1 = i2c_smbus_read_byte_data(gyro_fd,OUT_X_L);
                x_axis = (signed long )((temp2 << 8) & 0xff00) | (temp1 & 0xff);

		first_100_samples--;
		usleep(5);
	}while(first_100_samples != 0);

	 x_axis =0;
	y_axis =0;
	z_axis =0;

	while(!gyro_read_terminate){

    		chk_zyxda = i2c_smbus_read_byte_data(gyro_fd,STATUS_REG);
	     if(chk_zyxda & 0x8){
		temp2 = (signed char)i2c_smbus_read_byte_data(gyro_fd,OUT_Z_H);
        temp1 = i2c_smbus_read_byte_data(gyro_fd,OUT_Z_L);
     //   if (temp2 == 0xff) temp2=0;
        z_axis = (signed long )((temp2 << 8) & 0xff00) | (temp1 & 0xff);
		cum_gyro_Z += z_axis;

        temp2 = (signed char)i2c_smbus_read_byte_data(gyro_fd,OUT_Y_H);
        temp1 = i2c_smbus_read_byte_data(gyro_fd,OUT_Y_L);
		//if (temp2 == 0xff) temp2=0;
		y_axis = (signed long )((temp2 << 8) & 0xff00) | (temp1 & 0xff);
		cum_gyro_Y += y_axis;

        temp2 = (signed char)i2c_smbus_read_byte_data(gyro_fd,OUT_X_H);
        temp1 = i2c_smbus_read_byte_data(gyro_fd,OUT_X_L);
		//if (temp2 == 0xff) temp2=0;
        x_axis = (signed long )((temp2 << 8) & 0xff00) | (temp1 & 0xff);
		cum_gyro_X += x_axis;

		temp_value = (signed char)i2c_smbus_read_byte_data(gyro_fd,OUT_TEMP);

		num_samples++;

		os_mutex_take(drdata_mutex_hnd);
		/*Read the Gyro data*/
		tDrData.tGyro.ucSensorValid  = SENSOR_DATA_VALID;
		tDrData.tGyro.ulNum			 = num_samples;
		tDrData.tGyro.ulSysTick      = (U32)get_time_msecs();
		tDrData.tGyro.alCum[0]		 = cum_gyro_X;
		tDrData.tGyro.alCum[1]		 = cum_gyro_Y;
		tDrData.tGyro.alCum[2]		 = cum_gyro_Z;
		tDrData.tGyro.ucSensorValid |= SENSOR_TEMPERATURE_VALID ;
		tDrData.tGyro.lTemperature    = temp_value;

		 os_mutex_give(drdata_mutex_hnd);


	     } //if(chk_zyxda & 0x8)
	     usleep(9000);   //This corresponds to sampling at 100Hz == 10ms
	   // sem_timedwait((sem_t *)gyro_wait_sem, &sem_timeout);
	}


	LOGD("Exiting hal_read_gyro --");
	return 0;
}

void* hippo_msg_recv(void* param)
{
	struct sockaddr_in clientAddr;
	int clilen;
	clilen = sizeof(clientAddr);
	while(!hippo_logger_terminate){
		LOGD("Waiting for hippo client to connect.....");
		hippo_cli_socket_fd  = accept(hippo_socket_fd, (struct sockaddr *) &clientAddr, (socklen_t*) &clilen);
		LOGD(" Hippo client connected.....");
		while(!hippo_logger_terminate){
			usleep(5);
		}
	}
	return 0;

}



void hippoFile_open()
{
#ifdef SOC_LOGGING
		struct sockaddr_in serverAddr,clientAddr;
			int hip_th ;

	LOGD("Entering	hippoFile_open ++");

			hippo_socket_fd = socket(AF_INET, SOCK_STREAM,0 );
			if (hippo_socket_fd < 0)
			{
				LOGD("Error opening socket:");
				return;
			}
			bzero((char *) &serverAddr, sizeof(serverAddr));
			serverAddr.sin_family = AF_INET;
			serverAddr.sin_addr.s_addr = INADDR_ANY;
			serverAddr.sin_port = htons(atoi(portNumber));
			if (bind(hippo_socket_fd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
				{
							LOGD("Error on binding: %d", errno);
					return;
				}

			/* Start listening on the socket */
	        listen(hippo_socket_fd,5);


			hip_th = pthread_create( &hal_desc->hip_th, NULL, hippo_msg_recv, NULL);
			hippo_logger_terminate =false;
#else
	struct timeval tv;
		   struct tm *timeinfo = NULL;

		   char log_file[150] = {0};
		   char loc_buf[50];
		   memset(log_file, 0x0, 150);
		   memset(loc_buf, 0x0, 50);
		   strcpy(log_file, LOG_PATH"/TrimbleDR_");
		   gettimeofday(&tv, NULL);

		   timeinfo = localtime(&tv.tv_sec);

		   if (timeinfo == NULL)
				   return ;
		   strftime(loc_buf , 50, "%Y-%m-%d-%H-%M-%S", timeinfo);
		   strcat(log_file, loc_buf);
		   strcat(log_file, ".hip");

			if ((hippo_fd = fopen(log_file, "w+")) == NULL) {
							  LOGD("HippoLog file failed to open");
							  return;
		  }

			hippo_cli_socket_fd = os_sock_init(HIPPO_SOCK_PATH);

#endif
	return;

}


static int hippo_logger_func()
{

	char *udp_buf = new char[HIPPO_BUF_SZ]; /* Will be freed by user */
	int len;
	if(!udp_buf){
		LOGD("Error allocating the udp buf");
		return -1;
	}
	/* Wait to receive message on UDP socket */
	if((len = os_sock_recv(hippo_cli_socket_fd, udp_buf, HIPPO_BUF_SZ)) < 0)
		return -1;
	else{
		fwrite(udp_buf, sizeof(char), len, hippo_fd);
	}


	return 0;
}

static void *hippo_log_thread(void *arg)
{
	while(!hippo_thread_exit) {
		if(hippo_logger_func())
			break;
	}
	return 0;
}


signed short hippoTransmitData (unsigned char* pucBuf, unsigned short usSize)
{


	memcpy(hippo_snd_buf, pucBuf, usSize);
#ifdef SOC_LOGGING
	if(hippo_cli_socket_fd != 0){
		if(send(hippo_cli_socket_fd, (const void*)hippo_snd_buf, usSize, 0) != usSize)
		{
			LOGD("Mismatch in number of bytes send to socket: ");
		}
	}
#else
		os_sock_send(hippo_cli_socket_fd,HIPPO_SOCK_PATH, hippo_snd_buf ,usSize);
#endif
	return 0;
}
#endif
/*------------------------------------------------------------------------------
 * Implementation of core HAL GPS interface
 *----------------------------------------------------------------------------*/
struct ti_agnss_lcs_if ti_lcs_if;
static bool hal_deferred_flag = false;
static bool init_flag = false;
const char drnvs_file[]= "/data/gnss/nvs/DRNVSOUTPUT";
static bool dr_init_flag = false;

static int hal_init(GpsCallbacks *callbacks)
{

	int retval;
	int ret_th;
	int tim_th;
	int can_th;
	int dr_cal_th;
	int hip_log_th;
	int odb_init_th;
	LOGD("hal_init: GPS Enable ");


	if(init_flag != true){

	   	ni_msg_sockt_fd = -1;
		//set_log_server(1);
	}

	usleep(10 * 1000);
	set_devproxy_server(1);

	//start_logcat();

	if(init_flag != true){
		ALOGD("ENTERING hal_init");

		//LOGD("hal_init: Entering \n");
			hal_desc = new struct hal_descriptor;
			hal_desc->gps_callbacks = callbacks;
			hal_desc->gps_callbacks->set_capabilities_cb(GPS_CAPABILITY_SCHEDULING |
									 GPS_CAPABILITY_MSA |
									 GPS_CAPABILITY_MSB |
									 GPS_CAPABILITY_SINGLE_SHOT);

			struct ti_agnss_lcs_cb ti_lcs_cb;
			ti_lcs_cb.ue_need_assist = NULL,
			ti_lcs_cb.ue_decoded_aid = decoded_aid,
			ti_lcs_cb.ue_loc_results = dev_loc_results,
			ti_lcs_cb.ue_nmea_report = dev_nmea_rpt,
			ti_lcs_cb.ue_aux_info4sv = dev_aux_info4sv;
			ti_lcs_cb.decl_dev_param = NULL;
			ti_lcs_cb.ue_recovery_ind = dev_ue_recovery_ind;
		ti_hnd = ti_agnss_lcs_init(pform, ti_lcs_if, ti_lcs_cb);
		if(!ti_hnd) {
			LOGD("hal_init: ti_agnss_lcs_init FAILED !!! \n");
	        }
			hal_desc->th  = hal_desc->gps_callbacks->create_thread_cb("hal_thread", thread_rx,
		                                                  (void *)hal_desc);
			hal_desc->gps_callbacks->create_thread_cb("HAL_MTLR",
	                               hal_ni_notification_handler, NULL);
	        hal_desc->ni_msg_th = hal_desc->gps_callbacks->create_thread_cb(
	                               "ni_msg_snd_thrd",hal_crt_ni_msg_sockt,NULL);

	        sv_used_in_fix = &gps_sv_used_in_fix;


			ti_log_init(e_log_pfm_app);


			set_connect_server(1);

			set_pgps_server(1);
			set_cpm(1);

		}

	LOGD("ti_lcs_if.dev_init ++ ");


	ti_lcs_if.dev_init(ti_hnd);

	LOGD("ti_lcs_if.dev_init -- ");
	init_flag = true;
#ifdef TI_PPS_ENABLE
	PPS_init(PPS_DEVICE_NAME);
#endif

#ifdef TI_CAN_ENABLE
	/*Set the CAN bitrate*/
	if (property_set("ctl.start", "canconfig:stop") < 0) {
				LOGD("Failed to start canconfig\n");
				return -1;
			}
	usleep(100000);

	if (property_set("ctl.start", "canconfig:bitrate 500000 ctrlmode triple-sampling on") < 0) {
				LOGD("Failed to start canconfig\n");
				return -1;
			}
	sleep(1);
	if (property_set("ctl.start", "canconfig:start") < 0) {
				LOGD("Failed to start canconfig\n");
				return -1;
			}
	sleep(1);
	//CanIf_Init();

#endif
#ifdef TI_DR_ENABLE
			hippoFile_open();
#endif

#ifdef TI_OBDII_ENABLE
	odb_init_th = pthread_create( &hal_desc->obd_th, NULL, elm_327_init, NULL);
	//elm_327_init();
#endif



#ifdef TI_DR_ENABLE

	 LOGD("Entering  hal_DR_timer_start ++");

	 if ((DR_nvsoutFd= fopen(drnvs_file, "wr+")) == NULL) {
			 LOGD("DR NVS Log file failed to open");
			 exit(1);
		 }

	os_services_init();
	sig_dr_caller = os_sem_init();
	sig_DR_timer     = os_sem_init();

	drdata_mutex_hnd = os_mutex_init();
	gnssdata_mutex_hnd = os_mutex_init();


	LOGD("Call GYRO read init");
	memset (&tInit, 0, sizeof(tInit));
	os_mutex_take(drdata_mutex_hnd);
	memset (&tDrData, 0, sizeof(tDrData));
	os_mutex_give(drdata_mutex_hnd);

	os_mutex_take(gnssdata_mutex_hnd);
	memset (&tGnssData, 0, sizeof(tGnssData));
	os_mutex_give(gnssdata_mutex_hnd);

	memset (&tMM, 0, sizeof(tMM));
	memset (&tDrOutput, 0, sizeof(tDrOutput));
//	memset(&rtc, 0, sizeof(rtc));


	/* ignore the heading and position in NVRAM */
#ifdef BENCH_TEST
	tInit.ulConfig =  DRLIB_BENCH_TEST | DRLIB_OUTPUT_TEST ;
#else
	tInit.ulConfig = DRLIB_IGNORE_NVRAM_HDG | DRLIB_IGNORE_NVRAM_POS ;
#endif

	tInit.ulGnssIface = GNSS_IFACE_VERSION;
	tInit.ulDrLibIface = DRLIB_VERSION;
	ALOGD("Using GNSS interface version %lu, DRLIB version %lu", tInit.ulGnssIface, tInit.ulDrLibIface);

	tInit.tVehCfg.fltRoll	  = 0;
	tInit.tVehCfg.fltPitch   = 0;
	tInit.tVehCfg.fltYaw	  = 0;
	tInit.tVehCfg.fltSpeedScaleFactor = 1.0F;
	
	tInit.tGyro.afltDirCos[0][0] = 1.0F;
	tInit.tGyro.afltDirCos[0][1] = 0.0F;
	tInit.tGyro.afltDirCos[0][2] = 0.0F;
	tInit.tGyro.afltDirCos[1][0] = 0.0F;
	tInit.tGyro.afltDirCos[1][1] = 1.0F;
	tInit.tGyro.afltDirCos[1][2] = 0.0F;
	tInit.tGyro.afltDirCos[2][0] = 0.0F;
	tInit.tGyro.afltDirCos[2][1] = 0.0F;
	tInit.tGyro.afltDirCos[2][2] = -1.0F;    //As per Mike comment April 5 2014 relative inversion of GYRO

	tInit.tGyro.tSpec.fltMeasRange = 245;
	tInit.tGyro.tSpec.fltSensitivity = 0.00875f;
	tInit.tGyro.tSpec.fltZeroRateLevel = 25;
	tInit.tGyro.tSpec.fltNoise = 0.03f;
	tInit.tGyro.tSpec.fltColoredNoise = 0.0;
	tInit.tGyro.tSpec.fltTemperatureScaling = -1.0;
	/* indicate no temperature scaling for the accelerometer */
	tInit.tAcc.tSpec[0].fltTemperatureScaling = 0.0;
	tInit.tAcc.tSpec[1].fltTemperatureScaling = 0.0;
	tInit.tAcc.tSpec[2].fltTemperatureScaling = 0.0;

	tInit.tDrLibCfg.bUse3DFixesOnly = true;
	tInit.tDrLibCfg.ucMinNumUsedSVs = 4;

 	hal_gyro_init();

	gyro_read_terminate = false;
	timer_terminate = false;

	ret_th = pthread_create( &hal_desc->gyro_th, NULL, hal_read_gyro, NULL);
	tim_th = pthread_create( &hal_desc->tmr_th, NULL, hal_DR_timer_start, NULL);


	dr_cal_th = pthread_create( &hal_desc->DR_th, NULL, hal_DR_caller, NULL);

	hippo_thread_exit = 0;
	hip_log_th =pthread_create(&hal_desc->hip_th, NULL, hippo_log_thread, NULL);


#endif


#ifdef TI_PPS_ENABLE
		pps_sig = os_sem_init();
		sig_PPS_timer = os_sem_init();
		pps_timer_terminate = false;

		PPS_read_th = pthread_create( &hal_desc->pps_rd_th, NULL, hal_PPS_Read, NULL);


#endif

#ifdef TI_CAN_ENABLE
		running = 1;

		can_th = pthread_create( &hal_desc->can_th, NULL, CAN_Msgrecv, NULL);

#endif

	LOGD("hal_init: Exiting \n");
	return 0;
}



void hal_cleanup(void)
{
	GpsStatus hal_status;
	void* ignored_val;
	char log_archive_cmd[256];
	char log_archive_path[PATH_MAX];

	ALOGD("ENTERING hal_cleanup");

	hal_status.status = GPS_STATUS_ENGINE_OFF;
	hal_desc->gps_callbacks->status_cb(&hal_status);

#ifdef TI_OBDII_ENABLE
	hal_obd2_timer_stop();
		elm_327_Deinit();
#endif

#ifdef TI_DR_ENABLE

	// hexdump(&CL_tDrNVROut, sizeof(npRawNvramType));

	rewind(DR_nvsoutFd);
	fwrite(&CL_tDrNVROut, sizeof(char), sizeof(npRawNvramType), DR_nvsoutFd);

	gyro_read_terminate = true;
	timer_terminate = true;
	hal_DR_timer_stop();
	os_sem_give(sig_DR_timer);


	os_sem_give(sig_dr_caller);

	close(gyro_fd);
	gyro_fd = -1;

	dr_init_flag = false;

	fclose(DR_nvsoutFd);

	hippo_thread_exit = 1;

	os_sock_exit(hippo_cli_socket_fd);


#endif

#ifdef TI_CAN_ENABLE
	close(can_sock);

	running = 0;
	if (property_set("ctl.start", "canconfig:stop") < 0) {
			LOGD("Failed to stop canconfig\n");
			return ;
		}

#endif

#ifdef TI_PPS_ENABLE
		hal_PPS_timer_stop();
		pps_timer_terminate = true;

		os_sem_give(sig_PPS_timer);
		os_sem_give(pps_sig);

		PPS_read_th = -1;
		PPS_timer_th = -1;

#endif


	ti_lcs_if.ue_cleanup(ti_hnd);
	//set_connect_server(0);
	set_devproxy_server(0);
	//set_log_server(0);

	/* move all existing log files to a unique folder */
	iteration++;
	sprintf(log_archive_path, LOG_ARCHIVE_PATH"/run%d", iteration);
	mkdir(log_archive_path, 0777);
	sprintf(log_archive_cmd, "mv "LOG_PATH"/* %s/", log_archive_path);
	ALOGD("mv command: %s", log_archive_cmd);
	ALOGD("Logs will be stored in %s", log_archive_path);

	/* tail end of logcat buffer not appearing in trace */
	sleep(2);

	//stop_logcat();

	system(log_archive_cmd);

	//init_flag = false;

	LOGD("hal_cleanup: ue_cleanup --");

	LOGD("hal_cleanup: Exiting! \n");
}


static int hal_start(void)
{
	ALOGD("ENTERING hal_start");
	GpsStatus hal_status;
	unsigned int  ret;
	unsigned int fileSz;
#ifdef TI_DR_ENABLE

	if(dr_init_flag != true)
	{
		fseek(DR_nvsoutFd, 0L, SEEK_END);
		fileSz = ftell(DR_nvsoutFd);
		// ALOGD("NVRAM size is %u", fileSz);
		if(fileSz > 0)
		{
			rewind(DR_nvsoutFd); /* fseek(DR_nvsoutFd, 0L, SEEK_SET); */
			fread(&tDrNVROut, sizeof(char), sizeof(npRawNvramType), DR_nvsoutFd);

			// hexdump(&tDrNVROut, sizeof(npRawNvramType));
		}

		ALOGD("Initializing DR engine");
		while((ret= npTnlDrLibInit(&tInit, &tDrNVROut)) != INPUT_SUCCESS)
				{
					if(ret == (INPUT_FAULT_NULL_PTR ||INPUT_FAULT_NO_INIT||INPUT_FAULT_GYRO_FAILED
								||INPUT_FAULT_ZERO_TIMETAG)){
						ALOGD("DR LIB init retry")	;
						continue;
					}
					else{
					ALOGD("hal_start: DR Init failure, reason(s) = %u", ret);
					break;
				}
			   }//while (DR lib init)
		if(ret != INPUT_SUCCESS)
			   return -1;

		/* Now read and print the build information */
		navDrGetVerNum(&tVersion);
		ALOGD("Using DR library version %u.%u date %u-%u-%u", tVersion.ucMajor, tVersion.ucMinor,
				tVersion.ucMonth, tVersion.ucDay, tVersion.usYear);

	}
	//dr_init_flag = true;
	hal_DR_timer_Reload();
#endif

#ifdef TI_PPS_ENABLE

	//hal_PPS_timer_Reload();
		pps_timer.it_value.tv_sec = 1 /* NW delay */;
		pps_timer.it_value.tv_nsec = 0;
		pps_timer.it_interval.tv_sec = 0;
		pps_timer.it_interval.tv_nsec = 0;
		hal_PPS_timer_start();

#endif



#ifdef TI_CAN_ENABLE
	hal_CAN_timer_start();

#endif


	hal_status.status = GPS_STATUS_SESSION_BEGIN;
	hal_desc->gps_callbacks->status_cb(&hal_status);

        if(ti_lcs_if.loc_start)
                ti_lcs_if.loc_start(ti_hnd);

	hal_status.status = GPS_STATUS_ENGINE_ON;
	hal_desc->gps_callbacks->status_cb(&hal_status);

	LOGD("hal_start: Exiting \n");
	PF_OPER_BIN(PFM_APP_LOGB_ID_LOC_REQ, 0, NULL);

	return 0;
}

static void* hal_deferred_stop(void *ptr)
{
	LOGD("hal_deferred_stop: Entering! \n");
	ti_lcs_if.loc_stop(ti_hnd);
	LOGD("hal_deferred_stop: Exiting! \n");
	hal_deferred_flag = false;

	return NULL;
}

static int hal_stop(void)
{
	ALOGD("ENTERED hal_stop");
	GpsStatus hal_status;
#ifdef TI_DR_ENABLE
	hal_DR_timer_stop();
#endif

#ifdef TI_PPS_ENABLE
		hal_PPS_timer_stop();

#endif

#ifdef TI_OBDII_ENABLE
	hal_obd2_timer_stop();
#endif

#ifdef TI_CAN_ENABLE
	hal_CAN_timer_stop();
#endif

	hal_status.status = GPS_STATUS_SESSION_END;
	hal_desc->gps_callbacks->status_cb(&hal_status);

	if(ti_lcs_if.loc_stop) {
		if(false == hal_deferred_flag){
                	ti_lcs_if.loc_stop(ti_hnd);
		}
		else {
			pthread_t thread;
			pthread_create(&thread, NULL, hal_deferred_stop, NULL);
		}
	}



	hal_status.status = GPS_STATUS_ENGINE_OFF;
	hal_desc->gps_callbacks->status_cb(&hal_status);
	LOGD("hal_stop: Exiting! \n");
	PF_OPER_BIN(PFM_APP_LOGB_ID_LOC_END, 0, NULL);
	return 0;
}

int hal_inject_time(GpsUtcTime time, int64_t timeReference,
                         int uncertainty)
{
	LOGD("hgps_inject_time: Entering ");
	return 0;
}

int hal_inject_location(double latitude, double  longitude, float accuracy)
{
	return 0;
}

struct aid_hal_dev_id {
        GpsAidingData   h_flag;
        enum loc_assist assist;
};

static struct aid_hal_dev_id h2d_aid_id_tbl [ ] = {

        {GPS_DELETE_EPHEMERIS,     a_GPS_EPH},
        {GPS_DELETE_ALMANAC,       a_GPS_ALM},
        {GPS_DELETE_POSITION,      a_REF_POS},
        {GPS_DELETE_TIME,          a_GPS_TIM},
        {GPS_DELETE_IONO,          a_GPS_ION},
        {GPS_DELETE_UTC,           a_GPS_UTC},
	{GPS_DELETE_HEALTH,        a_GPS_RTI},
	{0,                        a_GPS_EPH}   /* Do not use */
};

struct aid_hal_dev_id *find_dev_aid_id(GpsAidingData flag)
{
        struct aid_hal_dev_id *dev_aid = h2d_aid_id_tbl;
        while(0 != dev_aid->h_flag) {
                if(dev_aid->h_flag == flag)
                        return dev_aid;

                dev_aid++;
        }

        return NULL;
}


void hal_delete_aiding_data(GpsAidingData flags)
{
	LOGD("hal_delete_aiding_data: Entering");
	if(!ti_lcs_if.ue_del_aiding)
		return;

	for(int i = 0; flags; i++) {
		unsigned int flag_bit = flags & (1 << i);
		if(!flag_bit)
			continue;

		struct aid_hal_dev_id *dev_id = find_dev_aid_id(flag_bit);
		if(dev_id) {
			LOGD("hal_delete_aiding_data flag: %d\n", flag_bit);

#ifdef DISABLE_DEL_TIME
			if (dev_id->assist == a_GPS_TIM)
				ti_lcs_if.ue_del_aiding(ti_hnd, dev_id->assist,
							0x00000000, 0xffffffff);
			else
#endif
				ti_lcs_if.ue_del_aiding(ti_hnd, dev_id->assist,
							0xffffffff, 0xffffffff);

			if (dev_id->assist == a_GPS_EPH)
				ti_lcs_if.ue_del_aiding(ti_hnd, a_GLO_EPH,
							0xffffffff, 0x00000003);
			else if (dev_id->assist == a_GPS_ALM)
				ti_lcs_if.ue_del_aiding(ti_hnd, a_GLO_ALM,
							0xffffffff, 0x00000003);
			else if (dev_id->assist == a_GPS_TIM)
				ti_lcs_if.ue_del_aiding(ti_hnd, a_GLO_TIM,
							0xffffffff, 0x00000003);
		}

		flags &= ~flag_bit;
	}

	LOGD("hal_delete_aiding_data: Exiting! \n");
	return;
}

static int
hal_set_position_mode(GpsPositionMode mode, GpsPositionRecurrence recurrence,
                      uint32_t min_interval, uint32_t preferred_accuracy,
                      uint32_t preferred_time)
{
	GpsStatus hal_status;
	int ret_val = 0;
	struct loc_instruct req;

	LOGD("hal_set_position_mode: Entering ");

	LOGD("hal_set_position_mode:  mode = %d", mode);
	switch(mode) {
		case GPS_POSITION_MODE_STANDALONE:
			mode = UE_LCS_CAP_GNSS_AUTO;
			break;
		case GPS_POSITION_MODE_MS_BASED:
			mode = UE_LCS_CAP_AGNSS_POS;
			break;
		case GPS_POSITION_MODE_MS_ASSISTED:
			hal_deferred_flag = true;
			mode = UE_LCS_CAP_AGNSS_MSR;
			break;
	}
	//#define GNSS_GPS_PMTHD_BIT   0x0001
	req.type_flags = mode;
	req.lc_methods = GNSS_GPS_PMTHD_BIT | GNSS_GLO_PMTHD_BIT;
	//req.lc_methods =  GNSS_GLO_PMTHD_BIT;
	req.lc_methods =  GNSS_GPS_PMTHD_BIT;
	LOGD("hal_set_position_mode: mode = %d  req.lc_methods = %d\n", mode, req.lc_methods);

	req.qop_intent.h_acc_M	=  preferred_accuracy;
	req.qop_intent.rsp_secs	=  preferred_time;

	if((0 != preferred_accuracy) && (0 != preferred_time))
		req.qop_intent.acc_sel = acc_3d;
	else
		req.qop_intent.acc_sel = no_acc;

        if(ti_lcs_if.set_loc_instruct)
                ret_val = ti_lcs_if.set_loc_instruct(ti_hnd, req);

        if(ret_val) {
			LOGD("hal_set_position_mode Failed, returning\n");
			return ret_val;
		}

	PF_OPER_BIN(PFM_APP_LOGB_ID_QOP,1,(void*)&(req.qop_intent));
        struct rpt_criteria rpt;
		LOGD("hal_set_position_mode min_interval = %d msec\n",
	     min_interval);
	if(0 == min_interval){
		min_interval = 1000;
		LOGD("hal_set_position_mode min_interval was 0, set to  = %d msec\n", min_interval);
	}
        rpt.interval = (unsigned int) ceilf(((float)min_interval) / 1000.0f);
		LOGD("hal_set_position_mode min_interval = %d sec\n",
	     rpt.interval);
        if(ti_lcs_if.set_rpt_criteria)
                ret_val = ti_lcs_if.set_rpt_criteria(ti_hnd, rpt);

	LOGD("hal_set_position_mode: Exiting ");

	return ret_val;
}

/*-----------------------------------------------------------------------------
 *  GPS Framework Xtra Callbacks i.e. GpsXtraInterface
 *----------------------------------------------------------------------------*/

/* Add functions */

int hal_xtra_init(GpsXtraCallbacks* callbacks)
{
	LOGD("hal_xtra_init: Entering");
	return 0;
}


int hal_xtra_data(char *data, int length)
{
	LOGD("hgps_xtra_data: Entering");

	return 0;
}

GpsXtraInterface gps_xtra_if = {

	sizeof(GpsXtraInterface),
	hal_xtra_init,
	hal_xtra_data,

};

/*------------------------------------------------------------------------------
 *  A-GPS Framework Callbacks i.e. AGpsInterface
 *----------------------------------------------------------------------------*/


/* Add functions */

void hal_agps_init(AGpsCallbacks *agpscb)
{
	return;
}


int hal_agps_data_conn_open(const char *apn)
{
	return 0;
}

int hal_agps_data_conn_closed()
{
	return 0;
}

int hal_agps_data_conn_failed()
{
	return 0;
}

int hal_agps_set_server(AGpsType type, const char* hostname, int port )
{
	return 0;
}

static void hal_ni_notification_recv(void* argument)
{
	int bytesRead = 0;
	int client_fd;
	GpsNiNotification ni;
	int noti_client_id;
	LOGD("hal_ni_notification_recv: Entering \n");
	if(NULL==argument)
	{
		LOGD("hal_ni_notification_recv: Invalid Socket FD\n");
		return;
	}
	client_fd = *((int*)argument);
	do
	{
		bytesRead = read(client_fd, (GpsNiNotification *)&ni, sizeof(GpsNiNotification));
		if(bytesRead>0)
		{
			noti_client_id = ni.notification_id;
			ni.notification_id = (((client_fd<<16)& MASK_NI_SOCK_FD)|(noti_client_id & MASK_NI_SESSION_FD));
			LOGD("hal_ni_notification_recv: noti_client_id %d, ni.notification_id = 0x%x \n",noti_client_id,ni.notification_id);
			p_niCallback->notify_cb(&ni);
		}

	}while(bytesRead>=0);
	close(client_fd);
	LOGD("hal_ni_notification_recv: Exiting Successfully. \n");

	return;
}
static void hal_ni_notification_handler(void* argument)
{
	struct sockaddr_un serverAddress;
	struct sockaddr_un clientAddress;
	int mtlr_socketfd = -1;
	int client_socketfd =-1;
	socklen_t clientAddressLength;
	int retVal = 1;

	LOGD("hal_ni_notification_handler: Entering");
	memset( &serverAddress, 0, sizeof(serverAddress) );
	serverAddress.sun_family = AF_UNIX;
	strcpy(serverAddress.sun_path, SOC_NAME_6121);
	unlink(SOC_NAME_6121);
	mtlr_socketfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (mtlr_socketfd < 0)
	{
		LOGD("hal_ni_notification_handler: Socket creation FAILED !!!");
		return;
	}
	if ( bind( mtlr_socketfd, (struct sockaddr *) &serverAddress,
				sizeof(serverAddress)) < 0 )
	{
		LOGD("hal_ni_notification_handler: bind FAILED !!! \n");
		close(mtlr_socketfd);
		return;
	}
	if ( chmod( SOC_NAME_6121, 0777) < 0 )
	{
		LOGD("hal_ni_notification_handler: permission change FAILED !!! \n");
	}
	if ( listen( mtlr_socketfd, MAX_CONNECTIONS) < 0 )
	{
		LOGD("hal_ni_notification_handler: listen FAILED !!! \n");
		close (mtlr_socketfd);
		return;
	}

	LOGD("hal_ni_notification_handler:Waiting for connection.....\n");
	do
	{
		clientAddressLength = sizeof(clientAddress);
		client_socketfd = accept( mtlr_socketfd,
				(struct sockaddr *) &clientAddress,
				&clientAddressLength);
		if ( client_socketfd < 0)
		{
			LOGD("hal_ni_notification_handler: accept FAILED !!! \n");
			close(mtlr_socketfd);
			return;
		}
		hal_desc->gps_callbacks->create_thread_cb("HAL_MTLR_Client", hal_ni_notification_recv, &client_socketfd);
		LOGD("hal_ni_notification_handler: Recived Notification Connection");

	}while (1);

	return;
}
void hal_ni_init(GpsNiCallbacks* nicb)
{
	pthread_t thread_GPSAL;
	LOGD("hal_ni_init: Entering \n");
	p_niCallback = nicb;
//	hal_desc->gps_callbacks->create_thread_cb("HAL_MTLR", hal_ni_notification_handler, NULL);
	LOGD("hal_ni_init: Exiting Successfully \n");
}
void hal_ni_response(int notifyID, int response)
{
	LOGD("hal_ni_response: Entering \n");
	int client_sockfd=-1;
	int client_session_id=-1;
	hal_ni_client_response ni_resp;
	LOGD("hal_ni_response: notifyID %d\n",notifyID);
	switch( response ) {
		case NI_RESPONSE_ACCEPT:
			{
				LOGD("hal_ni_response: User pressed Accept\n");
			}
			break;
		case NI_RESPONSE_DENY:
			{
				LOGD("hal_ni_response: User pressed Deny\n");
			}
			break;
		case NI_RESPONSE_NORESP:
			{
				LOGD("hal_ni_response: No response from user/timeout\n");
			}
			break;
			default:
				LOGD("hal_ni_response: unhandled response %d",response);

	}
	client_sockfd = ((notifyID&MASK_NI_SOCK_FD)>>16);
	ni_resp.client_session_id = notifyID&MASK_NI_SESSION_FD;
	ni_resp.resp= (eHAL_NIResp)response;
	if ( write(client_sockfd, &ni_resp, sizeof(hal_ni_client_response) ) < 0)
	{
		LOGD("hal_ni_response: Message Sending FAILED !!! \n");
		close(client_sockfd);
	}
	else{
		LOGD("hal_ni_response: Response Sent to socket %d \n", client_sockfd);
		//close(client_sockfd);
	}
	LOGD("hgps_ni_response: Exiting Successfully \n");
}
AGpsInterface agps_if = {

	sizeof(AGpsInterface),
	hal_agps_init,
	hal_agps_data_conn_open,
	hal_agps_data_conn_closed,
	hal_agps_data_conn_failed,
	hal_agps_set_server,
};

/*------------------------------------------------------------------------------
 *  NI Framework Interface i.e. GpsNiInterface
 *----------------------------------------------------------------------------*/

/* Add funcitons */

GpsNiInterface gps_ni_if = {
	sizeof(GpsNiInterface),
	hal_ni_init,
	hal_ni_response,
};

/*------------------------------------------------------------------------------
 *  AGPS RIL Framework Interface i.e. AGpsRilInterface
 *----------------------------------------------------------------------------*/

/* Add functions */

void ril_init(AGpsRilCallbacks* callbacks)
{
   hal_desc->ril_callbacks = callbacks;
   LOGD("ril_init: \n");
   /* : Create New Thread, Store Handle and Semaphore */
   /* Before Call hal_desc->ril_callbacks->request_setid,
                  hal_desc->ril_callbacks->request_refloc */
   /* Using hal_desc->ril_callbacks->create_thread_cb    */
   //hal_desc->ni_msg_th = hal_desc->ril_callbacks->create_thread_cb("ni_msg_snd_thrd",hal_crt_ni_msg_sockt,NULL);

   return;
}

void ril_set_ref_location(const AGpsRefLocation *agps_reflocation, size_t sz_struct)
{
   LOGD("ril_set_ref_location: \n");
   return;
}

void ril_set_set_id (AGpsSetIDType type, const char* setid)
{
   LOGD("ril_set_set_id : \n");
   return;
}

void ril_ni_message (uint8_t *msg, size_t len)
{
   unsigned int i;
   unsigned char *tmp_msg;
   tmp_msg = msg;

   LOGD("ril_ni_message : Len: %d, \nMsg:",len);
   for(i=0; i< len; i ++){
      LOGD("0x%x", *tmp_msg);
      tmp_msg++;
   }

   hal_send_NI_message(msg,len);
   return;
}

void ril_update_network_state (int connected, int type, int roaming, const char* extra_info)
{
   LOGD("ril_update_network_state : \n");
   return;
}

void ril_update_network_availability(int avaiable, const char* apn)
{
   LOGD("ril_update_network_availability : \n");
   return;
}

static void hal_crt_ni_msg_sockt(void *msg)
{
        struct sockaddr_un srv_addr;
        struct sockaddr_un clnt_addr;
        int ni_msg_sckt_fd = -1;
        int ni_clnt_sckt_fd =-1;
        socklen_t clnt_addr_len;
        int retVal = 1;

        LOGD("hal_crt_ni_msg_sockt: Entering");

        unlink(SUPL_SOCKT_NAME);
        memset( &srv_addr, 0, sizeof(srv_addr) );
        srv_addr.sun_family = AF_UNIX;
        strcpy(srv_addr.sun_path,SUPL_SOCKT_NAME);
        //ni_msg_sckt_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
        ni_msg_sckt_fd = socket(AF_UNIX, SOCK_STREAM, 0);
        if (ni_msg_sckt_fd < 0){
                LOGD("hal_crt_ni_msg_sockt: Socket creation FAILED !!!");
                return;
        }
        if (bind(ni_msg_sckt_fd, (struct sockaddr *) &srv_addr,
                                sizeof(srv_addr)) < 0 ){
                LOGD("hal_crt_ni_msg_sockt: bind FAILED !!! \n");
                close(ni_msg_sckt_fd);
                return;
        }
        if (chmod(SUPL_SOCKT_NAME,0777) < 0 ){
                LOGD("hal_crt_ni_msg_sockt: chmod FAILED !!! \n");
        }
        /*
	if(ni_msg_sockt_fd == -1 ){
                ni_msg_sockt_fd = ni_msg_sckt_fd;
	}
        */
        if (listen( ni_msg_sckt_fd, MAX_CONNECTIONS) < 0 ){
                LOGD("hal_crt_ni_msg_sockt: listen FAILED !!! \n");
                close (ni_msg_sckt_fd);
                return;
        }
        LOGD("hal_crt_ni_msg_sockt: Waiting for connection.....\n");
        do{
                clnt_addr_len = sizeof(clnt_addr);
                ni_clnt_sckt_fd = accept(ni_msg_sckt_fd,
                                (struct sockaddr *) &clnt_addr,
                                &clnt_addr_len);
                if ( ni_clnt_sckt_fd < 0){
                        LOGD("hal_crt_ni_msg_sockt: accept FAILED !!! \n");
                        close(ni_msg_sckt_fd);
                        return;
                }
		if(ni_msg_sockt_fd == -1 ){
		ni_msg_sockt_fd = ni_clnt_sckt_fd;
		LOGD("hal_crt_ni_msg_sockt:Socket Id %d", ni_msg_sockt_fd);
		}
		else{
			LOGD("hal_crt_ni_msg_sockt:More than One NI MSG Sockets are opened");
                        close(ni_msg_sckt_fd);
		}
        }while (1);

        return;
}

void hal_send_NI_message(unsigned char *msg,size_t len)
{

   static supl_ni_msg_t ni_msg;
   if((ni_msg_sockt_fd == -1)||
      (msg == NULL)||(len == 0)){
       LOGD("hal_send_NI_message: NI MSG Can not Send: Sockt%d, Msg Len %d, Msg Ptr %p",
                   ni_msg_sockt_fd,len,msg);
      return;
   }

   memset(&ni_msg,0x00,sizeof(supl_ni_msg_t));
   ni_msg.ni_msg_type = SUPL_NI_MSG;
   ni_msg.msg_len = len;
   memcpy(ni_msg.ni_msg,msg,len);

   if(write(ni_msg_sockt_fd,&ni_msg, sizeof(supl_ni_msg_t)) < 0){
       LOGD("hal_send_NI_message: NI MSG Tx error");
   }
   return;
}

/*------------------------------------------------------------------------------
 *  AGPS RIL Framework Interface i.e. AGpsRilInterface
 *----------------------------------------------------------------------------*/

GpsDebugInterface gps_debug_if = {
	0,
	NULL
};
const void* hal_get_extension(const char* name)
{
	unsigned char res= 0;
	LOGD("hal_get_extension: Entering \n");
	res = strcmp(name, AGPS_INTERFACE);
	if (res == 0)
	{
		return NULL;
	}
	res = strcmp(name, GPS_NI_INTERFACE);
	if (res == 0)
	{
		LOGD("hal_get_extension: Returning NI Interface\n");
		//return (void *)&gps_ni_if;
		return NULL;
	}
	res = strcmp(name, AGPS_RIL_INTERFACE);
	if (res == 0)
	{
		LOGD("hal_get_extension: Returning RIL Interface\n");
		//return (void *)&agps_rilInterface;
		return NULL;
	}
	res = strcmp(name, GPS_XTRA_INTERFACE);
	if (res == 0)
	{
		return NULL;
	}
	LOGD("hal_get_extension: Exiting Successfully \n");
	return NULL;
}



GpsInterface gps_if = {
	sizeof(GpsInterface),
	hal_init,
	hal_start,
	hal_stop,
	hal_cleanup,
	hal_inject_time,
	hal_inject_location,
	hal_delete_aiding_data,
	hal_set_position_mode,
	hal_get_extension
};

const GpsInterface* ti_get_gps_interface(struct gps_device_t* dev)
{
	LOGD("ti_get_gps_interface: Entering");
        return &gps_if;
}

static int open_gps(const struct hw_module_t* module, char const* name,
                    struct hw_device_t** device)
{
        struct gps_device_t *dev =
                (struct gps_device_t *) malloc(sizeof(struct gps_device_t));
        memset(dev, 0, sizeof(*dev));

	LOGD("open_gps: Entering");
        dev->common.tag        = HARDWARE_DEVICE_TAG;
        dev->common.version    = 0;
        dev->common.module     = (struct hw_module_t*)module;
        dev->get_gps_interface = ti_get_gps_interface;

        *device = (struct hw_device_t*)dev;
        return 0;
}


static struct hw_module_methods_t ti_gps_module_methods = {
        open: open_gps
};

struct hw_module_t HAL_MODULE_INFO_SYM = {

        tag: HARDWARE_MODULE_TAG,
        version_major: 1,
        version_minor: 0,
        id: GPS_HARDWARE_MODULE_ID,
        name: "TI GPS Module",
        author: "TI",
        methods: &ti_gps_module_methods,
        dso: NULL, /* remove compilation warnings */
        reserved: {0}, /* remove compilation warnings */
};

