LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE             := gps.$(TARGET_BOARD_PLATFORM)
LOCAL_MODULE_TAGS        := optional eng debug

ifneq ($(BOARD_GPS_LIBRARIES),)
  LOCAL_CFLAGS           += -DHAVE_GPS_HARDWARE -DCONFIG_PERIODIC_REPORT
  LOCAL_SHARED_LIBRARIES += libcutils
endif

LOCAL_CFLAGS           +=  -DENABLE_TRACE=1 -DENABLE_DEBUG=1
#LOCAL_CFLAGS           +=  -DDISABLE_DEL_TIME
LOCAL_LDLIBS          += -lrt -lm
LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_SHARED_LIBRARIES   += libdevproxy libagnss libclientlogger

TI_OBDII = 'n'
ifeq ($(TI_OBDII), 'y')
LOCAL_CFLAGS += -DTI_OBDII_ENABLE

#LOCAL_SHARED_LIBRARIES += libftdi1.0
#LOCAL_C_INCLUDES += \
#	external/libftdi1-1.1/src
endif


TIPPS_ENABLE = 'n'
ifeq ($(TIPPS_ENABLE), 'y')
LOCAL_CFLAGS += -DTI_PPS_ENABLE
LOCAL_CFLAGS += -DBENCH_TEST

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/pps
endif
TICAN_ENABLE = 'n'
ifeq ($(TICAN_ENABLE), 'y')

LOCAL_STATIC_LIBRARIES +=libcan
LOCAL_CFLAGS += -DTI_CAN_ENABLE \
                -DSO_RXQ_OVFL=40 \
                -DPF_CAN=29

LOCAL_C_INCLUDES += \
		  $(LOCAL_PATH)/can-utils/include \
                  $(LOCAL_PATH)/can-utils
endif

TIDR_FEATURE = 'n'
ifeq ($(TIDR_FEATURE), 'y')

LOCAL_STATIC_LIBRARIES +=libAndroidTrimbleDrLib

LOCAL_CFLAGS           +=  -DTI_DR_ENABLE

LOCAL_C_INCLUDES += \
		  $(LOCAL_PATH)/dr_inc
endif

LOCAL_C_INCLUDES +=\
		$(LOCAL_PATH)/../dproxy/include\
		$(LOCAL_PATH)/../dproxy/server\
		$(LOCAL_PATH)/../include/common\
		$(LOCAL_PATH)/../include/connect\
		$(LOCAL_PATH)/../include/dproxy\
		$(LOCAL_PATH)/../connect/include\
		$(LOCAL_PATH)/../connect/core\
		$(LOCAL_PATH)/../connect/common\
		$(LOCAL_PATH)/../connect/debug\
		hardware/libhardware/include/hardware\
		$(LOCAL_PATH)/../infrastructure/log/include\
		$(LOCAL_PATH)/../infrastructure/log/common\
		system/core/include/cutils\
		bionic/libc/include\
		bionic/libc/private 

LOCAL_SRC_FILES +=                        \
	hal_connect.cpp

LOCAL_PRELINK_MODULE     := false
LOCAL_MODULE_PATH        := $(TARGET_OUT_SHARED_LIBRARIES)/hw

include $(BUILD_SHARED_LIBRARY)

