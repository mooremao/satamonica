/* 
   Copyright Texas Instruments, 2011        

   This file attempts to summarize the function blocks that are required 
   to integrate TI GPS driver into the framework
 */


#include <stdint.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <properties.h>

#include <gps.h>
#include <device.h>
#include <gnss.h>
#include <dev_proxy.h>
#include <utils.h>
#include <rpc.h>

#include <utils/Log.h>
#include <sys/time.h>


/*------------------------------------------------------------------------------
 *  Declaration of Global HAL constructs
 *----------------------------------------------------------------------------*/

#define WEEK_HRS	          (7U*24U)
#define WEEK_SECS             (WEEK_HRS*3600U)        /* # of secs in week */
#define WEEK_BITS             (WEEK_SECS*50U)       /* # of bits in week */

#define GPS2UTC               315964800000LLU
#define LEAP_SECS             15000
#define SEC_MSEC              1000
#define CONFIG_FILE_PATH      "/system/etc/gnss/config/dproxy.conf"
#define SEVICE_PACK_FILE_PATH "/system/etc/gnss/patch/dproxy.patch"


static time_t ttff = 0;
static time_t ttff_flag = 0;

GpsStatus hal_status;			/* Required */
static unsigned char locFixMode = 1;    /* Required */

static int thread_rx_exit = 0;

struct dev_2_hal;                     /* Forward declaration */
extern AGpsInterface agps_if;	      /* Forward declaration */

struct hal_descriptor {

	sem_t             sem;        /* Define semaphore for signalling */
	pthread_mutex_t   mtx;        /* Define mutex for data integrity */
	pthread_t 	  th;
	struct dev_2_hal *dev2hal;

	GpsCallbacks     *gps_callbacks;
	AGpsCallbacks    *agps_callbacks;
};

static struct hal_descriptor *hal_desc;

void set_devproxy_server(int state);

/*------------------------------------------------------------------------------
 *  GPS Framework Callbacks i.e. GpsCallbacks
 *----------------------------------------------------------------------------*/
static int hal_location_rpt(void *cb_data, int data_len)
{	
	LOGD("[GNSS]hal_location_rpt: Entering \n");
	hal_desc->gps_callbacks->location_cb((GpsLocation*)cb_data);
	LOGD("[GNSS]hal_location_rpt: Exiting! \n");
	return 0;
}

static GpsStatus hal_GpsStatus;

static int hal_status_rpt(void *cb_data, int data_len)
{
	LOGD("[GNSS]hal_status_rpt: Entering\n");
	hal_desc->gps_callbacks->status_cb((GpsStatus*)cb_data);
	LOGD("[GNSS]hal_status_rpt: Exiting!\n");
	return 0;
}

static int hal_sv_status_rpt(void *cb_data, int data_len)
{
	LOGD("[GNSS]hal_sv_status_rpt: Entering\n");
	hal_desc->gps_callbacks->sv_status_cb((GpsSvStatus*)cb_data);
	LOGD("[GNSS]hal_sv_status_rpt: Exiting!\n");
	return 0;
}

static int hal_nmea_rpt(void *cb_data, int data_len)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);

	LOGD("[GNSS]hal_nmea_rpt: Entering \n");
	hal_desc->gps_callbacks->nmea_cb(tv.tv_sec*1000+tv.tv_usec/1000,(char*)cb_data, data_len);
	LOGD("[GNSS]hal_nmea_rpt: %s\n", (char*)cb_data);
	LOGD("[GNSS]hal_nmea_rpt: Exiting! \n");
	return 0;
}

/* Add support for remaining functions in GpsCallbacks */

/*------------------------------------------------------------------------------
 *  GPS Framework Xtra Callbacks i.e. GpsXtraCallbacks
 *----------------------------------------------------------------------------*/

/* Add */

/*------------------------------------------------------------------------------
 *  A-GPS Framework Callbacks i.e. AGpsCallbacks
 *----------------------------------------------------------------------------*/

/* Add */

/*------------------------------------------------------------------------------
 *  NI Framework Callbacks i.e. GpsNiCallbacks
 *----------------------------------------------------------------------------*/

/* Add */

/*------------------------------------------------------------------------------
 *  AGPS RIL Framework Callbacks i.e. AGpsRilCallbacks
 *----------------------------------------------------------------------------*/

/* Add */

/*------------------------------------------------------------------------------
 *  Connect device callbacks / indications to Framework HAL callbacks
 *----------------------------------------------------------------------------*/
typedef	int (*dev_2_hal_fn)(void *cb_data, int size);

#define HAL_GPS_CB_LOCATION_ID           0x00000001
#define	HAL_GPS_CB_STATUS_ID             0x00000002
#define	HAL_GPS_CB_SV_STATUS_ID          0x00000004
#define	HAL_GPS_CB_NMEA_ID               0x00000008

struct dev_2_hal {

	unsigned int  hal_cb_fn_id;    /* ID of HAL CB func to be invoked  */
	dev_2_hal_fn  hal_dispatch;    /* Function to invoke HAL CB func   */
	int           hal_max_data;    /* Maximum size of data in HAL CB   */

	int           dev_data_len;    /* Actual size of data in HAL CB    */
	void         *dev_2wr_data;    /* Storage (mem) for data from dev  */
	void         *hal_2rd_data;    /* Storage (mem) for data to HAL    */
};

typedef char gps_nmea[200];

typedef struct {} no_data[0];

#define DEV2HAL(id, func, dtype) {id, func, sizeof(dtype), 0, NULL, NULL}

struct dev_2_hal dev2hal_tbl[] = {

	DEV2HAL(HAL_GPS_CB_LOCATION_ID,    hal_location_rpt,  GpsLocation),
	DEV2HAL(HAL_GPS_CB_STATUS_ID,      hal_status_rpt,    GpsStatus),
	DEV2HAL(HAL_GPS_CB_SV_STATUS_ID,   hal_sv_status_rpt, GpsSvStatus), 
	DEV2HAL(HAL_GPS_CB_NMEA_ID,  	   hal_nmea_rpt,      gps_nmea),
	DEV2HAL(0xFFFFFFFF,                NULL,              no_data)
};

int dev2hal_mem_init(struct dev_2_hal *tbl)
{
	struct dev_2_hal *dev2hal = tbl;

	LOGD("[GNSS]dev2hal_mem_init: Entering \n");

	while(dev2hal->hal_dispatch) {
		int   size = dev2hal->hal_max_data;
		void *data = malloc(size + size);
		if(!data)
			goto exit1;

		dev2hal->dev_2wr_data = data;
		dev2hal->hal_2rd_data = (char *)data + size;

		dev2hal++;
	}
	LOGD("[GNSS]dev2hal_mem_init: Exiting! \n");
	return 0;

exit1:
	while(tbl != dev2hal) {
		free(dev2hal->dev_2wr_data);
		dev2hal--;
	}
	LOGD("[GNSS]dev2hal_mem_init: malloc failed!!! \n");
	return -1;
}

void dev2hal_mem_free(struct dev_2_hal *tbl)
{
	struct dev_2_hal *dev2hal = tbl;

	LOGD("[GNSS]dev2hal_mem_free: Entering \n");


	while(dev2hal->hal_dispatch) {
		free(dev2hal->dev_2wr_data);
		dev2hal++;
	}
	LOGD("[GNSS]dev2hal_mem_free: Exiting! \n");
}

struct hal_descriptor hal_desc_obj = {

	.dev2hal = &dev2hal_tbl[0]
};

static unsigned int hal_cb_fn_invoke_flags = 0x00000000;
static int dev_cb_handle(unsigned int hal_cb_fn_id, void *hal_data, int data_len)
{
	struct dev_2_hal *dev2hal = hal_desc_obj.dev2hal;
	LOGD("[GNSS]dev_cb_handle: Entering \n");

	while(dev2hal->hal_dispatch) {
		if(dev2hal->hal_cb_fn_id == hal_cb_fn_id) {
			pthread_mutex_lock(&(hal_desc->mtx));     /* Lock Mutex   */
			memcpy(dev2hal->dev_2wr_data, hal_data, data_len);
			dev2hal->dev_data_len = data_len;

			hal_cb_fn_invoke_flags |= hal_cb_fn_id;
			LOGD("[GNSS]dev_cb_handle:hal_cb_fn_invoke_flags = %d\n",hal_cb_fn_invoke_flags);

			if(hal_cb_fn_invoke_flags == hal_cb_fn_id) {
				/* First flag; wake hal rx thread */
				sem_post(&(hal_desc->sem));       
			}
			pthread_mutex_unlock(&(hal_desc->mtx));   /* Unlock Mutex */ 

			break;   /* Job done, quit */
		}

		dev2hal++;
	}
	LOGD("[GNSS]dev_cb_handle: Exiting! \n");
	return 0;
}

static int hal_queue_wr(unsigned int hal_cb_fn_id, void *hal_data, int data_len)
{
	LOGD("[GNSS]hal_queue_wr: Entering \n");
	LOGD("[GNSS]hal_queue_wr: hal_cb_fn_id = %d \n",hal_cb_fn_id);
	dev_cb_handle(hal_cb_fn_id, hal_data, data_len);
	LOGD("[GNSS]hal_queue_wr: Exiting! \n");
	return 0;
}

static int hal_cb_invoke(unsigned int hal_cb_fn_id)
{
	struct dev_2_hal *dev2hal = dev2hal_tbl;
	LOGD("[GNSS]hal_cb_invoke: Entering \n");

	while(dev2hal->hal_dispatch) {
		if(dev2hal->hal_cb_fn_id == hal_cb_fn_id) {
			LOGD("[GNSS]hal_cb_invoke:dev2hal->hal_cb_fn_id == hal_cb_fn_id\n");
			pthread_mutex_lock(&(hal_desc->mtx)); /* Lock Mutex   */
			memcpy(dev2hal->hal_2rd_data, dev2hal->dev_2wr_data, 
				   dev2hal->dev_data_len);
			pthread_mutex_unlock(&(hal_desc->mtx)); /* Unlock Mutex */ 

			dev2hal->hal_dispatch(dev2hal->hal_2rd_data, 
								  dev2hal->dev_data_len);

			break;
		}
		dev2hal++;
	}
	LOGD("[GNSS]hal_cb_invoke: Exiting! \n");
	return 0;
}

static unsigned int hal_queue_rd()
{
	unsigned int flags;
	LOGD("[GNSS]hal_queue_rd: Entering \n");

	/* Wait for semaphore */
	sem_wait(&(hal_desc->sem));
	LOGD("[GNSS]hal_queue_rd:sem_wait Released \n");

	pthread_mutex_lock(&(hal_desc->mtx));   /* Lock mutex   */
	flags = hal_cb_fn_invoke_flags;
	hal_cb_fn_invoke_flags = 0;
	pthread_mutex_unlock(&(hal_desc->mtx)); /* Unlock mutex */

	LOGD("[GNSS]hal_queue_rd: flags: %d \n",flags);
	LOGD("[GNSS]hal_queue_rd: Exiting! \n");
	return flags;
}

void thread_rx(void *arguement)
{
	LOGD("[GNSS]thread_rx: Entering \n");

	while(!thread_rx_exit) {
		unsigned int flags = hal_queue_rd();
		unsigned int i     = 0;
		LOGD("[GNSS]thread_rx: flags = %d\n",flags);
		while(flags) {
			unsigned int fn_id = flags & (1 << i++);
			if(fn_id) {
				hal_cb_invoke(fn_id);
				flags &= (~fn_id);
			}
		}
	}
	thread_rx_exit = 0;
	LOGD("[GNSS]thread_rx: Exiting!\n");
}

/*------------------------------------------------------------------------------
 * Core callbacks from device for GPS feature
 *----------------------------------------------------------------------------*/

/*
   Housekeeping for SV Status
 */
struct gps_sat_used_in_fix {

	unsigned int id_flags;
	unsigned int is_valid;
};

struct gps_sat_used_in_fix gps_sv_used_in_fix = {

	.id_flags = 0,
	.is_valid = 0
};

static struct gps_sat_used_in_fix *sv_used_in_fix;
static GpsSvStatus  gpsSvStatus;
static unsigned int msr_rpt_flags = 0;
static unsigned int loc_rpt_flags = 0;

#define DEV_SAT_RPT_INFO  0x0001
#define DEV_SAT_RPT_USED  0x0002
#define DEV_LOC_RPT_POS   0x0100
#define DEV_LOC_RPT_VEL   0x0200
#define DEV_LOC_RPT_UTC   0x0400
/*   ti_ue_pos_report  location_rpt */
static int dev_location_rpt(enum ue_pos_id pos_id, void *cb_data, int num_obj)
{
#define DEV_LOC_RPT_ALL (DEV_LOC_RPT_POS | DEV_LOC_RPT_VEL | DEV_LOC_RPT_UTC)
#define PYTHAGORAS_VAL(x,y) sqrt((x*x) + (y*y))
#define EAST_ER 	      (0.1)
#define NORTH_ER	      (0.1)
#define VERTICAL_ER	      (0.5)

#define GNSS_ID_GPS 0  
#define UNC_MAP_TO_K(unc_m, C, x)  \
		(unsigned char)(log10(((unc_m*1.0) / (C*1.0)) + 1.0) / log10(1.0 + x))


	static GpsLocation  gpsLocation;
	int i;
	GpsLocation *h_loc = &gpsLocation;
	GpsSvStatus *h_sv = &gpsSvStatus;

	LOGD("[GNSS]dev_location_rpt: pos_id = %d, num_obj = %d\n", pos_id, num_obj);

		switch(pos_id) {
			case loc_exl_native:
				{
					if (!ttff_flag) {
						ttff_flag = 1;
						LOGD("[GNSS]--TTFF-- [%d]", (int)(time(NULL) - ttff));
					}

					struct dev_pos_info *dev_pos = (struct dev_pos_info *) cb_data;
					enum loc_fix_type fix_type;

					LOGD("[GNSS]dev_location_rpt : loc_exl_native\n");

					h_loc->latitude    = ((double)dev_pos->lat_rad) * 180.0f / 4294967296.0f;
					h_loc->longitude   = ((double)dev_pos->lon_rad) * 180.0f / 2147483648.0f;
					h_loc->flags      |= GPS_LOCATION_HAS_LAT_LONG;
					h_loc->altitude    = (double)0;

					fix_type = (dev_pos->pos_flags & 0x2) ? fix_3d : fix_2d;
					if(fix_type == fix_3d){  	/* 3D FIX   */
						LOGD("[GNSS]dev_location_rpt: fix_3d");
						h_loc->altitude = ((double)dev_pos->alt_wgs84) / 2.0f;
						h_loc->flags   |= GPS_LOCATION_HAS_ALTITUDE;
					} else
						LOGD("[GNSS]dev_location_rpt: fix_2d");

					h_loc->accuracy = (float) PYTHAGORAS_VAL(
															 dev_pos->unc_east * EAST_ER, 
															 dev_pos->unc_north * NORTH_ER);
					h_loc->flags |= GPS_LOCATION_HAS_ACCURACY;

				loc_rpt_flags |= DEV_LOC_RPT_POS;

					LOGD("[GNSS]dev_location_rpt: Latitude  [%d]", dev_pos->lat_rad);
					LOGD("[GNSS]dev_location_rpt: Longitude [%d]", dev_pos->lon_rad);
					LOGD("[GNSS]dev_location_rpt: Altitude  [%d]", dev_pos->alt_wgs84);
					LOGD("[GNSS]dev_location_rpt: Latitude  [%lf]", h_loc->latitude);
					LOGD("[GNSS]dev_location_rpt: Longitude [%lf]", h_loc->longitude);
					LOGD("[GNSS]dev_location_rpt: Altitude  [%lf]", h_loc->altitude);
					LOGD("[GNSS]dev_location_rpt: Accuracy  [%f]", h_loc->accuracy);
				}
				break;

		case loc_gnss_ie:
			{
				if (!ttff_flag) {
					LOGD("[GNSS]TTFF [%d]", (int)(time(NULL) - ttff));
					ttff_flag = 1;
				}

				struct loc_gnss_info *gps_info = (struct loc_gnss_info*) cb_data;

				

				LOGD("[GNSS]dev_location_rpt : loc_gnss_ie\n");

				/* Convert 3GPP lat long to GPS */
				h_loc->latitude    = (double)gps_info->location.latitude_N * 90.0f/8388608.0f;
				h_loc->longitude   = (double)gps_info->location.longitude_N * 360.0f/16777216.0f;
				h_loc->flags      |= GPS_LOCATION_HAS_LAT_LONG;
				h_loc->altitude    = (double)0;

				if(gps_info->fix_type == fix_3d){  	/* 3D FIX   */
					LOGD("[GNSS]dev_location_rpt: fix_3d");
					h_loc->altitude = (double)gps_info->location.altitude_N;
					h_loc->flags   |= GPS_LOCATION_HAS_ALTITUDE;
				} else
					LOGD("[GNSS]dev_location_rpt: fix_2d");

#define UNC_MAP_K_TO_R(unc_k, C, x) (double)(C * (pow(1+x, unc_k) - 1))

				double unc_east = UNC_MAP_K_TO_R(
												 gps_info->location.unc_semi_maj_K, 
												 10.0f, 0.1f);

				double unc_north = UNC_MAP_K_TO_R(
												  gps_info->location.unc_semi_min_K, 
												  10.0f, 0.1f);

				h_loc->accuracy = (float) PYTHAGORAS_VAL(unc_east * EAST_ER, 
														 unc_north * NORTH_ER);
				h_loc->flags |= GPS_LOCATION_HAS_ACCURACY;

				loc_rpt_flags |= DEV_LOC_RPT_POS;

				LOGD("[GNSS]dev_location_rpt: Latitude  [%lf]", h_loc->latitude);
				LOGD("[GNSS]dev_location_rpt: Longitude [%lf]", h_loc->longitude);
				LOGD("[GNSS]dev_location_rpt: Altitude  [%lf]", h_loc->altitude);
				LOGD("[GNSS]dev_location_rpt: Accuracy  [%f]", h_loc->accuracy);
			}
			break;

			case vel_native:
				{
					struct dev_vel_info *dev_vel = (struct dev_vel_info *) cb_data;
					dev_vel->hdg_info = BUF_OFFSET_ST(dev_vel, struct dev_vel_info, 
													  struct dev_hdg_info);
					struct vel_gnss_info vel_gnss[1];

					LOGD("[GNSS]dev_location_rpt : vel_native\n");

					vel_gnss->type = h_v_vel;
					vel_gnss->h_speed = (unsigned short) 
						sqrt(pow(dev_vel->east, 2.0f) + 
							 pow(dev_vel->north, 2.0f)) * 720.0f / 13107.0f;
					vel_gnss->bearing = (dev_vel->hdg_info->truthful * 45) >> 13;
					vel_gnss->v_speed = abs(dev_vel->vertical) * 720.0f / 13107.0f;
					vel_gnss->ver_dir = (dev_vel->vertical > 0) ? 0 : 1;

					h_loc->speed    = (float) PYTHAGORAS_VAL(vel_gnss->h_speed, 
															 vel_gnss->v_speed);
					h_loc->flags   |= GPS_LOCATION_HAS_SPEED;
					h_loc->bearing  = vel_gnss->bearing;
					h_loc->flags   |= GPS_LOCATION_HAS_BEARING;

				loc_rpt_flags      |= DEV_LOC_RPT_VEL;

					LOGD("[GNSS]east              [%10d]\n", dev_vel->east);
					LOGD("[GNSS]north             [%10d]\n", dev_vel->north);
					LOGD("[GNSS]vertical          [%10d]\n", dev_vel->vertical);
					LOGD("[GNSS]uncetainty        [%10d]\n", dev_vel->uncertainty);
					LOGD("[GNSS]truthful          [%10d]\n", dev_vel->hdg_info->truthful);
					LOGD("[GNSS]magnetic          [%10d]\n", dev_vel->hdg_info->magnetic);
					LOGD("[GNSS]type              [%10d]\n", vel_gnss->type);
					LOGD("[GNSS]h_speed           [%10d]\n", vel_gnss->h_speed);
					LOGD("[GNSS]bearing           [%10d]\n", vel_gnss->bearing);
					LOGD("[GNSS]v_speed           [%10d]\n", vel_gnss->v_speed);
					LOGD("[GNSS]ver_dir           [%10d]\n", vel_gnss->ver_dir);
					LOGD("[GNSS]speed             [%f]\n", h_loc->speed);
				}
				break;

			case vel_gnss_ie:
				{
					struct vel_gnss_info *vel_info = (struct vel_gnss_info*) cb_data;

					LOGD("[GNSS]dev_location_rpt : vel_gnss_ie\n");

					h_loc->speed    = (float) PYTHAGORAS_VAL(vel_info->h_speed, 
															 vel_info->v_speed);
					h_loc->flags   |= GPS_LOCATION_HAS_SPEED;
					h_loc->bearing  = vel_info->bearing;
					h_loc->flags   |= GPS_LOCATION_HAS_BEARING;

				loc_rpt_flags      |= DEV_LOC_RPT_VEL;

					LOGD("[GNSS]type              [%10d]\n", vel_info->type);
					LOGD("[GNSS]h_speed           [%10d]\n", vel_info->h_speed);
					LOGD("[GNSS]bearing           [%10d]\n", vel_info->bearing);
					LOGD("[GNSS]v_speed           [%10d]\n", vel_info->v_speed);
					LOGD("[GNSS]ver_dir           [%10d]\n", vel_info->ver_dir);
					LOGD("[GNSS]speed             [%f]\n", h_loc->speed);
				}
				break;

			case tim_gps_native:
				{
					struct dev_gps_time *time = (struct dev_gps_time *) cb_data;
					int64_t gps_time_secs;

					LOGD("[GNSS]dev_location_rpt : tim_gps_native\n");

					gps_time_secs    = (((int64_t)time->week_num) * ((int64_t)WEEK_SECS)) + 
						(((int64_t)time->time_msec) / ((int64_t)1000));
					h_loc->timestamp = (((int64_t)gps_time_secs) * ((int64_t)SEC_MSEC)) +
						((int64_t)GPS2UTC) + ((int64_t)LEAP_SECS);

				loc_rpt_flags       |= DEV_LOC_RPT_UTC;

					LOGD("[GNSS]dev_location_rpt: timer_count       [%10d]\n", time->timer_count);
					LOGD("[GNSS]dev_location_rpt: week_num          [%10d]\n", time->week_num);
					LOGD("[GNSS]dev_location_rpt: time_msec         [%10d]\n", time->time_msec);
					LOGD("[GNSS]dev_location_rpt: time_bias         [%10d]\n", time->time_bias);
					LOGD("[GNSS]dev_location_rpt: time_unc          [%10d]\n", time->time_unc);
					LOGD("[GNSS]dev_location_rpt: freq_bias         [%10d]\n", time->freq_bias);
					LOGD("[GNSS]dev_location_rpt: freq_unc          [%10d]\n", time->freq_unc);
					LOGD("[GNSS]dev_location_rpt: is_utc_diff       [%10d]\n", time->is_utc_diff);
					LOGD("[GNSS]dev_location_rpt: utc_diff          [%10d]\n", time->utc_diff);
					LOGD("[GNSS]dev_location_rpt: timestamp         [%lld]", h_loc->timestamp);
				}
				break;

			case tim_gps_ie:
				{
					struct gps_time *time = (struct gps_time*) cb_data;
					int64_t gps_time_secs;

					LOGD("[GNSS]dev_location_rpt : tim_gps_ie\n");

					gps_time_secs    = (((int64_t)time->week_nr) * ((int64_t)WEEK_SECS)) + 
						((int64_t)time->tow);
					h_loc->timestamp = (((int64_t)gps_time_secs) * ((int64_t)SEC_MSEC)) +
						((int64_t)GPS2UTC) + ((int64_t)LEAP_SECS);

				loc_rpt_flags       |= DEV_LOC_RPT_UTC;

					LOGD("[GNSS]dev_location_rpt: timestamp         [%lld]", h_loc->timestamp);
				}
				break;

			case loc_sat_native:
				{
					struct dev_sat_info* sat_info = (struct dev_sat_info*) cb_data;
					uint32_t bitmap = 0;
					LOGD("[GNSS]dev_location_rpt : loc_sat_native\n");

					sv_used_in_fix->id_flags = 0;

					for(i = 0; i < num_obj; i++) {
						if(sat_info->gnss_id == GNSS_ID_GPS){
							bitmap |= (1 << (sat_info->svid - 1));
							LOGD("[GNSS]svid :%d, bitmap: 0x%x\n", sat_info->svid, bitmap);
						}
						LOGD("[GNSS]gnss_id           [%10d]\n", sat_info->gnss_id);
						LOGD("[GNSS]svid              [%10d]\n", sat_info->svid);
						LOGD("[GNSS] ->iode           [%10d]\n", sat_info->iode);
						LOGD("[GNSS] ->msr_residual   [%10d]\n", sat_info->msr_residual);
						LOGD("[GNSS] ->weigh_in_fix   [%10d]\n", sat_info->weigh_in_fix);

						sat_info++;	
					}

					sv_used_in_fix->id_flags = bitmap;

					LOGD("[GNSS]bitmap [0x%x]\n", bitmap);
					LOGD("[GNSS]sv_used_in_fix->id_flags [0x%x]\n", sv_used_in_fix->id_flags);

					sv_used_in_fix->is_valid = DEV_SAT_RPT_USED; 
				}
				break;

			default:
				LOGD("[GNSS]Unknown ue_pos_id!!");
				break;
		}

	/* If all required data is available, let app know about location */
	if((loc_rpt_flags & DEV_LOC_RPT_POS) && (loc_rpt_flags & DEV_LOC_RPT_UTC) 
	   && (loc_rpt_flags & DEV_LOC_RPT_VEL)) {
		LOGD("[GNSS]dev_location_rpt: All data obtained, report to android framework\n");

			hal_queue_wr(HAL_GPS_CB_LOCATION_ID, h_loc, sizeof(GpsLocation));       

		/* Clear flags and enteries for next iteration */
		memset(h_loc, 0, sizeof(GpsLocation)); 
		loc_rpt_flags    = 0;
	}

	return 0;
}

void populate_gps_sv_status(GpsSvStatus *gps_sv_status, 
							GpsSvStatus *glo_sv_status)
{
	int count = 0;
	GpsSvStatus *h_sv = &gpsSvStatus;
	GpsSvInfo *h_sv_info;

	h_sv->num_svs = 0;

	if (gps_sv_status->num_svs > GPS_MAX_SVS)
		gps_sv_status->num_svs = GPS_MAX_SVS;

	h_sv->num_svs += gps_sv_status->num_svs;
	h_sv_info = h_sv->sv_list;

	for (count = 0; count < gps_sv_status->num_svs; count++) {
		memcpy(h_sv_info + count, &(gps_sv_status->sv_list[count]), 
			   sizeof(GpsSvInfo));
	}

	if (glo_sv_status->num_svs > (GPS_MAX_SVS - gps_sv_status->num_svs))
		glo_sv_status->num_svs = GPS_MAX_SVS - gps_sv_status->num_svs;

	h_sv_info = h_sv->sv_list + gps_sv_status->num_svs;

	for (count = 0; count < glo_sv_status->num_svs; count++) {
		memcpy(h_sv_info + count, &(glo_sv_status->sv_list[count]), 
			   sizeof(GpsSvInfo));
	}

	h_sv->num_svs += glo_sv_status->num_svs;
}

/* ti_ue_msr_report  measure_info */
int dev_msr_rpt(enum ue_msr_id msr_id, void *cb_data, int num_obj)
{
#define DEV_SAT_RPT_ALL  (DEV_SAT_RPT_INFO | DEV_SAT_RPT_USED)
	GpsSvStatus *h_sv = &gpsSvStatus;
	static GpsSvStatus gps_sv_status[1];
	static GpsSvStatus glo_sv_status[1];
	int cnt = 0;
	int idx = 0;
	static unsigned int local_bitmap = 0;

	LOGD("[GNSS]dev_msr_rpt: msr_id = %d, num_obj = %d ", msr_id, num_obj);

	switch(msr_id) {
		case msr_gps_native:
			{
				float snr;
				float elevation;
				float azimuth;
				struct dev_msr_info *gps_msr = (struct dev_msr_info*) cb_data;

				LOGD("[GNSS]dev_msr_rpt: msr_gps_native");

				memset(gps_sv_status, 0, sizeof(GpsSvStatus)); 

				for(cnt = 0; cnt < num_obj; cnt++) {

					if (((((float)gps_msr->snr) / 10.0f) > 0) && 
						(idx < GPS_MAX_SVS) && (gps_msr->svid >= 1) && 
						(gps_msr->svid <= 32)) {

						snr                          = (float)gps_msr->cno * 0.1f;
						gps_sv_status->sv_list[idx].snr       = ceilf(snr * 100) / 100;

						elevation                    = (float)gps_msr->elevation *
							45.0f / 64.0f;
						gps_sv_status->sv_list[idx].elevation = ceilf(elevation * 100) / 100;

						azimuth                      = (float)gps_msr->azimuth *
							45.0f / 32.0f;
						gps_sv_status->sv_list[idx].azimuth   = ceilf(azimuth * 100) / 100;

						gps_sv_status->sv_list[idx].prn       = gps_msr->svid;

						local_bitmap |= (1 << (gps_msr->svid - 1));

						LOGD("[GNSS]gnss_id           [%10d]\n", gps_msr->gnss_id);
						LOGD("[GNSS]svid              [%10d]\n", gps_msr->svid);
						LOGD("[GNSS] ->elevation      [%10d]\n", gps_msr->elevation);
						LOGD("[GNSS] ->azimuth        [%10d]\n", gps_msr->azimuth);
						LOGD("[GNSS] ->idx = %d, cnt = %d\n", idx, cnt);

						idx++;
					}

					gps_msr++;
				}

				gps_sv_status->num_svs = idx;

				populate_gps_sv_status(gps_sv_status, glo_sv_status);

				hal_queue_wr(HAL_GPS_CB_SV_STATUS_ID, h_sv, sizeof(GpsSvStatus));

				msr_rpt_flags |= DEV_SAT_RPT_INFO;
			}
			break;

		case msr_glo_native:
			{
				float snr;
				float elevation;
				float azimuth;
				struct dev_msr_info *glo_msr = (struct dev_msr_info*) cb_data;

				LOGD("[GNSS]dev_msr_rpt: msr_glo_native");

				memset(glo_sv_status, 0, sizeof(GpsSvStatus)); 

				for(cnt = 0; cnt < num_obj; cnt++) {

					if (((((float)glo_msr->snr) / 10.0f) > 0) && 
						(idx < GPS_MAX_SVS) && (glo_msr->svid >= 65) && 
						(glo_msr->svid <= 88)) {

						snr                          = (float)glo_msr->cno * 0.1f;
						glo_sv_status->sv_list[idx].snr       = ceilf(snr * 100) / 100;

						elevation                    = (float)glo_msr->elevation *
							45.0f / 64.0f;
						glo_sv_status->sv_list[idx].elevation = ceilf(elevation * 100) / 100;

						azimuth                      = (float)glo_msr->azimuth *
							45.0f / 32.0f;
						glo_sv_status->sv_list[idx].azimuth   = ceilf(azimuth * 100) / 100;

						glo_sv_status->sv_list[idx].prn       = glo_msr->svid;

						local_bitmap |= (1 << (glo_msr->svid - 65));

						LOGD("[GNSS]gnss_id           [%10d]\n", glo_msr->gnss_id);
						LOGD("[GNSS]svid              [%10d]\n", glo_msr->svid);
						LOGD("[GNSS] ->elevation      [%10d]\n", glo_msr->elevation);
						LOGD("[GNSS] ->azimuth        [%10d]\n", glo_msr->azimuth);
						LOGD("[GNSS] ->idx = %d, cnt = %d\n", idx, cnt);

						idx++;
					}

					glo_msr++;
				}

				glo_sv_status->num_svs = idx;

				populate_gps_sv_status(gps_sv_status, glo_sv_status);

				hal_queue_wr(HAL_GPS_CB_SV_STATUS_ID, h_sv, sizeof(GpsSvStatus));

				msr_rpt_flags |= DEV_SAT_RPT_INFO;
			}
			break;

		case msr_gps_rpt_ie:
			{
				struct gps_msr_info *gps_msr = (struct gps_msr_info *) cb_data;

				LOGD("[GNSS]dev_msr_rpt: msr_gps_rpt_ie");

				for (cnt = 0; cnt < num_obj; cnt++) {

					LOGD("[GNSS]svid              [%10d]\n", gps_msr->svid);
					LOGD("[GNSS]c_no              [%10d]\n", gps_msr->c_no);
					LOGD("[GNSS]doppler           [%10d]\n", gps_msr->doppler);

					gps_msr++;
				}
			}
			break;

		case msr_glo_sig_G1:
		case msr_glo_sig_G2:
			{
				struct glo_msr_info *msr_info = (struct glo_msr_info *) cb_data;
				struct ganss_msr_info *msr;

				LOGD("[GNSS]dev_msr_rpt: msr_glo_sig_G1/msr_glo_sig_G2");

				for (cnt = 0; cnt < num_obj; cnt++) {

					msr = &msr_info->msr;

					LOGD("[GNSS]contents          [%10d]", msr->contents);
					LOGD("[GNSS]svid              [%10d]", msr->svid);
					LOGD("[GNSS]c_no              [%10d]", msr->c_no);
					LOGD("[GNSS]mpath_det         [%10d]", msr->mpath_det);
					LOGD("[GNSS]carrier_qual      [%10d]", msr->carrier_qual);
					LOGD("[GNSS]code_phase        [%10d]", msr->code_phase);
					LOGD("[GNSS]integer_cp        [%10d]", msr->integer_cp);
					LOGD("[GNSS]cp_rms_err        [%10d]", msr->cp_rms_err);
					LOGD("[GNSS]doppler           [%10d]", msr->doppler);
					LOGD("[GNSS]adr               [%10d]", msr->adr);

					msr_info++;
				}
			}
			break;

		case msr_device_result :
			{
				struct dev_msr_result *dev_msr_result = (struct dev_msr_result*) cb_data;

				LOGD("[GNSS]dev_msr_rpt: msr_device_result");
				LOGD("[GNSS]contents          [0x%x]", 
					 dev_msr_result->contents);
				LOGD("[GNSS]msr_qual_status   [%10d]", 
					 dev_msr_result->msr_qual_status);
				LOGD("[GNSS]glo_unc_K         [%10d]", 
					 dev_msr_result->glo_unc_K);	
				LOGD("[GNSS]gps_unc_K         [%10d]", 
					 dev_msr_result->gps_unc_K);
			}
			break;
		default:
			{
				LOGD("Unknown ue_msr_id!!");
			}
			break;
	}

	/* If all required data is available, let app know about location */
	if(msr_rpt_flags & DEV_SAT_RPT_INFO) {

		LOGD("[GNSS]dev_msr_rpt: All data obtained, report to android framework\n");

		h_sv->used_in_fix_mask = sv_used_in_fix->id_flags;
		h_sv->almanac_mask = 0;
		h_sv->ephemeris_mask = 0;
		LOGD("[GNSS]h_sv->used_in_fix_mask [0x%x]\n", h_sv->used_in_fix_mask);

		hal_queue_wr(HAL_GPS_CB_SV_STATUS_ID, h_sv, sizeof(GpsSvStatus));

		/* Reset values */
		msr_rpt_flags            = 0; 
	}

	return 0;
}


/* ti_nmea_sentence  nmea_message */
int dev_nmea_rpt(enum nmea_sn_id nmea, char *cb_data, int length)
{
	LOGD("[GNSS]dev_nmea_rpt: Entering");
	LOGD("[GNSS]dev_nmea_rpt: nmea = %d, length = %d",nmea,length);
	switch(nmea) {
		/** GPS specific sentences */
		case gpgga: {   
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length); 
						break;
					}
		case gpgll: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}     
		case gpgsa: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		case gpgsv: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		case gprmc: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		case gpvtg: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		case glgga: {   
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length); 
						break;
					}
		case glgll: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}     
		case glgsa: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		case glgsv: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		case glrmc: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		case glvtg: {
						hal_queue_wr(HAL_GPS_CB_NMEA_ID, cb_data, length);
						break;
					}
		default:{
					LOGD("[GNSS]dev_nmea_rpt: Unknown nmea!!");
					break;
				}

	}

	LOGD("[GNSS]dev_nmea_rpt: Exiting!");

	return 0;
}

/*  ti_ue_loc_failed  location_err */
int dev_loc_event(enum lc_err_id id)
{
	LOGD("[GNSS]dev_loc_event: Entering");

	switch (id) {

		default:
			LOGD("[GNSS]dev_loc_event: no new position, REI_LOC_EVT received "
				 "with invalid enum lc_err_id [%d]", id);
			break;
	}

	LOGD("[GNSS]dev_loc_event: Exiting!");
	
	return 0;
}

/* ti_dev_agnss_dec  device_agnss */
int dev_agnss_dec(enum loc_assist assist, void *assist_array,int num)
{
	LOGD("[GNSS]dev_agnss_dec: Entering");

	switch (assist) {
		case a_REF_POS:
			{
				struct loc_gnss_info *loc_info = (struct loc_gnss_info *) assist_array;
				struct location_desc *loc      = &loc_info->location;

				LOGD("[GNSS]dev_agnss_dec: a_REF_POS");
				LOGD("[GNSS]fix_type       [%10d]\n", loc_info->fix_type);
				LOGD("[GNSS]gnss_tid       [%10d]\n", loc_info->gnss_tid);
				LOGD("[GNSS]fix_type       [%10d]\n", loc_info->fix_type);
				LOGD("[GNSS]pos_bits       [%10d]\n", loc_info->pos_bits);
				LOGD("[GNSS]shape          [%10d]\n", loc->shape);
				LOGD("[GNSS]lat_sign       [%10d]\n", loc->lat_sign);
				LOGD("[GNSS]latitude       [%10d]\n", loc->latitude_N);
				LOGD("[GNSS]longitude      [%10d]\n", loc->longitude_N);
				LOGD("[GNSS]alt_dir        [%10d]\n", loc->alt_dir);
				LOGD("[GNSS]altitude       [%10d]\n", loc->altitude_N);
				LOGD("[GNSS]unc_semi_major [%10d]\n", loc->unc_semi_maj_K);
				LOGD("[GNSS]unc_semi_minor [%10d]\n", loc->unc_semi_min_K);
				LOGD("[GNSS]orientation    [%10d]\n", loc->orientation);
				LOGD("[GNSS]unc_altitude   [%10d]\n", loc->unc_altitude_K);
				LOGD("[GNSS]confidence     [%10d]\n", loc->confidence);
			}
			break;

		case a_GPS_ALM:
			{
				struct gps_alm *alm = (struct gps_alm *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GPS_ALM");
				LOGD("[GNSS]wna            [%10d]\n", alm->wna);
				LOGD("[GNSS]dataId         [%10d]\n", alm->dataId);
				LOGD("[GNSS]svid           [%10d]\n", alm->svid);
				LOGD("[GNSS]E              [%10d]\n", alm->E);
				LOGD("[GNSS]Toa            [%10d]\n", alm->Toa);
				LOGD("[GNSS]DeltaI         [%10d]\n", alm->DeltaI);
				LOGD("[GNSS]OmegaDot       [%10d]\n", alm->OmegaDot);
				LOGD("[GNSS]Health         [%10d]\n", alm->Health);
				LOGD("[GNSS]SqrtA          [%10d]\n", alm->SqrtA);
				LOGD("[GNSS]OmegaZero      [%10d]\n", alm->OmegaZero);
				LOGD("[GNSS]Mzero          [%10d]\n", alm->Mzero);
				LOGD("[GNSS]Omega          [%10d]\n", alm->Omega);
				LOGD("[GNSS]Af0            [%10d]\n", alm->Af0);
				LOGD("[GNSS]Af1            [%10d]\n", alm->Af1);
			}
			break;

		case a_GPS_EPH:
			{
				struct gps_eph *eph = (struct gps_eph *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GPS_EPH");
				LOGD("[GNSS]svid           [%10d]\n", eph->svid);
				LOGD("[GNSS]is_bcast       [%10d]\n", eph->is_bcast);
				LOGD("[GNSS]status         [%10d]\n", eph->status);
				LOGD("[GNSS]Code_on_L2     [%10d]\n", eph->Code_on_L2);
				LOGD("[GNSS]Ura            [%10d]\n", eph->Ura);
				LOGD("[GNSS]Health         [%10d]\n", eph->Health);
				LOGD("[GNSS]IODC           [%10d]\n", eph->IODC);
				LOGD("[GNSS]Tgd            [%10d]\n", eph->Tgd);
				LOGD("[GNSS]Toc            [%10d]\n", eph->Toc);
				LOGD("[GNSS]Af2            [%10d]\n", eph->Af2);
				LOGD("[GNSS]Af1            [%10d]\n", eph->Af1);
				LOGD("[GNSS]Af0            [%10d]\n", eph->Af0);
				LOGD("[GNSS]Crs            [%10d]\n", eph->Crs);
				LOGD("[GNSS]DeltaN         [%10d]\n", eph->DeltaN);
				LOGD("[GNSS]Mo             [%10d]\n", eph->Mo);
				LOGD("[GNSS]Cuc            [%10d]\n", eph->Cuc);
				LOGD("[GNSS]E              [%10d]\n", eph->E);
				LOGD("[GNSS]Cus            [%10d]\n", eph->Cus);
				LOGD("[GNSS]SqrtA          [%10d]\n", eph->SqrtA);
				LOGD("[GNSS]Toe            [%10d]\n", eph->Toe);
				LOGD("[GNSS]FitIntFlag     [%10d]\n", eph->FitIntervalFlag);
				LOGD("[GNSS]Aodo           [%10d]\n", eph->Aodo);
				LOGD("[GNSS]Cic            [%10d]\n", eph->Cic);
				LOGD("[GNSS]Omega0         [%10d]\n", eph->Omega0);
				LOGD("[GNSS]Cis            [%10d]\n", eph->Cis);
				LOGD("[GNSS]Io             [%10d]\n", eph->Io);
				LOGD("[GNSS]Crc            [%10d]\n", eph->Crc);
				LOGD("[GNSS]Omega          [%10d]\n", eph->Omega);
				LOGD("[GNSS]OmegaDot       [%10d]\n", eph->OmegaDot);
				LOGD("[GNSS]Idot           [%10d]\n", eph->Idot);
			}
			break;

		case a_GPS_TIM:
			{
				struct gps_ref_time *tim = (struct gps_ref_time *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GPS_TIM");
				LOGD("[GNSS]n_wk_ro        [%10d]\n", tim->time.n_wk_ro);
				LOGD("[GNSS]week_nr        [%10d]\n", tim->time.week_nr);
				LOGD("[GNSS]tow_unit       [%10d]\n", tim->time.tow_unit);
				LOGD("[GNSS]tow            [%10d]\n", tim->time.tow);
			}
			break;

		case a_GPS_ION:
			{
				struct gps_ion *ion = (struct gps_ion *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GPS_ION");
				LOGD("[GNSS]Alpha0         [%10d]\n", ion->Alpha0);
				LOGD("[GNSS]Alpha1         [%10d]\n", ion->Alpha1);
				LOGD("[GNSS]Alpha2         [%10d]\n", ion->Alpha2);
				LOGD("[GNSS]Alpha3         [%10d]\n", ion->Alpha3);
				LOGD("[GNSS]Beta0          [%10d]\n", ion->Beta0);
				LOGD("[GNSS]Beta1          [%10d]\n", ion->Beta1);
				LOGD("[GNSS]Beta2          [%10d]\n", ion->Beta2);
				LOGD("[GNSS]Beta3          [%10d]\n", ion->Beta3);
			}
			break;

		case a_GPS_UTC:
			{
				struct gps_utc *utc = (struct gps_utc *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GPS_UTC");
				LOGD("[GNSS]A1             [%10d]\n", utc->A1);
				LOGD("[GNSS]A0             [%10d]\n", utc->A0);
				LOGD("[GNSS]Tot            [%10d]\n", utc->Tot);
				LOGD("[GNSS]WNt            [%10d]\n", utc->WNt);
				LOGD("[GNSS]DeltaTls       [%10d]\n", utc->DeltaTls);
				LOGD("[GNSS]WNlsf          [%10d]\n", utc->WNlsf);
				LOGD("[GNSS]DN             [%10d]\n", utc->DN);
				LOGD("[GNSS]DeltaTlsf      [%10d]\n", utc->DeltaTlsf);
			}
			break;

		case a_GLO_ALM:
			{
				struct glo_alm *alm = (struct glo_alm *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GLO_ALM");
				LOGD("[GNSS]TauC           [%10d]\n", alm->TauC);
				LOGD("[GNSS]N4             [%10d]\n", alm->N4);
				LOGD("[GNSS]TauGPS         [%10d]\n", alm->TauGPS);
				LOGD("[GNSS]NA             [%10d]\n", alm->NA);
				LOGD("[GNSS]nA             [%10d]\n", alm->nA);
				LOGD("[GNSS]HnA            [%10d]\n", alm->HnA);
				LOGD("[GNSS]Lambda_nA      [%10d]\n", alm->Lambda_nA);
				LOGD("[GNSS]t_Lambda_nA    [%10d]\n", alm->t_Lambda_nA);
				LOGD("[GNSS]Delta_i_nA     [%10d]\n", alm->Delta_i_nA);
				LOGD("[GNSS]Delta_T_nA     [%10d]\n", alm->Delta_T_nA);
				LOGD("[GNSS]Delta_T_dot_nA [%10d]\n", alm->Delta_T_dot_nA);
				LOGD("[GNSS]Epsilon_nA     [%10d]\n", alm->Epsilon_nA);
				LOGD("[GNSS]Omega_nA       [%10d]\n", alm->Omega_nA);
				LOGD("[GNSS]M_nA           [%10d]\n", alm->M_nA);
				LOGD("[GNSS]B1             [%10d]\n", alm->B1);
				LOGD("[GNSS]B2             [%10d]\n", alm->B2);
				LOGD("[GNSS]KP             [%10d]\n", alm->KP);
				LOGD("[GNSS]Tau_nA         [%10d]\n", alm->Tau_nA);
				LOGD("[GNSS]C_nA           [%10d]\n", alm->C_nA);
			}
			break;

		case a_GLO_EPH:
			{
				struct glo_eph *eph = (struct glo_eph *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GLO_EPH");
				LOGD("[GNSS]the_svid       [%10d]\n", eph->the_svid);
				LOGD("[GNSS]is_bcast       [%10d]\n", eph->is_bcast);
				LOGD("[GNSS]m              [%10d]\n", eph->m);
				LOGD("[GNSS]tk             [%10d]\n", eph->tk);
				LOGD("[GNSS]tb             [%10d]\n", eph->tb);
				LOGD("[GNSS]M              [%10d]\n", eph->M);
				LOGD("[GNSS]Gamma          [%10d]\n", eph->Gamma);
				LOGD("[GNSS]Tau            [%10d]\n", eph->Tau);
				LOGD("[GNSS]Xc             [%10d]\n", eph->Xc);
				LOGD("[GNSS]Yc             [%10d]\n", eph->Yc);
				LOGD("[GNSS]Zc             [%10d]\n", eph->Zc);
				LOGD("[GNSS]Xv             [%10d]\n", eph->Xv);
				LOGD("[GNSS]Yv             [%10d]\n", eph->Yv);
				LOGD("[GNSS]Zv             [%10d]\n", eph->Zv);
				LOGD("[GNSS]Xa             [%10d]\n", eph->Xa);
				LOGD("[GNSS]Ya             [%10d]\n", eph->Ya);
				LOGD("[GNSS]Za             [%10d]\n", eph->Za);
				LOGD("[GNSS]P              [%10d]\n", eph->P);
				LOGD("[GNSS]NT             [%10d]\n", eph->NT);
				LOGD("[GNSS]n              [%10d]\n", eph->n);
				LOGD("[GNSS]FT             [%10d]\n", eph->FT);
				LOGD("[GNSS]En             [%10d]\n", eph->En);
				LOGD("[GNSS]Bn             [%10d]\n", eph->Bn);
				LOGD("[GNSS]P1             [%10d]\n", eph->P1);
				LOGD("[GNSS]P2             [%10d]\n", eph->P2);
				LOGD("[GNSS]P3             [%10d]\n", eph->P3);
				LOGD("[GNSS]P4             [%10d]\n", eph->P4);
				LOGD("[GNSS]Delta_Tau      [%10d]\n", eph->Delta_Tau);
				LOGD("[GNSS]ln             [%10d]\n", eph->ln);
			}
			break;

		case a_GLO_TIM:
			{
				struct ganss_ref_time *tim = (struct ganss_ref_time *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GLO_TIM");
				LOGD("[GNSS]tid            [%10d]\n", tim->tid);
				LOGD("[GNSS]day            [%10d]\n", tim->day);
				LOGD("[GNSS]tod            [%10d]\n", tim->tod);
				LOGD("[GNSS]tod_unc        [%10d]\n", tim->tod_unc);
				LOGD("[GNSS]accuracy       [%10d]\n", tim->accuracy);
			}
			break;

		case a_GLO_ION:
			{
				struct ganss_addl_iono *ion = (struct ganss_addl_iono *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GLO_ION");
				LOGD("[GNSS]data_id        [%10d]\n", ion->data_id);
				LOGD("[GNSS]Alpha0         [%10d]\n", ion->alfa0);
				LOGD("[GNSS]Alpha1         [%10d]\n", ion->alfa1);
				LOGD("[GNSS]Alpha2         [%10d]\n", ion->alfa2);
				LOGD("[GNSS]Alpha3         [%10d]\n", ion->alfa3);
				LOGD("[GNSS]Beta0          [%10d]\n", ion->beta0);
				LOGD("[GNSS]Beta1          [%10d]\n", ion->beta1);
				LOGD("[GNSS]Beta2          [%10d]\n", ion->beta2);
				LOGD("[GNSS]Beta3          [%10d]\n", ion->beta3);
			}
			break;

		case a_GLO_UTC:
			{
				struct glo_utc_model *utc = (struct glo_utc_model *) assist_array;

				LOGD("[GNSS]dev_agnss_dec: a_GLO_UTC");
				LOGD("[GNSS]NA             [%10d]\n", utc->NA);
				LOGD("[GNSS]TauC           [%10d]\n", utc->TauC);
				LOGD("[GNSS]B1             [%10d]\n", utc->B1);
				LOGD("[GNSS]B2             [%10d]\n", utc->B2);
				LOGD("[GNSS]KP             [%10d]\n", utc->KP);
			}
			break;

		default:
			LOGD("[GNSS]dev_agnss_dec: invalid assist id");
			break;
	}

	LOGD("[GNSS]dev_agnss_dec: Exiting!");
	return 0;
}

/*ti_dev_error_ind  device_error*/
int dev_error_ind(enum dev_err_id id)
{
	LOGD("[GNSS]dev_error_ind: Entering");

	LOGD("[GNSS]dev_error_ind: Exiting!");
	return 0;
}

/*ti_dev_event_ind  device_event*/
int dev_event_ind(enum dev_evt_id id, void *data_array, int size)
{
	LOGD("[GNSS]dev_event_ind: Entering");

	LOGD("[GNSS]dev_event_ind: Exiting!");
	return 0;
}

/*ti_app_ucase_ind  ucase_notify;*/
int dev_app_ucase_ind(enum app_rpt_id id, void *data)
{
	LOGD("[GNSS]dev_event_ind: Entering");

	LOGD("[GNSS]dev_event_ind: Exiting!");
	return 0;
}

struct ti_dev_callbacks dev_callbacks = {

	dev_location_rpt,
	dev_msr_rpt,
	dev_loc_event,
	dev_nmea_rpt,
	dev_agnss_dec,
	dev_error_ind,
	dev_event_ind,
	dev_app_ucase_ind	
};

static int hal_init(GpsCallbacks *callbacks)
{
#define GNSS_USE_GPS  0
#define GNSS_USE_BOTH 1
#define GNSS_USE_GLO  2

	int retval;
	struct ti_dev_proxy_params ti_dpParams;
	ti_dpParams.fn_call_timeout_ms = 1000;
	struct dev_start_up_info start_info;
	time_t bootup_time = 0;
	static char init_flag = 0;

	if (init_flag)
		return 0;

	LOGD("[GNSS]hal_init: Entering \n");
	hal_desc = &hal_desc_obj;
	hal_desc->gps_callbacks = callbacks;
	/* Callback to inform framework of the GPS engine's capabilities*/
	hal_desc->gps_callbacks->set_capabilities_cb(GPS_CAPABILITY_SCHEDULING | GPS_CAPABILITY_MSA | GPS_CAPABILITY_MSB);
	/* Init mutex and semaphore   */
	sem_init(&(hal_desc->sem), 0, 1);
	pthread_mutex_init(&(hal_desc->mtx), NULL);

	/*  Memory Initialization   */
	dev2hal_mem_init(hal_desc->dev2hal);

	hal_status.status = GPS_STATUS_ENGINE_ON;
	hal_queue_wr(HAL_GPS_CB_STATUS_ID, &hal_status, sizeof(GpsStatus));

	hal_desc->th = hal_desc->gps_callbacks->create_thread_cb("hal_thread", 
															 thread_rx, 
															 (void *)hal_desc);

	set_devproxy_server(1);

	retval = ti_dev_proxy_connect(&dev_callbacks, &ti_dpParams); 
	if(retval == -1){
		LOGD("devproxy_init: ti_dev_proxy_connect FAILED !!! \n");
		return -1;
	}

	start_info.nvs_aid_cfg.aid_config = 1; 	//  aid_push_all   
	start_info.nvs_aid_cfg.epoch_secs = 1;
	strncpy(start_info.config_file, CONFIG_FILE_PATH, sizeof(start_info.config_file)-1);
	strncpy(start_info.sevice_pack, SEVICE_PACK_FILE_PATH, sizeof(start_info.sevice_pack)-1);

	ti_dev_set_app_profile(GNSS_USE_BOTH, 0);

	bootup_time = time(NULL);
	ti_dev_connect(&start_info);
	LOGD("[GNSS]bootup_time [%d]", (int)(time(NULL) - bootup_time));

	ti_dev_exec_action(dev_dsleep);

	init_flag = 1;

	LOGD("[GNSS]hal_init: Exiting \n");
	return 0;
}

static unsigned int async_events;
static unsigned int dev_agnss;
static unsigned int nmea_flags;
static unsigned int msr_reports;
static unsigned int pos_reports;
static unsigned short interval;

static int hal_start(void)
{
	msr_rpt_flags = 0;
	loc_rpt_flags = 0;
	sv_used_in_fix = &gps_sv_used_in_fix;
	sv_used_in_fix->is_valid = 0;
	sv_used_in_fix->id_flags = 0;

	LOGD("[GNSS]hal_start: Entering \n");

	ttff_flag = 0;
	ttff = time(NULL);

	async_events = ASYC_EVT_NEW_EPH | ASYC_EVT_NEW_ALM | ASYC_EVT_NEW_HLT 
		| ASYC_EVT_NEW_UTC | ASYC_EVT_NEW_ION;

	dev_agnss = 0;

	/* SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) | 
	   SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg) | 
	   SET_BIT(glgga) | SET_BIT(glgll) | SET_BIT(glgsa) | 
	   SET_BIT(glgsv) | SET_BIT(glrmc) | SET_BIT(glvtg)
	 */
	nmea_flags = SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) | 
		SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg) | 
		SET_BIT(glgga) | SET_BIT(glgll) | SET_BIT(glgsa) | 
		SET_BIT(glgsv) | SET_BIT(glrmc) | SET_BIT(glvtg);

	/* MSR_RPT_GPS_NATIVE | MSR_RPT_GPS_IE_SEL | MSR_RPT_GLO_NATIVE | 
	   MSR_RPT_GLO_IE_SEL 
	 */
	msr_reports = MSR_RPT_GPS_NATIVE | MSR_RPT_GLO_NATIVE | MSR_RPT_DEV_RESULT;

	/* POS_RPT_LOC_NATIVE | POS_RPT_GPS_IE_SEL | POS_RPT_GLO_IE_SEL | 
	   POS_RPT_VEL_NATIVE | POS_RPT_VEL_IE_SEL | 
	   POS_RPT_TIM_GPS_NATIVE | POS_RPT_TIM_GLO_NATIVE | 
	   POS_RPT_TIM_GPS_IE_SEL | POS_RPT_TIM_GLO_IE_SEL
	 */
	pos_reports = POS_RPT_NATIVE_ALL | POS_RPT_TIM_GPS_NATIVE | 
		POS_RPT_TIM_GLO_NATIVE | POS_RPT_VEL_NATIVE | POS_RPT_GPS_IE_SEL | 
		POS_RPT_GLO_IE_SEL;

	if (ti_dev_config_reports(async_events, 
							  dev_agnss, nmea_flags, msr_reports, 
							  pos_reports, interval) < 0) {
		LOGD("ti_dev_config_reports failed\n");
		return -1;
	} else {
		LOGD("Successfully set TI dev-proxy parameters:\n");
		LOGD("async_events  : [0x%x]\n", async_events);
		LOGD("dev_agnss     : [0x%x]\n", dev_agnss);
		LOGD("nmea_flags    : [%d]\n", nmea_flags);
		LOGD("msr_reports   : [0x%x]\n", msr_reports);
		LOGD("pos_reports   : [0x%x]\n", pos_reports);
		LOGD("interval      : [%d]\n", interval);
	}

	ti_dev_exec_action(dev_active);

	hal_status.status = GPS_STATUS_SESSION_BEGIN;
	hal_queue_wr(HAL_GPS_CB_STATUS_ID, &hal_status, sizeof(GpsStatus));

	LOGD("[GNSS]hal_start: Exiting! \n");
	return 0;
}

static int hal_stop(void)
{
	LOGD("[GNSS]hal_stop: Entering \n");

	if (ti_dev_config_reports(0, 0, 0, 0, 0, 0) < 0) {
		LOGD("ti_dev_config_reports failed\n");
		return -1;
	} else {
		LOGD("Successfully set TI dev-proxy parameters:\n");
		LOGD("async_events  : [0x%x]\n", async_events);
		LOGD("dev_agnss     : [0x%x]\n", dev_agnss);
		LOGD("nmea_flags    : [%d]\n", nmea_flags);
		LOGD("msr_reports   : [0x%x]\n", msr_reports);
		LOGD("pos_reports   : [0x%x]\n", pos_reports);
		LOGD("interval      : [%d]\n", interval);
	}

	ti_dev_exec_action(dev_de_act);

	ti_dev_exec_action(dev_dsleep);
	hal_status.status = GPS_STATUS_SESSION_END;
	hal_queue_wr(HAL_GPS_CB_STATUS_ID, &hal_status, sizeof(GpsStatus));

	sv_used_in_fix = &gps_sv_used_in_fix;
	sv_used_in_fix->is_valid = 0;
	sv_used_in_fix->id_flags = 0;
	msr_rpt_flags = 0;
	loc_rpt_flags = 0;

	LOGD("[GNSS]hal_stop: Exiting! \n");
	return 0;
}

void hal_cleanup(void)
{
	void* ignored_val;

	LOGD("[GNSS]hal_cleanup: Entering \n");
	LOGD("[GNSS]hal_cleanup: Exiting! \n");
	
	return;

	ti_dev_release();

	ti_dev_proxy_release();

	thread_rx_exit = 1;

	hal_status.status = GPS_STATUS_ENGINE_OFF;
	hal_queue_wr(HAL_GPS_CB_STATUS_ID, &hal_status, sizeof(GpsStatus));
	dev2hal_mem_free(hal_desc->dev2hal);

	/* destroy semaphore */
	sem_destroy(&(hal_desc->sem)); 

	/*  Deleting the thread  */;
	pthread_join(hal_desc->th, &ignored_val);

	set_devproxy_server(0);

	LOGD("[GNSS]hal_cleanup: Exiting! \n");
}

void set_devproxy_server(int state)
{
	if (state == 1) {
		if (property_set("ctl.start", "devproxy") < 0) {
			LOGD("Failed to start devproxy\n");
			return;
			}
		
		/* Allow Navl server to init */
		sleep(6);
		LOGD("devproxy START \n");
	} else if (state == 0) {
		LOGD("devproxy STOP \n");
	}
}

int hal_inject_time(GpsUtcTime time, int64_t timeReference, int uncertainty)
{
	LOGD("[GNSS]hgps_inject_time: Entering \n");

	LOGD("[GNSS]hgps_inject_time: Exiting! \n");
	return 0;
}

int hal_inject_location(double latitude, double  longitude, float accuracy)
{
	LOGD("[GNSS]hal_inject_location: Entering \n");

	LOGD("[GNSS]hal_inject_location: Exiting! \n");
	return 0;
}

void hal_delete_aiding_data(GpsAidingData flags)
{
	enum loc_assist assist;
	unsigned int sv_id_map;
	unsigned int mem_flags;

	LOGD("[GNSS]hal_delete_aiding_data: Entering \n");

	LOGD("[GNSS]hal_delete_aiding_data: flags [0x%x]\n", flags);

	if (flags & GPS_DELETE_EPHEMERIS) {
		assist = a_GPS_EPH;
		sv_id_map = 0xFFFFFFFF;
		mem_flags = MEM_SRV_RAM | MEM_SRV_NVM | MEM_DEV_RAM;
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_EPHEMERIS.\n");

		ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);
	}

	if (flags & GPS_DELETE_ALMANAC) {
		assist = a_GPS_ALM;
		sv_id_map = 0xFFFFFFFF;
		mem_flags = MEM_SRV_RAM | MEM_SRV_NVM | MEM_DEV_RAM;
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_ALMANAC.\n");

		ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);
	}

	if (flags & GPS_DELETE_POSITION) {
		assist = a_REF_POS;
		sv_id_map = 1;
		mem_flags = MEM_SRV_RAM | MEM_SRV_NVM | MEM_DEV_RAM;
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_POSITION.\n");

		ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);
	}

	if (flags & GPS_DELETE_TIME) {
		assist = a_GPS_TIM;
		sv_id_map = 1;
		mem_flags = MEM_SRV_RAM | MEM_SRV_NVM | MEM_DEV_RAM;
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_TIME.\n");

		ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);
	}

	if (flags & GPS_DELETE_IONO) {
		assist = a_GPS_ION;
		sv_id_map = 1;
		mem_flags = MEM_SRV_RAM | MEM_SRV_NVM | MEM_DEV_RAM;
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_IONO.\n");

		ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);
	}

	if (flags & GPS_DELETE_UTC) {
		assist = a_GPS_UTC;
		sv_id_map = 1;
		mem_flags = MEM_SRV_RAM | MEM_SRV_NVM | MEM_DEV_RAM;
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_UTC.\n");

		ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);
	}

	/* Health is removed for devproxy interface */
	/* if (flags & GPS_DELETE_HEALTH) {
		assist = a_GPS_HLT;
		sv_id_map = 1;
		mem_flags = MEM_SRV_RAM | MEM_SRV_NVM | MEM_DEV_RAM;
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_HEALTH.\n");

		ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);
	}*/

	if (flags & GPS_DELETE_SVDIR) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_SVDIR not supported.\n");
	}

	if (flags & GPS_DELETE_SVSTEER) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_SVSTEER not supported.\n");
	}

	if (flags & GPS_DELETE_SADATA) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_SADATA not supported.\n");
	}

	if (flags & GPS_DELETE_RTI) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_RTI not supported.\n");
	}

	if (flags & GPS_DELETE_CELLDB_INFO) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_CELLDB_INFO not supported.\n");
	}

	if (flags & GPS_DELETE_SVDIR) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_SVDIR not supported.\n");
	}
	if (flags & GPS_DELETE_SVSTEER) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_SVSTEER not supported.\n");
	}
	if (flags & GPS_DELETE_SADATA) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_SADATA not supported.\n");
	}
	if (flags & GPS_DELETE_RTI) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_RTI not supported.\n");
	}
	if (flags & GPS_DELETE_CELLDB_INFO) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_CELLDB_INFO not supported.\n");
	}
	if (flags & GPS_DELETE_ALL) {
		LOGD("[GNSS]hal_delete_aiding_data: GPS_DELETE_ALL not supported.\n");
	}

	LOGD("[GNSS]hal_delete_aiding_data: Exiting! \n");
}

static int hal_set_position_mode(GpsPositionMode mode, 
								 GpsPositionRecurrence recurrence, 
								 uint32_t min_interval, 
								 uint32_t preferred_accuracy, 
								 uint32_t preferred_time)
{

	async_events = 0;

	dev_agnss = 0;

	/* SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) | 
	   SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg) | 
	   SET_BIT(glgga) | SET_BIT(glgll) | SET_BIT(glgsa) | 
	   SET_BIT(glgsv) | SET_BIT(glrmc) | SET_BIT(glvtg)
	 */
	nmea_flags = SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) | 
		SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg) | SET_BIT(glgga);

	/* MSR_RPT_GPS_NATIVE | MSR_RPT_GPS_IE_SEL | MSR_RPT_GLO_NATIVE | 
	   MSR_RPT_GLO_IE_SEL 
	 */
	msr_reports = MSR_RPT_GPS_NATIVE;

	/* POS_RPT_LOC_NATIVE | POS_RPT_GPS_IE_SEL | POS_RPT_GLO_IE_SEL | 
	   POS_RPT_VEL_NATIVE | POS_RPT_VEL_IE_SEL | 
	   POS_RPT_TIM_GPS_NATIVE | POS_RPT_TIM_GLO_NATIVE | 
	   POS_RPT_TIM_GPS_IE_SEL | POS_RPT_TIM_GLO_IE_SEL
	 */
	/*pos_reports = POS_RPT_NATIVE_ALL | POS_RPT_TIM_GPS_NATIVE 
	  | POS_RPT_VEL_NATIVE;*/
	pos_reports = POS_RPT_GPS_IE_SEL | POS_RPT_SAT_NATIVE | 
		POS_RPT_DOP_NATIVE | POS_RPT_TIM_GPS_IE_SEL | POS_RPT_VEL_IE_SEL;

	interval = min_interval;

	LOGD("[GNSS]hal_set_position_mode: Entering \n");
	LOGD("[GNSS]hal_set_position_mode: mode                [%d]\n", mode);
	LOGD("[GNSS]hal_set_position_mode: min_interval        [%d]\n", min_interval);
	LOGD("[GNSS]hal_set_position_mode: recurrence          [%d]\n", recurrence);
	LOGD("[GNSS]hal_set_position_mode: preferred_accuracy  [%d]\n", preferred_accuracy);
	LOGD("[GNSS]hal_set_position_mode: preferred_time      [%d]\n", preferred_time);
	hal_status.status = GPS_STATUS_SESSION_BEGIN;
	hal_queue_wr(HAL_GPS_CB_STATUS_ID, &hal_status,sizeof(GpsStatus));

	LOGD("[GNSS]hal_set_position_mode: Exiting! \n");
	return 0;
}


const void* hal_get_extension(const char* name)
{	
	unsigned char res= 0;

	LOGD("[GNSS]hal_get_extension: Entering \n");

	/* Include SUPL Support. */
	res = strcmp(name, AGPS_INTERFACE);
	if (res == 0)
	{
		if (locFixMode > 0 )
		{
			LOGD("[GNSS]hgps_get_extension: Returning AGPSInterface\n");
			return (void *)&agps_if;
		}
		else
		{
			return NULL;
		}
	}

	/* No XTRA Interface Support Included. */
	res = strcmp(name, GPS_XTRA_INTERFACE);
	if (res == 0)
	{
		/*Not Implemented */
		return NULL;
	}

	LOGD("[GNSS]hal_get_extension: Exiting Successfully \n");

	return NULL;
}

GpsInterface gps_if = {

	sizeof(GpsInterface),
	hal_init,
	hal_start,
	hal_stop,
	hal_cleanup,
	hal_inject_time,		
	hal_inject_location, 
	hal_delete_aiding_data,  
	hal_set_position_mode,
	hal_get_extension,

}; 

/*-----------------------------------------------------------------------------
 *  GPS Framework Xtra Callbacks i.e. GpsXtraInterface
 *----------------------------------------------------------------------------*/

/* Add functions */

int hal_xtra_init(GpsXtraCallbacks* callbacks)
{
	LOGD("[GNSS]hal_xtra_init: Entering \n");

	LOGD("[GNSS]hal_xtra_init: Exiting Successfully \n");

	return 0;
}


int hal_xtra_data(char *data, int length)
{
	LOGD("[GNSS]hgps_xtra_data: Entering \n");

	LOGD("[GNSS]hgps_xtra_data: Exiting Successfully \n");

	return 0;
}

GpsXtraInterface gps_xtra_if = {

	sizeof(GpsXtraInterface),
	hal_xtra_init,
	hal_xtra_data,

};

/*------------------------------------------------------------------------------
 *  A-GPS Framework Callbacks i.e. AGpsInterface
 *----------------------------------------------------------------------------*/


/* Add functions */

void hal_agps_init(AGpsCallbacks *agpscb)
{
	LOGD("[GNSS]hal_agps_init: Entering \n");

	LOGD("[GNSS]hal_agps_init: Exiting Successfully \n");
	return;
}


int hal_agps_data_conn_open(const char *apn)
{
	LOGD("[GNSS]hal_agps_data_conn_open: Entering \n");

	LOGD("[GNSS]hal_agps_data_conn_open: Exiting Successfully \n");
	return 0;
}

int hal_agps_data_conn_closed()
{
	LOGD("[GNSS]hal_agps_data_conn_closed: Entering \n");

	LOGD("[GNSS]hal_agps_data_conn_closed: Exiting Successfully \n");
	return 0;
}

int hal_agps_data_conn_failed()
{
	LOGD("[GNSS]hal_agps_data_conn_failed: Entering \n");

	LOGD("[GNSS]hal_agps_data_conn_failed: Exiting Successfully \n");
	return 0;
}

int hal_agps_set_server(AGpsType type, const char* hostname, int port )
{
	LOGD("[GNSS]hal_agps_set_server: Entering \n");

	LOGD("[GNSS]hal_agps_set_server: Exiting Successfully \n");
	return 0;
}

AGpsInterface agps_if = {

	sizeof(AGpsInterface),
	hal_agps_init,
	hal_agps_data_conn_open,
	hal_agps_data_conn_closed,
	hal_agps_data_conn_failed,
	hal_agps_set_server,
};

/*------------------------------------------------------------------------------
 *  NI Framework Interface i.e. GpsNiInterface
 *----------------------------------------------------------------------------*/

/* Add funcitons */

GpsNiInterface gps_ni_if = {

	.size = sizeof(GpsNiInterface)
};

/*------------------------------------------------------------------------------
 *  AGPS RIL Framework Interface i.e. AGpsRilInterface
 *----------------------------------------------------------------------------*/

/* Add functions */

AGpsRilInterface agps_ril_if = {
	.size = sizeof(AGpsRilInterface)
};

/*------------------------------------------------------------------------------
 *  AGPS RIL Framework Interface i.e. AGpsRilInterface
 *----------------------------------------------------------------------------*/

GpsDebugInterface gps_debug_if = {
	.size = sizeof(GpsDebugInterface)
};

const GpsInterface* ti_get_gps_interface(struct gps_device_t* dev)
{
	LOGD("[GNSS]ti_get_gps_interface: Entering \n");
	LOGD("[GNSS]ti_get_gps_interface: Exiting Successfully \n");
	return &gps_if;
}

static int open_gps(const struct hw_module_t* module, char const* name,
					struct hw_device_t** device)
{
	struct gps_device_t *dev = (struct gps_device_t *) malloc(sizeof(struct gps_device_t));
	if (!dev) {
		LOGD("[GNSS]open_gps: Error no memory\n");
		return -1;
	}
	
	memset(dev, 0, sizeof(*dev));
	LOGD("[GNSS]open_gps: Entering \n");
	dev->common.tag = HARDWARE_DEVICE_TAG;
	dev->common.version = 0;
	dev->common.module = (struct hw_module_t*)module;
	dev->get_gps_interface = ti_get_gps_interface;

	*device = (struct hw_device_t*)dev;
	LOGD("[GNSS]open_gps: Exiting Successfully \n");
	return 0;
}


static struct hw_module_methods_t ti_gps_module_methods = {
	.open = open_gps
};

const struct hw_module_t HAL_MODULE_INFO_SYM = {
	.tag = HARDWARE_MODULE_TAG,
	.version_major = 1,
	.version_minor = 0,
	.id = GPS_HARDWARE_MODULE_ID,
	.name = "TI GPS Module",
	.author = "TI",
	.methods = &ti_gps_module_methods,
};

