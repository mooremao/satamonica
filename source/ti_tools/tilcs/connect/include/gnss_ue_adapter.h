
#ifndef _GNSS_UE_ADAPTER_H_
#define _GNSS_UE_ADAPTER_H_

#include "gnss_lcs_common.h"
#include "gnss_lcs_private.h"

struct ti_gnss_ue_callbacks {

	/** Handle provided by TI A-GNSS Connect to be included in callbacks */
	void                      *hnd;           

	ti_gnfn_ue_decoded_aid     ue_decoded_aid;   /**< UE decoded LOC aiding */
	ti_gnfn_ue_loc_results     ue_loc_results;   /**< LOC info worked by UE */
	ti_gnfn_ue_nmea_report     ue_nmea_report;   /**< NMEA sentence from UE */

	ti_gnfn_ue_aux_info4sv     ue_aux_info4sv;   /**< Aux info about SAT(s) */

	ti_gnfn_ue_cw_test_results		ue_cw_test_results;	/**<Production Line test report>*/
        pv_gnfn_update_ue_time     update_ue_time;
	pv_gnfn_ue_device_err      handle_ue_err;/* add for GNSS recovery ++*/
	ti_gnfn_ue_recovery_ind    ue_recovery_ind;
};

/** This construct specifies the set of API(s) that will be called by TI's A-GNSS
  Connect framework to configure, operate and get device specific info about TI 
  GNSS receiver. The A-GNSS Connect also provisions a set of callback functions 
  to be utilized by the GNSS device / UE to deliver the supported LOC services.

  LOC services include both POS and MSR estimates.
 */

struct ti_gnss_ue_interface {

	/** To save implementation specific references / handle */ 
	void                       *hnd; /**< No assumption made about use */

	/** Initialize UE and provide reference to callback structure / funcs  */  
	int (*init)(const struct ti_gnss_ue_callbacks *gnss_ue_cb);
	int (*exit)(void); 

	/** Specify a start function for creation of thread by A-GNSS Connect at
	  the time of initialization of adapter implementation. This specified
	  function acts as an entry point to the implementation of the adapter
	  CB thread. The callbacks MUST be invoked from this thread.
	 */
	void *(*cb_thread_func)(void*);
	void                      *cb_thread_arg1; /**< Arg of entry point fn */


	ti_gnfn_loc_start          loc_start;        /**< Start LOC operation  */
	ti_gnfn_loc_stop           loc_stop;         /**< Stop  LOC operation  */

	ti_gnfn_get_ue_loc_caps    get_ue_loc_caps;  /**< Get LOC capabilities */

	/** Get capabilities of UE to support external GNSS assistance */
	ti_gnfn_get_ue_agnss_caps  get_assist_caps;  /**< Assist handled by UE */ 

	ti_gnfn_set_loc_interval   set_loc_interval; /**< Interval is in secs  */
	ti_gnfn_set_intended_qop   set_intended_qop; /**< Quality of position  */

	/* API(s) to support external assist and manage device aiding */
	ti_gnfn_ue_add_assist      ue_add_assist;    /**< Add assist from NW   */
	ti_gnfn_ue_del_aiding      ue_del_aiding;    /**< Del aiding from UE   */ 
	ti_gnfn_ue_get_aiding      ue_get_aiding;    /**< Get aiding from UE   */

	pv_gnfn_get_ue_req4assist  get_ue_req4assist;/**< Identify reqd assist */ 
	pv_gnfn_get_ue_aid_ttl     get_ue_aid_ttl;

	/* : include end of assist declaration */
	pv_gnfn_assist2ue_done     assist2ue_done;   /**< Completed assist2UE  */
        pv_gnfn_ue_is_msr_filter_gps_eph_reqd ue_is_msr_filter_gps_eph_reqd;
        pv_gnfn_ue_msr_filter_gps_eph         ue_msr_filter_gps_eph;
        pv_gnfn_ue_put2sleep        ue_put2sleep;
        pv_gnfn_ue_wake2idle        ue_wake2idle;
	ti_gnfn_ue_dev_oper_param	oper_ue_dev_param;
	pv_gnfn_ue_err_recovery         ue_err_recovery;/* add for GNSS recovery ++*/
	pv_gnfn_trigger_test_err		trigger_test_err;/* add for GNSS recovery ++*/
	ti_gnfn_devinit				dev_init;
	ti_gnfn_devplt				dev_plt;
};

#endif

