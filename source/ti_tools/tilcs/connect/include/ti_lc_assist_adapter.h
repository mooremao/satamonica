#ifndef _LC_ASSIST_ADAPTER_H_
#define _LC_ASSIST_ADAPTER_H_

#include "gnss_lcs_common.h"

/** This file describes an interface that enables the binding of external source
    of GNSS assistance data to "A-GNSS Connect". The specified interface must be
    implemented to adapt the needs of "A-GNSS Connect" to access GNSS assistance
    data from a standalone entity that is purely a source of assistance. Example 
    of such entities include SaGPS and PGPS. 

    The interface, henceforth called "Assist Adapter", is expected to bridge the
    nuances of the design of "A-GNSS Connect" with those of the source of assist.

    There are two parts to definition of "Assist Adapter": 
    a) "Assist Adapter" interface
    b) "Assist Adapter" callbacks
*/

/** This construct describes a select set of concerns of "A-GNSS Connect" module
    that can be invoked by adapter to provide information and / or report status
    of external entity. This callback interface is specified by "A-GNSS Connect"
    as part of initialization of adapter interace and MUST NOT be invoked in the 
    context of "A-GNSS Connect". Specifically, the interface MUST be called from
    a thread that is exclusive to the instance of the adapter being implemented.

    In this version of software, the construct does not introduce any property
    in addition to the ones that are inherited. 
*/
    
struct ti_lc_assist_adap_cb : public lc_assist_user_ops {

        /* Refer to parent construct for details */
};

/** This construct describes interface of adapter that must be implemented to
    interwork with an entity that acts as a GNSS assistance provider to UE to 
    facilitate estimation of location information in a relatively short time. 
    Such entity typically provides assistance data to GNSS device on demand.  

    This interface also provisions a set of callback functions to be used by 
    the adapter implementation. The implementation MUST invoke the callbacks
    from an independent thread. It is recommended that implementation SHOULD 
    make use of the support extended by "A-GNSS Connect" to create exclusive 
    and independent thread.

    Specifically, the construct inherits operations from its parent. 
*/

struct ti_lc_assist_adap_if : public lc_assist_pvdr_ops {

        /** Initialize adapter implementation to interwork with external entity.
            It is the first function to be invoked on the interface and must be 
            supported. This operation also provisions a set of callbacks, whose
            reference must be saved by the adapter implementation for later use.
        */
        int (*init)(const struct ti_lc_assist_adap_cb *adap_cb);
        int (*exit)(void);

        /** Specify a start function for creation of thread by A-GNSS Connect at
            the time of initialization of adapter implementation. This specified
            function acts as an entry point to the implementation of the adapter
            CB thread. The callbacks MUST be invoked from this thread. 
        */
        void* (*cb_thread_func)(void*);
        void                      *cb_thread_arg1; /**< Arg of entry point fn */

        /* Refer to parent construct for details */
};

#endif
