#ifndef _AGNSS_LCS_ADAPTER_H_
#define _AGNSS_LCS_ADAPTER_H_

#include "gnss_lcs_common.h"

/** This file describes an interface that enables the binding of external entity
    that is not only an App that utilizes LCS but is also a provider or a source 
    of GNSS assistance data to "A-GNSS Connect". The specified interface must be
    implemented to adapt the needs of "A-GNSS Connect" to access GNSS assistance
    data and at the same time provision the support of LCS from GNSS UE. Example 
    of such entities include SUPL and Control Plane components. 

    The interface, henceforth called "A-GNSS LCS Adapter", is expected to bridge 
    the nuances of the design of "A-GNSS Connect" with those of assist capabable
    application.

    There are two parts to definition of "A-GNSS LCS Adapter": 
    a) "A-GNSS LCS Adapter" interface
    b) "A-GNSS LCS Adapter" callbacks
*/

/** This construct describes a select set of concerns of "A-GNSS Connect" module
    that can be invoked by adapter to invoke LOC services and / or report status
    of external entity. This callback interface is specified by "A-GNSS Connect"
    as part of initialization of adapter interace and MUST NOT be invoked in the 
    context of "A-GNSS Connect". Specifically, the interface MUST be called from
    a thread that is exclusive to the instance of the adapter being implemented.
*/
struct ti_agnss_lcs_adap_cb : public agnss_lcs_pvdr_ops {
  
        /* Refer to parent construct for details */
};

/** This construct describes interface of adapter that must be implemented to
    interwork with an entity that invokes the LOC services (LCS) supported by 
    TI GNSS UE and if capable, is a provider of GNSS assistance data to UE to 
    facilitate estimation of location information in a relatively short time. 
    Such an entity, if capable, typically, provides the assistance data to UE  
    prior to invoking of LOC services (LCS).  

    This interface also provisions a set of callback functions to be used by
    the adapter implementation. The implementation MUST invoke the callbacks
    from an independent thread. It is recommended that implementation SHOULD 
    make use of the support extended by "A-GNSS Connect" to create exclusive 
    and independent thread.
*/
struct ti_agnss_lcs_adap_if : public agnss_lcs_user_ops {

        /** Initialize adapter implementation to interwork with external entity.
            It is the first function to be invoked on the interface and must be 
            supported. This operation also provisions a set of callbacks, whose
            reference must be saved by the adapter implementation for later use.
        */
        int (*init)(const struct ti_agnss_lcs_adap_cb *adap_cb);
        int (*exit)();

        /** Specify a start function for creation of thread by A-GNSS Connect at
            the time of initialization of adapter implementation. This specified
            function acts as an entry point to the implementation of the adapter
            CB thread. The callbacks MUST be invoked from this thread. 
        */
        void *(*cb_thread_func)(void*);
        void                      *cb_thread_arg1; /**< Arg of entry point fn */

        /* Refer to parent construct for details */
};

#endif
