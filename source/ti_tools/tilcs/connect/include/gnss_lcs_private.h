/*
 * File: gnss_lcs_private.h
 *
 * Common & internal constructs that are required for working of TI A-GNSS
 * framework.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
 */
#ifndef _GNSS_LCS_PRIVATE_H_
#define _GNSS_LCS_PRIVATE_H_

#include "dev_proxy.h"   // to be removed after re-aligning data structures in gnss.h
#include "gnss.h"

/** 
    This file lists constructs that are internal to A-GNSS Connect and are to 
    be utilized with in A-GNSS Connect. 
*/

/** 
    Get remaining valid time in seconds for EPH in UE. Validity of EPH is 
    ascertained only if TIME and POS information in UE are valid. In case,
    TIME or POS are not valid then, EPH in UE is also considered invalid. 
    
    @return -1 on error or no aiding availability else Time-to-live (TTL)
    in seconds.
*/

typedef int (*pv_gnfn_get_ue_aid_ttl)(void *hnd);

typedef int (*pv_gnfn_get_ue_req4assist)(void *hnd, 
					 struct gps_assist_req&   gps_assist,
                                         struct ganss_assist_req& ganss_assist);

typedef void (*pv_gnfn_update_ue_time)(void *hnd);

typedef int (*pv_gnfn_ue_device_err)(void *hnd, enum dev_err_id err_id);/* add for GNSS recovery ++*/
typedef bool (*pv_gnfn_ue_is_msr_filter_gps_eph_reqd)(void *hnd);
/* add for GNSS recovery ++*/
typedef int  (*pv_gnfn_ue_err_recovery)(void *hnd, enum dev_err_id err_id,unsigned int interval_secs);
// error trigger test code, to be removed
typedef int  (*pv_gnfn_trigger_test_err) (void *hnd, unsigned char val);
/* add for GNSS recovery --*/

typedef 
int  (*pv_gnfn_ue_msr_filter_gps_eph)(void *hnd, 
                                      const struct gps_msr_result& gps_result,
                                      const struct gps_msr_info   *gps_msr);

typedef int (*pv_gnfn_ue_put2sleep)(void *hnd);

typedef int (*pv_gnfn_ue_wake2idle)(void *hnd);

typedef int (*pv_gnfn_assist2ue_done)(void *hnd);

#endif
