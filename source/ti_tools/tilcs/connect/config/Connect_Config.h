#ifndef __CONNECT_CONFIG_H__
#define __CONNECT_CONFIG_H__

#include <stdio.h>



class Connect_Config {

 private:

        static Connect_Config *conf_obj;

 private:
        
        Connect_Config() {} // Default Constructor

        static void exit_if_obj_not_set(void);

 public:

        static int  init_connect_config(void);

        static bool is_control_plane_en(void);
        static bool is_supl_2x_plane_en(void);
        static bool is_predicted_eph_en(void);
        static  int get_idle2sleep_secs(void);
        static  int get_up_preference(void);
        static  int get_ee_preference(void);
        static bool is_multi_source_en(void);
};

#endif
