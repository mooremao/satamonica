#include <iostream>
#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include "debug.h"
#include "Ftrace.h"

#include "Connect_Config.h"


typedef int  (*fn_set_value)(struct config_param *param, int value);
typedef int  (*fn_get_value)(struct config_param *param);

struct config_param {

        char          *field;
        int            value;

        fn_set_value   set_value;
        fn_get_value   get_value;
};

static int set_param(struct config_param *param, int value)
{
        param->value = value;
		DBG_L1("set_param: %s ==> %d\n", param->field, param->value);
        return 0;
}

static int  get_param(struct config_param *param)
{
        return param->value;
}

static struct config_param cfg_prams[] = {

        {"CP_enable", 0, set_param, get_param},
        {"EE_enable", 0, set_param, get_param},
        {"UP_enable", 0, set_param, get_param},
        {"idle_secs", -1, set_param, get_param},
        {"en_multi_src4app", 0, set_param, get_param},
        {"EE_prefer", -1, set_param, get_param},
        {"UP_prefer", -1, set_param, get_param},
        {NULL,        0,      NULL, NULL}
};

static int get_param_name_tok(char *str, char **name)
{
        char c;
        
        *name = NULL;
        
        while(c = *str++) {
                
                /* check for leading spaces in the string */
                if(((c == ' ') || (c == '\t')) && !(*name))
                        continue;
                
                /* delimiters */
                if((c == '#') || (c == ' ') || (c == '=') || 
                   (c == '\n') || (c == '\t'))
                        break;

                /* begining of name of parameter */
                if(*name == NULL)
                        *name = str - 1;
        }

        return (*name) ? (--str - *name) : 0;
}

static int get_param_value_tok(char *str, char **value)
{
        char c;
        int  eq_flag = 0;

        *value = NULL;

        while(c = *str++) {

                /* check for leading spaces in string */
                if(((c == ' ') || (c == '\t') ) && !(*value))
                        continue;

                /* was '=' already encountered? */
                if((c == '=') && !(eq_flag)){
                        eq_flag = 1;
                        continue;
                }
                
                /* delimiters */
                if((c == '#') || (c == ' ') || (c == '=') || 
                   (c == '\n') || (c == '\t'))
                        break;


                /* begining of value of parameter */
                if(*value == NULL)
                        *value = str - 1;
        }
        
        return (*value) ? (--str - *value) : 0;
}

static int parse_line2setup_param(char *line)
{
        unsigned int name_len  = 0;
        unsigned int value_len = 0;

        char *name, *value; 
        struct config_param *param = cfg_prams;

        name_len = get_param_name_tok(line, &name);
        if(!(name_len > 0))
                return name_len;
        
        value_len = get_param_value_tok(name + name_len, &value);
        if(!(value_len > 0))
                return value_len;
        
       	for(param = cfg_prams; NULL != param->field; param++) { 

                if(0 != strncmp(param->field, name, name_len))
                        continue;

                return (param->set_value(param, atoi(value)));
        }
        
        DBG_L1("Warning: unknown configuration parameter\n");
	return 0;
}

Connect_Config* Connect_Config :: conf_obj = NULL;

void Connect_Config :: exit_if_obj_not_set(void)
{
        if(NULL == conf_obj) { 
                ERR_NM("Connect Config Un-initialized: exiting process ...");
                //exit(-1); 
                exit(EXIT_FAILURE);
        }

        return;
}

int Connect_Config :: init_connect_config(void)
{
        if(NULL != conf_obj)
                return 0;

        conf_obj = new Connect_Config;
        if(NULL == conf_obj) {
                ERR_NM("Failed to create Cfg Config Object, exiting ...");
                //exit(-1);
                exit(EXIT_FAILURE);
        }

#ifdef ANDROID
        FILE *fp = fopen("/system/etc/gnss/config/Connect_Config.txt", "r");
#else
        FILE *fp = fopen("/etc/Connect_Config.txt", "r");
#endif

        if(NULL == fp) {
		DBG_L1("config file open failed: %d\n", fp);
                return -1;
        }

        static char line[300] = {0};
        while(NULL  != fgets(line, 256, fp)) {
              	parse_line2setup_param(line);
        }

        fclose(fp);

        return 0;
}

static struct config_param *find_param(char *name2find)
{
        struct config_param *param = cfg_prams;
        
        while(NULL != param->field) {
                
                if(0 == strcmp(name2find, param->field))
                        return param;

                param++;
        }

        return NULL;
}

static bool find_bool_param(char *name2find)
{
        struct config_param *param = find_param(name2find);
        if(NULL == param)
                return false;

        return param->get_value(param) ? true : false;
}

static int  find_int_param(char *name2find)
{
        struct config_param *param = find_param(name2find);
        if(NULL == param)
                return -1;

        return param->get_value(param);
}

bool Connect_Config :: is_control_plane_en()
{
        exit_if_obj_not_set();

        return find_bool_param("CP_enable");
}

bool Connect_Config :: is_supl_2x_plane_en()
{
        exit_if_obj_not_set();

        return find_bool_param("UP_enable");
}

bool Connect_Config :: is_predicted_eph_en()
{
        exit_if_obj_not_set();

        return find_bool_param("EE_enable");
}

int Connect_Config :: get_idle2sleep_secs()
{
        exit_if_obj_not_set();

        return find_int_param("idle_secs");
}

int Connect_Config :: get_up_preference()
{
        exit_if_obj_not_set();

        return find_int_param("UP_prefer");
}

int Connect_Config :: get_ee_preference()
{
        exit_if_obj_not_set();

        return find_int_param("EE_prefer");
}

bool Connect_Config :: is_multi_source_en()
{
        exit_if_obj_not_set();

	return find_bool_param("en_multi_src4app");
}




