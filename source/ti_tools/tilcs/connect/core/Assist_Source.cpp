
#include "Assist_Source.h"
#include "Agnss_Connect.h"
#include "Gnss_Utils.h"
#include "Ftrace.h"

// Following control flag constant desribe the run time state of source  objects 
static const unsigned short CTL_INDP_AGNSS = 0x0001; // Can assist any  requeter
static const unsigned short CTL_PEND_STATE = 0x0002; // Has been suspended state
static const unsigned short CTL_ACTIVE_REQ = 0x0004; // Assist request is active

/* C style  macros */

#if  0
#define IS_AWAITED() ((true == ueb_wait_flag) || (0 != uea_gnss_wmap))
#define WAS_SOUGHT() ((ctl_flags & CTL_ACTIVE_REQ) || IS_AWAITED())
#endif

#define WAS_SOUGHT() (0 != (ctl_flags & CTL_ACTIVE_REQ))
#define ABORT_COND() (WAS_SOUGHT() && (0 != usr_id))

#define HAS_PENDED() (0 != (ctl_flags & CTL_PEND_STATE))
#define NOT_PENDED() (!HAS_PENDED())
#define CAN_PENDED() ((cap_flags & SRC_CAN_PENDED) && NOT_PENDED())
#define CAN_RESUME() (HAS_PENDED())

#define INDP_AGNSS() (0 != (ctl_flags & CTL_INDP_AGNSS))

#define MAX_REQ_NUM  2 /* Do not change at this time from 2 */


// Constructor
Assist_Source :: Assist_Source(const char *name, unsigned int src_flags)
        : Base_Entity(name), cap_flags(src_flags), ctl_flags(0), 
          gps_assist_na(0),  ganss_comm_na(0), uea_gnss_wmap(0),
          ueb_wait_flag(false), prefer_id(0xffffffff)
{
        ctl_flags |= CTL_INDP_AGNSS;

        requester_vect.resize(MAX_REQ_NUM); /* initialize vector */
}

bool Assist_Source :: do_can_solicit_assist(void)
{
        return (NOT_PENDED() && INDP_AGNSS());
}

void Assist_Source :: do_clr_assist_req(const unsigned int usr_id)
{
        CPP_FTRACE2();

        unsigned int count = 0;

        for(unsigned int idx = 0; idx < requester_vect.size(); idx++) {

                struct assist_requester& requester = requester_vect[idx];
                if(usr_id == requester.usr_id) {
                        requester.ueb_wait_flag = false;
                        requester.uea_gnss_wmap = 0;
                        requester.usr_id        = 0;
                }

                if(0 == requester.usr_id) count++;
        }

        if(MAX_REQ_NUM == count) ctl_flags &= ~CTL_ACTIVE_REQ;
       
        return;
}

char Assist_Source :: _get_ganss_na_idx(unsigned char ganss_id)
{
        CPP_FTRACE2();

        for(char idx = 0; idx < MAX_GANSS; idx++) {
                if(ganss_id == generic_na[idx].ganss_id)
                        return idx;
        }

        return -1;
}

static unsigned char get_ganss_id(enum gnss_pv_id en_ganss)
{
        unsigned char ganss_id = GANSS_ID_NONE;

        switch(en_ganss) {

        case e_gnss_glo:    ganss_id = GANSS_ID_GLO; break;

        case e_gnss_gps:
        default:                                     break;
        }

        return ganss_id;
}

char Assist_Source :: _get_ganss_na_idx(enum gnss_pv_id en_ganss,
                                        bool            mk_alloc)
{
        CPP_FTRACE2();

        char ganss_id = get_ganss_id(en_ganss);

        struct ganss_generic_na *ganss = generic_na;
        char idx = 0;

        for(idx = 0; idx < MAX_GANSS; idx++, ganss++) {

                if(ganss_id == ganss->ganss_id)      break;

                if(GANSS_ID_NONE == ganss->ganss_id) break;
        }

        if((GANSS_ID_NONE == ganss->ganss_id) && mk_alloc) {
                ganss->ganss_id = ganss_id;
        }

        return (GANSS_ID_NONE == ganss->ganss_id)? -1 : idx;
}

int  Assist_Source :: ue_service_ctl(enum ue_service_ind ind,
                                     unsigned int     usr_id)
{
        CPP_FTRACE2();

        switch(ind) {

        case e_ue_service_cancel:
                do_clr_assist_req(0);
                ctl_flags      |=  CTL_PEND_STATE;
                break;

        case e_ue_service_resume:
                ctl_flags      &= ~CTL_PEND_STATE;
                break; 

        case e_assist_need_ended:
        case e_assist_need_abort:
                do_clr_assist_req(usr_id);
                break;

        default:
                break;
        }
	return 0;
}

int 
Assist_Source :: ue_loc_results(const struct gnss_pos_result&  pos_result,
                                const struct gps_msr_result&   gps_result,
                                const struct gps_msr_info     *gps_msr,
                                const struct ganss_msr_result& ganss_result,
                                const struct ganss_msr_info   *ganss_msr)
{
        CPP_FTRACE2();

        /* Do nothing at this time */

        return 0;
}

int Assist_Source :: out_of_service() 
{    
	CPP_FTRACE2();			
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->rem_assist_source(this);
}

int Assist_Source :: set_in_service() 
{ 
	CPP_FTRACE2();
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->add_assist_source(this);
}

int Assist_Source :: ue_add_assist(const struct nw_assist_id& id,
                                   const void *assist_array, int num)
{
	CPP_FTRACE2();
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->ue_add_assist(this, id, assist_array, num);
}

int Assist_Source :: ue_del_aiding(enum loc_assist assist, 
                                   unsigned int sv_id_map, 
                                   unsigned int mem_flags)
{
	CPP_FTRACE2();
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->ue_del_aiding(this, assist, sv_id_map, mem_flags);
}

int Assist_Source :: ue_get_aiding(enum loc_assist assist, 
                                   void *assist_array,
                                   int num)
{
	CPP_FTRACE2();
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->ue_get_aiding(this, assist, assist_array, num);
}

void Assist_Source :: set_solicit_assist(bool flag = true)
{
        CPP_FTRACE2();

        if(true == flag) 
                ctl_flags |=  CTL_INDP_AGNSS;
        else
                ctl_flags &= ~CTL_INDP_AGNSS;

        return;
}

bool Assist_Source :: 
can_solicit_assist(const struct gps_assist_req& gps_assist,
                   const struct ganss_assist_req& ganss_assist)
{
        CPP_FTRACE2();

        if(false == do_can_solicit_assist()) {
                DBG_L3("Can not solicit assist from %s", get_name());
                return false;
        }

        if(gps_assist_na & gps_assist.assist_req_map) {
                DBG_L3("Can not REQ GPS assist for 0x%x (bits)", gps_assist_na);
                return false;
        }

        if(ganss_comm_na & ganss_assist.common_req_map) {
                DBG_L3("Can not REQ GANSS Common for 0x%x(bits)", ganss_comm_na);
                return false;
        }

        for(unsigned char idx = 0; idx < ganss_assist.n_of_ganss_req; idx++) {

                const struct ganss_req_info *req = NULL;
                req = ganss_assist.ganss_req_data + idx;

                struct ganss_generic_na *ganss = get_ganss_na(req->id_of_ganss);
                if(ganss && (ganss->gen_bits & req->req_bitmap)) {
                        DBG_L3("Can't REQ GANSS Generic for 0x%x(bits)",
                               req->req_bitmap);
                        return false;
                }
        }

        return true;
}

bool Assist_Source :: can_solicit_assist(void)
{
        CPP_FTRACE2();

        return do_can_solicit_assist();
}

int Assist_Source :: assist2ue_done(unsigned int usr_id)
{
	CPP_FTRACE2();

        int idx = get_requester_index(usr_id, false);
        if(-1 == idx) {
                DBG_L1("%s provided preparatory assist; returning", get_name());
                return 0;
        }

        bool           ueb_flag = false;
        unsigned short uea_wmap = 0;

        struct assist_requester& requester = requester_vect[idx];
        if(usr_id == requester.usr_id) {   /* Request for assist made in past */

                /* Race conditions: clear flags prior to calling Connect API  */
                ueb_flag = requester.ueb_wait_flag; requester.ueb_wait_flag = 0;
                uea_wmap = requester.uea_gnss_wmap; requester.uea_gnss_wmap = 0; 
        }

        Agnss_Connect *connect = Agnss_Connect :: get_instance();

        return connect->assist2ue_done(this, ueb_flag, uea_wmap, usr_id);
}

void Assist_Source :: set_unsupported_assist(unsigned int gps_bitmap,
                                             unsigned int ganss_comm)
{
        CPP_FTRACE2();

        gps_assist_na = gps_bitmap;
        ganss_comm_na = ganss_comm;      
}

void Assist_Source :: add_unsupported_assist(unsigned int gps_bitmap,
                                             unsigned int ganss_comm)
{
        CPP_FTRACE2();

        gps_assist_na |= gps_bitmap;
        ganss_comm_na |= ganss_comm;
}

void Assist_Source :: set_unsupported_assist(enum gnss_pv_id ganss_id,
                                             unsigned int    gen_bits)
{
        CPP_FTRACE2();

        struct ganss_generic_na *ganss = get_ganss_na(ganss_id, true);
        if(NULL != ganss) {
                ganss->gen_bits  = gen_bits;
        }

        return;
}

void Assist_Source :: add_unsupported_assist(enum gnss_pv_id ganss_id,
                                             unsigned int    gen_bits)
{
        CPP_FTRACE2();

        struct ganss_generic_na *ganss = get_ganss_na(ganss_id, true);
        if(NULL != ganss) {
                ganss->gen_bits |= gen_bits;
        }

        return;
}

int Assist_Source :: do_assist_req_control(enum ue_service_ind ind, 
                                           unsigned int     usr_id)
{
        CPP_FTRACE2();

        /*
          If usr_id is zero then, clear all requests for assists
          If usr_id is specific then, clear only related request
        */

        bool test = false;

        for(unsigned int idx = 0; idx < requester_vect.size(); idx++) {
                unsigned int rq_usr = requester_vect[idx].usr_id;
                
                if((0 == rq_usr)) continue;
                
                if((0 == usr_id) || (usr_id == rq_usr)) {
                        this->ue_service_ctl(ind, rq_usr);
                        DBG_L2("Test flag is not set");
                        do_clr_assist_req(rq_usr);
                        test = true;
                }
        }

        if(false == test) 
                ERR_NM("Fatal: assist request and AGNSS Connect out of sync");

        return 0;
}

int 
Assist_Source :: ue_assist_control(enum ue_service_ind ind, unsigned int usr_id)
{
        CPP_FTRACE2();

        DBG_L2("IND:%u, caps: 0x%x, state: 0x%x, UEB:%u, UEA: 0x%x for %s", ind,
               cap_flags, ctl_flags, ueb_wait_flag, uea_gnss_wmap, get_name());

        switch(ind) {
                
        case e_ue_service_cancel:
                if(true == CAN_PENDED())  this->ue_service_ctl(ind, usr_id);
                break;

        case e_ue_service_resume:
                if(true == CAN_RESUME())  this->ue_service_ctl(ind, usr_id);
                break;

        case e_assist_need_ended:
                if(true == WAS_SOUGHT()) this->do_assist_req_control(ind, usr_id);
                break;

        case e_assist_need_abort:
                if(true == ABORT_COND()) this->do_assist_req_control(ind, usr_id);
                break;

        default:
                break;

        }

        return 0;
}

void setup_ganss_assist_need(const struct ganss_assist_req&  ganss_assist,
                             unsigned short                  uea_flags,
                             bool&                           ueb_wait_flag,
                             unsigned short&                 uea_gnss_wmap)
{
        /* Set up assist needs for various GANSS */
        for(unsigned char idx = 0; idx < ganss_assist.n_of_ganss_req; idx++) {
                const struct ganss_req_info *ganss_req = NULL; 
                unsigned short pmthd_map = 0;

                ganss_req = ganss_assist.ganss_req_data + idx;
                pmthd_map = ganss_id2pmthd_bit(ganss_req->id_of_ganss);
                
                if(0 != (uea_flags & pmthd_map)) {
                        uea_gnss_wmap |= pmthd_map;
                        continue;
                }
                
                if((ganss_req->req_bitmap       & A_GANSS_NAV_REQ) || 
                   (ganss_assist.common_req_map & A_GANSS_POS_REQ) ||
                   (ganss_assist.common_req_map & A_GANSS_TIM_REQ))
                        ueb_wait_flag  = true;
        }

        return;
}

#define A_GPS_UEB_REQ (A_GPS_TIM_REQ | A_GPS_NAV_REQ | A_GPS_POS_REQ)  /* gnss.h */
#define A_GPS_UEA_REQ (A_GPS_TIM_REQ | A_GPS_ACQ_REQ)                  /* gnss.h */

int Assist_Source :: get_requester_index(unsigned int usr_id, bool for_alloc)
{
        int idx = 0;

        for(idx = 0; idx < requester_vect.size(); idx++) {

                struct assist_requester& requester = requester_vect[idx];

                if(usr_id == requester.usr_id) 
                        break; /* Found */

                if((0 == requester.usr_id) && (true  == for_alloc)) {
                        requester.usr_id = usr_id; /* Free */
                        break; 
                }
        }
        
        if(idx == requester_vect.size()) {
                if(true == for_alloc)
                        ERR_NM("Max # of %u reached, req REJECTED", MAX_REQ_NUM);
                
                return -1;
        }

        return idx;
}

int 
Assist_Source :: ue_need_assist_req(const struct gps_assist_req&   gps_assist,
				    const struct ganss_assist_req& ganss_assist,
				    unsigned short                 uea_flags,
				    unsigned int                   usr_id)
{
	CPP_FTRACE2();

	int ret_val = -1;

	int idx = get_requester_index(usr_id, true);
	if(-1 == idx)  return ret_val;       /* Index for requester not found */

	struct assist_requester& requester = requester_vect[idx];/* Reference */

	ret_val = this->ue_need_assist(gps_assist, ganss_assist, usr_id);
	if(0 != ret_val) {
		ERR_NM("Failed to seek assistance from source %s", get_name());
		return ret_val;
	}

#define PRINT(prefix, o) "%s: %s, need status UEB:%u, UEA:0x%0x, usr_id:0x%x", \
	get_name(), prefix, o.ueb_wait_flag, o.uea_gnss_wmap, o.usr_id

	DBG_L3(PRINT("prior proc", requester));

	/* Following elaborate implementation instead of a simple logic ensures
	   double checking of various flags - good way of detecting SW bugs  */ 

	/* Set up assist needs for GPS */
	if((0 == uea_flags) && (gps_assist.assist_req_map & A_GPS_UEB_REQ))
		requester.ueb_wait_flag  = true;

	if((0 != uea_flags) && (gps_assist.assist_req_map & A_GPS_UEA_REQ))
		requester.uea_gnss_wmap |= GNSS_GPS_PMTHD_BIT;

	DBG_L3(PRINT("GPS   proc", requester));

	/* Set up assist needs for various GANSS */
	setup_ganss_assist_need(ganss_assist, uea_flags, 
				requester.ueb_wait_flag,
				requester.uea_gnss_wmap);

	DBG_L3(PRINT("GANSS proc", requester));

	if((true == requester.ueb_wait_flag) || requester.uea_gnss_wmap)
		ctl_flags |= CTL_ACTIVE_REQ;

	return ret_val;
}


int Assist_Source :: nw_loc_results(const struct gnss_pos_result& pos_result,
                                    unsigned int usr_id)
{
        CPP_FTRACE2();

        int idx = get_requester_index(usr_id, false);
        if(-1 == idx) {
                ERR_NM("Fatal: Assist Source & AGNSS Connect Out of Sync");
                return -1;
        }

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->nw_loc_results(this, pos_result, usr_id);
}

void Assist_Source :: clr_assist_req(unsigned int usr_id)
{
        do_clr_assist_req(usr_id);
}

int Assist_Source :: loc2assist_src(const struct gnss_pos_result&  pos_result, 
                                    const struct gps_msr_result&   gps_result,
                                    const struct gps_msr_info     *gps_msr,
                                    const struct ganss_msr_result& ganss_result,
                                    const struct ganss_msr_info   *ganss_msr)
{
        CPP_FTRACE2();

        if(false == WAS_SOUGHT()) {
                DBG_L3("No assist sought from %s; ignoring", get_name());
                return 0;
        }

        return this->ue_loc_results(pos_result, gps_result, gps_msr, 
                                    ganss_result, ganss_msr);
}
