#include "Agnss_Services.h"
#include "Gnss_Utils.h"
#include "Ftrace.h"
#include "debug.h"
#ifndef ANDROID
#include <cstring>
#endif

#define ADAP_IF_FN(fn, ...) if(fn && fn(__VA_ARGS__))                   \
                ERR_NM("%s (adap) failed or couldn't process data", get_name());

static bool detect_multiple_others(unsigned int new_usr, unsigned int old_usr,
                                   unsigned int client)
{
        /* Ensure that if assist request is for other (not self client) then, 
           multiple other clients are not making requests for assistance */
        if(new_usr != client) {
                if((0 != old_usr) && (old_usr != new_usr)) {
                        ERR_NM("Out of sync; ue-usr Id old : 0x%x, now : 0x%x",
                               old_usr, new_usr);
                        return true;
                }
        }

        return false;
}

int 
Agnss_Services :: ue_service_ctl(enum ue_service_ind ind, unsigned int usr_id)
{
	CPP_FTRACE2();

	bool       is_other = false;
	unsigned int client = Remote_Client :: get_handle();

	Assist_Source :: ue_service_ctl(ind, usr_id);

	if(usr_id != client) {
		if(true == detect_multiple_others(usr_id, ue_usr, client))  {
			ERR_NM("Out of sync: AGNSS Services & AGNSS Connect");
			return 0;
		}

		is_other = true;
	}

	switch(ind) {

		case e_assist_need_ended: 
		case e_assist_need_abort: if(true == is_other) ue_usr = 0; break;
		default:                                              break;

	}

	if(false == is_other) {
		DBG_L1("%s: Not sending to adapter an IND %u for Client ID 0x%x",
		       get_name(), ind, usr_id);

		return 0;
	}

	DBG_L1("%s: sending a control from Client ID  0x%x (%sself)", 
	       get_name(), usr_id, (true == is_other)? "ue-" : "my");

	struct assist_reference assist_usr; 
	assist_usr.is_client_others  = is_other;

	ADAP_IF_FN(adap_if.ue_service_ctl, adap_if.hnd, ind, assist_usr);

        return 0;
}

int Agnss_Services :: ue_need_assist(const struct gps_assist_req& gps_assist,
                                     const struct ganss_assist_req& ganss_assist,
                                     unsigned int                   usr_id)
{
	CPP_FTRACE2();

	bool       is_other = false;
	unsigned int client = Remote_Client :: get_handle();

	if(usr_id != client) {
		if(true == detect_multiple_others(usr_id, ue_usr, client))  {
			ERR_NM("Out of sync: AGNSS Services & AGNSS Connect");
			return 0;
		}

		ue_usr   = usr_id;        /* Retain it for future references */
		is_other = true;
	}

	DBG_L1("%s: sending a request from Client ID  0x%x (%sself)", 
	       get_name(), usr_id, (true == is_other)? "ue-" : "my");

	struct assist_reference assist_usr; 
	assist_usr.is_client_others  = is_other;

	ADAP_IF_FN(adap_if.ue_need_assist, adap_if.hnd, 
		   gps_assist, ganss_assist, assist_usr);

        return 0;
}

// Constructor
Agnss_Services :: Agnss_Services(const char *name, const unsigned int a_flags,
				 gnss_adapter_load_fn load_fn)
: Base_Entity(name), Assist_Source(name, a_flags), Remote_Client(name),
	adap_ld(load_fn)
{
	CPP_FTRACE2();
	adap_cb.hnd                = (void*) this;
	adap_cb.get_ue_loc_caps    = adap_get_ue_loc_caps<Agnss_Services>;
	adap_cb.loc_start          = adap_loc_start<Agnss_Services>;
	adap_cb.loc_stop           = adap_loc_stop<Agnss_Services>;
	adap_cb.set_loc_instruct   = adap_set_loc_instruct<Agnss_Services>;
	adap_cb.set_rpt_criteria   = adap_set_rpt_criteria<Agnss_Services>;

	adap_cb.ue_add_assist      = adap_ue_add_assist<Agnss_Services>;
	adap_cb.ue_del_aiding      = adap_ue_del_aiding<Agnss_Services>;
	adap_cb.ue_get_aiding      = adap_ue_get_aiding<Agnss_Services>;
	adap_cb.assist2ue_done     = adap_ue_assist2ue_done<Agnss_Services>;
	adap_cb.oper_ue_dev_param  = adap_ue_dev_oper_param<Agnss_Services>;
        adap_cb.nw_loc_results     = adap_nw_loc_results<Agnss_Services>;
	adap_cb.ue_cleanup	= adap_ue_cleanup<Agnss_Services>;
	
	adap_cb.dev_init	= adap_dev_init<Agnss_Services>;
	adap_cb.dev_plt   = adap_dev_plt<Agnss_Services>;


}

extern int gnss_run_node(const Agnss_Services& node,
			 const struct ti_agnss_lcs_adap_if *adap_if,
			 const struct ti_agnss_lcs_adap_cb *adap_cb,
			 gnss_adapter_load_fn adap_ld, const char *thr_name);

int Agnss_Services :: run_entity(const char *thr_name)
{
	CPP_FTRACE2();
	return gnss_run_node(this, &adap_if, &adap_cb, adap_ld, thr_name);
}

int Agnss_Services :: await_exit()
{
	CPP_FTRACE2();
	return Base_Entity :: wait_wthread();
}

int Agnss_Services :: hlt_entity()
{
	CPP_FTRACE2();
	return Base_Entity :: stop_wthread();
}

/* Get LOC methods / capabilities of UE */
int Agnss_Services :: get_ue_loc_caps(unsigned int& gps_caps_bits,
				      struct ganss_ue_lcs_caps *ganss_caps)
{
	CPP_FTRACE2();
	return Remote_Client :: get_ue_loc_caps(gps_caps_bits, ganss_caps);
}

int Agnss_Services :: loc_start()
{
	CPP_FTRACE2();
	return Remote_Client :: loc_start();
}

int Agnss_Services :: loc_stop()
{
	CPP_FTRACE2();
	return Remote_Client :: loc_stop();
}

 int Agnss_Services :: ue_cleanup()
{
	CPP_FTRACE2();
	return Remote_Client :: ue_cleanup();
}

int Agnss_Services :: dev_init()
{
	CPP_FTRACE2();
	return Remote_Client :: dev_init();
}
int Agnss_Services :: dev_plt(const struct plt_param& plt_test)
{
	CPP_FTRACE2();
	return Remote_Client :: dev_plt(plt_test);
}
 
/* Set methods */
int Agnss_Services :: set_loc_instruct(const struct loc_instruct& req)
{
	CPP_FTRACE2();
	return Remote_Client :: set_loc_instruct(req);
}

int Agnss_Services :: set_rpt_criteria(const struct rpt_criteria& rpt)
{
	CPP_FTRACE2();
	return Remote_Client :: set_rpt_criteria(rpt);
}

#define ADAP_IF_FN(fn, ...) if(fn && fn(__VA_ARGS__))                   \
                ERR_NM("%s (adap) failed or couldn't process data", get_name());

int Agnss_Services :: ue_loc_results(const struct gnss_pos_result&    pos_result, 
				     const struct gps_msr_result&   gps_result,
				     const struct gps_msr_info     *gps_msr,
				     const struct ganss_msr_result& ganss_result,
				     const struct ganss_msr_info   *ganss_msr)
{
	CPP_FTRACE2();
        
        /* Caution: members of constant data structures are being updated */

        /* MSR reports: unconditional checks prior to sending to requestors */
        static struct gps_msr_result gps_update; /* Correct way, small data */
        memcpy(&gps_update, &gps_result, sizeof(struct gps_msr_result));

        unsigned int contents = gps_update.contents;
        gps_update.contents   = 0;

        if((contents &  GPS_MSR_NOT_REPORTED) && (0 == gps_update.n_sat)) {
                 gps_update.contents       =  GPS_MSR_ERR_REPORTED;
                 gps_update.error.error_id =  not_enough_gps_sv;
                 gps_update.error.contents = 0;
                 DBG_L3("GPS MSR ERR to service invoker %s", get_name());
        }

        /* POS reports:   conditional checks prior to sending to requestors */
	unsigned int mtypes = Remote_Client::get_method_types();
        const struct gnss_pos_result *p = &pos_result;
        contents = pos_result.contents;

#define BAD_UPDATE_POS_CONST(x, v) const_cast<struct gnss_pos_result *> x = v

        if(!(mtypes & UE_LCS_CAP_AGNSS_MSR) && (contents & POS_NOT_REPORTED)) {
                BAD_UPDATE_POS_CONST((p)->contents,   POS_ERR_REPORTED);
                DBG_L3("POS ERR out to service invoker %s", get_name());
        }

        DBG_L1("Final: POS Res/Err = %u / %u, GPS Res/Err = %u / %u",   \
               pos_result.contents, pos_result.lc_error.error_id,
               gps_result.contents, gps_result.error.error_id);
        
        ADAP_IF_FN(adap_if.ue_loc_results, adap_if.hnd, pos_result, gps_update, \
                   gps_msr, ganss_result, ganss_msr);
        
        BAD_UPDATE_POS_CONST((p)->contents, contents); /* Restor to original */

	return 0;
}

int Agnss_Services :: ue_nmea_report(enum nmea_sn_id nmea, const char *data, 
				     int len)
{
	CPP_FTRACE2();

        ADAP_IF_FN(adap_if.ue_nmea_report, adap_if.hnd, nmea, data, len);

	return 0;
}

int Agnss_Services :: 
ue_aux_info4sv(const struct dev_gnss_sat_aux_desc&	gps_desc,
	       const struct dev_gnss_sat_aux_info	*gps_info,
	       const struct dev_gnss_sat_aux_desc&  glo_desc,
	       const struct dev_gnss_sat_aux_info	*glo_info)
{
	CPP_FTRACE2();
        ADAP_IF_FN(adap_if.ue_aux_info4sv, adap_if.hnd, gps_desc, gps_info, \
                   glo_desc, glo_info);

	return 0;
}

int Agnss_Services :: ue_decoded_aid(enum loc_assist assist, 
				     const void *assist_array, int num)
{
	CPP_FTRACE2();

        ADAP_IF_FN(adap_if.ue_decoded_aid, adap_if.hnd, assist, assist_array, \
                   num);

	return 0;
}

int Agnss_Services :: ue_cw_test_results(const enum cw_test_rep cwt, 
		const struct cw_test_report *cw_rep, int len)
{
	CPP_FTRACE2();

        ADAP_IF_FN(adap_if.cw_test_results, adap_if.hnd, cwt, cw_rep, len);
	return 0;
}

int Agnss_Services :: ue_recovery_ind(const enum dev_reset_ind rstind, 
		 int len)
{
	CPP_FTRACE2();

        ADAP_IF_FN(adap_if.ue_recovery_ind, adap_if.hnd, rstind, len);

	return 0;
}

int Agnss_Services :: out_of_service()
{
	CPP_FTRACE2();
	return Assist_Source :: out_of_service();
}

int Agnss_Services :: set_in_service()
{
	CPP_FTRACE2();
	return Assist_Source :: set_in_service();
}

int Agnss_Services :: ue_add_assist(const struct nw_assist_id& id,
				    const void *assist_array, int num)
{
	CPP_FTRACE2();
	return Assist_Source :: ue_add_assist(id, assist_array, num);
}

int Agnss_Services :: ue_del_aiding(enum loc_assist assist, 
				    unsigned int sv_id_map,
				    unsigned int mem_flags)
{
	CPP_FTRACE2();
	return Assist_Source :: ue_del_aiding(assist, sv_id_map, mem_flags);
}

int Agnss_Services :: ue_get_aiding(enum loc_assist assist, 
				    void *assist_array, int num)
{
	CPP_FTRACE2();
	return Assist_Source :: ue_get_aiding(assist, assist_array, num);
}

static unsigned int get_usr_id(bool is_others, unsigned int ue_usr, 
                               unsigned int client)
{
        CPP_FTRACE2();

        if((true == is_others) && (0 == ue_usr)) {
                ERR_NM("Fatal: ue-client did not seek assist in past, abort");
                return 0; /* Invalid ID */
        }

        return (true == is_others) ? ue_usr : client;
}

int 
Agnss_Services :: assist2ue_done(const struct assist_reference& assist_usr)
{
	CPP_FTRACE2();

        unsigned int usr_id = get_usr_id(assist_usr.is_client_others, ue_usr,
                                         Remote_Client  ::    get_handle());
        if(0 == usr_id) {
                ERR_NM("%s: No usr Id found for requester (%s)", get_name(),
                       true == assist_usr.is_client_others? "Others" : "self");
                return 0;
        }
	return Assist_Source :: assist2ue_done(usr_id);
}

int Agnss_Services :: oper_ue_dev_param(enum ue_dev_param_id   id, 
                                        void *param_desc, int num)
{
	CPP_FTRACE2();
	return Remote_Client :: oper_ue_dev_param(id, param_desc, num);
}

int 
Agnss_Services :: nw_loc_results(const struct gnss_pos_result&       pos_result,
                                 const struct assist_reference& assist_usr)
{
        CPP_FTRACE2();

        unsigned int usr_id = get_usr_id(assist_usr.is_client_others, ue_usr,
                                         Remote_Client  ::    get_handle());
        if(0 == usr_id) {
                ERR_NM("%s: No usr Id found for requester (%s)", get_name(),
                       true == assist_usr.is_client_others? "Others" : "self");
                return 0;
        }

        return Assist_Source :: nw_loc_results(pos_result, usr_id);
}

int Agnss_Services :: decl_dev_param(enum ue_dev_param_id   id, 
                                     void *param_desc, int num)
{
        CPP_FTRACE2();

        ADAP_IF_FN(adap_if.decl_dev_param, adap_if.hnd, id, param_desc, num);
        
        return 0;
}



