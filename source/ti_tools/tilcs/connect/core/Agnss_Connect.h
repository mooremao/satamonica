
#ifndef __AGNSS_CONNECT_H__
#define __AGNSS_CONNECT_H__

#include <list>
#include <pthread.h>

#include "Gnss_Receiver.h"
#include "Assist_Source.h"
#include "Remote_Client.h"
#include "gnss.h"
#include "debug.h"

using namespace std;

class Cev_Manager; /* Forward declaration */

class Agnss_Connect {

        friend class Cev_Manager; /* Cev_Manager is a friend of Agnss_Connect */

	private:

		pthread_mutex_t       connect_mutex;

		static Agnss_Connect *connect;

		list<Remote_Client*> usr_client_list;   // active list 
		list<Assist_Source*> assist_src_list;   // active list

                unsigned short  assist4uea_gnss_wmap; // UEA: awaited GNSS assist  
                bool            assist4ueb_wait_flag; // UEB: Assistance  awaited 
                unsigned int    assist4ueb_ref_count; // # awaited UEB assist RSP 

                bool            cfg_req_multiple_src;
				bool					gnss_meas_rep;
				unsigned int 	ue_recovery_count;

		/* Private constructor */
		Agnss_Connect();

		inline int mtx_lockin() {
			return pthread_mutex_lock(&connect_mutex);
		}

		inline int mtx_unlock() {
			return pthread_mutex_unlock(&connect_mutex);
		}

		template<typename F, typename... Args> 
			int invoke_atomic(F pfn, Args... args) {
				mtx_lockin();
				int ret = (this->*pfn)(args...);
				mtx_unlock();

				return ret;
			}

		static Gnss_Receiver  *receiver;


                /* To be invoked by Friends */
                int fr_eval2req_assist(void);

                int fr_ue_need_assist(Remote_Client                  *client,
                                      const struct gps_assist_req&    gps_assist,
                                      const struct ganss_assist_req&  ganss_assist,
                                      unsigned short                  uea_types);

                int fr_msr_filter_gps_eph(const struct gps_msr_result& gps_result,
                                             const struct gps_msr_info   *gps_msr);
                
				/* add for GNSS recovery ++*/
				int fr_ue_err_recovery(enum dev_err_id err_id);
				// error trigger test code, to be removed
				int fr_trigger_test_err(unsigned char val);
				/* add for GNSS recovery --*/				
                int fr_put_ue2sleep(void);

                int  _do_set_loc_interval();

                Assist_Source* _do_find_assist_src(const char *name);

                Remote_Client* _do_find_usr_client(const char *name);                

		void   _do_setup_wait4assist(Assist_Source *source, 
					     unsigned short uea_types);

		bool  _is_client_an_usr(Remote_Client *client);

                bool _is_an_usr_client(const char *name);

		bool  can_decl_intended_qop(Remote_Client *client,
						const struct gnss_qop& qop_req,
						Gnss_Receiver *receiver);

		int _do_ue_assist_control(enum ue_service_ind ind,
                                          Remote_Client   *client);

                int _do_ue_loc_results(Remote_Client                  *client,
				       const struct gnss_pos_result&   pos_result,
                                       const struct gps_msr_result&    gps_result, 
                                       const struct gps_msr_info      *gps_msr,
                                       const struct ganss_msr_result&  ganss_result,
                                       const struct ganss_msr_info    *ganss_msr);

                bool __do_mark_assist4ueb(struct gps_assist_req&   gps_assist,
                                        struct ganss_assist_req& ganss_assist);

                bool __do_mark_assist4uea(unsigned short& gnss_methods, 
                                        struct gps_assist_req& gps_assist,
                                        struct ganss_assist_req& ganss_assist);

                bool _do_mark_assist(bool uea_only, unsigned short&   gnss_methods,
                                     const struct gps_assist_req&     gps_need,
                                     const struct ganss_assist_req&   ganss_need,
                                     struct gps_assist_req&           gps_assist,
                                     struct ganss_assist_req&         ganss_assist);

                bool __are_all_usr_type(unsigned int type);

                bool  _are_all_usr_auto(void) {
                        return __are_all_usr_type(UE_LCS_CAP_GNSS_AUTO); 
                }

                bool  _are_all_usr_aueb(void) {
                        return __are_all_usr_type(UE_LCS_CAP_AGNSS_POS); 
                }

                bool  _are_all_usr_auea(void) {
                        return __are_all_usr_type(UE_LCS_CAP_AGNSS_MSR); 
                }

                int fr_dev_params2src(Assist_Source *source, 
                                      enum ue_dev_param_id id, void*, int);

		  int wakeup_ue2wait4loc_req(Gnss_Receiver *receiver);

                int fr_assist_control(enum ue_service_ind ind,
                                      Remote_Client   *client);
	private:

		int do_ue_add_assist(const Assist_Source *source, 
				     const struct nw_assist_id& id, 
				     const void *assist_array, int num);

		int do_ue_del_aiding(const Assist_Source *source, 
                                     enum loc_assist assist,
				     unsigned int sv_id_map, 
                                     unsigned int mem_flags);

		int do_ue_get_aiding(const Assist_Source *source, 
                                     enum loc_assist assist, 
				     void *assist_array, int num);

		int do_ue_assist2ue_done(const Assist_Source *source,
                                         bool                 ueb_flag,
					 unsigned short       ueb_wmap,
                                         unsigned int         usr_id);

                int do_loc2assist_src(const struct gnss_pos_result&   pos_result,
                                      const struct gps_msr_result&    gps_result, 
                                      const struct gps_msr_info      *gps_msr,
                                      const struct ganss_msr_result&  ganss_result,
                                      const struct ganss_msr_info    *ganss_msr);

		int do_ue_decoded_aid(enum loc_assist assist, 
                                      const void *assist_array,
				      int num);

		int do_ue_need_assist(Remote_Client *client,
                                      const struct gps_assist_req& gps_assist,
				      const struct ganss_assist_req& ganss_assist,
                                      unsigned short uea_types);

		int do_ue_loc_results(const struct gnss_pos_result&   pos_result,
				      const struct gps_msr_result&    gps_result, 
				      const struct gps_msr_info      *gps_msr,
				      const struct ganss_msr_result&  ganss_result,
				      const struct ganss_msr_info    *ganss_msr, 
                      bool                            ue_ff);

		int do_ue_nmea_report(enum nmea_sn_id nmea, const char *data,
                                      int len);

		int do_ue_aux_info4sv(const struct dev_gnss_sat_aux_desc&  gps_desc,
				      const struct dev_gnss_sat_aux_info  *gps_info,
				      const struct dev_gnss_sat_aux_desc&  glo_desc,
				      const struct dev_gnss_sat_aux_info  *glo_info);

                int do_decl_intended_qop(Remote_Client           *client, 
                                         const struct gnss_qop& qop_req);

		//int do_loc_start(Remote_Client *client, unsigned int interval);
		int _do_loc_start(bool due2loc_req);

		int do_loc_stop(const Remote_Client *client);


		int do_decl_desired_qop(const struct gnss_qop& qop_req);

		int do_get_ue_loc_caps(unsigned int* gps_caps_bits,
				       struct ganss_ue_lcs_caps *ganss_caps);

                int do_set_loc_interval(Remote_Client      *client,
                                        unsigned int interval_secs);

		int do_add_client(Remote_Client *client);
		int do_remove_client(void);

                int do_put_assist_need(Remote_Client *client);

                int do_eval2req_assist(Remote_Client *client);

		int do_oper_ue_dev_param(enum ue_dev_param_id	id, 
					 void *param_desc, int num);

                int do_dev_params2src(Assist_Source        *source,
                                      enum ue_dev_param_id      id,
                                      void                  *param,
                                      int                      num);

                int do_state_tcxo_info(void);
		int do_ue_dev_oper_param(enum ue_dev_param_id	id, 
					 void *param_desc, int num);

		int do_ue_cw_test_results(const enum cw_test_rep cwt, const struct cw_test_report *cw_rep, int len);
		int do_ue_recovery_ind(dev_reset_ind, int);
	public:

		static Agnss_Connect *create_instance();
		static Agnss_Connect *get_instance() { return connect; }
 		int agnss_dev_init(void);
		int agnss_cleanup(void);
		int agnss_dev_plt(Remote_Client *client, const plt_param&);
 		int add_gnss_receiver(Gnss_Receiver *receiver);
		int rem_gnss_receiver(const Gnss_Receiver *receiver);

		int add_assist_source(Assist_Source *source);
		int rem_assist_source(const Assist_Source *source);

		/* Make a request for LOC */
		int loc_request(Remote_Client *new_client,
				const struct gnss_qop& qop_req, 
				unsigned int interval);

		/* Stop a previous request */
		int end_loc_req(Remote_Client *old_client);

		int renew_ue_aiding(void); /* Update aiding for present time */
		int abandon_renewal(void); /* Retract aiding renewal request */  

		int ue_add_assist(const Assist_Source *source, 
				  const struct nw_assist_id& id,
				  const void *assist_array, int num);

		int ue_del_aiding(const Assist_Source *source, enum loc_assist assist, 
				  unsigned int sv_id_map, unsigned int mem_flags);

		int ue_get_aiding(const Assist_Source *source, enum loc_assist assist, 
				  void *assist_array, int num);

		int assist2ue_done(const Assist_Source *source,
                                      bool ueb_flag, unsigned short uea_wmap,
                                      unsigned int usr_id);

		int ue_decoded_aid(enum loc_assist assist, const void *assist_array,
				   int num);

		int ue_loc_results(const struct gnss_pos_result&   pos_result,
				   const struct gps_msr_result&    gps_result, 
				   const struct gps_msr_info      *gps_msr,
				   const struct ganss_msr_result&  ganss_result,
				   const struct ganss_msr_info    *ganss_msr);

		int ue_nmea_report(enum nmea_sn_id nmea, const char *data, int len); 

		int ue_aux_info4sv(const struct dev_gnss_sat_aux_desc&	gps_desc,
				   const struct dev_gnss_sat_aux_info	*gps_info,
				   const struct dev_gnss_sat_aux_desc&  glo_desc,
				   const struct dev_gnss_sat_aux_info	*glo_info);

		//int ue_need_assist(const struct gps_assist_req& gps_assist,
                //	   const struct ganss_assist_req& ganss_assist);

		int get_ue_loc_caps(unsigned int& gps_caps_bits,
				    struct ganss_ue_lcs_caps *ganss_caps);

		int set_loc_interval(Remote_Client      *client, 
                                     unsigned int interval_secs);

                int  nw_loc_results(Assist_Source *source, 
                                    const struct gnss_pos_result&  pos_result,
                                    unsigned int                   usr_id);

                int decl_intended_qop(Remote_Client          *client,
                                      const struct gnss_qop& req_qop);

                void decl_ue_has_time(void);
				int dispatch_ue_err(enum dev_err_id err_id);/* add for GNSS recovery ++*/
                int  oper_ue_dev_param(enum ue_dev_param_id   id,
                                       void *param_desc, int num);

	int _do_mk_assist_req(Assist_Source& source, const struct gps_assist_req& gps_assist,
                  const struct ganss_assist_req& ganss_assist,
                  unsigned short uea_types, unsigned int usr_id);

	void sort_assist_src(void);
        void use_multi_src(bool multi_src_config){ cfg_req_multiple_src = multi_src_config; }
	int ue_cw_test_results(const enum cw_test_rep cwt, const struct cw_test_report *cw_rep, int len);
		int ue_recovery_ind(const enum dev_reset_ind rstind, int len);
};

#endif
