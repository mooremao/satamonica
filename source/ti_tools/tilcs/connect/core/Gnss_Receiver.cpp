
#include "Gnss_Receiver.h"
#include "Gnss_Utils.h"
#include "Agnss_Connect.h"
#include "Ftrace.h"

int Gnss_Receiver :: n_refers = 0;
Gnss_Receiver* Gnss_Receiver :: receiver = NULL;

Gnss_Receiver :: Gnss_Receiver(const char *name, gnss_adapter_load_fn load_fn)
        : Base_Entity(name), adap_ld(load_fn), has_fix1(false), has_time(false),
          in_works(false), in_sleep(true), interval(1)
{
	CPP_FTRACE2();

        adap_cb.hnd                 = (void*) this;
        adap_cb.ue_loc_results      = adap_ue_loc_results<Gnss_Receiver>;
        adap_cb.ue_decoded_aid      = adap_ue_decoded_aid<Gnss_Receiver>;
        adap_cb.ue_nmea_report      = adap_ue_nmea_report<Gnss_Receiver>;
        adap_cb.ue_aux_info4sv 	    = adap_ue_aux_info4sv<Gnss_Receiver>;

        /* Scope of func is limited to this class: not using template as above */
        adap_cb.update_ue_time      = adap_update_ue_time<Gnss_Receiver>;
        adap_cb.ue_cw_test_results  = adap_ue_cw_test_results<Gnss_Receiver>;
		adap_cb.handle_ue_err		= adap_handle_ue_err<Gnss_Receiver>;/* add for GNSS recovery ++*/
		adap_cb.ue_recovery_ind		= adap_ue_recovery_ind<Gnss_Receiver>;
}

Gnss_Receiver* Gnss_Receiver :: create_instance(const char *name,
                                                gnss_adapter_load_fn load_fn)
{
        CPP_FTRACE2();
        
        if(!receiver)
                receiver = new Gnss_Receiver(name, load_fn);
        
        return receiver;
}

extern int gnss_run_node(const Gnss_Receiver& node,
                         const struct ti_gnss_ue_interface *adap_if,
                         const struct ti_gnss_ue_callbacks *adap_cb,
                         gnss_adapter_load_fn adap_ld, const char* thr_name);


int Gnss_Receiver :: run_entity(const char* thr_name)
{
        CPP_FTRACE2();
        
        if(!n_refers++ && receiver)
                return gnss_run_node(this, &adap_if, &adap_cb, adap_ld, thr_name);
        
        return -1;
}

 int Gnss_Receiver :: dev_init()
{
	CPP_FTRACE2();
	adap_if.dev_init(NULL);
	return 0;
}

int Gnss_Receiver :: dev_plt( const struct plt_param& plt_test)
{
	CPP_FTRACE2();
	
	if((true  == in_sleep) && adap_if.ue_wake2idle) {
			in_sleep = (0 == adap_if.ue_wake2idle(adap_if.hnd))? false : true;
	}
	adap_if.dev_plt(NULL, plt_test);
	return 0;
}
 
int Gnss_Receiver :: cleanup_entity()
{
        CPP_FTRACE2();
          int ret_val = -1; 
        if((false == in_sleep) && (false == in_works) && 
           (adap_if.ue_put2sleep)) {
                ret_val  = adap_if.ue_put2sleep(adap_if.hnd);
                in_sleep = true;
        }
  
 

		adap_if.exit();
        
        return 0;
}
 


int Gnss_Receiver :: await_exit()
{
        CPP_FTRACE2();
        
        return Base_Entity :: wait_wthread();
}

int Gnss_Receiver :: hlt_entity()
{
        CPP_FTRACE2();
        
        return Base_Entity :: stop_wthread();
}

int Gnss_Receiver :: get_ue_loc_caps(unsigned int& gps_caps_bits,
                                     struct ganss_ue_lcs_caps *ganss_caps)
{
        CPP_FTRACE2();
        
        if(adap_if.get_ue_loc_caps)
                return adap_if.get_ue_loc_caps(adap_if.hnd, gps_caps_bits, 
                                               ganss_caps);
        
        return -1;
}

int Gnss_Receiver :: loc_start()
{
        CPP_FTRACE2();

        int ret_val = -1;
        if((true  == in_sleep) && adap_if.ue_wake2idle) {
                in_sleep = (0 == adap_if.ue_wake2idle(adap_if.hnd))? false : true;
        }

        if((false == in_sleep) && adap_if.set_loc_interval && adap_if.loc_start) {
                ret_val = adap_if.set_loc_interval(adap_if.hnd, interval);
                if(0 == ret_val) {
                        ret_val = adap_if.loc_start(adap_if.hnd);
                        in_works =  (0 == ret_val)? true : false;
                }
        }
        
        return ret_val;
}

int Gnss_Receiver :: loc_stop()
{
        CPP_FTRACE2();

        if((true == in_works) && adap_if.set_loc_interval) {
                adap_if.set_loc_interval(adap_if.hnd, 0);
        }

        has_fix1 = false;
        has_time = false;
        in_works = false;
        interval = 1;

        if(adap_if.loc_stop)
                return adap_if.loc_stop(adap_if.hnd);

        return -1;
}

/* Set methods */
int Gnss_Receiver :: set_loc_interval(unsigned int interval)
{
        CPP_FTRACE2();

#if  0
	if((true == in_sleep) && adap_if.ue_wake2idle) {
		in_sleep = (0 == adap_if.ue_wake2idle(adap_if.hnd))? false : true;
	}
#endif

        /* Save interval value and configure receiver (hardware), only if, it is
           active. At other instances, interval will be configured into receiver
           upon activation
        */
           
	this->interval = interval;
	if(false == in_works) return 0;
	
	if(adap_if.set_loc_interval)
                return adap_if.set_loc_interval(adap_if.hnd, interval);

 	return -1;
}

int Gnss_Receiver :: set_intended_qop(const struct gnss_qop& qop_req)
{
        CPP_FTRACE2();

        if(adap_if.set_intended_qop)
                return adap_if.set_intended_qop(adap_if.hnd, qop_req);

        return -1;
}

int Gnss_Receiver :: nw_loc_results(const gnss_pos_result&         pos_result, 
                                    const struct gps_msr_result&   gps_result,
                                    const struct gps_msr_info     *gps_msr,
                                    const struct ganss_msr_result& ganss_result,
                                    const struct ganss_msr_info   *ganss_msr)
{
        CPP_FTRACE2();


        return -1;
}

int Gnss_Receiver :: out_of_service()
{
        //return Base_Entity :: out_of_service();
        return 0;
}

int Gnss_Receiver :: set_in_service()
{
        //return Base_Entity :: set_in_service();
        return 0;
}

int Gnss_Receiver :: ue_add_assist(const struct nw_assist_id& id,
                                   const void *assist_array, int num)
{
        CPP_FTRACE2();

        if(adap_if.ue_add_assist)
                return adap_if.ue_add_assist(adap_if.hnd, id, assist_array, 
                                             num);
        
        return -1;
}

int 
Gnss_Receiver :: ue_del_aiding(enum loc_assist assist, unsigned int sv_id_map,
                               unsigned int mem_flags)
{
        CPP_FTRACE2();

        if(adap_if.ue_del_aiding)
                return adap_if.ue_del_aiding(adap_if.hnd, assist, sv_id_map,
                                             mem_flags);
        
        return -1;
}

int Gnss_Receiver :: ue_get_aiding(enum loc_assist assist, 
                                   void *assist_array, int num)
{
        CPP_FTRACE2();

        if(adap_if.ue_get_aiding)
                return adap_if.ue_get_aiding(adap_if.hnd, assist, assist_array,
                                             num);
        
        return -1;
}

int Gnss_Receiver :: assist2ue_done()
{
        CPP_FTRACE2();
        
        if(adap_if.assist2ue_done)
                return adap_if.assist2ue_done(adap_if.hnd);

        return -1;
}

int Gnss_Receiver :: ue_loc_results(const gnss_pos_result&         pos_result, 
                                    const struct gps_msr_result&   gps_result,
                                    const struct gps_msr_info     *gps_msr,
                                    const struct ganss_msr_result& ganss_result,
                                    const struct ganss_msr_info   *ganss_msr)
{
        CPP_FTRACE2();

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return in_works? connect->ue_loc_results(pos_result, gps_result, gps_msr,
                                                 ganss_result, ganss_msr) : 0;
}

int Gnss_Receiver :: ue_decoded_aid(enum loc_assist assist, 
                                    const void *assist_array, int num)
{
        CPP_FTRACE2();

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return in_works? connect->ue_decoded_aid(assist, assist_array, num) : 0;
}

int 
Gnss_Receiver :: ue_nmea_report(enum nmea_sn_id nmea, const char *data, int len)
{
        CPP_FTRACE2();

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return in_works? connect->ue_nmea_report(nmea, data, len) : 0;
}

int Gnss_Receiver :: 
ue_aux_info4sv(const struct dev_gnss_sat_aux_desc&	 gps_desc,
               const struct dev_gnss_sat_aux_info	*gps_info,
               const struct dev_gnss_sat_aux_desc&       glo_desc,
               const struct dev_gnss_sat_aux_info	*glo_info)
{
	CPP_FTRACE2();

	Agnss_Connect *connect = Agnss_Connect :: get_instance();
	return in_works? connect->ue_aux_info4sv(gps_desc, gps_info, glo_desc,
                                                 glo_info) : 0;
}

#if  0
int Gnss_Receiver :: ue_need_assist(const struct gps_assist_req& gps_assist, 
                                    const struct ganss_assist_req& ganss_assist)
{
        CPP_FTRACE2();

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->ue_need_assist(gps_assist, ganss_assist);
}
#endif

int Gnss_Receiver :: get_ue_req4assist(struct gps_assist_req& gps_assist, 
                                       struct ganss_assist_req& ganss_assist)
{
        CPP_FTRACE2();

        if(adap_if.get_ue_req4assist)
                return adap_if.get_ue_req4assist(adap_if.hnd, gps_assist, 
                                                 ganss_assist);

        return -1;
}

int Gnss_Receiver :: get_ue_aid_ttl(void)
{
        CPP_FTRACE2();

        if(adap_if.get_ue_aid_ttl)
                return adap_if.get_ue_aid_ttl(adap_if.hnd);

        return -1;
}

bool Gnss_Receiver :: ue_is_msr_filter_gps_eph_reqd(void)
{
        CPP_FTRACE2();

        bool ret_val = false;

        if(adap_if.ue_is_msr_filter_gps_eph_reqd) 
                ret_val = adap_if.ue_is_msr_filter_gps_eph_reqd(adap_if.hnd);

        DBG_L2("Status: EPH GPS MSR Filter: %s required", ret_val? "" : "not"); 
        
        return ret_val; 
}

int 
Gnss_Receiver :: ue_msr_filter_gps_eph(const struct gps_msr_result&  gps_result,
                                       const struct gps_msr_info    *gps_msr)
{
        CPP_FTRACE2();
        
        if(adap_if.ue_msr_filter_gps_eph) 
                return adap_if.ue_msr_filter_gps_eph(adap_if.hnd, gps_result, 
						     gps_msr);

        return 0;
}
/* add for GNSS recovery ++*/
int 
Gnss_Receiver :: ue_err_recovery(enum dev_err_id err_id, unsigned int interval_secs)
{
        CPP_FTRACE2();
        
        if(adap_if.ue_err_recovery) 
                return adap_if.ue_err_recovery(adap_if.hnd, err_id,interval_secs);

        return 0;
}

// error trigger test code, to be removed
int 
Gnss_Receiver :: trigger_test_err(unsigned char val)
{
        CPP_FTRACE2();
        
        if(adap_if.trigger_test_err) 
                return adap_if.trigger_test_err(adap_if.hnd, val);

        return 0;
}
/* add for GNSS recovery --*/

/* Can be enhanced to include GPS construct as a function parameter */
void
Gnss_Receiver :: update_ue_time(void)
{
        CPP_FTRACE2();

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        
        if(false == has_time) {
                connect->decl_ue_has_time();
                has_time = true;
        }

        return;
}
/* add for GNSS recovery ++*/
int
Gnss_Receiver :: handle_ue_err(enum dev_err_id err_id)
{
        CPP_FTRACE2();

		int ret_val = -1;

        Agnss_Connect *connect = Agnss_Connect :: get_instance();     
        ret_val = connect->dispatch_ue_err(err_id);

        return ret_val;
}
/* add for GNSS recovery --*/

int Gnss_Receiver :: ue_cw_test_results(const enum cw_test_rep cwt, 
										const struct cw_test_report *cw_rep, int len)
{
        CPP_FTRACE2();
		int ret_val;
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        connect->ue_cw_test_results(cwt, cw_rep, len);
        if((false == in_sleep) && (false == in_works) && 
           (adap_if.ue_put2sleep)) {
                ret_val  = adap_if.ue_put2sleep(adap_if.hnd);
                in_sleep = true;
        }
		 return 0;
}
int Gnss_Receiver :: ue_recovery_ind(const enum dev_reset_ind rstind , 
										 int len)
{
        CPP_FTRACE2();
		int ret_val;
        Agnss_Connect *connect = Agnss_Connect :: get_instance();

		 connect->ue_recovery_ind(rstind, len);

		 return 0;
}
int Gnss_Receiver :: ue_wake2idle()
{
        CPP_FTRACE2();

        int ret_val = -1;

        if((true == in_sleep) && adap_if.ue_wake2idle) {
                ret_val  = adap_if.ue_wake2idle(adap_if.hnd);
                in_sleep = (0 == ret_val)? false : true;
        }
        
        return ret_val;
}

int Gnss_Receiver :: ue_put2sleep()
{
        CPP_FTRACE2();
        
        int ret_val = -1;
        
        if((false == in_sleep) && (false == in_works) && 
           (adap_if.ue_put2sleep)) {
                ret_val  = adap_if.ue_put2sleep(adap_if.hnd);
                in_sleep = true;
        }
        
        return ret_val;
}

int
Gnss_Receiver :: oper_ue_dev_param(enum ue_dev_param_id   id, 
                                   void *param_desc, int num)
{
        CPP_FTRACE2();
	
        if(adap_if.oper_ue_dev_param)
                return adap_if.oper_ue_dev_param(adap_if.hnd, id,
                                                 param_desc, num);
        
        return -1;
}
