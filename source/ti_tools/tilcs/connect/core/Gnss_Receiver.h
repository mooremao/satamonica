
#ifndef __GNSS_RECEIVER_H__
#define __GNSS_RECEIVER_H__

#include "Base_Entity.h"
#include "gnss_ue_adapter.h"

class Gnss_Receiver : public Base_Entity {

 private:
      
        gnss_adapter_load_fn             adap_ld;

	struct ti_gnss_ue_callbacks      adap_cb;
	struct ti_gnss_ue_interface      adap_if;
        /* Private Constructor */
        Gnss_Receiver(const char *name, gnss_adapter_load_fn load_fn);

        static Gnss_Receiver       *receiver;

        static int                  n_refers;    /* Num of users   */
        bool                        has_fix1;
        bool                        has_time;
        bool                        in_works;
        bool                        in_sleep;    /* Device asleep? */
        unsigned int                interval;    /* Periodic secs  */
        
 public:
      
        static Gnss_Receiver *create_instance(const char *name, 
                                              gnss_adapter_load_fn load_fn); 

       
	   static Gnss_Receiver *get_instance() { return receiver; }

        /* Thread Management */
        int run_entity(const char* thr_name);
        int await_exit();
        int hlt_entity();

 		int dev_init();
		int dev_plt(const struct plt_param& plt_test);
		int cleanup_entity();
         /* Get LOC methods / capabilities of UE */
        int get_ue_loc_caps(unsigned int& gps_caps_bits,
                            struct ganss_ue_lcs_caps *ganss_caps); 

        int loc_start();
        int loc_stop();

        /* Set methods */
        int set_loc_interval(unsigned int interval); /* LOC Interval in secs */
        int set_intended_qop(const struct gnss_qop& qop_ret);

        int nw_loc_results(const gnss_pos_result&         pos_result, 
                           const struct gps_msr_result&   gps_result,
                           const struct gps_msr_info     *gps_msr,
                           const struct ganss_msr_result& ganss_result,
                           const struct ganss_msr_info   *ganss_msr);


        int out_of_service();  /* Node un-available for use */
        int set_in_service();  /* Node is available for use */ 

        int ue_loc_results(const gnss_pos_result&         pos_result,
                           const struct gps_msr_result&   gps_result,
                           const struct gps_msr_info     *gps_msr,
                           const struct ganss_msr_result& ganss_result,
                           const struct ganss_msr_info   *ganss_msr);

        int ue_decoded_aid(enum loc_assist assist,
                           const void *assist_array, int num);

	int ue_nmea_report(enum nmea_sn_id nmea, const char *data, int len);

	int ue_aux_info4sv(const struct dev_gnss_sat_aux_desc&	gps_desc,
                           const struct dev_gnss_sat_aux_info	*gps_info,
                           const struct dev_gnss_sat_aux_desc&  glo_desc,
                           const struct dev_gnss_sat_aux_info	*glo_info);

        //	int ue_need_assist(const struct gps_assist_req& gps_assist,
        //                           const struct ganss_assist_req& ganss_assist);

        int ue_add_assist(const struct nw_assist_id& id,
                          const void *assist_array, int num);

        int ue_del_aiding(enum loc_assist assist, unsigned int sv_id_map,
                          unsigned int mem_flags);

        int ue_get_aiding(enum loc_assist assist, void *assist_array, int num);

        int assist2ue_done();

        bool is_ff_done() { return has_fix1; }
        void mk_ff_done() { has_fix1 = true; }

        bool is_started() { return in_works; }
        bool is_insleep() { return in_sleep; }
        unsigned int get_interval() { return interval; }

        int get_ue_req4assist(struct gps_assist_req&    gps_assist, 
                              struct ganss_assist_req&  ganss_assist);

        int get_ue_aid_ttl(void);

        /* Does UE need filtering of external GPS EPH based on MSR?
           Returns: true for Yes and false for No
        */ 
        bool ue_is_msr_filter_gps_eph_reqd();

        /* Execute filtering of GPS EPH (ext) based on device MSR. Scheduled by
           A-GNSS Connect.
           Returns: true if filtering is no more required otherwise flase
        */
        int ue_msr_filter_gps_eph(const struct gps_msr_result& gps_result,
                                   const struct gps_msr_info   *gps_msr);
		/* add for GNSS recovery ++*/
		int ue_err_recovery(enum dev_err_id err_id, unsigned int interval_secs);
		int handle_ue_err(enum dev_err_id err_id);		 
		// error trigger test code, to be removed
		int trigger_test_err(unsigned char val);
		/* add for GNSS recovery --*/		

        /* POS, MSR and Time are managed through specific API(s) */
        /* For aiding info other than time, use ue_decoded_aid() */
        /* Presents a valid and updated GPS time from sky signal */
        void update_ue_time(void);
        int  ue_put2sleep(void);   /* Put device into sleep mode */
        int  ue_wake2idle(void);   /* Get device from sleep mode */
        int oper_ue_dev_param(enum ue_dev_param_id id, 
                              void *param_desc, int num); //: add parameters if necessary
		int ue_cw_test_results(const enum cw_test_rep cwt, 
			const struct cw_test_report *cw_rep, int len);
		int ue_recovery_ind(const enum dev_reset_ind rstind, 
					 int len);
							  
};

#endif
