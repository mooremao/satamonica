#include "Base_Entity.h"
#include "Ftrace.h"
#include "debug.h"

/* Utility place holder to manage multiple parameters */
struct entry_info {

        void*       (*func)(void *parm);
        void         *parm;
        Base_Entity  *base;
};

/* Utility function to get thread name into the logs */
static void* entry_func(void *param)
{
        struct entry_info *entry_data = (struct entry_info*) param;

        void* (*func)(void*) = entry_data->func;
        void          *parm  = entry_data->parm;
        Base_Entity   *base  = entry_data->base;

        ftrace_add_fthread(pthread_self(), base->get_name());

        delete entry_data;

	DBG_L1("Launching the thread for %s entity", base->get_name());

        return func(parm); /* Call original thread func w/ parameter */
}

Gnss_Thread :: Gnss_Thread()
        : thread_func(NULL), thread_parm(NULL), thread_desc(0) /* Bad init */
{
        
}

int Gnss_Thread :: work(void* (*thread_func)(void *thread_parm), 
                        void *thread_parm, Base_Entity *base, const char *thr_name)
{
        CPP_FTRACE2();

        struct entry_info *entry_data = new struct entry_info;
        if(NULL == entry_data) {
                ERR_NM("Fatal: failed to allocate memory, aborting");
                return -1;
        }

        /* Populate data to required to launch thread */
        entry_data->func    = thread_func;
        entry_data->parm    = thread_parm;
        entry_data->base    = base;

        int ret = pthread_create(&thread_desc, NULL, entry_func, entry_data);
        if(ret) {
                ERR_NM("Fatal: failed to create a thread, aborting");
                return -1;
        }

		if(pthread_setname_np(thread_desc, thr_name)  != 0) { 
			ERR_NM("agnss_comm_put2work :pthread_setname_np failed");
		} 

        this->thread_func = thread_func;
        this->thread_parm = thread_parm;
        
        return ret;
}

int Gnss_Thread :: stop(void)
{
	//_BUILD_STL
 
        CPP_FTRACE2();
        int ret = 0;//pthread_cancel(thread_desc);
        
        return ret;
}

int Gnss_Thread :: wait(void)
{
        int ret = pthread_join(thread_desc, NULL);

        CPP_FTRACE2();
        thread_func = NULL;
        thread_parm = NULL;

        return ret;
}

Base_Entity :: Base_Entity(const char* name)
        : name_string(name),   work_thread()
        
{
        
}

int Base_Entity :: stop_wthread()
{
        return work_thread.stop();
}

int Base_Entity :: work_wthread(void* (*thread_func)(void *parm), void *parm, const char *thr_name)
{
        return work_thread.work(thread_func, parm, this, thr_name);
}

int Base_Entity :: wait_wthread()
{
        return work_thread.wait();
}

Base_Entity :: ~Base_Entity()
{

}
//  replace with typedef all function pointers

