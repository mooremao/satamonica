
#include "a-gnss_xact.h"
#include "Agnss_Services.h"
#include "Agnss_Connect.h"
#include "Gnss_Assist_Source.h"

#include <string.h>
#include <stdlib.h>
#include <iostream>
#include "debug.h"
#include "Ftrace.h"
#ifdef ANDROID
#include "utils/Log.h"
#endif

#include "Connect_Config.h"
#include "ti_log.h"
#include "log_id.h"

using namespace std;


static void print_version()
{
	cout << "A-GNSS Connect, Version 0.3.0, " <<  __DATE__ << endl;
	cout << "Texas Instruments Incorporated, Copyright 2012, ";
	cout << "All Rights Reserved" << endl;
}

static void print_options()
{
	print_version();
	cout << endl;
	cout << "Options" << endl << endl;

	cout << "-h" << "  " << "Print this menu of options for use" << endl;
	cout << "-v" << "  " << "Version of A-GNSS Connect software" << endl;
	cout << "-p" << "  " << "Create services for platform App"   << endl;

	cout << endl;
}

int gnss_ue_adap_ld_func(void *data);

static int setup_core_mods(void)
{
	CPP_FTRACE1();
	char agnssThr_name[25] = "A-GNSS_CORE_THR";
	DBG_L1("setup_core_mods: Initializing Connect Core modules\n");
	/*----------------------------------------------------------------------
	 * Setup IPC mechanism to enable RPC interworking with other components.
	 *--------------------------------------------------------------------*/
	if(agnss_xact_mod_init(TI_AGNSS_CONNECT_NW_PATH, agnssThr_name)) {
		ERR_NM("Failed to set up IPC mechanism, exiting ...");
		//exit(-1);
		 exit(EXIT_FAILURE);
	}

	/*----------------------------------------------------------------------
	 * Setup mandatory modules to let A-GNSS Connect to be operational.  
	 *--------------------------------------------------------------------*/
	Agnss_Connect *connect  = Agnss_Connect :: create_instance();
	if(!connect) {
		ERR_NM("Failed to set up Agnss Connect, exiting ...");
		//exit(-1);
		 exit(EXIT_FAILURE);
	}


	Gnss_Receiver *receiver = Gnss_Receiver :: 
		create_instance("GNSS_RECIEVER", gnss_ue_adap_ld_func);
	if(!receiver) {
		ERR_NM("Failed to set up Gnss Receiver, exiting ...");
		//exit(-1);
		exit(EXIT_FAILURE);
	}


	if(connect->add_gnss_receiver(receiver)) {
		ERR_NM("Failed to attach Receiver to Connect, exiting");
		//exit(-1);
		exit(EXIT_FAILURE);
	}

	if(receiver->run_entity("GNSS_RECEIVER")) {
		ERR_NM("Failed to attach Gnss Receiver to HW, exiting");
		//exit(-1);
		exit(EXIT_FAILURE);
	}

	DBG_L1("Setup of IPC, Agnss-Connect, Gnss-Receiver, successful ....");

	return 0;
}
#ifdef SUPL_FEATURE
int supl_adap_load_func(void *data);

static Agnss_Services *create_supl_2x_and_run(void)
{
        Agnss_Services     *supl_2x = NULL;

        DBG_L1("Attempting to run services for SUPL2.x");
        supl_2x = new Agnss_Services(TI_AGNSS_SUPLx_NAME, SRC_CAN_PENDED,
                                     supl_adap_load_func);

        if(!supl_2x) {
                ERR_NM("SUPL Service Failed, exiting ... ");
                //exit(-1);
                exit(EXIT_FAILURE);
        }
        supl_2x->set_source_pref(Connect_Config :: get_up_preference());

        DBG_L1("SUPL Service Start, Entry ..... ");
        supl_2x->run_entity("SUPL_SERVICE");
        DBG_L1("SUPL Service Start, exiting ... ");

        return supl_2x;
}
#endif
int sa_pgps_adap_load_func(void *data);

static Gnss_Assist_Source *create_sa_pgps_and_run(void)
{
        Gnss_Assist_Source *sa_pgps = NULL;

        DBG_L1("Attempting to run services for Extended Ephemeris");
        sa_pgps = new Gnss_Assist_Source(TI_AGNSS_SPGPS_NAME, SRC_CAN_PENDED,
                                         sa_pgps_adap_load_func);

        if(!sa_pgps) {
                ERR_NM("Sa/PGPS Service Failed, exiting ... ");
                //exit(-1);
                exit(EXIT_FAILURE);
        }

        /* Mark assist data not supported by SaGPS / PGPS */
        sa_pgps->set_unsupported_assist(A_GPS_TIM_REQ, A_GANSS_TIM_REQ);// Time
        sa_pgps->add_unsupported_assist(A_GPS_ACQ_REQ, 0);           // GPS ACQ

        sa_pgps->set_unsupported_assist(e_gnss_glo, A_GANSS_MSR_REQ);// GLO ACQ
        sa_pgps->set_source_pref(Connect_Config :: get_ee_preference());

        sa_pgps->run_entity("SA-PGPS SERVICE");

        return sa_pgps;
}
#ifdef CPLANE_FEATURE
int ctrl_plane_adap_load_func(void *data);

static Agnss_Services *create_c_plane_and_run(void)
{
        Agnss_Services *c_plane = NULL;

        DBG_L1("Attempting to run services for C-Plane");
        c_plane = new Agnss_Services(TI_AGNSS_CPLNE_NAME, SRC_CALIB_TCXO,
                                     ctrl_plane_adap_load_func);

        if(!c_plane) {
                ERR_NM("C-Plane Service Failed, exiting ... ");
               // exit(-1);
               exit(EXIT_FAILURE);
        }

        /* Unless triggered by C-Plane, no assistance can be solicited */
        c_plane->set_solicit_assist(false);

        c_plane->run_entity("CPLANE-SERVICE");

        return c_plane;
}
#endif
int pfm_app_adap_load_func(void *data);

static Agnss_Services *create_pfm_app_and_run(void)
{
       Agnss_Services *pfm_app = NULL;

       DBG_L1("Attempting to run services for platform app");
       pfm_app = new Agnss_Services(TI_AGNSS_PFORM_NAME, SRC_CAN_PENDED,
                                    pfm_app_adap_load_func);

       if(!pfm_app) {
                ERR_NM("Pfm App Service Failed, exiting ... ");
                //exit(-1);
                exit(EXIT_FAILURE);
       }

       pfm_app->set_solicit_assist(false);

       pfm_app->run_entity("PFORM-SERVICE");

       return pfm_app;
}

template <typename T> static void await_exit_and_delete(T *t, const char *name)
{
        DBG_L1("Awaiting Exit for %s", name);
        t->await_exit();
        DBG_L1("Exit for %s complete", name);

        delete t;
} 



/*==============================================================================
 * DO NOT CHANGE THE SEQUENCE OF SERVICE LAUNCH - change at your own risk
 * 
 * 1. Platform Services
 * 2. SUPL 2.x
 * 3. SaGPS - PGPS 
 * 4. Control Plane
 *============================================================================*/
static int launch_services(int sched_pfm_app, int sched_sa_pgps, 
                           int sched_supl_2x, int sched_c_plane)
{
	CPP_FTRACE1();

	/*----------------------------------------------------------------------
	 * Create and run node to interwork with SUPL2.x component
	 *--------------------------------------------------------------------*/
	Agnss_Services     *supl_2x = NULL; 
#ifdef SUPL_FEATURE
	if(sched_supl_2x) {
                supl_2x = create_supl_2x_and_run();
        }
#endif
	/*----------------------------------------------------------------------
	 * Create and run node to interwork with SA-OR-PGPS component
	 *--------------------------------------------------------------------*/
        Gnss_Assist_Source *sa_pgps = NULL;

	if(sched_sa_pgps) {
                sa_pgps = create_sa_pgps_and_run();
        }

	/*----------------------------------------------------------------------
	 * Create and run node to interwork with CPLANE component
	 *--------------------------------------------------------------------*/
#ifdef CPLANE_FEATURE
	Agnss_Services      *c_plane = NULL;

	if(sched_c_plane) {
                c_plane = create_c_plane_and_run();
        }
#endif
	/*----------------------------------------------------------------------
	 * Create and run node to interwork with Platform App
	 *--------------------------------------------------------------------*/
	Agnss_Services	     *pfm_app = NULL;

	if(sched_pfm_app) {
                pfm_app = create_pfm_app_and_run();
        }

        /*---------------------------------------------------------------------
         * Kludges to make software work 
         *--------------------------------------------------------------------*/
         if((NULL != supl_2x) && (NULL != sa_pgps)) {
                /* If SUPL is configured in system then, do not blindly fetch
                   EPH from Sa/PGPS, if POS is not availalbe */
                sa_pgps->add_unsupported_assist(A_GPS_POS_REQ, A_GANSS_POS_REQ);
         }

	/*----------------------------------------------------------------------
	 * Everthing in place - finalize.
	 *--------------------------------------------------------------------*/
         Agnss_Connect *connect = Agnss_Connect :: get_instance();
         connect->sort_assist_src();
         connect->use_multi_src(Connect_Config :: is_multi_source_en());

	/*----------------------------------------------------------------------
	 * Wait for all services to exit.
	 *--------------------------------------------------------------------*/
	if(sched_pfm_app) await_exit_and_delete(pfm_app, TI_AGNSS_PFORM_NAME); 

	if(sched_sa_pgps) await_exit_and_delete(sa_pgps, TI_AGNSS_SPGPS_NAME);
#ifdef SUPL_FEATURE
	if(sched_supl_2x) await_exit_and_delete(supl_2x, TI_AGNSS_SUPLx_NAME);
#elif CPLANE_FEATURE
	if(sched_c_plane) await_exit_and_delete(c_plane, TI_AGNSS_CPLNE_NAME);
#endif
	DBG_L1("launch_services: Exiting");

	return 0;
}


int main(int argc, char **argv)
{
	int sched_pfm_app,  sched_sa_pgps,  sched_supl_2x,  sched_c_plane;
	sched_pfm_app = sched_sa_pgps = sched_supl_2x = sched_c_plane = 0;

	pthread_t thread_id;
    ti_log_init(e_log_connect); /* LOG MD */

#define FILE_NAME     TI_AGNSS_DIRECTORY_FPATH TI_AGNSS_CNECT_NAME 

        debug_init (FILE_NAME ".idebug");
        ftrace_init(FILE_NAME ".ftrace");
        dmsg_init  (FILE_NAME ".iomesg");

	Connect_Config :: init_connect_config();

	thread_id = pthread_self();

	if(pthread_setname_np(thread_id, "AGNSS_MAIN_THREAD") != 0)	{
		ERR_NM("pthread_setname_np failed in main");
	}
	CPP_FTRACE1();

        DBG_L1("Started Connect Process compiled %s %s...", __DATE__, __TIME__);

	sched_supl_2x =(true == Connect_Config :: is_supl_2x_plane_en())? 1 : 0;
	sched_sa_pgps =(true == Connect_Config :: is_predicted_eph_en())? 1 : 0;
	sched_c_plane =(true == Connect_Config :: is_control_plane_en())? 1 : 0;

	for(int i = 1; i < argc; i++) {

		if(!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
			print_options();
			exit(0);
		}

		if(!strcmp(argv[i], "-v") || !strcmp(argv[i], "-V")) {
			print_options();
			exit(0);
		}

		if(!strcmp(argv[i], "-p") || !strcmp(argv[i], "-P")) {
			sched_pfm_app = 1; // Need to service Platform App
			continue;
		}

		cout << "Unknown option " << argv[i] << " specified, exiting..." 
			<< endl << endl; 

		print_options();
		exit(0);
	}

	DBG_L1("Selection: pfm_app: %d, supl_2x: %d, sa_pgps: %d, c_plane: %d",
               sched_pfm_app,  sched_supl_2x,  sched_sa_pgps,  sched_c_plane);

	if(sched_pfm_app || sched_sa_pgps || sched_supl_2x || sched_c_plane) {
		DBG_L1("Launching selected services .... " );
		setup_core_mods();
		launch_services(sched_pfm_app, sched_sa_pgps, 
				sched_supl_2x, sched_c_plane);

	}

	exit(0);
}
