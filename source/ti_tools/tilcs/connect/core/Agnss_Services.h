

#ifndef __AGNSS_SERVICES_H__
#define __AGNSS_SERVICES_H__

#include "Assist_Source.h"
#include "Remote_Client.h"
#include "ti_agnss_lcs_adapter.h"

class Agnss_Services : public Assist_Source, public Remote_Client 
{
 private:
        gnss_adapter_load_fn             adap_ld;

        struct ti_agnss_lcs_adap_cb      adap_cb;
        struct ti_agnss_lcs_adap_if      adap_if;

		unsigned int                     ue_usr; // temp fixit
 protected:

        int ue_service_ctl(enum ue_service_ind ind,
                           unsigned int     usr_id);

        int ue_need_assist(const struct gps_assist_req& gps_assist, 
                           const struct ganss_assist_req& ganss_assist,
                           unsigned int     usr_id);

 public:
        /* Constructor */
        Agnss_Services(const char *name, unsigned int flags, 
		       gnss_adapter_load_fn load_fn);
 
        /* Thread Management */
        int run_entity(const char* thr_name);
        int await_exit();
        int hlt_entity();

        /* Get LOC methods / capabilities of UE */
        int get_ue_loc_caps(unsigned int& gps_caps_bits,
                            struct ganss_ue_lcs_caps *ganss_caps); 

        int loc_start();
        int loc_stop();

		int dev_init();
        int dev_plt(const struct plt_param& plt_test); /* PLT test */
		int ue_cleanup();
         /* Set methods */
        int set_loc_instruct(const struct loc_instruct& loc); /* LOC Instruct */

        int set_rpt_criteria(const struct rpt_criteria& rpt); /* RPT Criteria */


        int ue_loc_results(const struct gnss_pos_result&  pos_result, 
                           const struct gps_msr_result&   gps_result,
                           const struct gps_msr_info     *gps_msr,
                           const struct ganss_msr_result& ganss_result,
                           const struct ganss_msr_info   *ganss_msr);

        int ue_nmea_report(enum nmea_sn_id nmea, const char *data, int len);

        int ue_aux_info4sv(const struct dev_gnss_sat_aux_desc&  gps_desc,
                           const struct dev_gnss_sat_aux_info  *gps_info,
                           const struct dev_gnss_sat_aux_desc&  glo_desc,
                           const struct dev_gnss_sat_aux_info  *glo_info);

        int ue_decoded_aid(enum loc_assist assist, const void *assist_array,
                           int num);


        int out_of_service();  /* Node un-available for use */
        int set_in_service();  /* Node is available for use */ 

        int ue_add_assist(const struct nw_assist_id& id,
                          const void *assist_array, int num);

        int ue_del_aiding(enum loc_assist assist, unsigned int sv_id_map,
                          unsigned int mem_flags);

        int ue_get_aiding(enum loc_assist assist, void *assist_array, int num); 
        int assist2ue_done(const struct assist_reference& assist_usr);

        int set_loc_interval(unsigned int interval_secs);

	    int oper_ue_dev_param(enum ue_dev_param_id id, 
		                      void *param_desc, int num);

        int nw_loc_results(const struct gnss_pos_result&       pos_result,
                                const struct assist_reference& assist_usr);

        int decl_dev_param(enum ue_dev_param_id   id, 
                           void *param_desc, int num);

		
		int ue_cw_test_results(const enum cw_test_rep cwt, 
				const struct cw_test_report *cw_rep, int len);
		int ue_recovery_ind(const enum dev_reset_ind rstind, 
				int len);
};

#endif
