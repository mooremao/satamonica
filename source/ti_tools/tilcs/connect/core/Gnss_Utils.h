#ifndef __GNSS_UTILS_H__
#define __GNSS_UTILS_H__

#include "gnss.h"
extern "C" {
#include "dev_proxy.h"
}

#include "Gnss_Assist_Source.h"
#include "Agnss_Services.h"
#include "Gnss_Receiver.h"

inline struct gps_msr_info *create_gps_msr_info()
{
	return new gps_msr_info[MAX_SAT];
}

inline void delete_gps_msr_info(struct gps_msr_info *gps_msr)
{
	delete[] gps_msr;
}

inline struct ganss_msr_desc *create_ganss_msr_desc()
{
	return new ganss_msr_desc[MAX_GANSS];
}

inline void delete_ganss_msr_desc(struct ganss_msr_desc *msr_desc)
{
	delete[] msr_desc;
}

inline struct ganss_ue_lcs_caps *create_ganss_ue_lcs_caps()
{
	struct ganss_ue_lcs_caps *obj = new ganss_ue_lcs_caps;
        if(NULL != obj) {
		obj->n_ganss = MAX_GANSS ;
	}

	return obj;	
}

inline void delete_ganss_ue_lcs_caps(struct ganss_ue_lcs_caps *obj)
{
	delete obj;
}

/*------------------------------------------------------------------------------
 * Utilities to call Object Methods from Adapter functions (gnss_lcs_common.h).
 *-----------------------------------------------------------------------------*/
	template <typename T> 
static inline int adap_get_ue_loc_caps(void *hnd, unsigned int& gps_caps_bits, 
									   struct ganss_ue_lcs_caps *ganss_caps)
{
	return ((T*)hnd)->get_ue_loc_caps(gps_caps_bits, ganss_caps);
}

	template <typename T> 
static inline int adap_get_ue_agnss_caps(void *hnd, unsigned int& gps_caps_bits, 
										 struct ganss_ue_assist_caps *ganss_caps)
{
	return ((T*)hnd)->get_ue_agnss_caps(gps_caps_bits, ganss_caps);
}

	template <typename T> 
static inline int adap_set_loc_interval(void *hnd, unsigned int interval_secs)
{
	return ((T*)hnd)->set_loc_interval(interval_secs);
}


template <typename T>
static inline int adap_set_intended_qop(void *hnd); // TBD

	template <typename T> 
static inline int adap_ue_add_assist(void *hnd, const struct nw_assist_id& id,
									 const void *assist_array, int num)
{
	return ((T*)hnd)->ue_add_assist(id, assist_array, num);
}

	template <typename T> 
static inline int adap_ue_del_aiding(void *hnd, enum loc_assist assist, 
									 unsigned int sv_id_map, 
									 unsigned int mem_flags)
{
	return ((T*)hnd)->ue_del_aiding(assist, sv_id_map, mem_flags);
}

	template <typename T> 
static inline int adap_ue_get_aiding(void *hnd, enum loc_assist assist, 
									 void *assist_array, int num)
{
	return ((T*)hnd)->ue_get_aiding(assist, assist_array, num);
}

	template <typename T> 
static inline int adap_ue_assist2ue_done(void *hnd,
					const struct assist_reference& assist_ref)
{
	return ((T*)hnd)->assist2ue_done(assist_ref);
}
template <typename T> static inline int adap_loc_start(void *hnd)
{
	return ((T*)hnd)->loc_start();
}

template <typename T> static inline int adap_loc_stop(void *hnd)
{
	return ((T*)hnd)->loc_stop();
}

 template <typename T> static inline int adap_ue_cleanup(void *hnd)
{
	return ((T*)hnd)->ue_cleanup();
}
template <typename T> static inline int adap_dev_init(void *hnd)
{
	return ((T*)hnd)->dev_init();
}
 

template <typename T> 
static inline int adap_dev_plt(void *hnd, const struct plt_param& plt_test)
{
	return ((T*)hnd)->dev_plt(plt_test);
}

 

template <typename T> 
	static inline int 
adap_get_ue_req4assist(void *hnd, struct gps_assist_req& gps_assist,
					   struct ganss_assist_req *ganss_assist)
{
	return ((T*)hnd)->get_ue_req4assist(gps_assist, ganss_assist);
}


template <typename T> 
	static inline int 
adap_ue_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
					const struct gps_msr_result&    gps_result, 
					const struct gps_msr_info      *gps_msr,
					const struct ganss_msr_result&  ganss_result,
					const struct ganss_msr_info    *ganss_msr)
{
	return ((T*)hnd)->ue_loc_results(pos_result, gps_result, gps_msr, 
									 ganss_result, ganss_msr);
}

template <typename T> 
	static inline int 
adap_nw_loc_results(void *hnd, const struct gnss_pos_result& pos_result)
{
	return ((T*)hnd)->nw_loc_results(pos_result);
}

template <typename T> 
	static inline int 
adap_ue_need_assist(void *hnd, const struct gps_assist_req& gps_assist,
					const struct ganss_assist_req& ganss_assist)
{
	return ((T*)hnd)->ue_need_assist(gps_assist, ganss_assist);
}

	template <typename T> 
static inline int adap_ue_nmea_report(void *hnd, enum nmea_sn_id nmea, 
									  const char *data, int len)
{
	return ((T*)hnd)->ue_nmea_report(nmea, data, len);
}

	template <typename T> 
static inline int adap_ue_aux_info4sv(void *hnd, 
									  const struct dev_gnss_sat_aux_desc&  gps_desc,
									  const struct dev_gnss_sat_aux_info  *gps_info,
									  const struct dev_gnss_sat_aux_desc&  glo_desc,
									  const struct dev_gnss_sat_aux_info  *glo_info)
{
	return ((T*)hnd)->ue_aux_info4sv(gps_desc, gps_info, glo_desc, glo_info);
}
	template <typename T> 
static inline int adap_ue_decoded_aid(void *hnd, enum loc_assist assist, 
									  const void *assist_array, int num)
{
	return ((T*)hnd)->ue_decoded_aid(assist, assist_array, num);
}

	template <typename T>
static inline int adap_set_loc_instruct(void *hnd, const struct loc_instruct& req)
{
	return ((T*)hnd)->set_loc_instruct(req);
}

	template <typename T>
static inline int adap_set_rpt_criteria(void *hnd, const struct rpt_criteria& rpt)
{
	return ((T*)hnd)->set_rpt_criteria(rpt);
}

	template <typename T>
static inline void adap_update_ue_time(void *hnd)
{
	return ((T*)hnd)->update_ue_time();
}

/* add for GNSS recovery ++*/
	template <typename T>
static inline int adap_handle_ue_err(void *hnd, enum dev_err_id err_id)
{
	return ((T*)hnd)->handle_ue_err(err_id);
}	
/* add for GNSS recovery --*/

	template <typename T>
static inline int adap_ue_cw_test_results(void *hnd, const enum cw_test_rep cwt, 
											const cw_test_report*cw_rep, int len)
{
	return ((T*)hnd)->ue_cw_test_results(cwt, cw_rep, len);
}	

	template <typename T>
static inline int adap_ue_recovery_ind(void *hnd, const enum dev_reset_ind rstind, 
											 int len)
{
	return ((T*)hnd)->ue_recovery_ind(rstind, len);
}	

	template <typename T>
static inline int adap_ue_dev_oper_param(void *hnd,enum ue_dev_param_id id,
			                 void *param_desc, int num)
{
	return ((T*)hnd)->oper_ue_dev_param(id, param_desc, num);
}

	template <typename T>
static inline int adap_nw_loc_results(void *hnd, 
                                      const struct gnss_pos_result& pos_result, 
                                      const struct assist_reference& assist_ref)
{
        return ((T*)hnd)->nw_loc_results(pos_result, assist_ref);
}

/*------------------------------------------------------------------------------
 * Generic utilities
 *----------------------------------------------------------------------------*/
template <typename T>
static inline T min_val(const T& t1, const T& t2)
{
	return (t1 < t2) ? t1 : t2;
}


/*------------------------------------------------------------------------------
 * Declarations
 *----------------------------------------------------------------------------*/
const int          TIME2RSP_INVALID = 0x7FFFFFFF;  /* Invalid time to respond */
const unsigned int INTERVAL_INVALID = 0xFFFFFFFF;  /* Invalid time interval   */

int gnss_run_node(Agnss_Services *node,
				  const struct ti_agnss_lcs_adap_if *adap_if,
				  const struct ti_agnss_lcs_adap_cb *adap_cb,
				  gnss_adapter_load_fn adap_ld, const char* thr_name);

int gnss_run_node(Gnss_Assist_Source *node,
				  const struct ti_lc_assist_adap_if *adap_if,
				  const struct ti_lc_assist_adap_cb *adap_cb,
				  gnss_adapter_load_fn adap_ld,const char* thr_name);

int gnss_run_node(Gnss_Receiver *node,
				  const struct ti_gnss_ue_interface *adap_if,
				  const struct ti_gnss_ue_callbacks *adap_cb,
				  gnss_adapter_load_fn adap_ld,const char* thr_name);
 unsigned short ganss_id2pmthd_bit(const unsigned char ganss_id);


#endif
