#include "Remote_Client.h"
#include "Agnss_Connect.h"
#include "Gnss_Utils.h"

#include <sys/time.h>
#include <string.h>
#include <math.h>
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include "debug.h"
#include "Ftrace.h"

inline unsigned char alt_unc_3gpp_MtoK(float unc)
{
	CPP_FTRACE2();
        return ceil(log((unc / 45) + 1) / log(1.025));
}

inline float alt_unc_3gpp_KtoM(unsigned char K)
{
	CPP_FTRACE2();
        return 45*(pow((1 + 0.025), K) - 1);
}

inline unsigned char hor_unc_3gpp_MtoK(float unc)
{
	CPP_FTRACE2();
        return ceil(log((unc / 10) + 1) / log(1.100));
}

inline float hor_unc_3gpp_KtoM(unsigned char K)
{
	CPP_FTRACE2();
        return 10*(pow((1 + 0.100), K) - 1);
}

Loc_Rsp_Conf :: Loc_Rsp_Conf()
	: conf_flag(false), interval(0), time4rspn(0), time4rsp1(0), 
	  rsp1_pend(true)
{
        
}

int Loc_Rsp_Conf :: reset_config(void)
{
	CPP_FTRACE2();
        conf_flag  = false;
        interval   = 0;
        //num_fixes  = 0;
        
        time4rsp1  = 0;
        time4rspn  = 0;
        rsp1_pend  = true;
        
        return 0;
}

int os_monotonic_secs()
{
	CPP_FTRACE2();
	struct timespec tv;

	if(clock_gettime(CLOCK_MONOTONIC, &tv))
		return TIME2RSP_INVALID;

	return tv.tv_sec + ((tv.tv_nsec > 500000000)? 1 : 0);
}

/* : Change to set_rsp1_time */
int Loc_Rsp_Conf :: set_req_time(unsigned int rsp_secs)
{
	CPP_FTRACE2();
        time4rsp1 = os_monotonic_secs() + rsp_secs - 2;
        
        return 0;
}

int os_secs2elapse(int time_secs)
{
	CPP_FTRACE2();

        struct timeval tv;
        
        if(gettimeofday(&tv, NULL))
                return TIME2RSP_INVALID;
        
        return time_secs - (tv.tv_sec + ((tv.tv_usec > 500000)? 1 : 0));
}

int os_monotonic_secs2elapse(int monotonic_secs)
{
	CPP_FTRACE2();

	struct timespec tv;
        
        if(clock_gettime(CLOCK_MONOTONIC, &tv))
                return TIME2RSP_INVALID;
        
        return monotonic_secs - (tv.tv_sec + ((tv.tv_nsec > 500000000)? 1 : 0));
}

int Loc_Rsp_Conf :: time2next_rsp()
{
	CPP_FTRACE2();
        if(false == conf_flag)
                return TIME2RSP_INVALID;
        
        return os_monotonic_secs2elapse(time4rspn);
}

int Loc_Rsp_Conf :: time2put_rsp1()
{
	CPP_FTRACE2();
        if(false == conf_flag)
                return TIME2RSP_INVALID;

        return os_monotonic_secs2elapse(time4rsp1);
}

int Loc_Rsp_Conf :: prep4next_rsp()
{
	CPP_FTRACE2();
        if(true == rsp1_pend) {
                rsp1_pend = false;
                time4rspn = os_monotonic_secs();
        }
        
        time4rspn += interval;
        DBG_L3("Loc_Rsp_Conf: prep4next_rsp: time4rspn %u", time4rspn);
        return 0;
}

#if  0
unsigned short ganss_id2pmthd_bit(const unsigned char ganss_id)
{
        unsigned short ret_bit = 0;

        switch(ganss_id) {

        case GANSS_ID_GLO:
                ret_bit = GNSS_GLO_PMTHD_BIT;
                break;

        default:
                break;
        }

        return ret_bit;
}
#endif

Remote_Client :: Remote_Client(const char *name)
        : Base_Entity(name),
          rsp_config(),
          method_types(0),
          n_of_methods(0),
          methods_gnss(0)
          
{
	CPP_FTRACE2();
        memset(&qop_desired, 0, sizeof(struct gnss_qop));
        
        qop_desired.acc_sel = no_acc;

}

 /* Get LOC methods / capabilities of UE */
int Remote_Client :: get_ue_loc_caps(unsigned int& gps_caps_bits,
                                     struct ganss_ue_lcs_caps *ganss_caps)
{
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        return connect->get_ue_loc_caps(gps_caps_bits, ganss_caps);
}

double pythagorus(double t1,double t2)
{
	CPP_FTRACE2();
	return sqrt(t1*t1 + t2*t2);
}

/* Private function */
unsigned char Remote_Client :: min_hacc(const struct loc_results& results)
{
        CPP_FTRACE2();

        unsigned int flags = 0;
        unsigned char  val = 127; /* => 1800KM as per 3GPP 25.032, section 6.2 */
        
        /**** GNSS  POS ****/
        const struct gnss_pos_result *pos_result    = results.pos_result;
        flags = POS_ERR_REPORTED | POS_NOT_REPORTED;
        if(!(pos_result->contents & flags)) {
                DBG_L3("POS RPT is now available, evaluating HOR ACC params");
                unsigned char major = pos_result->location.unc_semi_maj_K;
                unsigned char minor = pos_result->location.unc_semi_min_K;

                val = hor_unc_3gpp_MtoK(pythagorus(hor_unc_3gpp_KtoM(major), 
                                                   hor_unc_3gpp_KtoM(minor)));
        }
        
        /**** GPS   MSR ****/ 
        const struct gps_msr_result *gps_result     = results.gps_result;
        flags = GPS_MSR_ERR_REPORTED | GPS_MSR_NOT_REPORTED;
        if(!(gps_result->contents & flags)) {
                DBG_L3("GPS MSR is now available, evaluating HOR ACC params");
                val = min_val(val, gps_result->h_acc_K);
        }

        /**** GANSS MSR ****/
        const struct ganss_msr_result *ganss_result = results.ganss_result;
        flags = GANSS_MSR_ERR_REPORTED | GANSS_MSR_NOT_REPORTED;
        for(int i = 0; i < ganss_result->n_ganss; i++) {
                if(!(ganss_result->contents & flags)) {
                        DBG_L3("Evaluating HOR ACC params for GANSS");
                        val = min_val(val, ganss_result->h_acc_K);
                }
        }

        return val;
}

unsigned char Remote_Client :: min_vacc(const struct loc_results& results)
{
        CPP_FTRACE2();

        unsigned int flags = 0;
        unsigned char  val = 127; /* => 990.5M as per 3GPP 25.032, section 6.4 */
        
        /**** GNSS  POS ****/
        const struct gnss_pos_result *pos_result    = results.pos_result;
        flags = POS_ERR_REPORTED | POS_NOT_REPORTED;
        if(!(pos_result->contents & flags)) {
                DBG_L3("POS RPT is now available, evaluating VER ACC params");
                val = pos_result->location.unc_altitude_K;
        }

        /**** GPS   MSR ****/ 
        const struct gps_msr_result *gps_result     = results.gps_result;
        flags = GPS_MSR_ERR_REPORTED | GPS_MSR_NOT_REPORTED;
        if(!(gps_result->contents & flags)) {
                DBG_L3("GPS RPT is now available, evaluating VER ACC params");
                val = min_val(val, gps_result->v_acc_K);
        }

        /**** GANSS MSR ****/
        const struct ganss_msr_result *ganss_result = results.ganss_result;
        flags = GANSS_MSR_ERR_REPORTED | GANSS_MSR_NOT_REPORTED;
        for(int i = 0; i < ganss_result->n_ganss; i++) {
                if(!(ganss_result->contents & flags)) {
                        DBG_L3("Evaluating VER ACC params for GANSS");
                        val = min_val(val, ganss_result->v_acc_K);
                }
        }

        return val;
}

/* Check whether established results have used reqd POS types & GNSS methods.
   Return true, if yes, otherwise, returns false.
*/
bool Remote_Client :: chk_methods(const struct loc_results& results)
{
        CPP_FTRACE2();

	int n_result = 0;
	const struct gps_msr_result *gps_result;
	const struct ganss_msr_result *ganss_result;

	/**** GNSS	POS ****/
	const struct gnss_pos_result *pos_result	= results.pos_result;
	if(method_types & (UE_LCS_CAP_AGNSS_POS | UE_LCS_CAP_GNSS_AUTO)) {
                n_result += (pos_result->contents &    POS_NOT_REPORTED)? 0 : 1;
                DBG_L3("%s: POS %sreported", get_name(), n_result? "" : "NOT ");
	}

	if(!(method_types & UE_LCS_CAP_AGNSS_MSR)) {	 /* UE Assisted */
                DBG_L3("MSR RPT(s) not required by %s", get_name());
                goto chk_methods_exit;
	}

	/* GNSS Measurements are required so, analyze a) GPS MSR b) GANSS MSR */

	/**** GPS MSR ****/ 
	gps_result = results.gps_result;
	if(methods_gnss &  GNSS_GPS_PMTHD_BIT) {
			n_result += (gps_result->contents & GPS_MSR_NOT_REPORTED)? 0 : 1; 
	}

	/**** GANSS MSR ****/
	ganss_result = results.ganss_result;
	if(methods_gnss & ~GNSS_GPS_PMTHD_BIT) {
			/* Are there GANSS methods that have been requested? */
			n_result += (ganss_result->contents & GANSS_MSR_NOT_REPORTED)? 0 : 1;
	}

chk_methods_exit:		
	DBG_L3("Total # of results for %s is %d", get_name(), n_result);
	return (n_result > 0) ? true : false;		 
}

bool Remote_Client :: fits_qop_need(const gnss_pos_result&          pos_result, 
                                    const struct gps_msr_result&    gps_result,
                                    const struct gps_msr_info      *gps_msr,
                                    const struct ganss_msr_result&  ganss_result,
                                    const struct ganss_msr_info    *ganss_msr)
{
        CPP_FTRACE2();

        unsigned char accuracy_K;

        struct loc_results results = {
                /* Make assignment to covering structure */
                &pos_result,
                &gps_result,
                gps_msr,
                &ganss_result,
                ganss_msr
        };

        if(false == chk_methods(results)) {
                DBG_L2("Info absent %s continues to wait for RSLT", get_name());
                return false;
        }

        if(no_acc == qop_desired.acc_sel) {
                DBG_L3("No QoP configutred for %s, using LOC RSLT", get_name()); 
                return true;
        }

        accuracy_K  = min_vacc(results);
        /* If a 3D LOC information is expected, assert vertical QoP */
        if(acc_3d == qop_desired.acc_sel) {
                float acc_M = alt_unc_3gpp_KtoM(accuracy_K);
                if(2*(qop_desired.v_acc_M) < acc_M) {
                        DBG_L1("Unfit V acc: %fM (RSLT) vs %dM (QoP)", acc_M, \
                               qop_desired.v_acc_M);
                        return false;
                }
        }

        accuracy_K  = min_hacc(results);
        float acc_M = hor_unc_3gpp_KtoM(accuracy_K);
        /* Assert horizontol QoP */
        if(2*(qop_desired.h_acc_M) < acc_M) {
                DBG_L1("Unfit H acc: %fM (RSLT) vs %dM (QoP)", acc_M, \
                       qop_desired.h_acc_M); 
                return false;
        }

        return true;
}

int Remote_Client :: time2next_rsp()
{
	CPP_FTRACE2();
	int ret_val = -1;
	 //return rsp_config.time2next_rsp();
        ret_val = rsp_config.time2next_rsp();
	 DBG_L3("time2next_rsp: ret_val %d", ret_val);
	return ret_val;
}

int Remote_Client :: time2put_rsp1()
{
	CPP_FTRACE2();
	int ret_val = -1;
       //return rsp_config.time2put_rsp1();
	ret_val = rsp_config.time2put_rsp1();
	 DBG_L3("time2next_rsp1: ret_val %d", ret_val);
	 return ret_val;
}

bool Remote_Client :: awaiting_rsp1()
{
	CPP_FTRACE2();
	bool ret_val = false;
       //return rsp_config.awaiting_rsp1();
	ret_val = rsp_config.awaiting_rsp1();
	DBG_L3("awaiting_rsp1: ret_val %d", ret_val);
	return ret_val;
}
int   Remote_Client :: prep4next_rsp(void)
{
	CPP_FTRACE2();
	int ret_val = -1;
	//return rsp_config.prep4next_rsp();
	ret_val = rsp_config.prep4next_rsp();
	 DBG_L3("prep4next_rsp: ret_val %d", ret_val);
	 return ret_val;
}

int Remote_Client :: loc_start()
{
        CPP_FTRACE2();

        if(rsp_config.is_cfg_valid()) {

                rsp_config.set_req_time(qop_desired.rsp_secs);
                Agnss_Connect *connect     = Agnss_Connect :: get_instance();
                unsigned int interval      = rsp_config.get_interval();
                return connect->loc_request(this, qop_desired, interval);
        }

        return -1;
}


int Remote_Client :: oper_ue_dev_param(enum ue_dev_param_id id,
                                       void *param_desc, int num)
{
        CPP_FTRACE2();	
        Agnss_Connect *connect = Agnss_Connect :: get_instance();

        return connect->oper_ue_dev_param(id, param_desc, num);
}
int Remote_Client :: loc_stop()
{
	 int ret_val = (Agnss_Connect :: get_instance())->end_loc_req(this);
        rsp_config.reset_config();
        
        qop_desired.acc_sel = no_acc;

        return ret_val;
}

 int Remote_Client :: ue_cleanup()
{
        CPP_FTRACE2();	
        Agnss_Connect *connect = Agnss_Connect :: get_instance();

        return connect->agnss_cleanup();
}

int Remote_Client :: dev_init()
{
        CPP_FTRACE2();	
        Agnss_Connect *connect = Agnss_Connect :: get_instance();

        return connect->agnss_dev_init();
}
int Remote_Client :: dev_plt(const struct plt_param& plt_test)
{
        CPP_FTRACE2();

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        connect->agnss_dev_plt(this, plt_test);
        return 0;
}
 

static bool is_ue_assisted(unsigned int method_types)
{
        return  method_types & UE_LCS_CAP_AGNSS_MSR ? true : false;
}

bool
Remote_Client :: assess_ganss4uea(unsigned int                    type_flags,
                                  unsigned short                  lc_methods,
                                  const struct ganss_ue_lcs_caps *ganss_caps)
{
        CPP_FTRACE2();

        unsigned short mthd_bit;

        if(!is_ue_assisted(type_flags)) {
                DBG_L2("Wrong invoke: LOC REQ is NOT UEA Method type");
                return false;
        }
                
        DBG_L3("MSR Number of GANSS in UE %d", ganss_caps->n_ganss); 	
        for(unsigned char i = 0; (i < ganss_caps->n_ganss) && lc_methods; i++) {

                mthd_bit = ganss_id2pmthd_bit(ganss_caps->lcs_caps[i].ganss_id);
                DBG_L1("UEA: do chk for UE GANSS (method bit 0x%x)", mthd_bit);

                /* Special check to ensure that if GANSSs are included in UE
                   Assisted request then, each of those GANSSs is enabled in 
                   UE.
                   
                   This version of UE supports separate MSR Report for  each 
                   of GNSS enabled on UE.

                   Refer to section 8.6.7.19.1 of 3GPP specification   25.331 
                   for details.

                   Step A: Ascertain, a UE GANSS, if included in MSR REQ, has
                   requisite capabilities.
                */
                if(!(lc_methods & mthd_bit)) {
                        DBG_L2("GANSS not mentioned in MSR REQ --> Continue");
                        continue;
                }

                if(!is_ue_assisted(ganss_caps->lcs_caps[i].caps_bits)) {
                        DBG_L1("No UEA for UE GANSS;   MSR REQ --> Rejected"); 
                        break;
                }

                lc_methods &= ~mthd_bit;
        }

        /* Step B: UE supports all requested GANSS? If not, return false   */
        DBG_L3("MSR REQ, pending GANSS(s) bit 0x%x", lc_methods);

        return (lc_methods == 0)? true : false;
}

static bool is_ue_based(unsigned int method_types) 
{
        return  method_types & UE_LCS_CAP_AGNSS_POS ? true : false;
}

bool 
Remote_Client :: assess_ganss4ueb(unsigned int                    type_flags,
                                  unsigned short                  lc_methods,
                                  const struct ganss_ue_lcs_caps *ganss_caps)
{
        CPP_FTRACE2();

        unsigned short mthd_bit;

        if(!is_ue_based(type_flags)) {
                DBG_L2("Wrong invoke: LOC REQ is NOT UEB Method type");
                return false; 
        }

        for(unsigned char i = 0; (i < ganss_caps->n_ganss); i++) {

                mthd_bit = ganss_id2pmthd_bit(ganss_caps->lcs_caps[i].ganss_id);
                DBG_L1("UEB use chk for UE GANSS (method bit 0x%x)", mthd_bit);

                /* Special check to ensure that if GANSS is configured in UE
                   then, request for UE based POS includes all GANSS enabled
                   on UE and vice-verca.
                   
                   This version of UE does not support separate UE based GPS 
                   and GANSS POS methods. 
                   
                   Refer to section 8.6.7.19.1 of 3GPP specification  25.331 
                   for details.

                   Step A: Ascertain, POS REQ includes all UE suppored GANSS
                   and each UE GANSS has requisite capabilities.
                */

                /* Exception to meet requirements from business management
                   to support GPS request even in GPS + GLO configuration.
                */
                if(!lc_methods && (GNSS_GLO_PMTHD_BIT == mthd_bit))  continue;
                if(!(lc_methods & mthd_bit)) {
                        DBG_L1("GANSS not mentioned in POS REQ --> Rejected");
                        return false;
                }

                if(!is_ue_based(ganss_caps->lcs_caps[i].caps_bits)) {
                        DBG_L1("No UEB CAP for GANSS  POS REQ  --> Rejected");
                        break;
                }

                lc_methods &= ~mthd_bit;
        }

        /* Step B: UE supports all requested GANSS? If not, return false   */
        DBG_L3("POS REQ, assessment pend for GANSS(s) bit 0x%x", lc_methods);

        return lc_methods == 0? true: false;
}

bool is_ue_standalone(unsigned int method_types)
{
        return method_types & UE_LCS_CAP_GNSS_AUTO ? true : false;
}

int Remote_Client :: cfg_loc_instruct(const struct loc_instruct& req,
                                      unsigned int gps_caps_bits,
                                      struct ganss_ue_lcs_caps *ganss_caps)
{
        CPP_FTRACE2();

        unsigned short lc_methods = req.lc_methods;
        unsigned int   type_flags = req.type_flags;

        DBG_L3("Instruct 0x%x (methods), 0x%x (types)", lc_methods, type_flags);
        if(type_flags & (type_flags - 1)) {
                ERR_NM("Has than one LOC types (UEA, UEB, AUTO); returning...");
                return -5;
        }

        if(true == is_ue_standalone(type_flags)) {
                type_flags = UE_LCS_CAP_GNSS_AUTO;
                DBG_L1("LOC REQ: positioning methods --> STAND ALONE");
        }


	unsigned short mthd_bit = lc_methods & GNSS_GPS_PMTHD_BIT;
	if((true == is_ue_assisted(type_flags)) && 
	   (mthd_bit)   && (type_flags & ~gps_caps_bits)) {
		DBG_L1("MSR REQ: Needed GPS capabilities not available");
		DBG_L1("%u (APP) vs %u (UE)", type_flags, gps_caps_bits);
		return -2;
	}

	if((true == is_ue_based(type_flags)) && 
	   ((!mthd_bit) || (type_flags & ~gps_caps_bits))) {
	   	DBG_L1("POS REQ: REJ, No GPS %s", mthd_bit? "in REQ" : "in UE");
	   	DBG_L1("Bits: %u (APP) vs %u (UE)", type_flags, gps_caps_bits);
	   	return -3;
	}

	lc_methods &= ~mthd_bit; /* Clear off GPS Method Bit from local copy */

        /* Not standalone: consistency check for UE-Based Request for GANSS */
        if(is_ue_based(type_flags) &&
           !assess_ganss4ueb(type_flags, lc_methods, ganss_caps)) {
                DBG_L1("POS REQ: GANSS for UEB NOT supported");
                return -3; 
        }
                        
        /* Not standalone: consistency check for UE-Assisted Request for GANSS */
        if(is_ue_assisted(type_flags) &&
           !assess_ganss4uea(type_flags, lc_methods, ganss_caps)) {
                DBG_L1("MSR REQ: GANSS for UEA NOT supported"); 
                return -4;
        }
        
        /* : Add support for velocity */
        
        /* All went well, save information into object members */
        method_types          = type_flags; /* Corrected for standalone */
        methods_gnss          = req.lc_methods;
        
        qop_desired.acc_sel   = req.qop_intent.acc_sel;
        qop_desired.h_acc_M   = req.qop_intent.h_acc_M;
        qop_desired.v_acc_M   = req.qop_intent.v_acc_M;
        qop_desired.max_age   = req.qop_intent.max_age;
        qop_desired.rsp_secs  = req.qop_intent.rsp_secs;

        return 0;
}

int Remote_Client :: set_loc_instruct(const struct loc_instruct& req)
{
        CPP_FTRACE2();

        /* Allocate GANSS Caps */
        struct ganss_ue_lcs_caps *ganss_caps = create_ganss_ue_lcs_caps();
        if(!ganss_caps) {
                ERR_NM("Memory  failed for ganss_ue_lcs_caps");
                ERR_NM("LOC REQ rejected");
                return -1;
        }

        unsigned int gps_caps_bits = 0;
        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        if(connect->get_ue_loc_caps(gps_caps_bits, ganss_caps)) {
                ERR_NM("Failed to fetch UE CAPS; LOC REQ rejected"); 
                delete_ganss_ue_lcs_caps(ganss_caps);
                return -1;
        }

	int ret_val = cfg_loc_instruct(req, gps_caps_bits, ganss_caps);
        if(0 == ret_val) {
                Agnss_Connect *connect = Agnss_Connect :: get_instance();
                connect->decl_intended_qop(this, req.qop_intent);
                rsp_config.mk_cfg_valid();
                DBG_L1("Qop config valid? : %d", rsp_config.is_cfg_valid());
        }

        /* Free GANSS Caps */
        delete_ganss_ue_lcs_caps(ganss_caps);

        return ret_val;
}

int Remote_Client :: set_rpt_criteria(const struct rpt_criteria& rpt)
{
        CPP_FTRACE2();

        /* Save a local copy */
        rsp_config.set_interval(rpt.interval);
        rsp_config.mk_cfg_valid();

        Agnss_Connect *connect = Agnss_Connect :: get_instance();
        connect->set_loc_interval(this, rpt.interval);
        return 0;
}

unsigned int Remote_Client :: get_loc_interval(void)
{
        return rsp_config.get_interval();
}
