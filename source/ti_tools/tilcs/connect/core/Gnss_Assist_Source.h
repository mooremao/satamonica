
#ifndef __GNSS_ASSIST_SOURCE_H__
#define __GNSS_ASSIST_SOURCE_H__

#include "gnss_lcs_common.h"
#include "Assist_Source.h"
#include "ti_lc_assist_adapter.h"

class Gnss_Assist_Source : public Assist_Source {

 private:
        gnss_adapter_load_fn             adap_ld;

        struct ti_lc_assist_adap_cb      adap_cb;
        struct ti_lc_assist_adap_if      adap_if;
	unsigned int                     ue_usr;

 protected:

        int ue_service_ctl(enum ue_service_ind ind,
                           unsigned int     usr_id);

        int ue_need_assist(const struct gps_assist_req& gps_assist, 
                           const struct ganss_assist_req& ganss_assist, 
                           unsigned int                   usr_id);

 public: 

        /* Constructor */
        Gnss_Assist_Source(const char *name, unsigned int a_flags, 
			   gnss_adapter_load_fn load_fn);

        /*----------------------------------------------------------------------
         * Realization of ABC cirtual methods to be excercised by A-GNSS Connect
         *--------------------------------------------------------------------*/
        int ue_decoded_aid(enum loc_assist assist, const void *assist_array,
                           int num);
		bool time_decoded ;

        /*----------------------------------------------------------------------
         * Other methods
         *--------------------------------------------------------------------*/

        int run_entity(const char* thr_name);
        int await_exit();
        int hlt_entity();

        int out_of_service();  /* Node un-available for use */
        int set_in_service();  /* Node is available for use */ 

        int ue_add_assist(const struct nw_assist_id& id, 
                          const void *assist_array, int num);

        int ue_del_aiding(enum loc_assist assist, unsigned int sv_id_map, 
                          unsigned int mem_flags);

        int ue_get_aiding(enum loc_assist assist, void *assist_array, int num);
	int assist2ue_done(const struct assist_reference& assist_usr);
        int decl_dev_param(enum ue_dev_param_id   id, 
                           void *param_desc, int num);
};

#endif

