
#include "Agnss_Connect.h"
#include "Gnss_Utils.h"
#include <pthread.h>
#include <semaphore.h>
#include <queue>
#include "debug.h"
#include "Ftrace.h"
#include "Connect_Config.h"
#include "a-gnss_xact.h"
#ifndef ANDROID
#include <cstring>
#endif
#include "ti_log.h"
#include "log_id.h"
#include "cmcc_definitions.h"
extern "C" {
	#include "os_services.h"
}
static const int AWAIT4LOC_REQ_SECS = 15;
static const int LOC_END2SLEEP_SECS = 40;
static const int AWAIT4MEAS_REP_SECS = 10;
static const int AWAIT4MEAS_REP_AFTER_RECOVERY = 5;

/******************************************************************************
 * A-GNSS CONNECT EVENT MANAGER IMPLEMENTATION
 *****************************************************************************/
enum cev_id {
        e_cev_unassigned,
        e_cev_chk_ue4aid,
        e_cev_wt4loc_req,
        e_cev_get_aid4ue,  // Includes data for AID request
        e_cev_dparam2src,
        e_cev_msr_filter_gps_eph,
/*add for GNSS recovery ++*/
        e_cev_err_recovery,
        e_cev_test_err ,// error trigger test timer code, to be removed 
/*add for GNSS recovery --*/
        e_cev_assist_ctl,
        e_cev_wt4meas_rep
};

struct cev_param {

        enum cev_id   evt_id;
        void         *param1;
        void         *param2;
        void         *param3;
        void         *param4;

        cev_param(): evt_id(e_cev_unassigned),   param1(NULL), 
                     param2(NULL), param3(NULL), param4(NULL)
	{ }
	
        cev_param(const struct cev_param& from);
        //struct cev_param(const struct cev_param& from);
        struct cev_param& operator =(const struct cev_param& from);                
};

cev_param :: cev_param(const struct cev_param& from)
{
        this->evt_id = from.evt_id;
        this->param1 = from.param1;
        this->param2 = from.param2;
        this->param3 = from.param3;
        this->param4 = from.param4;
}

struct cev_param& cev_param :: operator=(const struct cev_param& from)
{
        if (this == &from) return *this;

        this->evt_id = from.evt_id;
        this->param1 = from.param1;
        this->param2 = from.param2;
        this->param3 = from.param3;
        this->param4 = from.param4;

        return (*this);
} 

class Cev_Manager : public Base_Entity {

private:

        sem_t                     sem; 
        pthread_mutex_t           mtx;

        void                     *chk_ue4aid_timer;
        void                     *wt4loc_req_timer;
        void                     *wt4meas_rep_timer;

        queue<struct cev_param>  ev_queue;
        bool                     queue_proc;

        void do_proc(const struct cev_param& ev_param);

        inline int mtx_take() { return pthread_mutex_lock(&mtx);   }
        inline int mtx_give() { return pthread_mutex_unlock(&mtx); }


public:

	Cev_Manager();
        ~Cev_Manager();

	int run_entity(const char*thr_name);
	int await_exit(void);
	int out_of_service();
	int set_in_service();

        void ev_push(const struct cev_param& ev_param);

        void process(void);

        int  setup_timer2chk_ue4aid(unsigned int secs);
        int  erase_timer2chk_ue4aid(void);
        int  setup_timer2wt4loc_req(unsigned int secs);
        int  setup_timer2chk4Meas_rep(unsigned int secs);

		
		int  erase_timer2chk4Meas_rep(void);
        int  erase_timer2wt4loc_req(void);

        void proc_ev_chk_ue4aid(bool timer_evt) {
                mtx_take();
                if(timer_evt) chk_ue4aid_timer = NULL; /* Timer expired */
                mtx_give();

		
                /* Using friendship to invoke services of Agnss_Connect */
                Agnss_Connect *connect = Agnss_Connect :: get_instance();
                connect->fr_eval2req_assist(); 
        }
		/* add for GNSS recovery ++*/
		// error trigger test code, to be removed
		void proc_ev_test_err_recovery(bool timer_evt) {
									
				Agnss_Connect *connect = Agnss_Connect :: get_instance();
				/* 1 -> fatal error
				   2 -> exception error
				*/
				connect->fr_trigger_test_err(1);
				}
		/* add for GNSS recovery --*/		

        void proc_ev_get_aid4ue(Remote_Client *client, 
                                struct gps_assist_req *gps_assist,
                                struct ganss_assist_req *ganss_assist,
                                unsigned short uea_flags) {
                /* Using friendship to invoke services of Agnss_Connect */
                Agnss_Connect *connect = Agnss_Connect :: get_instance();
                connect->fr_ue_need_assist(client,       *gps_assist, 
                                           *ganss_assist, uea_flags);   
        }

        void proc_ev_wt4loc_req(void) {
                mtx_take();
                wt4loc_req_timer = NULL; /* Timer expired */
                mtx_give();

                /* Using friedship to invoke services of Agnss_Connect */
                Agnss_Connect *connect = Agnss_Connect :: get_instance();
                connect->fr_put_ue2sleep();
        }

		void proc_ev_wt4meas_rep(void) {
			enum dev_err_id err_id;
			  mtx_take();
			  wt4meas_rep_timer = NULL; /* Timer expired */
			  mtx_give();



		  /* Using friedship to invoke services of Agnss_Connect */
		  Agnss_Connect *connect = Agnss_Connect :: get_instance();
		  
		
		 // if(connect->gnss_meas_rep == false)
		  	{
			 
			  if(connect->ue_recovery_count < 2 ){
			  	  mtx_take();  
			  	  connect->ue_recovery_count++;
				  mtx_give();
					  connect->fr_ue_err_recovery(gnss_no_meas_rep_err);
				  connect->ue_recovery_ind(gnss_no_meas_resp_ind,1 );
					
			  }else if(connect->ue_recovery_count < 3 ){
				    mtx_take();  
			  	  connect->ue_recovery_count++;
				  mtx_give();
				  connect->fr_ue_err_recovery(gnss_no_resp_err );
     			  connect->ue_recovery_ind(gnss_no_reset_resp_ind,1 );

			  }  else {
				  connect->ue_recovery_ind(gnss_hard_reset_ind,1);
				
				connect->fr_ue_err_recovery(gnss_hard_reset_err);
				mtx_take();  
				connect->ue_recovery_count = 0 ; // Reset the counter here
				mtx_give();
				
			  }
			
			}
		 

		mtx_take();  
		connect->gnss_meas_rep= false;
		mtx_give();

	//  setup_timer2chk4Meas_rep(AWAIT4MEAS_REP_AFTER_RECOVERY);

		
			
				
	  }
        void proc_ev_msr_filter_gps_eph(const struct gps_msr_result&  gps_result,
                                        const struct gps_msr_info    *gps_msr)
        {
                Agnss_Connect *connect = Agnss_Connect :: get_instance();
                connect->fr_msr_filter_gps_eph(gps_result, gps_msr);
                return;
        }

        void proc_ev_dparam2src(Assist_Source *source, enum ue_dev_param_id id,
                                void *params, int num)
        {
		Agnss_Connect *connect = Agnss_Connect :: get_instance();
                connect->fr_dev_params2src(source, id, params, num);
                return;
        }
		/* add for GNSS recovery ++*/
		void proc_ev_handle_ue_err(dev_err_id err_id)
		{
				Agnss_Connect *connect = Agnss_Connect :: get_instance();
				connect->ue_recovery_ind(gnss_fw_fatal_ind ,1);
				connect->fr_ue_err_recovery(err_id);
				return;
		}
		/* add for GNSS recovery --*/

        void proc_ev_assist_ctl(enum ue_service_ind ind, Remote_Client *client) 
        {
                Agnss_Connect *connect = Agnss_Connect :: get_instance();
                connect->fr_assist_control(ind,  client);
                return;
        }

};

Cev_Manager :: Cev_Manager()
        : Base_Entity("Cev Mgr"), chk_ue4aid_timer(NULL), wt4loc_req_timer(NULL),
          ev_queue(), queue_proc(false),wt4meas_rep_timer(NULL)
          
{
        //CPP_FTRACE1();

        pthread_mutex_init(&mtx, NULL);
        sem_init(&sem, 0, 0);

		os_timer_module_init();
}

Cev_Manager :: ~Cev_Manager()
{
        pthread_mutex_destroy(&mtx);

		os_timer_module_exit();
        /* Destroy semaphore as well */
}

static Cev_Manager cev_mgr; /* single object in this file */

static void *cev_mgr_thread(void *arg)
{
        CPP_FTRACE1();

        Cev_Manager *mgr = (Cev_Manager*) arg;

        ftrace_add_fthread(pthread_self(), mgr->get_name());

        mgr->process();

	return NULL;
}

void Cev_Manager :: ev_push(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        mtx_take();

        ev_queue.push(ev_param);
        if((1 == ev_queue.size()) && (false == queue_proc)) {
                /* First element in Queue, trigger processing */
                DBG_L3("This is first element in Queue");
                queue_proc = true;
                sem_post(&sem);
        }
        
        mtx_give();

        return;
}

static int sched_eval_ue4aiding(bool due2timer = false); // forward declaration
static int sched_ue_err_recovery(enum dev_err_id err_id);/* add for GNSS recovery ++*/

static void cev_timer_cb(void *cb_param)
{
        CPP_FTRACE1();

        struct cev_param ev_param;
        ev_param.evt_id = *(enum cev_id*) &cb_param;

	DBG_L1("Timer Call Back invoked for event %u", ev_param.evt_id);

        switch(ev_param.evt_id) {

        case e_cev_chk_ue4aid:
                sched_eval_ue4aiding(true);
                break;

        case e_cev_wt4loc_req:
                cev_mgr.ev_push(ev_param);
                break;
/* add for GNSS recovery ++*/
		case e_cev_test_err:
				cev_mgr.ev_push(ev_param);
				break;
/* add for GNSS recovery --*/
		case e_cev_wt4meas_rep :
				cev_mgr.ev_push(ev_param);
			break;
        default:
                break;
        }

        return;
}

static void* _setup_1shot_timer(unsigned int secs, void *now_timer, 
                                enum cev_id evt_id)
{
        CPP_FTRACE2();

        /* Following implementation can be very well be optimized. But
           existing flow enables the debug of critical timer feature.
        */

        if(NULL != now_timer) {
                DBG_L1("Timer is already active, ignoring new request");
                return now_timer;
        }

        now_timer =  os_timer_1shot(secs, cev_timer_cb, (void*)evt_id);
        if(NULL == now_timer) {
                ERR_NM("Failed to create timer for evt id %d", evt_id);
                return now_timer;
        }

        DBG_L1("Timer started for evt %u, timeout: %us", evt_id, secs);

        return now_timer;
}

int Cev_Manager :: setup_timer2chk_ue4aid(unsigned int secs)
{
        CPP_FTRACE1();
        DBG_L1("Scheduling timer to check for UE Aid ...");

        mtx_take();
        chk_ue4aid_timer = _setup_1shot_timer(secs, chk_ue4aid_timer,
                                              e_cev_chk_ue4aid);
        mtx_give();

        return chk_ue4aid_timer ? 0 : -1;
}

int Cev_Manager :: setup_timer2wt4loc_req(unsigned int secs)
{
        CPP_FTRACE1();

        DBG_L1("Scheduling timer to wait for loc req  ...");

        mtx_take();
        wt4loc_req_timer = _setup_1shot_timer(secs, wt4loc_req_timer,
                                              e_cev_wt4loc_req);
        mtx_give();

        return wt4loc_req_timer ? 0 : -1;
}

int Cev_Manager :: setup_timer2chk4Meas_rep(unsigned int secs)
{
        CPP_FTRACE1();

        DBG_L1("Scheduling timer to check for measurement reports  ...");

        mtx_take();
        wt4meas_rep_timer = _setup_1shot_timer(secs, wt4meas_rep_timer,
                                              e_cev_wt4meas_rep);
        mtx_give();

        return wt4meas_rep_timer ? 0 : -1;
}
static void  _erase_timer(void *now_timer)
{
        CPP_FTRACE2();

        if(NULL != now_timer) {
                DBG_L3("Erasing a running timer ...");
                os_timer_stop(now_timer);
        }

        return;
}
int Cev_Manager :: erase_timer2chk4Meas_rep(void)
{
        CPP_FTRACE1();

        mtx_take();
        _erase_timer(wt4meas_rep_timer);
        wt4meas_rep_timer = NULL;
        mtx_give();
        
        return 0;
}

int Cev_Manager :: erase_timer2chk_ue4aid(void)
{
        CPP_FTRACE1();

        mtx_take();

        _erase_timer(chk_ue4aid_timer);
        chk_ue4aid_timer = NULL;
        mtx_give();
        
        return 0;
}

int Cev_Manager :: erase_timer2wt4loc_req(void)
{
        CPP_FTRACE1();

        mtx_take();
        _erase_timer(wt4loc_req_timer);
        wt4loc_req_timer = NULL;
        mtx_give();
        
        return 0;
}

static int sched_ue_need_assist(Remote_Client             *client,
                         struct gps_assist_req     *gps_assist,
                         struct ganss_assist_req   *ganss_assist,
                         unsigned short             uea_types)
{
        CPP_FTRACE1();

        struct cev_param ev_param;

        ev_param.evt_id = e_cev_get_aid4ue;
        ev_param.param1 = (void*) client;
        ev_param.param2 = (void*) gps_assist;
        ev_param.param3 = (void*) ganss_assist;
        ev_param.param4 = (void*) uea_types;

        cev_mgr.ev_push(ev_param);

        return 0;
}

static int sched_eval_ue4aiding(bool due2timer)
{
        CPP_FTRACE1();

        struct cev_param ev_param;
        
        ev_param.evt_id = e_cev_chk_ue4aid;
        ev_param.param1 = (false == due2timer)? NULL : (void*) 1;
        ev_param.param2 = NULL;
        ev_param.param3 = NULL;
        ev_param.param4 = NULL;

        cev_mgr.ev_push(ev_param);

        return 0;
}
/* add for GNSS recovery ++*/
static int sched_ue_err_recovery(enum dev_err_id err_id)
{
        CPP_FTRACE1();
 		
        struct cev_param ev_param;
        ev_param.evt_id = e_cev_err_recovery;
        ev_param.param1 = (void*)err_id;
        ev_param.param2 = NULL;
        ev_param.param3 = NULL;
        ev_param.param4 = NULL;
        cev_mgr.ev_push(ev_param);
        return 0;
}
/* add for GNSS recovery --*/

static int sched_dev_params2src(Assist_Source *source, enum ue_dev_param_id id,
                                void *params, int num = 1)
{
        CPP_FTRACE1();

        struct cev_param ev_param;

        ev_param.evt_id = e_cev_dparam2src;
        ev_param.param1 = source;
        ev_param.param2 = (void *)  id;
        ev_param.param3 = params; 
        ev_param.param4 = (void *) num; 

        cev_mgr.ev_push(ev_param);
       
        return 0;
}

static int sched_msr_filter_gps_eph(const struct gps_msr_result& gps_result,
                                     const struct gps_msr_info   *gps_msr)
{
        CPP_FTRACE1();

        unsigned n_sat = gps_result.n_sat;
        struct gps_msr_result *new_result = new struct gps_msr_result;
        struct gps_msr_info   *new_msr    = new struct gps_msr_info[n_sat];
        if(!new_result || !new_msr) {
                if(new_result) delete new_result;
                if(new_msr)    delete[] new_msr;
                ERR_NM("Abort: No new mem for gps_msr_result &/or gps_msr_info"); 
                return 0;
        }

        memcpy(new_result, &gps_result, sizeof(struct gps_msr_result));
        memcpy(new_msr,    gps_msr,  n_sat * sizeof(struct gps_msr_info));
		
        struct cev_param ev_param;
        ev_param.evt_id = e_cev_msr_filter_gps_eph;
        ev_param.param1 = new_result;
        ev_param.param2 = new_msr;
        ev_param.param3 = NULL;
        ev_param.param4 = NULL;

        cev_mgr.ev_push(ev_param);

        return 0;
}

static int sched_assist_control(enum ue_service_ind ind, Remote_Client *client)
{
        CPP_FTRACE1();

        struct cev_param ev_param;
        ev_param.evt_id = e_cev_assist_ctl;
        ev_param.param1 = (void*) ind;
        ev_param.param2 = client;
        ev_param.param3 = NULL;
        ev_param.param4 = NULL;

        cev_mgr.ev_push(ev_param);

        return 0;
}

static void _proc_ev_chk_ue4aid(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        bool timer_evt = (NULL == ev_param.param1)? false : true;

        cev_mgr.proc_ev_chk_ue4aid(timer_evt);
}

static void _proc_ev_wt4loc_req(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        cev_mgr.proc_ev_wt4loc_req();
}

static void _proc_ev_wt4meas_rep(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        cev_mgr.proc_ev_wt4meas_rep();
}
static void _proc_ev_get_aid4ue(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        struct gps_assist_req     *gps_assist;
        struct ganss_assist_req *ganss_assist;

        Remote_Client *client    = (Remote_Client*) ev_param.param1;
        gps_assist               = (struct gps_assist_req*)ev_param.param2;
        ganss_assist             = (struct ganss_assist_req*)ev_param.param3;
        unsigned short uea_types = *(unsigned short*)&ev_param.param4;

        cev_mgr.proc_ev_get_aid4ue(client,      gps_assist, 
                                   ganss_assist, uea_types);

        delete gps_assist;
        delete ganss_assist;
}
/* add for GNSS recovery ++*/
static void _proc_ev_handle_ue_err(const struct cev_param& ev_param)
{
		CPP_FTRACE1();

		enum dev_err_id err_id = *(enum dev_err_id*)&ev_param.param1;
	
		cev_mgr.proc_ev_handle_ue_err(err_id);

}

// error trigger test code, to be removed
static void _proc_ev_test_err(const struct cev_param& ev_param)
{
		CPP_FTRACE1();

		enum dev_err_id err_id = *(enum dev_err_id*)&ev_param.param1;
	
		cev_mgr.proc_ev_test_err_recovery(err_id);

}
/* add for GNSS recovery --*/

static void _proc_ev_dparam2src(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        Assist_Source *source   =  (Assist_Source*) ev_param.param1;
        enum ue_dev_param_id id =  *(enum ue_dev_param_id*) &ev_param.param2;
        void *param             =  ev_param.param3;
        int num                 = *(int*) &ev_param.param4;

        cev_mgr.proc_ev_dparam2src(source, id, param, num);

        delete param;

        return;
}

void _proc_ev_msr_filter_gps_eph(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        struct gps_msr_result *gps_result = 
                (struct gps_msr_result*) ev_param.param1;
        struct gps_msr_info   *gps_msr    = 
                (struct gps_msr_info*) ev_param.param2;

        cev_mgr.proc_ev_msr_filter_gps_eph(*gps_result, gps_msr);
  
        delete   gps_result;
        delete[] gps_msr;
}

static void _proc_ev_assist_ctl(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        enum ue_service_ind ind = *(enum ue_service_ind*)&ev_param.param1;
        Remote_Client   *client =  (Remote_Client*)       ev_param.param2;

        cev_mgr.proc_ev_assist_ctl(ind, client);

        return;
}

#if  0
void Cev_Manager :: do_proc(const struct cev_param& ev_param)
{
        CPP_FTRACE1();

        switch(ev_param.evt_id) {

        case e_cev_chk_ue4aid:
                _proc_ev_chk_ue4aid(ev_param); /* Utility function */
                break;

        case e_cev_wt4loc_req:
                _proc_ev_wt4loc_req(ev_param); /* Utility function */
                break;

        case e_cev_get_aid4ue:
                _proc_ev_get_aid4ue(ev_param); /* Utility function */
                break; 

        case e_cev_dparam2src:
                _proc_ev_dparam2src(ev_param); /* Utility function */
                break; 
                
        case e_cev_msr_filter_gps_eph:
                _proc_ev_msr_filter_gps_eph(ev_param);
                break;

        default:
                break;
        }

        return;
}
#endif

struct cev_evt_desc {
	enum  cev_id evt_id;
        void (*proc)(const struct cev_param& ev_param);
};

static struct cev_evt_desc cev_evt_tbl[] = {

	{e_cev_chk_ue4aid ,             _proc_ev_chk_ue4aid},
	{e_cev_wt4loc_req ,             _proc_ev_wt4loc_req},
	{e_cev_get_aid4ue ,             _proc_ev_get_aid4ue},
	{e_cev_dparam2src ,             _proc_ev_dparam2src},
	{e_cev_msr_filter_gps_eph ,     _proc_ev_msr_filter_gps_eph},
	{e_cev_assist_ctl ,             _proc_ev_assist_ctl},
/* add for GNSS recovery ++*/
	{e_cev_err_recovery ,			_proc_ev_handle_ue_err},
	{e_cev_test_err ,				_proc_ev_test_err},// error trigger test code, to be removed	
/* add for GNSS recovery --*/	
	{e_cev_wt4meas_rep,				_proc_ev_wt4meas_rep},
	{e_cev_unassigned ,             NULL},
};

void Cev_Manager :: do_proc(const struct cev_param& ev_param)
{
	CPP_FTRACE1();

	for(struct cev_evt_desc *evt_desc = cev_evt_tbl;
            NULL != evt_desc->proc; evt_desc++)         {

                if(ev_param.evt_id == evt_desc->evt_id) {
                        evt_desc->proc(ev_param);
                        break;
                }
        }

        return;
}

void Cev_Manager :: process()
{
        DBG_L1("CEV MGR thread launched and now awaits events from Connect"); 

        while(1) {
                mtx_take();

                if(0 == ev_queue.size()) {
                        queue_proc = false;
                        mtx_give();

                        sem_wait(&sem);

                        mtx_take();
                }
                
                /* Assumes that there is atleast one element in Queue */
                struct cev_param ev_param = ev_queue.front();
                ev_queue.pop();

                mtx_give();

                DBG_L1("Processing ev_id %u; Pending events in Q are %u", \
                       ev_param.evt_id, ev_queue.size());

                do_proc(ev_param);
        }

        return;
}

int Cev_Manager :: run_entity(const char *thr_name)
{
        return Base_Entity :: work_wthread(cev_mgr_thread, &cev_mgr, thr_name);
}

int Cev_Manager :: await_exit()
{
	return Base_Entity :: wait_wthread();
}

int Cev_Manager :: out_of_service()
{
	return 0;
}

int Cev_Manager :: set_in_service()
{
	return 0;
}

/******************************************************************************
 * A-GNSS CONNECT IMPLEMENTATION
 *****************************************************************************/

Agnss_Connect* Agnss_Connect :: connect  = NULL;
Gnss_Receiver* Agnss_Connect :: receiver = NULL;

Agnss_Connect :: Agnss_Connect()
        : usr_client_list(), assist_src_list(), assist4uea_gnss_wmap(0),
          assist4ueb_wait_flag(false), assist4ueb_ref_count(0),
          cfg_req_multiple_src(false), gnss_meas_rep(true),ue_recovery_count(0)
{
        pthread_mutex_init(&connect_mutex, NULL);
}

Agnss_Connect* Agnss_Connect :: create_instance() 
{
	CPP_FTRACE1();
#define AGNSS_THR_NAME	"A-GNSS_ADAPTER"
        if(!connect) {
                connect = new Agnss_Connect;
                if(NULL != connect)
                        cev_mgr.run_entity(AGNSS_THR_NAME);
        }
        
        return connect;
}


/*------------------------------------------------------------------------------
 *  Private Methods.
 *----------------------------------------------------------------------------*/
int Agnss_Connect :: wakeup_ue2wait4loc_req(Gnss_Receiver *receiver)
{
        CPP_FTRACE1();

        if(false == usr_client_list.empty()) {
                DBG_L3("%u LOC REQs active; returning", usr_client_list.size());
                return 0;
        }
        /* : invoke this API from other public API */
        cev_mgr.erase_timer2wt4loc_req(); /* If active to put device to sleep */

        receiver->ue_wake2idle();         /* Wake device, if not already done */

        /* Give a clean awake time to device to wait for LOC REQ from client */
        cev_mgr.setup_timer2wt4loc_req(AWAIT4LOC_REQ_SECS); 
        return 0;
}

int Agnss_Connect :: _do_loc_start(bool due2loc_req)
{
        CPP_FTRACE1();

        int ret_val = -1;

        /* Description of due2loc_req = false: 
           If there is no client that has asked for LOC REQ as yet, 
           then, we need to head start the receiver to ensure that 
           device works to showcase mandated 3GPP perfomance data.
        */

        /* Client request: stop timer, if it is still running */
        if(true == due2loc_req) cev_mgr.erase_timer2wt4loc_req(); 

        if(true == receiver->is_started()) {
                DBG_L3("Device is already ON; no need to re-start, returning");
                ret_val = 0; goto do_loc_start_end;
        }

        if((false == due2loc_req) && (0 != receiver->set_loc_interval(1))) {
                ERR_NM("Failed: to set configuration of device interval");
                goto do_loc_start_end;
        }

        ret_val = receiver->loc_start();
        if(0 != ret_val) {
                ERR_NM("Failed: to activate or start the device");
                goto do_loc_start_end;
        }

        if(false == due2loc_req) {
                DBG_L1("Device started and timer scheduled to await LOC REQ");
                cev_mgr.setup_timer2wt4loc_req(AWAIT4LOC_REQ_SECS);
        }

        do_state_tcxo_info(); /* All's well: send out TCXO info to users */
	//	cev_mgr.setup_timer2chk4Meas_rep(AWAIT4MEAS_REP_SECS);

 do_loc_start_end:

        return ret_val;
}

int Agnss_Connect :: 
do_ue_add_assist(const Assist_Source *source, const struct nw_assist_id& id,
                 const void *assist_array, int num)
{
		CPP_FTRACE1();
        int ret_val = -1;
        bool found  = false;
        list<Assist_Source*> :: iterator itr;
        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                if(*itr == source) {
                        found = true;
                        break; 
                }
        }
        if(false == found) {
                ERR_NM("Failed to find source %s returning", source->get_name());
                goto do_ue_add_assist_exit;
        }

        //ret_val = head_start_ue_b4loc_req(receiver);
        ret_val = _do_loc_start(false);
        if(0 != ret_val) {
                ERR_NM("Failed to head start UE, returning ..");
                goto do_ue_add_assist_exit;
        }
        
        ret_val = receiver->ue_add_assist(id, assist_array, num);
        if(0 != ret_val) {
                ERR_NM("Failed to add assist ID (NW) %u.", id);
                goto do_ue_add_assist_exit;
        }

 do_ue_add_assist_exit:

        return ret_val;
}

int Agnss_Connect :: 
do_ue_del_aiding(const Assist_Source *source, enum loc_assist assist, 
                 unsigned int sv_id_map, unsigned int mem_flags)
{
		CPP_FTRACE1();

		wakeup_ue2wait4loc_req(this->receiver);

        list<Assist_Source*> :: iterator itr;

        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                if(*itr == source)
                        return receiver->ue_del_aiding(assist, sv_id_map,
                                                       mem_flags);
        }

        return -1;
}

int Agnss_Connect :: 
do_ue_get_aiding(const Assist_Source *source, enum loc_assist assist,
                 void *assist_array, int num)
{
		CPP_FTRACE1();
        list<Assist_Source*> :: iterator itr;

        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                if(*itr == source)
                        return receiver->ue_get_aiding(assist, assist_array, 
                                                       num);
        }

        return -1;
}

Assist_Source* Agnss_Connect :: _do_find_assist_src(const char *name)
{
        CPP_FTRACE1();

        list<Assist_Source*> :: iterator itr;

        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                Assist_Source *source = *itr;
                
                if(0 == strcmp(source->get_name(), name))
                        return source;
        }
        
        return NULL;
}

Remote_Client* Agnss_Connect :: _do_find_usr_client(const char *name)
{
        CPP_FTRACE2();

        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                Remote_Client *client = *itr;
                
                if(0 == strcmp(client->get_name(), name))
                        return client;
        }

        return NULL;
}

int Agnss_Connect :: 
_do_mk_assist_req(Assist_Source& source, const struct gps_assist_req& gps_assist,
                  const struct ganss_assist_req& ganss_assist,
                  unsigned short uea_types, unsigned int usr_id)
{
        int ret_val = 0;

        if(0 != source.ue_need_assist_req(gps_assist, ganss_assist,
                                          uea_types,       usr_id)) {

                ERR_NM("Failed to send assist REQ to %s", source.get_name());
                ret_val = -1;
                goto _do_mk_assist_req_exit1;
        }

        DBG_L1("Sent REQ for Assist to source %s..", source.get_name());

        if(0 == uea_types) {
                DBG_L3("Awaits UEB assist for all configured GNSS(s) ");
                assist4ueb_wait_flag  = true; assist4ueb_ref_count++;
        } else {
                DBG_L3("Awaits UEA assist (GNSS method) %u", uea_types);
                assist4uea_gnss_wmap |= uea_types;
        }

 _do_mk_assist_req_exit1:
        return ret_val;
}

bool
is_source_apt4req(Remote_Client *client, const struct gps_assist_req& gps_assist,
                  const struct ganss_assist_req& ganss_assist,
                  Assist_Source& source)
{
        CPP_FTRACE1();

        bool ret_val = false;

        /* The param 'client' refers to the LOC requestor. A non NULL
           value of 'client' indicates that needed assist must be got
           from the same LOC requestor. A value of NULL suggests that
           assist can be fetched from any capable source of assist.
        */

        if(client) {
                /* Is this assist source component of LOC requestor? */
                if(0 == strcmp(source.get_name(), client->get_name()))
                        ret_val = true;                       /* Yes */
        } else {
                /* Can this source be solicited for required assist? */
                if(true ==
                   source.can_solicit_assist(gps_assist, ganss_assist))
                        ret_val = true;                        /* Yes */
        }

 is_source_apt4req_exit1:

        return ret_val;
}

int Agnss_Connect :: 
do_ue_need_assist(Remote_Client *client, const struct gps_assist_req& gps_assist,
                  const struct ganss_assist_req& ganss_assist, 
                  unsigned short uea_types)
{
        CPP_FTRACE1();

        list<Assist_Source*> :: iterator itr;
        Assist_Source *source = NULL;
        vector<Assist_Source*>  sources;

        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                source = *itr;          /* Reference to source of assistance */
                
                if(true == is_source_apt4req(client,    gps_assist,
                                             ganss_assist, *source)) {

                        DBG_L3("Found source %s for REQ: %s", source->get_name(),
                               (NULL != client)? client->get_name(): "UE");

			sources.push_back(source); /* Put source into array */

                        if(false == cfg_req_multiple_src) 
                                break;  /* Looking for one source of assist */
                }
        }

        /* Fix up: platform application does not double up as assist source */
        if(NULL == client) {
                client = _do_find_usr_client(TI_AGNSS_PFORM_NAME);
                DBG_L1("Getting pointer Pfm App Client: %s", client? "Y" : "N");
        }
        
        int n_reqs = 0; unsigned int usr_id = client->get_handle(); 

        for(int idx = 0; idx < sources.size(); idx++) {
                if(0 == _do_mk_assist_req(*(sources[idx]), gps_assist, ganss_assist,
                                          uea_types, usr_id))
                        n_reqs++;
        }

        DBG_L1("Sources Found: %u, Requests Sent: %d", sources.size(), n_reqs);
        return n_reqs - sources.size(); /* Identifies num of failed requests */
}

/* Was there an assist request sent while preparing for this one? */
static bool was_assist_req_amidst(bool ueb_wait_flag, unsigned short uea_gnss_wmap,
                           unsigned short req_uea_types)
{

        CPP_FTRACE1();

        bool ret_val = false;
        if((0 == req_uea_types) && (true == ueb_wait_flag)) ret_val = true;

        if(req_uea_types & uea_gnss_wmap) ret_val = true;

        DBG_L1("%s assist need was %s sent amidst processing of this req",
               req_uea_types? "UEA" : "UEB",  ret_val? "already" : "not");

        return ret_val;
}

int Agnss_Connect ::
fr_ue_need_assist(Remote_Client *client,
                  const struct gps_assist_req& gps_assist,
                  const struct ganss_assist_req& ganss_assist,
                  unsigned short uea_types)
{
        CPP_FTRACE1();

        int ret_val = 0;

        mtx_lockin();
        if(false == was_assist_req_amidst(assist4ueb_wait_flag,
                                          assist4uea_gnss_wmap, uea_types)) {

               ret_val = do_ue_need_assist(client,       gps_assist, 
                                           ganss_assist, uea_types);
        }
        mtx_unlock();
        
        return ret_val;
}

int Agnss_Connect :: 
do_ue_decoded_aid(enum loc_assist assist, const void *assist_array, int num)
{
        CPP_FTRACE1();

        list<Assist_Source*> :: iterator itr;

        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                if((*itr)->ue_decoded_aid(assist, assist_array, num))
                        return -1;
        }

        return 0;
}

int Agnss_Connect :: do_ue_nmea_report(enum nmea_sn_id nmea, const char *msg, 
                                       int len)
{
        CPP_FTRACE1();

        list<Remote_Client*> :: iterator itr;

        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                if((*itr)->ue_nmea_report(nmea, msg, len))
                        return -1;
        }

        return 0;
}

/* Move this to a util file */
static unsigned int calc_gcd(unsigned int n1, unsigned int n2)
{
        while(n1 != n2) {
         
                if(n2 > n1)
                        n2 = n2 - n1;
                else
                        n1 = n1 - n2;
        }

        return n1;
}

bool Agnss_Connect :: _is_an_usr_client(const char *name)
{
        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                if(0 == strcmp((*itr)->get_name(), name))
                        return true;
        }

        return false;
}

bool Agnss_Connect :: _is_client_an_usr(Remote_Client *client)
{
        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                if((*itr) == client)
                        return true;
        }

        return false;
}


int Agnss_Connect :: _do_set_loc_interval()
{
        CPP_FTRACE1();

        if(usr_client_list.empty()) {
                DBG_L3("Not set: as no user client is active, returning back");
                return 0;
        }

        unsigned int gcd = usr_client_list.front()->get_loc_interval();
 
        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                gcd = calc_gcd(gcd, (*itr)->get_loc_interval());
        }

        DBG_L1("Setting UE RPT interval as %u secs", gcd);
        return receiver->set_loc_interval(gcd);
}

int Agnss_Connect :: do_set_loc_interval(Remote_Client *client, 
                                         unsigned int interval)
{
        /* Can add check for presence of clients and
           remove it from _do_set_loc_interval() */
        if(false == _is_client_an_usr(client)) {
                DBG_L3("Itvl: No LOC REQ from %s, ignoring", client->get_name());
                return 0;
        }

        return _do_set_loc_interval();
}

int Agnss_Connect :: do_loc_stop(const Remote_Client *client)
{
        CPP_FTRACE1();

        list<Remote_Client*> :: iterator itr;

        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                if((*itr) == client) {
                        usr_client_list.erase(itr);
                        break;
                }
        }
        
        if(0 < usr_client_list.size()) {
                DBG_L3("Still active LOC Req, recalculating interval");
                _do_set_loc_interval();
                return 0;
        }

#if  0
        Assist_Source *source = _do_find_assist_src(client->get_name());
        if(NULL != source) source->clr_assist_req();
        
        /* : enhance Assist_Source class to manage such calls */
        _do_ue_assist_control(e_assist_need_abort);
#endif
	
        DBG_L3("All LOC Request ended, now, de-activating receiver");

        assist4ueb_ref_count = 0;
        assist4ueb_wait_flag = false;
        assist4uea_gnss_wmap = 0;
        cev_mgr.erase_timer2chk_ue4aid();
		//cev_mgr.erase_timer2chk4Meas_rep();
	
	 receiver->loc_stop(); 

        int idle2sleep_val = Connect_Config :: get_idle2sleep_secs();
        if (idle2sleep_val > 0){
       		cev_mgr.setup_timer2wt4loc_req(idle2sleep_val);
        }
        else {
                receiver->ue_put2sleep();   idle2sleep_val = 0;
        }

        DBG_L1("Device will enter to sleep state after %d secs", idle2sleep_val);

        return 0;
}

int Agnss_Connect :: _do_ue_assist_control(enum ue_service_ind ind,
                                           Remote_Client   *client)
{
        CPP_FTRACE1();

        list<Assist_Source*> :: iterator itr;
        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                Assist_Source *source = (*itr);
                DBG_L3("Sending ind %u to %s", ind, source->get_name());
                source->ue_assist_control(ind,    client->get_handle());
        }

        return 0;
}


static bool do_proc_for_ff(const struct gnss_pos_result& pos_result,
                           Gnss_Receiver* receiver)
{
        CPP_FTRACE1();

        unsigned int flags = POS_ERR_REPORTED | POS_NOT_REPORTED;
        if((pos_result.contents & flags) || (true == receiver->is_ff_done())) {
                return false;
        }

        receiver->mk_ff_done();
        
        DBG_L1("First Fix Attained by UE, chk for UE Aid status");
        
        //: do_end_assist_req(false);

        return true;
}

int 
Agnss_Connect :: _do_ue_loc_results(Remote_Client                  *client,
                                    const struct gnss_pos_result&   pos_result,
                                    const struct gps_msr_result&    gps_result,
                                    const struct gps_msr_info      *gps_msr,
                                    const struct ganss_msr_result&  ganss_result,
                                    const struct ganss_msr_info    *ganss_msr)
{
        CPP_FTRACE1();

        /* Definitely, not a recommended way */
#define LOC_ARGS pos_result, gps_result, gps_msr, ganss_result, ganss_msr

        /* Is client awaiting first response (RSP1) from GNSS device? */
        if(true == client->awaiting_rsp1()) {
                
                int time2put_rsp1 = client->time2put_rsp1();
                
                /* Waiting client: ascertain report fits intended QoP */
                if(true == client->fits_qop_need(LOC_ARGS))
                        goto send_ue_loc_reports; /* Report fit to go */
                
                DBG_L1("RPT QoP unfit, Time2RSP1 %d sec", time2put_rsp1); 
                
                /* Report unfit for client; can it wait for fit RSP1? */
                if(time2put_rsp1 > 0)
                        goto ue_loc_not_reported; /* Await better RSP1 */
                else
                        goto send_ue_loc_reports; /* RSP1 must go now */
        }
        
        DBG_L3("RSP1 done in past, follow-up LOC results for client..");
        
        /* RSP1 done, is it time to send followup response to client? */
        if(client->time2next_rsp() > 0)
                goto ue_loc_not_reported;         /* Not yet */      
        
 send_ue_loc_reports:
        DBG_L3("Sending LOC Results for %s", client->get_name());
        client->ue_loc_results(LOC_ARGS);
        client->prep4next_rsp();

 ue_loc_not_reported:
	 return 0;
}

int 
Agnss_Connect :: do_ue_loc_results(const struct gnss_pos_result&  pos_result,
                                   const struct gps_msr_result&   gps_result,
                                   const struct gps_msr_info     *gps_msr,
                                   const struct ganss_msr_result& ganss_result,
                                   const struct ganss_msr_info  *ganss_msr,
                                   bool                           ue_ff)
{
        CPP_FTRACE1();

        list<Remote_Client*> :: iterator itr;

        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {

                Remote_Client *client = *itr; 
                DBG_L2("Eval UE LOC Results for client %s", client->get_name());

                _do_ue_loc_results(client,  pos_result,   gps_result,
                                   gps_msr, ganss_result, ganss_msr);
                if(true == ue_ff) 
                        sched_assist_control(e_assist_need_ended, client);
        }

        return 0;
}

int Agnss_Connect :: do_get_ue_loc_caps(unsigned int* gps_caps_bits,
                                        struct ganss_ue_lcs_caps *ganss_caps)
{
        CPP_FTRACE1();

	return receiver->get_ue_loc_caps(*gps_caps_bits, ganss_caps);
}

int 
Agnss_Connect :: do_ue_aux_info4sv(const struct dev_gnss_sat_aux_desc& gps_desc,
                                   const struct dev_gnss_sat_aux_info *gps_info,
                                   const struct dev_gnss_sat_aux_desc& glo_desc,
                                   const struct dev_gnss_sat_aux_info *glo_info)
{
        CPP_FTRACE1();

        list<Remote_Client*> :: iterator itr;

        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                if((*itr)->ue_aux_info4sv(gps_desc, gps_info, 
                                          glo_desc, glo_info))
                        return -1;
        }
        return 0;
}

int Agnss_Connect :: do_ue_assist2ue_done(const Assist_Source *source, 
                                          bool                ueb_flag,
                                          unsigned short      uea_wmap,
                                          unsigned int          usr_id)
{
        CPP_FTRACE1();

        DBG_L1("Source %s has provisioned assist: UEB %s, UEA 0x%x usr_id 0x%x",
               source->get_name(), ueb_flag ? "Y" : "N", uea_wmap, usr_id);

        /* Clear up waiting flags for assist */
        if(true == ueb_flag) {
                if(assist4ueb_ref_count > 0)        --assist4ueb_ref_count;
                if(0 == assist4ueb_ref_count) assist4ueb_wait_flag = false;
        }

        assist4uea_gnss_wmap &= ~uea_wmap;

        /* n_users = 0 means assist was preparatory, to be followed by LOC REQ.
           Specifically, evaluation of assist would be at LOC REQ & not now */
        int n_users = usr_client_list.size();
        if(0 == n_users) {
                DBG_L2("Preparatory assist from source: %s", source->get_name());
                DBG_L2("Eval of assist @ LOC REQ (0 at this time), returning..");
                return 0;
        }

#if  0
        Remote_Client *client = _do_find_usr_client(source->get_name());
        DBG_L2("Eval assist for %s client(s)", client? client->get_name():"all");
#endif

        DBG_L3("Pending REQ(s): UEB %s (# %d), UEA: 0x%x", assist4ueb_wait_flag?
               "Y" : "N", assist4ueb_ref_count, assist4uea_gnss_wmap);

        Remote_Client *client = Remote_Client :: get_object(usr_id);
        DBG_L2("Eval assist for %s (client)",   client->get_name());
        do_eval2req_assist(client);

        return receiver->assist2ue_done();
}

int 
Agnss_Connect :: do_loc2assist_src(const struct gnss_pos_result&   pos_result,
                                   const struct gps_msr_result&    gps_result, 
                                   const struct gps_msr_info      *gps_msr,
                                   const struct ganss_msr_result&  ganss_result,
                                   const struct ganss_msr_info    *ganss_msr)
{
        CPP_FTRACE1();

        list<Assist_Source*> :: iterator itr;
        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                Assist_Source *src = (*itr);
                if(true  == _is_an_usr_client(src->get_name()))
                        continue;

                if(false == src->can_solicit_assist())
                        continue;

                DBG_L3("Sending LOC Results to assist src %s", src->get_name());
    
		  src->loc2assist_src(pos_result, gps_result, gps_msr,
                                    ganss_result, ganss_msr);

        }

        return 0;
}

int 
Agnss_Connect :: fr_msr_filter_gps_eph(const struct gps_msr_result&  gps_result,
                                       const struct gps_msr_info    *gps_msr)
{
        CPP_FTRACE1();

        /* Code to fetch MSR information for filtering */

        mtx_lockin();
        int ret_val = receiver->ue_msr_filter_gps_eph(gps_result, gps_msr);
        mtx_unlock();
	return ret_val;
}
/* add for GNSS recovery ++*/
int Agnss_Connect :: fr_ue_err_recovery(enum dev_err_id err_id)
{
        CPP_FTRACE1();

        mtx_lockin();
		unsigned int interval = receiver->get_interval();
        int ret_val = receiver->ue_err_recovery(err_id,interval);
        mtx_unlock();
	return ret_val;
}

// error trigger test code, to be removed
int Agnss_Connect :: fr_trigger_test_err(unsigned char val)
{
        CPP_FTRACE1();

        mtx_lockin();
        int ret_val = receiver->trigger_test_err(val);
        mtx_unlock();
	return ret_val;
}

/* add for GNSS recovery --*/

int Agnss_Connect :: do_add_client(Remote_Client *client)
{
        CPP_FTRACE1();
        
        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                if((*itr) == client) {
                        ERR_NM("Client %s; already active", client->get_name());
                        return -1; /* Already exists */
                 }
        }
        
         usr_client_list.push_back(client);
         
         return 0;
}
int Agnss_Connect :: do_remove_client(void)
{
        CPP_FTRACE1();
        
        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
					         usr_client_list.erase(itr);                
        }
        

         
         return 0;
}

int Agnss_Connect :: 
do_oper_ue_dev_param(enum ue_dev_param_id id, void *param_desc, int num)
{
	CPP_FTRACE1();
	
        if(false == receiver->is_started()) {
                DBG_L3("Device is inactive so, ignoring oper of param %id", id);
                return 0;
        }
	return receiver->oper_ue_dev_param( id, param_desc, num);
}

int Agnss_Connect :: do_dev_params2src(Assist_Source        *source,
                                       enum ue_dev_param_id      id,
                                       void                  *param,
                                       int                      num)
{
        CPP_FTRACE1();

        list<Assist_Source*> :: iterator itr;
        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                if((NULL != source) && (source != (*itr)))
                        continue;    /* Not this source */
                
                DBG_L3("Declaring %d obj(s) of dev param id %u to source %s",
                       num, id, (*itr)->get_name());

                (*itr)->decl_dev_param(id, param, num);
        }

        return 0;
}

int Agnss_Connect :: do_state_tcxo_info(void)
{
        CPP_FTRACE1();

        struct ue_tcxo_state_desc *tcxo = new struct ue_tcxo_state_desc;
        if(NULL == tcxo) {
                ERR_NM("Failed to alloc memory for ue_tcxo_state_desc");
                return -1;
        }

        tcxo->state = ue_tcxo_state_desc :: e_need_calib;

        list<Assist_Source*> :: iterator itr;
        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {

                Assist_Source *source = (*itr);
                if(false == source->can_calib_tcxo())
                        continue;
                
                DBG_L3("Scheduled TCXO state to source %s", source->get_name());
                sched_dev_params2src(source, e_ue_tcxo_state, tcxo, 1);
                return 0;
        }

        ERR_NM("No entity available to calibrate device TCXO, returning ...");
        delete tcxo;

        return 0;
}

int Agnss_Connect :: fr_put_ue2sleep(void)
{
        CPP_FTRACE1();

        mtx_lockin();
        int ret_val = receiver->ue_put2sleep();
        mtx_unlock();

	return ret_val;
}

bool Agnss_Connect :: __are_all_usr_type(unsigned int type)
{
        CPP_FTRACE1();

        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
	 	Remote_Client *client = (*itr);
                if(0 != (client->get_method_types() & (~type)))
                        return false; /* There are requestors other than type */
        }

        return true;   
}

int Agnss_Connect :: fr_dev_params2src(Assist_Source         *source, 
                                       enum ue_dev_param_id       id,
                                       void                   *param,
                                       int                       num)
{
        CPP_FTRACE1();

        mtx_lockin();
        int ret_val = do_dev_params2src(source, id, param, num);
        mtx_unlock();

        return ret_val;
}

int Agnss_Connect :: fr_assist_control(enum ue_service_ind ind,
                                       Remote_Client   *client)
{
        CPP_FTRACE1();

        mtx_lockin();
        int ret_val = _do_ue_assist_control(ind, client);
        mtx_unlock();

        return ret_val; 
}

/*------------------------------------------------------------------------------
 *  Public Methods.
 *----------------------------------------------------------------------------*/
 int Agnss_Connect :: agnss_dev_init(void)
{

	
	receiver->dev_init();
	return 0;
}

int Agnss_Connect :: agnss_dev_plt(Remote_Client *client, const struct plt_param& plt_test)
{
	int ret_val = 0;
	ret_val = do_add_client(client);
	receiver->dev_plt(plt_test);
	return 0;
}
int Agnss_Connect :: agnss_cleanup(void)
{

	cev_mgr.erase_timer2chk_ue4aid();
	cev_mgr.erase_timer2wt4loc_req();
	//cev_mgr.erase_timer2chk4Meas_rep();
	receiver->cleanup_entity();
	return 0;
}
 
int Agnss_Connect :: add_gnss_receiver(Gnss_Receiver *receiver)
{
		CPP_FTRACE1();
        if(this->receiver)
                return -1;

        this->receiver = receiver;
        return 0;
}

int Agnss_Connect :: rem_gnss_receiver(const Gnss_Receiver *receiver)
{
		CPP_FTRACE1();
        if(this->receiver != receiver)
                return -1;

        this->receiver = NULL;

        return 0;
}

int Agnss_Connect :: add_assist_source(Assist_Source *source)
{
		CPP_FTRACE1();
        assist_src_list.push_back(source);
        return 0;
}

int Agnss_Connect :: rem_assist_source(const Assist_Source *source)
{
        CPP_FTRACE1();

        list<Assist_Source*> :: iterator itr;
        
        for(itr = assist_src_list.begin(); itr != assist_src_list.end(); itr++) {
                if((*itr) == source) {
                        assist_src_list.erase(itr);
                        return 0;
                }
        }

        return -1;
}

int Agnss_Connect :: 
ue_get_aiding(const Assist_Source *source, enum loc_assist assist, 
              void *assist_array, int num)
{
        CPP_FTRACE1();

	return invoke_atomic(&Agnss_Connect::do_ue_get_aiding, source, assist, 
			     assist_array, num);
}

int Agnss_Connect :: 
ue_del_aiding(const Assist_Source *source, enum loc_assist assist,
              unsigned int sv_id_map, unsigned int mem_flags)
{
        CPP_FTRACE1();

        return invoke_atomic(&Agnss_Connect :: do_ue_del_aiding, source, assist,
                             sv_id_map, mem_flags);
}

int Agnss_Connect :: 
ue_add_assist(const Assist_Source *source, const struct nw_assist_id& id,
              const void *assist_array, int num)
{
        CPP_FTRACE1();

        return invoke_atomic(&Agnss_Connect :: do_ue_add_assist, source, id,
                             assist_array, num);
}

int Agnss_Connect :: ue_loc_results(const struct gnss_pos_result&   pos_result,
                                    const struct gps_msr_result&    gps_result, 
                                    const struct gps_msr_info      *gps_msr,
                                    const struct ganss_msr_result&  ganss_result,
                                    const struct ganss_msr_info    *ganss_msr)
{
        CPP_FTRACE1();

        int ret_val = 0;

        /* Definitely, not a recommended way */
#define LOC_ARGS pos_result, gps_result, gps_msr, ganss_result, ganss_msr

        mtx_lockin();

        bool ue_ff = do_proc_for_ff(pos_result, receiver); /* UE First Fix */

        /* Provide LOC information to sources of assist */ 
        ret_val = do_loc2assist_src(LOC_ARGS);
        if(0 != ret_val) {
                ERR_NM("Failed to dispatch LOC info to sources of assist");
                goto ue_loc_results_exit1;
        }

        ret_val = do_ue_loc_results(LOC_ARGS, ue_ff);// ue_ff: ends assist req
        if(0 != ret_val) {
                ERR_NM("Failed to dispatch LOC info to  (LCS) requestors");
                goto ue_loc_results_exit1;
        }

        if(true == ue_ff) { /* House keeping if this happens to be UE FF */
                assist4ueb_wait_flag = false;
                assist4uea_gnss_wmap = 0;

                /* Follows up on previously ended assistance requests */
                if(false == _are_all_usr_auto()) sched_eval_ue4aiding();
        }

        /* To Do: Add code to check whether GPS MSR information is available */

        if(receiver->ue_is_msr_filter_gps_eph_reqd())
              	sched_msr_filter_gps_eph(gps_result, gps_msr);

 ue_loc_results_exit1:
        mtx_unlock();

        return ret_val;
}

int Agnss_Connect :: 
ue_decoded_aid(enum loc_assist assist, const void *assist_array, int num)
{
        CPP_FTRACE1();

        return invoke_atomic(&Agnss_Connect :: do_ue_decoded_aid, assist, 
                             assist_array, num);
}

int Agnss_Connect :: ue_nmea_report(enum nmea_sn_id nmea, const char *data, 
                                    int len)
{
        CPP_FTRACE1();
#if 0
		cev_mgr.erase_timer2chk4Meas_rep();
		gnss_meas_rep = true;
		ue_recovery_count = 0;
		cev_mgr.setup_timer2chk4Meas_rep(AWAIT4MEAS_REP_AFTER_RECOVERY);
#endif
        return invoke_atomic(&Agnss_Connect::do_ue_nmea_report, nmea, data, len);
}

bool Agnss_Connect :: can_decl_intended_qop(Remote_Client *client,
                                            const struct gnss_qop& qop_req,
                                            Gnss_Receiver *receiver)
{
        if(false == _is_client_an_usr(client)) {
                DBG_L2("QoP: No LOC REQ from %s, ignoring", client->get_name());
                return false;
        }

        if(no_acc == qop_req.acc_sel) {
                DBG_L2("No QoP was specified by Requestor, returning");
                return false;
        }

        if(true == receiver->is_ff_done()) {
                DBG_L1("Device is tracking, QoP cfg not meaningful, ignoring");
                return false;
        }

        return true;
}

int Agnss_Connect :: do_decl_intended_qop(Remote_Client          *client, 
                                          const struct gnss_qop& qop_req)
{
        CPP_FTRACE1();

        if(false == can_decl_intended_qop(client, qop_req, receiver)) {
                DBG_L3("Requested QoP can not be configured into device");
                return 0;
        }
                
        if(1 == usr_client_list.size()) {
                DBG_L1("solitaty user, no arbitration reqd., set REQ QoP");
                return receiver->set_intended_qop(qop_req); 
        }

        //Remote_Client *client = NULL;
        unsigned int ttff_now = TIME2RSP_INVALID, ttff_req = qop_req.rsp_secs;
        
        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                unsigned int ttff = (*itr)->time2put_rsp1();
                if(ttff == TIME2RSP_INVALID) {
                        DBG_L1("Quiting, RSP1 expired for a previous client");
                        return 0;
                }

                if(ttff < ttff_now) { /* Ref: Client candidate with least TTFF */
                        ttff_now = ttff;
                        client   = *itr;
                }
        }

        struct gnss_qop qop_now = client->get_gnss_qop();
        DBG_L1("TTFF Now:%us, Req:%us; H ACC Now:%uM, Req:%uM", ttff_now, \
               ttff_req, qop_now.h_acc_M, qop_req.h_acc_M);
 
        /* QoP update rules: 
           a) If requested TTFF is shorter than the existing one 
           b) For the same TTFF, if requested accuracy is better
        */
        if((ttff_req < ttff_now) || 
	   ((ttff_req == ttff_now) && (qop_req.h_acc_M < qop_now.h_acc_M))) {
                DBG_L2("Device is ON; attempt to set compelling REQ QoP ...");
                return receiver->set_intended_qop(qop_req);
        }

        DBG_L1("REQ QoP not compelling enough, persisting with existing QoP");
        return 0;
}

int Agnss_Connect :: 
loc_request(Remote_Client *client, const struct gnss_qop& qop_req,
            unsigned int interval)
{
        CPP_FTRACE1();

        int ret_val = 0;
        
        mtx_lockin();  /* Take atomic */
        
        ret_val = do_add_client(client);
        if(0 != ret_val) {
                ERR_NM("Fail: add  client %s, returning", client->get_name());
                goto loc_request_exit1;
        }

        ret_val = do_set_loc_interval(client, interval);
        if(0 != ret_val) {
                ERR_NM("Fail: set period: %s, returning", client->get_name());
                goto loc_request_exit1;
        }

        //ret_val = do_loc_start(client, interval);
        ret_val = _do_loc_start(true);
        if(0 != ret_val) {
                ERR_NM("Fail: LOC REQ for %s, returning", client->get_name());
                goto loc_request_exit1;
        }

        ret_val = do_decl_intended_qop(client, qop_req);
        if(0 != ret_val) {
                ERR_NM("Fail: set QoP for %s, returning", client->get_name());
                goto loc_request_exit1;
        }

        //do_state_tcxo_info();      /* Send out TCXO info to users / support */
        DBG_L1("REQ activated, show assist needs for %s", client->get_name());
        do_put_assist_need(client);/* Send out assist needs for the LOC REQ */
        
 loc_request_exit1:
        
        mtx_unlock();  /* Give atomic */

        return ret_val;
}

int Agnss_Connect :: end_loc_req(Remote_Client *client)
{
        CPP_FTRACE1();

        mtx_lockin();

        /* A: Clear assist request to associated source to avoid lockup */
        Assist_Source *source =  _do_find_assist_src(client->get_name());
        if(NULL != source)  source->clr_assist_req(client->get_handle());

        /* B: Needs to be in same thread as caller to ensure synchronous &
           blocking call, while tearing down assist request to source.  */
        _do_ue_assist_control(e_assist_need_abort, /* for this */ client);

        /* : arrange this function in same fashion as loc_requiest */
        int ret_val = do_loc_stop(client);

        /* : Add code to restart fetch of assist information */

        mtx_unlock();

        return ret_val;
}

#if  0
int Agnss_Connect ::
ue_need_assist(const struct gps_assist_req& gps_assist,
               const struct ganss_assist_req& ganss_assist)
{
        CPP_FTRACE1();

	return invoke_atomic(&Agnss_Connect::do_ue_need_assist, 
                             gps_assist, ganss_assist);
}
#endif

int Agnss_Connect :: get_ue_loc_caps(unsigned int& gps_caps_bits,
                                     struct ganss_ue_lcs_caps *ganss_caps)
{
        CPP_FTRACE1();

	return invoke_atomic(&Agnss_Connect :: do_get_ue_loc_caps, 
                             &gps_caps_bits, ganss_caps);
}

int 
Agnss_Connect :: ue_aux_info4sv(const struct dev_gnss_sat_aux_desc& gps_desc,
                                const struct dev_gnss_sat_aux_info *gps_info,
                                const struct dev_gnss_sat_aux_desc& glo_desc,
                                const struct dev_gnss_sat_aux_info *glo_info)
{
        CPP_FTRACE1();

        return invoke_atomic(&Agnss_Connect :: do_ue_aux_info4sv, gps_desc, 
                             gps_info, glo_desc, glo_info);
}

int Agnss_Connect :: assist2ue_done(const Assist_Source *source, 
                                       bool ueb_flag, unsigned short uea_wmap,
                                       unsigned int usr_id)
{
        CPP_FTRACE1();

        return invoke_atomic(&Agnss_Connect :: do_ue_assist2ue_done, source, 
                             ueb_flag, uea_wmap, usr_id);
}

void Agnss_Connect :: decl_ue_has_time(void)
{
        CPP_FTRACE1();

        DBG_L2("UE, now, has Time, follow up to check UE for Aid Status");

        if(true == _are_all_usr_aueb()) sched_eval_ue4aiding();
}

/* add for GNSS recovery ++*/
int Agnss_Connect :: dispatch_ue_err(enum dev_err_id err_id)
{
        CPP_FTRACE1();

		int ret_val = 1;

        DBG_L1("err_seq: scheduling error recovery %d",err_id);

        ret_val = sched_ue_err_recovery(err_id);

		return ret_val;
}
/* add for GNSS recovery --*/
bool Agnss_Connect :: __do_mark_assist4ueb(struct gps_assist_req&     gps_assist,
                                           struct ganss_assist_req& ganss_assist)
{
        CPP_FTRACE1();

        bool assist = false;
        unsigned short map = -1;
        /* Assumes that consistency check has been done to ensure that request
           for POS includes all GNSS(es) that have been configured in the UE.

           The following implementation is not suitable in case the aforesaid 
           assumption is not valid. 
        */
          
        if(true == this->assist4ueb_wait_flag) {
                DBG_L1("No new REQ -> Previous REQ for Assist (UEB) is active");
                goto __do_mark_assist4ueb_exit1; /* No need to make new Req */
        }

#define A_GPS_UEB_REQ (A_GPS_TIM_REQ | A_GPS_POS_REQ | A_GPS_NAV_REQ)/* gnss.h */

        /* Step A1: Assess whether any of mandatory GPS assistance is required */
        map = gps_assist.assist_req_map &= A_GPS_UEB_REQ;
        if(0 != map) {
                DBG_L2("GPS UEB assist needs are (TIM | POS | EPH): 0x%x", map);
                assist = true;
        }

        /* Step B1: Assess mandatory but common GANSS assistance needs for UEB */ 
        map = ganss_assist.common_req_map &= (A_GANSS_TIM_REQ | A_GANSS_POS_REQ);
        if(0 != map) {
                DBG_L2("GANSS UEB assist needs are (TIM | POS): 0x%x", map);
                assist = true;
        }

        /* Step B2: Assess whether EPH assistance is required for any of GANSS */
        for(unsigned char n = 0; n < ganss_assist.n_of_ganss_req; n++) {
                struct ganss_req_info *req = ganss_assist.ganss_req_data + n;
                unsigned short  pmthd = ganss_id2pmthd_bit(req->id_of_ganss);
                map = req->req_bitmap & A_GANSS_NAV_REQ;
                if(0 != map) {
                        DBG_L2("GANSS UEB (pmthd) 0x%x assist needs EPH", pmthd);
                        assist = true;
                }
        }

__do_mark_assist4ueb_exit1:

        DBG_L1("UE has %s got all aid data for POS (UEB)", assist? "not" : " ");
        return assist;
}

bool Agnss_Connect :: __do_mark_assist4uea(unsigned short&          gnss_methods, 
                                         struct gps_assist_req&   gps_assist,
                                         struct ganss_assist_req& ganss_assist)
{
        CPP_FTRACE1();

        /* Assumes that consistency check has been done to ensure that request
           for MSR includes a subset of GNSS(es) that have been configured in 
           the UE. Note, that UE can provide independent MSR for each of GNSS
           that it supports.

           The following implementation is not suitable in case the aforesaid 
           assumption is not valid. 
        */
        
        /* Identify GNSS for which ACQ-assist has not been requested as yet */
        unsigned short gnss_uea = ~this->assist4uea_gnss_wmap & gnss_methods;

        if(0 == gnss_uea) {
                DBG_L2("Needed AA has been already requested & is awaited");
                return false;  /* Awaiting assist, no need to make new Req. */
        }

#define A_GPS_UEA_REQ (A_GPS_TIM_REQ | A_GPS_ACQ_REQ) /* Move to gnss.h, remove */

        if(gnss_uea & GNSS_GPS_PMTHD_BIT) {
                unsigned short map = gps_assist.assist_req_map &= A_GPS_UEA_REQ;
                DBG_L2("GPS ACQ Assist  %sneeded: 0x%x", map? "" : "NOT ", map);
        }

        unsigned short map = ganss_assist.common_req_map &= A_GANSS_TIM_REQ;
        DBG_L2("GANSS (common) UEA %sneeded:  0x%x", map? "" : "NOT ", map);

        for(unsigned char n = 0; n < ganss_assist.n_of_ganss_req; n++) {
                struct ganss_req_info *req = ganss_assist.ganss_req_data + n;
                unsigned short  pmthd = ganss_id2pmthd_bit(req->id_of_ganss);
                if(0 == (pmthd & gnss_uea))
                        continue;
               
                map = req->req_bitmap &= A_GANSS_MSR_REQ;
                DBG_L2("GANSS (pmthd) 0x%x UEA needed: 0x%x", pmthd, map);
        }

        DBG_L3("UE needs for AA for GNSS (pmthd) bits 0x%x", gnss_uea); 
        gnss_methods = gnss_uea;
        return true;
}

static bool _confirm_sched_req4assist(bool                       make_req,
                                      Remote_Client             *client,
                                      struct gps_assist_req     *gps_assist,
                                      struct ganss_assist_req   *ganss_assist,
                                      unsigned short             uea_types)
{
        CPP_FTRACE1();

        /* Fix up: platform application does not double up as assist source */
        if(client && (0 == strcmp(client->get_name(), TI_AGNSS_PFORM_NAME))) 
                client = NULL; 

        if((client != NULL) &&   (0 == gps_assist->assist_req_map) &&
           ((0 == strcmp(client->get_name(), TI_AGNSS_SUPLx_NAME)) ||
            (0 == strcmp(client->get_name(), TI_AGNSS_CPLNE_NAME))))
                make_req = false;

        if(false == make_req) {

                DBG_L1("UE has adequate aid to support both POS and MSR REQ");

                if(NULL == client) {
                        CT_OPER_BIN(PFM_APP_LOGB_ID_H_START, 0, NULL);
                        return false; // No assist is needed by UE
                }

                /* Client doesn't need any assist; let's indicate the same */
                gps_assist->assist_req_map   = 0;
                ganss_assist->n_of_ganss_req = 0;
                ganss_assist->common_req_map = 0;
                uea_types = 0;

                /* Place holder to log autonomous session */
        }
        
        DBG_L1("%s: External assist is %s for %s", uea_types? "UEA" : "UEB", \
               (false == make_req)? "not needed" :  "needed to do progress", \
               (NULL  != client)? client->get_name() : "a previous LOC REQ"); 

        /* Schedule REQ/IND for aiding from various sources of assistance */
        sched_ue_need_assist(client, gps_assist, ganss_assist, uea_types);
        return true;
}

/* Needs to be called from following trigger points
a) Location Request
b) First POS Response
c) Timeout of check
d) Assistance done (???)
*/

/* 
   Assumption: UE with adequate aid info for UEB can seamlessly support UEA 
   a. If UE has aiding info to support UEB then, no need to request assist
   b. If UE needs assist then, check if, client has requested for UEA only
   c. If LOC request is for UEA only then, seek GNSS specific AA from a NW

   Threads that can not invoke this function are:
   - UE Adapter thread.
*/
bool 
Agnss_Connect :: _do_mark_assist(bool uea_only, unsigned short&   gnss_methods,
                                 const struct gps_assist_req&     gps_need,
                                 const struct ganss_assist_req&   ganss_need,
                                 struct gps_assist_req&           gps_assist,
                                 struct ganss_assist_req&         ganss_assist)
{
        CPP_FTRACE1();

        bool make_req = false;

        /* Set up UE needs to generate assist request for UEB */
        memcpy(&gps_assist,    &gps_need,     sizeof(gps_assist_req));
        memcpy(&ganss_assist,  &ganss_need, sizeof(ganss_assist_req));

        if(true == __do_mark_assist4ueb(gps_assist, ganss_assist)) {/* step a */
                DBG_L2("UE hasn't got adequate aid to do neither POS nor MSR");
                make_req = true;
        }

        if((false == make_req) || (false == uea_only)) {
                DBG_L1("UE has %sadequate aid to do %s", make_req ? "in" : "", 
                       uea_only ? "UEA" : "UEB");

                return make_req;
        }

        /* Set up UE needs to generate assist request for UEA */
        memcpy(&gps_assist,   &gps_need,     sizeof(gps_assist_req));
        memcpy(&ganss_assist, &ganss_need, sizeof(ganss_assist_req));

        /* step b and step c */
        if(false == __do_mark_assist4uea(gnss_methods, gps_assist, ganss_assist)) {
                DBG_L2("UEA: No ACQ Assist is needed by UE to support LOC REQ");
                make_req = false;
        }

        return make_req;
}

int Agnss_Connect :: do_put_assist_need(Remote_Client *client)
{
        CPP_FTRACE1();

        bool make_req = false, uea_only = false;
        static struct gps_assist_req   gps_need;   /* Ensuring not on stack */
        static struct ganss_assist_req ganss_need; /* Ensuring not on stack */

        struct gps_assist_req   *gps_assist   = new struct gps_assist_req;
        struct ganss_assist_req *ganss_assist = new struct ganss_assist_req;

        if((NULL == ganss_assist) || (NULL == gps_assist)) {
                ERR_NM("Mem alloc failed: ganss_assist_req or gps_assist_req");
                if(gps_assist)   delete gps_assist;
                if(ganss_assist) delete ganss_assist;
                return -1;
        }

        unsigned int method_types = 0; unsigned short gnss_methods = 0;
        if(client) {
                gnss_methods = client->get_gnss_methods();
                method_types = client->get_method_types();

                if(!(method_types & ~UE_LCS_CAP_GNSS_AUTO)) goto auto_usr_exit;
                if(!(method_types & ~UE_LCS_CAP_AGNSS_MSR))    uea_only = true;
        }

	ganss_need.n_of_ganss_req = MAX_GANSS;
        if(0 != receiver->get_ue_req4assist(gps_need, ganss_need)) {
                ERR_NM("Failed to get needed assist info from UE ");
                goto no_sched_exit;
        }

        make_req = _do_mark_assist(uea_only, gnss_methods, gps_need, ganss_need, 
                                   *gps_assist, *ganss_assist);

 auto_usr_exit:
        DBG_L1("%s: assessment completed", client? client->get_name() : "UE");
        if(true == 
           _confirm_sched_req4assist(make_req, client, gps_assist, ganss_assist,
                                     (true == uea_only) ? gnss_methods : 0))  {
                return 0;
        }

 no_sched_exit:
        DBG_L1("REQ for assist by UE was not scheduled, returning....");
        delete ganss_assist; delete gps_assist;

        return 1; /* Temp Fix */
}

int Agnss_Connect :: do_eval2req_assist(Remote_Client *client)
{
        CPP_FTRACE1();

        int ttl_secs = receiver->get_ue_aid_ttl();
        if(ttl_secs > 0) {
                /* Enough time left for aiding to expire, so sched another chk */
                DBG_L1("Scheduling check for UE aiding after %us", ttl_secs); 
                cev_mgr.setup_timer2chk_ue4aid(ttl_secs);
        } else {
                /* If TTL for EPH less or equal to MIN_TTL then, time to fetch */
                DBG_L1("UE aiding is about to expire, %s needs to fetch assist",
                       (NULL == client)? "UE" : client->get_name());
                int rv = do_put_assist_need(client);
                if(1 == rv) { /* TEMP FIX */
                        DBG_L1("Scheduling RECHECK");       /* To be removed */
                        /* Follows up on previously ended assistance requests */
                        if(false == _are_all_usr_auto()) sched_eval_ue4aiding();
                }
        }

        return 0;
}

int Agnss_Connect :: fr_eval2req_assist(void)
{
        int ret_val = -1;

        mtx_lockin();

        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                Remote_Client *client = (*itr);
                if(0 != (client->get_method_types() & ~UE_LCS_CAP_AGNSS_POS))
                        continue;  // Not UEB request, let us find next one

                DBG_L2("Eval assist for %s (client)",  client->get_name());
                ret_val = do_eval2req_assist(client);
                break;             // Found an UEB request, so now move out
        }
 
        mtx_unlock();

        return ret_val;
}

int Agnss_Connect :: nw_loc_results(Assist_Source *source, 
                                    const struct gnss_pos_result& pos_result,
                                    unsigned int   usr_id)
{
        CPP_FTRACE1();

        mtx_lockin();
                
        /* Indicate NO GPS   MSR Result */
        static struct gps_msr_result     gps_result;
        memset(&gps_result, 0,  sizeof(gps_result));
        gps_result.n_sat       = 0;
        gps_result.contents    = GPS_MSR_NOT_REPORTED;

        /* Indicate NO GANSS MSR Result */
        static struct ganss_msr_result    ganss_result;
        memset(&ganss_result, 0, sizeof(ganss_result));
        ganss_result.n_ganss  = 0;
        ganss_result.contents = GANSS_MSR_NOT_REPORTED; 

        list<Remote_Client*> :: iterator itr;
        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                Remote_Client *client = *itr;
                if(0 == strcmp(source->get_name(), client->get_name()))
                        continue; /* Do not send to POS originator */
                
                DBG_L2("Giving async NW LOC Results to %s", client->get_name());
                client->ue_loc_results(pos_result, gps_result, NULL, 
                                       ganss_result,    NULL);
        }

        /* Indicate that assistance need for Client has ended */
        sched_assist_control(e_assist_need_ended,
                             Remote_Client :: get_object(usr_id));
        
        mtx_unlock();
        
        return 0;
}

int Agnss_Connect :: decl_intended_qop(Remote_Client    *client, 
                                      const struct gnss_qop& req_qop)
{
        CPP_FTRACE1();

        return invoke_atomic(&Agnss_Connect :: do_decl_intended_qop, client, 
                             req_qop);
}

int Agnss_Connect :: set_loc_interval(Remote_Client *client, 
                                      unsigned int interval)
{
        CPP_FTRACE1();

        return invoke_atomic(&Agnss_Connect :: do_set_loc_interval, client,
                             interval);
}

int Agnss_Connect :: 
oper_ue_dev_param(enum ue_dev_param_id id, void *param_desc, int num)
{
	CPP_FTRACE1();
	
        return invoke_atomic(&Agnss_Connect :: do_oper_ue_dev_param, id,
			     param_desc, num);
}
void Agnss_Connect :: sort_assist_src(void)

{
        Assist_Source *source = NULL;

        for(int i = 0; i < assist_src_list.size(); i++) {
                unsigned int min = 0xFFFFFFFF;
                list<Assist_Source*> :: iterator itr = assist_src_list.begin();

                for(int j = 0; j < assist_src_list.size() - i; j++, itr++) {
                        if((*itr)->get_source_pref() <= min) {
                                source = *itr;
                                min = source->get_source_pref();
                        }
                }
                assist_src_list.remove(source);
                assist_src_list.push_back(source);

        }

        return;

}
int Agnss_Connect :: do_ue_recovery_ind(const enum dev_reset_ind rst_ind, int len)
{
        CPP_FTRACE1();

        if(usr_client_list.empty()) {
                DBG_L3("Not set: as no user client is active, returning back");
                return 0;
        }

        list<Remote_Client*> :: iterator itr;

        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                DBG_L1("do_ue_recovery_ind client %s", (*itr)->get_name());
				(*itr)->ue_recovery_ind(rst_ind, len);
        }



		 DBG_L1("do_ue_recovery_ind Exiting");

        return 0;
}

int Agnss_Connect ::ue_recovery_ind(const enum dev_reset_ind rst_ind,  int len)
{
        CPP_FTRACE1();
		return invoke_atomic(&Agnss_Connect :: do_ue_recovery_ind, rst_ind, len);

}
int Agnss_Connect :: do_ue_cw_test_results(const enum cw_test_rep cwt, const struct cw_test_report *cw_rep, int len)
{
        CPP_FTRACE1();

        if(usr_client_list.empty()) {
                DBG_L3("Not set: as no user client is active, returning back");
                return 0;
        }

        list<Remote_Client*> :: iterator itr;

        for(itr = usr_client_list.begin(); itr != usr_client_list.end(); itr++) {
                DBG_L1("Eval UE LOC Results for client %s", (*itr)->get_name());
				(*itr)->ue_cw_test_results(cwt, cw_rep, len);
        }



		 DBG_L1("ue_cw_test_results Exiting");

        return 0;
}

int Agnss_Connect ::ue_cw_test_results(const enum cw_test_rep cwt, const struct cw_test_report *cw_rep, int len)
{
        CPP_FTRACE1();
		return invoke_atomic(&Agnss_Connect :: do_ue_cw_test_results, cwt,cw_rep, len);

}
