#include "Gnss_Utils.h"
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include <debug.h>
#include "Ftrace.h"

template <typename Node, typename ADAP_IF, typename ADAP_CB, typename LOAD_FN>
static int run_node(Node *node, const ADAP_IF *adap_if, 
		    const ADAP_CB *adap_cb, LOAD_FN  adap_ld, const char* thr_name)
{
	CPP_FTRACE2();
	DBG_L1(" [GNSS_UTILS] run_node");
	if(adap_ld((void*)adap_if))
		goto gnss_run_err1; 

	if(!adap_if->init || !adap_if->exit)
		goto gnss_run_err1;

	if(adap_if->init(adap_cb))
		goto gnss_run_err1;

	/* Schedule a thread */
	if(adap_if->cb_thread_func) {
		int ret = node->work_wthread(adap_if->cb_thread_func, 
					     adap_if->cb_thread_arg1, thr_name);
		if(ret)
			goto gnss_run_err2;
	}

	if(node->set_in_service()) {
		goto gnss_run_err3;
	}

	return 0;

gnss_run_err3:
	if(adap_if->cb_thread_func)
		node->hlt_entity();

gnss_run_err2:
	adap_if->exit();

gnss_run_err1:
	DBG_L1(" [GNSS_UTILS] run_node ERROR !!!");
	return -1;
}

 
int gnss_run_node(Agnss_Services *node,
		  const struct ti_agnss_lcs_adap_if *adap_if,
		  const struct ti_agnss_lcs_adap_cb *adap_cb,
		  gnss_adapter_load_fn adap_ld,const char* thr_name)
{
	CPP_FTRACE2();
	return run_node(node, adap_if, adap_cb, adap_ld, thr_name);
}

int gnss_run_node(Gnss_Assist_Source *node,
		  const struct ti_lc_assist_adap_if *adap_if,
		  const struct ti_lc_assist_adap_cb *adap_cb,
		  gnss_adapter_load_fn adap_ld, const char* thr_name)
{
	CPP_FTRACE2();
	return run_node(node, adap_if, adap_cb, adap_ld, thr_name);
}

int gnss_run_node(Gnss_Receiver *node,
		  const struct ti_gnss_ue_interface *adap_if,
		  const struct ti_gnss_ue_callbacks *adap_cb,
		  gnss_adapter_load_fn adap_ld, const char* thr_name)
{
	CPP_FTRACE2();
	return run_node(node, adap_if, adap_cb, adap_ld, thr_name);
}




unsigned short ganss_id2pmthd_bit(const unsigned char ganss_id)
{
	unsigned short ret_bit = 0;

        switch(ganss_id) {

        case GANSS_ID_GLO:
                ret_bit = GNSS_GLO_PMTHD_BIT;
		break;

        default:
                break;
        }

        return ret_bit;
}


