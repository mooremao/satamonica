
#ifndef __BASE_ENTITY_H__
#define __BASE_ENTITY_H__

#include <pthread.h>
#include <string>

typedef void* (*thread_function)(void* parm);

using namespace std;

class Base_Entity;

class Gnss_Thread {

 private:

        thread_function             thread_func;
        void                       *thread_parm;
        pthread_t                   thread_desc;

 public:
        /* Default constructor */
        Gnss_Thread();

        /* Start and launch work thread. Returns 0 on success or error */ 
        int work(void* (*thread_func)(void *thread_parm), 
                 void *thread_parm,   Base_Entity *base, const char *thr_name);
        int stop();  /* Cancel working thread. Returns 0 on success or error */
        int wait();  /* Await thread complete. Returns 0 on success or error */
};

class Base_Entity {

 private:

        string           name_string; /* Name of enity */
        Gnss_Thread      work_thread; /* Worker thread */

        
 public:

        Base_Entity(const char *name);

        const char *get_name() const {      /* Get assigned name of obj */ 
                return name_string.c_str(); 
        }

        virtual int  run_entity(const char* thr_name) = 0;
        virtual int  await_exit() = 0;
        virtual ~Base_Entity();
        virtual int  out_of_service() = 0;  /* Node un-available for use */
        virtual int  set_in_service() = 0;  /* Node is available for use */ 
        
        int  stop_wthread();
        int  work_wthread(void* (*thread_func)(void *parm), void *parm, const char* thr_name);
        int  wait_wthread();
};

#endif
