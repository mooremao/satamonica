
#ifndef __ASSIST_SOURCE_H__
#define __ASSIST_SOURCE_H__

#include <vector>
#include "gnss_lcs_common.h"
#include "Base_Entity.h"

/* Following flag constants describe the init time capabilities of source */
const unsigned short SRC_CALIB_TCXO = 0x0001; /* Support TCXO Calibration */
const unsigned short SRC_CAN_PENDED = 0x0002; /* Source can be sus-pended */

using namespace std;

class Assist_Source : virtual public Base_Entity {

 private:

        unsigned short      cap_flags;     /* Defines init time capabilities */
        unsigned short      ctl_flags;     /* Presents run time status/state */

	/* gps_assist_na & ganss_comm_na (GANSS common) use 3GPP assist bits */ 
        unsigned int        gps_assist_na; /* GPS Assist Data not avail (na) */
        unsigned int        ganss_comm_na; /* GANSS Comm info not avail (na) */

        struct ganss_generic_na {          /* GANSS Generic Assist not avail */

                unsigned char     ganss_id; /* GANSS ID -> Table A.40 (RRLP) */
                unsigned int      gen_bits; /* Generic assist not avail (na) */

                ganss_generic_na() :
                        ganss_id(GANSS_ID_NONE), gen_bits(0) { }
        };

        struct ganss_generic_na generic_na[MAX_GANSS];

	/* uea_gnss_wmap use 3GPP bits for GNSS Position methods; ref gnss.h */ 
        unsigned short      uea_gnss_wmap; /* Bitmap (pmthd): awaited assist */
        bool                ueb_wait_flag; /* If true assist for UEB awaited */

        /* Indicates preference of independent source for use: 0 is first... */ 
        unsigned int        prefer_id;     /* Relevant for independent srcs  */

        struct assist_requester {

                bool                   ueb_wait_flag;
                unsigned short         uea_gnss_wmap;

                unsigned int           usr_id;

                assist_requester(unsigned int cookie = 0) :
                        ueb_wait_flag(false), uea_gnss_wmap(0), usr_id(cookie)
                {

                }
        };

        vector<struct assist_requester> requester_vect;

 private:
        
        bool do_can_solicit_assist(void);

        void do_clr_assist_req(unsigned int usr_id);

        /* Returns index into generic_na array for given GANSS otherwise, -1 */
        char _get_ganss_na_idx(unsigned char ganss_id);

        /* Returns NULL if element for indicated GANSS is not found */
        struct ganss_generic_na* get_ganss_na(unsigned char ganss_id)   {
                char idx = _get_ganss_na_idx(ganss_id);
                return ((-1 < idx)&&(idx < MAX_GANSS))? generic_na + idx : NULL;
        }

        /* Returns index into generic_na array for given GANSS otherwise, -1
           mk_alloc causes assigning of element to GANSS, if not done so  */
        char _get_ganss_na_idx(enum gnss_pv_id en_ganss, bool mk_alloc);

        /* mk_alloc assigns element to indicated GANSS if already not done
           Returns NULL, if element for the indicated GANSS is not found */
        struct ganss_generic_na* get_ganss_na(enum gnss_pv_id en_ganss,
                                              bool            mk_alloc) {
                char idx =  _get_ganss_na_idx(en_ganss, mk_alloc);
                return ((-1 < idx)&&(idx < MAX_GANSS))? generic_na + idx : NULL;
        }

        int do_assist_req_control(enum ue_service_ind ind, unsigned int usr_id);

        int get_requester_index(unsigned int usr_id, bool can_alloc);

 protected:
       
        virtual int ue_service_ctl(enum ue_service_ind ind,
                                   unsigned int     usr_id);

        /*----------------------------------------------------------------------
         * ABC -> Virtual functions to be excercised by A-GNSS Connect
         *--------------------------------------------------------------------*/
        virtual int ue_need_assist(const struct gps_assist_req&   gps_assist,
                                   const struct ganss_assist_req& ganss_assist,
                                   unsigned int                   usr_id)
                = 0;

 public:

        /* Constructor */
        Assist_Source(const char *name, unsigned int flags);

        /*----------------------------------------------------------------------
         * ABC -> Virtual functions to be excercised by A-GNSS Connect
         *--------------------------------------------------------------------*/
        virtual int ue_decoded_aid(enum loc_assist assist, 
                                   const void *assist_array, int num) = 0;
        virtual int decl_dev_param(enum ue_dev_param_id   id, 
                                   void *param_desc, int num) = 0;

        /*----------------------------------------------------------------------
         * Virtual functions to be excercised by A-GNSS Connect
         *--------------------------------------------------------------------*/
        virtual int ue_loc_results(const struct gnss_pos_result&  pos_result,
                                   const struct gps_msr_result&   gps_result,
                                   const struct gps_msr_info     *gps_msr,
                                   const struct ganss_msr_result& ganss_result,
                                   const struct ganss_msr_info   *ganss_msr);

        /*----------------------------------------------------------------------
         * Other methods
         *--------------------------------------------------------------------*/
        int out_of_service();  /* Node un-available for use */
        int set_in_service();  /* Node is available for use */

        void set_source_pref(unsigned int pref) { prefer_id = pref; }
        unsigned int get_source_pref(void)      { return prefer_id; }

        int ue_add_assist(const struct nw_assist_id& id, 
                          const void *assist_array, int num);


        int ue_del_aiding(enum loc_assist assist, unsigned int sv_id_map, 
                          unsigned int mem_flags);

        int ue_get_aiding(enum loc_assist assist, void *assist_array,
                          int num);

        void set_solicit_assist(bool flag);
        bool can_solicit_assist(const struct gps_assist_req& gps_assist,
                                const struct ganss_assist_req& ganss_assist);

        bool can_solicit_assist(void);

        int assist2ue_done(unsigned int usr_id);

        void set_unsupported_assist(unsigned int gps_assist,
                                    unsigned int ganss_comm);
        void add_unsupported_assist(unsigned int gps_assist,
                                    unsigned int ganss_comm);

        void set_unsupported_assist(enum gnss_pv_id en_ganss,
                                    unsigned int    gen_bits);
        void add_unsupported_assist(enum gnss_pv_id en_ganss,
                                    unsigned int    gen_bits);

        int ue_assist_control(enum ue_service_ind    ind,
                              unsigned int        usr_id);

        int ue_need_assist_req(const struct gps_assist_req& gps_assist,
                               const struct ganss_assist_req& ganss_assist,
                               unsigned short                 uea_flags,
                               unsigned int                   usr_id);

        int nw_loc_results(const struct gnss_pos_result& pos_result,
                           unsigned int usr_id);

        bool can_calib_tcxo(void) {
                return (cap_flags & SRC_CALIB_TCXO)? true : false;
        }

        void clr_assist_req(unsigned int usr_id);

	int loc2assist_src(const struct gnss_pos_result&  pos_result,
                           const struct gps_msr_result&   gps_result,
                           const struct gps_msr_info     *gps_msr,
                           const struct ganss_msr_result& ganss_result,
                           const struct ganss_msr_info   *ganss_msr);
};

#endif
