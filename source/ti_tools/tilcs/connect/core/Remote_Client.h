#ifndef __REMOTE_CLIENT_H__
#define __REMOTE_CLIENT_H__
#include <stdint.h>
#include "gnss.h"
#include "gnss_lcs_common.h"
#include "Base_Entity.h"

using namespace std;

/*-----------------------------------------
 * Private utility class for Remote Client
 *-----------------------------------------
 */
class Loc_Rsp_Conf {

	private:
	volatile	bool               conf_flag;
	unsigned int       interval;  /* GCD of client */
	volatile	unsigned int       time4rspn; /* TTFF         */
	volatile	unsigned int       time4rsp1;
	volatile	bool               rsp1_pend;

	public:
		Loc_Rsp_Conf();

		int   reset_config(void);
		//        int   start_config(unsigned int interval);
		int   set_req_time(unsigned int);

		bool  is_cfg_valid(void) { return conf_flag; }
		void  mk_cfg_valid(void) { conf_flag = true; }

		void  set_interval(unsigned int interval) { 
                        this->interval = interval; 
                }

		int   time2next_rsp(void);
		int   time2put_rsp1(void);
		int   prep4next_rsp(void);

		void  set_rsp1_done(void) { rsp1_pend = false; }

		bool  awaiting_rsp1(void) { return rsp1_pend;  }

		unsigned int get_interval() { return interval; }

};

class Remote_Client : virtual public Base_Entity {

	private:
		Loc_Rsp_Conf         rsp_config;   /* Config for RSP(s) to be sent out */
		struct gnss_qop      qop_desired;

		unsigned int         method_types;  /* UE Based or Assisted   */

		unsigned char        n_of_methods;  /* N bits of GNSS methods */
		unsigned short       methods_gnss;  /* Map of GNSS to be used */

		/* A convenient and utility structure */
		struct loc_results  {

			const gnss_pos_result          *pos_result;
			const struct gps_msr_result    *gps_result;
			const struct gps_msr_info      *gps_msr;
			const struct ganss_msr_result  *ganss_result;
			const struct ganss_msr_info    *ganss_msr;
		};

		/* Get Minimum horizontol accuracy from POS, GPS-MSR and GANSS-MSR */
		unsigned char min_hacc(const struct loc_results& results);

		/* Get Minimum vertical accuracy from POS, GPS-MSR and GANSS-MSR  */
		unsigned char min_vacc(const struct loc_results& results);

		/* Ensures POS, GPS-MSR and GANSS-MSR has all needed information  */
		bool chk_methods(const struct loc_results& results);

		/* Configure location instruction from Client */
		int cfg_loc_instruct(const struct loc_instruct& loc,
				     unsigned int gps_caps_bits,
				     struct ganss_ue_lcs_caps *ganss_caps);

		/* Assesses whether UE supports requested GANSS(es) */
		unsigned char 
			assess_ganss4loc(unsigned int    type_flags,
					 unsigned short  lc_methods,
					 const struct ganss_ue_lcs_caps *ganss_caps);

		bool 
			assess_ganss4uea(unsigned int	 type_flags,
					 unsigned short  lc_methods,
					 const struct ganss_ue_lcs_caps *ganss_caps);
		bool 
			assess_ganss4ueb(unsigned int	 type_flags,
					 unsigned short  lc_methods,
					 const struct ganss_ue_lcs_caps *ganss_caps);
	public:
		Remote_Client(const char *name);

                unsigned int get_handle() { /* Returns a handle to object */
                        //return reinterpret_cast<unsigned int>     this; 
#ifdef ANDROID
                        return (unsigned int)     this; 
#else
                       return (uintptr_t )     this; 
#endif
                }

                static Remote_Client *get_object(unsigned int obj_id) {
                        return (Remote_Client*) obj_id;
                }

		/* Get LOC methods / capabilities of UE */
		int get_ue_loc_caps(unsigned int& gps_caps_bits,
				    struct ganss_ue_lcs_caps *ganss_caps);

		bool fits_qop_need(const gnss_pos_result&          pos_result,
				   const struct gps_msr_result&    gps_result,
				   const struct gps_msr_info      *gps_msr,
				   const struct ganss_msr_result&  ganss_result,
				   const struct ganss_msr_info    *ganss_msr);

		int loc_start();
		int loc_stop();
 		int dev_init();
		int ue_cleanup();
		int dev_plt(const struct plt_param& plt_test);

 		/* Estimates time (in secs) to wait prior to sending out next response */
		int  time2next_rsp();
		int  time2put_rsp1();  /* Returns time (secs) left prior to next RSP */
		bool awaiting_rsp1();  /* Is RSP1 (for TTFF) is still awaited? */
		int   prep4next_rsp(void);

		unsigned int   get_method_types() { return method_types; }
		unsigned short get_gnss_methods() { return methods_gnss; }
		/* Set methods */
		int set_loc_instruct(const struct loc_instruct& loc); /* LOC Instruct */

		int set_rpt_criteria(const struct rpt_criteria& rpt); /* RPT Criteria */

		int set_loc_criteria(const struct loc_instruct& loc,  /* LOC  +  RPT  */
				     const struct rpt_criteria& rpt);

		int oper_ue_dev_param(enum ue_dev_param_id id,
						void *param_desc, int num);
		struct gnss_qop get_gnss_qop() { return qop_desired; }
                unsigned int get_loc_interval(void);
		/*-----------------------------------------------------------------
		 * ABC --> virtual functions; Will be excercised by A-GNSS Connect 
		 *-----------------------------------------------------------------*/
		virtual int ue_loc_results(const gnss_pos_result&         pos_result, 
					   const struct gps_msr_result&   gps_result,
					   const struct gps_msr_info     *gps_msr,
					   const struct ganss_msr_result& ganss_result,
					   const struct ganss_msr_info   *ganss_msr) = 0;

		virtual int ue_nmea_report(enum nmea_sn_id nmea, const char *data, 
					   int len) = 0;

		virtual int ue_aux_info4sv(const struct dev_gnss_sat_aux_desc&  gps_desc,
					   const struct dev_gnss_sat_aux_info  *gps_info,
					   const struct dev_gnss_sat_aux_desc&  glo_desc,
					   const struct dev_gnss_sat_aux_info  *glo_info) = 0;
		//virtual int ue_decoded_aid() = 0;
		virtual int ue_cw_test_results(const enum cw_test_rep cwt, const struct cw_test_report *cw_rep, int len)=0;
		virtual int ue_recovery_ind(const enum dev_reset_ind rstind,  int len)=0;
};

#endif
