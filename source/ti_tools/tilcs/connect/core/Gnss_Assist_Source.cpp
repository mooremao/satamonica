
#include "Gnss_Assist_Source.h"
#include "Gnss_Utils.h"
#include "Ftrace.h"
#include "debug.h"

#define ADAP_IF_FN(fn, ...) if(fn && fn(__VA_ARGS__))                   \
                ERR_NM("%s (adap) failed or couldn't process data", get_name());

int Gnss_Assist_Source :: ue_service_ctl(enum ue_service_ind ind, 
                                         unsigned int      usr_id)
{
        CPP_FTRACE2();

        Assist_Source :: ue_service_ctl(ind, usr_id);

        /* Ensure that multiple users are not invoking services simultaneously */
        if((0 != ue_usr) && (ue_usr != usr_id)) {
               ERR_NM("Out of sync; ue-usr Id old : 0x%x, now : 0x%x",
                      ue_usr, usr_id);
               return 0;
        }

        struct assist_reference assist_usr;
        assist_usr.is_client_others  = true;   /* Always serves others */

        ADAP_IF_FN(adap_if.ue_service_ctl, adap_if.hnd, ind, assist_usr);

        return 0;
}

int 
Gnss_Assist_Source :: ue_need_assist(const struct gps_assist_req& gps_assist, 
                                     const struct ganss_assist_req& ganss_assist,
                                     unsigned int                   usr_id)
{
        CPP_FTRACE2();
        bool       is_other = false;

         /* Ensure that multiple users are not invoking services simultaneously */
        if((0 != ue_usr) && (ue_usr != usr_id)) {
               ERR_NM("Out of sync; ue-usr Id old : 0x%x, now : 0x%x",
                      ue_usr, usr_id);
               return 0;
        }

        ue_usr = usr_id;

        struct assist_reference assist_usr;
        assist_usr.is_client_others  = true;  /* Always serves others */

        ADAP_IF_FN(adap_if.ue_need_assist, adap_if.hnd, 
                   gps_assist, ganss_assist, assist_usr);

        return 0;
}

// Constructor
Gnss_Assist_Source :: 
Gnss_Assist_Source(const char *name, unsigned int a_flags, 
                   gnss_adapter_load_fn load_fn)
        : Base_Entity(name), Assist_Source(name, a_flags), adap_ld(load_fn), time_decoded(false)
{
		CPP_FTRACE2();
        adap_cb.hnd            = (void*) this;
        adap_cb.ue_add_assist  = adap_ue_add_assist<Gnss_Assist_Source>;
        adap_cb.ue_del_aiding  = adap_ue_del_aiding<Gnss_Assist_Source>;
        adap_cb.ue_get_aiding  = adap_ue_get_aiding<Gnss_Assist_Source>;
        adap_cb.assist2ue_done = adap_ue_assist2ue_done<Gnss_Assist_Source>;
}

extern int gnss_run_node(const Gnss_Assist_Source& node,
                         const struct ti_lc_assist_adap_if *adap_if,
                         const struct ti_lc_assist_adap_cb *adap_cb,
                         gnss_adapter_load_fn adap_ld, const char* thr_name);

int Gnss_Assist_Source :: run_entity(const char* thr_name)
{
		CPP_FTRACE2();
        return gnss_run_node(this, &adap_if, &adap_cb, adap_ld, thr_name);
}

int Gnss_Assist_Source :: await_exit()
{
		CPP_FTRACE2();
        return Base_Entity :: wait_wthread();
}

int Gnss_Assist_Source :: hlt_entity()
{
		CPP_FTRACE2();
        return Base_Entity :: stop_wthread();
}

int Gnss_Assist_Source :: ue_decoded_aid(enum loc_assist assist, 
                                         const void *assist_array, int num)
{
        CPP_FTRACE2();
		switch(assist){
			case a_GPS_TIM : 
			 
				if(time_decoded == false){
					ADAP_IF_FN(adap_if.ue_decoded_aid, adap_if.hnd,
						   assist, assist_array, num);
					}
				
				time_decoded  = true;
				
			
				break;
			case a_GPS_EPH :
			case a_GLO_EPH :
				
				ADAP_IF_FN(adap_if.ue_decoded_aid, adap_if.hnd,
						   assist, assist_array, num);
				break;
			default : 
					break;
		}
        return 0;
}

int Gnss_Assist_Source :: out_of_service()
{
		CPP_FTRACE2();
        return Assist_Source :: out_of_service();
}

int Gnss_Assist_Source :: set_in_service()
{
		CPP_FTRACE2();
        return Assist_Source :: set_in_service();
}

int Gnss_Assist_Source :: ue_add_assist(const struct nw_assist_id& id,
                                        const void *assist_array, int num)
{
		CPP_FTRACE2();
        return Assist_Source :: ue_add_assist(id, assist_array, num);
}

int Gnss_Assist_Source :: ue_del_aiding(enum loc_assist assist,
                                        unsigned int sv_id_map,
                                        unsigned int mem_flags)
{
		CPP_FTRACE2();
        return Assist_Source :: ue_del_aiding(assist, sv_id_map, mem_flags);
}

int Gnss_Assist_Source :: ue_get_aiding(enum loc_assist assist, 
                                        void *assist_array,
                                        int num)
{
		CPP_FTRACE2();
        return Assist_Source :: ue_get_aiding(assist, assist_array, num);
}

int Gnss_Assist_Source :: 
assist2ue_done(const struct assist_reference& assist_usr)
{
        CPP_FTRACE2();

        if(0 == ue_usr) {
                ERR_NM("%s: No usr Id found for requester", get_name());
                return 0;
        }

        return Assist_Source :: assist2ue_done(ue_usr);
}

int Gnss_Assist_Source :: decl_dev_param(enum ue_dev_param_id   id, 
                                         void *param_desc, int num)
{
        CPP_FTRACE2();

        ADAP_IF_FN(adap_if.decl_dev_param, adap_if.hnd, id, param_desc, num);
        
        return 0;
}
