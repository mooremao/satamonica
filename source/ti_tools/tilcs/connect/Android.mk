ifeq ($(TARGET_ARCH),arm)

LOCAL_PATH             := $(call my-dir)

###########################################
# Build libagnss.so
###########################################
include $(CLEAR_VARS)
LOCAL_MODULE           := libagnss
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0 -fno-rtti -fno-exceptions
LOCAL_LDFLAGS          += -g 
else
LOCAL_CFLAGS           += -O2 -fno-rtti -fno-exceptions
endif
LOCAL_CFLAGS           +=  -DENABLE_TRACE=1 -DENABLE_DEBUG=1
LOCAL_SHARED_LIBRARIES := libcutils liblog libstlport libclientlogger
LOCAL_STATIC_LIBRARIES := libstdc++

LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                            \
	config/Connect_Config.cpp                    \
	common/a-gnss_comm.cpp                       \
	common/agnss_ipc.cpp                         \
	common/a-gnss_xact.cpp                       \
	common/ti_agnss_lcs.cpp                      \
	debug/debug.c                                \
	debug/log_functions.c                        \
	debug/Ftrace.cpp

LOCAL_C_INCLUDES       :=                            \
	$(LOCAL_PATH)/../include/common              \
	$(LOCAL_PATH)/../include/connect             \
	$(LOCAL_PATH)/../include/dproxy              \
	$(LOCAL_PATH)/include                        \
	$(LOCAL_PATH)/core                           \
	$(LOCAL_PATH)/common                         \
	$(LOCAL_PATH)/debug                          \
	$(LOCAL_PATH)/config                         \
	$(LOCAL_PATH)/../infrastructure/log/include		\
	hardware/libhardware/include/hardware        \
	external/stlport/stlport                     \
	bionic
	
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_SHARED_LIBRARY)

###########################################
# Build libgnssutils.so
###########################################
include $(CLEAR_VARS)
LOCAL_MODULE           := libgnssutils
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2
endif
LOCAL_CFLAGS           +=  -DENABLE_TRACE=1 -DENABLE_DEBUG=1
LOCAL_SHARED_LIBRARIES := libcutils liblog libagnss libclientlogger
LOCAL_LDLIBS           += -lm -lpthread

LOCAL_SRC_FILES        :=                        	\
	utils/3gpp_utils.c	                     	\
	utils/gnss_utils.c	                     		 

LOCAL_C_INCLUDES       :=                        	\
	$(LOCAL_PATH)/../include/common              	\
	$(LOCAL_PATH)/../include/connect             	\
	$(LOCAL_PATH)/../include/dproxy              	\
	$(LOCAL_PATH)/debug/              		   	\
	$(LOCAL_PATH)/../infrastructure/log/include		\
	hardware/libhardware/include/hardware        	\
	external/stlport/stlport                     	\
    bionic/libc/include                   		\
    bionic/libc/private
	
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_SHARED_LIBRARY)

###########################################
# Build libassist.so
###########################################
include $(CLEAR_VARS)
LOCAL_MODULE           := libassist
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2
endif
LOCAL_CFLAGS           +=  -DENABLE_TRACE=1 -DENABLE_DEBUG=1
LOCAL_SHARED_LIBRARIES := libcutils liblog libdevproxy libagnss libgnssutils libclientlogger
LOCAL_LDLIBS           += -lm -lpthread

LOCAL_SRC_FILES        :=                           \
	assist/ext_assist.c			   	   \
	assist/gps_visible_eph.c				

LOCAL_C_INCLUDES       :=  		                 \
	$(LOCAL_PATH)/assist/			   \
	$(LOCAL_PATH)/../include/common              \
	$(LOCAL_PATH)/../include/dproxy              \
	$(LOCAL_PATH)/debug/              		   \
	$(LOCAL_PATH)/../infrastructure/log/include		\
	hardware/libhardware/include/hardware        \
	external/stlport/stlport                     \
    	bionic/libc/include                   	   \
    	bionic/libc/private
	
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_SHARED_LIBRARY)

########################################
# Build agnss_connect
########################################
include $(CLEAR_VARS)
LOCAL_MODULE           := agnss_connect
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0 -Wall -fno-rtti -fno-exceptions
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2 -Wall -fno-rtti -fno-exceptions
endif
LOCAL_CFLAGS           +=  -DENABLE_TRACE=1 -DENABLE_DEBUG=1
LOCAL_SHARED_LIBRARIES :=                        \
	libstdc++ libcutils liblog libstlport        \
	libdevproxy libagnss libassist libgnssutils libclientlogger

SUPL_FEATURE = 'n'
ifeq ($(SUPL_FEATURE), 'y')
LOCAL_SHARED_LIBRARIES += libsupl20if
endif

LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                        \
	core/Agnss_Connect.cpp                       \
	core/Agnss_Services.cpp                      \
	core/Assist_Source.cpp                       \
	core/Base_Entity.cpp                         \
	core/Gnss_Assist_Source.cpp                  \
	core/Gnss_Receiver.cpp                       \
	core/Gnss_Utils.cpp                          \
	core/Main.cpp                                \
	core/Remote_Client.cpp                       \
	adapter/gnss_ue_adapter.cpp                  \
	adapter/ti_agnss_assist_sa_pgps.cpp          \
	adapter/ti_agnss_lcs_pfm_app.cpp             \
        $(SUPL_C_FILES) \
	adapter/ti_agnss_assist_ctrl_plane.cpp	     

ifeq ($(SUPL_FEATURE), 'y')
SUPL_C_FILES :=	adapter/gnss_supl_adapter.cpp                \
		adapter/ti_agnss_assist_suplif.cpp           \
		adapter/ti_agnss_assist_suplni.cpp           
endif

LOCAL_C_INCLUDES       :=                            \
	$(LOCAL_PATH)/../include/common              \
	$(LOCAL_PATH)/../include/connect             \
	$(LOCAL_PATH)/../include/dproxy              \
	$(LOCAL_PATH)/assist		             \
	$(LOCAL_PATH)/include                        \
	$(LOCAL_PATH)/core                           \
	$(LOCAL_PATH)/common                         \
	$(LOCAL_PATH)/debug                          \
	$(LOCAL_PATH)/config                         \
	$(LOCAL_PATH)/../infrastructure/log/common	\
	$(LOCAL_PATH)/../infrastructure/log/adapter	\
	$(LOCAL_PATH)/../infrastructure/log/adapter/cmcc	\
	$(SUPL_H_FILES) \
	$(LOCAL_PATH)/../infrastructure/log/include		\
	hardware/libhardware/include/hardware        \
	external/stlport/stlport                     \
	bionic
	
ifeq ($(SUPL_FEATURE), 'y')
SUPL_H_FILES := $(LOCAL_PATH)/../../assist/SUPL/client/supl_if/inc_if 
endif

LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_EXECUTABLE)

#########################################################
include $(CLEAR_VARS)
LOCAL_MODULE           := pfmapp
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0 -Wall -fno-rtti -fno-exceptions
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2 -Wall -fno-rtti -fno-exceptions
endif
LOCAL_CFLAGS           +=  -DENABLE_TRACE=1 -DENABLE_DEBUG=1
LOCAL_SHARED_LIBRARIES :=                        \
        libstdc++ libcutils liblog libstlport        \
        libdevproxy libagnss libassist libgnssutils libclientlogger
		
LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                        \
        test/lcs_usr_test.cpp

LOCAL_C_INCLUDES       :=                            \
        $(LOCAL_PATH)/../include/common              \
        $(LOCAL_PATH)/../include/connect             \
        $(LOCAL_PATH)/../include/dproxy              \
        $(LOCAL_PATH)/assist                         \
        $(LOCAL_PATH)/include                        \
		$(LOCAL_PATH)/core                           \
        $(LOCAL_PATH)/common                         \
        $(LOCAL_PATH)/debug                          \
        $(LOCAL_PATH)/config                         \
        $(LOCAL_PATH)/../infrastructure/log/common      \
        $(LOCAL_PATH)/../infrastructure/log/adapter     \
        $(LOCAL_PATH)/../infrastructure/log/adapter/cmcc        \
        $(LOCAL_PATH)/../infrastructure/log/include             \
        bionic


LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_EXECUTABLE)


endif
