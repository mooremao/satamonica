#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include "Ftrace.h"
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include <sys/stat.h>

#define MAX_FSTR_SIZE 8000
static char  fstr[MAX_FSTR_SIZE];  // String place holder
static FILE *flog;
static int   n_line;               /* # of lines */

static const unsigned int N_LINE_MAP = 0xFFFF; /* 32767 lines */
static pthread_mutex_t fmutex;

unsigned int Ftrace :: nest_lvl = 0;

static const char* fnest2indent[] = {
	"\t",
	"\t|\t",
	"\t|\t|\t",
	"\t|\t|\t|\t",
	"\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t",
	"\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t|\t"
};

  
struct fthread_desc {

	char           name[50];
	unsigned int   t_id;
	unsigned int   nest;
};

#define MAX_FTHREAD  20 
struct fthread_desc  fthread_info[MAX_FTHREAD];
static int n_fthread = 0;

static char *fthread_name(unsigned int t_id) 
{
	int i = 0;
	static char ret[] = "unknown";
	for(i = 0; i < MAX_FTHREAD; i++) {
		if(fthread_info[i].t_id == t_id) {
			return fthread_info[i].name;
		}
	}
	
	return ret;
}


static void inline seek_fline()
{
        if(0 == (++n_line & N_LINE_MAP)) {
                fseek(flog, 0, SEEK_SET);
        }

}

Ftrace :: Ftrace(const char *file, const char *func, int line) 
{
	pthread_mutex_lock(&fmutex);

	int len = 0;
	unsigned int secs = 0;
	unsigned int usec = 0;
	struct timeval tv;
	if(0 == gettimeofday(&tv, NULL)) {
		secs = tv.tv_sec;
		usec = tv.tv_usec;
	}

	nest = nest_lvl++;
	len += sprintf(fstr + len, "%s", fnest2indent[nest]);
	len += sprintf(fstr + len, "[0x%x]", this);
	len += sprintf(fstr + len, "[%u.%u] ", secs, usec);
	len += sprintf(fstr + len, "%s:%d Enter [%s]\n", 
                       func, line, fthread_name(pthread_self()));
	
	
	fprintf(flog, "%s", fstr);
	fflush(flog);
	
	seek_fline();
	
	pthread_mutex_unlock(&fmutex);
}

Ftrace :: ~Ftrace()
{
	pthread_mutex_lock(&fmutex);

	nest_lvl--;
	int len = 0;
	
	len += sprintf(fstr + len, "%s",   fnest2indent[nest]);
	len += sprintf(fstr + len, "[0x%x] ", this);
	len += sprintf(fstr + len, "Exit\n"); 
	
	seek_fline();
	fprintf(flog, "%s", fstr);
	fflush(flog);

	pthread_mutex_unlock(&fmutex);
}

static void inline init_fline()
{
        if(n_line > (N_LINE_MAP + 1))  {
                fseek(flog, 0, SEEK_SET);
                n_line = 0;
        }
} 


#define FTRACE_LINE \
        "********************************************************************\n"

FILE *ftrace_init(const char *log_file)
{
	struct stat status;	
	
	const char *mode = (0 == stat(log_file, &status))? "r+" : "w+";
	
	flog = fopen(log_file, mode);
	if(!flog)
		return NULL;

	pthread_mutex_init(&fmutex, NULL);
        
        while(fgets(fstr, MAX_FSTR_SIZE, flog)) {
                n_line++;
        }
        
	init_fline();
        
	time_t ttime = time(NULL);
	fprintf(flog, "\n%s*\tFTrace Date --> %s%s\n", FTRACE_LINE,     \
                ctime(&ttime), FTRACE_LINE);
	fflush(flog);
        
	return flog;
}

int ftrace_add_fthread(unsigned int t_id, const char *name)
{
	int dst_size = 0;
	if(n_fthread < MAX_FTHREAD) {
		fthread_info[n_fthread].t_id = t_id;
		//strcpy(fthread_info[n_fthread].name, name);
		dst_size = sizeof(fthread_info[n_fthread].name);
		strncpy(fthread_info[n_fthread].name, name, dst_size -1);
		fthread_info[n_fthread].name[dst_size -1] = '\0';
		n_fthread++;
		return 0; 
	}
	
	return -1;
}

/*
int main()
{
	ftrace_init("connect.ftrace");
	ftrace_add_fthread(getpid(), "h2d");
	CPP_FTRACE1();
}*/
