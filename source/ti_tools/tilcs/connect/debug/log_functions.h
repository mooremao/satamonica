
#ifndef _LOG_FUNCTIONS_H
#define _LOG_FUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

/* Declarations */
void print_vel_gnss_info_data(void *data,int n_obj);
void print_loc_gnss_info_data(void *data,int n_obj);
void print_location_desc_data(void *data,int n_obj);
void print_loc_ganss_info_data(void *data,int n_obj);
void print_glo_utc_model_data(void *data,int n_obj);
void print_glo_nav_model_data(void *data,int n_obj);
void print_glo_orb_model_data(void *data,int n_obj);
void print_glo_alm_kp_set_data(void *data,int n_obj);
void print_gps_acq_data(void *data,int n_obj);
void print_gps_ref_time_data(void *data,int n_obj);
void print_gps_atow_data(void *data,int n_obj);

void print_glo_clk_model_data(void *data, int n_obj);
void print_ganss_ion_model_data(void *data, int n_obj);
void print_ganss_ref_msr_data(void *data, int n_obj);
void print_ganss_time_model_data(void *data, int n_obj);
void print_ganss_rti_data(void *data, int n_obj);
void print_dganns_sat_data(void *data, int n_obj);
void print_ganss_sig_info_data(void *data, int n_obj);
void print_ganss_msr_info_data(void *data, int n_obj);
void print_gnss_pos_result_data(void *data, int n_obj);
void print_ganss_msr_result_data(void *data, int n_obj);
void print_gps_msr_result_data(void *data, int n_obj);
void print_gnss_loc_err_data(void *data, int n_obj);
void print_ganss_assist_req_data(void *data, int n_obj);
void print_ganss_req_info_data(void *data, int n_obj);
void print_sat_nav_req_info_data(void *data, int n_obj);
void print_sat_nav_data(void *data, int n_obj);
void print_ganss_ue_assist_caps_data(void *data, int n_obj);
void print_ganss_assist_caps_data(void *data, int n_obj);
void print_ganss_data_model_data(void *data, int n_obj);
void print_ganss_ue_lcs_caps_data(void *data, int n_obj);
void print_ganss_ref_time_data(void *data, int n_obj);
void print_ganss_addl_iono_data(void *data, int n_obj);
void print_dgps_sat_data(void *data, int n_obj);

#ifdef __cplusplus
}
#endif

#endif
