#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdarg.h>
#ifdef ANDROID
#include <utils/Log.h>
#else
#include <pthread.h>
#endif
#include "debug.h"

int dbg_level = 4;
static int time_flag = 1;

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
static const unsigned int N_LINE_MAP = 0xFFFF; /* 16383 lines */

#define     MAX_STR_SIZE      4096
static char init_str[MAX_STR_SIZE];

static inline void init_fline(struct log_desc *ldesc)
{
        if(ldesc->n_line > (N_LINE_MAP + 1)) {
                fseek(ldesc->l_file, 0, SEEK_SET);
                ldesc->n_line = 0;
        }
} 

static inline void seek_fline(struct log_desc *ldesc)
{
        pthread_mutex_lock(&mtx);

        if(0 == (++ldesc->n_line & N_LINE_MAP)) {
                fseek(ldesc->l_file, 0, SEEK_SET);
        }

        pthread_mutex_unlock(&mtx);
}

void dbg_print(struct log_desc *ldesc, const char *prefix, 
               const char *tag, int line, const char *fmt, ...)
{
        FILE *lfile = NULL;
        struct timeval tv;
        unsigned int secs = 0, usec = 0; 

        if((NULL == ldesc) || (NULL == ldesc->l_file))
		return;

        if(time_flag && (0 == gettimeofday(&tv, NULL))) {
                secs = tv.tv_sec;
                usec = tv.tv_usec;
        }

        lfile = ldesc->l_file;

        fprintf(lfile, "[%u.%u] [%s] %s:%d: ", secs, usec, prefix, tag, line);
        
	va_list args;
	va_start(args, fmt);
	vfprintf(lfile, fmt, args);
	va_end(args);
        
	fprintf(lfile, "\n");
	fflush(lfile);

        seek_fline(ldesc);
}

struct log_desc dbg_log;

FILE* debug_init(const char *log_file)
{
        struct log_desc *dlog = &dbg_log;
        struct stat status;

        char *mode = (0 == stat(log_file, &status))? "r+" : "w+";
        dlog->l_file = fopen(log_file, mode);
        if(NULL == dlog->l_file)
                return NULL;

        while(fgets(init_str, MAX_STR_SIZE, dlog->l_file)) {
                dlog->n_line++;
        }
	        
        init_fline(dlog);

	time_t dtime = time(NULL);
        
	fprintf(dlog->l_file, "\n");
	fprintf(dlog->l_file, "*****************************************************\n");
	fprintf(dlog->l_file, "* Debug Log start time: %s",               ctime(&dtime));
	fprintf(dlog->l_file, "*****************************************************\n");
	fprintf(dlog->l_file, "\n");
	fflush(dlog->l_file);
        
	return dlog->l_file;
}

void debug_exit(FILE *file)
{
	fclose(file);
}

struct log_desc msg_log;

FILE* dmsg_init(const char *log_file)
{
        struct log_desc *mlog = &msg_log;
        struct stat status;
        
        char *mode = (0 == stat(log_file, &status))? "r+" : "w+";
        mlog->l_file = fopen(log_file, mode);
		if(NULL == mlog->l_file)
			return NULL;
        
        while(fgets(init_str, MAX_STR_SIZE, mlog->l_file)) {
                mlog->n_line++;
        }
        
        init_fline(mlog);

	time_t mtime = time(NULL);

        fprintf(mlog->l_file, "\n");
        fprintf(mlog->l_file, "*****************************************************\n");
        fprintf(mlog->l_file, "* Message Log start time: %s",             ctime(&mtime));
        fprintf(mlog->l_file, "*****************************************************\n");
        fprintf(mlog->l_file, "\n");
        fflush(mlog->l_file);
        
        return mlog->l_file;
}

void dmsg_exit(FILE *file)
{
	fclose(file);
}

void dmsg_print(struct log_desc *ldesc, const char *prefix, const char *module, 
                int line, dump_fn fn, void *data, int n_obj)
{
#if  1
	dbg_print(ldesc, prefix, module, line, "\n");
	if(fn)
		fn(data, n_obj);
#endif
}

void debug_init_log()
{
}

void debug_exit_log()
{
//	exit_log();
}
/*
void test_fn(void *data, int n_obj)
{
        MSGOUT("data = %u\n", (unsigned int)data);
        MSGOUT("_num = %d\n", n_obj);
	return; 
}*/

/*int main()
{
	debug_init("connect.idebug");
	DBG_L1("Hello - does this work?");
	DBG_L2("Hello - does this work? %x", 10);
	DBG_L3("Hello - does this work?");

	ERR_NM("This a test");

	dmsg_init("connect.message");
	MSG_L1(test_fn, NULL, 1);
	return 0;
}*/
