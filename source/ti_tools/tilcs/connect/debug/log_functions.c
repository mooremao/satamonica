

#include "gnss.h"
#include "debug.h"
#include "log_functions.h"

/* Definition */
void print_vel_gnss_info_data(void *data, int n_obj)
{
    int n = 0;
	struct vel_gnss_info *p = (struct vel_gnss_info *) data;

		MSGOUT("Name of structure: vel_gnss_info  \n");   
	
	for(n = 0; n < n_obj; n++) {	
    	MSGOUT("%-20s[%-10u]\n", "type", p->type);   		/* Enum */    
		MSGOUT("%-20s[%-10d]\n", "bearing", p->bearing);  	/* U2B */
		MSGOUT("%-20s[%-10d]\n","h_speed", p->h_speed);   	/* U2B */
		MSGOUT("%-20s[%-10d]\n","ver_dir", p->ver_dir);   	/* U1C */
		MSGOUT("%-20s[%-10d]\n","v_speed", p->v_speed);   	/* U1C */
		MSGOUT("%-20s[%-10d]\n","h_unc", p->h_unc);   		/* U1C */
		MSGOUT("%-20s[%-10d]\n","v_unc", p->v_unc);   		/* U1C */
		p++;
	}												
}

void print_loc_gnss_info_data(void *data,int n_obj)
{
	int n = 0;
	struct loc_gnss_info *p = (struct loc_gnss_info *) data;
				
		MSGOUT("Name of structure: loc_gnss_info  \n");   

	for(n = 0; n < n_obj; n++) {	
		MSGOUT("%-20s[%-10u]\n","gnss_tid", p->gnss_tid);   /* Enum */
		MSGOUT("%-20s[%-10d]\n","ref_secs", p->ref_secs);   /* U4B */
		MSGOUT("%-20s[%-10d]\n", "ref_msec", p->ref_msec);  /* U4B */
		MSGOUT("%-20s[%-10u]\n","fix_type", p->fix_type);   /* Enum */
		MSGOUT("%-20s[%-10d]\n", "pos_bits", p->pos_bits);  /* U4B */
		p++;
	}										
}

void print_location_desc_data(void *data,int n_obj)
{
	int n = 0;
	struct location_desc *p = (struct location_desc *) data;
						
		MSGOUT("Name of structure:  location_desc  \n");   

	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10u]\n","shape", p->shape);   	   			 /* Enum */
		MSGOUT("%-20s[%-10d]\n","latitude_N", p->latitude_N);   	   /* U4B */
		MSGOUT("%-20s[%-10d]\n","longitude _N", p->longitude_N);       /* S4B */
		MSGOUT("%-20s[%-10d]\n","altitude_N", p->altitude_N);   	   /* U2B */
		MSGOUT("%-20s[%-10d]\n","unc_semi_major_K", p->unc_semi_maj_K);/* U1C */
		MSGOUT("%-20s[%-10d]\n","unc_semi_minor_K", p->unc_semi_min_K);/* U1C */
		MSGOUT("%-20s[%-10d]\n","orientation", p->orientation);   	   /* U1C */	
		MSGOUT("%-20s[%-10d]\n","unc_altitude_K", p->unc_altitude_K);  /* U1C */
		MSGOUT("%-20s[%-10d]\n","confidence", p->confidence);   	   /* U1C */
		p++;
		
	}
}

void print_glo_utc_model_data(void *data,int n_obj)
{
	int n = 0;
	struct glo_utc_model *p = (struct glo_utc_model *) data;
			
	MSGOUT("Name of the structure: glo_utc_model\n");   

	for(n = 0; n < n_obj; n++) {			
		MSGOUT("%-20s[%-10d]\n","NA", p->NA);   		/* U2B */
		MSGOUT("%-20s[%-10d]\n","TauC", p->TauC);   	/* S4B */
		MSGOUT("%-20s[%-10d]\n","B1", p->B1);   		/* S2B */
		MSGOUT("%-20s[%-10d]\n","B2", p->B2);   		/* S2B */
		MSGOUT("%-20s[%-10d]\n","KP", p->KP);   		/* U1C */
		p++;
	}
}

void print_glo_nav_model_data(void *data,int n_obj)
{
	int n = 0;
	struct glo_nav_model *p = (struct glo_nav_model *) data;
		
			
		MSGOUT("Name of the structure: glo_nav_model\n");  

	for(n = 0; n < n_obj; n++) {						
		MSGOUT("%-20s[%-10d]\n","svid", p->the_svid);   	/* U1C */
		MSGOUT("%-20s[%-10d]\n","is_bcast", p->is_bcast); 	/* U1C */
		MSGOUT("%-20s[%-10d]\n","Bn", p->Bn);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","FT", p->FT);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","tb", p->tb);   			/* U1C */
		p++;
	}
}

void print_glo_orb_model_data(void *data,int n_obj)
{	
	int n = 0;
	struct glo_orb_model *p = (struct glo_orb_model *) data;			
			
		MSGOUT("Name of the structure: glo_orb_model \n");   

	for(n = 0; n < n_obj; n++) {			
		MSGOUT("%-20s[%-10d]\n","En", p->En);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","P1", p->P1);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","P2", p->P2);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","M", p->M);   				/* U1C */
		MSGOUT("%-20s[%-10d]\n","X", p->X);   				/* S4B */
		MSGOUT("%-20s[%-10d]\n","X_dot", p->X_dot); 		/* S4B */
		MSGOUT("%-20s[%-10d]\n","X_dot_dot", p->X_dot_dot); /* U1C */
		MSGOUT("%-20s[%-10d]\n","Y", p->Y);   				/* S4B */
		MSGOUT("%-20s[%-10d]\n","Y_dot", p->Y_dot);   		/* S4B */
		MSGOUT("%-20s[%-10d]\n","Y_dot_dot", p->Y_dot_dot); /* U1C */
		MSGOUT("%-20s[%-10d]\n","Z", p->Z);   				/* S4B */
		MSGOUT("%-20s[%-10d]\n","Z_dot", p->Z_dot);   		/* S4B */
		MSGOUT("%-20s[%-10d]\n","Z_dot_dot", p->Z_dot_dot); /* U1C */	
		p++;
	}
}

void print_glo_alm_kp_set_data(void *data,int n_obj)
{	
	int n = 0;
	struct glo_alm_kp_set *p = (struct glo_alm_kp_set *) data;
						
		MSGOUT("Name of the structure: glo_alm_kp_set\n");  

	for(n = 0; n < n_obj; n++) {				
		MSGOUT("%-20s[%-10d]\n","wna", p->wna);   				/* U1C */
		MSGOUT("%-20s[%-10d]\n","is_toa", p->is_toa);   		/* U1C */
		MSGOUT("%-20s[%-10d]\n","toa", p->toa);   				/* U1C */
		MSGOUT("%-20s[%-10d]\n","ioda", p->ioda);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","NA", p->NA);   				/* U2B */
		MSGOUT("%-20s[%-10d]\n","nA", p->nA);   				/* U1C */
		MSGOUT("%-20s[%-10d]\n","HnA", p->HnA);   				/* U1C */
		MSGOUT("%-20s[%-10d]\n","Lambda_nA", p->Lambda_nA);   	/* S4B */
		MSGOUT("%-20s[%-10d]\n","t_Lambda_nA", p->t_Lambda_nA); /* U4B */
		MSGOUT("%-20s[%-10d]\n","Delta_i_nA", p->Delta_i_nA);   /* S4B */
		MSGOUT("%-20s[%-10d]\n","Delta_T_nA", p->Delta_T_nA);   /* S4B */
		MSGOUT("%-20s[%-10d]\n","Delta_T_dot_nA", p->Delta_T_dot_nA);/* S1C */
		MSGOUT("%-20s[%-10d]\n","Epsilon_nA", p->Epsilon_nA);   /* U2B  */
		MSGOUT("%-20s[%-10d]\n","Omega_nA", p->Omega_nA);   	/* S2B  */
		MSGOUT("%-20s[%-10d]\n","Tau_nA", p->Tau_nA);   		/* S2B  */
		MSGOUT("%-20s[%-10d]\n","C_nA", p->C_nA);   			/* U1C  */
		MSGOUT("%-20s[%-10d]\n","M_nA", p->M_nA);   			/* U1C */
		p++;
		
	}
}

void print_gps_acq_data(void *data,int n_obj)
{	
	int n = 0;
	struct gps_acq *p = (struct gps_acq *) data;			

	MSGOUT("Name of the structure: gps_acq\n");  

	for(n = 0; n < n_obj; n++) {					
		MSGOUT("%-20s[%-10u]\n","tow_unit",p->tow_unit); 		/* Enum */
		MSGOUT("%-20s[%-10d]\n","gps_tow", p->gps_tow);   		/* U4B */	
		MSGOUT("%-20s[%-10d]\n","svid", p->svid);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","Doppler0", p->Doppler0);  		/* U2B */	
		MSGOUT("%-20s[%-10d]\n","Doppler1", p->Doppler1);  		/* U1C */
		MSGOUT("%-20s[%-10d]\n","DopplerUnc", p->DopplerUnc);	/* U1C */	
		MSGOUT("%-20s[%-10d]\n","CodePhase",p->CodePhase);   	/*U2B */
		MSGOUT("%-20s[%-10d]\n","IntCodePhase", p->IntCodePhase);/* U1C */
		MSGOUT("%-20s[%-10d]\n","GPSBitNum", p->GPSBitNum); 	/* U1C */
		MSGOUT("%-20s[%-10d]\n","SrchWin", p->SrchWin);   		/* U1C */	
		MSGOUT("%-20s[%-10d]\n","Azimuth", p->Azimuth);   		/*U1C */
		MSGOUT("%-20s[%-10d]\n","Elevation", p->Elevation);   	/* U1C */
		p++;
	}
}

void print_gps_ref_time_data(void *data,int n_obj)
{			
	int n = 0;
	struct gps_ref_time *p = (struct gps_ref_time *) data;
			
		MSGOUT("Name of the structure: gps_ref_time\n"); 
			
	for(n = 0; n < n_obj; n++) {
		MSGOUT("%-20s[%-10u]\n","accuracy", p->accuracy); 	/* Enum */
		MSGOUT("%-20s[%-10d]\n","time_unc", p->time_unc); 	/* U4B */	
		MSGOUT("%-20s[%-10d]\n","n_atow", p->n_atow);  		/* U1C */
		p++;
	}
}

void print_gps_atow_data(void *data,int n_obj)
{			
	int  n = 0;
	struct gps_atow *p = (struct gps_atow *) data;
		
		MSGOUT("Name of the structure: gps_atow\n");  

	for(n = 0; n < n_obj; n++) {				
		MSGOUT("%-20s[%-10d]\n","svid", p->svid);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","tlm_word", p->tlm_word);  		/* U2B */
		MSGOUT("%-20s[%-10d]\n","tlm_rsvd", p->tlm_rsvd);  		/* U1C */
		MSGOUT("%-20s[%-10d]\n","alert_flag", p->alert_flag); 	/* U1C */
		MSGOUT("%-20s[%-10d]\n","spoof_flag", p->spoof_flag); 	/* U1C */

		p++;
	}											
}

void print_glo_clk_model_data(void *data, int n_obj)
{
	int  n = 0;
	struct glo_clk_model *p = (struct glo_clk_model *) data;
		
		MSGOUT("Name of the structure: glo_clk_model\n");   
		
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","Tau", p->Tau);   			/* S4B */
		MSGOUT("%-20s[%-10d]\n","Gamma", p->Gamma);   		/* S2B */
		MSGOUT("%-20s[%-10d]\n","Delta_Tau", p->Delta_Tau); /* S1C */
		p++;
	}

}

void print_ganss_ion_model_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_ion_model *p = (struct ganss_ion_model *) data;
		
		MSGOUT("Name of the structure: ganss_ion_model\n");   
	
	for(n = 0; n < n_obj; n++) {
		MSGOUT("%-20s[%-10d]\n","a0", p->a0);   /* U2B */
		MSGOUT("%-20s[%-10d]\n","a1", p->a1);   /* U2B */
		MSGOUT("%-20s[%-10d]\n","a2", p->a2);   /* U2B */
		p++;
	}
}

void print_ganss_ref_msr_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_ref_msr *p = (struct ganss_ref_msr *) data;		

		MSGOUT("Name of the structure: ganss_ref_msr\n");   
	for(n = 0; n < n_obj; n++) {

		MSGOUT("%-20s[%-10d]\n","svid", p->svid);   				/* U1C */
		MSGOUT("%-20s[%-10d]\n","doppler0", p->doppler0);   		/* U2B */
		MSGOUT("%-20s[%-10d]\n","doppler1", p->doppler1);   		/* S1C */
		MSGOUT("%-20s[%-10d]\n","doppler_unc", p->doppler_unc);		/* U1C */
		MSGOUT("%-20s[%-10d]\n","code_phase", p->code_phase);		/* U2B */
		MSGOUT("%-20s[%-10d]\n","int_code_phase", p->int_code_phase);/* U1C */
		MSGOUT("%-20s[%-10d]\n","cp_search_win", p->cp_search_win);  /* U1C */
		MSGOUT("%-20s[%-10d]\n","azimuth", p->azimuth);   			/* U1C */
		MSGOUT("%-20s[%-10d]\n","elevation", p->elevation);  		/* U1C */
		p++;
	}
}

void print_ganss_time_model_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_time_model *p = (struct ganss_time_model *) data;
	
		MSGOUT("Name of the structure: ganss_time_model\n");   
	
	for(n = 0; n < n_obj; n++) {

		MSGOUT("%-20s[%-10d]\n","t_A0", p->ganss_id);   		/* U1C */
		MSGOUT("%-20s[%-10d]\n","t_A1", p->ref_time16s);		/* U2B */		
		MSGOUT("%-20s[%-10d]\n","t_A0", p->t_A0);   			/* S4B */
		MSGOUT("%-20s[%-10d]\n","t_A1", p->t_A1);   			/* S4B */
		MSGOUT("%-20s[%-10d]\n","t_A2", p->t_A2);   			/* S1C */
		MSGOUT("%-20s[%-10d]\n","gnss_TO_id", p->gnss_TO_id); 	/* U1C */
		MSGOUT("%-20s[%-10d]\n","ref_week", p->ref_week);     	/* U2B */
		p++;
	}
}

void print_ganss_rti_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_rti *p = (struct ganss_rti *) data;

		MSGOUT("Name of the structure: ganss_rti\n");   
	
	for(n = 0; n < n_obj; n++) {
		
		MSGOUT("%-20s[%-10d]\n","ganss_id", p->ganss_id);   /* U1C  */
		MSGOUT("%-20s[%-10d]\n","bad_svid", p->bad_svid);   /* U1C  */
		MSGOUT("%-20s[%-10d]\n","sig_bits", p->sig_bits);   /* U1C  */
		p++;
	}
}

void print_dganns_sat_data(void *data, int n_obj)
{
	int  n = 0;
	struct dganss_sat *p = (struct dganns_sat *) data;

		MSGOUT("Name of the structure: dganns_sat\n");   
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","ganss_id", p->ganss_id); 	/* U1C */
		MSGOUT("%-20s[%-10d]\n","ref_time", p->ref_time);   /* U1C */
		MSGOUT("%-20s[%-10d]\n","health", p->health);  		/* U1C*/
		MSGOUT("%-20s[%-10d]\n","svid", p->svid);   		/* U1C */
		MSGOUT("%-20s[%-10d]\n","iod", p->iod);   			/* U2B */
		MSGOUT("%-20s[%-10d]\n","udre", p->udre);   		/* U1C */
		MSGOUT("%-20s[%-10d]\n","prc", p->prc);   			/* S2B */
		MSGOUT("%-20s[%-10d]\n","rrc", p->rrc);   			/* S1C */
		p++;
	}
}

void print_ganss_sig_info_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_sig_info *p = (struct ganss_sig_info *) data;

		MSGOUT("Name of the structure: ganss_sig_info\n");  
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","ganss_id", p->ganss_id);   /* U1C */
		MSGOUT("%-20s[%-10d]\n","signal_id", p->signal_id); /* U1C */
		MSGOUT("%-20s[%-10d]\n","ambiguity", p->contents);  /* U1C */
		MSGOUT("%-20s[%-10d]\n","svid", p->ambiguity);   	/* U1C */
		MSGOUT("%-20s[%-10d]\n","n_sat", p->n_sat);   	  	/* U1C */
		p++;
	}
}


void print_ganss_msr_info_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_msr_info *p = (struct ganss_msr_info *) data;

		MSGOUT("Name of the structure: ganss_msr_info\n");   
}


void print_gnss_pos_result_data(void *data, int n_obj)
{
	int  n = 0;
	struct gnss_pos_result *p = (struct gnss_pos_result *) data;

		MSGOUT("Name of the structure: gnss_pos_result\n");   
	
	for(n = 0; n < n_obj; n++) {
		
		MSGOUT("%-20s[%-10d]\n","contents", p->contents);   /* U4B */
		MSGOUT("%-20s[%-10u]\n","gnss_tid", p->gnss_tid);   /* Enum */
		MSGOUT("%-20s[%-10u]\n","ref_secs", p->ref_secs);   /* U4B */
		MSGOUT("%-20s[%-10d]\n","ref_msec", p->ref_msec);   /* U4B */
		MSGOUT("%-20s[%-10u]\n","fix_type", p->fix_type);   /* Enum  */
		MSGOUT("%-20s[%-10d]\n","pos_bits", p->pos_bits);   /* U4B */
	
		p++;
	}
}

void print_ganss_msr_result_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_msr_result *p = (struct ganss_msr_result *) data;

		MSGOUT("Name of the structure: ganss_msr_result\n");
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","contents", p->contents);   /* U4B */
		MSGOUT("%-20s[%-10d]\n","h_acc_K", p->h_acc_K);     /* U1C */
		MSGOUT("%-20s[%-10d]\n","v_acc_K", p->v_acc_K);     /* U1C */
		MSGOUT("%-20s[%-10d]\n","ref_secs", p->ref_secs);   /* U4B */
		MSGOUT("%-20s[%-10d]\n","ref_msec", p->ref_msec);   /* U4B  */
		MSGOUT("%-20s[%-10d]\n","n_ganss", p->n_ganss);     /* U1C */	
		p++;
	}
}

void print_gps_msr_result_data(void *data, int n_obj)
{
	int  n = 0;
	struct gps_msr_result *p = (struct gps_msr_result *) data;

		MSGOUT("Name of the structure: gps_msr_result\n");  
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","contents", p->contents);   /* U4B */
		MSGOUT("%-20s[%-10d]\n","h_acc_K", p->h_acc_K);     /* U1C */
		MSGOUT("%-20s[%-10d]\n","v_acc_K", p->v_acc_K);     /* U1C */
		MSGOUT("%-20s[%-10d]\n","ref_secs", p->ref_secs);   /* U4B */
		MSGOUT("%-20s[%-10d]\n","ref_msec", p->ref_msec);   /* U4B */
		MSGOUT("%-20s[%-10d]\n","n_sat", p->n_sat);         /* U1C */	
		p++;
	}
}

void print_gnss_loc_err_data(void *data, int n_obj)
{
	int  n = 0;
	struct gnss_loc_err *p = (struct gnss_loc_err *) data;

		MSGOUT("Name of the structure: gnss_loc_err\n");   
	
	for(n = 0; n < n_obj; n++) {
		
		MSGOUT("%-20s[%-10u]\n","error_id", p->error_id);   /* Enum */
		MSGOUT("%-20s[%-10d]\n","contents", p->contents);   /* U4B  */
		
		p++;
	}
}

void print_ganss_assist_req_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_assist_req *p = (struct ganss_assist_req *) data;

		MSGOUT("Name of the structure: ganss_assist_req\n");   
	
	for(n = 0; n < n_obj; n++) {
		
		MSGOUT("%-20s[%-10d]\n","common_req_map", p->common_req_map);/* U4B */
		MSGOUT("%-20s[%-10d]\n","n_of_ganss_req", p->n_of_ganss_req);/* U4B */
		
		p++;
	}
}

void print_ganss_req_info_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_req_info *p = (struct ganss_req_info *) data;

		MSGOUT("Name of the structure: ganss_req_info\n");   
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","id_of_ganss", p->id_of_ganss); /* U4B */
		MSGOUT("%-20s[%-10d]\n","req_bitmap", p->req_bitmap); /* U4B */
		MSGOUT("%-20s[%-10d]\n","dganss_req", p->dganss_req); /*U1C */		
		p++;
	}
}

void print_sat_nav_req_info_data(void *data, int n_obj)
{
	int  n = 0;
	struct sat_nav_req_info *p = (struct sat_nav_req_info *) data;

		MSGOUT("Name of the structure: sat_nav_req_info\n");  
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","ref_secs", p->ref_secs);   /* U2B */
		MSGOUT("%-20s[%-10d]\n","gnss_toe", p->gnss_toe);   /* U1C */
		MSGOUT("%-20s[%-10d]\n","ttoe_age", p->ttoe_age);   /* U1C */
		MSGOUT("%-20s[%-10d]\n","n_of_sat", p->n_of_sat);   /* U1C */		
		p++;
	}
}

void print_sat_nav_data(void *data, int n_obj)
{
	int  n = 0;
	struct sat_nav *p = (struct sat_nav *) data;

		MSGOUT("Name of the structure: sat_nav\n");  
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","sat", p->sat);   /* U1C  */
		MSGOUT("%-20s[%-10d]\n","iod", p->iod);   /* U2B  */			
		p++;
	}
}


void print_ganss_ue_assist_caps_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_ue_assist_caps *p = (struct ganss_ue_assist_caps *) data;

		MSGOUT("Name of the structure: ganss_ue_assist_caps\n");   
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","common_bits", p->common_bits); /* U4B */
		MSGOUT("%-20s[%-10d]\n","n_ganss", p->n_ganss);   	  	/* U4B */			
		p++;
	}
}

void print_ganss_assist_caps_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_assist_caps *p = (struct ganss_assist_caps *) data;

		MSGOUT("Name of the structure: ganss_assist_caps\n");   
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","id_of_ganss", p->id_of_ganss);   /* U1C  */
		MSGOUT("%-20s[%-10d]\n","caps_bitmap", p->caps_bitmap);   /* U4B  */			
		p++;
	}
}

void print_ganss_data_model_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_data_model *p = (struct ganss_data_model *) data;

		MSGOUT("Name of the structure: ganss_data_model\n");  
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","orb_model", p->orb_model);  /* U1C */
		MSGOUT("%-20s[%-10d]\n","clk_model", p->clk_model);  /* U1C */
		MSGOUT("%-20s[%-10d]\n","alm_model", p->alm_model);  /* U1C */
		MSGOUT("%-20s[%-10d]\n","utc_model", p->utc_model);  /* U1C */		
		p++;
	}
}

void print_ganss_ue_lcs_caps_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_ue_lcs_caps *p = (struct ganss_ue_lcs_caps *) data;

		MSGOUT("Name of the structure: ganss_ue_lcs_caps\n");   
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","n_ganss", p->n_ganss); /* U1C */		
		p++;
	}
}


void print_ganss_lcs_caps_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_lcs_caps *p = (struct ganss_lcs_caps *) data;

		MSGOUT("Name of the structure: ganss_lcs_caps\n");   
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","ganss_id", p->ganss_id);   /* U1C */
		MSGOUT("%-20s[%-10d]\n","caps_bits", p->caps_bits); /* U4B */
		MSGOUT("%-20s[%-10d]\n","sigs_bits", p->sigs_bits); /* U1C */
		MSGOUT("%-20s[%-10d]\n","sbas_bits", p->sbas_bits); /* U1C */		
		p++;
	}
}


void print_ganss_msr_desc_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_msr_desc *p = (struct ganss_msr_desc *) data;

		MSGOUT("Name of the structure: ganss_msr_desc\n"); 
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","ganss_id", p->ganss_id);   /* U1C */
		MSGOUT("%-20s[%-10d]\n","n_signal", p->n_signal);   /* U1C */		
		p++;
	}
}

void print_ganss_ref_time_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_ref_time *p = (struct ganss_ref_time *) data;

		MSGOUT("Name of the structure: ganss_ref_time\n");  
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10u]\n","tid", p->tid);   			/* Enum */
		MSGOUT("%-20s[%-10d]\n","day", p->day);   			/* U2B */
		MSGOUT("%-20s[%-10d]\n","tod", p->tod);   			/* U4B */
		MSGOUT("%-20s[%-10d]\n","tod_unc", p->tod_unc);   	/* U1C */
		MSGOUT("%-20s[%-10u]\n","accuracy", p->accuracy); 	/* Enum */		
		p++;
	}
}

void print_ganss_addl_iono_data(void *data, int n_obj)
{
	int  n = 0;
	struct ganss_addl_iono *p = (struct ganss_addl_iono *) data;

		MSGOUT("Name of the structure: ganss_addl_iono\n");  
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10d]\n","data_id", p->data_id);   /* U1C */
		MSGOUT("%-20s[%-10d]\n","alfa0", p->alfa0);   	  /* U1C */
		MSGOUT("%-20s[%-10d]\n","alfa1", p->alfa1);   	  /* U1C */
		MSGOUT("%-20s[%-10d]\n","alfa2", p->alfa2);   	  /* U1C */
		MSGOUT("%-20s[%-10d]\n","alfa3", p->alfa3);   	  /* U1C */
		MSGOUT("%-20s[%-10d]\n","beta0", p->beta0);   	  /* U1C */
		MSGOUT("%-20s[%-10d]\n","beta1", p->beta1);   	  /* U1C */
		MSGOUT("%-20s[%-10d]\n","beta2", p->beta2);   	  /* U1C */
		MSGOUT("%-20s[%-10d]\n","beta3", p->beta3);   	  /* U1C */		
		p++;
	}
}

void print_dgps_sat_data(void *data, int n_obj)
{
	int  n = 0;
	struct dgps_sat *p = (struct dgps_sat *) data;

		MSGOUT("Name of the structure: dgps_sat\n");   
	
	for(n = 0; n < n_obj; n++) {		
		MSGOUT("%-20s[%-10u]\n","tow_unit", p->tow_unit);   /* Enum */
		MSGOUT("%-20s[%-10d]\n","gps_tow", p->gps_tow);     /* U4B */
		MSGOUT("%-20s[%-10d]\n","health", p->health);   	/* U1C */
		MSGOUT("%-20s[%-10d]\n","svid", p->svid);  	 	  	/* U1C */
		MSGOUT("%-20s[%-10d]\n","iod", p->iod);   		  	/* U1C */
		MSGOUT("%-20s[%-10d]\n","udre", p->udre);   	    /* U1C */
		MSGOUT("%-20s[%-10d]\n","prc", p->prc);   	      	/* S2B */
		MSGOUT("%-20s[%-10d]\n","rrc", p->rrc);   	      	/* S1C */
		MSGOUT("%-20s[%-10d]\n","dprc2", p->dprc2);         /* U1C */
		MSGOUT("%-20s[%-10d]\n","drrc2", p->drrc2);         /* U1C */
		MSGOUT("%-20s[%-10d]\n","dprc3", p->dprc3);         /* U1C */
		MSGOUT("%-20s[%-10d]\n","drrc3", p->drrc3);         /* U1C */			
		p++;
	}
}







