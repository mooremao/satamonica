
#ifndef __FTRACE_H__
#define __FTRACE_H__

#include <stdio.h>

class Ftrace {

private: 
	static unsigned int nest_lvl; 
	unsigned int        nest;
public:
	Ftrace(const char *file, const char *func, int line); 
       ~Ftrace();
 
};

FILE *ftrace_init(const char *log_file);
/* Add thread ID and name of the thread. */
int ftrace_add_fthread(unsigned int t_id, const char *name);

#if ENABLE_TRACE == 1
#define FUNCT_TRACE(x, y, z) Ftrace obj(x, y, z);
#define CPP_FTRACE1()        FUNCT_TRACE(__FILE__, __PRETTY_FUNCTION__, __LINE__)
#define CPP_FTRACE2()        FUNCT_TRACE(__FILE__, __PRETTY_FUNCTION__, __LINE__)
#define CPP_FTRACE3()        FUNCT_TRACE(__FILE__, __PRETTY_FUNCTION__, __LINE__)
#else
#define FUNCT_TRACE(x, y, z)
#define CPP_FTRACE1()
#define CPP_FTRACE2()
#define CPP_FTRACE3()
#endif

#endif
