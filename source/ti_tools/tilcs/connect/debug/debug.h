#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifdef __cplusplus
	extern "C" {
#endif 

#include <stdio.h>
#include "ti_log.h"

struct log_desc {
        
        FILE *l_file;  /* FILE stream of log file */
        int   n_line;  /* # of  lines in log file */
}; 


extern struct log_desc dbg_log; /* Do not use any where in your code */
extern int dbg_level;

//#define DBG_L TI_LOG

void dbg_print(struct log_desc *ldesc, const char *prefix, 
               const char *tag, int line, const char *fmt, ...);

#if ENABLE_DEBUG != 1
#define DPRINT(ldesc, prefix, ...)                                      \
                dbg_print(ldesc, prefix, __PRETTY_FUNCTION__, __LINE__, \
                          __VA_ARGS__);

#define DEBUGn(n, ...)                                                   \
        do {                                                             \
                if(dbg_level > n)                                        \
                        DPRINT(&dbg_log, "INF", __VA_ARGS__);            \
        } while(0);

#define DBG_L1(...) DEBUGn(1, __VA_ARGS__)
#define DBG_L2(...) DEBUGn(2, __VA_ARGS__)
#define DBG_L3(...) DEBUGn(3, __VA_ARGS__)

#define ERR_NM(...) DPRINT(&dbg_log, "ERR", __VA_ARGS__)
#else
#define DPRINT(lfile, prefix, ...)
#define DEBUGn(n, ...)
#ifdef ANDROID
#define DBG_L1(...) TI_LOG1(__VA_ARGS__)
#define DBG_L2(...) TI_LOG2(__VA_ARGS__)
#define DBG_L3(...) TI_LOG3(__VA_ARGS__)
#define ERR_NM(...) TI_ERRx(__VA_ARGS__)
#else
#define DBG_L1(...) printf
#define DBG_L2(...) printf
#define DBG_L3(...) printf
#define ERR_NM(...) printf

#endif

#endif

FILE* debug_init(const char *log_file);
void debug_exit(FILE *file);

/*
	MSG Debug
*/

extern struct log_desc msg_log; /* Do not use any where in your code */

typedef void (*dump_fn)(void*, int);
//void dmsg_print(FILE *lfile, const char *prefix, const char *module, 
//	        int line, dump_fn fn, void *data, int n_obj);

void dmsg_print(struct log_desc *ldesc, const char *prefix, const char *module, 
                int line, dump_fn fn, void *data, int n_obj);
			
#if ENABLE_DEBUG == 1
#define MPRINT(mfile, prefix, fn, data, n_obj)                          \
	dmsg_print(mfile, prefix, __FILE__, __LINE__, fn, data, n_obj);

#define MSG_Lx(n, fn, data, n_obj)                                      \
        do {                                                            \
                if(dbg_level > n)                                       \
                        MPRINT(&dbg_log, "MSG", fn, data, n_obj);           \
        } while(0);

#define MSG_L1(fn, data, n_obj) MSG_Lx(1, fn, data, n_obj);
#define MSG_L2(fn, data, n_obj) MSG_Lx(2, fn, data, n_obj);
#define MSG_L3(fn, data, n_obj) MSG_Lx(3, fn, data, n_obj);

#define MSGOUT(...) do { if(msg_log.l_file) fprintf(msg_log.l_file, "\t"__VA_ARGS__);}  while(0); 
#else
#define MPRINT(mfile, prefix, fn, data, n_obj)
#define MSG_Lx(n, fn, data, n_obj)
#define MSG_L1(fn, data, n_obj)
#define MSG_L2(fn, data, n_obj)
#define MSG_L3(fn, data, n_obj)
#define MSGOUT(...)
#endif

FILE* dmsg_init(const char *log_file);
void dmsg_exit(FILE *file);
//void test_fn(void *data, int n_obj);

void debug_init_log();

void debug_exit_log();

/* MACROS to dispatch operator specific data from AGNSS Connect */
#define CT_OPER_BIN(id, n_objs, data)   TI_LOG_BIN("OPER",  7, id, n_objs, data)

#ifdef __cplusplus
	}
#endif 

#endif
