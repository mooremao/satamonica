#include "ti_agnss_lcs_adapter.h"
#include "a-gnss_xact.h"

#include <string.h>
#include <iostream>

#include "debug.h"
#include "Ftrace.h"
using namespace std;

struct pfm_app_adapter {

	void                          *afw_hnd;
	struct ti_agnss_lcs_adap_cb    afw_cbs;

	void                          *rpc_hnd;
	struct agnss_lcs_user_ops      rpc_ifc;
	struct agnss_lcs_pvdr_ops      rpc_cbs;

};

static pfm_app_adapter *adap = NULL; /* : Make it OO; Update code to ensure 
					that appropriate handles are passed 
					across boundaries both ways.
				      */

/*------------------------------------------------------------------------------
 * RPC callbacks to A-GNSS Framework LCS callback Functions.
 *----------------------------------------------------------------------------*/
static int rpc_cbs_loc_start(void *hnd)
{
	CPP_FTRACE2();
	//struct pfm_app_adapter *adap = (struct pfm_app_adapter*) hnd;
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->loc_start(adap->afw_hnd);
}

static int rpc_cbs_loc_stop(void *hnd)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd); 
	return afw_cbs->loc_stop(adap->afw_hnd);
}

static int rpc_cbs_get_ue_loc_caps(void *hnd, unsigned int& gps_caps_bits,
				   struct ganss_ue_lcs_caps *ganss_caps)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->get_ue_loc_caps(adap->afw_hnd, gps_caps_bits,
					ganss_caps);
}

static int rpc_cbs_set_loc_instruct(void *hnd, const struct loc_instruct& req)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->set_loc_instruct(adap->afw_hnd, req);
}

static int rpc_cbs_set_rpt_criteria(void *hnd, const struct rpt_criteria& rpt)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->set_rpt_criteria(adap->afw_hnd, rpt);
}

static int rpc_cbs_ue_add_assist(void *hnd, const struct nw_assist_id& id,
				 const void *assist_array, int num)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->ue_add_assist(adap->afw_hnd, id, assist_array, num);  
}

static int rpc_cbs_ue_del_aiding(void *hnd, enum loc_assist assist, 
				 unsigned int sv_id_map,
				 unsigned int mem_flags)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->ue_del_aiding(adap->afw_hnd, assist, sv_id_map, 
				      mem_flags);
}

static int rpc_cbs_ue_get_aiding(void *hnd, enum loc_assist assist, 
				 void *assist_array, int num)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->ue_get_aiding(adap->afw_hnd, assist, assist_array, num);
}

static int rpc_cbs_ue_assist2ue_done(void *hnd,
                                                                        const struct assist_reference& assist_usr)
{
	CPP_FTRACE2();
	//LOGD(" [PFM ADP]  rpc_cbs_ue_assist2ue_done");
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->assist2ue_done(adap->afw_hnd, assist_usr);
}

static int rpc_cbs_ue_dev_oper_param(void *hnd, enum ue_dev_param_id id,
						void *param_desc, int num)
{
	CPP_FTRACE2();
	//struct pfm_app_adapter *adap = (struct pfm_app_adapter*) hnd;
	
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;
	UNUSED(hnd);
	return afw_cbs->oper_ue_dev_param(adap->afw_hnd, id,param_desc, num);
}






/*------------------------------------------------------------------------------
 * A-GNSS Framework LCS Interface Functions to RPC Invokes.
 *----------------------------------------------------------------------------*/
	static 
int rpc_ifc_ue_need_assist(void *hnd, const struct gps_assist_req& gps_assist,
			   const struct ganss_assist_req& ganss_assist,
			   const struct assist_reference& assist_usr)
{
	CPP_FTRACE2();
	struct pfm_app_adapter *adap       = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_need_assist)
		return 0;

	return rpc_ifc->ue_need_assist(adap->rpc_hnd, gps_assist, ganss_assist, assist_usr);
}

static 
int rpc_ifc_ue_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
			   const struct gps_msr_result&    gps_result, 
			   const struct gps_msr_info      *gps_msr,
			   const struct ganss_msr_result&  ganss_result,
			   const struct ganss_msr_info    *ganss_msr)
{
	CPP_FTRACE2();
	struct pfm_app_adapter       *adap = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_loc_results)
		return 0;

	return rpc_ifc->ue_loc_results(adap->rpc_hnd, pos_result, gps_result,
				       gps_msr, ganss_result, ganss_msr);
}

static int
rpc_ifc_decl_dev_param(void *hnd,
		       enum ue_dev_param_id id, void *param_desc, int num)
{
	CPP_FTRACE2();

	struct pfm_app_adapter *adap       = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->decl_dev_param)
		return 0;

	return rpc_ifc->decl_dev_param(adap->rpc_hnd, id, 
				       param_desc, num);
}


static 
int rpc_ifc_ue_nmea_report(void *hnd, enum nmea_sn_id nmea, const char *data,
			   int len)
{
	CPP_FTRACE2();
	struct pfm_app_adapter       *adap = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_nmea_report)
		return 0;

	return rpc_ifc->ue_nmea_report(adap->rpc_hnd, nmea, data, len);
}

static int 
rpc_ifc_ue_aux_info4sv(void *hnd, const struct dev_gnss_sat_aux_desc& gps_desc,
		       const struct dev_gnss_sat_aux_info *gps_info,
		       const struct dev_gnss_sat_aux_desc&  glo_desc,
		       const struct dev_gnss_sat_aux_info  *glo_info)
{
	CPP_FTRACE2();
	struct pfm_app_adapter       *adap = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;
	int ret_val;

	if(!rpc_ifc->ue_aux_info4sv)
		return 0;

        ret_val = rpc_ifc->ue_aux_info4sv(adap->rpc_hnd, gps_desc, gps_info, 
                                          glo_desc, glo_info);
	return ret_val;
}



static 
int rpc_ifc_ue_decoded_aid(void *hnd, enum loc_assist assist, 
			   const void *assist_array, int num)
{
	CPP_FTRACE2();
	struct pfm_app_adapter       *adap = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc  = &adap->rpc_ifc;

	if(!rpc_ifc->ue_decoded_aid)
		return 0;

	return rpc_ifc->ue_decoded_aid(adap->rpc_hnd, assist,
				       assist_array, num);

}

static int rpc_ifc_cw_test_results(void *hnd, const enum cw_test_rep cwt, const struct cw_test_report *cw_rep, int len)
{
	CPP_FTRACE2();
	struct pfm_app_adapter       *adap = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc  = &adap->rpc_ifc;

	if(!rpc_ifc->cw_test_results)
		return 0;

	return rpc_ifc->cw_test_results(adap->rpc_hnd, cwt, cw_rep,len);
}

static int rpc_ifc_ue_recovery_ind(void *hnd, const enum dev_reset_ind rstind, int len)
{
	CPP_FTRACE2();
	struct pfm_app_adapter       *adap = (struct pfm_app_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc  = &adap->rpc_ifc;

	if(!rpc_ifc->ue_recovery_ind)
		return 0;

	return rpc_ifc->ue_recovery_ind(adap->rpc_hnd, rstind, len);

}
static void* pfm_app_rpc_add_node(struct agnss_lcs_user_ops& rpc_ifc, 
				  struct agnss_lcs_pvdr_ops& rpc_cbs)
{
	CPP_FTRACE2();
	return agnss_xact_add_node(rpc_ifc, rpc_cbs,          // : rpc_cbs as part of app_adapter 
				   APP_LCS_PLATFORM_PVDR_ID,  // self-id 
				   APP_LCS_PLATFORM_USER_ID,  // peer_id
                   TI_AGNSS_USER_PFORM_PATH); // ur port
}

static void* adap_proc(void *arg1)
{
	CPP_FTRACE2();
	struct pfm_app_adapter *adap = (struct pfm_app_adapter*) arg1;

	agnss_xact_lup2proc(adap->rpc_hnd);

	return NULL; //  fix up the return part.
}

static int adap_init(const struct ti_agnss_lcs_adap_cb *adap_cb)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;

	memcpy(afw_cbs, adap_cb, sizeof(struct ti_agnss_lcs_adap_cb));

	adap->afw_hnd = afw_cbs->hnd;

	return 0;
}

static int adap_exit(void)
{
	agnss_xact_mod_exit(adap->rpc_hnd);
	return 0;
}

 static int rpc_cbs_ue_cleanup(void *hnd)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;

	
	UNUSED(hnd); 
	return afw_cbs->ue_cleanup(adap->afw_hnd);
}

static int rpc_cbs_dev_init(void *hnd)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;

	
	UNUSED(hnd); 
	return afw_cbs->dev_init(adap->afw_hnd);
}
static int rpc_cbs_dev_plt(void *hnd, const struct plt_param& plt_test)
{
	CPP_FTRACE2();
	struct ti_agnss_lcs_adap_cb *afw_cbs = &adap->afw_cbs;

	
	UNUSED(hnd); 
	return afw_cbs->dev_plt(adap->afw_hnd, plt_test);
}
 
int pfm_app_adap_load_func(void *data)
{
	// pfm_app_adapter *adap = new pfm_app_adapter;  re-introduce

	/*----------------------------------------------------------------------
	 * Allocate object for Platform Application Adapter (pfm_app_adapter).
	 *--------------------------------------------------------------------*/
	CPP_FTRACE2();
	adap = new pfm_app_adapter;
	if(!adap) {
		ERR_NM("Failed to allocate adapter for PFM APP, Exiting");
		return -1;
	}

	/*----------------------------------------------------------------------
	 * Set up communication transactions and RPC to support TI API(s).
	 *--------------------------------------------------------------------*/
	struct agnss_lcs_pvdr_ops  rpc_cbacks; // Warn: struct allocated on stack
	struct agnss_lcs_pvdr_ops *rpc_cbs = &rpc_cbacks;  

	/* Provision callbacks for RPC to invoke */
	rpc_cbs->loc_start         = rpc_cbs_loc_start;
	rpc_cbs->loc_stop          = rpc_cbs_loc_stop;
	rpc_cbs->get_ue_loc_caps   = rpc_cbs_get_ue_loc_caps;
	rpc_cbs->set_loc_instruct  = rpc_cbs_set_loc_instruct;
	rpc_cbs->set_rpt_criteria  = rpc_cbs_set_rpt_criteria;
	rpc_cbs->ue_add_assist     = rpc_cbs_ue_add_assist;
	rpc_cbs->ue_del_aiding     = rpc_cbs_ue_del_aiding;
	rpc_cbs->ue_get_aiding     = rpc_cbs_ue_get_aiding;
	rpc_cbs->assist2ue_done = rpc_cbs_ue_assist2ue_done;
	rpc_cbs->oper_ue_dev_param = rpc_cbs_ue_dev_oper_param;
 	rpc_cbs->ue_cleanup = rpc_cbs_ue_cleanup;
	rpc_cbs->dev_init = rpc_cbs_dev_init;
	rpc_cbs->dev_plt  = rpc_cbs_dev_plt;
 
	adap->rpc_hnd = pfm_app_rpc_add_node(adap->rpc_ifc, rpc_cbacks);
	if(!adap->rpc_hnd) {
		ERR_NM("Failed to open RPC for PFM APP adapter, Exiting");
		delete adap;
		return -1;
	}

	/* Adapter now has RPC functions to call */

	/*----------------------------------------------------------------------
	 * Provision interface to interwork with A-GNSS framework (afw).
	 *--------------------------------------------------------------------*/          
	struct ti_agnss_lcs_adap_if *afw_ifc  =  NULL;

	afw_ifc = (struct ti_agnss_lcs_adap_if*) data;

	afw_ifc->hnd            = adap; // To be returned by A-GNSS Framework.
	afw_ifc->init           = adap_init;
	afw_ifc->exit           = adap_exit;
	afw_ifc->cb_thread_func = adap_proc;
	afw_ifc->cb_thread_arg1 = adap;

	afw_ifc->ue_need_assist = rpc_ifc_ue_need_assist;
	afw_ifc->ue_decoded_aid = rpc_ifc_ue_decoded_aid;
	afw_ifc->ue_loc_results = rpc_ifc_ue_loc_results;
	afw_ifc->ue_nmea_report = rpc_ifc_ue_nmea_report;
	afw_ifc->ue_aux_info4sv = rpc_ifc_ue_aux_info4sv;
	afw_ifc->decl_dev_param = rpc_ifc_decl_dev_param;
	afw_ifc->ue_service_ctl = NULL;
	afw_ifc->cw_test_results = rpc_ifc_cw_test_results;
	afw_ifc->ue_recovery_ind = rpc_ifc_ue_recovery_ind ;
	return 0;
}
