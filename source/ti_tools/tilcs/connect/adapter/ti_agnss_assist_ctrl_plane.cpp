/* 

 */
#include "ti_agnss_lcs_adapter.h"
#include "a-gnss_xact.h"

#include <string.h>
#include <iostream>
#include <utils/Log.h>
#include "debug.h"
#include "Ftrace.h"

using namespace std;


struct ctrl_plane_adapter {

	void                          *ctrl_plane_hnd;
	struct ti_agnss_lcs_adap_cb    ctrl_plane_cbs;

	void                          *rpc_hnd;
	struct agnss_lcs_user_ops      rpc_ifc;
	struct agnss_lcs_pvdr_ops      rpc_cbs;

};

static ctrl_plane_adapter *ctrl_plane_adap = NULL; /* : Make it OO; Update code to ensure 
													  that appropriate handles are passed 
													  across boundaries both ways.
													*/
static int rpc_cbs_loc_start(void *hnd)
{
  	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_loc_start...");
  	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
  	UNUSED(hnd);
  	return ctrl_plane_cbs->loc_start(ctrl_plane_adap->ctrl_plane_hnd);
}
 
static int rpc_cbs_loc_stop(void *hnd)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_loc_stop...");
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd); 
	return ctrl_plane_cbs->loc_stop(ctrl_plane_adap->ctrl_plane_hnd);
}
 
static int rpc_cbs_get_ue_loc_caps(void *hnd, unsigned int& gps_caps_bits,
					 struct ganss_ue_lcs_caps *ganss_caps)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_get_ue_loc_caps...");
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->get_ue_loc_caps(ctrl_plane_adap->ctrl_plane_hnd, gps_caps_bits,
								  ganss_caps);
}
 
static int rpc_cbs_set_loc_instruct(void *hnd, const struct loc_instruct& req)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_set_loc_instruct...");
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->set_loc_instruct(ctrl_plane_adap->ctrl_plane_hnd, req);
}
 
static int rpc_cbs_set_rpt_criteria(void *hnd, const struct rpt_criteria& rpt)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_set_rpt_criteria...");
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->set_rpt_criteria(ctrl_plane_adap->ctrl_plane_hnd, rpt);
}


static int rpc_cbs_ue_add_assist(void *hnd, const struct nw_assist_id& id,
		const void *assist_array, int num)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_ue_add_assist...");
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->ue_add_assist(ctrl_plane_adap->ctrl_plane_hnd, id, assist_array, num);  
}

static int rpc_cbs_ue_del_aiding(void *hnd, enum loc_assist assist, 
		unsigned int sv_id_map,
		unsigned int mem_flags)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_ue_del_aiding...");
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->ue_del_aiding(ctrl_plane_adap->ctrl_plane_hnd, assist, sv_id_map, 
			mem_flags);
}

static int rpc_cbs_ue_get_aiding(void *hnd, enum loc_assist assist, 
		void *assist_array, int num)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_ue_get_aiding...");
	
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->ue_get_aiding(ctrl_plane_adap->ctrl_plane_hnd, assist, assist_array, num);
}

static int rpc_cbs_ue_assist2ue_done(void *hnd,
                                                                        const struct assist_reference& assist_usr)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_cbs_ue_assist2ue_done...");
	
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->assist2ue_done(ctrl_plane_adap->ctrl_plane_hnd, assist_usr);

}

static int rpc_cbs_ue_dev_oper_param(void *hnd, enum ue_dev_param_id id,
						void *param_desc, int num)
{
	CPP_FTRACE2();
		
	struct ti_agnss_lcs_adap_cb *ctrl_plane_cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	UNUSED(hnd);
	return ctrl_plane_cbs->oper_ue_dev_param(ctrl_plane_adap->ctrl_plane_hnd, id,param_desc, num);
}

/*------------------------------------------------------------------------------
 * A-GNSS Framework LCS Interface Functions to RPC Invokes.
 *----------------------------------------------------------------------------*/
	static 
int rpc_ifc_ue_need_assist(void *hnd, const struct gps_assist_req& gps_assist,
		const struct ganss_assist_req& ganss_assist,
		   const struct assist_reference& assist_usr)
{	
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_ifc_ue_need_assist..");
	struct ctrl_plane_adapter *adap    = (struct ctrl_plane_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_need_assist)
	{
		DBG_L2("[CPM ADP] rpc_ifc_ue_need_assist rpc_ifc->ue_need_assist = NULL");
		return 0;
	}
	return rpc_ifc->ue_need_assist(adap->rpc_hnd, gps_assist, ganss_assist, assist_usr);
}

	static 
int rpc_ifc_ue_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
		const struct gps_msr_result&    gps_result, 
		const struct gps_msr_info      *gps_msr,
		const struct ganss_msr_result&  ganss_result,
		const struct ganss_msr_info    *ganss_msr)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_ifc_ue_loc_results..");
	struct ctrl_plane_adapter    *adap = (struct ctrl_plane_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_loc_results)
		return 0;

	return rpc_ifc->ue_loc_results(adap->rpc_hnd, pos_result, gps_result,
			gps_msr, ganss_result, ganss_msr);
}

static int 	rpc_ifc_decl_dev_param(void *hnd,
				   enum ue_dev_param_id id, void *param_desc, int num)
{
		CPP_FTRACE2();
		struct ctrl_plane_adapter    *adap = (struct ctrl_plane_adapter*) hnd;
		struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;
		if(!rpc_ifc->decl_dev_param)
			return 0;
		return rpc_ifc->decl_dev_param(adap->rpc_hnd, id, 
						   param_desc, num);
}

	static 
int rpc_ifc_ue_decoded_aid(void *hnd, enum loc_assist assist, 
		const void *assist_array, int num)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] rpc_ifc_ue_decoded_aid..");
	struct ctrl_plane_adapter    *adap = (struct ctrl_plane_adapter*) hnd;
	struct agnss_lcs_user_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_loc_results)
		return 0;

	return rpc_ifc->ue_decoded_aid(adap->rpc_hnd, assist,
			assist_array, num);

}

static void* ctrl_plane_rpc_add_node(struct agnss_lcs_user_ops& rpc_ifc, 
				  struct agnss_lcs_pvdr_ops& rpc_cbs)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] ctrl_plane_rpc_add_node..");
	return agnss_xact_add_node(rpc_ifc, rpc_cbs, 
			APP_LCS_NW_CPLNE_PVDR_ID,  // self-id 
			APP_LCS_NW_CPLNE_USER_ID,  // peer_id
			TI_AGNSS_CPLNE_PVDR_PATH); // ur port
}

static void* ctrl_plane_adap_proc(void *arg1)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] ctrl_plane_adap_proc: Entering");
	
	struct ctrl_plane_adapter *adap = (struct ctrl_plane_adapter*) arg1;
	if(adap)
		agnss_xact_lup2proc(adap->rpc_hnd);
	else
		DBG_L2("[CPM ADP] ctrl_plane_adap_proc adap = NULL!!");

	DBG_L2("[CPM ADP] ctrl_plane_adap_proc: Exiting");
	return NULL; //  fix up the return part.
}

static int ctrl_plane_adap_init(const struct ti_agnss_lcs_adap_cb *adap_cb)
{
	CPP_FTRACE1();
	DBG_L2("[CPM ADP] ctrl_plane_adap_init Entering..");
	struct ti_agnss_lcs_adap_cb *cbs = &ctrl_plane_adap->ctrl_plane_cbs;
	DBG_L2("[CPM ADP] cctrl_plane_adap_init");
	memcpy(cbs, adap_cb, sizeof(struct ti_agnss_lcs_adap_cb));
	ctrl_plane_adap->ctrl_plane_hnd = cbs->hnd;
	
	DBG_L2("[CPM ADP] ctrl_plane_adap_init Exiting.");
	return 0;
}

static int ctrl_plane_adap_exit(void)
{
	return 0;
}

int ctrl_plane_adap_load_func(void *data)
{

	/*----------------------------------------------------------------------
	 * Allocate object for sa pgps Adapter (ctrl_plane_adapter).
	 *--------------------------------------------------------------------*/
	ctrl_plane_adap = new ctrl_plane_adapter;
	if(!ctrl_plane_adap) {
		DBG_L1("[CPM ADP] Failed to allocate adapter for CPM ");
		DBG_L1("[CPM ADP] Exiting...");
	
		return -1;
	}

	/*----------------------------------------------------------------------
	 * Set up communication transactions and RPC to support TI API(s).
	 *--------------------------------------------------------------------*/
	struct agnss_lcs_pvdr_ops  rpc_cbacks; // Warn: struct allocated on stack
	struct agnss_lcs_pvdr_ops *rpc_cbs = &rpc_cbacks;  
	/* Provision callbacks for RPC to invoke */
	rpc_cbs->loc_start         = rpc_cbs_loc_start;
	rpc_cbs->loc_stop          = rpc_cbs_loc_stop;
	rpc_cbs->get_ue_loc_caps   = rpc_cbs_get_ue_loc_caps;
	rpc_cbs->set_loc_instruct  = rpc_cbs_set_loc_instruct;
	rpc_cbs->set_rpt_criteria  = rpc_cbs_set_rpt_criteria;
	rpc_cbs->ue_add_assist  = rpc_cbs_ue_add_assist;
	rpc_cbs->ue_del_aiding  = rpc_cbs_ue_del_aiding;
	rpc_cbs->ue_get_aiding  = rpc_cbs_ue_get_aiding;
	rpc_cbs->assist2ue_done = rpc_cbs_ue_assist2ue_done;
	rpc_cbs->oper_ue_dev_param = rpc_cbs_ue_dev_oper_param;

	ctrl_plane_adap->rpc_hnd = ctrl_plane_rpc_add_node(ctrl_plane_adap->rpc_ifc, rpc_cbacks);
	if(!ctrl_plane_adap->rpc_hnd) {
		DBG_L1("[CPM ADP] Failed to open RPC for ctrl_plane adapter ");
		DBG_L1("[CPM ADP] Exiting...");
		
		delete ctrl_plane_adap;
		return -1;
	}

	/* Adapter now has RPC functions to call */

	/*----------------------------------------------------------------------
	 * Provision interface to interwork with A-GNSS framework
	 *--------------------------------------------------------------------*/          
	struct ti_agnss_lcs_adap_if *ctrl_plane_ifc  =  NULL;

	ctrl_plane_ifc = (struct ti_agnss_lcs_adap_if*) data;

	ctrl_plane_ifc->hnd            = ctrl_plane_adap; // To be returned by A-GNSS Framework.
	ctrl_plane_ifc->init           = ctrl_plane_adap_init;
	ctrl_plane_ifc->exit           = ctrl_plane_adap_exit;
	ctrl_plane_ifc->cb_thread_func = ctrl_plane_adap_proc;
	ctrl_plane_ifc->cb_thread_arg1 = ctrl_plane_adap;

	ctrl_plane_ifc->ue_need_assist = rpc_ifc_ue_need_assist;
	ctrl_plane_ifc->ue_decoded_aid = rpc_ifc_ue_decoded_aid;
	ctrl_plane_ifc->ue_loc_results = rpc_ifc_ue_loc_results;
	ctrl_plane_ifc->decl_dev_param = rpc_ifc_decl_dev_param;
	ctrl_plane_ifc->ue_service_ctl = NULL;

	return 0;
}
