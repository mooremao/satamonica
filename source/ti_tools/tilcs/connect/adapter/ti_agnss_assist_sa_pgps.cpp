#include "ti_lc_assist_adapter.h"
#include "a-gnss_xact.h"

#include <string.h>
#include <iostream>
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include "debug.h"
#include "Ftrace.h"

using namespace std;

struct sa_pgps_adapter {

	void                          *sa_pgps_hnd;
	struct ti_lc_assist_adap_cb    sa_pgs_cbs;

	void                          *rpc_hnd;
	struct lc_assist_pvdr_ops      rpc_ifc;
};

static sa_pgps_adapter *sa_pgps_adap = NULL; /* : Make it OO; Update code to ensure 
						that appropriate handles are passed 
						across boundaries both ways.
					      */

static int rpc_cbs_ue_add_assist(void *hnd, const struct nw_assist_id& id,
				 const void *assist_array, int num)
{
	CPP_FTRACE2();
	struct ti_lc_assist_adap_cb *sa_pgs_cbs = &sa_pgps_adap->sa_pgs_cbs;
	UNUSED(hnd);
	return sa_pgs_cbs->ue_add_assist(sa_pgps_adap->sa_pgps_hnd, id, assist_array, num);  
}

static int rpc_cbs_ue_del_aiding(void *hnd, enum loc_assist assist, 
				 unsigned int sv_id_map,
				 unsigned int mem_flags)
{
	CPP_FTRACE2();
	struct ti_lc_assist_adap_cb *sa_pgs_cbs = &sa_pgps_adap->sa_pgs_cbs;
	UNUSED(hnd);
	return sa_pgs_cbs->ue_del_aiding(sa_pgps_adap->sa_pgps_hnd, assist, sv_id_map, 
					 mem_flags);
}

static int rpc_cbs_ue_get_aiding(void *hnd, enum loc_assist assist, 
				 void *assist_array, int num)
{
	CPP_FTRACE2();
	struct ti_lc_assist_adap_cb *sa_pgs_cbs = &sa_pgps_adap->sa_pgs_cbs;
	UNUSED(hnd);
	return sa_pgs_cbs->ue_get_aiding(sa_pgps_adap->sa_pgps_hnd, assist, assist_array, num);
}

static int rpc_cbs_ue_assist2ue_done(void *hnd,
                                     const struct assist_reference& assist_usr)
{
	CPP_FTRACE2();
	struct ti_lc_assist_adap_cb *sa_pgs_cbs = &sa_pgps_adap->sa_pgs_cbs;
	UNUSED(hnd);
	return sa_pgs_cbs->assist2ue_done(sa_pgps_adap->sa_pgps_hnd, assist_usr);

}


/*------------------------------------------------------------------------------
 * A-GNSS Framework LCS Interface Functions to RPC Invokes.
 *----------------------------------------------------------------------------*/
	static 
int rpc_ifc_ue_need_assist(void *hnd, const struct gps_assist_req& gps_assist,
			   const struct ganss_assist_req& ganss_assist,
			   const struct assist_reference& assist_usr)
{
	CPP_FTRACE2();
	struct sa_pgps_adapter *adap       = (struct sa_pgps_adapter*) hnd;
	struct lc_assist_pvdr_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_need_assist)
		return 0;

	return rpc_ifc->ue_need_assist(adap->rpc_hnd, gps_assist, ganss_assist, assist_usr);
}

	static 
int rpc_ifc_ue_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
			   const struct gps_msr_result&    gps_result, 
			   const struct gps_msr_info      *gps_msr,
			   const struct ganss_msr_result&  ganss_result,
			   const struct ganss_msr_info    *ganss_msr)
{
	CPP_FTRACE2();
	struct sa_pgps_adapter       *adap = (struct sa_pgps_adapter*) hnd;
	struct lc_assist_pvdr_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_loc_results)
		return 0;

	return rpc_ifc->ue_loc_results(adap->rpc_hnd, pos_result, gps_result,
				       gps_msr, ganss_result, ganss_msr);
}


	static 
int rpc_ifc_ue_decoded_aid(void *hnd, enum loc_assist assist, 
			   const void *assist_array, int num)
{
	CPP_FTRACE2();
	struct sa_pgps_adapter		 *adap = (struct sa_pgps_adapter*) hnd;
	struct lc_assist_pvdr_ops *rpc_ifc = &adap->rpc_ifc;

	if(!rpc_ifc->ue_decoded_aid)
		return 0;

	return rpc_ifc->ue_decoded_aid(adap->rpc_hnd, assist,
				       assist_array, num);

}


static void* sa_pgps_rpc_add_node(struct lc_assist_pvdr_ops& rpc_ifc, 
				  struct lc_assist_user_ops& rpc_cbs)
{
	CPP_FTRACE2();
	return agnss_xact_add_node(rpc_ifc, rpc_cbs, 
				   APP_P_AND_SA_GPS_PVDR_ID,  // self-id 
				   APP_P_AND_SA_GPS_USER_ID,  // peer_id
                   TI_AGNSS_SPGPS_PVDR_PATH); // ur port
}

static void* sa_pgps_adap_proc(void *arg1)
{
	CPP_FTRACE2();
	struct sa_pgps_adapter *adap = (struct sa_pgps_adapter*) arg1;

	agnss_xact_lup2proc(adap->rpc_hnd);

	return NULL; //  fix up the return part.
}

static int sa_pgps_adap_init(const struct ti_lc_assist_adap_cb *adap_cb)
{
	CPP_FTRACE2();
	struct ti_lc_assist_adap_cb *cbs = &sa_pgps_adap->sa_pgs_cbs;

	memcpy(cbs, adap_cb, sizeof(struct ti_lc_assist_adap_cb));

	sa_pgps_adap->sa_pgps_hnd = cbs->hnd;

	return 0;
}

static int sa_pgps_adap_exit(void)
{
	CPP_FTRACE2();
	return 0;
}

int sa_pgps_adap_load_func(void *data)
{
	CPP_FTRACE2();

	/*----------------------------------------------------------------------
	 * Allocate object for sa pgps Adapter (sa_pgps_adapter).
	 *--------------------------------------------------------------------*/
	sa_pgps_adap = new sa_pgps_adapter;
	if(!sa_pgps_adap) {
		ERR_NM("[SA_PGPS ADP] Failed to allocate adapter for SA_PGPS, Exiting... ");
		return -1;
	}

	/*----------------------------------------------------------------------
	 * Set up communication transactions and RPC to support TI API(s).
	 *--------------------------------------------------------------------*/
	struct lc_assist_user_ops  rpc_cbacks; // Warn: struct allocated on stack
	struct lc_assist_user_ops *rpc_cbs = &rpc_cbacks;  

	rpc_cbs->ue_add_assist  = rpc_cbs_ue_add_assist;
	rpc_cbs->ue_del_aiding = rpc_cbs_ue_del_aiding;
	rpc_cbs->ue_get_aiding = rpc_cbs_ue_get_aiding;
	rpc_cbs->assist2ue_done = rpc_cbs_ue_assist2ue_done;

	sa_pgps_adap->rpc_hnd = sa_pgps_rpc_add_node(sa_pgps_adap->rpc_ifc, rpc_cbacks);
	if(!sa_pgps_adap->rpc_hnd) {
		DBG_L1("[SA_PGPS ADP] Failed to open RPC for SA_PGPS adapter,Exiting ");
		delete sa_pgps_adap;
		return -1;
	}

	/* Adapter now has RPC functions to call */

	/*----------------------------------------------------------------------
	 * Provision interface to interwork with A-GNSS framework
	 *--------------------------------------------------------------------*/          
	struct ti_lc_assist_adap_if *sa_pgps_ifc  =  NULL;

	sa_pgps_ifc = (struct ti_lc_assist_adap_if*) data;

	sa_pgps_ifc->hnd            = sa_pgps_adap; // To be returned by A-GNSS Framework.
	sa_pgps_ifc->init           = sa_pgps_adap_init;
	sa_pgps_ifc->exit           = sa_pgps_adap_exit;
	sa_pgps_ifc->cb_thread_func = sa_pgps_adap_proc;
	sa_pgps_ifc->cb_thread_arg1 = sa_pgps_adap;

	sa_pgps_ifc->ue_need_assist = rpc_ifc_ue_need_assist;
	sa_pgps_ifc->ue_decoded_aid = rpc_ifc_ue_decoded_aid;
	sa_pgps_ifc->ue_loc_results = rpc_ifc_ue_loc_results;
	sa_pgps_ifc->ue_service_ctl = NULL;

	return 0;
}
