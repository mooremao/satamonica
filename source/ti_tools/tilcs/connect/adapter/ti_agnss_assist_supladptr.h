/*
*
* ti_agnss_assist_supladptr.h
*
* Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
* ALL RIGHTS RESERVED
*
*/
#ifndef _AGNSS_SUPL_ADAPTER_H_
#define _AGNSS_SUPL_ADAPTER_H_

#define TRUE              0x01
#define FALSE             0x00
#define MAX_SUPL_SAT      32
#define MAX_SUPL_SAT_GPS  16
 
#define MAX_NI_MSG_SIZE   5120 /* 1024*5 */

#define SUPL_INIT         0x00000001
#define SUPL_SI_SESSION   0x00000002
#define SUPL_NI_SESSION   0x00000004 
#define SUPL_CONT_SESSION   0x00000008

#define SUPL_NI_MSG      	0x00000001
#define SUPL_SI_TRG_MSG		0x00000002
#define SUPL_SI_REQ3PRTY_MSG	0x00000003
#define SUPL_SI_SND3PRTY_MSG	0x00000004


#define BIT_SET(x,y) ((x) |= (1<<(y)))
#define BIT_CLEAR(x,y) ((x) &= ~(1<<(y)))
#define BITVAL(x,y) ((((unsigned int)x)>>(y)) & 1)


#define ASSISTANCE_DEFULT_POS_UNCRTNTY  10
#define ASSISTANCE_DEFULT_ALTITUDE      0x01
#define ASSISTANCE_DEFULT_ALTITUDE_SIGN 0x00
#define DFLT_QOP_HZTL_ACC_K             25 /* 96 Meter */
#define DFLT_QOP_VRTL_ACC_K             25 /* 96 Meter */
#define DFLT_QOP_MAX_AGE                10 /* 10 Sec */
#define DFLT_QOP_BY_DELAY               10 /* 10 Sec */
#define DFLT_SENS_REPT                  1 /* 1 Sec */
 
/* Ref: 3GPP TS 23.032 */
#define TYPE_SHAPE_ELLIPSOID                   0x00
#define TYPE_SHAPE_ELLIPSOID_UNCERT_CIRCLE     0x01
#define TYPE_SHAPE_ELLIPSOID_UNCERT_ELLIPSE    0x03
#define TYPE_SHAPE_POLYGON                     0x05
#define TYPE_SHAPE_ELLIPSOID_ALTITUDE          0x08
#define TYPE_SHAPE_ELLIPSOID_ALTITUDE_UNCERT_ELLIPSE    0x09
#define TYPE_SHAPE_ELLIPSOID_ARC               0x0A

#define TYPE_VELOCITY_TYPE_HZ_VL              0x00
#define TYPE_VELOCITY_TYPE_HZ_VRTCL_VL        0x01
#define TYPE_VELOCITY_TYPE_HZ_VL_UNCERT       0x02
#define TYPE_VELOCITY_TYPE_HZ_VRTCL_VL_UNCERT 0x03

#define C_FOUR_HRS_MSEC              14400000L /* 4 hours worth of msec */
#define C_GPS_WEEK_MSEC             604800000L /* A GPS Week of msec */

#define LOG_TAG "supladapter:"

#define SUPLADTR_LOGD ALOGD 

#define NI_MSB_COLD_START		1
#define NI_MSA_COLD_START		2
#define NI_MSB_HOT_START		3
#define NI_MSA_HOT_START		4

/* NOTE: Should be same as in hal_connect.cpp */
typedef struct {
    int ni_msg_type;
    unsigned int msg_len;
    unsigned char ni_msg[MAX_NI_MSG_SIZE];
}supl_ni_msg_t;

typedef enum {
        SUPL_NI_RESPONSE_ACCEPT = 1,
        SUPL_NI_RESPONSE_DENY = 2,
        SUPL_NI_RESPONSE_NORESP = 3,
}eHAL_NIResp;

typedef struct
{
        eHAL_NIResp resp;
        int client_session_id;
}hal_ni_client_response;

static const unsigned int k[128]=
    {0,1,2,3,4,5,6,7,8,9,10,
     11,12,13,14,15,16,17,18,19,20,
     21,22,23,24,25,26,27,28,29,30,
     31,32,33,34,35,36,37,38,39,40,
     41,42,43,44,45,46,47,48,49,50,
     51,52,53,54,55,56,57,58,59,60,
     61,62,63,64,65,66,67,68,69,70,
     71,72,73,74,75,76,77,78,79,80,
     81,82,83,84,85,86,87,88,89,90,
     91,92,93,94,95,96,97,98,99,100,
     101,102,103,104,105,106,107,108,109,110,
     111,112,113,114,115,116,117,118,119,120,
     121,122,123,124,125,126,127}; 
static const unsigned int r[128]=
    {0,1,2,3,4,6,7,9,11,13,15, /*10*/
     18,21,24,27,31,35,40,45,51,57, /*20*/
     64,71,79,88,98,109,121,134,148,164,/*30*/
     181,201,222,245,271,299,330,364,401,442,487,/*40*/
     537,592,652,718,791,871,960,1057,1163,
     1281,1410,1552,1708,1880,2069,2277,2506,2758,3034,
     3339,3674,4042,4447,4893,5384,5923,6516,7169,7887,
     8677,9545,10501,11552,12708,13980,15379,16918,18611,20474,
     22522,24775,27254,29980,32979,36278,39907,43899,48290,53120,
     58433,64277,70706,77777,85556,94113,103525,113879,125268,137796,
     151576,166735,183409,201751,221928,244121,268535,295389,324929,357423,
     393166,432484,475734,523308,575640,633205,696526,766180,842799,927080,
     1019789,1121769,1233947,1357343,1493078,1642387,1806627};


int connect_if_loc_start(void);
int connect_if_loc_stop(void);
int connect_if_get_ue_loc_caps(unsigned int& gps_caps_bits, 
                                 struct ganss_ue_lcs_caps *ganss_caps);

int connect_if_get_ue_get_ue_agnss_caps(unsigned int& gps_caps_bits, 
                                           struct ganss_ue_assist_caps *ganss_caps);


int connect_if_set_loc_instruct(const struct loc_instruct& req);

int connect_if_set_rpt_criteria(const struct rpt_criteria& rpt);

int connect_if_ue_add_assist(const struct nw_assist_id& id,
                             const void *assist_array, int num );

int connect_if_ue_del_aiding(enum loc_assist assist,unsigned int sv_id_map,
                             unsigned int mem_flags);

int connect_if_ue_get_aiding(enum loc_assist assist,void *assist_array, int num);
int connect_if_assist2ue_done(const struct assist_reference& assist_usr);
int connect_if_ue_get_req4assist(struct gps_assist_req& gps_assist,
                                           struct ganss_assist_req *ganss_assist);

int connect_if_nw_loc_results(const struct gnss_pos_result& pos_result,
			const struct assist_reference& assist_usr);


unsigned int supladptr_get_uncertainty_inK(unsigned int r);
unsigned int supladptr_get_uncertainty_inR(unsigned int k);
void supladptr_set_status_ni_session(void);
void supladptr_reset_status_ni_session(void);
int supladptr_get_status_ni_session(void);

void supladptr_set_gps_capabilities(unsigned int gps_caps_bits);
void supladptr_get_gps_capabilities(void);

void supladptr_set_status_si_session(void);
void supladptr_reset_status_si_session(void);
int supladptr_get_status_si_session(void);

void supladptr_set_status_supl_init(void);
void supladptr_reset_status_supl_init(void);
int supladptr_get_status_supl_init(void);

void supladptr_set_status_connect_session(void);
void supladptr_reset_status_connect_session(void);
int supladptr_get_status_connect_session(void);

void supladptr_ni_init(void);

int SUPLC_Cmd_init(void);
int SUPLC_Cmd_deinit(void);
int SUPLC_Cmd_assistanceRequest(const struct gps_assist_req& gps_assist,
             const struct ganss_assist_req& ganss_assist);
int SUPLC_Cmd_reassistanceRequest(const struct gps_assist_req& gps_assist,
             const struct ganss_assist_req& ganss_assist);
int SUPLC_Cmd_locationFix(const struct gnss_pos_result& cnnt_pos_result,
                          const struct gps_msr_result&  gps_result,
                          const struct gps_msr_info     *gps_msr,
                          const struct ganss_msr_result&  ganss_result,
                          const struct ganss_msr_desc    *ganss_msr);
int SUPLC_Cmd_MesurmntReport(enum nmea_sn_id nmea,const char *data, int len);
int SUPLC_Cmd_StopSession(const void *const p_inPayload);
int SUPLC_Cmd_send_NI_message(const void *const p_inPayload,
                                      int payload_Len);
int SUPLC_Cmd_Notify_UsrRsp(int notif_id, GpsUserResponseType user_response);
int SUPLC_Cmd_SuspendSUPL(const void *const p_inPayload);
int SUPLC_Cmd_ResumeSUPL(const void *const p_inPayload);
void supladptr_write_ni_nty(GpsNiNotification *pstNINotification);

/* MACROS to dispatch operator specific data from SUPL Adapter */
#define UP_OPER_BIN(id, n_objs, data)   TI_LOG_BIN("OPER",  7, id, n_objs, data)
#define UP_OPER_STR(...)                TI_LOG_STR("OPER",  7, __VA_ARGS__)

#endif/* _AGNSS_SUPL_ADAPTER_H_ */
