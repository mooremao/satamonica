/*
*
* ti_agnss_assist_suplif.cpp
*
* Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
* ALL RIGHTS RESERVED
*
*/
#include <gnss.h>
#include <gps.h>
#include "ti_lc_assist_adapter.h"
#include "a-gnss_xact.h"
#include "ti_agnss_assist_supladptr.h"
#include <math.h>
#include <string.h>
#include <iostream>
#include <utils/Log.h>
#include "debug.h"
#include "cmcc_definitions.h"
#include "ti_log.h"
#include "log_id.h"

extern "C" {
/* SUPL Include */
#include <stdio.h>
#include <stdlib.h>
#include "SUPL_Interface.h"
}

using namespace std;

void SUPLC_Evt_start_location_fix_req(SUPL_QOP_PARAMS *pstQOPParams, int nPeriod,
                        SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps);
void SUPLC_Evt_stop_location_fix_req(void);
void SUPLC_Evt_inject_assistance(SUPL_GPS_ASSISTANCE_DATA *pstGpsAssistanceData,
        SUPL_GANSS_ASSISTANCE_DATA *pstGanssAsstData,
        SUPL_ADDITIONAL_ASSISTANCE_DATA *pstAddlAsstData);
void SUPLC_Evt_set_position(SUPL_POSITION *pstGpsPositionFix, SUPL_QOP_PARAMS *pstLocFixQOP);
void SUPLC_Evt_change_qop(SUPL_QOP_PARAMS *pstQOPParam,
                SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps);
void SUPLC_Evt_ni_message_handlr(SUPL_NI_NOTIFICATION *pstNINotification);
void SUPLC_Evt_notify_3rdpty_loc(SUPL_SET_ID *pstSETId, SUPL_POSITION *pstGpsPositionFix);
void SUPLC_Evt_log_msg(int nMsgLength ,char *pMsg);
void SUPLC_Evt_GANSS_Cap(SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps);


static void supladptr_gps_assistanceData(const struct gps_assist_req& gps_assist, 
                                     SUPL_REQUEST_ASSISTANCE * stRequestAssistance);

static SUPL_INTERFACE_CALLBACKS SUPLC_interface_callbacks;
static void supladptr_update_gps_qos(SUPL_QOP_PARAMS *pstQOPParams, struct loc_instruct *req);
static void supladptr_processs_recvAGPSAssistance(SUPL_GPS_ASSISTANCE_DATA *pstGpsAssistanceData);
static void supladptr_processs_recvAGANSSAssistance(SUPL_GANSS_ASSISTANCE_DATA *pstGanssAsstData);
static void supladptr_processs_recvAddlAsstData(SUPL_ADDITIONAL_ASSISTANCE_DATA *pstAddlAsstData);
static void supladptr_inject_gpsTime(SUPL_GPS_ASSISTANCE_DATA *pstGpsAssistanceData);
static void supladptr_inject_ref_location(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_inject_ionospheric(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_inject_utc_model(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_inject_dgps_correction(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_inject_navmodel(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_inject_almanac(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_inject_acquistData(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_inject_rti_data(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData);
static void supladptr_start_cont_session(SUPL_QOP_PARAMS *pstQOPParams,int nPeriod,
                        SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps);
static void supladptr_delete_aiding_data(void);
static void supladptr_print_ephemeris(SUPL_NAVMODEL_ELEMENT *p_SLPNavModelElement);
static int supladptr_update_pos_result(const struct gnss_pos_result& cnnt_pos_result,
                                        SUPL_POSITION * pstSuplPosition);
static int supladptr_update_msr_result(const struct gps_msr_result&  gps_result,
                                       const struct gps_msr_info     *gps_msr,
                                       SUPL_GPS_MEASUREMENT *p_SUPLGPSMsrInfo);


static SUPL_NOTI_USER_RESP update_Notify_UsrRsp(GpsUserResponseType user_response);

/*--------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------*
 *            SUPL Client Event                              *
 *-----------------------------------------------------------*/


/**
 * Function:        SUPLC_Evt_start_location_fix_req
 * Brief:           Initiation of SUPL Client
 * Description:
 * Note:
 * Params:          pstQOPParams - SUPL QoS Parameter.
            nPeriod -
 *                  SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES 
 * Return:          none.
 */
void SUPLC_Evt_start_location_fix_req(SUPL_QOP_PARAMS *pstQOPParams, int nPeriod,
                        SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps)
{
    struct loc_instruct req;
    struct rpt_criteria rpt;
    int return_vlue = -1;
    int cont_sn_status = -1;
    time_t now;

    time(&now);
    SUPLADTR_LOGD("RECV->SUPLC_Evt_start_location_fix_req Rx Time :!!! %s\n",ctime(&now));
    return_vlue = supladptr_get_status_connect_session();
    if((pstQOPParams)&&
       (return_vlue == 0)){
          supladptr_start_cont_session(pstQOPParams,nPeriod,pstSLPGANSSCaps);
         /* update Session Info */
         // supladptr_set_status_ni_session();
    }
}


/**
 * Function:        SUPLC_Evt_stop_location_fix_req
 * Brief:           Stop location fix 
 * Description:     Stop Connect Session
 * Note:
 * Params:          none.
 * Return:          none.
 */
void SUPLC_Evt_stop_location_fix_req(void)
{
    /* All NI Sessions are stopped */
    int cont_sn_status = -1;
    time_t now;

    time(&now);
    SUPLADTR_LOGD("RECV->SUPLC_Evt_stop_location_fix_req Rx Time :!!! %s\n",ctime(&now));
    UP_OPER_BIN(UP_ADAP_LOGB_ID_LOC_END, 0, NULL);
    cont_sn_status = supladptr_get_status_connect_session();  
    if(cont_sn_status == 1 ){
           connect_if_loc_stop();
           supladptr_reset_status_connect_session();
    }
    supladptr_reset_status_ni_session();
}

/**
 * Function:        SUPLC_Evt_inject_assistance
 * Brief:           Inject Assistance to connect 
 * Description:     Rx. Assistance from Server 
 * Note:
 * Params:          
 * Return:          none.
 */
void SUPLC_Evt_inject_assistance(SUPL_GPS_ASSISTANCE_DATA *pstGpsAssistanceData,
        SUPL_GANSS_ASSISTANCE_DATA *pstGanssAsstData,
        SUPL_ADDITIONAL_ASSISTANCE_DATA *pstAddlAsstData)
{
    time_t now;
    int rtrn_vlue = -1,cnt_sn = -1;
    SUPL_QOP_PARAMS stQOPParams;
	  
    time(&now);
    SUPLADTR_LOGD("RECV->SUPLC_Evt_inject_assistance Rx Time :!!! %s\n",ctime(&now));
    /* Check Sessions With Connect */
    rtrn_vlue = supladptr_get_status_si_session();
    cnt_sn = supladptr_get_status_connect_session(); 
    if((rtrn_vlue == 0)&&(cnt_sn == 0)){
         SUPLADTR_LOGD("NO Session at Connect");
    }

    if( NULL != pstGpsAssistanceData){
         supladptr_processs_recvAGPSAssistance(pstGpsAssistanceData);
    }
    else {
          SUPLADTR_LOGD("SUPLC_Evt_inject_assistance: SUPL_GPS_ASSISTANCE_DATA  is NULL \n");
    }

    if(NULL != pstGanssAsstData){
       supladptr_processs_recvAGANSSAssistance(pstGanssAsstData);
    }
    else{
       SUPLADTR_LOGD("SUPLC_Evt_inject_assistance: SUPL_GANSS_ASSISTANCE_DATA  is NULL \n");
    }

    if(NULL != pstAddlAsstData){
       supladptr_processs_recvAddlAsstData(pstAddlAsstData);
    }
    else{
       SUPLADTR_LOGD("SUPLC_Evt_inject_assistance: SUPL_ADDITIONAL_ASSISTANCE_DATA is NULL \n");
    }

    time(&now);
    SUPLADTR_LOGD("SUPLC_Evt_inject_assistance Injected Time :!!! %s\n",ctime(&now));
}

/**
 * Function:        SUPLC_Evt_set_position
 * Brief:           Provide Position to connect 
 * Description:     Rx. Position from Server 
 * Note:
 * Params:          
 * Return:          none.
 */
void SUPLC_Evt_set_position(SUPL_POSITION *p_SUPLPositionFix, SUPL_QOP_PARAMS *pstLocFixQOP)
{
     time_t now;
     struct gnss_pos_result cnnt_pos_result; 
     int cpy_byte;
     //char prnt_utcTime[21];
     unsigned char uUncertainSemiMajor,u_RefLocShape;
     struct assist_reference requester;

     time(&now);
     SUPLADTR_LOGD("RECV->SUPLC_Evt_set_position Rx Time :!!! %s\n",ctime(&now));
     /* : SUPL QoP is not injected in connect */
     if(NULL == p_SUPLPositionFix){
         //SUPLADTR_LOGD("send_supl_locationFix_to_NAVL: NULL Pointer!!! \n");
         return;
     }

     memset(((struct gnss_pos_result *)(&cnnt_pos_result)),0x00,sizeof(struct gnss_pos_result));
	 
     cpy_byte = (p_SUPLPositionFix->byNoOfBytes < 20? p_SUPLPositionFix->byNoOfBytes:20);
     
     /* : Need UTC Time Stamp Support in Connect 
        memcpy( ,
           &(p_SUPLPositionFix->abyUTCTimeStamp[0]),cpy_byte);
	 cnnt_pos_result.gnss_tid = gps_time_id;
     */
	 
      /* Support UTC Time Stamp */
      cnnt_pos_result.gnss_tid = gps_time_id;
      cnnt_pos_result.utc_secs= p_SUPLPositionFix->dwUTCSeconds;
      cnnt_pos_result.ref_msec = 0;

      SUPLADTR_LOGD("Rx UTC Sec 0x%x", p_SUPLPositionFix->dwUTCSeconds);
      /* Is it DEV_POS_HAS_ELP,DEV_POS_HAS_UTC in contents?
       */
      cnnt_pos_result.contents = 0;
	 	 
      /* No Info Present on which SVs are used to calculate Fix */
      cnnt_pos_result.pos_bits = 0;
      /* No need to Update cnnt_pos_result.lc_error 
        p_SUPLPositionFix->stSphericalPosition.bCurrentFixValid; 
        Can be used for updating error
      */
      cnnt_pos_result.location.latitude_N =
              p_SUPLPositionFix->stSphericalPosition.dwLatitude;
      cnnt_pos_result.location.longitude_N =
              p_SUPLPositionFix->stSphericalPosition.nLongitude;
      cnnt_pos_result.location.lat_sign = 
              ((p_SUPLPositionFix->stSphericalPosition.byLatitudeSign == 0)?location_desc::north:location_desc::south);
		  
      uUncertainSemiMajor = ASSISTANCE_DEFULT_POS_UNCRTNTY;
      cnnt_pos_result.location.altitude_N = 
	         ASSISTANCE_DEFULT_ALTITUDE;
      cnnt_pos_result.location.alt_dir = 
	         ((ASSISTANCE_DEFULT_ALTITUDE_SIGN == 0)?location_desc::height:location_desc::depth);	 
      u_RefLocShape = p_SUPLPositionFix->stSphericalPosition.byShapeCode;
      SUPLADTR_LOGD("Rx Position Lat=0x%x,Long =0x%x",p_SUPLPositionFix->stSphericalPosition.dwLatitude,
                       p_SUPLPositionFix->stSphericalPosition.nLongitude);
	 
     switch(u_RefLocShape){
       case TYPE_SHAPE_ELLIPSOID:{
         cnnt_pos_result.fix_type = fix_2d;
         cnnt_pos_result.location.shape = ellipsoid; 
       }
       break;

       case TYPE_SHAPE_ELLIPSOID_UNCERT_CIRCLE:{
         cnnt_pos_result.fix_type = fix_2d;
         cnnt_pos_result.location.shape = ellipsoid_with_c_unc; 
         cnnt_pos_result.location.unc_semi_maj_K =		 
                     p_SUPLPositionFix->stSphericalPosition.byUncMajor;
       }
       break;

       case TYPE_SHAPE_ELLIPSOID_UNCERT_ELLIPSE:{
         cnnt_pos_result.fix_type = fix_2d;
         cnnt_pos_result.location.shape = ellipsoid_with_e_unc; 
		 cnnt_pos_result.location.unc_semi_maj_K = 
                     p_SUPLPositionFix->stSphericalPosition.byUncMajor;
         cnnt_pos_result.location.unc_semi_min_K = 
                     p_SUPLPositionFix->stSphericalPosition.byUncMinor;
         cnnt_pos_result.location.orientation = 
                     p_SUPLPositionFix->stSphericalPosition.byOrientMajor;
         cnnt_pos_result.location.confidence =
                     p_SUPLPositionFix->stSphericalPosition.byConfidence;
       }
       break;
	   
       case TYPE_SHAPE_ELLIPSOID_ALTITUDE:{
         cnnt_pos_result.fix_type = fix_3d;
         cnnt_pos_result.location.shape = ellipsoid_with_v_alt; 
         cnnt_pos_result.location.altitude_N = 
                      p_SUPLPositionFix->stSphericalPosition.wAltitude;
         cnnt_pos_result.location.alt_dir = 
                     ((p_SUPLPositionFix->stSphericalPosition.byAltSign == 0)?location_desc::height:location_desc::depth);
       }
       break;

       case TYPE_SHAPE_ELLIPSOID_ALTITUDE_UNCERT_ELLIPSE:{
         cnnt_pos_result.fix_type = fix_3d;	   
         cnnt_pos_result.location.shape = ellipsoid_with_v_alt_and_e_unc; 
         cnnt_pos_result.location.altitude_N = 
		     p_SUPLPositionFix->stSphericalPosition.wAltitude;
         cnnt_pos_result.location.alt_dir = 
		     ((p_SUPLPositionFix->stSphericalPosition.byAltSign == 0)?location_desc::height:location_desc::depth);
         cnnt_pos_result.location.unc_semi_maj_K = 
                     p_SUPLPositionFix->stSphericalPosition.byUncMajor;
         cnnt_pos_result.location.unc_semi_min_K = 
                     p_SUPLPositionFix->stSphericalPosition.byUncMinor;
         cnnt_pos_result.location.orientation = 
                     p_SUPLPositionFix->stSphericalPosition.byOrientMajor;
         cnnt_pos_result.location.unc_altitude_K =
                     p_SUPLPositionFix->stSphericalPosition.byAltUnc;
         cnnt_pos_result.location.confidence =
                     p_SUPLPositionFix->stSphericalPosition.byConfidence;
       }
       break;

       case TYPE_SHAPE_POLYGON:{
          SUPLADTR_LOGD("Polygon SHAPE TYPE NOT Supported");
       }
       case TYPE_SHAPE_ELLIPSOID_ARC:{
          SUPLADTR_LOGD("ELLPSOID ARC SHAPE TYPE NOT Supported");
       }
       default:
          SUPLADTR_LOGD("UnDefined SHAPE TYPE");
       return;
     }

     if(supladptr_get_status_si_session()== 1){
         supladptr_reset_status_si_session();
         requester.is_client_others = true;
     }
     else{
         /* SUPL dose not provide SI or NI Position */
         requester.is_client_others = false;
     }

     UP_OPER_BIN(UP_ADAP_LOGB_ID_POS_RESULT, 1, (void*)&cnnt_pos_result);
     connect_if_nw_loc_results(cnnt_pos_result, requester);
     return ;

}

/**
 * Function:        SUPLC_Evt_change_qop
 * Brief:           
 * Description:     Rx. change in QoP 
 * Note:
 * Params:          
 * Return:          none.
 */
void SUPLC_Evt_change_qop(SUPL_QOP_PARAMS *pstQOPParam,
                SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps)
{
    struct loc_instruct req;
    int return_vlue = -1;
    time_t now;
    unsigned short ni_fixtype = 0xFFFF;

    time(&now);
    SUPLADTR_LOGD("RECV->SUPLC_Evt_change_qop Rx Time :!!! %s\n",ctime(&now));
    if(pstQOPParam){
    /* : Need tobe Configurable */
    memset(&req,0x00,sizeof(struct loc_instruct));

    if(1 == supladptr_get_status_connect_session()){
        /* NI Session */
        supl_get_NI_FixType(&ni_fixtype);
        switch(ni_fixtype){
            case NI_MSB_HOT_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_POS;
            }
            break;

            case NI_MSA_HOT_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_MSR;
            }
            break;

            case NI_MSA_COLD_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_MSR;
            }
            break;

            default:
            case NI_MSB_COLD_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_POS;
            }
            break;
       }
    }
    else{
      /* SI Session */
      req.type_flags = UE_LCS_CAP_AGNSS_POS|UE_LCS_CAP_AGNSS_MSR/*|UE_LCS_CAP_GNSS_AUTO*/;
    }
    req.lc_methods = GNSS_GPS_PMTHD_BIT;
    req.vel_needed = false;
    supladptr_update_gps_qos(pstQOPParam,(struct loc_instruct *)&req);
     
    return_vlue = connect_if_set_loc_instruct(req);
    }
    /* : GANSS */
    return;
}

/**
 * Function:        SUPLC_Evt_ni_message_handlr
 * Brief:
 * Description:     NI Message Handler 
 * Note:
 * Params:
 * Return:          none.
 */
void SUPLC_Evt_ni_message_handlr(SUPL_NI_NOTIFICATION *pstNINotification)
{
     int retVal = 0;
     static GpsNiNotification ni_usr_req;
     time_t now;

     time(&now);
     SUPLADTR_LOGD("RECV->SUPLC_Evt_ni_message_handlr: Rx Time:%s !!!", ctime(&now));

     memset(&ni_usr_req,0x00,sizeof(GpsNiNotification));
     /* : Update GpsNiNotification with SUPL_NI_NOTIFICATION */
     ni_usr_req.size = pstNINotification->dwSize;
     ni_usr_req.notification_id = pstNINotification->nNotificationID;
     ni_usr_req.ni_type = (GpsNiType)pstNINotification->dwNIType;
     ni_usr_req.notify_flags = (GpsNiNotifyFlags)pstNINotification->dwNotifyFlags;
     ni_usr_req.timeout = pstNINotification->nTimeout;
     ni_usr_req.default_response = (GpsUserResponseType)pstNINotification->default_response;
     memcpy(ni_usr_req.requestor_id,pstNINotification->szRequestor_id,GPS_NI_SHORT_STRING_MAXLEN);
     memcpy(ni_usr_req.text,pstNINotification->szText,GPS_NI_LONG_STRING_MAXLEN);
     ni_usr_req.text[GPS_NI_LONG_STRING_MAXLEN-1] = '\0';
     ni_usr_req.requestor_id_encoding = (GpsNiEncodingType)pstNINotification->eRequestor_id_encoding;
     ni_usr_req.text_encoding = (GpsNiEncodingType)pstNINotification->eText_encoding;
     memcpy(ni_usr_req.extras,pstNINotification->szExtras,GPS_NI_LONG_STRING_MAXLEN);

     SUPLADTR_LOGD("SUPLC_Evt_ni_message_handlr: Rx From SUPL 20 !!! \n");

     supladptr_write_ni_nty(&ni_usr_req);
}



/**
 * Function:        SUPLC_Evt_notify_3rdpty_loc
 * Brief:
 * Description:     Notification of Thrid Party location  
 * Note:
 * Params:
 * Return:          none.
 */
void SUPLC_Evt_notify_3rdpty_loc(SUPL_SET_ID *pstSETId, SUPL_POSITION *pstGpsPositionFix)
{
     time_t now;
     time(&now);
     SUPLADTR_LOGD("RECV->SUPLC_Evt_notify_3rdpty_loc: Rx Time:%s !!!", ctime(&now));

}

/**
 * Function:       SUPLC_Evt_log_msg
 * Brief:
 * Description:    Log Message Event 
 * Note:
 * Params:
 * Return:          none.
 */
void SUPLC_Evt_log_msg(int nMsgLength ,char *pMsg)
{
     time_t now;
     time(&now);
     pMsg[nMsgLength-1] ='\0';
     SUPLADTR_LOGD("RECV->SUPLC_Evt_log_msg:%s!!!",pMsg);

        UP_OPER_STR("%s", pMsg);
}

/**
 * Function:       SUPLC_Evt_GANSS_Cap
 * Brief:
 * Description:    Requested for GANSS Capabilities. 
 * Note:
 * Params:
 * Return:         none.
 */
void SUPLC_Evt_GANSS_Cap(SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps)
{

}

static void supladptr_processs_recvAGANSSAssistance(SUPL_GANSS_ASSISTANCE_DATA *pstGanssAsstData)
{

}
static void supladptr_processs_recvAddlAsstData(SUPL_ADDITIONAL_ASSISTANCE_DATA *pstAddlAsstData)
{

}
static void supladptr_processs_recvAGPSAssistance(SUPL_GPS_ASSISTANCE_DATA *pstGpsAssistanceData)
{
    int dwFieldValidity;

    dwFieldValidity = pstGpsAssistanceData->dwFieldValidity;
    SUPLADTR_LOGD("SUPLC_Evt_inject_assistance Rx Assistance 0x%x\n",dwFieldValidity);

    if(SUPL_GET_REF_TIME_PRESENT(dwFieldValidity)){
      supladptr_inject_gpsTime(pstGpsAssistanceData);        
    }

    if(SUPL_GET_REF_LOCATION_PRESENT(dwFieldValidity)){
      supladptr_inject_ref_location(pstGpsAssistanceData);        
    }

    if(SUPL_GET_IONOSPHERIC_PRESENT(dwFieldValidity)){
      supladptr_inject_ionospheric(pstGpsAssistanceData);        
    }

    if(SUPL_GET_UTC_MODEL_PRESENT(dwFieldValidity)){
      supladptr_inject_utc_model(pstGpsAssistanceData);        
    }

    if(SUPL_GET_DGPS_CORRECTION_PRESENT(dwFieldValidity)){
      supladptr_inject_dgps_correction(pstGpsAssistanceData);        
    }

    if(SUPL_GET_NAVMODEL_ELEMENT_PRESENT(dwFieldValidity)){
      supladptr_inject_navmodel(pstGpsAssistanceData);        
    }

    if(SUPL_GET_ALMANAC_ELEMENT_PRESENT(dwFieldValidity)){
      supladptr_inject_almanac(pstGpsAssistanceData);        
    }

    if(SUPL_GET_ACQUIST_DATA_PRESENT(dwFieldValidity)){
      supladptr_inject_acquistData(pstGpsAssistanceData);        
    }

    if(SUPL_GET_RTI_DATA_PRESENT(dwFieldValidity)){
      supladptr_inject_rti_data(pstGpsAssistanceData);        
    }
	
    //connect_if_assist2ue_done();

}

static void supladptr_inject_gpsTime(SUPL_GPS_ASSISTANCE_DATA *pstGpsAssistanceData)
{
    struct nw_assist_id assit_id;
    struct gps_ref_time assist_ref_time;
    int dpxy_sv_count,contr;
    int avlbleSVData;
	
    memset(&assit_id,0x00,sizeof(struct nw_assist_id));
    memset(&assist_ref_time,0x00,sizeof(struct gps_ref_time));

    assit_id.type_select = NTV_ICD;
    assit_id.gnss_icd_ie = a_GPS_TIM;
    
    //assist_ref_time.time.n_wk_ro = 0;
    assist_ref_time.time.n_wk_ro = 1;
    /* :If gpsWeek is 10 bit then it need to add 1024 */
    assist_ref_time.time.week_nr 
         //= (pstGpsAssistanceData->gpsTime.gpsWeek+1024);
         = (pstGpsAssistanceData->gpsTime.gpsWeek);

    /* :If gpsTOW is 23 bit then it need to Multiply by 80 */
    assist_ref_time.time.tow_unit = sec_8by100;
    assist_ref_time.time.tow 
         //= (pstGpsAssistanceData->gpsTime.gpsTOW23b*80);
         = (pstGpsAssistanceData->gpsTime.gpsTOW23b);

    assist_ref_time.accuracy = coarse;
    assist_ref_time.time_unc = 83;
    //assist_ref_time.time_unc = 2000000;

    /* SV Time */
    for(contr = 0; contr < MAX_SUPL_SAT; contr++)
    {
        assist_ref_time.atow[contr].svid
                 = pstGpsAssistanceData->SLPGPSTOWassist[contr].satelliteID;
        assist_ref_time.atow[contr].tlm_word
                 = pstGpsAssistanceData->SLPGPSTOWassist[contr].tlmWord;
        assist_ref_time.atow[contr].spoof_flag
                 = pstGpsAssistanceData->SLPGPSTOWassist[contr].antiSpoof;
        assist_ref_time.atow[contr].alert_flag
                 = pstGpsAssistanceData->SLPGPSTOWassist[contr].alert;
        assist_ref_time.atow[contr].tlm_rsvd
                 = pstGpsAssistanceData->SLPGPSTOWassist[contr].tlmRsvdBits;
    }
	
    //SUPLADTR_LOGD("SUPLC_Evt_inject_assistance Set GPS Time Assistance\n");
    connect_if_ue_add_assist(assit_id, (void *)(& assist_ref_time),
                                  (int)(1));
}

static void supladptr_inject_ref_location(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct location_desc cnnt_location_desc;  
     SUPL_REF_LOCATION *p_supl_ref_location;
     
     double     		f_Uncertain;
     unsigned char      u_RefLocShape;  /* shape of the reference location */
     unsigned char      uUncertainSemiMajor, uUncertainSemiMinor;
     struct timeval  	now;

     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
     memset(&cnnt_location_desc ,0x00,sizeof(struct location_desc));
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_REF_POS;

     p_supl_ref_location =(SUPL_REF_LOCATION *) (&(p_in_gps_assistData->SLPRefLocation));

     //SUPLADTR_LOGD("get_postn_assist:Inject Reference Poisition");

     cnnt_location_desc.longitude_N = p_supl_ref_location->nLongitude;
     cnnt_location_desc.latitude_N  = p_supl_ref_location->dwLatitude;
     cnnt_location_desc.lat_sign    = ((p_supl_ref_location->byLatitudeSign == 0)?location_desc::north:location_desc::south);

     uUncertainSemiMajor = ASSISTANCE_DEFULT_POS_UNCRTNTY;
     cnnt_location_desc.altitude_N = ASSISTANCE_DEFULT_ALTITUDE;
     cnnt_location_desc.alt_dir = ((ASSISTANCE_DEFULT_ALTITUDE_SIGN == 0)?location_desc::height:location_desc::depth);

     u_RefLocShape = p_supl_ref_location->byShapeCode;
     SUPLADTR_LOGD("Rx SHAPE Type 0x%x", u_RefLocShape);

     switch(u_RefLocShape)
     {
       case TYPE_SHAPE_ELLIPSOID:{
         cnnt_location_desc.shape = ellipsoid; 
       }
       break;

       case TYPE_SHAPE_ELLIPSOID_UNCERT_CIRCLE:{
         cnnt_location_desc.shape = ellipsoid_with_c_unc; 
		 cnnt_location_desc.unc_semi_maj_K =
                     p_supl_ref_location->byUncMajor;
       }
       break;

       case TYPE_SHAPE_ELLIPSOID_UNCERT_ELLIPSE:{
         cnnt_location_desc.shape = ellipsoid_with_e_unc; 
		 cnnt_location_desc.unc_semi_maj_K =
                     p_supl_ref_location->byUncMajor;
		 cnnt_location_desc.unc_semi_min_K =
                     p_supl_ref_location->byUncMinor;
		 cnnt_location_desc.orientation =
                     p_supl_ref_location->byOrientMajor;
         cnnt_location_desc.confidence = 
                     p_supl_ref_location->byConfidence;
       }
       break;

       case TYPE_SHAPE_ELLIPSOID_ALTITUDE:{
         cnnt_location_desc.shape = ellipsoid_with_v_alt; 
         cnnt_location_desc.altitude_N = 
		             p_supl_ref_location->wAltitude;
         cnnt_location_desc.alt_dir =
                     ((p_supl_ref_location->byAltSign == 0)?location_desc::height:location_desc::depth);
       }
       break;

       case TYPE_SHAPE_ELLIPSOID_ALTITUDE_UNCERT_ELLIPSE:{
         cnnt_location_desc.shape = ellipsoid_with_v_alt_and_e_unc; 
         cnnt_location_desc.altitude_N = 
                     p_supl_ref_location->wAltitude;
         cnnt_location_desc.alt_dir   = 
                     ((p_supl_ref_location->byAltSign == 0)?location_desc::height:location_desc::depth);
		 cnnt_location_desc.unc_semi_maj_K = 
                     p_supl_ref_location->byUncMajor;
		 cnnt_location_desc.unc_semi_min_K =
                     p_supl_ref_location->byUncMinor;
		 cnnt_location_desc.orientation =
                     p_supl_ref_location->byOrientMajor;
         cnnt_location_desc.confidence = 
                     p_supl_ref_location->byConfidence;
       }
       break;

       case TYPE_SHAPE_POLYGON:{
          SUPLADTR_LOGD("Polygon SHAPE TYPE NOT Supported");
       }
       
       case TYPE_SHAPE_ELLIPSOID_ARC:{
          SUPLADTR_LOGD("ELLPSOID ARC SHAPE TYPE NOT Supported");
       }
       default:
             SUPLADTR_LOGD("UnDefined SHAPE TYPE");
       return;
     }
	
	connect_if_ue_add_assist(assit_id, (void *)(& cnnt_location_desc),
                                  (int)(1));
    return;
}

static void supladptr_inject_ionospheric(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct gps_ion cnnt_gps_ion;
     
     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
	 memset(&cnnt_gps_ion,0x00,sizeof(struct gps_ion));
	 
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_GPS_ION;
     
     cnnt_gps_ion.Alpha0  = p_in_gps_assistData->IonoModel.alfa0;
     cnnt_gps_ion.Alpha1  = p_in_gps_assistData->IonoModel.alfa1;
     cnnt_gps_ion.Alpha2  = p_in_gps_assistData->IonoModel.alfa2;
     cnnt_gps_ion.Alpha3  = p_in_gps_assistData->IonoModel.alfa3;
     cnnt_gps_ion.Beta0   = p_in_gps_assistData->IonoModel.beta0;
     cnnt_gps_ion.Beta1   = p_in_gps_assistData->IonoModel.beta1;
     cnnt_gps_ion.Beta2   = p_in_gps_assistData->IonoModel.beta2;
     cnnt_gps_ion.Beta3   = p_in_gps_assistData->IonoModel.beta3;
     
	 connect_if_ue_add_assist(assit_id, (void *)(& cnnt_gps_ion),
                                  (int)(1));
     return; 
}

static void supladptr_inject_utc_model(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct gps_utc cnnt_gps_utc;
     
     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
	 memset(&cnnt_gps_utc,0x00,sizeof(struct gps_utc));
	 
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_GPS_UTC;

     cnnt_gps_utc.A1          = p_in_gps_assistData->Utc.utcA1;
     cnnt_gps_utc.A0          = p_in_gps_assistData->Utc.utcA0;
     cnnt_gps_utc.Tot         = p_in_gps_assistData->Utc.utcTot;
     cnnt_gps_utc.WNt         = p_in_gps_assistData->Utc.utcWNt;
     cnnt_gps_utc.DeltaTls    = p_in_gps_assistData->Utc.utcDeltaTls;
     cnnt_gps_utc.WNlsf       = p_in_gps_assistData->Utc.utcWNlsf;
     cnnt_gps_utc.DN          = p_in_gps_assistData->Utc.utcDN;
     cnnt_gps_utc.DeltaTlsf   = p_in_gps_assistData->Utc.utcDeltaTlsf;
	 
	 connect_if_ue_add_assist(assit_id, (void *)(& cnnt_gps_utc),
                                  (int)(1));
     return;
}

static void supladptr_inject_dgps_correction(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct dgps_sat *cnnt_dgps_sat;
	 int nIndex, dgps_Nsat;
     
     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
	 dgps_Nsat  = p_in_gps_assistData->DGPSCorrectionData.byNoOfValidSats;
	 dgps_Nsat = (dgps_Nsat > MAX_SUPL_SAT_GPS)?MAX_SUPL_SAT_GPS:dgps_Nsat;
	 
	 cnnt_dgps_sat = (struct dgps_sat *) new struct dgps_sat[dgps_Nsat];
	 memset(cnnt_dgps_sat,0x00,(sizeof(struct nw_assist_id)*dgps_Nsat));
	 
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_GPS_DGP;
      
     /* Send Connect command for each SV */
     for(nIndex =0; nIndex < dgps_Nsat; nIndex++)
     {
	 	 cnnt_dgps_sat[nIndex].tow_unit = milli_secs; /*  */
         cnnt_dgps_sat[nIndex].gps_tow  
		              = p_in_gps_assistData->DGPSCorrectionData.dwGPSTOW;
         cnnt_dgps_sat[nIndex].health   
		              = p_in_gps_assistData->DGPSCorrectionData.byStatus;
		 cnnt_dgps_sat[nIndex].svid
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].bySatelliteID;
         cnnt_dgps_sat[nIndex].iod
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].byIODE;
         cnnt_dgps_sat[nIndex].udre
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].byUDRE;
         cnnt_dgps_sat[nIndex].prc
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].wPseudoRangeCor;
         cnnt_dgps_sat[nIndex].rrc
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].byRangeRateCor;
         cnnt_dgps_sat[nIndex].dprc2
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].byDeltaPseudoRangeCor2;
         cnnt_dgps_sat[nIndex].drrc2
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].byDeltaRangeRateCor2;
         cnnt_dgps_sat[nIndex].dprc3
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].byDeltaPseudoRangeCor3;
         cnnt_dgps_sat[nIndex].dprc3
                      = p_in_gps_assistData->DGPSCorrectionData.stSatElement[nIndex].byDeltaRangeRateCor3;
     }
	          
     connect_if_ue_add_assist(assit_id, (void *)(cnnt_dgps_sat),
                                  (int)(dgps_Nsat));
								  
	 delete [](struct dgps_sat *)(cnnt_dgps_sat);
	 
     return;
}

static void supladptr_inject_navmodel(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct gps_eph *cnnt_gps_eph = NULL;
	 SUPL_NAVMODEL_ELEMENT *p_SLPNavModelElement = NULL;
     int contr, avlbleSVData;
	 
     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
	  
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_GPS_EPH;
	 
     p_SLPNavModelElement = &(p_in_gps_assistData->SLPNavModelElement[0]);
     avlbleSVData = p_in_gps_assistData->nNavModelElement;
	 SUPLADTR_LOGD(" SUPL NavModel Total Rec SV %d",avlbleSVData);
     avlbleSVData = (avlbleSVData > MAX_SUPL_SAT)?MAX_SUPL_SAT:avlbleSVData;
	 
	 /* : NULL Pointer Verification */
	 cnnt_gps_eph = new struct gps_eph[avlbleSVData];
	 memset(cnnt_gps_eph,0x00,(sizeof(struct gps_eph)*avlbleSVData));
	   
     for(contr = 0; contr < avlbleSVData; contr++)
     {
	        supladptr_print_ephemeris(&(p_SLPNavModelElement[contr]));
            cnnt_gps_eph[contr].svid
                           = p_SLPNavModelElement[contr].satelliteID;
            cnnt_gps_eph[contr].status
						   = (enum gps_eph_sat_status)p_SLPNavModelElement[contr].satelliteID;
            cnnt_gps_eph[contr].Code_on_L2
                          = p_SLPNavModelElement[contr].Ephemeris.ephemCodeOnL2;
            cnnt_gps_eph[contr].Ura
                          = p_SLPNavModelElement[contr].Ephemeris.ephemURA;
            cnnt_gps_eph[contr].Health
                          = p_SLPNavModelElement[contr].Ephemeris.ephemSVhealth;
            cnnt_gps_eph[contr].IODC
                          = p_SLPNavModelElement[contr].Ephemeris.ephemIODC;
            cnnt_gps_eph[contr].Tgd
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemTgd /*-128*/);
            cnnt_gps_eph[contr].Toc
                          = p_SLPNavModelElement[contr].Ephemeris.ephemToc;
            cnnt_gps_eph[contr].Af2
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemAF2/*-128*/);
            cnnt_gps_eph[contr].Af1
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemAF1 /*-32768*/);
            cnnt_gps_eph[contr].Af0
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemAF0 /*-2097152*/);
            cnnt_gps_eph[contr].Crs
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemCrs/*-32768*/);
            cnnt_gps_eph[contr].DeltaN
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemDeltaN/*-32768*/);
            cnnt_gps_eph[contr].Mo
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemM0/*-2147483648L*/);
            cnnt_gps_eph[contr].Cuc
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemCuc/*-32768*/);
            cnnt_gps_eph[contr].E
                          = p_SLPNavModelElement[contr].Ephemeris.ephemE;
            cnnt_gps_eph[contr].Cus
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemCus/*- 32768*/);
            cnnt_gps_eph[contr].SqrtA
                          = p_SLPNavModelElement[contr].Ephemeris.ephemAPowerHalf;
            cnnt_gps_eph[contr].Toe
                          = p_SLPNavModelElement[contr].Ephemeris.ephemToe;
            cnnt_gps_eph[contr].FitIntervalFlag
						  = p_SLPNavModelElement[contr].Ephemeris.ephemFitFlag;
            cnnt_gps_eph[contr].Aodo
						  = p_SLPNavModelElement[contr].Ephemeris.ephemAODA;
            cnnt_gps_eph[contr].Cic
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemCic/*-32768*/);
            cnnt_gps_eph[contr].Omega0
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemOmegaA0/*-2147483648L*/);
            cnnt_gps_eph[contr].Cis
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemCis/*-32768*/);
            cnnt_gps_eph[contr].Io
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemI0/*-2147483648L*/);
            cnnt_gps_eph[contr].Crc
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemCrc/*-32768*/);
            cnnt_gps_eph[contr].Omega
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemW/*-2147483648L*/);
            cnnt_gps_eph[contr].OmegaDot
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemOmegaADot/*-8388608*/);
            cnnt_gps_eph[contr].Idot
                          = (p_SLPNavModelElement[contr].Ephemeris.ephemIDot/*-8192*/);
            /* cnnt_gps_eph[contr].status */
            cnnt_gps_eph[contr].is_bcast=1;
	    
	}
	
    connect_if_ue_add_assist(assit_id, (void *)(cnnt_gps_eph),
                                  (int)(avlbleSVData));

	 delete []cnnt_gps_eph;
     return;
}

static void supladptr_print_ephemeris(SUPL_NAVMODEL_ELEMENT *p_SLPNavModelElement)
{

    if( p_SLPNavModelElement == NULL)
	{
		SUPLADTR_LOGD("SUPL Navmodel Pointer is NULL");
		return;
    }
	SUPLADTR_LOGD("**************** SUPL NavModel ******************* ");
	SUPLADTR_LOGD(" SUPL NavModel .SV ID 0x%x",p_SLPNavModelElement->satelliteID);
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemCodeOnL2 0x%x",p_SLPNavModelElement->Ephemeris.ephemCodeOnL2);
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemURA 0x%x",p_SLPNavModelElement->Ephemeris.ephemURA);
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemSVhealth 0x%x",p_SLPNavModelElement->Ephemeris.ephemSVhealth );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemIODC 0x%x",p_SLPNavModelElement->Ephemeris.ephemIODC );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemTgd 0x%x",p_SLPNavModelElement->Ephemeris.ephemTgd );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemToc 0x%x",p_SLPNavModelElement->Ephemeris.ephemToc );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemAF2 0x%x",p_SLPNavModelElement->Ephemeris.ephemAF2 );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemAF1 0x%x",p_SLPNavModelElement->Ephemeris.ephemAF1 );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemAF0 0x%x",p_SLPNavModelElement->Ephemeris.ephemAF0 );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemCrs 0x%x",p_SLPNavModelElement->Ephemeris.ephemCrs );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemDeltaN 0x%x",p_SLPNavModelElement->Ephemeris.ephemDeltaN );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemM0 0x%x",p_SLPNavModelElement->Ephemeris.ephemM0 );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemCuc 0x%x",p_SLPNavModelElement->Ephemeris.ephemCuc );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemE 0x%x",p_SLPNavModelElement->Ephemeris.ephemE );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemCus 0x%x",p_SLPNavModelElement->Ephemeris.ephemCus );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemAPowerHalf 0x%x",p_SLPNavModelElement->Ephemeris.ephemAPowerHalf );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemToe 0x%x",p_SLPNavModelElement->Ephemeris.ephemToe );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemCic 0x%x",p_SLPNavModelElement->Ephemeris.ephemCic );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemOmegaA0 0x%x",p_SLPNavModelElement->Ephemeris.ephemOmegaA0 );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemCis 0x%x",p_SLPNavModelElement->Ephemeris.ephemCis );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemI0 0x%x",p_SLPNavModelElement->Ephemeris.ephemI0 );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemCrc 0x%x",p_SLPNavModelElement->Ephemeris.ephemCrc );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemW 0x%x",p_SLPNavModelElement->Ephemeris.ephemW );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemOmegaADot 0x%x",p_SLPNavModelElement->Ephemeris.ephemOmegaADot );
	SUPLADTR_LOGD(" SUPL NavModel .Ephemeris.ephemIDot 0x%x",p_SLPNavModelElement->Ephemeris.ephemIDot );
	SUPLADTR_LOGD("**************** SUPL NavModel ******************* ");
	
	return;
}

static void supladptr_inject_almanac(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct gps_alm *cnnt_gps_alm = NULL;
	 SUPL_ALMANAC_ELEMENT  *p_slp_almanac_assist;
     int contr, avlbleSVData;

     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
	  
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_GPS_ALM;

     p_slp_almanac_assist = &(p_in_gps_assistData->SLPAlmanacElement[0]);
     avlbleSVData = p_in_gps_assistData->nAlmanacElement;
	 //SUPLADTR_LOGD(" SUPL Almanac Total Rec SV %d",avlbleSVData);
     avlbleSVData = (avlbleSVData > MAX_SUPL_SAT)?MAX_SUPL_SAT:avlbleSVData;
	
	 /* : NULL Pointer Verification */
	 cnnt_gps_alm = new struct gps_alm[avlbleSVData];
	 memset(cnnt_gps_alm,0x00,(sizeof(struct gps_alm)*avlbleSVData));
	 
     for(contr = 0; contr < avlbleSVData; contr++){
          cnnt_gps_alm[contr].svid
                          = p_slp_almanac_assist[contr].satelliteID;
          cnnt_gps_alm[contr].E
                          = p_slp_almanac_assist[contr].almanacE;
          cnnt_gps_alm[contr].Toa
                          = p_slp_almanac_assist[contr].alamanacToa;
		/* : cnnt_gps_alm[contr].wna,  cnnt_gps_alm[contr].dataId */
          /* cnnt_gps_alm[contr].almanac_ksii
                          = p_slp_almanac_assist[contr].almanacKsii; */
		  cnnt_gps_alm[contr].OmegaDot
                          = p_slp_almanac_assist[contr].almanacOmegaDot;
          cnnt_gps_alm[contr].Health
                          = p_slp_almanac_assist[contr].almanacSVhealth;
          cnnt_gps_alm[contr].SqrtA
                          = p_slp_almanac_assist[contr].almanacAPowerHalf;
          cnnt_gps_alm[contr].OmegaZero
                          = p_slp_almanac_assist[contr].almanacOmega0;
          cnnt_gps_alm[contr].Omega
                          = p_slp_almanac_assist[contr].almanacW;
          cnnt_gps_alm[contr].Mzero
                          = p_slp_almanac_assist[contr].almanacM0;
          cnnt_gps_alm[contr].Af0
                          = p_slp_almanac_assist[contr].almanacAF0;
          cnnt_gps_alm[contr].Af1
                          = p_slp_almanac_assist[contr].almanacAF1;
		 
     }
		
	 connect_if_ue_add_assist(assit_id, (void *)(cnnt_gps_alm),
                                  (int)(avlbleSVData));
	 delete []cnnt_gps_alm;
     return;
}

static void supladptr_inject_acquistData(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct gps_acq *cnnt_gps_acq = NULL;
     SUPL_GPS_ACQUIST_ELEM  *p_acquist_data = NULL;
     int contr,avlbleSVData;

     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
	  
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_GPS_ACQ;
	   
     p_acquist_data = &(p_in_gps_assistData->GPSAcquistData.AcquistElem[0]);
     avlbleSVData = p_in_gps_assistData->GPSAcquistData.byNoOfValidAcquistElem;
	 //SUPLADTR_LOGD("SUPL Acquist Data Total Rec SV %d",avlbleSVData);
	 avlbleSVData = (avlbleSVData > MAX_SUPL_SAT_GPS)?MAX_SUPL_SAT_GPS:avlbleSVData;
		 
	 /* : NULL Pointer Verification */
	 cnnt_gps_acq = new struct gps_acq[avlbleSVData];
	 memset(cnnt_gps_acq,0x00,(sizeof(struct gps_acq)*avlbleSVData));

     for(contr = 0; contr < avlbleSVData; contr++){
          cnnt_gps_acq[contr].tow_unit = milli_secs;
		  cnnt_gps_acq[contr].gps_tow
                   = p_in_gps_assistData->GPSAcquistData.gwGPSTOW;
          cnnt_gps_acq[contr].svid
                   = p_acquist_data[contr].bySatelliteID;
          cnnt_gps_acq[contr].Doppler0
                   = (p_acquist_data[contr].sDoppler0);

          if(p_acquist_data[contr].bAdditionalDopplerPresent){
		    /* : Flag Needed */
            /* cnnt_gps_acq[contr].v_aa_doppler1
                   = MCP_TRUE; */
            cnnt_gps_acq[contr].Doppler1
                   = p_acquist_data[contr].byDoppler1;
			/* : Flag Needed */
            /* cnnt_gps_acq[contr].v_aa_doppler_uncertainty
                   = MCP_TRUE; */
            cnnt_gps_acq[contr].DopplerUnc
                   = p_acquist_data[contr].byDopplerUncertainty;
          }
          else {
            /* cnnt_gps_acq[contr].v_aa_doppler1
                   = MCP_FALSE; */
            cnnt_gps_acq[contr].Doppler1
                   = 0xFF;
            /* cnnt_gps_acq[contr].v_aa_doppler_uncertainty
                   = MCP_FALSE; */
            cnnt_gps_acq[contr].DopplerUnc
                   = 0xFF;
          }

		  cnnt_gps_acq[contr].CodePhase
                   = p_acquist_data[contr].wCodePhase;
          cnnt_gps_acq[contr].IntCodePhase
                   = p_acquist_data[contr].byIntCodePhase;
          cnnt_gps_acq[contr].GPSBitNum
                   = p_acquist_data[contr].byGPSBitNumber;
          cnnt_gps_acq[contr].SrchWin
                   = p_acquist_data[contr].byCodePhaseSearchWindow;

          if(p_acquist_data[contr].bAdditionalAnglePresent){
             /* cnnt_gps_acq[contr].v_aa_azimuth
                    = MCP_TRUE; */
             cnnt_gps_acq[contr].Azimuth
                   = p_acquist_data[contr].byAzimuth;
             /* cnnt_gps_acq[contr].v_aa_elevation
                   = MCP_TRUE; */
             cnnt_gps_acq[contr].Elevation
                   = p_acquist_data[contr].byElevation;
          }
          else{
             /* cnnt_gps_acq[contr].v_aa_azimuth
                    = MCP_FALSE; */
             cnnt_gps_acq[contr].Azimuth
                    = 0xFF;
             /* cnnt_gps_acq[contr].v_aa_elevation
                    = MCP_FALSE; */
             cnnt_gps_acq[contr].Elevation
                    = 0xFF;
          }
          
          /* : cnnt_gps_acq[contr].use_flag */
     }

	 connect_if_ue_add_assist(assit_id, (void *)(cnnt_gps_acq),
                                  (int)(avlbleSVData));
	 delete []cnnt_gps_acq;
     return;
}

static void supladptr_inject_rti_data(SUPL_GPS_ASSISTANCE_DATA *p_in_gps_assistData)
{
     struct nw_assist_id assit_id;
     struct gps_rti *cnnt_gps_rti = NULL;
     SUPL_BAD_SAT_LIST         *p_bad_SV_List;

     int avlbleSVData, contr;
     char sv_id;

     memset(&assit_id,0x00,sizeof(struct nw_assist_id));
	  
     assit_id.type_select = NTV_ICD;
     assit_id.gnss_icd_ie = a_GPS_RTI;
	 
     p_bad_SV_List = &(p_in_gps_assistData->BadSatellitesList);
     avlbleSVData = p_bad_SV_List->dwNoOfElements;
	 //SUPLADTR_LOGD("SUPL Acquist Data Total Rec SV %d",avlbleSVData);
	 avlbleSVData = (avlbleSVData > MAX_SUPL_SAT_GPS)?MAX_SUPL_SAT_GPS:avlbleSVData;
	 
	 /* : NULL Pointer Verification */
	 cnnt_gps_rti = new struct gps_rti[avlbleSVData];
	 memset(cnnt_gps_rti,0x00,(sizeof(struct gps_rti)*avlbleSVData));

	 for(contr = 0; contr < avlbleSVData; contr++){
           cnnt_gps_rti[contr].bad_svid 
               		   = p_bad_SV_List->SatelliteIDs[contr];
     }
	 connect_if_ue_add_assist(assit_id, (void *)(cnnt_gps_rti),
                                  (int)(avlbleSVData));
	 delete []cnnt_gps_rti;
     return;
}


static void supladptr_start_cont_session(SUPL_QOP_PARAMS *pstQOPParams,int nPeriod,
                        SUPL_SLP_SUPPORTED_GANSS_CAPABILITIES *pstSLPGANSSCaps)
{

    struct loc_instruct req;
    struct rpt_criteria rpt;
    int return_vlue = -1;
    int cont_sn_status = -1;
    unsigned short ni_fixtype = 0xFFFF;

    if(pstQOPParams){
        memset(&req,0x00,sizeof(struct loc_instruct));
        /* Delete for NI HOT Case */
        supl_get_NI_FixType(&ni_fixtype);
        switch(ni_fixtype){
            case NI_MSB_HOT_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_POS;
                 SUPLADTR_LOGD("NI MSB HOT START");
            }
            break;

            case NI_MSA_HOT_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_MSR;
                 SUPLADTR_LOGD("NI MSA HOT START");
            }
            break;

            case NI_MSA_COLD_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_MSR;
                 supladptr_delete_aiding_data();
                 SUPLADTR_LOGD("NI MSA COLD START");
            }
            break;

            default:
            case NI_MSB_COLD_START:{
                 req.type_flags = UE_LCS_CAP_AGNSS_POS;
                 supladptr_delete_aiding_data();
                 SUPLADTR_LOGD("NI MSB COLD START");
            }
            break;
        }
        SUPLADTR_LOGD("supladptr_start_cont_session\n");
        /* Do not Set QoP */
        req.qop_intent.h_acc_M = 0;
        req.qop_intent.rsp_secs = 0;
        req.qop_intent.acc_sel = no_acc;
        /* req.type_flags = UE_LCS_CAP_AGNSS_POS|UE_LCS_CAP_AGNSS_MSR|UE_LCS_CAP_GNSS_AUTO; */
        req.lc_methods = GNSS_GPS_PMTHD_BIT;
        req.vel_needed = false;
        /* */
        return_vlue = connect_if_set_loc_instruct(req);
        /* Set periodic report parameters */
        /* Verify with Default SUPL Config */
        nPeriod = nPeriod/1000;
        rpt.interval = ((nPeriod >DFLT_SENS_REPT)?nPeriod:DFLT_SENS_REPT);
        return_vlue = connect_if_set_rpt_criteria(rpt);
        /* Start Connect Session */
        cont_sn_status = supladptr_get_status_connect_session();
        if(cont_sn_status == 0){
               connect_if_loc_start();
               supladptr_set_status_connect_session();
        }
   }
}


/*-----------------------------------------------------------*
 *            SUPL Client Command                            * 
 *-----------------------------------------------------------*/

/**
 * Function:        SUPLC_Cmd_init
 * Brief:           Initiation of SUPL Client
 * Description:
 * Note:
 * Params:          none.
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_init(void)
{

     int supl_status = 0;
     int ret_Status = 0;
     time_t now;

     SUPLC_interface_callbacks.pfnSuplLocFixReq        = SUPLC_Evt_start_location_fix_req;
     SUPLC_interface_callbacks.pfnSuplLocFixStopReq    = SUPLC_Evt_stop_location_fix_req;
     SUPLC_interface_callbacks.pfnSuplInjectAssistance = SUPLC_Evt_inject_assistance;
     SUPLC_interface_callbacks.pfnSuplSetPosition      = SUPLC_Evt_set_position;
     SUPLC_interface_callbacks.pfnSetConfParams        = SUPLC_Evt_change_qop;
     SUPLC_interface_callbacks.pfnNiNotifyHandler      = SUPLC_Evt_ni_message_handlr;
     SUPLC_interface_callbacks.pfnNotify3rd            = SUPLC_Evt_notify_3rdpty_loc;
     SUPLC_interface_callbacks.pfnLogMsg               = SUPLC_Evt_log_msg;

     SUPLC_interface_callbacks.pfnGanssCap             = SUPLC_Evt_GANSS_Cap;


     time(&now);
     SUPLADTR_LOGD("SUPL:CALL->supl_init_interface: Rx Time:%s !!!", ctime(&now));
     
     supl_status = supl_init_interface(&SUPLC_interface_callbacks);

     if(SPM_STATUS_SUCCESS != supl_status) {
        SUPLADTR_LOGD("SUPL: supl_init_interface return error");
        ret_Status = -1;
     }

     return ret_Status;
}

/**
 * Function:        SUPLC_Cmd_deinit
 * Brief:           De-initiation of SUPL Client
 * Description:
 * Note:
 * Params:          none.
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_deinit(void)
{
     int supl_status;
     int ret_Status = 0;
     time_t now;

     time(&now);
     SUPLADTR_LOGD("CALL->supl_deinit_interface: Rx Time:%s !!!", ctime(&now));

     supl_status = supl_deinit_interface();
     if(SPM_STATUS_SUCCESS != supl_status)
     {
        ret_Status = -1;
     }

     SUPLC_interface_callbacks.pfnSuplLocFixReq        = NULL;
     SUPLC_interface_callbacks.pfnSuplLocFixStopReq    = NULL;
     SUPLC_interface_callbacks.pfnSuplInjectAssistance = NULL;
     SUPLC_interface_callbacks.pfnSuplSetPosition      = NULL;
     SUPLC_interface_callbacks.pfnSetConfParams        = NULL;
     SUPLC_interface_callbacks.pfnNiNotifyHandler      = NULL;
     SUPLC_interface_callbacks.pfnNotify3rd            = NULL;
     SUPLC_interface_callbacks.pfnLogMsg               = NULL;

     return ret_Status;
}

/**
 * Function:        SUPLC_Cmd_assistanceRequest
 * Brief:           Handles MCPF SUPL Assistance Request
 * Description:
 * Note:            Internal function.
 * Params:          .
 * Return:          Success: 0.
                    Failure: -1.
 */

int SUPLC_Cmd_assistanceRequest(const struct gps_assist_req& gps_assist,
             const struct ganss_assist_req& ganss_assist)
{
     int supl_status = supl_status;
     int ret_Status = 0;
     time_t now;
     SUPL_QOP_PARAMS stQOPParams;
     SUPL_REQUEST_ASSISTANCE stRequestAssistance;
     SUPL_GANSS_POS_CAPABILITIES stGanssPosCap;
     SUPL_GANSS_ASSIST_CAPABILITIES stGanssAsstCap;
     SUPL_POSITION stSuplPosition;
     int nIndex = 0;
    
     unsigned int ganss_caps_bits = 0;
     struct ganss_ue_assist_caps ganss_assist_caps;
     /* : QoP, Position */
     memset(&stQOPParams,0x00,sizeof(SUPL_QOP_PARAMS)); 
     stQOPParams.byHorAcc = DFLT_QOP_HZTL_ACC_K;
     stQOPParams.bVeraccPresent = 1;
     stQOPParams.byVerAcc = DFLT_QOP_VRTL_ACC_K; 
      
     /* : GANSS Capabilities */
     /* 
     ganss_caps_bits = UE_CAP_AGANSS_RTI | UE_CAP_AGANSS_DGA |
                        UE_CAP_AGANSS_ALM |UE_CAP_AGANSS_MSR |
                        UE_CAP_AGANSS_NAV | UE_CAP_AGANSS_UTC |
                        UE_CAP_AGANSS_GGT | UE_CAP_AGANSS_MDL;

     memset(&ganss_assist_caps,0x00,sizeof(struct ganss_ue_assist_caps)); 
     connect_if_get_ue_get_ue_agnss_caps(ganss_caps_bits,
                                           &ganss_assist_caps);	 

     Update SUPL_GANSS_POS_CAPABILITIES and SUPL_GANSS_ASSIST_CAPABILITIES
	 
     */
     supladptr_gps_assistanceData(gps_assist, &stRequestAssistance);
	 SUPLADTR_LOGD("CNT: Assistance Request 0x%x", gps_assist.assist_req_map);
     SUPLADTR_LOGD("CALL->SUPLC_Cmd_assistanceRequest: Assistance Req 0x%x Rx Time:%s !!!", stRequestAssistance.dwReqAssistance,ctime(&now));
     
     supl_status = supl_start_session(&stQOPParams/*SUPL_QOP_PARAMS* */,
                                   &stRequestAssistance /* SUPL_REQUEST_ASSISTANCE * */,
                                   NULL /* SUPL_GANSS_REQUEST_ASSISTANCE * */,
                                   NULL /* SUPL_GANSS_POS_CAPABILITIES * */,
                                   NULL /* SUPL_GANSS_ASSIST_CAPABILITIES * */,
                                   NULL /* SUPL_POSITION * */);
     if(SPM_STATUS_SUCCESS != supl_status){
        ret_Status = -1;
     }
     
     return ret_Status; 
	 
}

/**
 * Function:        SUPLC_Cmd_reassistanceRequest
 * Brief:           Requesting SUPL Re-Assistance
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_reassistanceRequest(const struct gps_assist_req& gps_assist,
             const struct ganss_assist_req& ganss_assist)
{
     int supl_status = SPM_STATUS_SUCCESS;
     int ret_Status = 0;
     time_t now;
	 
     SUPL_REQUEST_ASSISTANCE stRequestAssistance;

     SUPLADTR_LOGD("CALL->SUPLC_Cmd_reassistanceRequest: Rx Time:%s !!!", ctime(&now));

     supladptr_gps_assistanceData(gps_assist, &stRequestAssistance);
     supl_status = supl_rerequest_assistance( &stRequestAssistance, /* SUPL_REQUEST_ASSISTANCE * */
                             	 NULL, /* SUPL_GANSS_REQUEST_ASSISTANCE * */
                                 NULL /* SUPL_POSITION * */);
     if(SPM_STATUS_SUCCESS != supl_status){
        ret_Status = -1;
     }
	 return ret_Status; 
     
}

/**
 * Function:        SUPLC_Cmd_locationFix
 * Brief:           Handles MCPF location fix
 * Description:
 * Note:            Internal function.
 *                  Location Fix need Measurement
 *                  and Location Fix
 * Params:          
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_locationFix(const struct gnss_pos_result& cnnt_pos_result,
                          const struct gps_msr_result&  gps_result,
                          const struct gps_msr_info     *gps_msr,
                          const struct ganss_msr_result&  ganss_result,
                          const struct ganss_msr_desc    *ganss_msr)
{
     int supl_status = SPM_STATUS_SUCCESS;
     int ret_Status = 0;
     time_t now;
     unsigned char uUncertainSemiMajor,u_RefLocShape;
 
     SUPL_POSITION stSuplPosition;
     SUPL_POSITION *pstSuplPosition;
     SUPL_GPS_MEASUREMENT stGPSMeasurementInfo;
     SUPL_GPS_MEASUREMENT *pstGPSMsrInfo;
	 
     SUPLADTR_LOGD("CALL->SUPLC_Cmd_locationFix: Tx Time:%s !!!", ctime(&now));

     pstSuplPosition = &stSuplPosition;
     ret_Status = supladptr_update_pos_result(cnnt_pos_result,pstSuplPosition);
     if(ret_Status != 0){
            pstSuplPosition = NULL;
     }
 
     pstGPSMsrInfo = &stGPSMeasurementInfo;
     ret_Status = supladptr_update_msr_result(gps_result,gps_msr,pstGPSMsrInfo);
     if(ret_Status != 0){
            pstGPSMsrInfo = NULL;
     }	 
 
     supl_status = supl_measfix_report(pstSuplPosition,
                     pstGPSMsrInfo,NULL,NULL);
     if(SPM_STATUS_SUCCESS != supl_status){
                 ret_Status = -1;
     }

     return ret_Status; 
}


/**
 * Function:        SUPLC_Cmd_MesurmntReport
 * Brief:           Handles MCPF location fix
 * Description:
 * Note:            Internal function.
 *                  Send Measurement Report
 *                  and Location Fix
 * Params:          
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_MesurmntReport(enum nmea_sn_id nmea,const char *data, int len)
{
     int supl_status = SPM_STATUS_SUCCESS;
     int ret_Status = 0;
     time_t now;
	 
	 
     SUPLADTR_LOGD("CALL->SUPLC_Cmd_MesurmntReport: Tx Time:%s !!!", ctime(&now));
     /*supl_status = supl_measfix_report(NULL,
                     &stGPSMeasurementInfo,NULL,NULL); */
     if(SPM_STATUS_SUCCESS != supl_status){
        ret_Status = -1;
     }
	 return ret_Status; 
}


/**
 * Function:        SUPLC_Cmd_StopSession
 * Brief:           Request to SUPL Stop SI Session.
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 * Return:          Success: GPSAL_SUCCESS.
                    Failure: GPSAL_FAILURE_XXX.
 */
int SUPLC_Cmd_StopSession(const void *const p_inPayload)
{
     SUPLSTATUS supl_status;
     int ret_Status = 0;
     int session_id = 0;
     time_t now;

     time(&now);
     SUPLADTR_LOGD("CALL->supl_stop_sisession:!!! %s\n",ctime(&now));

     /* :Update with Payload */
     if(NULL == p_inPayload){
        /* All SI non Triggered Session */
        session_id = -1;
     }
     else{
        session_id =  *((int *)p_inPayload);
     }
     
     supl_status = supl_stop_sisession(session_id);
     if(SPM_STATUS_SUCCESS != supl_status){
        ret_Status = -1;
     }

     return ret_Status;
}


/**
 * Function:        SUPLC_Cmd_send_NI_message
 * Brief:           Send NI (SUPL Init) Message to SUPL.
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 *                  payload_len - Payload Length
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_send_NI_message(const void *const p_inPayload,
                                      int payload_Len)
{

     int supl_status;
     int ret_Status = 0;
     int ni_length=0;
     BYTE *ni_message;
     time_t now;

     time(&now);
     SUPLADTR_LOGD("CALL->supl_ni_message:!!! %s\n",ctime(&now));

     /* :Update with Payload */
        ni_message = (BYTE *)p_inPayload;
        ni_length = payload_Len;
     supl_status = supl_ni_message(ni_length,ni_message);
     if(SPM_STATUS_SUCCESS != supl_status)
     {
        ret_Status = -1;
     }

     return ret_Status;
}

/**
 * Function:        SUPLC_Cmd_Notify_UsrRsp
 * Brief:           Send User Notify Resp.
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 *
 * Return:          Success: GPSAL_SUCCESS.
                    Failure: GPSAL_FAILURE_XXX.
 */
int SUPLC_Cmd_Notify_UsrRsp(int notif_id, GpsUserResponseType user_response)
{

     int supl_status = SPM_STATUS_SUCCESS;
     int ret_Status = 0;
     SUPL_NOTI_USER_RESP usr_Rsp = SUPL_GPS_NI_RESP_NORESP;
     time_t now;

     /* :Update with Payload */
     /* usr_Rsp =  user_response */
     SUPLADTR_LOGD("Notification: USR RSP to SUPL20 Notfy ID 0x%x", notif_id);
     usr_Rsp = update_Notify_UsrRsp(user_response);
     time(&now);
     SUPLADTR_LOGD("CALL->supl_notification_usr_resp:!!! %s\n",ctime(&now));

     supl_notification_usr_resp(notif_id,usr_Rsp);
     if(SPM_STATUS_SUCCESS != supl_status)
     {
        ret_Status = -1;
     }

     return ret_Status;
}



/**
 * Function:        SUPLC_Cmd_si_triggeredSession
 * Brief:           Request for SI Triggered Session
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_si_triggeredSession(const void *const p_inPayload)
{

     int supl_status;
     int ret_Status = 0;

     static SUPL_QOP_PARAMS stQOPParamsr;
     static SUPL_TRIGGER_PARAMS stTriggerParams;
     time_t now;

     /* :Update with Payload */
     time(&now);
     SUPLADTR_LOGD("CALL->supl_start_si_triggered_session:!!! %s\n",ctime(&now));

     supl_status = supl_start_si_triggered_session(&stQOPParamsr,
                        &stTriggerParams,NULL,NULL);
     if(SPM_STATUS_SUCCESS != supl_status)
     {
        ret_Status = -1;
     }

     return ret_Status;
}


/**
 * Function:        SUPLC_Cmd_get3rdparty_location
 * Brief:           Get 3rd Party Location
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 * Return:          Success: 0.
                    Failure: -1.
 */
int SUPLC_Cmd_get3rdparty_location(const void *const p_inPayload)
{

     int supl_status;
     int ret_Status = 0;

     static SUPL_SET_ID stSETIdParams;
     time_t now;

     /* :Update with Payload */
     time(&now);
     SUPLADTR_LOGD("CALL->supl_get3rdparty_location:!!! %s\n",ctime(&now));

     supl_status = supl_get3rdparty_location(&stSETIdParams,NULL,NULL);

     if(SPM_STATUS_SUCCESS != supl_status){
        ret_Status = -1;
     }
     return ret_Status;
}

/**
 * Function:        SUPLC_Cmd_send_location_3rdparty
 * Brief:           Send location to 3rd Party
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 * Return:          Success: 0.
 *                  Failure: -1.
 */
int SUPLC_Cmd_send_location_3rdparty(const void *const p_inPayload)
{

     int supl_status;
     int ret_Status = 0;

     /* static SUPL_SET_ID stSETIdParams; */
     static SUPL_THIRD_PARTY_IDS stSETIdParams;
     static SUPL_QOP_PARAMS stQOPParams;
     static SUPL_TRIGGER_PARAMS stTriggerParams;
     time_t now;

     /* :Update with Payload */
     time(&now);
     SUPLADTR_LOGD("CALL->supl_sendlocationto_3rdparty :!!! %s\n",ctime(&now));

     supl_status = supl_sendlocationto_3rdparty(&stSETIdParams,
                   &stQOPParams, &stTriggerParams);

     if(SPM_STATUS_SUCCESS != supl_status){
        ret_Status = -1;
     }
     return ret_Status;
}


/**
 * Function:        SUPLC_Cmd_SuspendSUPL
 * Brief:           Suspend SUPL Client
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 * Return:          Success: GPSAL_SUCCESS.
                    Failure: GPSAL_FAILURE_XXX.
 */
int SUPLC_Cmd_SuspendSUPL(const void *const p_inPayload)
{

     int supl_status;
     int ret_Status = 0;
     time_t now;

     time(&now);
     SUPLADTR_LOGD("CALL->supl_suspend_sessions:!!! %s\n",ctime(&now));
     supl_status = supl_suspend_sessions();
     if(SPM_STATUS_SUCCESS != supl_status)
     {

        ret_Status = -1;
     }

     return ret_Status;
}

/**
 * Function:        SUPLC_Cmd_ResumeSUPL
 * Brief:           Resume SUPL Client
 * Description:
 * Note:            Internal function.
 * Params:          p_inPayload - Data payload.
 * Return:          Success: GPSAL_SUCCESS.
                    Failure: GPSAL_FAILURE_XXX.
 */

int SUPLC_Cmd_ResumeSUPL(const void *const p_inPayload)
{

     int supl_status;
     int ret_Status = 0;
     time_t now;

     time(&now);
     SUPLADTR_LOGD("CALL->supl_suspend_sessions:!!! %s\n",ctime(&now));

     supl_status = supl_resume_sessions();
     if(SPM_STATUS_SUCCESS != supl_status)
     {

        ret_Status = -1;
     }

     return ret_Status;
}


static int supladptr_update_msr_result(const struct gps_msr_result&  gps_result,
                                       const struct gps_msr_info     *gps_msr,
                                       SUPL_GPS_MEASUREMENT *p_SUPLGPSMsrInfo)
{
     int ret_Status = 0;
     int totl_svmsr;
     SUPL_GPS_MSR_ELEM_LIST *p_supl_gps_msr;
     int nIndex;

     if((gps_result.contents == GPS_MSR_ERR_REPORTED)||
        (NULL == p_SUPLGPSMsrInfo)) {
         //SUPLADTR_LOGD("");
         return -1;
     }

     memset(p_SUPLGPSMsrInfo,0x00,sizeof(SUPL_GPS_MEASUREMENT));
     totl_svmsr = gps_result.n_sat;
     totl_svmsr = (totl_svmsr < 16 ? totl_svmsr : 16);/* SUPL Total SV */
     /* If Total SVs are Zero then SUPL will not send MSR Reprot to SUPL Server */
     if(0 == totl_svmsr){
        p_SUPLGPSMsrInfo->bGPSMeasurementValid = FALSE;
     }
     else {
        p_SUPLGPSMsrInfo->bGPSMeasurementValid = TRUE;
     }

     p_SUPLGPSMsrInfo->dwNoOfGPSMeasurements = 1; /* No. Of MSR Report */
     /* A MSR having SVs Reprot */
     p_SUPLGPSMsrInfo->gpsmsrList[0].dwNoOfMSRElements = totl_svmsr;
     p_SUPLGPSMsrInfo->gpsmsrList[0].dwGPSTOW = (gps_result.ref_msec % C_GPS_WEEK_MSEC);
     p_SUPLGPSMsrInfo->gpsmsrList[0].bRefFrameValid = FALSE;

     SUPLADTR_LOGD("GPS ToW %d, 23bTow %d", p_SUPLGPSMsrInfo->gpsmsrList[0].dwGPSTOW, (int)(p_SUPLGPSMsrInfo->gpsmsrList[0].dwGPSTOW % C_FOUR_HRS_MSEC));
     /* p_SUPLGPSMsrInfo->gpsmsrList[0].dwGPSTOW = p_SUPLGPSMsrInfo->gpsmsrList[0].dwGPSTOW % C_FOUR_HRS_MSEC; */
     SUPLADTR_LOGD("GPS Mesaurement Uncretainty %d in K", gps_result.h_acc_K);
     p_SUPLGPSMsrInfo->gpsmsrList[0].bHorAccPresent  =  TRUE;
     p_SUPLGPSMsrInfo->gpsmsrList[0].dwHorAcc  = supladptr_get_uncertainty_inR(gps_result.h_acc_K);
     p_supl_gps_msr = p_SUPLGPSMsrInfo->gpsmsrList[0].gpsmsrElements;

     for(nIndex = 0; nIndex < totl_svmsr; nIndex++){
        p_supl_gps_msr[nIndex].bySatelliteID
               = gps_msr[nIndex].svid;
        p_supl_gps_msr[nIndex].byCNo
               = (unsigned char)(gps_msr[nIndex].c_no);
     SUPLADTR_LOGD("GPS Mesaurement C/N %d, %d",p_supl_gps_msr[nIndex].byCNo,gps_msr[nIndex].c_no);
        p_supl_gps_msr[nIndex].wDopplerValue
               = gps_msr[nIndex].doppler;
        p_supl_gps_msr[nIndex].wWholeChips
               = gps_msr[nIndex].whole_chips;
        p_supl_gps_msr[nIndex].wFracChips
               = gps_msr[nIndex].frac_chips;
        p_supl_gps_msr[nIndex].eMPathIndicator
               = (SUPL_GPS_MPATH_INDICATOR)(gps_msr[nIndex].mpath_indc);
        p_supl_gps_msr[nIndex].byPseudoRngRMSErr
               = gps_msr[nIndex].pr_rms_err;
        SUPLADTR_LOGD("Doppler %d for %d SV", gps_msr[nIndex].doppler, gps_msr[nIndex].svid);
     }

         return ret_Status;
}


static int supladptr_update_pos_result(const struct gnss_pos_result& cnnt_pos_result,
                                        SUPL_POSITION * pstSuplPosition)
{
     int ret_Status = 0;
     unsigned char u_RefLocShape;
	 unsigned int seconds, useconds;
	 
     if((cnnt_pos_result.contents == POS_ERR_REPORTED)||
        (cnnt_pos_result.contents == POS_NOT_REPORTED)) {
         SUPLADTR_LOGD("Position Error OR Reported");
         return -1;
     }

     memset(pstSuplPosition,0x00,sizeof(SUPL_POSITION));
     pstSuplPosition->stSphericalPosition.bCurrentFixValid = 1;
     pstSuplPosition->stSphericalPosition.dwLatitude
                     = cnnt_pos_result.location.latitude_N;
     pstSuplPosition->stSphericalPosition.nLongitude
                     = cnnt_pos_result.location.longitude_N;
     pstSuplPosition->stSphericalPosition.byLatitudeSign
                     = ((cnnt_pos_result.location.lat_sign == location_desc::north)?0:1);
     pstSuplPosition->dwGPSTow = cnnt_pos_result.ref_msec;

     pstSuplPosition->dwUTCSeconds = cnnt_pos_result.utc_secs;

     u_RefLocShape = cnnt_pos_result.location.shape;
	 
     switch(u_RefLocShape){
         case ellipsoid: {
            pstSuplPosition->stSphericalPosition.byShapeCode 
                             = TYPE_SHAPE_ELLIPSOID;
         }
         break;
         case ellipsoid_with_c_unc: {
            pstSuplPosition->stSphericalPosition.byShapeCode 
                             = TYPE_SHAPE_ELLIPSOID_UNCERT_CIRCLE;
            pstSuplPosition->stSphericalPosition.byUncMajor
                             = cnnt_pos_result.location.unc_semi_maj_K;
         }
         break;
         case ellipsoid_with_e_unc: {
            pstSuplPosition->stSphericalPosition.byShapeCode 
                             = TYPE_SHAPE_ELLIPSOID_UNCERT_ELLIPSE;
            pstSuplPosition->stSphericalPosition.byUncMajor
                             = cnnt_pos_result.location.unc_semi_maj_K;
            pstSuplPosition->stSphericalPosition.byUncMinor
                             = cnnt_pos_result.location.unc_semi_min_K;
            pstSuplPosition->stSphericalPosition.byOrientMajor
                             = cnnt_pos_result.location.orientation;
            pstSuplPosition->stSphericalPosition.byConfidence
                             = cnnt_pos_result.location.confidence;
         }
         break;
         case ellipsoid_with_v_alt: {
             pstSuplPosition->stSphericalPosition.byShapeCode 
                             = TYPE_SHAPE_ELLIPSOID_ALTITUDE;
             pstSuplPosition->stSphericalPosition.bAltInfoValid = 1;
             pstSuplPosition->stSphericalPosition.wAltitude
                             = cnnt_pos_result.location.altitude_N;
             pstSuplPosition->stSphericalPosition.bAltSignValid = 1;
             pstSuplPosition->stSphericalPosition.byAltSign
                             = ((cnnt_pos_result.location.alt_dir == location_desc::height)?0:1);
         }
         break;
         case ellipsoid_with_v_alt_and_e_unc: {
             pstSuplPosition->stSphericalPosition.byShapeCode 
                             = TYPE_SHAPE_ELLIPSOID_ALTITUDE_UNCERT_ELLIPSE;
             pstSuplPosition->stSphericalPosition.bAltInfoValid = 1;
             pstSuplPosition->stSphericalPosition.wAltitude
                             = cnnt_pos_result.location.altitude_N;
             pstSuplPosition->stSphericalPosition.bAltSignValid = 1;
             pstSuplPosition->stSphericalPosition.byAltSign
                             = ((cnnt_pos_result.location.alt_dir == location_desc::height)?0:1);
             pstSuplPosition->stSphericalPosition.byUncMajor
                             = cnnt_pos_result.location.unc_semi_maj_K;
             pstSuplPosition->stSphericalPosition.byUncMinor
                             = cnnt_pos_result.location.unc_semi_min_K;
             pstSuplPosition->stSphericalPosition.byOrientMajor
                             = cnnt_pos_result.location.orientation;
             pstSuplPosition->stSphericalPosition.byAltUnc
                             = cnnt_pos_result.location.unc_altitude_K;
             pstSuplPosition->stSphericalPosition.byConfidence
                             = cnnt_pos_result.location.confidence;
         }
         break;
         case polygon:
         case ellipsoid_with_arc:
         default:{
             SUPLADTR_LOGD("UnDefined SHAPE TYPE");
             ret_Status = -1;
         }
     }

     UP_OPER_BIN(UP_ADAP_LOGB_ID_POS_RESULT, 1, (void*)&cnnt_pos_result);
	
     return ret_Status;
}

static void supladptr_gps_assistanceData(const struct gps_assist_req& gps_assist, 
                                     SUPL_REQUEST_ASSISTANCE * stRequestAssistance)
{
     int nIndex;
 
     /* GPS Assistance */
     memset(stRequestAssistance,0x00,sizeof(SUPL_REQUEST_ASSISTANCE)); 
	 
     if(gps_assist.assist_req_map & A_GPS_ALM_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_ALMANAC;

     if(gps_assist.assist_req_map & A_GPS_UTC_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_UTCMODEL;

     if(gps_assist.assist_req_map & A_GPS_ION_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_IONOMODEL;

     if(gps_assist.assist_req_map & A_GPS_NAV_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_NAVMODEL;

     if(gps_assist.assist_req_map & A_GPS_DGP_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_DGPS;

     if(gps_assist.assist_req_map & A_GPS_POS_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_REF_LOC;

     if(gps_assist.assist_req_map & A_GPS_TIM_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_REF_TIME;

     if(gps_assist.assist_req_map & A_GPS_ACQ_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_ACQUIST;

     if(gps_assist.assist_req_map & A_GPS_RTI_REQ)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_RTI;

      /* Optional Assistance Need Support in Connect 
     if(gps_assist.assist_req_map &)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_EPHEM_EXT;

     if(gps_assist.assist_req_map &)
         stRequestAssistance->dwReqAssistance |= SUPL_REQASST_EPHEM_EXT_CHK;
      */
     /* : Connect is not supporting Navigation Model */ 
     stRequestAssistance->stNavigationModel.gpsWeek
                         /* = gps_assist.nav_assist_req.ref_secs%C_GPS_WEEK_MSEC; */
                         = 0;
     stRequestAssistance->stNavigationModel.gpsToe
                         /* = gps_assist.nav_assist_req.gnss_toe; */
                         = 0;
     stRequestAssistance->stNavigationModel.toeLimit
                         /* = gps_assist.nav_assist_req.ttoe_age; */
                         = 0;	
     stRequestAssistance->stNavigationModel.byNumOfSatInfo
						 = 0;
						/* = gps_assist.nav_assist_req.n_of_sat; */

     stRequestAssistance->stNavigationModel.bEphExtValid    = 0;
     stRequestAssistance->stNavigationModel.bEphExtChkValid = 0;

     return;
}



static SUPL_NOTI_USER_RESP update_Notify_UsrRsp(GpsUserResponseType user_response)
{
    SUPL_NOTI_USER_RESP supl_usr_rsp;

    switch(user_response)
    {

       case GPS_NI_RESPONSE_ACCEPT:
       {
          SUPLADTR_LOGD(": USR Nofication RSP: ACCEPT");
          supl_usr_rsp = SUPL_GPS_NI_RESP_ACCEPT;
       }
       break;
       case GPS_NI_RESPONSE_DENY:
       {
         SUPLADTR_LOGD(": USR Nofication RSP: DENY");
         supl_usr_rsp = SUPL_GPS_NI_RESP_DENY;
       }
       break;
       case GPS_NI_RESPONSE_NORESP:
       {
         SUPLADTR_LOGD(": USR Nofication RSP: NORESP");
         supl_usr_rsp = SUPL_GPS_NI_RESP_NORESP;
       }
       break;
       default:
       {
          SUPLADTR_LOGD(": UNKNOW USR Nofication RSP ");
          supl_usr_rsp = SUPL_GPS_NI_RESP_NORESP;
       }
    }

    return supl_usr_rsp;
}

static void supladptr_update_gps_qos(SUPL_QOP_PARAMS *pstQOPParams, struct loc_instruct *req)
{
     
    /* Set QoP */     
    SUPLADTR_LOGD("supladptr_update_gps_qos: \n");
    req->qop_intent.acc_sel = acc_2d; /* :*/
    req->qop_intent.h_acc_M = supladptr_get_uncertainty_inR(pstQOPParams->byHorAcc);
    SUPLADTR_LOGD("SUPL Horz Acc in K %d,%dMeter ",pstQOPParams->byHorAcc,req->qop_intent.h_acc_M);
    if(pstQOPParams->bVeraccPresent){
       SUPLADTR_LOGD("SUPL Vertical Acc in R %d",pstQOPParams->byVerAcc);
       req->qop_intent.v_acc_M = supladptr_get_uncertainty_inR(pstQOPParams->byVerAcc);
    }
    else{
       req->qop_intent.v_acc_M = supladptr_get_uncertainty_inR(DFLT_QOP_VRTL_ACC_K);
    }
      
    if(pstQOPParams->bMaxLocAgePresent){
       req->qop_intent.max_age = pstQOPParams->wMaxLocAge;
       SUPLADTR_LOGD("SUPL Max location Age %d",pstQOPParams->wMaxLocAge);
    }
    else{
       req->qop_intent.max_age = DFLT_QOP_MAX_AGE;
    }
   
    if(pstQOPParams->bdelayPresent){
       req->qop_intent.rsp_secs =(unsigned short)(floor(pow((double)2,(double)pstQOPParams->byDelay)));
       SUPLADTR_LOGD("SUPL By Delay %d, %dSec",pstQOPParams->byDelay,req->qop_intent.rsp_secs);
    }
    else{
       req->qop_intent.rsp_secs = DFLT_QOP_BY_DELAY;
    }
}

static void supladptr_delete_aiding_data(void)
{
   /* Delete GPS */
   connect_if_ue_del_aiding(a_GPS_EPH,0xffffffff,0xFFFFFFFF);
   connect_if_ue_del_aiding(a_GPS_ALM,0xffffffff,0xFFFFFFFF);
   connect_if_ue_del_aiding(a_GPS_TIM,0xffffffff,0xFFFFFFFF);
   connect_if_ue_del_aiding(a_REF_POS,0xffffffff,0xFFFFFFFF);
   connect_if_ue_del_aiding(a_GPS_ION,0xffffffff,0xFFFFFFFF);
   connect_if_ue_del_aiding(a_GPS_ACQ,0xffffffff,0xFFFFFFFF);

   SUPLADTR_LOGD("Delete EPH ALM TIM Aiding Data");
   return;
}
