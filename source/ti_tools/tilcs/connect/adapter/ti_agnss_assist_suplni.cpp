/*
*
* ti_agnss_assist_suplni.cpp
*
* Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
* ALL RIGHTS RESERVED
*
*/
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <gnss.h>
#include <gps.h>
#include "ti_lc_assist_adapter.h"
#include "a-gnss_xact.h"
#include "ti_agnss_assist_supladptr.h"

#include <string.h>
#include <iostream>
#include <utils/Log.h>
#include "debug.h"


#define SUPL_LOCAL_HOST     "localhost"
#define SUPL_SOCKT_NAME     "/tmp/gps7121"
#define SOC_NAME_6121       "/tmp/gps6121"

using namespace std;

typedef struct supl_ni_struct
{
   int ni_msg_socktid;
   int ni_ntfy_socktid;
   pthread_t ni_msg_thread;
   pthread_t ni_ntfy_thread;
};

static supl_ni_struct supl_ni_db;

/* Local Functions */
static int supladptr_opn_ni_socket(void);
static void supladptr_reset_ni_db(void);
static void* supladptr_read_nimsg(void *msg);
static void supladptr_procs_nimsg(supl_ni_msg_t *rvd_msg);
static void supladptr_ni_msg_init(void);


static void supladptr_ni_ntfy_init(void);
static int supladptr_opn_ni_ntfy_socket (void);
static void* supladptr_read_ni_nty(void *msg);
static GpsUserResponseType supladptr_cnvt_ntfy_rsp(eHAL_NIResp resp);



void supladptr_ni_init(void)
{
    supladptr_reset_ni_db();
    supladptr_ni_msg_init();
    supladptr_ni_ntfy_init();
}

static void supladptr_ni_msg_init(void)
{
    int rtn_vlu = -1;
    int sockFd;
    /* */
    /* Create Socket Read Thread */
    rtn_vlu = supladptr_opn_ni_socket();
    if(rtn_vlu > 0){
       sockFd = supl_ni_db.ni_msg_socktid; 
       rtn_vlu = pthread_create(&supl_ni_db.ni_msg_thread, NULL,supladptr_read_nimsg,(void *)(&(supl_ni_db.ni_msg_socktid)));
    }
     
}

static void supladptr_ni_ntfy_init(void)
{
    int rtn_vlu = -1;
    int sockFd;
    /* */
    /* Create Socket Read Thread */
    rtn_vlu = supladptr_opn_ni_ntfy_socket();
    if(rtn_vlu > 0){
       sockFd = supl_ni_db.ni_ntfy_socktid;
       rtn_vlu = pthread_create(&supl_ni_db.ni_ntfy_thread, NULL,supladptr_read_ni_nty,(void *)(&(supl_ni_db.ni_ntfy_socktid)));

    }
}

static int supladptr_opn_ni_socket(void)
{
   int rtn_vlu = -1;
   struct sockaddr_un suplni_clntaddr;
   struct hostent *suplni_host = NULL;
   int sockFd;
   socklen_t len;

   suplni_host = gethostbyname(SUPL_LOCAL_HOST);

   if(suplni_host == NULL){
       SUPLADTR_LOGD("supladptr_opn_ni_socket: Error in gethostbyname()");
       return rtn_vlu;
   }

   sockFd = socket(AF_UNIX,SOCK_STREAM, 0);
   if(sockFd == -1) {
       SUPLADTR_LOGD("supladptr_opn_ni_socket: Error to Open Socket"); 
       perror("Socket");
       //exit(1);
       return rtn_vlu;
   }
   //unlink(SUPL_SOCKT_NAME);
   memset(&suplni_clntaddr,0x00, sizeof(struct sockaddr_un));
   suplni_clntaddr.sun_family = AF_UNIX;
   strcpy(suplni_clntaddr.sun_path,SUPL_SOCKT_NAME);
   len = strlen(suplni_clntaddr.sun_path)+sizeof(suplni_clntaddr.sun_family);
   rtn_vlu = connect(sockFd, (struct sockaddr *)&suplni_clntaddr,
			len);
   if(rtn_vlu == -1){
      SUPLADTR_LOGD("supladptr_opn_ni_socket:Socket Connection Failure");
      perror("Socket");
      //exit(1);
      close(sockFd);
      return rtn_vlu;
   }

   if(supl_ni_db.ni_msg_socktid == -1){
       supl_ni_db.ni_msg_socktid = sockFd;
       SUPLADTR_LOGD("supladptr_opn_ni_socket: Socket fd %d",sockFd);
       rtn_vlu = 1;
   }
   else{
      SUPLADTR_LOGD("supladptr_opn_ni_socket:Client connection is already Open");
   }
   return rtn_vlu; 
}

static void* supladptr_read_nimsg(void *msg)
{
    int len = 0;
    int sockFd;
    static supl_ni_msg_t rcv_ni_msg;

    if(msg == NULL){
        SUPLADTR_LOGD("Error:Rcv msg is NULL");
        return NULL;
    }
    sockFd = *((int *)(msg));
    SUPLADTR_LOGD("NI Message Socket fd %d",sockFd);
    do{
        memset(&rcv_ni_msg,0x00,sizeof(supl_ni_msg_t)); 
        len = read(sockFd,&rcv_ni_msg,sizeof(supl_ni_msg_t));
        if (len >0){
           supladptr_procs_nimsg(&rcv_ni_msg);
        }
    }while(1);
}


static void supladptr_procs_nimsg(supl_ni_msg_t *rvd_msg)
{
   if(rvd_msg->msg_len == 0){
      SUPLADTR_LOGD("NI MSG received with zero length");
      return;
   }
   
   switch(rvd_msg->ni_msg_type){
       case SUPL_NI_MSG:
         SUPLC_Cmd_send_NI_message(rvd_msg->ni_msg,rvd_msg->msg_len);
       break; 
       case SUPL_SI_TRG_MSG:
         SUPLADTR_LOGD("SI Trigger is not supported");
         //SUPLC_Cmd_si_triggeredSession(); 
       break; 
       case SUPL_SI_REQ3PRTY_MSG:
         SUPLADTR_LOGD("SI Thrid Party Location Request is not supported");
         //SUPLC_Cmd_get3rdparty_location(); 
       break; 
       case SUPL_SI_SND3PRTY_MSG:
         SUPLADTR_LOGD("Send SI Thrid Party Location is not supported");
         //SUPLC_Cmd_send_location_3rdparty();
       break; 
       default:
       break;  
   }

}

/* Get NI Notification */
/* Prcoess NI Notification */
/* Write NI Notification in Socket */


/* Rv. NI Notification meesage */
//os_create_thread

static int supladptr_opn_ni_ntfy_socket (void)
{
   int rtn_vlu = -1;
   struct sockaddr_un suplni_clntaddr;
   struct hostent *suplni_host = NULL;
   int sockFd;

   suplni_host = gethostbyname(SUPL_LOCAL_HOST);

   if(suplni_host == NULL){
       SUPLADTR_LOGD("supladptr_opn_ni_ntfy_socket: Error in gethostbyname()");
       return rtn_vlu;
   }
   sockFd = socket(AF_UNIX, SOCK_STREAM, 0);
   if(sockFd == -1) {
       SUPLADTR_LOGD("supladptr_opn_ni_ntfy_socket: Error to Open Socket");
       perror("Socket");
       //exit(1);
       return rtn_vlu;
   }

   memset(&suplni_clntaddr,0x00, sizeof(struct sockaddr_un));
   suplni_clntaddr.sun_family = AF_UNIX;
   strcpy(suplni_clntaddr.sun_path,SOC_NAME_6121);

   if(connect(sockFd, (struct sockaddr *)&suplni_clntaddr,
                                sizeof(struct sockaddr_un)) == -1){
      SUPLADTR_LOGD("supladptr_opn_ni_ntfy_socket:Socket Connection Failure");
      //exit(1);
      close(sockFd);
      return rtn_vlu;

   }

   if(supl_ni_db.ni_ntfy_socktid == -1){
       supl_ni_db.ni_ntfy_socktid = sockFd;
       SUPLADTR_LOGD("supladptr_opn_ni_ntfy_socket:Socket %d",sockFd);
       rtn_vlu = 1;
   }
   else{
      SUPLADTR_LOGD("supladptr_opn_ni_ntfy_socket:Client connection is already Open");
   }
   return rtn_vlu;
}

static void* supladptr_read_ni_nty(void *msg)
{
    int len = 0;
    int sockFd;
    static hal_ni_client_response rcv_ntfy_msg;
    GpsUserResponseType ntfy_rsp;

    if(msg == NULL){
        SUPLADTR_LOGD("Error:Rcv msg is NULL");
        return NULL;
    }
    sockFd = *((int *)(msg));
    SUPLADTR_LOGD("NI Notify Socket fd %d",sockFd);
    do{
        memset(&rcv_ntfy_msg,0x00,sizeof(rcv_ntfy_msg));
        len = read(sockFd,&rcv_ntfy_msg,sizeof(hal_ni_client_response));
        if (len >0){
          ntfy_rsp = supladptr_cnvt_ntfy_rsp(rcv_ntfy_msg.resp); 
          SUPLC_Cmd_Notify_UsrRsp(rcv_ntfy_msg.client_session_id,ntfy_rsp);
        }
    }while(1);
}


void supladptr_write_ni_nty(GpsNiNotification *pstNINotification)
{
    int sockFd;

    sockFd = supl_ni_db.ni_ntfy_socktid; 
    if((NULL == pstNINotification)||(-1 == supl_ni_db.ni_ntfy_socktid)){
       SUPLADTR_LOGD("Error:Rcv msg is %d OR Socket fd %d",pstNINotification,sockFd);
       return; 
    }
    SUPLADTR_LOGD("supladptr_write_ni_nty:NI Notify Socket fd %d",sockFd); 
    if(write(sockFd, pstNINotification, sizeof(GpsNiNotification) ) < 0){
       SUPLADTR_LOGD("supladptr_write_ni_nty: Message Sending FAILED !!! \n"); 
    }
}


static GpsUserResponseType supladptr_cnvt_ntfy_rsp(eHAL_NIResp resp)
{
    GpsUserResponseType supl_usr_rsp;

    switch(resp)
    {

       case SUPL_NI_RESPONSE_ACCEPT:
       {
          SUPLADTR_LOGD("update_Notify_UsrRsp:USR Nofication RSP: ACCEPT");
          supl_usr_rsp = GPS_NI_RESPONSE_ACCEPT;
       }
       break;
       case SUPL_NI_RESPONSE_DENY:{
         SUPLADTR_LOGD("update_Notify_UsrRsp: USR Nofication RSP: DENY");
         supl_usr_rsp = GPS_NI_RESPONSE_DENY;
       }
       break;
       case SUPL_NI_RESPONSE_NORESP:{
         SUPLADTR_LOGD("update_Notify_UsrRsp: USR Nofication RSP: NORESP");
         supl_usr_rsp = GPS_NI_RESPONSE_NORESP ;
       }
       break;
       default:{
          SUPLADTR_LOGD("update_Notify_UsrRsp: UNKNOW USR Nofication RSP ");
          supl_usr_rsp = GPS_NI_RESPONSE_NORESP;
       }
    }

    return supl_usr_rsp;
}

static void supladptr_reset_ni_db(void)
{
    memset(&supl_ni_db,0x00,sizeof(supl_ni_struct));
    supl_ni_db.ni_msg_socktid   = -1;
    supl_ni_db.ni_ntfy_socktid  = -1;
}
