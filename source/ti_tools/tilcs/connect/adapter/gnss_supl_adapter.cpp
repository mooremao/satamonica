/*
*
* gnss_supl_adapter.cpp
*
* Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
* ALL RIGHTS RESERVED
*
*/
#include <gps.h>
#include "ti_agnss_lcs_adapter.h"
#include "a-gnss_xact.h"
#include "ti_agnss_assist_supladptr.h"

#include <string.h>
#include <iostream>
#include <utils/Log.h>
#include "debug.h"


using namespace std;


struct supl_adapter_t {

        void                          *afw_hnd;
        void                          *rpc_hnd;
        struct ti_agnss_lcs_adap_cb    connect_if;
        struct agnss_lcs_user_ops      rpc_ifc;
        struct agnss_lcs_pvdr_ops      rpc_cbs;
        unsigned int                   suplcifstat;
        unsigned int                   gps_caps_bits;
        struct ganss_ue_assist_caps   ganss_assist_caps; 
};

static struct supl_adapter_t *supladap = NULL; 

/*--------------------------------------------------------------------------*/

unsigned int supladptr_get_uncertainty_inK(unsigned int r)
{
    int k_value=127;


    return k_value;
}

unsigned int supladptr_get_uncertainty_inR(unsigned int k)
{
    int r_value=1806627; /* Max Value */

    if(k < 127){
      r_value = r[k];
    }
     
    return r_value;
}

int connect_if_loc_start(void)
{
     void *hnd;
     int return_vlue = -1;

     hnd = supladap->connect_if.hnd;
     if(hnd){ 
        return_vlue = supladap->connect_if.loc_start(hnd); 
        SUPLADTR_LOGD("CNT:loc_start!");
     }
     return return_vlue;    

}

int connect_if_loc_stop(void)
{
       void *hnd;
       int return_vlue = -1;

       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.loc_stop(hnd); 
          SUPLADTR_LOGD("CNT:loc_stop!");
       }
       return return_vlue;    

}

int connect_if_get_ue_loc_caps(unsigned int& gps_caps_bits, 
                                 struct ganss_ue_lcs_caps *ganss_caps)
{
       void *hnd;
       int return_vlue = -1;

       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.get_ue_loc_caps(hnd,gps_caps_bits,ganss_caps); 
          SUPLADTR_LOGD("CNT:get_ue_loc_caps!");
       }
       return return_vlue;    
       
}

int connect_if_get_ue_get_ue_agnss_caps(unsigned int& gps_caps_bits, 
                                           struct ganss_ue_assist_caps *ganss_caps)
{
       void *hnd;
       int return_vlue = -1;

       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.get_ue_agnss_caps(hnd,gps_caps_bits,ganss_caps); 
          SUPLADTR_LOGD("CNT:get_ue_agnss_caps!");
       }
       return return_vlue;    

}

int connect_if_set_loc_instruct(const struct loc_instruct& req)
{
       void *hnd;
       int return_vlue = -1;

       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.set_loc_instruct(hnd,req); 
          SUPLADTR_LOGD("CNT:set_loc_instruct!");
       }
       return return_vlue;    

}

int connect_if_set_rpt_criteria(const struct rpt_criteria& rpt)
{
       void *hnd;
       int return_vlue = -1;

       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.set_rpt_criteria(hnd,rpt); 
          SUPLADTR_LOGD("CNT:set_rpt_criteria!");
       }
       return return_vlue;    

}

int connect_if_ue_add_assist(const struct nw_assist_id& id,
                             const void *assist_array, int num )
{
       void *hnd;
       int return_vlue = -1;
 
       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.ue_add_assist(hnd,id,assist_array,num); 
          SUPLADTR_LOGD("CNT:ue_add_assist!");
       }
       return return_vlue;    
      
}

int connect_if_ue_del_aiding(enum loc_assist assist,unsigned int sv_id_map,
                             unsigned int mem_flags)

{
       void *hnd;
       int return_vlue = -1;
 
       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.ue_del_aiding(hnd, assist, sv_id_map, mem_flags); 
          SUPLADTR_LOGD("CNT:ue_del_aiding!");
       }
       return return_vlue;    
      
}

int connect_if_ue_get_aiding(enum loc_assist assist,void *assist_array, int num)

{
       void *hnd;
       int return_vlue = -1;
 
       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.ue_get_aiding(hnd, assist,assist_array,num); 
          SUPLADTR_LOGD("CNT:ue_get_aiding!");
       }
       return return_vlue;    
}

int connect_if_assist2ue_done(const struct assist_reference& assist_usr)
{
       void *hnd;
       int return_vlue = -1;
 
       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.assist2ue_done(hnd, assist_usr);
          SUPLADTR_LOGD("CNT:assist2ue_done!");
       }
       return return_vlue;    
}

int connect_if_ue_get_req4assist(struct gps_assist_req& gps_assist,
                                           struct ganss_assist_req *ganss_assist)
{
       void *hnd;
       int return_vlue = -1;
 
       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.get_ue_req4assist(hnd,gps_assist,ganss_assist); 
          SUPLADTR_LOGD("CNT:get_ue_req4assist!");
       }
       return return_vlue;    
}

int connect_if_nw_loc_results(const struct gnss_pos_result& pos_result,
			      const struct assist_reference& assist_usr)
{
       void *hnd;
       int return_vlue = -1;
 
       hnd = supladap->connect_if.hnd;
       if(hnd){ 
          return_vlue = supladap->connect_if.nw_loc_results(hnd,pos_result,assist_usr);
          SUPLADTR_LOGD("CNT:nw_loc_results!");
       }
       return return_vlue;    
}

void supladptr_set_status_ni_session(void)
{
       if(supladap != NULL){
            BIT_SET((supladap->suplcifstat),(SUPL_NI_SESSION)); 
       }
}

void supladptr_reset_status_ni_session(void)
{
       if(supladap != NULL){
            BIT_CLEAR((supladap->suplcifstat),(SUPL_NI_SESSION)); 
       }
}

int supladptr_get_status_ni_session(void)
{
        int return_vlue = -1;

        if(supladap != NULL){
            return_vlue = BITVAL((supladap->suplcifstat),(SUPL_NI_SESSION)); 
        }
        return return_vlue;
         
} 

void supladptr_set_status_si_session(void)
{
       if(supladap != NULL){
            BIT_SET((supladap->suplcifstat),(SUPL_SI_SESSION)); 
			SUPLADTR_LOGD("CNT: supladptr_set_status_si_session");
       }
}

void supladptr_reset_status_si_session(void)
{
       if(supladap != NULL){
            BIT_CLEAR((supladap->suplcifstat),(SUPL_SI_SESSION)); 
			SUPLADTR_LOGD("CNT: supladptr_reset_status_si_session");
       }
}

int supladptr_get_status_si_session(void)
{
        int return_vlue = -1;

        if(supladap != NULL){
            return_vlue = BITVAL((supladap->suplcifstat),(SUPL_SI_SESSION)); 
			SUPLADTR_LOGD("CNT: supladptr_get_status_si_session %d",return_vlue);

        }
        return return_vlue;
         
} 

void supladptr_set_status_supl_init(void)
{
       if(supladap != NULL){
            BIT_SET((supladap->suplcifstat),(SUPL_INIT)); 
			SUPLADTR_LOGD("CNT: supladptr_set_status_supl_init");
       }
}

void supladptr_reset_status_supl_init(void)
{
       if(supladap != NULL){
            BIT_CLEAR((supladap->suplcifstat),(SUPL_INIT)); 
            SUPLADTR_LOGD("CNT: supladptr_reset_status_supl_init");
       }
}

int supladptr_get_status_supl_init(void)
{
        int return_vlue = -1;
		SUPLADTR_LOGD("CNT: supladptr_get_status_supl_init");

        if(supladap != NULL){
            return_vlue = BITVAL((supladap->suplcifstat),(SUPL_INIT)); 
			SUPLADTR_LOGD("CNT: supladptr_get_status_supl_init %d",return_vlue);
        }
        return return_vlue;
         
} 

void supladptr_set_status_connect_session(void)
{
       if(supladap != NULL){
            BIT_SET((supladap->suplcifstat),(SUPL_CONT_SESSION));
                        SUPLADTR_LOGD("CNT:supladptr_set_status_connect_session");
       }
}

void supladptr_reset_status_connect_session(void)
{
       if(supladap != NULL){
            BIT_CLEAR((supladap->suplcifstat),(SUPL_CONT_SESSION));
                        SUPLADTR_LOGD("CNT:supladptr_reset_status_connect_session");
       }
}

int supladptr_get_status_connect_session(void)
{
        int return_vlue = -1;

        if(supladap != NULL){
            return_vlue = BITVAL((supladap->suplcifstat),(SUPL_CONT_SESSION));
                        SUPLADTR_LOGD("CNT:supladptr_get_status_connect_session %d",return_vlue);

        }
        return return_vlue;

}

/*--------------------------------------------------------------------------*/


static int connect_cmd_init(const struct ti_agnss_lcs_adap_cb *adap_cb)
{
    int ret_Status = 0;
	
    SUPLADTR_LOGD("CNT: connect_cmd_init Entry");

    if(!adap_cb){
        SUPLADTR_LOGD("Connect adapter Callbacks for SUPL are NULL");
        SUPLADTR_LOGD("Exiting...");
        return -1;
    }
    memcpy(&(supladap->connect_if),adap_cb,sizeof(ti_agnss_lcs_adap_cb));
    supladap->connect_if.hnd  = adap_cb->hnd;

    /* Do SUPL IF Init */
	ret_Status = SUPLC_Cmd_init();
	if(ret_Status > -1){
	 /* Update Status as SUPL IF Init */
	 supladptr_set_status_supl_init();
	 /* Get GNSS Capabilities */
	 
	 /* Do NI Interface */
         supladptr_ni_init(); 
	}
 
    SUPLADTR_LOGD("CNT: connect_cmd_init Exit");
	return ret_Status;
}

static int connect_cmd_exit(void)
{
    SUPLADTR_LOGD("CNT: connect_cmd_exit");
     SUPLC_Cmd_deinit();
	 supladptr_reset_status_supl_init();
	 return 0;
}


static int connect_cmd_assist_req(void *hnd, 
             const struct gps_assist_req& gps_assist,
			 const struct ganss_assist_req& ganss_assist,
			  const struct assist_reference& assist_usr)
{
     int ret_Status = 0;
	 
     SUPLADTR_LOGD("CNT:connect_cmd_assist_req Entry %d, %d",supladap->connect_if.hnd,hnd);
     if(supladptr_get_status_supl_init()){
        if(assist_usr.is_client_others == true){
            if(supladptr_get_status_si_session()){
               ret_Status = SUPLC_Cmd_reassistanceRequest(gps_assist, ganss_assist);
           }
           else{
               ret_Status = SUPLC_Cmd_assistanceRequest(gps_assist, ganss_assist);
               if(ret_Status == 0){
                   supladptr_set_status_si_session();
              }
           }
        }
        else{
           if(supladptr_get_status_ni_session()){
               /* NI re-assistance */
               ret_Status = SUPLC_Cmd_reassistanceRequest(gps_assist, ganss_assist);
           }
           else {
               ret_Status = SUPLC_Cmd_assistanceRequest(gps_assist, ganss_assist);
               if(ret_Status == 0){
                   supladptr_set_status_ni_session();
              }
           }
        }

     }/* supl_init */
     SUPLADTR_LOGD("CNT:connect_cmd_assist_req Exit");
     return ret_Status; 
}

static int connect_cmd_decoded_aid(void *hnd, enum loc_assist assist,
             const void *assist_array, int num)
{
     int ret_Status = -1;
	 
     if((supladap->connect_if.hnd == hnd)
	   &&(supladptr_get_status_supl_init())){
         /* : */
     }
	 return ret_Status;
}

static int connect_cmd_rvcd_loc_results(void *hnd, 
             const struct gnss_pos_result& pos_result,
			 const struct gps_msr_result&  gps_result,
			 const struct gps_msr_info     *gps_msr,
			 const struct ganss_msr_result&  ganss_result,
			 const struct ganss_msr_desc    *ganss_msr)
{
     int ret_Status = -1;

     SUPLADTR_LOGD("CNT:connect_cmd_rvcd_loc_results  Entry %d, %d",supladap->connect_if.hnd,hnd);
     if(/*(supladap->connect_if.hnd == hnd) &&*/
        (supladptr_get_status_supl_init())){
         ret_Status = SUPLC_Cmd_locationFix(pos_result,
                                            gps_result,
                                            gps_msr,
                                            ganss_result,
                                            ganss_msr);
     }
     return ret_Status; 
}

static int connect_cmd_rvcd_nmea_report(void *hnd, 
             enum nmea_sn_id nmea,const char *data, int len)
{
     int ret_Status = -1;
	 
     if((supladap->connect_if.hnd == hnd)
	   &&(supladptr_get_status_supl_init())){
         ret_Status = SUPLC_Cmd_MesurmntReport(nmea,data,len);
     }
	 return ret_Status; 
}

static int connect_cmd_aux_info4sv(void *hnd,
                 const struct dev_gnss_sat_aux_desc&  gps_desc,
                 const struct dev_gnss_sat_aux_info  *gps_info,
                 const struct dev_gnss_sat_aux_desc&  glo_desc,
                 const struct dev_gnss_sat_aux_info  *glo_info)
{
     int ret_Status = -1;
	 
     if((supladap->connect_if.hnd == hnd)
	   &&(supladptr_get_status_supl_init())){
         /* : */
     }
     return ret_Status;
}

static int connect_cmd_src_cntrl(void *hnd, enum ue_service_ind cmd,
                                 const struct assist_reference& assist_usr)
{
    int ret_Status = 0;
    bool cnt_sn;

    SUPLADTR_LOGD("CNT:connect_cmd_src_cntrl Entry %d, %d",supladap->connect_if.hnd,hnd);
    switch(cmd){
       case e_assist_need_ended:{
            SUPLADTR_LOGD("CNT:need_ended");
            cnt_sn = assist_usr.is_client_others;
            if((cnt_sn == true)&&(supladptr_get_status_si_session()== 1)){
               supladptr_reset_status_si_session();
            }
       }
       break;
       case e_assist_need_abort:{
            /* SUPL Stops SI Session */
            cnt_sn = assist_usr.is_client_others;
            if((cnt_sn == true)&&(supladptr_get_status_si_session()== 1)){
               ret_Status = SUPLC_Cmd_StopSession(NULL);
               if(ret_Status == 0){
                   supladptr_reset_status_si_session();
               }
            }
       }
       break; 
       case e_ue_service_resume:
             //SUPLC_Cmd_ResumeSUPL(NULL);
       break;
       case e_ue_service_cancel:
            //SUPLC_Cmd_SuspendSUPL(NULL);
       break;
       dafault:{
         SUPLADTR_LOGD("SUPL Adapter is not %d Connect Source Control Command",cmd);
       } 
       break;
    }
    SUPLADTR_LOGD("CNT:connect_cmd_src_cntrl Exit");
    return ret_Status;
}

static int connect_cmd_dev_param(void *hnd,enum ue_dev_param_id id, 
                                                void *param_desc,int num)
{
    int ret_Status = 0;
    SUPLADTR_LOGD("CNT:connect_cmd_dev_param Entry %d, %d",supladap->connect_if.hnd,hnd);

    return ret_Status;
}

static void* supl2cnnt_proc_thrd(void *arg1)
{
        struct supl_adapter_t *adap = (struct supl_adapter_t *) arg1;
        

	return NULL; //  fix up the return part.
}

int supl_adap_load_func(void *data)
{
        struct ti_agnss_lcs_adap_if *suplc_adp_ifc  =  NULL;

        SUPLADTR_LOGD("CNT: supl_adap_load_func Entry");

        supladap = new supl_adapter_t;
        if(!supladap) {
                SUPLADTR_LOGD("Failed to allocate adapter for SUPL");
                SUPLADTR_LOGD("Exiting...");
                return -1;
        }

        /*----------------------------------------------------------------------
         * Provision interface to interwork with A-GNSS framework (afw).
         *--------------------------------------------------------------------*/          

        suplc_adp_ifc  = (struct ti_agnss_lcs_adap_if*) data;
        
        suplc_adp_ifc->hnd            = supladap; // To be returned by A-GNSS Framework.
        suplc_adp_ifc->init           = connect_cmd_init;
        suplc_adp_ifc->exit           = connect_cmd_exit;
        suplc_adp_ifc->cb_thread_func = supl2cnnt_proc_thrd; /* May not needed */
        suplc_adp_ifc->cb_thread_arg1 = supladap;

        suplc_adp_ifc->ue_need_assist = connect_cmd_assist_req;
        suplc_adp_ifc->ue_decoded_aid = connect_cmd_decoded_aid;
        suplc_adp_ifc->ue_loc_results = connect_cmd_rvcd_loc_results;
        suplc_adp_ifc->ue_nmea_report = connect_cmd_rvcd_nmea_report;
        suplc_adp_ifc->ue_aux_info4sv = connect_cmd_aux_info4sv;
        suplc_adp_ifc->ue_service_ctl = connect_cmd_src_cntrl;
        suplc_adp_ifc->decl_dev_param = connect_cmd_dev_param;

        SUPLADTR_LOGD("CNT: supl_adap_load_func Exit");

        return 0;
}


