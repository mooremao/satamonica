#include "gnss_lcs_common.h"
#include "gnss_ue_adapter.h"
#include <iostream>
#include <string.h>
#include <math.h>
#include <errno.h>
#include "device.h"
#include "Ftrace.h"
#include <queue>
#include <semaphore.h>
#include <stdlib.h>
#include <signal.h>
extern "C" {
#include "dev_proxy.h"
#include "ext_assist.h"
#include "nvs_utils.h"
}

#include "debug.h"
//#include "log_functions.h"
#include "gnss.h"
#include "3gpp_Utils.h"
#ifdef ANDROID
#include <utils/Log.h>

#define CONFIG_FILE_PATH      "/system/etc/gnss/config/dproxy.conf"
#define SEVICE_PACK_FILE_PATH "/system/etc/gnss/patch/dproxy.patch"
#else
#include <sys/time.h>
#include <stdlib.h>

#define CONFIG_FILE_PATH      "/etc/dproxy.conf"
#define SEVICE_PACK_FILE_PATH "/etc/dproxy.patch"

#endif
#define MIN_TTL 60 /* Move to gnss.h */

using namespace std;


struct gnss_ue_adapter {

        void                          *gnss_ue_hnd;
        struct ti_gnss_ue_callbacks    gnss_ue_cbs;
};

struct gnss_ue_adapter *ue_adap = NULL;
#define MIN_EPH 3

/*------------------------------------------------------------------------------
 * Place Holders for Data. File name space variables start with ue_xxx
 *----------------------------------------------------------------------------*/

#define UE_CLK_RPT_SET (0x01)
#define UE_MSR_RPT_SET (0x02)
#define UE_POS_RPT_SET (0x04)
#define UE_ALL_RPT_SET (UE_MSR_RPT_SET | UE_POS_RPT_SET)

static unsigned char                  ue_clk_msr_pos_per_tck;
static struct gnss_pos_result         ue_pos_result[1];
static struct gps_msr_result          ue_gps_result[1];
static struct gps_msr_info            ue_gps_msr[32];
static struct ganss_msr_result        ue_ganss_result[1];
static struct ganss_msr_desc          ue_ganss_msr_desc[1];

static struct ganss_msr_info		 ue_ganss_msr[32];
static struct dev_gnss_sat_aux_info   ue_gps_aux_info[32];
static struct dev_gnss_sat_aux_info   ue_glo_aux_info[24];
static struct dev_gnss_sat_aux_desc   ue_gps_aux_desc[1];
static struct dev_gnss_sat_aux_desc   ue_glo_aux_desc[1];

static int chk_glo_count = 0;
static struct glo_eph chk_glo_eph[24];
static unsigned int chk_glo_secs = 0;
static unsigned int chk_glo_refs = 0;

#if 1
/*Added for recovery timer*/
void* wait4meas_rep_timer ;
static unsigned int ue_recovery_count;
#define AWAIT4MEAS_REP_SECS 20
static pthread_mutex_t recovery_mtx;
/* Mutex for atomicity     */ 
timer_t timerid;
struct itimerspec meas_timer;
struct itimerspec meas_oitval;
struct sigaction sa; 
struct sigevent sev;
void proc_wt4meas_rep(int param);

int _setup_meas_timer_Reload(unsigned int secs)
{
	DBG_L1("_setup_meas_timer_Reload Entering ++");	
	int ret; 	
	meas_timer.it_value.tv_sec = secs;	
	meas_timer.it_value.tv_nsec = 0; 	
	meas_timer.it_interval.tv_sec = 0;	
	meas_timer.it_interval.tv_nsec = 0; 	
	if (timer_settime(timerid, 0, &meas_timer, &meas_oitval) == -1) 		
		DBG_L1("error timer_settime failed");		
	
		DBG_L1("_setup_meas_timer_Reload Exiting --");
		return ret;
}
int  _setup_meas_recovery_timer(unsigned int secs, void *now_timer)
{
 	DBG_L1("_setup_meas_recovery_timer Entering ++");	
	sa.sa_sigaction = (void (*) (int, siginfo_t*, void *))proc_wt4meas_rep;		
	sa.sa_handler = proc_wt4meas_rep;		

	sigemptyset(&sa.sa_mask);	
	if (sigaction(SIGRTMAX-1, &sa, NULL) == -1)		
		DBG_L1("error sigaction failed");	

	sev.sigev_notify = SIGEV_SIGNAL;	
	sev.sigev_signo = SIGRTMAX-1;	
	sev.sigev_value.sival_ptr = &timerid;		
	/* Create the timer */	
	if (timer_create(CLOCK_MONOTONIC, &sev, &timerid) == -1)		
		DBG_L1("error timer_create failed");	

	meas_timer.it_value.tv_sec = secs ;	
	meas_timer.it_value.tv_nsec = 0;	 	
	meas_timer.it_interval.tv_sec = 0; 	
	meas_timer.it_interval.tv_nsec = 0;  	

	/* Start the timer */		
	if (timer_settime(timerid, 0, &meas_timer, &meas_oitval) == -1) 		
		DBG_L1("error timer_settime failed");	

	return 0;
}

int _erase_meas_recovery_timer(void )
{
	DBG_L1("_erase_meas_recovery_timer Entering ++");	
	int ret;		
	ret = timer_delete(timerid);			
	DBG_L1("_erase_meas_recovery_timer Exiting --");	
	return ret;
}
#endif
static void reset_ue_result_data(void)
{
        ue_clk_msr_pos_per_tck = 0;
        
        memset(ue_pos_result, 0, sizeof(gnss_pos_result));
        memset(ue_gps_result, 0, sizeof(gps_msr_result));
        memset(ue_gps_msr, 0, sizeof(gps_msr_info) * 32);
        memset(ue_ganss_result, 0, sizeof(ganss_msr_result));
        memset(ue_ganss_msr_desc, 0, sizeof(ganss_msr_desc));

		memset(ue_ganss_msr, 0, 32 *sizeof(ganss_msr_info));
}

static void reset_ue_aux_data(void)
{
        memset(ue_gps_aux_info, 0, 32 * sizeof(struct dev_gnss_sat_aux_info));
        memset(ue_glo_aux_info, 0, 24 * sizeof(struct dev_gnss_sat_aux_info));
        memset(ue_gps_aux_desc, 0, sizeof(struct dev_gnss_sat_aux_desc));
        memset(ue_glo_aux_desc, 0, sizeof(struct dev_gnss_sat_aux_desc));
}

static void reset_ue_adapter_data(void)
{
	reset_ue_result_data();
	reset_ue_aux_data();
}

/*------------------------------------------------------------------------------
 * Core callbacks from device for GPS feature
 *----------------------------------------------------------------------------*/
static int rpt_clk_msr_pos(const struct gnss_pos_result  &pos_result,
                           const struct gps_msr_result   &gps_result, 
                           const struct gps_msr_info     *gps_msr,
                           const struct ganss_msr_result &ganss_result,
                           const struct ganss_msr_desc   *ganss_msr)
{	
	CPP_FTRACE1();
	
	int ret_val = 0; 
	struct ti_gnss_ue_callbacks *cbs = &ue_adap->gnss_ue_cbs;

	if(cbs->ue_aux_info4sv && (ue_clk_msr_pos_per_tck & UE_MSR_RPT_SET)) {
                DBG_L2("Dispatching AUX SAT Info to AGNSS Framework");
                ret_val = cbs->ue_aux_info4sv(cbs->hnd, ue_gps_aux_desc[0], 
                                              ue_gps_aux_info, 
                                              ue_glo_aux_desc[0],
                                              ue_glo_aux_info);
        }

	if(ue_clk_msr_pos_per_tck != UE_ALL_RPT_SET) {
                DBG_L2("MSR | POS status 0x%x", ue_clk_msr_pos_per_tck);
                return 0;
        }


	if(!ret_val && cbs->ue_loc_results) {
		if(gps_msr->TCount != 0){
                DBG_L2("Dispatching MSR and POS Results to AGNSS Framework");
                ret_val = cbs->ue_loc_results(cbs->hnd, pos_result, gps_result, 
                                              gps_msr, ganss_result, ue_ganss_msr);
			}
        }
               
	/* CLK-MSR-POS reported so, clear up all place-holders */
	reset_ue_result_data();

        return ret_val;
}

/* Device to Connect */
static int a2c_ue_pos_result(struct loc_gnss_info *gnss_loc)
{
	CPP_FTRACE1();
	struct timeval timestp;
	//MSG_L2(print_loc_gnss_info_data, gnss_loc, 1);
	DBG_L1("Prep POS result originated at GNSS secs %d", gnss_loc->ref_secs);
	DBG_L1("a2c_ue_pos_result: ref_msec %u, fix_type = %d\n", gnss_loc->ref_msec, gnss_loc->fix_type);
	gettimeofday(&timestp, NULL);
	DBG_L1("Adapter Loc_results timestamp Sec = %d timestamp USec = %d", timestp.tv_sec, timestp.tv_usec );

	/* Using global data place holder to fill information */
	ue_pos_result->contents = 0; /* Has a valid POS Report   */
	ue_pos_result->gnss_tid = gnss_loc->gnss_tid;
	ue_pos_result->fix_type = gnss_loc->fix_type;
	ue_pos_result->pos_flags = gnss_loc->pos_flag;
	ue_pos_result->ref_secs = gnss_loc->ref_secs;
	ue_pos_result->ref_msec = gnss_loc->ref_msec;
	ue_pos_result->utc_secs = gnss_loc->utc_secs;
	ue_pos_result->utc_msec = gnss_loc->utc_msec;		
	ue_pos_result->pos_bits = gnss_loc->pos_bits;
	ue_pos_result->location = gnss_loc->location;
	/*SRK: Added to DR intr*/
	ue_pos_result->Tcount = gnss_loc->TCount;
	ue_pos_result->osc_bias = gnss_loc->osc_bias;
	ue_pos_result->osc_freq = gnss_loc->osc_freq;
	ue_pos_result->utcOffset = gnss_loc->utcOffset;
	ue_pos_result->week_num = gnss_loc->week_num;
	ue_pos_result->pps_state = gnss_loc->pps_state;

	ue_pos_result->time_sec = gnss_loc->timeTag_sec;
	ue_pos_result->time_usec = gnss_loc->timetag_usec;
	DBG_L1("UTC Secs = %d, Msecs = %d",gnss_loc->utc_secs,gnss_loc->utc_msec);

#define POS_RSLT_LOC(x)  ue_pos_result->location.x

	POS_RSLT_LOC(shape)          = gnss_loc->location.shape;
	POS_RSLT_LOC(lat_sign)       = gnss_loc->location.lat_sign;
	POS_RSLT_LOC(latitude_N)     = gnss_loc->location.latitude_N;
	POS_RSLT_LOC(longitude_N)    = gnss_loc->location.longitude_N;
	POS_RSLT_LOC(alt_dir)        = gnss_loc->location.alt_dir;
	POS_RSLT_LOC(altitude_N)     = gnss_loc->location.altitude_N;
	POS_RSLT_LOC(unc_semi_maj_K) = gnss_loc->location.unc_semi_maj_K;
	POS_RSLT_LOC(unc_semi_min_K) = gnss_loc->location.unc_semi_min_K;
	POS_RSLT_LOC(orientation)    = gnss_loc->location.orientation;
	POS_RSLT_LOC(unc_altitude_K) = gnss_loc->location.unc_altitude_K;
	POS_RSLT_LOC(confidence)     = gnss_loc->location.confidence;

	/*SRK: Added to DR intr*/
	POS_RSLT_LOC(altitude_MSL)	 = gnss_loc->location.altitude_MSL;
	POS_RSLT_LOC(east_unc)		 = gnss_loc->location.east_unc;
	POS_RSLT_LOC(north_unc)		 = gnss_loc->location.north_unc;
	POS_RSLT_LOC(vert_unc)		 = gnss_loc->location.vert_unc;
	ue_clk_msr_pos_per_tck |= UE_POS_RPT_SET;
	//ue_clk_msr_pos_per_tck |= UE_CLK_RPT_SET;

        ext_assist_put_gnss_loc(&gnss_loc->location);
        DBG_L1("a2c_location_rpt2: ref_msec %u\n", gnss_loc->ref_msec);
	return 0;
}

/* Adapter to Connect */
static int a2c_ue_gps_sat_used(struct dev_sat_info *sat_info, int num_obj)
{
        CPP_FTRACE1();

#define GNSS_ID_GPS 0
#define GNSS_ID_GLO 1
#define GNSS_ID_SBS 2  
#define GNSS_ID_QZS 3  

        ue_gps_aux_desc->pos_bits = 0;
        ue_glo_aux_desc->pos_bits = 0;

        for(int i = 0; i < num_obj; i++) {
                if(sat_info->gnss_id == GNSS_ID_GPS) {
                        ue_gps_aux_desc->pos_bits |= (1 << (sat_info->svid - 1));
                        ue_gps_aux_desc->eph_bits |= (1 << (sat_info->svid - 1));
				} else if (sat_info->gnss_id == GNSS_ID_GLO) {
		                ue_glo_aux_desc->pos_bits |= (1 << (sat_info->svid - 65));
		                ue_glo_aux_desc->eph_bits |= (1 << (sat_info->svid - 65));
				}
				else if (sat_info->gnss_id == GNSS_ID_SBS) {
		                ue_glo_aux_desc->pos_bits |= (1 << (sat_info->svid - 120));
		                ue_glo_aux_desc->eph_bits |= (1 << (sat_info->svid - 120));
				}
				else if (sat_info->gnss_id == GNSS_ID_QZS) {
		                ue_glo_aux_desc->pos_bits |= (1 << (sat_info->svid - 193));
		                ue_glo_aux_desc->eph_bits |= (1 << (sat_info->svid - 193));
				}
                sat_info++;	
        }

        return 0;
}

/* Adapter to Connect */
static int a2c_ue_vel_result(struct vel_gnss_info *gnss_vel)
{
        CPP_FTRACE1();

        //MSG_L2(print_vel_gnss_info_data, gnss_vel, 1);

        DBG_L3("POS has velocity component also built-in");

        struct vel_gnss_info *vel_info = &ue_pos_result->velocity;

        vel_info->type    = gnss_vel->type;
        vel_info->bearing = gnss_vel->bearing;
        vel_info->h_speed = gnss_vel->h_speed;
        vel_info->ver_dir = gnss_vel->ver_dir;
        vel_info->v_speed = gnss_vel->v_speed;
        vel_info->h_unc   = gnss_vel->h_unc;
        vel_info->v_unc   = gnss_vel->v_unc;
        vel_info->est_unc= gnss_vel->est_unc;
        vel_info->nth_unc= gnss_vel->nth_unc;
        vel_info->vup_unc= gnss_vel->vup_unc;
        
        ue_pos_result->contents |= POS_HAS_VELOCITY;
        
        return 0;
}
/* Adapter to Connect */
/*SRK: Added to report to clients*/
static int a2c_ue_dop_result(struct dev_dop_info *gnss_dop)
{
        CPP_FTRACE1();

#define DOP_RSLT_LOC(x)  ue_pos_result->dopinfo.x

        DBG_L3("POS has DOP component also built-in");

		DOP_RSLT_LOC(horizontal) = gnss_dop->horizontal;
		DOP_RSLT_LOC(position) = gnss_dop->position;
		DOP_RSLT_LOC(time) = gnss_dop->time;
		DOP_RSLT_LOC(vertical) = gnss_dop->vertical;


        ue_pos_result->contents |= POS_HAS_DOP;
        
        return 0;
}
static void a2c_ue_gps_time(struct gps_time *time)
{
        CPP_FTRACE1();

        struct ti_gnss_ue_callbacks *cbs = &ue_adap->gnss_ue_cbs;

        ext_assist_put_gps_time(time); 	
        cbs->update_ue_time(cbs->hnd);

        DBG_L3("GPS Time has been reported by device");
}

static void a2c_ue_glo_time(struct ganss_ref_time *time)
{
        CPP_FTRACE1();

         chk_glo_secs = time->day * MAX_SECS_IN_A_DAY + time->tod;



        DBG_L1("4YSecs: %u, GLO_DAY: %u, GLO_TOD: %u", chk_glo_secs, time->day, time->tod);
}

/* HACK: Remove once, proper implementation is available in EXT ASSIST */
unsigned int chk_glo_secs_get()
{
        if(!chk_glo_secs || !chk_glo_refs) {
                DBG_L1("No GLO TIME decode detected as yet");
                return 0;
        }

        struct timeval tv;
        gettimeofday(&tv, NULL);
        unsigned int secs = chk_glo_secs + (tv.tv_sec - chk_glo_refs);
        DBG_L1("GLO EPH GLO 4Y seconds are %u", secs);

	return secs;
}

/* Adapter to Connect */
static int a2c_location_rpt(enum ue_pos_id pos_id, void *cb_data, int num_obj)
{
	CPP_FTRACE1();

	switch(pos_id) {

		case loc_exl_native: 
			DBG_L3("POS ID --> loc_exl_native NOT USED!!!!");
			break;

		case loc_gnss_ie:
                a2c_ue_pos_result((struct loc_gnss_info*) cb_data);
			break;

		case loc_sat_native: 
			a2c_ue_gps_sat_used((struct dev_sat_info*) cb_data, num_obj);
			ue_clk_msr_pos_per_tck |= UE_POS_RPT_SET;
			break;

		case vel_native:
		case hdg_native:
			break;

		case vel_gnss_ie: 
			a2c_ue_vel_result((struct vel_gnss_info*) cb_data);
						ue_clk_msr_pos_per_tck |= UE_POS_RPT_SET;
			break;

		case tim_gps_ie: 
                a2c_ue_gps_time((struct gps_time*) cb_data);
							ue_clk_msr_pos_per_tck |= UE_POS_RPT_SET;
			break;

		case tim_gps_native:
			DBG_L1("POS ID --> tim_gps_native NOT USED!!!!");
			break;

		case tim_glo_ie:
			//DBG_L1("POS ID --> tim_glo_ie NOT USED!!!!");
			a2c_ue_glo_time((struct ganss_ref_time*) cb_data);
			break;

		/*SRK : Added to report the DOP and Heading info to the external clients */
		case loc_dop_native:
			a2c_ue_dop_result((struct dev_dop_info *)cb_data);
			break;
		default:
			DBG_L1(" [UE ADP] Unknown ue_pos_id");
			break;
	}
	if(ue_pos_result->Tcount == ue_gps_msr->TCount)
	{
		return rpt_clk_msr_pos(ue_pos_result[0], ue_gps_result[0], ue_gps_msr, 
					       ue_ganss_result[0], ue_ganss_msr_desc);
	}
	else
	{
		return 0; 
	}
}

static void setup_gps_aux_info(struct dev_gnss_sat_aux_desc *gps_desc,
                               struct dev_gnss_sat_aux_info *gps_info,
                               struct dev_msr_info          *msr_info)
{
	CPP_FTRACE1();

	gps_info->prn         = msr_info->svid;
        
	gps_info->snr_db_by10 = msr_info->cno;
	
	gps_info->azimuth_deg = 
                ceilf(((float)msr_info->azimuth) * 45.0f / 32.0f * 100.0f) / 100;
	gps_info->elevate_deg = 
                ceilf(((float)msr_info->elevation) * 45.0f / 64.0f * 100.0f) / 100;
	gps_info->rssi_dbm = msr_info->rssi_dbm;
	gps_info->meas_flags = msr_info->meas_flags;
	gps_info->meas_qual = msr_info->meas_qual;
        

	gps_desc->num_sats++;

	return;
}

static void setup_glo_aux_info(struct dev_gnss_sat_aux_desc *glo_desc,
                               struct dev_gnss_sat_aux_info *glo_info,
                               struct dev_msr_info          *msr_info)
{
	CPP_FTRACE1();
	glo_info->prn         = msr_info->svid; // CHECK it is confusing

	glo_info->snr_db_by10 = msr_info->cno;
	glo_info->azimuth_deg = ceilf(((float)msr_info->azimuth) * 45.0f 
                                      / 32.0f * 100.0f) / 100;
	glo_info->elevate_deg = ceilf(((float)msr_info->elevation) * 45.0f 
                                      / 64.0f * 100.0f) / 100;
	glo_info->meas_flags = msr_info->meas_flags;
    glo_info->meas_qual = msr_info->meas_qual;
	DBG_L1(" [UE ADP] gnss_id          [%10d]\n", msr_info->gnss_id);
	DBG_L1(" [UE ADP] svid             [%10d]\n", msr_info->svid);
	DBG_L1(" [UE ADP]  ->elevation     [%10d]\n", msr_info->elevation);
	DBG_L1(" [UE ADP]  ->azimuth       [%10d]\n", msr_info->azimuth);
        
	if(msr_info->svid >= 65 && msr_info->svid <= 88){
			glo_desc->num_sats++;
	}
	if((msr_info->svid >= 120 && msr_info->svid <= 138) || (msr_info->svid >= 193 && msr_info->svid <= 202) ){
			glo_desc->num_sbas_sats++;
	}

	return;
}

/* Adapter to Connect */
static int a2c_gps_sat_aux(struct dev_msr_info *msr_info, int num_obj)
{
        CPP_FTRACE1();

        struct dev_gnss_sat_aux_info *gps_info = ue_gps_aux_info;
        struct dev_gnss_sat_aux_desc *gps_desc = ue_gps_aux_desc;
        
        gps_desc->num_sats = 0;
        
        for(int cnt = 0; cnt < num_obj; cnt++, msr_info++) {
                
                if((((float)msr_info->snr)/10.0f) > 0 && msr_info->svid >= 1 
                   && msr_info->svid <= 32) {
                        setup_gps_aux_info(gps_desc, gps_info, msr_info);
                        gps_info++;
                }
        }
                
        ue_clk_msr_pos_per_tck |= UE_MSR_RPT_SET;
        
        return 0;
}

/* Adapter to Connect */
static int a2c_glo_sat_aux(struct dev_msr_info *msr_info, int num_obj)
{
        CPP_FTRACE1();

        struct dev_gnss_sat_aux_info *glo_info = ue_glo_aux_info;
        struct dev_gnss_sat_aux_desc *glo_desc = ue_glo_aux_desc;
        
        glo_desc->num_sats = 0;
        glo_desc->num_sbas_sats = 0;
        for(int cnt = 0; cnt < num_obj; cnt++, msr_info++) {
                
                if ((((float)msr_info->snr)/10.0f) > 0 /*&& msr_info->svid >= 65 
                    && msr_info->svid <= 88*/) {
                        setup_glo_aux_info(glo_desc, glo_info, msr_info);
                        glo_info++;
                }
        }
                
        ue_clk_msr_pos_per_tck |= UE_MSR_RPT_SET;
        
        return 0;
}

/* Adapter to Connect */
static int a2c_gps_msr_info(struct gps_msr_info *msr_info, int num_obj)
{
        CPP_FTRACE1();
        memcpy(ue_gps_msr, msr_info, num_obj*sizeof(struct gps_msr_info)); /* Copy into UE construct */
        ue_gps_result->n_sat = num_obj;
        ue_clk_msr_pos_per_tck |= UE_MSR_RPT_SET;
        return 0;
}

static int a2c_ganss_msr_info(struct ganss_msr_info *msr_info, int num_obj)
{
        CPP_FTRACE1();
        memcpy(ue_ganss_msr, msr_info, num_obj*sizeof(struct ganss_msr_info)); /* Copy into UE construct */
        ue_ganss_result->n_ganss = num_obj;
        ue_clk_msr_pos_per_tck |= UE_MSR_RPT_SET;
			DBG_L1(" [UE ADP] a2c_ganss_msr_info: n_ganss =%d\n", ue_ganss_result->n_ganss);
		
		for(int index= 0; index < num_obj; index++){
			DBG_L1(" [UE ADP] a2c_ganss_msr_info:");
			
			DBG_L1(" [UE ADP] contents             [%10d]", ue_ganss_msr[index].contents);
			DBG_L1(" [UE ADP] svid                 [%10d]", ue_ganss_msr[index].svid);
			DBG_L1(" [UE ADP] c_no                 [%10d]", ue_ganss_msr[index].c_no);
		}

        return 0;
}
static int a2c_gps_msr_rslt_info(struct dev_msr_result* dev_msr_rslts, int num_obj)
{
	CPP_FTRACE1();
	UNUSED(num_obj);

	DBG_L1(" [UE ADP] a2c_gps_msr_rslt_info: msr_qual_status = %d , gps_ref_msec = %u , gps_unc_K = %u",\
	       dev_msr_rslts->msr_qual_status, dev_msr_rslts->gps_ref_msec, dev_msr_rslts->gps_unc_K);
	/* need to set err content flags correctly based on the msr reports */
	if	(1 != dev_msr_rslts->msr_qual_status)
		ue_gps_result->contents |= GPS_MSR_NOT_REPORTED;

	ue_gps_result->h_acc_K = dev_msr_rslts->gps_unc_K;
	ue_gps_result->ref_msec = dev_msr_rslts->gps_ref_msec;
	ue_gps_result->ref_secs = dev_msr_rslts->gps_ref_secs;
	
	return 0;
}
int a2c_msr_rpt(enum ue_msr_id msr_id, void *cb_data, int num_obj)
{
	CPP_FTRACE1();

	int ret_val = 0;
	DBG_L1(" [UE ADP] a2c_msr_rpt enum %d", msr_id);

	switch(msr_id) {

		case msr_gps_native: 
			a2c_gps_sat_aux((struct dev_msr_info*) cb_data, num_obj);
			break;

		case msr_glo_native:
			a2c_glo_sat_aux((struct dev_msr_info*) cb_data, num_obj);
			break;

		case msr_gps_rpt_ie: {
					     a2c_gps_msr_info((struct gps_msr_info*) cb_data, num_obj);
				     }
				     break;

		case msr_glo_sig_G1:
		case msr_glo_sig_G2: {
#define GLO_SIG_G1 0
#define GLO_SIG_G2 1
#define GLO_SIG_G3 2

					     /* CHECK: is this required just for debug or some other purpose? */
					     struct glo_msr_info *glo_msr = (struct glo_msr_info*) cb_data;
					     struct ganss_msr_info *msr;

					     struct ganss_msr_desc *ganss_msr_inf = ue_ganss_msr_desc;
					     struct ganss_sig_info *signals       = ue_ganss_msr_desc->signals;
					     struct ganss_msr_info *msr_inf       = signals->sats;

					     ganss_msr_inf->ganss_id = GANSS_ID_GLO;
					     ganss_msr_inf->n_signal = 1;

					     signals->ganss_id  = GANSS_ID_GLO;
					     signals->signal_id = (msr_id == msr_glo_sig_G1) ? 
						     GLO_SIG_G1 : GLO_SIG_G2;
					     signals->contents = 0;//TO_DO
					     signals->ambiguity = 0;//TO_DO
					     signals->n_sat = num_obj;

					     DBG_L1(" [UE ADP] dev_msr_rpt: msr_glo_sig_G1/msr_glo_sig_G2");

					     while (num_obj--) {

						     msr = &glo_msr->msr;
#if  0
						     DBG_L1(" [UE ADP] glo_3gpp_msr_info:");
						     DBG_L1(" [UE ADP] contents             [%10d]", msr->contents);
						     DBG_L1(" [UE ADP] svid                 [%10d]", msr->svid);
						     DBG_L1(" [UE ADP] c_no                 [%10d]", msr->c_no);
						     DBG_L1(" [UE ADP] mpath_det            [%10d]", msr->mpath_det);
						     DBG_L1(" [UE ADP] carrier_qual         [%10d]", msr->carrier_qual);
						     DBG_L1(" [UE ADP] code_phase           [%10d]", msr->code_phase);
						     DBG_L1(" [UE ADP] integer_cp           [%10d]", msr->integer_cp);
						     DBG_L1(" [UE ADP] cp_rms_err           [%10d]", msr->cp_rms_err);
						     DBG_L1(" [UE ADP] doppler              [%10d]", msr->doppler);
						     DBG_L1(" [UE ADP] adr                  [%10d]", msr->adr);
#endif
						     *msr_inf = *msr;
						     msr_inf++;
						     glo_msr++;
					     }

					     ue_clk_msr_pos_per_tck |= UE_MSR_RPT_SET;
				     }
				     break;

		case msr_glo_signal:
                        break;

		case msr_device_result : {

						 struct dev_msr_result *dev_msr_result = (struct dev_msr_result*) cb_data;

						 DBG_L1("[GNSS]dev_msr_result: msr_device_result");
						 DBG_L1("[GNSS]contents		[0x%x]",dev_msr_result->contents);
						 DBG_L1("[GNSS]msr_qual_status	[%10d]",dev_msr_result->msr_qual_status);
						 DBG_L1("[GNSS]glo_unc_K		[%10d]",dev_msr_result->glo_unc_K);	
						 DBG_L1("[GNSS]gps_unc_K		[%10d]",dev_msr_result->gps_unc_K);
						 a2c_gps_msr_rslt_info(dev_msr_result , num_obj);
					 }
					 break;
		 case msr_ganss_rpt_ie: {
						  a2c_ganss_msr_info((struct ganss_msr_info*) cb_data, num_obj);
		 	}
		 break;
		default: {
				 DBG_L1(" [UE ADP] dev_msr_rpt: Unknown ue_msr_id");
			 }
			 break;
	}

	return ret_val;
}

/* ti_nmea_sentence  nmea_message */
int a2c_nmea_rpt(enum nmea_sn_id nmea, char *cb_data, int length)
{
	CPP_FTRACE1();

	int rpt2connect = 1;
	if(!cb_data || (0 == length)) {
		ERR_NM("Received NULL or NMEA sentence with 0 length bytes");
		rpt2connect = 0;
	}

	switch(nmea) {
		/** GPS specific sentences */
        case gpgga:   
        case gpgll:
        case gpgsa:
        case gpgsv:
        case gprmc:
        case gpvtg:
		case gpgrs:
		case gpgst:
                break;
                
		/** GLO specific sentences */
        case glgga:   
        case glgll:
        case glgsa:
        case glgsv:
        case glrmc:
        case glvtg:
                break;
        case qzgsv:
        		break;
        case wsgsv:
        		break;
		case gngrs:
		case gngll:
		case gngns:
		case gngsa:
		case gnvtg:
                break;
                
        default:
			rpt2connect = 0;
                break;
	}
        
        struct ti_gnss_ue_callbacks *cbs = &ue_adap->gnss_ue_cbs;
        if(cbs->ue_nmea_report && rpt2connect) {
                DBG_L2("Dispatching NMEA to AGNSS Framework"); /* : Add print of NMEA messages */
	        return cbs->ue_nmea_report(cbs->hnd, nmea, cb_data, length);
        }

	DBG_L2("Did not dispatch NMEA report to AGNSS Framework");
	return 0;
}

/*  ti_ue_loc_failed  location_err */
int a2c_loc_failed(enum lc_evt_id evt_id)
{
	CPP_FTRACE1();

        enum lc_err_id err_id;

        switch(evt_id) {

        case evt_nf_not_enough_svs: err_id = not_enough_gps_sv;  break;
        case evt_nf_not_enough_eph: err_id = no_gps_assist_data; break;
        default: err_id = undefined; break;

        }
        /* Need to add an conditional statement to ascertain the err ID */

        /* Using global data place holder to fill information */
	ue_pos_result->contents  = POS_NOT_REPORTED;		
	
	ue_pos_result->lc_error.error_id = err_id;
	ue_pos_result->lc_error.contents = 0;

	ue_clk_msr_pos_per_tck  |= UE_POS_RPT_SET;

        DBG_L1("LOC EVT ID %u, xlated to LOC ERR ID %u", evt_id, err_id);

	return rpt_clk_msr_pos(ue_pos_result[0], ue_gps_result[0], ue_gps_msr, 
                               ue_ganss_result[0], ue_ganss_msr_desc);
}

/* add for GNSS recovery ++*/
/*	ue device err */
int a2c_dev_err(enum dev_err_id err_id)
{
	CPP_FTRACE1();

	int    ret_val = -1;

	struct ti_gnss_ue_callbacks *cbs = &ue_adap->gnss_ue_cbs;

	DBG_L1("err_seq: error has been reported by device %d",err_id);

	if(cbs->handle_ue_err) {
		ret_val = cbs->handle_ue_err(cbs->hnd, err_id);
		DBG_L1("err_seq: dispatching to the connect to schedule error recovery");
	}

	return ret_val;
}
/* add for GNSS recovery --*/
/* ti_dev_agnss_dec  device_agnss */
int a2c_agnss_dec(enum loc_assist assist, void *assist_array, int num)
{
	CPP_FTRACE1();

	int    ret_val = -1;
	struct dev_gnss_sat_aux_desc *gps_desc = ue_gps_aux_desc;
	struct dev_gnss_sat_aux_desc *glo_desc = ue_glo_aux_desc;

	struct ti_gnss_ue_callbacks *gnss_ue_cbs = &ue_adap->gnss_ue_cbs;
	if(gnss_ue_cbs->ue_decoded_aid) {
		DBG_L1("Dispatching decoded info %u to AGNSS Framework", assist);
		ret_val = gnss_ue_cbs->ue_decoded_aid(gnss_ue_cbs->hnd, assist,
						      assist_array,       num);
	}

	switch (assist) {
                
        case a_GPS_EPH:  {
                struct gps_eph *eph = (struct gps_eph *) assist_array;
                while (num--) {
                        gps_desc->eph_bits |= (1 << eph->svid);
                        eph++;
                }
        }
                break;
                
        case a_GPS_ALM: {
                struct gps_alm *alm = (struct gps_alm *) assist_array;
                while (num--) {
                        gps_desc->alm_bits |= (1 << alm->svid);
                        alm++;
                }
        }
                break;
                
        case a_GLO_EPH: {
                struct glo_eph *eph = (struct glo_eph *) assist_array;
                while (num--) {
                        glo_desc->eph_bits |= (1 << eph->the_svid);
                        eph++;
                }
        }
                break;
        
        case a_GLO_ALM: {
                struct glo_alm *alm = (struct glo_alm *) assist_array;
                while (num--) {
                        glo_desc->alm_bits |= (1 << (alm->nA - 1));
                        alm++;
                }
        }
                break;
        
        default:  {
                
        }
                break;
	}
        
	return ret_val;
}

/*ti_dev_event_ind  device_event*/
int dev_event_ind(enum dev_evt_id id, void *data_array, int size)
{
	CPP_FTRACE1();
	DBG_L1(" [UE ADP] dev_event_ind: Entering");
	UNUSED(id);
	UNUSED(data_array);
	UNUSED(size);

	return 0;
}

int a2c_cw_test_rep(enum cw_test_rep cwt, const struct cw_test_report* cw_rep, int num)
{
	CPP_FTRACE1();

	int    ret_val = -1;
	struct ti_gnss_ue_callbacks *gnss_ue_cbs = &ue_adap->gnss_ue_cbs;
	if(gnss_ue_cbs->ue_cw_test_results) {
		DBG_L1("Dispatching CW Test Report to AGNSS FW" );
		ret_val = gnss_ue_cbs->ue_cw_test_results(gnss_ue_cbs->hnd, cwt,
						      cw_rep, num);
	}
		return 0;
}
/*ti_app_ucase_ind  ucase_notify;*/
int dev_app_ucase_ind(enum app_rpt_id id, void *data)
{
	CPP_FTRACE1();
	DBG_L1(" [UE ADP] dev_app_ucase_ind: Entering");
	UNUSED(id);
	UNUSED(data);

	return 0;
}

struct ue_dev2cfw_info {

        unsigned int  fnc_id;
        void         *param1;
        void         *param2;
        void         *param3;
        void         *param4;
};

static queue<struct ue_dev2cfw_info> ue_adap_queue;
static pthread_mutex_t ue_adap_queue_mtx;          /* Mutex for atomicity     */ 
static sem_t           ue_adap_queue_sem;          /* Signalling semaphore    */
static bool            ue_adap_queue_proc = false; /* status of queue process */

static struct ue_dev2cfw_info recv_ue_adap_queue(void)
{
        CPP_FTRACE1();

        pthread_mutex_lock(&ue_adap_queue_mtx);

        if(0 == ue_adap_queue.size()) {
                ue_adap_queue_proc = false; /* Queue is now empty, stop proc */

                pthread_mutex_unlock(&ue_adap_queue_mtx);
                sem_wait(&ue_adap_queue_sem);
                DBG_L3("First elem in Queue, start   processing ...");
                pthread_mutex_lock(&ue_adap_queue_mtx);
        }

        struct ue_dev2cfw_info dev2cfw_info = ue_adap_queue.front();
        ue_adap_queue.pop();

        pthread_mutex_unlock(&ue_adap_queue_mtx);

        return dev2cfw_info;
}

static int push2ue_adap_queue(struct ue_dev2cfw_info& dev2cfw_info)
{
        CPP_FTRACE1();

        pthread_mutex_lock(&ue_adap_queue_mtx);
        ue_adap_queue.push(dev2cfw_info);

       // if((1 == ue_adap_queue.size()) && (false == ue_adap_queue_proc)) { 
        if((ue_adap_queue.size() >=1) && (false == ue_adap_queue_proc)) { 
                /* Give if, worker thread has processed all queued elements */
                sem_post(&ue_adap_queue_sem);
                ue_adap_queue_proc = true; /* Worker thread will reset flag */
        }

        pthread_mutex_unlock(&ue_adap_queue_mtx);

        return 0;
}

#define VOID_PTR2INT(p) (*(int *)&p)
static int adap2cfw_location_rpt(const struct ue_dev2cfw_info& dev2cfw_info)
{
        CPP_FTRACE1();

        enum ue_pos_id pos_id;
        memcpy(&pos_id, &dev2cfw_info.param1, sizeof(pos_id));
        void         *cb_data =       dev2cfw_info.param2;
        int           num_obj = VOID_PTR2INT(dev2cfw_info.param3);

        int ret_val = a2c_location_rpt(pos_id, cb_data, num_obj);

        delete (char *)cb_data;

        return ret_val;
}

static int adap2cfw_msr_rpt(const struct ue_dev2cfw_info& dev2cfw_info)
{
        CPP_FTRACE1();

        enum ue_msr_id msr_id;
        memcpy(&msr_id, &dev2cfw_info.param1, sizeof(msr_id));
        void         *cb_data =       dev2cfw_info.param2;
        int           num_obj = VOID_PTR2INT(dev2cfw_info.param3);

        int ret_val = a2c_msr_rpt(msr_id, cb_data, num_obj);

        delete[] (char*) cb_data;

        return ret_val;
}

static int adap2cfw_nmea_rpt(const struct ue_dev2cfw_info& dev2cfw_info)
{
	CPP_FTRACE1();

	int ret_val;        

        enum nmea_sn_id nmea;
	memcpy(&nmea, &dev2cfw_info.param1, sizeof(nmea));

	char *cb_data = (char *) dev2cfw_info.param2;
	int    length = VOID_PTR2INT(dev2cfw_info.param3);

	ret_val = a2c_nmea_rpt(nmea, cb_data, length);
	delete[] cb_data;

	return ret_val;
}

/* add for GNSS recovery ++*/
static int adap2cfw_error_ind(const struct ue_dev2cfw_info& dev2cfw_info)
{
	CPP_FTRACE1();
	
	enum dev_err_id id;
	memcpy(&id, &dev2cfw_info.param1, sizeof(id));
	
	int ret_val = a2c_dev_err(id);
	
	return ret_val;

}	
/* add for GNSS recovery --*/
static int adap2cfw_loc_failed(const struct ue_dev2cfw_info& dev2cfw_info)
{
        CPP_FTRACE1();

        enum lc_evt_id id;
        memcpy(&id, &dev2cfw_info.param1, sizeof(id));
        
        int ret_val = a2c_loc_failed(id);

        return ret_val;
}

static int adap2cfw_agnss_dec(const struct ue_dev2cfw_info& dev2cfw_info)
{
        CPP_FTRACE1();

        enum loc_assist assist;
        memcpy(&assist, &dev2cfw_info.param1, sizeof(assist));

        void          *cb_data =                   dev2cfw_info.param2;
        int           num_obj = VOID_PTR2INT(dev2cfw_info.param3);
        
        int ret_val = a2c_agnss_dec(assist, cb_data, num_obj);
        
        delete[] (char*)cb_data;
        
        return ret_val;
}

static int adap2cfw_cw_test_rep(const struct ue_dev2cfw_info& dev2cfw_info)
{
	CPP_FTRACE1();
	int ret_val = -1;
	enum cw_test_rep	cwt;
	
	struct ti_gnss_ue_callbacks *gnss_ue_cbs = &ue_adap->gnss_ue_cbs;
	memcpy(&cwt, &dev2cfw_info.param1, sizeof(cwt));

	void		  *cb_data = dev2cfw_info.param2;
	int 		  num_obj = VOID_PTR2INT(dev2cfw_info.param3);


	if(gnss_ue_cbs->ue_cw_test_results) {
		DBG_L1("Dispatching CW test results");
		ret_val = gnss_ue_cbs->ue_cw_test_results(gnss_ue_cbs->hnd, cwt, (const struct cw_test_report*)cb_data, num_obj);
	}
	
	delete[] (char*)cb_data;
	
	return ret_val;
}

static const unsigned int DEV_CB_DEV_NO_WORK_FN_ID          = 0x000;
static const unsigned int DEV_CB_DEV_LOC_RPT_FN_ID          = 0x100;
static const unsigned int DEV_CB_DEV_MSR_RPT_FN_ID          = 0x101;
static const unsigned int DEV_CB_DEV_LOC_FAIL_FN_ID         = 0x102;
static const unsigned int DEV_CB_DEV_NMEA_MSG_FN_ID         = 0x103;
static const unsigned int DEV_CB_DEV_AGNSS_DEC_FN_ID        = 0x104;
static const unsigned int DEV_CB_DEV_ERR_IND_FN_ID          = 0x105;
static const unsigned int DEV_CB_DEV_EVT_IND_FN_ID          = 0x106;
static const unsigned int DEV_CB_DEV_USE_CASE_FN_ID         = 0x107;
static const unsigned int DEV_CB_DEV_CW_TEST_REP_FN_ID  	= 0x108;

struct adap_cfw_proc_desc {
	unsigned int  fnc_id;
	int           (*proc)(const struct ue_dev2cfw_info& dev2cfw_info);
};

static struct adap_cfw_proc_desc adap_cfw_proc_tbl [] = {

        {DEV_CB_DEV_LOC_RPT_FN_ID,           adap2cfw_location_rpt},
        {DEV_CB_DEV_MSR_RPT_FN_ID,           adap2cfw_msr_rpt},
        {DEV_CB_DEV_LOC_FAIL_FN_ID,          adap2cfw_loc_failed},
        {DEV_CB_DEV_AGNSS_DEC_FN_ID,         adap2cfw_agnss_dec},
        {DEV_CB_DEV_NMEA_MSG_FN_ID,          adap2cfw_nmea_rpt},
       	{DEV_CB_DEV_CW_TEST_REP_FN_ID, 		 adap2cfw_cw_test_rep},
		/*add for GNSS recovery ++*/
		{DEV_CB_DEV_ERR_IND_FN_ID,			adap2cfw_error_ind},
		/*add for GNSS recovery --*/		
        {DEV_CB_DEV_NO_WORK_FN_ID,           NULL},
};

static int proc_adap_queue2cfw(const struct ue_dev2cfw_info& dev2cfw_info)
{
        CPP_FTRACE1();
        int ret_val = -1;
        for(struct adap_cfw_proc_desc *proc_desc = adap_cfw_proc_tbl;
            NULL != proc_desc->proc;   proc_desc++) {
                if(dev2cfw_info.fnc_id == proc_desc->fnc_id) {
                        ret_val = proc_desc->proc(dev2cfw_info);
                        break;
                }
        }

        return ret_val;
}

static int sizeof_buf4dev_location_rpt(enum ue_pos_id pos_id)
{
        CPP_FTRACE1();
        
        int size = 0;
        
        switch(pos_id) {

        case loc_gnss_ie:     size = sizeof(struct loc_gnss_info); break;
        case loc_sat_native:  size = sizeof(struct dev_sat_info);  break;
        case vel_gnss_ie:     size = sizeof(struct vel_gnss_info); break;
	 	case tim_gps_ie:     size = sizeof(struct gps_time); break;
        case tim_glo_ie:     size = sizeof(struct ganss_ref_time); break;
        case loc_dop_native:     size = sizeof(struct dev_dop_info); break;
        case hdg_native:     size = sizeof(struct dev_hdg_info); break;

        default: DBG_L3("ID %u unsupported, ignoring", pos_id);    break;
        };

        return size;
}

static int dev_location_rpt(enum ue_pos_id pos_id, void *cb_data, int num_obj)
{
	CPP_FTRACE1();

	struct ue_dev2cfw_info dev2cfw_info;

	int size = sizeof_buf4dev_location_rpt(pos_id);
	if(0 == size) {
		//ERR_NM("Unexpected IE in POS RPT from device,  ignoring");
		return 0;
	}

	char *buffer = new char[size * num_obj];
	if(!buffer) {
		ERR_NM("Failed to alloc buf for ue_adap_queue, ignoring");
		return 0; // Check if changing to -1 helps.
	}

	if(loc_gnss_ie == pos_id) {
		DBG_L1("IN ref_secs %u", ((struct loc_gnss_info*) cb_data)->ref_secs);	
	}

	memcpy(buffer, cb_data, size * num_obj);

	dev2cfw_info.fnc_id = DEV_CB_DEV_LOC_RPT_FN_ID;
	dev2cfw_info.param1 = (void*) pos_id;
	dev2cfw_info.param2 = (void*) buffer;
	dev2cfw_info.param3 = (void*) num_obj;

	push2ue_adap_queue(dev2cfw_info);

	return 0;
}

static int sizeof_buf4dev_msr_rpt(enum ue_msr_id msr_id)
{
        CPP_FTRACE1();
        
        int size = 0;
        
        switch(msr_id) {

        case msr_gps_native:  size = sizeof(struct dev_msr_info); break;
        case msr_glo_native:  size = sizeof(struct dev_msr_info); break;
        case msr_gps_rpt_ie:  size = sizeof(struct gps_msr_info); break;
		case msr_device_result: size = sizeof(struct dev_msr_result); break; 
        case msr_ganss_rpt_ie:  size = sizeof(struct ganss_msr_info); break;

        default: DBG_L3("MSR ID %u not handled", msr_id); break;
        };

        return size;
}

static int dev_msr_rpt(enum ue_msr_id msr_id, void *cb_data, int num_obj)
{
        CPP_FTRACE1();

        struct ue_dev2cfw_info dev2cfw_info;

        int size = sizeof_buf4dev_msr_rpt(msr_id);
        if(0 == size) {
               // ERR_NM("Unexpected IE in MSR RPT from device,  ignoring");
                return 0;
        }

        char *buffer = new char[size * num_obj];
        if(!buffer) {
                ERR_NM("Failed to alloc buf for ue_adap_queue, ignoring");
                return 0; // Check if changing to -1 helps.
        }

        memcpy(buffer, cb_data, size * num_obj);

        dev2cfw_info.fnc_id = DEV_CB_DEV_MSR_RPT_FN_ID;
        dev2cfw_info.param1 = (void*) msr_id;
        dev2cfw_info.param2 = (void*) buffer;
        dev2cfw_info.param3 = (void*) num_obj;

        push2ue_adap_queue(dev2cfw_info);

        return 0;
}

static int sizeof_buf4dev_agnss_dec(enum loc_assist assist)
{
        CPP_FTRACE1();
        
        int size = 0;
        
        switch(assist) {

        case a_GPS_EPH:  size = sizeof(struct gps_eph); break;
        case a_GPS_ALM:  size = sizeof(struct gps_alm); break;
        case a_GLO_EPH:  size = sizeof(struct glo_eph); break;
        case a_GLO_ALM:  size = sizeof(struct glo_alm); break;
        case a_GPS_TIM:  size = sizeof(struct gps_ref_time); break;

        default: DBG_L3("LOC Assist %u not handled", assist); break;
        };

        return size;
}

static int dev_agnss_dec(enum loc_assist assist, void *assist_array, int num)
{
        CPP_FTRACE1();


        struct ue_dev2cfw_info dev2cfw_info;

        int size = sizeof_buf4dev_agnss_dec(assist);
        if(0 == size) {
               // ERR_NM("Unexpected IE in AGNSS DEC from device,  ignoring");
                return 0;
        }

        char *buffer = new char[size * num];
        if(!buffer) {
                ERR_NM("Failed to alloc buf for ue_adap_queue, ignoring");
                return 0; // Check if changing to -1 helps.
        }

        memcpy(buffer, assist_array, size * num);


        dev2cfw_info.fnc_id = DEV_CB_DEV_AGNSS_DEC_FN_ID;
        dev2cfw_info.param1 = (void*) assist;
        dev2cfw_info.param2 = (void*) buffer;
        dev2cfw_info.param3 = (void*) num;

        push2ue_adap_queue(dev2cfw_info);

        return 0;
}

int dev_nmea_rpt(enum nmea_sn_id nmea, char *cb_data, int length)
{
        CPP_FTRACE1();
	struct ue_dev2cfw_info dev2cfw_info;

	char *buffer = new char[length];
	if(!buffer) {
		ERR_NM("Failed to alloc buf for ue_adap_queue, ignoring");
                return 0;
        }

	memcpy(buffer, cb_data, length);

	dev2cfw_info.fnc_id = DEV_CB_DEV_NMEA_MSG_FN_ID;
	dev2cfw_info.param1 = (void*) nmea;
	dev2cfw_info.param2 = (void*) buffer;
	dev2cfw_info.param3 = (void*) length;

	if(nmea == gprmc){
		pthread_mutex_lock(&recovery_mtx);
		ue_recovery_count = 0;
		_setup_meas_timer_Reload(AWAIT4MEAS_REP_SECS);
		pthread_mutex_unlock(&recovery_mtx);

		
	}

	push2ue_adap_queue(dev2cfw_info);

	return 0;
}

static int dev_loc_failed(enum lc_evt_id id)
{
        CPP_FTRACE1();

        struct ue_dev2cfw_info dev2cfw_info;

        dev2cfw_info.fnc_id = DEV_CB_DEV_LOC_FAIL_FN_ID;
        dev2cfw_info.param1 = (void*) id;

        push2ue_adap_queue(dev2cfw_info);

        return 0;
}

/* add for GNSS recovery ++*/
/*ti_dev_error_ind  device_error*/
int dev_error_ind(enum dev_err_id id)
{
	CPP_FTRACE1();

	DBG_L1(" err_seq: received error indication from device %d",id);

	struct ue_dev2cfw_info dev2cfw_info;
	
	dev2cfw_info.fnc_id= DEV_CB_DEV_ERR_IND_FN_ID;
	dev2cfw_info.param1 = (void*) id;
	
	push2ue_adap_queue(dev2cfw_info);
	
	return 0;
}
/* add for GNSS recovery ++*/
int dev_cw_test_rpt(enum dev_cw_test cwt, void *cb_data, int length)
{
        CPP_FTRACE1();
	struct ue_dev2cfw_info dev2cfw_info;
	int len = sizeof(struct cw_test_report);
	char *buffer = new char[sizeof(struct cw_test_report)];
	if(!buffer) {
		ERR_NM("Failed to alloc buf for ue_adap_queue, ignoring");
                return 0;
        }
	
	memcpy(buffer, cb_data, len);

	dev2cfw_info.fnc_id = DEV_CB_DEV_CW_TEST_REP_FN_ID;
	dev2cfw_info.param1 = (void*) cwt;
	dev2cfw_info.param2 = (void*) buffer;
	dev2cfw_info.param3 = (void*) len;

	push2ue_adap_queue(dev2cfw_info);

	return 0;
}
int ue_exit(void)
{
	CPP_FTRACE1();
	DBG_L1(" [UE ADP] exit: Entering");

	ti_dev_release();

	ti_dev_proxy_release();
	DBG_L1(" [UE ADP] exit: Exiting");
	return 0;
}


static void *adap_queue2cfw_work(void *arg1)
{
        CPP_FTRACE1();
		UNUSED(arg1);

        while(1) {
                struct ue_dev2cfw_info dev2cfw_info = recv_ue_adap_queue();
                proc_adap_queue2cfw(dev2cfw_info);
        }// while(1)
		

        return 0;
}

struct ti_dev_callbacks dev_callbacks = {

	dev_location_rpt,
	dev_msr_rpt,
	dev_loc_failed,
	dev_nmea_rpt,
	dev_agnss_dec,
	dev_error_ind,
	dev_event_ind,
	dev_app_ucase_ind,
	dev_cw_test_rpt
};

static void setup_ext_assist(void)
{
	struct location_desc ref_pos[1];
	struct gps_ref_time tim[1];

	/* Read from NVS to get the reference pos */
	if (ti_dev_loc_aiding_get(a_REF_POS, ref_pos, 1) > 0) {
		/* update ext-assist pos transient info */
		ext_assist_put_gnss_loc_spec(ref_pos);		
	} else {
		DBG_L1(" [UE ADP] UE Has no reference position, nvs pos file not created");
	}

	/* Read from NVS to get the reference time */
	if (ti_dev_loc_aiding_get(a_GPS_TIM, tim, 1) > 0) {
		/* update ext-assist gps time transient info */
		ext_assist_put_gps_time_spec(&tim->time);		
	} else {
		DBG_L1(" [UE ADP] UE Has no gps reference time, nvs gps clk file not created");
	}
}
/*------------------------------------------------------------------------------
 * Invokes to device
 *----------------------------------------------------------------------------*/
int init(/*struct ti_gnss_ue_callbacks *gnss_ue_cbs*/)
{
	CPP_FTRACE1();
	int retval;
	struct ti_dev_proxy_params dev_proxy_params;
	dev_proxy_params.fn_call_timeout_ms = 1000;
	struct dev_start_up_info start_info;
	time_t bootup_time = 0;

	DBG_L1(" [UE ADP] init: Entering");

	//gnss_ue_callbacks = gnss_ue_cb;

	retval = ti_dev_proxy_connect(&dev_callbacks, &dev_proxy_params); 
	if(retval == -1){
		DBG_L1(" [UE ADP] init: ti_dev_proxy_connect FAILED");
		return -1;
	}

	start_info.nvs_aid_cfg.aid_config = dev_nvs_aid_config ::aid_dont_inj;
	start_info.nvs_aid_cfg.epoch_secs = 1;
	strncpy(start_info.config_file, CONFIG_FILE_PATH, sizeof(start_info.config_file)-1);
	strncpy(start_info.sevice_pack, SEVICE_PACK_FILE_PATH, sizeof(start_info.sevice_pack)-1);

	ti_dev_connect(&start_info);

	setup_ext_assist(); /* Fetch info prior to dev sleep */

	ti_dev_exec_action(dev_dsleep);
	UNUSED(bootup_time);

	DBG_L1(" [UE ADP] init: Exiting");

	return 0;
}


inline static unsigned int get_active_async_evts2cfg(void)
{
	CPP_FTRACE1();
	/*** Complete set or options availalbe ***/

	/*
	   ASYC_EXT_HW_EVT | ASYC_RX_DIAG_EVT | ASYC_RX_STATE_EVT | 
	   ASYC_RX_PWR_EVT | ASYC_FIRST_FIX_EVT
	 */

	/*** Set to be configured ***/
	unsigned int async_events = 
		ASYC_RX_STATE_EVT | ASYC_EXT_HW_EVT;

	return async_events;
}

inline static unsigned int get_active_bcast_evts2cfg(void)
{
	CPP_FTRACE1();
	/*** Complete set or options availalbe ***/

	/* 
	   ASYC_EVT_NEW_EPH | ASYC_EVT_NEW_ALM | ASYC_EVT_NEW_HLT | 
	   ASYC_EVT_NEW_UTC
	 */

	/*** Set to be configured ***/
	unsigned int bcast_events = 
		ASYC_EVT_NEW_EPH | ASYC_EVT_NEW_ALM | ASYC_EVT_NEW_HLT |
		ASYC_EVT_NEW_UTC | ASYC_EVT_NEW_ION;

	return bcast_events;
}

inline static unsigned int get_active_nmea_flags2cfg(void)
{
        /*** Complete set or options available ***/

        /* 
           SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) | 
	   SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg) | 
	   SET_BIT(glgga) | SET_BIT(glgll) | SET_BIT(glgsa) | 
	   SET_BIT(glgsv) | SET_BIT(glrmc) | SET_BIT(glvtg)
        */
        
        /*** Set to be configured ***/
	unsigned int nmea_flags = 
                SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) | 
		SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg) | 
		SET_BIT(glgga) | SET_BIT(glgll) | SET_BIT(glgsa) | 
		SET_BIT(glgsv) | SET_BIT(glrmc) | SET_BIT(glvtg) | SET_BIT(gngrs)|
		SET_BIT(gngll)|SET_BIT(gngsa)|SET_BIT(gngns)|SET_BIT(gnvtg) |SET_BIT(gpgrs)|
		SET_BIT(gpgst)|SET_BIT(qzgsv)|SET_BIT(wsgsv) ;

        return nmea_flags;
}

inline static unsigned int get_active_msr_report2cfg(void)
{
        /*** Complete set or options available ***/

        /* 
           MSR_RPT_GPS_NATIVE | MSR_RPT_GPS_IE_SEL | MSR_RPT_GLO_NATIVE | 
	   MSR_RPT_GLO_IE_SEL 
        */
  
        /*** Set to be configured ***/
        unsigned int msr_reports = 
                MSR_RPT_GPS_NATIVE | MSR_RPT_GLO_NATIVE  | MSR_RPT_DEV_RESULT | 
                MSR_RPT_GPS_IE_SEL  | MSR_RPT_GLO_IE_SEL ;
		
        return msr_reports;
}

inline static unsigned int get_active_pos_report2cfg(void)
{
        /*** Complete set or options available ***/
        
        /* POS_RPT_LOC_NATIVE | POS_RPT_GPS_IE_SEL | POS_RPT_GLO_IE_SEL | 
	   POS_RPT_VEL_NATIVE | POS_RPT_VEL_IE_SEL | 
	   POS_RPT_TIM_GPS_NATIVE | POS_RPT_TIM_GLO_NATIVE | 
	   POS_RPT_TIM_GPS_IE_SEL | POS_RPT_TIM_GLO_IE_SEL
	 */

        /*** Set to be configured ***/
	unsigned int pos_reports = 
		POS_RPT_NATIVE_ALL | POS_RPT_VEL_NATIVE | POS_RPT_GPS_IE_SEL | 
		POS_RPT_TIM_GPS_NATIVE | POS_RPT_TIM_GPS_IE_SEL | 
		POS_RPT_TIM_GLO_NATIVE | POS_RPT_TIM_GLO_IE_SEL |
		POS_RPT_VEL_IE_SEL;

        return pos_reports;
}

static int cfg_dev_rpt(unsigned int interval)
{
	CPP_FTRACE1();

        bool cfg2stop = (0 == interval) ? true : false;

        unsigned int async_events = cfg2stop ? 0: get_active_async_evts2cfg(); 
        unsigned int dev_agnss    = cfg2stop ? 0: get_active_bcast_evts2cfg();
        unsigned int nmea_flags   = cfg2stop ? 0: get_active_nmea_flags2cfg();
        unsigned int msr_reports  = cfg2stop ? 0: get_active_msr_report2cfg();
        unsigned int pos_reports  = cfg2stop ? 0: get_active_pos_report2cfg();

        DBG_L1("%s device: async_events: 0x%x, interval: %dsecs", cfg2stop? \
               "Stopping" : "Starting / reconfig", async_events, interval);

        DBG_L1("dev_agnss: 0x%x, nmea_flags: 0x%x, msr: 0x%x, pos: 0x%x",
                dev_agnss,       nmea_flags, msr_reports,  pos_reports);

        if(ti_dev_config_reports(async_events,  dev_agnss,  nmea_flags,
                                 msr_reports, pos_reports, interval) < 0) {
		ERR_NM("LOC %s : ti_dev_config_reports failed", \
                       cfg2stop? "STOP" : "START");
		return -1;
	} 

        return 0;
}

/**
  Start operation of device for LOC.

  @param[in]   hnd: abstract hadnle to object that implements needed behavior

  @return 0 on success else -1 on error
 */
int loc_start(void *hnd)
{
        CPP_FTRACE1();
	int sat_cnt, idx, prn;
	static struct aiding_status ttl[32];

        DBG_L1("Activating device through dev-proxy.... ");
        setup_ext_assist();

	/* CLK-MSR-POS reported so, clear up all place-holders */
        //reset_ue_adapter_data();
#if 1
	pthread_mutex_lock(&recovery_mtx);
	_setup_meas_recovery_timer(AWAIT4MEAS_REP_SECS, wait4meas_rep_timer);
	pthread_mutex_unlock(&recovery_mtx);
#endif	

	ti_dev_exec_action(dev_active);

	struct dev_clk_calibrate calib;
	calib.flags  = DEV_CALIB_AUTO;
	calib.ref_clk_freq = 26000000;
	calib.ref_clk_quality = 20;
	
	ti_dev_setup_oper_param(clk_calibrate,&calib);

	/* get gps eph ttl */
    	for (prn = 1; prn < (32 + 1); prn++)
		ttl[prn - 1].obj_id = prn;

	sat_cnt = ti_dev_loc_aiding_status(a_GPS_EPH, ttl, 32);
	for (idx = 0; idx < sat_cnt; idx++) {
		if ((ttl[idx].ttl_ms /1000) > MIN_TTL) {
                        ue_gps_aux_desc->eph_bits |= (1 << (ttl[idx].obj_id - 1));
		}
	}

	/* get glo eph ttl */
	for (prn = 1; prn < (24 + 1); prn++)
		ttl[prn - 1].obj_id = prn;
        
	sat_cnt = ti_dev_loc_aiding_status(a_GLO_EPH, ttl, 24);
	for (idx = 0; idx < sat_cnt; idx++) {
		if ((ttl[idx].ttl_ms /1000) > MIN_TTL) {
                        ue_glo_aux_desc->eph_bits |= (1 << (ttl[idx].obj_id - 1));
		}
	}

	/* get gps alm ttl */
    	for (prn = 1; prn < (32 + 1); prn++)
		ttl[prn - 1].obj_id = prn;

	sat_cnt = ti_dev_loc_aiding_status(a_GPS_ALM, ttl, 32);
	for (idx = 0; idx < sat_cnt; idx++) {
		if ((ttl[idx].ttl_ms /1000) > MIN_TTL) {
                        ue_gps_aux_desc->alm_bits |= (1 << (ttl[idx].obj_id - 1));
		}
	}

	/* get glo alm ttl */
	for (prn = 1; prn < (24 + 1); prn++)
		ttl[prn - 1].obj_id = prn;
        
	sat_cnt = ti_dev_loc_aiding_status(a_GLO_ALM, ttl, 24);
	for (idx = 0; idx < sat_cnt; idx++) {
		if ((ttl[idx].ttl_ms /1000) > MIN_TTL) {
                        ue_glo_aux_desc->alm_bits |= (1 << (ttl[idx].obj_id - 1));
		}
	}

	DBG_L1("Activating device through dev-proxy... Done ");
        
	UNUSED(hnd);
	return 0;
}

/**
  Stop LOC operation 

  @param[in]   hnd: abstract hadnle to object that implements needed behavior

  @return 0 on success else -1 on error
 */ 
int loc_stop(void *hnd)
{
        CPP_FTRACE1();

	DBG_L1("De-activating device through dev-proxy....");

	ti_dev_exec_action(dev_de_act);

	/*ti_dev_exec_action(dev_dsleep);*/

	/* CLK-MSR-POS reported so, clear up all place-holders */
        reset_ue_adapter_data();

        ext_assist_del_ueb();
        ext_assist_del_uea();

        /*reset glo variable*/
        chk_glo_count = 0;
        chk_glo_secs  = 0;
        chk_glo_refs  = 0;
        memset(chk_glo_eph, 0, 24 * sizeof(struct glo_eph));
#if 1		
	pthread_mutex_lock(&recovery_mtx);

//	_setup_meas_timer_Reload(0);
	_erase_meas_recovery_timer();
	pthread_mutex_unlock(&recovery_mtx);
#endif
	UNUSED(hnd);
	return 0;
}

static struct dev_caps ue_caps[2];
static int    ue_n_gnss = 0;

static int find_dev_caps(void)
{
	CPP_FTRACE1();

	return (ue_n_gnss > 0)? 
		ue_n_gnss : ue_n_gnss = ti_get_dev_caps(ue_caps, 2);
}

static unsigned int find_dev_gnss_cfg(void)
{
        CPP_FTRACE1();
        unsigned int gnss_map = 0;

        int n_gnss = find_dev_caps();
        if(n_gnss < 0)
                return 0;

	for(int idx = 0; idx < n_gnss; idx++) {
		if(ue_caps[idx].gnss_id == e_gnss_gps)
                gnss_map |= (1 << e_gnss_gps);

		if(ue_caps[idx].gnss_id == e_gnss_glo)
                gnss_map |= (1 << e_gnss_glo);
	}

        return gnss_map;
}

/**
  Get cabilities of UE to perform LOC (MSR / POS). 

  @param[in]    hnd: abstract handle to object that implements needed behavior  
  @param[out]   gps_caps_bits: bitmap, UE outlines supported GPS capabilities
  @param[inout] ganss_caps: array, UE outlines supported GANSS capabilities

  @return 0 on success else -1 on error

  @note as input struct ganss_ue_lcs_caps and nested members carries the number
  (max) of elements in corresponding array. Where as, as output, implementation
  must specify the actual number of populated elements in respective arrays.
 */
static int get_ue_loc_caps(void *hnd, unsigned int& gps_caps_bits, 
                           struct ganss_ue_lcs_caps *ganss_caps)
{
	CPP_FTRACE1();
	int n_gnss = find_dev_caps();
	DBG_L1(" [UE ADP] get_ue_loc_caps n_gnss = [%d]", n_gnss);
	if(n_gnss < 0){		
		return -1;
	}
	
	ganss_caps->n_ganss = 0; // Initializing or resetting ... 
	while (n_gnss--) {
		DBG_L1(" [UE ADP] get_ue_loc_caps gnss_id = [%d]", ue_caps[n_gnss].gnss_id);
		if (ue_caps[n_gnss].gnss_id == e_gnss_gps) {
			gps_caps_bits = ue_caps[n_gnss].caps_bits;
			DBG_L1(" [UE ADP] get_ue_loc_caps caps_bits = [%d]", ue_caps[n_gnss].caps_bits);
			continue;
		}
                
		if (ue_caps[n_gnss].gnss_id == e_gnss_glo) {
			ganss_caps->n_ganss++;
                        
#define LCS_CAPS(idx) ganss_caps->lcs_caps[idx]
                        
			LCS_CAPS(0).ganss_id  = GANSS_ID_GLO;
			LCS_CAPS(0).caps_bits = ue_caps[n_gnss].caps_bits;
			LCS_CAPS(0).sigs_bits = ue_caps[n_gnss].sigs_bits;
			LCS_CAPS(0).sbas_bits = ue_caps[n_gnss].sbas_bits;
			DBG_L1(" [UE ADP] get_ue_loc_caps caps_bits = [%d]", ue_caps[n_gnss].caps_bits);
			DBG_L1(" [UE ADP] get_ue_loc_caps sigs_bits = [%d]", ue_caps[n_gnss].sigs_bits);
			DBG_L1(" [UE ADP] get_ue_loc_caps sbas_bits = [%d]", ue_caps[n_gnss].sbas_bits);

			continue;
		}
	}
	UNUSED(hnd);
	return 0;
}

/**
  Get capabilities of UE to interwork Assisted-GNSS.

  @param[in]     hnd: abstract handle to object that implements needed behavior
  @param[in]  gps_caps_bits: bitmap, UE outlines supported A-GPS capabilities
  @param[inout] ganss_caps: array, UE outlines supported A-GANSS capabilities

  @return 0 on success else -1 on error

  @note as input struct ganss_ue_lcs_caps and nested members carries the number
  (max) of elements in corresponding array. Where as, as output, implementation
  must specify the actual number of populated elements in respective arrays.
 */
int get_ue_agnss_caps(void *hnd, unsigned int& gps_caps_bits, 
                      struct ganss_ue_assist_caps *ganss_caps)
{
	CPP_FTRACE1();
	DBG_L1(" [UE ADP] get_ue_agnss_caps");
	int n_gnss = find_dev_caps();
	if (n_gnss < 0){
		DBG_L1(" [UE ADP] get_ue_agnss_caps n_gnss = [%d]", n_gnss);
		return -1;
	}

	while (n_gnss--) {

		if (ue_caps[n_gnss].gnss_id == e_gnss_gps) {
			gps_caps_bits = ue_caps[n_gnss].asst_bits;
			continue;
		}

		if (ue_caps[n_gnss].gnss_id == e_gnss_glo) {
			ganss_caps->n_ganss = 1;
			ganss_caps->common_bits = 0;

#define GANSSS_CAPS(idx) ganss_caps->ganss_caps[idx]                        

			GANSSS_CAPS(0).id_of_ganss = GANSS_ID_GLO;
			GANSSS_CAPS(0).caps_bitmap = ue_caps[n_gnss].asst_bits;
			GANSSS_CAPS(0).data_mdl.orb_model = UE_ASSIST_ORB_MDL;
			GANSSS_CAPS(0).data_mdl.clk_model = UE_ASSIST_CLK_MDL;
			GANSSS_CAPS(0).data_mdl.alm_model = UE_ASSIST_ALM_MDL;
			GANSSS_CAPS(0).data_mdl.utc_model = UE_ASSIST_UTC_MDL;

			continue;
		}
	}
	
	UNUSED(hnd);
	return 0;
}

/**
  Set reporting interval in UE for LOC (MSR / POS).

  @param[in]     hnd: abstract handle to object that implements needed behavior
  @param[in]     interval_secs: LOC reporting interval in seconds.

  @return 0 on success else -1 on error
 */
int set_loc_interval(void *hnd, unsigned int interval_secs)
{
	CPP_FTRACE1();

        DBG_L1("UE ADP: setting inteval of %u secs into device", interval_secs);

        if(cfg_dev_rpt(interval_secs) < 0) {
                return -1;
        }
	UNUSED(hnd);
	/* CHECK: Don't we need to set config reports in the dev-proxy from here */
	/* CHECK: Discuss with Madhu */
	return 0;
}

static int set_intended_qop(void *hnd,const  struct gnss_qop &qop)
{
	CPP_FTRACE1();
	UNUSED(hnd);

	struct dev_qop_specifier dev_qop[1];
	dev_qop->accuracy_m = qop.h_acc_M;
	dev_qop->max_ttf_ms = (unsigned int)(qop.rsp_secs * 1000);

	DBG_L1("UE ADP: setting into device; accuracy = %u m,  max_ttf = %u ms",
               dev_qop->accuracy_m, dev_qop->max_ttf_ms);

	if(ti_dev_setup_oper_param(qop_specifier, dev_qop) < 0) {
		ERR_NM("UE ADP: ti_dev_setup_oper_param for qop failed");
		return -1;
	}
	return 0;
}


struct ext_assist_io_desc {

        void *rx_array;
        int   n_rx_obj;

        void *tx_array;
        int   n_tx_obj;
};

static int put_n_get_ext_assist_gps_eph(struct ext_assist_io_desc& assist_io)
{
        CPP_FTRACE1();
	int ret_val = 0;
        struct gps_eph *eph_inp = (struct gps_eph*) assist_io.rx_array;
        static struct gps_eph eph_out[MAX_SAT]; /* Ensuring not on stack */

	if(0 == assist_io.n_rx_obj) {
		DBG_L3("No external EPH to add; not trying to fetch ...");
		goto ext_assist_get_gps_eph;
	}

	ret_val = ext_assist_put_gps_eph(eph_inp, assist_io.n_rx_obj);
        if(ret_val < 0)
                return ret_val;

ext_assist_get_gps_eph:
       ret_val = ext_assist_get_gps_eph_visible(eph_out, MAX_SAT);
	   	   
	assist_io.tx_array = eph_out;
	assist_io.n_tx_obj = ret_val;

	DBG_L1("put_n_get_ext_assist_gps_eph: n_rx_obj = %d, ret_val = %d", assist_io.n_rx_obj, ret_val);
	return ret_val;
}

static int put_ext_assist_gps_time(struct ext_assist_io_desc& assist_io)
{
	CPP_FTRACE1();
	int ret_val = 0;

	struct gps_time *gps_ref_time = (struct gps_time*) assist_io.rx_array;

	ret_val = ext_assist_put_gps_time(gps_ref_time);

	assist_io.tx_array = gps_ref_time;
	assist_io.n_tx_obj = assist_io.n_rx_obj;

	return ret_val;
}

static int put_ext_assist_gnss_loc(struct ext_assist_io_desc& assist_io)
{
	CPP_FTRACE1();
	int ret_val = 0;

	struct location_desc *loc_desc = (struct location_desc*) assist_io.rx_array;

	ret_val = ext_assist_put_gnss_loc(loc_desc);

	assist_io.tx_array = loc_desc;
	assist_io.n_tx_obj = assist_io.n_rx_obj;

	return ret_val;
}
static int put_n_get_ext_assist_gps_acq(struct ext_assist_io_desc& assist_io)
{
	CPP_FTRACE2();
	int ret_val = 0;
	struct gps_acq *acq_inp = (struct gps_acq*) assist_io.rx_array;
	static struct gps_acq acq_out[MAX_SAT]; /* Ensuring not on stack */

	if(0 == assist_io.n_rx_obj) {
		DBG_L3("No external ACQ to add; not trying to fetch ...");
		goto ext_assist_get_gps_acq;
	}

	ret_val = ext_assist_put_gps_acq(acq_inp, assist_io.n_rx_obj);
	if(ret_val < 0)
		return ret_val;

ext_assist_get_gps_acq:

	ret_val = ext_assist_get_gps_acq(acq_out, MAX_SAT);
	if(ret_val < 0)
		return ret_val;

	assist_io.tx_array = acq_out;
	assist_io.n_tx_obj = ret_val;

	return ret_val;
}

static int put_n_get_ext_assist_glo_eph(struct ext_assist_io_desc& assist_io)
{
	CPP_FTRACE1();
	int ret_val = 0, n_obj = 0;
	struct glo_eph *eph_inp = (struct glo_eph*) assist_io.rx_array;
	//        static struct glo_eph eph_out[MAX_SAT]; /* Ensuring not on stack */

	if(0 == assist_io.n_rx_obj) {
		DBG_L3("No external GLO EPH to add; not trying to fetch ...");
		goto ext_assist_get_glo_eph;
	}

ext_assist_get_glo_eph:
	// ret_val = ext_assist_get_glo_eph_visible(eph_out, MAX_SAT);
	ret_val = chk_glo_count = n_obj = assist_io.n_rx_obj;
	for(int n = 0; n < n_obj; n++) {
		memcpy(chk_glo_eph + n, eph_inp + n, sizeof(struct glo_eph));
	}

	assist_io.tx_array = chk_glo_eph;
	assist_io.n_tx_obj = ret_val;

	DBG_L1("put_n_get_ext_assist_glo_eph: n_rx_obj = %d, ret_val = %d", assist_io.n_rx_obj, ret_val);
	return ret_val;
}

#if  0
static int put_ext_assist_glo_time(struct ext_assist_io_desc& assist_io)
{
	CPP_FTRACE1();

	int ret_val = 0;

	struct ganss_ref_time *glo_time = (struct ganss_ref_time*) assist_io.rx_array;
	chk_glo_secs = glo_time->day * MAX_SECS_IN_A_DAY + glo_time->tod;

	DBG_L1("GLO Time: GLO SECS: %u", chk_glo_secs); 

	return ret_val;
}
#endif

static bool chk_is_ext_glo_valid()
{
	if((chk_glo_count > 3) && (0 != chk_glo_secs)) {
		struct glo_eph *eph = chk_glo_eph + 0;
		int diff = (DO_GLO_NT_SECS(eph->NT) +
			    DO_GLO_Tk_SECS(eph->tk) +
			    - chk_glo_secs);

                /* Allow additional use of GLO EPH for MIN_TTL to avoid
                   race conditions at boundary of 0, 15, 30, 45 minutes.
                   GLO EPH are valid for 30 minutes and generated every 
                   15 minutes.
                */
                if(abs(diff) < (15*60 + MIN_TTL))
                        return true;

                chk_glo_count = 0; /* Invalid GLO EPH */
	}

	return false;
}

static int 
push2dev_icd_assist(enum loc_assist assist_id, void *assist_array, 
                    int num)
{
        CPP_FTRACE1();

        struct ext_assist_io_desc assist_io = {assist_array, num, 
                                               assist_array, num};

        DBG_L1("Ext assist ID: %u with num of elems: %d", assist_id, num);
        
        int ret_val = 0;

        switch(assist_id) {
                /* ICD format GPS external assistance */
        case a_GPS_EPH: {
                /* GPS EPH info MAY have been filtered by following function */
                ret_val = put_n_get_ext_assist_gps_eph(assist_io);
                break;
        }

        case a_GPS_TIM: {
                ret_val = put_ext_assist_gps_time(assist_io);
                break;
        }

        case a_REF_POS: {
                /* Need to update POS update times */
                ret_val = put_ext_assist_gnss_loc(assist_io);
                break;
        }

        case a_GPS_ACQ: {
                /* GPS ACQ info MAY have been updated by following function */
                ret_val = put_n_get_ext_assist_gps_acq(assist_io);
                break;
        }
		case a_GPS_ION: ret_val = 1; break;

        case a_GPS_ALM:
        case a_GPS_UTC:
        case a_GPS_DGP:
        case a_GPS_RTI:
                ret_val = num;
                break;

                /* ICD format GLO external assistance */
        case a_GLO_EPH:{
                ret_val = put_n_get_ext_assist_glo_eph(assist_io);
                break;
        }

        case a_GLO_TIM:
        case a_GLO_ALM:
                ret_val = num;
                break;

                /* add more GLO */
        default:
                ret_val = -1;
                break;
        }

	DBG_L1("Add assist Id %u w/ %d objs: %s", assist_id, assist_io.n_tx_obj,
	       ret_val? "Attempting for non zero objs" : "Ignored");

	if((0 < ret_val) && (0 != assist_io.n_tx_obj)) {
		ret_val = ti_dev_loc_aiding_add(assist_id, assist_io.tx_array, 
						assist_io.n_tx_obj);
	}

	return ret_val;
}

static int proc_icd_assist(enum loc_assist assist_id, void *assist_array, 
			   int num)
{
	CPP_FTRACE1();

	int ret_val = 0;

	switch(assist_id) {

		case a_REF_POS:
		case a_GPS_TIM:
			ret_val = push2dev_icd_assist(assist_id, assist_array, num);
			if(0 == ret_val) {
				/* Attempt to push previously provided EPH, if any */
				ret_val = push2dev_icd_assist(a_GPS_EPH, NULL, 0);
			}

			if(0 == ret_val) {
				/* Attempt to push previously provided ACQ, if any */
				ret_val = push2dev_icd_assist(a_GPS_ACQ, NULL, 0);
			}
			break;

		default:
			ret_val = push2dev_icd_assist(assist_id, assist_array, num);
			break;
	}

        return ret_val;
}

#if  0
static int proc_ext_assist_glo_nav_mdl(struct glo_nav_model *nav_mdl, int n_obj)
{
        CPP_FTRACE1();

        int ret_val = ext_assist_put_glo_nav_mdl(nav_mdl, n_obj);
        if(0 != ret_val) {
                ERR_NM("Failed to save ref to %d ext GLO NAV elems", n_obj);
                return -1;
        }

        static struct glo_eph eph[MAX_SAT]; /* Ensuring this is not on stack */
        int n_sat = ext_assist_get_glo_icd_eph(eph, MAX_SAT);
        if(0 == n_sat) {
                DBG_L3("GLO EPH based on ext non native info not ready, yet");
                return 0;
        }

        DBG_L3("GLO EPH: Pushing GLO ICD EPH from ext non native into device");
        return push2dev_icd_assist(a_GLO_EPH, (void*)eph, n_sat);
}

static int proc_ext_assist_glo_kp_set(struct glo_alm_kp_set *kp_set, int n_obj)
{
        CPP_FTRACE1();

        int ret_val = ext_assist_put_glo_kp_alm(kp_set, n_obj);
        if(0 != ret_val) {
                ERR_NM("Failed to save ref to %d ext GLO ALM elems", n_obj);
                return -1;
        }

        static struct glo_alm alm[MAX_SAT];/* Ensuring this is not on stack */
        int n_sat = ext_assist_get_glo_icd_alm(alm, MAX_SAT);
        if(0 == n_sat) {
                DBG_L3("KP  ALM: GLO ALM from ext non native not ready, yet");
                return 0;
        }

        DBG_L3("GLO ALM: Pushing GLO ICD ALM from ext non native into device");
        return push2dev_icd_assist(a_GLO_ALM, (void*)alm, n_sat);
}

static int proc_ext_assist_glo_utc_mdl(struct glo_utc_model *utc_mdl)
{
        CPP_FTRACE1();

        int ret_val = ext_assist_put_glo_time_mdl(utc_mdl);
        if(0 != ret_val) {
                ERR_NM("Failed to save ref to external GLO UTC MODEL");
                return 0;
        }

        static struct glo_alm alm[MAX_SAT];/* Ensuring this is not on stack */
        int n_sat = ext_assist_get_glo_icd_alm(alm, MAX_SAT);
        if(0 == n_sat) {
                DBG_L3("UTC MDL: GLO ALM from ext non native not ready, yet");
                return 0;
        }

        DBG_L3("GLO UTC: Pushing GLO ICD ALM from ext non native into device");
        return push2dev_icd_assist(a_GLO_ALM, (void*)alm, n_sat);
}

static int 
add2dev_nntv_assist(enum nnative_ie non_ntv_id, void *assist_array, int num)
{
	CPP_FTRACE1();

        int ret_val = 0;

        switch(non_ntv_id) {

        case glo_3gpp_eph: {
                struct glo_nav_model *nav_mdl = NULL;
                nav_mdl = (struct glo_nav_model*) assist_array;
                ret_val = proc_ext_assist_glo_nav_mdl(nav_mdl, num);
                break;
        }
         
        case glo_3gpp_alm: {
                struct glo_alm_kp_set *kp_set = NULL;
                kp_set  = (struct glo_alm_kp_set*) assist_array;
                ret_val = proc_ext_assist_glo_kp_set(kp_set, num); 
                break;
        }

        case glo_3gpp_utc: {
                struct glo_utc_model *utc_mdl = NULL;
                utc_mdl = (struct glo_utc_model*) = assist_array;
                ret_val = proc_ext_assist_glo_utc_mdl(utc_mdl);
                break;
        }

                /* Note: Need to include support for non zero Tau GPS */
                /* Need to have a kludge for GANSS Time Model */

        default:
                ERR_NM("Unexpected ID %u received, ignoring", non_ntv_id);
                ret_val = -1;
                break;
        }

        DBG_L3("%s provisiong non native assist info", ret_val? \
               "Problem in" : "Success in");

        return ret_val;
}
#endif 

/**
  Add stated (external) assistance to UE.

  @param[in]     hnd: abstract handle to object that implements needed behavior
  @param[in]  assist: identifies the type of GNSS assistance 
  @param[in] assist_array: array of information for specified GNSS assistance
  @param[in] num: number of elements in the array of GNSS assistance information

  @return 0 on success else -1 on error
 */
static int ue_add_assist(void *hnd, const struct nw_assist_id& id,
                         const void *assist_array, int num)
{
	CPP_FTRACE1();
	UNUSED(hnd);
	int ret_val = 0;
	switch (id.type_select) {

        case NTV_ICD: 
                DBG_L1("Processing native ICD assistance ID %u", id.gnss_icd_ie);
                ret_val = proc_icd_assist(id.gnss_icd_ie,
                                          const_cast<void*>(assist_array),  num);
                break;

        case NNATIVE:
		 //
                //ret_val = add2dev_nntv_assist(id.n_native_ie, assist_array, num);
                break;
                
        default:
                break;
	}
        
	return ret_val;
}

/**
  Delete stated aiding information from UE.

  @param[in]     hnd: abstract handle to object that implements needed behavior
  @param[in]  assist: identifies the type of GNSS aiding / assistance
  @param[in] sv_id_map: bitmap to ID satellites for the specified GNSS aiding

  @return 0 on success else -1 on error
 */
int ue_del_aiding(void *hnd, enum loc_assist assist, unsigned int sv_id_map,
                  unsigned int mem_flags)
{
	CPP_FTRACE1();
	UNUSED(hnd);

	unsigned int count = 0;

	DBG_L1("UE ADP: deleting aiding for %u", assist);

	switch (assist) {
		case a_GPS_ALM:
                        ue_gps_aux_desc->alm_bits = 0;
			break;

		case a_GPS_EPH:
                        ue_gps_aux_desc->eph_bits = 0;
			break;

		case a_GLO_ALM:
                        ue_glo_aux_desc->alm_bits = 0;
			break;

		case a_GLO_EPH:
                        ue_glo_aux_desc->eph_bits = 0;
			break;

		case a_REF_POS:
			ue_gps_aux_desc->pos_bits = 0;
			ue_glo_aux_desc->pos_bits = 0;
			break;

		default:
			break;
	}

	ti_dev_loc_aiding_del(assist, sv_id_map, mem_flags);

	return count;
}

/**
  Get specified aiding information from UE.

  @param[in]     hnd: abstract handle to object that implements needed behavior
  @param[in]  assist: identifies the type of GNSS aiding / assistance
  @param[inout] assist_array: array of information for specified GNSS aiding
  @param[in] num: number of elements in the array of GNSS aiding information

  @return 0 on success else -1 on error

  @note as input assist_array provides place holder to implementation to upload
  aiding data.
 */
int ue_get_aiding(void *hnd, enum loc_assist assist, 
                  void *assist_array, int num)
{
	CPP_FTRACE1();
	UNUSED(hnd);

    DBG_L2("ti_dev_loc_aiding_get Attempting to fetch aiding info id %d from device", assist);
 
	return ti_dev_loc_aiding_get(assist, assist_array, num);
}

static int assist2ue_done(void *hnd)
{
        CPP_FTRACE1();
        UNUSED(hnd);

        DBG_L1("No more UEB data is expected from active assist source");

        //ext_assist_del_ueb();

        return 0;
}


static unsigned int get_gps_wksec(unsigned int tow, enum tow_scale tow_unit)
{
        unsigned int secs = 0;

        switch(tow_unit) {

        case sec_8by100:
                secs = (tow *8) / 10;
                break;

	case milli_secs:
		secs = tow / 1000;
                break;

        case clear_secs:
                secs = tow;
                break;
                
        default:
                break;
        }

        return secs;
}

static bool mark_gps_tim_req4assist(struct gps_assist_req *gps_assist)
{
        CPP_FTRACE1();

	struct gps_ref_time  time_gps[1]; /* Srinivas Y: change in dev-proxy */

        if (ti_dev_loc_aiding_get(a_GPS_TIM, time_gps, 1) <= 0) {
                DBG_L1("UE has no ref GPS secs, need assist for TIM, POS, EPH");
                gps_assist->assist_req_map = 
                        A_GPS_TIM_REQ | A_GPS_POS_REQ | A_GPS_NAV_REQ;
                return true;
        }

        struct sat_nav_req_info *nav_req = &gps_assist->nav_assist_req;
        nav_req->ref_secs = time_gps->time.week_nr * 7*24*60*60; // get_week_secs(time_gps->week_nr);
        nav_req->ref_secs += (time_gps->time.n_wk_ro * 1024 * 7*24*60*60);
        nav_req->ref_secs += get_gps_wksec(time_gps->time.tow, time_gps->time.tow_unit);
        DBG_L1("ti_dev_loc_aiding_get UE has ref GPS secs of %u, RO: %u, Week: %u, units: %u", \ 
	       nav_req->ref_secs, time_gps->time.n_wk_ro,                \
	       time_gps->time.week_nr, time_gps->time.tow_unit);
        
        return false;
}

static bool mark_gps_pos_req4assist(struct gps_assist_req *gps_assist)
{
	struct location_desc ref_pos[1];
	
	if (ti_dev_loc_aiding_get(a_REF_POS, ref_pos, 1) <= 0) {
		DBG_L1("UE (NVS) has no REF POS, need ext assist for POS");
		gps_assist->assist_req_map = A_GPS_POS_REQ | A_GPS_NAV_REQ;
              return true;
	} 

	DBG_L1("latitude_N = %u,    longitude_N = %d,     altitude_N = %u", \
		ref_pos->latitude_N, ref_pos->longitude_N, ref_pos->altitude_N);

       return false;
}

static 
bool mark_gps_eph_req4assist(struct gps_assist_req *gps_assist, int& eph_sat)
{
	CPP_FTRACE1();
	static struct aiding_status eph_ttl[32];

	for (int prn = 1; prn < (32 + 1); prn++) {
		eph_ttl[prn - 1].obj_id = prn;
	}
        
	int sat_cnt = ti_dev_loc_aiding_status(a_GPS_EPH, eph_ttl, 32);
        if(sat_cnt < 0) {
	        DBG_L1("UE has no ref GPS EPHs, need assist for EPH");
                goto mark_gps_eph_need_exit;
        }
        
        eph_sat = 0;
	for (int idx = 0; idx < sat_cnt; idx++) {
		if ((eph_ttl[idx].ttl_ms /1000) > MIN_TTL) {
			eph_sat += 1;
		}
	}

	DBG_L3("Number of valid GPS EPH sats are %d (%sadequate)", eph_sat,
               (eph_sat > MIN_EPH)? " " : "in");

	if(eph_sat > MIN_EPH)
		return false;

 mark_gps_eph_need_exit:

        gps_assist->assist_req_map |= A_GPS_NAV_REQ;
        return true;
}

static int get_gps_req4assist(struct gps_assist_req *gps_assist)
{
	CPP_FTRACE1();

        int n_sat = 0;

        memset(gps_assist, 0, sizeof(struct gps_assist_req));
        
        if((true == mark_gps_tim_req4assist(gps_assist)) ||
           (true == mark_gps_pos_req4assist(gps_assist)) ||
           (true == mark_gps_eph_req4assist(gps_assist, n_sat))) {

                DBG_L1("GPS assist need bits: 0x%x", gps_assist->assist_req_map);
                return 0;
        }

        DBG_L1("UE seems to have adequate GPS aid; available EPH %d", n_sat);
        
        return n_sat;

        /* : Check whether sat_info has to be really populated, not used in 128x */
        /* : Check whether req_infi has to be really populated, not used in 128x */
}

static bool mark_glo_tim_req4assist(struct ganss_assist_req *ganss_assist)
{
        CPP_FTRACE1();

	struct ganss_ref_time time_glo[1]; /* Srinivas Y: change in dev-proxy */
        struct ganss_req_info *ganss_req = &ganss_assist->ganss_req_data[0];
        struct sat_nav_req_info *nav_req = &ganss_req->nav_assist;

        if (ti_dev_loc_aiding_get(a_GLO_TIM, time_glo, 1) <= 0) {
                DBG_L1("UE has no ref GLO secs, needs assist for TIM, POS, EPH");
		ganss_assist->common_req_map = A_GANSS_TIM_REQ | A_GANSS_POS_REQ;
                ganss_req->req_bitmap        = A_GANSS_NAV_REQ;

                return true;
        }

        nav_req->ref_secs = (time_glo->day - 1)*24*60*60; //get_day_secs(time_glo->day - 1);
        nav_req->ref_secs += time_glo->tod;
        DBG_L1("UE has ref GLO secs of %u (day: %u, tod: %ums)", \ 
	       nav_req->ref_secs, time_glo->day, time_glo->tod);                
        
        return false;
}

static bool mark_glo_pos_req4assist(struct ganss_assist_req *ganss_assist)
{
	UNUSED(ganss_assist);
        return false;
}

static 
bool mark_glo_eph_req4assist(struct ganss_req_info *ganss_req, int& eph_sat)
{
	static struct aiding_status eph_ttl[24];

	for (int prn = 1; prn < (24 + 1); prn++) {
		eph_ttl[prn - 1].obj_id = prn;
	}
        
	int sat_cnt = ti_dev_loc_aiding_status(a_GLO_EPH, eph_ttl, 24);
        if(sat_cnt < 0) {
            DBG_L1("UE has no ref GLO EPHs, need assist for EPH");
                goto mark_glo_eph_need_tag;
        }

       	DBG_L3("UE has returned %d sats in resp to GLO EPH status", sat_cnt);	
        eph_sat = 0;
	for (int idx = 0; idx < sat_cnt; idx++) {
		if ((eph_ttl[idx].ttl_ms / 1000) > MIN_TTL) {
			eph_sat += 1;
		}
	}

	DBG_L3("Number of valid GLO EPH sats are %d (%sadequate)", eph_sat,
               (eph_sat > MIN_EPH)? " " : "in");

        if(eph_sat > MIN_EPH)
                return false;

 mark_glo_eph_need_tag:
        ganss_req->req_bitmap          |= A_GANSS_NAV_REQ;
        return true;
}

static int get_glo_req4assist(struct ganss_assist_req *ganss_assist)
{
	CPP_FTRACE1();

        int n_sat = 0;

        memset(ganss_assist, 0, sizeof(struct ganss_assist_req)); //  Common 
        ganss_assist->n_of_ganss_req     = 1;
        struct ganss_req_info *ganss_req = &ganss_assist->ganss_req_data[0];
        ganss_req->req_bitmap            = 0;
        ganss_req->id_of_ganss           = GANSS_ID_GLO;
        ganss_req->nav_assist.n_of_sat   = 0;
        
        if((true == mark_glo_tim_req4assist(ganss_assist)) ||
           (true == mark_glo_pos_req4assist(ganss_assist)) ||
           (true == mark_glo_eph_req4assist(ganss_req, n_sat))) {

                DBG_L1("GANSS assist bits: 0x%x", ganss_assist->common_req_map);
                DBG_L1("GLO assist bits: 0x%x", ganss_req->req_bitmap);
                return 0;
        }

        DBG_L1("UE seems to have adequate GLO aid; available EPH %d", n_sat);
        
        return n_sat;
}

/**
  Get information about needed assistance from UE.

  @param[in]     hnd: abstract handle to object that implements needed behavior
  @param[out]  gps_assist: UE outlines needed GPS assistance information
  @param[inout] ganss_assist: array, UE outlines needed GANSS assistance info
  @param[in]    max_ganss: indicates num of GANSS(es) (elemets) in ganss_assist

  @return 0 on success else -1 on error

  @note as input struct ganss_assist_req and nested members carries the number
  (max) of elements in corresponding array. Where as, as output, implementation
  must specify the actual number of populated elements in respective arrays.
 */
static int get_dev_req4assist(struct gps_assist_req &gps_assist,
		      struct ganss_assist_req& ganss_assist)
{
	CPP_FTRACE1();
        int n_req = 0;
	unsigned int gnss_select = find_dev_gnss_cfg();
	if(0 == gnss_select) {
                ERR_NM("Failed: to fetch GNSS capabilities of device, returning");
		return -1;
        }

	/* initialize various data structures */
        unsigned char n_ganss = ganss_assist.n_of_ganss_req;/* Ref : array sz */
        gps_assist.assist_req_map   = 0; /* : can be removed, if reset outside */
        ganss_assist.common_req_map = 0; /* : can be removed; if reset outside */
        ganss_assist.n_of_ganss_req = 0;

        int n_eph  = get_gps_req4assist(&gps_assist);
        n_req += n_eph ? 0 : 1;

        if(n_ganss && (gnss_select & (1 << e_gnss_glo))) {
		n_eph  = get_glo_req4assist(&ganss_assist);
                n_req += n_eph ? 0 : 1;
        }

	return n_req;
}

/**
  Get information about needed assistance from UE.

  @param[in]     hnd: abstract handle to object that implements needed behavior
  @param[out]  gps_assist: UE outlines needed GPS assistance information
  @param[inout] ganss_assist: array, UE outlines needed GANSS assistance info
  @param[in]    max_ganss: indicates num of GANSS(es) (elemets) in ganss_assist

  @return 0 on success else -1 on error

  @note as input struct ganss_assist_req and nested members carries the number
  (max) of elements in corresponding array. Where as, as output, implementation
  must specify the actual number of populated elements in respective arrays.
*/
static int get_ue_req4assist(void *hnd, struct gps_assist_req &gps_assist,
                             struct ganss_assist_req& ganss_assist)
{
	UNUSED(hnd);

        if(0 == get_dev_req4assist(gps_assist, ganss_assist)){
                DBG_L1("Adequate aid in NVS; ignoring transient assist DB ....");
                return 0;
        }
        
        /* Following variables are accessed in atomic manner by AGNSS Connect */
        static struct gps_assist_req    ext_gps_assist;
        static struct ganss_assist_req  ext_ganss_assist;

        if(0 != ext_assist_get_need(&ext_gps_assist, &ext_ganss_assist)) {
                ERR_NM("Failed to get need from transient DB, returning");
                return -1;
        }
        /* HACK - SRINIVAS remove this when EXT ASSIST IS UPDATED */
        if(false == chk_is_ext_glo_valid())
                ext_ganss_assist.ganss_req_data[0].req_bitmap = A_GANSS_NAV_REQ;

        /* To ensure that UEA is requested, whenever required, NVS needs are
           always seeded with GPS ACQ. Note: UE assist need is appropriately
           filtered out at a higher layer. Following steps are made to state
           assistance needs.
           a) Always seed GPS ACQ as needed (in the NVS assessment)
           b) Find the common requisites across NVS & transient DB
           c) Ensure that alwyas mandatory needs are indicated out
        */
        gps_assist.assist_req_map |= A_GPS_ACQ_REQ;                 /* step a */
        gps_assist.assist_req_map &= ext_gps_assist.assist_req_map; /* step b */
        gps_assist.assist_req_map &=                                /* step c */
                (A_GPS_TIM_REQ | A_GPS_POS_REQ | A_GPS_NAV_REQ | A_GPS_ACQ_REQ);

        DBG_L1("NVS/Transient DB, GPS Needs: 0x%x", gps_assist.assist_req_map);

        ganss_assist.common_req_map &=  ext_ganss_assist.common_req_map;
        ganss_assist.common_req_map &= (A_GANSS_TIM_REQ | A_GANSS_POS_REQ);

        DBG_L1("NVS/Transient DB, GANSS Com: 0x%x", ganss_assist.common_req_map);
        
        for(char idx = 0; idx < ganss_assist.n_of_ganss_req; idx++) {

#define REQ_BITMAP(assist) assist.ganss_req_data[idx].req_bitmap

                REQ_BITMAP(ganss_assist) &= REQ_BITMAP(ext_ganss_assist);
                REQ_BITMAP(ganss_assist) &= A_GANSS_NAV_REQ;

                DBG_L1("NVS/Transient DB, GLO: 0x%x", REQ_BITMAP(ganss_assist));
        }

	return 0;
}

static int find_min_ttl(const struct aiding_status* eph_ttl, int eph_cnt, 
                        int& ttl_sec)
{
        int num_sat = 0;   /* number of satellites with valid EPH */
	ttl_sec     = 0x7fffffff;

        static char buf[512]; int len = 0; buf[0] = '\0' ;/* For debug LOG */

        for(int i = 0; i < eph_cnt; i++) {
                int sat_ttl = eph_ttl[i].ttl_ms / 1000;
                if(sat_ttl > MIN_TTL) {
                        if(sat_ttl < ttl_sec)
                                ttl_sec = sat_ttl;

                        num_sat++;
                }

                if(len < (512 - 16))
                        len += sprintf(buf + len, "%02d: %d ", i, sat_ttl);
        }

        DBG_L2("Valid # EPH: %d/%d, TTL (sec) %d [%s]", num_sat, eph_cnt, 
               ttl_sec, buf);

        return num_sat;
}

static int get_dev_aid_ttl(void *hnd)
{
        CPP_FTRACE1();
	 UNUSED(hnd);

        int ttl_sec = -1, min_ttl, gps_ttl, glo_ttl;
        int num_sat =  0;
        min_ttl = gps_ttl = glo_ttl = 0;
        static struct aiding_status eph_ttl[32]; /* Ensuring not on stack */

	unsigned int gnss_select = find_dev_gnss_cfg();
        if(0 == gnss_select) {
                ERR_NM("Failed to get GNSS capabilities of device, returning");
		return -1;
        }

        for (int prn = 1; prn < (32 + 1); prn++) {
	        eph_ttl[prn - 1].obj_id = prn;
	}
 
        num_sat = ti_dev_loc_aiding_status(a_GPS_EPH, eph_ttl, 32);
        if(num_sat > 0) {
                num_sat = find_min_ttl(eph_ttl, num_sat, ttl_sec);
                if(num_sat > MIN_EPH) {
                        DBG_L1("UE has good # of GPS sat, min ttl %d", ttl_sec);
                        gps_ttl = ttl_sec;
            }
        }

	if(!(gnss_select & (1 << e_gnss_glo))) {
                DBG_L1("GPS only UE config, not evaluating TTL for GNSSes");
                min_ttl = gps_ttl; goto dev_aid_ttl_exit;
        }

        num_sat = ti_dev_loc_aiding_status(a_GLO_EPH, eph_ttl, 24);
        if(num_sat > 0) {
                num_sat = find_min_ttl(eph_ttl, num_sat, ttl_sec);
                if(num_sat > MIN_EPH) {
                        DBG_L1("UE has good # of GLO sat, min ttl %d", ttl_sec);
                        glo_ttl = ttl_sec;
                }
        }

        min_ttl = (glo_ttl > gps_ttl)? gps_ttl : glo_ttl;

dev_aid_ttl_exit:

        DBG_L1("Smallest TTL for GNSS EPH(s) on UE is %d secs", min_ttl);
 
        /* : Incorporate information from the assistance database */

        return min_ttl;
}

static int get_ue_aid_ttl(void *hnd)
{
	int ttl_sec = get_dev_aid_ttl(hnd);
	if(ttl_sec > 0) {
		DBG_L1("UE has sufficient aid in NVS to support POS / MSR");
		return ttl_sec;
	}

	ttl_sec = ext_assist_get_ttl();

        /* Use Only if GLO EPH was ever provisioned and GLO Tine was decoded */
        if(chk_glo_count && chk_glo_secs) {
                if(false == chk_is_ext_glo_valid()) {
                        /* Enable this check only if GLO was ever provisioned */
                        DBG_L1("CHK: resetting TTL to 0 as no GLO EPH are valid");
                        ttl_sec = 0;
                } else {
                        struct glo_eph *eph = chk_glo_eph + 0;
                        int diff = (DO_GLO_NT_SECS(eph->NT) +
                                    DO_GLO_Tk_SECS(eph->tk) +
                                    - chk_glo_secs - MIN_TTL);

                        if(diff < ttl_sec)
                                ttl_sec = diff;
                }
        }

	DBG_L1("UE %s external assist to support POS / MSR", (ttl_sec > 0) ? \
	       "has sufficient" : "needs");

	return ttl_sec;
}

static bool ue_is_msr_filter_gps_eph_reqd(void *hnd)
{
	UNUSED(hnd);
       return ext_assist_gps_eph_count()? true : false;
}

static int ue_msr_filter_gps_eph(void *hnd,
                          const struct gps_msr_result&  gps_result,
                          const struct gps_msr_info    *gps_msr)
{
	CPP_FTRACE1();
	UNUSED(hnd);
	int ret_val, num_eph;
	static struct gps_eph out_eph[MAX_SAT];

	ret_val = ext_assist_get_gps_eph_from_msr( gps_msr,gps_result.n_sat,
                                                 out_eph, MAX_SAT);
	if(ret_val <= 0) {
		ERR_NM("Error in filtering ext GPS EPH using UE MSR");
	} else if (ret_val > 0) {
		num_eph = ret_val;
		DBG_L1("Adding %d (#) MSR filtered GPS EPH into device", num_eph);
		return ti_dev_loc_aiding_add(a_GPS_EPH, out_eph, num_eph);
	}
	   
	return ret_val;

}

/* add for GNSS recovery ++*/
static int ue_err_recovery(void *hnd, enum dev_err_id err_id, unsigned int interval_secs)
{
	CPP_FTRACE1();
	int ret_val = -1;
	DBG_L1(" err_seq: configuring the devproxy for error recovery %d",err_id);

	if (err_id != gnss_no_err) {
		if(err_id == gnss_no_meas_rep_err){
			ret_val = ti_dev_setup_oper_param(gnss_meas_rep_recovery, NULL);
		} else if(err_id == gnss_no_resp_err){
			ret_val = ti_dev_setup_oper_param(gnss_no_resp_recovery, NULL);
				/* Run the error recovery starting from protocol select after the Hard reset*/
			if(ret_val!= -1)
				ret_val = ti_dev_setup_oper_param(err_recovery,NULL);
			
		} else if(err_id == gnss_hard_reset_err){
			ret_val = ti_dev_setup_oper_param(gnss_hard_reset_recovery, NULL);
			
			/* Run the error recovery starting from protocol select after the Hard reset*/
			if(ret_val!= -1)
			//	ret_val = ti_dev_setup_oper_param(err_recovery,NULL);
			ret_val = ti_dev_setup_oper_param(gnss_meas_rep_recovery, NULL);
		} else{
			ret_val = ti_dev_setup_oper_param(err_recovery,NULL);
		}
	}
	else
	{
		DBG_L1(" err_seq: no device error ");	
		return 0;
	}	

	if(ret_val != -1 ){	
	
	    if(cfg_dev_rpt(interval_secs) < 0) {
	            return -1;
	    }

		ti_dev_exec_action(dev_active);
	}
	else {
		return -1;
	}
	
	return 0;


}

// error trigger test code, to be removed 
static int trigger_test_err(void *hnd,unsigned char val)
{
	CPP_FTRACE1();

	switch (val)
	{
		case 1:
			DBG_L3("  err_seq: configuring devproxy to inject fatal error packet");
			ti_dev_setup_oper_param(fatal_err,NULL);
			break;

		case 2:
			DBG_L3("  err_seq: configuring devproxy to inject cpu exception error packet");
			ti_dev_setup_oper_param(exception_err,NULL);
			break;

		default:
			break;
	}

	return 0;

}
/* add for GNSS recovery --*/

static int dev_calib_ctl(const struct ue_tcxo_calib_desc& calib)
{
        CPP_FTRACE1();

        int ret_val = 0;
        struct dev_clk_calibrate clk_calib;

#define SET_REF_CLK(clk_calib, calib)                           \
        clk_calib.ref_clk_freq    = calib.data.clock.freq_hz;   \
        clk_calib.ref_clk_quality = calib.data.clock.quality;

        switch(calib.oper) {
			case ue_tcxo_calib_desc::e_calib_enbl:
				clk_calib.flags = DEV_CALIB_ENABLE;
			switch(calib.base) {
				case ue_tcxo_calib_desc::e_ref_clk: 
				clk_calib.flags  |= DEV_CALIB_REF_CLOCK; 
				SET_REF_CLK(clk_calib, calib);       
				break;

				case  ue_tcxo_calib_desc::e_ts_info:  clk_calib.flags |= DEV_CALIB_TIME_STAMP;  
				break;

				default:  
                                break;
			}

			switch(calib.mode) {
				case  ue_tcxo_calib_desc::e_one_shot: clk_calib.flags |= DEV_CALIB_SINGLE_SHOT;  break;
				case  ue_tcxo_calib_desc::e_periodic: clk_calib.flags |= DEV_CALIB_PERIODIC;     break;
				default: 
					break;
			}
			break;

			case  ue_tcxo_calib_desc::e_calib_dsbl:
				ret_val = -1;
			break;

			case  ue_tcxo_calib_desc::e_calib_undo:
				clk_calib.flags = DEV_CALIB_UNDO;
				//ret_val = -1;
			break;

			default:
				ret_val = -1;
			break;
	}
        if(0 == ret_val) {
                DBG_L3("Trying to exec  calibrate operation %u", calib.oper);
                ret_val = ti_dev_setup_oper_param(clk_calibrate, &clk_calib);
        }
        return ret_val;
}

 
static int 
oper_dev_param(void *hnd, enum ue_dev_param_id id, void *param_desc, int num)
{
        CPP_FTRACE1();
	UNUSED(hnd);
	UNUSED(num);

        int ret_val = -1;
	//struct ue_tcxo_calib_desc tcxo[1];

	id = e_ue_tcxo_calib;
        switch(id) {
		case e_ue_tcxo_calib:
			ret_val = dev_calib_ctl(*(struct ue_tcxo_calib_desc*)param_desc);
			break;

		default:
		break;
        }
        
        return ret_val;
}

static int ue_wake2idle(void *hnd)
{
        CPP_FTRACE1();
	UNUSED(hnd);

        DBG_L3("Getting device out of sleep to idle");

        ti_dev_exec_action(dev_wakeup);

        return 0;
}

static int ue_put2sleep(void *hnd)
{
        CPP_FTRACE1();
	UNUSED(hnd);
		
        DBG_L3("Putting device into deep sleep");
        
        ti_dev_exec_action(dev_dsleep);
        
        return 0;
}
static int ue_adap_init(const struct ti_gnss_ue_callbacks *ue_cbs)
{
	CPP_FTRACE1();

	struct ti_gnss_ue_callbacks *gnss_cbs = &ue_adap->gnss_ue_cbs;

	ext_assist_mod_init();	

	/* Save a reference to callbacks supported by A-GNSS Framework */
	memcpy(gnss_cbs, ue_cbs, sizeof(struct ti_gnss_ue_callbacks));

	ue_adap->gnss_ue_hnd = gnss_cbs->hnd;
	pthread_mutex_init(&ue_adap_queue_mtx, NULL);
	
	pthread_mutex_init(&recovery_mtx, NULL);

	sem_init(&ue_adap_queue_sem, 0, 0);

	DBG_L1(" [UE ADP] INIT OF THE ADAPTER CALLED");
	return 0;
}



static int ue_adap_exit(void)
{
	CPP_FTRACE1();
		DBG_L1(" [UE ADP] ue_adap_exit Entering");
	
 	if(ue_exit()){
			DBG_L1(" [UE ADP] EXIT FAILED");
			return 0;
		}
 	return 0;
}


static int dev_init(void *hnd)
{
	CPP_FTRACE1();
	
	if(init()){
		DBG_L1(" [UE ADP] dev_init");
		return -1;
	}

	UNUSED(hnd);
	return 0;
		
}

static int dev_plt(void *hnd, const struct plt_param& plt_test)
{
	CPP_FTRACE1();
	UNUSED(hnd);
	DBG_L1(" [UE ADP]dev_plt CALLED");
	if(ti_dev_plt((const struct dev_plt_param*)&plt_test)){
		DBG_L1(" [UE ADP] dev_plt");
		return -1;
	}


	return 0;
		
}
void proc_wt4meas_rep(int param)
{
	enum dev_err_id err_id;		
	unsigned int count;	
	struct ti_gnss_ue_callbacks *gnss_ue_cbs = &ue_adap->gnss_ue_cbs;	

	pthread_mutex_lock(&recovery_mtx);  	 
	count = ue_recovery_count;	
	pthread_mutex_unlock(&recovery_mtx);  		

	if(count < 2 ){		  
		count++;	  
		ue_err_recovery(gnss_ue_cbs->hnd,gnss_no_meas_rep_err, 1);	  
		gnss_ue_cbs->ue_recovery_ind(gnss_ue_cbs->hnd,gnss_no_meas_resp_ind,1 );			
	}else if(count < 3 ){
		count++;	  
		ue_err_recovery(gnss_ue_cbs->hnd,gnss_no_resp_err,1 );	  
		gnss_ue_cbs->ue_recovery_ind(gnss_ue_cbs->hnd,gnss_no_reset_resp_ind,1 );	
	} else {	
		gnss_ue_cbs->ue_recovery_ind(gnss_ue_cbs->hnd,gnss_hard_reset_ind,1);	
		ue_err_recovery(gnss_ue_cbs->hnd,gnss_hard_reset_err, 1);	
		count = 0 ; // Reset the counter here	
	}
	/*Reload the recovery timer once the recovery sequence is sen tto device*/		
	pthread_mutex_lock(&recovery_mtx);		
	ue_recovery_count = count;	
	_setup_meas_timer_Reload(AWAIT4MEAS_REP_SECS);	
	pthread_mutex_unlock(&recovery_mtx);					

	return ;
}


int gnss_ue_adap_ld_func(void *data) // name change
{
	CPP_FTRACE1();
	DBG_L1(" [UE ADP] gnss_ue_adap_ld_func entry");
	
        /*----------------------------------------------------------------------
         * Allocate object for Platform Application Adapter (pfm_app_adapter).
         *--------------------------------------------------------------------*/
        ue_adap = new gnss_ue_adapter;
        if(!ue_adap) {
		DBG_L1(" [UE ADP]Failed to allocate adapter for UE,  Exiting...");
                return -1;
        }

        DBG_L1(" [UE ADP] ADAPTER LOADER FUNCTION ");
		
	/*----------------------------------------------------------------------
	* Provision interface to interwork with A-GNSS framework (afw).
	*--------------------------------------------------------------------*/          
        struct ti_gnss_ue_interface *gnss_ue_ifc  =  NULL;

        gnss_ue_ifc = (struct ti_gnss_ue_interface*) data; 
        
        gnss_ue_ifc->hnd              = ue_adap; // To be returned by A-GNSS Framework.
        gnss_ue_ifc->init             = ue_adap_init;
        gnss_ue_ifc->exit             = ue_adap_exit;
        gnss_ue_ifc->cb_thread_func   = adap_queue2cfw_work;
		gnss_ue_ifc->cb_thread_arg1   = NULL;
        gnss_ue_ifc->loc_start        = loc_start;
        gnss_ue_ifc->loc_stop	      = loc_stop;
        gnss_ue_ifc->get_ue_loc_caps  = get_ue_loc_caps;
        gnss_ue_ifc->set_loc_interval = set_loc_interval;
        gnss_ue_ifc->set_intended_qop = set_intended_qop;
        gnss_ue_ifc->ue_add_assist    = ue_add_assist;
        gnss_ue_ifc->ue_del_aiding    = ue_del_aiding;
        gnss_ue_ifc->ue_get_aiding    = ue_get_aiding;
        gnss_ue_ifc->get_ue_aid_ttl   = get_ue_aid_ttl;
        gnss_ue_ifc->get_ue_req4assist= get_ue_req4assist;
        
        gnss_ue_ifc->assist2ue_done   = assist2ue_done;

        gnss_ue_ifc->ue_is_msr_filter_gps_eph_reqd = 
                ue_is_msr_filter_gps_eph_reqd;
        gnss_ue_ifc->ue_msr_filter_gps_eph = ue_msr_filter_gps_eph;
	
	/* add for GNSS recovery ++*/		
			gnss_ue_ifc->ue_err_recovery = ue_err_recovery;
			// error trigger test code, to be removed 
			gnss_ue_ifc->trigger_test_err = trigger_test_err;
	/* add for GNSS recovery --*/		
        gnss_ue_ifc->oper_ue_dev_param = oper_dev_param;
        gnss_ue_ifc->ue_wake2idle     = ue_wake2idle;
        gnss_ue_ifc->ue_put2sleep     = ue_put2sleep;

		gnss_ue_ifc->dev_init     = dev_init;
		gnss_ue_ifc->dev_plt     = dev_plt;
        
        return 0;
}


