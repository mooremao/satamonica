#include "gnss_utils.h"
#include "os_services.h"

/* 1. Change utc_secs to utc_now
   2. Fix up roll overs in code
   3. Check about GLO NOW 
*/

static unsigned char get_gps_wk_ro()
{
        return 1;

        /* Following is good only if UTC alongwith leap second is available */
        /* Leap sec info can be retrieved from NVS, if provisioned from dev */
	/* return (os_utc_secs() - UTC_AT_GPS_ORIGIN) / wk2secs(1024);      */
}

static unsigned char get_glo_day_ro()
{
        return 0;

        /* Following is good only if valid UTC is available in the system */
	/* return (os_utc_secs() - UTC_AT_GLO_ORIGIN) / day2secs(8192);   */
}

unsigned int gps_time_now(unsigned short nr_week, unsigned int wk_ro, unsigned int wk_secs)
{
	return wk2secs(nr_week + (1024 * wk_ro) + wk_secs);
}

unsigned int glo_time_now(unsigned short day, unsigned int tod)
{
	return day2secs(day - 1 + 8192 * get_glo_day_ro()) + 
		tod + GLO_START_RU_TIME;
}

static unsigned char calc_glo_N4_param(unsigned int glo_now)
{
	unsigned int elapsed = glo_now - GLO_START_RU_TIME; 
	unsigned int calc_N4 = elapsed / MAX_SECS_4Y_BLOCK + 1;

	return (calc_N4 & 0xFF);
}

unsigned char get_glo_N4_param(unsigned int glo_now)
{
	return (glo_now > UTC_AT_GLO_ORIGIN)? calc_glo_N4_param(glo_now) : 0;  
}

unsigned char utc2glo_N4_param(unsigned int utc_now)
{
	return (utc_now > UTC_AT_GLO_ORIGIN)? 
		calc_glo_N4_param(utc2glo_time(utc_now)) : 0;
}

static unsigned short calc_glo_NT_param(unsigned int glo_now)
{
	unsigned int elapsed = glo_now - GLO_START_RU_TIME;
	unsigned int calc_N4 = elapsed / MAX_SECS_4Y_BLOCK + 1;
	unsigned int calc_NT = elapsed - (calc_N4 - 1) * MAX_SECS_4Y_BLOCK;

	calc_NT = calc_NT / MAX_SECS_IN_A_DAY; // Convert to days

	return ((calc_NT + 1) & 0xFFFF); // NT as per GLONASS ICD is 1 based.
}

unsigned short utc2glo_NT_param(unsigned int utc_now)
{
	return (utc_now > UTC_AT_GLO_ORIGIN)? 
		calc_glo_NT_param(utc2glo_time(utc_now)) : 0;
}

unsigned short get_glo_NT_param(unsigned int glo_now)
{
	return (glo_now > GLO_START_RU_TIME)? calc_glo_NT_param(glo_now) : 0;
}

static unsigned short calc_glo_tk_param(unsigned int glo_now)
{
	unsigned int elapsed = glo_now - GLO_START_RU_TIME;
	unsigned int tk      = elapsed % MAX_SECS_IN_A_DAY;
	unsigned int hrs     = tk / MAX_SECS_IN_1HOUR;
	unsigned int min     = (tk % MAX_SECS_IN_1HOUR) / MAX_SECS_IN_A_MIN;
	unsigned int sec     = (((tk % MAX_SECS_IN_1HOUR) % MAX_SECS_IN_A_MIN) >= 30) ? 1 : 0;

	return ((hrs & 0x0000001F) << 7) | ((min & 0x0000003F) << 1) | (sec & 0x00000001);
}

unsigned short utc2glo_tk_param(unsigned int utc_now)
{
	return (utc_now > UTC_AT_GLO_ORIGIN) ? 
		calc_glo_tk_param(utc2glo_time(utc_now)) : 0;
}

unsigned short get_glo_tk_param(unsigned int glo_now)
{
	return (glo_now > GLO_START_RU_TIME) ? calc_glo_tk_param(glo_now) : 0;
}

static unsigned char 
Mn_Dn_map_no_leap [] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

static unsigned char
Mn_Dn_map_leap_yr [] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

struct MDS1Y_offset secs2MDS1Y(unsigned int secs, unsigned char leap_yr)
{
	unsigned char i = 0, *Mn_Dn_map;
	struct MDS1Y_offset mds1y;
	unsigned short days;

	days = secs / MAX_SECS_IN_A_DAY;
	secs = secs - days * MAX_SECS_IN_A_DAY;

	Mn_Dn_map = leap_yr ? Mn_Dn_map_leap_yr : Mn_Dn_map_no_leap;

	for(i = 0; i < 12; i++) {
		if(days < Mn_Dn_map[i])
			break;

		days -= Mn_Dn_map[i];
	}

	mds1y.Mn =  i;
	mds1y.Dn = days;
	mds1y.ss = secs;

	return mds1y;
}

unsigned char days2months_1Y(unsigned short days, unsigned char leap_yr)
{
	unsigned char i = 0, *Mn_Dn_map;

	Mn_Dn_map = leap_yr ? Mn_Dn_map_leap_yr : Mn_Dn_map_no_leap;

	for(i = 0; i < 12; i++) {
		if(days < Mn_Dn_map[i])
			break;

		days -= Mn_Dn_map[i];
	}

	return i + 1;
}

unsigned short months2days_1Y(unsigned char months, unsigned char leap_yr)
{
	unsigned char i = 0, *Mn_Dn_map;
	unsigned short days = 0;

	Mn_Dn_map = leap_yr ? Mn_Dn_map_leap_yr : Mn_Dn_map_no_leap;

	for(i = 0; i < months; i++) {
		days += Mn_Dn_map[i];
	}

	return days; 
}

struct YMDS_offset secs2YMDS(unsigned int secs)
{
	unsigned char  Y4, Yr;
	struct YMDS_offset ymds;
	struct MDS1Y_offset mds1y;

	Y4   = secs / MAX_SECS_4Y_BLOCK;

	secs = secs - Y4 * MAX_SECS_4Y_BLOCK;
	Yr   = secs / MAX_SECS_IN_365DY;

	secs = secs - Yr * MAX_SECS_IN_365DY;

	mds1y = secs2MDS1Y(secs, Y4 ? !Yr : 0); 

	ymds.Yn = Y4 * 4 + Yr;
	ymds.Mn = mds1y.Mn;
	ymds.Dn = mds1y.Dn;
	ymds.ss = mds1y.ss;

	return ymds;
}

unsigned int YMDS2secs(struct YMDS_offset ymds)
{
	unsigned char  Y4, Yr;
	unsigned short Dn;
	unsigned int secs = 0;

	Y4 = ymds.Yn / 4;
	Yr = ymds.Yn - Y4 * 4;
	Dn = ymds.Dn + months2days_1Y(ymds.Mn, Y4 ? !Yr : 0); 

	secs += Y4 * MAX_SECS_4Y_BLOCK + Yr * MAX_SECS_IN_365DY;
	secs += Dn * MAX_SECS_IN_A_DAY;
	secs += ymds.ss;

	return secs;
}

struct week_offset secs2week(unsigned int secs)
{
	struct week_offset wkss;

	wkss.Wn = secs / MAX_SECS_IN_1WEEK;
	wkss.ss = secs - (wkss.Wn * MAX_SECS_IN_1WEEK);

	return wkss;
}






