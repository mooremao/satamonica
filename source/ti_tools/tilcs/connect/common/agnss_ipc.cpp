/* 
 * agnss_ipc.h
 *
 * this file describe the IPC for A_GNSS framework 
 *
 * Copyright (C) {YEAR} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 */

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "Ftrace.h"
#include "agnss_ipc.h"
#include <debug.h>
#ifdef ANDROID
#include <utils/Log.h>
#include <errno.h>
#endif

int os_udp_sock_init(const char* sock_path)
{
	CPP_FTRACE3();
	int ret_val;

	int sock_fd;
	socklen_t sock_len;
	struct sockaddr_un sock_addr;
	memset(&sock_addr,0,sizeof(sock_addr));

	/* Create a Unix Domain Socket */
	sock_fd = socket(AF_UNIX, SOCK_DGRAM,0);
//	if ((sock_fd = socket(AF_UNIX, SOCK_DGRAM,0)) < 0) {
	if (sock_fd  == -1) {
		ERR_NM("Failed to create socket\n");
		return -1;
	}
	
	sock_addr.sun_family = AF_UNIX; /* Internet IP */
	//sock_len = sizeof(sock_addr);
	if((strlen(sock_path)+1) < 108){
		strcpy(sock_addr.sun_path, sock_path);
	}else{
		close(sock_fd);
		return -1;
	}
	
	//strncpy(sock_addr.sun_path, sock_path, strlen(sock_path)+1);
	unlink(sock_addr.sun_path);
	sock_len = strlen(sock_addr.sun_path) + sizeof(sock_addr.sun_family);

	ret_val = bind(sock_fd, (struct sockaddr *)&sock_addr, sock_len);
//	if(bind(sock_fd, (struct sockaddr *)&sock_addr, sock_len) < 0) {
	if(ret_val < 0){
		ERR_NM("Failed to bind server socket on path %s\n", sock_path);
		close(sock_fd);
		return -1;
	}

	if(chmod(sock_path, 0777) < 0) {
		ERR_NM("Failed to change permission socket on path %s\n", sock_path);
	}

        DBG_L3("Sock created ID: %d & bound on path %s\n", sock_fd, sock_path);

	return sock_fd;
}

int os_udp_sock_exit(int fd)
{
	CPP_FTRACE3();

	if(close(fd) < 0){
		ERR_NM("ERROR closing\n");
		return -1;
	}

	return 0;
}


int os_udp_send(int fd, const char *dst_path, char *data, int len)
{
	CPP_FTRACE3();

	struct sockaddr_un sock_addr;
	int length = 0;
	memset(&sock_addr,0,sizeof(sock_addr));

	sock_addr.sun_family = AF_UNIX; /* Unix domain socket */
	if((strlen(dst_path)+1) < 108)
		strcpy(sock_addr.sun_path, dst_path);
	else
		return -1;
	//strncpy(sock_addr.sun_path, dst_path, strlen(dst_path)+1);
        
        DBG_L3(" os_udp_send socket bigens fd: %d len: %d dst_path: %s", fd, len, dst_path); 
        
	/* send over unix path */
	if((length = sendto(fd, data, len, 0, (struct sockaddr *)       
                            &sock_addr, sizeof(sock_addr))) < 0)  {
		ERR_NM("Failed to send message: length %d", length);
		return -1;
	}
        
	return length;
        
}

int os_udp_recv(int fd, char *data, int len)
{
	CPP_FTRACE3();
        
	struct sockaddr_un sock_dst_addr;
	int length=0;
	socklen_t addr_len = sizeof(sock_dst_addr);
        
	/* Receive over Unix path */
	if((length = recvfrom(fd, data, len, 0, (struct sockaddr*)&sock_dst_addr,
                               &addr_len)) < 0)  {
		ERR_NM("Failed to receive message: length %d\n", length);
		return -1;
	}
        
	return length;
}



