/*
 * a-gnss_xact.c
 *
 * Add Description
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
 */



/*********************************
 * 
 * a) Add time out for all make transactions
 * b) Add err mesg for all proc transactions
 *******************************/

#include <pthread.h>
#include <string.h>

#include "gnss_lcs_common.h"
#include "a-gnss_comm.h"
#include "Gnss_Utils.h"
#include "a-gnss_xact.h"
#include "debug.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include "Ftrace.h"

#define UNUSED(x) ((void)(&x))
#define GPS_MAX_SVS 32  /*  Remove */

static unsigned int GNFN_ID_LOC_START             = 0x00000001;
static unsigned int GNFN_ID_LOC_STOP              = 0x00000002;
static unsigned int GNFN_ID_GET_UE_LOC_CAPS       = 0x00000003;
static unsigned int GNFN_ID_GET_UE_AGNSS_CAPS     = 0x00000004;
static unsigned int GNFN_ID_SET_LOC_INTERVAL      = 0x00000005;
static unsigned int GNFN_ID_SET_INTENDED_QOP      = 0x00000006;
static unsigned int GNFN_ID_UE_ADD_ASSIST         = 0x00000007; 
static unsigned int GNFN_ID_UE_DEL_AIDING         = 0x00000008; 
static unsigned int GNFN_ID_UE_GET_AIDING         = 0x00000009; 
static unsigned int GNFN_ID_GET_UE_REQ4ASSIST     = 0x0000000A; 
static unsigned int GNFN_ID_NW_LOC_RESULTS        = 0x0000000B; 
static unsigned int GNFN_ID_UE_NEED_ASSIST        = 0x0000000C;
static unsigned int GNFN_ID_UE_DECODED_AID        = 0x0000000D;
static unsigned int GNFN_ID_UE_LOC_RESULTS        = 0x0000000E; 
static unsigned int GNFN_ID_UE_NMEA_REPORT        = 0x0000000F;
static unsigned int GNFN_ID_UE_SV_STATUS          = 0x00000010;
static unsigned int GNFN_ID_SET_LOC_INSTRUCT      = 0x00000011;
static unsigned int GNFN_ID_SET_RPT_CRITERIA      = 0x00000012;
static unsigned int GNFN_ID_ASSIST2UE_DONE        = 0x00000013;
static unsigned int GNFN_ID_UE_DEV_OPER_PARAM     = 0x00000014;
static unsigned int GNFN_ID_DECL_DEV_PARAM        = 0x00000016;
static unsigned int GNFN_ID_UE_SERVICE_CTL        = 0x00000017;
static unsigned int GNFN_ID_DEV_INIT        	  = 0x00000018;
static unsigned int GNFN_ID_UE_CLEANUP        	  = 0x00000019;
static unsigned int GNFN_ID_DEV_PLT        	  	  = 0x0000001A;
static unsigned int GNFN_ID_UE_CW_TEST_RESULTS    = 0x0000001B;
static unsigned int GNFN_ID_UE_RECOVERY_IND		  = 0x0000001C;



static unsigned int GNFN_ID_INVALID               = 0xFFFFFFFF;

struct agnss_lcs_callbacks{

	/* List out all callbacks */
        ti_gnfn_loc_start             loc_start;
	ti_gnfn_loc_stop              loc_stop;
        ti_gnfn_get_ue_loc_caps       get_ue_loc_caps;
        ti_gnfn_get_ue_agnss_caps     get_ue_agnss_caps;
	//ti_gnfn_set_loc_interval      set_loc_interval;
        //ti_gnfn_set_intended_qop      set_intended_qop;
        ti_gnfn_set_loc_instruct      set_loc_instruct;
        ti_gnfn_set_rpt_criteria      set_rpt_criteria;
        ti_gnfn_ue_add_assist         ue_add_assist;
        ti_gnfn_ue_del_aiding         ue_del_aiding;
        ti_gnfn_ue_get_aiding         ue_get_aiding;
        ti_gnfn_assist2ue_done        assist2ue_done;
        //ti_gnfn_get_ue_req4assist     get_ue_req4assist;
        ti_gnfn_nw_loc_results        nw_loc_results;
        ti_gnfn_ue_need_assist        ue_need_assist;
        ti_gnfn_ue_decoded_aid        ue_decoded_aid;
        ti_gnfn_ue_loc_results        ue_loc_results;
        ti_gnfn_ue_nmea_report        ue_nmea_report;
        ti_gnfn_ue_aux_info4sv        ue_aux_info4sv; 
        ti_gnfn_ue_dev_oper_param     oper_ue_dev_param;
        ti_gnfn_decl_dev_param        decl_dev_param;
		ti_gnfn_ue_service_ctl        ue_service_ctl;
		ti_gnfn_devinit			dev_init;
		ti_gnfn_cleanup        ue_cleanup;
		ti_gnfn_devplt				  dev_plt;
		ti_gnfn_ue_cw_test_results	  ue_cw_test_results;
		ti_gnfn_ue_recovery_ind		  ue_recovery_ind;	    
};

static const unsigned int xact_app_bit_ofs = 27;
static const unsigned int xact_app_sel = 0x1F << xact_app_bit_ofs;

#define USER_ID2XACT_ID_BASE(user_id) (user_id << xact_app_bit_ofs)

static inline unsigned int xact_id_inc(unsigned int xact_id)
{
        return ((xact_id & xact_app_sel) | ((xact_id + 1) & ~xact_app_sel));
}

struct agnss_xact_node {

	void                        *comm_user;  /* HND returned by Comm Module */
	struct agnss_lcs_callbacks   agnss_cbs;  /* Callbacks  provided by user */

	pthread_mutex_t              node_lock;  /* Ensures unmutable operation */
        
	unsigned int                 xact_id;    /* Last transaction ID by user */

	unsigned int 		term_flag;

	/* Constructors */
	agnss_xact_node(const struct lc_assist_pvdr_ops&, unsigned short user_id);
	agnss_xact_node(const struct lc_assist_user_ops&, unsigned short user_id);
	agnss_xact_node(const struct agnss_lcs_pvdr_ops&, unsigned short user_id);
	agnss_xact_node(const struct agnss_lcs_user_ops&, unsigned short user_id);
};

agnss_xact_node :: agnss_xact_node(const struct agnss_lcs_pvdr_ops& ops,
                                   unsigned short user_id)
        :comm_user(NULL), xact_id(USER_ID2XACT_ID_BASE(user_id))
{
        CPP_FTRACE3();
        
        agnss_cbs.loc_start             = ops.loc_start;
        agnss_cbs.loc_stop              = ops.loc_stop;
        agnss_cbs.get_ue_loc_caps       = ops.get_ue_loc_caps;
        agnss_cbs.get_ue_agnss_caps     = ops.get_ue_agnss_caps;
        agnss_cbs.set_rpt_criteria	= ops.set_rpt_criteria;
        agnss_cbs.set_loc_instruct	= ops.set_loc_instruct;
        agnss_cbs.ue_add_assist         = ops.ue_add_assist;
        agnss_cbs.ue_del_aiding         = ops.ue_del_aiding;
        agnss_cbs.ue_get_aiding         = ops.ue_get_aiding;
        agnss_cbs.assist2ue_done	= ops.assist2ue_done;
        //agnss_cbs.get_ue_req4assist     = ops.get_ue_req4assist;
        agnss_cbs.nw_loc_results        = ops.nw_loc_results;
        agnss_cbs.oper_ue_dev_param     = ops.oper_ue_dev_param;
        agnss_cbs.ue_need_assist        = NULL; 
        agnss_cbs.ue_decoded_aid        = NULL; 
        agnss_cbs.ue_loc_results        = NULL; 
        agnss_cbs.ue_nmea_report        = NULL; 
        agnss_cbs.ue_aux_info4sv        = NULL;
        agnss_cbs.decl_dev_param        = NULL;	
		agnss_cbs.ue_service_ctl        = NULL;
		agnss_cbs.dev_init		= ops.dev_init;
		agnss_cbs.ue_cleanup   = ops.ue_cleanup;
		agnss_cbs.dev_plt		= ops.dev_plt;
		
		agnss_cbs.ue_cw_test_results= NULL;
		agnss_cbs.ue_recovery_ind =NULL;
        pthread_mutex_init(&node_lock, NULL);
		term_flag = 0;
}

agnss_xact_node :: agnss_xact_node(const struct agnss_lcs_user_ops& ops,
                                   unsigned short user_id)
        : comm_user(NULL), xact_id(USER_ID2XACT_ID_BASE(user_id))
{
        CPP_FTRACE3();
        
        agnss_cbs.loc_start             = NULL;
        agnss_cbs.loc_stop              = NULL;
        agnss_cbs.get_ue_loc_caps       = NULL;
        agnss_cbs.get_ue_agnss_caps     = NULL;
        //	agnss_cbs.set_loc_interval      = NULL;
        //agnss_cbs.set_intended_qop      = NULL;
        agnss_cbs.set_rpt_criteria	= NULL;
        agnss_cbs.set_loc_instruct	= NULL;	
        agnss_cbs.ue_add_assist         = NULL;
        agnss_cbs.ue_del_aiding         = NULL;
        agnss_cbs.ue_get_aiding         = NULL;
        agnss_cbs.assist2ue_done        = NULL;
        //agnss_cbs.get_ue_req4assist     = NULL;
        agnss_cbs.nw_loc_results        = NULL;
        agnss_cbs.oper_ue_dev_param     = NULL;
        agnss_cbs.ue_need_assist        = ops.ue_need_assist;
        agnss_cbs.ue_decoded_aid        = ops.ue_decoded_aid;
        agnss_cbs.ue_loc_results        = ops.ue_loc_results;
        agnss_cbs.ue_nmea_report        = ops.ue_nmea_report;
        agnss_cbs.ue_aux_info4sv 	= ops.ue_aux_info4sv;
        agnss_cbs.decl_dev_param = ops.decl_dev_param;
	agnss_cbs.ue_service_ctl        = ops.ue_service_ctl;
		agnss_cbs.ue_cw_test_results	= ops.cw_test_results;
        agnss_cbs.ue_recovery_ind     = ops.ue_recovery_ind;
        pthread_mutex_init(&node_lock, NULL);
				term_flag = 0;
}

agnss_xact_node :: agnss_xact_node(const struct lc_assist_user_ops& ops,
                                   unsigned short user_id)
        : comm_user(NULL), xact_id(USER_ID2XACT_ID_BASE(user_id))
{
	CPP_FTRACE3();

	agnss_cbs.loc_start             = NULL;
	agnss_cbs.loc_stop              = NULL;
	agnss_cbs.get_ue_loc_caps       = NULL;
	agnss_cbs.get_ue_agnss_caps     = NULL;
        //	agnss_cbs.set_loc_interval      = NULL;
        //	agnss_cbs.set_intended_qop      = NULL;
	agnss_cbs.set_rpt_criteria	= NULL;
	agnss_cbs.set_loc_instruct	= NULL;		
	agnss_cbs.ue_add_assist         = ops.ue_add_assist;
	agnss_cbs.ue_del_aiding         = ops.ue_del_aiding;
	agnss_cbs.ue_get_aiding         = ops.ue_get_aiding;
	agnss_cbs.assist2ue_done        = ops.assist2ue_done;
	agnss_cbs.nw_loc_results        = NULL;
	agnss_cbs.oper_ue_dev_param     = NULL;
	agnss_cbs.ue_need_assist        = NULL;
	agnss_cbs.ue_decoded_aid        = NULL;
	agnss_cbs.ue_loc_results        = NULL;
	agnss_cbs.ue_nmea_report        = NULL;
	agnss_cbs.ue_aux_info4sv        = NULL;
	agnss_cbs.decl_dev_param        = NULL;
	agnss_cbs.ue_service_ctl        = NULL;
	agnss_cbs.ue_cw_test_results		= NULL;
	agnss_cbs.ue_recovery_ind       =NULL;
	pthread_mutex_init(&node_lock, NULL); 
			term_flag = 0;
}

agnss_xact_node :: agnss_xact_node(const struct lc_assist_pvdr_ops& ops,
                                   unsigned short user_id)
        : comm_user(NULL), xact_id(USER_ID2XACT_ID_BASE(user_id))
{
	CPP_FTRACE3();

	agnss_cbs.loc_start             = NULL;
	agnss_cbs.loc_stop              = NULL;
	agnss_cbs.get_ue_loc_caps       = NULL;
	agnss_cbs.get_ue_agnss_caps     = NULL;
        //	agnss_cbs.set_loc_interval      = NULL;
        //	agnss_cbs.set_intended_qop      = NULL;
	agnss_cbs.set_rpt_criteria	= NULL;
	agnss_cbs.set_loc_instruct	= NULL;		
	agnss_cbs.ue_add_assist         = NULL;
	agnss_cbs.ue_del_aiding         = NULL;
	agnss_cbs.ue_get_aiding         = NULL;
	agnss_cbs.ue_add_assist         = NULL;
	agnss_cbs.ue_del_aiding         = NULL;
	agnss_cbs.ue_get_aiding         = NULL;
	agnss_cbs.assist2ue_done        = NULL;
	agnss_cbs.nw_loc_results        = NULL;
	agnss_cbs.oper_ue_dev_param     = NULL;
	agnss_cbs.ue_need_assist        = ops.ue_need_assist;
	agnss_cbs.ue_decoded_aid        = ops.ue_decoded_aid;
	agnss_cbs.ue_loc_results        = ops.ue_loc_results;
	agnss_cbs.ue_nmea_report        = NULL;
	agnss_cbs.ue_aux_info4sv	= NULL;
	agnss_cbs.decl_dev_param        = ops.decl_dev_param;
	agnss_cbs.ue_service_ctl        = ops.ue_service_ctl;
	agnss_cbs.ue_cw_test_results		= ops.cw_test_results;
    agnss_cbs.ue_recovery_ind		=ops.ue_recovery_ind;
	pthread_mutex_init(&node_lock, NULL);
			term_flag = 0;
}

/*------------------------------------------------------------------------------
 * Utilities for this file 
 *----------------------------------------------------------------------------*/
static struct agnss_msg *alloc_msg2send(struct agnss_xact_node& xact_node,
                                        unsigned int func_id)
{
	CPP_FTRACE3();

	struct agnss_msg       *o_msg     = agnss_msg_mk_new(func_id);
	if(!o_msg)
		goto alloc_msg2send_exit1;
        
	pthread_mutex_lock(&xact_node.node_lock);
	o_msg->head.xact_id = xact_node.xact_id = xact_id_inc(xact_node.xact_id);
	pthread_mutex_unlock(&xact_node.node_lock);
        
 alloc_msg2send_exit1:
	return o_msg;
}

static int free_msgs_n_done(struct agnss_msg *o_msg, struct agnss_msg *i_msg,
                            int ret_val)
{
	CPP_FTRACE3();

	if(o_msg)
		agnss_msg_delete(o_msg);
        
	if(i_msg)
		agnss_msg_delete(i_msg);
        
	return ret_val;
}

static int
send_n_free_msgs(struct agnss_xact_node& xact_node, struct agnss_msg *o_msg,
                 struct agnss_msg *i_msg, int ret_val)
{
	CPP_FTRACE3();

	if(o_msg) {
		ret_val= agnss_comm_send_msg(xact_node.comm_user, o_msg, false);
		agnss_msg_delete(o_msg);
	}

	if(i_msg)
		agnss_msg_delete(i_msg);

	return ret_val;
}


static
struct agnss_msg *send_n_recv_msgs(struct agnss_xact_node& xact_node, 
                                   struct agnss_msg *o_msg, 
                                   unsigned int xact_id)
{
	CPP_FTRACE3();

	if(agnss_comm_send_msg(xact_node.comm_user, o_msg, true) < 0)
		return NULL;
        
	return agnss_comm_wait_msg(xact_node.comm_user, xact_id);
}

template <typename DATA>
static int push_data_in2msg(struct agnss_msg *msg, const DATA *data, int n_elem)
{
	CPP_FTRACE3();

	unsigned int data_sz = sizeof(DATA) *  n_elem;
	if((msg->head.max_buf - msg->head.buf_ofs) < data_sz) 
		return -1;
        
	memcpy(msg->buff + msg->head.buf_ofs, data, data_sz);
	msg->head.data_sz += data_sz;
	msg->head.buf_ofs += data_sz;
        
	return data_sz;
}

template <typename DATA>
static int pullout_data_msg(struct agnss_msg *msg, DATA *data, int n_elem)
{
	CPP_FTRACE3();

	unsigned int data_sz = sizeof(DATA) * n_elem;
	if(data_sz > msg->head.data_sz)
		return -1;
        
	memcpy(data, (void *)(msg->buff + (msg->head.data_sz - msg->head.buf_ofs)), data_sz);
	msg->head.buf_ofs -= data_sz;
	return data_sz;
}

/* Macros to invoke callback functiosn with ease */
#define INLINE_CALL_CB_FN(cb_fn, rv, ...)           \
        if(cb_fn)                                   \
                rv = cb_fn(__VA_ARGS__);




/*------------------------------------------------------------------------------
 * GANSS Assistance request
 *----------------------------------------------------------------------------*/
static int
packin_ganss_assist_req(struct agnss_msg *o_msg, 
                        const struct ganss_assist_req  *ganss_req)
        
{
	CPP_FTRACE3();
	
	if(NULL == ganss_req) {
                DBG_L3("Failed, working with NULL mem pointer, returning");
                return 0;
        }

	if(push_data_in2msg(o_msg, ganss_req, 1) < 0)
		return -1;
	return 0;
}

static int
unpack_ganss_assist_req(struct agnss_msg *i_msg, 
                        struct ganss_assist_req  *ganss_req)
{
	CPP_FTRACE3();

	if(NULL == ganss_req) {
		DBG_L3("Failed, working with NULL mem pointer, returning");
        return 0;
    }
	
  	if(pullout_data_msg(i_msg, ganss_req, 1) < 0)
		return -1;
	return 0;

}


/*------------------------------------------------------------------------------
 * GPS Assistance request
 *----------------------------------------------------------------------------*/
static int
packin_gps_assist_req(struct agnss_msg *o_msg, 
                      const struct gps_assist_req *gps_req)
        
{
	CPP_FTRACE3();

	if(NULL == gps_req) {
		DBG_L3("Failed, working with NULL mem pointer, returning");
        return 0;
    }

	if(push_data_in2msg(o_msg, gps_req, 1) < 0)
		return -1; 


	return 0;

}

static int
unpack_gps_assist_req(struct agnss_msg *i_msg, 
                      struct gps_assist_req *gps_req)
{
	CPP_FTRACE3();

	if(NULL == gps_req) {
		DBG_L3("Failed, working with NULL mem pointer, returning");
        return 0;
    }

	if(pullout_data_msg(i_msg, gps_req,   1) < 0)
		return -1;

	return 0;

}

/*------------------------------------------------------------------------------
 * need assist Ref
 *----------------------------------------------------------------------------*/
static int packin_assist_usr_info(struct agnss_msg *o_msg, 
                      const struct assist_reference *assist_usr)
{
	CPP_FTRACE3();

	if(NULL == assist_usr) {
		DBG_L3("Failed, working with NULL mem pointer, returning");
        return 0;
    }

	if(push_data_in2msg(o_msg, assist_usr, 1) < 0)
		return -1; 

	return 0;
}

static int unpack_assist_usr_info(struct agnss_msg *i_msg, 
                      struct assist_reference *assist_usr)
{
	CPP_FTRACE3();

	if(NULL == assist_usr) {
		DBG_L3("Failed, working with NULL mem pointer, returning");
        return 0;
    }

	if(pullout_data_msg(i_msg, assist_usr,   1) < 0)
		return -1;

	return 0;
}



/*------------------------------------------------------------------------------
 * Get UE LOC Capabilities
 *----------------------------------------------------------------------------*/


static int unpack_get_ue_loc_caps(struct agnss_msg *i_msg, 
                                  unsigned int& gps_caps_bits,
                                  struct ganss_ue_lcs_caps *ganss_caps)
{
	CPP_FTRACE3();
        
	unsigned char *n_ganss = &ganss_caps->n_ganss;

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &gps_caps_bits,  1) < 0) ||
	   (pullout_data_msg(i_msg, n_ganss,	     1) < 0)) {
                ERR_NM("Failed to work w/ GPS &/or meta data, returinig...");
		return -1;
        }

	if(*n_ganss)
		if(pullout_data_msg(i_msg, ganss_caps->lcs_caps, *n_ganss) < 0) {
                        ERR_NM("Failed to work w/ GANSS caps data, returning");
			return -1;
                }
        
	return 0;
}

static int packin_get_ue_loc_caps(struct agnss_msg *o_msg, 
                                  unsigned int& gps_caps_bits,
                                  struct ganss_ue_lcs_caps *ganss_caps)
{
	CPP_FTRACE3();
        
	unsigned char n_ganss = ganss_caps->n_ganss;
        
	if((push_data_in2msg(o_msg, &gps_caps_bits, 1) < 0) ||
	   (push_data_in2msg(o_msg, &n_ganss,       1) < 0)) {
                ERR_NM("Failed to work w/ GPS &/or meta data, returinig...");
		return -1;
        }
        
	if(n_ganss)
		if(push_data_in2msg(o_msg, ganss_caps->lcs_caps,  n_ganss) < 0) {
                        ERR_NM("Failed to work w/ GANSS caps data, returning");
			return -1;
                }
        
	return 0;
}

static int make_xact_get_ue_loc_caps(void *hnd, unsigned int& gps_caps_bits,
                                     struct ganss_ue_lcs_caps *ganss_caps) 
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;        
	struct agnss_msg *o_msg, *i_msg = NULL;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_GET_UE_LOC_CAPS);
	if(NULL == o_msg)
                goto make_xact_get_ue_loc_caps_exit;

	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg) 
                goto make_xact_get_ue_loc_caps_exit;
        
	/* Un-pack information from i_msg */
        if(unpack_get_ue_loc_caps(i_msg, gps_caps_bits, ganss_caps < 0))
                goto make_xact_get_ue_loc_caps_exit;

        ret_val = i_msg->head.ret_val;
        
 make_xact_get_ue_loc_caps_exit: 
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_get_ue_loc_caps(void *hnd, struct agnss_msg *i_msg, 
                                        struct ganss_ue_lcs_caps *ganss_caps)
{
	CPP_FTRACE3();

        unsigned int gps_caps_bits = 0;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
	struct agnss_msg *o_msg;
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_GET_UE_LOC_CAPS);
	if(NULL == o_msg)
                goto do_proc_xact_get_ue_loc_caps_exit; 
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.get_ue_loc_caps, 
                          ret_val, hnd, gps_caps_bits, ganss_caps);
                          
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
	if(packin_get_ue_loc_caps(o_msg, gps_caps_bits, ganss_caps) < 0) {
                /* Problem: send error in return value to remote processing */
                ret_val = -1;
        }

 do_proc_xact_get_ue_loc_caps_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_get_ue_loc_caps(void *hnd, struct agnss_msg *i_msg)
{
        CPP_FTRACE3();
        
        int ret_val = -1;
        struct agnss_xact_node *xact_node    = (struct agnss_xact_node*) hnd;
        struct ganss_ue_lcs_caps *ganss_caps = new struct ganss_ue_lcs_caps;
        if(NULL == ganss_caps) {
                ERR_NM("Failed: allocate mem for ganss_ue_lcs_caps, returning");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }
        
        ret_val = do_proc_xact_get_ue_loc_caps(hnd, i_msg, ganss_caps);

        delete ganss_caps;
        
	return ret_val;
}

/*------------------------------------------------------------------------------
 * Get UE GANSS Capabilities
 *----------------------------------------------------------------------------*/
#if  0
static int unpack_get_ue_agnss_caps(struct agnss_msg *i_msg, 
                                    struct ganss_ue_assist_caps *ganss_caps)
{
	CPP_FTRACE3();

	unsigned int *n_ganss = &ganss_caps->n_ganss;
        
	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &ganss_caps->common_bits, 1) < 0) ||
	   (pullout_data_msg(i_msg, n_ganss,		      1) < 0)) {
                ERR_NM("Failed to work w/ GANSS meta data, returning....");
		return -1;
        }
        
	if(*n_ganss)
		if(pullout_data_msg(i_msg, ganss_caps->ganss_caps, *n_ganss) < 0) {
                        ERR_NM("Failed to work w/ GANSS data, returning....");
			return -1;
                }
        
	return 0;
        
}
#endif

static int unpack_get_ue_agnss_caps(struct agnss_msg *i_msg,
                                    unsigned int& gps_caps_bits,
                                    struct ganss_ue_assist_caps *ganss_caps)
{
	CPP_FTRACE3();
        
	unsigned int *n_ganss = &ganss_caps->n_ganss;

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &gps_caps_bits,           1) < 0) ||
	   (pullout_data_msg(i_msg, &ganss_caps->common_bits, 1) < 0) ||
	   (pullout_data_msg(i_msg, n_ganss,		      1) < 0)) {
                ERR_NM("Failed to work w/ GANSS meta data, returning....");
		return -1;
        }
        
	if(*n_ganss)
		if(pullout_data_msg(i_msg, ganss_caps->ganss_caps, 
                                    *n_ganss) < 0) {
                        ERR_NM("Failed to work w/ GANSS data, returning....");
			return -1;
                }
        
	return 0;
}

static int packin_get_ue_agnss_caps(struct agnss_msg *o_msg, 
                                    unsigned int& gps_caps_bits,
                                    struct ganss_ue_assist_caps *ganss_caps)
{
	CPP_FTRACE3();
	        
	if((push_data_in2msg(o_msg, &gps_caps_bits, 	      1) < 0) ||
	   (push_data_in2msg(o_msg, &ganss_caps->common_bits, 1) < 0) ||
	   (push_data_in2msg(o_msg, &ganss_caps->n_ganss,     1) < 0)) {
                ERR_NM("Failed to work w/ GANSS meta data, returning....");
		return -1;
        }

	if(ganss_caps->n_ganss)
		if(push_data_in2msg(o_msg, ganss_caps->ganss_caps,  
                                    ganss_caps->n_ganss) < 0) {
                        ERR_NM("Failed to work w/ GANSS data, returning....");
			return -1;
                }
        
	return 0;
}

static int packin_get_ue_agnss_caps(struct agnss_msg *o_msg, 
                                    struct ganss_ue_assist_caps *ganss_caps)
{
	CPP_FTRACE3();
	        
	if((push_data_in2msg(o_msg, &ganss_caps->common_bits, 1) < 0) ||
	   (push_data_in2msg(o_msg, &ganss_caps->n_ganss,     1) < 0)) {
                ERR_NM("Failed to work w/ GANSS meta data, returning....");
		return -1;
        }
        
	if(ganss_caps->n_ganss)
		if(push_data_in2msg(o_msg, ganss_caps->ganss_caps,  
                                    ganss_caps->n_ganss) < 0) {
                        ERR_NM("Failed to work w/ GANSS data, returning....");
			return -1;
                }
        
	return 0;
}


static int make_xact_get_ue_agnss_caps(void *hnd, unsigned int& gps_caps_bits,
                                       struct ganss_ue_assist_caps *ganss_caps)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;        
	struct agnss_msg *o_msg, *i_msg = NULL; 

        o_msg = alloc_msg2send(*xact_node, GNFN_ID_GET_UE_AGNSS_CAPS);
	if(NULL == o_msg)
		goto make_xact_get_ue_agnss_caps_exit;
        
	/* Pack-in information to be sent */
	if(packin_get_ue_agnss_caps(o_msg, gps_caps_bits, ganss_caps) < 0)
		goto make_xact_get_ue_agnss_caps_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(!i_msg)
		goto make_xact_get_ue_agnss_caps_exit;

	ret_val = i_msg->head.ret_val;
        o_msg->head.xact_id = i_msg->head.xact_id;

	/* Un-pack information from i_msg */
	if(unpack_get_ue_agnss_caps(i_msg, gps_caps_bits, ganss_caps) < 0)
		ret_val = -1;

 make_xact_get_ue_agnss_caps_exit: 
        return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_get_ue_agnss_caps(void *hnd, struct agnss_msg *i_msg, 
                                          ganss_ue_assist_caps *ganss_caps ) 
{
	CPP_FTRACE3();

        unsigned int gps_caps_bits = 0;
        
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
	struct agnss_msg *o_msg = NULL;
	
	/* Un-pack information from i_msg */
	if(unpack_get_ue_agnss_caps(i_msg, gps_caps_bits, ganss_caps) < 0) 
		goto do_proc_xact_set_loc_instruct_exit;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_GET_UE_AGNSS_CAPS);
	if(NULL == o_msg)
		goto do_proc_xact_set_loc_instruct_exit;

        INLINE_CALL_CB_FN(xact_node->agnss_cbs.get_ue_agnss_caps, 
                          ret_val, hnd, gps_caps_bits, ganss_caps);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
	/* Pack-in information to be sent */
	if(packin_get_ue_agnss_caps(o_msg, ganss_caps) < 0)
		goto do_proc_xact_set_loc_instruct_exit;

 do_proc_xact_set_loc_instruct_exit:
        return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);       
}

static int proc_xact_get_ue_agnss_caps(void *hnd, struct agnss_msg *i_msg)
{
        int ret_val = -1;
        struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;

        struct ganss_ue_assist_caps *ganss_caps = NULL;
        ganss_caps   = new struct ganss_ue_assist_caps;
        if(NULL == ganss_caps) {
                ERR_NM("Failed: alloc mem for ganss_ue_assist_caps, returning");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }
        
        ret_val = do_proc_xact_get_ue_agnss_caps(hnd, i_msg, ganss_caps);
        
        delete ganss_caps;
        
        return ret_val;
}


/*------------------------------------------------------------------------------
 * Set loc instruct
 *----------------------------------------------------------------------------*/
static int make_xact_set_loc_instruct(void *hnd, const struct loc_instruct& req)
{
	CPP_FTRACE3();
	
        int ret_val = -1;
        struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
        struct agnss_msg *o_msg, *i_msg = NULL; 
	
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_SET_LOC_INSTRUCT);
	if(NULL == o_msg)
                goto make_xact_set_loc_instruct_exit;
        
        if(push_data_in2msg(o_msg, &req, 1) < 0)
                goto make_xact_set_loc_instruct_exit;
        
        i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_set_loc_instruct_exit;
        
        ret_val = i_msg->head.ret_val;

 make_xact_set_loc_instruct_exit:	
        return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int proc_xact_set_loc_instruct(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	struct loc_instruct req;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg = NULL;
        
	if(pullout_data_msg(i_msg, &req,  1) < 0)
		goto proc_xact_set_loc_instruct_exit;
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_SET_LOC_INSTRUCT);
	if(NULL == o_msg)
		goto proc_xact_set_loc_instruct_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.set_loc_instruct, 
                          ret_val, hnd, req);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;

 proc_xact_set_loc_instruct_exit:
        return send_n_free_msgs(*xact_node,o_msg,i_msg,ret_val);
}

/*------------------------------------------------------------------------------
 * Set report criteria
 *----------------------------------------------------------------------------*/
static int 
make_xact_set_report_criteria(void *hnd, const struct rpt_criteria& rpt)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
        struct agnss_msg *o_msg, *i_msg = NULL; 
	
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_SET_RPT_CRITERIA);
	if(NULL == o_msg)
		goto make_xact_set_report_criteria_exit;

	if(push_data_in2msg(o_msg, &rpt, 1) < 0)
		goto make_xact_set_report_criteria_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_set_report_criteria_exit;
        
	ret_val = i_msg->head.ret_val;

 make_xact_set_report_criteria_exit:
		return free_msgs_n_done(o_msg, i_msg, ret_val);
        
}

static int proc_xact_set_report_criteria(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	struct rpt_criteria rpt;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg = NULL;

	if(pullout_data_msg(i_msg, &rpt,  1) < 0)
		goto proc_xact_set_report_criteria_exit;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_SET_RPT_CRITERIA);
	if(NULL == o_msg)
		goto proc_xact_set_report_criteria_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.set_rpt_criteria, 
                          ret_val, hnd, rpt);

	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;

 proc_xact_set_report_criteria_exit:
        return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

/*------------------------------------------------------------------------------
 * UE Assist / Aiding Functions
 *----------------------------------------------------------------------------*/
static int 
packin_ue_gnss_icd_data(struct agnss_msg *o_msg, enum loc_assist& assist,
                        const void *assist_array, int num)
{
        int rv = 0;

        switch(assist) {

        case a_REF_POS:
                rv = push_data_in2msg(o_msg, 
                                      (struct location_desc*)assist_array, num);
                break;

        case a_GPS_TIM:
                rv = push_data_in2msg(o_msg, 
                                      (struct gps_ref_time*)assist_array, num);
                break;

        case a_GPS_DGP:
                rv = push_data_in2msg(o_msg, 
                                      (struct dgps_sat*)assist_array, num);
                break;
                
        case a_GPS_EPH:
                rv = push_data_in2msg(o_msg, (struct gps_eph*)assist_array, num);
                break;

        case a_GPS_ION:
                rv = push_data_in2msg(o_msg, (struct gps_ion*)assist_array, num);
                break;

        case a_GPS_UTC:
                rv = push_data_in2msg(o_msg, (struct gps_utc*)assist_array, num);
                break;

        case a_GPS_ALM:
                rv = push_data_in2msg(o_msg, (struct gps_alm*)assist_array, num);
                break;

        case a_GPS_ACQ:
                rv = push_data_in2msg(o_msg, (struct gps_acq*)assist_array, num);
                break;

        case a_GPS_RTI:
                rv = push_data_in2msg(o_msg, (struct gps_rti*)assist_array, num);
                break;

        case a_GLO_EPH:
                rv = push_data_in2msg(o_msg, (struct glo_eph*)assist_array, num);
                break;

        case a_GLO_ALM:
                rv = push_data_in2msg(o_msg, (struct glo_alm*)assist_array, num);
                break;

/*        case a_GLO_UTC:
                rv = push_data_in2msg(o_msg, (struct glo_utc*)assist_array, num);
                break;*/

        case a_GLO_ION:
                rv = push_data_in2msg(o_msg, 
                                      (struct ganss_ion_model*)assist_array, num);
                break;

        case a_GLO_TIM:
                rv = push_data_in2msg(o_msg, 
                                      (struct ganss_ref_time*)assist_array, num);
                break;

        case a_GLO_ACQ_G1:
        case a_GLO_ACQ_G2:
                rv = push_data_in2msg(o_msg, 
                                      (struct ganss_ref_msr*)assist_array, num);
                break;

        case a_GLO_RTI:
                rv = push_data_in2msg(o_msg, 
                                      (struct ganss_rti*)assist_array, num);
                break;

        case a_GLO_DGC_G1:
        case a_GLO_DGC_G2:
                rv = push_data_in2msg(o_msg, 
                                      (struct dganss_sat*)assist_array, num);
                break;

        default:
                ERR_NM("**** Unsupported GNSS ICD data, need a fix up ****");
                break;
        }

        return rv;
}

static int 
unpack_ue_gnss_icd_data(struct agnss_msg *i_msg, enum loc_assist& assist,
                        const void *assist_array, int num)
{
        int rv = 0;

        switch(assist) {

        case a_REF_POS:
                rv = pullout_data_msg(i_msg, 
                                      (struct location_desc*)assist_array, num);
                break;

        case a_GPS_TIM:
                rv = pullout_data_msg(i_msg, 
                                      (struct gps_ref_time*)assist_array, num);
                break;

        case a_GPS_DGP:
                rv = pullout_data_msg(i_msg, 
                                      (struct dgps_sat*)assist_array, num);
                break;
                
        case a_GPS_EPH:
                rv = pullout_data_msg(i_msg, (struct gps_eph*)assist_array, num);
                break;

        case a_GPS_ION:
                rv = pullout_data_msg(i_msg, (struct gps_ion*)assist_array, num);
                break;

        case a_GPS_UTC:
                rv = pullout_data_msg(i_msg, (struct gps_utc*)assist_array, num);
                break;

        case a_GPS_ALM:
                rv = pullout_data_msg(i_msg, (struct gps_alm*)assist_array, num);
                break;

        case a_GPS_ACQ:
                rv = pullout_data_msg(i_msg, (struct gps_acq*)assist_array, num);
                break;

        case a_GPS_RTI:
                rv = pullout_data_msg(i_msg, (struct gps_rti*)assist_array, num);
                break;

        case a_GLO_EPH:
                rv = pullout_data_msg(i_msg, (struct glo_eph*)assist_array, num);
                break;

        case a_GLO_ALM:
                rv = pullout_data_msg(i_msg, (struct glo_alm*)assist_array, num);
                break;

/*        case a_GLO_UTC:
                rv = pullout_data_msg(i_msg, (struct glo_utc*)assist_array, num);
                break;*/

        case a_GLO_ION:
                rv = pullout_data_msg(i_msg, 
                                      (struct ganss_ion_model*)assist_array, num);
                break;

        case a_GLO_TIM:
                rv = pullout_data_msg(i_msg, 
                                      (struct ganss_ref_time*)assist_array, num);
                break;

        case a_GLO_ACQ_G1:
        case a_GLO_ACQ_G2:
                rv = pullout_data_msg(i_msg, 
                                      (struct ganss_ref_msr*)assist_array, num);
                break;

        case a_GLO_RTI:
                rv = pullout_data_msg(i_msg, 
                                      (struct ganss_rti*)assist_array, num);
                break;

        case a_GLO_DGC_G1:
        case a_GLO_DGC_G2:
                rv = pullout_data_msg(i_msg, 
                                      (struct dganss_sat*)assist_array, num);
                break;

        default:
                ERR_NM("^^^^ Unsupported GNSS ICD data, need a fix up ^^^^");
                break;
        }

        return rv;
}

static int
packin_gnss_assist(struct agnss_msg *o_msg, struct nw_assist_id& assist,
		   const void *assist_array, int num)
{
        int ret_val = 0;

        switch(assist.type_select) {

        case NTV_ICD:
                ret_val = packin_ue_gnss_icd_data(o_msg, assist.gnss_icd_ie, 
                                                  assist_array, num);
                break;
                
        case NNATIVE:
                /* ret_val = packin_gnss_nntv_data(o_msg, assist.n_native_ie,
                   assist_array, num);
                */
                break;
                
        default:
                ret_val = -1;
        }
        
        return ret_val;
}

static int 
unpack_gnss_assist(struct agnss_msg *i_msg, struct nw_assist_id& assist, 
                   void *assist_array, int num)
{
        int ret_val = 0;

        switch(assist.type_select) {

        case NTV_ICD:
                ret_val = unpack_ue_gnss_icd_data(i_msg, assist.gnss_icd_ie, 
                                                  assist_array, num);
                break;
                
        case NNATIVE:
                /* ret_val = packin_gnss_nntv_data(o_msg, assist.n_native_ie,
                   assist_array, num);
                */
                break;
                
        default:
                ret_val = -1;
        }
        
        return ret_val;
}

/*------------------------------------------------------------------------------
 * UE Add Assist
 *----------------------------------------------------------------------------*/
static int 
unpack_ue_add_assist(struct agnss_msg *i_msg, 
                     struct nw_assist_id &assist, int &num)
{
	CPP_FTRACE3();

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &assist,  1) < 0) ||
	   (pullout_data_msg(i_msg, &num,     1) < 0)) {
                ERR_NM("Failed to work w/ assist meta data, returning ...");
		return -1;
        }

	return 0;
}

static int packin_ue_add_assist(struct agnss_msg *o_msg, 
                                struct nw_assist_id assist,
                                const void *assist_array, int num)
{
	CPP_FTRACE3();

	if((push_data_in2msg(o_msg, &assist, 1) < 0) ||
	   (push_data_in2msg(o_msg, &num,    1) < 0)) {
                ERR_NM("Failed to work w/ assist meta data, returning ...");
		return -1;
        }
        
	if(num) {
                if(packin_gnss_assist(o_msg, assist, assist_array, num) < 0) {
                        ERR_NM("Failed to work w/ assist data, returning...");
                        return -1;
                }
        }
        
	return 0;
}

static int make_xact_ue_add_assist(void *hnd, const struct nw_assist_id& assist,
				   const void *assist_array, int num)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 
	
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_ADD_ASSIST);
	if(NULL == o_msg)
		goto make_xact_ue_add_assist_exit;

	if(packin_ue_add_assist(o_msg, assist, assist_array, num) < 0)
		goto make_xact_ue_add_assist_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_ue_add_assist_exit;
        
	ret_val = i_msg->head.ret_val;
        
 make_xact_ue_add_assist_exit:
        return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_ue_add_assist(void *hnd, struct agnss_msg *i_msg,
                                      void *assist_array)
{
	CPP_FTRACE3();

	struct nw_assist_id assist;
	int num;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg = NULL;
	
	if(unpack_ue_add_assist(i_msg, assist, num) < 0)
		goto do_proc_xact_ue_add_assist_exit;

	if(0 < num) {
                if(unpack_gnss_assist(i_msg, assist, assist_array, num) < 0)
                        goto do_proc_xact_ue_add_assist_exit;
        }
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_ADD_ASSIST);
	if(NULL == o_msg)
		goto do_proc_xact_ue_add_assist_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_add_assist, ret_val,
                          hnd, assist, assist_array, num);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
do_proc_xact_ue_add_assist_exit:
        return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_ue_add_assist(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;

	void *assist_array = new char[256*32]; // : fixup
	if (NULL == assist_array) {
                ERR_NM("Failed to allocate mem for assist data, returning");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }

        ret_val = do_proc_xact_ue_add_assist(hnd, i_msg, assist_array);

        delete [] (char *)assist_array;
        
        return ret_val;
}

/*------------------------------------------------------------------------------
 * UE Del Assist
 *----------------------------------------------------------------------------*/
static int unpack_ue_del_aiding(struct agnss_msg *i_msg, enum loc_assist &assist,
                                unsigned int &sv_id_map, unsigned int &mem_flags)
{
	CPP_FTRACE3();

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &assist,          1) < 0) ||
	   (pullout_data_msg(i_msg, &sv_id_map,	      1) < 0) ||
	   (pullout_data_msg(i_msg, &mem_flags,       1) < 0)) {
                ERR_NM("Failed to work w/ aiding meta data, returning");
		return -1;
        }

	return 0;
}

static int packin_ue_del_aiding(struct agnss_msg *o_msg, enum loc_assist assist,
                                unsigned int sv_id_map,  unsigned int mem_flags)
{
	CPP_FTRACE3();

	if((push_data_in2msg(o_msg, &assist, 	 1) < 0) ||
	   (push_data_in2msg(o_msg, &sv_id_map,  1) < 0) ||
	   (push_data_in2msg(o_msg, &mem_flags,  1) < 0)) {
                ERR_NM("Failed to work w/ aiding meta data, returning");
		return -1;
        }

	return 0;
}

static int make_xact_ue_del_aiding(void *hnd, enum loc_assist assist, 
                                   unsigned int sv_id_map, 
                                   unsigned int mem_flags)
        
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;        
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_DEL_AIDING);
	if(NULL == o_msg)
                goto make_xact_ue_del_aiding_exit; 
        
	if(packin_ue_del_aiding(o_msg, assist, sv_id_map, mem_flags) < 0)
                goto make_xact_ue_del_aiding_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_del_aiding_exit;
        
        ret_val = i_msg->head.ret_val;
	
 make_xact_ue_del_aiding_exit:
	return free_msgs_n_done(o_msg, i_msg, ret_val);        
}

static int proc_xact_ue_del_aiding(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
        
        enum loc_assist assist;
	unsigned int sv_id_map;
	unsigned int mem_flags;

	int ret_val = -1; 
        struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;       
        struct agnss_msg *o_msg = NULL;
        
        if(unpack_ue_del_aiding(i_msg, assist, sv_id_map, mem_flags) < 0)
                goto proc_xact_ue_del_aiding_exit;         
        
        o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_DEL_AIDING);
        if(NULL == o_msg)
                goto proc_xact_ue_del_aiding_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_del_aiding, ret_val,
                          hnd, assist, sv_id_map, mem_flags);
        
        o_msg->head.ret_val = ret_val;
        o_msg->head.xact_id = i_msg->head.xact_id;
        
 proc_xact_ue_del_aiding_exit:
        return send_n_free_msgs(*xact_node,o_msg,i_msg,ret_val);
}

/*------------------------------------------------------------------------------
 * UE Del Assist
 *----------------------------------------------------------------------------*/
static int 
unpack_ue_aiding(struct agnss_msg *i_msg, enum loc_assist &assist, int &num)
{
	CPP_FTRACE3();

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &assist,  1) < 0) ||
	   (pullout_data_msg(i_msg, &num,     1) < 0)) {
                ERR_NM("Failed to work w/ aiding metadata, returning");
		return -1;
        }

	return 0;
}

static int packin_ue_aiding(struct agnss_msg *o_msg, enum loc_assist assist,
                            const void *assist_array, int num)
{
	CPP_FTRACE3();
	
        if((push_data_in2msg(o_msg, &assist, 1) < 0) ||
           (push_data_in2msg(o_msg, &num,    1) < 0)) {
                ERR_NM("Failed to work w/ aiding metadata, returning");
                return -1;
        }
        
        if(num) {
                if(packin_ue_gnss_icd_data(o_msg, assist, assist_array, 
                                           num) < 0) {
                        ERR_NM("Failed to work w/ aiding data, returning");
                        return -1;
                }
        }
        
        return 0;
}

static int make_xact_ue_get_aiding(void *hnd, enum loc_assist assist,
                                   void *assist_array, int num)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;        
	struct agnss_msg *o_msg, *i_msg = NULL; 
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_GET_AIDING);
	if(NULL == o_msg)
		goto make_xact_ue_get_aiding_exit; 

	if(packin_ue_aiding(o_msg, assist, assist_array, num) < 0)
                goto make_xact_ue_get_aiding_exit;
	
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_get_aiding_exit; 

	if(unpack_ue_aiding(i_msg, assist, num) < 0)
		goto make_xact_ue_get_aiding_exit; 

	if(0 < num) {
                if(unpack_ue_gnss_icd_data(i_msg, assist, assist_array, num) < 0)
                        goto make_xact_ue_get_aiding_exit;
        }
        
	ret_val = i_msg->head.ret_val;
        
 make_xact_ue_get_aiding_exit:
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}
 
static int do_proc_xact_ue_get_aiding(void *hnd, struct agnss_msg *i_msg, void *assist_array)
{
	CPP_FTRACE3();

	enum loc_assist assist; int num = 0;   

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL;   

	if(unpack_ue_aiding(i_msg, assist, num) < 0)
                goto proc_xact_ue_get_aiding_exit;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_GET_AIDING);
	if(NULL == o_msg) 	
                goto proc_xact_ue_get_aiding_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_get_aiding, ret_val,
                          hnd, assist, assist_array, num);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
	if(packin_ue_aiding(o_msg, assist, assist_array, ret_val) < 0) {
                /* Problem: send error in return value to remote processing */
                ret_val = -1;
	}
        
 proc_xact_ue_get_aiding_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_ue_get_aiding(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	void *assist_array = new char[256*32]; /*: Restrict to data type, later */
	if(NULL == assist_array) {
                ERR_NM("Failed: allocate mem for assist_array, returning");
		return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }
	
        ret_val = do_proc_xact_ue_get_aiding(hnd, i_msg, assist_array);

        delete [] (char *)assist_array;
        
	 return ret_val; 
}

/*------------------------------------------------------------------------------
 * assistance source indicates the completion of assistance provision
 *----------------------------------------------------------------------------*/
static 
int make_xact_ue_assist2ue_done(void *hnd,
                                const struct assist_reference& assist_usr)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 
	
        o_msg = alloc_msg2send(*xact_node, GNFN_ID_ASSIST2UE_DONE);
	if(NULL == o_msg)
		goto make_xact_ue_assist2ue_done_exit;

        /* Pack-in information to be sent */ 
        if(packin_assist_usr_info(o_msg, &assist_usr)  < 0)
		goto make_xact_ue_assist2ue_done_exit;
      
        i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_ue_assist2ue_done_exit;
        
	ret_val = i_msg->head.ret_val;
        
 make_xact_ue_assist2ue_done_exit:
        return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int proc_xact_ue_assist2ue_done(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;        
	struct agnss_msg *o_msg;
        struct assist_reference assist_usr;
        o_msg = alloc_msg2send(*xact_node, GNFN_ID_ASSIST2UE_DONE);
	if(NULL == o_msg)
		goto proc_xact_ue_assist2ue_done_exit;

        if(unpack_assist_usr_info(i_msg, &assist_usr) < 0)
		goto proc_xact_ue_assist2ue_done_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.assist2ue_done, 
                          ret_val, hnd,assist_usr);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 proc_xact_ue_assist2ue_done_exit:
        return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);        
}



static int packin_ganss_msr_desc(struct agnss_msg *o_msg, 
                                 const struct ganss_msr_info *ganss_msr,
                                 unsigned char n_ganss)
{
	CPP_FTRACE3();

	if(0 == n_ganss) {
                DBG_L3("MSR info: working w/ 0 GANSS satellites, returning");
                return 0;
        }

	if(NULL == ganss_msr) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }

	if(push_data_in2msg(o_msg, ganss_msr, n_ganss) < 0) {
                ERR_NM("Failed, to work w/ GANSS MSR data, returning");
		return -1;
        }

	return 0;
}

static int 
unpack_ganss_msr_desc(struct agnss_msg *i_msg, 
                      struct ganss_msr_info *ganss_msr,
                      unsigned char n_ganss)
{
	CPP_FTRACE3();

	if(0 == n_ganss) {
                DBG_L3("MSR info: working w/ 0 GANSS satellites, returning");
                return 0;
        }
        
     	if(NULL == ganss_msr) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }

	if(pullout_data_msg(i_msg, ganss_msr, n_ganss) < 0) {
                ERR_NM("Failed, to work w/ GANSS MSR data, returning");
		return -1;
        }

	return 0;
}

/*------------------------------------------------------------------------------
 * GNSS Position Results
 *----------------------------------------------------------------------------*/
static int packin_gnss_pos_result(struct agnss_msg *o_msg, 
                                  const struct gnss_pos_result *pos_result)
{
	CPP_FTRACE3();

	if(NULL == pos_result) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }
        
	if(push_data_in2msg(o_msg, pos_result, 1) < 0) {
                ERR_NM("Failed, to work w/ POS data, returning");
		return -1; 
        }

	return 0;
}

static int unpack_gnss_pos_result(struct agnss_msg *i_msg, 
                                  struct gnss_pos_result *pos_result)
{
	CPP_FTRACE3();

	if(NULL == pos_result) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }
        
	if(pullout_data_msg(i_msg, pos_result, 1) < 0) {
                ERR_NM("Failed, to work w/ POS data, returning");
		return -1;
        }

	return 0;
}

/*------------------------------------------------------------------------------
 * GPS Measurment Results
 *----------------------------------------------------------------------------*/
static int packin_gps_msr_result(struct agnss_msg *o_msg,
                                 const struct gps_msr_result *gps_result)
{
	CPP_FTRACE3();
	
	if(NULL == gps_result) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }

	if(push_data_in2msg(o_msg, gps_result, 1) < 0) {
                ERR_NM("Failed, to work w/ GPS MSR data, returning");
		return -1;
        }

	return 0;
}

static int unpack_gps_msr_result(struct agnss_msg *i_msg, 
                                 struct gps_msr_result *gps_result)
{
	CPP_FTRACE3();

	if(NULL == gps_result) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }
        
	if(pullout_data_msg(i_msg, gps_result, 1) < 0) {
                ERR_NM("Failed, to work w/ GPS result, returning");
		return -1;
        }

	return 0;
}



/*------------------------------------------------------------------------------
 * GPS Measurment Info
 *----------------------------------------------------------------------------*/
static int packin_gps_msr_info(struct agnss_msg *o_msg, 
                               const struct gps_msr_info *gps_msr,
                               unsigned char n_sat)
{
	CPP_FTRACE3();

	if(0 == n_sat) {
                DBG_L3("Working with 0 satelliters in GPS MSR, returning");
                return 0;
        }

	if(NULL == gps_msr) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }

	if(push_data_in2msg(o_msg, gps_msr, n_sat) < 0) {
                ERR_NM("Failed to work w/ GPS MSR, returning...");
		return -1; 
        }
        
	return 0;
}

static int unpack_gps_msr_info(struct agnss_msg *i_msg, 
                               struct gps_msr_info *gps_msr,
                               unsigned char n_sat)
{
	CPP_FTRACE3();

	if(0 == n_sat) {
                DBG_L3("Working with 0 satelliters in GPS MSR, returning");
                return 0;
        }

      	if(NULL == gps_msr) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }
        
	if(pullout_data_msg(i_msg, gps_msr, n_sat) < 0) {
                ERR_NM("Failed to work w/ GPS MSR, returning...");
		return -1;
        }
        
	return 0;
}

/*------------------------------------------------------------------------------
 * GANSS Measurment Results
 *----------------------------------------------------------------------------*/
static int packin_ganss_msr_result(struct agnss_msg *o_msg, 
                                   const struct ganss_msr_result *ganss_msr)
{
	CPP_FTRACE3();

	if(NULL == ganss_msr) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }

	if(push_data_in2msg(o_msg, ganss_msr, 1) < 0) {
		ERR_NM("Failed to work w/ GANSS MSR, returning...");
                return -1;
        }

	return 0;
}

static int unpack_ganss_msr_result(struct agnss_msg *i_msg, 
                                   struct ganss_msr_result *ganss_msr)
{
	CPP_FTRACE3();

	if(NULL == ganss_msr) {
                ERR_NM("Failed, working with NULL mem pointer, returning");
                return -1;
        }
        
	if(pullout_data_msg(i_msg, ganss_msr, 1) < 0) {
                ERR_NM("Failed to work w/ GANSS MSR, returning...");
		return -1;
        }

	return 0;
}

static int make_xact_ue_loc_results(void *hnd, 
                                    const struct gnss_pos_result&  pos_result,
                                    const struct gps_msr_result&   gps_result, 
                                    const struct gps_msr_info     *gps_msr,
                                    const struct ganss_msr_result& ganss_result, 
                                    const struct ganss_msr_info   *ganss_msr)
{
	CPP_FTRACE3();

        unsigned char n_sat,  n_ganss;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL;
	
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_LOC_RESULTS);
	if(NULL == o_msg)
		goto make_xact_ue_loc_results_exit;
        
	n_sat   = gps_result.n_sat;
	n_ganss = ganss_result.n_ganss;
        
	/* Pack-in information to be sent */ 
	if((packin_gnss_pos_result(o_msg, &pos_result)        < 0)   ||  
	   (packin_gps_msr_result(o_msg,  &gps_result)        < 0)   ||
	   (packin_gps_msr_info(o_msg, gps_msr, n_sat)        < 0)   ||
	   (packin_ganss_msr_result(o_msg, &ganss_result)     < 0)   ||
	   (packin_ganss_msr_desc(o_msg, ganss_msr, n_ganss)  < 0))
                goto make_xact_ue_loc_results_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_ue_loc_results_exit;
        
	ret_val  = i_msg->head.ret_val;
        
 make_xact_ue_loc_results_exit:
        return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_ue_loc_results(void *hnd, struct agnss_msg *i_msg,
                                       struct gnss_pos_result&  pos_result,
                                       struct gps_msr_result&   gps_result,
                                       struct gps_msr_info     *gps_msr,
                                       struct ganss_msr_result& ganss_result,
                                       struct ganss_msr_info   *ganss_msr)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
	struct agnss_msg *o_msg = NULL;
	   
	/* Ug-pack information from i_msg */
	if((unpack_gnss_pos_result(i_msg, &pos_result)            < 0)  ||
	   (unpack_gps_msr_result(i_msg, &gps_result)             < 0)  ||
	   (unpack_gps_msr_info(i_msg, gps_msr, gps_result.n_sat) < 0)  ||
	   (unpack_ganss_msr_result(i_msg, &ganss_result)         < 0)  ||
	   (unpack_ganss_msr_desc(i_msg, ganss_msr, ganss_result.n_ganss) < 0))
		goto do_proc_xact_ue_loc_results_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_loc_results, ret_val,
                          hnd, pos_result, gps_result, gps_msr, 
                          ganss_result, ganss_msr);
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_LOC_RESULTS);
	if(NULL == o_msg)
		goto do_proc_xact_ue_loc_results_exit;
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 do_proc_xact_ue_loc_results_exit:
        return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_ue_loc_results(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
	struct gnss_pos_result   pos_result;
	struct gps_msr_result    gps_result;
	struct gps_msr_info     *gps_msr   = create_gps_msr_info();
	struct ganss_msr_result  ganss_result;
	struct ganss_msr_info   *ganss_msr = new ganss_msr_info[MAX_SAT]; /*create_ganss_msr_desc(); */

	if(gps_msr && ganss_msr)
		ret_val = do_proc_xact_ue_loc_results(hnd, i_msg,   pos_result, 
                                                      gps_result,   gps_msr, 
                                                      ganss_result, ganss_msr);
	else
		ret_val = send_n_free_msgs(*xact_node,  NULL, i_msg, ret_val);
        
	if(gps_msr)   delete_gps_msr_info(gps_msr);
	//if(ganss_msr) delete_ganss_msr_desc(ganss_msr);
	if(ganss_msr) delete[] ganss_msr;

	return ret_val;                
}

/*------------------------------------------------------------------------------
 * NW LOC Results
 *----------------------------------------------------------------------------*/
static int 
make_xact_nw_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
                                    const struct assist_reference& assist_usr)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;        
	struct agnss_msg *o_msg, *i_msg = NULL; 
	
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_NW_LOC_RESULTS);
	if(NULL == o_msg)
		goto make_xact_nw_loc_results_exit;
        
	if((packin_gnss_pos_result(o_msg, &pos_result) < 0) ||
                (packin_assist_usr_info(o_msg, &assist_usr) < 0))
		goto make_xact_nw_loc_results_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_nw_loc_results_exit;
        
	ret_val = i_msg->head.ret_val;
	
 make_xact_nw_loc_results_exit: 
        return free_msgs_n_done(o_msg, i_msg, ret_val);        
}

static int do_proc_xact_nw_loc_results(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg = NULL;
 
	struct gnss_pos_result pos_result;
              struct assist_reference assist_usr;

	if((unpack_gnss_pos_result(i_msg, &pos_result) < 0) ||
           (unpack_assist_usr_info(i_msg, &assist_usr) < 0))
		goto proc_xact_nw_loc_results_exit;
        
	o_msg = alloc_msg2send(*xact_node,  GNFN_ID_NW_LOC_RESULTS);
	if(NULL == o_msg)
		goto proc_xact_nw_loc_results_exit;	
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.nw_loc_results, 
                          ret_val, hnd, pos_result, assist_usr);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
	
 proc_xact_nw_loc_results_exit:
        return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_nw_loc_results(void *hnd, struct agnss_msg *i_msg)
{
        CPP_FTRACE3();

        int ret_val = -1;
        struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;  
        //struct gnss_pos_result *pos_result = new gnss_pos_result;
        //struct assist_reference *assist_usr = new assist_reference;

        //(pos_result && assist_usr)
                ret_val = do_proc_xact_nw_loc_results(hnd, i_msg);//pos_result, assist_usr);
       // else
         //       ret_val = send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);

        //if(pos_result) delete pos_result;
        //if(assist_usr) delete assist_usr;

	return ret_val;
}

/*------------------------------------------------------------------------------
 * UE Need Assistance
 *----------------------------------------------------------------------------*/
static int make_xact_ue_need_assist(void *hnd, 
                                    const struct gps_assist_req&   gps_assist,
                                    const struct ganss_assist_req& ganss_assist,
                                    const struct assist_reference& assist_usr)
{
	CPP_FTRACE3();
        
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_NEED_ASSIST);
	if(NULL == o_msg)
		goto make_xact_ue_need_assist_exit;

	if((packin_gps_assist_req(o_msg, &gps_assist)     < 0) ||
	   (packin_ganss_assist_req(o_msg, &ganss_assist) < 0) ||
	    (packin_assist_usr_info(o_msg, &assist_usr) < 0))
		goto make_xact_ue_need_assist_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_ue_need_assist_exit;
        
	ret_val = i_msg->head.ret_val;
	
 make_xact_ue_need_assist_exit:
        return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int proc_xact_ue_need_assist(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

        struct gps_assist_req gps_assist;
	struct ganss_assist_req ganss_assist;
        struct assist_reference assist_usr;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;	
	struct agnss_msg *o_msg = NULL;
	
	if((unpack_gps_assist_req(i_msg, &gps_assist)     < 0) ||
	   (unpack_ganss_assist_req(i_msg, &ganss_assist) < 0) ||
	   (unpack_assist_usr_info(i_msg, &assist_usr) < 0))
		goto proc_xact_ue_need_assist_exit;
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_NEED_ASSIST);
	if(NULL == o_msg)
		goto proc_xact_ue_need_assist_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_need_assist, ret_val,
                          hnd, gps_assist, ganss_assist, assist_usr);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 proc_xact_ue_need_assist_exit: 
        return send_n_free_msgs(*xact_node,o_msg,i_msg,ret_val);
}

/*------------------------------------------------------------------------------
 * UE LOC Start 
 *----------------------------------------------------------------------------*/
static int make_xact_loc_start(void *hnd)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_LOC_START);
	if(NULL == o_msg)
		goto make_xact_loc_start_exit; 
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_loc_start_exit; 
        
	ret_val = i_msg->head.ret_val;
        
 make_xact_loc_start_exit:
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int proc_xact_loc_start(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;     
        struct agnss_msg *o_msg;
        o_msg = alloc_msg2send(*xact_node, GNFN_ID_LOC_START);
	if(NULL == o_msg)
                goto proc_xact_loc_start_exit;

        INLINE_CALL_CB_FN(xact_node->agnss_cbs.loc_start, ret_val, hnd);

	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 proc_xact_loc_start_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int make_xact_loc_stop(void *hnd)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_LOC_STOP);
	if(NULL == o_msg)
                goto make_xact_loc_stop_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_loc_stop_exit;
        
	ret_val = i_msg->head.ret_val;
        
 make_xact_loc_stop_exit:
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int proc_xact_loc_stop(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg;
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_LOC_STOP);
	if(NULL == o_msg)
                goto proc_xact_loc_stop_exit;

	INLINE_CALL_CB_FN(xact_node->agnss_cbs.loc_stop, ret_val, hnd);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 proc_xact_loc_stop_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}


 static int 
make_xact_dev_init(void *hnd)
{
	CPP_FTRACE3();
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_DEV_INIT);
	if(NULL == o_msg)
				goto make_xact_dev_init;
		
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
				goto make_xact_dev_init;
		
	ret_val = i_msg->head.ret_val;
		
 make_xact_dev_init:
	return free_msgs_n_done(o_msg, i_msg, ret_val);

	
}


static int proc_xact_dev_init(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg;
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_DEV_INIT);
	if(NULL == o_msg)
                goto proc_xact_dev_init;

	INLINE_CALL_CB_FN(xact_node->agnss_cbs.dev_init, ret_val, hnd);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 proc_xact_dev_init:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

 static int 
make_xact_dev_plt(void *hnd, const struct plt_param& plt_test)

 {
	 CPP_FTRACE3();
	 
	 int ret_val = -1;
	 struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
		 struct agnss_msg *o_msg, *i_msg = NULL; 
	 
	 o_msg = alloc_msg2send(*xact_node, GNFN_ID_DEV_PLT);
	 if(NULL == o_msg)
		 goto make_xact_dev_plt_exit;
 
	 if(push_data_in2msg(o_msg, &plt_test, 1) < 0)
		 goto make_xact_dev_plt_exit;
		 
	 i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	 if(NULL == i_msg)
		 goto make_xact_dev_plt_exit;
		 
	 ret_val = i_msg->head.ret_val;
 
make_xact_dev_plt_exit:
		 return free_msgs_n_done(o_msg, i_msg, ret_val);
		 
 }
 
 static int proc_xact_dev_plt(void *hnd, struct agnss_msg *i_msg)
 {
	 CPP_FTRACE3();
 
	 struct plt_param plt_test;
 
	 int ret_val = -1;
	 struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	 struct agnss_msg *o_msg = NULL;
 
	 if(pullout_data_msg(i_msg, &plt_test,  1) < 0)
		 goto proc_xact_dev_plt_exit;
 
	 o_msg = alloc_msg2send(*xact_node, GNFN_ID_DEV_PLT);
	 if(NULL == o_msg)
		 goto proc_xact_dev_plt_exit;
		 
		 INLINE_CALL_CB_FN(xact_node->agnss_cbs.dev_plt, 
						   ret_val, hnd, plt_test);
 
	 o_msg->head.ret_val = ret_val;
	 o_msg->head.xact_id = i_msg->head.xact_id;
 
proc_xact_dev_plt_exit:
		 return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
 }

static int 
make_xact_ue_cleanup(void *hnd)
{
	CPP_FTRACE3();
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_CLEANUP);
	if(NULL == o_msg)
				goto make_xact_cleanup;
		
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
				goto make_xact_cleanup;
		
	ret_val = i_msg->head.ret_val;
		
 make_xact_cleanup:
	return free_msgs_n_done(o_msg, i_msg, ret_val);

	
}


static int proc_xact_ue_cleanup(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg;
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_CLEANUP);
	if(NULL == o_msg)
                goto proc_xact_cleanup;

	INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_cleanup, ret_val, hnd);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 proc_xact_cleanup:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}
 

/*------------------------------------------------------------------------------
 * UE NMEA Report
 *----------------------------------------------------------------------------*/

static int make_xact_ue_nmea_report(void *hnd, enum nmea_sn_id nmea, 
                                    const char *data, int len)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;        
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_NMEA_REPORT);
	if(NULL == o_msg)
                goto make_xact_ue_nmea_report_exit;
        
	if((push_data_in2msg(o_msg, &nmea,  1) < 0) ||
	   (push_data_in2msg(o_msg, &len,   1) < 0) ||
	   (push_data_in2msg(o_msg, data, len) < 0))
                goto make_xact_ue_nmea_report_exit;

	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_nmea_report_exit;
        
	ret_val = i_msg->head.ret_val;

 make_xact_ue_nmea_report_exit:       
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_ue_nmea_report(void *hnd, struct agnss_msg *i_msg,
                                       char *data)
{
	CPP_FTRACE3();
        
	enum nmea_sn_id nmea; 
	int len;

	int ret_val = -1;        
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL;

        /* : Introduce a check for length of data */        
	if((pullout_data_msg(i_msg, &nmea, 1) < 0)  ||
	   (pullout_data_msg(i_msg, &len,  1) < 0)  ||
           (pullout_data_msg(i_msg, data, len) < 0))
                goto proc_xact_ue_nmea_report_exit;

        o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_NMEA_REPORT);
	if(NULL == o_msg)
                goto proc_xact_ue_nmea_report_exit;

        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_nmea_report, 
                          ret_val, hnd, nmea, data, len);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;

 proc_xact_ue_nmea_report_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_ue_nmea_report(void *hnd, struct agnss_msg *i_msg)
{
        int ret_val = -1;
        struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;

        char *data = new char[2048];
        if(NULL == data) {
                ERR_NM("Failed: allocate memory for NMEA buffer, returning");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }

        ret_val = do_proc_xact_ue_nmea_report(hnd, i_msg, data);
        
        delete[] data;
        
        return ret_val;
}

/*------------------------------------------------------------------------------
 * UE SV Status
 *----------------------------------------------------------------------------*/
static int packin_aux_info4sv(struct agnss_msg *o_msg, 
                              const struct dev_gnss_sat_aux_desc   *gps_desc,
                              const struct dev_gnss_sat_aux_info   *gps_info,
                              const struct dev_gnss_sat_aux_desc   *glo_desc,
                              const struct dev_gnss_sat_aux_info   *glo_info)
{
	CPP_FTRACE3();

	if((push_data_in2msg(o_msg, gps_desc, 1)                  < 0) ||
	   (push_data_in2msg(o_msg, gps_info, gps_desc->num_sats) < 0) ||
	   (push_data_in2msg(o_msg, glo_desc, 1)                  < 0) ||
	   (push_data_in2msg(o_msg, glo_info, (glo_desc->num_sats + glo_desc->num_sbas_sats)) < 0)) {
                ERR_NM("Failed to set up data: returning w/ Error");
		return -1; 
        }

	return 0;
}

static int unpack_aux_info4sv(struct agnss_msg *o_msg,
                              struct dev_gnss_sat_aux_desc    *gps_desc,
                              struct dev_gnss_sat_aux_info    *gps_info,
                              struct dev_gnss_sat_aux_desc    *glo_desc,
                              struct dev_gnss_sat_aux_info    *glo_info)
{
	CPP_FTRACE3();
	
	if(pullout_data_msg(o_msg, gps_desc, 1) < 0) {
                ERR_NM("Failed to work w/ GPS Aux Desc, returning");
		return -1;
        }

	if(pullout_data_msg(o_msg, gps_info, gps_desc->num_sats) < 0) {
                ERR_NM("Failed to work w/ GPS Aux Data, returning");
		return -1;
        }
        
	if(pullout_data_msg(o_msg, glo_desc, 1) < 0) {
                ERR_NM("Failed to work w/ GLO Aux Desc, returning");
		return -1;
        }

	if(pullout_data_msg(o_msg, glo_info, (glo_desc->num_sats + glo_desc->num_sbas_sats)) < 0) {
                ERR_NM("Failed to work w/ GLO Aux Data, returning");
		return -1; 
        }

	return 0;
}


static int 
make_xact_ue_aux_info4sv(void *hnd,
                         const struct dev_gnss_sat_aux_desc&  gps_desc,
                         const struct dev_gnss_sat_aux_info  *gps_info,
                         const struct dev_gnss_sat_aux_desc&  glo_desc,
                         const struct dev_gnss_sat_aux_info  *glo_info)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
 	struct agnss_msg *o_msg, *i_msg = NULL; 
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_SV_STATUS);
	if(NULL == o_msg)
                goto make_xact_ue_aux_info4sv_exit;
                
        if(packin_aux_info4sv(o_msg, &gps_desc, gps_info, 
                              &glo_desc, glo_info)   < 0)
                goto make_xact_ue_aux_info4sv_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_aux_info4sv_exit;
        
	ret_val = i_msg->head.ret_val;

 make_xact_ue_aux_info4sv_exit:
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_ue_aux_info4sv(void *hnd,     struct agnss_msg *i_msg,
                                       struct dev_gnss_sat_aux_desc  gps_desc,
                                       struct dev_gnss_sat_aux_info *gps_info,
                                       struct dev_gnss_sat_aux_desc  glo_desc,
                                       struct dev_gnss_sat_aux_info *glo_info)
{
	CPP_FTRACE3();
	
	int ret_val = -1;

	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL;

	if(unpack_aux_info4sv(i_msg, &gps_desc, gps_info, 
                              &glo_desc, glo_info)   < 0)
                goto do_proc_xact_ue_aux_info4sv_exit;
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_SV_STATUS);
	if(NULL == o_msg) 
                goto do_proc_xact_ue_aux_info4sv_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_aux_info4sv, ret_val,
                          hnd, gps_desc, gps_info, glo_desc, glo_info);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 do_proc_xact_ue_aux_info4sv_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_ue_aux_info4sv(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
        struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
                
	struct dev_gnss_sat_aux_desc  gps_desc;
	struct dev_gnss_sat_aux_info *gps_info = 
		new struct dev_gnss_sat_aux_info[GPS_MAX_SVS];
        
	struct dev_gnss_sat_aux_desc  glo_desc;
	struct dev_gnss_sat_aux_info *glo_info =
		new struct dev_gnss_sat_aux_info[GPS_MAX_SVS]; // MAX MACRO

	if(gps_info && glo_info) {
                 ret_val = do_proc_xact_ue_aux_info4sv(hnd, i_msg, gps_desc,
                                                      gps_info,   glo_desc, 
                                                      glo_info);
        }
        else {
                ERR_NM("Failed to alloc mem for satellite info, returning");
                ret_val = send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }
                if(gps_info) delete[] gps_info;
		if(glo_info) delete[] glo_info;
                
        
	return ret_val; 
}

/*------------------------------------------------------------------------------
 * UE Decoded Aid
 *----------------------------------------------------------------------------*/
static int make_xact_ue_decoded_aid(void *hnd, enum loc_assist assist, 
                                    const void *assist_array, int num)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_DECODED_AID);
	if(NULL == o_msg)
                goto make_xact_ue_decoded_aid_exit;
        
	if(packin_ue_aiding(o_msg, assist, assist_array, num) < 0)
                goto make_xact_ue_decoded_aid_exit;

	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_decoded_aid_exit;
        
	ret_val = i_msg->head.ret_val;

 make_xact_ue_decoded_aid_exit:
        return free_msgs_n_done(o_msg, i_msg, ret_val);     
}

static int do_proc_xact_ue_decoded_aid(void *hnd, struct agnss_msg *i_msg,
                                       void *assist_array)
{
	CPP_FTRACE3();
	
	enum loc_assist assist;  int num;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL;
        
	if(unpack_ue_aiding(i_msg, assist, num) < 0)
                goto do_proc_xact_ue_decoded_aid_exit;

	if(num) {
                if(unpack_ue_gnss_icd_data(i_msg, assist, assist_array, num) < 0)
                        goto do_proc_xact_ue_decoded_aid_exit;
        }
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_DECODED_AID);
	if(NULL == o_msg) 
                goto do_proc_xact_ue_decoded_aid_exit;

        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_decoded_aid, ret_val,
                          hnd, assist, assist_array, num);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 do_proc_xact_ue_decoded_aid_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);        
}

static int proc_xact_ue_decoded_aid(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
        
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        void *assist_array = new char[256*32]; /* To do Fixup */
	if (NULL == assist_array) {
                ERR_NM("Failed to alloc mem for assist_array, returing");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
	}
	ret_val = do_proc_xact_ue_decoded_aid(hnd, i_msg, assist_array);
	delete[] (char*) assist_array;
	return ret_val;
}

/*------------------------------------------------------------------------------
 * tcxo calib
 *----------------------------------------------------------------------------*/
static int
packin_ue_dev_oper_param(struct agnss_msg *o_msg, enum ue_dev_param_id id,
                         void *param_desc, int num)
{
        CPP_FTRACE3();

        if((push_data_in2msg(o_msg,   &id, 1) < 0)  ||
           (push_data_in2msg(o_msg,  &num, 1) < 0)) {
                ERR_NM("Failed to work w/ metadata, returning ....");
                return -1;
        }

        int ret_val = -1;
        
#define PUT_IN2MSG(st_name)                                             \
        push_data_in2msg(o_msg, (struct st_name*) param_desc, num)
        
        switch(id) {
                
        case e_ue_tcxo_calib:  ret_val = PUT_IN2MSG(ue_tcxo_calib_desc); break;
        default:                                           ret_val = -1; break; 
        }
        
        return ret_val;
}


static int 
unpack_ue_dev_oper_param(struct agnss_msg *i_msg, enum ue_dev_param_id& id,
                         void *param_desc, int& num)
{
	CPP_FTRACE3();

	int ret_val = -1;
        
	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg,  &id, 1) < 0)  ||
           (pullout_data_msg(i_msg, &num, 1) < 0)) {
                ERR_NM("Failed to work w/ metadata, returning ....");
                return -1;
        }

#define OUT_OF_MSG(st_name)                                             \
        pullout_data_msg(i_msg, (struct st_name*) param_desc, num)

        switch(id) {
        case e_ue_tcxo_calib:  ret_val = OUT_OF_MSG(ue_tcxo_calib_desc); break;
        default:                                           ret_val = -1; break; 
                
        }

	return ret_val = 0;
}

static int 
make_xact_ue_dev_oper_param(void *hnd, enum ue_dev_param_id id,
                            void *param_desc, int num)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
      	struct agnss_msg *o_msg, *i_msg = NULL;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_DEV_OPER_PARAM);
	if(NULL == o_msg)
                goto make_xact_ue_dev_oper_param_exit;
        
	/* Pack-in information to be sent */ 
	if(packin_ue_dev_oper_param(o_msg, id, param_desc, num)  < 0)
                goto make_xact_ue_dev_oper_param_exit;
	
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_dev_oper_param_exit;
        
	ret_val  = i_msg->head.ret_val;

 make_xact_ue_dev_oper_param_exit:
        
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}





static int do_proc_xact_ue_dev_oper_param(void *hnd, struct agnss_msg *i_msg,
                                          void *param_desc)
{
	CPP_FTRACE3();

	enum ue_dev_param_id id; int num;

	int ret_val = -1;	
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL; 

	if(unpack_ue_dev_oper_param(i_msg, id, param_desc, num) < 0)
                goto do_proc_xact_ue_dev_oper_param_exit;
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_DEV_OPER_PARAM);
	if(NULL == o_msg) 
                goto do_proc_xact_ue_dev_oper_param_exit;
	
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.oper_ue_dev_param, 
                          ret_val, hnd, id, param_desc, num);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;

 do_proc_xact_ue_dev_oper_param_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int proc_xact_ue_dev_oper_param(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	int ret_val = -1;
	int num;
	
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        
        /* Need to make it generic in the next iteration */
        void *param_desc = new struct ue_tcxo_calib_desc;
        if(NULL == param_desc) {
                ERR_NM("Failed to allocate memory for ue_tcxo_state_desc");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }

        ret_val = do_proc_xact_ue_dev_oper_param(hnd, i_msg, param_desc);
        
        delete (struct ue_tcxo_calib_desc*) param_desc;
        
	return ret_val;
}

/*------------------------------------------------------------------------------
 * UE DECL OPER DEV PARAM
 *----------------------------------------------------------------------------*/
static int 
unpack_decl_dev_param(struct agnss_msg *i_msg, enum ue_dev_param_id& id,
                      void *param_desc, int& num)
{
	CPP_FTRACE3();

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg,  &id, 1) < 0)  ||
	   (pullout_data_msg(i_msg, &num, 1) < 0)) {
                ERR_NM("Failed to work w/ metadata, returning ....");
		return -1;
        }

	int ret_val = -1;

#define OUT_OF_MSG(st_name)                                             \
	pullout_data_msg(i_msg, (struct st_name*) param_desc, num)

	switch(id) {
	case e_ue_tcxo_calib: ret_val = OUT_OF_MSG(ue_tcxo_calib_desc); break;
	case e_ue_tcxo_state: ret_val = OUT_OF_MSG(ue_tcxo_state_desc); break;
	default:	      ret_val = -1; 			        break; 
		
	}

	return ret_val;
}

static int
packin_decl_dev_param(struct agnss_msg *o_msg, enum ue_dev_param_id id,
                      void *param_desc, int num)
{
	CPP_FTRACE3();


	if((push_data_in2msg(o_msg,   &id, 1) < 0)  ||
	   (push_data_in2msg(o_msg,  &num, 1) < 0)) {
                ERR_NM("Failed to work w/ metadata, returning ....");
		return -1;
        }

	int ret_val = -1;
#define PUT_IN2MSG(st_name)                                              \
	push_data_in2msg(o_msg, (struct st_name*) param_desc, num)

	switch(id) {
	case e_ue_tcxo_calib:  ret_val = PUT_IN2MSG(ue_tcxo_calib_desc); break;
	case e_ue_tcxo_state:  ret_val = PUT_IN2MSG(ue_tcxo_state_desc); break;
	default:	       ret_val = -1; 	                         break; 
        }
        
	return ret_val;        
}

static int 
make_xact_decl_dev_param(void *hnd, enum ue_dev_param_id id,
                         void *param_desc, int num)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
      	struct agnss_msg *o_msg, *i_msg = NULL;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_DECL_DEV_PARAM);
	if(NULL == o_msg)
                goto make_xact_decl_dev_param_exit;
        
	/* Pack-in information to be sent */ 
	if(packin_decl_dev_param(o_msg, id, param_desc, num)  < 0)
				goto make_xact_decl_dev_param_exit;
        
	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_decl_dev_param_exit;
        
	ret_val  = i_msg->head.ret_val;
        
 make_xact_decl_dev_param_exit:
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_decl_dev_param(void *hnd, struct agnss_msg *i_msg, 
                                       void *param_desc)
{
	CPP_FTRACE3();

	enum ue_dev_param_id id; int num;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL;
	
	if(unpack_decl_dev_param(i_msg, id, param_desc, num) < 0)
                goto do_proc_xact_decl_dev_param_exit;
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_DECL_DEV_PARAM);
	if(NULL == o_msg)
                goto do_proc_xact_decl_dev_param_exit;
        
        INLINE_CALL_CB_FN(xact_node->agnss_cbs.decl_dev_param, 
                          ret_val, hnd, id, param_desc, num);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 do_proc_xact_decl_dev_param_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

static int
proc_xact_decl_dev_param(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;
	
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        
        /* Need to make it generic in the next iteration */
        void *param_desc = new struct ue_tcxo_state_desc;	
        if(NULL == param_desc) {
                ERR_NM("Failed to allocate memory for ue_tcxo_state_desc");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
        }

        ret_val = do_proc_xact_decl_dev_param(hnd, i_msg, param_desc);

        delete param_desc;

        return ret_val;
}

static int
unpack_ue_service_ctl(struct agnss_msg *i_msg, enum ue_service_ind& ind,
		      struct assist_reference* assist_usr)
{
	CPP_FTRACE3();

	if((pullout_data_msg(i_msg,  &ind,      1) < 0) || 
	   (pullout_data_msg(i_msg, &assist_usr, 1) < 0) ){
		ERR_NM("Failed to extract the requester info, returning");
		return -1;
	}

	return 0;
}

static int
packin_ue_service_ctl(struct agnss_msg *o_msg, enum ue_service_ind ind, 
                      const struct assist_reference *assist_usr)
{
	CPP_FTRACE3();

	if((push_data_in2msg(o_msg,  &ind,       1) < 0)  ||
	   (push_data_in2msg(o_msg,  &assist_usr, 1) < 0)) {
		ERR_NM("Failed to pack the data, returning ....");
		return -1;
	}

	return 0;
}

static int 
make_xact_ue_service_ctl(void *hnd,      enum ue_service_ind ind, 
			 const struct assist_reference& assist_usr)
{
	CPP_FTRACE3();

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node*) hnd;
	struct agnss_msg   *o_msg, *i_msg = NULL;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_SERVICE_CTL);
	if(NULL == o_msg)
		goto make_xact_ue_service_ctl_exit;

	/* Pack-in information to be sent */ 
	if(packin_ue_service_ctl(o_msg, ind, &assist_usr)  < 0)
		goto make_xact_ue_service_ctl_exit;

	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
		goto make_xact_ue_service_ctl_exit;

	ret_val  = i_msg->head.ret_val;

make_xact_ue_service_ctl_exit:
	return free_msgs_n_done(o_msg, i_msg, ret_val);
}

static int do_proc_xact_ue_service_ctl(void *hnd,      struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	enum ue_service_ind ind;
	struct assist_reference assist_usr;
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	struct agnss_msg *o_msg = NULL;

	if(unpack_ue_service_ctl(i_msg, ind, &assist_usr) < 0)
		goto do_proc_xact_ue_service_ctl_exit;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_SERVICE_CTL);
	if(NULL == o_msg)
		goto do_proc_xact_ue_service_ctl_exit;

	INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_service_ctl, 
			  ret_val, hnd, ind, assist_usr);

	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;

do_proc_xact_ue_service_ctl_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);
}

	static int 
proc_xact_ue_service_ctl(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	int ret_val = -1;

	//struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;

	//struct assist_requester_info requester;
	/* requester = new struct assist_requester_info;
	   if(NULL == requester) {
	   ERR_NM("Failed to allocate memory for assist_requester_info");
	   return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
	   }*/

	ret_val = do_proc_xact_ue_service_ctl(hnd, i_msg);

	//delete requester;

	return ret_val;
}


static int 
unpack_ue_cw_test_rep(struct agnss_msg *i_msg, enum cw_test_rep &cwt, int &num)
{
	CPP_FTRACE3();

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &cwt,  1) < 0) ||
	   (pullout_data_msg(i_msg, &num,     1) < 0)) {
                ERR_NM("Failed to unpack cw Test, returning");
		return -1;
        }

	return 0;
}

static int packin_ue_cw_test_rep(struct agnss_msg *o_msg, enum cw_test_rep cwt,
                            const void *cw_rep, int num)
{
	CPP_FTRACE3();
	
        if((push_data_in2msg(o_msg, &cwt, 1) < 0) ||
           (push_data_in2msg(o_msg, &num,    1) < 0)) {
                ERR_NM("Failed to pack CW test, returning");
                return -1;
        }
        
        if(num) {
                if(push_data_in2msg(o_msg, (struct cw_test_report*)cw_rep, 1) < 0) {
                        ERR_NM("Failed to pack cw Test, returning");
                        return -1;
                }
        }
        
        return 0;
}


/*------------------------------------------------------------------------------
 * UE CW Test report
 *----------------------------------------------------------------------------*/
static int make_xact_ue_cw_test_rep(void *hnd, enum cw_test_rep cwt, 
                                        const struct cw_test_report *cw_rep, int num)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 
	DBG_L1("make_xact_ue_cw_test_rep ++");
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_CW_TEST_RESULTS);
	if(NULL == o_msg)
                goto make_xact_ue_cw_test_rep_exit;
        
	if(packin_ue_cw_test_rep(o_msg, cwt, cw_rep, num) < 0)
                goto make_xact_ue_cw_test_rep_exit;

	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_cw_test_rep_exit;
        
	ret_val = i_msg->head.ret_val;
	DBG_L1("make_xact_ue_cw_test_rep %d--", ret_val);

 make_xact_ue_cw_test_rep_exit:
        return free_msgs_n_done(o_msg, i_msg, ret_val);     
}

static int do_proc_xact_ue_cw_test_rep(void *hnd, struct agnss_msg *i_msg,
                                       const struct cw_test_report *cw_rep)
{
	CPP_FTRACE3();
	
	enum cw_test_rep cwt;  int num;
	DBG_L1("do_proc_xact_ue_cw_test_rep ++");

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL;
        
	if(unpack_ue_cw_test_rep(i_msg, cwt, num) < 0)
                goto do_proc_xact_ue_cw_test_rep_exit;

	if(num) {
                if(pullout_data_msg(i_msg,(struct cw_test_report*)cw_rep, 1) < 0)
                        goto do_proc_xact_ue_cw_test_rep_exit;
        }
        
	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_CW_TEST_RESULTS);
	if(NULL == o_msg) 
                goto do_proc_xact_ue_cw_test_rep_exit;

        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_cw_test_results, ret_val,
                          hnd, cwt, cw_rep, num);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
	DBG_L1("do_proc_xact_ue_cw_test_rep --");
        
 do_proc_xact_ue_cw_test_rep_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);        
}

static int proc_xact_ue_cw_test_rep(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
        
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
    struct cw_test_report *cw_rep = new struct cw_test_report; /* To do Fixup */
	if (NULL == cw_rep) {
                ERR_NM("Failed to alloc mem for cw_rep, returing");
                return send_n_free_msgs(*xact_node, NULL, i_msg, ret_val);
	}
	ret_val = do_proc_xact_ue_cw_test_rep(hnd, i_msg, cw_rep);
	if(cw_rep) delete[] cw_rep;
	return ret_val;
}

static int 
unpack_ue_recovery_ind(struct agnss_msg *i_msg, enum dev_reset_ind &rst_ind, int &num)
{
	CPP_FTRACE3();

	/* Un-pack information from i_msg */
	if((pullout_data_msg(i_msg, &rst_ind,  1) < 0) ||
	   (pullout_data_msg(i_msg, &num,     1) < 0)) {
                ERR_NM("Failed to unpack recovery ind, returning");
		return -1;
        }

	return 0;
}

static int packin_ue_recovery_ind(struct agnss_msg *o_msg, enum dev_reset_ind rstind,
                            int num)
{
	CPP_FTRACE3();
	
        if((push_data_in2msg(o_msg, &rstind, 1) < 0) ||
           (push_data_in2msg(o_msg, &num,    1) < 0)) {
                ERR_NM("Failed to pack recovery ind, returning");
                return -1;
        }
        
        return 0;
}


/*------------------------------------------------------------------------------
 * UE Recovery Indication
 *----------------------------------------------------------------------------*/
static int make_xact_ue_recovery_ind(void *hnd, enum dev_reset_ind rstind, 
                                         int num)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
	struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
	struct agnss_msg *o_msg, *i_msg = NULL; 

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_RECOVERY_IND);
	if(NULL == o_msg)
                goto make_xact_ue_recovery_ind_exit;
        
	if(packin_ue_recovery_ind(o_msg, rstind,num) < 0)
                goto make_xact_ue_recovery_ind_exit;

	i_msg = send_n_recv_msgs(*xact_node, o_msg, o_msg->head.xact_id);
	if(NULL == i_msg)
                goto make_xact_ue_recovery_ind_exit;
        
	ret_val = i_msg->head.ret_val;

 make_xact_ue_recovery_ind_exit:
        return free_msgs_n_done(o_msg, i_msg, ret_val);     
}

static int do_proc_xact_ue_recovery_ind(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	
	enum dev_reset_ind rstind;  int num;

	int ret_val = -1;
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
        struct agnss_msg *o_msg = NULL;
        
	if(unpack_ue_recovery_ind(i_msg, rstind, num) < 0)
                goto do_proc_xact_ue_recovery_ind_exit;

	o_msg = alloc_msg2send(*xact_node, GNFN_ID_UE_RECOVERY_IND);
	if(NULL == o_msg) 
                goto do_proc_xact_ue_recovery_ind_exit;

        INLINE_CALL_CB_FN(xact_node->agnss_cbs.ue_recovery_ind, ret_val,
                          hnd, rstind, num);
        
	o_msg->head.ret_val = ret_val;
	o_msg->head.xact_id = i_msg->head.xact_id;
        
 do_proc_xact_ue_recovery_ind_exit:
	return send_n_free_msgs(*xact_node, o_msg, i_msg, ret_val);        
}

static int proc_xact_ue_recovery_ind(void *hnd, struct agnss_msg *i_msg)
{
	CPP_FTRACE3();
	
	int ret_val = -1;
        
	struct agnss_xact_node *xact_node = (agnss_xact_node *) hnd;
	ret_val = do_proc_xact_ue_recovery_ind(hnd, i_msg);

	return ret_val;
}

static void load_if_ops(const struct agnss_xact_node *xact_node, 
                        struct agnss_lcs_pvdr_ops& if_ops)
{
	CPP_FTRACE3();
	if_ops.hnd                     = (void*) xact_node;
        
	if_ops.loc_start               = make_xact_loc_start;
	if_ops.loc_stop                = make_xact_loc_stop;
	if_ops.get_ue_loc_caps         = make_xact_get_ue_loc_caps;
	if_ops.get_ue_agnss_caps       = make_xact_get_ue_agnss_caps;
	//if_ops.set_loc_interval        = make_xact_set_loc_interval;
	//if_ops.set_intended_qop        = make_xact_set_intended_qop;
	if_ops.set_loc_instruct	       = make_xact_set_loc_instruct;
	if_ops.set_rpt_criteria	       = make_xact_set_report_criteria;			
	if_ops.ue_add_assist           = make_xact_ue_add_assist;
	if_ops.ue_del_aiding           = make_xact_ue_del_aiding;
	if_ops.ue_get_aiding           = make_xact_ue_get_aiding;
	if_ops.assist2ue_done	       = make_xact_ue_assist2ue_done;
	//if_ops.get_ue_req4assist       = make_xact_get_ue_req4assist;
	if_ops.nw_loc_results          = make_xact_nw_loc_results;
	if_ops.oper_ue_dev_param       = make_xact_ue_dev_oper_param ;
 	if_ops.dev_init  =make_xact_dev_init;
	if_ops.ue_cleanup = make_xact_ue_cleanup;
	if_ops.dev_plt= make_xact_dev_plt;
 	return;
}

static void load_if_ops(const struct agnss_xact_node *xact_node, 
                        struct agnss_lcs_user_ops& if_ops)
{
	CPP_FTRACE3();

	if_ops.hnd                     = (void*) xact_node;
        
	if_ops.ue_need_assist          = make_xact_ue_need_assist;
	if_ops.ue_decoded_aid          = make_xact_ue_decoded_aid;
	if_ops.ue_loc_results          = make_xact_ue_loc_results;
	if_ops.ue_nmea_report          = make_xact_ue_nmea_report;
	if_ops.ue_aux_info4sv	       = make_xact_ue_aux_info4sv;
	if_ops.decl_dev_param          = make_xact_decl_dev_param;
	if_ops.ue_service_ctl          = make_xact_ue_service_ctl;
	if_ops.cw_test_results		   = make_xact_ue_cw_test_rep;
	if_ops.ue_recovery_ind		   = make_xact_ue_recovery_ind;
	return;
}

static void load_if_ops(const struct agnss_xact_node *xact_node, 
                        struct lc_assist_user_ops& if_ops)
{
	CPP_FTRACE3();

	if_ops.hnd                     = (void*) xact_node;

	if_ops.ue_add_assist           = make_xact_ue_add_assist;
	if_ops.ue_del_aiding           = make_xact_ue_del_aiding;
	if_ops.ue_get_aiding           = make_xact_ue_get_aiding;
	if_ops.assist2ue_done	       = make_xact_ue_assist2ue_done;

	return;
}

static void load_if_ops(const struct agnss_xact_node *xact_node, 
                        struct lc_assist_pvdr_ops& if_ops)
{
	CPP_FTRACE3();

	if_ops.hnd                     = (void*) xact_node;

	if_ops.ue_need_assist          = make_xact_ue_need_assist;
	if_ops.ue_decoded_aid          = make_xact_ue_decoded_aid;
	if_ops.ue_loc_results          = make_xact_ue_loc_results;
	if_ops.decl_dev_param          = make_xact_decl_dev_param;
	if_ops.ue_service_ctl          = make_xact_ue_service_ctl;
	if_ops.cw_test_results		   = make_xact_ue_cw_test_rep;
	if_ops.ue_recovery_ind		   = make_xact_ue_recovery_ind;
	return;
}


template <typename AGNSS_IF, typename AGNSS_CB>
static void *add_node(AGNSS_IF& if_ops,   const AGNSS_CB& cb_ops, 
                      unsigned int self_id, unsigned int peer_id, 
                      const char* dpath)
{
	CPP_FTRACE3();

	struct agnss_xact_node* xact_node = new agnss_xact_node(cb_ops, self_id);
	if(!xact_node)
		goto xact_add_user_exit1;

	xact_node->comm_user = agnss_comm_add_user(self_id, peer_id, dpath);
	if(!xact_node->comm_user)
		goto xact_add_user_exit2;
        
	load_if_ops(xact_node, if_ops);

	return (void*) xact_node;
        
 xact_add_user_exit2:
	delete xact_node;
        
 xact_add_user_exit1:
        
	return NULL;
}

void *agnss_xact_add_node(agnss_lcs_user_ops& if_ops,   
                          const agnss_lcs_pvdr_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath)
{
	CPP_FTRACE3();

	return add_node(if_ops, cb_ops, self_id, peer_id, dpath);
}

void *agnss_xact_add_node(agnss_lcs_pvdr_ops& if_ops,
                          const agnss_lcs_user_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath)
{
	CPP_FTRACE3();

	return add_node(if_ops, cb_ops, self_id, peer_id, dpath);
}

void *agnss_xact_add_node(lc_assist_pvdr_ops& if_ops,   
                          const lc_assist_user_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath)
{
	CPP_FTRACE3();

	return add_node(if_ops, cb_ops, self_id, peer_id, dpath);
}

void *agnss_xact_add_node(lc_assist_user_ops& if_ops,   
                          const lc_assist_pvdr_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath)
{
	CPP_FTRACE3();
	return add_node(if_ops, cb_ops, self_id, peer_id, dpath);
}


/* Creates a RX thread to block on RPC. It should be called only once in a process */
int agnss_xact_put2work(char *agnssthr_name)
{
	CPP_FTRACE3();

	return agnss_comm_put2work(agnssthr_name);
}


struct xact_proc {
        
	unsigned int  func_id;
	int (*proc_func)(void *hnd, struct agnss_msg *i_msg);
        
};

struct xact_proc xact_proc_tbl[] = {
        
	{ GNFN_ID_LOC_START         , proc_xact_loc_start},
	{ GNFN_ID_LOC_STOP          , proc_xact_loc_stop}, 
	{ GNFN_ID_GET_UE_LOC_CAPS   , proc_xact_get_ue_loc_caps},
	{ GNFN_ID_GET_UE_AGNSS_CAPS , proc_xact_get_ue_agnss_caps},
	//{ GNFN_ID_SET_LOC_INTERVAL  , proc_xact_set_loc_interval},
	//{ GNFN_ID_SET_INTENDED_QOP  , proc_xact_set_intended_qop},
	{ GNFN_ID_SET_LOC_INSTRUCT  , proc_xact_set_loc_instruct},
	{ GNFN_ID_SET_RPT_CRITERIA  , proc_xact_set_report_criteria},        
	{ GNFN_ID_UE_ADD_ASSIST     , proc_xact_ue_add_assist},
	{ GNFN_ID_UE_DEL_AIDING     , proc_xact_ue_del_aiding}, 
	{ GNFN_ID_UE_GET_AIDING     , proc_xact_ue_get_aiding}, 
	{ GNFN_ID_ASSIST2UE_DONE    , proc_xact_ue_assist2ue_done},
	//{ GNFN_ID_GET_UE_REQ4ASSIST , proc_xact_get_ue_req4assist}, 
	{ GNFN_ID_NW_LOC_RESULTS    , proc_xact_nw_loc_results},
	{ GNFN_ID_UE_NEED_ASSIST    , proc_xact_ue_need_assist},
	{ GNFN_ID_UE_DECODED_AID    , proc_xact_ue_decoded_aid},
	{ GNFN_ID_UE_LOC_RESULTS    , proc_xact_ue_loc_results}, 
	{ GNFN_ID_UE_NMEA_REPORT    , proc_xact_ue_nmea_report},
	{ GNFN_ID_UE_SV_STATUS      , proc_xact_ue_aux_info4sv},
	{ GNFN_ID_UE_DEV_OPER_PARAM , proc_xact_ue_dev_oper_param},
	{ GNFN_ID_DECL_DEV_PARAM    , proc_xact_decl_dev_param},
	{ GNFN_ID_UE_SERVICE_CTL    , proc_xact_ue_service_ctl},
 	{ GNFN_ID_DEV_INIT			, proc_xact_dev_init},
	{ GNFN_ID_UE_CLEANUP  		, proc_xact_ue_cleanup},
	{ GNFN_ID_DEV_PLT			, proc_xact_dev_plt},
	{ GNFN_ID_UE_CW_TEST_RESULTS, proc_xact_ue_cw_test_rep},
	{ GNFN_ID_UE_RECOVERY_IND, proc_xact_ue_recovery_ind},
 	{ GNFN_ID_INVALID           , NULL}
};

static int 
do_xact_process(struct agnss_xact_node& xact_node, struct agnss_msg *msg)
{
	CPP_FTRACE3();

	for(struct xact_proc *xact = xact_proc_tbl; xact->proc_func; xact++) {
                
		if(xact->func_id == msg->head.func_id) {
			xact->proc_func((void*)&xact_node, msg);
			return 0;
		}
	}
        
	/* No match for the message */
	//agnss_msg_delete(msg); /* : double check */ 

	return -1;
}

static int xact_proc(struct agnss_xact_node *xact_node)
{
	CPP_FTRACE3();
 while(1) {

                pthread_mutex_lock(&xact_node->node_lock);
                if(xact_node->term_flag != 0)
                {

                        pthread_mutex_unlock(&xact_node->node_lock);
                        break;
                }

                pthread_mutex_unlock(&xact_node->node_lock);
                struct agnss_msg *msg;
                msg = agnss_comm_recv_msg(xact_node->comm_user);
                if(!msg)
                        return -1;

                do_xact_process(*xact_node, msg);
        }


	return 0;
}

/* Loop to process user specific packets from Comm Module */
int agnss_xact_lup2proc(void *hnd)
{
	CPP_FTRACE3();

	return hnd ? xact_proc((struct agnss_xact_node*) hnd) : -1;
}

static int mod_init_count = 0;

int agnss_xact_mod_init(const char* listen_path, char *agnssthr_name)
{
	CPP_FTRACE3();

	if(mod_init_count)
		return 0;
        
	if(agnss_comm_mod_init(listen_path))
		goto xact_mod_init_exit1;
        
	if(agnss_comm_put2work(agnssthr_name))
		goto xact_mod_init_exit2;
        
	mod_init_count++;
	
        return 0;
 
 xact_mod_init_exit2:
	agnss_comm_mod_exit();
        
 xact_mod_init_exit1: 
        
        return -1;
}

int agnss_xact_stop_lup(void *hnd)
{
        CPP_FTRACE3();
		struct agnss_xact_node *xact_node = (struct agnss_xact_node *) hnd;
		pthread_mutex_lock(&xact_node->node_lock);
		xact_node->term_flag = 1;
		pthread_mutex_unlock(&xact_node->node_lock);

		return 0;
}


int agnss_xact_mod_exit(void *hnd)
{
        CPP_FTRACE3();
        return agnss_comm_mod_exit();
}
