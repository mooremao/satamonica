#include "ti_agnss_lcs.h"
#include "a-gnss_xact.h"
#include "Ftrace.h"
#include "debug.h"
#include "Connect_Config.h"

#include <iostream>
#ifdef ANDROID
#include <utils/Log.h>
#endif
using namespace std;

static void agnss_exit(void *hnd)
{
	CPP_FTRACE1();

	agnss_xact_stop_lup(hnd);

	return;

}

static int agnss_logs_init(const char *mod_name)
{
        char base_dir[] = TI_AGNSS_DIRECTORY_FPATH; 
        char name[100];
        
        snprintf(name, sizeof(name), "%s%s%s", base_dir, mod_name, ".idebug");
        if(NULL == debug_init(name))  {
                cout << "Failed to init log file for " << mod_name << endl;
                return -1;
        }
        
        snprintf(name, sizeof(name), "%s%s%s", base_dir, mod_name, ".ftrace");
        if(NULL == ftrace_init(name)) {
                ERR_NM("Failed to init FTRACE for %s", mod_name);
                return -1;
        }

        return 0;
}

/*  
 * if_holder  --> interface place holder: provided by caller, filled by callee
 * callbacks  --> callbacks place holder: filled by caller, consumed by callee
 * lpath      --> Listner path or path at which to await and receive messages
 * dpath      --> Destination path or targeted path to which messages be sent 
 * log        --> Component name to be used as a prefix for multiple log files
 */
template<typename AGNSS_IF, typename AGNSS_CB>
static void *agnss_init( AGNSS_IF& if_holder, const AGNSS_CB& callbacks,
                         unsigned int self_id,     unsigned int peer_id,
                         const char* lpath,           const char* dpath,
                         const char* log, char *agnssthr_name) 
{
	//CPP_FTRACE1();
        
        if(-1 == agnss_logs_init(log))
                goto agnss_init_err1;

	if(agnss_xact_mod_init(lpath,agnssthr_name ))
		goto agnss_init_err1;
        
	if(agnss_xact_add_node(if_holder, callbacks, self_id, peer_id, dpath))
		return if_holder.hnd;

	ERR_NM("Initialization of AGNSS Library FAILED !!!");

 agnss_init_err1:
	return NULL;        
}

void *ti_agnss_lcs_init(ti_lcs_app_enum app, struct ti_agnss_lcs_if& if_holder,
                        const struct ti_agnss_lcs_cb& callbacks)
{
       
#define NLOG(component_name)  component_name "_connect"  /* Log name */

	void *ret_hnd;

        Connect_Config :: init_connect_config();

	switch(app) {
                
        case pform:
                ret_hnd = agnss_init(if_holder,     callbacks,  // Methods
                                     APP_LCS_PLATFORM_USER_ID,  // Self ID
                                     APP_LCS_PLATFORM_PVDR_ID,  // User ID
                                     TI_AGNSS_USER_PFORM_PATH,  // My Path
                                     TI_AGNSS_CONNECT_NW_PATH,  // Ur Path
                                     NLOG(TI_AGNSS_PFORM_NAME), TI_AGNSS_PFORM_NAME);
                
                break;
                
        case cplane:
                if(false == Connect_Config :: is_control_plane_en()) {
                        cout << "Control Plane disabled from use " << endl;
                        return NULL;
                }
                
                ret_hnd = agnss_init(if_holder,     callbacks,  // Methods
                                     APP_LCS_NW_CPLNE_USER_ID,  // Self ID
                                     APP_LCS_NW_CPLNE_PVDR_ID,  // User ID
                                     TI_AGNSS_CPLNE_PVDR_PATH,  // My Path
                                     TI_AGNSS_CONNECT_NW_PATH,  // Ur Path
                                     NLOG(TI_AGNSS_CPLNE_NAME), TI_AGNSS_CPLNE_NAME);
                break;
                
        default:
                cout << "ti_agnss_lcs_init enum" << app << "not handled" << endl;
                ret_hnd = NULL;
	}
        
	return ret_hnd;
}

int  ti_agnss_lcs_work(void *hnd)
{
	CPP_FTRACE1();

	ftrace_add_fthread(pthread_self(), "agnss_user");

	if(hnd){
		return agnss_xact_lup2proc(hnd);
	}
	else {

		return -1;
	}
}

int  ti_agnss_lcs_halt(void *hnd)
{
	CPP_FTRACE1();

	//return agnss_xact_hlt_work(hnd);
	return 1;
}


void ti_agnss_lcs_exit(void *hnd)
{
	CPP_FTRACE1();
	
	agnss_exit(hnd);

	return;


}

void*
ti_assisted_ue_init(ti_assist_src_enum src, struct ti_lc_assist_if& if_holder,
                    const struct ti_lc_assist_cb& callbacks)
{
#define NLOG(component_name)  component_name "_connect"  /* Log name */

	void *ret_hnd;

        Connect_Config :: init_connect_config();

	switch(src) {

        case sa_or_pgps:
                if(false == Connect_Config :: is_predicted_eph_en()) {
                        cout << "SaGPS / PGPS disabled from use " << endl;
                        return NULL;
                }

                ret_hnd = agnss_init(if_holder,     callbacks,  // Methods
                                     APP_P_AND_SA_GPS_USER_ID,  // Self ID
                                     APP_P_AND_SA_GPS_PVDR_ID,  // Peer ID
                                     TI_AGNSS_SPGPS_PVDR_PATH,  // My Path
                                     TI_AGNSS_CONNECT_NW_PATH,  // Ur Path
                                     NLOG(TI_AGNSS_SPGPS_NAME),TI_AGNSS_SPGPS_NAME );
                break;

        default:
                ret_hnd = NULL;

	}

	return ret_hnd;
}

int ti_assisted_ue_work(void *hnd)
{
	CPP_FTRACE1();

	return agnss_xact_lup2proc(hnd);
}

int ti_assisted_ue_halt(void *hnd)
{
	CPP_FTRACE1();

	//return agnss_xact_hlt_work(hnd);
	return 1;
}

void ti_assisted_ue_exit(void *hnd)
{
	CPP_FTRACE1();

	return agnss_exit(hnd);
}


