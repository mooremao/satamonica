/* 
 * agnss_ipc.h
 *
 * this file describe the IPC for A_GNSS framework 
 *
 * Copyright (C) {YEAR} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 */


#ifndef __AGNSS_IPC_H__
#define __AGNSS_IPC_H__


/** create UDP socket and bind to specified port; 
    @param port
    @returns -1 on error otherwise fd
*/
int os_udp_sock_init(const char* sock_path);

/** Send buf to destination port; 
    @param fd identifies the socket to be closed.
    @param dst_path identifies the node where data has to be sent.
    @param data identifies the data to be send.
    @param len identifies the data length.
    @returns size of data sent otherwise -1 on error
*/  
int os_udp_send(int fd, const char* dst_path, char* data, int len);

/** Send buf to destination port; 
    @param fd identifies the socket to be closed.
    @param data identifies the data to be send.
    @param len identifies the data length.
    @returns size of data sent otherwise -1 on error
*/  
int os_udp_recv(int fd, char* data, int len);

/** Close UDP socket
    @param fd identifies the socket to be closed.
    @returns 0 on success or -1 on error 
*/
int os_udp_sock_exit(int fd);

#endif //__AGNSS_IPC_H__
