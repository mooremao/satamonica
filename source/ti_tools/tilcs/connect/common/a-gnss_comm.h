/*
 * a-gnss_comm.h
 *
 * This module lists the APIs to schedule and handle the transactions of custom
 * defined A-GNSS messages at run time across various components (processes) on
 * the local host or system. 
 *
 * Copyright (C) {YEAR} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                  
 */

#ifndef __A_GNSS_COMM_H__
#define __A_GNSS_COMM_H__


/* The following construct describes the header of the A-GNSS message. 
Note: 

a) xact_id is set by originator of request, command or indication. The same
xact_id MUST be used to complete the transaction through response (request)
and ACK / NACK for command / indication.

b) func_id is set by originator of request, command or indication. The same
func_id MUST be used to complete the transaction through response (request)
and ACK / NACK for command / indication.

c) ret_val is set by recepient of request, command or indication  in follow
through or completion of transaction. 
 */
struct agnss_msg_head {

	unsigned int xact_id;  /* Transaction ID for the Message  */
	unsigned int func_id;  /* ID of AGNSS API/func operation  */
	unsigned int ret_val;  /* Return value of func operation  */
	unsigned int rcvr_id;  /* ID of target user; ref user id  */ 
	unsigned int sndr_id;  /* ID of sender user; ref user id  */
	unsigned int max_buf;  /* Max length of the alloced buff  */

	unsigned int data_sz;  /* Length (B) of data in  Message  */
	unsigned int buf_ofs;  /* Buffer offset where data starts */
};

struct agnss_msg {

	struct agnss_msg_head    head;     /* Message  header */
	char                    *buff;     /* Data in message */

};

/* Create a new AGNSS Message */
struct agnss_msg *agnss_msg_mk_new(unsigned int func_id); 

inline void agnss_msg_delete(struct agnss_msg *msg)
{
	delete[] (char*)msg;
}

/* RPC Comm Module Init; returns -1 (error) else 0 */
int agnss_comm_mod_init(const char* listen_path); 
int agnss_comm_mod_exit(); /* RPC Comm Module Exit; returns -1 (error) else 0 */

int agnss_comm_put2work(char *agnssthr_name); /* Create a work thread; returns -1 (error) else 0 */
int agnss_comm_hlt_work(void *user_hnd); /* Remove worker thread; returns -1 (error) else 0 */

/* Add an user to NW Comm Module; returns handle on success else NULL (error) */
void* agnss_comm_add_user(unsigned int self_id, unsigned int peer_id,
                          const char* dpath); 

/* Remove the specified user from RPC Comm Module;  returns -1 (error) else 0 */
int   agnss_comm_rem_user(void *user_hnd); 

/* Send A-GNSS Msg onto Comm RPC; returns -1 (error) else length of data sent */
int agnss_comm_send_msg(void *user_hnd, struct agnss_msg *o_msg, 
                        bool wait4rsp_xact);

/* Blocks to return a valid Msg or NULL (error); user  MUST delete RX message */ 
struct agnss_msg *agnss_comm_recv_msg(void *user_hnd);

/* Blocks to return the xact Msg or NULL (error); user MUST delete RX message */ 
struct agnss_msg *agnss_comm_wait_msg(void *user_hnd, unsigned int xact_id);

#endif
