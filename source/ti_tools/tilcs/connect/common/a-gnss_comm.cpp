/*
 * a-gnss_comm.c
 *
 * This module lists the APIs to schedule and handle the transactions of custom
 * defined A-GNSS messages at run time across various components (processes) on
 * the local host or system.
 *
 * Copyright (C) {YEAR} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                  
 */

#include "a-gnss_comm.h"

#include <list>
#include <queue>
#include <pthread.h>
#include <string.h>
#include "agnss_ipc.h"
#include <unistd.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include "a-gnss_xact.h"
#include "Ftrace.h"
#include "debug.h"

using namespace std;

//static const int MAX_AGNSS_MSG_LEN = 4096;
static const int MAX_AGNSS_MSG_LEN = 72704;
static int thread_exit = 0;

struct agnss_msg *agnss_msg_mk_new(unsigned int func_id)
{
	CPP_FTRACE3();

	struct agnss_msg *new_msg = NULL;

	new_msg = (struct agnss_msg*) new char[MAX_AGNSS_MSG_LEN];
	if(!new_msg)
		return NULL;

	memset(new_msg, 0, MAX_AGNSS_MSG_LEN);

	new_msg->buff             = (char*) new_msg + sizeof(struct agnss_msg);
        
	new_msg->head.func_id     = func_id;
	new_msg->head.max_buf     = MAX_AGNSS_MSG_LEN - sizeof(struct agnss_msg);
        
	return new_msg;
}

/*------------------------------------------------------------------------------
 * Private Constructs - local to this file.
 *----------------------------------------------------------------------------*/

static const int MAX_PEND_USR2NW = 5;

class NW_User {

	private: 

		struct agnss_xact {

			unsigned int       xact_id;
			struct agnss_msg  *message;
			sem_t              sem_sig;

		}           xact_elems[MAX_PEND_USR2NW];

		unsigned int                 self_id;
		unsigned int                 peer_id;

		const char*                  dst_path;

		pthread_mutex_t              large_lock;
		sem_t                        nw2usr_sem;

		queue<struct agnss_msg*>     recv_queue;
                list<struct agnss_xact*>     xact_wlist; /* Wait list of NW transaction */
		list<struct agnss_xact*>     xact_flist; /* free list of place holders  */

		bool                         recv_queue_proc;  /* Is Q being processed? */

		/* Private function to support public interfaces */

		int pv_add_xact2wl(unsigned int xact_id);  /* Add to xact to wait list */
		int pv_rm_xact4mwl(unsigned int xact_id);  /* Remove from wait list    */

		/* Get xact for the specifieed id info from wait list */
		struct agnss_xact *pv_get_wl_xact(unsigned int xact_id);

	public:

		NW_User(unsigned int self, unsigned int peer, const char* dpath);
                ~NW_User();

		unsigned int get_self_id(void) {
			return self_id;
		}

		unsigned int get_peer_id(void) {
			return peer_id;
		}

		/* Wait for a specific transaction (Msg) from NW */
		struct agnss_msg *wait_nw2usr(unsigned int xact_id);

		struct agnss_msg *recv_nw2usr(void);       /* Recv a Msg from  NW peer */

		int push_nw2usr(struct agnss_msg *i_msg);  /* Push RX Msg from NW peer */

		//int hlt_nw_usr(struct agnss_msg *msg, const char* dpath);	/* exit all ques*/

		/* Send  Msg to the NW peer */
		int send_usr2nw(struct agnss_msg *o_msg, bool wait4rsp_xact);  
};

static list<NW_User*> nw_usr_list;  /* List of Users of RPC NW services */

static int sock_fd = 0;

/* Constructor */
NW_User :: NW_User(unsigned int self, unsigned int peer, const char* dpath)
        : self_id(self), peer_id(peer), dst_path(dpath), recv_queue_proc(false)
{
	CPP_FTRACE3();
        
	for(int i = 0; i < MAX_PEND_USR2NW; i++) {

		sem_init(&(xact_elems + i)->sem_sig, 0, 0); 
		xact_flist.push_back(xact_elems + i);
	}

	/* Initialize mutex */
	pthread_mutex_init(&large_lock, NULL);

	/*  Semaphore initialization */
	sem_init(&nw2usr_sem, 0, 0); 
}

NW_User :: ~NW_User()
{
	CPP_FTRACE3();

	sem_destroy(&nw2usr_sem);
}

struct agnss_msg *NW_User :: recv_nw2usr(void)
{
	CPP_FTRACE3();

	struct agnss_msg *i_msg = NULL;

	pthread_mutex_lock(&large_lock);          /* Begin critical section */
	if(0 == recv_queue.size()) {
		recv_queue_proc = false;          /* Queue is not empty     */

		pthread_mutex_unlock(&large_lock);/* Break critical section */
		sem_wait(&nw2usr_sem);
		pthread_mutex_lock(&large_lock);  /* Make  critical section */
	}
        
	i_msg = recv_queue.front();
	recv_queue.pop();  
        
	pthread_mutex_unlock(&large_lock);       /* Exit  critical section */
	return i_msg;
}

/* Private function and assumes called with Mutex Lock taken */
struct NW_User::agnss_xact *NW_User :: pv_get_wl_xact(unsigned int xact_id)
{
	CPP_FTRACE3();

	struct agnss_xact *xact = NULL;

	if(xact_wlist.size()) {

		list<struct agnss_xact*> :: iterator itr;

		for(itr = xact_wlist.begin(); itr != xact_wlist.end(); itr++) {	
			xact = *itr;
			if(xact_id == xact->xact_id)
				break;
		}
	}

	return xact;
}

struct agnss_msg *NW_User :: wait_nw2usr(unsigned int xact_id)
{
	CPP_FTRACE3();

	/* Get the wait listed xact */
	pthread_mutex_lock(&large_lock);
	struct agnss_xact *xact = pv_get_wl_xact(xact_id);
	pthread_mutex_unlock(&large_lock);

	if(!xact) {
		ERR_NM("FATAL: Transaction ID 0x%x not found", xact_id);  
		return NULL;
	}

	/* Now, user really blocks here for AGNSS transaction from NW */
	sem_wait(&xact->sem_sig);
        
	/* User awaken: needed AGNSS transaction is available from NW */
	pthread_mutex_lock(&large_lock);
        
	xact_wlist.remove(xact);
        
	struct agnss_msg *i_msg = xact->message;
        
	/* Update the free-list of place-holder of AGNSS transactions */
	xact->message = NULL;
	xact->xact_id = 0;
        
	xact_flist.push_back(xact);              /* Back to free list */
        
	pthread_mutex_unlock(&large_lock);
        
	return i_msg;
}

int NW_User :: push_nw2usr(struct agnss_msg *i_msg)
{
	CPP_FTRACE3();

	pthread_mutex_lock(&large_lock);          /* Begin critical section */
        
	/* 1. Find if, user is awaiting this transaction from NW */
	if(xact_wlist.size()) {
                
		list<struct agnss_xact*> :: iterator itr;
                
		for(itr = xact_wlist.begin(); itr != xact_wlist.end(); itr++) {
			struct agnss_xact *xact = *itr;
			if(xact->xact_id == i_msg->head.xact_id) {
			       xact->message = i_msg;

#define DEBUG "Awaited x-act Id:0x%x, Func Id:0x%x, Peer Id:0x%x, Self Id:0x%x" 

                               DBG_L3(DEBUG,                                   \
                                      i_msg->head.xact_id, i_msg->head.func_id,
                                      get_peer_id(),            get_self_id());

				/* Wake up waiting user for this transaction */
				sem_post(&xact->sem_sig);
				goto push_nw2usr_exit1;
			}
		}
	}

	DBG_L3("None awaited x-action Id %x", i_msg->head.xact_id);

	/* 2. User ain't waiting then, a new transaction from NW */
	recv_queue.push(i_msg);                   /* Push RX Msg into queue */
        
	//if((1 == recv_queue.size()) && (false == recv_queue_proc)) {
	if((recv_queue.size()>= 1) && (false == recv_queue_proc)) {
		recv_queue_proc = true;
		sem_post(&nw2usr_sem);            /* 1st entry, wakeup user */
	}
        
 push_nw2usr_exit1:
	pthread_mutex_unlock(&large_lock);        /* Exit critical section  */

	return 0;
}


/* Private functions, assumes called with Mutex Lock taken */
int NW_User :: pv_add_xact2wl(unsigned int xact_id)
{
	CPP_FTRACE3();

	if(xact_flist.empty()) {
		ERR_NM("Fatal: No XACT holder availalbe !!!"); 
		return -1;      /* No free elem in list */ 
	}	
        
	/* Get first elem from free list */
	struct agnss_xact *xact = xact_flist.front();
	xact_flist.pop_front(); /* Remove from free list */
        
	xact->xact_id = xact_id;
        
	xact_wlist.push_back(xact); /* Wait listed */
        
	return 0;
}

int NW_User :: pv_rm_xact4mwl(unsigned int xact_id)
{
	CPP_FTRACE3();

	int ret_val = -1;
        
	if(xact_wlist.size()) {
                
		list<struct agnss_xact*> :: iterator itr;
                
		for(itr = xact_wlist.begin(); itr != xact_wlist.end(); itr++) {
			struct agnss_xact *xact = *itr;
                        
                        if(xact_id != xact->xact_id)  {
			xact_wlist.remove(xact);     /* Remove from wait list */
			xact_flist.push_back(xact);  /* Back4use in free list */
			ret_val = 0;                 /* All's well            */
                                break;
                        }
		}
	}
        
	return ret_val; 
}

int NW_User :: send_usr2nw(struct agnss_msg *o_msg, bool wait4rsp_xact)
{
	CPP_FTRACE3();

	int ret_val = -1;

	pthread_mutex_lock(&large_lock);        /* Take Critical Section */

	DBG_L3("%s a x-action ID %x", wait4rsp_xact ? "Scheduling" : "ACK", \
               o_msg->head.xact_id);

	if(wait4rsp_xact && pv_add_xact2wl(o_msg->head.xact_id)) {
		pthread_mutex_unlock(&large_lock); /* Give Critical Section */
		return ret_val;
	}

	unsigned int msg_len = o_msg->head.data_sz; /* + o_msg->head.buf_ofs;  */
	msg_len += sizeof(struct agnss_msg);        /* Include hdr len as well */
	ret_val = os_udp_send(sock_fd, dst_path, (char*) o_msg, msg_len);
        
	if(ret_val < 0 && wait4rsp_xact)
		pv_rm_xact4mwl(o_msg->head.xact_id);

	pthread_mutex_unlock(&large_lock);          /* Give Critical Section */

	return ret_val;
}

static int worker_func()
{
	CPP_FTRACE3();

	int ret_val = -1;
	list<NW_User*> :: iterator itr;
	struct agnss_msg *msg; 

	char *udp_buf = new char[MAX_AGNSS_MSG_LEN]; /* Will be freed by user */
	if(!udp_buf)
		goto worker_func_exit1;

	/* Wait to receive message on UDP socket */
	if(os_udp_recv(sock_fd, udp_buf, MAX_AGNSS_MSG_LEN) < 0)
		goto worker_func_exit2;

	msg = (struct agnss_msg*) udp_buf;
	msg->buff = udp_buf + sizeof(struct agnss_msg);
	ret_val = 0;

	/* Find NW User from List */
	for(itr = nw_usr_list.begin(); itr != nw_usr_list.end(); itr++) {

		NW_User *nw_usr = (*itr);
		if(nw_usr->get_self_id() == msg->head.rcvr_id &&
		   nw_usr->get_peer_id() == msg->head.sndr_id) {

			ret_val = nw_usr->push_nw2usr(msg);
			if(ret_val)
				break;     /* Error */
                        
			return ret_val;
		}
	}
        
worker_func_exit2:
	delete[] udp_buf;

worker_func_exit1:

	return ret_val;
}

static void *work_thread(void *arg)
{
	while(!thread_exit) {
		if(worker_func())
			break;
	}
	return 0;
}

static int mod_init_count = 0;

/*------------------------------------------------------------------------------
 * Public API (s)
 *------------------------------------------------------------------------------
 */

void *agnss_comm_add_user(unsigned int self_id, unsigned int peer_id, 
                          const char* dpath)
{
	CPP_FTRACE3();

	NW_User *nw_usr = new NW_User(self_id, peer_id, dpath);
	if(!nw_usr)
		return NULL;

	nw_usr_list.push_back(nw_usr);

	return (void*) nw_usr;
}

int agnss_comm_rem_user(void *user_hnd)
{
	CPP_FTRACE3();

	list<NW_User*> :: iterator itr;
	int ret_val = -1;

	for(itr = nw_usr_list.begin(); itr != nw_usr_list.end(); itr++) {
		if((*itr) == (NW_User*) user_hnd) {
			nw_usr_list.remove(*itr);
			ret_val = 0;
			break;
		}
	}

	return ret_val;
}

int agnss_comm_send_msg(void *user_hnd, struct agnss_msg *o_msg,
                        bool wait4rsp_xact)
{
	CPP_FTRACE3();
	NW_User *nw_usr = (NW_User*) user_hnd;

	o_msg->head.sndr_id = nw_usr->get_self_id();
	o_msg->head.rcvr_id = nw_usr->get_peer_id();

	return nw_usr->send_usr2nw(o_msg, wait4rsp_xact);
}

struct agnss_msg *agnss_comm_recv_msg(void *user_hnd)
{
	CPP_FTRACE3();
	NW_User *nw_usr = (NW_User*) user_hnd;

	return nw_usr->recv_nw2usr();
}

struct agnss_msg *agnss_comm_wait_msg(void *user_hnd, unsigned int xact_id)
{
	CPP_FTRACE3();
	NW_User *nw_usr = (NW_User*) user_hnd;

	return nw_usr->wait_nw2usr(xact_id);
}

static pthread_t thread;

int agnss_comm_put2work(char *agnssthr_name)
{
	CPP_FTRACE3();

	thread_exit = 0;

	if(mod_init_count++ == 1) {
		if(pthread_create(&thread, NULL, work_thread, NULL)  != 0) { 
			return -1;			
		} 
			if(pthread_setname_np(thread,agnssthr_name)  != 0) { 
			ERR_NM("agnss_comm_put2work :pthread_setname_np failed");
		} 
	}
	return 0;
}


int agnss_comm_hlt_work(void *user_hnd)
{
	CPP_FTRACE3();
	return 0;
}

int agnss_comm_mod_exit(void)
{
	CPP_FTRACE3();
	if(!mod_init_count)
		return 0;

	while(nw_usr_list.size()) {

		NW_User *nw_usr = nw_usr_list.front();
		nw_usr_list.pop_front();
		delete nw_usr;
	}

	os_udp_sock_exit(sock_fd);

	return 0;        
}

int agnss_comm_mod_init(const char* local_path)
{
	CPP_FTRACE3();

	if(mod_init_count)
		return 0;

	/* Socket FD is created only once */
	sock_fd = os_udp_sock_init(local_path);
	if(-1 == sock_fd) {
		return -1;
	}
        
	mod_init_count++;
        
	return 0;
}


/* To Do a proper clean up */




