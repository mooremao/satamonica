/*
 * a-gnss_xact.h
 *
 * Add Description
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
 */

#ifndef _A_GNSS_XACT_H_
#define _A_GNSS_XACT_H_

#include "gnss_lcs_common.h"

#define TI_AGNSS_DIRECTORY_FPATH        "/mnt/userdata/gnss/"
#define TI_AGNSS_SOCKS_BASE_NAME        TI_AGNSS_DIRECTORY_FPATH "agnss_"

#define TI_AGNSS_CNECT_NAME             "connect"
#define TI_AGNSS_SPGPS_NAME             "sa-pgps"
#define TI_AGNSS_PFORM_NAME             "pfm-app"
#define TI_AGNSS_CPLNE_NAME             "c-plane"
#define TI_AGNSS_SUPLx_NAME             "supl-2x"

/*Network paths for unix domain sockets */
#define TI_AGNSS_CONNECT_NW_PATH    TI_AGNSS_SOCKS_BASE_NAME TI_AGNSS_CNECT_NAME
#define TI_AGNSS_SPGPS_PVDR_PATH    TI_AGNSS_SOCKS_BASE_NAME TI_AGNSS_SPGPS_NAME
#define TI_AGNSS_USER_PFORM_PATH    TI_AGNSS_SOCKS_BASE_NAME TI_AGNSS_PFORM_NAME
#define TI_AGNSS_CPLNE_PVDR_PATH    TI_AGNSS_SOCKS_BASE_NAME TI_AGNSS_CPLNE_NAME
#define TI_AGNSS_SUPLx_PVDR_PATH    TI_AGNSS_SOCKS_BASE_NAME TI_AGNSS_SUPLx_NAME

/* Application ID */
#define   APP_P_AND_SA_GPS_PVDR_ID        10
#define   APP_P_AND_SA_GPS_USER_ID        (APP_P_AND_SA_GPS_PVDR_ID + 1)

#define   APP_LCS_PLATFORM_PVDR_ID        (APP_P_AND_SA_GPS_USER_ID + 1)
#define   APP_LCS_PLATFORM_USER_ID        (APP_LCS_PLATFORM_PVDR_ID + 1)

#define   APP_LCS_NW_CPLNE_PVDR_ID        (APP_LCS_PLATFORM_USER_ID + 1)
#define   APP_LCS_NW_CPLNE_USER_ID        (APP_LCS_NW_CPLNE_PVDR_ID + 1)

#define   APP_LCS_NW_SUPLx_PVDR_ID        (APP_LCS_NW_CPLNE_USER_ID + 1)
#define   APP_LCS_NW_SUPLx_USER_ID        (APP_LCS_NW_SUPLx_PVDR_ID + 1)

#define   APP_LCS_CSTM_FWK_PVDR_ID        (APP_LCS_NW_SUPLx_USER_ID + 1)
#define   APP_LCS_CSTM_FWK_USER_ID        (APP_LCS_CSTM_FWK_USER_ID + 1)

int agnss_xact_mod_init(const char* listen_path, char *agnssThr_name);
int agnss_xact_mod_exit(void *hnd);


/* Creates a RX thread to block on RPC and must be called once in a process */
int agnss_xact_put2work(char *agnssthr_name);
int agnss_xact_hlt_work(void *hnd);

int agnss_xact_rem_user(void *user_hnd);

/* Loop to process user specific packets from Comm Module */
int agnss_xact_lup2proc(void *user_hnd); 
int agnss_xact_stop_lup(void *user_hnd); /* MUST BE IMPLEMENTED for proper Clean-up */

/* 
   self_id: Application ID of this process
   peer_id: Application ID of peer process
*/

void *agnss_xact_add_node(agnss_lcs_user_ops& if_ops,   
                          const agnss_lcs_pvdr_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath);

void *agnss_xact_add_node(agnss_lcs_pvdr_ops& if_ops,   
                          const agnss_lcs_user_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath);

void *agnss_xact_add_node(lc_assist_pvdr_ops& if_ops,   
                          const lc_assist_user_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath);

void *agnss_xact_add_node(lc_assist_user_ops& if_ops,   
                          const lc_assist_pvdr_ops& cb_ops, 
                          unsigned int self_id, unsigned int peer_id, 
                          const char* dpath);


#endif
