#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <mqueue.h>
#include "lcm_msg.h"

static mqd_t mq;

static int use_fb = 0;

int lcm_client_open(void)
{
	int count = 0;
	
	do
	{
		mq = mq_open(QUEUE_NAME, O_WRONLY);
		if (mq < 0)
		{
			sleep(0.5);
		}
		count++;
	} while( count < 20 );
	
	if( mq < 0)
		return -1;
		
	use_fb = 1;
	
	return 0;
}


int lcm_client_clear_all(void)
{
	unsigned char buffer[MSG_SIZE];
	
	if(!use_fb)
		return 0;
			
	t_LCM_DispayMsg *msg_p = (t_LCM_DispayMsg *) &buffer[0];

	msg_p->msg_id = MSG_ID_LCM_DISPLAY_CLEAR_ALL;
		
	return mq_send(mq, ( const char *) &buffer[0], MSG_SIZE, 0);
}

int lcm_client_draw_text(char * text, unsigned char cursor_pos_x, unsigned char cursor_pos_y)
{
	unsigned char buffer[MSG_SIZE];
	
	if(!use_fb)
		return 0;
			
	t_LCM_DispayMsg *msg_p = (t_LCM_DispayMsg *) &buffer[0];
	
	msg_p->msg_id = MSG_ID_LCM_DISPLAY_DRAW;
	msg_p->msg_data.msg_data_draw.cursor_pos_x = cursor_pos_x;
	msg_p->msg_data.msg_data_draw.cursor_pos_y = cursor_pos_y;
	
	memcpy(&msg_p->msg_data.msg_data_draw.text, (void *) text, MAX_CHARS_PAR_LINE );	
	msg_p->msg_data.msg_data_draw.text[MAX_CHARS_PAR_LINE ] =0;
	
	return mq_send(mq,  ( const char *) &buffer[0], MSG_SIZE, 0);
}


int lcm_client_clear_text(unsigned char cursor_pos_x, unsigned char cursor_pos_y, unsigned char num_chars)
{
	unsigned char buffer[MSG_SIZE];
	
	if(!use_fb)
		return 0;
			
	t_LCM_DispayMsg *msg_p = (t_LCM_DispayMsg *) &buffer[0];
	
	msg_p->msg_id = MSG_ID_LCM_DISPLAY_CLEAR;
	msg_p->msg_data.msg_data_clear.cursor_pos_x = cursor_pos_x;
	msg_p->msg_data.msg_data_clear.cursor_pos_y = cursor_pos_y;
	msg_p->msg_data.msg_data_clear.num_chars = num_chars;
	
	return mq_send(mq, ( const char *)  &buffer[0], MSG_SIZE, 0);
}

int lcm_client_send_result(int is_pass)
{
	unsigned char buffer[MSG_SIZE];
	
	if(!use_fb)
		return 0;
			
	t_LCM_DispayMsg *msg_p = (t_LCM_DispayMsg *) &buffer[0];
	
	msg_p->msg_id = MSG_ID_LCM_DISPLAY_TEST_RESULT;
	msg_p->msg_data.msg_data_result.is_pass = is_pass;

	return mq_send(mq, ( const char *)  &buffer[0], MSG_SIZE, 0);
}

int lcm_client_terminate_server(void)
{
	unsigned char buffer[MSG_SIZE];
	
	if(!use_fb)
		return 0;
			
	t_LCM_DispayMsg *msg_p = (t_LCM_DispayMsg *) &buffer[0];
	
	msg_p->msg_id = MSG_ID_LCM_DISPLAY_EXIT;
	
	return mq_send(mq, ( const char *)  &buffer[0], MSG_SIZE, 0);
}



void lcm_client_close(void)
{
	if(!use_fb)
		return;	
		
  lcm_client_terminate_server();
  
	CHECK((mqd_t)-1 != mq_close(mq));
}

void lcm_client_sync_fb(void)
{
	if(!use_fb)
		return;
			
}