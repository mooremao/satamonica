#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include "test_result.h"
#include "lcm_client.h"

#define NON_CONFIG_FILE_PASS_CN 35


#define MAX_ALLOWED_CHAMBER_TESTTIME_SEC 120

#define CHAMBER_START_AVG_SEC (multich_result_obj.TTFF+1)
#define OPENSKY_START_AVG_SEC (multich_result_obj.TTFF+120)
#define OPENSKY_END_SEC (multich_result_obj.TTFF+240)

#define MIN_COUNT_PER_SV 10
#define MAX_FIX_TIME 60
#define MAX_TEST_TIME_FIXONLY 120

#define TTCI_RW_PATH "/mnt/userdata/ttci/"

#define GNSS_CH_LOG_FILE TTCI_RW_PATH"gpschamber.log"
#define TESTMODE_LOG_FILE TTCI_RW_PATH"testmode4.log"
#define OPENSKY_LOG_FILE TTCI_RW_PATH"gpstest.log"
#define TTFF_LOG_FILE TTCI_RW_PATH"ttff.log"

static t_TestResult_MultiCH multich_result_obj;
static t_TestResult_TestMode testmode4_result_obj;
static int GPSBetterCN = 45,GPSGoodCN=42,GLOBetterCN=44,GLOGoodCN=41,ReasonableCN=39;
static int MinGNSSSatID=65;
static int t_TestResult_MultiCH_CheckTestCanFinish();
static int t_TestResult_MultiCH_CheckTestCanFinishByDetectedPRN();

void t_TestResult_MultiCH_init(eMultiChTestMode mode, int useFB)
{
	printf("%s:mode %d\n",__FUNCTION__,mode);
	
	memset(&multich_result_obj, 0x00, sizeof(multich_result_obj));
	multich_result_obj.MinCountPerSV = 10;
	multich_result_obj.mode = mode;
	multich_result_obj.useFB = useFB;
		
//	system("mkdir -p /opt/ipnc/test/");
	if(multich_result_obj.useFB)
	{
		if (lcm_client_open() < 0)
		{
			printf("can't init lcm queue");
		//exit(1);
		}
	}
	
	if(multich_result_obj.mode == eMultiChTestMode_Chamber )
	{
		unlink(GNSS_CH_LOG_FILE);

		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("Chamber",0,1);
		lcm_client_sync_fb();
  }		
	else if(multich_result_obj.mode == eMultiChTestMode_OpenSky )
	{
		unlink(OPENSKY_LOG_FILE);	

		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("Opensky",0,1);
		lcm_client_sync_fb();
	}
  else if(multich_result_obj.mode == eMultiChTestMode_FixOnly )
	{
		unlink(TTFF_LOG_FILE);

		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("FixOnly",0,1);
		lcm_client_sync_fb();		
  }	
  else if(multich_result_obj.mode == eMultiChTestMode_HibernateOnly )
	{
		lcm_client_clear_all();	
		lcm_client_draw_text("GNSS",0,0);
		lcm_client_draw_text("Hiber,,,",0,1);
		lcm_client_sync_fb();		
  }	  
  else if(multich_result_obj.mode == eMultiChTestMode_Endless )
	{
		lcm_client_clear_all();	
		lcm_client_draw_text("Init",0,0);
		lcm_client_draw_text("EndLess",0,1);
		lcm_client_sync_fb();	
  }	  
}


void t_TestResult_deinit()
{	
	
	lcm_client_close();
		
}

void t_TestResult_MultiCH_CleartCurrentSVStatus()
{
		int j;
			
		for(j=0;j<MaxSatellitesID;j++)
    {
    				multich_result_obj.SNR_current[j] = 0;
    }		
}

void t_TestResult_MultiCH_SVStatus(int satnr, int snr)
{
	int j;
    	
  if( satnr >= MaxSatellitesID )
  	return;
  	
  //printf("%s:satnr %d, snr %d\n",__FUNCTION__,satnr,snr);
  multich_result_obj.SNR_current[satnr] = snr;
  
	if(multich_result_obj.start_average)
	{
				multich_result_obj.SNR_total[satnr]+=snr;
    		multich_result_obj.SNR_count[satnr]++;
    		
		    for(j=0;j<MaxSatellitesID;j++)
    		{
    			if(multich_result_obj.SNR_count[j]!=0)
    				multich_result_obj.SNR_average[j] = multich_result_obj.SNR_total[j]/multich_result_obj.SNR_count[j];
    		}	
	}
		
}

static void t_TestResult_PlayEndSound()
{
	char cmd[256];
	
	sprintf(cmd,"echo 2700 > /sys/devices/platform/pwm-beeper/tune;sleep 2");
	system(cmd);
	sprintf(cmd,"echo 0 > /sys/devices/platform/pwm-beeper/tune");
	system(cmd);	
}

static void t_TestResult_MultiCH_DisplayResult() 
{
	lcm_client_send_result(multich_result_obj.bResult);
	lcm_client_clear_all();	
	lcm_client_draw_text("Test",0,0);
	if(multich_result_obj.bResult)
		lcm_client_draw_text("Pass",0,1);	
	else
		lcm_client_draw_text("FAIL",0,1);	
	
	lcm_client_sync_fb();
	
	t_TestResult_PlayEndSound();
}

int t_TestResult_MultiCH_TestCanExit()
{
	return multich_result_obj.TestCanExit;
}

static void t_TestResult_MultiCH_SetStartAvg()
{
		if(!multich_result_obj.IsFix)
			return;
			
		if(multich_result_obj.mode == eMultiChTestMode_Chamber )
		{
			if(multich_result_obj.time_count >= CHAMBER_START_AVG_SEC )
    		 multich_result_obj.start_average = 1;
    }
		if(multich_result_obj.mode == eMultiChTestMode_OpenSky )
		{
			if(multich_result_obj.time_count >= OPENSKY_START_AVG_SEC )
    		 multich_result_obj.start_average = 1;
    }        	 	
}

static void t_TestResult_MultiCH_GetSimpleSVStat(int * MaxCN, int *numGPS, int *numGLO)
{
		int i;
			
		*MaxCN = 0; *numGPS = 0;*numGLO = 0;
		
		for(i=0;i<MaxSatellitesID;i++)
    {
    	if(multich_result_obj.SNR_current[i] > 0)
    	{
    		if(i>=MinGNSSSatID)
    		{
    			(*numGLO)++;
    		}
    		else
    		{
    			(*numGPS)++;
    		}
    		if(multich_result_obj.SNR_current[i]>*MaxCN)
    			*MaxCN = multich_result_obj.SNR_current[i];
    	}
    }		
}

void t_TestResult_MultiCH_UpdateFixInfo(int time_count,int isFix, int TTFF, int latitude, int longitude)
{
	char lcd_text[20];
	
	int MaxCN,numGPS,numGLO;
	
	if(multich_result_obj.mode == eMultiChTestMode_HibernateOnly )
		return;
		
	multich_result_obj.IsFix = isFix;
	multich_result_obj.time_count = time_count;
			
	lcm_client_clear_all();
	
	if(isFix)
		snprintf(lcd_text, sizeof(lcd_text),"%ds TTFF:%d",time_count,TTFF);
	else
		snprintf(lcd_text, sizeof(lcd_text),"%ds nofix",time_count);
	lcm_client_draw_text(lcd_text, 0, 0);

  t_TestResult_MultiCH_GetSimpleSVStat(&MaxCN,&numGPS,&numGLO);

	snprintf(lcd_text, sizeof(lcd_text),"MCN:%d,GP:%d,GL:%d",MaxCN,numGPS,numGLO);
	lcm_client_draw_text(lcd_text, 0, 1);
  
	//printf("%s:time_count %d, isFix %d, TTFF %d, latitude %d, longitude %d \n",__FUNCTION__,time_count,isFix,TTFF,latitude,longitude);
	
	if(multich_result_obj.IsFix && multich_result_obj.IsFirstFix == 0 )
	{
		multich_result_obj.IsFirstFix = 1;
	  multich_result_obj.TTFF = TTFF;
	}
	if(multich_result_obj.IsFix)
	{
			t_TestResult_MultiCH_SetStartAvg();
    	multich_result_obj.latitude = latitude;
    	multich_result_obj.longitude = longitude;
	}
	
	if(t_TestResult_MultiCH_CheckTestCanFinish())
	{
		multich_result_obj.TestFinished = 1;
		multich_result_obj.TestCanExit = 1;
	}
	if(multich_result_obj.TestFinished)
	{		
		t_TestResult_MultiCH_Log2File();
		multich_result_obj.TestCanExit = 1;
		t_TestResult_MultiCH_DisplayResult();		
	}	
	
	lcm_client_sync_fb();

}

static int t_TestResult_MultiCH_CheckTestCanFinishOpenSky()
{
	  int elapsed_sec = multich_result_obj.time_count;
	  
		if(!multich_result_obj.IsFix && elapsed_sec>=MAX_FIX_TIME)
			return 1;
		if(elapsed_sec>=OPENSKY_END_SEC)
			return 1;
			
		return 0;
}

static int t_TestResult_MultiCH_CheckTestCanFinishFixOnly()
{
		int elapsed_sec = multich_result_obj.time_count;
		
	  if( multich_result_obj.IsFix)
	  	return 1;
	  else if(elapsed_sec >= MAX_TEST_TIME_FIXONLY )
	  	return 1;
	  return 0;
}

static int t_TestResult_MultiCH_CheckTestCanFinish()
{
	if(multich_result_obj.mode == eMultiChTestMode_Chamber)
		return t_TestResult_MultiCH_CheckTestCanFinishByDetectedPRN(); //return t_TestResult_MultiCH_CheckTestCanFinishByPrn7OrPrn8AndPrn74();
	if(multich_result_obj.mode == eMultiChTestMode_OpenSky)
		return t_TestResult_MultiCH_CheckTestCanFinishOpenSky();
	if(multich_result_obj.mode == eMultiChTestMode_FixOnly)
		return t_TestResult_MultiCH_CheckTestCanFinishFixOnly();
	if(multich_result_obj.mode == eMultiChTestMode_Endless)
		return 0;
				
	return 0;
		
}

static int t_TestResult_MultiCH_CheckTestCanFinishByDetectedPRN()
{
	int i;
	int svid;
	int ret = 0;
	int elapsed_sec = multich_result_obj.time_count;
	
	if(elapsed_sec>=MAX_ALLOWED_CHAMBER_TESTTIME_SEC)
		return 1;
		
	if(multich_result_obj.IsFix)
	{
		printf("t_TestResult_MultiCH_CheckTestCanFinish num_check_prn %d\n",multich_result_obj.num_check_prn);
		for(i=0; i<multich_result_obj.num_check_prn; i++)
		{
			printf("t_TestResult_MultiCH_CheckTestCanFinish check_prn %d\n",multich_result_obj.check_prn[i]);
			svid = multich_result_obj.check_prn[i];
			if(svid > 0 && svid < MaxSatellitesID )
			{
				if(multich_result_obj.SNR_average[svid] >= NON_CONFIG_FILE_PASS_CN
					&& multich_result_obj.SNR_count[svid]>=multich_result_obj.MinCountPerSV )
				{
					ret = 1;
				}
				else
				{
					ret = 0;
					break;
				}
			}
			else
			{
				ret = 0;
				break;
			}
		}
	}
	else
	{ // not fix yet

		if(elapsed_sec>MAX_FIX_TIME)
			ret = 1; //means already failed		
	}		
	return ret;	
}	


static int t_TestResult_MultiCH_CheckTestCanFinishByPrn7OrPrn8AndPrn74()
{
	int index;
	int elapsed_sec = multich_result_obj.time_count;
	
	if(elapsed_sec>=MAX_ALLOWED_CHAMBER_TESTTIME_SEC)
		return 1;
		
	if(multich_result_obj.IsFix)
	{							
			if( ( (multich_result_obj.SNR_average[7]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[7]>=multich_result_obj.MinCountPerSV)
				|| (multich_result_obj.SNR_average[8]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[8]>=multich_result_obj.MinCountPerSV) )
				&& (multich_result_obj.SNR_average[74]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[74]>=multich_result_obj.MinCountPerSV)
				)
				return 1;			
			else
				return 0;
		
	}
	else
	{ // not fix yet
		if(elapsed_sec>MAX_FIX_TIME)
			return 1; //means already failed		
	}		
	return 0;	
}	

static int t_TestResult_MultiCH_CheckResultbyPrn7OrPrn8AndPrn74(void)
{	  
	  int result;
	  
		if( ( (multich_result_obj.SNR_average[7]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[7]>=multich_result_obj.MinCountPerSV)
				|| (multich_result_obj.SNR_average[8]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[8]>=multich_result_obj.MinCountPerSV) )
				&& (multich_result_obj.SNR_average[74]>=NON_CONFIG_FILE_PASS_CN && multich_result_obj.SNR_count[74]>=multich_result_obj.MinCountPerSV)
				)
				result =  1;
		else
				result =  0;	
		return result;		
}

static int t_TestResult_MultiCH_CheckResultDetectedPRN(void)
{	  
	  int result = 0;
	  int i;
	  int svid;

		printf("multich_result_obj.num_check_prn %d\n",multich_result_obj.num_check_prn);

		for(i=0; i<multich_result_obj.num_check_prn; i++)
		{
			svid = multich_result_obj.check_prn[i];
			printf(" check svid %d\n",svid );
			if(svid > 0 && svid < MaxSatellitesID )
			{
				if(multich_result_obj.SNR_average[svid] >= NON_CONFIG_FILE_PASS_CN
					&& multich_result_obj.SNR_count[svid]>=multich_result_obj.MinCountPerSV )
				{
					result = 1;
				}
				else
				{
					result = 0;
					break;
				}
			}
			else
			{
				result = 0;
				break;
			}
		}
	
		return result;		
}

static int t_TestResult_MultiCH_CheckOpenSkyResult(void)
{
	int i;
  int nr_gps_better= 0, nr_glo_better= 0;
  int nr_gps_good= 0, nr_glo_good= 0;
  int nr_reasonable= 0;
  int Result;
      	
	for( i= 0; i< MaxSatellitesID; i++)
	{
		if( multich_result_obj.SNR_count[i]> 20)
		{
         if( i >= MinGNSSSatID)
         {
         		if( multich_result_obj.SNR_average[i]>= GLOBetterCN) nr_glo_better++;
            if( multich_result_obj.SNR_average[i]>= GLOGoodCN) nr_glo_good++;
         }
         else
       	 {
            if( multich_result_obj.SNR_average[i]>= GPSBetterCN) nr_gps_better++;
            if( multich_result_obj.SNR_average[i]>= GPSGoodCN) nr_gps_good++;
         }
         if( multich_result_obj.SNR_average[i]>= ReasonableCN) nr_reasonable++;			
		}
		
	}
    if( nr_gps_good >= 4 && nr_gps_better >= 1 &&
        nr_glo_good >= 3 && nr_glo_better >= 1)
    {
        Result= 1;
    }
    else
    {
        Result= 0;
    }
    return Result;	
}

int t_TestResult_MultiCH_Chamber_Log2File()
{
	int i;
	FILE * pFile;
	float lat, lon;
	char strNSind[2],strEWind[2];

	multich_result_obj.bResult = t_TestResult_MultiCH_CheckResultDetectedPRN();
	
	pFile = fopen (GNSS_CH_LOG_FILE,"w");
	if(pFile == NULL )
	{
		printf(" log file open error %s\n",GNSS_CH_LOG_FILE);
		return -1;
	}	
	fprintf(pFile,"[General]\r\n");
	
	lat = (float)(multich_result_obj.latitude)/10000000;
	lon = (float)(multich_result_obj.longitude)/10000000;
	
	if(lat > 0)
	{
		strcpy(strNSind,"N");
	}
	else
	{
		lat = lat*(-1);
		strcpy(strNSind,"S");
	}
	
	if(lon > 0)
	{
		strcpy(strEWind,"E");
	}
	else
	{
		lon = lon*(-1);
		strcpy(strEWind,"W");
	}
		
	fprintf(pFile,"Latitude=%f%s\r\n",lat,strNSind);
	fprintf(pFile,"Longitude=%f%s\r\n",lon,strEWind);
	
	for(i=1;i<=32;i++)
	{
		if(i<=9)
			fprintf(pFile,"PRN0%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
		else
			fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	for(i=65;i<=96;i++)
	{
		fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	fprintf(pFile,"Result=%d\r\n",multich_result_obj.bResult);
	fprintf(pFile,"TTFF=%d\r\n",multich_result_obj.TTFF);
	fclose(pFile);


	
	return 0;
}


int t_TestResult_MultiCH_OpenSky_Log2File()
{
	int i;
	FILE * pFile;
	float lat, lon;
	char strNSind[2],strEWind[2];

	multich_result_obj.bResult = t_TestResult_MultiCH_CheckOpenSkyResult();
	
	pFile = fopen (OPENSKY_LOG_FILE,"w");
	if(pFile == NULL )
	{
		printf(" log file open error %s\n",OPENSKY_LOG_FILE);
		return -1;
	}	
	fprintf(pFile,"[General]\r\n");
	
	lat = (float)(multich_result_obj.latitude)/10000000;
	lon = (float)(multich_result_obj.longitude)/10000000;
	
	if(lat > 0)
	{
		strcpy(strNSind,"N");
	}
	else
	{
		lat = lat*(-1);
		strcpy(strNSind,"S");
	}
	
	if(lon > 0)
	{
		strcpy(strEWind,"E");
	}
	else
	{
		lon = lon*(-1);
		strcpy(strEWind,"W");
	}
		
	fprintf(pFile,"Latitude=%f%s\r\n",lat,strNSind);
	fprintf(pFile,"Longitude=%f%s\r\n",lon,strEWind);
	
	for(i=1;i<=32;i++)
	{
		if(i<=9)
			fprintf(pFile,"PRN0%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
		else
			fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	for(i=65;i<=96;i++)
	{
		fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_average[i]);
	}
	fprintf(pFile,"Result=%d\r\n",multich_result_obj.bResult);
	fprintf(pFile,"TTFF=%d\r\n",multich_result_obj.TTFF);
	fclose(pFile);

	return 0;
}

static int t_TestResult_FixOnly_Log2File()
{
	int i;
	FILE * pFile;
	float lat, lon;
	char strNSind[2],strEWind[2];

	multich_result_obj.bResult = multich_result_obj.IsFix;
	
	pFile = fopen (TTFF_LOG_FILE,"w");
	if(pFile == NULL )
	{
		printf(" log file open error %s\n",TTFF_LOG_FILE);
		return -1;
	}	
	fprintf(pFile,"[General]\r\n");
	
	lat = (float)(multich_result_obj.latitude)/10000000;
	lon = (float)(multich_result_obj.longitude)/10000000;
	
	if(lat > 0)
	{
		strcpy(strNSind,"N");
	}
	else
	{
		lat = lat*(-1);
		strcpy(strNSind,"S");
	}
	
	if(lon > 0)
	{
		strcpy(strEWind,"E");
	}
	else
	{
		lon = lon*(-1);
		strcpy(strEWind,"W");
	}
		
	fprintf(pFile,"Latitude=%f%s\r\n",lat,strNSind);
	fprintf(pFile,"Longitude=%f%s\r\n",lon,strEWind);
	
	for(i=1;i<=32;i++)
	{
		if(i<=9)
			fprintf(pFile,"PRN0%d=%d\r\n",i,multich_result_obj.SNR_current[i]);
		else
			fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_current[i]);
	}
	for(i=65;i<=96;i++)
	{
		fprintf(pFile,"PRN%d=%d\r\n",i,multich_result_obj.SNR_current[i]);
	}
	fprintf(pFile,"Result=%d\r\n",multich_result_obj.bResult);
	fprintf(pFile,"TTFF=%d\r\n",multich_result_obj.TTFF);
	fclose(pFile);

	return 0;
}

int t_TestResult_MultiCH_Log2File()
{
	if(multich_result_obj.mode == eMultiChTestMode_Chamber)
		t_TestResult_MultiCH_Chamber_Log2File();
	if(multich_result_obj.mode == eMultiChTestMode_OpenSky)
		t_TestResult_MultiCH_OpenSky_Log2File();
	if(multich_result_obj.mode == eMultiChTestMode_FixOnly)
		t_TestResult_FixOnly_Log2File();			
}

void t_TestResult_TestMode_init(int useFB)
{
	
	unlink(TESTMODE_LOG_FILE);
	memset(&testmode4_result_obj, 0x00, sizeof(testmode4_result_obj));
	testmode4_result_obj.max_count = 0;
	
	testmode4_result_obj.useFB = useFB;
	
	if(!testmode4_result_obj.useFB )
		return;
		
	if (lcm_client_open() < 0)
	{
		printf("can't init lcm queue");
		//exit(1);
	}
	
	lcm_client_clear_all();
	lcm_client_draw_text("Init",0,0);
	lcm_client_draw_text("TestMod",0,1);
//	lcm_api_sync_fb();
}

static void t_TestResult_TestMode_DisplayResult(void) 
{
	lcm_client_clear_all();
	lcm_client_draw_text("Test",0,0);
	lcm_client_draw_text("Done",0,1);	
}


int t_TestResult_TestMode_Write2Log(char * string)
{
	FILE * pFile;
	
	pFile = fopen (TESTMODE_LOG_FILE,"a");
	if(pFile == NULL )
	{
		printf(" log file open error %s\n",TESTMODE_LOG_FILE);
		return -1;
	}
	fprintf(pFile,string);
	fclose(pFile);
	
	
}



int t_TestResult_TestMode_UpdateResult(char *testresult)
{

	t_TestResult_TestMode_Write2Log(testresult);
			
	testmode4_result_obj.time_count++;
	
}

int t_TestResult_TestMode_TestCanExit()
{
	if( testmode4_result_obj.time_count >= testmode4_result_obj.max_count )
	{
		t_TestResult_TestMode_DisplayResult();
//		lcm_api_sync_fb();
		return 1;
	}
		
	return 0;
}

void t_TestResult_MultiCH_SetExitDetectedPRN(int *svid, int num_svid)
{
	int i;
	
	if(num_svid > MAX_CHECK_PRN )
	{
		printf("%s:num_svid %d too many\n",__FUNCTION__,num_svid);
		return;
	}

	for(i=0;i<num_svid;i++)
	{
		multich_result_obj.check_prn[i] = svid[i];
	}
	//printf("%s:num_svid %d\n",__FUNCTION__, num_svid );
	multich_result_obj.num_check_prn = num_svid;
}