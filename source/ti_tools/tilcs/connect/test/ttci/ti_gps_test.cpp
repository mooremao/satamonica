#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <Ftrace.h>
#include <math.h>
#include <sys/time.h>
#include <fcntl.h>
#include "ti_agnss_lcs.h"
#include "gnss.h"
#include <stdint.h>
#include <signal.h>
#include <semaphore.h>
#include "test_result.h"
#define GPS_MAX_SVS 32
#define PI (3.14159265359)

struct timeval g_oldtime;
struct timeval g_newtime;
#define RESPTYP_WIDEBAND_SNR 	0x04
#define RESPTYP_NARROWBAND_SNR 	0x05
#define RESPTYP_TCXO_OFFSET		0x06
#define RESPTYP_NOISE_FIGURE 	0x07

#define COLD_START 1
#define HOT_START 2
#define WARM_START 3

static bool g_log_enb = 1;
static int aQuit=0;
static int ttff =0;

static	void * ghnd;

static int coldstart = 0, warmstart = 0, hotstart = 0;
/* test_mode bit 0 SNR mode, bit 1 TCXO offset test. bit 2 noise figure
*/
static int test_mode = 0, chamber_mode=0, open_sky_mode = 0 , fix_only_mode = 0, has_display=1,del_only_mode = 0;
static int svid[MAX_CHECK_PRN]={0,0,0,0};
static int svid_num = 0;
static int pipe_cmd_fd =-1;
#define DBG_LOG if(g_log_enb)printf
typedef int64_t GpsUtcTime;
/** Represents a location. */
typedef struct {
    /** set to sizeof(GpsLocation) */
    size_t          size;
    /** Contains GpsLocationFlags bits. */
    unsigned short        flags;
    /** Represents latitude in degrees. */
    double          latitude;
    /** Represents longitude in degrees. */
    double          longitude;
    /** Represents altitude in meters above the WGS 84 reference
     * ellipsoid. */
    double          altitude;
    /** Represents speed in meters per second. */
    float           speed;
    /** Represents heading in degrees. */
    float           bearing;
    /** Represents expected accuracy in meters. */
    float           accuracy;
    /** Timestamp for the location fix. */
    GpsUtcTime      timestamp;
} GpsLocation;

static	GpsLocation 		  gpsLocation;
	
/** Represents SV information. */
typedef struct {
    /** set to sizeof(GpsSvInfo) */
    size_t          size;
    /** Pseudo-random number for the SV. */
    int     prn;
    /** Signal to noise ratio. */
    float   snr;
    /** Elevation of SV in degrees. */
    float   elevation;
    /** Azimuth of SV in degrees. */
    float   azimuth;
	unsigned char meas_qual;
	unsigned short meas_flags;
} GpsSvInfo;

/** Represents SV status. */
typedef struct {
    /** set to sizeof(GpsSvStatus) */
    size_t          size;

    /** Number of SVs currently visible. */
    int         num_svs;

    /** Contains an array of SV information. */
    GpsSvInfo   sv_list[GPS_MAX_SVS];

    /** Represents a bit mask indicating which SVs
     * have ephemeris data.
     */
    unsigned int    ephemeris_mask;

    /** Represents a bit mask indicating which SVs
     * have almanac data.
     */
    unsigned int    almanac_mask;

    /**
     * Represents a bit mask indicating which SVs
     * were used for computing the most recent position fix.
     */
    unsigned int    used_in_fix_mask;
} GpsSvStatus;


static int loc_results(void *hnd, const struct gnss_pos_result& pos_result, 
                       const struct gps_msr_result& gps_result, 
                       const struct gps_msr_info *gps_msr, 
                       const struct ganss_msr_result& ganss_result, 
                       const struct ganss_msr_desc *ganss_msr)
{
       DBG_LOG("loc_results \n");

        return 0;
}


static int need_assist(void *hnd, const struct gps_assist_req& gps_assist,
			   const struct ganss_assist_req& ganss_assist,
			   const struct assist_reference& assist_usr)		           
{
	//CPP_FTRACE1();
	DBG_LOG(" [LCS USER] need_assist\n");
	return 0;
}



static int decoded_aid(void *hnd, enum loc_assist assist,
                       const void *assist_array, int num)
{




        return 0;
}


/*   ti_ue_pos_report  location_rpt */
static int dev_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
                           const struct gps_msr_result&    gps_result, 
                           const struct gps_msr_info      *gps_msr,
                           const struct ganss_msr_result&  ganss_result,
                           const struct ganss_msr_info   *ganss_msr)
{
	GpsLocation *h_loc = &gpsLocation;
	#define PYTHAGORAS_VAL(x,y) sqrt((x*x) + (y*y))
	#define EAST_ER 	      (0.1)
	#define NORTH_ER	      (0.1)
	unsigned int flags = POS_ERR_REPORTED | POS_NOT_REPORTED;

	float v_unc = 0.0 ,e_unc = 0.0, n_unc = 0.0, vup_unc=0.0;
	float hdop = 0.0, pdop = 0.0, vdop=0.0, tdop= 0.0;

	if( pos_result.contents & flags){
		return 0;
	 }



        const struct location_desc *location = &pos_result.location;
        
	 h_loc->size = sizeof(GpsLocation);
         if((location->lat_deg_by_2p8 == 0)&&(location->lon_deg_by_2p8 == 0)){

	 h_loc->latitude   = (double)location->latitude_N * 90.0f/8388608.0f;

	 if(location->lat_sign == 1 )	 	
	 	h_loc->latitude = -h_loc->latitude;
		 
	 h_loc->longitude  = (double)location->longitude_N * 360.0f/16777216.0f;
     //   h_loc->flags     |= GPS_LOCATION_HAS_LAT_LONG;
        h_loc->altitude   = (double)0;

        }else{
	h_loc->latitude = (double) location->lat_deg_by_2p8 * (180 / pow(2,32));
	h_loc->longitude = (double) location->lon_deg_by_2p8 * (180 / pow(2,31));

//	h_loc->flags	 |= GPS_LOCATION_HAS_LAT_LONG;

	h_loc->altitude   = (double)0;
        }

#define UNC_MAP_K_TO_R(unc_k, C, x) (double)(C * (pow(1+x, unc_k) - 1))
        
	double unc_east = UNC_MAP_K_TO_R(location->unc_semi_maj_K, 10.0f, 0.1f);
	double unc_north = UNC_MAP_K_TO_R(location->unc_semi_min_K,10.0f, 0.1f);
	h_loc->accuracy = (float) PYTHAGORAS_VAL(unc_east, unc_north);
	//h_loc->flags |= GPS_LOCATION_HAS_ACCURACY;				
        if(pos_result.location.shape == ellipsoid_with_v_alt ||
			pos_result.location.shape == ellipsoid_with_v_alt_and_e_unc) {
                h_loc->altitude  = (double)location->altitude_N;
//                h_loc->flags    |= GPS_LOCATION_HAS_ALTITUDE;
        }
        
        /* Include other fields as well. If done, remove this comment */

	#define PYTHAGORAS_VAL(x,y) sqrt((x*x) + (y*y))
                
        const struct vel_gnss_info *velocity = &pos_result.velocity;

		
        h_loc->bearing  = velocity->bearing;

		DBG_LOG("\tdev_loc_results: velocity Unc[%d]\n", pos_result.velocity.v_unc);
				
		v_unc= (float)(pos_result.velocity.v_unc * 1000.0/65535.0);

        h_loc->accuracy = (float) PYTHAGORAS_VAL(velocity->h_unc, v_unc );

	/* Include other fields as well. If done, remove this comment */
	if((h_loc->latitude != 0) && (h_loc->longitude != 0.0)){
				gettimeofday(&g_newtime, NULL);
		if(ttff == 0)
			ttff = g_newtime.tv_sec - g_oldtime.tv_sec;
		DBG_LOG("/**********************Position Info *****************/\n");
		DBG_LOG("\tTTFF [%d] Latitude  [%f] Longitude [%f]\n", ttff, h_loc->latitude,h_loc->longitude);
	}



	return 0;
}



static 
int dev_aux_info4sv(void *hnd, 
			const struct dev_gnss_sat_aux_desc&  gps_desc,
			const struct dev_gnss_sat_aux_info	 *gps_info,
			const struct dev_gnss_sat_aux_desc&  glo_desc,
			const struct dev_gnss_sat_aux_info	 *glo_info)

{
	GpsSvStatus h_sv[1];

	unsigned char n_sat;
	unsigned char i, j;
	unsigned char sbs_svs;
	//DBG_LOG("dev_aux_info4sv: gps num_sats	[%d]\n", gps_desc.num_sats);
	//DBG_LOG("dev_aux_info4sv: glo num_sats [%d]\n",glo_desc.num_sats);	

	h_sv->size = sizeof(GpsSvStatus);
	h_sv->num_svs = 0;
	h_sv->almanac_mask = 0;
	h_sv->ephemeris_mask = 0;
	h_sv->used_in_fix_mask = 0;
	gettimeofday(&g_newtime, NULL);

	n_sat = gps_desc.num_sats;

	//	DBG_LOG("RSSI Value = [%2f]\n", gps_info[0].rssi_dbm);
	t_TestResult_MultiCH_CleartCurrentSVStatus();

	for(i = 0; i < n_sat; i++) {

		h_sv->sv_list[i].size		 = sizeof(GpsSvStatus);

		h_sv->sv_list[i].prn	   = gps_info[i].prn;
		h_sv->sv_list[i].snr	   = (float)gps_info[i].snr_db_by10 / 10.0f;
		h_sv->sv_list[i].elevation = gps_info[i].elevate_deg;
		h_sv->sv_list[i].azimuth   = gps_info[i].azimuth_deg;
		h_sv->sv_list[i].meas_flags = gps_info[i].meas_flags;
		h_sv->sv_list[i].meas_qual = gps_info[i].meas_qual;
		DBG_LOG("SAT/CN:%2d/%2.1f ",
				h_sv->sv_list[i].prn,
				h_sv->sv_list[i].snr );
		if(i%6 == 5)
			DBG_LOG("\n");

	t_TestResult_MultiCH_SVStatus(h_sv->sv_list[i].prn,h_sv->sv_list[i].snr);
		h_sv->num_svs++;
	}
	h_sv->almanac_mask     = gps_desc.alm_bits;
	h_sv->ephemeris_mask   = gps_desc.eph_bits;
	h_sv->used_in_fix_mask = gps_desc.pos_bits;

	n_sat = glo_desc.num_sats + glo_desc.num_sbas_sats;

	for(unsigned char j = 0; ((i + j) < GPS_MAX_SVS) && (j < n_sat); j++) {

		h_sv->sv_list[i+j].size 	 = sizeof(GpsSvStatus);

		h_sv->sv_list[i+j].prn		 = glo_info[j].prn;
		h_sv->sv_list[i+j].snr		 = (float)glo_info[j].snr_db_by10 / 10.0f;
		h_sv->sv_list[i+j].elevation = glo_info[j].elevate_deg;
		h_sv->sv_list[i+j].azimuth	 = glo_info[j].azimuth_deg;
		h_sv->sv_list[i+j].meas_flags = glo_info[j].meas_flags;
		h_sv->sv_list[i+j].meas_qual = glo_info[j].meas_qual;
		DBG_LOG("SAT/CN:%2d/%2.1f ",
				h_sv->sv_list[i+j].prn,
				h_sv->sv_list[i+j].snr );
		if((i+j)%6 == 5)
			DBG_LOG("\n");
		t_TestResult_MultiCH_SVStatus(h_sv->sv_list[i+j].prn,h_sv->sv_list[i+j].snr);
		h_sv->num_svs ++;
	}
	if(gpsLocation.latitude!=0 &&gpsLocation.longitude!=0 )
		t_TestResult_MultiCH_UpdateFixInfo(g_newtime.tv_sec-g_oldtime.tv_sec,1,ttff,(int)(gpsLocation.latitude*10000000),(int)(gpsLocation.longitude*10000000));
	else
		t_TestResult_MultiCH_UpdateFixInfo(g_newtime.tv_sec-g_oldtime.tv_sec,0,0,0,0);

	aQuit = t_TestResult_MultiCH_TestCanExit();
//	hal_desc->gps_callbacks->sv_status_cb(h_sv);


	return 0;
}



/* ti_nmea_sentence  nmea_message */
int dev_nmea_rpt(void *hnd, enum nmea_sn_id nmea, const char *cb_data, int length)
{
	//DBG_LOG("dev_nmea_rpt: Entering \n");

	struct timeval tv;
	gettimeofday(&tv, NULL);
	GpsUtcTime timestamp = tv.tv_sec*1000+tv.tv_usec/1000;//
        switch(nmea) {

        /* GPS specific sentences */
        case gpgga:
        case gpgll:
        case gpgsa:
        case gpgsv:
        case gprmc:
        case gpvtg:
		case gngrs:
		case gngll:
		case gngsa:
		case gngns:
		case gnvtg:
		case gpgst:
		case gpgrs:
#ifdef PRINT_NMEA
			DBG_LOG("/**********************NMEA Info *****************/\n");
  			DBG_LOG("NMEA Output :\n \t\t%s\n", cb_data);
		  	DBG_LOG("/**********************NMEA Info *****************/\n");
#endif //PRINT_NMEA
                break;
                
        default:
                break;
        }


	return 0;
}


int dev_ue_recovery_ind(void *hnd, enum dev_reset_ind rstind, int num)
{

	DBG_LOG("dev_ue_recovery_ind: Entering!");
	switch(rstind){
		case gnss_no_meas_resp_ind :
				DBG_LOG("gnss_no_meas_resp_ind ");
			break;
		
		case gnss_no_reset_resp_ind:
				DBG_LOG("gnss_no_reset_resp_ind ");
			break;

	case gnss_hard_reset_ind:
			DBG_LOG("gnss_hard_reset_ind");
		break;

	case gnss_fw_fatal_ind:
			DBG_LOG("gnss_fw_fatal_ind ");
		break;
		default:
			break;
	}
	return 0;

}
int dev_cw_test_results(void *hnd, enum cw_test_rep 	cwt, 
						   const struct cw_test_report *cw_rep, int num)
{
	char result[512];
	char sub_result[100];
	DBG_LOG("dev_cw_test_results: Entering!");
	int i;

	DBG_LOG("RespType = %d\n", cw_rep->resp_type);

	switch(cw_rep->resp_type)
	{
		
		case RESPTYP_WIDEBAND_SNR 	:
				strcpy(result,"WB_SNR:Peak_Index/Peak_SNR\n");

				for(i = 0; i< cw_rep->wb_snr.peaks; i++){
					snprintf(sub_result,sizeof(sub_result),"%d/%f\n", cw_rep->wb_snr.wb_peak_index[i], cw_rep->wb_snr.wb_peak_SNR[i]);
					strcat(result,sub_result);
				}
		break;
		case RESPTYP_NARROWBAND_SNR :
				strcpy(result,"NB_SNR:Peak_Index/Peak_SNR\n");			
			
			snprintf(sub_result,sizeof(sub_result),"Center_Freq=%d\n", cw_rep->nb_snr.c_freq);
			strcat(result,sub_result);
			for(i = 0; i< cw_rep->nb_snr.peaks; i++){
				snprintf(sub_result,sizeof(sub_result),"%d/%f\n", cw_rep->nb_snr.nb_peak_index[i], cw_rep->nb_snr.nb_peak_SNR[i]);
				strcat(result,sub_result);
			}

		break;
		case RESPTYP_TCXO_OFFSET	:
				snprintf(result, sizeof(result),"TCXO_Offset=%f ppm\n", cw_rep->tcxo_offset);
		break;

		case RESPTYP_NOISE_FIGURE :
				snprintf(result, sizeof(result),"Noise_Fig=%f\n", cw_rep->noise_figure);
		break;
		
		
	}
	t_TestResult_TestMode_UpdateResult(result);
	aQuit = 1;
	return 0;

}


struct ti_agnss_lcs_if if_ops;



void* AgnssWorkerThread(void *arg)
{
	DBG_LOG( "AgnssWorkerThread: starting\n");

	ti_assisted_ue_work(arg);

	DBG_LOG("AgnssWorkerThread: exiting\n");
	return  0;
}

static void *ti_hnd;
static struct ti_lc_assist_if ti_lc_if[1];
static struct ti_lc_assist_cb ti_lc_cb[1];
pthread_t *thread_id;

static int agnss_reg_assist_src(void *hnd)
{
        ti_lc_cb->ue_need_assist = need_assist;
        ti_lc_cb->ue_decoded_aid = decoded_aid;
        ti_lc_cb->ue_loc_results = dev_loc_results;
        ti_lc_cb->ue_service_ctl = NULL;
        ti_lc_cb->decl_dev_param = NULL;
		DBG_LOG(" Entering agnss_reg_assist_src\n");
        if (!(ti_hnd = ti_assisted_ue_init(sa_or_pgps, ti_lc_if[0], ti_lc_cb[0]))) {
                DBG_LOG("agnss_reg_assist_src: failed.");
                return 0;
        } else {

                DBG_LOG("agnss_reg_assist_src: success.");
        }

		if(ti_hnd != NULL){
			if (pthread_create(thread_id, NULL, AgnssWorkerThread, ti_hnd) != 0)
			{
				DBG_LOG("AgnssWorker Thread start failure\n");
			}
		}
		

        return 1;
}

static int agnss_dereg_assist_src(void *hnd)
{
	if(ti_hnd)
		ti_assisted_ue_exit(ti_hnd);

	return 1;
}


static 
int handle_loc_start(void *hnd)
{
	gettimeofday(&g_oldtime, NULL);

	//CPP_FTRACE1();
	if(if_ops.loc_start)
		return if_ops.loc_start(hnd);
	
       return 0;
}

static 
int handle_loc_stop(void *hnd)
{
	//CPP_FTRACE1();
	if(if_ops.loc_stop)
		return if_ops.loc_stop(hnd);
	return 0;
}

static 
int handle_get_ue_agnss_caps(void *hnd)
{
	//CPP_FTRACE1();
	int result = -1;
	unsigned int gps_caps_bits = 1000; 
       struct ganss_ue_assist_caps ganss_caps;
	ganss_caps.n_ganss = 1;
	ganss_caps.common_bits = 101;

	int count = ganss_caps.n_ganss;

	while(count--){
		ganss_caps.ganss_caps[count].id_of_ganss = 1;
		ganss_caps.ganss_caps[count].caps_bitmap = 2;
		ganss_caps.ganss_caps[count].data_mdl.orb_model = 3;
		ganss_caps.ganss_caps[count].data_mdl.clk_model = 4;
		ganss_caps.ganss_caps[count].data_mdl.alm_model = 5;
		ganss_caps.ganss_caps[count].data_mdl.utc_model = 6;
	}

	if(if_ops.get_ue_agnss_caps)
		result = if_ops.get_ue_agnss_caps(hnd, gps_caps_bits, &ganss_caps);

	DBG_LOG(" [LCS USER] handle_get_ue_agnss_caps response ganss_caps.n_ganss[%d]\n",ganss_caps.n_ganss);

	count = ganss_caps.n_ganss;

	while(count--){
		DBG_LOG(" [LCS USER] id_of_ganss = %d\n", ganss_caps.ganss_caps[count].id_of_ganss);
		DBG_LOG(" [LCS USER] caps_bitmap = %d\n", ganss_caps.ganss_caps[count].caps_bitmap);
		DBG_LOG(" [LCS USER] orb_model = %d\n", ganss_caps.ganss_caps[count].data_mdl.orb_model);
		DBG_LOG(" [LCS USER] clk_model = %d\n", ganss_caps.ganss_caps[count].data_mdl.clk_model);
		DBG_LOG(" [LCS USER] alm_model = %d\n", ganss_caps.ganss_caps[count].data_mdl.alm_model);
		DBG_LOG(" [LCS USER] utc_model = %d\n\n", ganss_caps.ganss_caps[count].data_mdl.utc_model);
	}
	return result;
}

static 
int handle_set_loc_interval(void *hnd)
{
	//CPP_FTRACE1();
	return 1;
}

static 
int handle_add_assist(void *hnd)
{
	//CPP_FTRACE1();
	//enum loc_assist assist = a_GPS_ACQ;
	struct nw_assist_id assist;
	assist.type_select = NNATIVE;
	assist.gnss_icd_ie = a_GPS_EPH;
	int SIZE = 50;
    char *assist_array = new char[SIZE];
	sprintf(assist_array,"%s","get assist");
	int num = strlen( assist_array);
	if(if_ops.ue_add_assist)
		return if_ops.ue_add_assist(hnd, assist, (void *)assist_array, num);

	return 1;
	


}

static 
int handle_del_aiding(void *hnd, unsigned int val)
{
	//CPP_FTRACE1();
	


	enum loc_assist assist ;
	if(val == 14){
		assist = (enum loc_assist) (a_GPS_EPH | a_REF_POS | a_GPS_ALM | a_GLO_EPH | a_GPS_TIM |a_GLO_TIM | a_GLO_ALM);
	}
	else
		assist = (enum loc_assist)val;
	
    unsigned int sv_id_map = 0xffffffff;
    unsigned int mem_flags = 0xffffffff;


	if(val == COLD_START){ /*Delete all assistance*/
		if_ops.ue_del_aiding(hnd, a_GLO_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GLO_ALM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GLO_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_ALM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_REF_POS,
					0xffffffff, 0xffffffff);
	}
	if(val == WARM_START){	/*Delete ephemeris, time and position*/
		if_ops.ue_del_aiding(hnd, a_GLO_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GLO_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_REF_POS,
					0xffffffff, 0xffffffff);
	}
	
	return 1;

}

static 
int handle_get_aiding(void *hnd)
{
	//CPP_FTRACE1();
	int result = -1;
	enum loc_assist assist = a_GPS_EPH;
	char *arr = new char[20];
	sprintf(arr, "%s", "get aiding");
	int num = strlen(arr);
	struct gps_eph *eph = (struct gps_eph *)malloc(sizeof(struct gps_eph));

	if(if_ops.ue_get_aiding)
		result = if_ops.ue_get_aiding(hnd, assist, (char *)eph, num);
	switch (assist) {
	   case a_GPS_EPH:
			   {

				   DBG_LOG("###################Decoded GPS Ephemeris ######################## \n");
				   DBG_LOG("decoded_aid:\n gps eph svid [%d]\n, is_bcast [%d]\n, status [%d]\n, Code_on_L2 [%d]\n, Ura [%d]\n, Health [%d]\n, IODC [%d]\n, Tgd [%d]\n, Toc [%d]\n, Af2 [%d]\n, Af1 [%d]\n, Af0 [%d]\n, Crs [%d]\n, DeltaN [%d]\n, Mo [%d]\n, Cuc [%d]\n, E [%d]\n, Cus [%d]\n, SqrtA [%d]\n, Toe [%d]\n, FitIntervalFlag [%d]\n, Aodo [%d]\n, Cic [%d]\n, Omega0 [%d]\n, Cis [%d]\n, Io [%d]\n, Crc [%d]\n, Omega [%d]\n, OmegaDot [%d]\n, Idot [%d]\n", 
					   eph->svid+1, eph->is_bcast, eph->status, eph->Code_on_L2, eph->Ura, eph->Health, eph->IODC, eph->Tgd, eph->Toc, eph->Af2, eph->Af1, eph->Af0, eph->Crs, eph->DeltaN, eph->Mo, eph->Cuc, eph->E, eph->Cus, eph->SqrtA, eph->Toe, eph->FitIntervalFlag, eph->Aodo, eph->Cic, eph->Omega0, eph->Cis, eph->Io, eph->Crc, eph->Omega, eph->OmegaDot, eph->Idot);
				   DBG_LOG("###################Decoded GPS Ephemeris ######################## \n");

			   }
	   break;
	}
	DBG_LOG(" [LCS USER] >>>> handle_get_aiding assist[%d], assist_array[%s], num[%d]\n\n",assist, arr, num);
	return result;

}

static 
int handle_get_ue_req4assist(void *hnd)
{
	return 1;
}

static 
int handle_nw_loc_results(void *hnd)
{
	//CPP_FTRACE1();
	struct gnss_pos_result pos_result;
	return 1;
}

static 
int handle_set_intended_qop(void *hnd)
{
	//CPP_FTRACE1();
	struct gnss_qop qop_req;
	return 1;
}


static 
int handle_set_loc_instruct(void *hnd)
{
	//CPP_FTRACE1();
	struct loc_instruct req;

	req.lc_methods = GNSS_GPS_PMTHD_BIT | GNSS_GLO_PMTHD_BIT;;
//	req.vel_needed = 1;
//	req.type_flags = UE_LCS_CAP_GNSS_AUTO;
	req.type_flags = UE_LCS_CAP_AGNSS_POS;


	req.qop_intent.h_acc_M	=  20;
	req.qop_intent.rsp_secs	=  50;

	req.qop_intent.acc_sel = acc_3d;
	req.qop_intent.max_age = 1;

	if(if_ops.set_loc_instruct){
		DBG_LOG("Connect interface is active\n");
		return if_ops.set_loc_instruct(hnd, req);
		}

	return 1;
}

static 
int handle_set_rpt_criteria(void *hnd , int interval)
{
	//CPP_FTRACE1();
	struct rpt_criteria rpt;
	rpt.interval = interval;


	if(if_ops.set_rpt_criteria)
		return if_ops.set_rpt_criteria(hnd, rpt);
	return 1;

}




static 
int handle_get_ue_loc_caps(void *hnd)
{
	//CPP_FTRACE1();
	unsigned int gps_caps_bits = -1;
	int count = 0;
	int result = -1;
	struct ganss_ue_lcs_caps *ganss_caps = new struct ganss_ue_lcs_caps;	
	memset(ganss_caps, 0, sizeof(struct ganss_ue_lcs_caps));
	
	if(if_ops.get_ue_loc_caps)
		result = if_ops.get_ue_loc_caps(hnd, gps_caps_bits, ganss_caps);

	DBG_LOG(" [LCS USER] gps_caps_bits = %d\n\n",gps_caps_bits);
	DBG_LOG(" [LCS USER] ganss_caps.n_ganss = %d\n\n", ganss_caps->n_ganss);

	count = ganss_caps->n_ganss;
	while(count--){
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->ganss_id = %d\n\n", ganss_caps->lcs_caps[count].ganss_id);
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->caps_bits = %d\n\n", ganss_caps->lcs_caps[count].caps_bits);
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->sigs_bits = %d\n\n", ganss_caps->lcs_caps[count].sigs_bits);
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->sbas_bits = %d\n\n", ganss_caps->lcs_caps[count].sbas_bits);

		DBG_LOG("\n\n\n\n");
		//ganss_caps->lcs_caps++;
	}

	return result;

}

static 
int handle_device_init(void *hnd)
{
	/*Initialize the devproxy from platform app*/	
	if(if_ops.dev_init)
		if_ops.dev_init(hnd);

	return 1;
}


static
int handle_device_cleanup(void *hnd)
{
        /*De-Initialize the devproxy from platform app*/
        if(if_ops.ue_cleanup)
                if_ops.ue_cleanup(hnd);

        return 1;
}


static int handlePLT(void *hnd, int mode)
{
	struct plt_param plt_test;
	int val;

	plt_test.test_type = plt_cw_test; //CW TEST
	plt_test.timeout =0x05;
	memset(&plt_test.cw_test, 0, sizeof(cw_test_param));
	plt_test.cw_test.tests = test_mode;

	if((mode&0xffff) ==0x1)//SNR
	{
		val = (mode>>16)&0xffff;
		printf("SNR test type %d\n",val);
		if(val == 1)
			val = 0x01;
		else if(val == 2)
			val = 0x10;
		else if(val == 3)
			val = 0x11;
		else
			val =0;
	
		plt_test.cw_test.options= val;
	}
	
	plt_test.cw_test.num_wb_peaks = 5;
	plt_test.cw_test.num_nb_peaks = 5;
	plt_test.cw_test.wb_peak_samples= 5;
	plt_test.cw_test.nb_peak_samples= 5;
	plt_test.cw_test.nb_center_freq = 0x80000000;
	plt_test.cw_test.decim_factor= 6 ; /*HS7814 change 2 to 6 */
	plt_test.cw_test.tap_off_pts = 5;
	plt_test.cw_test.glonass_slot= 2;
	plt_test.cw_test.num_fft_avg = 0;
	plt_test.cw_test.noise_fig_corr_factor = 0;	
	plt_test.cw_test.in_tone_lvl = 0xfbb4;	
	plt_test.svid =0;
	plt_test.term_evt = 0;
	printf("test mode 0x%x decim_factor %d\n",mode,plt_test.cw_test.decim_factor);
	if(if_ops.dev_plt)
		if_ops.dev_plt(hnd, plt_test);

	return 1;
}


static pthread_t usr_thread;

static void *user_thread(void *arg)
{
	//CPP_FTRACE1();
	DBG_LOG(" [LCS USER] user_thread\n\n");

	while(1) {
			if(ti_agnss_lcs_work(arg) < 0)
		goto lcs_init_exit;
	}


lcs_init_exit:
		return 0;
}

static void err_handler(int sig) {
#if 0
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:size %zu\n", sig,size);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
#endif
  exit(1);
}

static void SignalHandler(int signo)
{
	printf("Received signal %s\n", strsignal(signo));
	switch (signo)
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
				 aQuit = 1;
				 break;
		case SIGABRT:
		case SIGSEGV:
					err_handler(signo);
				 break;
		default:
				 break;
	} // end switch
}

static int parse_svid(char * oparg)
{
	char * ptr, *next_ptr, *end_ptr;
	char svd_str[3];
	int copy_len, i;
	int svid_index;

	ptr = oparg;
	end_ptr = ptr+strlen(ptr);
	svid_index = 0;
	
	printf("parse_svid %s\n",oparg);

	while( ptr!=NULL && ptr < end_ptr)
	{
		if(	(next_ptr = strstr(ptr,":")) != NULL )
		{
			copy_len = (next_ptr - ptr);
			next_ptr++;
		}
		else
		{
			copy_len = strlen(ptr);
			next_ptr = NULL;
		}

		if(copy_len > 2)
		{
			printf("wrong svid len %s \r",oparg);
			break;
		}

		if( copy_len > 0)
		{
			memset(svd_str, 0x00, sizeof(svd_str));
			strncpy(svd_str, ptr, copy_len );
			svid[svid_index] = atoi(svd_str);
			svid_index ++;
			ptr = next_ptr;
		}
		else
		{
			break;
		}
		if(svid_index >= MAX_CHECK_PRN )
			break;
	}

	printf("\nsvid_index %d: svid: ",svid_index);
	for(i=0;i<svid_index;i++)
		printf("%d ",svid[i]);
	printf("\n");
	
	return svid_index;
	
}

static int check_chamber_svid(void)
{
	int i;
	
	if(svid_num == 0)
		return -1;

	for(i=0;i<svid_num;i++)
	{
		if(svid[i] <= 0 ||svid[i] >= MaxSatellitesID )
			return -1;
	}
	return 0;
}


static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "cold",  1, 0, 'c' },
			{ "tm",  1, 0, 't' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "chws:p:m:g:n:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
	
		case 'c':
			coldstart = 1;
			break;
		case 'h':
			hotstart = 1;
			break;
		case 'w':
			warmstart = 1;
			break;
		case 'm':
			if(strstr(optarg,"tm")!=NULL)
			{
				coldstart = 1;				
			}
			else if(strstr(optarg,"open")!=NULL)
			{
				coldstart = 1;
				open_sky_mode=1;						
			}		
			else if(strstr(optarg,"cham")!=NULL)
			{
				coldstart = 1;
				chamber_mode=1;						
			}
			else if(strstr(optarg,"fixonly")!=NULL)
			{
				fix_only_mode=1;						
			}
			else if(strstr(optarg,"delonly")!=NULL)
			{
				del_only_mode=1;						
			}			
			break;

		case 'n':
			test_mode=strtol(optarg, NULL, 0);
			break;
		case 'g':
			has_display = atoi(optarg);
			break;
									
		case 's':
			svid_num = parse_svid(optarg);
			break;

		default:
		//	print_usage(argv[0]);
		  printf("unknown option\n");
		  exit(1);
			break;
		}
	}
}

static void test_result_init(void)
{	
	if(test_mode)
	{
		t_TestResult_TestMode_init(has_display);
	}
	else if(chamber_mode)
	{
		if(check_chamber_svid() != 0)
		{
			memset(svid, 0x00, sizeof(svid));
			svid[0]=7;svid[1]=74;
			svid_num = 2;
		}
		t_TestResult_MultiCH_init(eMultiChTestMode_Chamber, has_display);
		t_TestResult_MultiCH_SetExitDetectedPRN(svid, svid_num);
	}
	else if(open_sky_mode)
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_OpenSky, has_display);
	}
	else if(fix_only_mode)
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_FixOnly, has_display);
	}			
	else
	{
		 t_TestResult_MultiCH_init(eMultiChTestMode_Endless, has_display);
	}		
		
}

static void read_cmd_pipe(void)
{
	char cmd;
	if(read( pipe_cmd_fd, &cmd,sizeof(cmd))>0)
	{
		if(cmd=='h')
		{
			handle_loc_stop(ghnd);
			sleep(1);

		 	DBG_LOG("hot start\n");
		 	handle_set_loc_instruct(ghnd);
		 	handle_set_rpt_criteria(ghnd, 1);
			handle_del_aiding(ghnd, HOT_START);
			sleep(1);
			handle_loc_start(ghnd);
			ttff=0;
		}
		else if(cmd=='w')
		{
			handle_loc_stop(ghnd);
			sleep(1);
			DBG_LOG("warm start\n");
		 	handle_set_loc_instruct(ghnd);
		 	handle_set_rpt_criteria(ghnd, 1);
			handle_del_aiding(ghnd, WARM_START);
			sleep(1);
			handle_loc_start(ghnd);
			ttff=0;
		}
		else if(cmd=='c')
		{
			handle_loc_stop(ghnd);
				sleep(1);
			DBG_LOG("cold start\n");
		 	handle_set_loc_instruct(ghnd);
		 	handle_set_rpt_criteria(ghnd, 1);
			handle_del_aiding(ghnd, COLD_START);
			sleep(1);
			handle_loc_start(ghnd);
			ttff=0;
		}
	}

}

int main(int argc, char *argv[])
{
//	//CPP_FTRACE1();
	DBG_LOG("Entering main \n");
	struct ti_agnss_lcs_cb cb_ops;
	cb_ops.ue_need_assist = NULL;
	cb_ops.ue_decoded_aid = decoded_aid;
	cb_ops.ue_loc_results = dev_loc_results;
	cb_ops.ue_nmea_report = dev_nmea_rpt;
	cb_ops.ue_aux_info4sv = dev_aux_info4sv;
	//cb_ops.decl_dev_param = dev_decl_dev_param;
	cb_ops.decl_dev_param = NULL;
	cb_ops.cw_test_results = dev_cw_test_results;
	cb_ops.ue_recovery_ind = dev_ue_recovery_ind;

	void * hnd = ti_agnss_lcs_init(pform, if_ops, cb_ops);
	ghnd = hnd;
	sem_t		   sem;

	int interval = 1;
	struct sigaction act; 
	sigemptyset(&act.sa_mask); 
	act.sa_flags   = 0;
	act.sa_handler = SignalHandler;
	sigaction(SIGTERM, &act, NULL); 
	sigaction(SIGINT, &act, NULL);
	
	sigaction(SIGABRT, &act, NULL);
	sigaction(SIGSEGV, &act, NULL);

	parse_opts(argc, argv);

  printf("create cmd pipe\n");
  system("mknod /tmp/gps_cmd p");
  if (pipe_cmd_fd < 0)
  {
        pipe_cmd_fd = open("/tmp/gps_cmd", O_RDONLY | O_NDELAY | O_NONBLOCK);

        if (pipe_cmd_fd < 0) {
            printf("PIPE: read open cmd device fail (%d)\n", pipe_cmd_fd);
            return -1;
        }
  }   
 
	if(hnd == NULL){
			DBG_LOG("Exit the pfmapp \n");
			//exit (0);
			 exit(EXIT_FAILURE);
	}

	if(pthread_create(&usr_thread, NULL, user_thread, hnd)  != 0){
		return -1;			
	}
	
	test_result_init();
	memset(&gpsLocation, 0x00, sizeof(gpsLocation));
	memset(svid, 0x00, sizeof(svid));

	handle_device_init(hnd);
	if(test_mode)
	{
		handlePLT(hnd,test_mode); //SNR
	}
	else
	{
		handle_set_loc_instruct(hnd);
		handle_set_rpt_criteria(hnd, 1);
		if(del_only_mode)
		{
			DBG_LOG("delete aiding\n");
			handle_del_aiding(hnd, COLD_START);
			aQuit = 1;
		}
		else if(coldstart)
		{
			DBG_LOG("cold start\n");
			handle_del_aiding(hnd, COLD_START);
			handle_loc_start(hnd);
		}
		else if(warmstart)
		{
			DBG_LOG("warm start\n");
			handle_del_aiding(hnd, WARM_START);
			handle_loc_start(hnd);
		}
		else if(hotstart)
		{
			DBG_LOG("hot start\n");
			handle_del_aiding(hnd, HOT_START);
			handle_loc_start(hnd);
		}
		else
		{
			DBG_LOG("no options\n");
		}
	}

lcs_init_exit:
	while(aQuit == 0)
	{
		read_cmd_pipe();
		sleep(0.5);
	}
	if(pipe_cmd_fd>0)
		close(pipe_cmd_fd);
	if(!test_mode && !del_only_mode)
		handle_loc_stop(hnd);
	handle_device_cleanup(hnd);
	DBG_LOG(" [LCS USER] EXIT !!!!!!\n\n");
	return 0;
}
