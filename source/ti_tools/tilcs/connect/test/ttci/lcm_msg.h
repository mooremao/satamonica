#ifndef _LCM_MSG_H_
#define _LCM_MSG_H_

#define QUEUE_NAME  "/test_queue"

//#define MSG_STOP    "exit"

#define MSG_ID_LCM_DISPLAY_DRAW 0 
#define MSG_ID_LCM_DISPLAY_CLEAR 1
#define MSG_ID_LCM_DISPLAY_CLEAR_ALL 2
#define MSG_ID_LCM_DISPLAY_EXIT 3
#define MSG_ID_LCM_DISPLAY_TEST_RESULT 4

#define MAX_CHARS_PAR_LINE 20

typedef struct _t_LCM_DispayMsg_Draw
{
	unsigned char cursor_pos_x;
	unsigned char cursor_pos_y;
	char text[MAX_CHARS_PAR_LINE+1];
} t_LCM_DispayMsg_Draw;

typedef struct _t_LCM_DispayMsg_Clear
{
	unsigned char cursor_pos_x;
	unsigned char cursor_pos_y;
	unsigned char num_chars;
} t_LCM_DispayMsgClear;

typedef struct _t_LCM_DispayMsg_Result
{
	int is_pass;
} t_LCM_DispayMsgResult;

typedef struct _t_LCM_DispayMsg
{
	unsigned char msg_id;
	union {
		t_LCM_DispayMsg_Draw msg_data_draw;
		t_LCM_DispayMsgClear msg_data_clear;
		t_LCM_DispayMsgResult msg_data_result;
	} msg_data;
} t_LCM_DispayMsg;

#define CHECK(x) \
    do { \
        if (!(x)) { \
            fprintf(stderr, "%s:%d: ", __func__, __LINE__); \
            perror(#x); \
            exit(-1); \
        } \
    } while (0) \

#define MAX_SIZE    sizeof(t_LCM_DispayMsg)
#define MSG_SIZE    MAX_SIZE
#endif /* #ifndef _LCM_MSG_H_ */