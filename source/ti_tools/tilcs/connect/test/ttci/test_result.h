#ifndef __TEST_RESULT_H_
#define __TEST_RESULT_H_



#define MaxSatellitesID   97


#define MAX_CHECK_PRN 4

typedef enum
{
	eMultiChTestMode_Chamber,
	eMultiChTestMode_OpenSky,
	eMultiChTestMode_FixOnly,
	eMultiChTestMode_Endless,
	eMultiChTestMode_HibernateOnly,
} eMultiChTestMode;
// GNSS CH runtime obj
typedef struct _t_TestResult_MultiCH
{
    int check_prn[MAX_CHECK_PRN];
    int num_check_prn;
    //int check_snr[MAX_CHECK_PRN];
    eMultiChTestMode mode;
    int MinCountPerSV;
    int latitude;
    int longitude;
    int IsFix;
    int TTFF;
    int bResult;
    int SNR_current[MaxSatellitesID];
    int SNR_average[MaxSatellitesID];
    int SNR_total[MaxSatellitesID];
    int SNR_count[MaxSatellitesID];
//    int satnrstored[MaxSatellitesID];   
    int IsFirstFix;
    int TestFinished;
    int TestCanExit;
    int start_average;
    int time_count;
    int useFB;
} t_TestResult_MultiCH;


//test mode 4 runtime obj
typedef struct _t_TestResult_TestMode
{
    int time_count;
    int max_count;
    int useFB;
} t_TestResult_TestMode;

typedef struct _test_mode4_t
{
	uint16_t bit_sync_time;
	int32_t  AbsI20ms;
	int32_t  AbsQ20ms;
	float cn0_mean;
	float cn0_sigma;
	float clock_drift_change;
	float clock_drift;
	float phase_lock;
	uint16_t RTCFrequency;
} test_mode4_t;


// chamber test functions
 void t_TestResult_MultiCH_SVStatus(int satnr, int snr);   
   

 int t_TestResult_MultiCH_Log2File();

 //int  t_TestResult_MultiCH_CheckTestCanFinishByPrn7OrPrn8AndPrn74(); 
// int t_TestResult_MultiCH_CheckResultbyPrn7OrPrn8AndPrn74(void);
 void t_TestResult_MultiCH_init(eMultiChTestMode mode, int useFB);
 void t_TestResult_MultiCH_UpdateFixInfo(int time_count,int isFix, int TTFF, int latitude, int longitude);
 int t_TestResult_MultiCH_TestCanExit();
 void t_TestResult_deinit();
 void t_TestResult_MultiCH_CleartCurrentSVStatus();
 
 void t_TestResult_MultiCH_SetExitDetectedPRN(int *svid, int num_svid);
 
 // test mode functions
int t_TestResult_TestMode_UpdateResult(char *testresult);
 void t_TestResult_TestMode_init(int useFB);
  
int t_TestResult_TestMode_TestCanExit();
#endif //__TEST_RESULT_H_
