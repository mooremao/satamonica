#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <Ftrace.h>
#include <math.h>
#include <sys/time.h>
#include "ti_agnss_lcs.h"
#include "gnss.h"
#include <stdint.h>
#include <semaphore.h>
#define GPS_MAX_SVS 32
#define PI (3.14159265359)

struct timeval g_oldtime;
struct timeval g_newtime;
#define RESPTYP_WIDEBAND_SNR 	0x04
#define RESPTYP_NARROWBAND_SNR 	0x05
#define RESPTYP_TCXO_OFFSET		0x06
#define RESPTYP_NOISE_FIGURE 	0x07

#define TRUE 1
#define FALSE 0

static bool g_log_enb = 1;

#define DBG_LOG if(g_log_enb)printf
typedef int64_t GpsUtcTime;
unsigned int g_num_of_sessions ;
sem_t		   loc_fix_wait_sem;
static unsigned int g_automation;
sem_t		   automate_wait_sem;

#define COLD_START 1
#define HOT_START 2
#define WARM_START 3
/** Represents a location. */
typedef struct {
    /** set to sizeof(GpsLocation) */
    size_t          size;
    /** Contains GpsLocationFlags bits. */
    unsigned short        flags;
    /** Represents latitude in degrees. */
    double          latitude;
    /** Represents longitude in degrees. */
    double          longitude;
    /** Represents altitude in meters above the WGS 84 reference
     * ellipsoid. */
    double          altitude;
    /** Represents speed in meters per second. */
    float           speed;
    /** Represents heading in degrees. */
    float           bearing;
    /** Represents expected accuracy in meters. */
    float           accuracy;
    /** Timestamp for the location fix. */
    GpsUtcTime      timestamp;
} GpsLocation;

/** Represents SV information. */
typedef struct {
    /** set to sizeof(GpsSvInfo) */
    size_t          size;
    /** Pseudo-random number for the SV. */
    int     prn;
    /** Signal to noise ratio. */
    float   snr;
    /** Elevation of SV in degrees. */
    float   elevation;
    /** Azimuth of SV in degrees. */
    float   azimuth;
	unsigned char meas_qual;
	unsigned short meas_flags;
} GpsSvInfo;

/** Represents SV status. */
typedef struct {
    /** set to sizeof(GpsSvStatus) */
    size_t          size;

    /** Number of SVs currently visible. */
    int         num_svs;

    /** Contains an array of SV information. */
    GpsSvInfo   sv_list[GPS_MAX_SVS];

    /** Represents a bit mask indicating which SVs
     * have ephemeris data.
     */
    unsigned int    ephemeris_mask;

    /** Represents a bit mask indicating which SVs
     * have almanac data.
     */
    unsigned int    almanac_mask;

    /**
     * Represents a bit mask indicating which SVs
     * were used for computing the most recent position fix.
     */
    unsigned int    used_in_fix_mask;
} GpsSvStatus;


static int loc_results(void *hnd, const struct gnss_pos_result& pos_result, 
                       const struct gps_msr_result& gps_result, 
                       const struct gps_msr_info *gps_msr, 
                       const struct ganss_msr_result& ganss_result, 
                       const struct ganss_msr_desc *ganss_msr)
{
       DBG_LOG("loc_results \n");

        return 0;
}


static int need_assist(void *hnd, const struct gps_assist_req& gps_assist,
			   const struct ganss_assist_req& ganss_assist,
			   const struct assist_reference& assist_usr)		           
{
	//CPP_FTRACE1();
	DBG_LOG(" [LCS USER] need_assist\n");
	return 0;
}



static int decoded_aid(void *hnd, enum loc_assist assist,
                       const void *assist_array, int num)
{

        //      unsigned char *wr_data;

    DBG_LOG("decoded_aid: entering");

        switch (assist) {
                case a_GPS_EPH:
                        {

                                struct gps_eph *eph = (struct gps_eph *)assist_array;
                                DBG_LOG("###################Decoded GPS Ephemeris ######################## \n");
                                DBG_LOG("decoded_aid:\n gps eph svid [%d]\n, is_bcast [%d]\n, status [%d]\n, Code_on_L2 [%d]\n, Ura [%d]\n, Health [%d]\n, IODC [%d]\n, Tgd [%d]\n, Toc [%d]\n, Af2 [%d]\n, Af1 [%d]\n, Af0 [%d]\n, Crs [%d]\n, DeltaN [%d]\n, Mo [%d]\n, Cuc [%d]\n, E [%d]\n, Cus [%d]\n, SqrtA [%d]\n, Toe [%d]\n, FitIntervalFlag [%d]\n, Aodo [%d]\n, Cic [%d]\n, Omega0 [%d]\n, Cis [%d]\n, Io [%d]\n, Crc [%d]\n, Omega [%d]\n, OmegaDot [%d]\n, Idot [%d]\n", 
									eph->svid+1, eph->is_bcast, eph->status, eph->Code_on_L2, eph->Ura, eph->Health, eph->IODC, eph->Tgd, eph->Toc, eph->Af2, eph->Af1, eph->Af0, eph->Crs, eph->DeltaN, eph->Mo, eph->Cuc, eph->E, eph->Cus, eph->SqrtA, eph->Toe, eph->FitIntervalFlag, eph->Aodo, eph->Cic, eph->Omega0, eph->Cis, eph->Io, eph->Crc, eph->Omega, eph->OmegaDot, eph->Idot);
                                DBG_LOG("###################Decoded GPS Ephemeris ######################## \n");

                                }
                        break;

                case a_GPS_TIM:
                        {


                                struct gps_ref_time *tim = (struct gps_ref_time *)assist_array;
                                DBG_LOG("###################Decoded GPS TIME  ######################## \n");
                               DBG_LOG("decoded_aid: gps time week_nr [%d] : msec [%d]  \n",
                                          tim->time.week_nr + (1024 * tim->time.n_wk_ro),
                                          tim->time.tow);
                                 DBG_LOG("decoded_aid: gps time n_wk_ro [%d]  \n", (1024 * tim->time.n_wk_ro));
                                 DBG_LOG("decoded_aid: gps time time_unc[%f] Freq_Unc [%f] \n", (tim->time_unc), tim->freq_unc);								 
                                DBG_LOG("###################Decoded GPS TIME  ######################## \n");


                        }
                        break;

                case a_GLO_EPH:
                        {

                                struct glo_eph *eph = (struct glo_eph *)assist_array;
                                DBG_LOG("###################Decoded GLONASS Ephemeris  ######################## \n");
                                DBG_LOG("decoded_aid: glo eph the_svid [%d]\n, is_bcast [%d]\n, m [%d]\n, tk [%d]\n, tb [%d]\n, M [%d]\n, Gamma [%d]\n, Tau [%d]\n, Xc [%d]\n, Yc [%d]\n, Zc [%d]\n, Xv [%d]\n, Yv [%d]\n, Zv [%d]\n, Xa [%d]\n, Ya [%d]\n, Za [%d]\n, P [%d]\n, NT [%d]\n, n [%d]\n, FT [%d]\n, En [%d]\n, Bn [%d]\n, P1 [%d]\n, P2 [%d]\n, P3 [%d]\n, P4 [%d]\n, Delta_Tau [%d]\n, ln [%d]\n", 
									eph->the_svid, eph->is_bcast, eph->m, eph->tk, eph->tb, eph->M, eph->Gamma, eph->Tau, eph->Xc, eph->Yc, eph->Zc, eph->Xv, eph->Yv, eph->Zv, eph->Xa, eph->Ya, eph->Za, eph->P, eph->NT, eph->n, eph->FT, eph->En, eph->Bn, eph->P1, eph->P2, eph->P3, eph->P4, eph->Delta_Tau, eph->ln);
                                DBG_LOG("###################Decoded GLONASS Ephemeris  ######################## \n");
                        }
                        break;
				case a_GPS_ION:
					   {

							   struct gps_ion *ion = (struct gps_ion*)assist_array;
							   DBG_LOG("###################Decoded GPS Iono  ######################## \n");
							   DBG_LOG("decoded_aid: iono : Alpha0 [%X]\n Alpha1 [%X]\n Alpha2 [%X]\n Alpha3 [%X]\n Beta0 [%X]\n Beta1 [%X]\n Beta2 [%X]\n Beta3 [%X]\n", ion->Alpha0,ion->Alpha1,ion->Alpha2,ion->Alpha3, ion->Beta0, ion->Beta1, ion->Beta2 ,ion->Beta3);
							   DBG_LOG("###################Decoded GPS IONO  ######################## \n");
					   }
					   break;

                default:
                        break;
        }

        UNUSED(hnd);


        return 0;
}


/*   ti_ue_pos_report  location_rpt */
static int dev_loc_results(void *hnd, const struct gnss_pos_result& pos_result,
                           const struct gps_msr_result&    gps_result, 
                           const struct gps_msr_info      *gps_msr,
                           const struct ganss_msr_result&  ganss_result,
                           const struct ganss_msr_info   *ganss_msr)
{

	GpsLocation 		  gpsLocation;
	GpsLocation *h_loc = &gpsLocation;
	#define PYTHAGORAS_VAL(x,y) sqrt((x*x) + (y*y))
	#define EAST_ER 	      (0.1)
	#define NORTH_ER	      (0.1)
	unsigned int flags = POS_ERR_REPORTED | POS_NOT_REPORTED;
	int ttff =0;
	float v_unc = 0.0 ,e_unc = 0.0, n_unc = 0.0, vup_unc=0.0;
	float hdop = 0.0, pdop = 0.0, vdop=0.0, tdop= 0.0;
	DBG_LOG("\tdev_loc_results: Pos Fcount [%d]\n", pos_result.Tcount);
	DBG_LOG("\tdev_loc_results: MSR Fcount [%d]\n", gps_msr->TCount);

	static unsigned int fix_count ;
	if( pos_result.contents & flags){
		return 0;
	 }

	fix_count++;
	printf("g_automation  %d\n", g_automation );
	if(g_automation == TRUE)
	{
		if(fix_count > 2){
			sem_post(&loc_fix_wait_sem);
			fix_count = 0;
		}
	}
		gettimeofday(&g_newtime, NULL);
		ttff = g_newtime.tv_sec - g_oldtime.tv_sec;
		
		gettimeofday(&g_oldtime, NULL);

        const struct location_desc *location = &pos_result.location;
        
	 h_loc->size = sizeof(GpsLocation);
         if((location->lat_deg_by_2p8 == 0)&&(location->lon_deg_by_2p8 == 0)){

	 h_loc->latitude   = (double)location->latitude_N * 90.0f/8388608.0f;

	 if(location->lat_sign == 1 )	 	
	 	h_loc->latitude = -h_loc->latitude;
		 
	 h_loc->longitude  = (double)location->longitude_N * 360.0f/16777216.0f;
     //   h_loc->flags     |= GPS_LOCATION_HAS_LAT_LONG;
        h_loc->altitude   = (double)0;

        }else{
	h_loc->latitude = (double) location->lat_deg_by_2p8 * (180 / pow(2,32));
	h_loc->longitude = (double) location->lon_deg_by_2p8 * (180 / pow(2,31));

//	h_loc->flags	 |= GPS_LOCATION_HAS_LAT_LONG;

	h_loc->altitude   = (double)0;
        }

#define UNC_MAP_K_TO_R(unc_k, C, x) (double)(C * (pow(1+x, unc_k) - 1))
        
	double unc_east = UNC_MAP_K_TO_R(location->unc_semi_maj_K, 10.0f, 0.1f);
	double unc_north = UNC_MAP_K_TO_R(location->unc_semi_min_K,10.0f, 0.1f);
	h_loc->accuracy = (float) PYTHAGORAS_VAL(unc_east, unc_north);
	//h_loc->flags |= GPS_LOCATION_HAS_ACCURACY;				
        if(pos_result.location.shape == ellipsoid_with_v_alt ||
			pos_result.location.shape == ellipsoid_with_v_alt_and_e_unc) {
                h_loc->altitude  = (double)location->altitude_N;
//                h_loc->flags    |= GPS_LOCATION_HAS_ALTITUDE;
        }
        
        /* Include other fields as well. If done, remove this comment */

	#define PYTHAGORAS_VAL(x,y) sqrt((x*x) + (y*y))
                
        const struct vel_gnss_info *velocity = &pos_result.velocity;

		
        h_loc->bearing  = velocity->bearing;

		DBG_LOG("\tdev_loc_results: velocity Unc[%d]\n", pos_result.velocity.v_unc);
				
		v_unc= (float)(pos_result.velocity.v_unc * 1000.0/65535.0);

        h_loc->accuracy = (float) PYTHAGORAS_VAL(velocity->h_unc, v_unc );

	/* Include other fields as well. If done, remove this comment */
	if((h_loc->latitude != 0) && (h_loc->longitude != 0.0)){
		DBG_LOG("/**********************Position Info *****************/\n");
		DBG_LOG("/\t TTFF [%d]\n", ttff);
		DBG_LOG("\tdev_loc_results: ref_secs  [%d]\n", pos_result.ref_secs);
		DBG_LOG("\tdev_loc_results: ref_msec  [%d]\n", pos_result.ref_msec);
		DBG_LOG("\tdev_loc_results: Latitude  [%f]\n", h_loc->latitude);
		DBG_LOG("\tdev_loc_results: Longitude [%f]\n",h_loc->longitude);
		DBG_LOG("\tdev_loc_results: Altitude  [%f]\n", h_loc->altitude);
		DBG_LOG("\tdev_loc_results: fix_Type  [%d]\n", pos_result.fix_type);
		DBG_LOG("\tdev_loc_results: Horizontal speed  	[%f]\n", velocity->h_speed);
		DBG_LOG("\tdev_loc_results: Vertical speed  	[%f]\n", velocity->v_speed);
		DBG_LOG("\tdev_loc_results: bearing  	[%f]\n", h_loc->bearing);
	}

	if((pos_result.utc_secs == 0xFFFFFFFF)&&(pos_result.utc_msec == 0xFFFF)){
		DBG_LOG("dev_loc_results: UTC time information not available - dropping fix");
	}
	else{
		h_loc->timestamp = (GpsUtcTime)(((GpsUtcTime)pos_result.utc_secs* (GpsUtcTime)1000)
							+ (GpsUtcTime)pos_result.utc_msec);

		DBG_LOG("\tdev_loc_results: timestamp [%llu]\n", h_loc->timestamp);
		DBG_LOG("\tdev_loc_results: UTC secs [%d],msecs %d\n",
			pos_result.utc_secs,pos_result.utc_msec);

		DBG_LOG("\tdev_loc_results: osc_bias  [%f]\n", pos_result.osc_bias);
		DBG_LOG("\tdev_loc_results: osc_freq  [%f]\n", pos_result.osc_freq);
		DBG_LOG("\tdev_loc_results: week_num  [%d]\n", pos_result.week_num);
		DBG_LOG("\tdev_loc_results: utc_offset  [%d]\n", pos_result.utcOffset);

		DBG_LOG("\tdev_loc_results: MSL Alt  [%f]\n", pos_result.location.altitude_MSL);
		DBG_LOG("\tdev_loc_results: Pos East Unc  [%f]\n", pos_result.location.east_unc);
		DBG_LOG("\tdev_loc_results: Pos North Unc  [%f]\n", pos_result.location.north_unc);
		DBG_LOG("\tdev_loc_results: Pos Vert Unc  [%f]\n", pos_result.location.vert_unc);

		e_unc = (float)(pos_result.velocity.est_unc * 100.0/65535.0);
		n_unc = (float)(pos_result.velocity.nth_unc* 100.0/65535.0);
		vup_unc = (float)(pos_result.velocity.vup_unc* 100.0/65535.0);
//		DBG_LOG("\tdev_loc_results: velocity Unc[%f]\n", pos_result.velocity.v_unc);
		DBG_LOG("\tdev_loc_results: velocity Unc[%f]\n", v_unc);
		DBG_LOG("\tdev_loc_results: E velocity Unc[%f]\n", e_unc);
		DBG_LOG("\tdev_loc_results: N velocity Unc[%f]\n", n_unc);
		DBG_LOG("\tdev_loc_results: VUP velocity Unc[%f]\n", vup_unc);

		DBG_LOG("\tdev_loc_results: PPS State  [%d]\n", pos_result.pps_state);
		DBG_LOG("\tdev_loc_results: Pos Flags  [0X%x]\n", pos_result.pos_flags);

		hdop = (float)(pos_result.dopinfo.horizontal * 1.0/10.0); 
		pdop = (float)(pos_result.dopinfo.position* 1.0/10.0); 
		tdop = (float)(pos_result.dopinfo.time* 1.0/10.0); 
		vdop = (float)(pos_result.dopinfo.vertical* 1.0/10.0); 
		DBG_LOG("\tdev_loc_results: pos dop  [%f]\n", pdop);
		DBG_LOG("\tdev_loc_results: horizontal dop  [%f]\n", hdop);
		DBG_LOG("\tdev_loc_results: vertical dop  [%f]\n", vdop);
		DBG_LOG("\tdev_loc_results: time  [%f]\n", tdop);


		
		DBG_LOG("/**********************Position Info *****************/\n");

	}
				int nsat= gps_result.n_sat;

#if 0
				DBG_LOG("/************************Measurement results ************/\n");
				while(nsat--)
				{

					DBG_LOG("svid			  [%10d]\n", gps_msr->svid + 1); // Added 1 because the devproxy has -1 for indexing in array.
					DBG_LOG(" ->c_no 		  [%10d]\n", gps_msr->c_no);
					DBG_LOG(" ->snr			  [%10d]\n", gps_msr->snr);
					DBG_LOG(" ->Meas Quality   [%10d]\n", gps_msr->msr_qual);
					DBG_LOG(" ->psedo range	  [%10lf]\n", gps_msr->pseudo_msr);
					DBG_LOG(" ->doppler		  [%10f]\n", gps_msr->doppler);
					DBG_LOG(" ->psedo range unc[%10f]\n", gps_msr->pseudo_msr_unc);
					DBG_LOG(" ->doppler UNC	  [%10f]\n", gps_msr->doppler_unc);
					DBG_LOG(" ->doppler resid  [%10f]\n", gps_msr->doppler_residuals);
					DBG_LOG(" ->pseudo resid	  [%10f]\n", gps_msr->pseudo_range_residuals);
					DBG_LOG(" ->Meas Flags		[0X%x]\n", gps_msr->meas_flags);
					DBG_LOG(" ->Azimuth 		  [%10d]\n", gps_msr->azimuth_deg);
					DBG_LOG(" ->Elevation	  [%10d]\n", gps_msr->elevate_deg);
					gps_msr++;
				}
				DBG_LOG("/************************Measurement results ************/\n");
		
#endif
			nsat =0;
			 DBG_LOG("ganss_msr n_ganss = %d\n", ganss_result.n_ganss);
                        nsat = ganss_result.n_ganss;
	        while(nsat--)
                        {

                                DBG_LOG("svid                      [%10d]\n", ganss_msr->svid );
                                DBG_LOG(" ->c_no                   [%10d]\n", ganss_msr->c_no);
                                DBG_LOG(" ->snr                    [%10d]\n", ganss_msr->snr);
                                DBG_LOG(" ->Meas Quality   [%10d]\n", ganss_msr->msr_qual);
                                DBG_LOG(" ->psedo range    [%10lf]\n", ganss_msr->pseudo_msr);
                                DBG_LOG(" ->doppler                [%10f]\n", ganss_msr->doppler);
                                DBG_LOG(" ->psedo range unc[%10f]\n", ganss_msr->pseudo_msr_unc);
                                DBG_LOG(" ->doppler UNC    [%10f]\n", ganss_msr->doppler_unc);
                                DBG_LOG(" ->doppler resid  [%10f]\n", ganss_msr->doppler_residuals);
                                DBG_LOG(" ->pseudo resid   [%10f]\n", ganss_msr->pseudo_range_residuals);

                                DBG_LOG(" ->Azimuth                [%10d]\n", ganss_msr->azimuth_deg);
                                DBG_LOG(" ->Elevation      [%10d]\n", ganss_msr->elevate_deg);
                                ganss_msr++;
                        }

	return 0;
}



static 
int dev_aux_info4sv(void *hnd, 
			const struct dev_gnss_sat_aux_desc&  gps_desc,
			const struct dev_gnss_sat_aux_info	 *gps_info,
			const struct dev_gnss_sat_aux_desc&  glo_desc,
			const struct dev_gnss_sat_aux_info	 *glo_info)

{
	GpsSvStatus h_sv[1];

	unsigned char n_sat;
	unsigned char i, j;
	unsigned char sbs_svs;
	DBG_LOG("dev_aux_info4sv: gps num_sats	[%d]\n", gps_desc.num_sats);
	DBG_LOG("dev_aux_info4sv: glo num_sats [%d]\n",glo_desc.num_sats);	

	h_sv->size = sizeof(GpsSvStatus);
	h_sv->num_svs = 0;
	h_sv->almanac_mask = 0;
	h_sv->ephemeris_mask = 0;
	h_sv->used_in_fix_mask = 0;

	n_sat = gps_desc.num_sats;

		DBG_LOG("RSSI Value = [%2f]\n", gps_info[0].rssi_dbm);

	for(i = 0; i < n_sat; i++) {

		h_sv->sv_list[i].size		 = sizeof(GpsSvStatus);

		h_sv->sv_list[i].prn	   = gps_info[i].prn;
		h_sv->sv_list[i].snr	   = (float)gps_info[i].snr_db_by10 / 10.0f;
		h_sv->sv_list[i].elevation = gps_info[i].elevate_deg;
		h_sv->sv_list[i].azimuth   = gps_info[i].azimuth_deg;
		h_sv->sv_list[i].meas_flags = gps_info[i].meas_flags;
		h_sv->sv_list[i].meas_qual = gps_info[i].meas_qual;
		DBG_LOG("[%3d]	 [%3f]	 [%4f]	 [%2f] [0x%x] [0x%x]\n",
				h_sv->sv_list[i].prn,
				h_sv->sv_list[i].snr ,
				h_sv->sv_list[i].elevation,
				h_sv->sv_list[i].azimuth,
				h_sv->sv_list[i].meas_qual, h_sv->sv_list[i].meas_flags);

		h_sv->num_svs++;
	}
	h_sv->almanac_mask     = gps_desc.alm_bits;
	h_sv->ephemeris_mask   = gps_desc.eph_bits;
	h_sv->used_in_fix_mask = gps_desc.pos_bits;

	n_sat = glo_desc.num_sats + glo_desc.num_sbas_sats;

	for(unsigned char j = 0; ((i + j) < GPS_MAX_SVS) && (j < n_sat); j++) {

		h_sv->sv_list[i+j].size 	 = sizeof(GpsSvStatus);

		h_sv->sv_list[i+j].prn		 = glo_info[j].prn;
		h_sv->sv_list[i+j].snr		 = (float)glo_info[j].snr_db_by10 / 10.0f;
		h_sv->sv_list[i+j].elevation = glo_info[j].elevate_deg;
		h_sv->sv_list[i+j].azimuth	 = glo_info[j].azimuth_deg;
		h_sv->sv_list[i+j].meas_flags = glo_info[j].meas_flags;
		h_sv->sv_list[i+j].meas_qual = glo_info[j].meas_qual;
		DBG_LOG("[%3d]	 [%3f]	 [%4f]	 [%2f]  [0x%x] [0x%x]\n",
				h_sv->sv_list[i+j].prn,
				h_sv->sv_list[i+j].snr ,
				h_sv->sv_list[i+j].elevation,
				h_sv->sv_list[i+j].azimuth,
				 h_sv->sv_list[i+j].meas_qual, h_sv->sv_list[i+j].meas_flags);
		h_sv->num_svs ++;
	}


//	hal_desc->gps_callbacks->sv_status_cb(h_sv);

	DBG_LOG("h_sv: GPS[%d]+GLO[%d]+ SBAS[%d]: num[%d] alm[%X] eph[%X] pos[%X] gps_pos[%X]  glo_pos[%X]  \n",
			gps_desc.num_sats,
			glo_desc.num_sats,
			glo_desc.num_sbas_sats,
			h_sv->num_svs,
			h_sv->almanac_mask,
			h_sv->ephemeris_mask,
			h_sv->used_in_fix_mask, 
			gps_desc.pos_bits,
			glo_desc.pos_bits);

	return 0;
}



/* ti_nmea_sentence  nmea_message */
int dev_nmea_rpt(void *hnd, enum nmea_sn_id nmea, const char *cb_data, int length)
{
	//DBG_LOG("dev_nmea_rpt: Entering \n");
		
	struct timeval tv;
	gettimeofday(&tv, NULL);
	GpsUtcTime timestamp = tv.tv_sec*1000+tv.tv_usec/1000;//
        switch(nmea) {

        /* GPS specific sentences */
        case gpgga:
        case gpgll:
        case gpgsa:
        case gpgsv:
        case gprmc:
        case gpvtg:
		case gngrs:
		case gngll:
		case gngsa:
		case gngns:
		case gnvtg:
		case gpgst:
		case gpgrs:			
			DBG_LOG("/**********************NMEA Info *****************/\n");
  //              hal_desc->gps_callbacks->nmea_cb(timestamp, cb_data, length);
  			DBG_LOG("NMEA Output :\n \t\t%s\n", cb_data);
		  	DBG_LOG("/**********************NMEA Info *****************/\n");
                break;
                
        default:
//                DBG_LOG("dev_nmea_rpt: Unknown nmea!!");
                break;
        }
	//DBG_LOG("dev_nmea_rpt: Exiting!");


	return 0;
}


int dev_ue_recovery_ind(void *hnd, enum dev_reset_ind rstind, int num)
{

	DBG_LOG("dev_ue_recovery_ind: Entering!");
	switch(rstind){
		case gnss_no_meas_resp_ind :
				DBG_LOG("gnss_no_meas_resp_ind ");
			break;
		
		case gnss_no_reset_resp_ind:
				DBG_LOG("gnss_no_reset_resp_ind ");
			break;

	case gnss_hard_reset_ind:
			DBG_LOG("gnss_hard_reset_ind");
		break;

	case gnss_fw_fatal_ind:
			DBG_LOG("gnss_fw_fatal_ind ");
		break;
		default:
			break;
	}
	return 0;

}
int dev_cw_test_results(void *hnd, enum cw_test_rep 	cwt, 
						   const struct cw_test_report *cw_rep, int num)
{
	DBG_LOG("dev_cw_test_results: Entering!%d ", cw_rep->rep_type);
	int i;
	if(cw_rep->rep_type == plt_cw_test_rept){

		DBG_LOG("RespType = %d\n", cw_rep->resp_type);

		switch(cw_rep->resp_type)
		{
			
			case RESPTYP_WIDEBAND_SNR 	:
					DBG_LOG("Wideband SNR\n");

					for(i = 0; i< cw_rep->wb_snr.peaks; i++){
						DBG_LOG("Peak Index =%d\t Peak SNR =%f\n", cw_rep->wb_snr.wb_peak_index[i], cw_rep->wb_snr.wb_peak_SNR[i]);
					}
			break;
			case RESPTYP_NARROWBAND_SNR :
					DBG_LOG("Narrowband SNR\n");			
				
				DBG_LOG("Center Freq = %d\n", cw_rep->nb_snr.c_freq);
				for(i = 0; i< cw_rep->nb_snr.peaks; i++){
					DBG_LOG("Peak Index =%d\t Peak SNR =%f\n", cw_rep->nb_snr.nb_peak_index[i], cw_rep->nb_snr.nb_peak_SNR[i]);
				}

			break;
			case RESPTYP_TCXO_OFFSET	:
					DBG_LOG("TCXO Offset = %f ppm\n", cw_rep->tcxo_offset);
			break;

			case RESPTYP_NOISE_FIGURE :
					DBG_LOG("noise Fig= %f \n", cw_rep->noise_figure);
			break;
			
			
		}
	}

	if(cw_rep->rep_type == plt_sigacq_test_rep)
	{
		DBG_LOG(" svs_acquired = %d \n", cw_rep->svs_acquired);
		for(i=0; i< cw_rep->svs_acquired ; i++){
			DBG_LOG("PRN = %d \n", cw_rep->prn[i]);
			DBG_LOG("Cno = %f \n", cw_rep->cno[i]);
					
		}
	}
	return 0;

}


struct ti_agnss_lcs_if if_ops;



void* AgnssWorkerThread(void *arg)
{
	DBG_LOG( "AgnssWorkerThread: starting\n");

	ti_assisted_ue_work(arg);

	DBG_LOG("AgnssWorkerThread: exiting\n");
	return  0;
}

static void *ti_hnd;
static struct ti_lc_assist_if ti_lc_if[1];
static struct ti_lc_assist_cb ti_lc_cb[1];
pthread_t *thread_id;

static int agnss_reg_assist_src(void *hnd)
{
        ti_lc_cb->ue_need_assist = need_assist;
        ti_lc_cb->ue_decoded_aid = decoded_aid;
        ti_lc_cb->ue_loc_results = dev_loc_results;
        ti_lc_cb->ue_service_ctl = NULL;
        ti_lc_cb->decl_dev_param = NULL;
		DBG_LOG(" Entering agnss_reg_assist_src\n");
        if (!(ti_hnd = ti_assisted_ue_init(sa_or_pgps, ti_lc_if[0], ti_lc_cb[0]))) {
                DBG_LOG("agnss_reg_assist_src: failed.");
                return 0;
        } else {

                DBG_LOG("agnss_reg_assist_src: success.");
        }

		if(ti_hnd != NULL){
			if (pthread_create(thread_id, NULL, AgnssWorkerThread, ti_hnd) != 0)
			{
				DBG_LOG("AgnssWorker Thread start failure\n");
			}
		}
		

        return 1;
}

static int agnss_dereg_assist_src(void *hnd)
{
	if(ti_hnd)
		ti_assisted_ue_exit(ti_hnd);

	return 1;
}


static 
int handle_loc_start(void *hnd)
{
	gettimeofday(&g_oldtime, NULL);

	//CPP_FTRACE1();
	if(if_ops.loc_start)
		return if_ops.loc_start(hnd);
	
       return 0;
}

static 
int handle_loc_stop(void *hnd)
{
	//CPP_FTRACE1();
	if(if_ops.loc_stop)
		return if_ops.loc_stop(hnd);
	return 0;
}

static 
int handle_get_ue_agnss_caps(void *hnd)
{
	//CPP_FTRACE1();
	int result = -1;
	unsigned int gps_caps_bits = 1000; 
       struct ganss_ue_assist_caps ganss_caps;
	ganss_caps.n_ganss = 1;
	ganss_caps.common_bits = 101;

	int count = ganss_caps.n_ganss;

	while(count--){
		ganss_caps.ganss_caps[count].id_of_ganss = 1;
		ganss_caps.ganss_caps[count].caps_bitmap = 2;
		ganss_caps.ganss_caps[count].data_mdl.orb_model = 3;
		ganss_caps.ganss_caps[count].data_mdl.clk_model = 4;
		ganss_caps.ganss_caps[count].data_mdl.alm_model = 5;
		ganss_caps.ganss_caps[count].data_mdl.utc_model = 6;
	}

	if(if_ops.get_ue_agnss_caps)
		result = if_ops.get_ue_agnss_caps(hnd, gps_caps_bits, &ganss_caps);

	DBG_LOG(" [LCS USER] handle_get_ue_agnss_caps response ganss_caps.n_ganss[%d]\n",ganss_caps.n_ganss);

	count = ganss_caps.n_ganss;

	while(count--){
		DBG_LOG(" [LCS USER] id_of_ganss = %d\n", ganss_caps.ganss_caps[count].id_of_ganss);
		DBG_LOG(" [LCS USER] caps_bitmap = %d\n", ganss_caps.ganss_caps[count].caps_bitmap);
		DBG_LOG(" [LCS USER] orb_model = %d\n", ganss_caps.ganss_caps[count].data_mdl.orb_model);
		DBG_LOG(" [LCS USER] clk_model = %d\n", ganss_caps.ganss_caps[count].data_mdl.clk_model);
		DBG_LOG(" [LCS USER] alm_model = %d\n", ganss_caps.ganss_caps[count].data_mdl.alm_model);
		DBG_LOG(" [LCS USER] utc_model = %d\n\n", ganss_caps.ganss_caps[count].data_mdl.utc_model);
	}
	return result;
}

static 
int handle_set_loc_interval(void *hnd)
{
	//CPP_FTRACE1();
	return 1;
}

static 
int handle_add_assist(void *hnd)
{
	//CPP_FTRACE1();
	//enum loc_assist assist = a_GPS_ACQ;
	struct nw_assist_id assist;
	assist.type_select = NNATIVE;
	assist.gnss_icd_ie = a_GPS_EPH;
	int SIZE = 50;
    char *assist_array = new char[SIZE];
	sprintf(assist_array,"%s","get assist");
	int num = strlen( assist_array);
	if(if_ops.ue_add_assist)
		return if_ops.ue_add_assist(hnd, assist, (void *)assist_array, num);

	return 1;
	


}

static 
int handle_del_aiding(void *hnd, unsigned int val)
{
	//CPP_FTRACE1();
	


	enum loc_assist assist ;
	if(val == 14){
		assist = (enum loc_assist) (a_GPS_EPH | a_REF_POS | a_GPS_ALM | a_GLO_EPH | a_GPS_TIM |a_GLO_TIM | a_GLO_ALM);
	}
	else
		assist = (enum loc_assist)val;
	
    unsigned int sv_id_map = 0xffffffff;
    unsigned int mem_flags = 0xffffffff;


	if(val == COLD_START){ /*Delete all assistance*/
		if_ops.ue_del_aiding(hnd, a_GLO_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GLO_ALM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GLO_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_ALM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_REF_POS,
					0xffffffff, 0xffffffff);
	}
	if(val == WARM_START){	/*Delete ephemeris, time and position*/
		if_ops.ue_del_aiding(hnd, a_GLO_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GLO_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_TIM,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_GPS_EPH,
					0xffffffff, 0xffffffff);
		if_ops.ue_del_aiding(hnd, a_REF_POS,
					0xffffffff, 0xffffffff);
	}
	
	return 1;

}

static 
int handle_get_aiding(void *hnd)
{
	//CPP_FTRACE1();
	int result = -1;
	enum loc_assist assist = a_GPS_EPH;
	char *arr = new char[20];
	sprintf(arr, "%s", "get aiding");
	int num = strlen(arr);
	struct gps_eph *eph = (struct gps_eph *)malloc(sizeof(struct gps_eph));

	if(if_ops.ue_get_aiding)
		result = if_ops.ue_get_aiding(hnd, assist, (char *)eph, num);
	switch (assist) {
	   case a_GPS_EPH:
			   {

				   DBG_LOG("###################Decoded GPS Ephemeris ######################## \n");
				   DBG_LOG("decoded_aid:\n gps eph svid [%d]\n, is_bcast [%d]\n, status [%d]\n, Code_on_L2 [%d]\n, Ura [%d]\n, Health [%d]\n, IODC [%d]\n, Tgd [%d]\n, Toc [%d]\n, Af2 [%d]\n, Af1 [%d]\n, Af0 [%d]\n, Crs [%d]\n, DeltaN [%d]\n, Mo [%d]\n, Cuc [%d]\n, E [%d]\n, Cus [%d]\n, SqrtA [%d]\n, Toe [%d]\n, FitIntervalFlag [%d]\n, Aodo [%d]\n, Cic [%d]\n, Omega0 [%d]\n, Cis [%d]\n, Io [%d]\n, Crc [%d]\n, Omega [%d]\n, OmegaDot [%d]\n, Idot [%d]\n", 
					   eph->svid+1, eph->is_bcast, eph->status, eph->Code_on_L2, eph->Ura, eph->Health, eph->IODC, eph->Tgd, eph->Toc, eph->Af2, eph->Af1, eph->Af0, eph->Crs, eph->DeltaN, eph->Mo, eph->Cuc, eph->E, eph->Cus, eph->SqrtA, eph->Toe, eph->FitIntervalFlag, eph->Aodo, eph->Cic, eph->Omega0, eph->Cis, eph->Io, eph->Crc, eph->Omega, eph->OmegaDot, eph->Idot);
				   DBG_LOG("###################Decoded GPS Ephemeris ######################## \n");

			   }
	   break;
	}
	DBG_LOG(" [LCS USER] >>>> handle_get_aiding assist[%d], assist_array[%s], num[%d]\n\n",assist, arr, num);
	return result;

}

static 
int handle_get_ue_req4assist(void *hnd)
{
	return 1;
}

static 
int handle_nw_loc_results(void *hnd)
{
	//CPP_FTRACE1();
	struct gnss_pos_result pos_result;
	return 1;
}

static 
int handle_set_intended_qop(void *hnd)
{
	//CPP_FTRACE1();
	struct gnss_qop qop_req;
	return 1;
}


static 
int handle_set_loc_instruct(void *hnd)
{
	//CPP_FTRACE1();
	struct loc_instruct req;

	req.lc_methods = GNSS_GPS_PMTHD_BIT | GNSS_GLO_PMTHD_BIT;;
//	req.vel_needed = 1;
	req.type_flags = UE_LCS_CAP_GNSS_AUTO;
//	req.type_flags = UE_LCS_CAP_AGNSS_POS;


	req.qop_intent.h_acc_M	=  20;
	req.qop_intent.rsp_secs	=  50;

	req.qop_intent.acc_sel = acc_3d;
	req.qop_intent.max_age = 1;

	if(if_ops.set_loc_instruct){
//		DBG_LOG("Connect interface is active\n");
		return if_ops.set_loc_instruct(hnd, req);
		}

	return 1;
}

static 
int handle_set_rpt_criteria(void *hnd , int interval)
{
	//CPP_FTRACE1();
	struct rpt_criteria rpt;
	rpt.interval = interval;


	if(if_ops.set_rpt_criteria)
		return if_ops.set_rpt_criteria(hnd, rpt);
	return 1;

}




static 
int handle_get_ue_loc_caps(void *hnd)
{
	//CPP_FTRACE1();
	unsigned int gps_caps_bits = -1;
	int count = 0;
	int result = -1;
	struct ganss_ue_lcs_caps *ganss_caps = new struct ganss_ue_lcs_caps;	
	memset(ganss_caps, 0, sizeof(struct ganss_ue_lcs_caps));
	
	if(if_ops.get_ue_loc_caps)
		result = if_ops.get_ue_loc_caps(hnd, gps_caps_bits, ganss_caps);

	DBG_LOG(" [LCS USER] gps_caps_bits = %d\n\n",gps_caps_bits);
	DBG_LOG(" [LCS USER] ganss_caps.n_ganss = %d\n\n", ganss_caps->n_ganss);

	count = ganss_caps->n_ganss;
	while(count--){
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->ganss_id = %d\n\n", ganss_caps->lcs_caps[count].ganss_id);
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->caps_bits = %d\n\n", ganss_caps->lcs_caps[count].caps_bits);
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->sigs_bits = %d\n\n", ganss_caps->lcs_caps[count].sigs_bits);
		DBG_LOG(" [LCS USER] ganss_caps->lcs_caps->sbas_bits = %d\n\n", ganss_caps->lcs_caps[count].sbas_bits);

		DBG_LOG("\n\n\n\n");
		//ganss_caps->lcs_caps++;
	}

	return result;

}

static 
int handle_device_init(void *hnd)
{
	/*Initialize the devproxy from platform app*/	
	if(if_ops.dev_init)
		if_ops.dev_init(hnd);

	return 1;
}


static
int handle_device_cleanup(void *hnd)
{
        /*De-Initialize the devproxy from platform app*/
        if(if_ops.ue_cleanup)
                if_ops.ue_cleanup(hnd);

        return 1;
}


static int handlePLT(void *hnd, int test_mode)
{
	struct plt_param plt_test;

	plt_test.test_type = plt_cw_test; //CW TEST
	plt_test.timeout =0x05;
	memset(&plt_test.cw_test, 0, sizeof(cw_test_param));
	plt_test.cw_test.tests = test_mode;

	if(test_mode ==0x1)//SNR
	{
	
	char str[5];
	unsigned int val;
	DBG_LOG("Enter the WB/NB mode\n");
	DBG_LOG("1: Report WideBand Mode\n");
	DBG_LOG("2: Report NarrowBand Mode\n");
	DBG_LOG("3: Report Widebanc and NarrowBand Mode\n");	
	
	fgets(str, sizeof(str), stdin);
	val = atoi(str);
	if(val == 1)
		val = 0x01;
	else if(val == 2)
		val = 0x10;
	else if(val == 3)
		val = 0x11;
	else
		val =0;
	
	plt_test.cw_test.options= val;
	
	}
	
	plt_test.cw_test.num_wb_peaks = 5;
	plt_test.cw_test.num_nb_peaks = 5;
	plt_test.cw_test.wb_peak_samples= 5;
	plt_test.cw_test.nb_peak_samples= 5;
	plt_test.cw_test.nb_center_freq = 0x80000000;
	plt_test.cw_test.decim_factor= 2 ;
	plt_test.cw_test.tap_off_pts = 5;
	plt_test.cw_test.glonass_slot= 2;
	plt_test.cw_test.num_fft_avg = 0;
	plt_test.cw_test.noise_fig_corr_factor = 0;	
	plt_test.cw_test.in_tone_lvl = 0xfbb4;	
	plt_test.svid =0;
	plt_test.term_evt = 0;

	if(if_ops.dev_plt)
		if_ops.dev_plt(hnd, plt_test);

	return 1;
}

static int handleSigAcqTest(void *hnd, int svid, int timeout)
{
	struct plt_param plt_test;

	plt_test.test_type = plt_sigacqtest; //SigAcq TEST
	plt_test.timeout =timeout;
	plt_test.svid = svid;
	memset(&plt_test.cw_test, 0, sizeof(cw_test_param));
	
	if(if_ops.dev_plt)
		if_ops.dev_plt(hnd, plt_test);

	return 1;
}
static pthread_t loc_fix_wait_thread;

static void *stop_loc_fix_thread(void *arg)
{
	printf("stop_loc_fix_thread started\n");
	sem_wait(&loc_fix_wait_sem);

	if(g_automation == TRUE)
		handle_loc_stop(arg);

	sleep(2);
	
	sem_post(&automate_wait_sem);
	
	return 0;	
}



static
int handle_test(int choice, void *hnd)
{
	//CPP_FTRACE1();
	int result = -1;
	switch(choice){
		case 1:
			DBG_LOG("Initialize the device[%d]\n",handle_device_init(hnd));
			break;
		case 2:	
			DBG_LOG("set loc instruct result[%d]\n",handle_set_loc_instruct(hnd));
			break;
		case 3:{
			
			printf("\t Enter the interval in seconds:");
			 char arr[15];
			int interval;
					fgets(arr, sizeof(arr), stdin);
					interval = atoi(arr);
			DBG_LOG("set rpt criteria result[%d]\n",handle_set_rpt_criteria(hnd, interval)); 		
			break;
			}
			
		case 4:
			
			DBG_LOG("loc start result[%d]\n",handle_loc_start(hnd));
			break;
		case 5:
			DBG_LOG("loc stop result[%d]\n",handle_loc_stop(hnd));
			break;
#if 0			
		case 6:
			DBG_LOG("get loc caps result[%d]\n",handle_get_ue_loc_caps(hnd));
			break;
		case 7:
			DBG_LOG("get agnss caps result[%d]\n",handle_get_ue_agnss_caps(hnd));
			break;
		case 8:
			DBG_LOG("add an assistance provider result[%d]\n",handle_add_assist(hnd));
			break;
#endif
		case 6:
		{
			char str[5];
			unsigned int val;
			printf("\t 1. Delete Reference Position \n ");
			printf("\t 2. Delete GPS Time \n ");
			printf("\t 3. Delete GPS DGPS corrections \n");
			printf("\t 4. Delete GPS Ephemeris\n ");
			printf("\t 5. Delete GPS IONO \n ");
			printf("\t 6. Delete GPS UTC \n");
			printf("\t 7. Delete GPS Almanac\n ");
			printf("\t 8. Delete GPS Acquisition \n");
			printf("\t 9. Delete GPS RTI \n");
			printf("\t 10. Delete GLONASS Ephemeris\n");
			printf("\t 11. Delete GLONASS Almanac\n");
			printf("\t 12. Delete GLONASS UTC\n");
			printf("\t 13. Delete GLONASS TIME \n");
			printf("\t 14. Dlelte All the assistance\n");

			fgets(str, sizeof(str), stdin);
			val = atoi(str);
			
			DBG_LOG("del aiding result[%d]\n",handle_del_aiding(hnd, val));
		}
			break;
#if 0			
		case 10:	
			DBG_LOG("get aiding result[%d]\n",handle_get_aiding(hnd));
			break;
		case 11:
			DBG_LOG("Register Assistance Source[%d]\n",agnss_reg_assist_src(hnd));
			break;
		case 12:
				DBG_LOG("Un-register assistance source[%d]\n",agnss_dereg_assist_src(hnd));
				break;
			
		case 13:	
			DBG_LOG("nw loc results result[%d]\n",handle_nw_loc_results(hnd));
			break;
		case 14:	
			DBG_LOG("set qop result[%d]\n",handle_set_intended_qop(hnd));
			break;
#endif
		case 7:
			char str[5];
			unsigned int val, testmode;
			printf("Enter the Test Mode \n");
			printf("1: TCXO Offset \n");
			printf("2: Noise Figure \n");
			printf("3: SNR \n");
			fgets(str, sizeof(str), stdin);
			val = atoi(str);
			switch(val)
			{
				case 1:
					testmode = 0x02;
					break;
				case 2:
					testmode = 0x04;
					break;
				case 3:
					testmode = 0x1;
					break;
					
			}
			
			DBG_LOG("\t Product line test\n", handlePLT(hnd, testmode));
			break;

		case 8:
			
			char str1[5];
			unsigned int val1, val2;
			printf("Enter the SVID\n");
			printf("For GPS : 1-32\n");
			printf("For GPS+GLONASS : 0\n");
			fgets(str1, sizeof(str1), stdin);
			val1 = atoi(str1);
			printf("Enter the timeout \n");
			fgets(str1, sizeof(str1), stdin);
			val2 = atoi(str1);
			handleSigAcqTest(hnd, val1, val2);
			break;
		case 9:
			if(g_automation  == FALSE){
				char str2[5];
				unsigned int cold_start_count;
				unsigned int value;
				printf("Enter the number cold starts \n");
				fgets(str2, sizeof(str2), stdin);
				value = atoi(str2);
				handle_set_loc_instruct(hnd);
				handle_set_rpt_criteria(hnd, 1);
//				handle_del_aiding(hnd, 14);
				handle_del_aiding(hnd, COLD_START);

				handle_loc_start(hnd);
				g_num_of_sessions = value;
				g_automation = TRUE;
				printf("g_automation  %d, g_num_of_sessions = %d\n", g_automation, g_num_of_sessions );
#if 1				
				if(pthread_create(&loc_fix_wait_thread, NULL, stop_loc_fix_thread, hnd) != 0){
					return -1;			
				}
#endif
				cold_start_count = 1;
				printf("/**************Cold Start Session #%d Started ************/\n", cold_start_count);

				do{	
					sem_wait(&automate_wait_sem);
					//handle_loc_stop(hnd);	
					sleep(10);
					printf("/**************Cold Start Session #%d Started ************/\n", cold_start_count);
				if(g_num_of_sessions != cold_start_count){
					if(pthread_create(&loc_fix_wait_thread, NULL, stop_loc_fix_thread, hnd)	!= 0){
						return -1;			
					}
				}
					handle_set_loc_instruct(hnd);
					handle_set_rpt_criteria(hnd, 1);
					handle_del_aiding(hnd, 14);					
					handle_loc_start(hnd);
					cold_start_count++;
					
				}while(g_num_of_sessions != cold_start_count);
					g_automation = FALSE;
	
				handle_loc_stop(hnd);
			}
			break;
		case 10:
			if(g_automation  == FALSE){
				char str3[5];
				unsigned int hot_start_count;
				unsigned int value1;
				printf("Enter the number Hot Start Sessions \n");
				fgets(str3, sizeof(str3), stdin);
				value1 = atoi(str3);
				handle_set_loc_instruct(hnd);
				handle_set_rpt_criteria(hnd, 1);
				handle_loc_start(hnd);

				g_num_of_sessions = value1;
				g_automation = TRUE;
				hot_start_count = 1;
				
				printf("/**************Hot Start Session #%d Started ************/", hot_start_count);
#if 1				
				if(pthread_create(&loc_fix_wait_thread, NULL, stop_loc_fix_thread, hnd) != 0){
					return -1;			
				}
#endif

				do{
					
					sem_wait(&automate_wait_sem);
					//handle_loc_stop(hnd);
					sleep(10);
					printf("/**************Hot Start Session #%d Started ************/", hot_start_count);
					if(g_num_of_sessions != hot_start_count){
						if(pthread_create(&loc_fix_wait_thread, NULL, stop_loc_fix_thread, hnd) != 0){
							return -1;			
						}
					}
					handle_set_loc_instruct(hnd);
					handle_set_rpt_criteria(hnd, 1);
					handle_loc_start(hnd);
					hot_start_count++;
				}while(g_num_of_sessions != hot_start_count);
				g_automation = FALSE;
	
				handle_loc_stop(hnd);
			}
			break;
		case 11:
			if(g_automation  == FALSE){
				char str4[5];
				unsigned int warm_start_count;
				unsigned int value2;
				printf("Enter the number Hot Start Sessions \n");
				fgets(str4, sizeof(str4), stdin);
				value2 = atoi(str4);
				handle_set_loc_instruct(hnd);
				handle_set_rpt_criteria(hnd, 1);
				handle_loc_start(hnd);

				g_num_of_sessions = value2;
				g_automation = TRUE;
				warm_start_count = 1;
				
				printf("/**************Warm Start Session #%d Started ************/", warm_start_count );
#if 1				
				if(pthread_create(&loc_fix_wait_thread, NULL, stop_loc_fix_thread, hnd) != 0){
					return -1;			
				}
#endif

				do{
					
					sem_wait(&automate_wait_sem);
					//handle_loc_stop(hnd);
					sleep(10);
					printf("/**************Warm Start Session #%d Started ************/", warm_start_count );
					if(g_num_of_sessions != warm_start_count){
						if(pthread_create(&loc_fix_wait_thread, NULL, stop_loc_fix_thread, hnd) != 0){
							return -1;			
						}
					}
					handle_set_loc_instruct(hnd);
					handle_set_rpt_criteria(hnd, 1);
					handle_del_aiding(hnd, WARM_START);
					handle_loc_start(hnd);
					warm_start_count ++;

				}while(g_num_of_sessions != warm_start_count);
				g_automation = FALSE;
	
				handle_loc_stop(hnd);
			}
			break;

		case 12:
			DBG_LOG("\t De-Initialize the device [%d]\n", handle_device_cleanup(hnd));
			break;
		}
	return result;
}

static
int test_menu()
{	
	//CPP_FTRACE1();
	int choice;
	do{
		printf("*****************************\n");
		printf("		MENU		    \n");		
		printf("*****************************\n");
		printf("\t 1. Initialize device \n");
		printf("\t 2. Set LOC instructs \n ");
		printf("\t 3 .Set Reports interval \n");
		printf("\t 4. Start LOC operation \n");
		printf("\t 5. Stop LOC operation \n");
		printf("\t 6. Del aiding from UE \n");
		printf("\t 7.PLT_TEST \n");
		printf("\t 8.Signal Acquisition Test \n");
		printf("\t********************************************\n");
		printf("\tNOTE: Automation test to be performed only after Device Initialization ie option 1\n");
		printf("\t********************************************\n");
		printf("\t 9.Cold Start Automation Test \n");
		printf("\t 10.Hot Start Automation Test \n");
		printf("\t 11.Warm Start Automation Test \n");
		printf("\t 12.De-Init device \n");

		char arr[13];
		fgets(arr, sizeof(arr), stdin);
		choice = atoi(arr);

		DBG_LOG("\n choice entered is [%d]\n",choice);	
		
	}	while((choice < 1 ) ||(choice > 13));

	return choice;
}


static pthread_t usr_thread;

static void *user_thread(void *arg)
{
	//CPP_FTRACE1();
	DBG_LOG(" [LCS USER] user_thread\n\n");

	while(1) {
			if(ti_agnss_lcs_work(arg) < 0)
		goto lcs_init_exit;
	}


lcs_init_exit:
		return 0;
}



int main()
{
//	//CPP_FTRACE1();
	
	DBG_LOG("Entering main \n");
	struct ti_agnss_lcs_cb cb_ops;
	cb_ops.ue_need_assist = NULL;
	cb_ops.ue_decoded_aid = decoded_aid;
	cb_ops.ue_loc_results = dev_loc_results;
	cb_ops.ue_nmea_report = dev_nmea_rpt;
	cb_ops.ue_aux_info4sv = dev_aux_info4sv;
	//cb_ops.decl_dev_param = dev_decl_dev_param;
	cb_ops.decl_dev_param = NULL;
	cb_ops.cw_test_results = dev_cw_test_results;
	cb_ops.ue_recovery_ind = dev_ue_recovery_ind;

	void * hnd = ti_agnss_lcs_init(pform, if_ops, cb_ops);
	sem_t		   sem;

	int interval = 1;
	
	if(hnd == NULL){
			DBG_LOG("Exit the pfmapp \n");
			//exit (0);
			 exit(EXIT_FAILURE);
	}

	if(pthread_create(&usr_thread, NULL, user_thread, hnd)  != 0){
		return -1;			
	}
	sem_init(&loc_fix_wait_sem, 0, 0);
	sem_init(&automate_wait_sem, 0, 0);

//	sem_init(&sem, 0, 0);

#if 0
	handle_device_init(hnd);
	
	handle_set_loc_instruct(hnd);
	
	
	handle_set_rpt_criteria(hnd, interval);

	gettimeofday(&g_oldtime, NULL);
	handle_loc_start(hnd);
#endif

	
	//sem_wait(&sem);
	int choice;
	while(1){
		choice = test_menu();
		int result = handle_test(choice, hnd);
		}
lcs_init_exit:

	DBG_LOG(" [LCS USER] EXIT !!!!!!\n\n");
	return 0;
}
