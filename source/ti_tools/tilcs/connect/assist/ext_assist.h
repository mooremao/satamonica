/*
 * ext_assist.h
 *
 * API(s) to manage 3GPP or otherwise provided external assistance.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *
 */

#ifndef _EXT_ASSIST_H_
#define _EXT_ASSIST_H_

#include <gnss.h>
#include <device.h>

#define MIN_TTL				60 /* In Seconds */

/* Data construct that holds references to updates received from GNSS receiver.
   Specifically, information relates to GNSS specific time references and time
   of their updates by UE. The information available in this construct is used
   to compliment content that hasn't been provided by external (ext) source of
   assistance.
*/
struct ue_aiding_ref_info {

        unsigned int gps_rec_secs; /* Record time (monotonic secs) of GPS Time */
        unsigned int gps_ref_secs; /* Not valid if gps_rec_secs is set as zero */

        unsigned int glo_rec_secs; /* Record time (monotonic secs) of GLO Time */
        unsigned int glo_ref_secs; /* Not valid if glo_rec_secs is set as zero */
        
        unsigned int pos_rec_secs; /* Record time (monotonic secs) of GNSS POS */
};


/* Get Time to Live (validity) in secs for externally provided assistance.
 * Note: assistance info should be valid for atleast MIN_TTL.
 * 
 * [in]    ue_aid_info: provides references to UE decoded aid
 *
 * Returns ttl in secs; 0 or more on success otherwise -1 on error
 */
int ext_assist_get_ttl(void);


/* Get missing assistance required to establish (UEB) LOC information 
 *
 * [in]      gps_assist: place holder to fill in GPS assist needs.
 * [in]    ganss_assist: place holder to fill in GANSS assist needs. 
 *
 * Returns 0 on success or -1 on error
 * 
 * @note as input struct ganss_assist_req and nested members carries the number
 * (max) of elements in corresponding array. Where as, in output, implementation
 * must specify the actual number of populated elements in respective arrays.
 */
int ext_assist_get_need(struct gps_assist_req     *gps_assist, 
                        struct ganss_assist_req *ganss_assist);

/* Save a copy of device reported Location info for later reference.
 * 
 * [in]     loc : location info descriptor
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_gnss_loc(struct location_desc *loc);
int ext_assist_put_gnss_loc_spec(struct location_desc *location);

/* get a copy of Location info for later reference.
 * 
 * [out]     loc : location info descriptor
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_get_gnss_loc(struct location_desc *loc);

/* Save a copy of externally provided EPH from reference.
 * 
 * [in]     eph : array of GPS EPH information
 * [in]    n_obj: number of elems in the array
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_gps_eph(struct gps_eph *eph, int n_obj);


/* Save a copy of externally provided ALM from reference.
 * 
 * [in]     alm : array of GPS EPH information
 * [in]    n_obj: number of elems in the array
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_gps_alm(struct gps_alm *alm, int num_obj);


/* Get a copy of externally provided ALM from reference.
 * 
 * [Out]     alm : array of GPS EPH information
 * [in]    n_obj: number of elems in the array
 *
 * Returns numer of elements returned on success otherwise -1 on error
 */
int ext_assist_get_gps_alm(struct gps_alm *alm, int num_obj);


/* Save a copy of externally provided GPS Time for later reference.
 * 
 * [in]    time : GPS Time information
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_gps_time(struct gps_time *time);
int ext_assist_put_gps_time_spec(struct gps_time *time);


/* Save a copy of externally provided GLO Time for later reference.
 * 
 * [in]    time : GLO Time information
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_glo_time(struct ganss_ref_time *time); 


/* Save a copy of externally provided GLO NAV for later reference.
 * 
 * [in]    time : GLO NAV MODEL information
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_glo_nav_mdl(unsigned int utc_secs,struct glo_nav_model *nav_mdl, unsigned char num_nav);


/* Save a copy of externally provided GLO Keplerian info for later reference.
 * 
 * [in]    time : GLO ALM Keplerian information
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_glo_kp_alm(unsigned int utc_secs,struct glo_alm_kp_set *kp_set, unsigned char num_set);


/* Save a copy of externally provided GLO UTC model for later reference.
 * 
 * [in]    time : GLO UTC information
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_glo_utc_mdl(struct glo_utc_model *utc_mdl);


/* Save a copy of externally provided GLO Time model for later reference.
 * 
 * [in]    glo_time : GLO Time information
 * [in]    time_mdl : GANSS Time Model
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_glo_time_mdl(struct ganss_ref_time   *glo_time,
                                struct ganss_time_model *time_mdl);

/* Get latest set of GLO ALM (ICD format) built using external assistance.
 * 
 * [inout]     alm: place holder (array) for GLO ALM information
 * [in]    num_obj: number of elems in the place holder (array)
 *
 * Returns num of elems fetched on success otherwise -1 on error
 */
int ext_assist_get_glo_icd_alm(struct glo_alm *alm, unsigned char num_obj);


/* Get latest set of GLO EPH (ICD format) built using external assistance.
 * 
 * [inout]     eph: place holder (array) for GLO EPH information
 * [in]    num_obj: number of elems in the place holder (array)
 *
 * Returns num of elems fetched on success otherwise -1 on error
 */
int ext_assist_get_glo_icd_eph(struct glo_eph *eph, unsigned char num_obj);


/* Delete all assistance that required to accomplish UE Based LOC.
 * 
 * Returns 0 on success else -1 on error 
 */
int ext_assist_del_ueb(void);


/* Save a copy of externally provided GPS ACQ for later reference.
 * 
 * [in]        acq: array of GPS ACQ information
 * [in]    num_obj: number of elems in the array
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_gps_acq(struct gps_acq *acq, int num_obj);


/* Save a copy of externally provided GANSS ACQ for later reference.
 * 
 * [in]        msr: array of GANSS ACQ information
 * [in]    num_obj: number of elems in msr array
 *
 * Returns 0 on success otherwise -1 on error
 */
int ext_assist_put_ganss_acq(struct ganss_ref_msr *msr, int num_obj);


/* Get a latest set of GPS ACQ built using external assistance.
 * 
 * [in]        acq: place holder (array) for GPS ACQ information
 * [in]    num_obj: number of elems in the place holder (array)
 *
 * Returns num of elems fetched on success otherwise -1 on error
 */
int ext_assist_get_gps_acq(struct gps_acq *acq, int num_obj);


/* Get a latest set of GANSS ACQ built using external assistance.
 * 
 * [in]        msr: place holder (array) for GANSS ACQ information
 * [in]    num_obj: number of elems in GANSS place holder (array)
 *
 * Returns num of elems fetched on success otherwise -1 on error
 */
int ext_assist_get_ganss_acq(struct ganss_ref_msr *msr, int num_obj);

/* Delete all assistance that required to accomplish UE assisted LOC.
 * 
 * Returns 0 on success else -1 on error 
 */
int ext_assist_del_uea(void);

/* Get The qualified set of GPS EPH built using external assistance with visibility map.
 * 
 * [out]        eph_data: place holder (array) for GPS EPH information
 *
 * Returns num of elems fetched on success otherwise -1 on error
 */
int ext_assist_get_gps_eph_visible (struct gps_eph* eph_data, int num_sat );

/* Get the count of gps eph present in the transient info.
 * 
 * 
 * Returns num of elems fetched 
 */
 int ext_assist_gps_eph_count();
	
/* Get The set of GPS EPH built using msr report for the sv which are not injected.
 * 
 * [in]      msr_info: place holder (array) for GPS EPH information
 * [in]      num_sat: number of satellite count in msr report 
 * [out]     eph_data: place holder (array) for GPS EPH information 
 *
 * Returns num of elems fetched on success otherwise -1 on error
 */
int ext_assist_get_gps_eph_from_msr (const struct gps_msr_info* msr_info,
										 unsigned short num_msr, 
										 struct gps_eph* eph_data,
										 int num_obj); 

/* Module init */
int ext_assist_mod_init(void);

/* Module exit */
int ext_assist_mod_exit(void);

#endif /* _EXT_ASSIST_H_ */