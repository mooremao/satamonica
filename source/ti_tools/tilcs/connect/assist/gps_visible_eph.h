/*
 * gps_visible_eph.c
 *
 * Returns the PRN map of visible sats from pos, time, and eph.
 *
 * Notes: Only gps sats with PRN 1-32 should be passed, violating this leads to
 * undefined behavior.
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/

#ifndef _GPS_VISIBILE_H_
#define _GPS_VISIBILE_H_

#include "gnss.h"

/* Satellite Position Information */
struct sat_pos
{
	unsigned char svid;
	double x, y, z;
};

/* Get Satellite Position from Ephemeris.
 * 
 * [in]    gps_tim : GPS Time information
 * [in]    eph     : Ephemeris Infomration
 * [out]   sat_pos : Satellite Position Inofrmation
 * [in]    no_sats : Satellite Position Inofrmation
 *
 * Returns 0 on success otherwise -1 on error
 */
int gps_sat_pos_from_eph(unsigned int gps_tim,
					struct gps_eph *eph,
					struct sat_pos *sat_pos_info);

/* Get Satellite Visibility from Position.
 * 
 * [in]    pos : GPS Time information
 * [out]   sat_pos : Satellite Position Inofrmation
 * [in]    no_sats : Satellite Position Inofrmation
 *
 * Returns 0 on success otherwise -1 on error
 */

unsigned int gps_sat_vis_from_pos(double ecef[],struct sat_pos* sat_pos, 
								  double *Az, double *El);

void gps_convert_lla2xyz(struct location_desc *pos, double xyz[]);
#endif /* _GPS_VISIBILE_H_ */


