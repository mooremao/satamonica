/*
 * gps_visible_eph.c
 *
 * Returns the PRN map of visible sats from pos, time, and eph.
 *
 * Notes: Only gps sats with PRN 1-32 should be passed, violating this leads to
 * undefined behavior.
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
*/
#include "gps_visible_eph.h"
#include <math.h>
#include <device.h>
#include "debug.h"
#include "utils.h"

struct decoded_gps_eph
{
	unsigned char		svid;
	unsigned char		flagsf1;
	unsigned char		valid;
	unsigned char		health;
	unsigned char		codel2;
	unsigned char		l2pdata;
	unsigned char		ura;
	unsigned char		iode;
	unsigned char		fti;
	unsigned short		iodc;
	double			m0;
	double			omega0;
	double			i0;
	double			omega;
	double			omegadot;
	double			sqrta;
	double			crs;
	double			deltan;
	double			cuc;
	double			e;
	double			cus;
	double			cic;
	double			cis;
	double			crc;
	double			idot;
	double			toe;
};

static void gps_decode_eph(const struct gps_eph *const eph,
					struct decoded_gps_eph *d_eph)
{
	d_eph->svid  =  eph->svid;

	/*---------------------
	 * Decoding subframe 2.
	 *---------------------
	 */
	/* IODE, 8 bits: 61 - 68 */
	d_eph->iode  =  eph->IODC;

	/* Crs, 16 bits: 69 - 84
	 * Two's complement, meters.
	 */
	d_eph->crs  =  (double)(short) eph->Crs * (1.0 / TWO_TO_5);

	/* Delta n, 16 bits: 91 - 106.
	 * Two's complement, semi-circles/sec.  Converting to radians/sec.
	 */
	d_eph->deltan  =  (double)(short) eph->DeltaN * (C_PI / TWO_TO_43);

	/* M0, 32 bits, split between 107 - 114 and 121 - 144.
	 * Two's complement, semi-circles.  Converting to radians.
	 */
	d_eph->m0  =  (double)(int) eph->Mo * (C_PI / TWO_TO_31);

	/* Cuc, 16 bits: 151 - 166.
	 * Two's complement, radians.
	 */
	d_eph->cuc  =  (double)(short) eph->Cuc * (1.0 / TWO_TO_29);

	/* Eccentricity - e, 32 bits: split between 167 - 174 and 181 - 204.
	 * Dimensionless.
	 */
	d_eph->e  =  (double) eph->E * (1.0 / TWO_TO_33);

	/* Cus, 16 bits, 211 - 226.
	 * Two's complement, radians.
	 */
	d_eph->cus  =  (double)(short) eph->Cus * (1.0 / TWO_TO_29);

	/* Square root of A, 32 bits: split between 227 - 234 and 241 - 264.
	 * Units of meters^(1/2).
	 */

	d_eph->sqrta  =  (double) eph->SqrtA * (1.0 / TWO_TO_19);

	/* Toe, 16 bits: 271 - 286.  seconds. */
	d_eph->toe  =  (double) eph->Toe * TWO_TO_4;

	/*---------------------
	 * Decoding subframe 3.
	 *---------------------
	*/
	/* Cic, 16 bits: 61 - 76,
	 * Two's complement, radians.
	 */
	d_eph->cic  =  (double)(short) eph->Cic * (1.0 / TWO_TO_29);

	/* Omega0, 32 bits: split between 77 - 84 and 91 - 114.
	 * Two's complement, semi-circles.  Converting to radians.
	 */
	d_eph->omega0  =  (double)(int) eph->Omega0 * (C_PI / TWO_TO_31);

	/* Cis, 16 bits: 121 - 136.
	 * Two's complement, radians.
	 */
	d_eph->cis  =  (double)(short) eph->Cis * (1.0 / TWO_TO_29);

	/* I0, 32 bits: split between 137 - 144 and 151 - 174.
	 * Two's complement, semi-circles.	Converting to radians.
	 */
	d_eph->i0  =  (double)(int) eph->Io * (C_PI / TWO_TO_31);

	/* Crc, 16 bits: 181 - 196.
	 * Two's complement, meters.
	 */
	d_eph->crc  =  (double)(short) eph->Crc * (1.0 / TWO_TO_5);

	/* omega, 32 bits: split between 197 - 204 and 211 - 234.
	 * Two's complement, semi-circles.  Converting to radians.
	 */
	d_eph->omega  =  (double)(int) eph->Omega * (C_PI / TWO_TO_31);

	/* omega dot, 24 bits: 241 - 264.
	 * Two's complement, semi-circles/sec.	Converting to radians/sec.
	 */
	d_eph->omegadot  =  (double)((int)(eph->OmegaDot << 8) >> 8)
		* (C_PI / TWO_TO_43);

	/* I dot, 14 bits: 279 - 292.
	 * Two's complement, semi-circles/sec.  Converting to radians/sec.
	 */
	d_eph->idot  =  (double)((short)(eph->Idot << 2) >> 2)
		* (C_PI / TWO_TO_43);
}

static void gps_kepler(double *mk, double *E, double *ek)
{
		/* Iteration with wegstein's accelerator. */
		double x, y, x1,y_1,x2;
		int i;

		x = *mk;
		y = *mk - (x - *E * sin(x));
		x1 = x;
		x = y;

		for(i = 0; i < 16; i++) {
			x2 = x1;
			x1 = x;
			y_1 = y;
			y = *mk - (x - *E * sin(x));
			if (fabs(y - y_1) < 1.0E-15)
				break;
			x = (x2 * y - x * y_1) / (y - y_1);
		}

		*ek = x;
}

int gps_sat_pos_from_eph(unsigned int gps_tim,
					struct gps_eph *eph,
					struct sat_pos *sat_pos)
{
	double n0; /* computed mean motion */
	double n; /* corrected mean motion */
	double tk; /* time from d_eph reference epoch */
	double mk; /* mean anomaly */
	double ek; /* eccentric anomaly */
	double vk; /* true anomaly */
	double pk; /* argument of latitude */
	double sin2pk; /* sin 2*pk */
	double cos2pk; /* cos 2*pk */
	double uk; /* corrected agument of latitude */
	double duk; /* latitude correction */
	double drk; /* radius correction */
	double dik; /* inclination correction */
	double rk; /* corrected radius */
	double ik; /* corrected inclination */
	double xkp; /* x position in orbital plane */
	double ykp; /* y position in orbital plane */
	double ok; /* corrected longitude of ascending node */
	double sinok; /* sin of ok */
	double cosok; /* cos of ok */
	double ykpcosik; /* ykp * cos (ik) */
	double sinvk; /* sin (vk) */
	double cosvk; /* cos (vk) */
	double sinek; /* sin (ek) */
	double cosek; /* cos (ek) */
	struct decoded_gps_eph d_eph;

		/* Explode eph. */
		gps_decode_eph(eph, &d_eph);

		/* Magic calculation starts. */
		sat_pos->svid  =  d_eph.svid;
		/* Computed mean motion. */
		n0 = sqrt(MU / pow(d_eph.sqrta, 6.0));
		tk = (gps_tim / 1000) - d_eph.toe;
		if (tk > HALFWEEK)
			tk -= WEEK;
		if (tk < -HALFWEEK)
			tk += WEEK;
		/* Corrected mean motion. */
		n = n0 + d_eph.deltan;
		/* Mean anomaly */
		mk = d_eph.m0 + n*tk;
		/* Kepler's equation for eccentric anomaly .*/
		gps_kepler(&mk, &d_eph.e, &ek);
		cosek = cos(ek);
		sinek = sin(ek);
		cosvk = (cosek - d_eph.e);
		sinvk = sqrt(1.0 - d_eph.e * d_eph.e) * sinek;
		vk = atan2(sinvk, cosvk); /* True anomaly. */
		if (vk < 0.0)
			vk += 2 * C_PI;
		pk = vk + d_eph.omega; /* Argument of latitude */
		sin2pk = sin(2.0 * pk);
		cos2pk = cos(2.0 * pk);
		/* Argument of latitude correction. */
		duk = d_eph.cus * sin2pk + d_eph.cuc * cos2pk;
		/* Radius correction. */
		drk = d_eph.crc * cos2pk + d_eph.crs * sin2pk;
		/* Correction to inclination. */
		dik = d_eph.cic * cos2pk + d_eph.cis * sin2pk;
		uk = pk + duk; /* Latitude. */
		/* Corrcted radius */
		rk = d_eph.sqrta * d_eph.sqrta * (1.0 - d_eph.e * cosek) + drk;
		/* Corrected inclination. */
		ik = d_eph.i0 + dik + d_eph.idot * tk;
		xkp = rk * cos(uk); /* x in orbital plane. */
		ykp = rk * sin(uk); /* y in orbital plane. */
		/* Longitude of ascending node. */
		ok = d_eph.omega0 + (d_eph.omegadot - OMEGA_DOT_E) * tk -
			OMEGA_DOT_E * d_eph.toe;
		ykpcosik = ykp * cos(ik);
		sinok = sin(ok);
		cosok = cos(ok);

		/* sv xyz */
		sat_pos->x = xkp * cosok - ykpcosik * sinok;
		sat_pos->y = xkp * sinok + ykpcosik * cosok;
		sat_pos->z = ykp * sin(ik);

	return 0;
}

static void gps_ecef_to_enu(double xfrom[][3], const double *ecef, double *enu)
{
	unsigned short   i, j;

	for (i=0; i < 3; i++)
	{
		enu[i] = 0.0;

		for(j = 0; j < 3; j++)
		{
			enu[i] += xfrom[i][j] * ecef[j];
		}
	}
}

static void gps_llh_to_enumatrix(double xform[][3], double llh[3])
{
	double	coslat, sinlat, coslon, sinlon;

	/* ECEF-to-ENU transformation matrix. */
	coslat = cos(llh[0]),
	sinlat = sin(llh[0]),
	coslon = cos(llh[1]),
	sinlon = sin(llh[1]);

	xform[0][0] = sinlat * coslon;
	xform[0][1] = sinlat * sinlon;
	xform[0][2] = -coslat;

	xform[1][0] = -sinlon;
	xform[1][1] = coslon;
	xform[1][2] = 0;

	xform[2][0] =  coslat * coslon;
	xform[2][1] =  coslat * sinlon;
	xform[2][2] =  sinlat;
}

static void gps_lla_to_ecef(double lla[], double ecef[])
{
	double coslat, sinlat, coslon, sinlon,e,t1;

	coslat = cos(lla[0]);
	sinlat = sin(lla[0]);
	coslon = cos(lla[1]);
	sinlon = sin(lla[1]);

	e  = sqrt(1.0 - ((WGS84_B * WGS84_B)/(WGS84_A * WGS84_A)));
	t1 = sqrt(1-e*e*sinlat*sinlat);
	
	ecef[0] = (WGS84_A/t1 + lla[2])*coslat * coslon;
	ecef[1] = (WGS84_A/t1 + lla[2])*coslat* sinlon;
	ecef[2] = (WGS84_A*(1-e*e)/t1 + lla[2]) * sinlat;

}

/* converts Cartesian Frame to Geodetic Frame */
 static void gps_convert_ecef_to_lla(double ecef[], double lla[])
 {
        double  t1,t3,t4,t;
        t = (1.0 - (WGS84_B * WGS84_B) / (WGS84_A * WGS84_A));

		t1 = sqrt(ecef[0] * ecef[0] + ecef[1] * ecef[1]);
        t3 = atan2(ecef[2] * WGS84_A,t1*WGS84_B);
        t4 = t/(1.0 - t);

		lla[1] = atan2(ecef[1],ecef[0]);
        lla[0] = atan2((ecef[2] + t4 * WGS84_B * sin(t3)*sin(t3)*sin(t3)),
                                        (t1 - t * WGS84_A * cos(t3)*cos(t3)*cos(t3)));
        lla[2]= t1 /cos(lla[0]) - (WGS84_A /sqrt((1.0 - t * sin(lla[0]) * sin(lla[0]))));
		
        return;
 }

void gps_convert_lla2xyz(struct location_desc *pos, double xyz[])
{
		int lat_rad, altitude;
		int lon_rad;
		double llh[3];

		lat_rad  = (double)(pos->latitude_N)* (1 << 8);
		lon_rad  = (double)(pos->longitude_N)* (1 << 8);
		altitude = (double)(pos->altitude_N);

		if (south == pos->lat_sign)
			lat_rad *= -1;

		if (depth == pos->alt_dir)
			altitude *= -1;

		DBG_L1("GNSS_ASSIST: converted , lat_rad %d,lon_rad %d, altitude %d",
			lat_rad,lon_rad,pos->altitude_N);
				
		llh[0] = (double)((lat_rad) /
				(93206.75556 * C_90_OVER_2_23 * C_LSB_LAT_REP));
		llh[1] = (double)((lon_rad) /
				(46603.37778 * C_360_OVER_2_24 * C_LSB_LON_REP));
		llh[2] = (double)(altitude);

		gps_lla_to_ecef(llh, xyz);
}

unsigned int gps_sat_vis_from_pos(double ecef[],struct sat_pos* sat_pos, 
								  double *Az, double *El)
{
	
		double rho, az, el;
		double dxyzp[3], denup[3], dummy[3][3],lla[3];

		/* Direction vector in ECEF. */
		dxyzp[0] = sat_pos->x - ecef[0];
		dxyzp[1] = sat_pos->y - ecef[1];
		dxyzp[2] = sat_pos->z - ecef[2];

        /*Get Geodetic position to obtain the Coordinate transformation matrix*/
		gps_convert_ecef_to_lla(ecef,&lla[0]);
		gps_llh_to_enumatrix(dummy,lla);
		
		/* Direction vector, denup, is in the East/North/Up system. */
		gps_ecef_to_enu(dummy, dxyzp, denup);

		/* Compute SV elevation and azimuth. */
		rho = sqrt(denup[0] * denup[0] + denup[1] * denup[1] + denup[2] * denup[2]);

		/* Extremely unlikely case near the zenith/nadir. */
		if (rho < 1.0) {
			if (dxyzp[2] > 0.0)
				el =  C_PI/2.0;	/* Straight up. */
			else
				el = -C_PI/2.0;	/* Straight down. */

			/* Azimuth is not define at the poles. */
			az = 0.0;
		}
		else {
			/* Our rho is large enough for safe computing. */
			el  = atan2(denup[2], rho);

			 /* Extremely rare case. */
			if (fabs(denup[0]) < 1.0) {
				if (denup[1] > 0)
					az = 0.0;
				else
					az = -C_PI;
			}
			else {
				az  = atan2(denup[1], -denup[0]);
			}
		}

		/* Only non-negative azimuths for CD and ME. */
		if (az < 0.0) {
			az += 2.0 * C_PI;
		}

		*Az  = az * (180.0/ C_PI);
		*El = el * (180.0/ C_PI);

		return 0;
}

