#include <stdlib.h>
#include "ext_assist.h"
#include "gnss.h"
#include "utils.h"
#include "gps_visible_eph.h"
#include "gnss_utils.h"
#include "3gpp_Utils.h"
#include "debug.h"
#include "os_services.h"

#define MAX_GPS_SAT			32
#define MAX_GLO_SAT			24
#define SV_CN0_THRESHOLD	10
#define GPS_SV_VALID(x)     (((x)>=0)  && ((x)<=(31)))
#define MIN(a, b) 			(a < b)? a : b

static unsigned int gps_assist_map = 0;
static unsigned int ganss_assist_map = 0;
/* GPS External Assistance transient data structures */
struct gps_eph_desc {

        unsigned int    flags;         		/* For use by 3GPP Utils      */
        unsigned int    rec_secs;      		/* Time of record of EPH data */
        unsigned char   num_sats;      		/* # of sat avail in EPH data */
		unsigned char   n_act_sats;			/* # of sat present EPH data */
		unsigned int    sv_avl_map;			/* # of visbile sv injected   */
        struct gps_eph  eph[MAX_GPS_SAT];  	/* EPH given by assist source */
};

struct gps_alm_desc {
        
        unsigned int    rec_secs;      		/* Time of record of ALM data */
        unsigned char   num_sats;      		/* # of sat avail in ALM data */ 
        struct gps_alm  alm[MAX_GPS_SAT];  	/* ALM given by assist source */
};

struct gps_acq_desc {
		unsigned char 	num_sats;			/* # of sat avail of ACQ data */
		unsigned int  	ref_secs;			/* Reference Time of ACQ data */
	unsigned int    sv_map;
		struct gps_acq acq[MAX_GPS_SAT];    /* ACQ given by assist source */
};

struct gps_ref_time_desc {
        
        unsigned int    rec_secs;      		/* Time of record of Time data */
        struct gps_time ref_time;			/* GPS Reference Time */
        unsigned int    ref_secs;			/* Previous saved GPS Time in sec */
		unsigned int    gps_msec;
};

struct gnss_pos_desc {
		unsigned int rec_secs;				/* Time of record of Pos data */
		struct loc_gnss_info loc; 			/* gnss location descriptor data */
};

/* GPS external assist data structure declarations */
static struct gnss_pos_desc 	 gnss_pos_assist_data;
static struct gps_ref_time_desc  gps_reftime_assist_data;
static struct gps_eph_desc       gps_eph_assist_data;
static struct gps_alm_desc       gps_alm_assist_data;
static struct gps_acq_desc		 gps_acq_assist_data;

/* GLONASS External Assistance transient data structures */
struct glo_eph_desc {
		struct glo_eph_info	glo_eph_data;	/* GLO EPH given by assist source */
	    unsigned int    rec_secs;      		/* Time of record of EPH data */
		unsigned char	n_act_sats;
};

struct glo_alm_desc {

		struct glo_alm_info	glo_alm_data;   /* GLO ALM given by assist source */     
        unsigned int    rec_secs;      		/* Time of record of ALM data */
};

struct glo_ref_time_desc {
        unsigned int        rec_secs;		/* Time of record of Time data */
        struct ganss_ref_time ref_time;		/* GLO Reference Time */
		unsigned int        ref_secs;		/* Previous saved GLO Time in sec */
};

/* GLO external assist data structure declarations */
static struct glo_ref_time_desc  glo_reftime_assist_data;
static struct glo_eph_desc       glo_eph_assist_data;
static struct glo_alm_desc       glo_alm_assist_data;

static int ext_assist_tpe_available();
/*------------------------------------------------------------------------------
 * Routines to assess Time-To-Live for assist info provided by external source.
 *----------------------------------------------------------------------------*/

inline static unsigned int gps_ref_now_secs(struct ue_aiding_ref_info *ri)
{
        return (0 == ri->gps_rec_secs) ? 
                0 : (os_monotonic_secs() - ri->gps_rec_secs) + ri->gps_ref_secs;
}

inline static unsigned int glo_ref_now_secs(struct ue_aiding_ref_info *ri)
{
        return (0 == ri->glo_rec_secs) ?
                0 : (os_monotonic_secs() - ri->glo_rec_secs) + ri->glo_ref_secs;
}

inline static unsigned int glo_eph_info_toe_secs(unsigned char n)
{
		unsigned int tb_min;
		unsigned int tb_min_scale;

		/* Apply scale factor for tb*/
		tb_min_scale = (glo_eph_assist_data.glo_eph_data.eph[n].tb * 15);
		
		/* Get time of glo ephemeris using tb */
		tb_min = (tb_min_scale < MAX_MINS_IN_GLO2UTC_DIFF ) ? 
				 (tb_min_scale + MAX_MINS_IN_A_DAY - MAX_MINS_IN_GLO2UTC_DIFF) : 
				 (tb_min_scale - MAX_MINS_IN_GLO2UTC_DIFF);

	/* Add (P1) time interval between two tb to indicate the 
	   actual Toe; tb - p1 -> Glo Toe */
	return tb_min * 60;
}

/* Add / include code from utilities generate ref_secs for each of GPS and GLO in 
   3GPP reference time.

*/

static int get_3gpp_eph_assist_ttl(struct ue_aiding_ref_info *ref_info)
{
        unsigned char num_sats = 0;
        unsigned char i =0;
        unsigned int eph_toe;
        int eph_age;
        int eph_ttl;
        unsigned int gps_secs;
        unsigned int glo_secs;
        int gps_min_ttl = 0x7FFFFFFF, glo_min_ttl = 0x7FFFFFFF;
        unsigned int lpyr_secs;
        unsigned char is_glonass = 0;

	if((0 == gps_eph_assist_data.n_act_sats) && 
	   (0 == glo_eph_assist_data.n_act_sats)) {
		DBG_L1("UE needs EPH; but assist source did not provide any");
		return -1;
	}

        if( 0 != ref_info->gps_rec_secs ) {

				DBG_L1("GNSS_ASSIST: gps %d sats available to get ttl",
								gps_eph_assist_data.n_act_sats);
			
                gps_secs = gps_ref_now_secs(ref_info);
                
                /* run for a loop of all available eph's */
                for(i = 0; i < MAX_GPS_SAT; i++) {

			if (0xFF != gps_eph_assist_data.eph[i].svid) {
				eph_toe = ((gps_reftime_assist_data.ref_time.week_nr + 
					    (1024 * gps_reftime_assist_data.ref_time.n_wk_ro)) * 
					   MAX_SECS_IN_1WEEK) + 
					(gps_eph_assist_data.eph[i].Toe * 16);

                                eph_age = abs((int)(eph_toe - gps_secs));

				eph_ttl = (2*60*60) - 
                                        ((((int)(eph_toe - gps_secs)) > 0) ? 
					 -eph_age : eph_age);

                                DBG_L1("GNSS_ASSIST: gps svid %d, gps_secs %u, week %d, Toe %d, eph_age %d and eph_ttl %d", 
				       gps_eph_assist_data.eph[i].svid, 
				       gps_secs, 
				       gps_secs / MAX_SECS_IN_1WEEK,
				       gps_eph_assist_data.eph[i].Toe,
				       eph_age, 
				       eph_ttl);

				if(eph_ttl > MIN_TTL) {
					num_sats++;
					gps_min_ttl = MIN(gps_min_ttl, eph_ttl);
				}
			}
		}
	}


	DBG_L1("GNSS_ASSIST: gps_min_ttl %d, glo_min_ttl %d",gps_min_ttl,glo_min_ttl);

    if(num_sats < 3) {
        DBG_L1("Inadequate GPS EPH(s); total # of sats is less than 3 (2D)");
        gps_assist_map |= A_GPS_NAV_REQ;
        return -1;
    }

    return gps_min_ttl;		
}

static void prep_ttl_ref_info(struct ue_aiding_ref_info *ref_info)
{
        
        if(0 != gps_reftime_assist_data.rec_secs) {
                /* If GPS Time Assist is available then, use that info */
                ref_info->gps_rec_secs = gps_reftime_assist_data.rec_secs;
                ref_info->gps_ref_secs = gps_reftime_assist_data.ref_secs;
        }

        if(0 != glo_reftime_assist_data.rec_secs) {
                /* If GLO Time Assist is available then, use that info */
                ref_info->glo_rec_secs = glo_reftime_assist_data.rec_secs;
                ref_info->glo_ref_secs = glo_reftime_assist_data.ref_secs;
        }
        
        if(0 != gnss_pos_assist_data.rec_secs) {
                /* If POS Ref  Assist is available then, use that info */
                ref_info->pos_rec_secs = gnss_pos_assist_data.rec_secs;
        }
        
}

/* Need reference GPS and GLO TIME */
int ext_assist_get_ttl(void)
{
        struct ue_aiding_ref_info    ref_info[1]; /* Assume small structure */
	memset(&ref_info[0], 0x00, sizeof(struct ue_aiding_ref_info ));	
	prep_ttl_ref_info(ref_info);
       
        if((0 == ref_info->gps_rec_secs) && (0 == ref_info->glo_rec_secs)) {
                DBG_L1("GNSS_ASSIST: No GPS & GLO Time info avail from assist source");
                return -1;
        }

        if(0 == ref_info->pos_rec_secs) {
                DBG_L1("GNSS_ASSIST: No Ref (GNSS) POS info avail from assist source");
                return -1;
        }
        
	return get_3gpp_eph_assist_ttl(ref_info);
}

int ext_assist_get_need(struct gps_assist_req     *gps_assist, 
                        struct ganss_assist_req *ganss_assist)
{
	gps_assist->assist_req_map = gps_assist_map;
	ganss_assist->common_req_map = ganss_assist_map;
	DBG_L1("GNSS_ASSIST: gps assist req map %0x, ganss assist req map %0x", 
		gps_assist_map, ganss_assist_map);	
	return 0;
}
int ext_assist_put_gnss_loc(struct location_desc *location)
{
	gnss_pos_assist_data.rec_secs = os_monotonic_secs();
	memcpy(&gnss_pos_assist_data.loc.location, location, 
			sizeof(struct location_desc));

	if ((0 != location->latitude_N) && (0 != location->longitude_N) &&
			(0 != location->altitude_N)) {
			gps_assist_map &= ~A_GPS_POS_REQ;
		}

	DBG_L1("GNSS_ASSIST: Latitude %d, Longitude %d, Altitude %d", 
	       location->latitude_N, location->longitude_N, 
	       location->altitude_N);
	return 1;
}
int ext_assist_put_gnss_loc_spec(struct location_desc *location)
{
	if (gps_assist_map & A_GPS_POS_REQ) {

		gnss_pos_assist_data.rec_secs = os_monotonic_secs();
			memcpy(&gnss_pos_assist_data.loc.location, location, 
			       sizeof(struct location_desc));
			
			if ((0 != location->latitude_N) && (0 != location->longitude_N) &&
			    (0 != location->altitude_N)) {
				gps_assist_map &= ~A_GPS_POS_REQ;
			}
		
			DBG_L1("GNSS_ASSIST: Latitude %d, Longitude %d, Altitude %d and gps_assist_map 0x%x", 
			       location->latitude_N, location->longitude_N, 
			       location->altitude_N, gps_assist_map);
	}

	return 1;
}

int ext_assist_put_gps_alm( struct gps_alm *alm, int num_obj)
{
	gps_alm_assist_data.rec_secs = os_monotonic_secs();
	gps_alm_assist_data.num_sats = num_obj;

	memcpy(&gps_alm_assist_data.alm + 0, alm, 
			sizeof(struct gps_alm) * num_obj);
	return 0;
}

int ext_assist_get_gps_alm(struct gps_alm *alm, int num_obj)
{
	unsigned char num = 0;
	
	num = gps_alm_assist_data.num_sats; 
	if(num > num_obj)
			num = num_obj;
	
	memcpy(alm, &gps_alm_assist_data.alm + 0, num*sizeof(struct gps_alm));
	
	return num;
}

int ext_assist_put_gps_time(struct gps_time *time)
{
	gps_reftime_assist_data.rec_secs = os_monotonic_secs();

	memcpy(&gps_reftime_assist_data.ref_time, time, 
	       sizeof(struct gps_time));

	if (milli_secs == time->tow_unit) {
		gps_reftime_assist_data.gps_msec = time->tow;
		gps_reftime_assist_data.ref_secs = time->tow / 1000; 
	} else if (sec_8by100 == time->tow_unit) {
		gps_reftime_assist_data.gps_msec = time->tow * 80;
		gps_reftime_assist_data.ref_secs = gps_reftime_assist_data.gps_msec / 1000; 
		
	} else if (clear_secs == time->tow_unit) {
		gps_reftime_assist_data.gps_msec = time->tow*1000;
		gps_reftime_assist_data.ref_secs = time->tow; 
	}

	gps_reftime_assist_data.ref_secs += gps_time_now(time->week_nr,time->n_wk_ro, 0);
	
	DBG_L1("GNSS_ASSIST: gps week_nr %d, n_wk_ro %d, tow %d, ref secs %d, msec %d", 
	       time->week_nr, time->n_wk_ro, time->tow, 
	       gps_reftime_assist_data.ref_secs, 
	       gps_reftime_assist_data.gps_msec);
	

	if (gps_reftime_assist_data.ref_secs)
		gps_assist_map &= ~A_GPS_TIM_REQ;

	return 1;
}

int ext_assist_put_gps_time_spec(struct gps_time *time)
{
	if (gps_assist_map & A_GPS_TIM_REQ) {
		gps_reftime_assist_data.rec_secs = os_monotonic_secs();
			
			memcpy(&gps_reftime_assist_data.ref_time, time, 
			       sizeof(struct gps_time));
			
			if (milli_secs == time->tow_unit) {
				gps_reftime_assist_data.gps_msec = time->tow;
					gps_reftime_assist_data.ref_secs = time->tow / 1000; 
			} else if (sec_8by100 == time->tow_unit) {
				gps_reftime_assist_data.gps_msec = time->tow * 80;
					gps_reftime_assist_data.ref_secs = gps_reftime_assist_data.gps_msec / 1000; 
					
			} else if (clear_secs == time->tow_unit) {
				gps_reftime_assist_data.gps_msec = time->tow*1000;
					gps_reftime_assist_data.ref_secs = time->tow; 
			}
		
			gps_reftime_assist_data.ref_secs += gps_time_now(time->week_nr,time->n_wk_ro, 0);

			if (gps_reftime_assist_data.ref_secs)
				gps_assist_map &= ~A_GPS_TIM_REQ;

			DBG_L1("GNSS_ASSIST: gps week_nr %d, n_wk_ro %d, tow %d, ref secs %d, msec %d and gps_assist_map 0x%x", 
			       time->week_nr, time->n_wk_ro, time->tow, 
			       gps_reftime_assist_data.ref_secs, 
			       gps_reftime_assist_data.gps_msec, gps_assist_map);
	}

	return 1;
}

int ext_assist_put_gps_eph(struct gps_eph *eph, int n_obj)
{
		struct gps_eph *dst;
		unsigned char sv_id;
        int n = 0;

	DBG_L1("GNSS_ASSIST: provisioned %d GPS EPH Obj(s)", n_obj);

        for(n = 0; n < n_obj; n++) {

		sv_id = eph[n].svid;	

		if (!(gps_eph_assist_data.sv_avl_map & (1 << sv_id)))
	          	gps_eph_assist_data.num_sats++; 			
		
			gps_eph_assist_data.sv_avl_map |= (1 << sv_id);
			DBG_L2("GNSS_ASSIST: svid %d", sv_id);
          	dst = gps_eph_assist_data.eph + sv_id;
          	memcpy(dst, eph + n, sizeof(struct gps_eph));                
        }

        gps_eph_assist_data.rec_secs = os_monotonic_secs();

	DBG_L1("GNSS_ASSIST: net external GPS EPH Obj(s) %d; sv available map 0x%x", 
	       gps_eph_assist_data.num_sats, gps_eph_assist_data.sv_avl_map);

		gps_eph_assist_data.n_act_sats = gps_eph_assist_data.num_sats;

	if (gps_eph_assist_data.num_sats)
		gps_assist_map &= ~A_GPS_NAV_REQ;
        return 0;
}

int ext_assist_put_gps_acq(struct gps_acq *acq, int num_obj)
{
        int n = 0;
	unsigned char sv_id;
        struct gps_acq *dst = gps_acq_assist_data.acq + 0;

	DBG_L1("GNSS_ASSIST: provisioned %d GPS ACQ Obj(s)", num_obj);
        /* Mark all previously availalbe ACQ data as alraedy known to device */
        for(n = 0; n < MAX_GPS_SAT; n++, dst++) {
                dst->use_flag = 0;
        }

        /* Save all new external ACQ data; use_flag is set as 1 for device */ 
        for(n = 0; n < num_obj; n++) {
		sv_id =  acq[n].svid;

		if (!(gps_acq_assist_data.sv_map & (1 << sv_id)))
			gps_acq_assist_data.num_sats++;

		dst = gps_acq_assist_data.acq + sv_id;
		gps_acq_assist_data.sv_map |= (1 << sv_id);
                memcpy(dst, acq + n, sizeof(struct gps_acq));
                dst->use_flag = 1; /* let device know about new entry */
        }

        gps_acq_assist_data.ref_secs = os_monotonic_secs();
        
	DBG_L1("GNSS_ASSIST: GPS ACQ sv map %0x ; net external GPS ACQ Obj(s) %d", \
	       gps_acq_assist_data.sv_map,gps_acq_assist_data.num_sats);

	if (gps_acq_assist_data.num_sats)
		gps_assist_map &= ~A_GPS_ACQ_REQ;

        return 0;
}

int ext_assist_get_gps_acq(struct gps_acq *acq, int num_obj)
{
        int n = 0,    n_obj = gps_acq_assist_data.num_sats;
	int k = 0;
	struct gps_acq *src_acq = gps_acq_assist_data.acq + 0;
	
	DBG_L1("GNSS_ASSIST: requested %d GPS ACQ Obj(s), Available %d GPS ACQ Obj(s)", 
		num_obj, n_obj);

	if (gps_assist_map & A_GPS_TIM_REQ) {
		DBG_L1("GNSS_ASSIST: GPS time required to inject GPS Acq Obj(s)");
		return 0;
	}
	
	if (0 == gps_acq_assist_data.num_sats) {		
		DBG_L1("GNSS_ASSIST: No GPS Acq Obj(s) Available");
		return 0;
	}

	while(( n < MAX_GPS_SAT)) {
			
		/* Pick ACQ data that has been provisioned by ext entity */
		if (0xFF != src_acq->svid) {

			DBG_L1("GNSS_ASSIST: GPS ACQ svid %d, GPS ACQ flag %d", 
				src_acq->svid,src_acq->use_flag);		
		
			memcpy(acq + k, src_acq, sizeof(struct gps_acq));
			k++;	
			//gps_acq_assist_data.sv_map &= ~(1 << src_acq->svid);
			//gps_acq_assist_data.num_sats--;	
		}
		n++;
		src_acq++;
	}
	DBG_L1("GNSS_ASSIST: providing %d GPS ACQ Obj(s)", k);
	
	
	
	return k;
}

int ext_assist_put_glo_time(struct ganss_ref_time *time)
{
        glo_reftime_assist_data.rec_secs = os_monotonic_secs();

        memcpy(&glo_reftime_assist_data.ref_time, time, 
					sizeof(struct ganss_ref_time));

        glo_reftime_assist_data.ref_secs = 
				glo_time_now(glo_reftime_assist_data.ref_time.day,
				glo_reftime_assist_data.ref_time.tod);

	DBG_L1("GNSS_ASSIST: glo day %d, tod %d, ref secs %d", 
	       time->day, time->tod, glo_reftime_assist_data.ref_secs);

	return 0;
}

int ext_assist_put_glo_nav_mdl(unsigned int utc_secs, 
							   struct glo_nav_model *nav_mdl, 
                               unsigned char num_nav)
{
        return add2glo_icd_eph(&glo_eph_assist_data.glo_eph_data, utc_secs, 
								nav_mdl, num_nav);
}

int ext_assist_put_glo_kp_alm(unsigned int utc_secs,
							  struct glo_alm_kp_set *kp_set, 
                              unsigned char num_set)
{
        return add_kp2glo_icd_alm(&glo_alm_assist_data.glo_alm_data, utc_secs, 
								kp_set, num_set);
}

int ext_assist_put_glo_utc_mdl(struct glo_utc_model *utc_mdl)
{
        return add_utc2glo_icd_alm(&glo_alm_assist_data.glo_alm_data, utc_mdl);
}

int ext_assist_put_glo_time_mdl(struct ganss_ref_time   *glo_time,
                                 struct ganss_time_model *time_mdl)
{
        return add_time2glo_icd_alm(&glo_alm_assist_data.glo_alm_data, 
									glo_time, time_mdl);
}


int ext_assist_get_gnss_loc(struct location_desc *location)
{
        memcpy(location,&gnss_pos_assist_data.loc.location,
				sizeof(struct location_desc));
	    return 0;
}

/* Returns number of ALM objects fetched */
int ext_assist_get_glo_icd_alm(struct glo_alm *alm, unsigned char num_obj)
{
        unsigned char num = 0;

        if(1 == is_glo_icd_alm(&glo_alm_assist_data.glo_alm_data)) {
                num = glo_alm_assist_data.glo_alm_data.n_sat; 
                if(num > num_obj)
                        num = num_obj;
                
                memcpy(alm, glo_alm_assist_data.glo_alm_data.alm + 0, 
					num*sizeof(struct glo_alm));
        }

        return num;
}

/* Returns number of EPH objects fetched */
int ext_assist_get_glo_icd_eph(struct glo_eph *eph, unsigned char num_obj)
{
        unsigned char num = 0;

        if(1 == is_glo_icd_eph(&glo_eph_assist_data.glo_eph_data)) {
                num = glo_eph_assist_data.glo_eph_data.n_sat; 
                if(num > num_obj)
                        num = num_obj;
                
                memcpy(eph, glo_eph_assist_data.glo_eph_data.eph + 0, 
						num*sizeof(struct glo_eph));
        }

        return num;
}

/* Qualify visible gps ephemeris. */
int ext_assist_get_gps_eph_visible (struct gps_eph* eph_data,  int max_sat )
{
	static struct sat_pos sat_pos_info[32];
	unsigned int sat_map = 0;
	unsigned int num_sats = 0;
	unsigned int num_obj = 0;
	int idx = 0, n = 0;
	double Az = 0.0, El = 0.0;
	double xyz[3];
	
	struct gps_eph* eph_ptr = &gps_eph_assist_data.eph[0];

	/* Make sure time, pos and eph available */
	if (!ext_assist_tpe_available()) {
		DBG_L1("GNSS_ASSIST: visibility computation needs eph, time and position");
		return 0;
	}

	DBG_L1("GNSS_ASSIST: %d sats available to filter sv-visibility computation",
	       gps_eph_assist_data.num_sats);
	/* gps time in msec */
	DBG_L1("GNSS_ASSIST: gps time in msec %d",
		gps_reftime_assist_data.gps_msec);
	
	gps_convert_lla2xyz(&gnss_pos_assist_data.loc.location, &xyz[0]);
	for (n=0;n < MAX_GPS_SAT; n++) {

		if (GPS_SV_VALID(gps_eph_assist_data.eph[n].svid)) {

			DBG_L1("GNSS_ASSIST: compute sv visibility for svid %d",gps_eph_assist_data.eph[n].svid);
						
			/* Calculate gps sat's position from externally provided gps eph. */
			gps_sat_pos_from_eph(gps_reftime_assist_data.gps_msec, 
								&gps_eph_assist_data.eph[n], 
								&sat_pos_info[n]);

			/* Compare gps sat's pos with receiver pos and calculate visibility. */
			gps_sat_vis_from_pos(&xyz[0], &sat_pos_info[n], &Az, &El);

			if ( El > 4.5) {
				sat_map |= (unsigned int) (1 << (gps_eph_assist_data.eph[n].svid));
				DBG_L1("GNSS_ASSIST: svid %d,elevation[deg] %f",gps_eph_assist_data.eph[n].svid,El);
			}
		}
	}
	DBG_L1("GNSS_ASSIST: sv available map %0x;sat visbile map %0x ",
			gps_eph_assist_data.sv_avl_map,sat_map);


	for(idx = 0; idx < MAX_GPS_SAT; idx++, eph_ptr++) {

		if (GPS_SV_VALID(eph_ptr->svid)) {
			
			/* Check if sv is available in sat visibility map. */
			if (0 == ((1 << (eph_ptr->svid)) & sat_map)) {
				continue;
			}

			/* Only available eph to select */
			if (gps_eph_assist_data.sv_avl_map & (1 << eph_ptr->svid)) {
				
				DBG_L1("GNSS_ASSIST: copy visible svid %d ephemeris for injection",
						eph_ptr->svid);
				memcpy(eph_data, eph_ptr , sizeof(struct gps_eph));
			    num_obj++;
			    eph_data++;

				/* Knock off the availability flag in sv map */
				gps_eph_assist_data.sv_avl_map &= ~(1 << eph_ptr->svid);
				gps_eph_assist_data.num_sats--;
			
			}
		}
    }

	DBG_L1("GNSS_ASSIST: sv available map %0x; num sats available %d",
		gps_eph_assist_data.sv_avl_map, gps_eph_assist_data.num_sats);

	DBG_L1("GNSS_ASSIST: visible sv eph injected from sv-visibility computation %d", 
	       num_obj);

	return num_obj;
}	

/* scan msr report to extract sv which have not been injected to the device*/
int ext_assist_get_gps_eph_from_msr (const struct gps_msr_info* msr_info,
                                     unsigned short num_msr, 
                                     struct gps_eph* eph_data,
                                     int num_obj) 
{
	unsigned char svid = 0;
	int idx;
	struct gps_eph *ext_eph = &gps_eph_assist_data.eph[0];
	struct gps_eph *msr_eph = eph_data;

	if(gps_eph_assist_data.num_sats == 0 ){
		DBG_L1("GNSS_ASSIST: UE needs EPH; but no eph available");
		return -1;
	}

	DBG_L1("GNSS_ASSIST: %d sats present in msr info, sv available map %0x ",
		num_msr,gps_eph_assist_data.sv_avl_map);

	DBG_L1("GNSS_ASSIST: %d sats available for msr sv visibility search",
		gps_eph_assist_data.num_sats);
		

	
	/* Search for all msr reported satellites */
    for(idx = 0; (idx < num_msr) && num_obj; idx++, msr_info++) {

	    svid = msr_info->svid;

		DBG_L1("GNSS_ASSIST: msr svid %d, msr cn0 %d",svid,msr_info->c_no);
		 
		if((!GPS_SV_VALID(svid))  || (0 == ((1 << svid) & gps_eph_assist_data.sv_avl_map))) 
		          continue;
		
		  if ((msr_info->c_no > SV_CN0_THRESHOLD)) {
		  	
			 	memcpy(msr_eph, ext_eph + svid, sizeof(struct gps_eph));
	            num_obj--;
	            msr_eph++;
	                
	            DBG_L2("GNSS_ASSIST: selected msr visible svid %d for injection", svid);

	            gps_eph_assist_data.sv_avl_map &= ~(1 << svid);
	            gps_eph_assist_data.num_sats--;
		 	}
	    }

	DBG_L1("GNSS_ASSIST: sv available map %0x, msr sv visible for injection %d ",
			gps_eph_assist_data.sv_avl_map, (msr_eph - eph_data));

		DBG_L1("GNSS_ASSIST: end --> msr sv visible search");
		
		return msr_eph - eph_data;
}

int ext_assist_gps_eph_count()
{
	DBG_L1("GNSS_ASSIST: gps eph available count %d",
			gps_eph_assist_data.num_sats);
	return	gps_eph_assist_data.num_sats;
}

static int ext_assist_tpe_available()
{
	DBG_L1("GNSS_ASSIST: Assistance availability map %0x",
			gps_assist_map);

	if ( !(gps_assist_map & A_GPS_POS_REQ ) && !(gps_assist_map & A_GPS_TIM_REQ) &&
			!(gps_assist_map & A_GPS_NAV_REQ))  
		return 1;
	else
		return 0;
}

int ext_assist_put_ganss_acq(struct ganss_ref_msr *msr, int num_obj)
{
	/* GLONASS MSA : TBD */

	return 0;
}

int ext_assist_get_ganss_acq(struct ganss_ref_msr *msr, int num_obj)
{ 
	/* GLONASS MSA : TBD */

	return 0;
}

int ext_assist_del_ueb(void)
{
	/* Reset GPS reference time */
	gps_reftime_assist_data.rec_secs = 0;
	
	/* Reset GPS EPH info */
	gps_eph_assist_data.num_sats = 0;
	gps_eph_assist_data.n_act_sats = 0;
	gps_eph_assist_data.rec_secs = 0;
	gps_eph_assist_data.flags = 0;
	gps_eph_assist_data.sv_avl_map = 0;
	
	memset(&gps_eph_assist_data.eph, 0x00, 
			MAX_GPS_SAT * sizeof(struct gps_eph));

	/* Reset GPS ALM info */
	gps_alm_assist_data.num_sats = 0;
	gps_alm_assist_data.rec_secs = 0;
	
	memset(&gps_alm_assist_data.alm, 0x00, 
			MAX_GPS_SAT * sizeof(struct gps_alm));

	/* Reset GLO reference time */
	glo_reftime_assist_data.rec_secs = 0;

	/* Reset GLO EPH info */
	glo_eph_assist_data.glo_eph_data.n_sat = 0;
	glo_eph_assist_data.rec_secs = 0;
	glo_eph_assist_data.glo_eph_data.flags = 0;

	memset(&glo_eph_assist_data.glo_eph_data.eph, 0x00, 
			MAX_GLO_SAT * sizeof(struct glo_eph));

	/* Reset GLO ALM info */
	glo_alm_assist_data.glo_alm_data.n_sat = 0;
	glo_alm_assist_data.rec_secs = 0;
	glo_alm_assist_data.glo_alm_data.flags = 0;

	memset(&glo_alm_assist_data.glo_alm_data.alm, 0x00, 
			MAX_GLO_SAT * sizeof(struct glo_alm));

	ganss_assist_map = ( A_GANSS_TIM_REQ | A_GANSS_POS_REQ | A_GANSS_NAV_REQ | A_GANSS_MSR_REQ );

	gps_assist_map = ( A_GPS_TIM_REQ | A_GPS_POS_REQ | A_GPS_NAV_REQ | A_GPS_ACQ_REQ );
	
					/* A_GPS_ALM_REQ | A_GPS_UTC_REQ | A_GPS_ION_REQ );
					   A_GPS_RTI_REQ | A_GPS_DGP_REQ */	

	return 0;
}

int ext_assist_del_uea(void)
{
	struct gps_acq *src;
	int n = 0;

	/* Clear all contents that were provisioned for UE-Assisted case */
	gps_acq_assist_data.num_sats = 0;
	gps_acq_assist_data.sv_map = 0;
	gps_acq_assist_data.ref_secs = 0;
	
	for(n = 0; n < MAX_GPS_SAT; n++) {
		src = gps_acq_assist_data.acq + n;
		src->svid     = 0xFF;
		src->use_flag = 0;
	}

	memset(&gps_reftime_assist_data,0x00, sizeof(struct gps_ref_time_desc));

	ganss_assist_map = ( A_GANSS_TIM_REQ | A_GANSS_POS_REQ | A_GANSS_NAV_REQ | A_GANSS_MSR_REQ );
	gps_assist_map = ( 	A_GPS_TIM_REQ | A_GPS_POS_REQ | A_GPS_NAV_REQ | A_GPS_ACQ_REQ );

	return 0;
}
/* Module init */
int ext_assist_mod_init(void)
{

	unsigned short sat_num;
	
	memset (&gnss_pos_assist_data, 0x00,sizeof(struct gnss_pos_desc));
	memset (&gps_reftime_assist_data, 0x00,
			sizeof(struct gps_ref_time_desc));  
	memset (&gps_eph_assist_data,0x00, sizeof(struct gps_eph_desc));

	for(sat_num = 0; sat_num < MAX_GPS_SAT; sat_num++) {
			struct gps_eph *src = gps_eph_assist_data.eph+ sat_num;
			src->svid	  = 0xFF;
	}	
	
	memset (&gps_alm_assist_data,0x00,sizeof(struct gps_alm_desc));		 

	memset (&gps_acq_assist_data,0x00, sizeof(struct gps_acq_desc));	

	for(sat_num = 0; sat_num < MAX_GPS_SAT; sat_num++) {
			struct gps_acq *src = gps_acq_assist_data.acq + sat_num;
			src->svid	  = 0xFF;
			src->use_flag = 0;
	}
	
	memset (&glo_reftime_assist_data,0x00 , 
			sizeof (struct glo_ref_time_desc));  
	memset (&glo_eph_assist_data,0x00,sizeof(struct glo_eph_desc));       
	memset (&glo_alm_assist_data,0x00,sizeof(struct glo_alm_desc));  

	/*Request GLONASS assistance */
	ganss_assist_map = ( A_GANSS_TIM_REQ | A_GANSS_POS_REQ | A_GANSS_NAV_REQ | A_GANSS_MSR_REQ );
	gps_assist_map = ( 	A_GPS_TIM_REQ | A_GPS_POS_REQ | A_GPS_NAV_REQ | A_GPS_ACQ_REQ );

						/* A_GPS_ALM_REQ | A_GPS_UTC_REQ | A_GPS_ION_REQ | 
						   A_GPS_ACQ_REQ */
	return 0;

}

/* Module exit */
int ext_assist_mod_exit(void)
{
	return 0;
}

