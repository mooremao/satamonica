.PHONY: help
help:
	@echo "******************** make ********************"
	@echo "Build options for TI dev-proxy"
	@echo "1. make all"
	@echo "2. make libagnss"
	@echo "3. make libgnssutils"
	@echo "4. make libassist"
	@echo "5. make agnss_connect"
	@echo "6. make pfmapp"
	@echo "7. make clean"
	@echo "**********************************************"
	@echo ""


PWD			     = $(shell pwd)

CC			     = $(CROSS_COMPILE)gcc
CPP			     = $(CROSS_COMPILE)g++
LD			     = $(CROSS_COMPILE)ld
STRIP			    = $(CROSS_COMPILE)strip

INCLUDE_PATH     = include config core common assist debug $(LOCAL_PATH)../include/connect $(LOCAL_PATH)../include/dproxy $(LOCAL_PATH)../include/common $(LOCAL_PATH)../infrastructure/log/include $(LOCAL_PATH)../infrastructure/log/adapter/cmcc $(LOCAL_PATH)../dproxy/server

INCLUDE          = $(foreach DIR,$(INCLUDE_PATH),-I$(DIR))

CLI_LIBRARY      = libagnss
CLI_LIB_SRCS     = $(wildcard common/*.cpp config/*.cpp debug/*.cpp)
CLI_LIB_OBJS     = $(patsubst %.cpp,%.o,$(CLI_LIB_SRCS))
CLI_LIB_SRCS1    = $(wildcard debug/*.c)
CLI_LIB_OBJS1    = $(patsubst %.c,%.o,$(CLI_LIB_SRCS1))

CLI_UTIL_LIBRARY = libgnssutils
CLI_UTIL_SRCS    = $(wildcard utils/*.c)
CLI_UTIL_OBJS    = $(patsubst %.c,%.o,$(CLI_UTIL_SRCS))

CLI_ASSIST_LIBRARY = libassist
CLI_ASSIST_SRCS    = $(wildcard assist/*.c)
CLI_ASSIST_OBJS    = $(patsubst %.c,%.o,$(CLI_ASSIST_SRCS))

SRV_APP         = agnss_connect
SRV_APP_SRCS    = $(wildcard core/*.cpp adapter/gnss_ue_adapter.cpp  adapter/ti_agnss_assist_sa_pgps.cpp adapter/ti_agnss_lcs_pfm_app.cpp)
SRV_APP_OBJS    = $(patsubst %.cpp,%.o,$(SRV_APP_SRCS))

PFM_APP         = pfmapp
PFM_APP_SRCS    = $(wildcard test/lcs_usr_test.cpp)
PFM_APP_OBJS    = $(patsubst %.cpp,%.o,$(PFM_APP_SRCS))

FIX_TEST_APP         = fix_test
FIX_TEST_APP_SRCS    = $(wildcard test/fix_test.cpp)
FIX_TEST_APP_OBJS    = $(patsubst %.cpp,%.o,$(FIX_TEST_APP_SRCS))

TI_GPS_TEST_APP         = ti_gps_test
TI_GPS_TEST_APP_SRCS    = $(wildcard test/ttci/*.cpp)
TI_GPS_TEST_APP_OBJS    = $(patsubst %.cpp,%.o,$(TI_GPS_TEST_APP_SRCS))

CXXFLAGS           =-Wall -fPIC -g ${INCLUDE} -DENABLE_DEBUG=1 -DENABLE_TRACE=1 -DGNSS_RESULTS
CFLAGS           = -Wall -fPIC -g ${INCLUDE}
LDFLAGS            = "-Wl,--no-as-needed" -lpthread -lm -lrt
LIBDIRS	      = -L../libs

all: $(CLI_LIBRARY) $(CLI_UTIL_LIBRARY) $(CLI_ASSIST_LIBRARY) $(SRV_APP) $(PFM_APP) $(FIX_TEST_APP) $(TI_GPS_TEST_APP)
	$(info Successfully built all.)

$(CLI_LIBRARY): $(CLI_LIB_OBJS) $(CLI_LIB_OBJS1)
	@$(CC) ${INCLUDE} -g -c -fPIC $(CLI_LIB_OBJS1)
	@$(CPP) -o libagnss.so $(INCLUDE) -fPIC $(CLI_LIB_SRCS) $(CLI_LIB_OBJS1) $(LDFLAGS) -shared
	@$(STRIP) libagnss.so
	@ln -sf libagnss.so libagnss.so.1.0
	@ln -sf libagnss.so libagnss.so.1
	@cp -Prv libagnss.so ../libs/
	@cp -Prv libagnss.so.1 ../libs/
	@cp -Prv libagnss.so.1.0 ../libs/
	$(info Successfully built $(CLI_LIBRARY).)

$(CLI_UTIL_LIBRARY): $(CLI_UTIL_OBJS)
	@$(CC) ${INCLUDE} -g -c -fPIC $(CLI_UTIL_OBJS)
	@$(CC) -o libgnssutils.so $(CLI_UTIL_OBJS) -shared $(LDFLAGS)
	@$(STRIP) libgnssutils.so
	@ln -sf libgnssutils.so libgnssutils.so.1.0
	@ln -sf libgnssutils.so libgnssutils.so.1
	@cp -Prv libgnssutils.so ../libs/
	@cp -Prv libgnssutils.so.1 ../libs/
	@cp -Prv libgnssutils.so.1.0 ../libs/
	$(info Successfully built $(CLI_UTIL_LIBRARY).)

$(CLI_ASSIST_LIBRARY): $(CLI_ASSIST_OBJS)
	@$(CC) ${INCLUDE} -g -c -fPIC $(CLI_ASSIST_OBJS)
	@$(CC) -o libassist.so $(CLI_ASSIST_OBJS) -shared $(LIBDIRS) $(LDFLAGS) -ldevproxy -lgnssutils
	@$(STRIP) libassist.so
	@ln -sf libassist.so libassist.so.1.0
	@ln -sf libassist.so libassist.so.1
	@cp -Prv libassist.so ../libs/
	@cp -Prv libassist.so.1 ../libs/
	@cp -Prv libassist.so.1.0 ../libs/
	$(info Successfully built $(CLI_ASSIST_LIBRARY).)


$(SRV_APP): $(SRV_APP_OBJS)
	@$(CPP) -o $@ $(SRV_APP_SRCS) $(INCLUDE) -fPIC $(LIBDIRS) $(LDFLAGS) -lstdc++ -lagnss -lgnssutils -ldevproxy -lassist -lclientlogger
	@$(STRIP) $(SRV_APP)
	@cp $(SRV_APP) ../bins/
	$(info Successfully built $(SRV_APP).)
	
$(PFM_APP): $(PFM_APP_OBJS)
	@$(CPP) -o $@ $(PFM_APP_SRCS) $(INCLUDE) -fPIC $(LIBDIRS) $(LDFLAGS) -lagnss -lgnssutils -ldevproxy -lassist -lclientlogger
	@$(STRIP) $(PFM_APP)
	@cp $(PFM_APP) ../bins/
	$(info Successfully built $(PFM_APP).)

$(FIX_TEST_APP): $(FIX_TEST_APP_OBJS)
	@$(CPP) -o $@ $(FIX_TEST_APP_SRCS) $(INCLUDE) -fPIC $(LIBDIRS) $(LDFLAGS) -lagnss -lgnssutils -ldevproxy -lassist -lclientlogger
	@$(STRIP) $(FIX_TEST_APP)
	@cp $(FIX_TEST_APP) ../bins/
	$(info Successfully built $(FIX_TEST_APP).)

$(TI_GPS_TEST_APP): $(TI_GPS_TEST_APP_OBJS)
	@$(CPP) -o $@ $(TI_GPS_TEST_APP_SRCS) $(INCLUDE) -fPIC $(LIBDIRS) $(LDFLAGS) -lagnss -lgnssutils -ldevproxy -lassist -lclientlogger
	@$(STRIP) $(TI_GPS_TEST_APP)
	@cp $(TI_GPS_TEST_APP) ../bins/
	$(info Successfully built $(TI_GPS_TEST_APP).)
	
.SUFFIXES: .o
.o:
	@$(CPP) $< -o $@

.PHONY: clean
clean:
	@rm -rvf $(CLI_LIBRARY)
	@rm -rvf $(CLI_LIB_OBJS)
	@rm -rvf $(CLI_LIB_OBJS1)
	@rm -rvf $(SRV_APP)
	@rm -rvf $(SRV_APP_OBJS)
	@rm -rvf $(PFM_APP)
	@rm -rvf $(PFM_APP_OBJS)
	@rm -rvf $(FIX_TEST_APP)
	@rm -rvf $(FIX_TEST_APP_OBJS)
	@rm -rvf $(TI_GPS_TEST_APP)
	@rm -rvf $(TI_GPS_TEST_APP_OBJS)
	@rm -rvf $(CLI_UTIL_LIBRARY)
	@rm -rvf $(CLI_UTIL_OBJS)
	@rm -rvf $(CLI_ASSIST_LIBRARY)
	@rm -rvf $(CLI_ASSIST_OBJS)
	@rm -rvf libagnss.so.1.0
	@rm -rvf libagnss.so.1
	@rm -rvf libgnssutils.so.1.0
	@rm -rvf libgnssutils.so.1
	@rm -rvf libassist.so.1.0
	@rm -rvf libassist.so.1
	@rm -rvf *.so
	@rm -rvf *.o
	@echo "Removed all."
