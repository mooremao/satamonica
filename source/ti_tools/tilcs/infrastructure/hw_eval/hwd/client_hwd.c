/*
 * client_app.c
 *
 * RPC Client test application
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/un.h>
#include <sys/stat.h>

#include "device.h"
#include "dev_proxy.h"
#include "rpc.h"
#include "utils.h"

unsigned int ai2_get_nbytes(unsigned char **buf, unsigned char num_bytes)
{
	unsigned int result = 0;
	unsigned char i;

	if (num_bytes > 4)
		num_bytes = 4;

	*buf += (num_bytes - 1);

	for(i = num_bytes; i; i--) {
		result <<= 8;
		result |= (unsigned int) *((*buf)--);
	}

	*buf += (num_bytes + 1);
	return result;
}

unsigned char *ai2_put_nbytes(unsigned int bytes, unsigned char *buf,
						unsigned char num_bytes)
{
	if (num_bytes > 4)
		num_bytes = 4;

	for(; num_bytes; num_bytes--) {
		*buf++ = (unsigned char)bytes;
		bytes >>= 8;
	}

	return buf;
}

#include "ai2.h"

struct hw_eval_msg_head {

        unsigned int   xact_id;                        /* Transaction ID for the Message  */
        unsigned int   func_id;                        /* ID of  API/func operation  */
        unsigned int   sndr_id;                        /* ID of sender user; ref user id  */
        unsigned short data_sz;                        /* Length (B) of data in  Message  */
};

#define MAX_HW_EVAL_MSG_LEN (sizeof(struct hw_eval_msg_head) + 65535)

static int get_integer_input(void)
{
        char arr[100];

        fgets(arr, sizeof(arr), stdin);

        return atoi(arr);
}

static int get_choice(const char *str, int lower_limit, int upper_limit)
{
        int choice;

        do {
                printf("%s", str);

                choice = get_integer_input();

                printf("\nChoice [%d] entered\n", choice);

        } while ((choice < lower_limit && (printf("Invalid choice [%d]\n", choice))) ||
                 ((choice > upper_limit && printf("Invalid choice [%d]\n", choice))));

        return choice;
}

static int menu(void)
{
        int choice;

        do {
                printf("	MENU\n");
                printf("************\n");
                printf("\t1. gps_init\n");
                printf("\t2. gps_deinit\n");
                printf("\t3. gps start\n");
                printf("\t4. gps stop\n");
                printf("Enter choice [1-4]: ");

                choice = get_integer_input();

                printf("\nChoice [%d] entered\n", choice);
        } while ((choice < 1 && (printf("Invalid choice [%d]\n", choice))) ||
                 ((choice > 4) && printf("Invalid choice [%d]\n", choice)));

        return choice;
}

int main(int argc, char **argv)
{
        int choice;
        int sock_fd;
        struct sockaddr_in dest_sock_addr;
        unsigned int   xact_id;                        /* Transaction ID for the Message  */
        unsigned int   func_id;                        /* ID of  API/func operation  */
        unsigned int   sndr_id;                        /* ID of sender user; ref user id  */
        unsigned short data_sz;                        /* Length (B) of data in  Message  */
        const struct hw_eval_msg_head init   = {0x00000000, 0xAAAAAAA0, 0xEEEEEEEE, 0x0000};
        const struct hw_eval_msg_head deinit = {0x00000001, 0xAAAAAAA1, 0xEEEEEEEE, 0x0000};
        const struct hw_eval_msg_head start  = {0x00000002, 0xAAAAAAA2, 0xEEEEEEEE, 0x0000};
        const struct hw_eval_msg_head stop   = {0x00000003, 0xAAAAAAA3, 0xEEEEEEEE, 0x0000};
        const struct hw_eval_msg_head *msg;
        unsigned char mem[MAX_HW_EVAL_MSG_LEN];
        unsigned char *buff;
        socklen_t sock_len;

        if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                printf("Error creating socket\n");
                return -1;
        }

        memset(&dest_sock_addr, 0, sizeof(dest_sock_addr));
        dest_sock_addr.sin_family = AF_INET;
        dest_sock_addr.sin_port = htons(6000);
        dest_sock_addr.sin_addr.s_addr = INADDR_ANY;
        sock_len = sizeof(dest_sock_addr);

        if (connect(sock_fd, (struct sockaddr *) &dest_sock_addr, 
                    sock_len) < 0) {
                printf("Error in connect\n");
                close(sock_fd);
                return -1;
        }

        while (1) {

                choice = menu();

                switch (choice) {
                        case 1:
                                msg = &init;
                                break;

                        case 2:
                                msg = &deinit;
                                break;

                        case 3:
                                msg = &start;
                                break;

                        case 4:
                                msg = &stop;
                                break;

                        default:
                                printf("Invalid choice [%d]\n", choice);
                                continue;
                }

                buff = mem;

                buff = BUF_LE_U4B_WR_INC(buff, msg->xact_id);
                buff = BUF_LE_U4B_WR_INC(buff, msg->func_id);
                buff = BUF_LE_U4B_WR_INC(buff, msg->sndr_id);
                buff = BUF_LE_U2B_WR_INC(buff, msg->data_sz);

                if(send(sock_fd, mem, (buff - mem), 0) < 0)
                        printf("Error in sending msg\n");
                else
                        printf("Successfully sent msg\n");
        }

        UNUSED(argc);
        UNUSED(argv);
        return 0;
}
