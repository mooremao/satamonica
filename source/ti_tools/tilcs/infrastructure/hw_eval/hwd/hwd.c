/*
 * client_app.c
 *
 * RPC Client test application
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/un.h>
#include <sys/stat.h>

#include "device.h"
#include "dev_proxy.h"
#include "rpc.h"
#include "utils.h"

unsigned int ai2_get_nbytes(unsigned char **buf, unsigned char num_bytes)
{
        unsigned int result = 0;
        unsigned char i;

        if (num_bytes > 4)
                num_bytes = 4;

        *buf += (num_bytes - 1);

        for(i = num_bytes; i; i--) {
                result <<= 8;
                result |= (unsigned int) *((*buf)--);
        }

        *buf += (num_bytes + 1);
        return result;
}

unsigned char *ai2_put_nbytes(unsigned int bytes, unsigned char *buf,
                              unsigned char num_bytes)
{
        if (num_bytes > 4)
                num_bytes = 4;

        for(; num_bytes; num_bytes--) {
                *buf++ = (unsigned char)bytes;
                bytes >>= 8;
        }

        return buf;
}

#include "ai2.h"

int os_net_sock_init(short port)
{
        int yes = 1;
        int sock_fd;
        socklen_t sock_len;
        struct sockaddr_in sock_addr;

        if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                return -1;
        }

        if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes, 
                       sizeof(int)) < 0) {
                return -1;
        }

        memset(&sock_addr, 0, sizeof(sock_addr));
        sock_addr.sin_family = AF_INET;
        sock_addr.sin_port = htons(port);
        sock_addr.sin_addr.s_addr = INADDR_ANY;
        sock_len = sizeof(sock_addr);

        if (bind(sock_fd, (struct sockaddr *)&sock_addr, sock_len) < 0) {
                close(sock_fd);
                return -1;
        }

        if (listen(sock_fd, 1) < 0)
                return -1;

        return sock_fd;
}

int os_net_sock_exit(int fd)
{
        if(close(fd) < 0)
                return -1;

        return 0;
}

int os_net_accept(int fd, struct sockaddr_in *sndr_sock_addr, 
                  socklen_t addr_len)
{
        return accept(fd, (struct sockaddr *)sndr_sock_addr, &addr_len);
}

int os_net_recv(int fd, char *data, int len, 
                struct sockaddr_in *src_sock_addr)
{
        int length=0;
        socklen_t addr_len = sizeof(struct sockaddr_in);

        if((length = recv(fd, data, len, 0)) < 0)
                return -1;

        return length;
}

int os_net_send(int fd, struct sockaddr_in *dst_sock_addr, char *data, 
                int len)
{
        int length = 0;

        if((length = send(fd, data, len, 0)) < 0)
                return -1;

        return length;
}

struct queue_node {

        void              *data;
        struct queue_node *next;
};

struct queue {

        struct queue_node *front;
        struct queue_node *rear;
};

static struct queue * queue_init(void)
{
        struct queue *q = malloc(sizeof(struct queue));
        if (!q)
                return NULL;

        q->front = q->rear = NULL;

        return q;
}

static void queue_deinit(struct queue *q)
{
        if (q)
                free(q);
}

static void queue_push(struct queue *q, void *data)
{
        struct queue_node *node = NULL;

        if (!q->rear || !q->front)
                node = q->front = q->rear = (struct queue_node *)
                        malloc(sizeof(struct queue_node));
        else
                node = q->rear->next = (struct queue_node *)
                        malloc(sizeof(struct queue_node));

        if (!node)
                return;

        node->data = data;
        node->next = NULL;
        q->rear = node;
}

static void queue_pop(struct queue *q)
{
        if (q && q->front) {
                q->front = q->front->next;

                if (!q->front)
                        q->rear = NULL;
        }

        return;
}

static void * queue_front(struct queue *q)
{
        if (q && q->front && q->front->data)
                return q->front->data;
        else
                return NULL;
}

static size_t queue_size(struct queue *q)
{
        int count = 0;
        struct queue_node *node = q->front;

        if (node)
                count++;

        while (node && node != q->rear) {
                node = node->next;
                count++;
        }

        return count;
}

struct hw_eval_msg_head {

        unsigned int   xact_id;                        /* Transaction ID for the Message  */
        unsigned int   func_id;                        /* ID of  API/func operation  */
        unsigned int   sndr_id;                        /* ID of sender user; ref user id  */
        unsigned short data_sz;                        /* Length (B) of data in  Message  */
};

#define MAX_HW_EVAL_MSG_LEN (sizeof(struct hw_eval_msg_head) + 65535)

struct hw_eval_msg {

        struct hw_eval_msg_head head;                  /* Message header  */
        struct sockaddr_in      addr;                  /* Sock addr       */
        char                    *buff;                 /* Data in message */
};

typedef struct queue * (*init_que)(void);
typedef void (*deinit_que)(struct queue * q);
typedef void (*push_que)(struct queue *q, void *data);
typedef void (*pop_que)(struct queue *q);
typedef void * (*front_que)(struct queue *q);
typedef size_t (*size_que)(struct queue *q);

struct queue_msg {

        struct queue    *rec_que;
        pthread_mutex_t lock;
        sem_t           sem;

        init_que        init;
        deinit_que      deinit;
        push_que        push;
        pop_que         pop;
        front_que       front;
        size_que        size;
};

static int push_to_que(struct queue_msg *que, struct hw_eval_msg *msg)
{
        pthread_mutex_lock(&que->lock);

        que->push(que->rec_que, msg);

        sem_post(&que->sem);

        pthread_mutex_unlock(&que->lock);

        return 0;
}

static struct hw_eval_msg * pop_from_que(struct queue_msg *que)
{
        struct hw_eval_msg *msg = NULL;

        pthread_mutex_lock(&que->lock);

        while (0 == que->size(que->rec_que)) {

                pthread_mutex_unlock(&que->lock);

                sem_wait(&que->sem);

                pthread_mutex_lock(&que->lock);
        }

        msg = que->front(que->rec_que);
        que->pop(que->rec_que);

        pthread_mutex_unlock(&que->lock);

        return msg;
}

static int queue_initialize(struct queue_msg **q)
{
        struct queue_msg *que = *q =
                (struct queue_msg *) malloc(sizeof(struct queue_msg));
        if (que == NULL)
                goto queue_initialize_err4;

        if (pthread_mutex_init(&que->lock, NULL) < 0)
                goto queue_initialize_err3;

        if (sem_init(&que->sem, 0, 0) < 0)
                goto queue_initialize_err2;

        que->init      = queue_init;
        que->deinit    = queue_deinit;
        que->push      = queue_push;
        que->pop       = queue_pop;
        que->front     = queue_front;
        que->size      = queue_size;

        que->rec_que = que->init();
        if (!que->rec_que)
                goto queue_initialize_err1;

        return 0;

queue_initialize_err1:
        sem_destroy(&que->sem);

queue_initialize_err2:
        pthread_mutex_destroy(&que->lock);

queue_initialize_err3:
        free(que);

queue_initialize_err4:
        printf("Error in queue_initialize\n");
        return -1;
}

static int queue_deinitialize(struct queue_msg *que)
{
        sem_destroy(&que->sem);

        pthread_mutex_destroy(&que->lock);

        que->deinit(que->rec_que);

        free(que);

        return 0;
}

static int location_rpt_cb(enum ue_pos_id id, void *data_array, int size);
static int measure_info_cb(enum ue_msr_id id, void *data_array, int size);
static int location_evt_cb(enum lc_evt_id id);
static int nmea_message_cb(enum nmea_sn_id id, char *data, int size);
static int device_agnss_cb(enum loc_assist assist, void *assist_array, int size);
static int device_error_cb(enum dev_err_id id);
static int device_event_cb(enum dev_evt_id id, void *data_array, int size);
static int ucase_notify_cb(enum app_rpt_id id, void *data);

/* ti_dev_proxy_connect parameters */
struct ti_dev_callbacks cbs[] = {
        {
                location_rpt_cb,    /* Device location info              */
                measure_info_cb,    /* SV signal Measurement info        */
                location_evt_cb,    /* Location Error                    */
                nmea_message_cb,    /* Report NMEA message               */
                device_agnss_cb,    /* Assist info decoded by dev        */
                device_error_cb,    /* Notify multiple errors            */
                device_event_cb,    /* Events from device                */
                ucase_notify_cb     /* Usecase indication from dev       */
        }
};

struct ti_dev_proxy_params params[] = {
        {
                1000	/* Timeout for communication w/ dev */
        }
};

struct dev_start_up_info start_info;

/* ti_dev_config_reports parameters */
unsigned int async_events;
unsigned int dev_agnss;
unsigned int nmea_flags;
unsigned int msr_reports;
unsigned int pos_reports;
unsigned short interval;

struct dev_clk_calibrate calib;

int dproxy_init(struct hw_eval_msg *msg)
{
        printf("Received dproxy_init\n");

        /*if (ti_dev_proxy_connect(cbs, params) < 0) {
          printf("ti_dev_proxy_connect failed\n");
          return -1;
          } else
          printf("ti_dev_proxy_connect successful\n");

          if (ti_dev_connect(&start_info) < 0) {
          printf("ti_dev_connect failed\n");
          return -1;
          } else
          printf("ti_dev_connect successful\n");

          ti_dev_exec_action(dev_dsleep);*/

        return 0;
}

int dproxy_deinit(struct hw_eval_msg *msg)
{
        printf("Received dproxy_deinit\n");

        /*ti_dev_release();

          ti_dev_proxy_release();*/

        return 0;
}

int dproxy_start(struct hw_eval_msg *msg)
{
        printf("Received dproxy_start\n");

        /*ti_dev_exec_action(dev_wakeup);

          async_events = ASYC_RX_STATE_EVT;

          dev_agnss = ASYC_EVT_NEW_EPH | ASYC_EVT_NEW_ALM | ASYC_EVT_NEW_HLT |
          ASYC_EVT_NEW_UTC | ASYC_EVT_NEW_ION;

          nmea_flags = SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) | 
          SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg) | 
          SET_BIT(glgga) | SET_BIT(glgll) | SET_BIT(glgsa) | 
          SET_BIT(glgsv) | SET_BIT(glrmc) | SET_BIT(glvtg);

          msr_reports = MSR_RPT_GPS_NATIVE | MSR_RPT_GLO_NATIVE  | 
          MSR_RPT_DEV_RESULT | MSR_RPT_GPS_IE_SEL;

          pos_reports = POS_RPT_NATIVE_ALL | POS_RPT_VEL_NATIVE | 
          POS_RPT_GPS_IE_SEL | POS_RPT_TIM_GPS_NATIVE | 
          POS_RPT_TIM_GPS_IE_SEL | POS_RPT_TIM_GLO_NATIVE | 
          POS_RPT_TIM_GLO_IE_SEL | POS_RPT_VEL_IE_SEL;

          interval     = 1;

          if (ti_dev_config_reports(async_events, dev_agnss, nmea_flags, 
          msr_reports, pos_reports, interval) < 0) {
          printf("ti_dev_config_reports failed\n");
          return -1;
          } else
          printf("Successfully set TI dev-proxy parameters:\n");

          ti_dev_exec_action(dev_active);

          calib.flags  = DEV_CALIB_AUTO;
          calib.ref_clk_freq = 26000000;
          calib.ref_clk_quality = 20;

          ti_dev_setup_oper_param(clk_calibrate,&calib);*/

        return 0;
}

int dproxy_stop(struct hw_eval_msg *msg)
{
        printf("Received dproxy_stop\n");

        /*async_events = 0;
          dev_agnss    = 0;
          nmea_flags   = 0;
          msr_reports  = 0;
          pos_reports  = 0;
          interval     = 0;

          if (ti_dev_config_reports(async_events, dev_agnss, nmea_flags, 
          msr_reports, pos_reports, interval) < 0) {
          printf("ti_dev_config_reports failed\n");
          return -1;
          } else
          printf("Successfully set TI dev-proxy parameters:\n");

          ti_dev_exec_action(dev_de_act);

          sleep(30);

          ti_dev_exec_action(dev_dsleep);*/

        return 0;
}

typedef int (*hw_eval_fn)(struct hw_eval_msg *msg);

struct hw_eval_fn_entry {

        unsigned int    func_id;
        hw_eval_fn      func;
};

/* : Need to fill up table with handlers depending on H/W eval tests */
static struct hw_eval_fn_entry hw_eval_fn_tbl[] = {

        {0xAAAAAAA0, dproxy_init},
        {0xAAAAAAA1, dproxy_deinit},
        {0xAAAAAAA2, dproxy_start},
        {0xAAAAAAA3, dproxy_stop},
        {0xFFFFFFFF, NULL}
};

static struct hw_eval_fn_entry *hw_eval_fn_find(unsigned int func_id)
{
        struct hw_eval_fn_entry *fn_entry = hw_eval_fn_tbl + 0;

        while(fn_entry->func_id != 0xFFFFFFFF) {
                if(fn_entry->func_id == func_id && fn_entry->func != NULL)
                        return fn_entry;

                fn_entry++;
        }

        return NULL;
}

static void process_message(struct hw_eval_msg *msg)
{
        struct hw_eval_msg_head *hdr = &msg->head;

        struct hw_eval_fn_entry *fn_entry = hw_eval_fn_find(hdr->func_id);
        if(!fn_entry || !fn_entry->func) {
                printf("Received unsupported msg in queue\n");
                goto process_message_exit1;
        }

        printf("Received supported msg in queue\n");
        fn_entry->func(msg);

process_message_exit1:
        free(msg);

        return;
}

static void * que_handling_thread(void* arg)
{
        struct queue_msg *que = (struct queue_msg *) arg;

        printf("Starting que_handling_thread!\n");

        while (1) {

                struct hw_eval_msg *msg = pop_from_que(que);

                printf("Received msg in queue\n");

                if (msg)
                        process_message(msg);
        }

        return NULL;
}

struct ipc_thrd_data {

        int              sock_fd;
        struct queue_msg *que;
};

static void * ipc_handling_thread(void *arg)
{
        struct ipc_thrd_data *ipc_data = (struct ipc_thrd_data *) arg;
        struct hw_eval_msg *msg = NULL;
        struct hw_eval_msg_head hdr;
        struct hw_eval_msg_head *msg_hdr = NULL;
        char tcp_buf[MAX_HW_EVAL_MSG_LEN];
        unsigned char *buff;
        char *msg_buf = NULL;
        struct sockaddr_in sndr_addr;
        int length;
        int ret_val;

        printf("Starting ipc_handling_thread!\n");

        ret_val = os_net_accept(ipc_data->sock_fd, &sndr_addr, 
                                sizeof(sndr_addr));
        if (ret_val < 0) {
                printf("Error in ipc_handling_thread!\n");
                return NULL;
        }

        ipc_data->sock_fd = ret_val;

        while (1) {

                if((length = os_net_recv(ipc_data->sock_fd, tcp_buf, 
                                         MAX_HW_EVAL_MSG_LEN, &sndr_addr)) < 0)
                        break;

                buff = tcp_buf;
                hdr.xact_id = BUF_LE_U4B_RD_INC(buff);
                hdr.func_id = BUF_LE_U4B_RD_INC(buff);
                hdr.sndr_id = BUF_LE_U4B_RD_INC(buff);
                hdr.data_sz = BUF_LE_U2B_RD_INC(buff);
                /*hdr.xact_id = *(unsigned int   *)(tcp_buf);
                  hdr.func_id = *(unsigned int   *)(tcp_buf + 4);
                  hdr.sndr_id = *(unsigned int   *)(tcp_buf + 8);
                  hdr.data_sz = *(unsigned short *)(tcp_buf + 12);*/

                printf("Received pkt of size %d bytes, xact_id 0x%x, func_id 0x%x, sndr_id 0x%x and data_sz %d\n", 
                       length, 
                       hdr.xact_id, 
                       hdr.func_id, 
                       hdr.sndr_id, 
                       hdr.data_sz);

                if (hdr.data_sz != (length - 14)) {
                        printf("Incorrect payload length\n");
                        continue;
                }

                msg_hdr = (struct hw_eval_msg_head *) tcp_buf;
                msg_hdr->xact_id = hdr.xact_id;
                msg_hdr->func_id = hdr.func_id;
                msg_hdr->sndr_id = hdr.sndr_id;
                msg_hdr->data_sz = hdr.data_sz;

                msg_buf = (char *) malloc(sizeof(struct hw_eval_msg) +
                                          msg_hdr->data_sz);
                if(!msg_buf) {
                        printf("malloc failed\n");
                        break;
                }

                msg = (struct hw_eval_msg *) msg_buf;
                memcpy(&msg->head, &hdr, sizeof(struct sockaddr));

                memcpy(&msg->addr, &sndr_addr, sizeof(struct sockaddr));

                msg->buff = msg_buf + sizeof(struct hw_eval_msg);
                memcpy(msg->buff, (tcp_buf + 14), msg_hdr->data_sz);

                push_to_que(ipc_data->que, msg);
        }

        return NULL;
}

static int location_rpt_cb(enum ue_pos_id id, void *data_array, int size)
{
        printf("location_rpt indication callback: enum [%d] and size [%d]\n", id, size);

        switch (id) {
                case loc_exl_native:
                        {
                                struct dev_pos_info *dev_pos = (struct dev_pos_info *) data_array;
                                struct dev_ellp_unc *dev_ell = dev_pos->ell =
                                        BUF_OFFSET_ST(dev_pos, struct dev_pos_info, 
                                                      struct dev_ellp_unc);
                                struct dev_utc_info *dev_utc = dev_pos->utc =
                                        BUF_OFFSET_ST(dev_ell, struct dev_ellp_unc, 
                                                      struct dev_utc_info);

                                printf("pos_flags         [%10d]\n", dev_pos->pos_flags);
                                printf("lat_rad           [%10d]\n", dev_pos->lat_rad);
                                printf("lon_rad           [%10d]\n", dev_pos->lon_rad);
                                printf("alt_wgs84         [%10d]\n", dev_pos->alt_wgs84);
                                printf("has_msl           [%10d]\n", dev_pos->has_msl);
                                printf("alt_msl           [%10d]\n", dev_pos->alt_msl);
                                printf("unc_east          [%10d]\n", dev_pos->unc_east);
                                printf("unc_north         [%10d]\n", dev_pos->unc_north);
                                printf("unc_vertical      [%10d]\n", dev_pos->unc_vertical);

                                printf("orientation       [%10d]\n", dev_ell->orientation);
                                printf("semi_major        [%10d]\n", dev_ell->semi_major);
                                printf("semi_minor        [%10d]\n", dev_ell->semi_minor);

                                printf("hours             [%10d]\n", dev_utc->hours);
                                printf("minutes           [%10d]\n", dev_utc->minutes);
                                printf("seconds           [%10d]\n", dev_utc->seconds);
                                printf("sec_1by10         [%10d]\n", dev_utc->sec_1by10);
                        }
                        break;

                case loc_sat_native:
                        {
                                struct dev_sat_info *dev_sat = (struct dev_sat_info *) data_array;

                                printf("No. of satellites     [%10d]\n", size);

                                while (size--) {
                                        printf("gnss_id           [%10d]\n", dev_sat->gnss_id);
                                        printf("svid              [%10d]\n", dev_sat->svid);
                                        printf(" ->iode           [%10d]\n", dev_sat->iode);
                                        printf(" ->msr_residual   [%10d]\n", dev_sat->msr_residual);
                                        printf(" ->weigh_in_fix   [%10d]\n", dev_sat->weigh_in_fix);

                                        dev_sat++;
                                }
                        }
                        break;

                case loc_dop_native:
                        {
                                struct dev_dop_info *dev_dop = (struct dev_dop_info *) data_array;

                                printf("position          [%10d]\n", dev_dop->position);
                                printf("horizontal        [%10d]\n", dev_dop->horizontal);
                                printf("vertical          [%10d]\n", dev_dop->vertical);
                                printf("time              [%10d]\n", dev_dop->time);
                        }
                        break;

                case loc_all_native:
                        {
                                struct dev_pos_info *dev_pos = (struct dev_pos_info *) data_array;
                                struct dev_ellp_unc *dev_ell = dev_pos->ell =
                                        BUF_OFFSET_ST(dev_pos, struct dev_pos_info, 
                                                      struct dev_ellp_unc);
                                struct dev_utc_info *dev_utc = dev_pos->utc =
                                        BUF_OFFSET_ST(dev_ell, struct dev_ellp_unc, 
                                                      struct dev_utc_info);
                                struct dev_dop_info *dev_dop =
                                        BUF_OFFSET_ST(dev_utc, struct dev_utc_info, 
                                                      struct dev_dop_info);
                                struct dev_vel_info *dev_vel =
                                        BUF_OFFSET_ST(dev_dop, struct dev_dop_info, 
                                                      struct dev_vel_info);
                                struct dev_hdg_info *dev_hdg =
                                        BUF_OFFSET_ST(dev_vel, struct dev_vel_info, 
                                                      struct dev_hdg_info);
                                struct dev_sat_info *dev_sat =
                                        BUF_OFFSET_ST(dev_hdg, struct dev_hdg_info, 
                                                      struct dev_sat_info);

                                printf("pos_flags         [%10d]\n", dev_pos->pos_flags);
                                printf("lat_rad           [%10d]\n", dev_pos->lat_rad);
                                printf("lon_rad           [%10d]\n", dev_pos->lon_rad);
                                printf("alt_wgs84         [%10d]\n", dev_pos->alt_wgs84);
                                printf("has_msl           [%10d]\n", dev_pos->has_msl);
                                printf("alt_msl           [%10d]\n", dev_pos->alt_msl);
                                printf("unc_east          [%10d]\n", dev_pos->unc_east);
                                printf("unc_north         [%10d]\n", dev_pos->unc_north);
                                printf("unc_vertical      [%10d]\n", dev_pos->unc_vertical);
                                printf("orientation       [%10d]\n", dev_ell->orientation);
                                printf("semi_major        [%10d]\n", dev_ell->semi_major);
                                printf("semi_minor        [%10d]\n", dev_ell->semi_minor);
                                printf("hours             [%10d]\n", dev_utc->hours);
                                printf("minutes           [%10d]\n", dev_utc->minutes);
                                printf("seconds           [%10d]\n", dev_utc->seconds);
                                printf("sec_1by10         [%10d]\n", dev_utc->sec_1by10);

                                printf("position          [%10d]\n", dev_dop->position);
                                printf("horizontal        [%10d]\n", dev_dop->horizontal);
                                printf("vertical          [%10d]\n", dev_dop->vertical);
                                printf("time              [%10d]\n", dev_dop->time);

                                printf("gnss_id           [%10d]\n", dev_sat->gnss_id);
                                printf("svid              [%10d]\n", dev_sat->svid);
                                printf("iode              [%10d]\n", dev_sat->iode);
                                printf("msr_residual      [%10d]\n", dev_sat->msr_residual);
                                printf("weigh_in_fix      [%10d]\n", dev_sat->weigh_in_fix);
                        }
                        break;

                case loc_gnss_ie:
                        {
                                struct loc_gnss_info  *dev_gnss = (struct loc_gnss_info *) data_array;
                                struct location_desc *loc = &dev_gnss->location;

                                printf("fix_type          [%10d]\n", dev_gnss->fix_type);
                                printf("gnss_tid          [%10d]\n", dev_gnss->gnss_tid);
                                //printf("loc_time          [%10d]\n", dev_gnss->loc_time);
                                printf("fix_type          [%10d]\n", dev_gnss->fix_type);
                                printf("pos_bits          [%10d]\n", dev_gnss->pos_bits);
                                printf("shape             [%10d]\n", loc->shape);
                                printf("lat_sign          [%10d]\n", loc->lat_sign);
                                printf("latitude          [%10d]\n", loc->latitude_N);
                                printf("longitude         [%10d]\n", loc->longitude_N);
                                printf("alt_dir           [%10d]\n", loc->alt_dir);
                                printf("altitude          [%10d]\n", loc->altitude_N);
                                printf("unc_semi_major    [%10d]\n", loc->unc_semi_maj_K);
                                printf("unc_semi_minor    [%10d]\n", loc->unc_semi_min_K);
                                printf("orientation       [%10d]\n", loc->orientation);
                                printf("unc_altitude      [%10d]\n", loc->unc_altitude_K);
                                printf("confidence        [%10d]\n", loc->confidence);
                        }
                        break;

                case vel_native:
                        {
                                struct dev_vel_info *dev_vel = (struct dev_vel_info *) data_array;
                                dev_vel->hdg_info = BUF_OFFSET_ST(dev_vel, struct dev_vel_info, 
                                                                  struct dev_hdg_info);

                                printf("east              [%10d]\n", dev_vel->east);
                                printf("north             [%10d]\n", dev_vel->north);
                                printf("vertical          [%10d]\n", dev_vel->vertical);
                                printf("uncetainty        [%10d]\n", dev_vel->uncertainty);
                                printf("truthful          [%10d]\n", dev_vel->hdg_info->truthful);
                                printf("magnetic          [%10d]\n", dev_vel->hdg_info->magnetic);

                        }
                        break;

                case vel_gnss_ie:
                        {
                                struct vel_gnss_info *dev_vel = (struct vel_gnss_info *) data_array;

                                printf("type              [%10d]\n", dev_vel->type);
                                printf("bearing           [%10d]\n", dev_vel->bearing);
                                printf("h_speed           [%10d]\n", dev_vel->h_speed);
                                printf("ver_dir           [%10d]\n", dev_vel->ver_dir);
                                printf("v_speed           [%10d]\n", dev_vel->v_speed);
                                printf("h_unc             [%10d]\n", dev_vel->h_unc);
                                printf("v_unc             [%10d]\n", dev_vel->v_unc);
                        }
                        break;

                case tim_gps_native:
                        {
                                struct dev_gps_time *dev_time = (struct dev_gps_time *) data_array;

                                printf("timer_count       [%10d]\n", dev_time->timer_count);
                                printf("week_num          [%10d]\n", dev_time->week_num);
                                printf("time_msec         [%10d]\n", dev_time->time_msec);
                                printf("time_bias         [%10d]\n", dev_time->time_bias);
                                printf("time_unc          [%10d]\n", dev_time->time_unc);
                                printf("freq_bias         [%10d]\n", dev_time->freq_bias);
                                printf("freq_unc          [%10d]\n", dev_time->freq_unc);
                                printf("is_utc_diff       [%10d]\n", dev_time->is_utc_diff);
                                printf("utc_diff          [%10d]\n", dev_time->utc_diff);
                        }
                        break;

                case tim_glo_native:
                        break;

                case tim_gps_ie:
                        {
                                struct gps_time *time = (struct gps_time *) data_array;

                                printf("week_nr           [%10d]\n", time->week_nr);
                                printf("tow_23b           [%10d]\n", time->tow);
                        }
                        break;

                case tim_glo_ie:
                        break;

                default:
                        break;
        }

        return 0;
}

static int measure_info_cb(enum ue_msr_id id, void *data_array, int size)
{
        printf("measure_info indication callback: enum [%d] and size [%d]\n", id, size);

        switch (id) {
                case msr_gps_native:
                        {
                                struct dev_msr_info *dev_msr = (struct dev_msr_info *) data_array;

                                printf("No. of satellites     [%10d]\n", size);

                                while (size--) {
                                        printf("gnss_id           [%10d]\n", dev_msr->gnss_id);
                                        printf("svid              [%10d]\n", dev_msr->svid);
                                        /*printf(" ->snr            [%10d]\n", dev_msr->snr);
                                          printf(" ->cno            [%10d]\n", dev_msr->cno);
                                          printf(" ->latency_ms     [%10d]\n", dev_msr->latency_ms);
                                          printf(" ->pre_int        [%10d]\n", dev_msr->pre_int);
                                          printf(" ->post_int       [%10d]\n", dev_msr->post_int);*/
                                        printf(" ->msec           [%10d]\n", dev_msr->msec);
                                        printf(" ->sub_msec       [%10d]\n", dev_msr->sub_msec);
                                        /*printf(" ->time_unc       [%10d]\n", dev_msr->time_unc);
                                          printf(" ->speed          [%10d]\n", dev_msr->speed);
                                          printf(" ->speed_unc      [%10d]\n", dev_msr->speed_unc);
                                          printf(" ->msr_flags      [%10d]\n", dev_msr->msr_flags);
                                          printf(" ->ch_states      [%10d]\n", dev_msr->ch_states);
                                          printf(" ->accum_cp       [%10d]\n", dev_msr->accum_cp);
                                          printf(" ->carrier_vel    [%10d]\n", dev_msr->carrier_vel);
                                          printf(" ->carrier_acc    [%10d]\n", dev_msr->carrier_acc);
                                          printf(" ->loss_lock_ind  [%10d]\n", dev_msr->loss_lock_ind);
                                          printf(" ->good_obv_cnt   [%10d]\n", dev_msr->good_obv_cnt);
                                          printf(" ->total_obv_cnt  [%10d]\n", dev_msr->total_obv_cnt);
                                          printf(" ->sv_slot        [%10d]\n", dev_msr->sv_slot);
                                          printf(" ->diff_sv        [%10d]\n", dev_msr->diff_sv);
                                          printf(" ->early_term     [%10d]\n", dev_msr->early_term);*/
                                        printf(" ->elevation      [%10d]\n", dev_msr->elevation);
                                        printf(" ->azimuth        [%10d]\n", dev_msr->azimuth);

                                        dev_msr++;
                                }
                        }
                        break;

                case msr_gps_rpt_ie:
                        {
                                struct gps_msr_info *dev_msr = (struct gps_msr_info *) data_array;

                                printf("No. of satellites [%10d]\n", size);

                                while (size--) {
                                        printf("svid              [%10d]\n", dev_msr->svid);
                                        printf(" ->c_no           [%10d]\n", dev_msr->c_no);
                                        printf(" ->doppler        [%10d]\n", dev_msr->doppler);
                                        dev_msr++;
                                }
                        }
                        break;

                case msr_glo_native:
                        {
                        }
                        break;

                case msr_glo_signal:
                        {
                                struct glo_sig_info *dev_msr =
                                        (struct glo_sig_info *) data_array;
                                struct ganss_sig_info *dev_sig;

                                printf("No. of satellites [%10d]\n", size);

                                while (size--) {
                                        dev_sig = (struct ganss_sig_info *)&dev_msr->signal;

                                        printf("options           [%10d]\n", dev_sig->ganss_id);
                                        printf(" ->signal_id      [%10d]\n", dev_sig->signal_id);
                                        printf(" ->num_sat        [%10d]\n", dev_sig->n_sat);
                                        printf(" ->ambiguity      [%10d]\n", dev_sig->ambiguity);

                                        dev_msr++;
                                }
                        }
                        break;

                case msr_glo_sig_G1:
                        {
                        }
                        break;

                case msr_glo_sig_G2:
                        {
                        }
                        break;

                default:
                        break;
        }

        return 0;
}

static int location_evt_cb(enum lc_evt_id id)
{
        printf("location_evt indication callback\n");

        UNUSED(id);
        return 0;
}

static int nmea_message_cb(enum nmea_sn_id id, char *data, int size)
{
        char *nmea = calloc(size+1, sizeof(char));

        if(nmea==NULL){
                printf("error: nmea var could not be allocated\n");
                return 0;
        }
        printf("nmea_message indication callback\n");

        memcpy(nmea, data, size);
        nmea[size]= '\0';

        printf("NMEA [%s]\n", nmea);

        free(nmea);

        UNUSED(id);
        return 0;
}

static int device_agnss_cb(enum loc_assist assist, void *assist_array, int size)
{
        printf("device_agnss indication callback\n");

        UNUSED(assist);
        UNUSED(assist_array);
        UNUSED(size);
        return 0;
}

static int device_error_cb(enum dev_err_id id)
{
        printf("device_error indication callback\n");

        UNUSED(id);
        return 0;
}

static int device_event_cb(enum dev_evt_id id, void *data_array, int size)
{
        printf("device_event indication callback: enum [%d] and size [%d]\n", id, 
               size);

        switch (id) {
                case evt_dev_ver:
                        {
                                struct dev_version *ver = (struct dev_version *)data_array;

                                printf("chip_id_major       : [%d]\n", ver->chip_id_major);
                                printf("chip_id_minor       : [%d]\n", ver->chip_id_minor);
                                printf("rom_id_major        : [%d]\n", ver->rom_id_major);
                                printf("rom_id_minor        : [%d]\n", ver->rom_id_minor);
                                printf("rom_id_sub_minor1   : [%d]\n", ver->rom_id_sub_minor1);
                                printf("rom_id_sub_minor2   : [%d]\n", ver->rom_id_sub_minor2);
                                printf("rom_day             : [%d]\n", ver->rom_day);
                                printf("rom_month           : [%d]\n", ver->rom_month);
                                printf("rom_year            : [%d]\n", ver->rom_year);
                                printf("patch_major         : [%d]\n", ver->patch_major);
                                printf("patch_minor         : [%d]\n", ver->patch_minor);
                                printf("patch_day           : [%d]\n", ver->patch_day);
                                printf("patch_month         : [%d]\n", ver->patch_month);
                                printf("patch_year          : [%d]\n", ver->patch_year);
                        }
                        break;

                default:
                        break;
        }

        printf("device_event indication callback: enum [%d] and size [%d] done\n", id, 
               size);
        return 0;
}

static int ucase_notify_cb(enum app_rpt_id id, void *data)
{
        printf("ucase_notify indication callback\n");

        UNUSED(id);
        UNUSED(data);
        return 0;
}

/* ti_dev_setup_oper_param parameters */
int param_id;

struct dev_ts_pulse_info pulse;
struct dev_ref_clk_param ref_ck;
struct dev_clk_calibrate clk_calib;
struct geofencing_params geofence;
struct buffered_location buff_loc;
struct dev_qop_specifier qop;

void * param_value_arr[] = {
        (void *)&pulse, 
        (void *)&ref_ck, 
        (void *)&clk_calib, 
        (void *)&geofence, 
        (void *)&buff_loc, 
        (void *)&qop
};

/* ti_dev_set_app_profile parameters */
unsigned int use_cases;
unsigned int gnss_select;

/* ti_dev_exec_action parameters */
enum dev_action act_exec;

/* ti_dev_version parameters */
struct dev_version version;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_eph eph;
struct dev_gps_eph *dev_gps_eph_info = &eph;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_alm gps_almanac;
struct dev_gps_alm *dev_gps_alm_info = &gps_almanac;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_ion gps_ion;
struct dev_gps_ion *dev_gps_ion_info = &gps_ion;

/* ti_dev_loc_assist_get parameters */
struct dev_gps_utc utc;
struct dev_gps_utc *dev_gps_utc_info = &utc;

static int get_integer_input(void)
{
        char arr[100];

        fgets(arr, sizeof(arr), stdin);
        return atoi(arr);
}

static int get_choice(const char *str, int lower_limit, int upper_limit)
{
        int choice;

        do {
                printf("%s", str);

                choice = get_integer_input();

                printf("\nChoice [%d] entered\n", choice);
        } while ((choice < lower_limit && (printf("Invalid choice [%d]\n", choice))) ||
                 ((choice > upper_limit && printf("Invalid choice [%d]\n", choice))));

        return choice;
}

static int menu(void)
{
        int choice;

        do {
                printf("	MENU\n");
                printf("************\n");
                printf("\t1. ti_dev_proxy_connect\n");
                printf("\t2. ti_dev_proxy_release\n");
                printf("\t3. ti_dev_connect\n");
                printf("\t4. ti_dev_release\n");
                printf("\t5. ti_dev_version\n");
                printf("\t6. ti_dev_config_reports\n");
                printf("\t7. ti_dev_set_app_profile\n");
                printf("\t8. ti_dev_setup_oper_param\n");
                printf("\t9. ti_dev_exec_seq\n");
                printf("\t10. ti_dev_is_alive\n");
                printf("\t11. ti_dev_proxy_version\n");
                printf("\t12. ti_dev_loc_assist_get gps ephemeris\n");
                printf("\t13. ti_dev_loc_assist_get gps almanac\n");
                printf("\t14. ti_dev_loc_assist_get gps ionospheric info\n");
                printf("\t15. ti_dev_loc_assist_get gps utc\n");
                printf("Enter choice [1-15]: ");

                choice = get_integer_input();

                printf("\nChoice [%d] entered\n", choice);
        } while ((choice < 1 && (printf("Invalid choice [%d]\n", choice))) ||
                 ((choice > 15) && printf("Invalid choice [%d]\n", choice)));

        switch (choice) {
                case 6:
                        //async_events = get_choice("Enter async_events value: ", 1, 100);
                        //dev_agnss = get_choice("Enter dev_agnss value: ", 1, 100);
                        //nmea_flags = get_choice("Enter nmea_flags value: ", 1, 100);
                        //msr_reports = get_choice("Enter msr_reports value: ", 1, 100);
                        //pos_reports = get_choice("Enter pos_reports value: ", 1, 100);
                        interval = get_choice("Enter interval value: ", 1, 100);
                        break;

                case 7:
                        use_cases = get_choice("Enter use_cases value: ", 1, 100);
                        gnss_select = get_choice("Enter gnss_select value: ", 1, 100);
                        break;

                case 8:
                        param_id = get_choice("Setup parameter:\n1. dev_ts_pulse_info\n2. dev_ref_clk_param\n3. dev_clk_calibrate\n4. geofencing_params\n5. buffered_location\n6. dev_qop_specifier\nEnter choice [1-6]: ", 1, 6);
                        break;

                case 9:
                        act_exec = get_choice("Enter act to execute:\n1. act_active\n2. act_de_act\n3. act_dsleep\n4. act_wakeup\nEnter choice [1-4]: ", 1, 4);
                        break;

                case 12:
                        memset(dev_gps_eph_info, 0, sizeof(struct dev_gps_eph));
                        dev_gps_eph_info->prn = (unsigned char)get_choice("Enter gps sv id:\nEnter choice [1-32]: ", 1, 32);
                        break;

                case 13:
                        memset(dev_gps_alm_info, 0, sizeof(struct dev_gps_alm));
                        dev_gps_alm_info->prn = (unsigned char)get_choice("Enter gps sv id:\nEnter choice [1-32]: ", 1, 32);
                        break;

                case 14:
                        memset(dev_gps_ion_info, 0, sizeof(struct dev_gps_ion));
                        break;

                case 15:
                        memset(dev_gps_utc_info, 0, sizeof(struct dev_gps_utc));
                        break;

                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 10:
                case 11:
                default:
                        break;
        }

        printf("\n");

        return choice;
}

int start_tcp_sock_server(void)
{
        static struct ipc_thrd_data ipc_data;
        pthread_t ipc_thrd_id;
        pthread_t que_thrd_id;

        ipc_data.sock_fd = os_net_sock_init(6000);
        if(-1 == ipc_data.sock_fd)
                goto start_tcp_sock_server_err3;

        if (queue_initialize(&ipc_data.que) < 0)
                goto start_tcp_sock_server_err2;

        if(pthread_create(&ipc_thrd_id, NULL, ipc_handling_thread, 
                          &ipc_data) != 0)
                goto start_tcp_sock_server_err1;

        if(pthread_create(&que_thrd_id, NULL, que_handling_thread, 
                          ipc_data.que) != 0)
                goto start_tcp_sock_server_err1;

        return 0;

start_tcp_sock_server_err1:
        queue_deinitialize(ipc_data.que);

start_tcp_sock_server_err2:
        os_net_sock_exit(ipc_data.sock_fd);

start_tcp_sock_server_err3:
        printf("Error in start_tcp_sock_server\n");
        return -1;
}

int main(int argc, char **argv)
{
#define CONFIG_FILE_PATH      "/system/etc/gnss/config/dproxy.conf"
#define SEVICE_PACK_FILE_PATH "/system/etc/gnss/patch/dproxy.patch"

        int choice;
        int ret_val;
        struct dev_proxy_version proxy_version;

        start_info.nvs_aid_cfg.aid_config = 0;
        start_info.nvs_aid_cfg.epoch_secs = 1;
        strncpy(start_info.config_file, CONFIG_FILE_PATH, 
                sizeof(start_info.config_file)-1);
        strncpy(start_info.sevice_pack, SEVICE_PACK_FILE_PATH, 
                sizeof(start_info.sevice_pack)-1);

        if (start_tcp_sock_server() < 0) {
                printf("Error, exiting!\n");
                return -1;
        }

        sleep(6);

        while (1) {
                choice = menu();

                switch (choice) {
                        case 1:
                                if ((ret_val = ti_dev_proxy_connect(cbs, params)) < 0)
                                        printf("ti_dev_proxy_connect failed\n");
                                else
                                        printf("ti_dev_proxy_connect successful\n");
                                break;

                        case 2:
                                ti_dev_proxy_release();
                                break;

                        case 3:
                                {
                                        if ((ret_val = ti_dev_connect(&start_info)) < 0)
                                                printf("ti_dev_connect failed\n");
                                        else
                                                printf("ti_dev_connect successful\n");
                                }
                                break;

                        case 4:
                                ti_dev_release();
                                break;

                        case 5:
                                if ((ret_val = ti_dev_version(&version)) < 0)
                                        printf("Failed to obtain TI dev version\n");
                                else {
                                        printf("TI dev version:\n");
                                        printf("chip_id_major       : [%d]\n", version.chip_id_major);
                                        printf("chip_id_minor       : [%d]\n", version.chip_id_minor);
                                        printf("rom_id_major        : [%d]\n", version.rom_id_major);
                                        printf("rom_id_minor        : [%d]\n", version.rom_id_minor);
                                        printf("rom_id_sub_minor1   : [%d]\n", version.rom_id_sub_minor1);
                                        printf("rom_id_sub_minor2   : [%d]\n", version.rom_id_sub_minor2);
                                        printf("rom_day             : [%d]\n", version.rom_day);
                                        printf("rom_month           : [%d]\n", version.rom_month);
                                        printf("rom_year            : [%d]\n", version.rom_year);
                                        printf("patch_major         : [%d]\n", version.patch_major);
                                        printf("patch_minor         : [%d]\n", version.patch_minor);
                                        printf("patch_day           : [%d]\n", version.patch_day);
                                        printf("patch_month         : [%d]\n", version.patch_month);
                                        printf("patch_year          : [%d]\n", version.patch_year);
                                }
                                break;

                        case 6:
                                async_events = 0;

                                dev_agnss = 0;

                                /* SET_BIT(gpgga) | SET_BIT(gpgll) | SET_BIT(gpgsa) |
                                   SET_BIT(gpgsv) | SET_BIT(gprmc) | SET_BIT(gpvtg)
                                 */
                                nmea_flags = 0;

                                /* MSR_RPT_GPS_NATIVE | MSR_RPT_GPS_IE_SEL | MSR_RPT_GLO_NATIVE |
                                   MSR_RPT_GLO_IE_SEL
                                 */
                                msr_reports = MSR_RPT_GPS_NATIVE;

                                /* POS_RPT_LOC_NATIVE | POS_RPT_GPS_IE_SEL | POS_RPT_GLO_IE_SEL |
                                   POS_RPT_VEL_NATIVE | POS_RPT_VEL_IE_SEL |
                                   POS_RPT_TIM_GPS_NATIVE | POS_RPT_TIM_GLO_NATIVE |
                                   POS_RPT_TIM_GPS_IE_SEL | POS_RPT_TIM_GLO_IE_SEL
                                 */
                                //pos_reports = POS_RPT_NATIVE_ALL | POS_RPT_TIM_GPS_NATIVE | POS_RPT_VEL_NATIVE;
                                pos_reports = POS_RPT_GPS_IE_SEL | POS_RPT_SAT_NATIVE |
                                        POS_RPT_VEL_NATIVE | POS_RPT_DOP_NATIVE | POS_RPT_TIM_GPS_IE_SEL;

                                interval = 1;

                                if ((ret_val = ti_dev_config_reports(async_events, 
                                                                     dev_agnss, nmea_flags, msr_reports, 
                                                                     pos_reports, interval)) < 0)
                                        printf("ti_dev_config_reports failed\n");
                                else {
                                        printf("Successfully set TI dev-proxy parameters:\n");
                                        printf("async_events	: [0x%x]\n", async_events);
                                        printf("dev_agnss		: [0x%x]\n", dev_agnss);
                                        printf("nmea_flags		: [%d]\n", nmea_flags);
                                        printf("msr_reports		: [0x%x]\n", msr_reports);
                                        printf("pos_reports		: [0x%x]\n", pos_reports);
                                        printf("interval		: [%d]\n", interval);
                                }
                                break;

                        case 7:
                                if ((ret_val = ti_dev_set_app_profile(use_cases, gnss_select)) < 0)
                                        printf("ti_dev_set_app_profile failed\n");
                                else
                                        printf("ti_dev_set_app_profile was successful\n");
                                break;

                        case 8:
                                if ((ret_val = ti_dev_setup_oper_param(param_id, 
                                                                       param_value_arr[param_id])) < 0)
                                        printf("ti_dev_setup_oper_param failed");
                                else
                                        printf("Successfully set operation parameter [%d]\n", 
                                               param_id);
                                break;

                        case 9:
                                if ((ret_val = ti_dev_exec_action(--act_exec)) < 0)
                                        printf("ti_dev_exec_seq failed");
                                else
                                        printf("Successfully executed sequence\n");
                                break;

                        case 10:
                                if ((ret_val = ti_dev_is_alive()) < 0)
                                        printf("ti_dev_is_alive failed");
                                else
                                        printf("Received device status [%d]\n", ret_val);
                                break;

                        case 11:
                                if ((ret_val = ti_dev_proxy_version(&proxy_version)) < 0)
                                        printf("ti_dev_proxy_version failed");
                                else {
                                        printf("TI devproxy version:\n");
                                        printf("major               : [%d]\n", proxy_version.major);
                                        printf("minor               : [%d]\n", proxy_version.minor);
                                        printf("patch               : [%d]\n", proxy_version.patch);
                                        printf("build               : [%d]\n", proxy_version.build);
                                        printf("exten               : [%c]\n", proxy_version.exten);
                                        printf("qfier               : [%s]\n", proxy_version.qfier);
                                        printf("day                 : [%d]\n", proxy_version.day);
                                        printf("month               : [%d]\n", proxy_version.month);
                                        printf("year                : [%d]\n", proxy_version.year);
                                }
                                break;

                        case 12:
                                if ((ret_val = ti_dev_loc_aiding_get(a_GPS_EPH, dev_gps_eph_info, 1)) < 0)
                                        printf("ti_dev_loc_aiding_get gps eph failed");
                                else {
                                        printf("TI GPS EPH details:\n");
                                        printf("prn                 : [%10d]\n", dev_gps_eph_info->prn);
                                        printf("Code_on_L2          : [%10d]\n", dev_gps_eph_info->Code_on_L2);
                                        printf("SV_Accuracy         : [%10d]\n", dev_gps_eph_info->Accuracy);
                                        printf("SV_Health           : [%10d]\n", dev_gps_eph_info->Health);
                                        printf("Tgd                 : [%10d]\n", dev_gps_eph_info->Tgd);
                                        printf("IODC                : [%10d]\n", dev_gps_eph_info->IODC);
                                        printf("Toc                 : [%10d]\n", dev_gps_eph_info->Toc);
                                        printf("Af2                 : [%10d]\n", dev_gps_eph_info->Af2);
                                        printf("Af1                 : [%10d]\n", dev_gps_eph_info->Af1);
                                        printf("Af0                 : [%10d]\n", dev_gps_eph_info->Af0);
                                        printf("IODE                : [%10d]\n", dev_gps_eph_info->IODE);
                                        printf("Crs                 : [%10d]\n", dev_gps_eph_info->Crs);
                                        printf("DeltaN              : [%10d]\n", dev_gps_eph_info->DeltaN);
                                        printf("Mo                  : [%10d]\n", dev_gps_eph_info->Mo);
                                        printf("Cuc                 : [%10d]\n", dev_gps_eph_info->Cuc);
                                        printf("E                   : [%10d]\n", dev_gps_eph_info->E);
                                        printf("Cus                 : [%10d]\n", dev_gps_eph_info->Cus);
                                        printf("SqrtA               : [%10d]\n", dev_gps_eph_info->SqrtA);
                                        printf("Toe                 : [%10d]\n", dev_gps_eph_info->Toe);
                                        printf("Cic                 : [%10d]\n", dev_gps_eph_info->Cic);
                                        printf("Omega0              : [%10d]\n", dev_gps_eph_info->Omega0);
                                        printf("Cis                 : [%10d]\n", dev_gps_eph_info->Cis);
                                        printf("Io                  : [%10d]\n", dev_gps_eph_info->Io);
                                        printf("Crc                 : [%10d]\n", dev_gps_eph_info->Crc);
                                        printf("Omega               : [%10d]\n", dev_gps_eph_info->Omega);
                                        printf("OmegaDot            : [%10d]\n", dev_gps_eph_info->OmegaDot);
                                        printf("Idot                : [%10d]\n", dev_gps_eph_info->Idot);
                                        printf("Week                : [%10d]\n", dev_gps_eph_info->Week);
                                        printf("UpdateWeek          : [%10d]\n", dev_gps_eph_info->UpdateWeek);
                                        printf("UpdateMS            : [%10d]\n", dev_gps_eph_info->UpdateMS);
                                }
                                break;

                        case 13:
                                if ((ret_val = ti_dev_loc_aiding_get(a_GPS_ALM, dev_gps_alm_info, 1)) < 0)
                                        printf("ti_dev_loc_aiding_get gps alm failed");
                                else {
                                        printf("TI GPS ALM details:\n");
                                        printf("prn                 : [%10d]\n", dev_gps_alm_info->prn);
                                        printf("Health              : [%10d]\n", dev_gps_alm_info->Health);
                                        printf("Toa                 : [%10d]\n", dev_gps_alm_info->Toa);
                                        printf("E                   : [%10d]\n", dev_gps_alm_info->E);
                                        printf("DeltaI              : [%10d]\n", dev_gps_alm_info->DeltaI);
                                        printf("OmegaDot            : [%10d]\n", dev_gps_alm_info->OmegaDot);
                                        printf("SqrtA               : [%10d]\n", dev_gps_alm_info->SqrtA);
                                        printf("OmegaZero           : [%10d]\n", dev_gps_alm_info->OmegaZero);
                                        printf("Omega               : [%10d]\n", dev_gps_alm_info->Omega);
                                        printf("MZero               : [%10d]\n", dev_gps_alm_info->MZero);
                                        printf("Af0                 : [%10d]\n", dev_gps_alm_info->Af0);
                                        printf("Af1                 : [%10d]\n", dev_gps_alm_info->Af1);
                                        printf("Week                : [%10d]\n", dev_gps_alm_info->Week);
                                        printf("UpdateWeek          : [%10d]\n", dev_gps_alm_info->UpdateWeek);
                                        printf("UpdateMS            : [%10d]\n", dev_gps_alm_info->UpdateMS);
                                }
                                break;

                        case 14:
                                if ((ret_val = ti_dev_loc_aiding_get(a_GPS_ION, dev_gps_ion_info, 1)) < 0)
                                        printf("ti_dev_loc_aiding_get gps ion failed");
                                else {
                                        printf("TI GPS ION details:\n");
                                        printf("Alpha0              : [%10d]\n", dev_gps_ion_info->Alpha0);
                                        printf("Alpha1              : [%10d]\n", dev_gps_ion_info->Alpha1);
                                        printf("Alpha2              : [%10d]\n", dev_gps_ion_info->Alpha2);
                                        printf("Alpha3              : [%10d]\n", dev_gps_ion_info->Alpha3);
                                        printf("Beta0               : [%10d]\n", dev_gps_ion_info->Beta0);
                                        printf("Beta1               : [%10d]\n", dev_gps_ion_info->Beta1);
                                        printf("Beta2               : [%10d]\n", dev_gps_ion_info->Beta2);
                                        printf("Beta3               : [%10d]\n", dev_gps_ion_info->Beta3);
                                }
                                break;

                        case 15:
                                if ((ret_val = ti_dev_loc_aiding_get(a_GPS_UTC, dev_gps_utc_info, 1)) < 0)
                                        printf("ti_dev_loc_aiding_get gps utc failed");
                                else {
                                        printf("TI GPS UTC details:\n");
                                        printf("A0                  : [%10d]\n", dev_gps_utc_info->A0);
                                        printf("A1                  : [%10d]\n", dev_gps_utc_info->A1);
                                        printf("DeltaTls            : [%10d]\n", dev_gps_utc_info->DeltaTls);
                                        printf("Tot                 : [%10d]\n", dev_gps_utc_info->Tot);
                                        printf("WNt                 : [%10d]\n", dev_gps_utc_info->WNt);
                                        printf("WNlsf               : [%10d]\n", dev_gps_utc_info->WNlsf);
                                        printf("DN                  : [%10d]\n", dev_gps_utc_info->DN);
                                        printf("DeltaTlsf           : [%10d]\n", dev_gps_utc_info->DeltaTlsf);
                                }
                                break;
                        default:
                                printf("Invalid choice [%d]\n", choice);
                                break;
                }
        }

        UNUSED(argc);
        UNUSED(argv);
        return 0;
}
