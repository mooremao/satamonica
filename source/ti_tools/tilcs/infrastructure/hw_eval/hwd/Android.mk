ifeq ($(TARGET_ARCH),arm)

LOCAL_PATH             := $(call my-dir)

# Build client_hwd
include $(CLEAR_VARS)
LOCAL_MODULE           := client_hwd
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2
endif
LOCAL_SHARED_LIBRARIES := libcutils liblog libdevproxy libgnssutils
LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                  \
    client_hwd.c
LOCAL_C_INCLUDES       :=                  \
    $(LOCAL_PATH)/../../../dproxy/include  \
    $(LOCAL_PATH)/../../../include/common  \
    $(LOCAL_PATH)/../../../include//dproxy \
    $(LOCAL_PATH)/../../../dproxy/server
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_EXECUTABLE)


# Build hwd
include $(CLEAR_VARS)
LOCAL_MODULE           := hwd
BTIPS_DEBUG            ?= 0
BTIPS_TARGET_PLATFORM  ?= zoom2
ifeq ($(BTIPS_DEBUG),1)
LOCAL_CFLAGS           += -g -O0
LOCAL_LDFLAGS          += -g
else
LOCAL_CFLAGS           += -O2
endif
LOCAL_SHARED_LIBRARIES := libcutils liblog libdevproxy libgnssutils
LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        :=                  \
    hwd.c
LOCAL_C_INCLUDES       :=                  \
    $(LOCAL_PATH)/../../../dproxy/include  \
    $(LOCAL_PATH)/../../../include/common  \
    $(LOCAL_PATH)/../../../include//dproxy \
    $(LOCAL_PATH)/../../../dproxy/server
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_EXECUTABLE)

endif
