
#include <stdio.h>   /* Standard input/output definitions */
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/time.h>
#include <signal.h>


#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
#define MAX_BUFFER_LEN 1000

unsigned char commandarray[]={0x10,0x00,0xf5,0x01,0x00,0x01,0x07,0x01,0x10,0x03};//len=10
unsigned char buffer[MAX_BUFFER_LEN];
int terminate = 0;

int main()
{
        int sockFd, newSockFd, readCount, clilen, i;
        struct sockaddr_in serverAddr, clientAddr;

        sockFd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockFd < 0)
	{
                printf("Error opening socket:");
		perror(NULL);
		return -1;
	}
        bzero((char *) &serverAddr, sizeof(serverAddr));
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_addr.s_addr = INADDR_ANY;
        serverAddr.sin_port = htons(4145);
        if (bind(sockFd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
	{
                printf("Error on binding: ");
		perror(NULL);
		return -1;
	}
	/* Start listening on the socket */
        listen(sockFd,5);
        clilen = sizeof(clientAddr);

        while(!terminate)
        {
		printf("Waiting for client to connect... \n");
                newSockFd = accept(sockFd, (struct sockaddr *) &clientAddr, (socklen_t*) &clilen);
                if (newSockFd < 0)
		{
                        printf("ERROR on accept: ");
			perror(NULL);
			terminate = 1;
			continue;
		}
                printf("Serial Socket client connected to server.\n");
	
		sleep(5);

		printf("Sending command to chip....\n");			
		if((readCount = send(newSockFd, (const void*)commandarray, sizeof(commandarray), 0)) != sizeof(commandarray))
        	{
                	printf("Mismatch in number of bytes send to socket: ");
			perror(NULL);
			break;
        	}
		else if(readCount == 0)
		{
			printf("Client closed connection.\n");
			break;
		}
		printf("Send %d bytes to socket\n", readCount);		
		printf("Waiting for response...\n");		
		
		readCount = recv(newSockFd, (void*)buffer, (MAX_BUFFER_LEN -1), 0);
		if(readCount < 0)
		{
			printf("Reading from socket failed: ");
			perror(NULL);
			break;
		}
		else if(readCount == 0)
		{
			printf("Client closed connection.\n");
			break;
		}
		printf("Received %d bytes from socket\n", readCount);			
		for(i=0; i<readCount; i++)
		{
			printf("0x%x ", buffer[i]);
		}
		printf("\n");
                
        }

	/* Close the socket */
        close(sockFd);

        printf("Thread exiting\n");

        return 0;
}


