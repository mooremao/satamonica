/**************************************************************************
 * File Name: serial2socket.c
 * Description: This program bridges a serial port and socket.
 * Author: Rejo G. Parekkattil
 * mail: rejo@ti.com
 * Date 10/12/2010
 *
 * ************************************************************************/

#include <stdio.h>   /* Standard input/output definitions */
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/time.h>
#include <signal.h>

#include "log.h"

/********** DEFINES ***********************************************************/
#define BAUD_RATE B115200
#define SER_SERVER_PORT 4145
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
#define MAX(x,y) (x) > (y) ? (x) : (y)
#define MAX_PORT_STRING_LEN 100
#define MAX_PORT_NUMBER_LEN 10
#define MAX_IP_ADDRESS_LEN 100
#define MAX_READ_SIZE 2000

/********** GLOBALS ***********************************************************/
int uartFD = 0;
int gDebugLevel = 0;
int mode = 0;
int terminate = 0;
char portName[MAX_PORT_STRING_LEN];
char portNumber[MAX_PORT_NUMBER_LEN];
char ipAddress[MAX_IP_ADDRESS_LEN];
char buffer[MAX_READ_SIZE];

/********** PROTOTYPES ********************************************************/
int getbaud(int fd);

/******************************************************************************/
/* Function Name        : Die                                                 */
/* Function Description : Exit function on error                              */
/* Input Parameters     : cahr * msg: Error message                           */
/* Output Parameters    : None                                                */
/* Return Value         : void                                                */
/******************************************************************************/
void Die(char *msg)
{
    	perror(msg);
    	exit(1);
}

int getbaud(int fd) 
{
	struct termios termAttr;
	int inputSpeed = -1;
	speed_t baudRate;

	tcgetattr(fd, &termAttr);
	/* Get the input speed */
	baudRate = cfgetispeed(&termAttr);
	switch (baudRate) {
		case B0:      inputSpeed = 0; break;
		case B50:     inputSpeed = 50; break;
		case B110:    inputSpeed = 110; break;
		case B134:    inputSpeed = 134; break;
		case B150:    inputSpeed = 150; break;
		case B200:    inputSpeed = 200; break;
		case B300:    inputSpeed = 300; break;
		case B600:    inputSpeed = 600; break;
		case B1200:   inputSpeed = 1200; break;
		case B1800:   inputSpeed = 1800; break;
		case B2400:   inputSpeed = 2400; break;
		case B4800:   inputSpeed = 4800; break;
		case B9600:   inputSpeed = 9600; break;
		case B19200:  inputSpeed = 19200; break;
		case B38400:  inputSpeed = 38400; break;
		case B115200:  inputSpeed = 115200; break;
	}
	return inputSpeed;
}

int InitSerialPort(int fd) {
	struct termios options;
	/* Get the current options for the port */
	tcgetattr(fd, &options);
	/* Set the baud rates to specified */
	cfsetispeed(&options, BAUD_RATE);
	cfsetospeed(&options, BAUD_RATE);
	/* Enable the receiver and set local mode */
	options.c_cflag |= (CLOCAL | CREAD);

	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;

	/* Set the new options for the port */
	tcsetattr(fd, TCSANOW, &options);
	return 1;
}

void SerialSocketServer()
{
        int sockFd, newSockFd, readCount, clilen, maxfd;
        struct sockaddr_in serverAddr, clientAddr;
	fd_set  readRdy;

        sockFd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockFd < 0)
	{
                ERROR("Error opening socket:");
		perror(NULL);
		return;
	}
        bzero((char *) &serverAddr, sizeof(serverAddr));
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_addr.s_addr = INADDR_ANY;
        serverAddr.sin_port = htons(atoi(portNumber));
        if (bind(sockFd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
	{
                ERROR("Error on binding: ");
		perror(NULL);
		return;
	}
	/* Start listening on the socket */
        listen(sockFd,5);
        clilen = sizeof(clientAddr);

        while(!terminate)
        {
		INFO("Waiting for client to connect... \n");
                newSockFd = accept(sockFd, (struct sockaddr *) &clientAddr, (socklen_t*) &clilen);
                if (newSockFd < 0)
		{
                        ERROR("ERROR on accept: ");
			perror(NULL);
			terminate = 1;
			continue;
		}
                DEBUG("Serial Socket client connected to server.\n");

		maxfd = MAX(uartFD, newSockFd)+1;	

                while(!terminate)
                {
			/* Set the fd_set */
			FD_ZERO(&readRdy); 
			FD_SET((unsigned int)uartFD, &readRdy);
			FD_SET((unsigned int)newSockFd, &readRdy);

			INFO("Waiting for bytes to arrive from serial port / socket... \n");
			if((select(maxfd, &readRdy, NULL, NULL, NULL)) >= 0)
			{
				/* Check if serial port is ready for reading */
				if(FD_ISSET(uartFD, &readRdy))
				{
					/* Read from the serial port and write to the socket */
					readCount = read(uartFD, (void*)buffer, MAX_READ_SIZE);
					if(readCount < 0)
					{
						ERROR("Reading from serial device failed: ");
						perror(NULL);
						terminate = 1;
						continue;
					}
					INFO("Received %d bytes from serial port\n", readCount);			
					if(send(newSockFd, (const void*)buffer, readCount, 0) != readCount)
        				{
                				ERROR("Mismatch in number of bytes send to socket: ");
						perror(NULL);
						terminate = 1;
						continue;
        				}
					INFO("Send %d bytes to socket\n", readCount);			
				}
				/* Check if server socket is ready for reading */ 
				if(FD_ISSET(newSockFd, &readRdy)) 
				{
					/* Read from the server socket and write to the serial port */
					readCount = recv(newSockFd, (void*)buffer, MAX_READ_SIZE, 0);
					if(readCount < 0)
					{
						ERROR("Reading from socket failed: ");
						perror(NULL);
						terminate = 1;
						continue;
					}
					INFO("Received %d bytes from socket\n", readCount);			
					if(write(uartFD, (const void*)buffer, readCount) != readCount)
        				{
                				ERROR("Mismatch in number of bytes written to serial port: ");
						perror(NULL);
						terminate = 1;
						continue;
        				}
					INFO("Send %d bytes to serial port\n", readCount);			
				}			
			}	
			else
			{
				ERROR("Select returned error: ");
				perror(NULL);
				terminate = 1;
			}
                }
        }

	/* Close the socket */
        close(sockFd);

        DEBUG("Thread exiting\n");

        return ;
}

void SerialSocketClient()
{
	struct sockaddr_in sockAddress;  
	int serverSock = 0, maxfd, readCount;
	fd_set  readRdy;
	
	/* Check if serial port is opened */
	if(!uartFD)
	{
		ERROR("Serial port is not open. \n");
		return;
	}

	/* Create the TCP socket */
        if((serverSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        {
                ERROR("Failed to create socket: ");
		perror(NULL);
		return;
        }
        /* Construct the server sockaddr_in structure */
        memset(&sockAddress, 0, sizeof(sockAddress));       /* Clear struct */
        sockAddress.sin_family = AF_INET;                  /* Internet/IP */
        sockAddress.sin_addr.s_addr = inet_addr(ipAddress);  /* IP address */
        sockAddress.sin_port = htons(atoi(portNumber));       /* server port */
        
	INFO("Connecting to server at %s:%s... \n", ipAddress, portNumber);
        /* Establish connection */
        if(connect(serverSock, (struct sockaddr *) &sockAddress, sizeof(sockAddress)) < 0)
        {
                ERROR("Failed to connect with server: ");
		perror(NULL);
		return;
        }

	maxfd = MAX(uartFD, serverSock)+1;	

	while(!terminate)
	{
		/* Set the fd_set */
		FD_ZERO(&readRdy); 
		FD_SET((unsigned int)uartFD, &readRdy);
		FD_SET((unsigned int)serverSock, &readRdy);

		INFO("Waiting for bytes to arrive from serial port / socket... \n");
		if((select(maxfd, &readRdy, NULL, NULL, NULL)) >= 0)
		{
			/* Check if serial port is ready for reading */
			if(FD_ISSET(uartFD, &readRdy))
			{
				/* Read from the serial port and write to the socket */
				readCount = read(uartFD, (void*)buffer, MAX_READ_SIZE);
				if(readCount < 0)
				{
					ERROR("Reading from serial device failed: ");
					perror(NULL);
					terminate = 1;
					continue;
				}
				INFO("Received %d bytes from serial port\n", readCount);			
				if(send(serverSock, (const void*)buffer, readCount, 0) != readCount)
        			{
                			ERROR("Mismatch in number of bytes send to socket: ");
					perror(NULL);
					terminate = 1;
					continue;
        			}
				INFO("Send %d bytes to socket\n", readCount);			
			}
			/* Check if server socket is ready for reading */ 
			if(FD_ISSET(serverSock, &readRdy)) 
			{
				/* Read from the server socket and write to the serial port */
				readCount = recv(serverSock, (void*)buffer, MAX_READ_SIZE, 0);
				if(readCount < 0)
				{
					ERROR("Reading from socket failed: ");
					perror(NULL);
					terminate = 1;
					continue;
				}
				INFO("Received %d bytes from socket\n", readCount);			
				if(write(uartFD, (const void*)buffer, readCount) != readCount)
        			{
                			ERROR("Mismatch in number of bytes written to serial port: ");
					perror(NULL);
					terminate = 1;
					continue;
        			}
				INFO("Send %d bytes to serial port\n", readCount);			
			}			
		}	
		else
		{
			ERROR("Select returned error: ");
			perror(NULL);
			terminate = 1;
		}
	
	}

	/* Close the socket */
	close(serverSock);
 
        DEBUG("Thread exiting\n");
}


void parse_arguments(int argc, char **argv)
{
	int i;

        for(i=0; i<argc; i++)
        {
                if(strstr(argv[i], "-h"))
                {
                        /* Display help and exit */
                        printf("Usage: %s [OPTIONS] \n", argv[0]);
                        printf("-d\tGPS device node to be used. (default: /dev/tigps )\n");
                        printf("-p\tTCP socket port number to be used in server or client mode. (default: 4145 ) \n");
                        printf("-m\tMode of operation 0 = client mode (default) 1 = server mode. \n");
                        printf("-a\tServer IP address (default = 192.168.1.10)\n");
                        printf("-v\tlog level (default: 0)\n");
                        printf("\t0 : Log no messages\n");
                        printf("\t1 : Log error messages only\n");
                        printf("\t2 : Log error and warning messages\n");
                        printf("\t3 : Log error, warning and debug messages\n");
                        printf("\t4 : Log error, warning, debug and information messages\n");
                        printf("-h\tPrints this help message\n");

                        exit(0);
                }
                if(strstr(argv[i], "-p"))
                {
                        if((strlen(argv[i])) > 2)
                        {
                                strcpy(portNumber, (char*)(argv[i]) + 2);
                        }
                        else if(argv[i+1] != NULL)
                        {
                                strcpy(portNumber, argv[i+1]);
                                i++;
                        }
                        continue;
                }
		if(strstr(argv[i], "-d"))
                {
                        if((strlen(argv[i])) > 2)
                        {
                                strcpy(portName, (char*)(argv[i]) + 2);
                        }
                        else if(argv[i+1] != NULL)
                        {
                                strcpy(portName, argv[i+1]);
                                i++;
                        }
                        continue;
                }
                if(strstr(argv[i], "-a"))
                {
                        if((strlen(argv[i])) > 2)
                        {
                                strcpy(ipAddress, (char*)(argv[i]) + 2);
                        }
                        else if(argv[i+1] != NULL)
                        {
                                strcpy(ipAddress, argv[i+1]);
                                i++;
                        }
                        continue;
                }
		if(strstr(argv[i], "-v"))
                {
                        if((strlen(argv[i])) > 2)
                        {
                                gDebugLevel = atoi((char*)(argv[i]) + 2);
                        }
                        else if(argv[i+1] != NULL)
                        {
                                gDebugLevel = atoi(argv[i+1]);
                                i++;
                        }
			continue;
		}

                if(strstr(argv[i], "-m"))
                {
                        if((strlen(argv[i])) > 2)
                        {
                                mode = atoi((char*)(argv[i]) + 2);
                        }
                        else if(argv[i+1] != NULL)
                        {
                                mode = atoi(argv[i+1]);
                                i++;
                        }
                        continue;
                }
	}
}

int main(int argc, char **argv) 
{

	/* Initialise portName, portNumber and IP address */
	sprintf(portName, "%s", "/dev/tigps");
	sprintf(portNumber, "%s", "4145");
	sprintf(ipAddress, "%s", "192.168.1.10");

	/* Parse the arguments */
	parse_arguments(argc, argv);

	/* Print a summary of operation */
	MSG("Mode of operation selected: %s\n", (mode==0)?"Client":"Server");
	if(!mode)
		MSG("Server IP Address: %s\n", ipAddress);
	else
		MSG("Server IP Address: %s\n", "system IP address");

	MSG("Server port number : %s\n", portNumber);
	MSG("Using serial device %s\n", portName);
	
	/* Open the serial port device */
	uartFD = open(portName, O_RDWR | O_NOCTTY | O_NDELAY);
	if (uartFD < 0) 
	{
		Die("Opening serial port failed.");
	}
	else
	{
		fcntl(uartFD, F_SETFL, 0);
	}

	/* Depending on mode selected, start either server thread or 
           client thread */
	if(mode)
	{
		SerialSocketServer();
	}
	else
	{
		SerialSocketClient();
	}
	

	/* Close the Serial file descriptor */
	close(uartFD);
	return 0;
}

