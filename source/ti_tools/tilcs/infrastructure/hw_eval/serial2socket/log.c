
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "log.h"

extern int gDebugLevel;

void debugprint(int level, const char *file, int line, const char *fmt, ...)
{
        va_list argp;
	char dinfo[][10] = {"MSG", "ERROR", "WARNING", "DEBUG", "INFO"};

	if(level > gDebugLevel)
		return;

        va_start(argp, fmt);

        if(DBGStream != NULL)
        {
                fprintf(DBGStream, "[%s] %s +%d : ", dinfo[level], file, line);
                vfprintf(DBGStream, fmt, argp);
                fflush(DBGStream);
        }
        va_end(argp);
}

