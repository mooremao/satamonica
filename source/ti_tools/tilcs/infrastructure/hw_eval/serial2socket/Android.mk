LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE           := ser2soc
LOCAL_SRC_FILES        := serial2socket.c log.c
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_C_INCLUDES       :=                 \
    $(LOCAL_PATH)
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_MODULE           := test_server
LOCAL_SRC_FILES        := test_server.c
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_C_INCLUDES       :=                 \
    $(LOCAL_PATH)
include $(BUILD_EXECUTABLE)
