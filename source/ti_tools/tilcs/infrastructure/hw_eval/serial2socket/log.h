
#ifndef __LOG_H
#define __LOG_H

#define DBGStream stdout

#define LEVEL_MSG	0
#define LEVEL_ERROR	1
#define LEVEL_WARNING	2
#define LEVEL_DEBUG	3
#define LEVEL_INFO	4

void debugprint(int level, const char *file, int line, const char *fmt, ...);
#define MSG(...) debugprint(LEVEL_MSG, __FILE__, __LINE__, __VA_ARGS__)
#define ERROR(...) debugprint(LEVEL_ERROR, __FILE__, __LINE__, __VA_ARGS__)
#define WARNING(...) debugprint(LEVEL_WARNING, __FILE__, __LINE__, __VA_ARGS__)
#define DEBUG(...) debugprint(LEVEL_DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#define INFO(...) debugprint(LEVEL_INFO, __FILE__, __LINE__, __VA_ARGS__)

#endif /* __LOG_H */

