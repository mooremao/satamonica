/*
  Add License Header 
*/

#ifndef __TI_LOG_H__
#define __TI_LOG_H__

#ifdef __cplusplus
	extern "C" {
#endif

enum log_source {
         e_log_connect,
         e_log_d_proxy,
         e_log_supl_2x,
         e_log_c_plane,
         e_log_pfm_app
};


/** Initializes the LOG Client for the specified source. This API must be 
    invoked only once in the life time of a process. It checks for the 
    presence of the Server i.e. Log MD.
    
    @param[in] source identifies intended user of LOG API(s) @see log_source

    @return -1 on error otherwise 0
*/
int  ti_log_init(const enum log_source source);

/** Sends LOG in string format along with additional information to the Server 
    i.e. Log MD.

    This function makes no assumption about the string contents that are being
    logged.

    @param[in] prefix  refers, typically, to the criticality of the string log
    @param[in] fnc_nm  name of the function that is posting this string log 
    @param[in] f_line  line # in the file that is posting this string log
    @param[in] rank_n  refers, numerally, to the criticality of the string log
    @param[in] format  defines a C style format of the string to be generated
    @param[in] ...     ellipsis anything and everything

    @return no return value
*/
void ti_log_string(const char *prefix, const char *fnc_nm, int f_line,
                   int rank_n, const char *format,   ...);

/** Sends LOG in binary format along with additional information to the Server
    i.e. Log MD

    This function makes no assumption about the binary contents that are being
    logged.

    @param[in] prefix  refers, typically, to the criticality of the binary log
    @param[in] fnc_nm  name of the function that is posting this binary log 
    @param[in] f_line  line # in the file that is posting this binary log
    @param[in] rank_n  refers, numerally, to the criticality of the binary log
    @param[in] bin_id  uniquely identifies the binary data @file log_id.h
    @param[in] n_objs  number of instances of the binary data being logged
    @param[in] binary  the real and the actual binary contents

    @return no return value
*/
void ti_log_binary(const char *prefix,  const char *fnc_nm,  int f_line, 
                   int rank_n, unsigned int bin_id, unsigned int n_objs,
                   void *binary);

void ti_log_ftrace(); /* TBD */

/** Exits the Log services.
    @return no return value
*/
void ti_log_exit();

/** Provides internal assigned name to the specified LOG Source. Utility
    function.

    @param[in] source identifies intended user of LOG API(s) @see log_source

    @return name of LOG Source
*/
const char *ti_log_name(const enum log_source src_id);

/*-----------------------------------------------------------------------
 *  MACROS - the real agents for logging
 *-----------------------------------------------------------------------
 */

#define TI_LOG_STR(prefix, level, ...)                                  \
        ti_log_string(prefix, __PRETTY_FUNCTION__, __LINE__, level,     \
                      __VA_ARGS__) 

#define TI_LOG_BIN(prefix, level, data_id, n_objs, data_ptr)            \
        ti_log_binary(prefix, __PRETTY_FUNCTION__, __LINE__, level,     \
                      data_id, n_objs, data_ptr)

/* Following are standard services that are available to processes / daemons */
#define TI_DATA(id, n_objs, data)  TI_LOG_BIN("DATA",  4, id, n_objs, data)
#ifdef ANDROID
/* For backward compatibility and to enables logging for minute details */
#define TI_LOG1(...)  TI_LOG_STR("INFO",  3, __VA_ARGS__)
#define TI_LOG2(...)  TI_LOG_STR("INFO",  4, __VA_ARGS__)
#define TI_LOG3(...)  TI_LOG_STR("INFO",  5, __VA_ARGS__)

#define TI_BIN1(id, n_objs, data)  TI_LOG_BIN("DATA",  3, id, n_objs, data)
#define TI_BIN2(id, n_objs, data)  TI_LOG_BIN("DATA",  4, id, n_objs, data)
#define TI_BIN3(id, n_objs, data)  TI_LOG_BIN("DATA",  5, id, n_objs, data)


#define TI_INFO(...)  TI_LOG_STR("INFO",  3, __VA_ARGS__)
#define TI_WARN(...)  TI_LOG_STR("WARN",  2, __VA_ARGS__)
#define TI_ERRx(...)  TI_LOG_STR("ERRx",  1, __VA_ARGS__)
#endif

#ifdef __cplusplus
	}
#endif 

#endif
