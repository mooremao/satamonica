/*
   Add License Header 
 */

#ifndef __LOG_ID_H__
#define __LOG_ID_H__

#include "gnss.h"
#include "ti_log.h"
#include <stdio.h>


/* Network communication related nuances */
#define TI_LOG_DIRECTORY_FPATH        "/mnt/userdata/gnss/log_MD/"
#define TI_LOG_SOCKS_BASE_NAME        TI_LOG_DIRECTORY_FPATH
#define TI_LOG_SERVER_NET_PATH        TI_LOG_SOCKS_BASE_NAME "MD_sock"

enum log_typeof {

	e_log_string = 1,
	e_log_binary,
	e_log_ftrace
};

#define LOG_DATA_LEN 8092
#define LOG_NAME_LEN 256 

/* Construct to transact LOG Data from Clients to LOG Server */
struct log_buf {

	enum log_source  source;                  /* Log Maker       */

	unsigned int     t_secs;                  /* Time secs       */
	unsigned int     t_usec;                  /* Time usec       */
	char             prefix[LOG_NAME_LEN];    /* Text Info       */
	char             fnc_nm[LOG_NAME_LEN];    /* Func name       */
	char             f_name[LOG_NAME_LEN];    /* File name       */
	int              f_line;                  /* File Line #     */  
	int              rank_n;                  /* Log level #     */

	enum log_typeof  l_type;                  /* Log  type       */
	union {
		int      bin_id;                  /* For Binary      */ 
		int      fn_lvl;                  /* For Ftrace      */
	} u;

	int              length;                 /* Data size        */
        
        /* Note: Buffer should be the last element in this structure */
	char             buffer[LOG_DATA_LEN];   /* Real data        */
#define LOG_NW_SIZEOF(log) (log.length + sizeof(struct log_buf) - LOG_DATA_LEN)

};

struct logb_info {

	unsigned int     bin_id;
	unsigned int     bin_sz;
};

#define LOGB_ID_BLOCK_SIZE          (0x10000)
#define LOGB_ID_BLOCK_MASK          (LOGB_ID_BLOCK_SIZE - 0x1)

#define IS_LOGB_ID_IN_RANGE(id, base)                           \
        ((base < id) && (id < (base + LOGB_ID_BLOCK_SIZE)))

#define CONNECT_LOGB_ID_BASE        0x00000000
#define D_PROXY_LOGB_ID_BASE        CONNECT_LOGB_ID_BASE + LOGB_ID_BLOCK_SIZE
#define EE_ADAP_LOGB_ID_BASE        D_PROXY_LOGB_ID_BASE + LOGB_ID_BLOCK_SIZE
#define PFM_APP_LOGB_ID_BASE        EE_ADAP_LOGB_ID_BASE + LOGB_ID_BLOCK_SIZE
#define UP_ADAP_LOGB_ID_BASE        PFM_APP_LOGB_ID_BASE + LOGB_ID_BLOCK_SIZE

/* TBD: include check for offset > LOGB_ID_BLOCK_SIZE */
#define MK_LOGB_ID(base, offset)    (base + offset) 

/*------------------------------------------------------------------------------
 * Binary LOG Event ID for Platform Application i.e. PFM APP 
 *------------------------------------------------------------------------------
 */

#define MK_PFM_APP_LOGB_ID(offset)  MK_LOGB_ID(PFM_APP_LOGB_ID_BASE, offset)

#define PFM_APP_LOGB_ID_LOC_REQ           MK_PFM_APP_LOGB_ID(0x00000001)
#define PFM_APP_LOGB_ID_H_START           MK_PFM_APP_LOGB_ID(0x00000002)
#define PFM_APP_LOGB_ID_POS_RESULT        MK_PFM_APP_LOGB_ID(0x00000003)
#define PFM_APP_LOGB_ID_LOC_END           MK_PFM_APP_LOGB_ID(0x00000004)
#define PFM_APP_LOGB_ID_QOP               MK_PFM_APP_LOGB_ID(0x00000005)


/*------------------------------------------------------------------------------
 * Binary LOG Event ID for User Plane (UP) i.e. SUPL Adapter 
 *------------------------------------------------------------------------------
 */
#define MK_UP_ADAP_LOGB_ID(offset)  MK_LOGB_ID(UP_ADAP_LOGB_ID_BASE, offset)

#define UP_ADAP_LOGB_ID_POS_RESULT        MK_UP_ADAP_LOGB_ID(0x00000001)
#define UP_ADAP_LOGB_ID_GPS_ALM           MK_UP_ADAP_LOGB_ID(0x00000002)
#define UP_ADAP_LOGB_ID_GPS_EPH	          MK_UP_ADAP_LOGB_ID(0x00000003)
#define UP_ADAP_LOGB_ID_GPS_ION	          MK_UP_ADAP_LOGB_ID(0x00000004)
#define UP_ADAP_LOGB_ID_GPS_UTC	          MK_UP_ADAP_LOGB_ID(0x00000005)
#define UP_ADAP_LOGB_ID_GPS_ATOW          MK_UP_ADAP_LOGB_ID(0x00000006)
#define UP_ADAP_LOGB_ID_GPS_TIME          MK_UP_ADAP_LOGB_ID(0x00000007)
#define UP_ADAP_LOGB_ID_GPS_REF_ALM       MK_UP_ADAP_LOGB_ID(0x00000008)
#define UP_ADAP_LOGB_ID_GPS_REF_POS       MK_UP_ADAP_LOGB_ID(0x00000009)
#define UP_ADAP_LOGB_ID_GPS_ACQ           MK_UP_ADAP_LOGB_ID(0x0000000a)
#define UP_ADAP_LOGB_ID_GPS_QOP           MK_UP_ADAP_LOGB_ID(0x0000000b)
#define UP_ADAP_LOGB_ID_LOC_END           MK_UP_ADAP_LOGB_ID(0x0000000c)



static struct logb_info pfm_app_logb_tbl[] = {

        {PFM_APP_LOGB_ID_LOC_REQ,               0},    
        {PFM_APP_LOGB_ID_H_START,               0},
        {PFM_APP_LOGB_ID_POS_RESULT,            sizeof(struct gnss_pos_result)},
        {PFM_APP_LOGB_ID_LOC_END,               0},
        {0xFFFFFFFF,                            0}
};

static struct logb_info up_adap_logb_tbl[] = {

        {UP_ADAP_LOGB_ID_POS_RESULT,            sizeof(struct gnss_pos_result)},
	{UP_ADAP_LOGB_ID_GPS_ALM,               sizeof(struct gps_alm)},
	{UP_ADAP_LOGB_ID_GPS_EPH,               sizeof(struct gps_eph)},
	{UP_ADAP_LOGB_ID_GPS_ION,               sizeof(struct gps_ion)},
	{UP_ADAP_LOGB_ID_GPS_UTC,               sizeof(struct gps_utc)},
	{UP_ADAP_LOGB_ID_GPS_ATOW,              sizeof(struct gps_atow)},
	{UP_ADAP_LOGB_ID_GPS_TIME,              sizeof(struct gps_time)},
	{UP_ADAP_LOGB_ID_GPS_REF_ALM,           sizeof(struct gps_ref_time)},
        {UP_ADAP_LOGB_ID_LOC_END,               0},
        {0xFFFFFFFF,                            0}
};

static struct logb_info *get_logb_info(unsigned int bin_id)
{
	struct logb_info *tbl = NULL;

#define IS_LOGB_IN_RANGE(base) IS_LOGB_ID_IN_RANGE(bin_id, base)

	if(IS_LOGB_IN_RANGE(PFM_APP_LOGB_ID_BASE)) tbl = pfm_app_logb_tbl;
	if(IS_LOGB_IN_RANGE(UP_ADAP_LOGB_ID_BASE)) tbl = up_adap_logb_tbl;

	if(NULL == tbl)  return NULL;

	/* Find out if ID of the data is known; if not, return NULL*/
	while ((0xFFFFFFFF != tbl->bin_id) && 
	       (LOGB_ID_BLOCK_MASK != (tbl->bin_id & LOGB_ID_BLOCK_MASK))) {
		if(tbl->bin_id == bin_id)
			return tbl;

		tbl++;
	}

	return NULL;
}

#endif

