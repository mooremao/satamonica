
/*
 * new_utils.h
 *
 *
 * Copyright (C) {2012} Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *
 */

#ifndef _NVS_UTIL_H_
#define _NVS_UTIL_H_

#ifdef __cplusplus
	extern "C" {
#endif 

#define MAX_SECS_IN_A_MIN     (60)
#define MAX_SECS_IN_1HOUR     (60   * MAX_SECS_IN_A_MIN)
#define MAX_SECS_IN_A_DAY     (24   * MAX_SECS_IN_1HOUR)
#define MAX_SECS_IN_1WEEK     (7    * MAX_SECS_IN_A_DAY)
#define MAX_SECS_IN_365DY     (365  * MAX_SECS_IN_A_DAY)
#define MAX_SECS_4Y_BLOCK     (1461 * MAX_SECS_IN_A_DAY)

#define DO_GLO_NT_SECS(NT) ((NT - 1) * MAX_SECS_IN_A_DAY)

#define DO_GLO_Tk_SECS(Tk)                                   \
(                                                            \
	(((Tk >> 7) & 0x1F) * MAX_SECS_IN_1HOUR) +  /* HH */ \
	(((Tk >> 1) & 0x3F) * 60)                +  /* MM */ \
	(((Tk << 0) & 0x01) ? 30 : 0)               /* SS */ \
)

#define DO_GLO_N4_SECS(N4)    ((N4 - 1) * MAX_SECS_4Y_BLOCK)
#define DO_GLO_NA_SECS(NA)    ((NA - 1) * MAX_SECS_IN_A_DAY)

#define UTC_AT_GLO_ORIGIN                                               \
(                                                                       \
	365 *24*60*60 +     /* Y1970 */                                 \
	365 *24*60*60 +     /* Y1971 */                                 \
	1461*24*60*60 * 6  /* 6 groups of 4 years, i.e. 1972 to 1996 */ \
)
static unsigned int calc_min_secs(unsigned int min)
{
	return min * MAX_SECS_IN_A_MIN;
}

static unsigned int calc_hour_secs(unsigned int hrs)
{
	return hrs * MAX_SECS_IN_1HOUR;
}


static unsigned int calc_week_secs(unsigned int week)
{
        return week * MAX_SECS_IN_1WEEK;
}

// Move to C file 
inline static unsigned int get_week_secs(unsigned int week)
{
        return calc_week_secs(week);
}

// Move to C file 
inline static unsigned int wk2secs(unsigned int week)
{
        return calc_week_secs(week);
}

static unsigned int calc_day_secs(unsigned int day)
{
        return day * MAX_SECS_IN_A_DAY;
}

// Move to C file 
inline static unsigned int get_day_secs(unsigned int day)
{
        return calc_day_secs(day);
}

// Move to C file 
inline static unsigned int day2secs(unsigned int day)
{
        return calc_day_secs(day);
}

static unsigned int calc_year_secs(unsigned int yrs)
{
	return yrs * MAX_SECS_IN_365DY;
}

static unsigned int calc_4year_secs(unsigned int N4_year)
{
        return N4_year * MAX_SECS_4Y_BLOCK;
}

// Move to C file 
inline static unsigned int get_gps_eph_secs(unsigned char week, unsigned char toe)
{
        return calc_week_secs(week) + toe*16;
}

// Move to C file 
inline static unsigned int get_glo_eph_secs_N4(unsigned char N4, unsigned short NT, 
                                        unsigned short Tk)
{
        return  calc_4year_secs(N4 - 1)          + /* 4Y */
                calc_day_secs(NT - 1)            + /* DD */
                calc_hour_secs((Tk >> 7) & 0x1F) + /* HH */                     
                calc_min_secs((Tk >> 1) & 0x3F)  + /* MM */ 
                ((Tk << 0) & 0x01)? 30 : 0;        /* SS */
}

// Move to C file 
inline static unsigned int get_glo_eph_secs_NT(unsigned short NT, unsigned short Tk)
{
        return  calc_year_secs(NT - 1)           + /* 4Y */
                calc_hour_secs((Tk >> 7) & 0x1F) + /* HH */                     
                calc_min_secs((Tk >> 1) & 0x3F)  + /* MM */ 
                ((Tk << 0) & 0x01)? 30 : 0;        /* SS */
}

// Move to C file 
inline static unsigned int get_glo_alm_secs_N4(unsigned char N4, unsigned short NA)
{
        return calc_4year_secs(N4 - 1) + calc_day_secs(NA - 1);
} 

inline static unsigned int glo_alm_days(unsigned char N4, unsigned short NA)
{
	return (N4 - 1)*MAX_SECS_4Y_BLOCK + (NA - 1)*MAX_SECS_IN_A_DAY;
}

static unsigned char 
Mn_Dn_map_no_leap [] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

static unsigned char
Mn_Dn_map_leap_yr [] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

struct MDS1Y_offset {

	unsigned char   Mn; /* 0 based, 0 to 11    */ 
	unsigned short  Dn; /* 0 based, 0 to 30    */
	unsigned int    ss; /* 0 based, 0 to 86399 */
};

struct YMDS_offset {

	unsigned char   Yn;   /* 0 based, 0O to 99    */
	unsigned char   Mn;   /* 0 based, 0  to 11    */
	unsigned short  Dn;   /* 0 based, 0  to 30    */
	unsigned int    ss;   /* 0 based, 0  to 86399 */
};

struct week_offset {

	unsigned short Wn;  /* 1 Based */
	unsigned int   ss;  /* 0 Based */
};


struct MDS1Y_offset secs2MDS1Y(unsigned int secs, unsigned char leap_yr);

unsigned char days2months_1Y(unsigned short days, unsigned char leap_yr);

unsigned short months2days_1Y(unsigned char months, unsigned char leap_yr);

struct YMDS_offset secs2YMDS(unsigned int secs);

unsigned int YMDS2secs(struct YMDS_offset ymds);

struct week_offset secs2week(unsigned int secs);


#ifdef __cplusplus
}
#endif 


#endif
