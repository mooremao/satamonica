
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "gnss.h"
#include "gnss_lcs_common.h"
#include "cmcc_definitions.h"
#include "Logger_Adapter.h"
#include "cmcc_log_parser.h"
#include "cmcc_common.h"

#include "nvs_utils.h"
#include <utils/Log.h>
#include <math.h>

char cmcc_buf[LOG_DATA_LEN];
static int cmcc_fp = 0;
int session_id  = -1;
bool assist_loc = false;
bool auto_loc   = false;
int pos_res_cnt = 0;
unsigned int start_msec = 0;

extern struct gnss_qop qop;

extern int cmcc_log_bin_autonomous(const struct log_buf& log);
extern int cmcc_log_string_autonomous(const struct log_buf& log);

static int gps_qop_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size  = 0;                 /* data length */
        int n_obj    = log.length / sizeof(struct gps_eph);
        struct gnss_qop *qop = (struct gnss_qop*) log.buffer;
        
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, "0X%.8X:%d",
                                MSG_INFO_SESSION_QOP_VALUE, session_id);
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, ", %d,%d,%d,%d", qop->h_acc_M,
                                qop->v_acc_M, qop->max_age, qop->rsp_secs);
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, " #%s\n", STR_MSG_QOP);
        
        return wr_size;
}


static int gps_eph_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size  = 0;                 /* data length */
        int n_obj    = log.length / sizeof(struct gps_eph);
        struct gps_eph *eph = (struct gps_eph*) log.buffer;

        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, "0X%.8X:%d",
                                        MSG_INFO_EPH_AIDING_VALUE, session_id);
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, ", %d", eph[0].Toe);        

        for(int i = 0; i < n_obj; i++) {
                if(wr_size < len)
                wr_size += sprintf(buf + wr_size, " :%d", eph[i].svid);
        }        

        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, " #%s\n", STR_MSG_EPH);

        return wr_size;
}


static int gps_alm_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size    = 0;                 /* data length */
        int n_obj    = log.length / sizeof(struct gps_eph);
        struct gps_alm *alm = (struct gps_alm*) log.buffer;

        if(wr_size < len)
                wr_size += sprintf(buf+wr_size,"0X%.8X:%d",
                                MSG_INFO_EPH_AIDING_VALUE, session_id);
        if(wr_size < DATA_LEN)
                                wr_size += sprintf(buf+wr_size, ", %d", alm[0].Toa);

        for(int i = 0; i < n_obj; i++) {
                        if(wr_size < DATA_LEN)
                        wr_size += sprintf(buf+wr_size, " :%d", alm[i].svid);
        }        

        if(wr_size < DATA_LEN)
                wr_size += sprintf(buf+wr_size + wr_size, " #%s\n", STR_MSG_EPH);

        return wr_size;
	
}


unsigned int get_gps_time_ms(struct gps_time *tim)
{
        unsigned int secs = 0;

        switch(tim->tow_unit) {

        case sec_8by100:
                secs = (tim->tow *8) / 10;
                break;
        case milli_secs:
                secs = (tim->tow) / 1000;
                break;
        case clear_secs:
                secs = tim->tow;
                break;
        default:
                break;
        }

        return secs;
}

int gps_time_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size    = 0;                 /* data length */
        struct gps_ref_time *time = (struct gps_ref_time *)log.buffer;	

        if(wr_size < len)
                wr_size += sprintf(buf+wr_size, "0X%.8X:%d",
                                MSG_INFO_TIM_AIDING_VALUE,
                                session_id);

        if(wr_size < len)
                        wr_size += sprintf(buf+wr_size, ", %d,%d",
                        time->time.week_nr, get_gps_time_ms(&(time->time)));

        if(wr_size < len)
                        wr_size += sprintf(buf+wr_size, " #%s\n", STR_MSG_TIM);

        return wr_size;
}

static 
int gps_ref_pos_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size    = 0;
        struct location_desc *loc = ( struct location_desc *)log.buffer;

        if(wr_size < len)
                wr_size += sprintf(buf+wr_size, "0X%.8X:%d",
                                MSG_INFO_POS_AIDING_VALUE,
                                session_id);

        if(wr_size < len)
                wr_size += sprintf(buf+wr_size, ", %d,%d",
                                loc->longitude_N, loc->latitude_N);

        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, " #%s\n", STR_MSG_REF_POS);

        return wr_size;
}

int acq_data_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        struct gps_acq *acq = (struct gps_acq *)log.buffer;
        int wr_size    = 0;                 /* data length */
        int n_obj    = log.length / sizeof(struct gps_eph);

        wr_size += sprintf(buf+wr_size, "0X%.8X:%d", 
                                        MSG_INFO_ACQ_AIDING_VALUE, session_id);

        if(wr_size < DATA_LEN)
                                wr_size += sprintf(buf + wr_size,  ", %d", acq[0].gps_tow);

        for(int i = 0; i < n_obj; i++) {
                if(wr_size < DATA_LEN)
                wr_size += sprintf(buf + wr_size,  " :%d", acq[i].svid);
        }        

        if(wr_size < DATA_LEN)
                wr_size += sprintf(buf + wr_size,  " #%s\n", STR_MSG_ACQ);

        return wr_size;
}

static 
int pos_res_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size = 0;
        char utc_tim_buf[32];
        struct gnss_pos_result *pResult = (struct gnss_pos_result *)log.buffer;
        const struct location_desc&   loc = (struct location_desc)pResult->location;
        double latitude = 0, longitude = 0;
        unsigned int end_msec = (log.t_secs*1000) + (log.t_usec/1000);
        if(wr_size<len)
                wr_size += sprintf(buf+wr_size, "0X%.8X:%d",
                                MSG_INFO_POSITION_RESULT_VALUE, session_id);

        if(wr_size < len){
                get_utc_tim_str(pResult->utc_secs, utc_tim_buf, sizeof(utc_tim_buf));
                wr_size += sprintf(buf + wr_size, ", %s", utc_tim_buf);
        }

        if(wr_size<len){
                char *alt_sign = "+";
                if(0 == loc.alt_dir )
                        alt_sign = "+";
                else
                        alt_sign = "-";

                if((loc.lat_deg_by_2p8 == 0)&&(loc.lon_deg_by_2p8 == 0)){

                        latitude   = (double)loc.latitude_N * 90.0f/8388608.0f;
                        if(loc.lat_sign == 1 )
                               latitude = -latitude;

                        longitude  = (double)loc.longitude_N * 360.0f/16777216.0f;
                }else{
                        latitude = (double) loc.lat_deg_by_2p8 * (180 / pow(2.0, 32));
                        longitude = (double) loc.lon_deg_by_2p8 * (180 / pow(2.0,31));

                }

                wr_size += sprintf(buf+wr_size, ", %lf,%lf,%s%lf",
                                        latitude,
                                        longitude,
                                        alt_sign,
                                        (double)loc.altitude_N );
        }

        if(wr_size<len)
                wr_size += sprintf(buf+wr_size, ",%d,%d",
                        pResult->location.orientation, pResult->location.height);

        if(wr_size<len)
                wr_size += sprintf(buf+wr_size, ",%d,%d",
                        pResult->location.unc_semi_maj_K, pResult->location.unc_semi_min_K);

        if(wr_size<len)
                wr_size += sprintf(buf + wr_size, ": %d", (end_msec - start_msec));

        if(wr_size < len)
                wr_size += sprintf(buf+wr_size, " #%s\n", STR_MSG_POS);

        return wr_size;

}

static 
int loc_end_put2cmcc_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size = 0;

        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, "0X%.8X:%d",
                        MSG_INFO_SESSION_END_VALUE, session_id);
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size, ", #%s\n", "Session End");

        return wr_size;
}

static int is_no_network_msg(char *buf)
{
        if(strstr(buf, MSG_INFO_NET_CONN_FAILURE))
                return 1;

        return 0;
}

static int is_ignore_msg(char* in_buf)
{
        if(strstr(in_buf,MSG_INFO_SRVER_CONN_CONNECTING) ||
                strstr(in_buf,MSG_INFO_TIME_AIDING) ||
                strstr(in_buf,MSG_INFO_SESSION_END) ||
                strstr(in_buf,MSG_INFO_POSITION_RESULT)||
                strstr(in_buf,MSG_INFO_RTI_AIDING)){

                ALOGD("Ignored: %s", in_buf);
                return 1;
        }
        return 0;
}

static int parse_string_message(char* in_buf, char* out_buf)
{
        char *logmsg = in_buf;
        cmcc_log_parser parser(session_id);
        struct supl_log slog;
        int buf_len = 0;

        if(parser.parse_message(logmsg,slog) == SUCCESS){
                memset(out_buf, 0, sizeof(out_buf));
                buf_len = parser.prepare_cmcc_msg(slog, out_buf, LOG_DATA_LEN);
         }

        return buf_len;
}

static int file_size(int fd){

        struct stat sb;

        if(!fd)
                return 0;

        if(fstat(fd, &sb) < 0 )
                return 0;

        return sb.st_size;
}
int open_cmcc_log_file(int flags, int mode)
{
        cmcc_fp = open(CMCC_LOG_FILE_PATH, flags, mode);
        if( cmcc_fp < 0)
                return -1;

        return 1;
}
static int write_to_file(char* buf, int len){
#define FILE_SZ_1KB  1024
#define FILE_SZ_32KB FILE_SZ_1KB * 32

        if(cmcc_fp && len){

                int fil_sz = file_size(cmcc_fp);
                if( ( fil_sz  + len ) > FILE_SZ_32KB ){
                        ALOGD("CMCC: FILE SIZE EXCEEDS 32KB");
                        lseek(cmcc_fp, 0, 0 );
                }

                if(cmcc_fp){
                        write(cmcc_fp,(void*)buf,len);
                        return SUCCESS;
                }
        }

        return FAIL;
}

static int cmcc_adapter_bin_assisted_loc(const struct log_buf& log)
{
        bool eos_set = false;  // eos -> End of Session 
        memset(cmcc_buf, 0, sizeof(cmcc_buf));
        int wr_size = log_time2cmcc_buf(log.t_secs, log.t_usec,
                                        cmcc_buf, LOG_DATA_LEN);

        switch(log.u.bin_id) {

        case PFM_APP_LOGB_ID_POS_RESULT:
        case UP_ADAP_LOGB_ID_POS_RESULT:

        wr_size += pos_res_put2cmcc_buf(log, cmcc_buf + wr_size,
                        LOG_DATA_LEN  - wr_size);
        pos_res_cnt++;
        if(1 < pos_res_cnt) wr_size = 0;
        break;

        case PFM_APP_LOGB_ID_LOC_END:
        case UP_ADAP_LOGB_ID_LOC_END:
                eos_set  = true;
                start_msec = 0;
                wr_size += loc_end_put2cmcc_buf(log, cmcc_buf + wr_size,
                                        LOG_DATA_LEN  - wr_size); break;

        default:
                wr_size = 0;
                break;
        }
        if(wr_size && assist_loc){ /* Define bool assist_loc elsewhere */
                write_to_file(cmcc_buf,wr_size);
                ALOGD("CMCC_BIN_ASSIST_OUT: %s",cmcc_buf);
        }

        if(eos_set) { assist_loc = false; pos_res_cnt = 0; }

        return wr_size;
}


static int cmcc_adapter_string_assisted_loc(const struct log_buf& log)
{
        bool sess_end = false;
        
        ALOGD("CMCC_STRING_IN: %s", log.buffer);

        if(is_ignore_msg((char*)log.buffer))
                return 0;

        if(strstr(log.buffer, "SESSION_START")) {
                session_id += 1; assist_loc = true; auto_loc = false;
                pos_res_cnt = 0;
                start_msec  = (log.t_secs*1000) + (log.t_usec/1000);
        }

        int wr_size = parse_string_message((char*)log.buffer, cmcc_buf);

        if(wr_size && assist_loc){
                write_to_file(cmcc_buf, wr_size);
                ALOGD("CMCC_STRING_OUT: %s", cmcc_buf);
        }

        return 0;
}

int cmcc_adapter_binary_handler(const struct log_buf& log)
{
        int ret_val = 0;
        if(!cmcc_fp)
                open_cmcc_log_file( O_CREAT|O_RDWR|O_APPEND, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );

        if(true == assist_loc)
                ret_val = cmcc_adapter_bin_assisted_loc(log);
        else
                ret_val = cmcc_log_bin_autonomous(log);

        return 0;
}

static void start_auto_on_net_error(const struct log_buf& log)
{
         static struct log_buf new_log;
         new_log.t_secs   = log.t_secs;
         new_log.t_usec   = log.t_usec;
         new_log.u.bin_id = PFM_APP_LOGB_ID_H_START;

         cmcc_log_bin_autonomous(new_log);

         return;
}

int cmcc_adapter_string_handler(const struct log_buf& log)
{        
        if(!cmcc_fp)
                open_cmcc_log_file( O_CREAT|O_RDWR|O_APPEND, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );

        if(is_no_network_msg((char*)log.buffer)){
                ALOGD("CMCC_ NW Error", cmcc_buf);
                assist_loc = false; /* NW error: so no assisted LOC possible */
                start_auto_on_net_error(log);
                return 0;
        }

        cmcc_adapter_string_assisted_loc(log);

        return 0;
}







