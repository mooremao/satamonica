#ifndef CMCC_LOGGER_ADAPTER_H
#define CMCC_LOGGER_ADAPTER_H

#include "Logger_Adapter.h"

int cmcc_log_adapter_load(struct log_adapter_ops& ops);

#endif
