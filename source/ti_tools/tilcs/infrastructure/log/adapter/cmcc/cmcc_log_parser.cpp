
#include "cmcc_log_parser.h"
#include "cmcc_definitions.h"
#include "nvs_utils.h"
#include <utils/Log.h>

#include <stdio.h>
#include <stdlib.h>

static struct msg_info msg_info_tbl[] = {
	{  MSG_INFO_SESSION_START,	MSG_INFO_SESSION_START_VALUE},
	{  MSG_INFO_SESSION_END,	MSG_INFO_SESSION_END_VALUE},
	{  MSG_INFO_SESSION_QOP,	MSG_INFO_SESSION_QOP_VALUE},
	{  MSG_INFO_POSITION_RESULT,    MSG_INFO_POSITION_RESULT_VALUE},
	{  MSG_INFO_EPH_AIDING,		MSG_INFO_EPH_AIDING_VALUE},
	{  MSG_INFO_ALM_AIDING,		MSG_INFO_ALM_AIDING_VALUE},
	{  MSG_INFO_POS_AIDING,		MSG_INFO_POS_AIDING_VALUE},
	{  MSG_INFO_TIME_AIDING,	MSG_INFO_TIM_AIDING_VALUE},
	{  MSG_INFO_ACQ_AIDING,		MSG_INFO_ACQ_AIDING_VALUE},
	{  MSG_INFO_SUPL_SW_VER,	MSG_INFO_SUPL_SW_VER_VALUE},
	{  MSG_INFO_NET_CONN_SUCCESS,   MSG_INFO_NET_CONN_SUCCESS_VALUE},
	{  MSG_INFO_SRVER_CONN_SUCCESS, MSG_INFO_SRVER_CONN_SUCCESS_VALUE},
	{  MSG_INFO_OTA_MSG,		MSG_INFO_OTA_MSG_VALUE},
	{  MSG_INFO_GPS_NOT_PROVD_POSITION_WITHIN_RSP_TIME, MSG_INFO_GPS_NOT_PROVD_POSITION_WITHIN_RSP_TIME_VALUE},
	{  MSG_INFO_SI_SESSN_NO_NET_CONN_DEFLT_STADALONE,	MSG_INFO_SI_SESSN_NO_NET_CONN_DEFLT_STADALONE_VALUE},
	{  MSG_INFO_UNKNOW_OTA_MSG_DISCARD,   			MSG_INFO_UNKNOW_OTA_MSG_DISCARD_VALUE},
	{  MSG_INFO_UNKNOW_GPS_MSG_DISCARD,   			MSG_INFO_UNKNOW_GPS_MSG_DISCARD_VALUE},
	{  MSG_INFO_RX_UNEXPTED_OTA_MESG_FSM_OUTOFSYNC,   	MSG_INFO_RX_UNEXPTED_OTA_MESG_FSM_OUTOFSYNC_VALUE},
	{  MSG_INFO_SI_SESSN_WRNG_INPUT_CONFIG,   		MSG_INFO_SI_SESSN_WRNG_INPUT_CONFIG_VALUE},
	{  MSG_INFO_NET_CONN_FAILURE,   			MSG_INFO_NET_CONN_FAILURE_VALUE},
	{  MSG_INFO_SRVR_CONN_FAILURE,   			MSG_INFO_SRVR_CONN_FAILURE_VALUE},
	{  MSG_INFO_TLS_FAILURE,   				MSG_INFO_TLS_FAILURE_VALUE},
	{  MSG_INFO_SUPL_END_ERROR,   				MSG_INFO_SUPL_END_ERROR_VALUE},
	{  MSG_INFO_OTA_MSG_TIMEOUT,   				MSG_INFO_OTA_MSG_TIMEOUT_VALUE},
	{  MSG_INFO_OTA_MSG_DECODING_ERROR,   			MSG_INFO_OTA_MSG_DECODING_ERROR_VALUE},
	{  MSG_INFO_GPS_MSG_TIME_OUT,   			MSG_INFO_GPS_MSG_TIME_OUT_VALUE},
	{  MSG_INFO_SRVER_CONN_CONNECTING,   			MSG_INFO_SRVER_CONN_SUCCESS_VALUE},
	{  MSG_INFO_END,   					-1},
};

cmcc_log_parser::cmcc_log_parser(unsigned int sid)
{
	session_cnt 	= sid;
}

cmcc_log_parser::~cmcc_log_parser()
{

}

int cmcc_log_parser::get_msg_info(const char* msg)
{
	msg_info *record = msg_info_tbl;
	int msg_info_val = -1;

	while (record->msg_info_value != -1 ){
		if(strcmp(msg,record->pMsg_info_str) == 0){
			msg_info_val =  record->msg_info_value;
			break;
		}
		record++;
	}

	return msg_info_val;
}

char* cmcc_log_parser::get_delimiter(char *buf, char val)
{
        while('\0' != *buf) {
                if(val == *buf)
                        break;                
                buf++;
        }
        return buf;
}

char* cmcc_log_parser::buf4next_parse(char *ptr)
{
        *ptr  = '\0';

         return ptr + 1; 
}

/* Parse the whole string message and check for the valid format */
int cmcc_log_parser :: parse_message(char* buf, supl_log& supl)
{
        char *ptr = NULL;

        supl.time =  buf;        
        ptr = get_delimiter(buf, MSG_SEPERATOR);
        if('\0' == *ptr)
                goto parse_exit1;

        buf = buf4next_parse(ptr);
         
        supl.type =  buf;
        ptr = get_delimiter(buf, MSG_SEPERATOR);
        if('\0' == *ptr)
                goto parse_exit1;

        buf = buf4next_parse(ptr);
        
        supl.info =  buf;
        ptr = get_delimiter(buf, MSG_SEPERATOR);
        if('\0' == *ptr)
                goto parse_exit1;

        buf = buf4next_parse(ptr);
        
        supl.data =  buf;
        ptr = get_delimiter(buf, MSG_SEPERATOR);
        if('\0' == *ptr)
                goto parse_exit1;

        buf = buf4next_parse(ptr);
        
        supl.note =  buf;        

        return SUCCESS;

 parse_exit1:
        
        supl.time = NULL;
        supl.info = NULL;
        supl.data = NULL;
        supl.note = NULL;

	return FAIL;
}

int cmcc_log_parser::is_substr(char* str, char* sample)
{
	if(strstr(str,sample))
                        return 1;
        else
                return 0;
}

bool cmcc_log_parser::is_hash_required(char *data)
{
	/* If DATA field contains one of these, prepend # */
	if(is_substr(data, "SUPL_END") || 
	   is_substr(data, "SUPL_INIT")||
	   is_substr(data, "SUPL_START")||
		is_substr(data, MSG_INFO_POSITION_RESULT)||
		is_substr(data, MSG_INFO_SUPL_SW_VER)||
		is_substr(data, MSG_INFO_SRVER_CONN_SUCCESS)||
		is_substr(data, MSG_INFO_NET_CONN_SUCCESS)||     
		is_substr(data, MSG_INFO_SRVR_CONN_FAILURE)||
		is_substr(data, MSG_INFO_SESSION_QOP)||
		is_substr(data, MSG_INFO_EPH_AIDING)||
		is_substr(data, MSG_INFO_TIME_AIDING)||
		is_substr(data, MSG_INFO_ALM_AIDING)||
		is_substr(data, MSG_INFO_ACQ_AIDING)||
		is_substr(data, MSG_INFO_POS_AIDING)||
		is_substr(data, MSG_INFO_SESSION_START)||
		is_substr(data, MSG_INFO_SESSION_END))
			return true;

	return false;
}

int cmcc_log_parser::get_utc_time_str(unsigned int utc_sec, char *buf)
{
	struct YMDS_offset ymds = secs2YMDS(utc_sec);
	unsigned int t_hh = (ymds.ss / (60*60));
	unsigned int t_mm = (ymds.ss - (t_hh * 60 *60)) / 60;
	unsigned int t_ss = (ymds.ss % 60);
	int wr_size = 0;
        
	wr_size += sprintf(buf, "%4d-%02d-%02d %02d:%02d:%02d",
				ymds.Yn + 1970, ymds.Mn + 1,
				ymds.Dn + 1, t_hh,  t_mm,  t_ss);

	return wr_size;
}
int cmcc_log_parser::prepare_cmcc_msg(struct supl_log& supl, char* buf, int len)
{
	bool  hash = false;
	char *ptr1 = NULL, *vers = NULL, *data = supl.data, *tim_stamp = NULL;
        int   size = 0;
	unsigned int utc_secs = 0;
	char tim_stamp_buf[32];
	bool pos_res_msg = false;

        ptr1 = get_delimiter(supl.data, ',');       /* Skip local session# of SUPL */
        if('\0' == *ptr1) {
                hash = true; data = NULL; vers = NULL;
                goto prep_final;
        }
        
        data = ptr1+ 1;                   /* Do we need to check for *data */
	// If POSITION_RESULT, first element is TIMESTAMP. Fetch it.
        if(0 == strcmp(supl.info, MSG_INFO_POSITION_RESULT)){

		pos_res_msg = true;
		
		tim_stamp = data;
		ptr1 = get_delimiter(data, ',');
		if('\0' == *ptr1) {
                        goto prep_exit1;
                }
                
                *ptr1 = '\0';
                data = ptr1 + 1;

		utc_secs = atoi(tim_stamp);

		get_utc_time_str(utc_secs, tim_stamp_buf);

		ALOGD("POS RESULT MSG: Time stamp = %s", tim_stamp_buf);
	}	
        if(0 == strcmp(supl.info, "OTA_MSG")) {
                vers = data;
                ptr1 = get_delimiter(data, ',');
                if('\0' == *ptr1) {
                        goto prep_exit1;
                }
                
                *ptr1 = '\0';
                data = ptr1 + 1;         /* Do we need to check for *data */
        }
        
        
        if(!is_substr(data, "SUPL_POSINIT") && !is_substr(data, "SUPL_INIT") &&
	   !is_substr(data, "SUPL_START")   && !is_substr(data, "SUPL_POS") &&
	   !is_substr(data, "SUPL_END") && !is_substr(data, "SUPL_RESPONSE")){
                vers = NULL;            /* No version */
        }
                	
	hash = is_hash_required(data);
	if(!hash)
		hash = is_hash_required(supl.info);
        
 prep_final:
        
        if(size < len)
                size += sprintf(buf + size, "[%s]0X%.8X:%d, ", supl.time, 
                                get_msg_info(supl.info),     session_cnt);
	if((size < len) && pos_res_msg )
		size += sprintf(buf + size, "%s, ", tim_stamp_buf);

        if(size < len)
                if(vers) size += sprintf(buf + size, "%s.0,", vers);

        if(size < len)
                if(data) size += sprintf(buf + size, "%s ",  data);

        if(size < len)
                if(hash)  size += sprintf(buf + size, "#%s",  supl.note);

         if(size<len)
                size += sprintf(buf + size, "%s",  "\n");     

        
        ALOGD("prepare_cmcc_msg: NOTE = %s", supl.note);

	return  size;
        
 prep_exit1:
	return 0;
		

}


