#ifndef CMCC_LOG_PARSER_H
#define CMCC_LOG_PARSER_H

#include <string>
#include "cmcc_definitions.h"

struct parsed_message{
	char time[32];
	char type[20];
	char info[25];
	char params[256];
	char details[256];
};

/* The members of structure points to buffers for indicated contents.
   Specifically, this construct enables setting up of substrings upon
   parsing of the LOG information provided by the SUPL stack.

   The format of LOG from SUPL component is:

   <UTC_Time>:<MSG_TYPE>:<MSG_INFO>:<MSG_DATA>:<MSG_NOTE>
*/
struct supl_log {

        char *time;   /* Pointer to Time */
        char *type;   /* Pointer to Type */
        char *info;   /* Pointer to Info */
        char *data;   /* Pointer to Data */
        char *note;   /* Pointer to Note */
};

/* Can this be removed? */
struct msg_info{
	char *pMsg_info_str;
	int  msg_info_value;
};

class cmcc_log_parser{

public:
        cmcc_log_parser(unsigned int);
        ~cmcc_log_parser();

        int session_cnt;
        
public:
       
        int parse_message(char* buf, supl_log& supl);
        int prepare_cmcc_msg(supl_log& supl, char* buf, int len);
        int get_msg_info(const char* msg);

        int is_substr(char* str, char* sample);
        char* buf4next_parse(char *ptr);
        char* get_delimiter(char *buf, char val);
        bool is_hash_required(char *buf);
        int get_utc_time_str(unsigned int utc_sec, char *buf);
};

#endif

