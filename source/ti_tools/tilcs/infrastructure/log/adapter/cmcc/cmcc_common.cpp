


#include <stdio.h>

#include "cmcc_common.h"
#include "nvs_utils.h"

 int
log_time2cmcc_buf(unsigned int secs, unsigned int usecs, char *buf, int len)
{
	struct YMDS_offset ymds = secs2YMDS(secs);
	unsigned int t_hh = (ymds.ss / (60*60));
	unsigned int t_mm = (ymds.ss - (t_hh * 60 *60)) / 60;
	unsigned int t_ss = (ymds.ss % 60);
	int wr_size = 0;
        
        if(wr_size < len)
                wr_size += sprintf(buf,    "[%4d%02d%02d%02d%02d%02d.%03d]",
                                   ymds.Yn + 1970, ymds.Mn + 1, ymds.Dn,
                                   t_hh,  t_mm,  t_ss,  usecs/1000);
        
	return wr_size;
}

 int get_utc_tim_str(unsigned int secs, char *buf, int len)
{
	struct YMDS_offset ymds = secs2YMDS(secs);
	unsigned int t_hh = (ymds.ss / (60*60));
	unsigned int t_mm = (ymds.ss - (t_hh * 60 *60)) / 60;
	unsigned int t_ss = (ymds.ss % 60);
	int wr_size = 0;
        
        if(wr_size < len)
                wr_size += sprintf(buf, "%4d-%02d-%02d %02d:%02d:%02d",
				   ymds.Yn + 1970, ymds.Mn + 1,
				   ymds.Dn, t_hh,  t_mm,  t_ss);
        
	return wr_size;
}
