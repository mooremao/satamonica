
#include "log_id.h"
#include "nvs_utils.h"
#include "cmcc_definitions.h"
#include "Logger_Adapter.h"
#include "cmcc_log_parser.h"
#include "cmcc_common.h"
#include "gnss_lcs_common.h"

#include <utils/Log.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

extern char cmcc_buf[LOG_DATA_LEN];
extern unsigned int session_id;
extern bool auto_loc;
extern bool assist_loc;

static int auto_sess_id = 0;
static int start_secs   = 0;
static int auto_log_fp  = -1;
struct gnss_qop qop;

int open_autonomous_log_file()
{
        auto_log_fp = open(AUTO_LOG_FILE_PATH, O_RDWR);
        if( auto_log_fp<0)
                return -1;

        return 1;
}

int close_autonomous_log_file()
{
        if(auto_log_fp){
                close(auto_log_fp);
                return 1;
        }
        return 0;
}

int pos_res_put2auto_buf(const struct log_buf& log, char *buf, int len)
{
#define UNC_MAP_K_TO_R(unc_k, C, x) (double)(C * (pow(1+x, unc_k) - 1))

        int wr_size = 0;
        char utc_tim_buf[32];
        double unc_axis, unc_per_axis;
        unsigned char unc_angle;
        double latitude = 0, longitude = 0, altitude = 0;
        const struct gnss_pos_result *pos = (struct gnss_pos_result*)log.buffer;
        const struct location_desc&   loc = (struct location_desc)pos->location;

        int end_secs = (log.t_secs*1000) + (log.t_usec/1000);

        if(wr_size < len){
                wr_size += sprintf(buf + wr_size,
                                "0X%.8X:%d, #Session end (Success ",
                                MSG_INFO_SESSION_END_VALUE, auto_sess_id);
        }

        if(wr_size < len){
                get_utc_tim_str(pos->utc_secs, utc_tim_buf, sizeof(utc_tim_buf));
                wr_size += sprintf(buf + wr_size, "Time stamp=%s,", utc_tim_buf);
        }

        unc_axis = UNC_MAP_K_TO_R(loc.unc_semi_maj_K, 10.0f, 0.1f);
        unc_per_axis = UNC_MAP_K_TO_R(loc.unc_semi_min_K, 10.0f, 0.1f);
        unc_angle = loc.orientation;

        if(wr_size < len){
                char *alt_sign = "+";
                if(0 == loc.alt_dir )
                        alt_sign = "+";
                else
                        alt_sign = "-";

                if((loc.lat_deg_by_2p8 == 0)&&(loc.lon_deg_by_2p8 == 0)){

                        latitude   = (double)loc.latitude_N * 90.0f/8388608.0f;
                        if(loc.lat_sign == 1 )
                               latitude = -latitude;

                        longitude  = (double)loc.longitude_N * 360.0f/16777216.0f;
                        altitude   = (double)0;
                }else{
                        latitude = (double) loc.lat_deg_by_2p8 * (180 / pow(2.0, 32));
                        longitude = (double) loc.lon_deg_by_2p8 * (180 / pow(2.0,31));
                        altitude   = (double)loc.altitude_N;
                }

                wr_size += sprintf(buf + wr_size,
                                        "Lat=%lf, Long=%lf, Altitude=%s%lf, ",
                                        latitude,
                                        longitude,
                                        alt_sign,
                                        altitude);
        }
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size,
                                "GPS Week= %d, GPS Time= %u ms, ",
                                pos->ref_secs / MAX_SECS_IN_1WEEK,
                                pos->ref_msec);
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size,
                                "TTFF= %d ms, RSP Time= %u ms, ",
                                (end_secs - start_secs), qop.rsp_secs);
        if(wr_size < len)
                wr_size += sprintf(buf + wr_size,"Loc Unc Axis = %lf m, ",
                                unc_axis );

       if(wr_size < len)
                wr_size += sprintf(buf + wr_size,
                                "Loc Unc Perpendicular Axis = %lf m, ",
                                unc_per_axis );

        if(wr_size < len)
                wr_size += sprintf(buf + wr_size,"Unc Angle = %d deg)\n",
                                unc_angle );

        return wr_size;
}


int sess_start_put2auto_buf(const struct log_buf& log, char *buf, int len)
{
        int wr_size = 0;

        start_secs = (log.t_secs*1000) + (log.t_usec/1000);

        if(wr_size < len){
                wr_size+= sprintf(buf+wr_size,"0X%.8X:%d, #Session starts\n",
                                MSG_INFO_SESSION_START_VALUE, ++auto_sess_id);
        }

        return wr_size;
}

static int write_to_file(char* buf, int len){

        if(auto_log_fp && len){
                write(auto_log_fp,(void*)buf,len);
                return SUCCESS;
        }

        return FAIL;
}

static void update_qop(const struct log_buf& log)
{
        struct gnss_qop  *pqop = (struct gnss_qop *)log.buffer;

        qop.rsp_secs = pqop->rsp_secs;
        qop.acc_sel  = pqop->acc_sel;
        qop.h_acc_M  = pqop->h_acc_M;
        qop.max_age  = pqop->max_age;
        qop.v_acc_M  = pqop->v_acc_M;
        return ;
}

int log_autonomous_message(const struct log_buf& log)
{
        memset(cmcc_buf, 0 ,sizeof(cmcc_buf));

        bool end_sess = false;
        int   wr_size = log_time2cmcc_buf(log.t_secs, log.t_usec, 
                                          cmcc_buf, LOG_DATA_LEN);
        switch(log.u.bin_id) {
         case PFM_APP_LOGB_ID_H_START:
                if( true == auto_loc )
                         wr_size = 0;
                else{
                        wr_size  += sess_start_put2auto_buf(log, cmcc_buf + wr_size,
                                                LOG_DATA_LEN  - wr_size);
                        auto_loc  = true;
                }
                break;
        case PFM_APP_LOGB_ID_POS_RESULT:
                wr_size  += pos_res_put2auto_buf(log, cmcc_buf + wr_size,
                                                 LOG_DATA_LEN  - wr_size );
                end_sess = true; break;
        case PFM_APP_LOGB_ID_QOP:
                update_qop(log); wr_size = 0; break;
        case PFM_APP_LOGB_ID_LOC_REQ:
        case PFM_APP_LOGB_ID_LOC_END:
                default:
                wr_size = 0;  break;
        }
        if(wr_size && auto_loc ) {
                if(open_autonomous_log_file()){
                        ALOGD("CMCC_BIN_AUTO_OUT: %s",cmcc_buf);
                        write_to_file(cmcc_buf, wr_size);
                        close_autonomous_log_file();
                 }
        }

        if(true == end_sess) auto_loc = false;

        return wr_size;
}


int cmcc_log_bin_autonomous(const struct log_buf& log)
{
        log_autonomous_message(log);

        return 1;
}


