/*
  Add License Header 
*/

#include "Logger_Adapter.h"

#include "nvs_utils.h"
#include <utils/Log.h>

#include "cmcc_logger_adapter.h"
#include "Logger.h"

extern int cmcc_adapter_binary_handler(const struct log_buf& log);
extern int cmcc_adapter_string_handler(const struct log_buf& log);

static int _generic_log_handler(Logger& logger, const log_buf& log)
{
        if(0 == strcmp(log.prefix, "OPER"))
                return 0;

        ALOGD("[%s] %s", ti_log_name(log.source), log.buffer); 
        
        return 0;
}

static int connect_log_handler(Logger& logger, const log_buf& log)
{
        if(log.rank_n > 4) return 0; // In this version print L1 & L2 logs

        ALOGD("[%s][%u.%u][%s] %s:%d:%s ", ti_log_name(log.source), log.t_secs,
              log.t_usec, log.prefix, log.fnc_nm, log.f_line, log.buffer);
 
        return 0;
}

static int d_proxy_log_handler(Logger& logger, const log_buf& log)
{
        return _generic_log_handler(logger, log);
}

static int pfm_app_log_handler(Logger& logger, const log_buf& log)
{
        return _generic_log_handler(logger, log);
}

static int all_str_log_handler(const struct log_buf& log)
{
	if(0 != strcmp(log.prefix, "OPER"))
		return 0;

	return cmcc_adapter_string_handler(log);
}

static int all_bin_log_handler(const struct log_buf& log)
{
	if(0 != strcmp(log.prefix, "OPER"))
		return 0;

	return cmcc_adapter_binary_handler(log);
}

int log_adapter_load(struct log_adapter_ops& ops)
{
        ops.supl_2x_log_handler = NULL;
        ops.c_plane_log_handler = NULL;
        ops.pfm_app_log_handler = pfm_app_log_handler;
        ops.connect_log_handler = connect_log_handler;
        ops.d_proxy_log_handler = d_proxy_log_handler;
                
        ops.all_bin_log_handler = all_bin_log_handler;
        ops.all_str_log_handler = all_str_log_handler;

        return 0;
}
