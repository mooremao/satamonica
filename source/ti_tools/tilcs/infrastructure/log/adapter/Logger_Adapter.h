/*
  Add License Header 
*/

#ifndef __LOGGER_ADAPTER_H__
#define __LOGGER_ADAPTER_H__

#include "Logger.h"
#include "Log_MD.h"

struct log_adapter_ops {

        plugin_logs_fn  connect_log_handler;
        plugin_logs_fn  d_proxy_log_handler;
        plugin_logs_fn  supl_2x_log_handler;
        plugin_logs_fn  c_plane_log_handler;
        plugin_logs_fn  pfm_app_log_handler;

        plugin_type_fn  all_bin_log_handler;
        plugin_type_fn  all_str_log_handler;
};

int log_adapter_load(struct log_adapter_ops& ops);


#endif
