
/*
  Add License Header 
*/

#include <stdio.h>
#include <sys/time.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>


#include "ti_log.h"
#include "log_id.h"
#include "gnss.h"
#include "os_services.h"
#ifdef ANDROID
#include <utils/Log.h>

#define LOGD ALOGD
#else
#define LOGD printf

#endif
static unsigned int socket_fd = 0;
static enum log_source source_log;
static pthread_mutex_t log_mtx = PTHREAD_MUTEX_INITIALIZER;

static struct log_buf log_buf; /* A protected single instance for a given process */

static int os_time(unsigned int *secs, unsigned int *usec)
{
        struct timeval tv;

        if(gettimeofday(&tv, NULL) == -1){
        //        exit(-1);
        	
        	exit(EXIT_FAILURE);
        }

        *secs = tv.tv_sec;
        *usec = tv.tv_usec;

        return 0;
}

int  ti_log_init(const enum log_source source)
{
        socket_fd  = ti_log_udp_sock_init(NULL);
        source_log = source;

        return socket_fd;
}

void ti_log_buf_print(struct log_buf *log)
{

}

void ti_log_binary(const char *prefix,  const char *fnc_nm,  int f_line, 
                   int rank_n, unsigned int bin_id, unsigned int n_objs,    
                   void *binary)
{
        struct logb_info *logb = get_logb_info(bin_id);
        if(NULL == logb) {
                return;
        }
        
        /* Take Mutex */
        pthread_mutex_lock(&log_mtx);

        /* Set up the Log Buffer */
        log_buf.source   = source_log; 

        os_time(&log_buf.t_secs, &log_buf.t_usec);

        strncpy(log_buf.fnc_nm, fnc_nm,  LOG_NAME_LEN - 1);
        strncpy(log_buf.prefix, prefix,  LOG_NAME_LEN - 1);
        
        log_buf.rank_n     = rank_n;
        log_buf.f_line     = f_line;
        log_buf.l_type     = e_log_binary;
        log_buf.u.bin_id   = bin_id;
        log_buf.length     = logb->bin_sz *  n_objs;

        if (NULL != binary)
               memcpy(log_buf.buffer, binary, log_buf.length);

        ti_log_buf_print(&log_buf);
        
        ti_log_udp_send(socket_fd, TI_LOG_SERVER_NET_PATH, 
                        &log_buf, LOG_NW_SIZEOF(log_buf));

        /* Give Mutex */
        pthread_mutex_unlock(&log_mtx);

        return;
}

void ti_log_string(const char *prefix, const char *fnc_nm, int f_line,
                   int rank_n, const char *format,   ...)

{
	va_list ap;

        /* Take Mutex */
        pthread_mutex_lock(&log_mtx);

	log_buf.source  = source_log; 

        os_time(&log_buf.t_secs, &log_buf.t_usec);

	strncpy(log_buf.fnc_nm, fnc_nm,  LOG_NAME_LEN - 1);
	strncpy(log_buf.prefix, prefix,  LOG_NAME_LEN - 1);

	log_buf.rank_n  = rank_n;

	log_buf.l_type  = e_log_string;
        log_buf.f_line  = f_line;

	va_start(ap, format);
	log_buf.length = vsnprintf(log_buf.buffer,    sizeof(log_buf.buffer), 
                                   format, ap) + 1  /* For NULL character */;
        va_end(ap);
	
	ti_log_udp_send(socket_fd, TI_LOG_SERVER_NET_PATH, 
                        &log_buf, LOG_NW_SIZEOF(log_buf));
        
        /* Give Mutex */
	pthread_mutex_unlock(&log_mtx);
}

void ti_log_exit()
{
	ti_log_udp_sock_exit(socket_fd);
	return;
}

const char *ti_log_name(enum log_source src_id)
{
        static const char connect[] = "CONNECT";
        static const char d_proxy[] = "D-PROXY";
        static const char supl_2x[] = "SUPL-2x";
        static const char c_plane[] = "C-PLANE";
        static const char pfm_app[] = "PFM-APP";
        static const char unknown[] = "UNKNOWN";

        const char *ret_val;

        switch(src_id) {

        case e_log_connect: ret_val = connect; break;
        case e_log_d_proxy: ret_val = d_proxy; break;
        case e_log_supl_2x: ret_val = supl_2x; break;
        case e_log_c_plane: ret_val = c_plane; break;
        case e_log_pfm_app: ret_val = pfm_app; break;
        default:            ret_val = unknown; break;

        }

        return ret_val;
}
