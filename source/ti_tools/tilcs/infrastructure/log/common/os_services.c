/*
 * os_services.c
 *
 * OS specific procedures
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <math.h>
#include <sys/time.h>
#include "logger.h"

#include "os_services.h"
#include "utils.h"
#ifdef ANDROID
#include <utils/Log.h>

#define LOGD ALOGD
#else
#define LOGD printf
#endif
#define MAX_OS_TIMER 4



static struct os_timer_desc  os_timer_array2use[MAX_OS_TIMER];
static struct os_timer_desc *free_os_timer_list = NULL;

static pthread_mutex_t os_timer_mtx;
struct os_mutex {

	pthread_mutex_t  mutex;
	struct os_mutex *next;
};

#define MAX_OS_MUTEXES 32

static struct os_mutex *os_mutex_free_list = NULL;
static struct os_mutex os_mutex_array[MAX_OS_MUTEXES] = {
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL},
	{PTHREAD_MUTEX_INITIALIZER, NULL}
};

//static void *osal_timer_sched(unsigned int tout_secs, unsigned int intv_secs, os_timer_cb cb_fn, void *param);

static void  osal_timer_abort(void *timer_hnd);

static void os_timer_handler_thread(union sigval val);

static void os_mutex_list_init(void)
{
	struct os_mutex *mutex = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_MUTEXES; i++) {
		mutex = os_mutex_array + i;

		ADD_LIST_ATOMIC(NULL, mutex, os_mutex_free_list);
	}
}

void* os_mutex_init(void)
{
	struct os_mutex *mutex = NULL;

	POP_LIST_ATOMIC(NULL, os_mutex_free_list, mutex);

	return mutex;
}

int os_mutex_take(void *mtx_hnd)
{
	int ret_val;
	struct os_mutex *mutex = (struct os_mutex *)mtx_hnd;

	if (mutex) {
		ret_val = pthread_mutex_lock(&mutex->mutex);
		if (ret_val != 0) {
			//LOGPRINT(os, err, "pthread_mutex_lock() failed: %s",
			//				strerror(ret_val));
			exit(EXIT_FAILURE);
		}

		return 0;
	} else {
		//LOGPRINT(os, err, "invalid pointer");
		exit(EXIT_FAILURE);
	}
}

int os_mutex_give(void *mtx_hnd)
{
	int ret_val;
	struct os_mutex *mutex = (struct os_mutex *)mtx_hnd;

	if (mutex) {
		ret_val = pthread_mutex_unlock(&mutex->mutex);
		if (ret_val != 0) {
			//LOGPRINT(os, err, "pthread_mutex_unlock() failed: %s",
			//				strerror(ret_val));
			exit(EXIT_FAILURE);
		}

		return 0;
	} else{
		//LOGPRINT(os, err, "invalid pointer");
		exit(EXIT_FAILURE);
	}
}

void os_mutex_exit(void *mtx_hnd)
{
	struct os_mutex *mutex = (struct os_mutex *)mtx_hnd;

	ADD_LIST_ATOMIC(NULL, mutex, os_mutex_free_list);
}

static void os_mutex_list_exit(void)
{
	struct os_mutex *mutex = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_MUTEXES && os_mutex_free_list; i++) {

		mutex = os_mutex_free_list;
		os_mutex_free_list = os_mutex_free_list->next;
		mutex->next = NULL;
	}
}

struct os_sem {

	sem_t          sem;
	struct os_sem *next;
};

#define MAX_OS_SEMAPHORES 20

static struct os_sem *os_sem_free_list = NULL;
static struct os_sem os_sem_array[MAX_OS_SEMAPHORES];

static void os_sem_list_init(void)
{
	struct os_sem *sem = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_SEMAPHORES; i++) {
		sem = os_sem_array + i;

		sem_init(&sem->sem, 0, 0);

		ADD_LIST_ATOMIC(NULL, sem, os_sem_free_list);
	}
}

void* os_sem_init(void)
{
	struct os_sem *sem = NULL;

	POP_LIST_ATOMIC(NULL, os_sem_free_list, sem);

	return sem;
}

int os_sem_take(void *sem_hnd)
{
	int ret_val;
	struct os_sem *sem = (struct os_sem *)sem_hnd;

	if (sem) {
		ret_val = sem_wait(&sem->sem);
		if (ret_val != 0) {
			exit(EXIT_FAILURE);
		}

		return 0;
	} else {
		exit(EXIT_FAILURE);
	}
}

int os_sem_give(void *sem_hnd)
{
	int ret_val;
	struct os_sem *sem = (struct os_sem *)sem_hnd;

	if (sem) {
		ret_val = sem_post(&sem->sem);
		if (ret_val != 0) {
			exit(EXIT_FAILURE);
		}

		return 0;
	} else {
		
		exit(EXIT_FAILURE);
	}
}

void os_sem_exit(void *sem_hnd)
{
	struct os_sem *sem = (struct os_sem *)sem_hnd;

	ADD_LIST_ATOMIC(NULL, sem, os_sem_free_list);
}

static void os_sem_list_exit(void)
{
	struct os_sem *sem = NULL;
	int i = 0;

	for(i = 0; i < MAX_OS_SEMAPHORES && os_sem_free_list; i++) {

		sem = os_sem_free_list;
		os_sem_free_list = os_sem_free_list->next;
		sem->next = NULL;
	}
}

unsigned int os_time_msecs()
{
	struct timeval tv;

	if(gettimeofday(&tv, NULL) == -1){
		exit(EXIT_FAILURE);
	}

	return ((tv.tv_sec * 1000) + (tv.tv_usec/1000)); 
}
