#include "log_socket.h"




int ti_log_udp_sock_init(const char* sock_path)
{
	int sock_fd;
	socklen_t sock_len;
	struct sockaddr_un sock_addr;
	memset(&sock_addr, 0, sizeof(sock_addr));

	/* Create a Unix Domain Socket */
	if ((sock_fd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
		LOGD("Failed to create socket %d\n ", errno);
		return -1;
	}

        if(NULL == sock_path) {
			LOGD("Any socket will do %d", sock_fd);
			return sock_fd; /* Any socket will do */
        }

        if(108 < (strlen(sock_path) + 1)) goto sock_init_err;
	
	sock_addr.sun_family = AF_UNIX; /* Internet IP */
        strcpy(sock_addr.sun_path, sock_path);
	
	unlink(sock_addr.sun_path);
	sock_len = strlen(sock_addr.sun_path) + sizeof(sock_addr.sun_family);
	
	if(bind(sock_fd, (struct sockaddr *)&sock_addr, sock_len) < 0) {
		LOGD("Failed to bind socket on path %s\n", sock_path);
                goto sock_init_err;
	}

	if(chmod(sock_path, 0777) < 0) {
		LOGD("Failed to change permission on socket path %s\n", sock_path);
	}

    LOGD("Sock created ID: %d & bound on path %s\n", sock_fd, sock_path);


	return sock_fd;

sock_init_err:
        close(sock_fd);
        return -1;
}


int ti_log_udp_sock_exit(int fd)
{
	if(fd > 0){
		if(close(fd) < 0){
			return -1;
		}
	}

	return 0;
}


int ti_log_udp_send(int fd, const char *dst_path, char *data, int len)
{
	struct sockaddr_un sock_addr;
	int length = 0;
	memset(&sock_addr,0,sizeof(sock_addr));

	sock_addr.sun_family = AF_UNIX; /* Unix domain socket */
	if((strlen(dst_path)+1) < 108)
		strcpy(sock_addr.sun_path, dst_path);
	else
		return -1;
        
    //LOGD("socket bigens fd: %d len: %d dst_path: %s", fd, len, dst_path); 
        
	/* send over unix path */
	if((length = sendto(fd, data, len, 0, (struct sockaddr *)       
                            &sock_addr, sizeof(sock_addr))) < 0)  {
		//LOGD("Failed to send message: length %d, err = %d , %s", length, errno, dst_path);
		return -1;
	}
        
	return length;
        
}

int ti_log_udp_recv(int fd, char *data, int len)
{
	struct sockaddr_un sock_dst_addr;
	int length=0;
	socklen_t addr_len = sizeof(sock_dst_addr);

    //LOGD("receive message started in socket: fd %d len %d\n", fd, len);
		
	/* Receive over Unix path */
	if((length = recvfrom(fd, data, len, 0, (struct sockaddr*)&sock_dst_addr,
                               &addr_len)) < 0)  {
		LOGD("Failed to receive message: length %d\n", length);
		return -1;
	}

    //LOGD("receive message success in socket: length %d\n", length);
	
	return length;
}

