#ifndef _LOG_SOCKET_H_
#define _LOG_SOCKET_H_


#include "log_socket.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#ifdef ANDROID
#include <utils/Log.h>
#endif
#include <errno.h>
#ifdef __cplusplus
	extern "C" {
#endif 

#ifdef ANDROID
#define LOGD ALOGD
#else
#define LOGD printf

#endif

int ti_log_udp_sock_init(const char* sock_path);
int ti_log_udp_sock_exit(int fd);
int ti_log_udp_send(int fd, const char *dst_path, char *data, int len);
int ti_log_udp_recv(int fd, char *data, int len);

#ifdef __cplusplus
	}
#endif 

#endif // _LOG_SOCKET_H_


