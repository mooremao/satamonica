/*
  Add License Header 
*/

#include <iostream>
#include <stdio.h>

#include "log_socket.h"
#include "Log_MD.h"
#include "log_id.h"


Log_MD* Log_MD :: instance = NULL;

/*--------------------------------------------------------------------------
 * Private Methods
 *--------------------------------------------------------------------------
 */
static int str_fprintf(const struct log_buf& log, FILE *fp)
{
        int rv = -1;

        if(e_log_string != log.l_type) {
                return rv;
        }
        
        /*fprintf(fp, "[%u.%u] [%s] %s:%s:%d: %s.\n", log.t_secs, log.t_usec,
                log.prefix, CNAME(log), log.fnc_nm, log.f_line, log.buffer); */
        
        rv = fprintf(fp, "[%u.%u] [%s] %s:%s:%d: %s.\n", log.t_secs, log.t_usec,
                     log.prefix, log.f_name, log.fnc_nm, log.f_line, log.buffer);
        
	fflush(fp);

        return rv;
}        

static char buffer[LOG_DATA_LEN];  /* There is just one thread in Log_MD */

static int bin_fprintf(const struct log_buf& log, FILE *fp)
{
        if(e_log_binary != log.l_type) {
                return -1;
        }

#if  0
        /* Need to implement following function */
        if(0 <= log_get_bin2buf(log.u.bin_id, log.buffer, buffer, LOG_DATA_LEN))
                return;

        fprintf(fp, "[%u.%u] [%s] %s:%s:%d: %s.\n",  log.t_secs, log.t_usec,
                log.prefix, CNMAME(log), log.fnc_nm, log.f_line,     buffer);

        fflush(fp);
#endif

        return 0;
}

static int ftr_fprintf(const struct log_buf& log, FILE *fp)
{
        return 0;
}

void Log_MD :: default_log_binary(const struct log_buf& log)
{
        /* Add code for wrap around */
        bin_fprintf(log, bin_file);
}

void Log_MD :: default_log_string(const struct log_buf& log)
{
        /* Add code for wrap around */
        str_fprintf(log, str_file);
}

void Log_MD :: log_binary(const struct log_buf& log)
{
        if(NULL !=  adapter_log_binary)
                adapter_log_binary(log);
        else 
                default_log_binary(log);

        return;
}

void Log_MD :: log_string(const struct log_buf& log)
{
        if(NULL !=  adapter_log_string)
                adapter_log_string(log);
        else
                default_log_string(log);

        return;
}

Log_MD  :: Log_MD()
        : loggers(), bin_file(NULL),str_file(NULL), adapter_log_binary(NULL), 
        adapter_log_string(NULL)
{

}

/*--------------------------------------------------------------------------
 * Public Methods
 *--------------------------------------------------------------------------
 */

Log_MD *Log_MD :: create_MD(void)
{
        if(NULL != instance) {
                return instance;
        } 

        instance =  new Log_MD(); 
        if(NULL == instance) {
                goto create_MD_err1;
        }

#if  0  /* To be enhanced */

        /* Need to enhance it for wrap around */
        str_file = fopen("log_MD.idebug", "w+");
        if(NULL == str_file) {
                goto create_MD_err2;
        }

        /* Need to enhance it for wrap around */
        bin_file = fopen("log_MD.iomesg", "w+");
        if(NULL == bin_file) {
                goto create_MD_err3;
        }
#endif

        return instance;

#if  0

 create_MD_err3:
        fclose(str_file);
        
 create_MD_err2:
        delete instance;
#endif

 create_MD_err1:
        return NULL;
}

int Log_MD :: log_fprintf(const char *buf, FILE *fp)
{
        int ret_val = fprintf(fp, "%s\n", buf);
        fflush(fp);

        return ret_val;
}

int Log_MD :: log_fprintf(const struct log_buf& log, FILE *fp)
{
        int ret_val = -1;

        switch(log.l_type) {

        case e_log_string: ret_val = str_fprintf(log, fp); break;
        case e_log_binary: ret_val = bin_fprintf(log, fp); break;
        case e_log_ftrace: ret_val = ftr_fprintf(log, fp); break;
        default:           ret_val = -1;                   break;

        }

        return ret_val;;
}

int Log_MD :: add_logger(Logger *logger)
{
        list<Logger *>::iterator itr;
        for(itr = loggers.begin(); itr != loggers.end(); itr++) {
                if((*itr) == logger)
                        return -1;          /* Already added */
        }

        loggers.push_back(logger);

        return 0;
}

static struct log_buf log_b; /* Safe for single thread and singleton Log MD */

int  Log_MD :: run(int socket_fd)
{
        char *pbuf_len = NULL; 
        char *pbuf_data = NULL; 
        //static struct log_buf log;
        unsigned int len;

        do {
		log_b.length = LOG_DATA_LEN;
                ti_log_udp_recv(socket_fd, (char *)&log_b, LOG_NW_SIZEOF(log_b));
                
                if(e_log_binary == log_b.l_type) log_binary(log_b);/* Process bin */
                
                if(e_log_string == log_b.l_type) log_string(log_b);/* Process str */
                
                list<Logger *>::iterator itr;    /* Process for sender source */
                for(itr = loggers.begin(); itr != loggers.end(); itr++) {
                        Logger *logger = (*itr);
                        if(log_b.source == logger->get_source()) {
                                logger->log_handler(log_b);
                                break;
                        }
                }
               
        } while(1);
        
        return 0;
}

