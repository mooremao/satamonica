/*
  Add License Header 

  LOG Managing Director: Log_MD
*/

#ifndef __LOG_MD_H__
#define __LOG_MD_H__

#include "Logger.h"
#include <list>

using namespace std;

/* Returns number of Bytes added or processed, -1 on error */
typedef int (*plugin_type_fn)(const struct log_buf& log);

class Log_MD {
        
private:
        
        list<Logger*>    loggers;              /* List of Loggers in the system */

        FILE             *bin_file;            /* Files for default BIN logging */        
        FILE             *str_file;            /* Files for default STR logging */

        plugin_type_fn   adapter_log_binary;   /* Plug-in for custom binary LOG */
        plugin_type_fn   adapter_log_string;   /* Plug-in for custom string LOG */
        
        void             default_log_binary(const struct log_buf& log);
        void             default_log_string(const struct log_buf& log);

        void             log_binary(const struct log_buf& log);
        void             log_string(const struct log_buf& log);

        static Log_MD    *instance;

        Log_MD();                             /* Default constructor */

public:	
       static Log_MD *get_instance() { return instance; }
       
       static Log_MD *create_MD();
       
       void plugin_binary(plugin_type_fn log_binary) {
               adapter_log_binary = log_binary;
       }
       
       void plugin_string(plugin_type_fn log_string) {
               adapter_log_string = log_string;
       }
       
       int add_logger(Logger *logger);           /* Returns: -1 on error else 0 */
       
       int rem_logger(Logger *logger);           /* Returns: -1 on error else 0 */
       
       int log_fprintf(const char *buf, FILE *fp);          /* Returns # Bytes */
       
       int log_fprintf(const struct log_buf& log, FILE *fp);/* Returns # Bytes */
       
       int run(int socket_fd);                              /* Blocking Call   */
};

#endif
