/*
  Add License Header 
*/

#include <iostream>
#include "Logger_Adapter.h"

#include "Log_MD.h"
#include "log_socket.h"
#include "log_id.h"
#ifdef ANDROID
#include <utils/Log.h>
#endif
using namespace std;

static Logger *try_new_Logger_obj(enum log_source source, const char *dbg_name)
{
        Logger *logger = new Logger(source);
        if(NULL == logger) {
                cout << dbg_name << ": memory alloc failed, exiting ..." << endl; 
             //   exit(-1);
             
             exit(EXIT_FAILURE);
        }

        return logger;
}

int main()
{
        int socket_fd = 0;
        Log_MD *MD    = Log_MD :: create_MD();
        if(NULL == MD) {
                cout << "Log  MD: Failed to allocate memory exiting ...." << endl; 
               // exit(-1);
               exit(EXIT_FAILURE);
        }

        socket_fd = ti_log_udp_sock_init(TI_LOG_SERVER_NET_PATH);
        cout << "Socket fd in server  " << socket_fd << endl;
        if(socket_fd == -1 ){
                cout << "Error in Creating socket...." << endl;
                return -1;
        }
        
        Logger *connect = try_new_Logger_obj(e_log_connect, "connect");  
        Logger *d_proxy = try_new_Logger_obj(e_log_d_proxy, "d_proxy");  
        Logger *supl_2x = try_new_Logger_obj(e_log_supl_2x, "supl_2x");
        Logger *c_plane = try_new_Logger_obj(e_log_c_plane, "c_plane");
        Logger *pfm_app = try_new_Logger_obj(e_log_pfm_app, "pfm_app");

        /* All required Loggers created, let the MD know about them */
        MD->add_logger(d_proxy);
        MD->add_logger(connect);
        MD->add_logger(supl_2x);
        MD->add_logger(c_plane);
        MD->add_logger(pfm_app);

        log_adapter_ops   ops;
        log_adapter_load(ops);/* Load implementation specific ops */
        
        connect->add_plugin(ops.connect_log_handler);
        d_proxy->add_plugin(ops.d_proxy_log_handler);
        supl_2x->add_plugin(ops.supl_2x_log_handler);
        c_plane->add_plugin(ops.c_plane_log_handler);
        pfm_app->add_plugin(ops.pfm_app_log_handler);

        MD->plugin_binary(ops.all_bin_log_handler);
        MD->plugin_string(ops.all_str_log_handler);
        
        MD->run(socket_fd);

        cout << "Exiting Log MD ...." << endl;
	return 0;
}
