/*
  Add License Header 
*/

#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "ti_log.h"
#include "log_id.h"
#include <stdio.h>

#define DATA_LEN 8092
#define NAME_LEN 64

using namespace std;

struct lfile_desc {        /* Descriptor of the LOG FILE */

        int       rank;    /* Level of criticality LOGs to be written */
        FILE     *file;    /* Handle to  file to be used  for Logging */
        int       n_wr;    /* Wrap around: max num of lines permitted */

        lfile_desc() : rank(0x7FFFFFFF), file(NULL), n_wr(0)
        {

        }
};

class Logger; /* Forward declaration */

/*  Returns -1 on error else the number of bytes processed or added. */
typedef int (*plugin_logs_fn)(Logger& logger, const log_buf& log);

class Logger {

 private:

        log_source           source;

        plugin_logs_fn       adapter_log_handler;

#define MAX_LFILE    3
        struct lfile_desc    lf_info[MAX_LFILE]; 

private:
        
        int               _log_process(const struct log_buf& log);
        struct lfile_desc* get_lf_info(const struct log_buf& log);
        
 public:

        Logger(enum log_source src);       /* Constructor */
        
        void    set_bin_level(int level);  /* Set criticality level for BIN LOG */
        void    set_str_level(int level);  /* Set criticality level for STR LOG */
        void    set_ftr_level(int level);  /* Set criticality level for FTR LOG */
        
        /* All log_fprintf methods:
           Returns -1 on error else the number of bytes processed or added. 
        */
        int     log_fprintf(const struct log_buf& log);          // Default file
        int     log_fprintf(const struct log_buf& log, FILE *fp);

        int     log_fprintf(const char *buf, enum log_source source); // Default
        int     log_fprintf(const char *buf, FILE *fp);

        /* For given Log type and associated level of criticality configured, 
           the method provides default processing of Logs through the LOG MD.
           Retuns -1 on error else the number of bytes processed or added.
        */
	int     log_process(const struct log_buf& log);

        /* Uses plug-in, if provisioned else goes for (default) processing.
           Refer to the method log_process for  note on default processing.
           Returns -1 on error else the number of bytes processed or added. 
        */
        int     log_handler(const struct log_buf& log); 
        
        void    add_plugin(plugin_logs_fn handler) { 
                adapter_log_handler = handler; 
        }

        enum log_source get_source(void) { return source; }
};

#endif
