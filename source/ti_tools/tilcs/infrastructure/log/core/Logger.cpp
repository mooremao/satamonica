/*
  Add License Header 
*/

#include "Logger.h"
#include "Log_MD.h"
#include "log_id.h"
#include <stdio.h>

#define STR_INDEX    0
#define BIN_INDEX   (STR_INDEX + 1)
#define FTR_INDEX   (BIN_INDEX + 1)

#if(MAX_LFILE < (FTR_INDEX + 1))
#error "Fix MAX_LFILE such that this condition is not met"
#endif


struct lfile_desc* Logger :: get_lf_info(const struct log_buf& log)
{
        struct lfile_desc *lf = NULL;

        switch(log.l_type) {
                
        case e_log_string: lf = lf_info + STR_INDEX; break;
        case e_log_binary: lf = lf_info + BIN_INDEX; break;
        default:                                     break;
                
        }

        return lf;
}

Logger :: Logger(enum log_source src) 
        : source(src), adapter_log_handler(NULL) 
{
        
}

int Logger :: _log_process(const struct log_buf& log)
{
        struct lfile_desc *lf = get_lf_info(log);

        if(lf && lf->file && (log.rank_n > lf->rank)) {
                Log_MD *MD = Log_MD::get_instance();
                return MD->log_fprintf(log, lf->file);
        }

        return 0;
}

void Logger :: set_str_level(int level) { lf_info[STR_INDEX].rank = level; }
void Logger :: set_bin_level(int level) { lf_info[BIN_INDEX].rank = level; }
void Logger :: set_ftr_level(int level) { lf_info[FTR_INDEX].rank = level; }


int Logger :: log_handler(const struct log_buf& log)
{
        return (NULL != adapter_log_handler) ? 
                adapter_log_handler(*this , log) : _log_process(log);
}

int Logger :: log_process(const struct log_buf& log)
{
        return _log_process(log);
}

int Logger :: log_fprintf(const struct log_buf& log)
{
        struct lfile_desc *lf = get_lf_info(log);
        
        if(lf && lf->file) {
                Log_MD *MD = Log_MD :: get_instance();
                return MD->log_fprintf(log, lf->file);
        }

        return 0;
}

int Logger :: log_fprintf(const struct log_buf& log, FILE *fp)
{
        Log_MD *MD = Log_MD :: get_instance();
        return MD->log_fprintf(log, fp);
}
