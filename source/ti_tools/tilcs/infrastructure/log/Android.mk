LOCAL_PATH             := $(call my-dir)
#######################libclientlogger##########################
include $(CLEAR_VARS)

LOCAL_MODULE           := libclientlogger
LOCAL_SHARED_LIBRARIES := libcutils liblog libc libstdc++
LOCAL_LDLIBS           += -lm -lpthread

LOCAL_SRC_FILES        := common/log_socket.c	\
						common/os_services.c	\
						common/ti_log.c

LOCAL_C_INCLUDES       :=                  	\
	$(LOCAL_PATH)/include/	\
	$(LOCAL_PATH)/common/	\
	$(LOCAL_PATH)/../../include/common/		\
	$(LOCAL_PATH)/../../dproxy/include/		\
	external/stlport/stlport		\
	bionic

LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_SHARED_LIBRARY)

#######################Log_MD############################
include $(CLEAR_VARS)
LOCAL_MODULE           := Log_MD
LOCAL_LDFLAGS          += -g
LOCAL_SHARED_LIBRARIES := libstdc++ libcutils libagnss libstlport libdevproxy libclientlogger
	
LOCAL_LDLIBS           += -lm -lpthread
LOCAL_SRC_FILES        := 	core/Main.cpp		\
							core/Logger.cpp		\
							core/Log_MD.cpp		\
							adapter/cmcc/cmcc_logger_adapter.cpp	\
							adapter/cmcc/cmcc_log_parser.cpp		\
							adapter/default/Default_Adapter.cpp		\
							adapter/cmcc/autonomous_loggger_adapter.cpp	\
							adapter/cmcc/cmcc_common.cpp	\
							adapter/nvs_utils.c
							
LOCAL_C_INCLUDES       :=                        	\
	$(LOCAL_PATH)/../../include/common/		\
	$(LOCAL_PATH)/../../include/connect/			\
	$(LOCAL_PATH)/../../include/dproxy/			\
	$(LOCAL_PATH)/../../connect/common/		\
	$(LOCAL_PATH)/../../connect/debug/		\
	$(LOCAL_PATH)/core/		\
	$(LOCAL_PATH)/adapter/		\
	$(LOCAL_PATH)/common/		\
	$(LOCAL_PATH)/include/			\
	$(LOCAL_PATH)/adapter/cmcc/		\
	$(LOCAL_PATH)/adapter/dproxy/		\
	$(LOCAL_PATH)/					\
	external/stlport/stlport			\
	bionic
	
LOCAL_MODULE_TAGS      := optional eng debug
LOCAL_PRELINK_MODULE   := false
include $(BUILD_EXECUTABLE)
