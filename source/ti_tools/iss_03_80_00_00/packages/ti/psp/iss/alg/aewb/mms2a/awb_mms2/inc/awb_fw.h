

/* =============================================================================
* MultiMedia Solutions Ltd.
* (c) Copyright 2006-2008, MultiMedia Solutions Ltd. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
* @file main.c
*
* Simulator for MMS AWB algorithm.
*
* ^path (TOP)
*
* @author Dessislava Tzoneva (MultiMedia Solutions Ltd.)
*
* @date 12.06.2008
*
* @version 1.00
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 09-Sept-2008 : Dessislava Tzoneva (MultiMedia Solutions Ltd.)
*! Created
* =========================================================================== */

#ifndef __AWB_FW_H__
#define __AWB_FW_H__

#include <iawb.h>
#include <iaewb.h>

//#define ALG_AWB_DEBUG

#define     AWB_FW_ERROR    1
#define     AWB_FW_SUCCESS  0

enum awb_mode
{
    AWB_WB_MODE_AUTO = 0,       /* Auto detect */
    AWB_WB_MODE_MANUAL,         /* Manual mode */
    AWB_WB_MODE_DAYLIGHT,       /* Daylight mode */
    AWB_WB_MODE_SUNSET,         /* Sunset mode */
    AWB_WB_MODE_CLOUDY,         /* Cloudy mode */
    AWB_WB_MODE_TUNGSTEN,       /* Tungsten mode */
    AWB_WB_MODE_FLUORESCENT,    /* Fluorescent mode */
    AWB_WB_MODE_INCANDESCENT,   /* Incandescent mode */
    AWB_WB_MODE_OFFICE,         /* Office mode */
    AWB_WB_MODE_SHADOW,         /* Shadow mode */
    AWB_WB_MODE_FLASH,          /* Flash mode */
    AWB_WB_MODE_HORIZON,        /* Horizon mode */
    AWB_WB_MODE_WARM_FLUORESCENT,
    AWB_WB_MODE_UNDERWATER,
    AWB_WB_MODE_TWILIGHT,
    AWB_WB_MODE_FIREWORKS,
    AWB_WB_MODE_SUN,
    AWB_WB_MODE_COUNT
};

int auto_white_balance_create(void **algorithm);
int auto_white_balance_process( void *algorithm,
                                IAWB_InArgs *input,
                                IAWB_OutArgs *output,
                                IAEWB_Rgb* stat,
                                int32 *ccm,
                                uint32 capture);
int auto_white_balance_delete(void *algorithm);
int auto_white_balance_control(void *algorithm, enum awb_mode mode);
int auto_white_balance_tuning_version(uint32 *version);
int auto_white_balance_version(uint32 *version);
#endif





