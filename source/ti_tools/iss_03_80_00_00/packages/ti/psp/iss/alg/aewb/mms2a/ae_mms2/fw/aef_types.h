/* =============================================================================
* MultiMedia Solutions AD
* (c) Copyright 2010, MultiMedia Solutions AD All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
* @file aef_types.h
*
* This file c types used in ae_mms2 algorithm
*
* ^path (TOP)/fw/aef_types.h
*
* @author Anton Dimitrov (MultiMedia Solutions AD)
*
* @date 15.12.2014
*
* @version 1.00
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*!
*!  15-Dec-2014 : Anton Dimitrov (MultiMedia Solutions AD)
*!  Created
*!
* =========================================================================== */

/* =============================================================================
*                                  INCLUDE FILES
* =========================================================================== */

#ifndef AEF_TYPES_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define AEF_TYPES_H_

#ifndef NULL
#define NULL    ((void *) 0)
#endif

// ---------------------------------------------------------------------------
// data types
// ---------------------------------------------------------------------------
#ifndef uint8
typedef unsigned char       uint8;
#endif
#ifndef uint16
typedef unsigned short      uint16;
#endif
#ifndef uint32
typedef unsigned long       uint32;
#endif
#ifndef uint64
typedef unsigned long long  uint64;
#endif
#ifndef int8
typedef signed char         int8;
#endif
#ifndef int16
typedef signed short        int16;
#endif
#ifndef int32
typedef signed long         int32;
#endif
#ifndef int64
typedef signed long long    int64;
#endif

#endif /* AEF_TYPES_H_ */
