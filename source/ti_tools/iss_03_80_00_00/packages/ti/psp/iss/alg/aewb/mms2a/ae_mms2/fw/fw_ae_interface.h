/* =============================================================================
* MultiMedia Solutions AD
* (c) Copyright 2010, MultiMedia Solutions AD All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
* @file fw_ae_interface.h
*
* This file contains algorithm-common interface used for connection between
* AE framework and system.
*
* ^path (TOP)/fw/fw_ae_interface.h
*
* @author Anton Dimitrov (MultiMedia Solutions AD)
*
* @date 13.12.2014
*
* @version 1.00
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*!
*!  13-Dec-2014 : Anton Dimitrov (MultiMedia Solutions AD)
*!  Created
*!
* =========================================================================== */

/* =============================================================================
*                                  INCLUDE FILES
* =========================================================================== */
#ifndef  __ALGO_INTERFACE_AE_H__
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define  __ALGO_INTERFACE_AE_H__

#include "aef_types.h"
/* =============================================================================
*                               PUBLIC DECLARATIONS
* =========================================================================== */

#define MAX_REC_STAGES (10)

#ifdef H3A
typedef uint16 AE_RGB_DATA;  /**< H3A available: 16-bit unsigned */
#else
typedef uint32 AE_RGB_DATA; /**< SW Implementation: 32-bit unsigned */
#endif

    typedef struct MMS_Rgb{
        AE_RGB_DATA R; /**< Red value 16 bit */
        AE_RGB_DATA G; /**< Green value 16 bit */
        AE_RGB_DATA B; /**< Blue value 16 bit */
    } MMS_Rgb;


// AE calculation mode - enumerate ae_calc_mode
    enum ae_calc_mode {
        AE_CALC_MODE_PREVIEW = 0,           // Calculation for viewfinder
        AE_CALC_MODE_PREVIEW_FAST,          // Calculation for quick viewfinder
        AE_CALC_MODE_ALLOT,                 // Allot calculation mode
        AE_CALC_MODE_ALLOT_CAPTURE,         // Allot Capture calculation mode
        AE_CALC_MODE_FOCUS,                 // calculation mode for focus
        AE_CALC_MODE_CAPTURE,               // Calculation for capure
        AE_CALC_MODE_CAPTURE_PREFLASH,      // Calculation for preflash
        AE_CALC_MODE_CAPTURE_MAINFLASH,     // Calculation for mainflash
        AE_CALC_MODE_ZERO_SHUTTER,              // Calculation for zero shutter lag mode
        AE_CALC_MODE_BRACKETING             // Bracketing
    };

// AE control mode - enumerate ae_ctrl_mode
    enum ae_ctrl_mode {
        AE_CTRL_MODE_NORMAL = 0,            // AE normal control mode
        AE_CTRL_MODE_SUSPEND                // AE suspend mode
    };

    enum ae_correct_flicker{
        REC_MAINS_50HZ = 0,                          // 50Hz
        REC_MAINS_60HZ,                              // 60Hz
        REC_MAINS_OFF                                // Disabled
    };


// AE control mode - enumerate ae_exp_mode
    enum ae_exp_mode {
        AE_EXP_MODE_AUTO = 0,               // Auto mode
        AE_EXP_MODE_BACKLIGHT,              // Backlight mode - the backlight is less
                                            // weighted in calculations
        AE_EXP_MODE_NIGHT,                  // Night mode - longer exposure time
        AE_EXP_MODE_SPORT,                  // Sport mode - shorter exposure time
        AE_EXP_MODE_VERYLONG,               // Very long mode - long exposure
        AE_EXP_MODE_MACRO,
        AE_EXP_MODE_PORTRAIT,
        AE_EXP_MODE_LANDSCAPE,
        AE_EXP_MODE_NIGHT_PORTRAIT,
        AE_EXP_MODE_MANUAL,
        AE_EXP_MODE_THEATRE,
        AE_EXP_MODE_FIREWORKS,
        AE_EXP_MODE_COUNT
    };

    enum ae_view_mode {
        AE_VIEW_MODE_VID = 0,
        AE_VIEW_MODE_CAP
    };

// AE mains frequency - enumerate ae_mains_freq
    enum ae_mains_freq {
        AE_MAINS_50HZ = 0,                      // 50Hz
        AE_MAINS_60HZ,                          // 60Hz
        AE_MAINS_OFF,                           // Disabled
        AE_MAINS_AUTO,                          // Auto detect
        AE_MAINS_100HZ,                         // 100Hz (new request)
        AE_MAINS_120HZ                          // 120Hz (new request)
    };

// AE spot weighting modes - enumerate ae_spot_weighting
    enum ae_spot_weighting {
        AE_SPOTWEIGHTING_NORMAL = 0,
        AE_SPOTWEIGHTING_CENTER,
        AE_SPOTWEIGHTING_WIDE,
        AE_SPOTWEIGHTING_COUNT
    };

    enum exposure_status {
        AE_STATUS_UNDER = 0,
        AE_STATUS_OVER,
        AE_STATUS_GOOD,
        AE_STATUS_PREFLASH     // Preflash done?
    };


// Indicates risk of camera shake. For instance, if exposure time is
// greater than 1/f but less than 2/f, camera shake can be considered
// MEDIUM_RISK. If exposure time is greater than 2/f, camera_shake can be
// considered a HIGH_RISK.
    enum exposure_shake_risk {
        AE_SHAKE_LOW_RISK = 0,
        AE_SHAKE_MEDIUM_RISK,
        AE_SHAKE_HIGH_RISK
    };

    enum exposure_bracketing_sequence {
        AE_BRACKETING_SEQUENCE_N_MIN_MAX,    // NORMAL MIN MAX sequence
        AE_BRACKETING_SEQUENCE_N_MAX_MIN,    // NORMAL MAX MIN sequence
        AE_BRACKETING_SEQUENCE_MIN_N_MAX,    // MAIN NORMAL MAX sequence
        AE_BRACKETING_SEQUENCE_MAX_N_MIN,    // MAX MIN NORMAL sequence
        AE_BRACKETING_SEQUENCE_MIN_MAX_N,    // MIN MAX NORMAL sequence
        AE_BRACKETING_SEQUENCE_MAX_MIN_N,    // MAX MIN NORMAL sequence
        AE_BRACKETING_SEQUENCE_N_MIN_MAX_ALT, // NORMAL MIN MAX alternating sequence
        AE_BRACKETING_SEQUENCE_N_MAX_MIN_ALT, // NORMAL MAX MIN alternating sequence
    };

    enum af_mode
    {
        MODE_AUTO = 0,
        MODE_MACRO,
        MODE_PORTRAIT,
        MODE_INFINITY,
        MODE_HYPERFOCAL,
        MODE_EXTENDED,
        MODE_MANUAL,
        MODE_CONTINUOUS,
        MODE_CONTINUOUS_NORMAL,
        MODE_CONTINUOUS_EXTENDED,
        MODE_MODE_COUNT
    };

    enum af_result
    {
        RESULT_UNKNOWN = 0,
        RESULT_FAIL,
        RESULT_SUCCESS,
    };

// AE manual parameters - structure ae_manual_prms
    struct ae_manual_prms {
        uint32 shutter;                         // Manual exposure time,
                                                // [us]. 0 - auto
        uint32 again;                           // Manual analog gain,
                                                // [0.1EV]. 0 - auto
        uint32 dgain;                           // Manual digital gain,
                                                // [0.1EV]. 0 - auto
        uint32 aperture;                        // Manual aperture. 0 - auto
        uint16 mask;                            // Mask - bitmask that shows
                                                // which manual parameter is
                                                // enabled
    };

// Next Table defines structure representing current sensor mode limitations.
// Structure was filled from 3AFW and passed to AE algorithm using *_control() API.
// AE range - structure  ae_range_limits
    struct ae_range_limits {
        uint32 min_shutter;                     // the exposure time lower
                                                // limit, [us]; 0 - auto
        uint32 max_shutter;                     // the exposure time upper
                                                // limit, [us]; 0 - auto
        uint32 min_again;                       // the analog gain lower
                                                // limit, [0.1EV]; 0 - auto
        uint32 max_again;                       // the analog gain upper
                                                // limit, [0.1EV]; 0 - auto
        uint32 min_dgain;                       // the digital gain lower
                                                // limit, [0.1EV]; 0 - auto
        uint32 max_dgain;                       // the digital gain upper
                                                // limit, [0.1EV]; 0 - auto
        uint32 aperture;                        // To be developed
    };

    typedef struct
    {
        /** Exposure multistage thresholds for multistage AE, microseconds
        If less then MAX_REC_STAGES stages are needed, pad with 0.
        !!! MUST be in ascending order !!!
        */
        uint32  exposure_us[MAX_REC_STAGES];

        /** Gain multistage thresholds for multistage AE, U16Q8
        If less then MAX_REC_STAGES stages are needed, pad with 0.
        Gain entries may be as many as exposure entries on one less.
        !!! MUST be in ascending order !!!
        */
        uint16  gain[MAX_REC_STAGES];
    } rel_multistage_ae_t;

// Enumeration used for frames description during preflash sequence
    enum ae_flash_calc_mode {
        AE_FLASH_MODE_LED,
        AE_FLASH_MODE_XENON,
        AE_FLASH_MODE_CAPTURE
    };


    typedef struct {
        uint16 delay_output_change;
        uint16 freeze_output_change;
        uint8  freeze_output_start;
        uint8  gesture_detected;
        uint8  enable;
        uint32 MinTH;
        uint32 MaxTH;
    }ae_gesture_detect_data_t;
//
// Complete Auto Exposure configuration structure was shown in next table.
// Structure was filled from 3AFW and passed to AE algorithm using *_control() API.
// AE configuration structure - structure ae_configuration
    struct ae_configuration {
        enum ae_ctrl_mode ctrl_mode;            // AE control mode (NORMAL,
                                                // SUSPEND)
        enum ae_calc_mode calc_mode;            // AE calculation mode
                                                // (PREVIEW, CAPTURE, ALLOT)
        enum ae_exp_mode exp_mode;              // AE exposure mode (AUTO,
                                                // BACKLIGHT, VERYLONG,...)
        enum ae_view_mode view_mode;            // AE mode indicating either we
                                                // are in video or capture
                                                // viewfinder.
        enum ae_flash_calc_mode flash_calc_mode; // Stage in preflash/mainflash sequence

        int16 flash_index;                      // Number of preflash / mainflash frame

        int16 EV_compensation;                  // exposure compensation,
                                                // [EV] (positive and
                                                // negative)
        uint16             speed;               // to be developed
        uint16             history;             // to be developed
        uint8              framerate;           // Frame rate required by application
        enum ae_mains_freq mains_frequency;     // AE mains frequency, (AUTO,
                                                // 50Hz, 60Hz, OFF)
        struct ae_manual_prms manual_params;    // Manual exposure parameters
                                                //
        struct ae_range_limits limits;          // Shutter, gain and aperture
                                                // limits
        struct ae_range_limits limits_cap;      // Shutter, gain and aperture
                                                // limits for capture
        enum ae_spot_weighting manual_spot_weighting; // Parameter related to the

        uint32 rows_in_pax;                     // needed for flicker detection

        uint32 row_time_10ns;                   // needed for flicker detection
                                                // region of interest
        uint32 ver_win_count;                   // needed for flicker detection

        uint32 hor_win_count;                   // needed for flicker detection
        //[MT+
        uint16 draft2fine_sens;                // Sensor mode (sensitivity)
                                               //   0x100 is capture sensitivity
        //[MT-

        //struct sensor_params sensor_info;           // sensitivity, active area, pixel clock

        enum exposure_bracketing_sequence bracketing_sequence; // Bracketing sequence

        uint32 bracketing_number;               // Number of bracketing frames

        uint32 bracketing_step;                 // bracketing step [EV]

        uint16 iso_base;                        // ISO for the current sensor, ISO setting based on reference ISO

        ae_gesture_detect_data_t gesture_data;  // gesture support parameters

    };

    struct af2ae_params_t {

        enum af_mode     mode;

        /* Last status of AF/CAF algorithms*/
        enum af_result   result;

        // distance to object
        uint32          o_distance;

    };

    struct preflash_frame_params {
        void  *frame_ptr;
        uint32 frame_size_x;                    // X size in bytes of preflash,dark frame
        uint32 frame_size_y;                    // Y size in bytes of preflash,dark frame
        uint32 frame_ppln;                      // Pixel per line of preflash,dark frame

    };

    /* generic structure which could be passes to all
     algorithm frameworks during creation time */
    struct fw_create_cofiguration {
        uint16 dummy;
    };

    //
    // Following table describe complete H3A statistic information that is passed to
    // Auto Exposure algorithm. Structure was filled from 3AFW and passed to AE algorithm
    // using *_process() API.
    // AE Input - structure ae_calculation_input
    struct ae_calculation_input {

        struct MMS_Rgb                *h3a;     // H3A data buffer
        uint32 pixs_in_pax;                     // number of pixels in paxel
        uint32 ver_win_count;                   // number of vertical statistic grid
        uint32 hor_win_count;                   // number of horizontal statistic grid
        uint32                        sensor_shutter; // Shutter time, [us]
        uint32                        sensor_again; // [EV units] Sensor analog gain
        uint32                        sensor_dgain; // Sensor digital gain
        uint32                        framerate; // sensor current frame rate
        uint32 sensor_aperture;                 // the aperture used while
                                                // collecting statistics,
                                                // [???]
        uint32 gain_0;                          // [U16Q8] white balance red gain
                                                //    used while collecting
                                                //    statistics
        uint32 gain_1;                          // [U16Q8]white balance green gain
                                                //    used while collecting
                                                //    statistics
        uint32 gain_2;                          // [U16Q8]white balance blue gain
                                                //    used while collecting
                                                //    statistics
        uint32 gain_3;                           // [U16Q8]white balance blue gain
                                                 //    used while collecting
                                                 //    statistics
        struct preflash_frame_params preflash_frm; // Preflash frame

        //struct face_det_result face_detect;     // Structure for face detection
        //struct exp_region_result exp_region;        // Structure for exposure regions

        struct af2ae_params_t af_params;              // Structure for af data needed for flash

        uint16 draft2fine_sens;                // Sensor mode (sensitivity)
    };


//
// Output from Auto Exposure algorithm is shown in next table. Structure was
// filled from AE algorithm.
// AE calculation complete structure - structure ae_calculation_output
    struct ae_calculation_output {
        uint32 shutter;                         // shutter speed, [us]
        uint32 shutter_cap;                     // shutter speed for
                                                // capture, [us]
        uint32 again;                           // analog gain, [EV units]
        uint32 again_cap;                       // analog gain for
                                                // capture, [EV units]
        uint32 dgain;                           // digital gain, [U16Q8]
        uint32 aperture;                        // Aperture
        uint32 aperture_cap;                    // Aperture for capture
        uint32 texp;                            // total exposure of the
                                                // statistics, [us] (limited
                                                // by the allot)
        uint64 req_total_exp;                   // required total exposure
                                                // this value can be unreachable
                                                // for real system.
        uint16 illuminance;                     // absolute illumination of
                                                // the scene, [lx]
        uint16 ave_y_level;                     // the average Y level,
                                                // calculated from the
                                                // statistics
        uint32 converged;                       // 1: the algorithm is
                                                // converged; 0: the
                                                // algorithm is not converged

        uint16                   flash_light_ratio;        // flash light ratio
        uint16                   af_assist_required;       // af assist control flag

        uint8                    ae_reach_target;          // 1: the algorithm reached
                                                // required total exposure
                                                // 0 - not reached, yet.
        //enum flicker_detection   ae_flkr_detect;
        enum exposure_status     status;
        enum exposure_shake_risk risk;

        //struct flash_control     flash;                 // Suggested flash intensity
        //struct flash_control     assist;                // Suggested af_assistintensity
    };

    struct mms_ae_version {
        uint32 version;
    };
/* ========================================================================== */
/**
*  auto_exposure_create()    Create Auto Exposure algorithm instance
*
*  The above API should be used to create Auto Exposure algorithm instance.
*  On successful return from this function first argument will point to
*  algorithm object and will be used as reference for sequential access
*  to exported algorithm functionality. This parameter is opaque to 3AFW
*  and is responsibility for algorithm to fill appropriate information.
*  Second parameter can be used from 3AFW to setup initial algorithm processing
*  parameters.
*
*  @param   algorithm - void ** - algorithm descriptor
*
*  @param   data - structure fw_create_cofiguration * - camera ID,  DTP server and
*           EEPROM parser server.
*
*  @return  0 for success else error code (EBUSY, EINVAL)
*/
/* ========================================================================== */
    int auto_exposure_create(void * *algorithm, struct fw_create_cofiguration *data);

/* ========================================================================== */
/**
*  auto_exposure_delete()    Delete Auto Exposure algorithm instance
*
*  This API should be used when 3AFW need to remove and free any resources
*  owned from Auto Exposure algorithm. First argument is descriptor returned
*  from *_create() API call.
*
*  @param   algorithm - void * - algorithm descriptor
*
*  @return  0 for success else error code (EBUSY, EINVAL)
*/
/* ========================================================================== */
    int auto_exposure_delete(void *algorithm);

/* ========================================================================== */
/**
*  auto_exposure_control()    Use this API for fine algorithm tuning
*
*  This API will be used to configure, start and stop the Auto Exposure
*  algorithm. First argument is descriptor returned from *_create() API call.
*
*  @param   algorithm - void * - algorithm descriptor
*
*  @param   configuration - structure ae_configuration * - new algorithm configuration
*
*  @return  0 for success else error code (EBUSY, EINVAL)
*/
/* ========================================================================== */
    int auto_exposure_control(void *algorithm, struct ae_configuration *configuration);

/* ========================================================================== */
/**
*  auto_exposure_process()    Main Auto Exposure calculation function
*
*  Above API will be called when 3AFW read new H3A statistic or new
*  HISTOGRAM statistic buffer from Camera driver. Input parameter hold
*  address of H3A statistic buffer, it is not allowed algorithm to keep
*  reference to this buffer after competition of this function. 3AFW is
*  free to reuse memory occupied from statistic for other purposes. On
*  successful competition of this call Auto Exposure algorithm should
*  return proposal configuration of OMAP34XX ISP module. This API call
*  should be completed for maximum 5 msec. 3AFW will process output from
*  algorithm and change ISP configuration.
*
*  @param   algorithm - void * - algorithm descriptor
*
*  @param   input - structure ae_calculation_input * - data collected from SCM module inside ISP.
*
*  @param   output - structure ae_calculation_output * - Auto Exposure calculation output
*
*  @return  0 for success else error code (EBUSY, EINVAL)
*/
/* ========================================================================== */
    int auto_exposure_process(void *algorithm, struct ae_calculation_input *input,
                              struct ae_calculation_output *output);

/* ========================================================================== */
/**
*  auto_exposure_version()    API for algorithm version get
*
*  This API should be usable without algorithm instance creation, so
*  it does not need algorithm descriptor as an input parameter
*
*  @param   version - structure mms_ae_version* - AE version
*
*  @return  0 for success else error code (EBUSY, EINVAL)
*/
/* ========================================================================== */
    int auto_exposure_version(struct mms_ae_version *version);

#endif // __ALGO_INTERFACE_AE_H__

