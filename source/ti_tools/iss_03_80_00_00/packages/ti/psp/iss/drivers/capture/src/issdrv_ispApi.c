/** ==================================================================

 *  @file   issdrv_ispApi.c

 *

 *  @path   /ti/psp/iss/drivers/capture/src/

 *

 *  @desc   This  File contains.

 * ===================================================================

 *  Copyright (c) Texas Instruments Inc 2011, 2012

 *

 *  Use of this software is controlled by the terms and conditions found

 *  in the license agreement under which this software has been supplied

 * ===================================================================*/

#include <xdc/std.h>
#include <ti/psp/iss/hal/iss/isp/isif/inc/isif.h>
#include <ti/psp/iss/hal/iss/isp/rsz/inc/rsz.h>
#include <ti/psp/iss/hal/iss/isp/ipipe/inc/ipipe.h>
#include <ti/psp/iss/core/inc/iss_drv_common.h>
#include <ti/psp/iss/core/inc/iss_drv.h>
#include <ti/psp/examples/utility/vpsutils_mem.h>
#include <ti/psp/iss/drivers/alg/vstab/alg_vstab.h>
#include <ti/psp/iss/drivers/alg/2A/inc/issdrv_alg2APriv.h>
#include <ti/psp/iss/drivers/capture/src/issdrv_capturePriv.h>
#include <ti/psp/iss/drivers/capture/src/issdrv_ispPriv.h>
#include <ti/psp/iss/common/iss_evtMgr.h>

extern isif_regs_ovly isif_reg;
extern ipipe_regs_ovly ipipe_reg;
extern rsz_regs_ovly rsz_reg;
extern rsz_A_regs_ovly rszA_reg;
extern rsz_B_regs_ovly rszB_reg;
extern ipipeif_regs_ovly ipipeif_reg;
extern VIDEO_VsStream gVsStream;

Iss_IspCommonObj gIss_ispCommonObj;

/* Stack for process task */
#pragma DATA_ALIGN(gIssIsp_processTskStack,32)
#pragma DATA_SECTION(gIssIsp_processTskStack, ".bss:taskStackSection")

UInt8 gIssIsp_processTskStack[ISS_ISP_INST_MAX][ISS_ISP_PROCESS_TASK_STACK];

Int32 IspDrv_processReq(Iss_IspObj *pObj);
Void ispDrvCalcFlipOffset(Iss_IspObj *pObj);
Void ispDrvSetOneShotMode(Iss_IspObj *pObj);
Void ispDrvSetCfg(Iss_IspObj *pObj);
Iss_IspObj *issIspAllocObj();
Void issIspFreeObj(Iss_IspObj *pObj);
Int32 Iss_isplock();
Int32 Iss_ispUnlock();
Void issIspLink_printIpipeISReg();

UInt32 Utils_tilerGetOriAddr(
        UInt32 tilerAddr,
        UInt32 cntMode,
        UInt32 oriFlag,
        UInt32 width,
        UInt32 height);

UInt32 Utils_tilerAddr2CpuAddr(UInt32 tilerAddr);

Int32 ispDrvIsYUV422ILEFormat(Int32 format)
{
    if((format == FVID2_DF_YUV422I_UYVY) ||
            (format == FVID2_DF_YUV422I_YUYV) ||
            (format == FVID2_DF_YUV422I_YVYU) ||
            (format == FVID2_DF_YUV422I_VYUY))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/* ISS Post Process Write reg */
Void Iss_ispWriteReg(volatile UInt32 *pRegAddr,UInt32 value,UInt32 bitPosn,UInt32 numBits)
{
    UInt32 i,mask = 0;

    for(i = 0;i < numBits;i ++)
    {
        mask |= (1 << (bitPosn + i));
    }
    mask = ~mask;

    *pRegAddr &= mask;
    *pRegAddr |= (value << bitPosn);
}

Void Issdrv_ispDrvRszOvfl()
{
    Iss_IspObj *pObj;
    FVID2_ProcessList *errList;

    pObj = gIss_ispCommonObj.curActIspObj;

    gIss_ispCommonObj.numRszOvfl ++;
    Vps_rprintf("\n\n ============ RESIZER OVERFLOW %d============= \n\n", gIss_ispCommonObj.numRszOvfl);

    if ((NULL != pObj) && (pObj->state == ISS_ISP_STATE_CREATED))
    {
        if (1 == pObj->usrCbFlag)
        {
            /* Main Task is waiting for the Luma to finish */

            /* So upate use flag to 2 and post the semaphore,
               main task should process further if the use flag is set to 2*/
            pObj->isRszOvfl = 1u;
            Semaphore_post(pObj->sem420);
        }

        ispDrvSetOneShotMode(pObj);

        ispDrvSetCfg(pObj);

        if (pObj->cbPrm.fdmErrCbFxn)
        {
            errList = (FVID2_ProcessList *)pObj->cbPrm.errList;

            errList->numInLists = 1u;
            errList->numOutLists = 1u;

            /* Copy in and out frame Lists */
            errList->inFrameList[0] =
                pObj->procList.inFrameList[0];
            errList->outFrameList[0] =
                pObj->procList.outFrameList[0];

            pObj->cbPrm.fdmErrCbFxn(
                    pObj->cbPrm.fdmData,
                    errList,
                    pObj);
        }

        gIss_ispCommonObj.curActIspObj = NULL;

        Iss_ispUnlock();
    }
}

/* ISS Post Process Callback */
Int32 Iss_ispDrvCallBack()
{
    Iss_IspObj *pObj;
    VpsUtils_QueHandle *pQueueHndl;

    pObj = gIss_ispCommonObj.curActIspObj;

    if ((NULL == pObj) || (pObj->state == ISS_ISP_STATE_IDLE))
    {
        return FVID2_SOK;
    }

    if(pObj->cbPrm.fdmCbFxn != NULL)
    {
        if(pObj->usrCbFlag == 0)
        {
            if(pObj->vsEnable == TRUE)
            {
                pQueueHndl = &pObj->processQueue;
            }
            else
            {
                pQueueHndl = &pObj->outQueue;
            }

            /* Queue the buffers in in queue */
            if(pObj->pOutFrame[0] != NULL)
            {
                VpsUtils_quePut(pQueueHndl,
                        pObj->pOutFrame[0],
                        BIOS_WAIT_FOREVER);
            }

            if(pObj->pOutFrame[1] != NULL)
            {
                VpsUtils_quePut(pQueueHndl,
                        pObj->pOutFrame[1],
                        BIOS_WAIT_FOREVER);
            }

            if(pObj->runVs == FALSE)
            {
                Semaphore_post(gIss_ispCommonObj.bscSync);
            }

            Semaphore_post(gIss_ispCommonObj.process);

            pObj->cbPrm.fdmCbFxn(
                    pObj->cbPrm.fdmData,
                    pObj);
        }
        else
        {
            pObj->usrCbFlag = 0;
            Semaphore_post(pObj->sem420);
        }
    }
    gIss_ispCommonObj.curActIspObj = NULL;

    return FVID2_SOK;
}

/* ISS Post Process lock */
Int32 Iss_ispLock()
{
    /*
     * take semaphore for locking
     */
    Semaphore_pend(gIss_ispCommonObj.lock, BIOS_WAIT_FOREVER);

    return FVID2_SOK;
}

/* ISS Post Process unlock */
Int32 Iss_ispUnlock()
{
    /*
     * release semaphore for un-locking
     */
    Semaphore_post(gIss_ispCommonObj.lock);

    return FVID2_SOK;
}

/* ISS ISP Post BSC sync semaphore */
Int32 Iss_ispPostBscSem(UInt32 prevBscBufAddr,UInt32 curBscBufAddr)
{
    Iss_IspObj *pObj;

    pObj = gIss_ispCommonObj.curActIspObj;

    if((pObj->state == ISS_ISP_STATE_CREATED) && (pObj->vsEnable == TRUE))
    {
        pObj->prevBscBufAddr = prevBscBufAddr;
        pObj->curBscBufAddr  = curBscBufAddr;
        Semaphore_post(gIss_ispCommonObj.bscSync);
    }

    return FVID2_SOK;
}

/* ISS ISP Get VS run status */
Bool Iss_ispGetVsRunStatus()
{
    Iss_IspObj *pObj;

    pObj = gIss_ispCommonObj.curActIspObj;

    return (pObj->runVs);
}

void Iss_IspRszDmaEndInt(const UInt32 event, Ptr arg)
{
    Iss_ispDrvCallBack();
}

void Iss_IspRszOvfInt(const UInt32 event, Ptr arg)
{
    Issdrv_ispDrvRszOvfl();
}

/* ISS ISP Process Task */
Void Iss_ispProcessTask(UArg arg1, UArg arg2)
{
    Int32 retVal = FVID2_SOK;
    Iss_IspObj *pObj;
    FVID2_Frame *pFrame;
    ALG_VstabRunPrm runPrm;
    ALG_VstabStatus vstabStatus;
    Iss_IspPerFrameCfg *pPerFrameCfg;
    UInt32 nonStabilizedWidth,nonStabilizedHeight;
    UInt32 startX = 0,startY = 0;
    UInt32 offsetY = 0,offsetUV = 0;

    pObj = &gIss_ispCommonObj.ispObj[0];

    Vps_printf("Entered ISS ISP Process task\n");

    while(1)
    {
        /* Wait for BSC semaphore */
        Semaphore_pend(gIss_ispCommonObj.bscSync,BIOS_WAIT_FOREVER);

        if(gIss_ispCommonObj.exitTask == TRUE)
        {
            break;
        }

        /* Apply VS algorithm */
        if(pObj->runVs == TRUE)
        {
            runPrm.curBscDataVirtAddr  = (Uint8*)pObj->curBscBufAddr;
            runPrm.curBscDataPhysAddr  = NULL;
            runPrm.prevBscDataVirtAddr = (Uint8*)pObj->prevBscBufAddr;
            runPrm.prevBscDataPhysAddr = NULL;

            ALG_vstabRun(gVsStream.algVsHndl, &runPrm, &vstabStatus);
        }

        /* Wait for process semaphore */
        Semaphore_pend(gIss_ispCommonObj.process,BIOS_WAIT_FOREVER);

        do
        {
            /* Get the frames from process queue */
            retVal = VpsUtils_queGet(&pObj->processQueue,(Ptr *)&pFrame,1,BIOS_NO_WAIT);

            if(retVal == FVID2_SOK)
            {
                /* Stabilize the frame */
                pPerFrameCfg = (Iss_IspPerFrameCfg*)(pFrame->perFrameCfg);

                nonStabilizedWidth  = (pPerFrameCfg->frameWidth * VSTAB_SCALE_NUMERATOR)/VSTAB_SCALE_DENOMINATOR;
                nonStabilizedHeight = (pPerFrameCfg->frameHeight * VSTAB_SCALE_NUMERATOR)/VSTAB_SCALE_DENOMINATOR;

                startX = (nonStabilizedWidth * vstabStatus.startX)/pObj->bscInWidth;
                startX = ISP_floor(startX,4);

                startY = (nonStabilizedHeight * vstabStatus.startY)/pObj->bscInHeight;
                startY = ISP_floor(startY,2);

                if (pPerFrameCfg->queueId == 0)
                {
                    offsetY  = (startY * pObj->pitch[0][0]) + startX;
                    offsetUV = (startY * (pObj->pitch[0][1] >> 1)) + startX;
                }
                else
                {
                    offsetY  = (startY * pObj->pitch[1][0]) + startX;
                    offsetUV = (startY * (pObj->pitch[0][1] >> 1)) + startX;
                }

                if((pPerFrameCfg->dataFormat == FVID2_DF_YUV420SP_UV) ||
                        (pPerFrameCfg->dataFormat == FVID2_DF_YUV420SP_VU))
                {
                    pFrame->addr[0][0] = (Ptr)(((Int32)pFrame->addr[1][0] + offsetY) & 0xFFFFFFE0);
                    pFrame->addr[0][1] = (Ptr)(((Int32)pFrame->addr[1][1] + offsetUV) & 0xFFFFFFE0);
                }
                else
                {
                    pFrame->addr[0][0] = (Ptr)(((Int32)pFrame->addr[1][0] + (offsetY)) & 0xFFFFFFE0);
                }

                /* Put it in out queue */
                VpsUtils_quePut(&pObj->outQueue,
                        pFrame,
                        BIOS_WAIT_FOREVER);
            }
        }while(retVal == FVID2_SOK);
    }

    Vps_printf("Exited ISS ISP Process task\n");
}

/* ISS Post Process Create */
Fdrv_Handle Iss_ispCreate(UInt32 drvId,
        UInt32 instanceId,
        Iss_IspCreateParams *pCreateArgs,
        Iss_IspCreateStatus *pCreateStatus,
        const FVID2_CbParams *pCbPrm)
{
    Iss_IspObj *pObj;
    Semaphore_Params semParams;
    Task_Params tskParams;

    if (instanceId == ISS_ISP_INST_ALL)
    {
        /*
         * gloabl handle open requested, no action required,
         * just return a special handle ID
         */
        return (Fdrv_Handle)ISS_ISP_INST_ALL;
    }

    if (pCreateStatus == NULL)
    {
        return NULL;
    }

    if ((instanceId >= ISS_ISP_INST_MAX) || (pCreateArgs == NULL) || (drvId != FVID2_ISS_M2M_ISP_DRV))
    {
        /*
         * invalid parameters - return NULL
         */
        pCreateStatus->retVal = FVID2_EBADARGS;
        return NULL;
    }

    /*
     * lock driver instance
     */
    Iss_ispLock();

    pObj = issIspAllocObj();
    if (NULL == pObj)
    {
        Vps_rprintf("Could not allocate handle object\n");
        return NULL;
    }

    /* Semaphore for the YUV420 -> YUV420 resizing */
    Semaphore_Params_init(&semParams);

    semParams.mode = Semaphore_Mode_BINARY;
    pObj->sem420 = Semaphore_create(0u, &semParams, NULL);

    /*
     * check if object is already opended
     */

    if (pObj->state != ISS_ISP_STATE_IDLE)
    {
        pCreateStatus->retVal = FVID2_EDEVICE_INUSE;

        issIspFreeObj(pObj);

        Iss_ispUnlock();
        return NULL;
    }

    memcpy(&pObj->createArgs, pCreateArgs, sizeof(pObj->createArgs));

    pObj->instanceId = instanceId;
    pObj->vsEnable   = pCreateArgs->vsEnable;

    if (pCbPrm != NULL)
    {
        memcpy(&pObj->cbPrm, pCbPrm, sizeof(pObj->cbPrm));
    }

    pObj->state = ISS_ISP_STATE_CREATED;

    VpsUtils_queCreate(&pObj->outQueue,                 // handle
            ISS_ISP_MAX_ELEMENTS_QUEUE,      // maxElements
            &pObj->outQueueMem[0],           // queueMem
            VPSUTILS_QUE_FLAG_NO_BLOCK_QUE); // flags

    VpsUtils_queCreate(&pObj->processQueue,             // handle
            ISS_ISP_MAX_ELEMENTS_QUEUE,      // maxElements
            &pObj->processQueueMem[0],       // queueMem
            VPSUTILS_QUE_FLAG_NO_BLOCK_QUE); // flags

    /* Create process task */
    if (gIss_ispCommonObj.processTask == NULL)
    {
        Task_Params_init(&tskParams);

        tskParams.priority  = ISS_ISP_PROCESS_TASK_PRI;
        tskParams.stack     = gIssIsp_processTskStack[instanceId];
        tskParams.stackSize = ISS_ISP_PROCESS_TASK_STACK;

        gIss_ispCommonObj.processTask = Task_create(Iss_ispProcessTask,
                &tskParams,
                NULL);
    }

    pObj->pOutFrame[0] = NULL;
    pObj->pOutFrame[1] = NULL;

    gIss_ispCommonObj.numObjUsed ++;

    if(pObj->createArgs.doRszCfg == TRUE)
    {
        /* Configure one shot mode only if
           doRszCfg is set to TRUE*/
        ispDrvSetOneShotMode(pObj);

        /* Register Callback for RSZ Interrupt interrupt */
        pObj->rszIntHandle = Iem_register(IEM_IRQ_NUM_0,
                IEM_IRQ_BANK_NUM_ISP_IRQ0,
                IEM_EVENT_RSZ_INT_DMA,
                IEM_PRIORITY0,
                Iss_IspRszDmaEndInt,
                NULL);
        if (NULL == pObj->rszIntHandle)
        {
            Vps_printf("\n\n Cannot register RSZ ISR\n");
            return NULL;
        }

        /* Register Resizer Overflow interrupt interrupt */
        pObj->rszOvfIntHandle = Iem_register(IEM_IRQ_NUM_0,
                IEM_IRQ_BANK_NUM_ISP_IRQ0,
                IEM_EVENT_RSZ_INT_FIFO_OVF,
                IEM_PRIORITY0,
                Iss_IspRszOvfInt,
                NULL);

        if (NULL == pObj->rszOvfIntHandle)
        {
            Vps_printf("\n\n Cannot register RSZ OVERFLOW ISR\n");
            return NULL;
        }
    }

    /*
     * unlock driver instance
     */
    Iss_ispUnlock();

    return pObj;
}

/* ISS Post Process Process Frames */
Int32 Iss_ispProcessFrames(FVID2_Handle handle,
        FVID2_ProcessList *procList)
{
    Int32 retVal = FVID2_SOK;
    Iss_IspObj *pObj = (Iss_IspObj *)handle;

    if (handle == NULL || procList == NULL)
    {
        retVal = FVID2_EBADARGS;
        return retVal;
    }

    Iss_ispLock();

    pObj->pOutFrame[0] = NULL;
    pObj->pOutFrame[1] = NULL;

    pObj->usrCbFlag = 0;
    pObj->isRszOvfl = 0u;

    if(pObj->numStream == 0)
    {
        Iss_ispUnlock();
        Iss_ispDrvCallBack();
        return retVal;
    }

    /*
     * Turn OFF RSZ,IPPIEIF and IPIPE before starting them
     */
    ipipeif_start(IPIPEIF_STOP);
    rsz_submodule_start(RSZ_STOP, RESIZER_A);
    rsz_submodule_start(RSZ_STOP, RESIZER_B);
    ipipe_start(IPIPE_STOP);

    if(pObj->inDataFmt == FVID2_DF_BAYER_RAW)
    {
        /*
         * IPIPE is bypassed for YUV input
         */
        while(ipipe_reg->SRC_EN == 1);   //TODO
    }

    pObj->procList.inFrameList[0] = procList->inFrameList[0];
    pObj->procList.outFrameList[0] = procList->outFrameList[0];

    pObj->inFrame = procList->inFrameList[0]->frames[0];
    pObj->pOutFrame[0] = procList->outFrameList[0]->frames[0];
    if (pObj->numStream == 2)
        pObj->pOutFrame[1] = procList->outFrameList[0]->frames[1];

    if (pObj->inFrame == NULL || pObj->pOutFrame[0] == NULL)
    {
        Iss_ispUnlock();
        Iss_ispDrvCallBack();
        return FVID2_EFAIL;
    }
    if (pObj->numStream > 1 && (pObj->pOutFrame[1] == NULL))
    {
        Iss_ispUnlock();
        Iss_ispDrvCallBack();
        return FVID2_EFAIL;
    }

    gIss_ispCommonObj.curActIspObj = pObj;

    retVal = IspDrv_processReq(pObj);

    Iss_ispUnlock();

    return retVal;
}

/* ISS Get Processed Frames */
Int32 Iss_ispGetProcessedFrames(Fdrv_Handle handle,
        FVID2_ProcessList *processList,
        UInt32 timeout)
{
    Int32 retVal = FVID2_SOK;
    Iss_IspObj *pObj = (Iss_IspObj *)handle;
    FVID2_Frame *pFrame;

    processList->outFrameList[0]->numFrames = 0;

    do
    {
        retVal = VpsUtils_queGet(&pObj->outQueue,(Ptr *)&pFrame,1,BIOS_NO_WAIT);

        if(retVal == FVID2_SOK)
        {
            processList->outFrameList[0]->frames[processList->outFrameList[0]->numFrames] = pFrame;
            processList->outFrameList[0]->numFrames ++;
        }
    }while(retVal == FVID2_SOK);

    return FVID2_SOK;
}

/* ISS Post Process Control */
Int32 Iss_ispControl(Fdrv_Handle handle, UInt32 cmd,
        Ptr cmdArgs, Ptr cmdStatusArgs)
{
    Int32 status = FVID2_SOK;
    UInt32 pCnt;
    Iss_IspObj *pObj = (Iss_IspObj *)handle;

    switch(cmd)
    {
        case IOCTL_ISS_ISP_RSZ_CONFIG:
            {
                Iss_IspRszConfig *pRszCfg = (Iss_IspRszConfig*)cmdArgs;

                pObj->inDataFmt   = pRszCfg->inDataFmt;
                pObj->outDataFmt0 = pRszCfg->outDataFmt0;
                pObj->outDataFmt1 = pRszCfg->outDataFmt1;
                pObj->numStream   = (pRszCfg->numStream >= 2)?2:1;
                pObj->inWidth     = pRszCfg->inWidth;
                pObj->inHeight    = pRszCfg->inHeight;
                pObj->inStartX    = pRszCfg->inStartX;
                pObj->inStartY    = pRszCfg->inStartY;
                pObj->outWidth0   = pRszCfg->outWidth0;
                pObj->outHeight0  = pRszCfg->outHeight0;
                pObj->outWidth1   = pRszCfg->outWidth1;
                pObj->outHeight1  = pRszCfg->outHeight1;
                pObj->mirrorMode  = pRszCfg->mirrorMode;
                pObj->outStartX   = pRszCfg->outStartX;
                pObj->outStartY   = pRszCfg->outStartY;
                pObj->inPitch     = pRszCfg->inPitch;

                for (pCnt = 0; pCnt < 3; pCnt ++)
                {
                    pObj->pitch[0][pCnt] = pRszCfg->pitch[0][pCnt];
                    pObj->pitch[1][pCnt] = pRszCfg->pitch[1][pCnt];
                }

                if(pObj->numStream == 0)
                {
                    break;
                }

                pObj->runVs = pRszCfg->runVs;

                if(pObj->runVs == TRUE)
                {
                    pObj->bscInWidth  = pObj->inWidth;
                    pObj->bscInHeight = pObj->inHeight;
                }

                ispDrvCalcFlipOffset(pObj);
            }
            break;

        case IOCTL_ISS_ISP_RSZ_INIT_INTR:
            {
                Bool bInit = *(Bool*)cmdArgs;
                if(bInit == TRUE)
                {
                    /* Unregister all cameraLink interrupt handlers */
                    IssCdrv_unRegisterInt();

                    ispDrvSetOneShotMode(pObj);
                    /* Register Callback for RSZ Interrupt interrupt */
                    pObj->rszIntHandle = Iem_register(IEM_IRQ_NUM_0,
                            IEM_IRQ_BANK_NUM_ISP_IRQ0,
                            IEM_EVENT_RSZ_INT_DMA,
                            IEM_PRIORITY0,
                            Iss_IspRszDmaEndInt,
                            NULL);
                    if (NULL == pObj->rszIntHandle)
                    {
                        Vps_printf("\n\n Cannot register RSZ ISR\n");
                        return NULL;
                    }
                    /* Register Resizer Overflow interrupt interrupt */
                    pObj->rszOvfIntHandle = Iem_register(IEM_IRQ_NUM_0,
                            IEM_IRQ_BANK_NUM_ISP_IRQ0,
                            IEM_EVENT_RSZ_INT_FIFO_OVF,
                            IEM_PRIORITY0,
                            Iss_IspRszOvfInt,
                            NULL);
                    if (NULL == pObj->rszOvfIntHandle)
                    {
                        Vps_printf("\n\n Cannot register RSZ OVERFLOW ISR\n");
                        return NULL;
                    }
                }
                else
                {
                    /* Check whether the interrupt handler for resizer is registered
                     * before unregistering. Resizer handler is registered only in
                     * transcode usecase */
                    if((pObj->rszOvfIntHandle != NULL) && (pObj->rszIntHandle != NULL)) {
                        Iem_unRegister(pObj->rszIntHandle);
                        Iem_unRegister(pObj->rszOvfIntHandle);
                    }

                    /* Re-register all cameraLink interrupt handlers */
                    IssCdrv_registerInt();
                }
            }
            break;

        default:
            break;
    }

    return status;
}

/* ISS Post Process Delete */
Int32 Iss_ispDelete(Fdrv_Handle handle, Ptr reserved)
{
    Int32 status = FVID2_SOK;
    Iss_IspObj *pObj = (Iss_IspObj *)handle;
    UInt32 sleepTime = 16;

    if (handle == (Fdrv_Handle) ISS_ISP_INST_ALL)
    {
        return FVID2_SOK;
    }

    GT_assert(GT_DEFAULT_MASK, pObj != NULL);

    /*
     * lock driver instance
     */
    Iss_ispLock();

    if (pObj->state == ISS_ISP_STATE_STOPPED || pObj->state == ISS_ISP_STATE_CREATED)
    {
        // Stop the rsz and ipipeif
        rsz_start(RSZ_STOP);
        ipipeif_start(IPIPEIF_STOP);

        pObj->state = ISS_ISP_STATE_IDLE;
    }

    VpsUtils_queDelete(&pObj->outQueue);
    VpsUtils_queDelete(&pObj->processQueue);

    issIspFreeObj(pObj);

    gIss_ispCommonObj.numObjUsed --;

    if (0 == gIss_ispCommonObj.numObjUsed)
    {
        /* Process task exit sequence */
        gIss_ispCommonObj.exitTask = TRUE;
        Semaphore_post(gIss_ispCommonObj.bscSync);
        while(Task_Mode_TERMINATED != Task_getMode(gIss_ispCommonObj.processTask))
        {
            Task_sleep(sleepTime);
        }
        Task_delete(&gIss_ispCommonObj.processTask);

        gIss_ispCommonObj.curActIspObj = NULL;
    }

    if(pObj->createArgs.doRszCfg == TRUE)
    {
        /* Unregister RSZ interrupt */
        pObj->rszIntHandle    = NULL;
        pObj->rszOvfIntHandle = NULL;
    }

    /*
     * unlock driver instance
     */
    Iss_ispUnlock();

    Semaphore_delete(&pObj->sem420);

    return status;
}

/* ISS Post Process Init */
Int32 Iss_ispInit()
{
    Int32 status = FVID2_SOK, instId;
    Semaphore_Params semParams;

    memset(&gIss_ispCommonObj, 0, sizeof(gIss_ispCommonObj));

    /* create locking semaphore */
    Semaphore_Params_init(&semParams);

    semParams.mode = Semaphore_Mode_BINARY;
    gIss_ispCommonObj.lock = Semaphore_create(1u, &semParams, NULL);

    if (gIss_ispCommonObj.lock == NULL)
    {
        Vps_printf(" ISP:%s:%d: Semaphore_create() failed !!!\n",
                __FUNCTION__, __LINE__);
        status = FVID2_EALLOC;
    }

    /* Semaphore for BSC sync */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    gIss_ispCommonObj.bscSync = Semaphore_create(0u, &semParams, NULL);
    if (gIss_ispCommonObj.bscSync == NULL)
    {
        Vps_printf(" ISP:%s:%d: Semaphore_create() failed !!!\n",
                __FUNCTION__, __LINE__);
        status = FVID2_EALLOC;
    }

    /* Semaphore for process */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    gIss_ispCommonObj.process = Semaphore_create(0u, &semParams, NULL);
    if (gIss_ispCommonObj.process == NULL)
    {
        Vps_printf(" ISP:%s:%d: Semaphore_create() failed !!!\n",
                __FUNCTION__, __LINE__);
        status = FVID2_EALLOC;
    }

    for (instId = 0; instId < ISS_ISP_INST_HANDLES; instId++)
    {
        gIss_ispCommonObj.isAllocated[instId] = FALSE;
    }

    gIss_ispCommonObj.exitTask = FALSE;
    gIss_ispCommonObj.numRszOvfl = 0;

    if (status == FVID2_SOK)
    {
        /* register driver to FVID2 layer */
        gIss_ispCommonObj.fvidDrvOps.create             = (FVID2_DrvCreate)Iss_ispCreate;
        gIss_ispCommonObj.fvidDrvOps.delete             = Iss_ispDelete;
        gIss_ispCommonObj.fvidDrvOps.control            = Iss_ispControl;
        gIss_ispCommonObj.fvidDrvOps.queue              = NULL;
        gIss_ispCommonObj.fvidDrvOps.dequeue            = NULL;
        gIss_ispCommonObj.fvidDrvOps.processFrames      = Iss_ispProcessFrames;
        gIss_ispCommonObj.fvidDrvOps.getProcessedFrames = Iss_ispGetProcessedFrames;
        gIss_ispCommonObj.fvidDrvOps.drvId              = FVID2_ISS_M2M_ISP_DRV;

        status = FVID2_registerDriver(&gIss_ispCommonObj.fvidDrvOps);

        if (status != FVID2_SOK)
        {
            Vps_printf(" ISP:%s:%d: FVID2_registerDriver() failed !!!\n",
                    __FUNCTION__, __LINE__);
        }
    }

    if (status != FVID2_SOK)
    {
        Iss_ispDeInit();

        return status;
    }

    return status;
}

/* ISS Post Process Deinit */
Int32 Iss_ispDeInit()
{
    /*
     * unregister from FVID2 layer
     */
    FVID2_unRegisterDriver(&gIss_ispCommonObj.fvidDrvOps);

    /*
     * delete driver locks
     */

    /* delete semaphore */
    if (NULL != gIss_ispCommonObj.lock)
        Semaphore_delete(&gIss_ispCommonObj.lock);
    if (NULL != gIss_ispCommonObj.bscSync)
        Semaphore_delete(&gIss_ispCommonObj.bscSync);
    if (NULL != gIss_ispCommonObj.process)
        Semaphore_delete(&gIss_ispCommonObj.process);


    return FVID2_SOK;
}

Int32 IspDrv_processReq(Iss_IspObj *pObj)
{
    Int32 retVal = FVID2_SOK;
    Ptr inAddr,inAddrC;
    Ptr outAddrAY = NULL,outAddrAC = NULL,outAddrBY = NULL,outAddrBC = NULL;

    // set the input address
    inAddr = pObj->inFrame->addr[0][0];
    inAddrC = pObj->inFrame->addr[0][1];

    if(pObj->numStream == 2)
    {
        outAddrAY = pObj->pOutFrame[0]->addr[1][0];
        outAddrAC = pObj->pOutFrame[0]->addr[1][1];
        outAddrBY = pObj->pOutFrame[1]->addr[1][0];
        outAddrBC = pObj->pOutFrame[1]->addr[1][1];

        if (!ispDrvIsYUV422ILEFormat(pObj->outDataFmt0))
            if (NULL == outAddrAC)
            {
                Vps_rprintf("\n%s:%d\n", __func__, __LINE__, "Address is NULL\n");
                return FVID2_EBADARGS;
            }

        if (!ispDrvIsYUV422ILEFormat(pObj->outDataFmt1))
            if (NULL == outAddrBC)
            {
                Vps_rprintf("\n%s:%d\n", __func__, __LINE__, "Address is NULL\n");
                return FVID2_EBADARGS;
            }

        if ((NULL == outAddrAY) || (NULL == outAddrBY))
        {
            Vps_rprintf("\n%s:%d\n", __func__, __LINE__, "Address is NULL\n");
            return FVID2_EBADARGS;
        }
    }
    else if(pObj->numStream == 1)
    {
        outAddrAY = pObj->pOutFrame[0]->addr[1][0];
        outAddrAC = pObj->pOutFrame[0]->addr[1][1];

        if (!ispDrvIsYUV422ILEFormat(pObj->outDataFmt0))
            if (NULL == outAddrAC)
            {
                Vps_rprintf("\n%s:%d\n", __func__, __LINE__, "Address is NULL\n");
                return FVID2_EBADARGS;
            }

        if (NULL == outAddrAY)
        {
            Vps_rprintf("\n%s:%d\n", __func__, __LINE__, "Address is NULL\n");
            return FVID2_EBADARGS;
        }
    }
    else
    {
        Vps_printf(" ISP:%s:%d: Invalid numstream !!!\n",
                __FUNCTION__, __LINE__);
        retVal = FVID2_EBADARGS;
        return retVal;
    }

    outAddrAY = (UInt32 *)outAddrAY + (pObj->pitch[0][0] * pObj->outStartY + pObj->outStartX)/4;
    outAddrAC = (UInt32 *)outAddrAC + (pObj->pitch[0][1] * pObj->outStartY/2 + pObj->outStartX)/4;

    // Add Offset
    outAddrAY = (UInt32 *)outAddrAY + pObj->flipOffset[0][0]/4;
    outAddrAC = (UInt32 *)outAddrAC + pObj->flipOffset[0][1]/4;

    outAddrBY = (UInt32 *)outAddrBY + pObj->flipOffset[1][0]/4;
    outAddrBC = (UInt32 *)outAddrBC + pObj->flipOffset[1][1]/4;

    ispDrvSetCfg(pObj);

    if(ispDrvIsYUV422ILEFormat(pObj->inDataFmt) ||
            (FVID2_DF_BAYER_RAW == pObj->inDataFmt))
    {
        inAddr  = (UInt16*)inAddr + ((pObj->inPitch * pObj->inStartY) + (pObj->inStartX * 2))/2;
    }
    else
    {
        inAddr  = (UInt8*)inAddr + ((pObj->inPitch * pObj->inStartY) + pObj->inStartX);
        inAddrC = (UInt8*)inAddrC + ((pObj->inPitch * pObj->inStartY/2) + pObj->inStartX);
    }

    ipipeif_set_sdram_in_addr((UInt32)inAddr);

    if (ispDrvIsYUV422ILEFormat(pObj->inDataFmt) ||
            (FVID2_DF_BAYER_RAW == pObj->inDataFmt))
    {
        if (ispDrvIsYUV422ILEFormat(pObj->outDataFmt0))
        {
            retVal = issSetRszOutAddress(RESIZER_A,
                    RSZ_YUV422_RAW_RGB_OP,
                    outAddrAY,
                    pObj->pitch[0][0]);
        }
        else
        {
            retVal = issSetRszOutAddress(RESIZER_A,
                    RSZ_YUV420_Y_OP,
                    outAddrAY,
                    pObj->pitch[0][0]);

            retVal = issSetRszOutAddress(RESIZER_A,
                    RSZ_YUV420_C_OP,
                    outAddrAC,
                    pObj->pitch[0][1]);
        }

        if (2 == pObj->numStream)
        {
            if (ispDrvIsYUV422ILEFormat(pObj->outDataFmt1))
            {
                retVal = issSetRszOutAddress(RESIZER_B,
                        RSZ_YUV422_RAW_RGB_OP,
                        outAddrBY,
                        pObj->pitch[1][0]);
            }
            else
            {
                retVal = issSetRszOutAddress(RESIZER_B,
                        RSZ_YUV420_Y_OP,
                        outAddrBY,
                        pObj->pitch[1][0]);

                retVal = issSetRszOutAddress(RESIZER_B,
                        RSZ_YUV420_C_OP,
                        outAddrBC,
                        pObj->pitch[1][1]);
            }
        }
    }
    else
    {
        // Y resizing
        // YEN -> Y output enable and 422to420 conversion enabled
        // CEN -> C output disable
        rsz_cfg_yuv420_op(RESIZER_A, RSZ_YUV420_Y_OP_ONLY);

        retVal = issSetRszOutAddress(RESIZER_A,
                RSZ_YUV420_Y_OP,
                outAddrAY,
                pObj->pitch[0][0]);
        retVal = issSetRszOutAddress(RESIZER_B,
                RSZ_YUV420_Y_OP,
                outAddrBY,
                pObj->pitch[1][0]);

        pObj->usrCbFlag = 1;

        // Trigger RSZ and IPIPEIF
        rsz_start(RSZ_SINGLE);
        rsz_submodule_start(RSZ_SINGLE, RESIZER_A);

        if (2 == pObj->numStream)
        {
            rsz_submodule_start(RSZ_SINGLE, RESIZER_B);
        }
        else
        {
            rsz_submodule_start(RSZ_STOP, RESIZER_B);
        }

        if(pObj->inDataFmt == FVID2_DF_BAYER_RAW)
        {
            /*
             * IPIPE is bypassed for YUV input
             */
            ipipe_start(IPIPE_SINGLE);
        }

        ipipeif_start(IPIPEIF_SINGLE);

        Semaphore_pend(pObj->sem420,BIOS_WAIT_FOREVER);

        /*
         * Turn OFF RSZ,IPPIEIF and IPIPE before starting them
         */
        ipipeif_start(IPIPEIF_STOP);
        rsz_submodule_start(RSZ_STOP, RESIZER_A);
        rsz_submodule_start(RSZ_STOP, RESIZER_B);
        ipipe_start(IPIPE_STOP);

        gIss_ispCommonObj.curActIspObj = pObj;

        if (!pObj->isRszOvfl)
        {
            ipipeif_set_sdram_in_addr((UInt32)inAddrC);

            // UV resizing
            // YEN -> Y output disable
            // CEN -> C output enable and 422to420 conversion enabled
            rsz_cfg_yuv420_op(RESIZER_A, RSZ_YUV420_C_OP_ONLY);

            rszASetOutConfig(pObj->inWidth - 2,
                    (pObj->inHeight >> 1) - 2,
                    pObj->outWidth0,
                    pObj->outHeight0 >> 1);

            if(pObj->numStream == 2)
            {
                rszBSetOutConfig(pObj->inWidth - 2,
                        (pObj->inHeight >> 1) - 2,
                        pObj->outWidth1,
                        pObj->outHeight1 >> 1);
            }
            else
            {
                rszBSetOutConfig(pObj->inWidth - 2,
                        (pObj->inHeight >> 1) - 2,
                        pObj->outWidth0,
                        pObj->outHeight0 >> 1);
            }

            retVal = issSetRszOutAddress(RESIZER_A,
                    RSZ_YUV420_C_OP,
                    outAddrAC,
                    pObj->pitch[0][1]);
            retVal = issSetRszOutAddress(RESIZER_B,
                    RSZ_YUV420_C_OP,
                    outAddrBC,
                    pObj->pitch[1][1]);
        }
    }

    if (!pObj->isRszOvfl)
    {
        ipipeif_set_data_type(RAW_DATA);
        rsz_cfg_io_pixel_col(YUV420_IP_COL_C);                          // COL -> chroma

        // Trigger RSZ and IPIPEIF
        rsz_start(RSZ_SINGLE);
        rsz_submodule_start(RSZ_SINGLE, RESIZER_A);

        if(pObj->numStream == 2)
        {
            rsz_submodule_start(RSZ_SINGLE, RESIZER_B);
        }
        else
        {
            rsz_submodule_start(RSZ_STOP, RESIZER_B);
        }

        if(pObj->inDataFmt == FVID2_DF_BAYER_RAW)
        {
            /*
             * IPIPE is bypassed for YUV input
             */
            ipipe_start(IPIPE_SINGLE);
        }

        ipipeif_start(IPIPEIF_SINGLE);
    }

    return (retVal);
}


Void ispDrvCalcFlipOffset(Iss_IspObj *pObj)
{
    Bool flipH, flipV;

    switch (pObj->mirrorMode)
    {
        case 1:
            flipH = TRUE;
            flipV = FALSE;
            break;

        case 2:
            flipH = FALSE;
            flipV = TRUE;
            break;

        case 3:
            flipH = TRUE;
            flipV = TRUE;
            break;

        case 0:
        default:
            flipH = FALSE;
            flipV = FALSE;
            break;
    }

    pObj->flipOffset[0][0] = 0;
    pObj->flipOffset[0][1] = 0;
    pObj->flipOffset[1][0] = 0;
    pObj->flipOffset[1][1] = 0;

    if (flipV)
    {
        switch(pObj->outDataFmt0)
        {
            case FVID2_DF_YUV422I_UYVY:
            case FVID2_DF_YUV422I_YUYV:
            case FVID2_DF_YUV422I_YVYU:
            case FVID2_DF_YUV422I_VYUY:
                pObj->flipOffset[0][0] += pObj->pitch[0][0] * (pObj->outHeight0 - 1);
                break;

            default:
            case FVID2_DF_YUV420SP_UV:
            case FVID2_DF_YUV420SP_VU:
                pObj->flipOffset[0][0] += pObj->pitch[0][0] * (pObj->outHeight0 - 1);
                pObj->flipOffset[0][1] += pObj->pitch[0][1] * (pObj->outHeight0/2 - 1);
                break;
        }

        switch(pObj->outDataFmt1)
        {
            case FVID2_DF_YUV422I_UYVY:
            case FVID2_DF_YUV422I_YUYV:
            case FVID2_DF_YUV422I_YVYU:
            case FVID2_DF_YUV422I_VYUY:
                pObj->flipOffset[1][0] += pObj->pitch[1][0] * (pObj->outHeight1 - 1);
                break;

            default:
            case FVID2_DF_YUV420SP_UV:
            case FVID2_DF_YUV420SP_VU:
                pObj->flipOffset[1][0] += pObj->pitch[1][0] * (pObj->outHeight1 - 1);
                pObj->flipOffset[1][1] += pObj->pitch[1][1] * (pObj->outHeight1/2 - 1);
                break;
        }
    }

    if (flipH)
    {
        switch(pObj->outDataFmt0)
        {
            case FVID2_DF_YUV422I_UYVY:
            case FVID2_DF_YUV422I_YUYV:
            case FVID2_DF_YUV422I_YVYU:
            case FVID2_DF_YUV422I_VYUY:
                pObj->flipOffset[0][0] += (pObj->outWidth0 * 2 - 1);
                break;

            default:
            case FVID2_DF_YUV420SP_UV:
            case FVID2_DF_YUV420SP_VU:
                pObj->flipOffset[0][0] += (pObj->outWidth0 - 1);
                pObj->flipOffset[0][1] += (pObj->outWidth0 - 1);
                break;
        }

        switch(pObj->outDataFmt1)
        {
            case FVID2_DF_YUV422I_UYVY:
            case FVID2_DF_YUV422I_YUYV:
            case FVID2_DF_YUV422I_YVYU:
            case FVID2_DF_YUV422I_VYUY:
                pObj->flipOffset[1][0] += (pObj->outWidth1 * 2 - 1);
                break;

            default:
            case FVID2_DF_YUV420SP_UV:
            case FVID2_DF_YUV420SP_VU:
                pObj->flipOffset[1][0] += (pObj->outWidth1 - 1);
                pObj->flipOffset[1][1] += (pObj->outWidth1 - 1);
                break;
        }
    }
}

Void ispDrvSetOneShotMode(Iss_IspObj *pObj)
{
    // IPIPEIF register setting
    ipipeif_path_cfg_t ipipeif_cfg;
    ipipe_src_cfg_t    ipipe_cfg;

    ipipeif_cfg.ipipe_inpsrc                   = IPIPEIF_IPIPE_INPSRC_SDRAM_RAW;
    ipipeif_cfg.ipipe_ip_cfg.sdram_ip.clkdiv_m = pObj->createArgs.clkDivM;
    ipipeif_cfg.ipipe_ip_cfg.sdram_ip.clkdiv_n = pObj->createArgs.clkDivN;
    ipipeif_cfg.isif_inpsrc                    = IPIPEIF_ISIF_INPSRC_SDRAM_RAW;
    ipipeif_cfg.isif_ip_cfg.sdram_ip.clkdiv_m  = pObj->createArgs.clkDivM;
    ipipeif_cfg.isif_ip_cfg.sdram_ip.clkdiv_n  = pObj->createArgs.clkDivN;

    ipipeif_clk_config(&ipipeif_cfg);

    // IPIPE register setting
    ipipe_cfg.wrt                   = 1;
    ipipe_cfg.io_pixel_fmt          = IPIPE_YCRCB_INPUT_YCRCB_OUTPUT;
    ipipe_cfg.dims.vps              = 0;
    ipipe_cfg.dims.v_size           = pObj->inHeight;
    ipipe_cfg.dims.hps              = 0;
    ipipe_cfg.dims.hsz              = pObj->inWidth;
    ipipe_cfg.Even_line_Even_pixel  = IPIPE_PIXEL_COLOUR_B;
    ipipe_cfg.Even_line_Odd_pixel   = IPIPE_PIXEL_COLOUR_GB;
    ipipe_cfg.Odd_line_Even_pixel   = IPIPE_PIXEL_COLOUR_GR;
    ipipe_cfg.Odd_line_Odd_pixel    = IPIPE_PIXEL_COLOUR_R;

    ipipe_config_input_src(&ipipe_cfg);

    // RSZ register setting
    rsz_clock_enable(RESIZER_A,RSZ_CLK_ENABLE);
    rsz_clock_enable(RESIZER_B,RSZ_CLK_ENABLE);
    isp_enable_interrupt(ISP_RSZ_INT_DMA);

}

Void ispDrvSetCfg(Iss_IspObj *pObj)
{
    UInt32 adjustPixels;
    UInt32 clkWidth,clkHeight;
    float horzRatio,vertRatio,fracDiv;
    UInt32 clkDivN;
    ipipeif_frame_cfg_t ipipeif_cfg;
    ipipe_dims_t ipipe_dims_cfg;
    ipipeif_path_cfg_t ipipeif_clk_cfg;

    adjustPixels = 0;

    if (ispDrvIsYUV422ILEFormat(pObj->inDataFmt) ||
            (FVID2_DF_BAYER_RAW == pObj->inDataFmt))
    {
        /*
         * Input is either RAW or YUV422I
         * Make sure the startX is multiple of 16 so that the input address is 32 byte alligned
         */
        if((pObj->inStartX % 16) != 0)
        {
            adjustPixels = ((pObj->inStartX + 15)/16) * 16 - pObj->inStartX;
        }
    }
    else
    {
        /*
         * Input is YUV420 SP
         * Make sure the startX is multiple of 32 so that the input address is 32 byte alligned
         */
        if((pObj->inStartX % 32) != 0)
        {
            adjustPixels = ((pObj->inStartX + 31)/32) * 32 - pObj->inStartX;
        }
    }

    /*
     * If adjustPixels is non-zero then inWidth should be reduced by the same amount.
     */
    pObj->inStartX += adjustPixels;
    pObj->inWidth  -= adjustPixels;

    ipipeif_cfg.ppln_hs_interval  = (pObj->inWidth + 16) - 1;
    ipipeif_cfg.lpfr_vs_interval  = (pObj->inHeight + 16) - 1;
    ipipeif_cfg.hnum              = pObj->inWidth - 1;
    ipipeif_cfg.vnum              = pObj->inHeight - 1;
    ipipeif_cfg.adofs             = pObj->inPitch >> 5;

    // IPIPE size setting
    ipipe_dims_cfg.vps    = 0;
    ipipe_dims_cfg.v_size = pObj->inHeight;
    ipipe_dims_cfg.hps    = 0;
    ipipe_dims_cfg.hsz    = pObj->inWidth;
    ipipe_config_dimensions(&ipipe_dims_cfg);

    switch(pObj->inDataFmt)
    {
        default:
        case FVID2_DF_BAYER_RAW:
            {
                /* RAW input YUV output */

                // IPIPEIF reg
                ipipeif_set_input_src (IPIPEIF_CFG1_INPSRC2, IPIPEIF_IPIPE_INPSRC_SDRAM_RAW);
                ipipeif_cfg.unpack       = UNPACK_NORMAL_16_BITS_PIXEL;                     // UNPACK -> 16 bits / pixel

                // RSZ reg
                rsz_cfg_ip_port(RSZ_IP_IPIPE);                       // SEL -> IPIPE
                rsz_cfg_io_pixel_format(YUV422_IP_YUV420_OP);

                switch(pObj->outDataFmt0)
                {
                    case FVID2_DF_YUV422I_UYVY:
                    case FVID2_DF_YUV422I_YUYV:
                    case FVID2_DF_YUV422I_YVYU:
                    case FVID2_DF_YUV422I_VYUY:
                        {
                            // RSZA reg
                            // YEN -> Y output disable
                            // CEN -> C output disable
                            rsz_cfg_yuv420_op(RESIZER_A, RSZ_YUV422_OP);
                            break;
                        }

                    default:
                    case FVID2_DF_YUV420SP_UV:
                    case FVID2_DF_YUV420SP_VU:
                        {
                            // RSZA reg
                            // YEN -> Y output enable and 422to420 conversion enabled
                            // CEN -> C output enable and 422to420 conversion enabled
                            rsz_cfg_yuv420_op(RESIZER_A, RSZ_YUV420_YC_OP);
                            break;
                        }
                }

                switch(pObj->outDataFmt1)
                {
                    case FVID2_DF_YUV422I_UYVY:
                    case FVID2_DF_YUV422I_YUYV:
                    case FVID2_DF_YUV422I_YVYU:
                    case FVID2_DF_YUV422I_VYUY:
                        {
                            // RSZB reg
                            // YEN -> Y output disable
                            // CEN -> C output disable
                            rsz_cfg_yuv420_op(RESIZER_B, RSZ_YUV422_OP);
                            break;
                        }

                    default:
                    case FVID2_DF_YUV420SP_UV:
                    case FVID2_DF_YUV420SP_VU:
                        {
                            // RSZB reg
                            // YEN -> Y output enable and 422to420 conversion enabled
                            // CEN -> C output enable and 422to420 conversion enabled
                            rsz_cfg_yuv420_op(RESIZER_B, RSZ_YUV420_YC_OP);
                            break;
                        }
                }

                break;
            }

        case FVID2_DF_YUV422I_UYVY:
        case FVID2_DF_YUV422I_YUYV:
        case FVID2_DF_YUV422I_YVYU:
        case FVID2_DF_YUV422I_VYUY:
            {
                /* YUV 422 input and YUV output */

                // IPIPEIF reg
                ipipeif_set_input_src (IPIPEIF_CFG1_INPSRC2, IPIPEIF_IPIPE_INPSRC_SDRAM_YUV);
                ipipeif_cfg.unpack       = UNPACK_NORMAL_16_BITS_PIXEL;  // UNPACK -> 16 bits / pixel

                // RSZ reg
                rsz_cfg_ip_port(RSZ_IP_IPIPEIF);                       // SEL -> IPIPEIF
                rsz_cfg_io_pixel_format(YUV422_IP_YUV420_OP);

                switch(pObj->outDataFmt0)
                {
                    case FVID2_DF_YUV422I_UYVY:
                    case FVID2_DF_YUV422I_YUYV:
                    case FVID2_DF_YUV422I_YVYU:
                    case FVID2_DF_YUV422I_VYUY:
                        {
                            // RSZA reg
                            // YEN -> Y output disable
                            // CEN -> C output disable
                            rsz_cfg_yuv420_op(RESIZER_A, RSZ_YUV422_OP);
                            break;
                        }

                    default:
                    case FVID2_DF_YUV420SP_UV:
                    case FVID2_DF_YUV420SP_VU:
                        {
                            // RSZA reg
                            // YEN -> Y output enable and 422to420 conversion enabled
                            // CEN -> C output enable and 422to420 conversion enabled
                            rsz_cfg_yuv420_op(RESIZER_A, RSZ_YUV420_YC_OP);
                            break;
                        }
                }

                switch(pObj->outDataFmt1)
                {
                    case FVID2_DF_YUV422I_UYVY:
                    case FVID2_DF_YUV422I_YUYV:
                    case FVID2_DF_YUV422I_YVYU:
                    case FVID2_DF_YUV422I_VYUY:
                        {
                            // RSZB reg
                            // YEN -> Y output disable
                            // CEN -> C output disable
                            rsz_cfg_yuv420_op(RESIZER_B, RSZ_YUV422_OP);
                            break;
                        }

                    default:
                    case FVID2_DF_YUV420SP_UV:
                    case FVID2_DF_YUV420SP_VU:
                        {
                            // RSZB reg
                            // YEN -> Y output enable and 422to420 conversion enabled
                            // CEN -> C output enable and 422to420 conversion enabled
                            rsz_cfg_yuv420_op(RESIZER_B, RSZ_YUV420_YC_OP);
                            break;
                        }
                }

                break;
            }

        case FVID2_DF_YUV420SP_UV:
        case FVID2_DF_YUV420SP_VU:
            {
                /* YUV 420 input and YUV 420 output */

                // IPIPEIF reg
                ipipeif_set_input_src (IPIPEIF_CFG1_INPSRC2, IPIPEIF_IPIPE_INPSRC_SDRAM_RAW);
                ipipeif_set_input_src (IPIPEIF_CFG1_INPSRC1, IPIPEIF_IPIPE_INPSRC_SDRAM_RAW);
                ipipeif_cfg.unpack       = UNPACK_PACK_8_BITS_PIXEL_LINEAR;  // UNPACK -> 8 bits / pixel

                ipipeif_set_data_type(YCBCR_16_BIT_DATA);

                // RSZ reg
                rsz_cfg_ip_port(RSZ_IP_IPIPEIF);                       // SEL -> IPIPEIF
                // RAW -> Flipping preserves YCbCr format
                // IN420 -> YUV4:2:0 is input
                // COL -> Luma
                rsz_cfg_io_pixel_format(YUV420_IP_YUV420_OP);
                break;
            }
    }
    ipipeif_config_frame(&ipipeif_cfg);

    if(pObj->numStream == 2)
    {
        rszASetOutConfig(pObj->inWidth - 2,
                pObj->inHeight - 4,
                pObj->outWidth0,
                pObj->outHeight0);

        rszBSetOutConfig(pObj->inWidth - 2,
                pObj->inHeight - 4,
                pObj->outWidth1,
                pObj->outHeight1);
        clkWidth = 0;

        if((pObj->outWidth0 > pObj->inWidth) ||
                (pObj->outWidth1 > pObj->inWidth))
        {
            /*
             * Upscaling case
             */
            if(pObj->outWidth0 > pObj->outWidth1)
                clkWidth = pObj->outWidth0;
            else
                clkWidth = pObj->outWidth1;
        }

        clkHeight = 0;

        if((pObj->outHeight0 > pObj->inHeight) ||
                (pObj->outHeight1 > pObj->inHeight))
        {
            /*
             * Upscaling case
             */
            if(pObj->outHeight0 > pObj->outHeight1)
                clkHeight = pObj->outHeight0;
            else
                clkHeight = pObj->outHeight1;
        }
    }
    else
    {
        rszASetOutConfig(pObj->inWidth - 2,
                pObj->inHeight - 4,
                pObj->outWidth0,
                pObj->outHeight0);

        /* Set the resolution params even if RSZ B is going to be disabled */
        rszBSetOutConfig(pObj->inWidth - 2,
                pObj->inHeight - 4,
                pObj->outWidth0,
                pObj->outHeight0);
        clkWidth = 0;

        if(pObj->outWidth0 > pObj->inWidth)
        {
            /*
             * Upscaling case
             */
            clkWidth = pObj->outWidth0;
        }

        clkHeight = 0;

        if(pObj->outHeight0 > pObj->inHeight)
        {
            /*
             * Upscaling case
             */
            clkHeight = pObj->outHeight0;
        }
    }

    horzRatio = 1.0;
    vertRatio = 1.0;

    if(clkWidth > 0)
    {
        horzRatio = (float)clkWidth/pObj->inWidth;
    }

    if(clkHeight > 0)
    {
        vertRatio = (float)clkHeight/pObj->inHeight;
    }

    fracDiv = (horzRatio * vertRatio);

    clkDivN = (UInt32)((float)pObj->createArgs.clkDivN * fracDiv);

    /* Clk reduction is required here. Otherwise Reszier overflow is observed*/
    ipipeif_clk_cfg.ipipe_inpsrc                   = IPIPEIF_IPIPE_INPSRC_SDRAM_RAW;
    ipipeif_clk_cfg.ipipe_ip_cfg.sdram_ip.clkdiv_m = pObj->createArgs.clkDivM;
    ipipeif_clk_cfg.ipipe_ip_cfg.sdram_ip.clkdiv_n = clkDivN;
    ipipeif_clk_cfg.isif_inpsrc                    = IPIPEIF_ISIF_INPSRC_SDRAM_RAW;
    ipipeif_clk_cfg.isif_ip_cfg.sdram_ip.clkdiv_m  = pObj->createArgs.clkDivM;
    ipipeif_clk_cfg.isif_ip_cfg.sdram_ip.clkdiv_n  = clkDivN;

    ipipeif_clk_config(&ipipeif_clk_cfg);
}

Iss_IspObj *issIspAllocObj()
{
    UInt32 cnt;
    Iss_IspObj *pObj = NULL;

    for (cnt = 0; cnt < ISS_ISP_INST_HANDLES; cnt ++)
    {
        if (gIss_ispCommonObj.isAllocated[cnt] == FALSE)
        {
            pObj = &gIss_ispCommonObj.ispObj[cnt];
            gIss_ispCommonObj.isAllocated[cnt] = TRUE;
            break;
        }
    }

    return (pObj);
}

Void issIspFreeObj(Iss_IspObj *pObj)
{
    UInt32 cnt;

    for (cnt = 0; cnt < ISS_ISP_INST_HANDLES; cnt ++)
    {
        if (&gIss_ispCommonObj.ispObj[cnt] == pObj)
        {
            gIss_ispCommonObj.isAllocated[cnt] = FALSE;
        }
    }
}
/* function to print all the reg values */
Void issIspLink_printIpipeISReg()
{
    Vps_printf("\n\n=====================================================\n\n");
    Vps_printf("ISP: REG 1 IPIPEIF %x\n",  ipipeif_reg->IPIPEIF_ENABLE);
    Vps_printf("ISP: REG 2 CFG1    %x\n",  ipipeif_reg->CFG1);
    Vps_printf("ISP: REG 3 PPLN    %x\n",  ipipeif_reg->PPLN);
    Vps_printf("ISP: REG 4 LPFR    %x\n",  ipipeif_reg->LPFR);
    Vps_printf("ISP: REG 5 HNUM    %x\n",  ipipeif_reg->HNUM);
    Vps_printf("ISP: REG 6 VNUM    %x\n",  ipipeif_reg->VNUM);
    Vps_printf("ISP: REG 7 ADDRU   %x\n",  ipipeif_reg->ADDRU);
    Vps_printf("ISP: REG 8 ADDRL   %x\n",  ipipeif_reg->ADDRL);
    Vps_printf("ISP: REG 9 ADOFS   %x\n",  ipipeif_reg->ADOFS);
    Vps_printf("ISP: REG 10 RSZ     %x\n",  ipipeif_reg->RSZ);
    Vps_printf("ISP: REG 11 GAIN    %x\n",  ipipeif_reg->GAIN);
    Vps_printf("ISP: REG 12 DPCM    %x\n",  ipipeif_reg->DPCM);
    Vps_printf("ISP: REG 13 CFG2    %x\n",  ipipeif_reg->CFG2);
    Vps_printf("ISP: REG 14 INIRSZ  %x\n",  ipipeif_reg->INIRSZ);
    Vps_printf("ISP: REG 15 OCLIP   %x\n",  ipipeif_reg->OCLIP);
    Vps_printf("ISP: REG 16 UFERR   %x\n",  ipipeif_reg->UFERR);
    Vps_printf("ISP: REG 17 CLKDIV  %x\n",  ipipeif_reg->CLKDIV);
    Vps_printf("ISP: REG 18 DPC1    %x\n",  ipipeif_reg->DPC1);
    Vps_printf("ISP: REG 19 DPC2    %x\n",  ipipeif_reg->DPC2);
    Vps_printf("ISP: REG 20 DFSGVL  %x\n",  ipipeif_reg->DFSGVL);
    Vps_printf("ISP: REG 21 DFSGTH  %x\n",  ipipeif_reg->DFSGTH);
    Vps_printf("ISP: REG 22 RSZ2    %x\n",  ipipeif_reg->RSZ2);
    Vps_printf("ISP: REG 23 INIRSZ2 %x\n",  ipipeif_reg->INIRSZ2);
    Vps_printf("ISP: isif_reg SYNCEN %x\n",  isif_reg->SYNCEN);
    Vps_printf("ISP: isif_reg MODESET %x\n",  isif_reg->MODESET);
    Vps_printf("\n\n=====================================================\n\n");
}
