/** ==================================================================
 *  @file   issdrv_algTIaewb.c
 *
 *  @path    /proj/vsi/users/venu/DM812x/IPNetCam_rel_1_8/ti_tools/iss_02_bkup/packages/ti/psp/iss/drivers/alg/2A/src/
 *
 *  @desc   This  File contains.
 * ===================================================================
 *  Copyright (c) Texas Instruments Inc 2011, 2012
 *
 *  Use of this software is controlled by the terms and conditions found
 *  in the license agreement under which this software has been supplied
 * ===================================================================*/


#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <ti/psp/iss/drivers/alg/2A/inc/issdrv_algAewbPriv.h>
#include "ae_ti.h"
#include "TI_aewb.h"
#include <ti/psp/iss/drivers/alg/2A/inc/issdrv_algTIaewb.h>
//#include "Rfile.h"
#include <ti/psp/vps/common/vps_config.h>
#include "../../../../alg/aewb/mms2a/ae_mms2/fw/fw_ae_interface.h"
#include "alg_ti_flicker_detect.h"
#include <ti/psp/iss/alg/aewb/mms2a/awb_mms2/inc/awb_fw.h>

ALG_AewbObj gALG_TI_aewbObj;

ti2aControlParams_t gTi2aControlParams;
UInt32 gAewbStabilizeCnt = 0;

#ifdef DUMP_STILL_IMAGE_ADDITIONAL_INFO
#define STILL_ADDITIONAL_INFO_SIZE 1544
UInt32 gStillAdditionalInfo[STILL_ADDITIONAL_INFO_SIZE];
/*The variable is used to make sure h3a statistics
  are printed only once during a burst capture*/
volatile UInt32 gPrintH3aStatistics = 0;
#endif
#define ABS(x) ((x)<0 ? -(x) : (x))
#define SIGN(x) ((x)<0 ? -1 : 1)
#define LIMIT_MOVING_AVERAGE(x,y,z) ( ( (ABS(x-y)) > ((y*256)/z) ) ? ( (SIGN(x-y))*((y*256)/z)+y ) : (x) )

UInt32 Iss_captIsStillCapMode();
UInt32 Iss_captIsAewbStabilization();
UInt32 Iss_deviceGetSensorFrameRate();
FVID2_Standard Iss_deviceGetSensorMode();
UInt32 Iss_captIsTimelapseMode();
UInt32 CameraLink_isViewfinderMode();

extern UInt32 gBayerFormat;
int new_awb_data_available;

int *g_flickerMem 		= NULL; //algorithm persistent memory
IAEWB_Rgb *rgbData     	= NULL;
aewDataEntry *aew_data 	= NULL;
int aew_enable         	= AEW_ENABLE;
int aewbFrames         	= 0;
int awb_alg 			= MMS_VSP_AWB; //TI_DSPRND_AWB;//TI_VSP_AWB; //TI_DSPRND_AWB;
UInt32 gResetSensorGain = 0;

//UInt32 gCapture_mode_flag = 0;
UInt32 gCapture_cnt = 3;
UInt32 gAE_conv;

enum ae_calc_mode gcalc_mode_hist;            // AE calculation mode
enum ae_spot_weighting gae_spot_weighting = AE_SPOTWEIGHTING_CENTER;

Int16 gev_compensation = 256;

typedef enum{
	DRV_IMGS_SENSOR_MODE_640x480 = 0,
	DRV_IMGS_SENSOR_MODE_720x480,
	DRV_IMGS_SENSOR_MODE_800x600,
	DRV_IMGS_SENSOR_MODE_1024x768,
	DRV_IMGS_SENSOR_MODE_1280x720,
	DRV_IMGS_SENSOR_MODE_1280x960,
	DRV_IMGS_SENSOR_MODE_1280x1024,
	DRV_IMGS_SENSOR_MODE_1600x1200,
	DRV_IMGS_SENSOR_MODE_1920x1080,
	DRV_IMGS_SENSOR_MODE_2048x1536,
	DRV_IMGS_SENSOR_MODE_2592x1920
} DRV_IMGS_SENSOR_MODE;


#define FDC_ENABLED  1
#define FDC_DISABLED 0

#define DO_2A 1
#define NO_2A 0

//#define _PROFILE_AWB_ALGO_
#ifdef _PROFILE_AWB_ALGO_
  volatile uint32              awb_start;
  volatile uint32              awb_end;
  #include <xdc/runtime/Timestamp.h>
#endif

ti2a_output ti2a_output_params = {
  5500,
  2000,
  15000,
  2000,
  15000,
  50,
  1,
  0,
  {
    184,
    128,
    128,
    236,
    0,
    0,
    0,
    0,
    512
  },
  {
        256, 0,    0,
        0,   256,  0,
        0,   0,    256,
        0,   0,    0
  },
  {
        256, 0,    0,
        0,   256,  0,
        0,   0,    256,
        0,   0,    0
  }
};

ti2a_output ti2a_output_params_prev = {
  5500,
  2000,
  15000,
  2000,
  15000,
  50,
  1,
  0,
  {
    184,
    128,
    128,
    236,
    0,
    0,
    0,
    0,
    512
  },
  {
        256, 0,    0,
        0,   256,  0,
        0,   0,    256,
        0,   0,    0
  },
  {
        256, 0,    0,
        0,   256,  0,
        0,   0,    256,
        0,   0,    0
  }
};

awb_params ipipe_awb_gain_prev = {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    512
};

int32 rgb2rgb_1_output[9] = {    461, -190, -15,
                                 -25, 413, -132,
                                  11, -190, 435};
/*CCM for CT range 0k to 3335k*/
int32 rgb2rgb1_0k_3335k[9]     = {  369, -138,   25,
                                   -104,  437,  -77,
                                    -39, -267,  561};

/*CCM for CT range 3336k to 4616k*/
int32 rgb2rgb1_3336k_4615k[9]  = {  478, -199,  -23,
                                    -65,  428, -107,
                                      2, -242,  496};

/*CCM for CT range 4616k to 5950k*/
int32 rgb2rgb1_4616k_5950k[9]  = {  461, -190,  -15,
                                    -25,  413, -132,
                                     11, -190, 435};

/*CCM for CT range 5951k to 7090k*/
int32 rgb2rgb1_5951k_7090k[9]  = {  501, -234,  -12,
                                    -46,  464, -163,
                                      3, -198,  451};

/*CCM for CT range 7091k to 20000k*/
int32 rgb2rgb1_7091k_20000k[9] = {  509, -240,  -13,
                                    -52,  483, -175,
                                      0, -194,  450};
/*CCM for underwater red filter*/
int32 rgb2rgb1_red_filter[9]   = {  509, -240,  -13,
                                    -52,  453, -145,
                                    -50, -194,  500};
/*CCM for underwater magenta filter*/
int32 rgb2rgb1_magenta_filter[9] = {  356, -80,  -20,
                                      -25, 306,  -25,
                                      -10, -90,  356};


/* ===================================================================
 *  @func     ALG_aewbCreate
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void *ALG_aewbCreate(int aewbNumWinH,int aewbNumWinV,int aewbNumPix)
{
  struct fw_create_cofiguration AECreateIn; // This structure is dummy for now

  int retval;
  uint32 awb_version;
  struct mms_ae_version ae_version;

#ifdef ALG_AWB_DEBUG
  Vps_printf("DBG_AWB: ALG_aewbCreate\n");
#endif

  memset(&gALG_TI_aewbObj, 0, sizeof(gALG_TI_aewbObj));
  
  if(rgbData == NULL)
	rgbData  = calloc(sizeof(IAEWB_Rgb), aewbNumWinH * aewbNumWinV);
  if(aew_data == NULL)
	aew_data = calloc(sizeof(aewDataEntry), (aewbNumWinH * aewbNumWinV + 7) >> 3);
  if(g_flickerMem == NULL)
      g_flickerMem = calloc(sizeof(int), 6*1024);

  gALG_TI_aewbObj.reduceShutter          = 100;
  gALG_TI_aewbObj.saldre                 = 0;
  gALG_TI_aewbObj.aewbType               = ALG_AEWB_AEWB;
  gALG_TI_aewbObj.env_50_60Hz            =  VIDEO_PAL;
  gALG_TI_aewbObj.flicker_detect         = FDC_DISABLED;

  gTi2aControlParams.update              = 0;
  gTi2aControlParams.flicker_sel         = 0;

  gTi2aControlParams.flickerFreq         = 0;
  gTi2aControlParams.minExposure         = 5000;
  gTi2aControlParams.maxExposure         = 16667;
  gTi2aControlParams.stepSize            = 200;
  gTi2aControlParams.aGainMin            = 1000;
  gTi2aControlParams.dGainMin            = 128;		//1024;		//128
  gTi2aControlParams.dGainMax            = 4092;
  gTi2aControlParams.targetBrightnessMin = 30;
  gTi2aControlParams.targetBrightnessMax = 50;
  gTi2aControlParams.targetBrightness    = 40;

#ifdef IMGS_ALTASENS_AL30210
  gTi2aControlParams.maxExposure         = 33333;
#elif defined IMGS_PANASONIC_MN34041
  gTi2aControlParams.minExposure         = 1000;
  gTi2aControlParams.maxExposure         = 32000;
  gTi2aControlParams.stepSize            = 200;
  gTi2aControlParams.aGainMin            = 1000;
  gTi2aControlParams.aGainMax            = 32000;
  gTi2aControlParams.dGainMax            = 4092 * 2;
#elif defined IMGS_MICRON_AR0331_WDR
  gTi2aControlParams.aGainMax            = 2000;
  gTi2aControlParams.minExposure         = 10000;		//5000;		//100
  gTi2aControlParams.stepSize            = 50;		//200;		//50
  gTi2aControlParams.aGainMin            = 100;		//1000;		//100
  gTi2aControlParams.maxExposure         = 20000; //WDR
#elif defined (IMGS_MICRON_AR0331) | defined (IMGS_MICRON_AR0330)
  gTi2aControlParams.aGainMax            = 8000;
  gTi2aControlParams.minExposure         = 100;		//5000;		//100
  gTi2aControlParams.stepSize            = 50;		//200;		//50
  gTi2aControlParams.aGainMin            = 100;		//1000;		//100
  gTi2aControlParams.maxExposure         = 33333; //16667;
#ifdef WDR_ON
	gTi2aControlParams.dGainMin 	   = 1024;
	gTi2aControlParams.dGainMax 	   = 1024;
	gTi2aControlParams.minExposure     = 237;
	gTi2aControlParams.maxExposure     = 33185;
#endif
#elif defined IMGS_SAMSUNG_S5K2P1
  gTi2aControlParams.aGainMax            = 16000;
  gTi2aControlParams.minExposure         = 30;

  /*
    Set minimum digital gain to 1X
    1X => ti2a_output_params.ipipe_awb_gain.dGain = 512
    => ti2a_output_params.ipipe_awb_gain.dGain * 2 = gTi2aControlParams.dGainMin = 1024
  */
  gTi2aControlParams.dGainMin            = 256;  // 1X
  gTi2aControlParams.dGainMax            = 512;  // 2X
  gTi2aControlParams.stepSize            = 200;
  gTi2aControlParams.aGainMin            = 1000;
  gTi2aControlParams.maxExposure         = 33333;  // (1/30)*1000*1000 (us)
#elif defined IMGS_ALTASENS_AL30210
  gTi2aControlParams.aGainMax            = 5600;
#elif defined IMGS_SONY_IMX104
  gTi2aControlParams.dGainMin            = 1024;		//1024;		//128
  gTi2aControlParams.minExposure         = 100;		//5000;		//100
  gTi2aControlParams.maxExposure         = 33333;
  gTi2aControlParams.dGainMax            = 8092;
  gTi2aControlParams.aGainMax            = 251000;
#ifdef WDR_ON
  gTi2aControlParams.minExposure         = 7155;
  gTi2aControlParams.maxExposure         = 32755;
  gTi2aControlParams.dGainMin            = 1024;
  gTi2aControlParams.dGainMax 		     = 1024;
  gTi2aControlParams.aGainMin            = 1679;//4.5dB
  gTi2aControlParams.aGainMax            = 6683;//16.5dB
#endif
#elif defined (IMGS_SONY_IMX136) | defined (IMGS_SONY_IMX140)
  gTi2aControlParams.dGainMin            = 1024;		//128
  gTi2aControlParams.aGainMax            = 256000; //251000;
  gTi2aControlParams.dGainMax            = 1024; //4092 * 2;
  gTi2aControlParams.maxExposure         = 33333;
#ifdef WDR_ON
  gTi2aControlParams.minExposure         = 4740;
  gTi2aControlParams.maxExposure         = 33184;
  gTi2aControlParams.dGainMin 		   = 1024;
  gTi2aControlParams.dGainMax 		   = 1024;
  gTi2aControlParams.aGainMin            = 1679;//4.5dB
  gTi2aControlParams.aGainMax            = 6683;//16.5dB
#endif
#elif defined IMGS_SONY_IMX122
  gTi2aControlParams.dGainMin            = 1024;		//128
  gTi2aControlParams.aGainMax            = 251000;
  gTi2aControlParams.dGainMax            = 4092 * 2;
  gTi2aControlParams.maxExposure         = 33333;
  gTi2aControlParams.flickerFreq         = 50000;
#elif defined IMGS_MICRON_MT9M034
  gTi2aControlParams.dGainMin            = 1024;		//1024;		//128
  gTi2aControlParams.dGainMax            = 8092;
#ifdef WDR_ON
  gTi2aControlParams.minExposure         = 4300;//100;		//5000;		//100
  gTi2aControlParams.maxExposure         = 22100;//33333;
  gTi2aControlParams.dGainMin 		   = 1024;
  gTi2aControlParams.dGainMax 		   = 1024;
#else
  gTi2aControlParams.minExposure         = 100;		//5000;		//100
  gTi2aControlParams.maxExposure         = 33333;
#endif
  gTi2aControlParams.aGainMax            = 230000;  // 2.88(DCG) * 7.96875(D) * 8(A)
#else
  gTi2aControlParams.aGainMax            = 32000;
#endif
  gTi2aControlParams.maxCapExposure      = 100000;

  gTi2aControlParams.aewbType            = ALG_AEWB_AEWB;

  gTi2aControlParams.day_night           = AE_DAY;
  gTi2aControlParams.blc                 = BACKLIGHT_LOW;
  gTi2aControlParams.brightness          = 128;
  gTi2aControlParams.contrast            = 128;
  gTi2aControlParams.sharpness           = 128;
  gTi2aControlParams.saturation          = 128;
  gTi2aControlParams.wbSceneMode         = AWB_AUTO;
  gTi2aControlParams.aeMeteringMode      = AE_CENTER;
  gTi2aControlParams.evCompensation      = 0;
  gTi2aControlParams.hue                 = 128;

  new_awb_data_available = 0;


  TI_2A_init_tables(aewbNumWinH, aewbNumWinV);
  //Initial AE

  gALG_TI_aewbObj.weight = TI_WEIGHTING_MATRIX;

  retval = auto_exposure_create((void*) &gALG_TI_aewbObj.handle_ae,(struct fw_create_cofiguration*) &AECreateIn);
  if(retval == -1) {
    OSA_ERROR("AE_MMS2 Create Failed()\n");
    return NULL;
  }

  auto_exposure_version(&ae_version);
  Vps_printf("AE LIBRARY VER : %d\n",ae_version.version);


  retval = auto_white_balance_create(&(gALG_TI_aewbObj.awb_ctx));
  if(retval == AWB_FW_ERROR) {
    Vps_printf("=== === === ERROR  === === ===  auto_white_balance_create FAILS \n");
    return NULL;
  }

	auto_white_balance_tuning_version(&awb_version);
	Vps_printf("AWB TUNING VER : %d\n",awb_version);
	auto_white_balance_version(&awb_version);
	Vps_printf("AWB LIBRARY VER : %d\n",awb_version);

  gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert  = aewbNumWinV;
  gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz  = aewbNumWinH;
  gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin   = aewbNumPix;
  gALG_TI_aewbObj.sensorMode = DRV_IMGS_SENSOR_MODE_1920x1080;

  retval = TI_2A_config(gALG_TI_aewbObj.flicker_detect, gALG_TI_aewbObj.saldre);
  if(retval == -1) {
    return NULL;
  }

  return &gALG_TI_aewbObj;
}

IAE_DynamicParams aeDynamicParams;

/* ===================================================================
 *  @func     TI_Increase_Exposure
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void TI_Increase_Exposure()
{
  if(((ti2a_output_params.sensorExposure + 500) <= aeDynamicParams.exposureTimeRange[0].max))
  {
    ti2a_output_params.sensorExposure += 500;
    ti2a_output_params.mask = 1;
  }
  else if(((ti2a_output_params.sensorGain + 100) <= aeDynamicParams.sensorGainRange[1].max))
  {
    ti2a_output_params.sensorGain += 100;
    ti2a_output_params.mask = 2;
  }
  else if((((ti2a_output_params.ipipe_awb_gain.dGain + 8) << 2) <= aeDynamicParams.ipipeGainRange[2].max))
  {
    ti2a_output_params.ipipe_awb_gain.dGain += 8;
    ti2a_output_params.mask = 4;
  }
#ifdef ALG_AEWB_DEBUG
  Vps_printf("sensor Exposure Time   :  < %5d > \n",ti2a_output_params.sensorExposure  );
  Vps_printf("Sensor Analog Gain     :  < %5d > \n",ti2a_output_params.sensorGain );
  Vps_printf("IPIPE Digital Gain     :  < %5d > \n",(ti2a_output_params.ipipe_awb_gain.dGain * 2) );
#endif
}
/* ===================================================================
 *  @func     TI_Decrease_Exposure
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void TI_Decrease_Exposure()
{
  if((((ti2a_output_params.ipipe_awb_gain.dGain - 8) << 2) >= aeDynamicParams.ipipeGainRange[2].min))
  {
    ti2a_output_params.ipipe_awb_gain.dGain -= 8;
    ti2a_output_params.mask = 4;
  }
  else if(((ti2a_output_params.sensorGain - 100) >= aeDynamicParams.sensorGainRange[1].min))
  {
    ti2a_output_params.sensorGain -= 100;
    ti2a_output_params.mask = 2;
  }
  else if(((ti2a_output_params.sensorExposure - 500) >= aeDynamicParams.exposureTimeRange[0].min))
  {
    ti2a_output_params.sensorExposure -= 500;
    ti2a_output_params.mask = 1;
  }
#ifdef ALG_AEWB_DEBUG
  Vps_printf("sensor Exposure Time   :  < %5d > \n",ti2a_output_params.sensorExposure  );
  Vps_printf("Sensor Analog Gain     :  < %5d > \n",ti2a_output_params.sensorGain );
  Vps_printf("IPIPE Digital Gain     :  < %5d > \n",(ti2a_output_params.ipipe_awb_gain.dGain * 2) );
#endif
}

struct ae_configuration cfg;
/* ===================================================================
 *  @func     TI_2A_config
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
int TI_2A_config(int flicker_detection, int saldre)
{

  int stepSize, min_exp;
  int retval;

  UInt32 IsStabMode;


#ifdef ALG_AWB_DEBUG
  Vps_printf("DBG_AWB: TI_2A_config\n");
#endif

  /* set stepSize based on input from Flicker detectiom and PAL/NTSC environment */
  if(flicker_detection == 1)
  {
    if(gALG_TI_aewbObj.env_50_60Hz == VIDEO_NTSC)
      stepSize = 8333;
    else
      stepSize = 10000;

    min_exp  = stepSize;
  }
  else
  {
    stepSize = 200;
    min_exp  = 5000;
  }


  if(gALG_TI_aewbObj.env_50_60Hz == VIDEO_NTSC && flicker_detection == 3){
    min_exp = 8333;
    stepSize = (8333*gALG_TI_aewbObj.reduceShutter)/100;
  }
  else if(gALG_TI_aewbObj.env_50_60Hz == VIDEO_PAL && flicker_detection == 2){
    min_exp = 10000;
    stepSize = 10000;
  }

  if (flicker_detection == 0)
  {
    if (gTi2aControlParams.flickerFreq == 0)
    {
      min_exp = gTi2aControlParams.minExposure;
      stepSize = gTi2aControlParams.stepSize ;
    }
    else
    {
      min_exp = ((500000)/gTi2aControlParams.flickerFreq);

      if (min_exp < gTi2aControlParams.minExposure)
      {
          min_exp =  gTi2aControlParams.minExposure + 1;
      }

      if (min_exp > gTi2aControlParams.maxExposure)
      {
          min_exp =  gTi2aControlParams.maxExposure - 1;
      }
      stepSize = min_exp;

    }
  }


#ifdef ALG_AEWB_DEBUG
  Vps_printf("min_exp = %d, stepSize = %d\n", min_exp, stepSize);
#endif
  if (flicker_detection != 0)
  {
    ti2a_output_params.sensorExposure = min_exp;
  }

  memcpy( (void *)&gALG_TI_aewbObj.AWB_InArgs.statMat,
    (void *)&gALG_TI_aewbObj.IAEWB_StatMatdata,
    sizeof(IAEWB_StatMat) );

  IsStabMode = Iss_captIsAewbStabilization();
  if (IsStabMode){
    cfg.calc_mode = AE_CALC_MODE_PREVIEW_FAST;
    //Vps_printf("AE_CALC_MODE_PREVIEW_FAST %d\n" , 1);
    gAE_conv = 0;

    //Save last control AE mode
    gcalc_mode_hist = AE_CALC_MODE_PREVIEW_FAST;
  } else{
    cfg.calc_mode = AE_CALC_MODE_PREVIEW;
    //Vps_printf("AE_CALC_MODE_PREVIEW %d\n" , 1);

    //Save last control AE mode
    gcalc_mode_hist = AE_CALC_MODE_PREVIEW;
  }


  // Configuration parameters
  cfg.ctrl_mode = AE_CTRL_MODE_NORMAL;
  cfg.exp_mode  = AE_EXP_MODE_AUTO;
  cfg.view_mode = AE_VIEW_MODE_VID;  //Video tuning will be used!
  cfg.flash_calc_mode = AE_FLASH_MODE_LED;// Flash is not available!
  cfg.flash_index     = 0; // Flash is not available!
  cfg.EV_compensation = gev_compensation;
  cfg.speed           = 0; // to be developed
  cfg.history         = 0; // to be developed
  cfg.framerate       = Iss_deviceGetSensorFrameRate();
  cfg.mains_frequency = AE_MAINS_50HZ;// Flicker may be implemented in the future.
  cfg.manual_params.shutter = 0;
  cfg.manual_params.again = 0xFFFF;// Disable value is 0xFFFF
  cfg.manual_params.mask = 0;
  cfg.manual_params.dgain = 0;
  cfg.manual_params.aperture = 0;
  cfg.limits.aperture    = 0;// Not used.
  cfg.limits.min_again   = gTi2aControlParams.aGainMin;
  cfg.limits.max_again   = gTi2aControlParams.aGainMax;
  cfg.limits.min_dgain   = gTi2aControlParams.dGainMin;
  cfg.limits.max_dgain   = gTi2aControlParams.dGainMax;
  cfg.limits.min_shutter = gTi2aControlParams.minExposure;
  cfg.limits.max_shutter = gTi2aControlParams.maxExposure;
  cfg.limits_cap.aperture    = 0;// Not used.
  cfg.limits_cap.min_again   = gTi2aControlParams.aGainMin; //Not used.
  cfg.limits_cap.max_again   = gTi2aControlParams.aGainMax; //Not used.
  cfg.limits_cap.min_dgain   = gTi2aControlParams.dGainMin; //Not used.
  cfg.limits_cap.max_dgain   = gTi2aControlParams.dGainMax; //Not used.
  cfg.limits_cap.min_shutter = gTi2aControlParams.minExposure; //Not used.
  cfg.limits_cap.max_shutter = gTi2aControlParams.maxCapExposure;
  cfg.manual_spot_weighting = gae_spot_weighting;
  cfg.rows_in_pax        = 0; // needed for flicker detection. Not used!
  cfg.row_time_10ns      = 0; // needed for flicker detection. Not used!
  cfg.ver_win_count      = 0; // needed for flicker detection. Not used!
  cfg.hor_win_count      = 0; // needed for flicker detection. Not used!
  cfg.draft2fine_sens    = 0x100; // Sensor mode (sensitivity) 0x100 is capture sensitivity
  cfg.bracketing_sequence = AE_BRACKETING_SEQUENCE_N_MIN_MAX; // Not used.
  cfg.bracketing_number   = 0; // Not used.
  cfg.bracketing_step     = 0; // Not used.
  cfg.iso_base     = 100; // This is not used and must be check again.
  cfg.gesture_data.enable = 0;// Not used.
  cfg.gesture_data.MaxTH = 0;// Not used.
  cfg.gesture_data.MinTH = 0;// Not used.
  cfg.gesture_data.delay_output_change = 0;// Not used.
  cfg.gesture_data.freeze_output_change = 0;// Not used.
  cfg.gesture_data.freeze_output_start = 0;// Not used.
  cfg.gesture_data.gesture_detected = 0;// Not used.

  retval = auto_exposure_control((IAE_Handle)gALG_TI_aewbObj.handle_ae, (struct ae_configuration*)&cfg);
  if(retval == 1) {
    OSA_ERROR("AE_MMS Control Failed\n");
    return -1;
  }

  if(flicker_detection == 1) ti2a_output_params.sensorExposure = 5000;


  return 0;
}

#ifdef ALG_AEWB_DEBUG
static void H3A_DEBUG_PRINT()
{
	int i,j,cnt;
	static int ShowCnt=0;
	char line_buffer[200];
	char word_buffer[20];
 
	ShowCnt ++;
	if(ShowCnt == 10)
	{	
		Vps_printf("pixCtWin=%d,WinVNum=%d,WinHNum=%d,\n",
			gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin,
			gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert,
			gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz);
		ShowCnt = 0;
		Vps_printf("\n******* H3A DUMP Start ********\n");
		Vps_printf("=====R Data=====\n");
		cnt=0;
		for(i=0; i< gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert; i++)
		{
			sprintf(line_buffer,"[%3d]",i);
			for(j=0;j< gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz; j++) {
				sprintf(word_buffer,"%5u, ",rgbData[cnt+j ].r);
				strcat(line_buffer,word_buffer);
			}
			Vps_printf("%s",line_buffer);
			cnt=cnt+gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz; 
		}

		Vps_printf("=====G Data=====\n");
		cnt=0;
		for(i=0; i< gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert; i++)
		{
			sprintf(line_buffer,"[%3d]",i);
			for(j=0;j< gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz; j++) {
				sprintf(word_buffer,"%5u, ",rgbData[cnt+j].g);
				strcat(line_buffer,word_buffer);
			}
			Vps_printf("%s",line_buffer);
			cnt=cnt+gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz; 
		}
		Vps_printf("=====B Data=====\n");
		cnt=0;
		for(i=0; i< gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert; i++)
		{
			sprintf(line_buffer,"[%3d]",i);
			for(j=0;j< gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz; j++) {
				sprintf(word_buffer,"%5u, ",rgbData[cnt+j].b);
				strcat(line_buffer,word_buffer);
			}
			Vps_printf("%s",line_buffer);
			cnt=cnt+gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz; 
		}
		Vps_printf("\n******* H3A DUMP END ********\n");
	}	
	
}
#endif

int accValue[4];
/* ===================================================================
 *  @func     GETTING_RGB_BLOCK_VALUE
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
static void GETTING_RGB_BLOCK_VALUE(unsigned short * BLOCK_DATA_ADDR,
            IAEWB_Rgb *rgbData, aewDataEntry *aew_data, int shift)
{
  unsigned short i,j,k, numWin, idx1, idx2;
  Uint8 *curAewbAddr;
  H3aAewbOutUnsatBlkCntOverlay *pAewbUnsatBlk;
  H3aAewbOutSumModeOverlay *pAewbWinData;
  int aew_win_vt_cnt = gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert;
  int aew_win_hz_cnt = gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz;

  curAewbAddr = (Uint8*)BLOCK_DATA_ADDR;
  numWin=0;

#ifdef ALG_AEWB_DEBUG
  accValue[0]=accValue[1]=accValue[2]=accValue[3]=0;
#endif
  /*skip the first row of black window*/
  curAewbAddr += (aew_win_hz_cnt/8) * (sizeof(H3aAewbOutSumModeOverlay) * 8 + sizeof(H3aAewbOutUnsatBlkCntOverlay));

#ifdef ALG_AWB_DEBUG
    Vps_printf("DBG_AWB: IAEWB_StatMatdata %d %d %d\n", gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz,
                                                        gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert,
                                                        gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin);
#endif

  for(i=0;i<aew_win_vt_cnt; i++) {
    for(j=0;j<aew_win_hz_cnt; j++) {

      pAewbWinData = (H3aAewbOutSumModeOverlay *)curAewbAddr;

      idx1 = numWin/8;
      idx2 = numWin%8;

      aew_data[idx1].window_data[idx2][0] = pAewbWinData->subSampleAcc[0];
      aew_data[idx1].window_data[idx2][1] = pAewbWinData->subSampleAcc[1];
      aew_data[idx1].window_data[idx2][2] = pAewbWinData->subSampleAcc[2];
      aew_data[idx1].window_data[idx2][3] = pAewbWinData->subSampleAcc[3];

#ifdef ALG_AWB_DEBUG
    Vps_printf("DBG_AWB: idx1 %d idx2 %d numWin %d\n",idx1,idx2,numWin);
    Vps_printf("DBG_AWB: subSampleAcc %d %d %d %d\n",pAewbWinData->subSampleAcc[0],
                                            pAewbWinData->subSampleAcc[1],
                                            pAewbWinData->subSampleAcc[2],
                                            pAewbWinData->subSampleAcc[3]);
#endif

#ifdef ALG_AEWB_DEBUG
      accValue[0] += pAewbWinData->subSampleAcc[0];
      accValue[1] += pAewbWinData->subSampleAcc[1];
      accValue[2] += pAewbWinData->subSampleAcc[2];
      accValue[3] += pAewbWinData->subSampleAcc[3];
#endif
      curAewbAddr += sizeof(H3aAewbOutSumModeOverlay);

      numWin++;

      if(numWin%8==0) {
        pAewbUnsatBlk = (H3aAewbOutUnsatBlkCntOverlay*)curAewbAddr;

        for(k=0; k<8;k++){
          aew_data[idx1].unsat_block_ct[k] = pAewbUnsatBlk->unsatCount[k];
            #ifdef ALG_AWB_DEBUG
                Vps_printf("DBG_AWB: k %d unsatCount %d\n",k,pAewbUnsatBlk->unsatCount[k]);
            #endif
        }
        curAewbAddr += sizeof(H3aAewbOutUnsatBlkCntOverlay);
      }
    }
    //curAewbAddr = (Uint8*)OSA_align( (Uint32)curAewbAddr, 32);
    //curAewbAddr = (Uint8*)(( (Uint32)curAewbAddr+ 31) & (~32));

  }

#ifdef IMGS_SAMSUNG_S5K2P1
  if(gBayerFormat == 0)
  {
    for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
      for(j = 0; j < 8; j ++){
        rgbData[i * 8 + j].r = aew_data[i].window_data[j][1] >> shift;
        rgbData[i * 8 + j].b = aew_data[i].window_data[j][2] >> shift;
        rgbData[i * 8 + j].g = (aew_data[i].window_data[j][0]
        + aew_data[i].window_data[j][3]+ 1) >> (1 + shift) ;
      }
    }
  }
  else
  {
    for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
      for(j = 0; j < 8; j ++){
        rgbData[i * 8 + j].r = aew_data[i].window_data[j][2] >> shift;
        rgbData[i * 8 + j].b = aew_data[i].window_data[j][1] >> shift;
        rgbData[i * 8 + j].g = (aew_data[i].window_data[j][3]
        + aew_data[i].window_data[j][0]+ 1) >> (1 + shift) ;
      }
    }
  }
#endif
#ifdef IMGS_SONY_IMX104
  for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
    for(j = 0; j < 8; j ++){
      rgbData[i * 8 + j].r = aew_data[i].window_data[j][2] >> shift;
      rgbData[i * 8 + j].b = aew_data[i].window_data[j][1] >> shift;
      rgbData[i * 8 + j].g = (aew_data[i].window_data[j][0]
      + aew_data[i].window_data[j][3]+ 1) >> (1 + shift) ;
    }
  }
#endif
#ifdef IMGS_PANASONIC_MN34041
 #ifndef MN34041_DATA_OP_LVDS324
  for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
    for(j = 0; j < 8; j ++){
      rgbData[i * 8 + j].r = aew_data[i].window_data[j][0] >> shift;
      rgbData[i * 8 + j].b = aew_data[i].window_data[j][3] >> shift;
      rgbData[i * 8 + j].g = (aew_data[i].window_data[j][1]
      + aew_data[i].window_data[j][2]+ 1) >> (1 + shift) ;

    }
  }
 #endif
#endif
#ifdef IMGS_ALTASENS_AL30210
  for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
    for(j = 0; j < 8; j ++){
      rgbData[i * 8 + j].r = aew_data[i].window_data[j][0] >> shift;
      rgbData[i * 8 + j].b = aew_data[i].window_data[j][3] >> shift;
      rgbData[i * 8 + j].g = (aew_data[i].window_data[j][1]
      + aew_data[i].window_data[j][2]+ 1) >> (1 + shift) ;
    	}
  }
#endif
#ifdef IMGS_OMNIVISION_OV2715
  for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
    for(j = 0; j < 8; j ++){
      rgbData[i * 8 + j].r = aew_data[i].window_data[j][3] >> shift;
      rgbData[i * 8 + j].b = aew_data[i].window_data[j][0] >> shift;
      rgbData[i * 8 + j].g = (aew_data[i].window_data[j][1]
      + aew_data[i].window_data[j][2]+ 1) >> (1 + shift) ;
    	}
  }
#endif
#if defined(IMGS_SONY_IMX136) 
  for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
    for(j = 0; j < 8; j ++){
      rgbData[i * 8 + j].r = aew_data[i].window_data[j][0] >> shift;
      rgbData[i * 8 + j].b = aew_data[i].window_data[j][3] >> shift;
      rgbData[i * 8 + j].g = (aew_data[i].window_data[j][1]
      + aew_data[i].window_data[j][2]+ 1) >> (1 + shift) ;
    	}
  }
#endif
#if defined(IMGS_SONY_IMX140)
  for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
    for(j = 0; j < 8; j ++){
      rgbData[i * 8 + j].r = aew_data[i].window_data[j][2] >> shift;
      rgbData[i * 8 + j].b = aew_data[i].window_data[j][1] >> shift;
      rgbData[i * 8 + j].g = (aew_data[i].window_data[j][0]
      + aew_data[i].window_data[j][3]+ 1) >> (1 + shift) ;
    	}
  }
#endif
#ifdef IMGS_SONY_IMX122 
  for(i = 0; i < (aew_win_hz_cnt * aew_win_vt_cnt)>>3;i ++){
    for(j = 0; j < 8; j ++){
      rgbData[i * 8 + j].r = aew_data[i].window_data[j][0] >> shift;
      rgbData[i * 8 + j].b = aew_data[i].window_data[j][3] >> shift;
      rgbData[i * 8 + j].g = (aew_data[i].window_data[j][1]
      + aew_data[i].window_data[j][2]+ 1) >> (1 + shift) ;
    	}
  }
#endif
#ifdef ALG_AEWB_DEBUG
  accValue[0] /= numWin*gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin;
  accValue[1] /= numWin*gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin;
  accValue[2] /= numWin*gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin;
  accValue[3] /= numWin*gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin;
  if(aewbFrames % (NUM_STEPS*32) == 0)
  {
    Vps_printf("AEWB: Avg Color      :  < %5d, %5d, %5d, %5d >\n",
    accValue[0], accValue[1], accValue[2], accValue[3]);
  }
#endif
}

/*AE repartitioning for night mode*/

void Ae_repartition_night_mode( UInt32 input_exposure,
                                UInt32 input_gain,
                                Uint32 * output_exposure,
                                UInt32 * output_gain)
{
     double exp_in,exp_out,gain_in,gain_out;

     exp_in  = (double)input_exposure;
     gain_in = (double)input_gain;

     exp_in  /= 1000;
     gain_in /= 1000;


     /*exposure 0 - 30ms gain 1x*/
     if((exp_in*gain_in) <= (30 * 1))
     {
         exp_out  = (exp_in*gain_in)/1;
         gain_out = 1;
     }
     /*exposure 30 gain 1x -2x*/
     else if(((exp_in*gain_in) > (30 * 1)) && ((exp_in*gain_in) <= (30 * 2)))
     {
         exp_out  = 30;
         gain_out = (exp_in*gain_in)/30;
     }
     /*exposure 30 - 60ms gain 2x*/
     else if(((exp_in*gain_in) > (30 * 2)) && ((exp_in*gain_in) <= (60 * 2)))
     {
         exp_out  = (exp_in*gain_in)/2;
         gain_out = 2;
     }
     /*exposure 60ms gain 2x - 4x*/
     else if(((exp_in*gain_in) > (60 * 2)) && ((exp_in*gain_in) <= (60 * 4)))
     {
         exp_out  = 60;
         gain_out = (exp_in*gain_in)/60;
     }
     /*exposure > 60ms gain 4x*/
     else if((exp_in*gain_in) > (60 * 4))
     {
         exp_out  = (exp_in*gain_in)/4;
         gain_out = 4;
     }


     * output_exposure = (UInt32)(exp_out * 1000);
     * output_gain     = (UInt32)(gain_out * 1000);

     return;
}

//convert H3A RGB data into the luma image (int16) the FD algorithm needed
/* ===================================================================
 *  @func     GETTING_RGB_BLOCK_VALUE_Y
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
static void GETTING_RGB_BLOCK_VALUE_Y(unsigned short * BLOCK_DATA_ADDR, short *y, int shift)
{
  unsigned short i,j, numWin;
  Uint8 *curAewbAddr;
#ifndef MN34041_DATA_OP_LVDS324
  H3aAewbOutSumModeOverlay *pAewbWinData;
#endif
  int aew_win_vt_cnt = gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert;
  int aew_win_hz_cnt = gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz;
  int r, g, b;

  curAewbAddr = (Uint8*)BLOCK_DATA_ADDR;
  numWin=0;

  /*skip the first row of black window*/
  curAewbAddr += (aew_win_hz_cnt/8) * (sizeof(H3aAewbOutSumModeOverlay) * 8 + sizeof(H3aAewbOutUnsatBlkCntOverlay));

  for(i=0;i<aew_win_vt_cnt; i++)
  {
    for(j=0;j<aew_win_hz_cnt; j++)
    {
#ifndef MN34041_DATA_OP_LVDS324
      pAewbWinData = (H3aAewbOutSumModeOverlay *)curAewbAddr;
#endif

#ifdef IMGS_PANASONIC_MN34041
#ifdef MN34041_DATA_OP_FPGA
      g = (pAewbWinData->subSampleAcc[1] + pAewbWinData->subSampleAcc[2]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[3] >> shift;
      b = pAewbWinData->subSampleAcc[0] >> shift;
#endif
/*#ifdef MN34041_DATA_OP_LVDS324
      g = (pAewbWinData->subSampleAcc[0] + pAewbWinData->subSampleAcc[3]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[1] >> shift;
      b = pAewbWinData->subSampleAcc[2] >> shift;
#endif*/
#elif defined (IMGS_SONY_IMX136) | defined(IMGS_SONY_IMX140)
      g = (pAewbWinData->subSampleAcc[1] + pAewbWinData->subSampleAcc[2]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[0] >> shift;
      b = pAewbWinData->subSampleAcc[3] >> shift;
#elif defined IMGS_ALTASENS_AL30210
      g = (pAewbWinData->subSampleAcc[1] + pAewbWinData->subSampleAcc[2]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[0] >> shift;
      b = pAewbWinData->subSampleAcc[3] >> shift;
#elif defined IMGS_OMNIVISION_OV2715
      g = (pAewbWinData->subSampleAcc[1] + pAewbWinData->subSampleAcc[2]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[3] >> shift;
      b = pAewbWinData->subSampleAcc[0] >> shift;
#elif defined IMGS_SONY_IMX104
      g = (pAewbWinData->subSampleAcc[0] + pAewbWinData->subSampleAcc[3]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[2] >> shift;
      b = pAewbWinData->subSampleAcc[1] >> shift;
#elif defined IMGS_SONY_IMX122
      g = (pAewbWinData->subSampleAcc[1] + pAewbWinData->subSampleAcc[2]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[0] >> shift;
      b = pAewbWinData->subSampleAcc[3] >> shift;
#else
      g = (pAewbWinData->subSampleAcc[0] + pAewbWinData->subSampleAcc[3]) >> (1+shift);
      r = pAewbWinData->subSampleAcc[1] >> shift;
      b = pAewbWinData->subSampleAcc[2] >> shift;
#endif
      *y++ = ((0x4D * r) + (0x96 * g) + (0x1D * b) + 128 ) / 256;

      curAewbAddr += sizeof(H3aAewbOutUnsatBlkCntOverlay);

      numWin++;

      if(numWin%8==0) {
        curAewbAddr += sizeof(H3aAewbOutUnsatBlkCntOverlay);
      }
    }
    //curAewbAddr = (Uint8*)(( (Uint32)curAewbAddr+ 31) & (~32));
  }
}


TI2A_FDC_Create()
{
}
static int flicker_detect_complete = 0;

/* ===================================================================
 *  @func     TI2A_FDC_Process
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
int TI2A_FDC_Process(void *h3aDataVirtAddr)
{

  /*-------------------------------------------------------------------------*/
  /*Some of the code has to be create time.                                  */
  /*-------------------------------------------------------------------------*/
  static int frame_cnt = -1;
  frame_cnt++;

  flicker_ret_st ret_st;
  static int exp;
  /*-------------------------------------------------------------------------*/
  /*hard coding of window size of h3a need to check with kumar               */
  /*-------------------------------------------------------------------------*/
  int w=16, h=32; //H3A AEWB has w x h windows
  static int row_time;
  static int pinp;
  static int h3aWinHeight;
  static int fd_res_support = 1;
  Int16 h3a[512]; //h3a luma image for FD algorithm
  /*-------------------------------------------------------------------------*/
  /*pass h3a virtual address                                                 */
  /*-------------------------------------------------------------------------*/
  Int16 *pAddr = (Int16*)h3aDataVirtAddr;
  Int32 expG;
  int   fd_gain;
  int   dGain;
  static int stab_count = 0;
  static int flicker_detect_enable = 0;
  static int frame_cnt_fd = 0;
  static int orig_exp = 0;
  static int orig_gain = 0;
  static int orig_dGain = 0;
  static int fd_count = 0;

#ifdef ALG_AWB_DEBUG
  Vps_printf("DBG_AWB: TI2A_FDC_Process\n");
#endif

  //Rfile_printf ("Flicker Detect trace begin ..... \n");
  if (frame_cnt == 0)
  {
    /* multi-resolution flicker detection support */
    if(gALG_TI_aewbObj.sensorMode == DRV_IMGS_SENSOR_MODE_720x480)
    {
      /* D1 */
      row_time = 47;
      pinp = 60;
      h3aWinHeight = 14;
    }
    else if (gALG_TI_aewbObj.sensorMode  == DRV_IMGS_SENSOR_MODE_1280x720)
    {
      /* 720p30 & 720p60*/
      row_time = (44*100)/gALG_TI_aewbObj.reduceShutter;
      pinp = 72;
      h3aWinHeight = 22;
    }
    else if (gALG_TI_aewbObj.sensorMode == DRV_IMGS_SENSOR_MODE_1920x1080)
    {
      /* 1080p */
      row_time = 29/2;
      pinp = 64;
      h3aWinHeight = 32;
    }
    else if (gALG_TI_aewbObj.sensorMode == DRV_IMGS_SENSOR_MODE_1280x960)
    {
      /* SXVGA */
      row_time = 34;
      pinp = 56;
      h3aWinHeight = 30;
    }
    else if (gALG_TI_aewbObj.sensorMode == DRV_IMGS_SENSOR_MODE_1600x1200)
    {
      /* 2MP */
      row_time = 26;
      pinp = 44;
      h3aWinHeight = 38;
    }
    else if (gALG_TI_aewbObj.sensorMode == DRV_IMGS_SENSOR_MODE_2048x1536)
    {
      /* 3MP */
      row_time = 31;
      pinp = 30;
      h3aWinHeight = 48;
    }
    else if (gALG_TI_aewbObj.sensorMode == DRV_IMGS_SENSOR_MODE_2592x1920)
    {
      /* 5MP */
      row_time = 36;
      pinp = 24;
      h3aWinHeight = 60;
    }
    else
    {
      /* FD resolution not currently supported, turn off FD */
      fd_res_support = 0;
    }

    //Rfile_printf("row_time = %d, pinp = %d, h3aWinHeight = %d\n", row_time, pinp, h3aWinHeight);
    if(fd_res_support)
    {
      //(1) first API call to get persistent memory size
      int ms = flicker_alloc(w, h);

      //(2) second API call; fail if ret!=0
      int ret = flicker_init((int32*)g_flickerMem, w, h,
          row_time,
          h3aWinHeight,   	//H3A window height is 22 for 720p, 14 for D1
          100);   			//threshold (100 is default)
      //Rfile_printf("FD init is done ret = %d\n", ret);
    }
  }

  /*-------------------------------------------------------------------------*/
  /* Wait for 2A to stabailize to enable FD                                  */
  /* Waits for the five frames of stable 2A                                  */
  /*-------------------------------------------------------------------------*/
  if (gALG_TI_aewbObj.AE_PrmsOut.shutter == gALG_TI_aewbObj.AE_PrmsIn.sensor_shutter &&
      gALG_TI_aewbObj.AE_PrmsOut.again == gALG_TI_aewbObj.AE_PrmsIn.sensor_again &&
      (flicker_detect_enable==0) && (flicker_detect_complete==0))
  {
    stab_count++;
    if(stab_count==5)
    {
      //Rfile_printf("2A is stabilized ... Enabling flicker detect ... \n");
      /*---------------------------------------------------------------------*/
      /*enable flicker detection only when 2A is stable                      */
      /*---------------------------------------------------------------------*/
      flicker_detect_enable = 1;
      frame_cnt_fd = 0;
    }
  }
  else
  {
    if (stab_count)
    {
      stab_count = 0;
      //Rfile_printf("waiting for 2A to stabilize ....  \n");
    }
  }


  /* begin Flicker Detection process */
  if (fd_res_support && flicker_detect_enable && (flicker_detect_complete==0))
  {

    //Rfile_printf("Running the flicker detection .... 2A will be disabled ...\n");
    /*---------------------------------------------------------------------*/
    /*set the exposure to the rounded to a 10ms to avoid any phase diff      */
    /*---------------------------------------------------------------------*/
    if(frame_cnt_fd == 0)
    {
      orig_exp = gALG_TI_aewbObj.AE_PrmsOut.shutter;
      orig_gain = gALG_TI_aewbObj.AE_PrmsOut.again;
      expG = orig_exp * orig_gain;

      //Rfile_printf("first stage ....org exp = %d, org gain = %d \n", orig_exp,orig_gain);
      //1st exposure
      exp = 10000*((gALG_TI_aewbObj.AE_PrmsOut.shutter + 5000)/10000);

      if(exp <10000) exp = 10000;

      fd_gain = expG / exp;

      orig_dGain = gALG_TI_aewbObj.AE_PrmsOut.dgain;

      if(fd_gain<1000)
      {
        dGain = 256*fd_gain/1000;  //nextAe->ipipeGain>> 2;1
        fd_gain = 1000;

        //ALG_aewbSetIpipeWb(&ipipe_awb_gain);
        ti2a_output_params.ipipe_awb_gain.dGain = dGain;
      }

      /* Set sensor exposure time and analog gain for 1st stage FD */
      //ALG_aewbSetSensorExposure(exp);
      //ALG_aewbSetSensorGain(fd_gain);

      ti2a_output_params.sensorGain           = fd_gain;
      ti2a_output_params.sensorExposure       = exp;
      ti2a_output_params.mask  = 3; //Enable for exp and aGain
      //Rfile_printf("first stage complete .... modified exp = %d, modified gain = %d \n", exp,fd_gain);
    }
    /*---------------------------------------------------------------------*/
    /*increase the exposure by 5ms to get 2nd sample point                 */
    /*---------------------------------------------------------------------*/
    else if(frame_cnt_fd == FD_FRAME_STEPS)
    {
      expG = gALG_TI_aewbObj.AE_PrmsOut.shutter * gALG_TI_aewbObj.AE_PrmsOut.again;
      //Rfile_printf("secound stage \n");

      //2nd exposure
      exp += 5000;

      fd_gain = expG / exp;

      //(3) API call for detection
      /* Pass H3A buffer to data conversion function */
      GETTING_RGB_BLOCK_VALUE_Y((unsigned short*)pAddr, h3a, 2);
      ret_st = flicker_detect((int32*)g_flickerMem, h3a, pinp, FLICKER_STATE_STAT);

      if(fd_gain<1000)
      {
        dGain = 256*fd_gain/1000;  //nextAe->ipipeGain>> 2;
        fd_gain = 1000;

        //ALG_aewbSetIpipeWb(&ipipe_awb_gain);
        ti2a_output_params.ipipe_awb_gain.dGain = dGain;
      }

      /* Set sensor exposure time and analog gain for 2nd stage FD (add 5 ms to 1st stage exposure time) */
      ti2a_output_params.sensorGain           =  fd_gain;
      ti2a_output_params.sensorExposure       = exp;
      ti2a_output_params.mask  = 3; //Enable for exp and aGain
      //Rfile_printf("secound stage complete .... modified exp = %d, modified gain = %d \n", exp,fd_gain);
      //ALG_aewbSetSensorExposure(exp);
      //ALG_aewbSetSensorGain(fd_gain);
    }
    /*---------------------------------------------------------------------*/
    /*set the sensor exp to 8.33 ms(120Hz) multiple                       */
    /*---------------------------------------------------------------------*/
    else if(frame_cnt_fd == FD_FRAME_STEPS*2)
    {
      expG = gALG_TI_aewbObj.AE_PrmsOut.shutter * gALG_TI_aewbObj.AE_PrmsOut.again;

      //Rfile_printf("third stage \n");
      //3rd exposure
      exp = 8333*((gALG_TI_aewbObj.AE_PrmsOut.shutter + 4167)/8333);

      if(exp <8333) exp = 8333;

      fd_gain = expG / exp;

      //(3) API call for detection
      /* Pass H3A buffer to data conversion function */
      GETTING_RGB_BLOCK_VALUE_Y((unsigned short*)pAddr, h3a, 2);
      ret_st = flicker_detect((int32*)g_flickerMem, h3a, pinp, FLICKER_STATE_STAT);

      if(fd_gain<1000)
      {
        dGain = 256*fd_gain/1000;  //nextAe->ipipeGain>> 2;
        fd_gain = 1000;

        //ALG_aewbSetIpipeWb(&ipipe_awb_gain);
        ti2a_output_params.ipipe_awb_gain.dGain = dGain;
      }

      /* Set sensor exposure time and analog gain for 3rd stage FD */
      ti2a_output_params.sensorGain           =fd_gain;
      ti2a_output_params.sensorExposure       = exp;
      ti2a_output_params.mask  = 3; //Enable for exp and aGain
      //Rfile_printf("third stage complete .... modified exp = %d, modified gain = %d \n", exp,fd_gain);
      //ALG_aewbSetSensorExposure(exp);
      //ALG_aewbSetSensorGain(fd_gain);
    }
    /*---------------------------------------------------------------------*/
    /*increase the sensor gain by 8.33/2 ms                                */
    /*---------------------------------------------------------------------*/
    else if(frame_cnt_fd == FD_FRAME_STEPS*3)
    {
      expG = gALG_TI_aewbObj.AE_PrmsOut.shutter * gALG_TI_aewbObj.AE_PrmsOut.again;

      //Rfile_printf("fourth stage \n");
      //4th exposure
      exp += 4167;

      fd_gain = expG / exp;

      //(3) API call for detection
      /* Pass H3A buffer to data conversion function */
      GETTING_RGB_BLOCK_VALUE_Y((unsigned short*)pAddr, h3a, 2);
      ret_st = flicker_detect((int32*)g_flickerMem, h3a, pinp, FLICKER_STATE_STAT);

      if(fd_gain<1000)
      {
        dGain = 256*fd_gain/1000;  //nextAe->ipipeGain>> 2;
        fd_gain = 1000;

        //ALG_aewbSetIpipeWb(&ipipe_awb_gain);
        ti2a_output_params.ipipe_awb_gain.dGain = dGain;
      }

      ti2a_output_params.sensorGain           = fd_gain;
      ti2a_output_params.sensorExposure       = exp;
      //ALG_aewbSetSensorExposure(exp);
      ti2a_output_params.mask  = 3; //Enable for exp and aGain
      //ALG_aewbSetSensorGain(fd_gain);
      //Rfile_printf("fourth stage complete .... modified exp = %d, modified gain = %d \n", exp,fd_gain);
    }
    else if(frame_cnt_fd == FD_FRAME_STEPS*4)
    {

      //Rfile_printf("Final statge of detection .... ");
      expG = gALG_TI_aewbObj.AE_PrmsOut.shutter * gALG_TI_aewbObj.AE_PrmsOut.again;

      //(3) API call for detection
      /* Pass H3A buffer to data conversion function */
      GETTING_RGB_BLOCK_VALUE_Y((unsigned short*)pAddr, h3a, 2);
      ret_st = flicker_detect((int32*)g_flickerMem, h3a, pinp, FLICKER_STATE_CALC);

      ti2a_output_params.ipipe_awb_gain.dGain = orig_dGain;
      ti2a_output_params.sensorGain           = orig_gain;
      ti2a_output_params.sensorExposure       = orig_exp;
      ti2a_output_params.mask  = 3; //Enable for exp and aGain
      //ALG_aewbSetSensorExposure(orig_exp);
      //ALG_aewbSetSensorGain(orig_gain);

      //ipipe_awb_gain.dGain = orig_dGain;
      //ALG_aewbSetIpipeWb(&ipipe_awb_gain);

      /* Configure 2A based on results of FD */
#ifdef ALG_AEWB_DEBUG
      Vps_printf("Detection status ... %d \n", ret_st);
#endif

      //Rfile_printf("Doing config call ... \n");
      TI_2A_config(ret_st, gALG_TI_aewbObj.saldre);
#ifdef FD_DEBUG_MSG
      OSA_printf("\n ret_st=%dti2a_output_params.sensorExposure\n", ret_st);
#endif
      if(((ret_st == 2)&&(gALG_TI_aewbObj.env_50_60Hz == VIDEO_PAL))  ||
          ((ret_st == 3)&&(gALG_TI_aewbObj.env_50_60Hz == VIDEO_NTSC)) ||
          (fd_count==6))
      {
#ifdef ALG_AEWB_DEBUG
        Vps_printf("Flicker detection completed ... ");
#endif
        flicker_detect_complete = 1;
        flicker_detect_enable = 0;

        fd_count = 0;
      }
      else
      {
        flicker_detect_enable = 0;
        fd_count++;

      }
    }

    frame_cnt_fd++;

    /*do not call 2A if the flicker detection is in progress*/
    return NO_2A;
  }
  else
  {
    return DO_2A;
  }
}

Int32 ResetSensorGain()
{
	gResetSensorGain = 1;
	return 0;
}

/* ===================================================================
 *  @func     TI2AFunc
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
void TI2AFunc(void *pAddr,UInt32 aewbSteps,UInt32 sensorFrameRate)
{
  //GETTING_RGB_BLOCK_VALUE(pAddr, rgbData, aew_data, 2);

  int retval = 0;
  UInt32 IsStabMode;
  Uint32 Configure_AE;
  gAE_conv = 0;
  UInt32 CaptureMode = 0;
  Int32 rGain,grGain,gbGain,bGain;

#ifdef ALG_AWB_DEBUG
    Vps_printf("DBG_AWB: TI2AFunc\n");
#endif

    IsStabMode = Iss_captIsAewbStabilization();

  Configure_AE = 0;

  if ((!IsStabMode) && (gcalc_mode_hist == AE_CALC_MODE_PREVIEW_FAST) ){
    cfg.calc_mode = AE_CALC_MODE_PREVIEW;
    gcalc_mode_hist = AE_CALC_MODE_PREVIEW;
    Configure_AE = 1;
  }
  else if((IsStabMode) && (gcalc_mode_hist == AE_CALC_MODE_PREVIEW) ){
    cfg.calc_mode = AE_CALC_MODE_PREVIEW_FAST;
    gcalc_mode_hist = AE_CALC_MODE_PREVIEW_FAST;
    Configure_AE = 1;
  }

  if(gTi2aControlParams.update & TI2A_UPDATE_CONTROL_PARAMS_2A){
    cfg.limits_cap.max_shutter = gTi2aControlParams.maxCapExposure;
    cfg.framerate       = Iss_deviceGetSensorFrameRate();
    Configure_AE = 1;
  }

  if (Configure_AE){
    retval = auto_exposure_control((void*)gALG_TI_aewbObj.handle_ae, (struct ae_configuration*)&cfg);

    if(retval == 1) {
      OSA_ERROR("AE_MMS Control Failed\n");
    }
  }

  /*Restart AWB algorithm before every video/still/timelapse capture*/
  if(gTi2aControlParams.update & TI2A_UPDATE_AWB_MODE_CONTROL)
  {
       aewbFrames   = 2;
       auto_white_balance_control(gALG_TI_aewbObj.awb_ctx,AWB_WB_MODE_AUTO);
       gTi2aControlParams.update &= (~TI2A_UPDATE_AWB_MODE_CONTROL);
  }

  if(aewbSteps == 0)
  {
	aewbSteps = NUM_STEPS;
  }

  if(gResetSensorGain == 1)
  {
	ti2a_output_params.sensorGain                = 1000;
    gALG_TI_aewbObj.AE_PrmsOut.again             = 1000;
	aewbFrames                                   = 0;  
	gResetSensorGain                             = 0;
  }	
  
  if(aew_enable == AEW_ENABLE)
  {
    GETTING_RGB_BLOCK_VALUE(pAddr, rgbData, aew_data, 2);

    // AE process parameters
    gALG_TI_aewbObj.AE_PrmsIn.sensor_aperture = 0; // Not used
    gALG_TI_aewbObj.AE_PrmsIn.sensor_shutter = ti2a_output_params.sensorExposure;
    gALG_TI_aewbObj.AE_PrmsIn.sensor_again   = ti2a_output_params.sensorGain;
    gALG_TI_aewbObj.AE_PrmsIn.sensor_dgain    = ti2a_output_params.ipipe_awb_gain.dGain;
    gALG_TI_aewbObj.AE_PrmsIn.gain_0        = ti2a_output_params.ipipe_awb_gain.rGain << 1;
    gALG_TI_aewbObj.AE_PrmsIn.gain_1      = ti2a_output_params.ipipe_awb_gain.grGain << 1;
    gALG_TI_aewbObj.AE_PrmsIn.gain_2      = ti2a_output_params.ipipe_awb_gain.gbGain << 1;
    gALG_TI_aewbObj.AE_PrmsIn.gain_3      = ti2a_output_params.ipipe_awb_gain.bGain << 1;
    gALG_TI_aewbObj.AE_PrmsIn.framerate = sensorFrameRate;// Sensor current frame rate
    gALG_TI_aewbObj.AE_PrmsIn.preflash_frm.frame_ppln = 0;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.preflash_frm.frame_ptr = 0;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.preflash_frm.frame_size_x = 0;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.preflash_frm.frame_size_y = 0;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.af_params.mode   = MODE_AUTO;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.af_params.o_distance  = 0;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.af_params.result   = RESULT_UNKNOWN;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.draft2fine_sens = 0x100;// Not used
    gALG_TI_aewbObj.AE_PrmsIn.h3a = (struct MMS_Rgb*) rgbData;
    gALG_TI_aewbObj.AE_PrmsIn.pixs_in_pax   = gALG_TI_aewbObj.IAEWB_StatMatdata.pixCtWin;// Check it!
    gALG_TI_aewbObj.AE_PrmsIn.ver_win_count = gALG_TI_aewbObj.IAEWB_StatMatdata.winCtVert;
    gALG_TI_aewbObj.AE_PrmsIn.hor_win_count = gALG_TI_aewbObj.IAEWB_StatMatdata.winCtHorz;

    if(gALG_TI_aewbObj.aewbType == ALG_AEWB_AE || gALG_TI_aewbObj.aewbType == ALG_AEWB_AEWB)
    {
#ifdef ALG_AEWB_DEBUG
      H3A_DEBUG_PRINT();
#endif
        if (!(aewbFrames % gCapture_cnt)){

            auto_exposure_process((void*)gALG_TI_aewbObj.handle_ae,
                              (struct ae_calculation_input*) &gALG_TI_aewbObj.AE_PrmsIn,
                              (struct ae_calculation_output*) &gALG_TI_aewbObj.AE_PrmsOut);
            //Get converge flag
            gAE_conv = gALG_TI_aewbObj.AE_PrmsOut.converged;
        }
        else{
            gALG_TI_aewbObj.AE_PrmsOut.shutter = gALG_TI_aewbObj.AE_PrmsIn.sensor_shutter;
            gALG_TI_aewbObj.AE_PrmsOut.again   = gALG_TI_aewbObj.AE_PrmsIn.sensor_again;
            gALG_TI_aewbObj.AE_PrmsOut.dgain   = gALG_TI_aewbObj.AE_PrmsIn.sensor_dgain;
        }



#ifdef ALG_AEWB_DEBUG
        Vps_printf("DSP AE: \n");
        Vps_printf("nextExposure: %d, curExposure:%d\n",
           gALG_TI_aewbObj.AE_PrmsOut.shutter,
           gALG_TI_aewbObj.AE_PrmsIn.sensor_shutter);
        Vps_printf("nextGain: %d, curGain:%d\n",
           gALG_TI_aewbObj.AE_PrmsOut.again,
           gALG_TI_aewbObj.AE_PrmsIn.sensor_again);
		Vps_printf("nextIGain: %d, curIGain:%d\n\n",
           gALG_TI_aewbObj.AE_PrmsOut.dgain,
           gALG_TI_aewbObj.AE_PrmsIn.sensor_dgain);
#endif

    }
    else
    {
      gALG_TI_aewbObj.AE_PrmsOut.shutter = gALG_TI_aewbObj.AE_PrmsIn.sensor_shutter;
      gALG_TI_aewbObj.AE_PrmsOut.again = gALG_TI_aewbObj.AE_PrmsIn.sensor_again;
      gALG_TI_aewbObj.AE_PrmsOut.dgain = gALG_TI_aewbObj.AE_PrmsIn.sensor_dgain;
    }

    if ((gAE_conv) &&
      (gALG_TI_aewbObj.aewbType == ALG_AEWB_AWB || gALG_TI_aewbObj.aewbType == ALG_AEWB_AEWB) && (gTi2aControlParams.wbSceneMode==AWB_AUTO) &&
      !(aewbFrames % aewbSteps) )
    {

       /* calling awb only we AE has converged */
      gALG_TI_aewbObj.AWB_InArgs.curWb.bGain = ti2a_output_params.ipipe_awb_gain.bGain << 1;
      gALG_TI_aewbObj.AWB_InArgs.curWb.gGain = ti2a_output_params.ipipe_awb_gain.grGain << 1;
      gALG_TI_aewbObj.AWB_InArgs.curWb.rGain = ti2a_output_params.ipipe_awb_gain.rGain << 1;
      gALG_TI_aewbObj.AWB_InArgs.curWb.colorTemp = ti2a_output_params.colorTemparaure;
      gALG_TI_aewbObj.AWB_InArgs.curAe.exposureTime = gALG_TI_aewbObj.AE_PrmsIn.sensor_shutter;
      gALG_TI_aewbObj.AWB_InArgs.curAe.sensorGain   = gALG_TI_aewbObj.AE_PrmsIn.sensor_again;
      gALG_TI_aewbObj.AWB_InArgs.curAe.ipipeGain    = gALG_TI_aewbObj.AE_PrmsIn.sensor_dgain;

      CaptureMode = Iss_captIsStillCapMode();

#ifdef ALG_AWB_DEBUG
          Vps_printf("DBG_AWB: exposureTime %d  apertureLevel %d sensorGain %d ipipeGain %d \n",
				                gALG_TI_aewbObj.AWB_InArgs.curAe.exposureTime,
				                gALG_TI_aewbObj.AWB_InArgs.curAe.apertureLevel,
				                gALG_TI_aewbObj.AWB_InArgs.curAe.sensorGain,
				                gALG_TI_aewbObj.AWB_InArgs.curAe.ipipeGain);



#endif
#ifdef _PROFILE_AWB_ALGO_
       awb_start    = Timestamp_get32();
#endif


      retval =  auto_white_balance_process(  gALG_TI_aewbObj.awb_ctx,
        &gALG_TI_aewbObj.AWB_InArgs,
        &gALG_TI_aewbObj.AWB_OutArgs,
                    rgbData,
        &rgb2rgb_1_output[0],
                CaptureMode);

     if(retval == AWB_FW_ERROR) {
        Vps_printf("=== === === ERROR  === === === auto_white_balance_process FAILS \n");
     }


#ifdef _PROFILE_AWB_ALGO_
      awb_end    = Timestamp_get32();
      Vps_printf("DBG_AWB: AWB execution time: %d \n",(awb_end - awb_start));
#endif

#ifdef DUMP_STILL_IMAGE_ADDITIONAL_INFO
      if(Iss_captIsStillCapMode() == 1){
           gStillAdditionalInfo[0] = gALG_TI_aewbObj.AWB_InArgs.curAe.exposureTime;
           gStillAdditionalInfo[1] = gALG_TI_aewbObj.AWB_InArgs.curAe.apertureLevel;
           gStillAdditionalInfo[2] = gALG_TI_aewbObj.AWB_InArgs.curAe.sensorGain;
           gStillAdditionalInfo[3] = gALG_TI_aewbObj.AWB_InArgs.curAe.ipipeGain;
           gStillAdditionalInfo[4] = gALG_TI_aewbObj.AWB_OutArgs.nextWb.rGain;
           gStillAdditionalInfo[5] = gALG_TI_aewbObj.AWB_OutArgs.nextWb.gGain;
           gStillAdditionalInfo[6] = gALG_TI_aewbObj.AWB_OutArgs.nextWb.bGain;
           gStillAdditionalInfo[7] = gALG_TI_aewbObj.AWB_OutArgs.nextWb.colorTemp;

           memcpy(&gStillAdditionalInfo[8],rgbData,(STILL_ADDITIONAL_INFO_SIZE-8)*sizeof(UInt32));
           gPrintH3aStatistics = 1;
      }
#endif

      /*For timelpase use cumulative moving average for WB gains to reduce instability*/
      if((Iss_captIsTimelapseMode()==1) && (ipipe_awb_gain_prev.rGain !=0) && (ipipe_awb_gain_prev.grGain !=0) &&
                        (ipipe_awb_gain_prev.gbGain !=0) && (ipipe_awb_gain_prev.bGain !=0))
      {
        rGain  = ((gALG_TI_aewbObj.AWB_OutArgs.nextWb.rGain >> 1) + ipipe_awb_gain_prev.rGain)/2;
        grGain = ((gALG_TI_aewbObj.AWB_OutArgs.nextWb.gGain >> 1) + ipipe_awb_gain_prev.grGain)/2;
        gbGain = ((gALG_TI_aewbObj.AWB_OutArgs.nextWb.gGain >> 1) + ipipe_awb_gain_prev.gbGain)/2;
        bGain  = ((gALG_TI_aewbObj.AWB_OutArgs.nextWb.bGain >> 1) + ipipe_awb_gain_prev.bGain)/2;

        ti2a_output_params.ipipe_awb_gain.rGain  = LIMIT_MOVING_AVERAGE(rGain,(Int32)ipipe_awb_gain_prev.rGain,
                                                                       (Int32)((gALG_TI_aewbObj.AE_PrmsOut.again*gALG_TI_aewbObj.AE_PrmsOut.dgain)/256));
        ti2a_output_params.ipipe_awb_gain.grGain = LIMIT_MOVING_AVERAGE(grGain,(Int32)ipipe_awb_gain_prev.grGain,
                                                                       (Int32)((gALG_TI_aewbObj.AE_PrmsOut.again*gALG_TI_aewbObj.AE_PrmsOut.dgain)/256));
        ti2a_output_params.ipipe_awb_gain.gbGain = LIMIT_MOVING_AVERAGE(gbGain,(Int32)ipipe_awb_gain_prev.gbGain,
                                                                       (Int32)((gALG_TI_aewbObj.AE_PrmsOut.again*gALG_TI_aewbObj.AE_PrmsOut.dgain)/256));
        ti2a_output_params.ipipe_awb_gain.bGain  = LIMIT_MOVING_AVERAGE(bGain,(Int32)ipipe_awb_gain_prev.bGain,
                                                                       (Int32)((gALG_TI_aewbObj.AE_PrmsOut.again*gALG_TI_aewbObj.AE_PrmsOut.dgain)/256));

      }
      else
      {
        ti2a_output_params.ipipe_awb_gain.rGain  = gALG_TI_aewbObj.AWB_OutArgs.nextWb.rGain >> 1;
        ti2a_output_params.ipipe_awb_gain.grGain = gALG_TI_aewbObj.AWB_OutArgs.nextWb.gGain >> 1;
        ti2a_output_params.ipipe_awb_gain.gbGain = gALG_TI_aewbObj.AWB_OutArgs.nextWb.gGain >> 1;
        ti2a_output_params.ipipe_awb_gain.bGain  = gALG_TI_aewbObj.AWB_OutArgs.nextWb.bGain >> 1;
      }

          ti2a_output_params.ipipe_awb_gain.rOffset   = 0;
          ti2a_output_params.ipipe_awb_gain.grOffset  = 0;
          ti2a_output_params.ipipe_awb_gain.gbOffset  = 0;
          ti2a_output_params.ipipe_awb_gain.bOffset   = 0;

      /* For timelapse apply a constant CCM to avoid instability */

      /* Use the same CCM for viewfinder to avoid sudden difference in color
       * in timelapse video if viewfinder is switched off inbetween */
      if((Iss_captIsTimelapseMode()==1)|| (CameraLink_isViewfinderMode()==1))
      {
         memcpy(rgb2rgb_1_output,rgb2rgb1_4616k_5950k,sizeof(rgb2rgb_1_output));
      }

#ifdef ALG_AWB_DEBUG
          Vps_printf("DBG_AWB: CT : %5d, R : %4d, G : %4d B : %4d \n",
				                gALG_TI_aewbObj.AWB_OutArgs.nextWb.colorTemp,
				                gALG_TI_aewbObj.AWB_OutArgs.nextWb.rGain,
				                gALG_TI_aewbObj.AWB_OutArgs.nextWb.gGain,
				                gALG_TI_aewbObj.AWB_OutArgs.nextWb.bGain);
          Vps_printf("DBG_AWB:  %d %d %d %d %d %d\n", aew_enable , gALG_TI_aewbObj.aewbType , gTi2aControlParams.wbSceneMode,gAewbStabilizeCnt,aewbFrames,aewbSteps );
#endif

      ti2a_output_params.colorTemparaure = gALG_TI_aewbObj.AWB_OutArgs.nextWb.colorTemp;

      if((gALG_TI_aewbObj.AE_PrmsOut.dgain == gALG_TI_aewbObj.AE_PrmsIn.sensor_dgain) && (Iss_captIsAewbStabilization() == 1))
       {
	     gAewbStabilizeCnt ++;
       }

#ifdef ALG_AEWB_DEBUG
      if(aewbFrames % (NUM_STEPS*32) == 0)
      {
          Vps_printf("DSP AWB : CT : %5d, R : %4d, G : %4d B : %4d \n",
				gALG_TI_aewbObj.AWB_OutArgs.nextWb.colorTemp,
				ti2a_output_params.ipipe_awb_gain.rGain,
				ti2a_output_params.ipipe_awb_gain.grGain,
				ti2a_output_params.ipipe_awb_gain.bGain);
          //ti2a_output_params.colorTemparaure = Illum.temperature;
          Vps_printf(" AWB %d %d %d\n", aew_enable , gALG_TI_aewbObj.aewbType , gTi2aControlParams.wbSceneMode );
      }
#endif

    }


    if(gTi2aControlParams.wbSceneMode != AWB_AUTO)
    {
         switch(gTi2aControlParams.wbSceneMode){
             case AWB_7500K:
                    ti2a_output_params.ipipe_awb_gain.rGain  = 238;
                    ti2a_output_params.ipipe_awb_gain.grGain = 128;
                    ti2a_output_params.ipipe_awb_gain.gbGain = 128;
                    ti2a_output_params.ipipe_awb_gain.bGain  = 175;
                    ti2a_output_params.ipipe_awb_gain.rOffset  = 0;
                    ti2a_output_params.ipipe_awb_gain.grOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.gbOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.bOffset  = 0;
                    memcpy(rgb2rgb_1_output,rgb2rgb1_7091k_20000k,sizeof(rgb2rgb_1_output));
                    ti2a_output_params.colorTemparaure       = 7500;
                    break;
             case AWB_5000K:
                    ti2a_output_params.ipipe_awb_gain.rGain  = 190;
                    ti2a_output_params.ipipe_awb_gain.grGain = 128;
                    ti2a_output_params.ipipe_awb_gain.gbGain = 128;
                    ti2a_output_params.ipipe_awb_gain.bGain  = 213;
                    ti2a_output_params.ipipe_awb_gain.rOffset  = 0;
                    ti2a_output_params.ipipe_awb_gain.grOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.gbOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.bOffset  = 0;
                    memcpy(rgb2rgb_1_output,rgb2rgb1_4616k_5950k,sizeof(rgb2rgb_1_output));
                    ti2a_output_params.colorTemparaure       = 5000;
                    break;
             case AWB_4000K:
                    ti2a_output_params.ipipe_awb_gain.rGain  = 152;
                    ti2a_output_params.ipipe_awb_gain.grGain = 128;
                    ti2a_output_params.ipipe_awb_gain.gbGain = 128;
                    ti2a_output_params.ipipe_awb_gain.bGain  = 256;
                    ti2a_output_params.ipipe_awb_gain.rOffset  = 0;
                    ti2a_output_params.ipipe_awb_gain.grOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.gbOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.bOffset  = 0;
                    memcpy(rgb2rgb_1_output,rgb2rgb1_3336k_4615k,sizeof(rgb2rgb_1_output));
                    ti2a_output_params.colorTemparaure       = 4000;
                    break;
             case AWB_3000K:
                    ti2a_output_params.ipipe_awb_gain.rGain  = 128;
                    ti2a_output_params.ipipe_awb_gain.grGain = 138;
                    ti2a_output_params.ipipe_awb_gain.gbGain = 138;
                    ti2a_output_params.ipipe_awb_gain.bGain  = 335;
                    ti2a_output_params.ipipe_awb_gain.rOffset  = 0;
                    ti2a_output_params.ipipe_awb_gain.grOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.gbOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.bOffset  = 0;
                    memcpy(rgb2rgb_1_output,rgb2rgb1_0k_3335k,sizeof(rgb2rgb_1_output));
                    ti2a_output_params.colorTemparaure       = 3000;
                    break;
             case AWB_2000K:
                    ti2a_output_params.ipipe_awb_gain.rGain  = 128;
                    ti2a_output_params.ipipe_awb_gain.grGain = 169;
                    ti2a_output_params.ipipe_awb_gain.gbGain = 169;
                    ti2a_output_params.ipipe_awb_gain.bGain  = 476;
                    ti2a_output_params.ipipe_awb_gain.rOffset  = 0;
                    ti2a_output_params.ipipe_awb_gain.grOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.gbOffset = 0;
                    ti2a_output_params.ipipe_awb_gain.bOffset  = 0;
                    memcpy(rgb2rgb_1_output,rgb2rgb1_0k_3335k,sizeof(rgb2rgb_1_output));
                    ti2a_output_params.colorTemparaure       = 2000;
                    break;
             case AWB_RED_FILTER:
                    ti2a_output_params.ipipe_awb_gain.rGain  = 315;
                    ti2a_output_params.ipipe_awb_gain.grGain =  81;
                    ti2a_output_params.ipipe_awb_gain.gbGain =  81;
                    ti2a_output_params.ipipe_awb_gain.bGain  = 129;
                    ti2a_output_params.ipipe_awb_gain.rOffset = -10;
                    ti2a_output_params.ipipe_awb_gain.grOffset= -10;
                    ti2a_output_params.ipipe_awb_gain.gbOffset= -10;
                    ti2a_output_params.ipipe_awb_gain.bOffset = -10;
                    memcpy(rgb2rgb_1_output,rgb2rgb1_red_filter,sizeof(rgb2rgb_1_output));
                    break;
             case AWB_MAGENTA_FILTER:
                    ti2a_output_params.ipipe_awb_gain.rGain  = 197;
                    ti2a_output_params.ipipe_awb_gain.grGain = 128;
                    ti2a_output_params.ipipe_awb_gain.gbGain = 128;
                    ti2a_output_params.ipipe_awb_gain.bGain  = 200;
                    ti2a_output_params.ipipe_awb_gain.rOffset  = -5;
                    ti2a_output_params.ipipe_awb_gain.grOffset = -5;
                    ti2a_output_params.ipipe_awb_gain.gbOffset = -5;
                    ti2a_output_params.ipipe_awb_gain.bOffset  = -5;
                    memcpy(rgb2rgb_1_output,rgb2rgb1_magenta_filter,sizeof(rgb2rgb_1_output));
                    break;
             default:
                 break;
         }
    }

    ti2a_output_params.sensorExposure = gALG_TI_aewbObj.AE_PrmsOut.shutter;
    ti2a_output_params.sensorGain     =  gALG_TI_aewbObj.AE_PrmsOut.again;
    ti2a_output_params.ipipe_awb_gain.dGain = gALG_TI_aewbObj.AE_PrmsOut.dgain;

    ti2a_output_params.sensorExposureCapture = gALG_TI_aewbObj.AE_PrmsOut.shutter_cap;
    ti2a_output_params.sensorGainCapture     = gALG_TI_aewbObj.AE_PrmsOut.again_cap;

    if(ti2a_output_params_prev.sensorGain != ti2a_output_params.sensorGain)
    {
      ti2a_output_params_prev.sensorGain = ti2a_output_params.sensorGain;
      ti2a_output_params.mask |= 2;
    }
    else
    {
      ti2a_output_params.mask &= ~(2);
    }
    // Capture gain stored
    if(ti2a_output_params_prev.sensorGainCapture != ti2a_output_params.sensorGainCapture)
    {
      ti2a_output_params_prev.sensorGainCapture = ti2a_output_params.sensorGainCapture;
    }

    if(ti2a_output_params_prev.sensorExposure != ti2a_output_params.sensorExposure)
    {
      ti2a_output_params_prev.sensorExposure = ti2a_output_params.sensorExposure;
      ti2a_output_params.mask |= 1;
    }
    else
    {
      ti2a_output_params.mask &= ~(1);
    }
    // Capture Exposure time stored
    if(ti2a_output_params_prev.sensorExposureCapture != ti2a_output_params.sensorExposureCapture)
    {
      ti2a_output_params_prev.sensorExposureCapture = ti2a_output_params.sensorExposureCapture;
    }

    if(ti2a_output_params_prev.ipipe_awb_gain.dGain != ti2a_output_params.ipipe_awb_gain.dGain)
    {
      ti2a_output_params_prev.ipipe_awb_gain.dGain = ti2a_output_params.ipipe_awb_gain.dGain;
    }
    ti2a_output_params.mask |= (4);

     //Vps_printf(" AEwb E:%d :G:%d DG:%d mask:%d\n", ti2a_output_params.sensorExposure ,
     //            ti2a_output_params.sensorGain , ti2a_output_params.ipipe_awb_gain.dGain,
     //            ti2a_output_params.mask);

  }
  else if(aew_enable == AEW_ENABLE && (gALG_TI_aewbObj.aewbType == ALG_AEWB_AE || gALG_TI_aewbObj.aewbType == ALG_AEWB_AWB || gALG_TI_aewbObj.aewbType == ALG_AEWB_AEWB )){
    ti2a_output_params.sensorExposure = gALG_TI_aewbObj.AE_PrmsIn.sensor_shutter;
    ti2a_output_params.sensorGain     =  gALG_TI_aewbObj.AE_PrmsIn.sensor_again;
    ti2a_output_params.ipipe_awb_gain.dGain = gALG_TI_aewbObj.AE_PrmsIn.sensor_dgain;

    if(ti2a_output_params_prev.sensorGain != ti2a_output_params.sensorGain)
    {
      ti2a_output_params_prev.sensorGain = ti2a_output_params.sensorGain;
      ti2a_output_params.mask |= 2;
    }
    else
    {
      ti2a_output_params.mask &= ~(2);
    }
    if(ti2a_output_params_prev.sensorExposure != ti2a_output_params.sensorExposure)
    {
      ti2a_output_params_prev.sensorExposure = ti2a_output_params.sensorExposure;
      ti2a_output_params.mask |= 1;
    }
    else
    {
      ti2a_output_params.mask &= ~(1);
    }

    if(ti2a_output_params_prev.ipipe_awb_gain.dGain != ti2a_output_params.ipipe_awb_gain.dGain)
    {
      ti2a_output_params_prev.ipipe_awb_gain.dGain = ti2a_output_params.ipipe_awb_gain.dGain;
    }
    ti2a_output_params.mask |= (4);

    //Vps_printf(" AE E:%d :G:%d DG:%d mask:%d\n", ti2a_output_params.sensorExposure ,
    //             ti2a_output_params.sensorGain , ti2a_output_params.ipipe_awb_gain.dGain,
    //             ti2a_output_params.mask);
  }
  /* remove the count and put it into the process */
  aewbFrames ++;

}

/* ===================================================================
 *  @func     TI2A_applySettings
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
/*
static void TI2A_applySettings(IAEWB_Ae *curAe, IAEWB_Ae *nextAe, int numSmoothSteps, int step)
{
  int delta_sensorgain = ((int)nextAe->sensorGain - (int)curAe->sensorGain)/numSmoothSteps;
  int delta_exposure = ((int)nextAe->exposureTime - (int)curAe->exposureTime)/numSmoothSteps;
  int delta_ipipe = ((int)nextAe->ipipeGain - (int)curAe->ipipeGain)/numSmoothSteps;

#ifdef ALG_AWB_DEBUG
  Vps_printf("DBG_AWB: TI2A_applySettings\n");
#endif

  step ++;

  ti2a_output_params.sensorGain           = delta_sensorgain * step + curAe->sensorGain;
  ti2a_output_params.sensorExposure       = delta_exposure * step + curAe->exposureTime;
  ti2a_output_params.ipipe_awb_gain.dGain = (delta_ipipe * step +curAe->ipipeGain) >> 2;

  if(step >= numSmoothSteps) {
    ti2a_output_params.sensorGain = nextAe->sensorGain;
    ti2a_output_params.sensorExposure = nextAe->exposureTime;
    ti2a_output_params.ipipe_awb_gain.dGain = nextAe->ipipeGain>> 2;
  }
  if(ti2a_output_params_prev.sensorGain != ti2a_output_params.sensorGain)
  {
    ti2a_output_params_prev.sensorGain = ti2a_output_params.sensorGain;
    ti2a_output_params.mask |= 2;
#ifdef ALG_AEWB_DEBUG
    Vps_printf("Sensor Analog Gain   :  < %5d, %5d ,%5d > \n",
    ti2a_output_params.sensorGain,curAe->sensorGain,nextAe->sensorGain);
    Vps_printf("AEWB: Avg Color      :  < %5d, %5d, %5d, %5d >\n",
    accValue[0], accValue[1], accValue[2], accValue[3]);
#endif
  }
  else
  {
    ti2a_output_params.mask &= ~(2);
  }
  if(ti2a_output_params_prev.sensorExposure != ti2a_output_params.sensorExposure)
  {
    ti2a_output_params_prev.sensorExposure = ti2a_output_params.sensorExposure;
    ti2a_output_params.mask |= 1;
#ifdef ALG_AEWB_DEBUG
    Vps_printf("Sensor Shutter Speed :  < %5d, %5d ,%5d > \n",
    ti2a_output_params.sensorExposure,curAe->exposureTime,nextAe->exposureTime);
    Vps_printf("AEWB: Avg Color      :  < %5d, %5d, %5d, %5d > \n",
    accValue[0], accValue[1], accValue[2], accValue[3]);
#endif
  }
  else
  {
    ti2a_output_params.mask &= ~(1);
  }

#if 1
  if(ti2a_output_params_prev.ipipe_awb_gain.dGain != ti2a_output_params.ipipe_awb_gain.dGain)
  {
    ti2a_output_params_prev.ipipe_awb_gain.dGain = ti2a_output_params.ipipe_awb_gain.dGain;
    #ifdef ALG_AEWB_DEBUG
    Vps_printf("AEWB Digital Gain    :  < %5d, %5d ,%5d > \n",
    ti2a_output_params.ipipe_awb_gain.dGain,curAe->ipipeGain,nextAe->ipipeGain);
    Vps_printf("AEWB: Avg Color      :  < %5d, %5d, %5d, %5d > \n",
    accValue[0], accValue[1], accValue[2], accValue[3]);
    #endif
  }
  ti2a_output_params.mask |= (4);
#else

  if(memcmp(&(ti2a_output_params_prev.ipipe_awb_gain),&(ti2a_output_params.ipipe_awb_gain),(sizeof(AWB_PARAM))))
  {
    ti2a_output_params.mask |= 4;
    ti2a_output_params_prev.ipipe_awb_gain.rGain  = ti2a_output_params.ipipe_awb_gain.rGain;
    ti2a_output_params_prev.ipipe_awb_gain.grGain = ti2a_output_params.ipipe_awb_gain.grGain;
    ti2a_output_params_prev.ipipe_awb_gain.gbGain = ti2a_output_params.ipipe_awb_gain.gbGain;
    ti2a_output_params_prev.ipipe_awb_gain.bGain  = ti2a_output_params.ipipe_awb_gain.bGain;
    ti2a_output_params_prev.ipipe_awb_gain.dGain  = ti2a_output_params.ipipe_awb_gain.dGain;
  }
  else
  {
    ti2a_output_params.mask &= ~(4);
  }
#endif

}
*/



/* ===================================================================
 *  @func     ALG_aewbRun
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
int ALG_aewbRun(void *h3aDataVirtAddr,UInt32 aewbSteps, UInt32 sensorFrameRate)
{


  int do2a = DO_2A;
  /*Debug*/
#ifdef ALG_AWB_DEBUG
  Vps_printf("DBG_AWB: ALG_aewbRun\n");
#endif

  /*if flicker detection is enabled*/
  if (gALG_TI_aewbObj.flicker_detect == FDC_ENABLED)
  {
      do2a = TI2A_FDC_Process((void*)h3aDataVirtAddr);
  }

  if (do2a == DO_2A)
  {
      TI2AFunc( (void *)h3aDataVirtAddr,aewbSteps, sensorFrameRate);
#ifdef ALG_AEWB_DEBUG
      H3A_DEBUG_PRINT();
#endif
  }
  return 0;
}

/* ===================================================================
 *  @func     ALG_aewbDelete
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
int ALG_aewbDelete(void *hndl)
{
#ifdef ALG_AWB_DEBUG
  Vps_printf("DBG_AWB: ALG_aewbDelete\n");
#endif

  auto_exposure_delete((void*) gALG_TI_aewbObj.handle_ae);

  auto_white_balance_delete(gALG_TI_aewbObj.awb_ctx);

  if(rgbData!=NULL) {
  	free(rgbData);
  	rgbData = NULL;
  }

  if(aew_data!=NULL) {
  	free(aew_data);
  	aew_data = NULL;
  }

  if(g_flickerMem!=NULL) {
  	free(g_flickerMem);
  	g_flickerMem = NULL;
  }

  return 0;
}


/* ===================================================================
 *  @func     ALG_aewbControl
 *
 *  @desc     Function does the following
 *
 *  @modif    This function modifies the following structures
 *
 *  @inputs   This function takes the following inputs
 *            <argument name>
 *            Description of usage
 *            <argument name>
 *            Description of usage
 *
 *  @outputs  <argument name>
 *            Description of usage
 *
 *  @return   Return value of this function if any
 *  ==================================================================
 */
int ALG_aewbControl(void)
{
#ifdef ALG_AWB_DEBUG
    Vps_printf("DBG_AWB: ALG_aewbControl\n");
#endif

    gALG_TI_aewbObj.aewbType = gTi2aControlParams.aewbType;
    if (gTi2aControlParams.update & TI2A_UPDATE_AE_DAY_NIGHT)
    {
      if(gTi2aControlParams.day_night == AE_DAY)
      {
        gTi2aControlParams.maxExposure         = 16666;
      }
      else
      {
        gTi2aControlParams.maxExposure         = 33330;
#if defined(IMGS_MICRON_MT9M034) & defined(WDR_ON)
        gTi2aControlParams.maxExposure         = 22100;//33330;
#endif
#if defined(IMGS_SONY_IMX104) & defined(WDR_ON)
		gTi2aControlParams.maxExposure		 = 32755;//33330;
#endif
#if (defined(IMGS_SONY_IMX136) | defined(IMGS_SONY_IMX140) ) & defined(WDR_ON)
	  gTi2aControlParams.maxExposure		 = 33184;
#endif
      }
      gTi2aControlParams.update |= TI2A_UPDATE_CONTROL_PARAMS_2A;
      gTi2aControlParams.update &= (~TI2A_UPDATE_AE_DAY_NIGHT);
    }

    if (gTi2aControlParams.update & TI2A_UPDATE_AE_METERING)
    {

          if(gTi2aControlParams.aeMeteringMode == AE_CENTER)
          {
              gae_spot_weighting = AE_SPOTWEIGHTING_CENTER;
          }
          else if(gTi2aControlParams.aeMeteringMode == AE_NORMAL)
          {
              gae_spot_weighting = AE_SPOTWEIGHTING_NORMAL;
          }
          else
          {
              gae_spot_weighting = AE_SPOTWEIGHTING_WIDE;
          }

          gTi2aControlParams.update |= TI2A_UPDATE_CONTROL_PARAMS_2A;
          gTi2aControlParams.update &= (~TI2A_UPDATE_AE_METERING);

    }

    if (gTi2aControlParams.update & TI2A_UPDATE_EV_COMPENSATION)
    {
          double evValue = (double)gTi2aControlParams.evCompensation;

          evValue = evValue/100;
          gev_compensation = (int)(256 * pow(2.0,evValue));

          gTi2aControlParams.update |= TI2A_UPDATE_CONTROL_PARAMS_2A;
          gTi2aControlParams.update &= (~TI2A_UPDATE_EV_COMPENSATION);
    }

  if ( (gTi2aControlParams.update & TI2A_UPDATE_CONTROL_PARAMS_2A) || (new_awb_data_available == 1))
    {
	  new_awb_data_available = 0;
#ifdef ALG_AEWB_DEBUG
      Vps_printf("Doing 2A config Call ...\n ");
#endif
      gTi2aControlParams.update &= (~TI2A_UPDATE_CONTROL_PARAMS_2A);
	  if (gTi2aControlParams.flicker_sel == 1)
      {
        gALG_TI_aewbObj.env_50_60Hz = VIDEO_PAL;
      }
      else if (gTi2aControlParams.flicker_sel == 2)
      {
        gALG_TI_aewbObj.env_50_60Hz = VIDEO_NTSC;
      }
      TI_2A_config(0, gALG_TI_aewbObj.saldre);
    }

    if (gTi2aControlParams.update & TI2A_UPDATE_BLC)
    {

        if(gTi2aControlParams.blc==BACKLIGHT_LOW ||
            gTi2aControlParams.blc==BACKLIGHT_LOW2 )
        {
            gALG_TI_aewbObj.weight= TI_WEIGHTING_MATRIX;

        }
        else if(gTi2aControlParams.blc==BACKLIGHT_HIGH ||
                gTi2aControlParams.blc==BACKLIGHT_HIGH2 )
        {
            gALG_TI_aewbObj.weight=TI_WEIGHTING_SPOT;
        }
        else
        {
            gALG_TI_aewbObj.weight=TI_WEIGHTING_CENTER;
        }
      gTi2aControlParams.update &= (~TI2A_UPDATE_BLC);
   }

    // TO_DO : manual modes support
    //switch(gTi2aControlParams.wbSceneMode){
    //    case TI2A_WB_SCENE_MODE_AUTO:
    //    default:
    //        auto_white_balance_control(gALG_TI_aewbObj.awb_ctx,AWB_WB_MODE_AUTO);
    //        break;
    //    case TI2A_WB_SCENE_MODE_D65:
    //         auto_white_balance_control(gALG_TI_aewbObj.awb_ctx,AWB_WB_MODE_CLOUDY);
    //        break;
    //    case TI2A_WB_SCENE_MODE_D55:
    //         auto_white_balance_control(gALG_TI_aewbObj.awb_ctx,AWB_WB_MODE_DAYLIGHT);
    //        break;
    //    case TI2A_WB_SCENE_MODE_FLORESCENT:
    //         auto_white_balance_control(gALG_TI_aewbObj.awb_ctx,AWB_WB_MODE_FLUORESCENT);
    //        break;
    //    case TI2A_WB_SCENE_MODE_INCANDESCENT:
    //         auto_white_balance_control(gALG_TI_aewbObj.awb_ctx,AWB_WB_MODE_INCANDESCENT);
    //        break;
    //}

    return 0;
}

void ALG_aewbGetOutputParams(ti2a_output *outputParams)
{
#ifdef ALG_AWB_DEBUG
    Vps_printf("DBG_AWB: ALG_aewbGetOutputParams\n");
#endif
	memcpy(outputParams, &ti2a_output_params, sizeof(ti2a_output));	
}


