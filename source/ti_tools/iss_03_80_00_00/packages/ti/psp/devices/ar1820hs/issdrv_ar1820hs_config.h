/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2013 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
#ifndef _ISSDRV_AR1820HS_CONFIG_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _ISSDRV_AR1820HS_CONFIG_H_


#define PSP_I2C_MAX_REG_RD_WR       	(0x300)
#define AR_1820HS_ADDR					(0x36)  // CCI write address
#define AR_1820HS_BOARD_PRECONFIG		(TRUE)	//TURE/FALSE

typedef struct
{
    UInt32 i2cInstId;
    UInt32 numArgs;
    UInt16 regAddr[PSP_I2C_MAX_REG_RD_WR];
    UInt16 regValue[PSP_I2C_MAX_REG_RD_WR];
	UInt32 numRegs;
} I2c_Ctrl;

UInt32 SensorConfigScript_1080p30[353][3] = {
	{0x301A,0x0001},		//RESET 	//LOAD = Streaming Off
	{0x31AE, 0x0204},		//'mte.Sensor.Register("SERIAL_FORMAT").Value = &H304& 	4-Lane MIPI
	{0x0112, 0x0C0C},		//'mte.Sensor.Register("CCP_DATA_FORMAT").Value = &HA0A& Bit-Width is 12
	{0x0300, 0x0003},		//'mte.Sensor.Register("VT_PIX_CLK_DIV").Value = &H3& 	
	{0x0306, 0x0018},  		// pll multiplier  originally 0x60 for 120fps
	{0x0308, 0x000C},		//'mte.Sensor.Register("OP_PIX_CLK_DIV").Value = &HA& 	
	{0x3016, 0x0111},		//,row speed
	{0x3064, 0x5800},		//'mte.Sensor.Register("SMIA_TEST").Value = &H5800& 								
	{0x0104, 0x01},		//GROUPED_PARAMETER_HOLD = 1
	{0x3012, 1116},		//COARSE_INTEGRATION_TIME = 100
	{0x300A, 1126},		//FRAME_LENGTH_LINE = 1142
	{0x300C, 3976},		//LINE_LENGTH_PCK = 3892
	{0x3004, 544},		//X_ADDR_START = 
	{0x3002, 770},		//Y_ADDR_START =
	{0x3008, 4383},		//X_ADDR_END =  
	{0x3006, 2930},		//Y_ADDR_END =   
	{0x034C, 1920},		//X_OUTPUT_SIZE = 1920
	{0x034E, 1080},		//Y_OUTPUT_SIZE = 1080
	{0x303E, 0x0000},		//READ_STYLE = 0
	{0x3040, 0x28C3},		//READ_MODE = 65 //&H24C3  'x bin2 y-sum2 0x24C3
	{0x0400, 0x00},              //scalng mode /disable
	{0x0404, 16},  		//scale M/
	{0x0408, 0x1010},		//SECOND_RESIDUAL = 4112
	{0x040A, 0x0210},		//SECOND_CROP = 528
	{0x3F00, 8},			//BM_To= 
	{0x3F02, 65},		//BM_T1=
	{0x0104, 0x00},		//GROUPED_PARAMETER_HOLD = 0
	{0x3F0A, 0x0302},	//FIELD_WR= NOISE_FLOOR10, 0x0302	//(**1/4)		
	{0x3F0C, 0x0403},	//FIELD_WR= NOISE_FLOOR32, 0x0403 	//(**2/4)	
	{0x3F1E, 0x0004},	//FIELD_WR= NOISE_COEF, 0x0032		//(**4/4)
	{0x3F40, 0x0909},	//FIELD_WR= COUPLE_K_FACTOR0, 	
	{0x3F42, 0x0303},	//FIELD_WR= COUPLE_K_FACTOR1, 
	{0x3F18, 0x0304},	//FIELD_WR= CROSSFACTOR1, 0x0304	//(**3/4)
	{0x305E, 0x3089},
	{0x3ED2, 0xE49D},
	{0x3EE0, 0x9518},
	{0x3EDC, 0x01C9},
	{0x3EC8, 0x00A4},
	{0x3ED8, 0x60B0},
	{0x3ECC, 0x7386},
	{0x3F1A, 0x0404},
	{0x3F44, 0x0101}
	};

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif

