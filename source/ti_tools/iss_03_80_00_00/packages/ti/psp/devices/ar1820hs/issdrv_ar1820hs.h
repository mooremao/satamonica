/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2013 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _ISSDRV_AR1820HS_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _ISSDRV_AR1820HS_H_

#include <ti/psp/devices/iss_sensorDriver.h>
#include <ti/psp/vps/drivers/fvid2_drvMgr.h>



Int32 Iss_Ar1820hsInit (  );

Fdrv_Handle Iss_Ar1820hsCreate ( UInt32 drvId,
                                UInt32 instanceId,
                                Ptr createArgs,
                                Ptr createStatusArgs,
                                const FVID2_DrvCbParams * fdmCbParams );


Int32 Iss_Ar1820hsControl ( Fdrv_Handle handle,
                           UInt32 cmd, Ptr cmdArgs, Ptr cmdStatusArgs );
                           
Int32 Iss_Ar1820hsDelete ( Fdrv_Handle handle, Ptr deleteArgs );                       
                           
Int32 Iss_Ar1820hsDeInit (  );

Int32 Iss_Ar1820hsPinMux (  );

int Transplant_DRV_imgsSetRegs();

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif

