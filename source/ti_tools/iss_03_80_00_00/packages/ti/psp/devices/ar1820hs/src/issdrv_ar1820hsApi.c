/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <ti/psp/devices/ar1820hs/src/issdrv_ar1820hsPriv.h>
#include <ti/psp/devices/ar1820hs/issdrv_ar1820hs_config.h>
#include <ti/psp/platforms/iss_platform.h>
#define LONG_EXPO                       0x3012
#define LONG_GAIN                       0x305e



/* Global object storing all information related to all
  AR1820HS driver handles */
static Iss_Ar1820hsCommonObj 	gIss_Ar1820hsCommonObj;
static I2c_Ctrl 				gAr1820hs_I2c_ctrl;
extern ti2a_output 				ti2a_output_params;


Uint16    AR1820HS_GAIN_TAB[AR1820HS_GAIN_MAX +1]= {
/*0*/ 0,
/*1*/ 1,
/*2*/ 2,
/*3*/ 3,
/*4*/ 5,
/*5*/ 6,
/*6*/ 8,
/*7*/ 9,
/*8 */ 11,
/*9 */ 13,
/*10*/ 15,
/*11*/ 17,
/*12*/ 18,
/*13*/ 19,
/*14*/ 21,
/*15*/ 22,
/*16*/ 24,
/*17*/ 25,
/*18*/ 27,
/*19*/ 29,
/*20*/ 31,
/*21*/ 49,
/*22*/ 50,
/*23*/ 51,
/*24*/ 53,
/*25*/ 54,
/*26*/ 56,
/*27*/ 57,
/*28*/ 59,
/*29*/ 61,
/*30*/ 63,
/*31*/ 113,
/*32*/ 114,
/*33*/ 115,
/*34*/ 117,
/*35*/ 118,
/*36*/ 120,
/*37*/ 121,
/*38*/ 123,
/*39*/ 125,
/*40*/ 127
};

#define OTPM_VERSION_REG1 0x300E
#define OTPM_VERSION_REG2 0x30F0
#define OTPM_VERSION_REG3 0x3072
#define RESET_REG_ADDR    0x301A
#define MODE_SELECT       0x0100
#define RESET_REG_VAL     0x19
#define MIPI_4_LANE       0x1

Int32 Iss_Ar1820hsUpdateItt(Iss_Ar1820hsObj* pObj, Itt_RegisterParams * gItt_RegisterParams);


UInt AR1820HS_SetRegs()
{

	UInt i=0;
	UInt j = 0;;
    int status = 0;
    int devAddr = AR_1820HS_ADDR;
	Uint16 * regAddr=gAr1820hs_I2c_ctrl.regAddr;
	Uint16 * regValue=gAr1820hs_I2c_ctrl.regValue;

	regAddr[i] = 0x3EF2;  regValue[i] = 0x4056;
	; i++ ; regAddr[i] = 0x3ED6	;  regValue[i] = 0x7C4A;
	; i++ ; regAddr[i] = 0x3EE0	;  regValue[i] = 0x3550;
	; i++ ; regAddr[i] = 0x3ED6	;  regValue[i] = 0x7C6A;
	; i++ ; regAddr[i] = 0x3180	;  regValue[i] = 0xB800;
	; i++ ; regAddr[i] = 0x3ED2	;  regValue[i] = 0xE49D;
	; i++ ; regAddr[i] = 0x3EC8	;  regValue[i] = 0x0074;
	; i++ ; regAddr[i] = 0x3ECC	;  regValue[i] = 0x73F5;
	; i++ ; regAddr[i] = 0x30D4	;  regValue[i] = 0x3005;
	; i++ ; regAddr[i] = 0x3EE8	;  regValue[i] = 0x0000;
	; i++ ; regAddr[i] = 0x3042	;  regValue[i] = 0x0004;
	; i++ ; regAddr[i] = 0x30C0	;  regValue[i] = 0xA000;
	; i++ ; regAddr[i] = 0x30D2	;  regValue[i] = 0x000C;
	; i++ ; regAddr[i] = 0x30D4	;  regValue[i] = 0x3000;
	; i++ ; regAddr[i] = 0x30D6	;  regValue[i] = 0x6800;
	; i++ ; regAddr[i] = 0x30DA	;  regValue[i] = 0x0060;
	; i++ ; regAddr[i] = 0x30DC	;  regValue[i] = 0x0080;
	; i++ ; regAddr[i] = 0x30EE	;  regValue[i] = 0x3138;
	; i++ ; regAddr[i] = 0x316A	;  regValue[i] = 0x0000;
	; i++ ; regAddr[i] = 0x316C	;  regValue[i] = 0x0400;
	; i++ ; regAddr[i] = 0x316E	;  regValue[i] = 0x0400;
	; i++ ; regAddr[i] = 0x3170	;  regValue[i] = 0x236E;
	; i++ ; regAddr[i] = 0x3172	;  regValue[i] = 0x0286;
	; i++ ; regAddr[i] = 0x3174	;  regValue[i] = 0x0000;
	; i++ ; regAddr[i] = 0x3176	;  regValue[i] = 0x9000;
	; i++ ; regAddr[i] = 0x3178	;  regValue[i] = 0x0000;
	; i++ ; regAddr[i] = 0x3180	;  regValue[i] = 0xF800;
	; i++ ; regAddr[i] = 0x317A	;  regValue[i] = 0x416E;
	; i++ ; regAddr[i] = 0x317C	;  regValue[i] = 0xE104;
	; i++ ; regAddr[i] = 0x3EC8	;  regValue[i] = 0x0041;
	; i++ ; regAddr[i] = 0x3ECC	;  regValue[i] = 0xF5A8;
	; i++ ; regAddr[i] = 0x3ECE	;  regValue[i] = 0xFAF7;
	; i++ ; regAddr[i] = 0x3ED0	;  regValue[i] = 0x00FF;
	; i++ ; regAddr[i] = 0x3ED2	;  regValue[i] = 0x449D;
	; i++ ; regAddr[i] = 0x3ED4	;  regValue[i] = 0x50C8;
	; i++ ; regAddr[i] = 0x3ED6	;  regValue[i] = 0x784A;
	; i++ ; regAddr[i] = 0x3ED8	;  regValue[i] = 0xC0A0;
	; i++ ; regAddr[i] = 0x3EDA	;  regValue[i] = 0x1142;
	; i++ ; regAddr[i] = 0x3EDC	;  regValue[i] = 0x1078;
	; i++ ; regAddr[i] = 0x3EDE	;  regValue[i] = 0x00E0;
	; i++ ; regAddr[i] = 0x3EE0	;  regValue[i] = 0x3558;
	; i++ ; regAddr[i] = 0x3EE2	;  regValue[i] = 0x8A3C;
	; i++ ; regAddr[i] = 0x3EE4	;  regValue[i] = 0x2B79;
	; i++ ; regAddr[i] = 0x3EE6	;  regValue[i] = 0xF87A;
	; i++ ; regAddr[i] = 0x3EE8	;  regValue[i] = 0x2800;
	; i++ ; regAddr[i] = 0x3EEA	;  regValue[i] = 0x6400;
	; i++ ; regAddr[i] = 0x3EEC	;  regValue[i] = 0x0400;
	; i++ ; regAddr[i] = 0x3EEE	;  regValue[i] = 0x0000;
	; i++ ; regAddr[i] = 0x3EF0	;  regValue[i] = 0x043F;
	; i++ ; regAddr[i] = 0x3EF2	;  regValue[i] = 0xC456;
	; i++ ; regAddr[i] = 0x3EFA	;  regValue[i] = 0xA484;
	; i++ ; regAddr[i] = 0x3EFC	;  regValue[i] = 0x2020;
	; i++ ; regAddr[i] = 0x3EFE	;  regValue[i] = 0x9096;
	; i++ ; regAddr[i] = 0x3F20	;  regValue[i] = 0x0249;
	; i++ ; regAddr[i] = 0x3F26	;  regValue[i] = 0x4428;
	; i++ ; regAddr[i] = 0x3F2C	;  regValue[i] = 0x111D;
	; i++ ; regAddr[i] = 0x3F32	;  regValue[i] = 0x1104;
	; i++ ; regAddr[i] = 0x3F38	;  regValue[i] = 0x1328;
	; i++ ; regAddr[i] = 0x3F3A	;  regValue[i] = 0x8827;
	; i++ ; regAddr[i] = 0x3F3A	;  regValue[i] = 0x002D;
	; i++ ; regAddr[i] = 0x0112	;  regValue[i] = 0x0C0C;
	; i++ ; regAddr[i] = 0x0308	;  regValue[i] = 0x000C;
	; i++ ; regAddr[i] = 0x3172	;  regValue[i] = 0x4286;
	; i++ ; regAddr[i] = 0x3F2F	;  regValue[i] = 0x1123;
	; i++ ; regAddr[i] = 0x3F38	;  regValue[i] = 0x1328;
	; i++ ; regAddr[i] = 0x3EDA	;  regValue[i] = 0x1152;
	; i++ ; regAddr[i] = 0x3ECE	;  regValue[i] = 0xA8A6;
	; i++ ; regAddr[i] = 0x3ECC	;  regValue[i] = 0xF386;
	; i++ ; regAddr[i] = 0x3154	;  regValue[i] = 0x0260;
	; i++ ; regAddr[i] = 0x3EF2	;  regValue[i] = 0x6456;
	; i++ ; regAddr[i] = 0x3ED8	;  regValue[i] = 0x60A0;
	; i++ ; regAddr[i] = 0x3EDC	;  regValue[i] = 0x1179;
	; i++ ; regAddr[i] = 0x3180	;  regValue[i] = 0xBFFF;
	; i++ ; regAddr[i] = 0x3F38	;  regValue[i] = 0x1328;
	; i++ ; regAddr[i] = 0x3F3C	;  regValue[i] = 0x0003;
	; i++ ; regAddr[i] = 0x3120	;  regValue[i] = 0x0021;
	; i++ ; regAddr[i] = 0x3EE8	;  regValue[i] = 0x1100;
	; i++ ; regAddr[i] = 0x3ED6	;  regValue[i] = 0x786B;
	; i++ ; regAddr[i] = 0x3EDC	;  regValue[i] = 0x0179;
	; i++ ; regAddr[i] = 0x3ED2	;  regValue[i] = 0x449D;
	; i++ ; regAddr[i] = 0x3ECC	;  regValue[i] = 0x7786;
	; i++ ; regAddr[i] = 0x3EC8	;  regValue[i] = 0x0074;
	; i++ ; regAddr[i] = 0x3F3C	;  regValue[i] = 0x0001;
	; i++ ; regAddr[i] = 0x3EE0	;  regValue[i] = 0x1558;
	; i++ ; regAddr[i] = 0x3ED8	;  regValue[i] = 0x60A0;
	; i++ ; regAddr[i] = 0x3EDC	;  regValue[i] = 0x0179;
	; i++ ; regAddr[i] = 0x3EF2	;  regValue[i] = 0x6456;
	; i++ ; regAddr[i] = 0x30D2	;  regValue[i] = 0x000C;
	; i++ ; regAddr[i] = 0x31E0	;  regValue[i] = 0x0741;
	; i++ ; regAddr[i] = 0x31E6	;  regValue[i] = 0x1000;
	; i++ ; regAddr[i] = 0x3F04	;  regValue[i] = 0x0002;
	; i++ ; regAddr[i] = 0x3F06	;  regValue[i] = 0x0004;
	; i++ ; regAddr[i] = 0x3F08	;  regValue[i] = 0x0008;
	; i++ ; regAddr[i] = 0x3F10	;  regValue[i] = 0x0303;
	; i++ ; regAddr[i] = 0x3F12	;  regValue[i] = 0x0303;
	; i++ ; regAddr[i] = 0x3F14	;  regValue[i] = 0x0303;
	; i++ ; regAddr[i] = 0x3F16	;  regValue[i] = 0x0204;
	; i++ ; regAddr[i] = 0x3F1C	;  regValue[i] = 0x03FF;
	; i++ ; regAddr[i] = 0x0100; regValue[i] = 0x0000;		// MODE_SELECT
	; i++;

    for(j=0; j < i; j++)
    {
        status = Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&gAr1820hs_I2c_ctrl.regAddr[j],&gAr1820hs_I2c_ctrl.regValue[j],gAr1820hs_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }
    }
	return i;
}



UInt AR1820HS_Set1080p60Regs()
{
	int status = 0;
	UInt i=0;
	UInt j = 0;
	Uint16 * regAddr=gAr1820hs_I2c_ctrl.regAddr;
	Uint16 * regValue=gAr1820hs_I2c_ctrl.regValue;
	int devAddr = AR_1820HS_ADDR;

	regAddr[i] = 0x31AE; regValue[i] = 0x0204; 	// SERIAL_FORMAT
	; i++ ; regAddr[i] = 0x0112; regValue[i] = 0x0A0A; 	// CCP_DATA_FORMAT
	; i++ ; regAddr[i] = 0x0300; regValue[i] = 0x0003; 	// VT_PIX_CLK_DIV
	; i++ ; regAddr[i] = 0x0306; regValue[i] = 0x0030;	// PLL_MULTIPLIER
	; i++ ; regAddr[i] = 0x0308; regValue[i] = 0x000A; 	// OP_PIX_CLK_DIV
	; i++ ; regAddr[i] = 0x3016; regValue[i] = 0x0111; 	// ROW_SPEED
	; i++ ; regAddr[i] = 0x3064; regValue[i] = 0x5800; 	// SMIA_TEST
	; i++ ; regAddr[i] = 0x3F3A; regValue[i] = 0x0027; 	// ANALOG_CONTROL8
	; i++ ; regAddr[i] = 0x0112; regValue[i] = 0x0A0A; 	// CCP_DATA_FORMAT
	; i++ ; regAddr[i] = 0x0308; regValue[i] = 0x000A; 	// OP_PIX_CLK_DIV
	; i++ ; regAddr[i] = 0x3172; regValue[i] = 0x0286; 	// ANALOG_CONTROL2
	; i++ ; regAddr[i] = 0x3EDA; regValue[i] = 0x1152; 	// DAC_LD_18_19
	; i++ ; regAddr[i] = 0x3EF2; regValue[i] = 0x7A57; 	// DAC_LD_42_43
	; i++ ; regAddr[i] = 0x3EDC; regValue[i] = 0x01C9; 	// DAC_LD_20_21
	; i++ ; regAddr[i] = 0x3154; regValue[i] = 0x0500;	// GLOBAL_BOOST
	; i++ ; regAddr[i] = 0x3120; regValue[i] = 0x0021; 	// GAIN_DITHER_CONTROL
	; i++ ; regAddr[i] = 0x0104; regValue[i] = 0x01; 	// GROUPED_PARAMETER_HOLD
	; i++ ; regAddr[i] = 0x3012; regValue[i] = 0x060F; 	// COARSE_INTEGRATION_TIME_
	; i++ ; regAddr[i] = 0x300A; regValue[i] = 0x0720; 	// FRAME_LENGTH_LINE_
	; i++ ; regAddr[i] = 0x300C; regValue[i] = 0x1300; 	// LINE_LENGTH_PCK_
	; i++ ; regAddr[i] = 0x3002; regValue[i] = 0x01D8; 	// Y_ADDR_START_
	; i++ ; regAddr[i] = 0x3004; regValue[i] = 0x000C; 	// X_ADDR_START_
	; i++ ; regAddr[i] = 0x30BE; regValue[i] = 0x0004; 	// X_OUTPUT_OFFSET
	; i++ ; regAddr[i] = 0x3006; regValue[i] = 0x0C9A;	// Y_ADDR_END_ //@@@##
	; i++ ; regAddr[i] = 0x3008; regValue[i] = 0x132D; 	// X_ADDR_END_
	; i++ ; regAddr[i] = 0x034C; regValue[i] = 0x0990; 	// X_OUTPUT_SIZE, 1920 -> 1952, 7A0
	; i++ ; regAddr[i] = 0x034E; regValue[i] = 0x0562; 	// Y_OUTPUT_SIZE, 1080 -> 1112, 458
	; i++ ; regAddr[i] = 0x303E; regValue[i] = 0x0000; 	// READ_STYLE
	; i++ ; regAddr[i] = 0x3040; regValue[i] = 0x28C3; 	// READ_MODE
	; i++ ; regAddr[i] = 0x0400; regValue[i] = 0x0000; 	// SCALING_MODE
	; i++ ; regAddr[i] = 0x0404; regValue[i] = 0x0010; 	// SCALE_M
	; i++ ; regAddr[i] = 0x0408; regValue[i] = 0x1010; 	// SECOND_RESIDUAL
	; i++ ; regAddr[i] = 0x040A; regValue[i] = 0x0210; 	// SECOND_CROP
	; i++ ; regAddr[i] = 0x3F00; regValue[i] = 0x000E; 	// BM_T0 //@@@##
	; i++ ; regAddr[i] = 0x3F02; regValue[i] = 0x0072; 	// BM_T1 //@@@##
	; i++ ; regAddr[i] = 0x0104; regValue[i] = 0x00; 	// GROUPED_PARAMETER_HOLD
	; i++ ; regAddr[i] = 0x3F0A; regValue[i] = 0x0302; 	// NOISE_FLOOR10
	; i++ ; regAddr[i] = 0x3F0C; regValue[i] = 0x0403; 	// NOISE_FLOOR32
	; i++ ; regAddr[i] = 0x3F1E; regValue[i] = 0x0004; 	// NOISE_COEF //@@@##
	; i++ ; regAddr[i] = 0x3F40; regValue[i] = 0x0909; 	// COUPLE_K_FACTOR0 //@@@++
	; i++ ; regAddr[i] = 0x3F42; regValue[i] = 0x0303; 	// COUPLE_K_FACTOR1 //@@@++
	; i++ ; regAddr[i] = 0x3F18; regValue[i] = 0x0304; 	// CROSSFACTOR1
	; i++ ; regAddr[i] = 0x305E; regValue[i] = 0x2D81; 	// GLOBAL_GAIN
	; i++ ; regAddr[i] = 0x3ED2; regValue[i] = 0xE49D; 	// DAC_LD_10_11
	; i++ ; regAddr[i] = 0x3EDC; regValue[i] = 0x01C9; 	// DAC_LD_20_21
	; i++ ; regAddr[i] = 0x3EE0; regValue[i] = 0x9518; 	// DAC_LD_24_25
	; i++ ; regAddr[i] = 0x3EC8; regValue[i] = 0x00A4; 	// DAC_LD_0_1
	; i++ ; regAddr[i] = 0x3ED8; regValue[i] = 0x60B0; 	// DAC_LD_16_17
	; i++ ; regAddr[i] = 0x3ECC; regValue[i] = 0x7386;	// DAC_LD_4_5
	; i++ ; regAddr[i] = 0x3F1A; regValue[i] = 0x0404; 	// CROSSFACTOR2 //@@@##
	; i++ ; regAddr[i] = 0x3F44; regValue[i] = 0x0101;	// COUPLE_K_FACTOR2
	; i++ ; regAddr[i] = 0x0100; regValue[i] = 0x0000;		// MODE_SELECT
	; i++ ; regAddr[i] = 0x31BC; regValue[i] = 0x8509;  // for continue-clk
	; i++ ; regAddr[i] = 0x3070; regValue[i] = 0x0000;    // disable pattern
	; i++;

	for(j=0; j < i; j++)
	{
		status = Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&gAr1820hs_I2c_ctrl.regAddr[j],&gAr1820hs_I2c_ctrl.regValue[j],gAr1820hs_I2c_ctrl.numRegs);

		if(status != FVID2_SOK)
		{
			Vps_rprintf("I2C write Error,index:%d\n",j);
			return status;
		}	
	}

	return i;
}



UInt AR1820HS_Set1080p60RegsTiming2()
{
	int status = 0;
	UInt i=0;
	UInt j = 0;
	Uint16 * regAddr=gAr1820hs_I2c_ctrl.regAddr;
	Uint16 * regValue=gAr1820hs_I2c_ctrl.regValue;
	int devAddr = AR_1820HS_ADDR;

	regAddr[i] = 0x31AE; regValue[i] = 0x0204; 	// SERIAL_FORMAT
	; i++ ; regAddr[i] = 0x0112; regValue[i] = 0x0A0A; 	// CCP_DATA_FORMAT
	; i++ ; regAddr[i] = 0x0300; regValue[i] = 0x0003; 	// VT_PIX_CLK_DIV
	; i++ ; regAddr[i] = 0x0306; regValue[i] = 0x0030;	// PLL_MULTIPLIER
	; i++ ; regAddr[i] = 0x0308; regValue[i] = 0x000A; 	// OP_PIX_CLK_DIV
	; i++ ; regAddr[i] = 0x3016; regValue[i] = 0x0111; 	// ROW_SPEED
	; i++ ; regAddr[i] = 0x3064; regValue[i] = 0x5800; 	// SMIA_TEST
	; i++ ; regAddr[i] = 0x3F3A; regValue[i] = 0x0027; 	// ANALOG_CONTROL8
	; i++ ; regAddr[i] = 0x0112; regValue[i] = 0x0A0A; 	// CCP_DATA_FORMAT
	; i++ ; regAddr[i] = 0x0308; regValue[i] = 0x000A; 	// OP_PIX_CLK_DIV
	; i++ ; regAddr[i] = 0x3172; regValue[i] = 0x0286; 	// ANALOG_CONTROL2
	; i++ ; regAddr[i] = 0x3EDA; regValue[i] = 0x1152; 	// DAC_LD_18_19
	; i++ ; regAddr[i] = 0x3EF2; regValue[i] = 0x7A57; 	// DAC_LD_42_43
	; i++ ; regAddr[i] = 0x3EDC; regValue[i] = 0x01C9; 	// DAC_LD_20_21
	; i++ ; regAddr[i] = 0x3154; regValue[i] = 0x0500;	// GLOBAL_BOOST
	; i++ ; regAddr[i] = 0x3120; regValue[i] = 0x0021; 	// GAIN_DITHER_CONTROL
	; i++ ; regAddr[i] = 0x0104; regValue[i] = 0x01; 	// GROUPED_PARAMETER_HOLD
	; i++ ; regAddr[i] = 0x3012; regValue[i] = 0x045C; 	// COARSE_INTEGRATION_TIME_
	; i++ ; regAddr[i] = 0x300A; regValue[i] = 0x045D; 	// FRAME_LENGTH_LINE_
	; i++ ; regAddr[i] = 0x300C; regValue[i] = 0x0F88; 	// LINE_LENGTH_PCK_
	; i++ ; regAddr[i] = 0x3002; regValue[i] = 0x0302; 	// Y_ADDR_START_
	; i++ ; regAddr[i] = 0x3004; regValue[i] = 0x021C; 	// X_ADDR_START_
	; i++ ; regAddr[i] = 0x30BE; regValue[i] = 0x0004; 	// X_OUTPUT_OFFSET
	; i++ ; regAddr[i] = 0x3006; regValue[i] = 0x0B70;	// Y_ADDR_END_ //@@@##
	; i++ ; regAddr[i] = 0x3008; regValue[i] = 0x111D; 	// X_ADDR_END_
	; i++ ; regAddr[i] = 0x034C; regValue[i] = 0x0780; 	// X_OUTPUT_SIZE, 1920 -> 1952, 7A0
	; i++ ; regAddr[i] = 0x034E; regValue[i] = 0x0438; 	// Y_OUTPUT_SIZE, 1080 -> 1112, 458
	; i++ ; regAddr[i] = 0x303E; regValue[i] = 0x0000; 	// READ_STYLE
	; i++ ; regAddr[i] = 0x3040; regValue[i] = 0x28C3; 	// READ_MODE
	; i++ ; regAddr[i] = 0x0400; regValue[i] = 0x0000; 	// SCALING_MODE
	; i++ ; regAddr[i] = 0x0404; regValue[i] = 0x0010; 	// SCALE_M
	; i++ ; regAddr[i] = 0x0408; regValue[i] = 0x1010; 	// SECOND_RESIDUAL
	; i++ ; regAddr[i] = 0x040A; regValue[i] = 0x0210; 	// SECOND_CROP
	; i++ ; regAddr[i] = 0x3F00; regValue[i] = 0x0008; 	// BM_T0 //@@@##
	; i++ ; regAddr[i] = 0x3F02; regValue[i] = 0x0041; 	// BM_T1 //@@@##
	; i++ ; regAddr[i] = 0x0104; regValue[i] = 0x00; 	// GROUPED_PARAMETER_HOLD
	; i++ ; regAddr[i] = 0x3F0A; regValue[i] = 0x0302; 	// NOISE_FLOOR10
	; i++ ; regAddr[i] = 0x3F0C; regValue[i] = 0x0403; 	// NOISE_FLOOR32
	; i++ ; regAddr[i] = 0x3F1E; regValue[i] = 0x0004; 	// NOISE_COEF //@@@##
	; i++ ; regAddr[i] = 0x3F40; regValue[i] = 0x0909; 	// COUPLE_K_FACTOR0 //@@@++
	; i++ ; regAddr[i] = 0x3F42; regValue[i] = 0x0303; 	// COUPLE_K_FACTOR1 //@@@++
	; i++ ; regAddr[i] = 0x3F18; regValue[i] = 0x0304; 	// CROSSFACTOR1
	; i++ ; regAddr[i] = 0x305E; regValue[i] = 0x2D81; 	// GLOBAL_GAIN
	; i++ ; regAddr[i] = 0x3ED2; regValue[i] = 0xE49D; 	// DAC_LD_10_11
	; i++ ; regAddr[i] = 0x3EDC; regValue[i] = 0x01C9; 	// DAC_LD_20_21
	; i++ ; regAddr[i] = 0x3EE0; regValue[i] = 0x9518; 	// DAC_LD_24_25
	; i++ ; regAddr[i] = 0x3EC8; regValue[i] = 0x00A4; 	// DAC_LD_0_1
	; i++ ; regAddr[i] = 0x3ED8; regValue[i] = 0x60B0; 	// DAC_LD_16_17
	; i++ ; regAddr[i] = 0x3ECC; regValue[i] = 0x7386;	// DAC_LD_4_5
	; i++ ; regAddr[i] = 0x3F1A; regValue[i] = 0x0404; 	// CROSSFACTOR2 //@@@##
	; i++ ; regAddr[i] = 0x3F44; regValue[i] = 0x0101;	// COUPLE_K_FACTOR2
	; i++ ; regAddr[i] = 0x0100; regValue[i] = 0x0000;		// MODE_SELECT
	; i++ ; regAddr[i] = 0x31BC; regValue[i] = 0x8509;  // for continue-clk
	; i++ ; regAddr[i] = 0x3070; regValue[i] = 0x0000;    // disable pattern
	; i++;

	for(j=0; j < i; j++)
	{
		status = Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&gAr1820hs_I2c_ctrl.regAddr[j],&gAr1820hs_I2c_ctrl.regValue[j],gAr1820hs_I2c_ctrl.numRegs);

		if(status != FVID2_SOK)
		{
			Vps_rprintf("I2C write Error,index:%d\n",j);
			return status;
		}	
	}

	return i;
}



/*
  System init for AR1820HS driver

  This API
  - create semaphore locks needed
  - registers driver to FVID2 sub-system
  - gets called as part of Iss_deviceInit()
*/
Int32 Iss_Ar1820hsInit (  )
{
    Semaphore_Params semParams;
    Int32 status = FVID2_SOK;

Vps_printf ( "Iss_Ar1820hsInit \n" );

    /*
     * Set to 0's for global object, descriptor memory
     */
    memset ( &gIss_Ar1820hsCommonObj, 0, sizeof ( gIss_Ar1820hsCommonObj ) );

    /*
     * Create global AR1820HS lock
     */
    Semaphore_Params_init ( &semParams );

    semParams.mode = Semaphore_Mode_BINARY;

    gIss_Ar1820hsCommonObj.lock = Semaphore_create ( 1u, &semParams, NULL );

    if ( gIss_Ar1820hsCommonObj.lock == NULL )
        status = FVID2_EALLOC;


	Transplant_DRV_imgsSetRegs();
    if ( status == FVID2_SOK )
    {
        gIss_Ar1820hsCommonObj.fvidDrvOps.create = ( FVID2_DrvCreate ) Iss_Ar1820hsCreate;
        gIss_Ar1820hsCommonObj.fvidDrvOps.delete = Iss_Ar1820hsDelete;
        gIss_Ar1820hsCommonObj.fvidDrvOps.control = Iss_Ar1820hsControl;
        gIss_Ar1820hsCommonObj.fvidDrvOps.queue = NULL;
        gIss_Ar1820hsCommonObj.fvidDrvOps.dequeue = NULL;
        gIss_Ar1820hsCommonObj.fvidDrvOps.processFrames = NULL;
        gIss_Ar1820hsCommonObj.fvidDrvOps.getProcessedFrames = NULL;
        gIss_Ar1820hsCommonObj.fvidDrvOps.drvId = FVID2_ISS_SENSOR_AR1820HS_DRV;

        status = FVID2_registerDriver ( &gIss_Ar1820hsCommonObj.fvidDrvOps );
        if ( status != FVID2_SOK )
        {
          
          Vps_printf ( "Iss_Ar1820hsInit, status != FVID2_SOK \n" );
          
            /*
             * Error - free acquired resources
             */
            Semaphore_delete ( &gIss_Ar1820hsCommonObj.lock );
        }
    }

    if ( status != FVID2_SOK )
    {
        Vps_printf ( " ERROR %s:%s:%d !!!\n", __FILE__, __FUNCTION__, __LINE__ );
    }

    return status;
}



/*
  Create API that gets called when FVID2_create is called

  This API does not configure the AR1820HSis any way.

  This API
  - validates parameters
  - allocates driver handle
  - stores create arguments in its internal data structure.

  Later the create arguments will be used when doing I2C communcation with
  AR1820HS

  drvId - driver ID, must be FVID2_ISS_VID_DEC_AR1820HS_DRV
  instanceId - must be 0
  createArgs - create arguments
  createStatusArgs - create status
  fdmCbParams  - NOT USED, set to NULL

  returns NULL in case of any error
*/
Fdrv_Handle Iss_Ar1820hsCreate ( UInt32 drvId,
                                UInt32 instanceId,
                                Ptr createArgs,
                                Ptr createStatusArgs,
                                const FVID2_DrvCbParams * fdmCbParams )
{
    Iss_Ar1820hsObj *pObj;
    Iss_SensorCreateParams *sensorCreateArgs
        = ( Iss_SensorCreateParams * ) createArgs;

    Iss_SensorCreateStatus *sensorCreateStatus
        = ( Iss_SensorCreateStatus * ) createStatusArgs;

	Vps_printf ( "DBG: +++++++++>== !! In AR1820HS drv !!==<+++++++++++++++++ !!!\n" );
    /*
     * check parameters
     */
    if ( sensorCreateStatus == NULL )
        return NULL;

    sensorCreateStatus->retVal = FVID2_SOK;

    if ( drvId != FVID2_ISS_SENSOR_AR1820HS_DRV
         || instanceId != 0 || sensorCreateArgs == NULL )
    {
        sensorCreateStatus->retVal = FVID2_EBADARGS;
        return NULL;
    }

    if ( sensorCreateArgs->deviceI2cInstId >= ISS_DEVICE_I2C_INST_ID_MAX )
    {
        sensorCreateStatus->retVal = FVID2_EINVALID_PARAMS;
        return NULL;
    }

    /*
     * allocate driver handle
     */
    pObj = Iss_Ar1820hsAllocObj (  );
    if ( pObj == NULL )
    {
        sensorCreateStatus->retVal = FVID2_EALLOC;
        return NULL;
    }

   Iss_Ar1820hsLock (  );
	
		
		//Transplant_AR1820HS_HDR_Enable(0);
   Iss_Ar1820hsUnlock (  );
    /*
     * copy parameters to allocate driver handle
     */
    memcpy ( &pObj->createArgs, sensorCreateArgs,  sizeof ( *sensorCreateArgs ) );

    Iss_Ar1820hsResetRegCache(pObj);

    /*
     * return driver handle object pointer
     */
    return pObj;
}


/* Control API that gets called when FVID2_control is called

  This API does handle level semaphore locking

  handle - AR1820HS driver handle
  cmd - command
  cmdArgs - command arguments
  cmdStatusArgs - command status

  returns error in case of
  - illegal parameters
  - I2C command RX/TX error
*/
Int32 Iss_Ar1820hsControl ( Fdrv_Handle handle,
                           UInt32 cmd, Ptr cmdArgs, Ptr cmdStatusArgs )
{
    Iss_Ar1820hsObj *pObj = ( Iss_Ar1820hsObj * ) handle;
    Int32 status;
    
    if ( pObj == NULL )
        return FVID2_EBADARGS;

    /*
     * lock handle
     */
    Iss_Ar1820hsLockObj ( pObj );

    switch ( cmd )
    {
        case FVID2_START:
            break;

        case FVID2_STOP:
            break;

        case IOCTL_ISS_SENSOR_GET_CHIP_ID:
            break;

        case IOCTL_ISS_SENSOR_RESET:
            break;

        case IOCTL_ISS_SENSOR_REG_WRITE:
            break;

        case IOCTL_ISS_SENSOR_REG_READ:
            break;

        case IOCTL_ISS_SENSOR_UPDATE_EXP_GAIN:
            status = Iss_Ar1820hsUpdateExpGain ( pObj, cmdArgs);
			break;
         
        case IOCTL_ISS_SENSOR_UPDATE_FRAMERATE:
            status = Iss_Ar1820hsUpdateFrameRate( pObj, cmdArgs);	
            break;
            
        case IOCTL_ISS_SENSOR_FRAME_RATE_SET:
            break;
        
		case IOCTL_ISS_SENSOR_UPDATE_ITT:
			 status = Iss_Ar1820hsUpdateItt(pObj, cmdArgs);
            break;
			
        case IOCTL_ISS_SENSOR_PWM_CONFIG:
        	break;

        default:
            status = FVID2_EUNSUPPORTED_CMD;
            break;
    }

    /*
     * unlock handle
     */
    Iss_Ar1820hsUnlockObj ( pObj );

    return status;
}



/*
  Delete function that is called when FVID2_delete is called

  This API
  - free's driver handle object

  handle - driver handle
  deleteArgs - NOT USED, set to NULL

*/
Int32 Iss_Ar1820hsDelete ( Fdrv_Handle handle, Ptr deleteArgs )
{
    Iss_Ar1820hsObj *pObj = ( Iss_Ar1820hsObj * ) handle;

    if ( pObj == NULL )
        return FVID2_EBADARGS;

    /*
     * free driver handle object
     */
    Iss_Ar1820hsFreeObj ( pObj );

    return FVID2_SOK;
}

/*
  System de-init for AR1820HS driver

  This API
  - de-registers driver from FVID2 sub-system
  - delete's allocated semaphore locks
  - gets called as part of Iss_deviceDeInit()
*/
Int32 Iss_Ar1820hsDeInit (  )
{
    /*
     * Unregister FVID2 driver
     */
    FVID2_unRegisterDriver ( &gIss_Ar1820hsCommonObj.fvidDrvOps );

    /*
     * Delete semaphore's
     */
    Semaphore_delete ( &gIss_Ar1820hsCommonObj.lock );

    return 0;
}


/*
  Handle level lock
*/
Int32 Iss_Ar1820hsLockObj ( Iss_Ar1820hsObj * pObj )
{
    Semaphore_pend ( pObj->lock, BIOS_WAIT_FOREVER );

    return FVID2_SOK;
}

/*
  Handle level unlock
*/
Int32 Iss_Ar1820hsUnlockObj ( Iss_Ar1820hsObj * pObj )
{
    Semaphore_post ( pObj->lock );

    return FVID2_SOK;
}

/*
  Global driver level lock
*/
Int32 Iss_Ar1820hsLock (  )
{
    Semaphore_pend ( gIss_Ar1820hsCommonObj.lock, BIOS_WAIT_FOREVER );

    return FVID2_SOK;
}

/*
  Global driver level unlock
*/
Int32 Iss_Ar1820hsUnlock (  )
{
    Semaphore_post ( gIss_Ar1820hsCommonObj.lock );

    return FVID2_SOK;
}

/*
  Allocate driver object

  Searches in list of driver handles and allocate's a 'NOT IN USE' handle
  Also create's handle level semaphore lock

  return NULL in case handle could not be allocated
*/
Iss_Ar1820hsObj *Iss_Ar1820hsAllocObj (  )
{
    UInt32 handleId;
    Iss_Ar1820hsObj *pObj;
    Semaphore_Params semParams;
    UInt32 found = FALSE;

    /*
     * Take global lock to avoid race condition
     */
    Iss_Ar1820hsLock (  );

    /*
     * find a unallocated object in pool
     */
    for ( handleId = 0; handleId < ISS_DEVICE_MAX_HANDLES; handleId++ )
    {

        pObj = &gIss_Ar1820hsCommonObj.Ar1820hsObj[handleId];

        if ( pObj->state == ISS_AR1820HS_OBJ_STATE_UNUSED )
        {
            /*
             * free object found
             */

            /*
             * init to 0's
             */
            memset ( pObj, 0, sizeof ( *pObj ) );

            /*
             * init state and handle ID
             */
            pObj->state = ISS_AR1820HS_OBJ_STATE_IDLE;
            pObj->handleId = handleId;

            /*
             * create driver object specific semaphore lock
             */
            Semaphore_Params_init ( &semParams );

            semParams.mode = Semaphore_Mode_BINARY;

            pObj->lock = Semaphore_create ( 1u, &semParams, NULL );

            found = TRUE;

            if ( pObj->lock == NULL )
            {
                /*
                 * Error - release object
                 */
                found = FALSE;
                pObj->state = ISS_AR1820HS_OBJ_STATE_UNUSED;
            }

            break;
        }
    }

    /*
     * Release global lock
     */
    Iss_Ar1820hsUnlock (  );

    if ( found )
        return pObj;    /* Free object found return it */

    /*
     * free object not found, return NULL
     */
    return NULL;
}

/*
  De-Allocate driver object

  Marks handle as 'NOT IN USE'
  Also delete's handle level semaphore lock
*/
Int32 Iss_Ar1820hsFreeObj ( Iss_Ar1820hsObj * pObj )
{
    /*
     * take global lock
     */
    Iss_Ar1820hsLock (  );

    if ( pObj->state != ISS_AR1820HS_OBJ_STATE_UNUSED )
    {
        /*
         * mark state as unused
         */
        pObj->state = ISS_AR1820HS_OBJ_STATE_UNUSED;

        /*
         * delete object locking semaphore
         */
        Semaphore_delete ( &pObj->lock );
    }

    /*
     * release global lock
     */
    Iss_Ar1820hsUnlock (  );

    return FVID2_SOK;
}


/*
	This function will enable the CSI2 streaming.
*/
void Iss_CSI2EnableStreaming()
{
	int status = FVID2_SOK;
	
	Vps_rprintf("Enable CSI2 streaming...\n",0);
	
	/* Start streaming */
	gAr1820hs_I2c_ctrl.regAddr[0]  = RESET_REG_ADDR;
	gAr1820hs_I2c_ctrl.regValue[0] = 0x1C;

	status = Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId,AR_1820HS_ADDR,&gAr1820hs_I2c_ctrl.regAddr[0],&gAr1820hs_I2c_ctrl.regValue[0],gAr1820hs_I2c_ctrl.numRegs);	
	if(status != FVID2_SOK)
	{
		Vps_rprintf("I2C write Error,index:%d\n",0);
		return;
	}	
	Task_sleep(100);
}


Int32 Iss_Ar1820hsUpdateItt(Iss_Ar1820hsObj* pObj,
                           Itt_RegisterParams * gItt_RegisterParams)
{
    int status = 0;
    int devAddr, count = 0;
    I2c_Ctrl gAr1820hs_I2c_ctrl;

    gAr1820hs_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
    devAddr = AR_1820HS_ADDR;
    if (gItt_RegisterParams->Control == 1)
    {
        gAr1820hs_I2c_ctrl.regAddr[count] = gItt_RegisterParams->regAddr;
        gAr1820hs_I2c_ctrl.regValue[count] = gItt_RegisterParams->regValue;
        gAr1820hs_I2c_ctrl.numRegs = 1;
        status =
            Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId, devAddr,
                              &gAr1820hs_I2c_ctrl.regAddr[count],
                              &gAr1820hs_I2c_ctrl.regValue[count],
                              gAr1820hs_I2c_ctrl.numRegs);
    }
    else if (gItt_RegisterParams->Control == 0)
    {
        gAr1820hs_I2c_ctrl.regAddr[count] = gItt_RegisterParams->regAddr;
        gAr1820hs_I2c_ctrl.regValue[count] = 0;
        gAr1820hs_I2c_ctrl.numRegs = 1;
        status =
            Iss_deviceRead16(gAr1820hs_I2c_ctrl.i2cInstId, devAddr,
                             &gAr1820hs_I2c_ctrl.regAddr[count],
                             &gAr1820hs_I2c_ctrl.regValue[count],
                             gAr1820hs_I2c_ctrl.numRegs);
        count = 0;
        gItt_RegisterParams->regValue = gAr1820hs_I2c_ctrl.regValue[count];
    }
    return status;
}


/*
  Update exposure and gain value from the 2A
*/
Int32 Iss_Ar1820hsUpdateExpGain ( Iss_Ar1820hsObj * pObj, Ptr createArgs )
{
	Int32 status = FVID2_SOK;
	Int32 exp_time_rows;
	Int32 exp_time_rows_max;
	//UInt16 g_int=0;
	UInt32 i2cInstId = Iss_platformGetI2cInstId();
	//Int32 devAddr = AR_1820HS_ADDR;

	gAr1820hs_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
	//devAddr = AR_1820HS_ADDR;

	exp_time_rows_max = 2200;	//AR_1820HS_COARSE_INT_TIME_MAX;
	//g_int =AR1820HS_GainTableMap(ti2a_output_params.sensorGain);

	exp_time_rows = exp_time_rows_max;
	// clamp the calculated exposure time to its maximum value
	if( exp_time_rows > exp_time_rows_max )
	{
		exp_time_rows = exp_time_rows_max;
	}

	/*
     * take global lock
     */
    Iss_Ar1820hsLock (  );
    {
		//UInt16 regAddr= AR1820HS_ANALOG_GAIN;
		/*status = Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&regAddr,&g_int,1);	//RAJAT: Need to un-comment after initial bringup and 2A is done*/
	}

     /*
     * release global lock
     */
    Iss_Ar1820hsUnlock (  );

	return status;
}

/*
  Update sensor frame rate
*/
Int32 Iss_Ar1820hsUpdateFrameRate (Iss_Ar1820hsObj *pObj,Ptr createArgs)
{
	Int32 status = 0;
    Iss_CaptFrameRate *pFrameRatePrm = (Iss_CaptFrameRate*)createArgs;
	
	Iss_Ar1820hsLock();
	
	pFrameRatePrm->FrameRate = 30;
	
	Iss_Ar1820hsUnlock();

	return status;	
}


int Transplant_DRV_imgsSetRegs()
{
	int status = FVID2_SOK;

	gAr1820hs_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
	Int32 devAddr = AR_1820HS_ADDR;
	UInt i; //,j;
	
	Uint16 readRegAddr[20];
	memset(readRegAddr,0,20);
	Uint16 readRegValue[20];
	memset(readRegValue,0,20);

	gAr1820hs_I2c_ctrl.numRegs = 1;

	Vps_rprintf("Transplant_DRV_imgsSetRegs...\n");
	Vps_rprintf("gAr1820hs_I2c_ctrl.i2cInstId=%d\n", gAr1820hs_I2c_ctrl.i2cInstId);
	Vps_rprintf("devAddr=0x%X\n", devAddr);

	/* Reset sensor */
	gAr1820hs_I2c_ctrl.regAddr[0]  = RESET_REG_ADDR;
	gAr1820hs_I2c_ctrl.regValue[0] = RESET_REG_VAL;;

	status = Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&gAr1820hs_I2c_ctrl.regAddr[0],&gAr1820hs_I2c_ctrl.regValue[0],gAr1820hs_I2c_ctrl.numRegs);
	if(status != FVID2_SOK)
	{
		Vps_rprintf("I2C write Error,index:%d\n",0);
		return status;
	}	

	Task_sleep(100);// Every write to RESET_REG_ADDR is followed by 100ms of delay

	i = AR1820HS_SetRegs();
#if 1
	/* Mode select */
	gAr1820hs_I2c_ctrl.regAddr[0]  = MODE_SELECT;
	gAr1820hs_I2c_ctrl.regValue[0] = 0x0000;
	status = Iss_deviceWrite16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&gAr1820hs_I2c_ctrl.regAddr[0],&gAr1820hs_I2c_ctrl.regValue[0],gAr1820hs_I2c_ctrl.numRegs);

	if(status != FVID2_SOK)
	{
		Vps_rprintf("I2C write Error,index:%d\n",0);
		return status;
	}
	
	Task_sleep(100);
#endif
	
	Vps_rprintf("Polling register R0x303C[1] till it become to 1\n");
	
	while(1)
	{
		readRegAddr[0]=0x303C;
		status = Iss_deviceRead16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&readRegAddr[0],&readRegValue[0],1);
		if(status != FVID2_SOK)
		{
			Vps_rprintf("I2C read Error,index:%d\n",0);
			return status;
		}
		
		if(readRegValue[0] & (1<<1))
		{
		  Vps_rprintf("DBG:1 !!\n");
		  break;
		}
		
		Vps_rprintf("DBG:0 !!\n");
		
		Task_sleep(1);
	}
	
	//PLL, timing and address settings
	i = AR1820HS_Set1080p60Regs();
	Vps_rprintf("AR1820HS_Set1080p60Regs, i=%d\n",i);
	

#if 1	
	Vps_rprintf("Read chip revision\n");
	readRegAddr[0]=0x0;
	
	status = Iss_deviceRead16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&readRegAddr[0],&readRegValue[0],1);
	if(status != FVID2_SOK)
	{
		Vps_rprintf("I2C read Error,index:%d\n",0);
		return status;
	}
		
	Vps_rprintf("readRegAddr=0x%X, readRegValue=0x%X\n", readRegAddr[0], readRegValue[0]);
#else
  Vps_rprintf("Read chip information\n");
  for(j=0; j < i; j++)
	{
    status = Iss_deviceRead16(gAr1820hs_I2c_ctrl.i2cInstId,devAddr,&gAr1820hs_I2c_ctrl.regAddr[j],&RegValue,1);

		if(status != FVID2_SOK)
		{
			Vps_rprintf("I2C read Error,index:%d\n",j);
			return status;
		}
		
		Vps_rprintf("readRegAddr=0x%X, readRegValue=0x%X\n", gAr1820hs_I2c_ctrl.regAddr[j], RegValue);
	}
#endif

	//printf("Finished Demo Init with AR1820HS\n");
	return FVID2_SOK;
}




Int32 Iss_Ar1820hsPinMux (  )
{
	/* setup CAM input pin mux */
	*PINCNTL156 = 0x00050002;				// select function 2 with receiver enabled and pullup/down disabled  - only works in supervisor mode
	*PINCNTL157 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL158 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL159 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL160 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL161 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL162 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL163 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL164 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL165 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL166 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL167 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL168 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL169 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL170 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL171 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL172 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL173 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL174 = 0x00050002;				// select function 2  - only works in supervisor mode
	*PINCNTL175 = 0x00050002;				// select function 2  - only works in supervisor mode

    #if defined(TI_8107_BUILD)
    *PINCNTL263 = 0xD0001;   /* i2c0_scl_mux0 */
    *PINCNTL264 = 0xD0001;   /* i2c0_sda_mux0 */
    #endif
    #if defined(TI_814X_BUILD)
    *PINCNTL74 = 0x00020;
    *PINCNTL75 = 0x00020;
    #endif

    *PINCNTL111 = 0x80;
    *PINCNTL215 = 0x80;
    *PINCNTL216 = 0x80;

    *GIO_INPUT_OUTPUT_DIR &= ~(0x1 << 17);
    *GIO_ENABLE_DISABLE_WAKEUP &= ~(0x1 << 17);

    *GIO3_INPUT_OUTPUT_DIR &= ~((0x1 << 7) + (0x1 << 8));
    *GIO3_ENABLE_DISABLE_WAKEUP &= ~((0x1 << 7) + (0x1 << 8));

    /* power on sequence , off -> on */
    // IMAGE_CLK_EN low
    *GIO3_CLEAR_DATA = 0x1 << 8;
    
    // wait 1ms
    Task_sleep(1);

    // IMAGE_RESET low
    *GIO3_CLEAR_DATA = 0x1 << 7;
    
    // wait 1ms
    Task_sleep(1);

    // IMAGE_PWR_EN low
    *GIO_CLEAR_DATA = 0x1 << 17;
    
    // wait 500ms
    Task_sleep(500);

    // IMAGE_PWR_EN high
    *GIO_WRITE_DATA |= 0x1 << 17;
    
    // wait 10ms
    Task_sleep(10);

    // IMAGE_RESET high
    *GIO3_WRITE_DATA |= 0x1 << 7;
    
    // wait 1ms
    Task_sleep(1);
    
    // IMAGE_CLK_EN high
    *GIO3_WRITE_DATA |= 0x1 << 8;

    // wait 10ms
    Task_sleep(10);

	return 0;
}

