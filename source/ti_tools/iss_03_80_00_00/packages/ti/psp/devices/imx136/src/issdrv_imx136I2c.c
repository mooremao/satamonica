/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <ti/psp/devices/mn34041/src/issdrv_mn34041Priv.h>

/*
  write to device registers
*/
Int32 Iss_Mn34041RegWrite ( Iss_Mn34041Obj * pObj,
                            Iss_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 1;
    //Iss_SensorCreateParams *pCreateArgs;
    return status;
}

/*
  read from device registers
*/
Int32 Iss_Mn34041RegRead ( Iss_Mn34041Obj * pObj,
                           Iss_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 1;
    //Iss_SensorCreateParams *pCreateArgs;
    return status;
}
