/*******************************************************************************
 *                                                                             *
 *               Copyright (c) 2014 TomTom International B.V.                  *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <ti/psp/iss/hal/iss/isp/isif/inc/isif.h>
#include <ti/psp/devices/s5k2p1/src/issdrv_s5k2p1Priv.h>
#include <ti/psp/devices/s5k2p1/issdrv_s5k2p1_config.h>
#include <ti/psp/platforms/iss_platform.h>

/* #define PRINT_EXPGAIN */
/* #define S5K2P1_VERBOSE */

/* Global object storing all information related to all S5K2P1 driver handles */
#pragma DATA_SECTION(gIss_S5k2p1CommonObj, ".bss")
#pragma DATA_SECTION(gS5k2p1_I2c_ctrl, ".bss")

#define CLIP(X,Y,Z)        if(X>Z){X=Z;}else if(X<Y){X=Y;}
#define FIXED_FINE_TIME    0x000A

static Iss_S5k2p1CommonObj    gIss_S5k2p1CommonObj;
static I2c_Ctrl               gS5k2p1_I2c_ctrl;
extern Iss_SensorStatus       gSensorStreamingStatus;
extern ti2a_output            ti2a_output_params;
extern ti2aControlParams_t    gTi2aControlParams;

static Uint16 gFrame_timing_line_length_pck;   /* line_length_pck(0xD0000342) */
static Uint16 gFrame_timing_frame_length_lines;/* frame_length_lines(0xD0000340)*/
static Uint16 gClocks_pll_multiplier;          /* pll_multiplier (0xD0000306) */
static Uint32 gFrameRate = 60;
static FVID2_Standard gSensorMode = FVID2_STD_1080P_60;

/*
 * Sensor Orientation
 * 0 - No sensor rotation
 * 3 - 180 degree sensor rotation (default)
 */
static Uint32 gSensorOrientation = 3;

/* Cached values for exposure and gain control */
static double prevAGain;
static int prevCoarseTime, prevFineTime;

Int32 Iss_S5k2p1UpdateItt(Iss_S5k2p1Obj* pObj, Itt_RegisterParams * gItt_RegisterParams);
UInt32 Iss_captIsStillCapMode();
UInt32 Iss_getAewbStabFrameRate();
UInt32 Iss_getStillCapFrameRate();
UInt32 Iss_captGetSensorFrameRate();
UInt32 Iss_captIsTimelapseMode();
void Ae_repartition_night_mode(UInt32 input_exposure,UInt32 input_gain,UInt32 * output_exposure,
                               UInt32 * output_gain);

UInt S5K2P1_SetCommonRegs()
{
    int status = 0;
    UInt i = 0;
    UInt j = 0;
    Uint32 * regAddr = gS5k2p1_I2c_ctrl.regAddr;
    Uint16 * regValue = gS5k2p1_I2c_ctrl.regValue;
    int devAddr = S5K_2P1_ADDR;

    for (; i < REG_ARR_SIZE(sensorCommonRegs); i++) {

        regAddr[i] = sensorCommonRegs[i][0];
        regValue[i] = sensorCommonRegs[i][1];
    }

    for (j = 0; j < i; j++)
    {
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &gS5k2p1_I2c_ctrl.regAddr[j],
                &gS5k2p1_I2c_ctrl.regValue[j],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }
    }

    return i;
}

/*
   System init for S5K2P1 driver

   This API
   - create semaphore locks needed
   - registers driver to FVID2 sub-system
   - gets called as part of Iss_deviceInit()
 */
Int32 Iss_S5k2p1Init(void)
{
    Semaphore_Params semParams;
    Int32 status = FVID2_SOK;

#ifdef S5K2P1_VERBOSE
    Vps_printf("Iss_S5k2p1Init \n");
#endif
    /*
     * Set to 0's for global object, descriptor memory
     */
    memset(&gIss_S5k2p1CommonObj, 0, sizeof(gIss_S5k2p1CommonObj));

    /*
     * Create global S5K2P1 lock
     */
    Semaphore_Params_init(&semParams);

    semParams.mode = Semaphore_Mode_BINARY;

    gIss_S5k2p1CommonObj.lock = Semaphore_create(1u, &semParams, NULL);

    if (gIss_S5k2p1CommonObj.lock == NULL)
        status = FVID2_EALLOC;

    Transplant_DRV_imgsSetRegs();

    if (status == FVID2_SOK)
    {
        gIss_S5k2p1CommonObj.fvidDrvOps.create = (FVID2_DrvCreate) Iss_S5k2p1Create;
        gIss_S5k2p1CommonObj.fvidDrvOps.delete = Iss_S5k2p1Delete;
        gIss_S5k2p1CommonObj.fvidDrvOps.control = Iss_S5k2p1Control;
        gIss_S5k2p1CommonObj.fvidDrvOps.queue = NULL;
        gIss_S5k2p1CommonObj.fvidDrvOps.dequeue = NULL;
        gIss_S5k2p1CommonObj.fvidDrvOps.processFrames = NULL;
        gIss_S5k2p1CommonObj.fvidDrvOps.getProcessedFrames = NULL;
        gIss_S5k2p1CommonObj.fvidDrvOps.drvId = FVID2_ISS_SENSOR_S5K2P1_DRV;

        status = FVID2_registerDriver(&gIss_S5k2p1CommonObj.fvidDrvOps);
        if(status != FVID2_SOK)
        {
            Vps_printf("Iss_S5k2p1Init, status != FVID2_SOK \n");

            /*
             * Error - free acquired resources
             */
            Semaphore_delete(&gIss_S5k2p1CommonObj.lock);
        }
    }

    if (status != FVID2_SOK)
    {
        Vps_printf(" ERROR %s:%s:%d !!!\n", __FILE__, __FUNCTION__, __LINE__);
    }

    return status;
}

/*
   Configures sensor according to the standard passed.

   This API does not configure the S5K2P1 is any way.

   This API
   - validates parameters
   - allocates driver handle
   - stores create arguments in its internal data structure.

   Later the create arguments will be used when doing I2C communcation with
   S5K2P1

   drvId - driver ID, must be FVID2_ISS_VID_DEC_S5K2P1_DRV
   instanceId - must be 0
   createArgs - create arguments
   createStatusArgs - create status
   fdmCbParams  - NOT USED, set to NULL

   returns NULL in case of any error
 */
UInt32 previous_cap_preview_exp   = 9981;
UInt32 previous_cap_preview_gain  = 1000;
UInt32 previous_cap_preview_dgain = 256;
UInt32 backup_dgain = 0;

Int32 Iss_S5k2p1SetMode(FVID2_Standard standard)
{
    int status = 0;
    UInt i = 0;
    UInt j = 0;
    Uint32 * regAddr = gS5k2p1_I2c_ctrl.regAddr;
    Uint16 * regValue = gS5k2p1_I2c_ctrl.regValue;
    int devAddr = S5K_2P1_ADDR;
    double aGain;
    UInt32 coarse_time,fine_time;
    UInt32 aewbStabFrameRate;
    UInt32 stillCapFrameRate;
    UInt32 frameRate;
    isif_gain_offset_cfg_t isifgain;

    switch(standard)
    {
        default:
        case FVID2_STD_4656_3492_10:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_4656_3492_10\n");
            for (; i < REG_ARR_SIZE(sensorConfig_16MP_10_still); i++) {
                regAddr[i] = sensorConfig_16MP_10_still[i][0];
                regValue[i] = sensorConfig_16MP_10_still[i][1];
            }
            frameRate = 10;
            break;

        case FVID2_STD_4656_3492_5:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_4656_3492_5\n");
            for (; i < REG_ARR_SIZE(sensorConfig_16MP_5_burst); i++) {
                regAddr[i] = sensorConfig_16MP_5_burst[i][0];
                regValue[i] = sensorConfig_16MP_5_burst[i][1];
            }
            frameRate = 5;
            break;

        case FVID2_STD_4K_4608_2592_15:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_4K_4608_2592_15\n");
            for (; i < REG_ARR_SIZE(sensorConfig_4K_15_wide_video); i++) {
                regAddr[i] = sensorConfig_4K_15_wide_video[i][0];
                regValue[i] = sensorConfig_4K_15_wide_video[i][1];
            }
            frameRate = 15;
            break;

        case FVID2_STD_4K_4608_2592_12_5:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_4K_4608_2592_12_5\n");
            for (; i < REG_ARR_SIZE(sensorConfig_4K_12_5_wide_video); i++) {
                regAddr[i] = sensorConfig_4K_12_5_wide_video[i][0];
                regValue[i] = sensorConfig_4K_12_5_wide_video[i][1];
            }
            frameRate = 12;
            break;

        case FVID2_STD_4K_4608_2592_14:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_4K_4608_2592_14\n");
            for (; i < REG_ARR_SIZE(sensorConfig_4K_14_wide_timelapse); i++) {
                regAddr[i] = sensorConfig_4K_14_wide_timelapse[i][0];
                regValue[i] = sensorConfig_4K_14_wide_timelapse[i][1];
            }
            frameRate = 14;
            break;

        case FVID2_STD_2_7K_4608_2592_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_2_7K_4608_2592_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_2_7K_30_wide_video); i++) {
                regAddr[i]  = sensorConfig_2_7K_30_wide_video[i][0];
                regValue[i] = sensorConfig_2_7K_30_wide_video[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_2_7K_4608_2592_25:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_2_7K_4608_2592_25\n");
            for (; i < REG_ARR_SIZE(sensorConfig_2_7K_25_wide_video); i++) {
                regAddr[i] = sensorConfig_2_7K_25_wide_video[i][0];
                regValue[i] = sensorConfig_2_7K_25_wide_video[i][1];
            }
            frameRate = 25;
            break;

        case FVID2_STD_2_7K_3840_2160_25:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_2_7K_3840_2160_25\n");
            for (; i < REG_ARR_SIZE(sensorConfig_2_7K_25_normal_video); i++) {
                regAddr[i] = sensorConfig_2_7K_25_normal_video[i][0];
                regValue[i] = sensorConfig_2_7K_25_normal_video[i][1];
            }
            frameRate = 25;
            break;

        case FVID2_STD_1080P_4608_2592_60:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_4608_2592_60\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_60_wide_video); i++) {
                regAddr[i]  = sensorConfig_1080P_60_wide_video[i][0];
                regValue[i] = sensorConfig_1080P_60_wide_video[i][1];
            }
            frameRate = 60;
            break;

        case FVID2_STD_1080P_3840_2160_60:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_3840_2160_60\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_60_normal_video); i++) {
                regAddr[i] = sensorConfig_1080P_60_normal_video[i][0];
                regValue[i] = sensorConfig_1080P_60_normal_video[i][1];
            }
            frameRate = 60;
            break;

        case FVID2_STD_1080P_4608_2592_50:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_4608_2592_50\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_50_wide_video); i++) {
                regAddr[i] = sensorConfig_1080P_50_wide_video[i][0];
                regValue[i] = sensorConfig_1080P_50_wide_video[i][1];
            }
            frameRate = 50;
            break;

        case FVID2_STD_1080P_3840_2160_50:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_3840_2160_50\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_50_normal_video); i++) {
                regAddr[i] = sensorConfig_1080P_50_normal_video[i][0];
                regValue[i] = sensorConfig_1080P_50_normal_video[i][1];
            }
            frameRate = 50;
            break;

        case FVID2_STD_1080P_4608_2592_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_4608_2592_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_30_wide_video); i++) {
                regAddr[i] = sensorConfig_1080P_30_wide_video[i][0];
                regValue[i] = sensorConfig_1080P_30_wide_video[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_1080P_3840_2160_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_3840_2160_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_30_normal_video); i++) {
                regAddr[i] = sensorConfig_1080P_30_normal_video[i][0];
                regValue[i] = sensorConfig_1080P_30_normal_video[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_1080P_4608_2592_25:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_4608_2592_25\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_25_wide_video); i++) {
                regAddr[i] = sensorConfig_1080P_25_wide_video[i][0];
                regValue[i] = sensorConfig_1080P_25_wide_video[i][1];
            }
            frameRate = 25;
            break;

        case FVID2_STD_1080P_3840_2160_25:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_3840_2160_25\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_25_normal_video); i++) {
                regAddr[i] = sensorConfig_1080P_25_normal_video[i][0];
                regValue[i] = sensorConfig_1080P_25_normal_video[i][1];
            }
            frameRate = 25;
            break;

        case FVID2_STD_720P_4608_2592_120:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_4608_2592_120\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_120_wide_video); i++) {
                regAddr[i] = sensorConfig_720P_120_wide_video[i][0];
                regValue[i] = sensorConfig_720P_120_wide_video[i][1];
            }
            frameRate = 120;
            break;

        case FVID2_STD_720P_3840_2160_120:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_3840_2160_120\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_120_normal_video); i++) {
                regAddr[i] = sensorConfig_720P_120_normal_video[i][0];
                regValue[i] = sensorConfig_720P_120_normal_video[i][1];
            }
            frameRate = 120;
            break;

        case FVID2_STD_720P_4608_2592_100:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_4608_2592_100\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_100_wide_video); i++) {
                regAddr[i] = sensorConfig_720P_100_wide_video[i][0];
                regValue[i] = sensorConfig_720P_100_wide_video[i][1];
            }
            frameRate = 100;
            break;

        case FVID2_STD_720P_3840_2160_100:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_3840_2160_100\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_100_normal_video); i++) {
                regAddr[i] = sensorConfig_720P_100_normal_video[i][0];
                regValue[i] = sensorConfig_720P_100_normal_video[i][1];
            }
            frameRate = 100;
            break;

        case FVID2_STD_720P_4608_2592_60:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_4608_2592_60\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_60_wide_video); i++) {
                regAddr[i] = sensorConfig_720P_60_wide_video[i][0];
                regValue[i] = sensorConfig_720P_60_wide_video[i][1];
            }
            frameRate = 60;
            break;

        case FVID2_STD_720P_3840_2160_60:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_3840_2160_60\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_60_normal_video); i++) {
                regAddr[i] = sensorConfig_720P_60_normal_video[i][0];
                regValue[i] = sensorConfig_720P_60_normal_video[i][1];
            }
            frameRate = 60;
            break;

        case FVID2_STD_720P_4608_2592_50:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_4608_2592_50\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_50_wide_video); i++) {
                regAddr[i] = sensorConfig_720P_50_wide_video[i][0];
                regValue[i] = sensorConfig_720P_50_wide_video[i][1];
            }
            frameRate = 50;
            break;

        case FVID2_STD_720P_3840_2160_50:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_720P_3840_2160_50\n");
            for (; i < REG_ARR_SIZE(sensorConfig_720P_50_normal_video); i++) {
                regAddr[i] = sensorConfig_720P_50_normal_video[i][0];
                regValue[i] = sensorConfig_720P_50_normal_video[i][1];
            }
            frameRate = 50;
            break;

        case FVID2_STD_WVGA_4608_2592_180:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_WVGA_4608_2592_180\n");
            for (; i < REG_ARR_SIZE(sensorConfig_WVGA_180_wide_video); i++) {
                regAddr[i] = sensorConfig_WVGA_180_wide_video[i][0];
                regValue[i] = sensorConfig_WVGA_180_wide_video[i][1];
            }
            frameRate = 180;
            break;

        case FVID2_STD_WVGA_4608_2592_150:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_WVGA_4608_2592_150\n");
            for (; i < REG_ARR_SIZE(sensorConfig_WVGA_150_wide_video); i++) {
                regAddr[i] = sensorConfig_WVGA_150_wide_video[i][0];
                regValue[i] = sensorConfig_WVGA_150_wide_video[i][1];
            }
            frameRate = 150;
            break;

        case FVID2_STD_WVGA_4608_2592_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_WVGA_4608_2592_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_WVGA_30_wide_viewfinder); i++) {
                regAddr[i] = sensorConfig_WVGA_30_wide_viewfinder[i][0];
                regValue[i] = sensorConfig_WVGA_30_wide_viewfinder[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_WVGA_3840_2160_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_WVGA_3840_2160_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_WVGA_30_normal_viewfinder); i++) {
                regAddr[i] = sensorConfig_WVGA_30_normal_viewfinder[i][0];
                regValue[i] = sensorConfig_WVGA_30_normal_viewfinder[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_WVGA_4608_2592_50:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_WVGA_4608_2592_50\n");
            for (; i < REG_ARR_SIZE(sensorConfig_WVGA_50_wide_stabilization); i++) {
                regAddr[i] = sensorConfig_WVGA_50_wide_stabilization[i][0];
                regValue[i] = sensorConfig_WVGA_50_wide_stabilization[i][1];
            }
            frameRate = 50;
            break;

        case FVID2_STD_8MP_4656_3492_15:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_8MP_4656_3492_15\n");
            for (; i < REG_ARR_SIZE(sensorConfig_8MP_15_still); i++) {
                regAddr[i] = sensorConfig_8MP_15_still[i][0];
                regValue[i] = sensorConfig_8MP_15_still[i][1];
            }
            frameRate = 15;
            break;

        case FVID2_STD_8MP_4656_3492_5:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_8MP_4656_3492_5\n");
            for (; i < REG_ARR_SIZE(sensorConfig_8MP_5_burst); i++) {
                regAddr[i] = sensorConfig_8MP_5_burst[i][0];
                regValue[i] = sensorConfig_8MP_5_burst[i][1];
            }
            frameRate = 5;
            break;

        case FVID2_STD_1080P_1920_1080_C_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_1920_1080_C_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_30_C_factory); i++) {
                regAddr[i] = sensorConfig_1080P_30_C_factory[i][0];
                regValue[i] = sensorConfig_1080P_30_C_factory[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_LT_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_1920_1080_LT_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_30_LT_factory); i++) {
                regAddr[i] = sensorConfig_1080P_30_LT_factory[i][0];
                regValue[i] = sensorConfig_1080P_30_LT_factory[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_RT_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_1920_1080_RT_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_30_RT_factory); i++) {
                regAddr[i] = sensorConfig_1080P_30_RT_factory[i][0];
                regValue[i] = sensorConfig_1080P_30_RT_factory[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_LB_30:
            Vps_printf("S5K2P1: Sensor standard FVID2_STD_1080P_1920_1080_LB_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_30_LB_factory); i++) {
                regAddr[i] = sensorConfig_1080P_30_LB_factory[i][0];
                regValue[i] = sensorConfig_1080P_30_LB_factory[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_RB_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1080P_1920_1080_RB_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1080P_30_RB_factory); i++) {
                regAddr[i] = sensorConfig_1080P_30_RB_factory[i][0];
                regValue[i] = sensorConfig_1080P_30_RB_factory[i][1];
            }
            frameRate = 30;
            break;

        case FVID2_STD_1440_1080_2332_1748_30:
            Vps_printf("S5K2P1: Sensor standard is FVID2_STD_1440_1080_2332_1748_30\n");
            for (; i < REG_ARR_SIZE(sensorConfig_1440P_30_factory); i++) {
                regAddr[i] = sensorConfig_1440P_30_factory[i][0];
                regValue[i] = sensorConfig_1440P_30_factory[i][1];
            }
            frameRate = 30;
            break;
    }

    gSensorMode = standard;
    aewbStabFrameRate = Iss_getAewbStabFrameRate();
    stillCapFrameRate = Iss_getStillCapFrameRate();

    if((Iss_captIsStillCapMode() == 1) && (stillCapFrameRate == frameRate))
    {
        double frame_length_lines;
        double line_length_pck;
        double out_frame_length_lines;
        double out_line_length_pck;
        UInt32 output_exposure,output_gain;

        if((Iss_captIsTimelapseMode() == 1) && (gTi2aControlParams.maxCapExposure > 100000))
        {
               /*AE repartitioning for Night mode*/
               Ae_repartition_night_mode(ti2a_output_params.sensorExposureCapture,
                                         ti2a_output_params.sensorGainCapture,
                                         &output_exposure,
                                         &output_gain);

               ti2a_output_params.sensorExposureCapture =  output_exposure;
               ti2a_output_params.sensorGainCapture     =  output_gain;
        }

        /*Extend frame-rate if capture exposure is greater than possible exposure value */
        if(ti2a_output_params.sensorExposureCapture > (1000000/stillCapFrameRate))
        {
               line_length_pck     = (double) regValue[0];
               frame_length_lines  = (double) regValue[1];
               out_line_length_pck = line_length_pck;

               out_frame_length_lines = ((frame_length_lines *
                                         (double)ti2a_output_params.sensorExposureCapture *
                                         (double) stillCapFrameRate)/1000000) + 12;

               if(out_frame_length_lines > 0xFFFF)
               {
                   out_line_length_pck = (out_frame_length_lines* line_length_pck)/0xFFFF;
                   out_frame_length_lines = 0xFFFF;

               }

               regValue[0] = (UInt16)out_line_length_pck;
               regValue[1] = (UInt16)out_frame_length_lines;
        }

    }

    gFrame_timing_line_length_pck      = regValue[0]; /* S5K_2P1_LINE_LENGTH_PCK */
    gFrame_timing_frame_length_lines   = regValue[1]; /* S5K_2P1_FRAME_LENGTH_LINES */
    gClocks_pll_multiplier             = regValue[2]; /* S5K_2P1_PLL_MULTIPLIER */

    for(j = 0; j < i; j++)
    {
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &gS5k2p1_I2c_ctrl.regAddr[j],
                &gS5k2p1_I2c_ctrl.regValue[j],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }
    }

    if(Iss_captIsStillCapMode() == 1)
    {
        /*
         *    In Still Capture Mode apply the gain and exposure computed
         *    during AEWB stabilization phase.
         */

        if(stillCapFrameRate == frameRate)
        {

           /*Backup capture preview values for next capture*/
           previous_cap_preview_exp   = ti2a_output_params.sensorExposure;
           previous_cap_preview_gain  = ti2a_output_params.sensorGain;
           previous_cap_preview_dgain = ti2a_output_params.ipipe_awb_gain.dGain;

           aGain = (ti2a_output_params.sensorGainCapture * 128);
           aGain = aGain/1000;

           coarse_time = (ti2a_output_params.sensorExposureCapture * ((0x1A * gClocks_pll_multiplier) /
                         (8 * 1 * 2))) / gFrame_timing_line_length_pck;
           /*Limit the coarse time to allowed range of values*/
           CLIP( coarse_time, 4 , (gFrame_timing_frame_length_lines - 11) );
           /*Fine time fixed value*/
           fine_time   = FIXED_FINE_TIME;

           if(gTi2aControlParams.maxExposure != (1000000/stillCapFrameRate))
           {
               gTi2aControlParams.maxExposure = (1000000/stillCapFrameRate);
               gTi2aControlParams.update |= TI2A_UPDATE_CONTROL_PARAMS_2A;
           }

           /* If we are using capture exposure and capture gain backup the value and reset the digital gain to 1x*/
           backup_dgain = ti2a_output_params.ipipe_awb_gain.dGain;
           ti2a_output_params.ipipe_awb_gain.dGain = 256;

        }
        else
        {

           /*For faster convergence restore previous capture preview AE values*/
           ti2a_output_params.sensorExposure       = previous_cap_preview_exp;
           ti2a_output_params.sensorGain           = previous_cap_preview_gain;
           ti2a_output_params.ipipe_awb_gain.dGain = previous_cap_preview_dgain;

           aGain = (ti2a_output_params.sensorGain * 128);
           aGain = aGain/1000;

           /*Reduce the exposure if it is greater than max value*/
           if(ti2a_output_params.sensorExposure > (1000000/aewbStabFrameRate))
           {
               /*Set the maximum possible value with 200 step size*/
               ti2a_output_params.sensorExposure = ((1000000/aewbStabFrameRate)/200)*200;
           }

           coarse_time = (ti2a_output_params.sensorExposure * ((0x1A * gClocks_pll_multiplier) /
                         (8 * 1 * 2))) / gFrame_timing_line_length_pck;
           /*Limit the coarse time to allowed range of values*/
           CLIP( coarse_time, 4 , (gFrame_timing_frame_length_lines - 11) );
           /*Fine time fixed value*/
           fine_time = FIXED_FINE_TIME;

#ifdef PRINT_EXPGAIN
           Vps_printf("#### sensorGain     = %d,aGain                         = %f\n",
                   ti2a_output_params.sensorGain,aGain);
           Vps_printf("#### sensorExposure = %d,gFrame_timing_line_length_pck = %d\n",
                   ti2a_output_params.sensorExposure,gFrame_timing_line_length_pck);
           Vps_printf("#### coarse_time    = %d,fine_time                     = %d\n",
                   coarse_time,fine_time);
#endif

           if(gTi2aControlParams.maxExposure != (1000000/aewbStabFrameRate))
           {
               gTi2aControlParams.maxExposure = (1000000/aewbStabFrameRate);
               gTi2aControlParams.update |= TI2A_UPDATE_CONTROL_PARAMS_2A;
           }
        }

        /*
         *    Analog Gain
         */
        regAddr[0]  = ANALOG_GAIN_CODE_GLOBAL;
        regValue[0] = (UInt32)aGain; 
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &regAddr[0],
                &regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }

        /* Coarse Integration time */
        regAddr[0]  = S5K_2P1_COARSE_INTEGRATION_TIME;
        regValue[0] = coarse_time & 0xFFFF; 
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &regAddr[0],
                &regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }

        /*
         *    Fine Integration time
         */
        regAddr[0]  = S5K_2P1_FINE_INTEGRATION_TIME;
        regValue[0] = fine_time & 0xFFFF; 
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &regAddr[0],
                &regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }

        isifgain.gain_r = isifgain.gain_gr = isifgain.gain_gb = isifgain.gain_bg = ti2a_output_params.ipipe_awb_gain.dGain * 2;
        isifgain.offset = 0;

        isifgain.gain_offset_featureflag =
              ISIF_H3A_WHITE_BALANCE_FLAG | ISIF_H3A_OFFSET_CTRL_FLAG |
              ISIF_IPIPE_WHITE_BALANCE_FLAG | ISIF_IPIPE_OFFSET_CTRL_FLAG |
              ISIF_SDRAM_WHITE_BALANCE_FLAG | ISIF_SDRAM_OFFSET_CTRL_FLAG;


        isif_config_gain_offset(&isifgain);

    }
    else
    {

        /*Reduce the exposure if it is greater than max value*/
        if(ti2a_output_params.sensorExposure > (1000000/frameRate))
        {
             /*Set the maximum possible value with 200 step size*/
             ti2a_output_params.sensorExposure = ((1000000/frameRate)/200)*200;
        }

        if(backup_dgain)
        {
             ti2a_output_params.ipipe_awb_gain.dGain = backup_dgain;
             backup_dgain = 0;
        }

        aGain = (ti2a_output_params.sensorGain * 128);
        aGain = aGain/1000;

        coarse_time = (ti2a_output_params.sensorExposure * ((0x1A * gClocks_pll_multiplier) /
                    (8 * 1 * 2))) / gFrame_timing_line_length_pck;
        /*Limit the coarse time to allowed range of values*/
        CLIP( coarse_time, 4 , (gFrame_timing_frame_length_lines - 11) );
        /*Fine time fixed value*/
        fine_time = FIXED_FINE_TIME;

        /*
         *    Analog Gain
         */
        regAddr[0]  = ANALOG_GAIN_CODE_GLOBAL;
        regValue[0] = (UInt32)aGain;
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &regAddr[0],
                &regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }

        /* Coarse Integration time */
        regAddr[0]  = S5K_2P1_COARSE_INTEGRATION_TIME;
        regValue[0] = coarse_time & 0xFFFF;
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &regAddr[0],
                &regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }

        /*
         *    Fine Integration time
         */
        regAddr[0]  = S5K_2P1_FINE_INTEGRATION_TIME;
        regValue[0] = fine_time & 0xFFFF;
        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &regAddr[0],
                &regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",j);
            return status;
        }

        isifgain.gain_r = isifgain.gain_gr = isifgain.gain_gb = isifgain.gain_bg = ti2a_output_params.ipipe_awb_gain.dGain * 2;
        isifgain.offset = 0;

        isifgain.gain_offset_featureflag =
              ISIF_H3A_WHITE_BALANCE_FLAG | ISIF_H3A_OFFSET_CTRL_FLAG |
              ISIF_IPIPE_WHITE_BALANCE_FLAG | ISIF_IPIPE_OFFSET_CTRL_FLAG |
              ISIF_SDRAM_WHITE_BALANCE_FLAG | ISIF_SDRAM_OFFSET_CTRL_FLAG;


        isif_config_gain_offset(&isifgain);

        if(gTi2aControlParams.maxExposure != (1000000/frameRate))
        {
               gTi2aControlParams.maxExposure = (1000000/frameRate);
               gTi2aControlParams.update |= TI2A_UPDATE_CONTROL_PARAMS_2A;
        }

    }

    return status;
}

/*
   Create API that gets called when FVID2_create is called

   This API does not configure the S5K2P1 is any way.

   This API
   - validates parameters
   - allocates driver handle
   - stores create arguments in its internal data structure.

   Later the create arguments will be used when doing I2C communcation with
   S5K2P1

   drvId - driver ID, must be FVID2_ISS_VID_DEC_S5K2P1_DRV
   instanceId - must be 0
   createArgs - create arguments
   createStatusArgs - create status
   fdmCbParams  - NOT USED, set to NULL

   returns NULL in case of any error
 */
Fdrv_Handle Iss_S5k2p1Create(UInt32 drvId,
        UInt32 instanceId,
        Ptr createArgs,
        Ptr createStatusArgs,
        const FVID2_DrvCbParams * fdmCbParams)
{
    Iss_S5k2p1Obj *pObj;
    Iss_SensorCreateParams *sensorCreateArgs = (Iss_SensorCreateParams *)createArgs;

    Iss_SensorCreateStatus *sensorCreateStatus = (Iss_SensorCreateStatus *)createStatusArgs;

    /*
     * check parameters
     */
    if (sensorCreateStatus == NULL)
        return NULL;

    sensorCreateStatus->retVal = FVID2_SOK;

    if (drvId != FVID2_ISS_SENSOR_S5K2P1_DRV ||
            instanceId != 0 || sensorCreateArgs == NULL)
    {
        sensorCreateStatus->retVal = FVID2_EBADARGS;
        return NULL;
    }

    if (sensorCreateArgs->deviceI2cInstId >= ISS_DEVICE_I2C_INST_ID_MAX)
    {
        sensorCreateStatus->retVal = FVID2_EINVALID_PARAMS;
        return NULL;
    }

    /*
     * allocate driver handle
     */
    pObj = Iss_S5k2p1AllocObj();
    if (pObj == NULL)
    {
        sensorCreateStatus->retVal = FVID2_EALLOC;
        return NULL;
    }

    Iss_S5k2p1Lock();

    /*
     *    Program the sensor according to the Input standard
     */
    Iss_S5k2p1SetMode((FVID2_Standard)sensorCreateArgs->InputStandard);

    Iss_S5k2p1Unlock();

    /*
     * copy parameters to allocate driver handle
     */
    memcpy(&pObj->createArgs, sensorCreateArgs,  sizeof (*sensorCreateArgs));

    Iss_S5k2p1ResetRegCache(pObj);

    /* Set the default streaming state to disabled */
    gSensorStreamingStatus.streamingEnabled = FALSE;

    /*
     * return driver handle object pointer
     */

    return pObj;
}

/* Control API that gets called when FVID2_control is called

   This API does handle level semaphore locking

   handle - S5K2P1 driver handle
   cmd - command
   cmdArgs - command arguments
   cmdStatusArgs - command status

   returns error in case of
   - illegal parameters
   - I2C command RX/TX error
 */
Int32 Iss_S5k2p1Control(Fdrv_Handle handle, UInt32 cmd, Ptr cmdArgs, Ptr cmdStatusArgs)
{
    Iss_S5k2p1Obj *pObj = (Iss_S5k2p1Obj *) handle;
    Int32 status = FVID2_SOK;

    if (pObj == NULL)
        return FVID2_EBADARGS;

    if (IOCTL_ISS_SENSOR_UPDATE_EXP_GAIN != cmd)
    {
        /* Invalidate the cached values */
        prevAGain = 0;
        prevCoarseTime = 0;
        prevFineTime = 0;
    }

    /* lock handle */
    Iss_S5k2p1LockObj(pObj);

    switch (cmd)
    {
        case FVID2_START:
            Iss_S5k2p1EnableStreaming(TRUE);
            break;

        case FVID2_STOP:
            Iss_S5k2p1EnableStreaming(FALSE);
            break;

        case IOCTL_ISS_SENSOR_GET_CHIP_ID:
            break;

        case IOCTL_ISS_SENSOR_RESET:
            break;

        case IOCTL_ISS_SENSOR_REG_WRITE:
            break;

        case IOCTL_ISS_SENSOR_REG_READ:
            break;

        case IOCTL_ISS_SENSOR_UPDATE_EXP_GAIN:
            status = Iss_S5k2p1UpdateExpGain(pObj, cmdArgs);
            break;

        case IOCTL_ISS_SENSOR_UPDATE_FRAMERATE:
            status = Iss_S5k2p1UpdateFrameRate(pObj, cmdArgs);
            break;

        case IOCTL_ISS_SENSOR_FRAME_RATE_SET:
            status = Iss_S5k2p1UpdateFrameRate(pObj, cmdArgs);
            break;

        case IOCTL_ISS_SENSOR_UPDATE_ITT:
            status = Iss_S5k2p1UpdateItt(pObj, cmdArgs);
            break;

        case IOCTL_ISS_SENSOR_PWM_CONFIG:
            break;

        case IOCTL_ISS_SENSOR_ENABLE_STREAMING:
            /* Enable/disable streaming support is removed
             * Use FVid2_start/stop for start/stop streaming */
            break;

        case IOCTL_ISS_SENSOR_SET_ORIENTATION:
            Iss_S5k2p1SetOrientation(pObj, cmdArgs);
            break;

        case IOCTL_ISS_SENSOR_SET_VIDEO_MODE:
        {
            Iss_SensorVideoModeParams *pModePrms = (Iss_SensorVideoModeParams *)cmdArgs;
            /* changing the input standard which is set during initialization
             * for dynamic switch
             */
            pObj->createArgs.InputStandard = pModePrms->standard;
            Iss_S5k2p1SetMode((FVID2_Standard)pModePrms->standard);
            break;
        }

        case IOCTL_ISS_SENSOR_GET_STANDARD:
            {
                *((Uint32*)cmdArgs) = (Uint32)pObj->createArgs.InputStandard;
                break;
            }

        case IOCTL_ISS_GET_SENSOR_STREAMING_ENABLED:
            {
                *((Uint32*)cmdArgs) = gSensorStreamingStatus.streamingEnabled;
                break;
            }

        case IOCTL_ISS_CAPT_GET_SENSOR_INFO:
            {
                Iss_CaptSensorInfo *pSensorInfo = (Iss_CaptSensorInfo*)cmdArgs;
                Iss_S5k2p1GetSensorInfo(pSensorInfo);
                break;
            }

        default:
            status = FVID2_EUNSUPPORTED_CMD;
            break;
    }

    /* unlock handle */
    Iss_S5k2p1UnlockObj(pObj);

    return status;
}

/*
   Delete function that is called when FVID2_delete is called

   This API
   - free's driver handle object

   handle - driver handle
   deleteArgs - NOT USED, set to NULL

 */
Int32 Iss_S5k2p1Delete(Fdrv_Handle handle, Ptr deleteArgs)
{
    Iss_S5k2p1Obj *pObj = (Iss_S5k2p1Obj *) handle;

    if (pObj == NULL)
        return FVID2_EBADARGS;

    /*
     * free driver handle object
     */
    Iss_S5k2p1FreeObj (pObj);

    return FVID2_SOK;
}

/*
   System de-init for S5K2P1 driver

   This API
   - de-registers driver from FVID2 sub-system
   - delete's allocated semaphore locks
   - gets called as part of Iss_deviceDeInit()
 */
Int32 Iss_S5k2p1DeInit()
{
    /*
     * Unregister FVID2 driver
     */
    FVID2_unRegisterDriver(&gIss_S5k2p1CommonObj.fvidDrvOps);

    /*
     * Delete semaphore's
     */
    Semaphore_delete(&gIss_S5k2p1CommonObj.lock);

    return 0;
}

/*
   Handle level lock
 */
Int32 Iss_S5k2p1LockObj(Iss_S5k2p1Obj * pObj)
{
    Semaphore_pend(pObj->lock, BIOS_WAIT_FOREVER);

    return FVID2_SOK;
}

/*
   Handle level unlock
 */
Int32 Iss_S5k2p1UnlockObj(Iss_S5k2p1Obj * pObj)
{
    Semaphore_post(pObj->lock);

    return FVID2_SOK;
}

/*
   Global driver level lock
 */
Int32 Iss_S5k2p1Lock(void)
{
    Semaphore_pend(gIss_S5k2p1CommonObj.lock, BIOS_WAIT_FOREVER);

    return FVID2_SOK;
}

/*
   Global driver level unlock
 */
Int32 Iss_S5k2p1Unlock(void)
{
    Semaphore_post(gIss_S5k2p1CommonObj.lock);

    return FVID2_SOK;
}

/*
   Allocate driver object

   Searches in list of driver handles and allocate's a 'NOT IN USE' handle
   Also create's handle level semaphore lock

   return NULL in case handle could not be allocated
 */
Iss_S5k2p1Obj *Iss_S5k2p1AllocObj(void)
{
    UInt32 handleId;
    Iss_S5k2p1Obj *pObj;
    Semaphore_Params semParams;
    UInt32 found = FALSE;

    /*
     * Take global lock to avoid race condition
     */
    Iss_S5k2p1Lock();

    /*
     * find a unallocated object in pool
     */
    for (handleId = 0; handleId < ISS_DEVICE_MAX_HANDLES; handleId++)
    {
        pObj = &gIss_S5k2p1CommonObj.s5k2p1Obj[handleId];

        if (pObj->state == ISS_S5K2P1_OBJ_STATE_UNUSED)
        {
            /*
             * free object found
             */

            /*
             * init to 0's
             */
            memset(pObj, 0, sizeof(*pObj));

            /*
             * init state and handle ID
             */
            pObj->state = ISS_S5K2P1_OBJ_STATE_IDLE;
            pObj->handleId = handleId;

            /*
             * create driver object specific semaphore lock
             */
            Semaphore_Params_init(&semParams);

            semParams.mode = Semaphore_Mode_BINARY;

            pObj->lock = Semaphore_create(1u, &semParams, NULL);

            found = TRUE;

            if (pObj->lock == NULL)
            {
                /*
                 * Error - release object
                 */
                found = FALSE;
                pObj->state = ISS_S5K2P1_OBJ_STATE_UNUSED;
            }

            break;
        }
    }

    /*
     * Release global lock
     */
    Iss_S5k2p1Unlock();

    if (found)
        return pObj;    /* Free object found return it */

    /*
     * free object not found, return NULL
     */
    return NULL;
}

/*
   De-Allocate driver object

   Marks handle as 'NOT IN USE'
   Also delete's handle level semaphore lock
 */
Int32 Iss_S5k2p1FreeObj(Iss_S5k2p1Obj * pObj)
{
    /*
     * take global lock
     */
    Iss_S5k2p1Lock();

    if (pObj->state != ISS_S5K2P1_OBJ_STATE_UNUSED)
    {
        /*
         * mark state as unused
         */
        pObj->state = ISS_S5K2P1_OBJ_STATE_UNUSED;

        /*
         * delete object locking semaphore
         */
        Semaphore_delete(&pObj->lock);
    }

    /*
     * release global lock
     */
    Iss_S5k2p1Unlock();

    return FVID2_SOK;
}

/*
   This function will enable the CSI2 streaming.
 */
void Iss_CSI2EnableStreaming(void)
{
    int status = FVID2_SOK;

#ifdef S5K2P1_VERBOSE
    Vps_rprintf("Enable CSI2 streaming...\n",0);
#endif
    /* Start streaming */
    gS5k2p1_I2c_ctrl.regAddr[0]  = STREAMING_REG_ADDR;
    gS5k2p1_I2c_ctrl.regValue[0] = STREAMING_ENABLE | gSensorOrientation;

    status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
            S5K_2P1_ADDR,
            &gS5k2p1_I2c_ctrl.regAddr[0],
            &gS5k2p1_I2c_ctrl.regValue[0],
            gS5k2p1_I2c_ctrl.numRegs);
    if(status != FVID2_SOK)
    {
        Vps_rprintf("I2C write Error,index:%d\n",0);
        return;
    }
    gSensorStreamingStatus.streamingEnabled = TRUE;
}

void Iss_S5k2p1EnableStreaming(Uint32 enable)
{
    int status = FVID2_SOK;

    Iss_S5k2p1Lock();

    gS5k2p1_I2c_ctrl.regAddr[0]  = STREAMING_REG_ADDR;
    if(enable == 1)
        gS5k2p1_I2c_ctrl.regValue[0] = STREAMING_ENABLE | gSensorOrientation;
    else
        gS5k2p1_I2c_ctrl.regValue[0] = STREAMING_DISABLE | gSensorOrientation;

    status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
            S5K_2P1_ADDR,
            &gS5k2p1_I2c_ctrl.regAddr[0],
            &gS5k2p1_I2c_ctrl.regValue[0],
            gS5k2p1_I2c_ctrl.numRegs);
    if(status != FVID2_SOK)
    {
        Vps_rprintf("I2C write Error,index:%d\n", 0);
    }
    else
    {
        /* Set the streaming state to appropriately */
        gSensorStreamingStatus.streamingEnabled = enable;
    }

    Iss_S5k2p1Unlock();
}

void Iss_S5k2p1SetOrientation(Iss_S5k2p1Obj *pObj, Ptr createArgs)
{
    if(*((UInt32*)createArgs) == 0)
        gSensorOrientation = 0;
    else
        gSensorOrientation = 3;
}

Int32 Iss_S5k2p1UpdateItt(Iss_S5k2p1Obj* pObj,
        Itt_RegisterParams * gItt_RegisterParams)
{
    int status = 0;
    int devAddr, count = 0;
    I2c_Ctrl gS5k2p1_I2c_ctrl;

    gS5k2p1_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
    devAddr = S5K_2P1_ADDR;
    if (gItt_RegisterParams->Control == 1)
    {
        gS5k2p1_I2c_ctrl.regAddr[count] = gItt_RegisterParams->regAddr;
        gS5k2p1_I2c_ctrl.regValue[count] = gItt_RegisterParams->regValue;
        gS5k2p1_I2c_ctrl.numRegs = 1;

        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,
                &gS5k2p1_I2c_ctrl.regAddr[count],
                &gS5k2p1_I2c_ctrl.regValue[count],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error, Iss_S5k2p1I2cWrite\n");
            return status;
        }
    }
    else if (gItt_RegisterParams->Control == 0)
    {
        gS5k2p1_I2c_ctrl.regAddr[count] = gItt_RegisterParams->regAddr;
        gS5k2p1_I2c_ctrl.regValue[count] = 0;
        gS5k2p1_I2c_ctrl.numRegs = 1;

        status = Iss_S5k2p1I2cRead(gS5k2p1_I2c_ctrl.i2cInstId,
                devAddr,&gS5k2p1_I2c_ctrl.regAddr[count],
                &gS5k2p1_I2c_ctrl.regValue[count],
                gS5k2p1_I2c_ctrl.numRegs);

        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error, Iss_S5k2p1I2cRead\n");
            return status;
        }

        count = 0;
        gItt_RegisterParams->regValue = gS5k2p1_I2c_ctrl.regValue[count];
    }

    return status;
}

/*
   Update exposure and gain value from the 2A
 */
Int32 Iss_S5k2p1UpdateExpGain(Iss_S5k2p1Obj * pObj, Ptr createArgs)
{
    Int32 status = FVID2_SOK;
    double aGain;
    int coarseTime, fineTime;
    Uint32 exposure;

    /*
     * take global lock
     */
    Iss_S5k2p1Lock();

    exposure = 1000000 / gFrameRate;

    if(Iss_captIsStillCapMode() == 0)
    {
        /* Update max exposure time */
        if(gTi2aControlParams.maxExposure != exposure)
        {
            gTi2aControlParams.maxExposure = exposure;
            gTi2aControlParams.update |= TI2A_UPDATE_CONTROL_PARAMS_2A;
            Iss_S5k2p1Unlock();
            return status;
        }
    }

    if(ti2a_output_params.sensorExposure > gTi2aControlParams.maxExposure)
    {
        Iss_S5k2p1Unlock();
        return status;
    }

    aGain = (ti2a_output_params.sensorGain * 128);
    aGain = aGain / 1000;

    if (prevAGain != aGain)
    {
        /* Update aGain */
        gS5k2p1_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
        gS5k2p1_I2c_ctrl.regAddr[0] = ANALOG_GAIN_CODE_GLOBAL;
        gS5k2p1_I2c_ctrl.regValue[0] = (int)aGain;

        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId, S5K_2P1_ADDR,
                &gS5k2p1_I2c_ctrl.regAddr[0], &gS5k2p1_I2c_ctrl.regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);
        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",0);
            return status;
        }
    }

    coarseTime = (ti2a_output_params.sensorExposure * ((0x1A * gClocks_pll_multiplier) /
                (8 * 1 * 2))) / gFrame_timing_line_length_pck;
    /*Limit the coarse time to allowed range of values*/
    CLIP( coarseTime, 4 , (gFrame_timing_frame_length_lines - 11) );

    if (prevCoarseTime != coarseTime)
    {
        /* Update coarse integration time (exposure) */
        gS5k2p1_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
        gS5k2p1_I2c_ctrl.regAddr[0]  = S5K_2P1_COARSE_INTEGRATION_TIME;
        gS5k2p1_I2c_ctrl.regValue[0] = coarseTime & 0xFFFF;

        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId, S5K_2P1_ADDR,
                &gS5k2p1_I2c_ctrl.regAddr[0], &gS5k2p1_I2c_ctrl.regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);
        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",0);
            return status;
        }
    }
    /*Fine time fixed value*/
    fineTime = FIXED_FINE_TIME;

    if (prevFineTime != fineTime)
    {
        /* Update fine integration time (exposure) */
        gS5k2p1_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
        gS5k2p1_I2c_ctrl.regAddr[0]  = S5K_2P1_FINE_INTEGRATION_TIME;
        gS5k2p1_I2c_ctrl.regValue[0] = fineTime & 0xFFFF;

        status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId, S5K_2P1_ADDR,
                &gS5k2p1_I2c_ctrl.regAddr[0], &gS5k2p1_I2c_ctrl.regValue[0],
                gS5k2p1_I2c_ctrl.numRegs);
        if(status != FVID2_SOK)
        {
            Vps_rprintf("I2C write Error,index:%d\n",0);
            return status;
        }
    }

    /* Cache the values for reference */
    prevAGain = aGain;
    prevCoarseTime = coarseTime;
    prevFineTime = fineTime;

#if 0
    Vps_printf("adts: aGain = %d\n", aGain);
    Vps_printf("adts: coarseTime = %d\n", coarseTime);
    Vps_printf("adts: fineTime = %d\n", fineTime);
#endif

    /*
     * release global lock
     */
    Iss_S5k2p1Unlock();

    return status;
}

/*
   Update sensor frame rate
 */
Int32 Iss_S5k2p1UpdateFrameRate(Iss_S5k2p1Obj *pObj,Ptr createArgs)
{
    Int32 status = 0;
    Iss_CaptFrameRate *pFrameRatePrm = (Iss_CaptFrameRate*)createArgs;

    Iss_S5k2p1Lock();

    switch(pObj->createArgs.InputStandard)
    {
        default:
        case FVID2_STD_1080P_60:
            pFrameRatePrm->FrameRate = 60;
            break;

        case FVID2_STD_2332_1316_55:
            pFrameRatePrm->FrameRate = 55;
            break;

        case FVID2_STD_4096_2160_15:
            pFrameRatePrm->FrameRate = 15;
            break;

        case FVID2_STD_3840_2160_15:
            pFrameRatePrm->FrameRate = 15;
            break;

        case FVID2_STD_4608_2432_12:
            pFrameRatePrm->FrameRate = 12;
            break;

        case FVID2_STD_4664_3496_10:
            pFrameRatePrm->FrameRate = 10;
            break;

        case FVID2_STD_4656_3492_5:
            pFrameRatePrm->FrameRate = 5;
            break;

        case FVID2_STD_4656_3492_10:
            pFrameRatePrm->FrameRate = 10;
            break;

        case FVID2_STD_4K_4608_2592_15:
            pFrameRatePrm->FrameRate = 15;
            break;

        case FVID2_STD_4K_4608_2592_14:
            pFrameRatePrm->FrameRate = 14;
            break;

        case FVID2_STD_4K_4608_2592_12_5:
            pFrameRatePrm->FrameRate = 12.5;
            break;

        case FVID2_STD_2_7K_4608_2592_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_2_7K_4608_2592_25:
            pFrameRatePrm->FrameRate = 25;
            break;

        case FVID2_STD_2_7K_3840_2160_25:
            pFrameRatePrm->FrameRate = 25;
            break;

        case FVID2_STD_1080P_4608_2592_60:
            pFrameRatePrm->FrameRate = 60;
            break;

        case FVID2_STD_1080P_3840_2160_60:
            pFrameRatePrm->FrameRate = 60;
            break;

        case FVID2_STD_1080P_4608_2592_50:
            pFrameRatePrm->FrameRate = 50;
            break;

        case FVID2_STD_1080P_3840_2160_50:
            pFrameRatePrm->FrameRate = 50;
            break;

        case FVID2_STD_1080P_4608_2592_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_1080P_3840_2160_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_1080P_4608_2592_25:
            pFrameRatePrm->FrameRate = 25;
            break;

        case FVID2_STD_1080P_3840_2160_25:
            pFrameRatePrm->FrameRate = 25;
            break;

        case FVID2_STD_720P_4608_2592_120:
            pFrameRatePrm->FrameRate = 120;
            break;

        case FVID2_STD_720P_3840_2160_120:
            pFrameRatePrm->FrameRate = 120;
            break;

        case FVID2_STD_720P_4608_2592_100:
            pFrameRatePrm->FrameRate = 100;
            break;

        case FVID2_STD_720P_3840_2160_100:
            pFrameRatePrm->FrameRate = 100;
            break;

        case FVID2_STD_720P_4608_2592_60:
            pFrameRatePrm->FrameRate = 60;
            break;

        case FVID2_STD_720P_3840_2160_60:
            pFrameRatePrm->FrameRate = 60;
            break;

        case FVID2_STD_720P_4608_2592_50:
            pFrameRatePrm->FrameRate = 50;
            break;

        case FVID2_STD_720P_3840_2160_50:
            pFrameRatePrm->FrameRate = 50;
            break;

        case FVID2_STD_WVGA_4608_2592_180:
            pFrameRatePrm->FrameRate = 180;
            break;

        case FVID2_STD_WVGA_4608_2592_150:
            pFrameRatePrm->FrameRate = 150;
            break;

        case FVID2_STD_WVGA_4608_2592_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_WVGA_3840_2160_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_WVGA_4608_2592_50:
            pFrameRatePrm->FrameRate = 50;
            break;

        case FVID2_STD_8MP_4656_3492_15:
            pFrameRatePrm->FrameRate = 15;
            break;

        case FVID2_STD_8MP_4656_3492_5:
            pFrameRatePrm->FrameRate = 5;
            break;

        case FVID2_STD_1080P_1920_1080_C_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_LT_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_RT_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_LB_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_1080P_1920_1080_RB_30:
            pFrameRatePrm->FrameRate = 30;
            break;

        case FVID2_STD_1440_1080_2332_1748_30:
            pFrameRatePrm->FrameRate = 30;
            break;

    }

    gFrameRate = pFrameRatePrm->FrameRate;

    Iss_S5k2p1Unlock();

    return status;
}

int Transplant_DRV_imgsSetRegs(void)
{
    int status = FVID2_SOK;

    gS5k2p1_I2c_ctrl.i2cInstId = Iss_platformGetI2cInstId();
    Int32 devAddr = S5K_2P1_ADDR;
#ifdef S5K2P1_VERBOSE
    UInt writeCount = 0;
#endif

    Uint32 readRegAddr[20];
    memset(readRegAddr,0,20);
    Uint16 readRegValue[20];
    memset(readRegValue,0,20);

    gS5k2p1_I2c_ctrl.numRegs = 1;

#ifdef S5K2P1_VERBOSE
    Vps_rprintf("Transplant_DRV_imgsSetRegs...\n");
    Vps_rprintf("gS5k2p1_I2c_ctrl.i2cInstId=%d\n", gS5k2p1_I2c_ctrl.i2cInstId);
    Vps_rprintf("devAddr=0x%X\n", devAddr);
#endif

    /* Reset sensor */
    readRegAddr[0]  = RESET_REG_ADDR;
    readRegValue[0] = RESET_REG_VAL;

    status = Iss_S5k2p1I2cWrite(gS5k2p1_I2c_ctrl.i2cInstId,devAddr,
            &readRegAddr[0], &readRegValue[0], gS5k2p1_I2c_ctrl.numRegs);
    if(status != FVID2_SOK)
    {
        Vps_rprintf("I2C write Error,index:%d\n", 0);
        return status;
    }

    /* Delay recommended by Samsung in Application Note is 10ms, keeping 12ms for safer side */
    Task_sleep(12);

#ifdef S5K2P1_VERBOSE
    writeCount =
#endif
    S5K2P1_SetCommonRegs();

#ifdef S5K2P1_VERBOSE
    Vps_rprintf("S5K2P1_SetCommonRegs, written = %d\n", writeCount);

    Vps_rprintf("Read model id\n");
#endif
    readRegAddr[0] = MODEL_ID_REG_ADDR;

    status = Iss_S5k2p1I2cRead(gS5k2p1_I2c_ctrl.i2cInstId, devAddr,
            &readRegAddr[0], &readRegValue[0], 1);
    if(status != FVID2_SOK)
    {
        Vps_rprintf("I2C read Error,index:%d\n",0);
        return status;
    }
#ifdef S5K2P1_VERBOSE
    Vps_rprintf("readRegAddr=0x%X, readRegValue=0x%X\n", readRegAddr[0], readRegValue[0]);
#endif

    return FVID2_SOK;
}

Int32 Iss_S5k2p1PinMux(void)
{
    *GIO_INPUT_OUTPUT_DIR &= ~(0x1 << 17);
    *GIO_ENABLE_DISABLE_WAKEUP &= ~(0x1 << 17);

    *GIO3_INPUT_OUTPUT_DIR &= ~((0x1 << 7) + (0x1 << 8));
    *GIO3_ENABLE_DISABLE_WAKEUP &= ~((0x1 << 7) + (0x1 << 8));

    /* power on sequence , off -> on */

    /* IMAGE_RESET low */
    *GIO3_CLEAR_DATA = 0x1 << 7;

    /* wait 1ms */
    Task_sleep(1);

    /* IMAGE_CLK_EN high */
    *GIO3_WRITE_DATA |= 0x1 << 8;

    /* wait 1ms */
    Task_sleep(1);

    /* IMAGE_RESET high */
    *GIO3_WRITE_DATA |= 0x1 << 7;

    /* wait 10ms */
    Task_sleep(10);

    return 0;
}

/*
   Get sensor frame rate
 */
UInt32 Iss_S5k2p1GetFrameRate()
{
    return gFrameRate;
}

FVID2_Standard Iss_deviceGetSensorMode()
{
    return gSensorMode;
}

/*
 * Gets the sensor related things
 */
void Iss_S5k2p1GetSensorInfo(Iss_CaptSensorInfo *pSensorInfo)
{
    /* by counting the no of isif vd interrupts that occured between 100ms
     * is used to find the sensor fps
     */
    pSensorInfo->fps = Iss_captGetSensorFrameRate();
}
