/*******************************************************************************
 *                                                                             *
 *               Copyright (c) 2014 TomTom International B.V.                  *
 * Copyright (c) 2013 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _ISSDRV_S5K2P1_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _ISSDRV_S5K2P1_H_

#include <ti/psp/devices/iss_sensorDriver.h>
#include <ti/psp/vps/drivers/fvid2_drvMgr.h>

Int32 Iss_S5k2p1Init (  );

Fdrv_Handle Iss_S5k2p1Create ( UInt32 drvId,
                                UInt32 instanceId,
                                Ptr createArgs,
                                Ptr createStatusArgs,
                                const FVID2_DrvCbParams * fdmCbParams );

Int32 Iss_S5k2p1Control ( Fdrv_Handle handle,
                           UInt32 cmd, Ptr cmdArgs, Ptr cmdStatusArgs );
                           
Int32 Iss_S5k2p1Delete ( Fdrv_Handle handle, Ptr deleteArgs );                       
                           
Int32 Iss_S5k2p1DeInit (  );

Int32 Iss_S5k2p1PinMux (  );

void Iss_S5k2p1GetSensorInfo(Iss_CaptSensorInfo*);

int Transplant_DRV_imgsSetRegs (  );

UInt32 Iss_S5k2p1GetFrameRate (  );

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif

