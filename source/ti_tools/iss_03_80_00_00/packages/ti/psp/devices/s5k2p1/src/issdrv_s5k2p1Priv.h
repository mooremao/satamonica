/*******************************************************************************
 *                                                                             *
 *               Copyright (c) 2014 TomTom International B.V.                  *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _ISS_S5K2P1_PRIV_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _ISS_S5K2P1_PRIV_H_

#include <string.h>
#include <xdc/runtime/System.h>

#include <ti/psp/vps/common/trace.h>
#include <ti/psp/devices/s5k2p1/issdrv_s5k2p1.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/psp/iss/drivers/alg/2A/inc/issdrv_algTIaewb.h>

/* Driver object state - NOT IN USE */
#define ISS_S5K2P1_OBJ_STATE_UNUSED     (0)

/* Driver object state - IN USE and IDLE */
#define ISS_S5K2P1_OBJ_STATE_IDLE       (1)

/* S5K2P1 I2C Port1 offset from Port0  */
#define ISS_S5K2P1_I2C_PORT1_OFFSET     (4)

/* S5K2P1 Default TMDS Core ID to use  */
#define ISS_S5K2P1_CORE_ID_DEFAULT      (0)

/* S5K2P1 Registers - I2C Port 0 */
#define ISS_S5K2P1_REG_VND_IDL          (0x00)
#define ISS_S5K2P1_REG_VND_IDH          (0x01)
#define ISS_S5K2P1_REG_DEV_IDL          (0x02)
#define ISS_S5K2P1_REG_DEV_IDH          (0x03)
#define ISS_S5K2P1_REG_DEV_REV          (0x04)
#define ISS_S5K2P1_REG_SW_RST_0         (0x05)
#define ISS_S5K2P1_REG_STATE            (0x06)
#define ISS_S5K2P1_REG_SW_RST_1         (0x07)
#define ISS_S5K2P1_REG_SYS_CTRL_1       (0x08)
#define ISS_S5K2P1_REG_SYS_SWTCHC       (0x09)
#define ISS_S5K2P1_REG_H_RESL           (0x3A)
#define ISS_S5K2P1_REG_H_RESH           (0x3B)
#define ISS_S5K2P1_REG_V_RESL           (0x3C)
#define ISS_S5K2P1_REG_V_RESH           (0x3D)
#define ISS_S5K2P1_REG_VID_CTRL         (0x48)
#define ISS_S5K2P1_REG_VID_MODE_2       (0x49)
#define ISS_S5K2P1_REG_VID_MODE_1       (0x4A)
#define ISS_S5K2P1_REG_VID_BLANK1       (0x4B)
#define ISS_S5K2P1_REG_VID_BLANK2       (0x4C)
#define ISS_S5K2P1_REG_VID_BLANK3       (0x4D)
#define ISS_S5K2P1_REG_DE_PIXL          (0x4E)
#define ISS_S5K2P1_REG_DE_PIXH          (0x4F)
#define ISS_S5K2P1_REG_DE_LINL          (0x50)
#define ISS_S5K2P1_REG_DE_LINH          (0x51)
#define ISS_S5K2P1_REG_VID_STAT         (0x55)
#define ISS_S5K2P1_REG_VID_CH_MAP       (0x56)
#define ISS_S5K2P1_REG_VID_XPCNTL       (0x6E)
#define ISS_S5K2P1_REG_VID_XPCNTH       (0x6F)

/* S5K2P1 Registers - I2C Port 1 */
#define ISS_S5K2P1_REG_SYS_PWR_DWN_2    (0x3E)
#define ISS_S5K2P1_REG_SYS_PWR_DWN      (0x3F)
#define ISS_S5K2P1_REG_AVI_TYPE         (0x40)
#define ISS_S5K2P1_REG_AVI_DBYTE15      (0x52)

#define     S5K_2P1_I2C_DEV_ADDR        (S5K_2P1_ADDR)
#define     S5K_2P1_I2C_CHANNEL         (1)
#define     S5K_2P1_MODEL_ID_REG_ADDR   (0x00000002)
#define     S5K_2P1_MODEL_ID_REG_VALUE  (0x2C01)

/* xtal frequency in Khz  */
#define ISS_S5K2P1_XTAL_KHZ             (10000)
 
#define PLL_M                           49  /* pll_multiplier */
#define PLL_pre_div                     2   /* pre_pll_clk_div */
#define PLL_P1                          1   /* vt_sys_clk_div */
#define PLL_P2                          8   /* vt_pix_clk_div */

/* color space  */
#define ISS_S5K2P1_AVI_INFO_COLOR_RGB444    (0)
#define ISS_S5K2P1_AVI_INFO_COLOR_YUV444    (1)
#define ISS_S5K2P1_AVI_INFO_COLOR_YUV422    (2)
#define S5K2P1_ANALOG_GAIN                  0x3060
#define S5K2P1_COARSE_IT_TIME_A             0x3012

/* (2*LINE_LENGTH / OUT_CLK)  27.2us,two paths readout */
#define ROW_TIME                            27

#define S5K2P1_GAIN_MAX                     41  /* maximum index in the gain EVT */

/*
  Driver handle object
*/
typedef struct
{

    UInt32 state;               /* handle state */

    UInt32 handleId;            /* handle ID, 0..ISS_DEVICE_MAX_HANDLES-1 */

    Semaphore_Handle lock;      /* handle lock */

    Iss_SensorCreateParams createArgs;    /* create time arguments  */

    UInt8 regCache[2][256];     /* register read cache */

} Iss_S5k2p1Obj;

/*
  Glabal driver object
*/
typedef struct
{
    FVID2_DrvOps fvidDrvOps;    /* FVID2 driver ops */

    Semaphore_Handle lock;      /* global driver lock */

    Iss_S5k2p1Obj s5k2p1Obj[ISS_DEVICE_MAX_HANDLES];
    /*
     * handle objects
     */

} Iss_S5k2p1CommonObj;


extern Iss_S5k2p1CommonObj gIss_S5k2p1CommonObj;

Int32 Iss_S5k2p1GetChipId ( Iss_S5k2p1Obj * pObj,
                             Iss_SensorChipIdParams * pPrm,
                             Iss_SensorChipIdStatus * pStatus );

Int32 Iss_S5k2p1Reset ( Iss_S5k2p1Obj * pObj );

Int32 Iss_S5k2p1Start ( Iss_S5k2p1Obj * pObj );

Int32 Iss_S5k2p1Stop ( Iss_S5k2p1Obj * pObj );

Int32 Iss_S5k2p1RegWrite ( Iss_S5k2p1Obj * pObj,
                            Iss_VideoDecoderRegRdWrParams * pPrm );

Int32 Iss_S5k2p1RegRead ( Iss_S5k2p1Obj * pObj,
                           Iss_VideoDecoderRegRdWrParams * pPrm );

Int32 Iss_S5k2p1LockObj ( Iss_S5k2p1Obj * pObj );
Int32 Iss_S5k2p1UnlockObj ( Iss_S5k2p1Obj * pObj );
Int32 Iss_S5k2p1Lock (  );
Int32 Iss_S5k2p1Unlock (  );
Iss_S5k2p1Obj *Iss_S5k2p1AllocObj (  );
Int32 Iss_S5k2p1FreeObj ( Iss_S5k2p1Obj * pObj );


Int32 Iss_S5k2p1PowerDown ( Iss_S5k2p1Obj * pObj, UInt32 powerDown );
Int32 Iss_S5k2p1OutputEnable ( Iss_S5k2p1Obj * pObj, UInt32 enable );

Int32 Iss_S5k2p1ResetRegCache(Iss_S5k2p1Obj * pObj);
static int s5k2p1_GainTableMap(int sensorGain);
Int32 Iss_S5k2p1UpdateExpGain ( Iss_S5k2p1Obj * pObj, Ptr createArgs );
Int32 Iss_S5k2p1UpdateFrameRate (Iss_S5k2p1Obj *pObj,Ptr createArgs);
void Iss_S5k2p1SetOrientation(Iss_S5k2p1Obj *pObj, Ptr createArgs);
void Iss_S5k2p1EnableStreaming(Uint32 enable);

/* i2c APIs for S5K2P1 write / read, indirect method, 
 * check S5K2P1YX_AN_preliminary_04.pdf page 28 for detail */
Int32 Iss_S5k2p1I2cRead ( UInt32 i2cInstId, UInt32 i2cDevAddr,
                        const UInt32 * regAddr,
                        UInt16 * regValue, UInt32 numRegs );

Int32 Iss_S5k2p1I2cWrite ( UInt32 i2cInstId, UInt32 i2cDevAddr,
                        const UInt32 * regAddr,
                        UInt16 * regValue, UInt32 numRegs );

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /*  _ISS_S5K2P1_PRIV_H_  */
