/*******************************************************************************
 *                                                                             *
 *             Copyright (c) 2014 TomTom International B.V.                    *
 * Copyright (c) 2013 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
#ifndef _ISSDRV_S5K2P1_CONFIG_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _ISSDRV_S5K2P1_CONFIG_H_

#define PSP_I2C_MAX_REG_RD_WR        (0x300)
#define S5K_2P1_ADDR                 (0x10)  /* CCI write address */
#define S5K_2P1_BOARD_PRECONFIG      (TRUE)

typedef struct
{
    UInt32 i2cInstId;
    UInt32 numArgs;
    UInt32 regAddr[PSP_I2C_MAX_REG_RD_WR];
    UInt16 regValue[PSP_I2C_MAX_REG_RD_WR];
    UInt32 numRegs;
} I2c_Ctrl;

#define RESET_REG_ADDR                  0xD0006010
#define RESET_REG_VAL                   0x0001

#define STREAMING_REG_ADDR              0xD0000100

#define STREAMING_ENABLE                0x0100
#define STREAMING_DISABLE               0x0000

#define MODEL_ID_REG_ADDR               0xD0000000
#define ANALOG_GAIN_CODE_GLOBAL         0xD0000204

#define REG_ARR_SIZE(type)              (sizeof(type)/sizeof(type[0]))

#define S5K_2P1_LINE_LENGTH_PCK         (0xD0000342)
#define S5K_2P1_FRAME_LENGTH_LINES      (0xD0000340)
#define S5K_2P1_PLL_MULTIPLIER          (0xD0000306)
#define S5K_2P1_X_ADDR_START            (0xD0000344)
#define S5K_2P1_Y_ADDR_START            (0xD0000346)
#define S5K_2P1_X_ADDR_END              (0xD0000348)
#define S5K_2P1_Y_ADDR_END              (0xD000034A)
#define S5K_2P1_X_EVEN_INC              (0xD0000380)
#define S5K_2P1_X_ODD_INC               (0xD0000382)
#define S5K_2P1_Y_EVEN_INC              (0xD0000384)
#define S5K_2P1_Y_ODD_INC               (0xD0000386)
#define S5K_2P1_BINNING_MODE            (0xD0000900)
#define S5K_2P1_SCALE_M                 (0xD0000404)
#define S5K_2P1_X_OUTPUT_SIZE           (0xD000034C)
#define S5K_2P1_Y_OUTPUT_SIZE           (0xD000034E)
#define S5K_2P1_FINE_INTEGRATION_TIME   (0xD0000200)
#define S5K_2P1_COARSE_INTEGRATION_TIME (0xD0000202)

static UInt32 sensorCommonRegs[][2] = {
    { 0x70001560, 0x10B5 },
    { 0x70001562, 0x00F0 },
    { 0x70001564, 0xC7F8 },
    { 0x70001566, 0x00F0 },
    { 0x70001568, 0xC9F8 },
    { 0x7000156A, 0x10BC },
    { 0x7000156C, 0x08BC },
    { 0x7000156E, 0x1847 },
    { 0x70001570, 0x2DE9 },
    { 0x70001572, 0xF041 },
    { 0x70001574, 0x9FE5 },
    { 0x70001576, 0x4451 },
    { 0x70001578, 0xD5E1 },
    { 0x7000157A, 0xB010 },
    { 0x7000157C, 0x9FE5 },
    { 0x7000157E, 0x4081 },
    { 0x70001580, 0xA0E3 },
    { 0x70001582, 0x0160 },
    { 0x70001584, 0x01E2 },
    { 0x70001586, 0x0140 },
    { 0x70001588, 0xD8E1 },
    { 0x7000158A, 0xB010 },
    { 0x7000158C, 0x9FE5 },
    { 0x7000158E, 0x3471 },
    { 0x70001590, 0xA0E1 },
    { 0x70001592, 0x1621 },
    { 0x70001594, 0x81E2 },
    { 0x70001596, 0x0110 },
    { 0x70001598, 0x82E1 },
    { 0x7000159A, 0x1611 },
    { 0x7000159C, 0xD7E1 },
    { 0x7000159E, 0xB020 },
    { 0x700015A0, 0xC2E1 },
    { 0x700015A2, 0x0110 },
    { 0x700015A4, 0xC7E1 },
    { 0x700015A6, 0xB010 },
    { 0x700015A8, 0x00EB },
    { 0x700015AA, 0x5600 },
    { 0x700015AC, 0xD8E1 },
    { 0x700015AE, 0xB000 },
    { 0x700015B0, 0xA0E1 },
    { 0x700015B2, 0x1610 },
    { 0x700015B4, 0x80E2 },
    { 0x700015B6, 0x0100 },
    { 0x700015B8, 0x81E1 },
    { 0x700015BA, 0x1600 },
    { 0x700015BC, 0xD7E1 },
    { 0x700015BE, 0xB010 },
    { 0x700015C0, 0x80E1 },
    { 0x700015C2, 0x0100 },
    { 0x700015C4, 0xC7E1 },
    { 0x700015C6, 0xB000 },
    { 0x700015C8, 0x9FE5 },
    { 0x700015CA, 0xFC00 },
    { 0x700015CC, 0xD0E1 },
    { 0x700015CE, 0xB40D },
    { 0x700015D0, 0xA0E1 },
    { 0x700015D2, 0xA010 },
    { 0x700015D4, 0x9FE5 },
    { 0x700015D6, 0xF400 },
    { 0x700015D8, 0xD0E1 },
    { 0x700015DA, 0xBA29 },
    { 0x700015DC, 0x52E1 },
    { 0x700015DE, 0x0100 },
    { 0x700015E0, 0xD081 },
    { 0x700015E2, 0xBC19 },
    { 0x700015E4, 0xC581 },
    { 0x700015E6, 0xB013 },
    { 0x700015E8, 0xD081 },
    { 0x700015EA, 0xBE19 },
    { 0x700015EC, 0x8181 },
    { 0x700015EE, 0x0410 },
    { 0x700015F0, 0xC581 },
    { 0x700015F2, 0xB010 },
    { 0x700015F4, 0xD081 },
    { 0x700015F6, 0xB809 },
    { 0x700015F8, 0x008A },
    { 0x700015FA, 0x0600 },
    { 0x700015FC, 0xD8E1 },
    { 0x700015FE, 0xB200 },
    { 0x70001600, 0xC5E1 },
    { 0x70001602, 0xB003 },
    { 0x70001604, 0xD8E1 },
    { 0x70001606, 0xB400 },
    { 0x70001608, 0xC0E3 },
    { 0x7000160A, 0x0100 },
    { 0x7000160C, 0x80E1 },
    { 0x7000160E, 0x0400 },
    { 0x70001610, 0xC5E1 },
    { 0x70001612, 0xB000 },
    { 0x70001614, 0xD8E1 },
    { 0x70001616, 0xB600 },
    { 0x70001618, 0xC5E1 },
    { 0x7000161A, 0xBE00 },
    { 0x7000161C, 0x9FE5 },
    { 0x7000161E, 0xB000 },
    { 0x70001620, 0xA0E3 },
    { 0x70001622, 0x021B },
    { 0x70001624, 0xD0E5 },
    { 0x70001626, 0x0120 },
    { 0x70001628, 0xA0E3 },
    { 0x7000162A, 0x3D0B },
    { 0x7000162C, 0x00EB },
    { 0x7000162E, 0x3700 },
    { 0x70001630, 0x9FE5 },
    { 0x70001632, 0xA000 },
    { 0x70001634, 0xD0E5 },
    { 0x70001636, 0x5F10 },
    { 0x70001638, 0x51E3 },
    { 0x7000163A, 0x0A00 },
    { 0x7000163C, 0xD005 },
    { 0x7000163E, 0x5E00 },
    { 0x70001640, 0x5003 },
    { 0x70001642, 0x0100 },
    { 0x70001644, 0x9F05 },
    { 0x70001646, 0x9000 },
    { 0x70001648, 0xD001 },
    { 0x7000164A, 0xBE0A },
    { 0x7000164C, 0xC501 },
    { 0x7000164E, 0xB603 },
    { 0x70001650, 0xBDE8 },
    { 0x70001652, 0xF041 },
    { 0x70001654, 0x2FE1 },
    { 0x70001656, 0x1EFF },
    { 0x70001658, 0x9FE5 },
    { 0x7000165A, 0x7400 },
    { 0x7000165C, 0x2DE9 },
    { 0x7000165E, 0x1040 },
    { 0x70001660, 0xA0E3 },
    { 0x70001662, 0x0310 },
    { 0x70001664, 0xC0E1 },
    { 0x70001666, 0xB616 },
    { 0x70001668, 0x9FE5 },
    { 0x7000166A, 0x7010 },
    { 0x7000166C, 0x9FE5 },
    { 0x7000166E, 0x7820 },
    { 0x70001670, 0x80E5 },
    { 0x70001672, 0x7810 },
    { 0x70001674, 0x9FE5 },
    { 0x70001676, 0x6810 },
    { 0x70001678, 0x9FE5 },
    { 0x7000167A, 0x6800 },
    { 0x7000167C, 0x80E5 },
    { 0x7000167E, 0x5010 },
    { 0x70001680, 0x42E0 },
    { 0x70001682, 0x0110 },
    { 0x70001684, 0xC0E1 },
    { 0x70001686, 0xB415 },
    { 0x70001688, 0x9FE5 },
    { 0x7000168A, 0x6000 },
    { 0x7000168C, 0x4FE2 },
    { 0x7000168E, 0x491F },
    { 0x70001690, 0x00EB },
    { 0x70001692, 0x2000 },
    { 0x70001694, 0x9FE5 },
    { 0x70001696, 0x2810 },
    { 0x70001698, 0xC1E1 },
    { 0x7000169A, 0xB000 },
    { 0x7000169C, 0x9FE5 },
    { 0x7000169E, 0x1C00 },
    { 0x700016A0, 0xD0E1 },
    { 0x700016A2, 0xB023 },
    { 0x700016A4, 0xC1E1 },
    { 0x700016A6, 0xB220 },
    { 0x700016A8, 0xD0E1 },
    { 0x700016AA, 0xB020 },
    { 0x700016AC, 0xC1E1 },
    { 0x700016AE, 0xB420 },
    { 0x700016B0, 0xD0E1 },
    { 0x700016B2, 0xBE00 },
    { 0x700016B4, 0xC1E1 },
    { 0x700016B6, 0xB600 },
    { 0x700016B8, 0xBDE8 },
    { 0x700016BA, 0x1040 },
    { 0x700016BC, 0x2FE1 },
    { 0x700016BE, 0x1EFF },
    { 0x700016C0, 0x00D0 },
    { 0x700016C2, 0x00F4 },
    { 0x700016C4, 0x0070 },
    { 0x700016C6, 0x3417 },
    { 0x700016C8, 0x00D0 },
    { 0x700016CA, 0x0061 },
    { 0x700016CC, 0x0070 },
    { 0x700016CE, 0xD011 },
    { 0x700016D0, 0x0070 },
    { 0x700016D2, 0x0000 },
    { 0x700016D4, 0x0070 },
    { 0x700016D6, 0x5014 },
    { 0x700016D8, 0x0070 },
    { 0x700016DA, 0xD810 },
    { 0x700016DC, 0x0070 },
    { 0x700016DE, 0x000B },
    { 0x700016E0, 0x0070 },
    { 0x700016E2, 0x2017 },
    { 0x700016E4, 0x0070 },
    { 0x700016E6, 0x3C17 },
    { 0x700016E8, 0x0070 },
    { 0x700016EA, 0x7005 },
    { 0x700016EC, 0x0070 },
    { 0x700016EE, 0x9426 },
    { 0x700016F0, 0x0000 },
    { 0x700016F2, 0x4021 },
    { 0x700016F4, 0x7847 },
    { 0x700016F6, 0xC046 },
    { 0x700016F8, 0xFFEA },
    { 0x700016FA, 0xD6FF },
    { 0x700016FC, 0x7847 },
    { 0x700016FE, 0xC046 },
    { 0x70001700, 0x1FE5 },
    { 0x70001702, 0x04F0 },
    { 0x70001704, 0x0000 },
    { 0x70001706, 0x80EE },
    { 0x70001708, 0x1FE5 },
    { 0x7000170A, 0x04F0 },
    { 0x7000170C, 0x0000 },
    { 0x7000170E, 0x4021 },
    { 0x70001710, 0x1FE5 },
    { 0x70001712, 0x04F0 },
    { 0x70001714, 0x0000 },
    { 0x70001716, 0x1868 },
    { 0x70001718, 0x1FE5 },
    { 0x7000171A, 0x04F0 },
    { 0x7000171C, 0x0000 },
    { 0x7000171E, 0x14EE },
    { 0x70001720, 0x0C00 },
    { 0x70001722, 0x0003 },
    { 0x70001724, 0x000C },
    { 0x70001726, 0x030A },
    { 0x70001728, 0x0A00 },
    { 0x7000172A, 0x0901 },
    { 0x7000172C, 0x0003 },
    { 0x7000172E, 0x000A },
    { 0x70001730, 0x0000 },
    { 0x70001732, 0x0000 },

    { 0xD0003168, 0x0040 },
    { 0xD000012A, 0x0100 },
    { 0xD000012E, 0x0B1C },
    { 0xD0000128, 0x0158 },
    { 0xD000012C, 0x7987 },
    { 0xD000374E, 0x0008 },
    { 0xD00032AA, 0x0005 },
    { 0xD000354A, 0x0005 },
    { 0xD0003216, 0x0252 },
    { 0xD0003218, 0x0222 },
    { 0xD0003242, 0x0108 },
    { 0xD0003244, 0x0150 },
    { 0xD0003246, 0x0220 },
    { 0xD0003248, 0x0630 },
    { 0xD0003252, 0x0078 },
    { 0xD0003254, 0x000A },
    { 0xD000325A, 0x0050 },
    { 0xD000325C, 0x0000 },
    { 0xD00031FE, 0x00C9 },
    { 0xD00031AC, 0x000B },
    { 0xD00031AE, 0x000B },
    { 0xD00031B0, 0x000A },
    { 0xD00031B2, 0x0007 },
    { 0xD00031B6, 0x0DD6 },
    { 0xD00031B8, 0x0C50 },
    { 0xD00031BA, 0x0B0B },
    { 0xD0003268, 0x0111 },
    { 0xD0003270, 0x0249 },
    { 0xD0003278, 0x0113 },
    { 0xD0003280, 0x0258 },
    { 0xD0003288, 0x0113 },
    { 0xD0003290, 0x0249 },
    { 0xD0003294, 0x00CD },
    { 0xD0003298, 0x00F1 },
    { 0xD000329C, 0x00CB },
    { 0xD00032A0, 0x00F3 },
    { 0xD00032A8, 0x0008 },
    { 0xD00032AC, 0x00F1 },
    { 0xD00032B0, 0x00FF },
    { 0xD00032C0, 0x00CD },
    { 0xD00032C4, 0x024B },
    { 0xD00032C8, 0x00CB },
    { 0xD00032CC, 0x024D },
    { 0xD00032D8, 0x0100 },
    { 0xD00032DC, 0x010D },
    { 0xD00032E0, 0x00CB },
    { 0xD00032E4, 0x0100 },
    { 0xD00032F4, 0x0249 },
    { 0xD0003304, 0x0249 },
    { 0xD0003314, 0x00CB },
    { 0xD000331C, 0x00CB },
    { 0xD0003320, 0x0181 },
    { 0xD0003324, 0x0249 },
    { 0xD0003328, 0x00CD },
    { 0xD000332C, 0x00E5 },
    { 0xD0003330, 0x00D5 },
    { 0xD0003334, 0x00ED },
    { 0xD0003338, 0x00DD },
    { 0xD000333C, 0x00ED },
    { 0xD0003340, 0x00CD },
    { 0xD0003344, 0x00CF },
    { 0xD0003350, 0x00D5 },
    { 0xD0003354, 0x00ED },
    { 0xD0003360, 0x00CD },
    { 0xD0003364, 0x00CF },
    { 0xD0003370, 0x00CD },
    { 0xD0003374, 0x00CF },
    { 0xD0003388, 0x00CD },
    { 0xD000338C, 0x00E5 },
    { 0xD00033B8, 0x00D5 },
    { 0xD00033BC, 0x00ED },
    { 0xD00033E0, 0x00DD },
    { 0xD00033E4, 0x00ED },
    { 0xD0003408, 0x00CD },
    { 0xD000340C, 0x00CF },
    { 0xD0003438, 0x00D5 },
    { 0xD000343C, 0x00ED },
    { 0xD0003468, 0x00CD },
    { 0xD000346C, 0x00E9 },
    { 0xD00034A4, 0x00CC },
    { 0xD00034A8, 0x0180 },
    { 0xD00034AC, 0x024A },
    { 0xD00034BC, 0x024F },
    { 0xD00034C8, 0x00CD },
    { 0xD00034CC, 0x00D0 },
    { 0xD00034D0, 0x024B },
    { 0xD00034D4, 0x024E },
    { 0xD00034D8, 0x00CD },
    { 0xD0003534, 0x024A },
    { 0xD0003548, 0x0008 },
    { 0xD000354C, 0x00F1 },
    { 0xD0003550, 0x00F9 },
    { 0xD000326A, 0x00F9 },
    { 0xD0003272, 0x0219 },
    { 0xD000327A, 0x00FB },
    { 0xD0003282, 0x0228 },
    { 0xD000328A, 0x00FB },
    { 0xD0003292, 0x0219 },
    { 0xD0003296, 0x00B5 },
    { 0xD000329A, 0x00D9 },
    { 0xD000329E, 0x00B3 },
    { 0xD00032A2, 0x00DB },
    { 0xD00032AE, 0x00D9 },
    { 0xD00032B2, 0x00E7 },
    { 0xD00032BA, 0x0052 },
    { 0xD00032BE, 0x005F },
    { 0xD00032C2, 0x00B5 },
    { 0xD00032C6, 0x021B },
    { 0xD00032CA, 0x00B3 },
    { 0xD00032CE, 0x021D },
    { 0xD00032D6, 0x005E },
    { 0xD00032DA, 0x00E8 },
    { 0xD00032DE, 0x00F5 },
    { 0xD00032E2, 0x00B3 },
    { 0xD00032E6, 0x00E8 },
    { 0xD00032EE, 0x0052 },
    { 0xD00032F2, 0x0052 },
    { 0xD00032F6, 0x0219 },
    { 0xD00032FE, 0x005D },
    { 0xD0003302, 0x005D },
    { 0xD0003306, 0x0219 },
    { 0xD000330E, 0x005D },
    { 0xD0003316, 0x00B3 },
    { 0xD000331A, 0x006F },
    { 0xD000331E, 0x00B3 },
    { 0xD0003322, 0x0151 },
    { 0xD0003326, 0x0219 },
    { 0xD000332A, 0x00B5 },
    { 0xD000332E, 0x00CD },
    { 0xD0003332, 0x00BD },
    { 0xD0003336, 0x00D5 },
    { 0xD000333A, 0x00C5 },
    { 0xD000333E, 0x00D5 },
    { 0xD0003342, 0x00B5 },
    { 0xD0003346, 0x00B7 },
    { 0xD0003352, 0x00BD },
    { 0xD0003356, 0x00D5 },
    { 0xD0003362, 0x00B5 },
    { 0xD0003366, 0x00B7 },
    { 0xD0003372, 0x00B5 },
    { 0xD0003376, 0x00B7 },
    { 0xD000338A, 0x00B5 },
    { 0xD000338E, 0x00CD },
    { 0xD00033BA, 0x00BD },
    { 0xD00033BE, 0x00D5 },
    { 0xD00033E2, 0x00C5 },
    { 0xD00033E6, 0x00D5 },
    { 0xD000340A, 0x00B5 },
    { 0xD000340E, 0x00B7 },
    { 0xD000343A, 0x00BD },
    { 0xD000343E, 0x00D5 },
    { 0xD000346A, 0x00B5 },
    { 0xD000346E, 0x00D1 },
    { 0xD00034A2, 0x006E },
    { 0xD00034A6, 0x00B4 },
    { 0xD00034AA, 0x0150 },
    { 0xD00034AE, 0x021A },
    { 0xD00034BA, 0x005E },
    { 0xD00034BE, 0x021F },
    { 0xD00034C2, 0x005F },
    { 0xD00034C6, 0x0061 },
    { 0xD00034CA, 0x00B5 },
    { 0xD00034CE, 0x00B8 },
    { 0xD00034D2, 0x021B },
    { 0xD00034D6, 0x021E },
    { 0xD00034DA, 0x00B5 },
    { 0xD0003536, 0x021A },
    { 0xD000354E, 0x00D9 },
    { 0xD0003552, 0x00E1 },
    { 0xD0006218, 0xF1D0 },
    { 0xD0006214, 0xF9F0 },
    { 0xD0006226, 0x0001 },
    { 0xD000F400, 0x0B1C },
    { 0xD000F40E, 0x0158 },
    { 0xD000F430, 0x7987 },
    { 0xD000962A, 0x0000 },
    { 0xD000F43C, 0x0048 },
    { 0xD0006226, 0x0000 },
    { 0xD0006218, 0xF9F0 },
    { 0xD00031DA, 0x0000 },
    { 0xD00031DC, 0x0000 },
    { 0xD00031FE, 0x00E9 },
    { 0xD0003200, 0x3400 },
    { 0xD00031C2, 0x001E },
    { 0xD00031C4, 0x0028 },
    { 0xD00031C6, 0x0002 },
    { 0xD00031C8, 0x0002 },
    { 0xD00031CA, 0x0014 },
    { 0xD0006218, 0xF1D0 },/* HW Registers: 0xD0006000 - 0xD000FFFF */
    { 0xD0006214, 0xF9F0 },
    { 0xD0006226, 0x0001 },
    { 0xD000F442, 0x0026 },
    { 0xD000F446, 0x005B },
    { 0xD000F448, 0x0033 },
    { 0xD000F44A, 0x0022 },
    { 0xD000F44E, 0x0024 },
    { 0xD0006226, 0x0000 },
    { 0xD0006218, 0xF9F0 }, /* HW Registers: 0xD0006000 - 0xD000FFFF */
    { 0xD00031DA, 0x0000 },
    { 0xD00031DC, 0x0000 },
    { 0xD0003222, 0x0300 },
    { 0xD0000110, 0x0002 }, /* channel_identifier = 2 -> CSI-2 */
    { 0xD0003040, 0x0000 },
    { 0xD0003042, 0x0000 },
    { 0xD0003046, 0x0000 },
    { 0xD0000114, 0x0300 }, /* lane_mode = 3 -> 4 lanes */
    { 0xD0003000, 0x0000 },
    { 0xD0000136, 0x1400 }, /* extclk_frequency_mhz = 26 MHz */
    { 0xD0000304, 0x0005 }, /* pre_pll_clk_div */
    { 0xD0000302, 0x0001 }, /* vt_sys_clk_div */
    { 0xD0000300, 0x0002 }, /* vt_pix_clk_div */
    { 0xD000030A, 0x0002 }, /* op_sys_clk_div */
    { 0xD0000308, 0x0008 }, /* op_pix_clk_div    */
    { 0xD0000112, 0x0A0A }, /* data_format = Top 10 bit pixel data RAW 10 */
    { 0xD0003010, 0x0000 },
    { 0xD0000902, 0x0100 },
    { 0xD0000400, 0x0002 },
    { 0xD0003018, 0x0311 },
    { 0xD0003012, 0x0300 },
    { 0xD00031A0, 0x0000 },
    { 0xD000311E, 0x0101 },
    { 0xD0000600, 0x0000 },
    { 0xD0000120, 0x0000 }, /* gain_mode */
    { 0xD0000204, 0x0080 }, /* Analog Gain Control Register */
    { 0xD000020E, 0x0100 },
    { 0xD0000210, 0x0100 },
    { 0xD0000212, 0x0100 },
    { 0xD0000214, 0x0100 },
    { 0xD00037B4, 0x0140 },
    { 0xD00037B6, 0x0140 },
    { 0xD00037B8, 0x0140 },
    { 0xD00037BA, 0x0140 },
    { 0xD00037BC, 0x0000 },
    { 0xD00037BE, 0x0000 },
    { 0xD00037C0, 0x0000 },
    { 0xD00037C2, 0x0000 },
    { 0xD0000104, 0x0001 }
};

/**
  * Resolution     : 4656x3492 (16MP)
  * Mode           : Still Picture
  * Frame Rate     : 10
  * FOV            : Wide
  * Aspect Ratio   : 4:3
  * FVID2 Standard : FVID2_STD_4656_3492_10
 */
static UInt32 sensorConfig_16MP_10_still[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x3000 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0F3D },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0008 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0006 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x1237 },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0DA9 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x1230 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0DA0 }     /* y_output_size */
};

/**
  * Resolution     : 3264x2448 (8MP)
  * Mode           : Still Picture
  * Frame Rate     : 15
  * FOV            : Wide
  * Aspect Ratio   : 4:3
  * FVID2 Standard : FVID2_STD_8MP_4656_3492_15
 */
static UInt32 sensorConfig_8MP_15_still[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x23D8 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0D99 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0008 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0006 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x1237 },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0DA9 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0016 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0D3A },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x09EB }     /* y_output_size */
};

/**
  * Resolution     : 4656x3492 (16MP)
  * Mode           : Burst
  * Frame Rate     : 5
  * FOV            : Wide
  * Aspect Ratio   : 4:3
  * FVID2 Standard : FVID2_STD_4656_3492_5
 */
static UInt32 sensorConfig_16MP_5_burst[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x3000 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x1E7A },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0008 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0006 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x1237 },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0DA9 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x1230 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0DA0 }     /* y_output_size */
};

/**
  * Resolution     : 3264x2448 (8MP)
  * Mode           : Burst
  * Frame Rate     : 5
  * FOV            : Wide
  * Aspect Ratio   : 4:3
  * FVID2 Standard : FVID2_STD_8MP_4656_3492_5
 */
static UInt32 sensorConfig_8MP_5_burst[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x23D8 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x27FC },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0008 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0006 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x1237 },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0DA9 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0016 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0D3A },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x09EB }     /* y_output_size */
};

/**
  * Resolution     : 3840x2160 (4K)
  * Mode           : Video
  * Frame Rate     : 15
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_4K_4608_2592_15
 */
static UInt32 sensorConfig_4K_15_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x2912 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0BE1 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0F28 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0886 }     /* y_output_size */
};

/**
  * Resolution     : 3840x2160 (4K)
  * Mode           : Video
  * Frame Rate     : 12.5
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_4K_4608_2592_12_5
 */
static UInt32 sensorConfig_4K_12_5_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x3490 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0B22 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0F28 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0886 }     /* y_output_size */
};

/**
  * Resolution     : 3840x2160 (4K)
  * Mode           : Video
  * Frame Rate     : 12.5
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_4K_4608_2592_14
 */
static UInt32 sensorConfig_4K_14_wide_timelapse[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x31AB },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0A85 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0F28 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0886 },    /* y_output_size */
};

/**
  * Resolution     : 2704x1520 (2.7K)
  * Mode           : Video
  * Frame Rate     : 30
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_2_7K_4608_2592_30
 */
static UInt32 sensorConfig_2_7K_30_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x17B2 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0A4A },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x001B },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0AAA },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0600 }     /* y_output_size */
};

/**
  * Resolution     : 2704x1520 (2.7K)
  * Mode           : Video
  * Frame Rate     : 25
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_2_7K_4608_2592_25
 */
static UInt32 sensorConfig_2_7K_25_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A45 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0B23 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x001B },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0AAA },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0600 }     /* y_output_size */
};

/**
  * Resolution     : 2704x1520 (2.7K)
  * Mode           : Video
  * Frame Rate     : 25
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_2_7K_3840_2160_25
 */
static UInt32 sensorConfig_2_7K_25_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x226C },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0880 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0016 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0AE8 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0622 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 60
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_4608_2592_60
 */
static UInt32 sensorConfig_1080P_60_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1489 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x05F0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0794 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0443 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 60
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_3840_2160_60
 */
static UInt32 sensorConfig_1080P_60_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1453 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0600 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 50
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_4608_2592_50
 */
static UInt32 sensorConfig_1080P_50_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A81 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0585 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0794 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0443 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 50
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_3840_2160_50
 */
static UInt32 sensorConfig_1080P_50_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A81 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0585 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 30
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_4608_2592_30
 */
static UInt32 sensorConfig_1080P_30_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1E14 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x081B },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0794 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0443 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 30
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_3840_2160_30
 */
static UInt32 sensorConfig_1080P_30_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1453 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0C00 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 25
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_4608_2592_25
 */
static UInt32 sensorConfig_1080P_25_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x2573 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0794 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0443 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Video
  * Frame Rate     : 25
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_3840_2160_25
 */
static UInt32 sensorConfig_1080P_25_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x2573 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 120
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_4608_2592_120
 */
/**
  * FRAME_LENGTH_LINES value set below is based on observations.
  * According to formula, to yield an fps of 120 FRAME_LENGTH_LINES
  * needs to be 0x395. But Observed fps with this value was 120.12.
  * 0.12 extra fps would result in an extra frame every 8 seconds.
  * 0x396 FRAME_LENGTH_LINES will result in fps of 119.85 according
  * to the formula, but its observed fps is 119.999
  *
  * For 720P modes, setting line_length_pck = 2.71 * x_output_size
  * results in a drop in expected frame rate.
  * The value below is experimentally found to be the safe value
  * where frame rate is as expected
 */
static UInt32 sensorConfig_720P_120_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1105 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0396 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x050C },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D7 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 120
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_3840_2160_120
 */
static UInt32 sensorConfig_720P_120_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1105 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0396 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0500 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D0 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 100
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_4608_2592_100
 */
static UInt32 sensorConfig_720P_100_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1294 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x03F0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x050C },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D7 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 100
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_3840_2160_100
 */
static UInt32 sensorConfig_720P_100_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1294 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x03F0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0500 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D0 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 60
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_4608_2592_60
 */
static UInt32 sensorConfig_720P_60_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1105 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x072C },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x050C },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D7 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 60
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_3840_2160_60
 */
static UInt32 sensorConfig_720P_60_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1910 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x04DE },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0500 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D0 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 50
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_4608_2592_50
 */
static UInt32 sensorConfig_720P_50_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A56 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x058E },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0013 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x050C },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D7 }     /* y_output_size */
};

/**
  * Resolution     : 1280x720 (720P)
  * Mode           : Video
  * Frame Rate     : 50
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_720P_3840_2160_50
 */
static UInt32 sensorConfig_720P_50_normal_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x21F8 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x044E },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0005 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0005 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0133 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0500 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x02D0 }     /* y_output_size */
};

/**
  * Resolution     : 848x480 (WVGA)
  * Mode           : Video
  * Frame Rate     : 180
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_WVGA_4608_2592_180
 */

/**
  * For WVGA, setting line_length_pck = 2.71 * x_output_size
  * results in a drop in expected frame rate.
  * The value below is experimentally found to be the safe value
  * where frame rate is as expected
  */

static UInt32 sensorConfig_WVGA_180_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x0EC7 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x02C1 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0007 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0007 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0144 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0015 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x036C },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x01ED }     /* y_output_size */
};

/**
  * Resolution     : 848x480 (WVGA)
  * Mode           : Video
  * Frame Rate     : 150
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_WVGA_4608_2592_150
 */
static UInt32 sensorConfig_WVGA_150_wide_video[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x102C },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x0304 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0007 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0007 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0144 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0015 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x036C },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x01ED }     /* y_output_size */
};

/**
  * Resolution     : 848x480 (WVGA)
  * Mode           : Viewfinder
  * Frame Rate     : 30
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_WVGA_4608_2592_30
 */
static UInt32 sensorConfig_WVGA_30_wide_viewfinder[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x2428 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x06BE },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0007 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0007 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0144 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0018 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0300 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x01B0 }     /* y_output_size */
};

/**
  * Resolution     : 848x480 (WVGA)
  * Mode           : Viewfinder
  * Frame Rate     : 30
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_WVGA_3840_2160_30
 */
static UInt32 sensorConfig_WVGA_30_normal_viewfinder[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x242B },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x06BE },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x01A0 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x02A0 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x109F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0B0F },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0007 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0007 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0144 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0014 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0300 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x01B0 }     /* y_output_size */
};

/**
  * Resolution     : 848x480 (WVGA)
  * Mode           : Stabilization
  * Frame Rate     : 50
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_WVGA_4608_2592_50
 */
static UInt32 sensorConfig_WVGA_50_wide_stabilization[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A56 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x059A },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0007 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0007 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0144 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0012 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0400 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0240 }     /* y_output_size */
};

/**
  * Resolution     : 848x480 (WVGA)
  * Mode           : Stabilization
  * Frame Rate     : 5
  * FOV            : Wide
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_WVGA_4608_2592_5
 */
static UInt32 sensorConfig_WVGA_5_wide_stabilization[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A56 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x3798 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00F0 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0020 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x01C8 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x121F },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0BE7 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0007 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0007 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0144 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0012 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0400 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0240 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Factory
  * Frame Rate     : 30
  * FOV            : Center
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_1920_1080_C_30
 */
static UInt32 sensorConfig_1080P_30_C_factory[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A08 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00C8 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0560 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x04BC },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x0CDF },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x08F3 },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Factory
  * Frame Rate     : 30
  * FOV            : RB
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_1920_1080_RB_30
 */
static UInt32 sensorConfig_1080P_30_RB_factory[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A08 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00C8 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0004 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0004 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x0783 },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x043B },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Factory
  * Frame Rate     : 30
  * FOV            : LB
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_1920_1080_LB_30
 */
static UInt32 sensorConfig_1080P_30_LB_factory[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A08 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00C8 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0ABC },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0004 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x123B },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x043B },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Factory
  * Frame Rate     : 30
  * FOV            : RT
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_1920_1080_RT_30
 */
static UInt32 sensorConfig_1080P_30_RT_factory[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A08 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00C8 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0004 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0974 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x0783 },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0DAB },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1920x1080 (1080P)
  * Mode           : Factory
  * Frame Rate     : 30
  * FOV            : LT
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1080P_1920_1080_LT_30
 */
static UInt32 sensorConfig_1080P_30_LT_factory[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A08 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00C8 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0ABC },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0974 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x123B },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0DAB },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0001 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0001 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0111 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x0780 },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x0438 }     /* y_output_size */
};

/**
  * Resolution     : 1440P
  * Mode           : Factory
  * Frame Rate     : 30
  * FOV            : Normal
  * Aspect Ratio   : 16:9
  * FVID2 Standard : FVID2_STD_1440_1080_2332_1748_30
 */
static UInt32 sensorConfig_1440P_30_factory[][2] = {
    { S5K_2P1_LINE_LENGTH_PCK,         0x1A08 },    /* line_length_pck = (x_output_size * 2.7) */
    { S5K_2P1_FRAME_LENGTH_LINES,      0x07D0 },    /* frame_length_lines */
    { S5K_2P1_PLL_MULTIPLIER,          0x00C8 },    /* pll_multiplier */
    { S5K_2P1_X_ADDR_START,            0x0004 },    /* x_start */
    { S5K_2P1_Y_ADDR_START,            0x0004 },    /* y_start */
    { S5K_2P1_X_ADDR_END,              0x123B },    /* x_end */
    { S5K_2P1_Y_ADDR_END,              0x0DAB },    /* Y-end */
    { S5K_2P1_X_EVEN_INC,              0x0001 },    /* x_even_inc */
    { S5K_2P1_X_ODD_INC,               0x0003 },    /* x_odd_inc */
    { S5K_2P1_Y_EVEN_INC,              0x0001 },    /* y_even_inc */
    { S5K_2P1_Y_ODD_INC,               0x0003 },    /* y_odd_inc */
    { S5K_2P1_BINNING_MODE,            0x0122 },    /* binning_mode */
    { S5K_2P1_SCALE_M,                 0x0010 },    /* scale_factor = scale_n/scale_m = 16/16 = 1 */
    { S5K_2P1_X_OUTPUT_SIZE,           0x091C },    /* x_output_size */
    { S5K_2P1_Y_OUTPUT_SIZE,           0x06D4 }     /* y_output_size */
};

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif

