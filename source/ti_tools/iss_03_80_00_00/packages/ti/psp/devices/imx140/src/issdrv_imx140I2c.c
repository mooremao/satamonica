/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <ti/psp/devices/imx140/src/issdrv_imx140Priv.h>

/*
  write to device registers
*/
Int32 Iss_Imx140RegWrite ( Iss_Imx140Obj * pObj,
                            Iss_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 1;
    //Iss_SensorCreateParams *pCreateArgs;
    return status;
}

/*
  read from device registers
*/
Int32 Iss_Imx140RegRead ( Iss_Imx140Obj * pObj,
                           Iss_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 1;
    //Iss_SensorCreateParams *pCreateArgs;
    return status;
}
