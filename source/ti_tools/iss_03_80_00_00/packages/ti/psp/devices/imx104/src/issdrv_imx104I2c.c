/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <ti/psp/devices/imx104/src/issdrv_imx104Priv.h>

/*
  write to device registers
*/
Int32 Iss_Imx104RegWrite ( Iss_Imx104Obj * pObj,
                            Iss_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 0;
    //Iss_SensorCreateParams *pCreateArgs;


    return status;
}

/*
  read from device registers
*/
Int32 Iss_Imx104RegRead ( Iss_Imx104Obj * pObj,
                           Iss_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 0;
    //Iss_SensorCreateParams *pCreateArgs;


    return status;
}
