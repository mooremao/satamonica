/*
 * Support functions for OMAP GPIO
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Akshaya Bhat <akshaya.bhat@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * This work is derived from the linux 2.6.27 kernel source
 * To fetch, use the kernel repository
 * git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux-2.6.git
 * Use the v2.6.27 tag.
 *
 *
 */
#include <common.h>
#include <asm/arch/hardware.h>
#include <asm/arch/gpio.h>
#include <asm/io.h>
#include <asm/errno.h>

static struct gpio_bank gpio_bank_ti81xx[4] = {
	{ (void *)GPIO0_BASE},
	{ (void *)GPIO1_BASE},
	{ (void *)GPIO2_BASE},
	{ (void *)GPIO3_BASE},
};

static struct gpio_bank *gpio_bank = &gpio_bank_ti81xx[0];

static inline struct gpio_bank *get_gpio_bank(int gpio)
{
	return &gpio_bank[gpio >> 5];
}

static inline int get_gpio_index(int gpio)
{
	return gpio & 0x1f;
}

static inline int gpio_valid(int gpio)
{
	if (gpio < 0)
		return -1;
	if (gpio < 128)
		return 0;
	return -1;
}

static int check_gpio(int gpio)
{
	if (gpio_valid(gpio) < 0) {
		printf("ERROR : check_gpio: invalid GPIO %d\n", gpio);
		return -1;
	}
	return 0;
}

static void _set_gpio_direction(struct gpio_bank *bank, int gpio, int is_input)
{
	void *reg = bank->base;
	u32 l;

	gpio = gpio % 32;
	reg += TI81XX_GPIO_OE;
	l = __raw_readl(reg);
	if (is_input)
		l |= 1 << gpio;
	else
		l &= ~(1 << gpio);
	__raw_writel(l, reg);
}

void ti81xx_set_gpio_direction(int gpio, int is_input)
{
	struct gpio_bank *bank;

	if (check_gpio(gpio) < 0)
		return;
	bank = get_gpio_bank(gpio);
	_set_gpio_direction(bank, get_gpio_index(gpio), is_input);
}

static void _set_gpio_dataout(struct gpio_bank *bank, int gpio, int enable)
{
	void *reg = bank->base;
	u32 l = 0;
	gpio = gpio % 32;

	if (enable)
		reg += TI81XX_GPIO_SETDATAOUT;
	else
		reg += TI81XX_GPIO_CLEARDATAOUT;
	l = 1 << gpio;
	__raw_writel(l, reg);
}

void ti81xx_set_gpio_dataout(int gpio, int enable)
{
	struct gpio_bank *bank;

	if (check_gpio(gpio) < 0)
		return;
	bank = get_gpio_bank(gpio);
	_set_gpio_dataout(bank, get_gpio_index(gpio), enable);
}

int ti81xx_get_gpio_datain(int gpio)
{
	struct gpio_bank *bank;
	void *reg;

	if (check_gpio(gpio) < 0)
		return -EINVAL;
	bank = get_gpio_bank(gpio);
	reg = bank->base;
	reg += TI81XX_GPIO_DATAIN;
	gpio = gpio % 32;
	return (__raw_readl(reg) & (1 << get_gpio_index(gpio))) != 0;
}

static void _reset_gpio(struct gpio_bank *bank, int gpio)
{
	_set_gpio_direction(bank, get_gpio_index(gpio), 1);
}

int ti81xx_request_gpio(int gpio)
{
	if (check_gpio(gpio) < 0)
		return -EINVAL;

	return 0;
}

void ti81xx_gpio_set_debounce(int gpio, int debounce)
{
	struct gpio_bank *bank;
	void *reg;
	int l, val;

	if (check_gpio(gpio) < 0)
		return -EINVAL;

	bank = get_gpio_bank(gpio);
	gpio = gpio % 32;
	l = 1 << get_gpio_index(gpio);

	reg = bank->base;
	reg += TI81XX_GPIO_DEBOUNCE_VAL;

	if (debounce < 32)
		debounce = 0x01;
	else if (debounce > 7936)
		debounce = 0xff;
	else
		debounce = (debounce / 0x1f) - 1;

	__raw_writel(debounce, reg);

	reg = bank->base;
	reg += TI81XX_GPIO_DEBOUNCE_EN;

	val = __raw_readl(reg);
	if (debounce)
		val |= l;
	else
		val &= ~l;
	__raw_writel(val, reg);
}
void ti81xx_free_gpio(int gpio)
{
	struct gpio_bank *bank;

	if (check_gpio(gpio) < 0)
		return;
	bank = get_gpio_bank(gpio);

	_reset_gpio(bank, gpio);
}
