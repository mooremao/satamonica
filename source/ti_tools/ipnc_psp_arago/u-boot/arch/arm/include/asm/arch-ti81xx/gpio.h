/*
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Akshaya Bhat <akshaya.bhat@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * This work is derived from the linux 2.6.27 kernel source
 * To fetch, use the kernel repository
 * git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux-2.6.git
 * Use the v2.6.27 tag.
 *
 */
#ifndef _TI81XX_GPIO_H
#define _TI81XX_GPIO_H

#define TI81XX_GPIO_REVISION		0x0000
#define TI81XX_GPIO_SYSCONFIG		0x0010
#define TI81XX_GPIO_EOI				0x0020
#define TI81XX_GPIO_CTRL			0x0130
#define TI81XX_GPIO_OE				0x0134
#define TI81XX_GPIO_DATAIN			0x0138
#define TI81XX_GPIO_DATAOUT			0x013c
#define TI81XX_GPIO_LEVELDETECT0	0x0140
#define TI81XX_GPIO_LEVELDETECT1	0x0144
#define TI81XX_GPIO_RISINGDETECT	0x0148
#define TI81XX_GPIO_FALLINGDETECT	0x014c
#define TI81XX_GPIO_DEBOUNCE_EN		0x0150
#define TI81XX_GPIO_DEBOUNCE_VAL	0x0154
#define TI81XX_GPIO_CLEARDATAOUT	0x0190
#define TI81XX_GPIO_SETDATAOUT		0x0194

struct gpio_bank {
	void *base;
};


/* This is the interface */

/* Request a gpio before using it */
int ti81xx_request_gpio(int gpio);
/* Reset and free a gpio after using it */
void ti81xx_free_gpio(int gpio);
/* Sets the gpio as input or output */
void ti81xx_set_gpio_direction(int gpio, int is_input);
/* Set or clear a gpio output */
void ti81xx_set_gpio_dataout(int gpio, int enable);
/* Get the value of a gpio input */
int ti81xx_get_gpio_datain(int gpio);

#endif /* _TI81XX_GPIO_H_ */
