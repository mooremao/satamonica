/*
 * Commands to load factory data (FDT) partition into memory etc.
 *
 * Copyright (C) 2010 TomTom International B.V.
 * 
 ************************************************************************
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
 * 02110-1301, USA.
 ************************************************************************
 */

/* #define DEBUG */

#include <common.h>
#include <dsasig.h>
#include <factorydata.h>

#ifdef CONFIG_CMD_FD

DECLARE_GLOBAL_DATA_PTR;

#include <command.h>
#include <libfdt.h>

#if (defined(CONFIG_CMD_IDE) || \
     defined(CONFIG_CMD_MG_DISK) || \
     defined(CONFIG_CMD_SATA) || \
     defined(CONFIG_CMD_SCSI) || \
     defined(CONFIG_CMD_USB) || \
     defined(CONFIG_MMC)		|| \
     defined(CONFIG_SYSTEMACE) )
#include <part.h>
#else
#endif

#if (defined(CONFIG_CMD_NAND) && defined(CONFIG_CMD_FASTBOOT))
extern int do_nand_fdload (unsigned int part, void *dst, unsigned int *nbytes);
#endif

#define FD_MAXSECTORS 128*1024/512

#if (defined(CONFIG_CMD_IDE) || \
     defined(CONFIG_CMD_MG_DISK) || \
     defined(CONFIG_CMD_SATA) || \
     defined(CONFIG_CMD_SCSI) || \
     defined(CONFIG_CMD_USB) || \
     defined(CONFIG_MMC)		|| \
     defined(CONFIG_SYSTEMACE) )
#else
int get_partition_info (block_dev_desc_t *dev_desc, int part
					, disk_partition_t *info)
{
	switch (dev_desc->part_type) {
#ifdef CONFIG_MAC_PARTITION
	case PART_TYPE_MAC:
		if (get_partition_info_mac(dev_desc,part,info) == 0) {
			PRINTF ("## Valid MAC partition found ##\n");
			return (0);
		}
		break;
#endif

#ifdef CONFIG_DOS_PARTITION
	case PART_TYPE_DOS:
		if (get_partition_info_dos(dev_desc,part,info) == 0) {
			PRINTF ("## Valid DOS partition found ##\n");
			return (0);
		}
		break;
#endif

#ifdef CONFIG_ISO_PARTITION
	case PART_TYPE_ISO:
		if (get_partition_info_iso(dev_desc,part,info) == 0) {
			PRINTF ("## Valid ISO boot partition found ##\n");
			return (0);
		}
		break;
#endif

#ifdef CONFIG_AMIGA_PARTITION
	case PART_TYPE_AMIGA:
	    if (get_partition_info_amiga(dev_desc, part, info) == 0)
	    {
		PRINTF ("## Valid Amiga partition found ##\n");
		return (0);
	    }
	    break;
#endif

#ifdef CONFIG_EFI_PARTITION
	case PART_TYPE_EFI:
		if (get_partition_info_efi(dev_desc,part,info) == 0) {
			PRINTF ("## Valid EFI partition found ##\n");
			return (0);
		}
		break;
#endif
	default:
		break;
	}
	return (-1);
}
#endif

static int do_blkdev_fdload (cmd_tbl_t *cmdtp, int argc, char *argv[], ulong *nbytes)
{
	char *ep;
	int dev, part = 1;
	ulong blksz, start, readlen;
	disk_partition_t info;
	block_dev_desc_t *dev_desc = NULL;
	char *addr_str;

	if (argc == 4) {
		addr_fd = (void *) simple_strtoul (argv[3], NULL, 16);
	} else if (argc == 3) {
		addr_str = getenv("fdaddr");
		if (addr_str != NULL) {
			addr_fd = (void *)
				simple_strtoul(addr_str, NULL, 16);
		} else {
			addr_fd = CFG_FDT_ADDR;
		}
	}

	dev = (int)simple_strtoul (argv[2], &ep, 16);
	dev_desc = get_dev (argv[1], dev);
	if (dev_desc == NULL) {
		printf("** Block device %s %d not supported\n", argv[1], dev);
		return(1);
	}

	if (*ep) {
		if (*ep != ':') {
			printf("** Invalid boot device, use `dev[:part]'\n");
			return(1);
		}
		part = (int)simple_strtoul(++ep, NULL, 16);
	}

	debug("Using device %s%d, partition %d\n", argv[1], dev, part);
	
	readlen = FD_MAXSECTORS;
	blksz = dev_desc->blksz;
	start = 0;

	if (part != 0) {
		if (get_partition_info (dev_desc, part, &info)) {
			printf("** Bad partition %d\n", part);
			return(1);
		}

		if (info.size > FD_MAXSECTORS) {
			printf("** Warning - Factory Data partition size "
				"exceeds %d sectors, truncating\n",
				FD_MAXSECTORS);
		}
		else {
			readlen = info.size;
		}
		blksz = info.blksz;
		start = info.start;

		debug("Loading from block device %s device %d, partition "
			"%d: Name: %.32s  Type: %.32s\n", argv[1], dev, part,
			info.name, info.type);
	} else {
		debug("Loading factory data from block device %s device "
			"%d\n", argv[1], dev);
	}

	*nbytes = readlen << 9;
	if (dev_desc->block_read(dev_desc->dev,
			start, readlen, addr_fd) != readlen) {
		printf("** Error reading factory area - start sector %ld - size %ld\n", start, readlen);
		memset(addr_fd, 0, readlen << 9);
		return(1);
	}
	
	return(0);
}

/* Load TomTom factory data partition into RAM (device-tree version) */
int do_fdload (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	unsigned int size;
	char ulong_str[16];
	unsigned int part;
	char *addr_str;

	if (argc < 2) {
		printf("Usage:\n%s\n", cmdtp->usage);
		return(1);
	}

	size_fd = 0;
	setenv("fdsize", NULL);

	part = (unsigned int) simple_strtoul (argv[1], NULL, 16);

	addr_str = getenv("fdaddr");
	if (addr_str != NULL)
		addr_fd = (void *) simple_strtoul(addr_str, NULL, 16);
	else
		addr_fd = CFG_FDT_ADDR;

#if (defined(CONFIG_CMD_NAND) && defined(CONFIG_CMD_FASTBOOT))
	if (do_nand_fdload (part, addr_fd, &size))
		return (1);
#else
	return (1);
#endif

	/* TT: ignore the first byte, we clobber it sometimes to force factory mode */
	if ((fdt_magic(addr_fd) & 0x00ffffff) != (FDT_MAGIC & 0x00ffffff)) {
		printf("** Invalid dtb magic (%x/%x)\n", fdt_magic(addr_fd), FDT_MAGIC);
		memset(addr_fd, 0, size);
		return(1);
	}

	/* Get real device-tree binary total size */
	size_fd = fdt_totalsize(addr_fd);
	sprintf(ulong_str, "%#x", size_fd);
	setenv("fdsize", ulong_str);

	/* make sure we didn't pick up any trailing trash */
	memset(addr_fd + size_fd + DSASIG_SIZE, 0, size - size_fd - DSASIG_SIZE);
	printf("Loaded device-tree at address %p of length %lu bytes\n", addr_fd, (unsigned long) size_fd);

	return(0);
}

U_BOOT_CMD(
	fdload,	2,	0,	do_fdload,
	"fdload  - Load TomTom factory data device-tree from MTD partition into ram",
	"fdload partition"
);

int check_fdt_machid(void)
{
	int n, *p, _t;

	if ((n = fdt_path_offset(addr_fd, "/features")) >= 0 &&
	    (p = (int*) fdt_getprop(addr_fd, n, "machine-id", &_t)) != NULL &&
		fdt32_to_cpu(*p) != gd->bd->bi_arch_number) {
		eprintf ("Bad machine ID from FDT: %lu\n", gd->bd->bi_arch_number);
		return 1;
	} 

	return 0;
}

#ifdef CONFIG_FDT_FB

const char *fdt_get_str_property(const char *path, const char *name)
{
	int n, _t;

	n = fdt_path_offset(addr_fd, path);
	if(n < 0) {
		return NULL;
	}

	return (const char*) fdt_getprop(addr_fd, n, name, &_t);
}


int fdt_get_string_property_or_fallback(const char *path, const char *name, char *dest, unsigned int maxSize, const char *fallback)
{
	const char* result;
	int rv = 0;

	if(maxSize == 0) {
		return -1;
	}

	result = fdt_get_str_property(path, name);
	if(result == NULL) {
		result = fallback;
		rv = 1;
	}

	strncpy(dest, result, maxSize);
	dest[maxSize-1] = '\0';

	return rv;
}

int fdt_get_int_property(const char *path, const char *name, int *result)
{
	int n, *p, _t;

	if(result == NULL) {
		return -1;
	}

	n = fdt_path_offset(addr_fd, path);
	if(n < 0) {
		return -2;
	}

	p = (int*) fdt_getprop(addr_fd, n, name, &_t);
	if(p == NULL) {
		return -3;
	}

	*result = fdt32_to_cpu(*p);
	return 0;
}

#endif /* CONFIG_FDT_FB */

#endif /* CONFIG_CMD_FD */

