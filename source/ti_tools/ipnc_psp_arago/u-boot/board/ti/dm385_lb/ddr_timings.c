/*
 * Copyright (C) 2009, Texas Instruments, Incorporated
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "ddr_timings.h"

/* Contains the ddr settings that are PCB and memory speed related */
/* Index: [pcb_id] */
const ddr_pcb_timing_t dm38x_ddr_pcb_timings[MAX_N_PCB_IDS] = {
#if defined(CONFIG_DM385)
/* LB DDR settings for 533MHz */
#if defined(CONFIG_DM385_DDR3_533)
		{ /* SM_WS1_BOARD */
			.pcb_name = "SM_WS1_BOARD_DDR3_533",
			.ddr3_phy_rd_dqs_cs0_byte0 = 0x37,
			.ddr3_phy_rd_dqs_cs0_byte1 = 0x38,
			.ddr3_phy_rd_dqs_cs0_byte2 = 0x37,
			.ddr3_phy_rd_dqs_cs0_byte3 = 0x36,

			.ddr3_phy_rd_dqs_gate_cs0_byte0 = 0x96,
			.ddr3_phy_rd_dqs_gate_cs0_byte1 = 0x9d,
			.ddr3_phy_rd_dqs_gate_cs0_byte2 = 0x9c,
			.ddr3_phy_rd_dqs_gate_cs0_byte3 = 0x9a,

			.ddr3_phy_wr_dqs_cs0_byte0 = 0x41,
			.ddr3_phy_wr_dqs_cs0_byte1 = 0x44,
			.ddr3_phy_wr_dqs_cs0_byte2 = 0x42,
			.ddr3_phy_wr_dqs_cs0_byte3 = 0x45,

			.ddr3_phy_wr_data_cs0_byte0 = 0x7c,
			.ddr3_phy_wr_data_cs0_byte1 = 0x7d,
			.ddr3_phy_wr_data_cs0_byte2 = 0x7f,
			.ddr3_phy_wr_data_cs0_byte3 = 0x7d,
		},
#elif defined(CONFIG_DM385_DDR3_400)
		{ /* SM_WS1_BOARD */
			.pcb_name = "SM_WS1_BOARD_DDR3_400",
			.ddr3_phy_rd_dqs_cs0_byte0 = 0x3a,
			.ddr3_phy_rd_dqs_cs0_byte1 = 0x38,
			.ddr3_phy_rd_dqs_cs0_byte2 = 0x3a,
			.ddr3_phy_rd_dqs_cs0_byte3 = 0x38,

			.ddr3_phy_rd_dqs_gate_cs0_byte0 = 0x92,
			.ddr3_phy_rd_dqs_gate_cs0_byte1 = 0x95,
			.ddr3_phy_rd_dqs_gate_cs0_byte2 = 0x96,
			.ddr3_phy_rd_dqs_gate_cs0_byte3 = 0x96,

			.ddr3_phy_wr_dqs_cs0_byte0 = 0x42,
			.ddr3_phy_wr_dqs_cs0_byte1 = 0x43,
			.ddr3_phy_wr_dqs_cs0_byte2 = 0x42,
			.ddr3_phy_wr_dqs_cs0_byte3 = 0x45,

			.ddr3_phy_wr_data_cs0_byte0 = 0x7f,
			.ddr3_phy_wr_data_cs0_byte1 = 0x7f,
			.ddr3_phy_wr_data_cs0_byte2 = 0x7e,
			.ddr3_phy_wr_data_cs0_byte3 = 0x7f,
		},
#elif defined(CONFIG_DM385_DDR3_529)
		{ /* SM_WS1_BOARD */
			.pcb_name = "SM_WS1_BOARD_DDR3_529",
			.ddr3_phy_rd_dqs_cs0_byte0 = 0x3a,
			.ddr3_phy_rd_dqs_cs0_byte1 = 0x35,
			.ddr3_phy_rd_dqs_cs0_byte2 = 0x39,
			.ddr3_phy_rd_dqs_cs0_byte3 = 0x37,

			.ddr3_phy_rd_dqs_gate_cs0_byte0 = 0x97,
			.ddr3_phy_rd_dqs_gate_cs0_byte1 = 0x99,
			.ddr3_phy_rd_dqs_gate_cs0_byte2 = 0x9f,
			.ddr3_phy_rd_dqs_gate_cs0_byte3 = 0x9b,

			.ddr3_phy_wr_dqs_cs0_byte0 = 0x3c,
			.ddr3_phy_wr_dqs_cs0_byte1 = 0x3e,
			.ddr3_phy_wr_dqs_cs0_byte2 = 0x3d,
			.ddr3_phy_wr_dqs_cs0_byte3 = 0x3f,

			.ddr3_phy_wr_data_cs0_byte0 = 0x77,
			.ddr3_phy_wr_data_cs0_byte1 = 0x78,
			.ddr3_phy_wr_data_cs0_byte2 = 0x79,
			.ddr3_phy_wr_data_cs0_byte3 = 0x77,
		},
#elif defined(CONFIG_DM385_DDR3_397)
		{ /* SM_WS1_BOARD */
			.pcb_name = "SM_WS1_BOARD_DDR3_397",
			.ddr3_phy_rd_dqs_cs0_byte0 = 0x3a,
			.ddr3_phy_rd_dqs_cs0_byte1 = 0x37,
			.ddr3_phy_rd_dqs_cs0_byte2 = 0x3a,
			.ddr3_phy_rd_dqs_cs0_byte3 = 0x38,

			.ddr3_phy_rd_dqs_gate_cs0_byte0 = 0x90,
			.ddr3_phy_rd_dqs_gate_cs0_byte1 = 0x91,
			.ddr3_phy_rd_dqs_gate_cs0_byte2 = 0x95,
			.ddr3_phy_rd_dqs_gate_cs0_byte3 = 0x92,

			.ddr3_phy_wr_dqs_cs0_byte0 = 0x3e,
			.ddr3_phy_wr_dqs_cs0_byte1 = 0x3f,
			.ddr3_phy_wr_dqs_cs0_byte2 = 0x3e,
			.ddr3_phy_wr_dqs_cs0_byte3 = 0x3f,

			.ddr3_phy_wr_data_cs0_byte0 = 0x79,
			.ddr3_phy_wr_data_cs0_byte1 = 0x7a,
			.ddr3_phy_wr_data_cs0_byte2 = 0x7c,
			.ddr3_phy_wr_data_cs0_byte3 = 0x7a,
		},
#else
  #error "there's no DDR PHY settings"
#endif
#endif
};

ddr_pcb_timing_t *ddr_pcb_timing=dm38x_ddr_pcb_timings;
