/*
 * Copyright (C) 2009, Texas Instruments, Incorporated
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/cache.h>
#include <asm/arch/cpu.h>
#include <asm/arch/ddr_defs.h>
#include <asm/arch/hardware.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/mmc_host_def.h>
#include <asm/arch/clock.h>
#include <asm/arch/mem.h>
#include <asm/arch/nand.h>
#include <asm/arch/gpio.h>
#include <linux/mtd/nand.h>
#include <nand.h>
#include <net.h>
#include <miiphy.h>
#include <netdev.h>
#include <fastboot.h>
#include <watchdog.h>

#define PWRCTRL				0x0
#define CLKCTRL				0x4
#define TENABLE				0x8
#define TENABLEDIV			0xC
#define M2NDIV				0x10
#define MN2DIV				0x14
#define STATUS				0x24

#define PLL_BASE_ADDRESS		(0x481C5000)
#define DSS_PLL_BASE			(PLL_BASE_ADDRESS+0x170)
#define VIDEO_0_PLL_BASE		(PLL_BASE_ADDRESS+0x1A0)
#define VIDEO_1_PLL_BASE		(PLL_BASE_ADDRESS+0x1D0)
#define EMIF_CLK_GATE 			(0x48140694)
#define SECSS_CLK_SRC 			(0x481C52EC)

#define CTROL_STATUS_REG	(0x48140040)
#define SYSBOOT_SD			0x17
#define SYSBOOT_NAND			0x13
#define SYSBOOT		((__raw_readl(CTROL_STATUS_REG)&0x1F))

/* ISP (ISS) clock, reset and power management */
#define CM_ISP_CLKSTCTRL		0x0700
#define CM_ISP_ISP_CLKCTRL		0x0720
#define RM_ISP_RSTCTRL			0x0D10
#define ISP_RST_MASK			0x00000004
#define ISP_CLKCTRL_MODULEMODE_EN	0x00000002

#define CTRL_MMR_LOCK4			(0x48140070) /* PWR_CTL1 of CTRL module */
#define CTRL_PWR_CTL1			(0x4814131C) /* Clock gating of CTRL Module */
#define OCMC_RAM_PWRDN			(0x48140654)

#define UNKNOWN_BOARD			0xFFFFFFFF
/*
// Board id is defined in the kernel/arch/arm/plat-tomtom/include/plat/board_id.h
// Since we will pass board_id to kernel and some kernel drivers will refer it.
// For Santa Monica WS1 bring-up, we set borad id to 0x00000006 temporary.
#define LONG_BEACH_WS1_BOARD		0x00000001
#define LONG_BEACH_WS2_BOARD		0x00000002
#define LONG_BEACH_PR1_BOARD		0x00000003 // PR1 / PR2 not defined in FDT
#define LONG_BEACH_PR2_BOARD		0x00000004 // PR2 defined in FDT
#define LONG_BEACH_MFB1_BOARD		0x00000005 // MFD1 defined in FDT
*/
#define SANTA_MONICA_WS1_BOARD		0x00000006

#define GPIO_KEY_UP	                (2*32+8)
#define GPIO_KEY_DOWN	                (2*32+9)
#define GPIO_KEY_STOP	                (2*32+10)

#ifdef CONFIG_CMD_FASTBOOT
#define GPIO_FB_SW			GPIO_KEY_UP
#define GPIO_FB_INDICATOR		(0*32+8) // LED1 (record).
#define MAX_PTN				9
#endif

#define GPIO_SENSOR_RESET		103
#define GPIO_SENSOR_POWER_EN		48
#define GPIO_GPS_RESET			99
#define GPIO_GPS_ON_OFF			94
#define GPIO_GPS_WAKE			93
#define GPIO_PWR_KEY			71
#define GPIO_BACK_CAP_DET		(0*32+17)
#define GPIO_KILL_POWER			119
#define GPIO_REGULATOR_VSEL0		47
#define GPIO_REGULATOR_VSEL1		48
#define GPIO_REGULATOR_CORE2_VSEL0	56
#define GPIO_REGULATOR_CORE2_VSEL1	46
#define GPIO_MCU_RESET			108
#define GPIO_nCHGR_EN			24
#define GPIO_USB_OTG			68
#define GPIO_BOOT_LOAD			81

#define LPJ_390_MHZ			1945600
#define LPJ_970_MHZ			4833280

#define RTC_SCRATCHPAD2_REG		0x480c0068
#define FLIPFLOP_SOFTBOOT_MAGIC		0x50F1B001

#define USB_POWER_ON_MASK 0xF0000000 /* Upper 4 bits are used for USB Power on */
#define USB_POWER_ON_MAGIC 0xC0000000 /* Upper 4 bits are used for USB Power on */

DECLARE_GLOBAL_DATA_PTR;

/**
 * Address is shared between A8 and M3. A8 checks this
 * address to know if M3 init is done. The value needs
 * to be reset during boot time to prevent false init
 * status read
 */
#define M3_INIT_CHECK_PHYSICAL_ADDRESS    (0xbf900000)

/**
 * For gpio keys we need to configure multiplexer and gpio as input.
 * This requires the PIN multiplexer register and the gpio number.
 * To define them at the same spot, this structure is introduced.
 */
typedef struct {
	u32 gpio; /* The gpio number */
} gpio_key_t;

enum pmic_vol_sel {
	CORE_1P20V,	/* 1.20v */
	CORE_1P36V,	/* 1.36v */
};

enum modena_freq_sel {
	MODENA_FREQ_NORMAL,	/* Normal frequency at default voltage */
	MODENA_FREQ_HIGH,	/* High frequency with high voltage */
};

/**
 * Boot type
 */
enum boot_type {
	BOOT_TYPE__NORMAL,	/* Normal boot */
	BOOT_TYPE__RECOVERY,	/* Boot into recovery */
	BOOT_TYPE__FASTBOOT,	/* Boot into fastboot mode */
	BOOT_TYPE__SDBOOT,	/* Boot into configni mode */
};

/**
 * Reset source. Used to express the SoC's reset reason.
 */
enum reset_source {
	RESET_SOURCE__COLD, 	/* Power on. (SoC is coming out of reset after power on) */
	RESET_SOURCE__WARM,	/* HW / SW reset. Initiated by HW/SW. All resets excluding watchdog reset */
	RESET_SOURCE__WATCHDOG, /* HW (SoC) Watchdog reset */
};

#ifdef CONFIG_CMD_FASTBOOT
/* Initialize the name of fastboot flash name mappings */
fastboot_ptentry ptn[MAX_PTN] = {
	{
		.name   = "uboot",
		.start  = 0x0000000,
		.length = 0x0020000,
		/* Written into the one 0x20000 block
		   Use HW ECC */
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_I |
			FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC,
	},
	{
		.name   = "uboot2",
		.start  = 0x0020000,
		.length = 0x0240000,
		/* Skip bad blocks on write
		   Use HW ECC */
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_I |
			FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC,
	},
	{
		.name   = "environment",
		.start  = MNAND_ENV_OFFSET,  /* set in config file */
		.length = 0x00A0000,
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC |
			FASTBOOT_PTENTRY_FLAGS_WRITE_ENV,
	},
	{
		.name   = "kernel",
		/* Test with start close to bad block
		   The is dependent on the individual board.
		   Change to what is required */
		/* The real start */
		.start  = 0x0300000,
		.length = 0x0C00000,
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC |
			FASTBOOT_PTENTRY_FLAGS_WRITE_I,
	},
	{
		.name   = "system",
		.start  = 0x0F00000,
		.length = 0x5000000,
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC |
			FASTBOOT_PTENTRY_FLAGS_WRITE_I,
	},
	{
		.name   = "userdata",
		.start  = 0x5F00000,
		.length = 0x1000000,
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC |
			FASTBOOT_PTENTRY_FLAGS_WRITE_I,
	},
	{
		.name   = "recovery",
		.start  = 0x6F00000,
		.length = 0x1000000,
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC |
			FASTBOOT_PTENTRY_FLAGS_WRITE_I,
	},
	{
		.name   = "fdt",
		.start  = 0x7F00000,
		.length = 0x0040000,
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC |
			FASTBOOT_PTENTRY_FLAGS_WRITE_I,
	},
	{
		.name   = "reserved",
		.start  = 0x7F40000,
		.length = 0x00C0000,
		.flags  = FASTBOOT_PTENTRY_FLAGS_WRITE_HW_BCH8_ECC |
			FASTBOOT_PTENTRY_FLAGS_WRITE_I,
	},
};

int num_mtd_part = (sizeof(ptn) / sizeof(struct fastboot_ptentry));

#endif /* CONFIG_FASTBOOT */

static u32 hardware_version;

#define N_RECOVERY_COMBO_KEYS 2 /* Excluding power/start button */

static const gpio_key_t recovery_key_combo[N_RECOVERY_COMBO_KEYS] = {
	{
		.gpio = GPIO_KEY_STOP, /* SW_STOP */
	},
	{
		.gpio = GPIO_KEY_DOWN, /* SW_DOWN (down key) */
	}
};

#ifdef CONFIG_DM385_CONFIG_DDR
static void cmd_macro_config(u32 ddr_phy, u32 inv_clk_out,
		u32 ctrl_slave_ratio_cs0, u32 cmd_dll_lock_diff)
{
	u32 ddr_phy_base = DDR0_PHY_BASE_ADDR;

	__raw_writel(inv_clk_out,
			ddr_phy_base + CMD1_REG_PHY_INVERT_CLKOUT_0);
	__raw_writel(inv_clk_out,
			ddr_phy_base + CMD0_REG_PHY_INVERT_CLKOUT_0);
	__raw_writel(inv_clk_out,
			ddr_phy_base + CMD2_REG_PHY_INVERT_CLKOUT_0);

	__raw_writel(((ctrl_slave_ratio_cs0 << 10) | ctrl_slave_ratio_cs0),
		ddr_phy_base + CMD0_REG_PHY_CTRL_SLAVE_RATIO_0);
	__raw_writel(((ctrl_slave_ratio_cs0 << 10) | ctrl_slave_ratio_cs0),
		ddr_phy_base + CMD1_REG_PHY_CTRL_SLAVE_RATIO_0);
	__raw_writel(((ctrl_slave_ratio_cs0 << 10) | ctrl_slave_ratio_cs0),
		 ddr_phy_base + CMD2_REG_PHY_CTRL_SLAVE_RATIO_0);

	__raw_writel(cmd_dll_lock_diff,
		 ddr_phy_base + CMD0_REG_PHY_DLL_LOCK_DIFF_0);
	__raw_writel(cmd_dll_lock_diff,
		 ddr_phy_base + CMD1_REG_PHY_DLL_LOCK_DIFF_0);
	__raw_writel(cmd_dll_lock_diff,
		 ddr_phy_base + CMD2_REG_PHY_DLL_LOCK_DIFF_0);
}

static void data_macro_config(u32 macro_num, u32 emif, u32 rd_dqs_cs0,
		u32 wr_dqs_cs0, u32 fifo_we_cs0, u32 wr_data_cs0)
{
	/* 0xA4 is size of each data macro mmr region.
	 * phy1 is at offset 0x400 from phy0
	 */
	u32 base = (macro_num * 0xA4) + (emif * 0x400);

	__raw_writel(((rd_dqs_cs0 << 10) | rd_dqs_cs0),
		(DATA0_REG_PHY0_RD_DQS_SLAVE_RATIO_0 + base));
	__raw_writel(((wr_dqs_cs0 << 10) | wr_dqs_cs0),
		(DATA0_REG_PHY0_WR_DQS_SLAVE_RATIO_0 + base));
	__raw_writel(((PHY_WRLVL_INIT_CS1_DEFINE << 10) |
		PHY_WRLVL_INIT_CS0_DEFINE),
		(DATA0_REG_PHY0_WRLVL_INIT_RATIO_0 + base));
	__raw_writel(((PHY_GATELVL_INIT_CS1_DEFINE << 10) |
		PHY_GATELVL_INIT_CS0_DEFINE),
		(DATA0_REG_PHY0_GATELVL_INIT_RATIO_0 + base));
	__raw_writel(((fifo_we_cs0 << 10) | fifo_we_cs0),
		(DATA0_REG_PHY0_FIFO_WE_SLAVE_RATIO_0 + base));
	__raw_writel(((wr_data_cs0 << 10) | wr_data_cs0),
		(DATA0_REG_PHY0_WR_DATA_SLAVE_RATIO_0 + base));
	__raw_writel(PHY_DLL_LOCK_DIFF_DEFINE,
		(DATA0_REG_PHY0_DLL_LOCK_DIFF_0 + base));
}
#endif

static u32 pll_dco_freq_sel(u32 clkout_dco);
static u32 pll_sigma_delta_val(u32 clkout_dco);
static void pll_config(u32, u32, u32, u32, u32);
static void audio_pll_config(void);
static void modena_pll_config(enum modena_freq_sel);
static void l3_pll_config(void);
static void ddr_pll_config(void);
static void iss_pll_config(void);
static void iva_pll_config(void);
static void usb_pll_config(void);

static void unlock_pll_control_mmr(void);
static void gpio_init(void);

static u32 board_identify(void);
/*
 * spinning delay to use before udelay works
 */
static inline void delay(unsigned long loops)
{
	__asm__ volatile ("1:\n" "subs %0, %1, #1\n"
		"bne 1b" : "=r" (loops) : "0"(loops));
}

static void pmic_voltage_select(enum pmic_vol_sel voltage)
{
	/* Voltage selection */
	ti81xx_request_gpio(GPIO_REGULATOR_VSEL0);
	ti81xx_request_gpio(GPIO_REGULATOR_VSEL1);
	ti81xx_request_gpio(GPIO_REGULATOR_CORE2_VSEL0);
	ti81xx_request_gpio(GPIO_REGULATOR_CORE2_VSEL1);

	ti81xx_set_gpio_direction(GPIO_REGULATOR_VSEL0, 0);
	ti81xx_set_gpio_direction(GPIO_REGULATOR_VSEL1, 0);
	ti81xx_set_gpio_direction(GPIO_REGULATOR_CORE2_VSEL0, 0);
	ti81xx_set_gpio_direction(GPIO_REGULATOR_CORE2_VSEL1, 0);

	ti81xx_set_gpio_dataout(GPIO_REGULATOR_VSEL1, 0);
	ti81xx_set_gpio_dataout(GPIO_REGULATOR_CORE2_VSEL1, 0);
	if(voltage == CORE_1P20V) {
		ti81xx_set_gpio_dataout(GPIO_REGULATOR_VSEL0, 0);
		ti81xx_set_gpio_dataout(GPIO_REGULATOR_CORE2_VSEL0, 0);
	} else if(voltage == CORE_1P36V) {
		ti81xx_set_gpio_dataout(GPIO_REGULATOR_VSEL0, 1);
		ti81xx_set_gpio_dataout(GPIO_REGULATOR_CORE2_VSEL0, 1);
	}
}

static void modena_freq_scale_up(void)
{
	pmic_voltage_select(CORE_1P36V);

	/* voltage ramp timing - 5us required, keeping 10us for safety */
	udelay(10);

	modena_pll_config(MODENA_FREQ_HIGH);
}

/**
 * Determines the reset source and returns this.
 */
static enum reset_source determine_reset_source()
{
	enum reset_source reset_source;
	u32 prm_reset_status;
	u32 rtc_reg;

	prm_reset_status = __raw_readl(PRM_DEVICE_RSTST);

	/* clear the sticky bits by writing back the reason */
	__raw_writel(prm_reset_status, PRM_DEVICE_RSTST);

	if (prm_reset_status & GLOBAL_RST_COLD) {
		/*
		 * Warm reset is implemented as a cold reset.
		 * So to detect a warm reset, check flip flop value.
		 */
		rtc_reg = __raw_readl(RTC_SCRATCHPAD2_REG);
		if (rtc_reg == FLIPFLOP_SOFTBOOT_MAGIC)
			reset_source = RESET_SOURCE__WARM;
		else
			reset_source = RESET_SOURCE__COLD;
	} else if (prm_reset_status & GLOBAL_WARM_SW_RST)
		reset_source = RESET_SOURCE__WARM;
	else if (prm_reset_status & MPU_WDT_RST)
		reset_source = RESET_SOURCE__WATCHDOG;

	return reset_source;
}

/**
 * Returns 1 when recovery combination is being pressed.
 */
static int is_recovery_combo_pressed()
{
	int i;
	int result = 1;

	/* Check state each recovery key */
	for (i = 0; i < N_RECOVERY_COMBO_KEYS; i++) {
		if (ti81xx_get_gpio_datain(recovery_key_combo[i].gpio) != 0) {
			result = 0;
			break;
		}
	}

	return result;
}

/**
 * Returns 1 when boot-up from SD.
 */
static int is_sd_boot()
{
	return (SYSBOOT==SYSBOOT_SD)?1:0;
}

/**
 * Returns 1 when the fastboot key is allowed
 * Returns 0 when the fastboot key is not allowed.
 *
 * Fastboot key is not allowed for production device.
 * Device is a production device when device-class is 0 and device-serial is valid.
 */
static int fastboot_key_allowed(int fdtloaded)
{
	const char* device_serial;
	int device_class;

	/* If fdt is not loaded. Device-class flag is not valid => Fastboot key allowed */
	if(fdtloaded != 0)
		return 1;

#ifdef CONFIG_CMD_FD
	/*
	 * If device-serial is not valid => Fastboot key allowed
	 * The device-serial is valid when not NULL and does not equal to "FAKEWIN@".
	 */
	device_serial = fdt_get_str_property("/features", "device-serial");
	if(device_serial == NULL || strcmp(device_serial, "FAKEWIN@") == 0)
		return 1;

	/* Read device-class. If it fails => Fastboot key allowed. */
	if(fdt_get_int_property("/features", "device-class", &device_class) != 0)
		return 1;

	/*
	 * When device-class equals to 1, than this is a development device => Fastboot key allowed
	 * When device-class is not 1, than this is a production device => Fastboot key not allowed
	 */
	return device_class == 1 ? 1 : 0;
#else /* CONFIG_CMD_FD */
	return 1; /* No support for FDT => Fastboot key allowed */
#endif /* CONFIG_CMD_FD */
}

/**
 * Determines the bootcmd and boot_reason.
 */
static void determine_boot_reason_and_bootcmd(int fdtloaded)
{
	enum reset_source reset_source;
	enum boot_type boot_type;
	const char* boot_reason;
	const char* bootcmd;
	const char* deftarget;

	/* Determine boot type based on reset reason and state of flipflop
	 * and state of buttons.
	 */
	deftarget = "default.target";
	reset_source = determine_reset_source();

	switch (reset_source) {
	case RESET_SOURCE__WARM:
		boot_reason = "warm";
		break;

	case RESET_SOURCE__WATCHDOG:
		boot_reason = "watchdog";
		break;

	case RESET_SOURCE__COLD:  /* Fall-through */
	default:
		boot_reason = "cold";
		break;
	}

	boot_type = BOOT_TYPE__NORMAL;

#ifdef CONFIG_FASTBOOT
	if (__raw_readl(RTC_SCRATCHPAD1_REG) == FLIPFLOP_FASTBOOT_MAGIC)
		boot_type = BOOT_TYPE__FASTBOOT;
	else if (fastboot_key_allowed(fdtloaded) && ti81xx_get_gpio_datain(GPIO_FB_SW) == 0)
		boot_type = BOOT_TYPE__FASTBOOT;
	else
#endif
	if (__raw_readl(RTC_SCRATCHPAD0_REG) == FLIPFLOP_RECOVERY_BOOT_MAGIC) {
		boot_type = BOOT_TYPE__RECOVERY;
		boot_reason = "recovery";
	} else if ((__raw_readl(RTC_SCRATCHPAD1_REG) & USB_POWER_ON_MASK) == USB_POWER_ON_MAGIC) {
		boot_type = BOOT_TYPE__NORMAL;
		boot_reason = "usb";
		deftarget = "usb.target";
	} else if (__raw_readl(RTC_SCRATCHPAD0_REG) == FLIPFLOP_RECOVERY_BOOT_UPDATE_MAGIC) {
		boot_type = BOOT_TYPE__RECOVERY;
		boot_reason = "update";
	} else if (is_recovery_combo_pressed()) {
		boot_type = BOOT_TYPE__RECOVERY;
		boot_reason = "recoverykey";
	} else if (is_sd_boot()) {
		boot_type = BOOT_TYPE__SDBOOT;
		boot_reason = "configni";
	}
#ifdef CONFIG_CMD_FD
	else if (fdtloaded != 0) {
		boot_type = BOOT_TYPE__RECOVERY;
		boot_reason = "ftd";
	}
#endif

	/* Determine bootcmd based on boot type. */
	switch (boot_type) {
	case BOOT_TYPE__FASTBOOT:
		bootcmd = "fastboot\0";
		break;

	case BOOT_TYPE__RECOVERY:
		bootcmd = "run recovery_bootcmd; fastboot\0";
		break;

	case BOOT_TYPE__SDBOOT:
		bootcmd = "run sd_bootcmd\0";
		break;

	case BOOT_TYPE__NORMAL: /* Fall-through */
	default:
		bootcmd = "run normal_bootcmd; run recovery_bootcmd; fastboot\0";
		break;
	}

	setenv("boot_reason", (char *) boot_reason);
	setenv("bootcmd", (char *) bootcmd);
	setenv("deftarget", (char *) deftarget);
}

static void get_lpj_value(void)
{
	int arm_freq;
	char lpj[16];

	arm_freq = ((OSC_0_FREQ / (MODENA_N + 1) * MODENA_M) / MODENA_M2);

	switch (arm_freq) {
	case 390:
		sprintf(lpj, "%d", LPJ_390_MHZ);
		setenv("lpj", lpj);
		break;
	case 970:
		sprintf(lpj, "%d", LPJ_970_MHZ);
		setenv("lpj", lpj);
		break;
	default:
		printf("\n*** Warning - No lpj value available for ARM frequency %d MHz\n",
				arm_freq);
		printf("Kernel will calculate lpj during boot\n\n");
		setenv("lpj", "");
		break;
	}
}

/*
 * Basic board specific setup
 */
int board_init(void)
{
	u32 regVal;

	/* Do the required pin-muxing before modules are setup */
	set_muxconf_regs();

#if defined(CONFIG_HW_WATCHDOG)
	hw_watchdog_init();
#endif

	/* setup RMII_REFCLK to be sourced from audio_pll */
	__raw_writel(0x4, RMII_REFCLK_SRC);

	/*program GMII_SEL register for RGMII mode */
	__raw_writel(0x30a, GMII_SEL);

	gpio_init();

	modena_freq_scale_up();

	/* Get Timer and UART out of reset */
	/* UART softreset */
	regVal = __raw_readl(UART_SYSCFG);
	regVal |= 0x2;
	__raw_writel(regVal, UART_SYSCFG);
	while ((__raw_readl(UART_SYSSTS) & 0x1) != 0x1)
		;

	/* Disable smart idle */
	regVal = __raw_readl(UART_SYSCFG);
	regVal |= (1<<3);
	__raw_writel(regVal, UART_SYSCFG);

	/* mach type passed to kernel */
	gd->bd->bi_arch_number = MACH_TYPE_DM385LB;

	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_DRAM_1 + 0x100;
	gpmc_init();

#ifndef CONFIG_NOR
	/* GPMC will come up with default buswidth configuration,
	 * we will override it based on BW pin CONFIG_STATUS register.
	 * This is currently required only for NAND/NOR to
	 * support 8/16 bit NAND/NOR part. Also we always use chipselect 0
	 * for NAND/NOR boot.
	 *
	 * NOTE: This code is DM8168 EVM specific, hence we are using CS 0.
	 * Also, even for other boot modes user is expected to
	 * on/off the BW pin on the EVM.
	 */
	gpmc_set_cs_buswidth(0, get_sysboot_ipnc_bw());
#endif

#ifdef CONFIG_CMD_FASTBOOT
	int i;

	for (i = 0; i < MAX_PTN; i++)
		fastboot_flash_add_ptn(&ptn[i]);
#endif /* CONFIG_FASTBOOT */

	/* M3 init check address needs to be reset */
	__raw_writel(0,M3_INIT_CHECK_PHYSICAL_ADDRESS);

	return 0;
}

static u32 board_identify(void)
{
	// GPIO_OE Output Enable Register
	__raw_writel(__raw_readl(0x48032134) | (1 << 9), 0x48032134);
	while(!(__raw_readl(0x48032134) & (1 << 9)));

	// GPIO_OE Output Enable Register
	//__raw_writel(__raw_readl(0x48032134) | (1 << 12), 0x48032134);
	//while(!(__raw_readl(0x48032134) & (1 << 12)));

	// GPIO_OE Output Enable Register
	__raw_writel(__raw_readl(0x48032134) | (1 << 13), 0x48032134);
	while(!(__raw_readl(0x48032134) & (1 << 13)));

	// GPIO_DATAIN Data In Register
	// SANTA_MONICA_WS1_BOARD
	//if((((__raw_readl(0x48032138) & 0x200) >> 9) == 0) && (((__raw_readl(0x48032138) & 0x1000) >> 12) == 0) && (((__raw_readl(0x48032138) & 0x2000) >> 13) == 0))
	if((((__raw_readl(0x48032138) & 0x200) >> 9) == 0) && (((__raw_readl(0x48032138) & 0x2000) >> 13) == 0))
		return SANTA_MONICA_WS1_BOARD;
	// Simulate on LB_BOARD, will remove it when SM_WS1_BOARD is arrival
        //else if((((__raw_readl(0x48032138) & 0x200) >> 9) == 0) && (((__raw_readl(0x48032138) & 0x1000) >> 12) == 0) && (((__raw_readl(0x48032138) & 0x2000) >> 13) == 1))
	else if((((__raw_readl(0x48032138) & 0x200) >> 9) == 0) && (((__raw_readl(0x48032138) & 0x2000) >> 13) == 1))
                return SANTA_MONICA_WS1_BOARD;
	else
		return UNKNOWN_BOARD;
}

static u32 get_hardware_version(int fdtloaded)
{
	int value = 0;
// [LBP-7103] Read Board ID by using GPIO (GP0[9],GP0[12],GP0[13])
/*
#ifdef CONFIG_CMD_FD
	if (fdtloaded == 0) {
		if (fdt_get_int_property("/features", "hardware-version", &value) == 0 && value >= 0) {
			return (u32) value;
		}
	}
#endif
*/
	/* Fallback */
	return board_identify();
}

/*
 * sets uboots idea of sdram size
 */
int dram_init(void)
{
	/* Fill up board info */
	gd->bd->bi_dram[0].start = PHYS_DRAM_1;
	gd->bd->bi_dram[0].size = PHYS_DRAM_1_SIZE;

	return 0;
}
#ifdef CONFIG_SERIAL_TAG
/* *********************************************************
 * * get_board_serial() - setup to pass kernel board serial
 * * returns: board serial number
 * **********************************************************
 */
void get_board_serial(struct tag_serialnr *serialnr)
{
	/* ToDo: read eeprom and return*/
	serialnr->high = 0x0;
	serialnr->low = 0x0;
}
#endif

#ifdef CONFIG_REVISION_TAG
/**********************************************************
 * * get_board_rev() - setup to pass kernel board revision
 * * returns: revision
 * ********************************************************
*/
u32 get_board_rev(void)
{
	return hardware_version;
}
#endif

void check_batlock_state()
{
	/* Get BACK_CAP_DET switch state */
	int batlock_state = ti81xx_get_gpio_datain(GPIO_BACK_CAP_DET);
	if (batlock_state){
		printf("\nBACK_CAP_DET switch pressed, shutting down.\n");

		// Clear warm start flags. We are shutting down, so next start is cold start.
		__raw_writel(0, RTC_SCRATCHPAD2_REG);

		ti81xx_request_gpio(GPIO_KILL_POWER);
		ti81xx_set_gpio_dataout(GPIO_KILL_POWER, 1); // Set output value
		ti81xx_set_gpio_direction(GPIO_KILL_POWER, 0); // Set direction as output
		while (1) ; // Power-off is not immediate, do not let booting proceede.
	}
}

int misc_init_r(void)
{
	int fdtloaded;
	printf("Kernel will be auto-loaded if countdown not interrupted\n");

#ifdef CONFIG_DM385_ASCIIART
	int i = 0, j = 0;

	char dm385[22][67] = {
"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",
"@@                                                               @@",
"@@                                                               @@",
"@@      L O N G   B E A CH         L O N G   B E A C H           @@",
"@@                                                               @@",
"@@      @@@        @.   @.         @@@@    @@@@  G@@@@@@         @@",
"@@      @@@@@     L@@  @@@        @@@@@@  @@@@@i @@@@@@          @@",
"@@      @@ @@@.   @@@  @@@            @@  @,  @@ :@@             @@",
"@@      @@   @@   @@@  @@@          @@@@  @@;@@: C@@@@@.         @@",
"@@      @@    @@  @@@.l@L@         G@@@   ,@@@@  @@@  @@         @@",
"@@      @@    @@ l@ @@@@ @.          l@@  @@ L@@  @   G@         @@",
"@@      @@    @@ @@ @@@@ @@           @@ C@   @@      @@         @@",
"@@      @@   ,@C @@ @@@  @@       @i  @@ C@   @@ @@   @@         @@",
"@@      @@@@@@@  @@  @@  @@       @@@@@l  @@@@@@ L@@@@@          @@",
"@@       @@@@@   @   @    @        L@@     @@@,   ,@@G           @@",
"@@                                                               @@",
"@@      L O N G   B E A CH         L O N G   B E A C H           @@",
"@@                                                               @@",
"@@                                                               @@",
"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",
"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",};

	for (i = 0; i < 22; i++) {
		for (j = 0; j < 67; j++)
			printf("%c", dm385[i][j]);
			printf("\n");
	}
	printf("\n");
#endif

	/* Check if batlock is pressed. If so, shut down. */
	check_batlock_state();

#ifdef CONFIG_CMD_FD
	/* Try to load fdt */
	fdtloaded = run_command("fdload 7", 0);
#else
	fdtloaded = 0;
#endif

	/* Determine boot reason and boot cmd */
	determine_boot_reason_and_bootcmd(fdtloaded);

	hardware_version = get_hardware_version(fdtloaded);

	/* get lpj value from fdt */
	get_lpj_value();

	return 0;
}
#ifdef CONFIG_DM385_CONFIG_DDR
#include "ddr_timings.h"
static void config_dm385_ddr(u32 board_id)
{
	ddr_pcb_timing_t *ddr_pcb_timing=TO_SRAM_ADDR(dm38x_ddr_pcb_timings);

	/* Enable the Power Domain Transition of L3 Fast Domain Peripheral */
	__raw_writel(0x2, CM_DEFAULT_FW_CLKCTRL);
	/* Enable the Power Domain Transition of L3 Fast Domain Peripheral */
	__raw_writel(0x2, CM_DEFAULT_L3_FAST_CLKSTCTRL);
	__raw_writel(0x2, CM_DEFAULT_EMIF_0_CLKCTRL); /* Enable EMIF0 Clock */
	__raw_writel(0x2, CM_DEFAULT_DMM_CLKCTRL);

	/* Poll for L3_FAST_GCLK  & DDR_GCLK  are active */
	while ((__raw_readl(CM_DEFAULT_L3_FAST_CLKSTCTRL) & 0x300) != 0x300)
		;
	/* Poll for Module is functional */
	while ((__raw_readl(CM_DEFAULT_EMIF_0_CLKCTRL)) != 0x2)
		;
	while ((__raw_readl(CM_DEFAULT_DMM_CLKCTRL)) != 0x2)
		;

	cmd_macro_config(DDR_PHY0, DDR3_PHY_INVERT_CLKOUT_OFF,
			DDR3_PHY_CTRL_SLAVE_RATIO_CS0_DEFINE,
			PHY_CMD0_DLL_LOCK_DIFF_DEFINE);


	switch(board_id)
	{
		case SANTA_MONICA_WS1_BOARD:
		default:
			ddr_pcb_timing+=SM_WS1_BOARD;
			break;
	}

	data_macro_config(DATA_MACRO_0, DDR_PHY0,
		ddr_pcb_timing->ddr3_phy_rd_dqs_cs0_byte0,
		ddr_pcb_timing->ddr3_phy_wr_dqs_cs0_byte0,
		ddr_pcb_timing->ddr3_phy_rd_dqs_gate_cs0_byte0,
		ddr_pcb_timing->ddr3_phy_wr_data_cs0_byte0);

	data_macro_config(DATA_MACRO_1, DDR_PHY0,
		ddr_pcb_timing->ddr3_phy_rd_dqs_cs0_byte1,
		ddr_pcb_timing->ddr3_phy_wr_dqs_cs0_byte1,
		ddr_pcb_timing->ddr3_phy_rd_dqs_gate_cs0_byte1,
		ddr_pcb_timing->ddr3_phy_wr_data_cs0_byte1);

	data_macro_config(DATA_MACRO_2, DDR_PHY0,
		ddr_pcb_timing->ddr3_phy_rd_dqs_cs0_byte2,
		ddr_pcb_timing->ddr3_phy_wr_dqs_cs0_byte2,
		ddr_pcb_timing->ddr3_phy_rd_dqs_gate_cs0_byte2,
		ddr_pcb_timing->ddr3_phy_wr_data_cs0_byte2);

	data_macro_config(DATA_MACRO_3, DDR_PHY0,
		ddr_pcb_timing->ddr3_phy_rd_dqs_cs0_byte3,
		ddr_pcb_timing->ddr3_phy_wr_dqs_cs0_byte3,
		ddr_pcb_timing->ddr3_phy_rd_dqs_gate_cs0_byte3,
		ddr_pcb_timing->ddr3_phy_wr_data_cs0_byte3);

	/* DDR IO CTRL config */
	__raw_writel(DDR0_IO_CTRL_DEFINE, DDR0_IO_CTRL);

	__raw_writel(__raw_readl(VTP0_CTRL_REG) | 0x00000040 , VTP0_CTRL_REG);

	/* Write 0 to CLRZ bit */
	__raw_writel(__raw_readl(VTP0_CTRL_REG) & 0xfffffffe , VTP0_CTRL_REG);

	/* Write 1 to CLRZ bit */
	__raw_writel(__raw_readl(VTP0_CTRL_REG) | 0x00000001 , VTP0_CTRL_REG);

	/* Read VTP control registers & check READY bits */
	while ((__raw_readl(VTP0_CTRL_REG) & 0x00000020) != 0x20)
		;

	/*
	 * Program the DDR3 DMM to Access EMIF0
	 * 1G contiguous section with no interleaving
	 */
	__raw_writel(DDR3_DMM_LISA_MAP__0, DMM_LISA_MAP__0);
	__raw_writel(DDR3_DMM_LISA_MAP__1, DMM_LISA_MAP__1);
	__raw_writel(DDR3_DMM_LISA_MAP__2, DMM_LISA_MAP__2);
	__raw_writel(DDR3_DMM_LISA_MAP__3, DMM_LISA_MAP__3);

	while (__raw_readl(DMM_LISA_MAP__0) != DDR3_DMM_LISA_MAP__0)
		;
	while (__raw_readl(DMM_LISA_MAP__1) != DDR3_DMM_LISA_MAP__1)
		;
	while (__raw_readl(DMM_LISA_MAP__2) != DDR3_DMM_LISA_MAP__2)
		;
	while (__raw_readl(DMM_LISA_MAP__3) != DDR3_DMM_LISA_MAP__3)
		;

	/* Lock the LISA mapping, reset required to unlock */
	__raw_writel(1, DMM_LISA_LOCK);
	while (__raw_readl(DMM_LISA_LOCK) != 1);

	__raw_writel(0x80000000, DMM_PAT_BASE_ADDR);

	/* Program EMIF0 CFG Registers */
	__raw_writel(DDR3_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_1);
	__raw_writel(DDR3_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_1_SHADOW);
	__raw_writel(DDR3_EMIF_TIM1, EMIF4_0_SDRAM_TIM_1);
	__raw_writel(DDR3_EMIF_TIM1, EMIF4_0_SDRAM_TIM_1_SHADOW);
	__raw_writel(DDR3_EMIF_TIM2, EMIF4_0_SDRAM_TIM_2);
	__raw_writel(DDR3_EMIF_TIM2, EMIF4_0_SDRAM_TIM_2_SHADOW);
	__raw_writel(DDR3_EMIF_TIM3, EMIF4_0_SDRAM_TIM_3);
	__raw_writel(DDR3_EMIF_TIM3, EMIF4_0_SDRAM_TIM_3_SHADOW);
	// setting "NARROW_MODE" bit to 1 (Data Bus Width, 0: 32 bits, 1: 16 bits) in the EMIF SDRAM Control Register(SDRCR)
	__raw_writel((DDR3_EMIF_SDRAM_CONFIG | (1 << 14)), EMIF4_0_SDRAM_CONFIG);

	__raw_writel(DDR_EMIF_REF_CTRL | DDR_EMIF_REF_TRIGGER,
					 EMIF4_0_SDRAM_REF_CTRL);
	__raw_writel(DDR_EMIF_REF_CTRL, EMIF4_0_SDRAM_REF_CTRL_SHADOW);
	__raw_writel(DDR3_EMIF_SDRAM_ZQCR, EMIF4_0_SDRAM_ZQCR);
	__raw_writel(DDR_EMIF_REF_CTRL, EMIF4_0_SDRAM_REF_CTRL);
	__raw_writel(DDR_EMIF_REF_CTRL, EMIF4_0_SDRAM_REF_CTRL_SHADOW);

	__raw_writel(DDR3_EMIF_REF_CTRL, EMIF4_0_SDRAM_REF_CTRL);
	__raw_writel(DDR3_EMIF_REF_CTRL, EMIF4_0_SDRAM_REF_CTRL_SHADOW);
}
#endif

static void audio_pll_config()
{
	pll_config(AUDIO_PLL_BASE,
			AUDIO_N, AUDIO_M,
			AUDIO_M2, AUDIO_CLKCTRL);
}

static void usb_pll_config()
{
	pll_config(USB_PLL_BASE,
			USB_N, USB_M,
			USB_M2, USB_CLKCTRL);
}

static void modena_pll_config(enum modena_freq_sel frequency)
{
	if(frequency == MODENA_FREQ_NORMAL)
		pll_config(MODENA_PLL_BASE, MODENA_N, MODENA_M_LOW,
				MODENA_M2, MODENA_CLKCTRL);
	else if(frequency == MODENA_FREQ_HIGH)
		pll_config(MODENA_PLL_BASE, MODENA_N, MODENA_M,
				MODENA_M2, MODENA_CLKCTRL);
}

static void l3_pll_config()
{
	pll_config(L3_PLL_BASE,
			L3_N, L3_M,
			L3_M2, L3_CLKCTRL);
}

static void ddr_pll_config()
{
	pll_config(DDR_PLL_BASE,
			DDR_N, DDR_M,
			DDR_M2, DDR_CLKCTRL);
}

static void iss_pll_config()
{
	pll_config(ISS_PLL_BASE,
			ISS_N, ISS_M,
			ISS_M2, ISS_CLKCTRL);
}

static void iva_pll_config()
{
	pll_config(IVA_PLL_BASE,
			IVA_N, IVA_M,
			IVA_M2, IVA_CLKCTRL);
}

/*
 * select the HS1 or HS2 for DCO Freq
 * return : CLKCTRL
 */
static u32 pll_dco_freq_sel(u32 clkout_dco)
{

	if (clkout_dco >= DCO_HS2_MIN && clkout_dco < DCO_HS2_MAX)
		return SELFREQDCO_HS2;
	else if (clkout_dco >= DCO_HS1_MIN && clkout_dco < DCO_HS1_MAX)
		return SELFREQDCO_HS1;
	else
		return -1;

}

/*
 * select the sigma delta config
 * return: sigma delta val
 */
static u32 pll_sigma_delta_val(u32 clkout_dco)
{
	u32 sig_val = 0;
	float frac_div;

	frac_div = (float) clkout_dco / 250;
	frac_div = frac_div + 0.90;
	sig_val = (int)frac_div;
	sig_val = sig_val << 24;

	return sig_val;
}

/*
 * configure individual ADPLLJ
 */
static void pll_config(u32 base, u32 n, u32 m, u32 m2, u32 clkctrl_val)
{
	u32 m2nval, mn2val, read_clkctrl = 0, clkout_dco = 0;
	u32 sig_val = 0, hs_mod = 0;

	m2nval = (m2 << 16) | n;
	mn2val = m;

	/* calculate clkout_dco */
	clkout_dco = ((OSC_0_FREQ / (n+1)) * m);

	/* sigma delta & Hs mode selection skip for ADPLLS*/
	if (MODENA_PLL_BASE != base) {
		sig_val = pll_sigma_delta_val(clkout_dco);
		hs_mod = pll_dco_freq_sel(clkout_dco);
	}

	/* by-pass pll */
	read_clkctrl = __raw_readl(base + ADPLLJ_CLKCTRL);
	__raw_writel((read_clkctrl | 0x00800000), (base + ADPLLJ_CLKCTRL));
	while ((__raw_readl(base + ADPLLJ_STATUS) & 0x101) != 0x101);

	/* Clear TINITZ */
	read_clkctrl = __raw_readl(base + ADPLLJ_CLKCTRL);
	__raw_writel((read_clkctrl & 0xfffffffe), (base + ADPLLJ_CLKCTRL));

	/*
	 * ref_clk = 20/(n + 1);
	 * clkout_dco = ref_clk * m;
	 * clk_out = clkout_dco/m2;
	 */

	read_clkctrl = __raw_readl(base + ADPLLJ_CLKCTRL) & 0xffffe3ff;
	__raw_writel(m2nval, (base + ADPLLJ_M2NDIV));
	__raw_writel(mn2val, (base + ADPLLJ_MN2DIV));

	/* Skip for modena(ADPLLS) */
	if (MODENA_PLL_BASE != base) {
		__raw_writel(sig_val, (base + ADPLLJ_FRACDIV));
		__raw_writel((read_clkctrl | hs_mod), (base + ADPLLJ_CLKCTRL));
	}

	/* Load M2, N2 dividers of ADPLL */
	__raw_writel(0x1, (base + ADPLLJ_TENABLEDIV));
	__raw_writel(0x0, (base + ADPLLJ_TENABLEDIV));

	/* Loda M, N dividers of ADPLL */
	__raw_writel(0x1, (base + ADPLLJ_TENABLE));
	__raw_writel(0x0, (base + ADPLLJ_TENABLE));

	/* configure CLKDCOLDOEN,CLKOUTLDOEN,CLKOUT Enable BITS */
	read_clkctrl = __raw_readl(base + ADPLLJ_CLKCTRL) & 0xdfe5ffff;
	if (MODENA_PLL_BASE != base)
		__raw_writel((read_clkctrl | ADPLLJ_CLKCRTL_CLKDCO),
						base + ADPLLJ_CLKCTRL);

	/* Enable TINTZ and disable IDLE(PLL in Active & Locked Mode */
	read_clkctrl = __raw_readl(base + ADPLLJ_CLKCTRL) & 0xff7fffff;
	__raw_writel((read_clkctrl | 0x1), base + ADPLLJ_CLKCTRL);

	/* Wait for phase and freq lock */
	while ((__raw_readl(base + ADPLLJ_STATUS) & 0x600) != 0x600);

}

static void PLL_CLKOUT_ENABLE (u32 base)
{
    __raw_writel(__raw_readl((base + CLKCTRL)) | 0x00100000,(base + CLKCTRL));
	while (( (__raw_readl(base + STATUS)) & 0x00000020) != 0x00000020);
    __raw_writel(__raw_readl(base + CLKCTRL)| 0x00000001,(base + CLKCTRL));
}

/*
 * Enable the clks & power for perifs (TIMER1, UART0,...)
 */
void per_clocks_enable(void)
{
	u32 temp;

	__raw_writel(0x2, CM_ALWON_L3_SLOW_CLKSTCTRL);

#if 0
	/* TIMER 1 */
	__raw_writel(0x2, CM_ALWON_TIMER_1_CLKCTRL);
#endif
	/* Selects OSC0 (20MHz) for DMTIMER1 */
	temp = __raw_readl(DMTIMER_CLKSRC);
	temp &= ~(0x7 << 3);
	temp |= (0x4 << 3);
	__raw_writel(temp, DMTIMER_CLKSRC);

#if 0
	while(((__raw_readl(CM_ALWON_L3_SLOW_CLKSTCTRL) & (0x80000<<1)) >> (19+1)) != 1);
	while(((__raw_readl(CM_ALWON_TIMER_1_CLKCTRL) & 0x30000)>>16) !=0);
#endif
	__raw_writel(0x2,(DM_TIMER1_BASE + 0x54));
	while(__raw_readl(DM_TIMER1_BASE + 0x10) & 1);

	__raw_writel(0x1,(DM_TIMER1_BASE + 0x38));

	/* UARTs */
	__raw_writel(0x2, CM_ALWON_UART_0_CLKCTRL);
	while(__raw_readl(CM_ALWON_UART_0_CLKCTRL) != 0x2);

	__raw_writel(0x2, CM_ALWON_UART_1_CLKCTRL);
	while(__raw_readl(CM_ALWON_UART_1_CLKCTRL) != 0x2);

	while((__raw_readl(CM_ALWON_L3_SLOW_CLKSTCTRL) & 0x2100) != 0x2100);

	/* GPIO0 */
	__raw_writel(0x2, CM_ALWON_GPIO_0_CLKCTRL);
	while(__raw_readl(CM_ALWON_GPIO_0_CLKCTRL) != 0x2);
	__raw_writel(0x102, CM_ALWON_GPIO_1_CLKCTRL);
	while(__raw_readl(CM_ALWON_GPIO_1_CLKCTRL) != 0x102);

	/* I2C0 and I2C2 */
	__raw_writel(0x2, CM_ALWON_I2C_0_CLKCTRL);
	while(__raw_readl(CM_ALWON_I2C_0_CLKCTRL) != 0x2);

	/* I2C1 and I2C3 */
	__raw_writel(0x2, CM_ALWON_I2C_1_CLKCTRL);
	while(__raw_readl(CM_ALWON_I2C_1_CLKCTRL) != 0x2);

#ifdef SD_BOOT_ENABLE_MMC1
	/* HSMMC */
	__raw_writel(0x2, CM_ALWON_HSMMC_CLKCTRL);
	while(__raw_readl(CM_ALWON_HSMMC_CLKCTRL) != 0x2);
#endif
	/*
	 * McASP2
	 * select mcasp2 clk from sys_clk_22 (OSC 0)
	 * so that audio clk (sys_clk_20) can be used for RMII
	 * ToDo :
	 * This can be removed once kernel exports set_parent()
	 */
	__raw_writel(0x2, CM_AUDIOCLK_MCASP1_CLKSEL);
	while (__raw_readl(CM_AUDIOCLK_MCASP1_CLKSEL) != 0x2)
		;

	/* WDT */
	/* For WDT to be functional, it needs to be first stopped by writing
	 * the pattern 0xAAAA followed by 0x5555 in the WDT start/stop register.
	 * After that a write-once register in Control module needs to be
	 * configured to unfreeze the timer.
	 * Note: It is important to stop the watchdog before unfreezing it
	*/
	__raw_writel(0xAAAA, WDT_WSPR);
	while (__raw_readl(WDT_WWPS) != 0x0);
	__raw_writel(0x5555, WDT_WSPR);
	while (__raw_readl(WDT_WWPS) != 0x0);

	/* Unfreeze WDT */
	__raw_writel(0x13, WDT_UNFREEZE);

	/* LBP-380: temp allow SPI always on */
	__raw_writel(0x2, CM_ALWON_SPI_CLKCTRL);
	while (__raw_readl(CM_ALWON_SPI_CLKCTRL) != 0x2);
}

/*
 * inits clocks for PRCM as defined in clocks.h
 */

static void PLL_Bypass (u32 base)
{
	__raw_writel(__raw_readl((base + CLKCTRL))|0x00800000, (base + CLKCTRL));
	while (( (__raw_readl(base + STATUS)) & 0x00000101) != 0x00000101);
	__raw_writel(__raw_readl(base + CLKCTRL) & 0xfffffffe, (base + CLKCTRL));
}

static void PLL_CLKOUT_DISABLE (u32 base)
{
	__raw_writel(__raw_readl((base + CLKCTRL)) & 0xffefffff, (base + CLKCTRL));
	while (( (__raw_readl(base + STATUS)) & 0x00000020) != 0x00000000);
	__raw_writel(__raw_readl(base + CLKCTRL) & 0xfffffffe, (base + CLKCTRL));
	__raw_writel(0x7, (base + PWRCTRL));
}

/*
 * inits clocks for PRCM as defined in clocks.h
 */
void prcm_init(u32 in_ddr)
{
	u32 regVal;

	/* Enable the control module */
	__raw_writel(0x2, CM_ALWON_CONTROL_CLKCTRL);

#ifdef CONFIG_SETUP_PLL
	/* Setup the various plls */
	audio_pll_config();

	modena_pll_config(MODENA_FREQ_NORMAL);
	l3_pll_config();
	ddr_pll_config();
	iva_pll_config();
	iss_pll_config();

	usb_pll_config();

	__raw_writel(0x1,SECSS_CLK_SRC);


	/*  With clk freqs setup to desired values,
	 *  enable the required peripherals
	 */
	per_clocks_enable();
#endif

	/* Unlock MMRs. Refer to documentation for the values */
	__raw_writel(0x6F361E05,CTRL_MMR_LOCK4);

	regVal = __raw_readl(CTRL_PWR_CTL1);
	regVal |= 0x02; /* disable secss */
	__raw_writel(regVal,CTRL_PWR_CTL1);

	__raw_writel(0x143F832C,CTRL_MMR_LOCK4); /* Lock MMRs */

	/* Wakeup ISP clock domain */
	__raw_writel(0x2, PRCM_BASE + CM_ISP_CLKSTCTRL);

	/* Clear reset for ISP and enable clocks */
	regVal == __raw_readl(PRCM_BASE + RM_ISP_RSTCTRL);
	regVal &= ~ISP_RST_MASK;
	__raw_writel(regVal, PRCM_BASE + RM_ISP_RSTCTRL);

	regVal == __raw_readl(PRCM_BASE + CM_ISP_ISP_CLKCTRL);
	regVal |= ISP_CLKCTRL_MODULEMODE_EN;
	__raw_writel(regVal, PRCM_BASE + CM_ISP_ISP_CLKCTRL);
}

void disable_unused_clocks(void)
{
	/* disabling unused HDVPSS video clocks */
	PLL_Bypass(DSS_PLL_BASE);
	PLL_Bypass(VIDEO_0_PLL_BASE);
	PLL_Bypass(VIDEO_1_PLL_BASE);
	PLL_CLKOUT_DISABLE(DSS_PLL_BASE);
	PLL_CLKOUT_DISABLE(VIDEO_0_PLL_BASE);
	PLL_CLKOUT_DISABLE(VIDEO_1_PLL_BASE);
	PLL_CLKOUT_DISABLE(HDMI_PLL_BASE);

	/* explicitly gating clocks for unused blocks */
	__raw_writel(0x0, CM_ALWON_UART_2_CLKCTRL);
	/* LBP-380: temp allow SPI always on */
	// __raw_writel(0x0, CM_ALWON_SPI_CLKCTRL);
	__raw_writel(0x0, CM_ALWON_HSMMC0_CLKCTRL);
#ifndef SD_BOOT_ENABLE_MMC1
	__raw_writel(0x0, CM_ALWON_HSMMC_CLKCTRL);
#endif

	/* keeping commented code for future reference */
	/* power down internal ocmc0 */
	/*__raw_writel(0x3, 0x48140654); */

	/* Smart-reflex sensors disabled */
	__raw_writel(0x0, 0x481406a0);
	/* A8 Hardware debug select disable */
	__raw_writel(0x0, 0x481406a4);

	/* unused auxosc register disabled */
	__raw_writel(0x8, 0x4814046c);
}

/*
 * board specific muxing of pins
 */
void set_muxconf_regs(void)
{
	u32 i, add, val;
	u32 pad_conf[N_PINS] = {
#include "mux.h"
	};

	for (i = 0; i < N_PINS; i++)
	{
		add = PIN_CTRL_BASE + (i * 4);
		val = __raw_readl(add);
		if(pad_conf[i]) { /* check for mode-0 */
			val &= 0xFFFCFF00;
			val |= pad_conf[i];
			val |= (1 << 18); /* ensure bit-18 is always 1 */
		} else {
			val &= (1 << 19); /* use the reset value */
			val |= (1 << 18); /* pull down, and mode unused */
		}
		__raw_writel(val, add);
	}

}

/*
 * gpio init
 */
void gpio_init(void)
{
	int i;
	u32 gpio;

	/* Set to low to avoid glitch on VDD_GYRO. Will be enabled in kernel */
	ti81xx_request_gpio(GPIO_SENSOR_POWER_EN);
	ti81xx_set_gpio_dataout(GPIO_SENSOR_POWER_EN, 0);
	ti81xx_set_gpio_direction(GPIO_SENSOR_POWER_EN, 0);

	/* Set to low to avoid reset. PS: This is high active */
	ti81xx_request_gpio(GPIO_GPS_RESET);
	ti81xx_set_gpio_direction(GPIO_GPS_RESET, 0);
	ti81xx_set_gpio_dataout(GPIO_GPS_RESET, 0);

	/* Set to low to avoid on. */
	ti81xx_request_gpio(GPIO_GPS_ON_OFF);
	ti81xx_set_gpio_direction(GPIO_GPS_ON_OFF, 0);
	ti81xx_set_gpio_dataout(GPIO_GPS_ON_OFF, 0);

	/* Set to input */
	ti81xx_request_gpio(GPIO_GPS_WAKE);
	ti81xx_set_gpio_direction(GPIO_GPS_WAKE, 1);

	/**Setting the debounce value for GPIO_PWR_KEY in u-boot
	  *because it requires some time before it stablizes
	  *and reports the correct the gpio state **/

	ti81xx_request_gpio(GPIO_PWR_KEY);
	ti81xx_set_gpio_direction(GPIO_PWR_KEY, 1);
	ti81xx_gpio_set_debounce(GPIO_PWR_KEY, 200 * 1000);

#ifdef CONFIG_FASTBOOT
	ti81xx_request_gpio(GPIO_FB_SW);
	ti81xx_set_gpio_direction(GPIO_FB_SW, 1);

	/* set fastboot indicator led as output */
	ti81xx_request_gpio(GPIO_FB_INDICATOR);
	ti81xx_set_gpio_direction(GPIO_FB_INDICATOR, 0);
	ti81xx_set_gpio_dataout(GPIO_FB_INDICATOR, 0);
#endif

	/**
	 * Configure recovery combo keys as inputs
	 */
	for (i = 0; i < N_RECOVERY_COMBO_KEYS; i++) {
		gpio = recovery_key_combo[i].gpio;
		ti81xx_request_gpio(gpio);
		ti81xx_set_gpio_direction(gpio, 1);
	}

	/* Configure batlock switch as input */
	ti81xx_request_gpio(GPIO_BACK_CAP_DET);
	ti81xx_set_gpio_direction(GPIO_BACK_CAP_DET, 1);
	ti81xx_request_gpio(GPIO_KILL_POWER);
	ti81xx_set_gpio_direction(GPIO_KILL_POWER, 0); // Set direction as output
	ti81xx_set_gpio_dataout(GPIO_KILL_POWER, 0); // Set output low, keep device on

	ti81xx_request_gpio(GPIO_MCU_RESET);
	ti81xx_set_gpio_direction(GPIO_MCU_RESET, 0); // Set direction as output
	ti81xx_set_gpio_dataout(GPIO_MCU_RESET, 0); // Set output low, otherwise the MCU will be in reset mode, can not be tested

	ti81xx_request_gpio(GPIO_nCHGR_EN);
	ti81xx_set_gpio_direction(GPIO_nCHGR_EN, 0); // Set direction as output
	ti81xx_set_gpio_dataout(GPIO_nCHGR_EN, 0); // Set output low, it is best the charging IC is enabled

	ti81xx_request_gpio(GPIO_USB_OTG);
	ti81xx_set_gpio_direction(GPIO_USB_OTG, 0); // Set direction as output
	ti81xx_set_gpio_dataout(GPIO_USB_OTG, 1); // Set output high

	ti81xx_request_gpio(GPIO_BOOT_LOAD);
        ti81xx_set_gpio_direction(GPIO_BOOT_LOAD, 0); // Set direction as output
        ti81xx_set_gpio_dataout(GPIO_BOOT_LOAD, 1); // Set output high
}

void unlock_pll_control_mmr()
{
	/* ??? */
	__raw_writel(0x1EDA4C3D, 0x481C5040);
	__raw_writel(0x2FF1AC2B, 0x48140060);
	__raw_writel(0xF757FDC0, 0x48140064);
	__raw_writel(0xE2BC3A6D, 0x48140068);
	__raw_writel(0x1EBF131D, 0x4814006c);
	__raw_writel(0x6F361E05, 0x48140070);

}

static void unlock_rtc(void)
{
	/* Unlock the rtc's registers */
	writel(0x83e70b13, (DM385_RTC_BASE + 0x6C));
	writel(0x95a4f1e0, (DM385_RTC_BASE + 0x70));
}

/*
 * early system init of muxing and clocks.
 */
void s_init(u32 in_ddr)
{
	u32 board_id;
	/*
	 * Disable Write Allocate on miss to avoid starvation of other masters
	 * (than A8).
	 *
	 * Ref DM385 Erratum: TODO
	 */
	l2_disable_wa();

	/* Can be removed as A8 comes up with L2 enabled */
	l2_cache_enable();
	unlock_pll_control_mmr();
	/* Setup the PLLs and the clocks for the peripherals */
	prcm_init(in_ddr);

	/* disabling un-used clocks for power saving */
	disable_unused_clocks();
	unlock_rtc();

	board_id = board_identify();

#ifdef CONFIG_DM385_CONFIG_DDR
	if (!in_ddr)
		config_dm385_ddr(board_id);	/* Do DDR settings */
#endif
}

/*
 * Reset the board
 */
void reset_cpu(ulong addr)
{
	/* Set the soft reboot magic number to flipflop.2,
	 * to ensure the reset reason is set as warm. */
	addr = __raw_writel(FLIPFLOP_SOFTBOOT_MAGIC, RTC_SCRATCHPAD2_REG);

	addr = __raw_readl(PRM_DEVICE_RSTCTRL);
	addr &= ~BIT(1);
	addr |= BIT(1);
	__raw_writel(addr, PRM_DEVICE_RSTCTRL);
}


#ifdef CONFIG_NAND_TI81XX
/******************************************************************************
 * Command to switch between NAND HW and SW ecc
 *****************************************************************************/
extern void ti81xx_nand_switch_ecc(nand_ecc_modes_t hardware, int32_t mode);
int do_switch_ecc(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int type = 0;
	if (argc < 2)
		goto usage;

	if (strncmp(argv[1], "hw", 2) == 0) {
		if (argc == 3)
			type = simple_strtoul(argv[2], NULL, 10);
		ti81xx_nand_switch_ecc(NAND_ECC_HW, type);
	} else if (strncmp(argv[1], "sw", 2) == 0) {
		ti81xx_nand_switch_ecc(NAND_ECC_SOFT, 0);
	} else
		goto usage;

	return 0;

usage:
	printf("Usage: nandecc %s\n", cmdtp->usage);
	return 1;
}

U_BOOT_CMD(
	nandecc, 3, 1,	do_switch_ecc,
	"Switch NAND ECC calculation algorithm b/w hardware and software",
	"[sw|hw <hw_type>] \n"
	"   [sw|hw]- Switch b/w hardware(hw) & software(sw) ecc algorithm\n"
	"   hw_type- 0 for Hamming code\n"
	"            1 for bch4\n"
	"            2 for bch8\n"
	"            3 for bch16\n"
);

#endif /* CONFIG_NAND_TI81XX */

#ifdef CONFIG_GENERIC_MMC
int board_mmc_init(bd_t *bis)
{
	omap_mmc_init(0);
	omap_mmc_init(1);
	return 0;
}
#endif

#ifndef CONFIG_SYS_DCACHE_OFF
void enable_caches(void)
{
	/* Enable D-cache. I-cache is enabled in start.S */
	dcache_enable();
}
#endif

#ifdef CONFIG_FASTBOOT
int fastboot_init(struct cmd_fastboot_interface *interface)
{
	/* Enable the fastboot indicator LED */
	ti81xx_set_gpio_dataout(GPIO_FB_INDICATOR, 1);

	/* Clear flip flop that is used to enter fastboot */
	__raw_writel(0, RTC_SCRATCHPAD1_REG);
	return 0;
}

/* Cleans up the board specific fastboot */
void fastboot_shutdown(void)
{
	/* Disable the fastboot indicator LED */
	ti81xx_set_gpio_dataout(GPIO_FB_INDICATOR, 0);
}

#endif /* CONFIG_FASTBOOT */

void arch_preboot_os(void)
{
	/* Ensure we ensure SW/HW resets as warm reset. Should be cleared when powering off */
	__raw_writel(FLIPFLOP_SOFTBOOT_MAGIC, RTC_SCRATCHPAD2_REG);

	/* Ensure we boot into recovery if boot fails, to be cleared once booted */
	__raw_writel(FLIPFLOP_RECOVERY_BOOT_MAGIC, RTC_SCRATCHPAD0_REG);
}
