/*
 * (C) Copyright 2009 TomTom BV <http://www.tomtom.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * Implementation of the flip-flop. This can store 1 bit of information
 * across a (watchdog) reset.
 */

/* #define DEBUG */

#include <common.h>
#include <asm/io.h>
#include <asm/hardware.h>
#include <flipflop.h>

#ifdef CONFIG_SW_FLIPFLOP

#define FLIP_FLOP_LOCATION	(DM385_RTC_BASE + 0x68)
#define FLIP_FLOP_BIT 		0

void flipflop_set(int state)
{
	unsigned long value= readl(FLIP_FLOP_LOCATION);

	if (state)
		value |= (1 << FLIP_FLOP_BIT);
	else
		value &= ~(1 << FLIP_FLOP_BIT);

	writel(value, FLIP_FLOP_LOCATION);
}

int flipflop_get(void)
{
	const unsigned long value = readl(FLIP_FLOP_LOCATION);

	return (value & (1 << FLIP_FLOP_BIT));
}

#else /* CONFIG_SW_FLIPFLOP */

static int is_setup = 0;

static void flipflop_setup(void)
{
	u32 reg;
	if (is_setup)
		return;

	/* Set GP2_2 (clock) output low */
	reg = readl((GPIO2_BASE + GPIO_DATAOUT)) & (~(1<<2));
	writel(reg, (GPIO2_BASE + GPIO_DATAOUT));
	/* Set GP2_2 as output (clock) 
	   Set GP3_21 as input (Q output) */
	reg = readl((GPIO2_BASE + GPIO_OE)) & (~(1<<2));
	writel(reg, (GPIO2_BASE + GPIO_OE));

	is_setup = 1;
}

void flipflop_set(int v)
{
	u32 reg;
	int i;

	flipflop_setup();

	for(i=0; i<10; i++) {
		if (!flipflop_get() == !v)
			break;
		/* Pulse GPK2 clock output */
		reg = readl((GPIO2_BASE + GPIO_DATAOUT)) & (~(1<<2));
		writel(reg | (1<<2), (GPIO2_BASE + GPIO_DATAOUT));
		udelay(10);
		writel(reg, (GPIO2_BASE + GPIO_DATAOUT));
		udelay(10);
	}

	debug("Flipflop set to [%d] / got [%d]\n", v, flipflop_get());
}

int flipflop_get(void)
{
	u32 reg;

	flipflop_setup();

	/* Read GPM1 (Q output) */
	reg = readl((GPIO3_BASE + GPIO_DATAIN)) & (1<<21);

	debug("Flipflop get = [%d]\n", (!!reg));

	return (!!reg);
}

#endif /* CONFIG_SW_FLIPFLOP */

