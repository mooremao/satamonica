/*
 * bootcount.c - use flipflop as 1-bit boot counter
 *
 * Copyright (C) 2012 TomTom International B.V.
 * Author: Ard Biesheuvel <ard.biesheuvel@tomtom.com>
 *
 ************************************************************************
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 ************************************************************************
 */

/* #define DEBUG */

#include <common.h>
#include <flipflop.h>

void bootcount_store(ulong count)
{
	if (count)
		flipflop_set(1);
	else
		flipflop_set(0);
}

ulong bootcount_load(void)
{
	if (flipflop_get())
		return 1;

	return 0;
}
