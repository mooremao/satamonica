#ifndef __DDR_TIMINGS_H__
#define __DDR_TIMINGS_H__
#include <config.h>
/* pcb_id's */
typedef enum {
  SM_WS1_BOARD = 0,
  MAX_N_PCB_IDS
} pcb_id_t;


typedef struct ddr_pcb_timings {
	char *pcb_name;
	unsigned int ddr3_phy_rd_dqs_cs0_byte0;
	unsigned int ddr3_phy_rd_dqs_cs0_byte1;
	unsigned int ddr3_phy_rd_dqs_cs0_byte2;
	unsigned int ddr3_phy_rd_dqs_cs0_byte3;
	unsigned int ddr3_phy_rd_dqs_gate_cs0_byte0;
	unsigned int ddr3_phy_rd_dqs_gate_cs0_byte1;
	unsigned int ddr3_phy_rd_dqs_gate_cs0_byte2;
	unsigned int ddr3_phy_rd_dqs_gate_cs0_byte3;
	unsigned int ddr3_phy_wr_dqs_cs0_byte0;
	unsigned int ddr3_phy_wr_dqs_cs0_byte1;
	unsigned int ddr3_phy_wr_dqs_cs0_byte2;
	unsigned int ddr3_phy_wr_dqs_cs0_byte3;
	unsigned int ddr3_phy_wr_data_cs0_byte0;
	unsigned int ddr3_phy_wr_data_cs0_byte1;
	unsigned int ddr3_phy_wr_data_cs0_byte2;
	unsigned int ddr3_phy_wr_data_cs0_byte3;
} ddr_pcb_timing_t;

extern const ddr_pcb_timing_t dm38x_ddr_pcb_timings[MAX_N_PCB_IDS];

#define TO_SRAM_ADDR(X) ((ddr_pcb_timing_t *)(((u32)X)-TEXT_BASE+SRAM0_START))

#endif  /* __DDR_TIMINGS_H__ */

