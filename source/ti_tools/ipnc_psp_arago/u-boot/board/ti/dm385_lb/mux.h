/*
 * Copyright (C) 2009, Texas Instruments, Incorporated
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/*
 * Usage help for various pull/mux configs
 * 	BITS(17, mode)	- Pull-up enabled for the selected mode
 * 	BITS(16, mode)	- Pull disabled for the selected mode
 * 	BIT(mode)	- Pull-down enabled for the selected mode
 * 	0		- Safe mode (mode-0) selected, pull disabled
 */

/* 1-4 */	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),
/* -08 */	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),
/* -12 */	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),
		/* 14 mca0_mclk, 15-led1, 16-board ID */
#ifdef CONFIG_FASTBOOT
/* -16 */	BITS(17, 0),	BITS(16, 2),	BITS(16, 7),	BITS(16, 7),
#else
/* -16 */	BITS(17, 0),	BITS(16, 2),	BITS(16, 6),	BITS(17, 7),
#endif
/* -20 */	0,		0,		BITS(16, 0),	BITS(16, 0),
/* -24 */	BITS(17, 0), 	0,		0,		0,
/* -28 */	0,		0,		0,		0,
/* -32 */	0,		0,		0,		0,
/* -36 */	BITS(16, 0),	BITS(16, 0),	BITS(17, 0),	BITS(16, 0),
/* -40 */	0,		0,		0,		0,
		/* 41-board ID */
/* -44 */	0,		BITS(16, 7),	0,		0,
/* -48 */	0,		0,		0,		0,
/* -52 */	0,		0,		0,		0,
/* -56 */	0,		0,		0,		0,
/* -60 */	0,		0,		0,		0,
/* -64 */	0,		0,		0,		0,
/* -68 */	0,		0,		0,		0,
/* -72 */	0,		BITS(17, 0),	BITS(16, 0),	0,
		/* 75 - display_5v_en, 76 uart1_txd for BT */
/* -76 */	0,		0,		BITS(16, 7), 	BITS(16, 2),
		/* 77 - uart1_rxd */
/* -80 */	BITS(17, 2), 	BITS(16, 0), 	BITS(16, 0), 	BITS(17, 1),
/* -84 */	0,		0,		0,		0,
/* -88 */	BITS(17, 0),	BITS(16, 0),	BITS(17, 0),	BITS(16, 0),
		/* gpmc_d0-d15 */
/* -92 */	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),
/* -96 */	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),
/* -100 */	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),
/* -104 */	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),
		/* 108 - core2_vsel1 */
/* -108 */	0,		0,		0,		BITS(16, 7),
		/* 109-vsel0, 110-vsel1, 111-image_pwr_en, 112-backlight_en */
/* -112 */	BITS(16, 7),	BITS(16, 7),	BITS(16, 7),	BITS(16, 6),
		/* 115-display_extcom, 116-display_on */
/* -116 */	0,		0,		BITS(16, 6),	BIT(7),
/* -120 */	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),	BITS(17, 0),
		/* 123 - core2_vsel0 */
/* -124 */	BITS(17, 0),	BITS(16, 0),	BITS(16, 7),	0,
		/* 125 - spi2_cs, 126 - sd2_cmd */
/* -128 */	BIT(2), 	BITS(17, 1), 	BITS(16, 4), 	BITS(16, 0),
/* -132 */	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),	0,
		/* 135-i2c2_sda, 136-i2c2_scl */
/* -136 */	BITS(17, 0),	0,		BITS(16, 6),	BITS(16, 6),
		/* 139-usb_otg, 140 - gps_data_ready */
/* -140 */	0,		0,		BITS(16, 7),	BIT(7),
		/* 141-gps_tm_1pps, 142-sw_start, 143-sw_up, 144-sw_down */
/* -144 */	BITS(17, 7), 	BITS(16, 7), 	BITS(17, 7), 	BITS(17, 7),
		/* 145-sw_stop */
/* -148 */	BITS(17, 7), 	0,		0,		0,
		/* 151-wlan_irq_1v8, 152-boot_load */
/* -152 */	0,		0,		BITS(16, 7),	BITS(16, 7),
		/* 153-accel_int2, 154-accel_int, 155-gyro_int, 156-host_int_mcu */
/* -156 */	BITS(16, 7), 	BITS(16, 7), 	BITS(17, 7), 	BITS(16, 7),
		/* 157-vbus_on 158-host_int_chgr */
/* -160 */	BITS(16, 7), 	BITS(16, 7),	0,		0,
		/* 163-back_cap_det */
/* -164 */	0,		0,		BITS(16, 7),	0,
/* -168 */	0,		0,		0,		0,
		/* 170-nchgr_en, 171-chgr_stat */
/* -172 */	0,		BITS(16, 7),	BITS(16, 7),	0,
/* -176 */	0,		0,		0,		0,
		/* 179-tim7_io */
/* -180 */	0,		0,		BITS(16, 6),	0,
/* -184 */	0,		0,		0,		0,
/* -188 */	0,		0,		0,		0,
/* -192 */	0,		0,		0,		0,
/* -196 */	0,		0,		0,		0,
/* -200 */	0,		0,		0,		0,
/* -204 */	0,		0,		0,		0,
		/* 205-gps_wakeup, 206-gps_on_off, 208-aud_rstn*/
/* -208 */	BITS(16, 7),	BITS(16, 7),	0,		BITS(16, 7),
		/* 209-wifi_pwen, 210-bt_pwen, 211-gps_reset */
/* -212 */	BITS(16, 7), 	BITS(16, 7), 	BITS(16, 7), 	0,
		/* 213-i2c3_scl, 214-i2c3_sda, 215-image_reset, 216-image_clk_en */
/* -216 */	BITS(16, 5), 	BITS(16, 5), 	BITS(16, 7), 	0,
		/* 220-mcu_reset */
/* -220 */	0,		0,		0,		BITS(16, 7),
		/* 221-nand_wp */
/* -224 */	BITS(16, 7),	0,		0,		0,
/* -228 */	0,		0,		0,		0,
		/* 229-rfs_boot_q, 230-hdmi_hpd, 231-hdmi_cec */
/* -232 */	0,		BIT(4),		BITS(17, 4),	0,
		/* 235-pwr_down */
/* -236 */	0,		0,		BITS(16, 7),	0,
/* -240 */	0,		0,		0,		0,
/* -244 */	0,		0,		0,		0,
		/* 248-spi2_sclk */
/* -248 */	0,		0,		0,		BITS(16, 5),
		/* 249-spi2_miso, 250-spi2_mosi */
/* -252 */	0,		BITS(16, 5),	0,		0,
/* -256 */	0,		0,		0,		0,
		/* 257-uart1_cts, 258-uart1_rts, 259-32kHz_from_gps */
/* -260 */	BITS(17, 5),	BITS(16, 5),	BIT(0),		BITS(17, 0),
/* -264 */	BITS(17, 0),	BITS(16, 0),	BITS(16, 0),	BITS(16, 0),
/* -268 */	0,		0,		0,		0,
/* -270 */	0,		0,
