/*
 * ti81xx_nand_opt: optimized routine for NAND prefetch read
 *
 * Copyright (c) 2014   TomTom International B.V.
 *
 * Copyright (c) 2014   Renjith Thomas <renjith.thomas@pathpartnertech.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <config.h>
#include <version.h>

	.section	.text.ti81xx_nand_read_buf_prefetch_opt,"ax",%progbits
	.align	2
	.global ti81xx_nand_read_buf_prefetch_opt
	.type	ti81xx_nand_read_buf_prefetch_opt, %function
ti81xx_nand_read_buf_prefetch_opt:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	ip, #1342177280
	mov	r3, #1
#ifdef CONFIG_CORTEXA8_NEON
	stmfd	sp!, {lr}
#else
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
#endif
	stmfd	sp!, {r0-r3}
	mov	r0, #0
	bl	ti81xx_nand_prefetch_init
	ldmfd	sp!, {r0-r3}

	mov	r0, #134217728
	str	r2, [ip, #484]
	str	r3, [ip, #492]
	b	toread
waitmore:
	ldr	r3, [ip, #496]
	cmp	r2, #31
	ubfx	r3, r3, #24, #7
	cmp	r3, #31
	blt	waitmore
	cmp	r3, #64
	bne	read32
	mov	r3, r1

#ifdef CONFIG_CORTEXA8_NEON
	vldm	r0!, {d0-d7}
	vstm	r3!, {d0-d7}
#else
	ldmia	r0!, {r4-r11}
	stmia	r3!, {r4-r11}
	ldmia	r0!, {r4-r11}
	stmia	r3!, {r4-r11}
#endif

	add	r1, r1, #64
	sub	r2, r2, #64
	b	toread
read32:
	cmp	r3, #31
	bls	toread
	mov	r3, r1

#ifdef CONFIG_CORTEXA8_NEON
	vldm	r0!, {d0-d3}
	vstm	r3!, {d0-d3}
#else
	ldmia	r0!, {r4-r11}
	stmia	r3!, {r4-r11}
#endif
	add	r1, r1, #32
	sub	r2, r2, #32
toread:
	cmp	r2, #0
	bne	waitmore
#ifdef CONFIG_CORTEXA8_NEON
	ldmfd	sp!, {pc}
#else
	ldmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
#endif
	.align	2
	.size	ti81xx_nand_read_buf_prefetch_opt, .-ti81xx_nand_read_buf_prefetch_opt

	.align	2
	.global ti81xx_nand_write_buf_postwrite
	.type	ti81xx_nand_write_buf_postwrite, %function
ti81xx_nand_write_buf_postwrite:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r0, #1
	mov	r3, #0
	str	r1, [sp, #4]
	str	r3, [sp, #12]
	str	r2, [sp, #0]
	bl	ti81xx_nand_prefetch_init
	ldr	ip, [sp, #0]
	mov	r2, #1342177280
	mov	r3, #1
	ldr	r1, [sp, #4]
	str	ip, [r2, #484]
	str	r3, [r2, #492]
	b	towrite

waitagain:
	mov	r3, #1342177280
	ldr	r3, [r3, #496]
	str	r3, [sp, #12]
	ldr	r3, [sp, #12]
	ubfx	r3, r3, #24, #7
	str	r3, [sp, #12]
	ldr	r3, [sp, #12]
	cmp	r3, #63
	ble	waitagain
	ldr	r3, [sp, #12]
	cmp	r3, #64
	bne	write32
	mov	r3, #134217728

	ldmia  r1!, {r4-r11}
	stmia  r3!,{r4-r11}
	ldmia  r1!, {r4-r11}
	stmia  r3!,{r4-r11}

	sub	ip, ip, #64
	b	towrite
write32:
	ldr	r3, [sp, #12]
	cmp	r3, #31
	ble	towrite
	mov	r3, #134217728

	ldmia  r1!, {r4-r11}
	stmia  r3!,{r4-r11}

	sub	ip, ip, #32
towrite:
	cmp	ip, #0
	bne	waitagain

wait4finish:
	mov	r2, #1342177280
	ldr	r3, [r2, #496]
	str	r3, [sp, #8]
	ldr	r3, [sp, #8]
	ubfx	r3, r3, #24, #7
	str	r3, [sp, #8]
	ldr	r3, [sp, #8]
	cmp	r3, #64
	bne	wait4finish
	mov	r3, #0
	str	r3, [r2, #492]
	ldmfd	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
	.size	ti81xx_nand_write_buf_postwrite, .-ti81xx_nand_write_buf_postwrite

