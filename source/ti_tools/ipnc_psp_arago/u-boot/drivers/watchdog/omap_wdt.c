/*
 * omap_wdt.c
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Akshaya Bhat <akshaya.bhat@pathpartnertech.com>
 *
 * Based on:
 *
 * Watchdog driver for the TI OMAP 16xx & 24xx/34xx 32KHz (non-secure) watchdog
 *
 * Author: MontaVista Software, Inc.
 *	 <gdavis@mvista.com> or <source@mvista.com>
 */

#include <common.h>
#include <watchdog.h>
#include <asm/arch/hardware.h>
#include <asm/io.h>
#include <asm/processor.h>
#include <asm/arch/cpu.h>

/* Hardware timeout in seconds */
#define WDT_HW_TIMEOUT 30

static unsigned int wdt_trgr_value = 0;

void hw_watchdog_reset(void)
{
	/**
	 * As we don't wait for previous post to the 32kHz clock domain to
	 * be finished, we need to make sure we don't write a value that we
	 * already wrote to the WTGR register before as the register value
	 * (in the 32kHz domain) should change, otherwise is does not count
	 * as a watchdog reset. */
	wdt_trgr_value++;
	writel(wdt_trgr_value, WDT_WTGR);
}

static int omap_wdt_set_timeout(unsigned int timeout)
{
	u32 pre_margin = GET_WLDR_VAL(timeout);

	/* just count up at 32 KHz */
	while (readl(WDT_WWPS) & WDT_WWPS_PEND_WLDR)
		;

	writel(pre_margin, WDT_WLDR);
	while (readl(WDT_WWPS) & WDT_WWPS_PEND_WLDR)
		;

	return 0;
}

void hw_watchdog_init(void)
{
	/* initialize prescaler */
	while (readl(WDT_WWPS) & WDT_WWPS_PEND_WCLR)
		;

	writel(WDT_WCLR_PRE | (PTV << WDT_WCLR_PTV_OFF), WDT_WCLR);
	while (readl(WDT_WWPS) & WDT_WWPS_PEND_WCLR)
		;

	omap_wdt_set_timeout(WDT_HW_TIMEOUT);

	/* Sequence to enable the watchdog */
	writel(0xBBBB, WDT_WSPR);
	while ((readl(WDT_WWPS)) & WDT_WWPS_PEND_WSPR)
		;

	writel(0x4444, WDT_WSPR);
	while ((readl(WDT_WWPS)) & WDT_WWPS_PEND_WSPR)
		;

	hw_watchdog_reset();
}

void hw_watchdog_disable(void)
{
	/*
	 * Disable watchdog
	 */
	writel(0xAAAA, WDT_WSPR);
	while (readl(WDT_WWPS) != 0x0)
		;
	writel(0x5555, WDT_WSPR);
	while (readl(WDT_WWPS) != 0x0)
		;
}

static ulong gettbl(void)
{
	ulong r;

	asm("mftbl %0" : "=r" (r));

	return r;
}

int watchdog_post_test()
{
		/* 10-second delay */
		int ints = disable_interrupts();
		ulong base = gettbl();
		ulong clk = get_tbclk();

		while ((gettbl() - base) / 10 < clk)
			;

		if (ints)
			enable_interrupts();

		/*
		 * If we have reached this point, the watchdog timer
		 * does not work
		 */
		return -1;
}

