/*
 * TI's AM335x platform specific USB wrapper functions.
 *
 * Copyright (c) 2009 Texas Instruments
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * Author: Prashantha Krishna x0156546@ti.com
 */

#ifndef __TI81XX_USB_H__
#define __TI81XX_USB_H__

#include "musb_core.h"

/* Base address of USB subsystem */
#define USBSS_BASE 0x47400000

/* Base address of musb wrapper */
#define MUSB_BASE 0x47401000

/* Base address of musb core */
#define MENTOR_BASE (MUSB_BASE+0x400)

#define CM_USB_CLKCTRL 		0x48180558
#define TI81XX_USBCTRL0		0x48140620
#define TI81XX_USBSTAT0         0x48140624

/*
 * TI81XX platform USB wrapper register overlay. Note: Only the required
 * registers are included in this structure. It can be expanded as required.
 */
struct ti81xx_usb_regs {
	u32	revision;
	u32	reserved[0x4];
	u32	control;
	u32	status;
	u32	reserved0[0x1];
	u32	irq_mergestatus;
	u32	irq_eoi;
	u32	irq_statusraw0;
	u32	irq_statusraw1;
	u32	irq_status0;
	u32	irq_status1;
	u32	irq_enableset0;
	u32	irq_enableset1;
	u32	irq_enableclr0;
	u32	irq_enableclr1;
	u32	reserved1[0x5];
	u32	tx_mode;
	u32	rx_mode;
	u32	reserved2[0x2];
	u32	rndis_epnsize[0xF];
	u32	reserved3[0x5];
	u32	auto_req;
	u32	srp_fixtime;
	u32	teardown;
	u32	reserved4[0x1];
	u32	threshold_xdmaidle;
	u32	phy_utmi;
	u32	mgc_utmiloopback;
	u32	mode;
	u32	reserved5[0xC5];
	u32	core_fifo1[0x1A];
	u32	core_hwvers;
	u32	core_fifo2[0x4B];
};



/* USBSS EOI bits */
#define USBSS_EOI               0

/* DPLL PER clock enable */
#define DPLL_PER_CLKEN          (0x100)

/* Control register bits */
#define USB_SOFT_RESET_MASK     1

/* Timeout for MUSB module */
#define MUSB_TIMEOUT 0x3FFFFFF

/* TI814X USB interface and PHY clock control bits */
#define USB_ICLK_CTRL           (0x2)
#define USB_PHYCLK_CTRL         (0x300)

/* TI814X USB IRQ set bit */
#define USB_IRQENABLE_SET0	(0xFFFE0000)
#define USB_IRQENABLE_SET1	(0x000003FF)

#define USBPHY_CM_PWRDN         (1 << 0)
#define USBPHY_OTG_PWRDN        (1 << 1)

/* TI814X PHY controls bits */
#define TI814X_USBPHY_CM_PWRDN          (1 << 0)
#define TI814X_USBPHY_OTG_PWRDN         (1 << 1)
#define TI814X_USBPHY_CHGDET_DIS        (1 << 2)
#define TI814X_USBPHY_CHGDET_RSTRT      (1 << 3)
#define TI814X_USBPHY_SRCONDM           (1 << 4)
#define TI814X_USBPHY_SINKONDP          (1 << 5)
#define TI814X_USBPHY_CHGISINK_EN       (1 << 6)
#define TI814X_USBPHY_CHGVSRC_EN        (1 << 7)
#define TI814X_USBPHY_DMPULLUP          (1 << 8)
#define TI814X_USBPHY_DPPULLUP          (1 << 9)
#define TI814X_USBPHY_CDET_EXTCTL       (1 << 10)
#define TI814X_USBPHY_GPIO_MODE         (1 << 12)
#define TI814X_USBPHY_DPOPBUFCTL        (1 << 13)
#define TI814X_USBPHY_DMOPBUFCTL        (1 << 14)
#define TI814X_USBPHY_DPINPUT           (1 << 15)
#define TI814X_USBPHY_DMINPUT           (1 << 16)
#define TI814X_USBPHY_DPGPIO_PD         (1 << 17)
#define TI814X_USBPHY_DMGPIO_PD         (1 << 18)
#define TI814X_USBPHY_OTGVDET_EN        (1 << 19)
#define TI814X_USBPHY_OTGSESSEND_EN     (1 << 20)
#define TI814X_USBPHY_DATA_POLARITY     (1 << 23)
#endif	/* __TI81XX_USB_H__ */
