/*
 * TI81XX platform specific USB wrapper functions.
 *
 * Copyright (c) 2009 Texas Instruments
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * Author: Prashantha Krishna x0156546@ti.com
 */

#include <common.h>
#include "ti81xx.h"

/* MUSB platform configuration */
struct musb_config musb_cfg = {
	(struct	musb_regs *)MENTOR_BASE,
	MUSB_TIMEOUT,
	0,
	0
};

/* MUSB module register overlay */
struct ti81xx_usb_regs *regs;

/*
 * Enable the USB phy
 */
static u8 phy_on(void)
{
	u32 usbphycfg;
	
	usbphycfg = readl(TI81XX_USBCTRL0);
	usbphycfg &= ~(TI814X_USBPHY_CM_PWRDN
                                | TI814X_USBPHY_OTG_PWRDN
                                | TI814X_USBPHY_DMPULLUP
                                | TI814X_USBPHY_DPPULLUP
                                | TI814X_USBPHY_DPINPUT
                                | TI814X_USBPHY_DMINPUT
                                | TI814X_USBPHY_DATA_POLARITY);
        usbphycfg |= (TI814X_USBPHY_SRCONDM
                                | TI814X_USBPHY_SINKONDP
                                | TI814X_USBPHY_CHGISINK_EN
                                | TI814X_USBPHY_CHGVSRC_EN
                                | TI814X_USBPHY_CDET_EXTCTL
                                | TI814X_USBPHY_DPOPBUFCTL
                                | TI814X_USBPHY_DMOPBUFCTL
                                | TI814X_USBPHY_DPGPIO_PD
                                | TI814X_USBPHY_DMGPIO_PD
                                | TI814X_USBPHY_OTGVDET_EN
                                | TI814X_USBPHY_OTGSESSEND_EN);

	writel(usbphycfg, TI81XX_USBCTRL0);
	udelay(5000);

	return 1;
}

/*
 * Disable the USB phy
 */
static void phy_off(void)
{
	u32 usbphycfg;

	/*
	 * TODO-Power down the on-chip PHY.
	 */
}

/*
 * This function performs platform specific initialization for usb0.
 */
int musb_platform_init(void)
{

	u32 sw_reset, cmen;
	cmen = readl(CM_USB_CLKCTRL);
	cmen |= (1 << 1);
	writel(cmen, CM_USB_CLKCTRL);
	udelay(5000);

	sw_reset = readl((USBSS_BASE + 0x10));
	sw_reset |= (1 << 0);
	writel(sw_reset, (USBSS_BASE + 0x10));
	udelay(5000);
	
	sw_reset = readl((MUSB_BASE + 0x14));
	sw_reset |= (1 << 0);
	writel(sw_reset, (MUSB_BASE + 0x14));
	while ((readl(MUSB_BASE) & 0x1));
	
	if (!phy_on())	{
		printf("phy_on failed");
		return -1;
	}

	regs = (struct ti81xx_usb_regs *)MUSB_BASE;

	/* Enable the IRQ on USB0 wrapper register instead of MUSB register */
	writel(USB_IRQENABLE_SET0, &regs->irq_enableset0);
	writel(USB_IRQENABLE_SET1, &regs->irq_enableset1);

	return 0;
}

/*
 * This function performs platform specific deinitialization for usb0.
 */
void musb_platform_deinit(void)
{
	/* Turn of the phy */
	phy_off();
}
