#ifndef __FLIPFLOP_H__
#define __FLIPFLOP_H__

void flipflop_set(int v);
int flipflop_get(void);

#endif
