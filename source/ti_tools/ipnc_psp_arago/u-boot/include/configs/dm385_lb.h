/*
 * Copyright (C) 2011, Texas Instruments, Incorporated
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __CONFIG_DM385_LB_H
#define __CONFIG_DM385_LB_H

#include <mem_regions.h>
#include <asm/sizes.h>
/*
 *#define CONFIG_DM385_NO_RUNTIME_PG_DETECT
 */

/* select the DDR3 Freq and timing paramets */
#ifdef LOW_POWER_OPP100_MODE
//#define CONFIG_DM385_DDR3_400 /* Values supported 400,533 */
#define CONFIG_DM385_DDR3_397 /* Values supported 397,529 */
#else
//#define CONFIG_DM385_DDR3_533 /* Values supported 400,533 */
#define CONFIG_DM385_DDR3_529 /* Values supported 397,529 */
//#define CONFIG_DM385_DDR3_397 /* Values supported 397,529 */
#endif

/* Display CPU info */
#define CONFIG_DISPLAY_CPUINFO          1
/* In the 1st stage we have just 110K, so cut down wherever possible */
#ifdef CONFIG_DM385_MIN_CONFIG

#define CONFIG_CORTEXA8_NEON
#define CONFIG_CMD_MEMORY      /* for mtest */
#define CONFIG_SETUP_PLL
#define CONFIG_DM385_CONFIG_DDR
#define CONFIG_DM385_IPNC_DDR3
#define CONFIG_ENV_RANGE	(0x20000 * 5)
#define CONFIG_ENV_SIZE		(1024 * 8)
#define CONFIG_SYS_MALLOC_LEN	(1 << 20)
#define CONFIG_SYS_PROMPT		"DM385_LB#"
#define CONFIG_CMD_EDITENV
#define CONFIG_CMD_SAVEENV	/* saveenv */
#define CONFIG_CMD_RUN
#define CONFIG_SYS_LONGHELP
#define CONFIG_ENV_OVERWRITE
#define CONFIG_SERIAL_TAG              1
#define CONFIG_FACTORYDATA_TAG		/* Pass factory data address in ATAGs */
#define CONFIG_REVISION_TAG            1
/* enable passing of ATAGs  */
#define CONFIG_CMDLINE_TAG             1
#define CONFIG_SETUP_MEMORY_TAGS       1
/* Required for ramdisk support */
#define CONFIG_INITRD_TAG              1
#ifdef ZERO_BOOT_DELAY
#define CONFIG_BOOTDELAY                0
#else
#define CONFIG_BOOTDELAY                3
#endif
#ifdef QUIET_BOOT
#define CONFIG_KERNEL_ARG_CONSOLE "quiet"
#else
#define CONFIG_KERNEL_ARG_CONSOLE "earlyprintk"
#endif
#ifdef ROOTFS_READONLY
#define CONFIG_ROOTFS_PERM "ro "
#else
#define CONFIG_ROOTFS_PERM "rw "
#endif
#define CONFIG_MUSB_UDC
#define CONFIG_FASTBOOT			/* Android fast boot */
#define CONFIG_CMD_FD			/* factory data load command */
#define CONFIG_DM385LB_FD_DATA_SIZE	4096
#define CONFIG_USB_MUSB_TI81XX		1

#define CONFIG_FDT_FB			1
#define CONFIG_HW_WATCHDOG		1
#define CONFIG_OMAP_WATCHDOG		1

#if defined(CONFIG_SPI_BOOT)           /* Autoload the 2nd stage from SPI */
#define CONFIG_SPI                    1
# if defined(CONFIG_TI81XX_PCIE_BOOT)
# define CONFIG_CMDLINE_TAG		1	/* enable passing of ATAGs  */
# define CONFIG_SETUP_MEMORY_TAGS	1
# define CONFIG_INITRD_TAG		1	/* for ramdisk support */
# define CONFIG_CMD_SOURCE
# define CONFIG_EXTRA_ENV_SETTINGS \
	"verify=yes\0" \
	"bootcmd=source 0x80400000\0" \
	""
/* user can override default size configuration here.
 * it will only come in effect if TI81xx_NO_PIN_GPMC
 * is defined in include/asm/arch/pcie.h
 */
#define CONFIG_BAR1_32  (0x1000000ULL)
#define CONFIG_BAR2_32  (0x800000ULL)
#define CONFIG_BAR3_32  (0xfffULL)
#define CONFIG_BAR4_32  (0x1001ULL)
#define CONFIG_REG2_64  (0x1000000ULL)
#define CONFIG_REG4_64  (0x2000000ULL)

# else
#  define CONFIG_EXTRA_ENV_SETTINGS \
        "verify=yes\0" \
        "bootcmd=sf probe 0; sf read 0x81000000 0x20000 0x40000; go 0x81000000\0" \

# endif
#elif defined(CONFIG_NAND_BOOT)                /* Autoload the 2nd stage from NAND */

#define CONFIG_NAND                   1
#define CONFIG_EXTRA_ENV_SETTINGS \
        "verify=no\0" \
	"normal_bootcmd=setenv bootargs ${normal_bootargs} lpj=${lpj} boot_reason=${boot_reason} systemd.unit=${deftarget}; nboot 0x81000000 0 0x300000; bootm\0" \
	"normal_bootargs=console=ttyO0,115200n8 rootwait=1 ubi.mtd=4,2048 rootfstype=ubifs root=ubi0:rootfs init=/init mem=80M vram=4M notifyk.vpssm3_sva=0xBFD00000 omap_wdt.timer_margin=30 " CONFIG_ROOTFS_PERM CONFIG_KERNEL_ARG_CONSOLE "\0" \
	"recovery_bootcmd=setenv bootargs ${recovery_bootargs} lpj=${lpj} boot_reason=${boot_reason}; nboot 0x81000000 0 0x6F00000; bootm\0" \
	"recovery_bootargs=console=ttyO0,115200n8 noinitrd mem=384M rootwait rw omap_wdt.timer_margin=30 " CONFIG_KERNEL_ARG_CONSOLE "\0" \
	"bootcmd=run normal_bootcmd; run recovery_bootcmd; fastboot\0" \

#elif defined(CONFIG_SD_BOOT)          /* Autoload the 2nd stage from SD */
#define CONFIG_MMC                    1
#undef  CONFIG_FASTBOOT
#define CONFIG_MMC_ENV                1
#define CONFIG_NAND                   1
#define CONFIG_EXTRA_ENV_SETTINGS \
        "verify=yes\0" \
	"sd_bootcmd=ttmtest; setenv bootargs ${bootargs} boot_reason=${boot_reason}; mmc rescan 0; fatload mmc 0 0x81000000 uImage; bootm 0x81000000\0" \
	"bootargs=console=ttyO0,115200n8 noinitrd mem=54M rootwait vram=4M notifyk.vpssm3_sva=0xBFD00000 root=/dev/mmcblk0p2 rw init=/init earlyprintk\0" \

#elif defined(CONFIG_UART_BOOT)                /* stop in the min prompt */
#define CONFIG_EXTRA_ENV_SETTINGS \
        "verify=yes\0" \
        "bootcmd=\0" \

#endif

/* Remove unused modules */

#undef CONFIG_CMD_NFS
#undef CONFIG_SPI
#undef CONFIG_I2C
#undef CONFIG_SYS_HUSH_PARSER
#undef CONFIG_CMD_AUTOTEST	/* for autotest */
#undef CONFIG_MMC
#undef CONFIG_ZLIB
#undef CONFIG_GZIP
#undef CONFIG_CMD_XIMG
#undef CONFIG_CMD_ITEST
#undef CONFIG_CMD_LOADB	/* loadb */
#undef CONFIG_CMD_LOADY	/* loady */
#undef CONFIG_CMD_LOADS	/* loads */
#undef CONFIG_CMD_IMI
#undef CONFIG_BOOTM_NETBSD
#undef CONFIG_BOOTM_RTEMS

#else /*2nd stage configs*/

#include <config_cmd_default.h>
#define CONFIG_SERIAL_TAG		1
#define CONFIG_REVISION_TAG		1
# define CONFIG_ENV_SIZE		(0x20000 * 5)
#define CONFIG_SYS_MALLOC_LEN		(CONFIG_ENV_SIZE + (32 * 1024))
/* size in bytes reserved for initial data */
#define CONFIG_ENV_OVERWRITE
#define CONFIG_SYS_LONGHELP
#define CONFIG_SYS_PROMPT		"DM385_LB#"
/* enable passing of ATAGs  */
#define CONFIG_CMDLINE_TAG		1
#define CONFIG_SETUP_MEMORY_TAGS	1
/* Required for ramdisk support */
#define CONFIG_INITRD_TAG		1
/* set to negative value for no autoboot */
#ifdef ZERO_BOOT_DELAY
#define CONFIG_BOOTDELAY                0
#else
#define CONFIG_BOOTDELAY		3
#endif
#define CONFIG_NAND			1
#define CONFIG_MUSB_UDC
#define CONFIG_FASTBOOT			/* Android fast boot */
#define CONFIG_USB_MUSB_TI81XX		1
#define CONFIG_CMD_FD			/* factory data load command */


/* Undef to remove unused modules */
#define CONFIG_NO_ETH
#undef CONFIG_CMD_NFS
#undef CONFIG_SPI
#undef CONFIG_I2C
#undef CONFIG_SYS_HUSH_PARSER
#undef CONFIG_CMD_AUTOTEST	/* for autotest */
#undef CONFIG_MMC
#undef CONFIG_ZLIB
#undef CONFIG_GZIP
#undef CONFIG_CMD_XIMG
#undef CONFIG_CMD_ITEST
#undef CONFIG_CMD_LOADB	/* loadb */
#undef CONFIG_CMD_LOADY	/* loady */
#undef CONFIG_CMD_LOADS	/* loads */
#undef CONFIG_CMD_IMI
#undef CONFIG_BOOTM_NETBSD
#undef CONFIG_BOOTM_RTEMS

#define CONFIG_EXTRA_ENV_SETTINGS \
	"verify=yes\0" \
	"bootfile=uImage\0" \
	"ramdisk_file=ramdisk.gz\0" \
	"loadaddr=0x81000000\0" \
	"script_addr=0x80900000\0" \
	"loadbootscript=fatload mmc 0 ${script_addr} boot.scr\0" \
	"bootscript= echo Running bootscript from MMC/SD to set the ENV...; " \
		"source ${script_addr}\0" \

#if defined(CONFIG_MMC_ENV)
#define CONFIG_BOOTARGS \
	"console=ttyO0,115200n8 noinitrd mem=80M rootwait vram=4M notifyk.vpssm3_sva=0xBFD00000 root=/dev/mmcblk0p2 rw cmemk.phys_start=0x85000000 cmemk.phys_end=0x89000000 cmemk.allowOverlap=1 earlyprintk"

#define CONFIG_BOOTCOMMAND \
	"mmc rescan 0; fatload mmc 0 0x81000000 uImage; bootm 0x81000000"

#elif defined(CONFIG_NAND_ENV)
#define CONFIG_BOOTARGS \
	"console=ttyO0,115200n8 rootwait=1 rw ubi.mtd=4,2048 rootfstype=ubifs root=ubi0:rootfs init=/init mem=80M vram=4M notifyk.vpssm3_sva=0xBFD00000 cmemk.phys_start=0x85000000 cmemk.phys_end=0x89000000 cmemk.allowOverlap=1 earlyprintk"

#define CONFIG_BOOTCOMMAND \
	"nboot 0x81000000 0 0x300000; bootm"

#endif

#endif

#ifdef CONFIG_MUSB_UDC
#ifdef CONFIG_FASTBOOT
#define CONFIG_CMD_FASTBOOT
#define CONFIG_FASTBOOT_TRANSFER_BUFFER         (0x81000000)
#define CONFIG_FASTBOOT_TRANSFER_BUFFER_SIZE    (0x07000000)
/* if already present, use already existing NAND macros for block & oob size */
#define FASTBOOT_NAND_BLOCK_SIZE                2048
#define FASTBOOT_NAND_OOB_SIZE                  64
/* Fastboot product name */
#define FASTBOOT_PRODUCT_NAME   "dm385"
/* Use HS */
#define USB_BCD_VERSION                 0x0200
/* Enables Fastboot to NAND */
#else
/* USB device configuration */
#define CONFIG_SYS_CONSOLE_IS_IN_ENV    1
#endif /* CONFIG_FASTBOOT */
/* Change these to suit your needs */
/*Changed Vendor Id and Product Id for fastboot device detection */
#define CONFIG_USB_DEVICE               1
#define CONFIG_USBD_VENDORID            0x1390
#define CONFIG_USBD_PRODUCTID           0xd001
#define CONFIG_USBD_MANUFACTURER        "TOMTOM B.V."
#define CONFIG_USBD_PRODUCT_NAME        "TTBANDIT"
#endif /* CONFIG_MUSB_UDC */

#define CONFIG_SYS_GBL_DATA_SIZE	128
#define CONFIG_MISC_INIT_R		1
#ifndef CONFIG_DM385_MIN_CONFIG
#define CONFIG_DM385_ASCIIART		1
#endif
#define CONFIG_CMD_CACHE
#define CONFIG_CMD_ECHO

/*
 * Miscellaneous configurable options
 */

/* max number of command args */
#define CONFIG_SYS_MAXARGS		32
/* Console I/O Buffer Size */
#define CONFIG_SYS_CBSIZE		512
/* Print Buffer Size */
#define CONFIG_SYS_PBSIZE		(CONFIG_SYS_CBSIZE \
					+ sizeof(CONFIG_SYS_PROMPT) + 16)
/* Boot Argument Buffer Size */
#define CONFIG_SYS_BARGSIZE		CONFIG_SYS_CBSIZE

#if defined(CONFIG_SD_BOOT)
/* Memory test */
#define CONFIG_SYS_ALT_MEMTEST			/* Extended memory test */
#define CONFIG_SYS_MEMTEST_ITERS	1	/* No. times to run the memory test */
#define CONFIG_SYS_MEMTEST_SCRATCH	(PHYS_DRAM_1 + SZ_4M) 	/* Scratch memory for use by memory test */
#define CONFIG_SYS_MEMTEST_START	(PHYS_DRAM_1) 		/* Memory test start offset */
#define CONFIG_SYS_MEMTEST_END	  (PHYS_DRAM_1 + (256 * 1024 * 1024))	/* Memory test end offset */
#else
/* memtest works on 8 MB in DRAM after skipping 32MB from start addr
 * of ram disk
 */
#define CONFIG_SYS_MEMTEST_START	(PHYS_DRAM_1 + (64 * 1024 * 1024))
#define CONFIG_SYS_MEMTEST_END		(CONFIG_SYS_MEMTEST_START \
					+ (8 * 1024 * 1024))
#endif


#if defined(CONFIG_CMD_AUTOTEST)
#define CONFIG_SYS_MEMTEST_SIZE 0x10000000	/* autotest memory size*/
#endif
/* everything, incl board info, in Hz */
#undef  CONFIG_SYS_CLKS_IN_HZ
/* Default load address */
#define CONFIG_SYS_LOAD_ADDR		0x81000000
/* 1ms clock */
#define CONFIG_SYS_HZ			1000

/* Hardware related */

/**
 * Physical Memory Map
 */
/* we have 1 bank of DRAM */
#define CONFIG_NR_DRAM_BANKS		1
#define PHYS_DRAM_1			0x80000000	/* DRAM Bank #1 */
#define PHYS_DRAM_1_SIZE		0x40000000	/* 1 GB */
#define CFG_FDT_ADDR			MEMADDR_FDT_START	/* Factory data load address */

/**
 * Platform/Board specific defs
 */
#define CONFIG_SYS_CLK_FREQ		20000000
#define CONFIG_SYS_TIMERBASE		0x4802E000

/*
 * NS16550 Configuration
 */
#define CONFIG_SERIAL_MULTI	1
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_SYS_NS16550_REG_SIZE	(-4)
#define CONFIG_SYS_NS16550_CLK		(48000000)
#define CONFIG_SYS_NS16550_COM1		0x48020000	/* Base EVM has UART0 */
#define CONFIG_SYS_NS16550_COM3		0x48024000

#define CONFIG_BAUDRATE		115200
#define CONFIG_SYS_BAUDRATE_TABLE	{ 110, 300, 600, 1200, 2400, \
4800, 9600, 14400, 19200, 28800, 38400, 56000, 57600, 115200 }

/*
 * select serial console configuration
 */
#define CONFIG_SERIAL1			1
#define CONFIG_CONS_INDEX		1
#define CONFIG_SYS_CONSOLE_INFO_QUIET

#if defined(CONFIG_NO_ETH)
# undef CONFIG_CMD_NET
#else
# define CONFIG_CMD_NET
# define CONFIG_CMD_DHCP
# define CONFIG_CMD_PING
#endif

#if defined(CONFIG_CMD_NET)
#define CONFIG_DRIVER_TI_CPSW
#define CONFIG_MII
#define CONFIG_BOOTP_DEFAULT
#define CONFIG_BOOTP_DNS
#define CONFIG_BOOTP_DNS2
#define CONFIG_BOOTP_SEND_HOSTNAME
#define CONFIG_BOOTP_GATEWAY
#define CONFIG_BOOTP_SUBNETMASK
#define CONFIG_NET_RETRY_COUNT		10
#define CONFIG_NET_MULTI
#define CONFIG_PHY_GIGE
/* increase network receive packet buffer count for reliable TFTP */
#define CONFIG_SYS_RX_ETH_BUFFER      16
#endif

#if defined(CONFIG_SYS_NO_FLASH)
# define CONFIG_ENV_IS_NOWHERE
#endif

/* NAND support */
#ifdef CONFIG_NAND
#define CONFIG_CMD_NAND
#define CONFIG_NAND_TI81XX
#define GPMC_NAND_ECC_LP_x16_LAYOUT	1
#define NAND_BASE			(0x08000000)
/* physical address to access nand */
#define CONFIG_SYS_NAND_ADDR		NAND_BASE
/* physical address to access nand at CS0 */
#define CONFIG_SYS_NAND_BASE		NAND_BASE
/* Max number of NAND devices */
#define CONFIG_SYS_MAX_NAND_DEVICE	1
#define CONFIG_SYS_NAND_USE_FLASH_BBT
#endif

/* ENV in NAND */
#define MNAND_ENV_OFFSET                0x260000

#if defined(CONFIG_NAND_ENV)
# undef CONFIG_ENV_IS_NOWHERE
# define CONFIG_ENV_IS_IN_NAND		1
# ifdef CONFIG_ENV_IS_IN_NAND
/* max number of sectors in a chip */
#define CONFIG_SYS_MAX_FLASH_SECT	520
/* max number of flash banks */
#define CONFIG_SYS_MAX_FLASH_BANKS	2
/* Reserve 2 sectors */
#define CONFIG_SYS_MONITOR_LEN	(256 << 10)
#define CONFIG_SYS_FLASH_BASE		boot_flash_base
#define CONFIG_SYS_MONITOR_BASE	CONFIG_SYS_FLASH_BASE
/* environment starts here */
#define CONFIG_SYS_ENV_SECT_SIZE	boot_flash_sec
#define CONFIG_ENV_OFFSET		boot_flash_off
#define CONFIG_ENV_ADDR		MNAND_ENV_OFFSET
#endif

# ifndef __ASSEMBLY__
extern unsigned int boot_flash_base;
extern volatile unsigned int boot_flash_env_addr;
extern unsigned int boot_flash_off;
extern unsigned int boot_flash_sec;
extern unsigned int boot_flash_type;
# endif
#endif /* NAND support */

/* SPI support */
#ifdef CONFIG_SPI
#define CONFIG_OMAP3_SPI
#define CONFIG_MTD_DEVICE
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_WINBOND
#define CONFIG_CMD_SF
#define CONFIG_SF_DEFAULT_SPEED	(75000000)
#endif

/* ENV in SPI */
#if defined(CONFIG_SPI_ENV)
#undef CONFIG_ENV_IS_NOWHERE
#undef CONFIG_ENV_SIZE
#define CONFIG_ENV_IS_IN_SPI_FLASH	1
#ifdef CONFIG_ENV_IS_IN_SPI_FLASH
#define CONFIG_ENV_SIZE		0x2000 /*use a small env */
#define CONFIG_SYS_FLASH_BASE		(0)
/* sector size of SPI flash */
#define SPI_FLASH_ERASE_SIZE		(4 * 1024)
/* env size */
#define CONFIG_SYS_ENV_SECT_SIZE	(2 * SPI_FLASH_ERASE_SIZE)
#define CONFIG_ENV_SECT_SIZE		(CONFIG_SYS_ENV_SECT_SIZE)
#define CONFIG_ENV_OFFSET		(96 * SPI_FLASH_ERASE_SIZE)
#define CONFIG_ENV_ADDR		(CONFIG_ENV_OFFSET)
/* no of sectors in SPI flash */
#define CONFIG_SYS_MAX_FLASH_SECT	(1024)
#define CONFIG_SYS_MAX_FLASH_BANKS	(1)
#endif
#endif /* SPI support */

/* ENV in MMC */
#if defined(CONFIG_MMC_ENV)
#define CONFIG_MMC                    1
#undef CONFIG_ENV_IS_NOWHERE
#define CONFIG_ENV_IS_IN_MMC		1
#define CONFIG_SYS_MMC_ENV_DEV		0
#undef CONFIG_ENV_SIZE
#undef CONFIG_ENV_OFFSET
#define CONFIG_ENV_OFFSET		(72293*1024)
#define CONFIG_ENV_SIZE			(8 * 1024)
#if defined(CONFIG_CMD_FASTBOOT)
#define MNAND_ENV_OFFSET                0x260000
#endif
#endif /* MMC support */

/* NOR support */
#if defined(CONFIG_NOR)
/* Remove NAND support */
#undef CONFIG_CMD_NAND
#undef CONFIG_NAND_TI81XX
#undef CONFIG_SKIP_LOWLEVEL_INIT
#define CONFIG_DM385_CONFIG_DDR
#define CONFIG_SETUP_PLL
#define CONFIG_DM385_IPNC_DDR3
#undef CONFIG_ENV_IS_NOWHERE
#ifdef CONFIG_SYS_MALLOC_LEN
#undef CONFIG_SYS_MALLOC_LEN
#endif
#define CONFIG_SYS_FLASH_USE_BUFFER_WRITE 1
#define CONFIG_SYS_MALLOC_LEN		(0x100000)
#define CONFIG_SYS_FLASH_CFI
#define CONFIG_FLASH_CFI_DRIVER
#define CONFIG_FLASH_CFI_MTD
#define CONFIG_SYS_MAX_FLASH_SECT	512
#define CONFIG_SYS_MAX_FLASH_BANKS	1
#define CONFIG_SYS_FLASH_BASE		(0x08000000)
#define CONFIG_SYS_MONITOR_BASE	CONFIG_SYS_FLASH_BASE
#define CONFIG_ENV_IS_IN_FLASH	1
#define NOR_SECT_SIZE			(128 * 1024)
#define CONFIG_SYS_ENV_SECT_SIZE	(NOR_SECT_SIZE)
#define CONFIG_ENV_SECT_SIZE		(NOR_SECT_SIZE)
#define CONFIG_ENV_OFFSET		(2 * NOR_SECT_SIZE)
#define CONFIG_ENV_ADDR		(CONFIG_SYS_FLASH_BASE + \
						CONFIG_ENV_OFFSET)
#define CONFIG_MTD_DEVICE
#endif	/* NOR support */


/* No I2C support in 1st stage */
#ifdef CONFIG_I2C

#define CONFIG_CMD_I2C
#define CONFIG_CMD_DATE
#define CONFIG_I2C_MULTI_BUS
#define CONFIG_HARD_I2C			1
#define CONFIG_SYS_I2C_SPEED		100000
#define CONFIG_SYS_I2C_SLAVE		1
#define CONFIG_SYS_I2C_BUS		0
#define CONFIG_SYS_I2C_BUS_SELECT	1
#define CONFIG_DRIVER_TI81XX_I2C	1

#endif

/* HSMMC support */
#ifdef CONFIG_MMC
#define CONFIG_CMD_MMC		1
#define CONFIG_GENERIC_MMC
#define CONFIG_OMAP_HSMMC
#define CONFIG_DOS_PARTITION	1
#define CONFIG_CMD_FAT		1
#endif
/* U-boot Version */
#define CONFIG_VERSION_VARIABLE
#define CONFIG_IDENT_STRING " DM385_LB_3.80.00"
/* Unsupported features */
#undef CONFIG_USE_IRQ

#define RTC_SCRATCHPAD0_REG 0x480c0060
#define RTC_SCRATCHPAD1_REG 0x480c0064
#define RTC_SCRATCHPAD2_REG 0x480c0068
#define FLIPFLOP_RECOVERY_BOOT_MAGIC 0x73647570
#define FLIPFLOP_RECOVERY_BOOT_UPDATE_MAGIC (~FLIPFLOP_RECOVERY_BOOT_MAGIC)
#define FLIPFLOP_FASTBOOT_MAGIC 0xFA51B001
#define FLIPFLOP_SOFTBOOT_MAGIC  0x50F1B001

#endif	  /* ! __CONFIG_DM385_LB_H */

