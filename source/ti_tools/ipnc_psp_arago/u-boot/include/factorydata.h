/*
 * Factory data interface
 *
 * Copyright (C) 2010 TomTom International B.V.
 * 
 ************************************************************************
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
 * 02110-1301, USA.
 ************************************************************************
 */

#ifndef __FACTORYDATA_H
#define __FACTORYDATA_H

//extern int set_kernel_bricknums_fdt(void);

extern void *addr_fd;			/* location of factory data area */
extern size_t size_fd;			/* size of factory data area */
extern ulong load_size;			/* size of the loaded kernel image */

# ifdef CONFIG_CMD_FD
extern int check_fdt_machid(void);
# else
static int check_fdt_machid(void)
{
	return 0;
}
# endif

# ifdef CONFIG_FDT_FB

/**
 * Returns a pointer to the property specified by it's path and name within the factory data tree.
 * Returns null when fdt is invalid or property is not present.
 */
extern const char *fdt_get_str_property(const char *path, const char *name);

/**
 * Copies the property value to dest for maxSize length. Property is specified by it's path and name.
 * When property can not be read from fdt, the fallback is copied to dest instead.
 *
 * If the length of property value or fallback is less than maxSize, this function writes additional
 * null bytes to dest to ensure that a total of maxSize bytes are written. (it uses strncpy(dest, ..., maxSize))
 *
 * Returns:
 * -1 when maxSize equals 0
 *  0 when property value is copied
 *  1 when fallback value is copied
 */
extern int fdt_get_string_property_or_fallback(const char *path, const char *name,
		char *dest, unsigned int maxSize, const char *fallback);

/**
 * Copies int value property to result. Property is specified by it's path and name.
 *
 * Returns:
 *  0 when property is succesfully read
 * <0 when something went wrong
 */
extern int fdt_get_int_property(const char *path, const char *name, int *result);

extern int do_fdload(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]);

# endif /* __FACTORYDATA_H */
# endif
