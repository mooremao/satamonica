/*
 *  Copyright (C) 2009 TomTom BV <http://www.tomtom.com/>
 *
 *  Export flip-flop value R/W via sysfs
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/of.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/printk.h>
#include <linux/tomtom/flipflop.h>

static void flipflop_set_state(struct platform_device *pdev, int state)
{
#ifdef CONFIG_TOMTOM_FLIPFLOP_HW
	unsigned long flags;
	volatile int value;
	struct flipflop_pdata *pdata = (struct flipflop_pdata *)pdev->dev.platform_data;
	int i;

	local_irq_save(flags);

	for(i=0; i<10; i++) {
		value = gpio_get_value(pdata->boot_q_gpio);
		if (!state == !value)
			break;
		/* Pulse BOOT clock output */
		gpio_set_value(pdata->boot_clk_gpio, 1);
		msleep(1);
		gpio_set_value(pdata->boot_clk_gpio, 0);
		msleep(1);
	}

	local_irq_restore(flags);
#else
	unsigned long flags, value;
	struct flipflop_pdata *pdata = (struct flipflop_pdata *)pdev->dev.platform_data;

	local_irq_save(flags);

	switch (state) {
	case 0:
		value = 0;
		break;
	case 1: /* Fall-though */
	default:
		value = pdata->magic1;
		break;

	case 2:
		value = pdata->magic2;
		break;
	}

	__raw_writel(value, pdata->address);
	dev_dbg(&pdev->dev, "set to %lu\n", value);

	local_irq_restore(flags);
#endif
}

static int flipflop_get_state(struct platform_device *pdev)
{
	unsigned long flags, value;
	int state;
	struct flipflop_pdata *pdata = (struct flipflop_pdata *)pdev->dev.platform_data;

	local_irq_save(flags);
#ifdef CONFIG_TOMTOM_FLIPFLOP_HW
	state = gpio_get_value(pdata->boot_q_gpio);
#else
	value = __raw_readl(pdata->address);

	if (value == pdata->magic1)
		state = 1;
	else if(pdata->magic2 != 0 && value == pdata->magic2)
		state = 2;
	else
		state = 0;
#endif
	local_irq_restore(flags);

	return state;
}

static ssize_t flipflop_show(struct device *dev,
			struct device_attribute *attr,
			char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	int state = flipflop_get_state(pdev);

	return snprintf(buf, PAGE_SIZE, "%x\n", state);
}

static ssize_t flipflop_store(struct device *dev,
			struct device_attribute *attr,
			const char *buf,
			size_t count)
{
	ssize_t ret = count;
	struct platform_device *pdev = to_platform_device(dev);
	struct flipflop_pdata *pdata = (struct flipflop_pdata *)pdev->dev.platform_data;

	if (count > 2)
		return -EINVAL;

	if ((count == 2) && (buf[1] != '\n'))
		return -EINVAL;

	if (buf[0] == '0')
		flipflop_set_state(pdev, 0);
	else if (buf[0] == '1')
		flipflop_set_state(pdev, 1);
	else if (pdata->magic2 != 0 && buf[0] == '2')
		flipflop_set_state(pdev, 2);
	else
		ret = -EINVAL;

	return ret;
}

DEVICE_ATTR(value, S_IRUGO| S_IWUGO, flipflop_show, flipflop_store);

#ifdef CONFIG_PM
static int flipflop_resume(struct platform_device *pdev)
{
	dev_dbg(&pdev->dev, "resuming\n");

	flipflop_set_state(pdev, 0);

	return 0;
}
#else
#define flipflop_resume NULL
#endif

static int flipflop_probe(struct platform_device *pdev)
{
	int rc = -1;
	struct flipflop_pdata *pdata = pdev->dev.platform_data;

#ifdef CONFIG_TOMTOM_FLIPFLOP_HW
	rc = gpio_request_one(pdata->boot_clk_gpio, GPIOF_OUT_INIT_LOW, "boot_clk");
	if(rc) {
		dev_err(&pdev->dev, "Failed to request gpio boot_clk\n");
		goto err_gpio_boot_clk;
	}
	rc = gpio_request_one(pdata->boot_q_gpio, GPIOF_IN, "boot_q");
	if(rc) {
		dev_err(&pdev->dev, "Failed to request gpio boot_q\n");
		goto err_gpio_boot_q;
	}
#else
	u32 phys_address;

	switch(pdata->scratch_reg_number)
	{
	case 0:
		phys_address = (TI81XX_RTC_BASE + RTC_SCRATCH0_REG);
		break;

	case 1:
		phys_address = (TI81XX_RTC_BASE + RTC_SCRATCH1_REG);
		break;

	case 2:
		phys_address = (TI81XX_RTC_BASE + RTC_SCRATCH2_REG);
		break;

	default:
		rc = -EINVAL;
		dev_err(&pdev->dev, "Invalid scratch_reg_number\n");
		goto err_scratch_reg_number;
	}

	pdata->address = ioremap(phys_address, sizeof(int));
#endif

	rc = device_create_file(&pdev->dev, &dev_attr_value);
	if (rc < 0) {
		dev_err(&pdev->dev, "creation of sysfs attribute failed\n");
		goto err_sysfs;
	}

	return 0;

err_sysfs:
#ifdef CONFIG_TOMTOM_FLIPFLOP_HW
	gpio_free(pdata->boot_q_gpio);
err_gpio_boot_q:
	gpio_free(pdata->boot_clk_gpio);
err_gpio_boot_clk:
#else
	iounmap(pdata->address);
err_scratch_reg_number:
#endif
	return rc;
};

static int flipflop_remove(struct platform_device *pdev)
{
	struct flipflop_pdata *pdata = pdev->dev.platform_data;

	device_remove_file(&pdev->dev, &dev_attr_value);

#ifdef CONFIG_TOMTOM_FLIPFLOP_HW
	gpio_free(pdata->boot_clk_gpio);
	gpio_free(pdata->boot_q_gpio);
#else
	iounmap(pdata->address);
#endif

	return 0;
}

static struct platform_driver flipflop_driver = {
	.probe		= flipflop_probe,
	.remove		= flipflop_remove,
	.suspend	= NULL,
	.resume		= flipflop_resume,
	.driver	= {
		.owner	= THIS_MODULE,
		.name	= "flipflop",
	}
};

int __init flipflop_init_module(void)
{
	printk(KERN_DEBUG "flipflop driver, (c) 2009 TomTom BV\n");
	return platform_driver_register(&flipflop_driver);
}

void __exit flipflop_exit_module(void)
{
	platform_driver_unregister(&flipflop_driver);
}

module_init(flipflop_init_module);
module_exit(flipflop_exit_module);


MODULE_DESCRIPTION("TomTom flipflop");
MODULE_LICENSE("GPL");
