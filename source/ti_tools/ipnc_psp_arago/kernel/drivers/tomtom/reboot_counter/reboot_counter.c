/*
 *  Copyright (C) 2015 TomTom BV <http://www.tomtom.com/>
 *
 *  Export reboot counter value R/W via sysfs
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/string.h>
#include <linux/printk.h>
#include <linux/tomtom/reboot_counter.h>

static ssize_t reboot_counter_show(struct device *dev,
			struct device_attribute *attr,
			char *buf)
{
	unsigned int value;
	struct platform_device *pdev = to_platform_device(dev);
	struct reboot_counter_pdata *pdata = (struct reboot_counter_pdata *)pdev->dev.platform_data;

	value = pdata->load_reboot_counter();

	return snprintf(buf, PAGE_SIZE, "%u\n", value);
}

static ssize_t reboot_counter_store(struct device *dev,
			struct device_attribute *attr,
			const char *buf,
			size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct reboot_counter_pdata *pdata = (struct reboot_counter_pdata *)pdev->dev.platform_data;
	unsigned long value;

	if (strict_strtoul(buf, 10, &value) != 0)
		return -EINVAL;

	pdata->store_reboot_counter(value);

	return count;
}

static DEVICE_ATTR(value, S_IRUGO| S_IWUGO, reboot_counter_show, reboot_counter_store);

static int reboot_counter_probe(struct platform_device *pdev)
{
	int rc = -1;
	struct reboot_counter_pdata *pdata = pdev->dev.platform_data;

	printk("reboot_counter_probe\n");

	if (pdata->load_reboot_counter == NULL || pdata->store_reboot_counter == NULL)
		return -ENODEV;

	rc = device_create_file(&pdev->dev, &dev_attr_value);
	if (rc < 0) {
		dev_err(&pdev->dev, "creation of sysfs attribute failed\n");
		goto err_sysfs;
	}

	return 0;

err_sysfs:
	return rc;
};

static int reboot_counter_remove(struct platform_device *pdev)
{
	device_remove_file(&pdev->dev, &dev_attr_value);

	return 0;
}

static struct platform_driver reboot_counter_driver = {
	.probe		= reboot_counter_probe,
	.remove		= reboot_counter_remove,
	.suspend	= NULL,
	.resume		= NULL,
	.driver	= {
		.owner	= THIS_MODULE,
		.name	= "reboot-counter",
	}
};

int __init reboot_counter_init_module(void)
{
	printk("reboot_counter_init_module\n");
	printk(KERN_DEBUG "Reboot_counter driver, (c) 2015 TomTom BV\n");
	return platform_driver_register(&reboot_counter_driver);
}

void __exit reboot_counter_exit_module(void)
{
	platform_driver_unregister(&reboot_counter_driver);
}

module_init(reboot_counter_init_module);
module_exit(reboot_counter_exit_module);


MODULE_DESCRIPTION("TomTom Reboot Counter");
MODULE_LICENSE("GPL");
