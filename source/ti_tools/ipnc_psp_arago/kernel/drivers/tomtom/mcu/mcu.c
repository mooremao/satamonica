/*
 *  Copyright (C) 2014 TomTom BV <http://www.tomtom.com/>
 *
 *  Driver for TomTom ATMEL MCU (Long Beach Project).
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/err.h>
#include <linux/i2c.h>
#include <linux/kobject.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/tomtom/mcu.h>

#define MCU_DETECTION_DELAY_TIME 10 /* msecs */
#define MCU_I2C_DELAY_TIME 30 /* usecs */
#define MCU_I2C_RECONFIG_TIME 60 /* usecs */

/* REGISTERS */
#define REG_APP_VERSION		0x00
#define REG_STATUS1		0x01
#define REG_AC_IN_LIMIT		0x02
#define REG_USB_LIMIT		0x03
#define REG_FG_FAILS		0x04
#define REG_WATCHDOG_RESET	0x05
#define REG_WAKEUP_LSB		0x06
#define REG_WAKEUP_MSB		0x07
#define REG_PASSWORD		0x08
#define REG_BATTERY_TYPE	0x09
#define REG_TEST_MODE		0x0A
#define REG_STATUS2		0x0B
#define REG_INTERRUPT		0x0C
#define REG_PORT_A_IN		0x0D
#define REG_PORT_B_IN		0x0E
#define REG_USB_POWERON		0x0F
#define REG_PORT_D_IN		0x10
#define REG_PORT_A_OUT		0x11
#define REG_PORT_B_OUT		0x12
#define REG_PORT_C_OUT		0x13
#define REG_PORT_D_OUT		0x14

/* Status #1 register bit/mask definitions */
#define MASK_STATUS1__EXT_MIC_PRN		0x01
#define MASK_STATUS1__BATT_DISCHARGE		0x02
#define MASK_STATUS1__POWER_PRN			0x04
#define MASK_STATUS1__USB_BRIDGE		0x08
#define MASK_STATUS1__FG_SOC_LOW_BAT		0x10
#define MASK_STATUS1__FG_SOC_FAIL_BAT		0x20
#define MASK_STATUS1__USB2_POWER_PRN		0x40
#define MASK_STATUS1__USB3_POWER_PRN		0x80

/* AC IN Limit register bit/mask definitions */
#define MASK_AC_IN_LIMIT__CHARGING_RATE		0x10

/* Test mode register bit/mask definitions */
#define MASK_TEST_MODE__USB2			0x01
#define MASK_TEST_MODE__USB3			0x02
#define MASK_TEST_MODE__BATT			0x80

/* Status #2 register bit/mask definitions */
#define MASK_STATUS2__BATT_TEMP_IN_RANGE	0x08

#define MASK_INTERRUPT__EXT_MIC			0x01
#define MASK_INTERRUPT__WAKEUP			0x02
#define MASK_INTERRUPT__POWER_PRN		0x04
#define MASK_INTERRUPT__BATT_TEMP		0x08
#define MASK_INTERRUPT__SOC_BATT_LOW		0x10
#define MASK_INTERRUPT__SOC_TURN_OFF		0x20
#define MASK_INTERRUPT__USB_WAKEUP		0x40
#define MASK_INTERRUPT__SW_BUTTON_PRESSED	0x80

/* mcu wakeup source masks */
#define MCU_WAKEUP_SOURCE_TIMER        0x02
#define MCU_WAKEUP_SOURCE_POWER_PRN    0x04
#define MCU_WAKEUP_SOURCE_BATT_TEMP    0x08
#define MCU_WAKEUP_SOURCE_SOC_BATT_LOW 0x20 /* SOC1 and SOCF swapped */
#define MCU_WAKEUP_SOURCE_SOC_TURN_OFF 0x10 /* SOC1 and SOCF swapped */
#define MCU_WAKEUP_SOURCE_BUTTON       0x80

#define PASSWORD 0x48

#define MCU_BATTERY_2000mAH		0x00
#define MCU_BATTERY_3000mAH		0x01

#define MCU_FW_VERSION_WAKEUP_RESOLUTION_1_SECOND 28

#define MCU_WAKEUP_TIMER_MAX_TIMEOUT 0xFFFF

extern void update_boot_reason(bool);

long tomtom_mcu_time_to_sleep_sec;
unsigned long tomtom_mcu_wakeup_timer_set_time;

static unsigned long tomtom_mcu_wakeup_timeout_set_time;
static long tomtom_mcu_wakeup_timeout_sec;

static uint32_t tomtom_mcu_init_flag = 1;
static int tomtom_mcu_irq_value = 0x0;
static bool tomtom_mcu_debug_flag = 0;

static unsigned long last_rw_jiffies = 0;

static struct tomtom_mcu {
	struct i2c_client *client;
	struct tomtom_mcu_pdata *pdata;
	struct mutex io_lock;
	struct delayed_work work;
	struct power_supply usbpower;
	int wakeup_timer_resolution; /* Seconds */
	struct tomtom_mcu_info info;
} mcu;

static BLOCKING_NOTIFIER_HEAD(host_int_notifier);

int host_int_reg_notifier(struct notifier_block *nb)
{
	return blocking_notifier_chain_register(&host_int_notifier, nb);
}
EXPORT_SYMBOL_GPL(host_int_reg_notifier);

void host_int_unreg_notifier(struct notifier_block *nb)
{
	blocking_notifier_chain_unregister(&host_int_notifier, nb);
}
EXPORT_SYMBOL_GPL(host_int_unreg_notifier);

void host_int_interrupt_event(enum host_int_type type)
{
	blocking_notifier_call_chain(&host_int_notifier, type, NULL);
}

static inline int tomtom_mcu_read(struct tomtom_mcu *mcu, u8 reg)
{
	int ret;

	/* MCU requires a minimum delay between two consecutive
	   i2c transactions */
	if (last_rw_jiffies && time_before(jiffies, last_rw_jiffies
				+ usecs_to_jiffies(MCU_I2C_DELAY_TIME)))
		udelay(MCU_I2C_DELAY_TIME);

	ret = i2c_smbus_read_byte_data(mcu->client, reg);

	if (ret < 0) {
		/* MCU needs 50.25 us from STOP bit to re-configured
		   as a I2C slave. If the i2c error was due to MCU
		   reconfiguration delay, wait and try again */
		udelay(MCU_I2C_RECONFIG_TIME);
		ret = i2c_smbus_read_byte_data(mcu->client, reg);
	}

	last_rw_jiffies = jiffies;

	return ret;
}

static inline int tomtom_mcu_write(struct tomtom_mcu *mcu, u8 reg, u8 val)
{
	int ret;

	/* MCU requires a minimum delay between two consecutive
	   i2c transactions */
	if (last_rw_jiffies && time_before(jiffies, last_rw_jiffies
				+ usecs_to_jiffies(MCU_I2C_DELAY_TIME)))
		udelay(MCU_I2C_DELAY_TIME);

	ret = i2c_smbus_write_byte_data(mcu->client, reg, val);

	if (ret < 0) {
		/* MCU needs 50.25 us from STOP bit to re-configured
		   as a I2C slave. If the i2c error was due to MCU
		   reconfiguration delay, wait and try again */
		udelay(MCU_I2C_RECONFIG_TIME);
		ret = i2c_smbus_write_byte_data(mcu->client, reg, val);
	}

	last_rw_jiffies = jiffies;

	return ret;
}

static int tomtom_mcu_reg_read(struct tomtom_mcu *mcu, u8 reg)
{
	int data;

	mutex_lock(&mcu->io_lock);

	data = tomtom_mcu_read(mcu, reg);
	if (data < 0)
		dev_err(&mcu->client->dev, "Read from reg 0x%x failed. Error: %d\n", reg, data);

	mutex_unlock(&mcu->io_lock);
	return data;
}

static int tomtom_mcu_reg_write(struct tomtom_mcu *mcu, u8 reg, u8 val)
{
	int data;

	mutex_lock(&mcu->io_lock);

	data = tomtom_mcu_write(mcu, reg, val);
	if (data < 0)
		dev_err(&mcu->client->dev, "Write to reg 0x%x failed. Error: %d\n", reg, data);

	mutex_unlock(&mcu->io_lock);
	return data;
}

static void tomtom_mcu_worker_func(struct work_struct *work)
{
	int value;

	if(unlikely(tomtom_mcu_init_flag)) {
		tomtom_mcu_irq_value = tomtom_mcu_reg_read(&mcu, REG_INTERRUPT);
		value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);

		if (tomtom_mcu_irq_value >= 0 && value >= 0) {
			if (value & MASK_STATUS1__POWER_PRN) {
				gpio_direction_output(mcu.pdata->vbus_on_gpio, 1);
				host_int_interrupt_event(HOST_INT_POWER_PRESENT);

				if (mcu.pdata->notify_vbus)
					mcu.pdata->notify_vbus(VBUS_PRESENT);
			} else {
				gpio_direction_output(mcu.pdata->vbus_on_gpio, 0);
				host_int_interrupt_event(HOST_INT_POWER_NOT_PRESENT);

				if (mcu.pdata->notify_vbus)
					mcu.pdata->notify_vbus(VBUS_REMOVED);
			}

			if (tomtom_mcu_irq_value & MASK_INTERRUPT__EXT_MIC) {
				if (value & MASK_STATUS1__EXT_MIC_PRN) {
					host_int_interrupt_event(HOST_INT_EXT_MIC_PRESENT);
				} else {
					host_int_interrupt_event(HOST_INT_EXT_MIC_NOT_PRESENT);
				}
			}
		}

		tomtom_mcu_init_flag = 0;
		return;
	}

	/* Ext MIC */
	if (tomtom_mcu_irq_value & MASK_INTERRUPT__EXT_MIC) {
		value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
		if (value >= 0) {
			if (value & MASK_STATUS1__EXT_MIC_PRN) {
				host_int_interrupt_event(HOST_INT_EXT_MIC_PRESENT);
			} else {
				host_int_interrupt_event(HOST_INT_EXT_MIC_NOT_PRESENT);
			}
		}
	}

	/* External power present */
	if ((tomtom_mcu_irq_value & MASK_INTERRUPT__POWER_PRN)) {
		power_supply_changed(&mcu.usbpower); /* To trigger battery check */
		/* host_int_interrupt_event already generated */
	}

	/* Battery low */
	if ((tomtom_mcu_irq_value & MASK_INTERRUPT__SOC_BATT_LOW)) {
		power_supply_changed(&mcu.usbpower); /* To trigger battery check */
		host_int_interrupt_event(HOST_INT_SOC_BATT_LOW_CHANGED);
	}

	/* Battery turn off */
	if ((tomtom_mcu_irq_value & MASK_INTERRUPT__SOC_TURN_OFF)) {
		power_supply_changed(&mcu.usbpower); /* To trigger battery check */
		host_int_interrupt_event(HOST_INT_SOC_BATT_TURN_OFF_CHANGED);
	}

	/* Wakeup Timer Timeout */
	if (tomtom_mcu_irq_value & MASK_INTERRUPT__WAKEUP) {
		host_int_interrupt_event(HOST_INT_WAKEUP_TIMER_TIMEOUT);
	}

	/* Battery Temperature status change */
	if (tomtom_mcu_irq_value & MASK_INTERRUPT__BATT_TEMP) {
		value = tomtom_mcu_reg_read(&mcu, REG_STATUS2);
		if (value >= 0) {
			if (value & MASK_STATUS2__BATT_TEMP_IN_RANGE) {
				host_int_interrupt_event(HOST_INT_BATT_TEMP_IN_RANGE);
			} else {
				host_int_interrupt_event(HOST_INT_BATT_TEMP_NOT_IN_RANGE);
			}
		}
	}

	/* SW Button Pressed */
	if (tomtom_mcu_irq_value & MASK_INTERRUPT__SW_BUTTON_PRESSED) {
		host_int_interrupt_event(HOST_INT_SW_BUTTON_PRESSED);
	}

	return;
}

static ssize_t tomtom_mcu_get_version(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_APP_VERSION);
	if (value < 0)
		return -EIO;

	return sprintf(buf, "%d\n", value);
}

static int tomtom_mcu_is_ac_online_internal(struct tomtom_mcu *mcu) {
	int value;

	value = tomtom_mcu_reg_read(mcu, REG_AC_IN_LIMIT);
	if (value < 0)
		return -EIO;

	return (value & MASK_AC_IN_LIMIT__CHARGING_RATE) ? 1 : 0;
}

static int tomtom_mcu_is_ac_online(struct tomtom_mcu_info *info) {
	if (info == NULL || info->priv == NULL)
		return -EINVAL;

	return tomtom_mcu_is_ac_online_internal((struct tomtom_mcu *) info->priv);
}

static ssize_t tomtom_mcu_get_ac_online(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_is_ac_online_internal(&mcu);

	return scnprintf(buf, PAGE_SIZE, "%d\n", value);
}

static ssize_t tomtom_mcu_get_vbus(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
	if(value < 0)
		return -EIO;

	value = (value & (MASK_STATUS1__POWER_PRN | MASK_STATUS1__USB2_POWER_PRN));
	if (value == (MASK_STATUS1__POWER_PRN | MASK_STATUS1__USB2_POWER_PRN))
		value = 1;

	return sprintf(buf, "%d\n", value);
}

static ssize_t tomtom_mcu_get_ext_mic_present(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
	if (value < 0)
		return -EIO;

	value = (value & MASK_STATUS1__EXT_MIC_PRN);
	if (value == MASK_STATUS1__EXT_MIC_PRN)
		value = 1;

	return sprintf(buf, "%d\n", value);
}

static ssize_t tomtom_mcu_get_power_present(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
	if (value < 0)
		return -EIO;

	value = (value & MASK_STATUS1__POWER_PRN);
	if (value == MASK_STATUS1__POWER_PRN)
		value = 1;

	return sprintf(buf, "%d\n", value);
}

static ssize_t tomtom_mcu_get_usb2_power_present(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
	if (value < 0)
		return -EIO;

	value = (value & MASK_STATUS1__USB2_POWER_PRN);
	if (value == MASK_STATUS1__USB2_POWER_PRN)
		value = 1;

	return sprintf(buf, "%d\n", value);
}

static ssize_t tomtom_mcu_get_battery_type(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_BATTERY_TYPE);
	if (value < 0)
		return -EIO;

	if (value == MCU_BATTERY_2000mAH)
		return sprintf(buf, "2000mAH\n");
	else if (value == MCU_BATTERY_3000mAH)
		return sprintf(buf, "3000mAH\n");
	else
		return sprintf(buf, "unknown\n");
}

static ssize_t tomtom_mcu_get_battery_temp(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_STATUS2);
	if (value < 0)
		return -EIO;

	value = (value & MASK_STATUS2__BATT_TEMP_IN_RANGE);
	if (value == MASK_STATUS2__BATT_TEMP_IN_RANGE)
		value = 1;

	return sprintf(buf, "%d\n", value);
}

static ssize_t tomtom_mcu_get_password(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_PASSWORD);
	if (value < 0)
		return -EIO;

	return sprintf(buf, "0x%x\n", value);
}

static ssize_t tomtom_mcu_set_password(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t count)
{
	unsigned long value;
	int status = strict_strtoul(buf, 16, &value);

	if (0 != status)
		return status;

	tomtom_mcu_reg_write(&mcu, REG_PASSWORD, value);

	return count;
}

static ssize_t tomtom_mcu_get_test_mode(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_TEST_MODE);
	if (value < 0)
		return -EIO;

	return sprintf(buf, "0x%x\n", value);
}

static ssize_t tomtom_mcu_set_test_mode(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t count)
{
	unsigned long value;
	int status = strict_strtoul(buf, 16, &value);

	if (0 != status)
		return status;

	tomtom_mcu_reg_write(&mcu, REG_TEST_MODE, value);

	return count;
}

static ssize_t tomtom_mcu_get_wakeup_timeout(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_WAKEUP_MSB);
	if (value < 0)
		return -EIO;

	value = value << 8;
	value |= tomtom_mcu_reg_read(&mcu, REG_WAKEUP_LSB);
	if (value < 0)
		return -EIO;

	value = value * mcu.wakeup_timer_resolution;
	return sprintf(buf, "%d\n", value);
}

static ssize_t tomtom_mcu_set_wakeup_timeout(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t count)
{
	unsigned long value;
	int status = strict_strtoul(buf, 10, &value);

	if (0 != status)
		return status;

	/* Zero is a valid value for timer count as it can be used
	   to clear the wakeup timer. We do not allow a non-zero
	   value less than wakeup timer resolution */
	if ((value > 0) && (value < mcu.wakeup_timer_resolution))
		return -EINVAL;

	tomtom_mcu_wakeup_timeout_set_time = jiffies;
	tomtom_mcu_wakeup_timeout_sec = value;

	return count;
}

static ssize_t tomtom_mcu_get_wakeup_source(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	const char* reason;

	if (tomtom_mcu_irq_value & MCU_WAKEUP_SOURCE_BUTTON)
		reason = "button";
	else if (tomtom_mcu_irq_value & MCU_WAKEUP_SOURCE_TIMER)
		reason = "timer";
	else if (tomtom_mcu_irq_value & MCU_WAKEUP_SOURCE_POWER_PRN)
		reason = "power_prn";
	else if (tomtom_mcu_irq_value & MCU_WAKEUP_SOURCE_SOC_TURN_OFF)
		reason = "soc_turn_off";
	else if (tomtom_mcu_irq_value & MCU_WAKEUP_SOURCE_SOC_BATT_LOW)
		reason = "batt_low";
	else if (tomtom_mcu_irq_value & MCU_WAKEUP_SOURCE_BATT_TEMP)
		reason = "batt_temp";
	else
		reason = "other";

	return scnprintf(buf, PAGE_SIZE, "%s", reason);
}

static ssize_t tomtom_mcu_get_debug_flag(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_debug_flag;

	return sprintf(buf, "%d\n", value);
}

static ssize_t tomtom_mcu_set_debug_flag(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t count)
{
	unsigned long value;
	int status = strict_strtoul(buf, 10, &value);

	if (0 != status)
		return status;

	tomtom_mcu_debug_flag = value;

	return count;
}

static ssize_t tomtom_mcu_get_wakeup_resolution(struct device *dev,
			struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", mcu.wakeup_timer_resolution);
}

static ssize_t tomtom_mcu_get_usb_poweron(struct device *dev, struct device_attribute *attr, char *buf)
{
	int value;

	value = tomtom_mcu_reg_read(&mcu, REG_APP_VERSION);
	if (value < 0)
		return -EIO;

	if (value < 29)
		return sprintf(buf, "%d\n", -1);

	value = tomtom_mcu_reg_read(&mcu, REG_USB_POWERON);
	if (value < 0)
		return -EIO;

	return sprintf(buf, "%d\n", value ? 1 : 0);
}

static ssize_t tomtom_mcu_set_usb_poweron(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t count)
{
	unsigned long value;
	int ret;
	int status = strict_strtoul(buf, 10, &value);

	if (0 != status)
		return status;

	ret = tomtom_mcu_reg_read(&mcu, REG_APP_VERSION);
	if (ret < 0)
		return -EIO;

	if (ret < 29)
		return -ENOTSUPP;

	tomtom_mcu_reg_write(&mcu, REG_USB_POWERON, value ? 0xFF : 0x00);

	return count;
}

static struct device_attribute attributes[] = {
	__ATTR(version, 0444, tomtom_mcu_get_version, NULL),
	__ATTR(ac_online, 0444, tomtom_mcu_get_ac_online, NULL),
	__ATTR(vbus, 0444, tomtom_mcu_get_vbus, NULL),
	__ATTR(ext_mic_present, 0444, tomtom_mcu_get_ext_mic_present, NULL),
	__ATTR(power_present, 0444, tomtom_mcu_get_power_present, NULL),
	__ATTR(usb2_power_present, 0444, tomtom_mcu_get_usb2_power_present, NULL),
	__ATTR(battery_type, 0444, tomtom_mcu_get_battery_type, NULL),
	__ATTR(battery_temp, 0444, tomtom_mcu_get_battery_temp, NULL),
	__ATTR(password, 0664, tomtom_mcu_get_password, tomtom_mcu_set_password),
	__ATTR(test_mode, 0664, tomtom_mcu_get_test_mode, tomtom_mcu_set_test_mode),
	__ATTR(debug, 0664, tomtom_mcu_get_debug_flag, tomtom_mcu_set_debug_flag),
	__ATTR(wakeup_timeout, 0664, tomtom_mcu_get_wakeup_timeout,
			tomtom_mcu_set_wakeup_timeout),
	__ATTR(wakeup_source, 0444, tomtom_mcu_get_wakeup_source, NULL),
	__ATTR(wakeup_resolution, 0444, tomtom_mcu_get_wakeup_resolution, NULL),
	__ATTR(usb_poweron, 0664, tomtom_mcu_get_usb_poweron, tomtom_mcu_set_usb_poweron),
};

static int create_sysfs_interfaces(struct device *dev)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(attributes); i++)
		if (device_create_file(dev, attributes + i))
			goto error;
	return 0;

error:
	for (; i >= 0; i--)
		device_remove_file(dev, attributes + i);
	dev_err(dev, "%s:Unable to create interface\n", __func__);
	return -1;
}

static int remove_sysfs_interfaces(struct device *dev)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(attributes); i++)
		device_remove_file(dev, attributes + i);
	return 0;
}

static irqreturn_t host_irq_handler(int irq, void *data)
{
	int value;

	/* To avoid race condition between mcu and musb, switch USB VBUS as early as possible. */
	/* Check Power Present state change only */
	tomtom_mcu_irq_value = tomtom_mcu_reg_read(&mcu, REG_INTERRUPT);
	if (tomtom_mcu_debug_flag)
		printk(KERN_ERR "[APTS] HOST_INT = 0x%x\n", tomtom_mcu_irq_value);

	if (tomtom_mcu_irq_value < 0)
		return IRQ_HANDLED;

	if (tomtom_mcu_irq_value & MASK_INTERRUPT__POWER_PRN) {
		value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
		if (value >= 0) {
			if (value & MASK_STATUS1__POWER_PRN) {
				gpio_direction_output(mcu.pdata->vbus_on_gpio, 1);
				host_int_interrupt_event(HOST_INT_POWER_PRESENT);

				if (mcu.pdata->notify_vbus)
					mcu.pdata->notify_vbus(VBUS_PRESENT);
			} else {
				gpio_direction_output(mcu.pdata->vbus_on_gpio, 0);
				host_int_interrupt_event(HOST_INT_POWER_NOT_PRESENT);

				if (mcu.pdata->notify_vbus)
					mcu.pdata->notify_vbus(VBUS_REMOVED);
			}
		}
	}

	/* Inform power management framework that a wake up event has occured */
	pm_wakeup_event(&mcu.client->dev, 0);

	schedule_delayed_work(&mcu.work, msecs_to_jiffies(MCU_DETECTION_DELAY_TIME));

	return IRQ_HANDLED;
}

static int tomtom_mcu_usbpower_get_property(struct power_supply *psy,
					enum power_supply_property psp,
					union power_supply_propval *val)
{
	int value;

	switch (psp) {
		case POWER_SUPPLY_PROP_ONLINE:
			value = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
			if (value < 0)
				return -EIO;

			val->intval = (value & MASK_STATUS1__POWER_PRN) != 0;
			break;
	default:
		return -EINVAL;
	}

	return 0;
}

static enum power_supply_property tomtom_mcu_usbpower_props[] = {
		POWER_SUPPLY_PROP_ONLINE,
};

static int tomtom_mcu_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int ret;
	int irqval, status;
	bool usb_poweron = false;
	struct tomtom_mcu_pdata *pdata = client->dev.platform_data;

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_SMBUS_BYTE_DATA))
		return -EIO;

	mutex_init(&mcu.io_lock);

	mcu.client = client;
	mcu.pdata = pdata;
	INIT_DELAYED_WORK(&mcu.work, tomtom_mcu_worker_func);

	/*
	 * Read version register. If failuer, probe failed.
	 * We need version to determine wakeup timer resolution
	 */
	ret = tomtom_mcu_reg_read(&mcu, REG_APP_VERSION);
	if(ret < 0)
		return -ENODEV;
	else if(ret >= MCU_FW_VERSION_WAKEUP_RESOLUTION_1_SECOND)
		mcu.wakeup_timer_resolution = 1;
	else
		mcu.wakeup_timer_resolution = 8;

	/* Check if boot reason was USB PowerOn */
	irqval = tomtom_mcu_reg_read(&mcu, REG_INTERRUPT);
	if(irqval < 0)
		return -EIO;

	if (irqval & MASK_INTERRUPT__USB_WAKEUP) {
		status = tomtom_mcu_reg_read(&mcu, REG_STATUS1);
		if(status < 0)
			return -EIO;

		status = (status & (MASK_STATUS1__POWER_PRN | MASK_STATUS1__USB2_POWER_PRN));
		if (status == (MASK_STATUS1__POWER_PRN | MASK_STATUS1__USB2_POWER_PRN))
			usb_poweron = true;
	}


	/* If USB PowerOn is enabled, disable it */
	ret = tomtom_mcu_reg_read(&mcu, REG_USB_POWERON);
	if (ret < 0) {
		dev_err(&client->dev, "Failed to read USB poweron status\n");
		return ret;
	} else if (ret != 0) {
		ret = tomtom_mcu_reg_write(&mcu, REG_PASSWORD, PASSWORD);
		if (ret < 0) {
			dev_err(&client->dev, "Cannot prepare to set USB poweron status\n");
			return ret;
		}
		ret = tomtom_mcu_reg_write(&mcu, REG_USB_POWERON, 0x00);
		if (ret < 0) {
			dev_err(&client->dev, "Cannot set USB poweron status\n");
			return ret;
		}
	}

	ret = gpio_request_one(pdata->host_int_gpio, GPIOF_IN, "host_int");
	if (ret) {
		dev_err(&client->dev, "Failed to request HOST_INT pin GPIO\n");
		return ret;
	}

	ret = gpio_request_one(pdata->vbus_on_gpio, GPIOF_OUT_INIT_LOW, "vbus_on");
	if (ret) {
		dev_err(&client->dev, "Failed to request VBUS_ON pin GPIO\n");
		goto err_gpio_to_irq;
	}

	pdata->host_int_irq = gpio_to_irq(pdata->host_int_gpio);
	if (pdata->host_int_irq < 0) {
		dev_err(&client->dev, "Failed to get IRQ for HOST_INT GPIO\n");
		ret = pdata->host_int_irq;
		goto err_request_irq;
	}

	ret = request_threaded_irq(pdata->host_int_irq, NULL, host_irq_handler, IRQF_TRIGGER_FALLING, "host_int", NULL);
	if (ret) {
		dev_err(&client->dev, "Failed to request HOST_INT IRQ\n");
		goto err_request_threaded;
	}

	i2c_set_clientdata(client, &mcu);

	ret = create_sysfs_interfaces(&client->dev);
	if (ret < 0) {
		dev_err(&client->dev,
		   "device TOMTOM_MCU_DEV_NAME sysfs register failed\n");
		goto err_create_sysfs;
	}

	/* Register usb power supply so we can notify fuel gauge driver about external power changes */
	mcu.usbpower.type = POWER_SUPPLY_TYPE_MAINS;
	mcu.usbpower.name = id->name;
	mcu.usbpower.properties = tomtom_mcu_usbpower_props;
	mcu.usbpower.num_properties = ARRAY_SIZE(tomtom_mcu_usbpower_props);
	mcu.usbpower.get_property = tomtom_mcu_usbpower_get_property;
	mcu.usbpower.supplied_to = pdata->usbpower_supplicants;
	mcu.usbpower.num_supplicants = pdata->usbpower_num_supplicants;
	ret = power_supply_register(&client->dev, &mcu.usbpower);
	if (ret) {
		dev_err(&client->dev, "failed to register battery: %d\n", ret);
		goto err_power_supply;
	}

	/* When register call-back is set in the platform data, call it here */
	mcu.info.priv = &mcu;
	mcu.info.is_ac_online = tomtom_mcu_is_ac_online;
	if (pdata->register_callback != NULL)
		pdata->register_callback(&mcu.info);

	device_init_wakeup(&client->dev, 1);

	tomtom_mcu_init_flag = 1;
	schedule_delayed_work(&mcu.work, msecs_to_jiffies(MCU_DETECTION_DELAY_TIME));

	update_boot_reason(usb_poweron);
	return 0;

err_power_supply:
	remove_sysfs_interfaces(&client->dev);
err_create_sysfs:
err_request_threaded:
	free_irq(pdata->host_int_irq, NULL);
err_request_irq:
	gpio_free(pdata->vbus_on_gpio);
err_gpio_to_irq:
	gpio_free(pdata->host_int_gpio);

	return ret;
}

static int __devexit tomtom_mcu_remove(struct i2c_client *client)
{
	struct tomtom_mcu *mcu = i2c_get_clientdata(client);
	struct tomtom_mcu_pdata *pdata = mcu->client->dev.platform_data;

	device_init_wakeup(&client->dev, 0);

	cancel_delayed_work_sync(&mcu->work);

	if (pdata->unregister_callback != NULL)
		pdata->unregister_callback(&mcu->info);

	power_supply_unregister(&mcu->usbpower);

	remove_sysfs_interfaces(&client->dev);

	free_irq(pdata->host_int_irq, NULL);
	gpio_free(pdata->host_int_gpio);
	gpio_free(pdata->vbus_on_gpio);


	return 0;
}

#ifdef CONFIG_PM
static int tomtom_mcu_suspend(struct i2c_client *client, pm_message_t mesg)
{
	struct tomtom_mcu *mcu = i2c_get_clientdata(client);
	int ret;
	long time_elapsed_msec, value, nj;

	/* Set the wake up timer if needed */

	if (tomtom_mcu_wakeup_timeout_sec) {
		/* Calculate the time elapsed from start of suspend */
		nj = jiffies - tomtom_mcu_wakeup_timeout_set_time;
		time_elapsed_msec = jiffies_to_msecs(abs(nj));

		if (tomtom_mcu_debug_flag) {
			pr_err("MCU suspend jiffies elapsed = %ld\n", nj);
			pr_err("MCU suspend time elapsed in msec %ld\n",
					time_elapsed_msec);
			pr_err("MCU suspend wakeup timeout in sec = %ld\n",
					tomtom_mcu_wakeup_timeout_sec);
		}

		tomtom_mcu_time_to_sleep_sec =
			((tomtom_mcu_wakeup_timeout_sec * MSEC_PER_SEC)
			 - time_elapsed_msec) / MSEC_PER_SEC;

		if (tomtom_mcu_time_to_sleep_sec < mcu->wakeup_timer_resolution) {
			dev_err(&client->dev, "Not enough time left to sleep\n");
			return -ETIME;
		}

		/* Set wake up timer */
		value = tomtom_mcu_time_to_sleep_sec / mcu->wakeup_timer_resolution;
		if (value > MCU_WAKEUP_TIMER_MAX_TIMEOUT) {
			dev_err(&client->dev, "Cannot set wake up timer\n");
			return -EOVERFLOW;
		}

		if (tomtom_mcu_debug_flag) {
			pr_err("MCU suspend time to sleep in sec = %ld\n",
					tomtom_mcu_time_to_sleep_sec);
			pr_err("MCU suspend wakeup timer resolution = %d\n",
					mcu->wakeup_timer_resolution);
			pr_err("MCU suspend setting mcu wake up count to %ld\n",
					value);
		}

		tomtom_mcu_wakeup_timer_set_time = jiffies;

		ret = tomtom_mcu_reg_write(mcu, REG_WAKEUP_LSB, value & 0xFF);
		if (ret < 0) {
			dev_err(&client->dev, "Cannot set wake up timer\n");
			return ret;
		}

		ret = tomtom_mcu_reg_write(mcu, REG_WAKEUP_MSB,
				(value >> 8) & 0xFF);
		if (ret < 0) {
			dev_err(&client->dev, "Cannot set wake up timer\n");
			return ret;
		}
	}

	cancel_delayed_work_sync(&mcu->work);

	return 0;
}

static int tomtom_mcu_resume(struct i2c_client *client)
{
	struct tomtom_mcu *mcu = i2c_get_clientdata(client);

	tomtom_mcu_wakeup_timeout_sec = 0;
	tomtom_mcu_wakeup_timeout_set_time = 0;
	tomtom_mcu_wakeup_timer_set_time = 0;
	tomtom_mcu_time_to_sleep_sec = 0;

	/* Stop wakeup timer */
	tomtom_mcu_reg_write(mcu, REG_WAKEUP_LSB, 0);
	tomtom_mcu_reg_write(mcu, REG_WAKEUP_MSB, 0);

	schedule_delayed_work(&mcu->work, msecs_to_jiffies(MCU_DETECTION_DELAY_TIME));

	return 0;
}
#else
#define tomtom_mcu_suspend NULL
#define tomtom_mcu_resume  NULL
#endif /* CONFIG_PM */

static void tomtom_mcu_shutdown(struct i2c_client *client)
{
	struct tomtom_mcu *mcu = i2c_get_clientdata(client);
	int ret;

	/* Enable USB PowerOn */
	ret = tomtom_mcu_reg_write(mcu, REG_PASSWORD, PASSWORD);
	if (ret < 0) {
		dev_err(&client->dev, "Cannot prepare to set USB poweron status\n");
		return;
	}

	ret = tomtom_mcu_reg_write(mcu, REG_USB_POWERON, 0x01);
	if (ret < 0)
		dev_err(&client->dev, "Cannot set USB poweron status\n");

	return;
}

static const struct i2c_device_id tomtom_mcu_id[] = {
		{ TOMTOM_MCU_DEV_NAME, 0 },
		{ },
};

MODULE_DEVICE_TABLE(i2c, tomtom_mcu_id);

static struct i2c_driver tomtom_mcu_driver = {
	.driver  = {
		.name = TOMTOM_MCU_DEV_NAME,
		.owner = THIS_MODULE,
	},
	.probe   = tomtom_mcu_probe,
	.remove = __devexit_p(tomtom_mcu_remove),
	.suspend = tomtom_mcu_suspend,
	.resume  = tomtom_mcu_resume,
	.shutdown = tomtom_mcu_shutdown,
	.id_table = tomtom_mcu_id,
};

static int __init tomtom_mcu_init(void)
{
	return i2c_add_driver(&tomtom_mcu_driver);
}
subsys_initcall(tomtom_mcu_init);

static void __exit tomtom_mcu_exit(void)
{
	i2c_del_driver(&tomtom_mcu_driver);
}
module_exit(tomtom_mcu_exit);

MODULE_DESCRIPTION("TomTom mcu");
MODULE_LICENSE("GPL");
