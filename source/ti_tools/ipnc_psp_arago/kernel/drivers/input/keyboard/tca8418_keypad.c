/*
 *  Copyright (C) 2014 TomTom BV <http://www.tomtom.com/>
 *
 *  Driver for TCA8418 I2C keyboard (GPI interrupt Mode).
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/types.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/input/tca8418_keypad.h>

/* TCA8418 hardware limits */
#define TCA8418_MAX_ROWS	8
#define TCA8418_MAX_COLS	10

/* TCA8418 register offsets */
#define REG_CFG			0x01
#define REG_INT_STAT		0x02
#define REG_KEY_LCK_EC		0x03
#define REG_KEY_EVENT_A		0x04
#define REG_KEY_EVENT_B		0x05
#define REG_KEY_EVENT_C		0x06
#define REG_KEY_EVENT_D		0x07
#define REG_KEY_EVENT_E		0x08
#define REG_KEY_EVENT_F		0x09
#define REG_KEY_EVENT_G		0x0A
#define REG_KEY_EVENT_H		0x0B
#define REG_KEY_EVENT_I		0x0C
#define REG_KEY_EVENT_J		0x0D
#define REG_KP_LCK_TIMER	0x0E
#define REG_UNLOCK1		0x0F
#define REG_UNLOCK2		0x10
#define REG_GPIO_INT_STAT1	0x11
#define REG_GPIO_INT_STAT2	0x12
#define REG_GPIO_INT_STAT3	0x13
#define REG_GPIO_DAT_STAT1	0x14
#define REG_GPIO_DAT_STAT2	0x15
#define REG_GPIO_DAT_STAT3	0x16
#define REG_GPIO_DAT_OUT1	0x17
#define REG_GPIO_DAT_OUT2	0x18
#define REG_GPIO_DAT_OUT3	0x19
#define REG_GPIO_INT_EN1	0x1A
#define REG_GPIO_INT_EN2	0x1B
#define REG_GPIO_INT_EN3	0x1C
#define REG_KP_GPIO1		0x1D
#define REG_KP_GPIO2		0x1E
#define REG_KP_GPIO3		0x1F
#define REG_GPI_EM1		0x20
#define REG_GPI_EM2		0x21
#define REG_GPI_EM3		0x22
#define REG_GPIO_DIR1		0x23
#define REG_GPIO_DIR2		0x24
#define REG_GPIO_DIR3		0x25
#define REG_GPIO_INT_LVL1	0x26
#define REG_GPIO_INT_LVL2	0x27
#define REG_GPIO_INT_LVL3	0x28
#define REG_DEBOUNCE_DIS1	0x29
#define REG_DEBOUNCE_DIS2	0x2A
#define REG_DEBOUNCE_DIS3	0x2B
#define REG_GPIO_PULL1		0x2C
#define REG_GPIO_PULL2		0x2D
#define REG_GPIO_PULL3		0x2E

/* TCA8418 bit definitions */
#define CFG_AI			BIT(7)
#define CFG_GPI_E_CFG		BIT(6)
#define CFG_OVR_FLOW_M		BIT(5)
#define CFG_INT_CFG		BIT(4)
#define CFG_OVR_FLOW_IEN	BIT(3)
#define CFG_K_LCK_IEN		BIT(2)
#define CFG_GPI_IEN		BIT(1)
#define CFG_KE_IEN		BIT(0)

#define INT_STAT_CAD_INT	BIT(4)
#define INT_STAT_OVR_FLOW_INT	BIT(3)
#define INT_STAT_K_LCK_INT	BIT(2)
#define INT_STAT_GPI_INT	BIT(1)
#define INT_STAT_K_INT		BIT(0)

/* TCA8418 register masks */
#define KEY_LCK_EC_KEC		0x7
#define KEY_EVENT_CODE		0x7f
#define KEY_EVENT_VALUE		0x80

#define TCA8148_IRQ_MASK 0xFFFF

/* TCA8418 keypad irq source masks */
#define KEYPAD_IRQ_SOURCE_BLE 0x0200
#define KEYPAD_IRQ_SOURCE_MCU 0x0800
#define KEYPAD_IRQ_SOURCE_BUTTON 0x151F /* All keys including BAT_LOCK */

static unsigned int tomtom_keypad_irq_source = 0x0;

struct tca8418_keypad {
	struct i2c_client *client;
	struct input_dev *input;
	int irqnum;
	int always_on;
	uint16_t row_pinmask;
	uint16_t col_pinmask;
	uint16_t row_debmask;
	uint16_t col_debmask;
	struct tca8418_button buttons[0];
};

/*
 * Write a byte to the TCA8418
 */
static int tca8418_write_byte(struct tca8418_keypad *keypad_data,
				int reg, u8 val)
{
	int error;

	error = i2c_smbus_write_byte_data(keypad_data->client, reg, val);
	if (error < 0) {
		dev_err(&keypad_data->client->dev,
			"%s failed, reg: %d, val: %d, error: %d\n",
			__func__, reg, val, error);
		return error;
	}

	return 0;
}

/*
 * Read a byte from the TCA8418
 */
static int tca8418_read_byte(struct tca8418_keypad *keypad_data,
				int reg, u8 *val)
{
	int error;

	error = i2c_smbus_read_byte_data(keypad_data->client, reg);
	if (error < 0) {
		dev_err(&keypad_data->client->dev,
				"%s failed, reg: %d, error: %d\n",
				__func__, reg, error);
		return error;
	}

	*val = (u8)error;

	return 0;
}

static void tca8418_keys_scan(struct tca8418_keypad *keypad_data)
{
	struct input_dev *input = keypad_data->input;
	u8 reg_val1, reg_val2, reg_val3;
	u8 stat1, stat2, stat3;
	int reg_val, stat;
	int error, i, pin_index, pin_total;

	reg_val1 = reg_val2 = reg_val3 = 0;

	if (keypad_data->row_pinmask) {
		error = tca8418_read_byte(keypad_data, REG_GPIO_INT_STAT1, &reg_val1);
		if (error)
			return;
		error = tca8418_read_byte(keypad_data, REG_GPIO_DAT_STAT1, &stat1);
		if (error)
			return;
	}
	if (keypad_data->col_pinmask) {
		error = tca8418_read_byte(keypad_data, REG_GPIO_INT_STAT2, &reg_val2);
		if (error)
			return;
		error = tca8418_read_byte(keypad_data, REG_GPIO_DAT_STAT2, &stat2);
		if (error)
			return;
	}
	if ((keypad_data->col_pinmask >> 8)) {
		error = tca8418_read_byte(keypad_data, REG_GPIO_INT_STAT3, &reg_val3);
		if (error)
			return;
		error = tca8418_read_byte(keypad_data, REG_GPIO_DAT_STAT3, &stat3);
		if (error)
			return;
	}
	reg_val1 &= keypad_data->row_pinmask;
	reg_val = (((reg_val3 << 8) | reg_val2) & keypad_data->col_pinmask);
	tomtom_keypad_irq_source |= (reg_val1 | (reg_val << 8));

	/* R0IS - R7IS */
	for (i = 0, pin_index = 0; i < 8; i++) {
		if (reg_val1 & (1 << i)) {
			struct tca8418_button *button = &keypad_data->buttons[pin_index];
			unsigned int type = button->type ?: EV_KEY;
			int state = ((stat1 & (1 << i)) ? 1 : 0)
						^ button->active_low;
			input_event(input, type, button->code, !!state);
			input_report_key(input, button->code, !state);
		}
		if (keypad_data->row_pinmask & (1 << i))
			pin_index++;
	}

	/* C0IS - C9IS */
	pin_total = pin_index;
	stat = ((stat3 << 8) | stat2);
	for (i = 0, pin_index = 0; i < 10; i++) {
		if (reg_val & (1 << i)) {
			struct tca8418_button *button = &keypad_data->buttons[(pin_index + pin_total)];
			unsigned int type = button->type ?: EV_KEY;
			int state = ((stat & (1 << i)) ? 1 : 0)
						^ button->active_low;
			input_event(input, type, button->code, !!state);
			input_report_key(input, button->code, !state);
		}
		if (keypad_data->col_pinmask & (1 << i))
			pin_index++;
	}

	input_sync(input);
}

static ssize_t attr_get_irq_source(struct device *dev,
				struct device_attribute *attr,
				char *buf)
{
	const char *reason;
	unsigned keypad_irq_source = tomtom_keypad_irq_source & TCA8148_IRQ_MASK;

	if (keypad_irq_source & KEYPAD_IRQ_SOURCE_MCU)
		reason = "mcu";
	else if (keypad_irq_source & KEYPAD_IRQ_SOURCE_BLE)
		reason = "ble";
	else if (keypad_irq_source & KEYPAD_IRQ_SOURCE_BUTTON)
		reason = "button";
	else
		reason = "other";

	return scnprintf(buf, PAGE_SIZE, "%s", reason);
}

static DEVICE_ATTR(irq_source, S_IWUSR | S_IRUGO,
		attr_get_irq_source, NULL);

static struct attribute *tca8418_attrs[] = {
	&dev_attr_irq_source.attr,
	NULL
};

static const struct attribute_group tca8418_attr_group = {
	.attrs = tca8418_attrs,
};

static int tca8418_clear_interrupt_stat(struct tca8418_keypad *keypad_data, u8 int_stat)
{
	int error = 0;

	/* Clear interrupts state */
	error = tca8418_write_byte(keypad_data, REG_INT_STAT, int_stat);
	if (error)
		dev_err(&keypad_data->client->dev, "unable to clear REG_INT_STAT\n");

	return error;
}

/*
 * Threaded IRQ handler and this can (and will) sleep.
 */
static irqreturn_t tca8418_irq_handler(int irq, void *dev_id)
{
	struct tca8418_keypad *keypad_data = dev_id;
	u8 reg;
	int error;

	error = tca8418_read_byte(keypad_data, REG_INT_STAT, &reg);
	if (error) {
		dev_err(&keypad_data->client->dev,
			"unable to read REG_INT_STAT\n");
		return IRQ_NONE;
	}

	if (!reg)
		return IRQ_NONE;

	if (reg & INT_STAT_GPI_INT)
		tca8418_keys_scan(keypad_data);

	/* Clear all interrupts */
	tca8418_clear_interrupt_stat(keypad_data, 0xff);

	return IRQ_HANDLED;
}

/*
 * Configure the TCA8418 for keypad operation
 */
static int tca8418_configure(struct tca8418_keypad *keypad_data)
{
	int error;

	/* Clear all interrupts */
	tca8418_clear_interrupt_stat(keypad_data, 0xff);

	/* Write config register, if this fails assume device not present */
	error = tca8418_write_byte(keypad_data, REG_CFG, (CFG_OVR_FLOW_IEN | CFG_GPI_IEN));
	if (error < 0)
		return -ENODEV;

	error = tca8418_write_byte(keypad_data, REG_GPIO_INT_EN1, keypad_data->row_pinmask);
	if (error < 0)
		return -ENODEV;
	error = tca8418_write_byte(keypad_data, REG_GPIO_INT_EN2, (keypad_data->col_pinmask & 0xFF));
	if (error < 0)
		return -ENODEV;
	error = tca8418_write_byte(keypad_data, REG_GPIO_INT_EN3, (keypad_data->col_pinmask >> 8));
	if (error < 0)
		return -ENODEV;

	error = tca8418_write_byte(keypad_data, REG_DEBOUNCE_DIS1, keypad_data->row_debmask);
	if (error < 0)
		return -ENODEV;
	error = tca8418_write_byte(keypad_data, REG_DEBOUNCE_DIS2, (keypad_data->col_debmask & 0xFF));
	if (error < 0)
		return -ENODEV;
	error = tca8418_write_byte(keypad_data, REG_DEBOUNCE_DIS3, (keypad_data->col_debmask >> 8));
	if (error < 0)
		return -ENODEV;
	/* Setting COL2, bit5 to 1 to trigger low-to-high value interrupt */
	error = tca8418_write_byte(keypad_data, REG_GPIO_INT_LVL2, (1 << 4));
	if (error < 0)
		return -ENODEV;

	return error;
}

static int tca8418_keypad_enable(struct tca8418_keypad *keypad_data)
{
	int error;

	error = tca8418_configure(keypad_data);
	if (error) {
		dev_err(&keypad_data->client->dev,
			"Unable to config TCA8418 keypad %d\n", error);
	}

	return error;
}

static int tca8418_keypad_disable(struct tca8418_keypad *keypad_data)
{
	int error;

	error = tca8418_write_byte(keypad_data, REG_GPIO_INT_EN1, 0x0);
	if (error < 0)
		return -ENODEV;
	error = tca8418_write_byte(keypad_data, REG_GPIO_INT_EN2, 0x0);
	if (error < 0)
		return -ENODEV;
	error = tca8418_write_byte(keypad_data, REG_GPIO_INT_EN3, 0x0);
	if (error < 0)
		return -ENODEV;

	error = tca8418_write_byte(keypad_data, REG_CFG, 0x0);
	if (error < 0)
		return -ENODEV;

	return 0;
}

static int tca8418_keypad_probe(struct i2c_client *client,
				const struct i2c_device_id *id)
{
	struct tca8418_keypad_platform_data *pdata ;
	struct tca8418_keypad *keypad;
	struct input_dev *input;
	int i, error;
	unsigned int type;

	dev_info(&client->dev, "probe start.\n");

	/* Check i2c driver capabilities */
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_SMBUS_BYTE)) {
		dev_err(&client->dev, "%s adapter not supported\n",
			dev_driver_string(&client->adapter->dev));
		return -ENODEV;
	}

	pdata = client->dev.platform_data;
	if (!pdata) {
		dev_dbg(&client->dev, "no platform data\n");
		return -EINVAL;
	}

	keypad = kzalloc(sizeof(struct tca8418_keypad) +
				(pdata->nrow_buttons + pdata->ncol_buttons) * sizeof(struct tca8418_button),
				GFP_KERNEL);
	input = input_allocate_device();
	if (!keypad || !input) {
		error = -ENOMEM;
		goto fail1;
	}

	keypad->client = client;
	keypad->input = input;
	keypad->row_pinmask = pdata->row_pinmask;
	keypad->col_pinmask = pdata->col_pinmask;
	keypad->row_debmask = pdata->row_debmask;
	keypad->col_debmask = pdata->col_debmask;
	keypad->always_on = pdata->always_on;

	input->phys = "tca8418-keys/input0";
	input->name = client->name;
	input->dev.parent = &client->dev;

	input->id.bustype = BUS_HOST;
	input->id.vendor = 0x0001;
	input->id.product = 0x0001;
	input->id.version = 0x0100;

	/* Enable auto repeat feature of Linux input subsystem */
	if (pdata->rep)
		__set_bit(EV_REP, input->evbit);

	for (i = 0; i < pdata->nrow_buttons; i++) {
		keypad->buttons[i] = pdata->row_buttons[i];
		type = (pdata->row_buttons[i].type) ?: EV_KEY;
		input_set_capability(input, type, pdata->row_buttons[i].code);
	}
	for (i = 0; i < pdata->ncol_buttons; i++) {
		keypad->buttons[(i + pdata->nrow_buttons)] = pdata->col_buttons[i];
		type = (pdata->col_buttons[i].type) ?: EV_KEY;
		input_set_capability(input, type, pdata->col_buttons[i].code);
	}

	input_set_drvdata(input, keypad);

	/* Initialize the chip or fail if chip isn't present */
	error = tca8418_configure(keypad);
	if (error) {
		dev_dbg(&client->dev,
			"Unable to config TCA8418 keypad %d\n", error);
		goto fail1;
	}

	if (!keypad->always_on) {
		error = tca8418_keypad_disable(keypad);
		if (error) {
			dev_dbg(&client->dev,
				"Unable to disable TCA8418 keypad %d\n", error);
			goto fail1;
		}
	}

	keypad->irqnum = gpio_to_irq(pdata->interrupt_gpio);
	if (keypad->irqnum < 0) {
		dev_err(&client->dev, "Failed to get IRQ for DEVOSC_WAKE GPIO\n");
		error = keypad->irqnum;
		goto fail1;
	}

	error = request_threaded_irq(keypad->irqnum, NULL,
					tca8418_irq_handler,
					IRQF_TRIGGER_FALLING,
					"tca8418-keypad", keypad);
	if (error) {
		dev_dbg(&client->dev,
			"Unable to claim irq %d; error %d\n",
			keypad->irqnum, error);
		goto fail2;
	}

	error = input_register_device(input);
	if (error) {
		dev_dbg(&client->dev,
			"Unable to register input device, error: %d\n", error);
		goto fail2;
	}

	i2c_set_clientdata(client, keypad);

	error = sysfs_create_group(&client->dev.kobj, &tca8418_attr_group);
	if (error)
		goto fail2;

	dev_info(&client->dev, "%s: probed\n", TCA8418_NAME);

	return 0;

fail2:
	free_irq(keypad->irqnum, keypad);
fail1:
	input_free_device(input);
	kfree(keypad);
	return error;
}

static int tca8418_keypad_remove(struct i2c_client *client)
{
	struct tca8418_keypad *keypad = i2c_get_clientdata(client);

	free_irq(keypad->irqnum, keypad);
	sysfs_remove_group(&client->dev.kobj, &tca8418_attr_group);
	input_unregister_device(keypad->input);
	kfree(keypad);

	return 0;
}

#ifdef CONFIG_PM
static int tca8418_keypad_suspend(struct i2c_client *client, pm_message_t mesg)
{
	struct tca8418_keypad *keypad = i2c_get_clientdata(client);

	tomtom_keypad_irq_source = 0;

	if (!keypad->always_on)
		tca8418_keypad_enable(keypad);

	return 0;
}

static int tca8418_keypad_resume(struct i2c_client *client)
{
	struct tca8418_keypad *keypad = i2c_get_clientdata(client);

	if (!keypad->always_on)
		tca8418_keypad_disable(keypad);

	return 0;
}
#else
#define tca8418_keypad_suspend NULL
#define tca8418_keypad_resume  NULL
#endif /* CONFIG_PM */

static const struct i2c_device_id tca8418_id[] = {
	{ TCA8418_NAME, 8418 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, tca8418_id);

static struct i2c_driver tca8418_keypad_driver = {
	.driver = {
		.name	= TCA8418_NAME,
		.owner	= THIS_MODULE,
	},
	.probe		= tca8418_keypad_probe,
	.remove		= tca8418_keypad_remove,
	.suspend	= tca8418_keypad_suspend,
	.resume		= tca8418_keypad_resume,
	.id_table	= tca8418_id,
};

static int __init tca8418_keypad_init(void)
{
	return i2c_add_driver(&tca8418_keypad_driver);
}
subsys_initcall(tca8418_keypad_init);

static void __exit tca8418_keypad_exit(void)
{
	i2c_del_driver(&tca8418_keypad_driver);
}
module_exit(tca8418_keypad_exit);

MODULE_DESCRIPTION("Keypad driver for TCA8418");
MODULE_LICENSE("GPL");
