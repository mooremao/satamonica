/*
 * spi_csrgsd5xp.c - GSD5xp from CSR connected via SPI
 *
 * Copyright (C) 2014 TomTom
 *
 * General Public License version 2 (GPLv2)
 *
 * Notes:
 * 1. Driver based on the spi-tty-ds driver from Federico Vaga
 * 2. spi_bus_type symbol is a GPL only symbol
 * 3. TODO Not yet interrupt driven. GPS_DATA_READY might be possible to use
 *  as interrupt source. (This requires a additional patch and we still need polling
 *  as well. Polling is needed while uploading the patch. Once we see an interrupt,
 *  we don't need to poll anymore)
 * 4. When the GSD restarts during a SPI transfer, the data received after the reset
 *    might be bit shifted. This depends upon the state of the SPI clock during the
 *    reset. The bit shifted data can contain the the OK-to-send message. As result
 *    user-space sometimes fails wait for this message.
 *    As user space needs SPI to restart the GDS and because user space needs SPI to
 *    wait for OK-to-send message and reset duration is not always the same,
 *    preventing the SPI transfer during the GSD restart is not really feasible.
 *    As the data is bit-shifted until next SPI transfer one or more bits are getting
 *    lost at the end of the shifted data. So correcting the bit shifting is not an
 *    option either. Waiting for an interrupt is not an option either as the ROM FW
 *    version of the GSD does not support this feature (by default).
 *    To prevent the least number of bit shifted data, the solution perform SPI
 *    transfers of only one byte at the time. This results in only one byte being bit
 *    shifted. As first bytes received from GSD are idle bytes, this does not matter.
 *    User space will have to inform the driver about the up coming GSD restart by
 *    writing a '1' to 'restarting' sysfs entry. It needs to write a '0' after
 *    the the device has been started. This implementations keeps the driver free
 *    from parsing payload of OSP messages.
 *    As idle byte pattern consist of 2 bytes, we need switch between both of them.
 */

#include <linux/sched.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi_csrgsd5xp.h>
#include <linux/spinlock.h>
#include <linux/gpio.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/semaphore.h>

enum spi_csrgsd5xp_auto_hibernate_t {
	SPI_CSRGSD5XP__AUTO_HIBERNATE__DISABLED = 0,		/* do not hibernate GSD */
	SPI_CSRGSD5XP__AUTO_HIBERNATE__TTY_CLOSED = 1,		/* hibernate GSD when tty is closed */
	SPI_CSRGSD5XP__AUTO_HIBERNATE__TTY_CLOSED_SUSPSEND = 2, /* hibernate GSD when tty is closed and when SoC suspends (default) */
};

enum {
	RX_IDLE = 0, /* Receiving idle byte pattern. This is the only state in which will stop actively reading more bytes for some time */
	RX_HEADER_1,
	RX_LEN_1,
	RX_LEN_2,
	RX_PAYLOAD,
	RX_TRAILER_1,
	RX_TRAILER_2,
	RX_LOST_TRACK
};

/* Restart state. See note 4 for details */
enum {
	RST_NOT_RESTARTING,	/* Not restarting, normal operation */
	RST_IDLE_BYTE_1,	/* Send idle byte 1 */
	RST_IDLE_BYTE_2,	/* Send idle byte 2 */
	RST_IDLE_BYTE_2_DONE	/* Restart done, but we need to send idle byte 2 still */
};

#define N_GPIOS 4

#define MAX_PROBE_RETRIES 2000 /* 2000 * 1ms = 2 seconds */

#define SPI_CSRGSD5XP_MAX_MESSAGE_LEN (4+2047+2)	/* Max OSP message size: header+2^11-1+footer*/
static unsigned char idle_buffer[1024];

static int max_pending_bytes = SPI_CSRGSD5XP_MAX_MESSAGE_LEN;

static int poll_delay = 50; //m sec. Based on sirfgps_test
/* sleep_flag: if the GSD has been put to sleep manually, it can be awaken manually only...or through system restart. O: awake (default); 1: asleep */
int sleep_flag = 0;

module_param(max_pending_bytes, int, 0444);
module_param(poll_delay, int, 0444);

static unsigned int dev_count = 0;
static spinlock_t lock;

/*
 * GSD5xp device structure
 */
struct spi_csrgsd5xp_dev {
	struct spi_device *spi;

	unsigned int tty_minor;
	struct device *tty_dev;
	struct tty_port port;

	struct semaphore sem;
	spinlock_t lock;
	int active;
	int active_bytes_count;
	int active_suspended;
	wait_queue_head_t wait;

	struct timer_list poll_timer;

	struct gpio gpios[N_GPIOS];

	enum spi_csrgsd5xp_auto_hibernate_t auto_hibernate;

	int suspend_awake_state;
	int hibernate_cnt;
	int sleep;
	/* rx state-machine for GPS messages */
	int rx_state;
	/* length of the current GPS message */
	int rx_msg_len;
	/* local buffer for the extracted GPS messages */
	/* +1 is to cover the corner case where an header is split between 2 SPI messages */
	uint8_t rx_msg_buf[SPI_CSRGSD5XP_MAX_MESSAGE_LEN + 1];

	int restart_state;

#ifdef DEBUG_MAX_RX_TIME
	long last_rx_jiffies;
	long max_rx_time_msec;
#endif
};

/*
 * It is used to contain the information about a single SPI message between
 * this driver and the csrgsd5xp
 */
struct spi_csrgsd5xp_message {
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	struct spi_message	message;
	unsigned int len;
	uint8_t tx_buf[SPI_CSRGSD5XP_MAX_MESSAGE_LEN];
	uint8_t rx_buf[SPI_CSRGSD5XP_MAX_MESSAGE_LEN];

	struct list_head list;
};

#define tty_to_csrgsd5xp_dev(_ptr) ((struct spi_csrgsd5xp_dev *) dev_get_drvdata(_ptr->dev))

#define SPI_CSRGSD5XP_MINORS 1
static struct tty_driver *spi_csrgsd5xp_tty_driver = NULL;
static struct spi_csrgsd5xp_dev *spi_csrgsd5xp_all_dev[SPI_CSRGSD5XP_MINORS];

static int spi_csrgsd5xp_send_message(struct spi_csrgsd5xp_dev *csrgsd5xp_dev,
				const uint8_t *data, unsigned int count);

/*
 * This function removes all SPI transfers from a given SPI message and it
 * releases the memory.
 */
static void spi_csrgsd5xp_remove_transfers(struct spi_csrgsd5xp_message *csrgsd5xp_msg)
{
	struct spi_transfer *t, *tmp_t;

	list_for_each_entry_safe(t, tmp_t,&csrgsd5xp_msg->message.transfers, transfer_list) {
		spi_transfer_del(t);
		kfree(t);
	}
}

/*
 * Sends idle pattern to retrieve data back.
 *
 * In normal operation we send 1024 bytes to retrieve 1024 bytes back.
 * While restarting we send only 1 byte at the time. See note 4 for more details.
 */
static inline int spi_csrgsd5xp_send_idle_pattern(struct spi_csrgsd5xp_dev *csrgsd5xp_dev) {
	unsigned long flags;
	int ret;
	int restart_state;

	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	restart_state = csrgsd5xp_dev->restart_state;
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	switch (restart_state) {
		case RST_NOT_RESTARTING: /* Normal case */
			ret = spi_csrgsd5xp_send_message(csrgsd5xp_dev, idle_buffer, sizeof(idle_buffer));
			break;
		case RST_IDLE_BYTE_1:  /* Restarting. First idle byte */
			ret = spi_csrgsd5xp_send_message(csrgsd5xp_dev, idle_buffer, 1);
			csrgsd5xp_dev->restart_state = RST_IDLE_BYTE_2;
			break;
		case RST_IDLE_BYTE_2: /* Restarting. Second idle byte */
			ret = spi_csrgsd5xp_send_message(csrgsd5xp_dev, idle_buffer + 1, 1);
			csrgsd5xp_dev->restart_state = RST_IDLE_BYTE_1;
			break;
		case RST_IDLE_BYTE_2_DONE: /* Restart done but we need to prevent to send same idle byte twice */
			ret = spi_csrgsd5xp_send_message(csrgsd5xp_dev, idle_buffer + 1, sizeof(idle_buffer) - 1);
			csrgsd5xp_dev->restart_state = RST_NOT_RESTARTING;
			break;
	}

	return ret;
}

#ifdef DEBUG_LOST_RX_MESSAGE

static void spi_csrgsd5xp_dump_rx_message(struct spi_csrgsd5xp_message *csrgsd5xp_msg)
{
	int i;
	int offset = 0;
	char buf[3*16+10];
	int byte = 0;
	static int dump_count = 0;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = csrgsd5xp_msg->csrgsd5xp_dev;

	if(dump_count >= 10)
		return;
	dump_count++;

	dev_info(&csrgsd5xp_dev->spi->dev, "RX data: (%d)\n", dump_count);

	for (i=0; i < csrgsd5xp_msg->len; i++) {
		offset += snprintf(buf + offset, sizeof(buf)-offset, "%02x ", csrgsd5xp_msg->rx_buf[i]);
		byte++;
		if (byte >= 16) {
			dev_info(&csrgsd5xp_dev->spi->dev, "%s\n", buf);
			offset = 0;
			byte=0;
		}
	}

	if (byte != 0)
		dev_info(&csrgsd5xp_dev->spi->dev, "%s\n", buf);
}

#else /* DEBUG_LOST_RX_MESSAGE */

static void inline spi_csrgsd5xp_dump_rx_message(struct spi_csrgsd5xp_message *csrgsd5xp_msg) { }

#endif /* DEBUG_LOST_RX_MESSAGE */

/*
 * Handles the received data. Restarts poll timer or keeps reading for more data
 */
static void spi_csrgsd5xp_handler_rx_message(struct tty_struct *tty,
				   struct spi_csrgsd5xp_message *csrgsd5xp_msg)
{
	unsigned long flags;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = csrgsd5xp_msg->csrgsd5xp_dev;
	int buf_idx, i;
#ifdef DEBUG_MAX_RX_TIME
	long diff;
#endif

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d\n", __func__, __LINE__);

#ifdef DEBUG_MAX_RX_TIME
	diff = ((long) jiffies) - csrgsd5xp_dev->last_rx_jiffies;
	csrgsd5xp_dev->last_rx_jiffies = (long) jiffies;

	diff = diff * 1000 / HZ;
	if (diff > csrgsd5xp_dev->max_rx_time_msec) {
		dev_info(&csrgsd5xp_dev->spi->dev, "%s: new max_rx_time_msec: %ld\n", __func__, diff);
		csrgsd5xp_dev->max_rx_time_msec = diff;
	}
#endif

	buf_idx = 0;
	/* loop over the SPI buffer to extract valid GPS messages (i.e. between 0xA0,0xA2 and 0xB0,0xB3) */
	for (i=0; i < csrgsd5xp_msg->len; i++) {
		switch (csrgsd5xp_dev->rx_state) {
			/* Received idle byte. Expect more idle bytes or first byte of header. */
			case RX_IDLE:
				if (csrgsd5xp_msg->rx_buf[i] == 0xA0) {
					csrgsd5xp_dev->rx_state = RX_HEADER_1;
				} else if (csrgsd5xp_msg->rx_buf[i] != 0xA7 && csrgsd5xp_msg->rx_buf[i] != 0xB4) {
					dev_info(&csrgsd5xp_dev->spi->dev, "%s: malformed idle bytes - expected 0xA0, 0xA7 or 0xB4 found: 0x%02X\n", __func__, csrgsd5xp_msg->rx_buf[i]);
					spi_csrgsd5xp_dump_rx_message(csrgsd5xp_msg);
					csrgsd5xp_dev->rx_state = RX_LOST_TRACK;
					csrgsd5xp_dev->rx_msg_len = 0;
				}
				break;
			/* first byte of the header received */
			case RX_HEADER_1:
				if (csrgsd5xp_msg->rx_buf[i] == 0xA2) {
					/* insert the header in the local buffer */
					csrgsd5xp_dev->rx_msg_buf[buf_idx++] = 0xA0;
					csrgsd5xp_dev->rx_msg_buf[buf_idx++] = 0xA2;
					csrgsd5xp_dev->rx_state = RX_LEN_1;
				}
				/* if 0xA0 again, remain in the same state */
				else if (csrgsd5xp_msg->rx_buf[i] != 0xA0) {
					/* something is wrong, reset the state machine */
					dev_info(&csrgsd5xp_dev->spi->dev, "%s: malformed header - expected: 0xA2 found: 0x%02X\n", __func__, csrgsd5xp_msg->rx_buf[i]);
					spi_csrgsd5xp_dump_rx_message(csrgsd5xp_msg);
					csrgsd5xp_dev->rx_state = RX_LOST_TRACK;
					csrgsd5xp_dev->rx_msg_len = 0;
				}
				break;
			/* upper part of the length */
			case RX_LEN_1:
				csrgsd5xp_dev->rx_msg_len = (csrgsd5xp_msg->rx_buf[i] << 8);
				csrgsd5xp_dev->rx_msg_buf[buf_idx++] = csrgsd5xp_msg->rx_buf[i];
				csrgsd5xp_dev->rx_state = RX_LEN_2;
				break;
			/* lower part of the length */
			case RX_LEN_2:
				csrgsd5xp_dev->rx_msg_len += (csrgsd5xp_msg->rx_buf[i]) + 2; /* add 2 bytes for the checksum */
				csrgsd5xp_dev->rx_msg_buf[buf_idx++] = csrgsd5xp_msg->rx_buf[i];
				csrgsd5xp_dev->rx_state = RX_PAYLOAD;
				break;
			/* inside a message payload */
			case RX_PAYLOAD:
				if ((csrgsd5xp_msg->len-i) > csrgsd5xp_dev->rx_msg_len) {
					/* the first byte of the trailer is expected after msg_len bytes from the length */
					if (csrgsd5xp_msg->rx_buf[csrgsd5xp_dev->rx_msg_len+i] != 0xB0) {
						/* something is wrong, reset the state machine */
						dev_info(&csrgsd5xp_dev->spi->dev, "%s: malformed message - expected: 0xB0 found: 0x%02X\n", __func__, csrgsd5xp_msg->rx_buf[csrgsd5xp_dev->rx_msg_len+i]);
						spi_csrgsd5xp_dump_rx_message(csrgsd5xp_msg);
						csrgsd5xp_dev->rx_state = RX_LOST_TRACK;
						csrgsd5xp_dev->rx_msg_len = 0;
					}
					else {
						/* first byte of the trailer found: copy the message including it */
						csrgsd5xp_dev->rx_state = RX_TRAILER_1;
						memcpy(csrgsd5xp_dev->rx_msg_buf + buf_idx, csrgsd5xp_msg->rx_buf + i, csrgsd5xp_dev->rx_msg_len + 1);
						buf_idx += csrgsd5xp_dev->rx_msg_len + 1;
						i += csrgsd5xp_dev->rx_msg_len;
					}
				}
				else {
					/* the payload doesn't finish in this SPI message: copy what is available */
					memcpy(csrgsd5xp_dev->rx_msg_buf + buf_idx, csrgsd5xp_msg->rx_buf + i, csrgsd5xp_msg->len-i);
					buf_idx += csrgsd5xp_msg->len-i;
					csrgsd5xp_dev->rx_msg_len -= csrgsd5xp_msg->len-i;
					i = csrgsd5xp_msg->len;
				}
				break;
			/* first byte of the trailer received */
			case RX_TRAILER_1:
				if (csrgsd5xp_msg->rx_buf[i] == 0xB3) {
					/* second byte of the trailer found -> message complete */
					csrgsd5xp_dev->rx_msg_buf[buf_idx++] = 0xB3;
					csrgsd5xp_dev->rx_state = RX_TRAILER_2;
					csrgsd5xp_dev->rx_msg_len = 0;
				}
				else {
					/* something is wrong, reset the state machine */
					dev_info(&csrgsd5xp_dev->spi->dev, "%s: malformed trailer - expected: 0xB3 found: 0x%02X\n", __func__, csrgsd5xp_msg->rx_buf[i]);
					spi_csrgsd5xp_dump_rx_message(csrgsd5xp_msg);
					csrgsd5xp_dev->rx_state = RX_LOST_TRACK;
					csrgsd5xp_dev->rx_msg_len = 0;
				}
				break;
			/* second byte of the trailer received. Expect header or idle. Prevent waiting poll timeout in this state. */
			case RX_TRAILER_2:
				if (csrgsd5xp_msg->rx_buf[i] == 0xA0) {
					csrgsd5xp_dev->rx_state = RX_HEADER_1;
				} else if (csrgsd5xp_msg->rx_buf[i] == 0xA7 || csrgsd5xp_msg->rx_buf[i] == 0xB4) {
					csrgsd5xp_dev->rx_state = RX_IDLE;
				} else {
					dev_info(&csrgsd5xp_dev->spi->dev, "%s: malformed trailer - expected: 0xA0, 0xA7 or 0xB4 found: 0x%02X\n", __func__, csrgsd5xp_msg->rx_buf[i]);
					spi_csrgsd5xp_dump_rx_message(csrgsd5xp_msg);
					csrgsd5xp_dev->rx_state = RX_LOST_TRACK;
					csrgsd5xp_dev->rx_msg_len = 0;
				}
				break;

			/* we are lost. Wait for next header and try to pick up from that point. */
			case RX_LOST_TRACK:
				if (csrgsd5xp_msg->rx_buf[i] == 0xA0) {
					if (csrgsd5xp_dev->rx_msg_len > 0) {
						dev_info(&csrgsd5xp_dev->spi->dev, "%s: discarded %d bytes. in transmit: %d\n", __func__, csrgsd5xp_dev->rx_msg_len, csrgsd5xp_dev->active_bytes_count);
						if (csrgsd5xp_dev->rx_msg_len > 100) {
							spi_csrgsd5xp_dump_rx_message(csrgsd5xp_msg);
						}
					}
					csrgsd5xp_dev->rx_state = RX_HEADER_1;
					csrgsd5xp_dev->rx_msg_len = 0;
				} else {
					csrgsd5xp_dev->rx_msg_len++;
				}
				break;
		}
	}

	/* if we are parsing a message, request more data by sending idle patterns */
	if (csrgsd5xp_dev->rx_state != RX_IDLE && csrgsd5xp_dev->rx_state != RX_LOST_TRACK) {
		spi_csrgsd5xp_send_idle_pattern(csrgsd5xp_dev);
	} else {
		dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d found idle pattern in RX\n", __func__, __LINE__);

		/*
		 * Reactivate poll timer when we are still active.
		 */
		spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
		if (csrgsd5xp_dev->active)
			mod_timer(&csrgsd5xp_dev->poll_timer, jiffies + poll_delay*HZ/1000);
		spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);
	}

	/* if something has been put in the local buffer, send it to the tty layer */
	if (buf_idx) {
		int cnt = tty_insert_flip_string(tty, csrgsd5xp_dev->rx_msg_buf, buf_idx);
		/* if the tty buffer is full, some data might be lost */
		if (cnt != buf_idx)
			printk(KERN_WARNING "%s: fail to copy tty data, some messages might be lost\n", __func__);

		tty_flip_buffer_push(tty);
	}

	return;
}

/*
 * This function is the SPI callback, invoked when message is completed
 */
static void spi_csrgsd5xp_message_complete(void *context)
{
	struct spi_csrgsd5xp_message *csrgsd5xp_msg = context;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = csrgsd5xp_msg->csrgsd5xp_dev;
	struct tty_struct *tty = tty_port_tty_get(&csrgsd5xp_msg->csrgsd5xp_dev->port);
	unsigned long flags;

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d\n", __func__, __LINE__);

	/* Push incoming data to user space thought TTY interface */
	if (tty)
		spi_csrgsd5xp_handler_rx_message(tty, csrgsd5xp_msg);

	/* Decrement active byte counter */
	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	csrgsd5xp_dev->active_bytes_count -= csrgsd5xp_msg->len;
	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d [%d]\n", __func__, __LINE__,
		 csrgsd5xp_dev->active_bytes_count);
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	wake_up_all(&csrgsd5xp_dev->wait);

	if (tty)
		/* Wake up queue (wait until all transfered) */
		tty_wakeup(tty);

	tty_kref_put(tty);

	/* Clean up memory */
	spi_csrgsd5xp_remove_transfers(csrgsd5xp_msg);
	kfree(csrgsd5xp_msg);
}

/*
 * This function converts a given stream of bytes in an SPI message
 */
static struct spi_csrgsd5xp_message *spi_csrgsd5xp_message_build(const uint8_t *data,
						     unsigned int len, void (*complete)(void *context))
{
	struct spi_csrgsd5xp_message *csrgsd5xp_msg;
	struct spi_transfer *transfer;
	int err;

	/* Message length cannot be greater than the maximum message length */
	if (len >= SPI_CSRGSD5XP_MAX_MESSAGE_LEN)
		return ERR_PTR(-EINVAL);

	csrgsd5xp_msg = kmalloc(sizeof(struct spi_csrgsd5xp_message),
			GFP_KERNEL | GFP_ATOMIC);
	if (!csrgsd5xp_msg)
		return ERR_PTR(-ENOMEM);

	spi_message_init(&csrgsd5xp_msg->message);

	memcpy(csrgsd5xp_msg->tx_buf, data, len);
	csrgsd5xp_msg->len = len;

	transfer = kzalloc(sizeof(struct spi_transfer), GFP_KERNEL | GFP_ATOMIC);
	if (!transfer) {
		err = -ENOMEM;
		goto err_trans_alloc;
	}

	transfer->len = len;
	transfer->tx_buf = csrgsd5xp_msg->tx_buf;
	transfer->rx_buf = csrgsd5xp_msg->rx_buf;

	spi_message_add_tail(transfer, &csrgsd5xp_msg->message);

	csrgsd5xp_msg->message.context = csrgsd5xp_msg;
	csrgsd5xp_msg->message.complete = complete;

	return csrgsd5xp_msg;

err_trans_alloc:
	spi_csrgsd5xp_remove_transfers(csrgsd5xp_msg);
	kfree(csrgsd5xp_msg);

	return ERR_PTR(err);
}

/*
 * This function is an helper that build an SPI message and it sends the message
 * immediately on the SPI bus
 */
static int spi_csrgsd5xp_send_message(struct spi_csrgsd5xp_dev *csrgsd5xp_dev,
				const uint8_t *data, unsigned int count)
{
	struct spi_csrgsd5xp_message *csrgsd5xp_msg;
	unsigned long flags;
	int rv;
	int active;

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d\n", __func__, __LINE__);

	/* Build SPI message */
	csrgsd5xp_msg = spi_csrgsd5xp_message_build(data, count, spi_csrgsd5xp_message_complete);
	if (IS_ERR(csrgsd5xp_msg)) {
		dev_err(&csrgsd5xp_dev->spi->dev, "%s: Cannot build message (%ld)\n",
			__func__, PTR_ERR(csrgsd5xp_msg));
		return PTR_ERR(csrgsd5xp_msg);
	}
	csrgsd5xp_msg->csrgsd5xp_dev = csrgsd5xp_dev;

	/* Check active and increase active bytes count while having the lock.*/
	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	active = csrgsd5xp_dev->active;
	if (active)
	{
		csrgsd5xp_dev->active_bytes_count += csrgsd5xp_msg->len;
		dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d [%d]\n", __func__, __LINE__,
			 csrgsd5xp_dev->active_bytes_count);
	}
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	//Return 0 (nothing sent) when not active anymore
	if (!active) {
		rv = 0;
		goto not_active;
	}

	/* Send SPI message */
	rv = spi_async(csrgsd5xp_dev->spi, &csrgsd5xp_msg->message);
	if (rv) {
		dev_err(&csrgsd5xp_dev->spi->dev, "%s: Cannot send message (%d)\n",
			__func__, rv);
		goto err_spi_async;
	}

	return count;

err_spi_async:
not_active:
	spi_csrgsd5xp_remove_transfers(csrgsd5xp_msg);
	kfree(csrgsd5xp_msg);
	return rv;
}

/*
 * Returns 1 when device is awake.
 */
static int spi_csrgsd5xp_is_awake(struct spi_csrgsd5xp_dev *csrgsd5xp_dev) {
	struct csrgsd5xp_platform_data* pdata = (struct csrgsd5xp_platform_data*) csrgsd5xp_dev->spi->dev.platform_data;

	return gpio_get_value(pdata->gps_wakeup_gpio);
}

/*
 * Function to pulse the on off pin. This could change the system from power off to on or initiates transaction
 * to hibernate mode.
 *
 * csrgsd5xp_dev->sem must be down before this function is called
 */
static void spi_csrgsd5xp_pulse_on_off(struct spi_csrgsd5xp_dev *csrgsd5xp_dev) {
	unsigned int on_off_gpio;
	struct csrgsd5xp_platform_data* pdata = (struct csrgsd5xp_platform_data*) csrgsd5xp_dev->spi->dev.platform_data;

	on_off_gpio = pdata->gps_on_off_gpio;

	/* According to the gsd5x spec any transition on on_off pin
	 * is required to hold the state for 60us minimum for
	 * guaranteed detection. Making 100us here for safe side
	 */
	gpio_set_value(on_off_gpio, 0);
	usleep_range(200, 200);
	gpio_set_value(on_off_gpio, 1);
	usleep_range(200, 200);
	gpio_set_value(on_off_gpio, 0);
}

/*
 * This function sends idle pattern to 5xp until it reports not awake anymore.
 * It like sending idle pattern, but we use spi_sync, instead of spi_async and we ignore all received data
 */
static int spi_csrgsd5xp_wait_for_sleeping(struct spi_csrgsd5xp_dev *csrgsd5xp_dev)
{
	struct spi_csrgsd5xp_message *csrgsd5xp_msg;
	unsigned long flags;
	int rv;
	unsigned long timeout;
#ifdef VERBOSE_DEBUG
	unsigned int i;
	int n_idle_bytes = 0;
	int n_data_bytes = 0;
	int offset = 0;
	char buf[3*16+10];
#endif /* VERBOSE_DEBUG */

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d\n", __func__, __LINE__);

	/* Build SPI message */
	csrgsd5xp_msg = spi_csrgsd5xp_message_build(idle_buffer, sizeof(idle_buffer), NULL);
	if (IS_ERR(csrgsd5xp_msg)) {
		dev_err(&csrgsd5xp_dev->spi->dev, "%s: Cannot build message (%ld)\n",
			__func__, PTR_ERR(csrgsd5xp_msg));
		return PTR_ERR(csrgsd5xp_msg);
	}
	csrgsd5xp_msg->csrgsd5xp_dev = csrgsd5xp_dev;

	/* Increment active bytes counter */
	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	csrgsd5xp_dev->active_bytes_count += csrgsd5xp_msg->len;
	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d [%d]\n", __func__, __LINE__,
		 csrgsd5xp_dev->active_bytes_count);
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	/* Send SPI message */
	timeout = jiffies + HZ*10; /* 10 seconds should be enough. */
	while (spi_csrgsd5xp_is_awake(csrgsd5xp_dev)) {
		/* Check timeout */
		if (!time_before(jiffies, timeout)) {
			dev_err(&csrgsd5xp_dev->spi->dev, "%s:%d hibernate timeout.\n", __func__, __LINE__);
			rv = -ETIME;
			goto err_timeout;
		}

		dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d hibernating...\n", __func__, __LINE__);

		rv = spi_sync(csrgsd5xp_dev->spi, &csrgsd5xp_msg->message);
		if (rv) {
			dev_err(&csrgsd5xp_dev->spi->dev, "%s: Cannot send message (%d)\n",
				__func__, rv);

			goto err_spi_sync;
		}

#ifdef VERBOSE_DEBUG
		for (i = 0; i < csrgsd5xp_msg->len; i++) {
			if (csrgsd5xp_msg->rx_buf[i] != 0xA7 && csrgsd5xp_msg->rx_buf[i] != 0xB4) {
				if (n_idle_bytes != 0) {
					dev_vdbg(&csrgsd5xp_dev->spi->dev, "Skipped %d idle bytes\n", n_idle_bytes);
					n_idle_bytes=0;
				}
				offset += snprintf(buf + offset, sizeof(buf)-offset, "%02x ", csrgsd5xp_msg->rx_buf[i]);
				n_data_bytes++;
				if (n_data_bytes >= 16) {
					dev_vdbg(&csrgsd5xp_dev->spi->dev, "Data: %s\n", buf);
					offset = 0;
					n_data_bytes=0;
				}
			} else {
				if (n_data_bytes != 0) {
					dev_vdbg(&csrgsd5xp_dev->spi->dev, "Data: %s\n", buf);
					offset = 0;
					n_data_bytes=0;
				}
				n_idle_bytes++;
			}
		}
#endif /* VERBOSE_DEBUG */
	}

	csrgsd5xp_dev->hibernate_cnt++;

	rv = 0;

err_spi_sync:
err_timeout:
#ifdef VERBOSE_DEBUG
	if (n_idle_bytes != 0)
		dev_vdbg(&csrgsd5xp_dev->spi->dev, "Skipped %d idle bytes\n", n_idle_bytes);

	if (n_data_bytes != 0)
		dev_vdbg(&csrgsd5xp_dev->spi->dev, "Data: %s\n", buf);
#endif /* VERBOSE_DEBUG */

	/* Decrement active bytes counter */
	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	csrgsd5xp_dev->active_bytes_count -= csrgsd5xp_msg->len;
	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d [%d]\n", __func__, __LINE__,
			csrgsd5xp_dev->active_bytes_count);
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	spi_csrgsd5xp_remove_transfers(csrgsd5xp_msg);
	kfree(csrgsd5xp_msg);

	return rv;
}

/**
 * Helper function which returns 1 when active bytes count is zero. Can be used as condition
 * for wait_event() call.
 */
static int spi_csrgsd5xp_active_bytes_zero(struct spi_csrgsd5xp_dev *csrgsd5xp_dev) {
	unsigned long flags;
	int zero;

	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	zero = csrgsd5xp_dev->active_bytes_count == 0;
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	return zero;
}

/**
 * Resets the device by toggling the reset pin.
 *
 * csrgsd5xp_dev->sem must be down before this function is called
 */
static void spi_csrgsd5xp_reset(struct spi_csrgsd5xp_dev *csrgsd5xp_dev) {
	struct csrgsd5xp_platform_data* pdata;
	unsigned int reset_gpio;
	int reset_active_high;

	dev_dbg(&csrgsd5xp_dev->spi->dev, "Resetting device\n");

	pdata = (struct csrgsd5xp_platform_data*) csrgsd5xp_dev->spi->dev.platform_data;

	reset_gpio = pdata->gps_reset_gpio;
	reset_active_high = pdata->gps_reset_active_high;

	/* Toggle reset pin . Timing follows Berlin project */
	gpio_set_value(reset_gpio, reset_active_high ? 1 : 0);
	mdelay(20);
	gpio_set_value(reset_gpio, reset_active_high ? 0 : 1);
	mdelay(500);
}

/**
 * Puts device in hibernate mode when not already
 *
 * csrgsd5xp_dev->sem must be down before this function is called
 */
static void spi_csrgsd5xp_put_in_hibernate(struct spi_csrgsd5xp_dev *csrgsd5xp_dev)
{
	int err;

	spi_csrgsd5xp_pulse_on_off(csrgsd5xp_dev);

	/* 5xp continues to run until SPI transmit/output buffers are emptied */
	err = spi_csrgsd5xp_wait_for_sleeping(csrgsd5xp_dev);
	if (err) {
		/*
		 * Failed to wait for sleep. Device does not response, so reset it.
		 * This will get the device in a known state (powered off).
		 * Yes, afterwards the TTFF will be long, but better than having
		 * a device not working at all and draining power.
		 */
		dev_err(&csrgsd5xp_dev->spi->dev, "%s:%d hibernate failed. Resetting.\n", __func__, __LINE__);
		spi_csrgsd5xp_reset(csrgsd5xp_dev);
	}

	/* Reset state machines */
	csrgsd5xp_dev->rx_state = RX_LOST_TRACK; /* Wait for next header */
	csrgsd5xp_dev->rx_msg_len = 0;
	csrgsd5xp_dev->restart_state = RST_NOT_RESTARTING;
}

/* * * * TTY Operations * * * */

/*
 * The kernel invokes this function when a program opens the TTY interface of
 * this driver
 */
static int spi_csrgsd5xp_tty_open(struct tty_struct * tty, struct file * filp)
{
	int rv;
	unsigned long flags;
	int active;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = tty_to_csrgsd5xp_dev(tty);

	dev_dbg(&csrgsd5xp_dev->spi->dev, "%s:%d\n", __func__, __LINE__);

	/**
	 * Prevent multiple openings by checking the tty counter
	 * Note: this is necessary because the driver has some issues in case of more than one open file descriptor using the tty:
	 *  - Rx data would not be properly split between the instances
	 *  - in case the tty subsystem uses the tty_reopen call, the new opened session doesn't exchange any data
	 *  - the use of tty_reopen also causes a pending kref to remain active after the device is closed, preventing successive openings
	 */
	if (tty->count > 1)
		return -EBUSY;

	/**
	 * Down semaphore to exclusively activate chip and SPI comm when we are the first
	 * to open the device.
	 */
	if (down_interruptible(&csrgsd5xp_dev->sem))
		 return -ERESTARTSYS;

	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	csrgsd5xp_dev->active++;
	active = csrgsd5xp_dev->active;
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	if (active == 1)
	{
		/*
		 * Initialize waiting queue. It is initialized here because the queue
		 * it is used only on an opened TTY port
		 */
		init_waitqueue_head(&csrgsd5xp_dev->wait);

		/* Configure the SPI bus */
		csrgsd5xp_dev->spi->mode = 1;
		csrgsd5xp_dev->spi->bits_per_word = 8;
		csrgsd5xp_dev->spi->max_speed_hz = 2000000;
		spi_setup(csrgsd5xp_dev->spi);

		/* If not yet awake, power it on here. */
		if (!spi_csrgsd5xp_is_awake(csrgsd5xp_dev)) {
			spi_csrgsd5xp_pulse_on_off(csrgsd5xp_dev);
		}
	}

	rv = tty_port_open(&csrgsd5xp_dev->port, tty, filp);
	if (rv)
	{
		spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
		csrgsd5xp_dev->active--;
		active = csrgsd5xp_dev->active;
		spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

		if (active == 0) {
			if (csrgsd5xp_dev->auto_hibernate != SPI_CSRGSD5XP__AUTO_HIBERNATE__DISABLED) {
				if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev))
					spi_csrgsd5xp_put_in_hibernate(csrgsd5xp_dev);
			}
		}

		goto err_tty_port_open;
	}

	/* Start poll timer to start polling data */
	if (active == 1)
	{
		csrgsd5xp_dev->poll_timer.expires = jiffies + poll_delay*HZ/1000;
		add_timer(&csrgsd5xp_dev->poll_timer);
	}

#ifdef DEBUG_MAX_RX_TIME
	csrgsd5xp_dev->last_rx_jiffies = jiffies;
	csrgsd5xp_dev->max_rx_time_msec = 0;
	dev_info(&csrgsd5xp_dev->spi->dev, "%s: Resetting max_rx_time_msec\n", __func__);
#endif

	rv = 0;

err_tty_port_open:
	up(&csrgsd5xp_dev->sem);

	return rv;
}

/*
 * The kernel invokes this function when a program closes the TTY interface of
 * this driver
 *
 * Do not use *_interruptible calls here, process might calls this function while
 * being signaled to terminate and this all _interruptible calls will interrupt.
 */
static void spi_csrgsd5xp_tty_close(struct tty_struct * tty, struct file * filp)
{
	unsigned long flags;
	int active;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = tty_to_csrgsd5xp_dev(tty);

	dev_dbg(&csrgsd5xp_dev->spi->dev, "%s:%d\n", __func__, __LINE__);

	/**
	 * Exit immediately if there is one more instance active (i.e. when called after an EBUSY in spi_csrgsd5xp_tty_open)
	 * The de-init should only happen for the very last tty context
	 */
	if (tty->count > 1)
		return;

	/*
	 * Down semaphore to exclusively de-activate chip and SPI comm when we are the last one
	 * having the device open.
	 */
	down(&csrgsd5xp_dev->sem);

	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	csrgsd5xp_dev->active--;
	active = csrgsd5xp_dev->active;
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	if (active == 0) {
		del_timer_sync(&csrgsd5xp_dev->poll_timer);

		wait_event(csrgsd5xp_dev->wait, spi_csrgsd5xp_active_bytes_zero(csrgsd5xp_dev));

		/* Once here, we are sure active_bytes_count will not be increased anymore since
		 * we only increase it when active is 1 (and we check and increase bytes count while
		 * holding the lock)
		 */

		/* If still awake, put it in hibernate mode */
		if (csrgsd5xp_dev->auto_hibernate != SPI_CSRGSD5XP__AUTO_HIBERNATE__DISABLED) {
			if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev))
				spi_csrgsd5xp_put_in_hibernate(csrgsd5xp_dev);
		}
	}

	up(&csrgsd5xp_dev->sem);

	tty_ldisc_flush(tty);
	tty_port_close(&csrgsd5xp_dev->port, tty, filp);

	wake_up(&csrgsd5xp_dev->port.open_wait);
	wake_up(&csrgsd5xp_dev->port.close_wait);
}

/*
 * This function return the number of bytes that this driver can accept. There
 * is not a real limit because se redirect all the traffic to the SPI
 * framework. So, the limit here is indicative.
 */
static int spi_csrgsd5xp_tty_write_room(struct tty_struct *tty)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = tty_to_csrgsd5xp_dev(tty);
	int size;

	size = max_pending_bytes - csrgsd5xp_dev->active_bytes_count;
	if (size >= SPI_CSRGSD5XP_MAX_MESSAGE_LEN)
		size = SPI_CSRGSD5XP_MAX_MESSAGE_LEN - 1;

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d room size: %d\n", __func__, __LINE__,
		size);

	return size;
}

/*
 * The kernel invokes this function when a program writes on the TTY interface
 * of this driver
 */
static int spi_csrgsd5xp_tty_write(struct tty_struct * tty,
				const unsigned char *buf, int count)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = tty_to_csrgsd5xp_dev(tty);
	unsigned int left, len;
	int i;

	if (!buf || !count)
		return 0;

	left = spi_csrgsd5xp_tty_write_room(tty);
	if (left <= 0)
		return 0;

	len = left > count ? count : left;

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d %d\n", __func__, __LINE__, len);
	for (i = 0; i < len; ++i)
		dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d 0x%02x\n", __func__, __LINE__, buf[i]);

	return spi_csrgsd5xp_send_message(csrgsd5xp_dev, buf, len);
}

/*
 * It waits until all messages are transmitted. This driver takes track of
 * all pending messages, so we know when the driver sent all messages
 */
static void spi_csrgsd5xp_tty_wait_until_sent(struct tty_struct *tty,
					   int timeout)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = tty_to_csrgsd5xp_dev(tty);

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d %d\n", __func__, __LINE__,
		csrgsd5xp_dev->active_bytes_count);

	/* Wait until all messages were sent */
	wait_event_interruptible(csrgsd5xp_dev->wait, spi_csrgsd5xp_active_bytes_zero(csrgsd5xp_dev));
}

static struct tty_operations spi_csrgsd5xp_ops = {
	.open		= spi_csrgsd5xp_tty_open,
	.close		= spi_csrgsd5xp_tty_close,
	.write		= spi_csrgsd5xp_tty_write,
	.write_room	= spi_csrgsd5xp_tty_write_room,
	.wait_until_sent= spi_csrgsd5xp_tty_wait_until_sent
};

static const struct tty_port_operations spi_csrgsd5xp_tty_port_ops; /* we have none */

/**
 * Poll timer handler. Called when we need to poll data from the device
 */
static void spi_csrgsd5xp_poll_timer_handler(unsigned long data) {
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev = (struct spi_csrgsd5xp_dev*) data;

	dev_vdbg(&csrgsd5xp_dev->spi->dev, "%s:%d\n", __func__, __LINE__);

	spi_csrgsd5xp_send_idle_pattern(csrgsd5xp_dev);
}

/**
 * Handles write to sysfs entry reset and will reset the device when tty is not opened.
 *
 * Alternative implementation might be to add stopping and starting spi communication
 * before and after the the reset. but this adds quite a bit of complexity like handling
 * all kind of corner cases like:
 * - write meanwhile reset
 * - close tty meanwhile reset
 * - etc.
 *
 * If user space considers resetting the device it can close the tty as well.
 */
static ssize_t do_reset(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	ssize_t rv;
	struct spi_device *spi;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	struct tty_struct *tty;

	dev_dbg(dev, "do_reset\n");

	spi = to_spi_device(dev);
	csrgsd5xp_dev = spi_get_drvdata(spi);

	/**
	 * Down semaphore to exclusively reset chip.
	 */
	down(&csrgsd5xp_dev->sem);

	tty = tty_port_tty_get(&csrgsd5xp_dev->port);

	if (tty) {
		/*
		 * Do not try to reset the device while having the device open. SPI bus needs to be idle
		 * while we reset the device.
		 */
		dev_warn(dev, "Resetting GSD5xp device not possible while tty is open\n");
		tty_kref_put(tty);
		rv = -EBUSY;
		goto out;
	}

	spi_csrgsd5xp_reset(csrgsd5xp_dev);
	rv = count;

out:
	up(&csrgsd5xp_dev->sem);
	return rv;
}

/**
 * Handles read from sysfs entry awake. Prints 1 when device is awake, otherwise 0
 */
static ssize_t show_awake(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	struct spi_device *spi;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	int awake;

	dev_dbg(dev, "show_awake\n");

	spi = to_spi_device(dev);
	csrgsd5xp_dev = spi_get_drvdata(spi);

	awake = spi_csrgsd5xp_is_awake(csrgsd5xp_dev);

	return snprintf(buf, PAGE_SIZE, "%d\n", awake);
}

/**
 * Handles read from sysfs entry autohibernate.
 * When 0, GSD will not hibernate at all even when tty is closed or SoC suspends.
 * When 1, GSD will hibernate when tty is closed.
 * When 2, GSD will hibernate when tty is closed or when SoC suspends.
 */
static ssize_t show_autohibernate(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	struct spi_device *spi;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;

	dev_dbg(dev, "show_autohibernate\n");

	spi = to_spi_device(dev);
	csrgsd5xp_dev = spi_get_drvdata(spi);

	return snprintf(buf, PAGE_SIZE, "%d\n", (int) csrgsd5xp_dev->auto_hibernate);
}

static ssize_t store_autohibernate(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	struct spi_device *spi;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	unsigned long value;

	if (strict_strtoul(buf, 0, &value))
		return -EINVAL;

	spi = to_spi_device(dev);
	csrgsd5xp_dev = spi_get_drvdata(spi);

	csrgsd5xp_dev->auto_hibernate = value;

	return count;
}

static ssize_t show_hibernate_cnt(struct device *dev,
                struct device_attribute *attr, char *buf)
{
	struct spi_device *spi;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;

	dev_dbg(dev, "show_hibernate_cnt\n");

	spi = to_spi_device(dev);
	csrgsd5xp_dev = spi_get_drvdata(spi);

	return snprintf(buf, PAGE_SIZE, "%d\n", csrgsd5xp_dev->hibernate_cnt);
}

/**
 * Activates/de-activate restarting mode. See note 4 for details.
 */
static ssize_t store_restarting(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	struct spi_device *spi;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	unsigned long value;

	if (strict_strtoul(buf, 0, &value))
		return -EINVAL;

	spi = to_spi_device(dev);
	csrgsd5xp_dev = spi_get_drvdata(spi);

	dev_dbg(&csrgsd5xp_dev->spi->dev, "%s:%d restarting %lu\n", __func__, __LINE__, value);

	if (value == 0) { /* Deactivate */
		if (csrgsd5xp_dev->restart_state == RST_IDLE_BYTE_2) /* Special case when still need to send idle byte 2. */
			csrgsd5xp_dev->restart_state = RST_IDLE_BYTE_2_DONE;
		else /* Normal case */
			csrgsd5xp_dev->restart_state = RST_NOT_RESTARTING;
	} else if (value == 1) { /* Activate */
		if (csrgsd5xp_dev->restart_state == RST_NOT_RESTARTING) /* Normal case */
			csrgsd5xp_dev->restart_state = RST_IDLE_BYTE_1;
		else if (csrgsd5xp_dev->restart_state == RST_IDLE_BYTE_2_DONE) /* Special case when we still need to send idle byte 2 */
			csrgsd5xp_dev->restart_state = RST_IDLE_BYTE_2;
		/* Otherwise, leave state as current set */
	} else {
		return -EINVAL;
	}

	return count;
}

static int spi_csrgsd5xp_suspend(struct spi_device *spi, pm_message_t mesg);
static int spi_csrgsd5xp_resume(struct spi_device *spi);
/**
 * Puts the GPS to sleep. To check the state, cat awake.
 */
static size_t store_sleep(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	struct spi_device *spi;
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	unsigned long value;

	if (strict_strtoul(buf, 0, &value))
		return -EINVAL;

	dev_dbg(dev, "store_sleep\n");

	spi = to_spi_device(dev);
	csrgsd5xp_dev = spi_get_drvdata(spi);

	/* Save manually set value */
	sleep_flag = value;
	/* Go to sleep, if possible */
	if (value == 1) {
		/* If still awake, but in hibernate mode */
		if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev)) {
			dev_dbg(dev, "trying to sleep\n");
			spi_csrgsd5xp_suspend(spi, PMSG_USER_SUSPEND);
		}
	} else if (value == 0) {
		/* If asleep, wake it up */
		if (!spi_csrgsd5xp_is_awake(csrgsd5xp_dev)) {
			dev_dbg(dev, "trying to wake up\n");
			spi_csrgsd5xp_resume(spi);
		}

	} else {
		dev_dbg(dev, "operation not supported\n");
	}

	return count;
}

static DEVICE_ATTR(reset, S_IWUGO, NULL, do_reset);
static DEVICE_ATTR(awake, S_IRUGO, show_awake, NULL);
static DEVICE_ATTR(autohibernate, S_IRUGO | S_IWUGO, show_autohibernate, store_autohibernate);
static DEVICE_ATTR(hibernate_cnt, S_IRUGO, show_hibernate_cnt, NULL);
static DEVICE_ATTR(restarting, S_IWUGO, NULL, store_restarting);
static DEVICE_ATTR(sleep, S_IWUGO, NULL, store_sleep);

static struct attribute *dev_attrs[] = {
	&dev_attr_autohibernate.attr,
	&dev_attr_awake.attr,
	&dev_attr_reset.attr,
	&dev_attr_hibernate_cnt.attr,
	&dev_attr_restarting.attr,
	&dev_attr_sleep.attr,
	NULL,
};

static struct attribute_group spi_csrgsd5xp_device_attr_grp = {
	.attrs = dev_attrs,
};

/* * * * Driver Initialization * * * */
static int spi_csrgsd5xp_probe(struct spi_device *spi)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	int err = 0;
	int tty_minor;
	int probe_retries = 0;
	unsigned long flags;
	struct csrgsd5xp_platform_data* pdata;

	dev_info(&spi->dev, "probe\n");

	if (spi->dev.platform_data == NULL) {
		dev_err(&spi->dev, "Missing platform_data\n");
		return -EINVAL;
	}

	spin_lock_irqsave(&lock, flags);
	if (dev_count >= SPI_CSRGSD5XP_MINORS)
		err = -ENOMEM;

	tty_minor = dev_count;
	dev_count++;
	spin_unlock_irqrestore(&lock, flags);

	if (err)
		return err;

	csrgsd5xp_dev = kzalloc(sizeof(struct spi_csrgsd5xp_dev), GFP_KERNEL);
	if (!csrgsd5xp_dev) {
		err = -ENOMEM;
		goto err_kzalloc;
	}

	sema_init(&csrgsd5xp_dev->sem, 1);

	/*
	 * Down semaphore to exclusively activate chip
	 */
	down(&csrgsd5xp_dev->sem);

	spi_set_drvdata(spi, csrgsd5xp_dev);
	csrgsd5xp_dev->spi = spi;

	spin_lock_init(&csrgsd5xp_dev->lock);
	init_timer(&csrgsd5xp_dev->poll_timer);
	csrgsd5xp_dev->poll_timer.data = (unsigned long) csrgsd5xp_dev;
	csrgsd5xp_dev->poll_timer.function = spi_csrgsd5xp_poll_timer_handler;
	csrgsd5xp_dev->auto_hibernate = SPI_CSRGSD5XP__AUTO_HIBERNATE__TTY_CLOSED_SUSPSEND;
	csrgsd5xp_dev->rx_state = RX_LOST_TRACK; /* Wait for next header */
	csrgsd5xp_dev->rx_msg_len = 0;
	csrgsd5xp_dev->restart_state = RST_NOT_RESTARTING;

	pdata = (struct csrgsd5xp_platform_data*) spi->dev.platform_data;

	//We try to prevent resetting the device after a reboot. So active high = init low.
	csrgsd5xp_dev->gpios[0].gpio = pdata->gps_reset_gpio;
	csrgsd5xp_dev->gpios[0].flags = pdata->gps_reset_active_high ? GPIOF_OUT_INIT_LOW : GPIOF_OUT_INIT_HIGH;
	csrgsd5xp_dev->gpios[0].label = "gps_reset";

	csrgsd5xp_dev->gpios[1].gpio = pdata->gps_on_off_gpio;
	csrgsd5xp_dev->gpios[1].flags = GPIOF_OUT_INIT_LOW;
	csrgsd5xp_dev->gpios[1].label = "gps_on_off";

	csrgsd5xp_dev->gpios[2].gpio = pdata->gps_wakeup_gpio;
	csrgsd5xp_dev->gpios[2].flags = GPIOF_IN;
	csrgsd5xp_dev->gpios[2].label = "gps_wake";

	csrgsd5xp_dev->gpios[3].gpio = pdata->gps_data_ready_gpio;
	csrgsd5xp_dev->gpios[3].flags = GPIOF_IN;
	csrgsd5xp_dev->gpios[3].label = "gps_ready";

	err = gpio_request_array(csrgsd5xp_dev->gpios, N_GPIOS);
	if (err)
		goto err_gpio_request;

//	err = spi_csrgsd5xp_register_interrupt(csrgsd5xp_dev,
//			(unsigned int)spi->dev.platform_data);
//	if (err)
//		goto err_req_irq;
//

	/*
	 * If gps is already on, we need to reset it. We have to do this since after a reset of the soc
	 * the gps device will not respond to spi commands anymore as result of the SC getting low during
	 * the reset of the chip. When the device is in hibernate mode (not on), we do not need to this.
	 */
	if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev))
	{
		dev_warn(&spi->dev, "device is already awake. Resetting device.\n");
		spi_csrgsd5xp_reset(csrgsd5xp_dev);
	}

	/* Put it on. */
	spi_csrgsd5xp_pulse_on_off(csrgsd5xp_dev);

	/* Check awake pin. Should rices within after powering off. If not probe failed. */
	while (probe_retries < MAX_PROBE_RETRIES) {
		if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev))
			/* Awake, probe ok! */
			break;

		usleep_range(1000, 1000);
		probe_retries++;
	}

	if (probe_retries == MAX_PROBE_RETRIES) {
		/* Asume device is not present when device is still not awake. Probe failed. */
		dev_err(&spi->dev, "Failed to detect GSD device. GSD not awake.\n");
		spi_csrgsd5xp_reset(csrgsd5xp_dev);
		err = -ENODEV;
		goto err_awake;
	}

	dev_info(&spi->dev, "Detect GSD device (retries=%d)\n", probe_retries);

	err = sysfs_create_group(&spi->dev.kobj, &spi_csrgsd5xp_device_attr_grp);
	if (err)
		goto err_sysfs_create_group;

	/* Initialize port */
	tty_port_init(&csrgsd5xp_dev->port);
	csrgsd5xp_dev->port.ops = &spi_csrgsd5xp_tty_port_ops;

	/* Register new port*/
	csrgsd5xp_dev->tty_minor = tty_minor;
	csrgsd5xp_dev->tty_dev = tty_register_device(spi_csrgsd5xp_tty_driver,
					csrgsd5xp_dev->tty_minor, &csrgsd5xp_dev->spi->dev);
	if (IS_ERR(csrgsd5xp_dev->tty_dev)) {
		err = PTR_ERR(csrgsd5xp_dev->tty_dev);
		goto err_req_tty;
	}

	/* add private data to the device */
	dev_set_drvdata(csrgsd5xp_dev->tty_dev, csrgsd5xp_dev);

	/* Store new device */
	spi_csrgsd5xp_all_dev[csrgsd5xp_dev->tty_minor] = csrgsd5xp_dev;

	up(&csrgsd5xp_dev->sem);

	return 0;
err_req_tty:
//	spi_csrgsd5xp_unregister_interrupt(csrgsd5xp_dev);
//err_req_irq:
	device_remove_file(&spi->dev, &dev_attr_awake);
	sysfs_remove_group(&spi->dev.kobj, &spi_csrgsd5xp_device_attr_grp);
err_sysfs_create_group:
err_awake:
	gpio_free_array(csrgsd5xp_dev->gpios, N_GPIOS);
err_gpio_request:
	up(&csrgsd5xp_dev->sem);
	kfree(csrgsd5xp_dev);
err_kzalloc:
	spin_lock_irqsave(&lock, flags);
	dev_count--;
	spin_unlock_irqrestore(&lock, flags);

	return err;
}

static int spi_csrgsd5xp_remove(struct spi_device *spi)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	unsigned long flags;

	dev_vdbg(&spi->dev, "%s:%d\n", __func__, __LINE__);

	csrgsd5xp_dev = spi_get_drvdata(spi);

	/*
	 * Down semaphore to exclusively de-activate chip
	 */
	down(&csrgsd5xp_dev->sem);

	/* If still awake, but it in hibernate mode */
	if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev))
		spi_csrgsd5xp_put_in_hibernate(csrgsd5xp_dev);

	/* Remove device */
	spi_csrgsd5xp_all_dev[csrgsd5xp_dev->tty_minor] = NULL;
//	spi_csrgsd5xp_unregister_interrupt(csrgsd5xp_dev);
	tty_unregister_device(spi_csrgsd5xp_tty_driver, csrgsd5xp_dev->tty_minor);

	sysfs_remove_group(&spi->dev.kobj, &spi_csrgsd5xp_device_attr_grp);

	if (spi->dev.platform_data != NULL)
		gpio_free_array(csrgsd5xp_dev->gpios, N_GPIOS);

	spin_lock_irqsave(&lock, flags);
	if (csrgsd5xp_dev->tty_minor == dev_count - 1)
		dev_count--;
	spin_unlock_irqrestore(&lock, flags);

	up(&csrgsd5xp_dev->sem);

	kfree(csrgsd5xp_dev);

	return 0;
}

static void spi_csrgsd5xp_shutdown(struct spi_device *spi)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;

	dev_dbg(&spi->dev, "%s:%d\n", __func__, __LINE__);

	csrgsd5xp_dev = spi_get_drvdata(spi);

	/*
	 * Down semaphore to exclusively de-activate chip
	 */
	down(&csrgsd5xp_dev->sem);

	/* If still awake, put it in hibernate mode */
	if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev))
		spi_csrgsd5xp_put_in_hibernate(csrgsd5xp_dev);

	up(&csrgsd5xp_dev->sem);
}

static int spi_csrgsd5xp_suspend(struct spi_device *spi, pm_message_t mesg)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	unsigned long flags;

	dev_dbg(&spi->dev, "%s:%d\n", __func__, __LINE__);

	csrgsd5xp_dev = spi_get_drvdata(spi);

	/*
	 * Down semaphore to exclusively stop SPI communication when it is active currently
	 */
	down(&csrgsd5xp_dev->sem);

	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	csrgsd5xp_dev->active_suspended = csrgsd5xp_dev->active;
	csrgsd5xp_dev->active = 0;
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	if (csrgsd5xp_dev->active_suspended != 0)
	{
		del_timer_sync(&csrgsd5xp_dev->poll_timer);

		wait_event(csrgsd5xp_dev->wait, spi_csrgsd5xp_active_bytes_zero(csrgsd5xp_dev));

		/* Once here, we are sure active_bytes_count will not be increased anymore since
		 * we only increase it when active is 1 (and we check and increase bytes count while
		 * holding the lock)
		 */
	}

	/* When hibernating the device during suspend is enabled and device is awake,
	 * put it in hibernate mode */
	if (csrgsd5xp_dev->auto_hibernate == SPI_CSRGSD5XP__AUTO_HIBERNATE__TTY_CLOSED_SUSPSEND) {
		if (spi_csrgsd5xp_is_awake(csrgsd5xp_dev)) {
			spi_csrgsd5xp_put_in_hibernate(csrgsd5xp_dev);
			csrgsd5xp_dev->suspend_awake_state = 1;
		}
	}

	up(&csrgsd5xp_dev->sem);

	return 0;
}

static int spi_csrgsd5xp_resume(struct spi_device *spi)
{
	struct spi_csrgsd5xp_dev *csrgsd5xp_dev;
	unsigned long flags;

	/* If hibernation was initiated manually, don't resume until its state is changed also manually */
	if (sleep_flag == 1) {
		return 0;
	}

	dev_dbg(&spi->dev, "%s:%d\n", __func__, __LINE__);

	csrgsd5xp_dev = spi_get_drvdata(spi);

	/*
	 * Down semaphore to exclusively start SPI communication when it was active before
	 * suspending.
	 */
	down(&csrgsd5xp_dev->sem);

	spin_lock_irqsave(&csrgsd5xp_dev->lock, flags);
	csrgsd5xp_dev->active = csrgsd5xp_dev->active_suspended;
	spin_unlock_irqrestore(&csrgsd5xp_dev->lock, flags);

	if (csrgsd5xp_dev->active_suspended != 0)
	{
		csrgsd5xp_dev->poll_timer.expires = jiffies + poll_delay*HZ/1000;
		add_timer(&csrgsd5xp_dev->poll_timer);
	}

	/* Restore power state. Power on device when it was awake while suspending */
	if (csrgsd5xp_dev->suspend_awake_state && !spi_csrgsd5xp_is_awake(csrgsd5xp_dev))
		spi_csrgsd5xp_pulse_on_off(csrgsd5xp_dev);

	up(&csrgsd5xp_dev->sem);

	return 0;
}

enum dm385lm_devices {
	ID_CSRGSD5XP,
};

static const struct spi_device_id spi_csrgsd5xp_id[] = {
	{"spi_csrgsd5xp", ID_CSRGSD5XP},
	{}
};

static struct spi_driver spi_csrgsd5xp_driver = {
	.driver = {
		.name   = KBUILD_MODNAME,
		.bus    = &spi_bus_type,
		.owner  = THIS_MODULE,
	},
	.id_table   = spi_csrgsd5xp_id,
	.probe	    = spi_csrgsd5xp_probe,
	.remove	    = spi_csrgsd5xp_remove,
	.shutdown   = spi_csrgsd5xp_shutdown,
	.suspend    = spi_csrgsd5xp_suspend,
	.resume     = spi_csrgsd5xp_resume,
};

static int spi_csrgsd5xp_tty_init(void)
{
	int err;

	/*
	 * Allocate driver structure and reserve space for a number of
	 * devices
	 */
	spi_csrgsd5xp_tty_driver = alloc_tty_driver(SPI_CSRGSD5XP_MINORS);
	if (!spi_csrgsd5xp_tty_driver)
		return -ENOMEM;

	/*
	 * Configure driver
	 */
	spi_csrgsd5xp_tty_driver->driver_name = "spi_csrgsd5xp";
	spi_csrgsd5xp_tty_driver->name = "ttyGSD5xp";
	spi_csrgsd5xp_tty_driver->major = 0;
	spi_csrgsd5xp_tty_driver->minor_start = 0;
	spi_csrgsd5xp_tty_driver->type = TTY_DRIVER_TYPE_SERIAL;
	spi_csrgsd5xp_tty_driver->subtype = SERIAL_TYPE_NORMAL;
	spi_csrgsd5xp_tty_driver->flags = TTY_DRIVER_DYNAMIC_DEV | TTY_DRIVER_REAL_RAW;

	/*
	 * Start with tty_std_termios since we need some line discipline otherwise read
	 * does not work. But clear allmost all flags afterwards to prevent flow control
	 * Speeds are ignored.
	 */
	spi_csrgsd5xp_tty_driver->init_termios = tty_std_termios;
	spi_csrgsd5xp_tty_driver->init_termios.c_cflag = B9600 | CS8 | CREAD
							| HUPCL | CLOCAL;
	spi_csrgsd5xp_tty_driver->init_termios.c_lflag = 0;
	spi_csrgsd5xp_tty_driver->init_termios.c_oflag = 0;
	spi_csrgsd5xp_tty_driver->init_termios.c_iflag = 0;
	spi_csrgsd5xp_tty_driver->init_termios.c_ispeed = 9600;
	spi_csrgsd5xp_tty_driver->init_termios.c_ospeed = 9600;

	tty_set_operations(spi_csrgsd5xp_tty_driver, &spi_csrgsd5xp_ops);
	err = tty_register_driver(spi_csrgsd5xp_tty_driver);
	if (err) {
		pr_err("%s - tty_register_driver failed\n", __func__);
		goto exit_reg_driver;
	}

	return 0;

exit_reg_driver:
	put_tty_driver(spi_csrgsd5xp_tty_driver);
	return err;
}

static int __init spi_csrgsd5xp_init(void) {
	int err, i;

	i = 0;
	while(i < sizeof(idle_buffer) - 1) {
		idle_buffer[i++] = 0xa7;
		idle_buffer[i++] = 0xb4;
	}

	spin_lock_init(&lock);

	err = spi_csrgsd5xp_tty_init();
	if (err)
		return err;

	return spi_register_driver(&spi_csrgsd5xp_driver);
}

static void __exit spi_csrgsd5xp_exit(void) {
	int err;

	err = tty_unregister_driver(spi_csrgsd5xp_tty_driver);
	if (err)
		pr_err("failed to unregister spiserial driver(%d)\n", err);
	put_tty_driver(spi_csrgsd5xp_tty_driver);

	driver_unregister(&spi_csrgsd5xp_driver.driver);
}

deferred_module_init(spi_csrgsd5xp_init);
module_exit(spi_csrgsd5xp_exit);
MODULE_AUTHOR("Jeroen Jensen <JeroenErik.Jensen@tomtom.com>");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("spi:spi_csrgsd5xp"); //To support automatic module loading by udev
