/*
 * tps62360-regulator.c
 *
 * Supports TPS62360 Regulator
 *
 *  Copyright (C) 2014 TomTom BV <http://www.tomtom.com/>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/regulator/driver.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/tps62360.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/slab.h>

/* Register definitions */
#define REG_VSET0			0
#define REG_VSET1			1
#define REG_VSET2			2
#define REG_VSET3			3
#define REG_CONTROL			4
#define REG_TEMP			5
#define REG_RAMPCTRL		6
#define REG_CHIPID			8

#define REG_VSET_MASK		0x7F
#define FORCE_PWM_ENABLE	BIT(7)

/* Number of step-down converters available */
#define TPS62360_NUM_DCDC		1

/* Number of total regulators available */
#define TPS62360_NUM_REGULATOR	(TPS62360_NUM_DCDC)

/* DCDCs */
#define TPS62360_DCDC_1			0
#define TPS62360_DCDC_2			1
#define TPS62360_DCDC_3			2

/* Supported voltage values for regulators */
static const u16 VDCDC1_VSEL_table[] = {
	500, 510, 520, 530, 540, 550, 560, 570, 580, 590,
	600, 610, 620, 630, 640, 650, 660, 670, 680, 690,
	700, 710, 720, 730, 740, 750, 760, 770, 780, 790,
	800, 810, 820, 830, 840, 850, 860, 870, 880, 890,
	900, 910, 920, 930, 940, 950, 960, 970, 980, 990,
	1000, 1010, 1020, 1030, 1040, 1050, 1060, 1070, 1080, 1090,
	1100, 1110, 1120, 1130, 1140, 1150, 1160, 1170, 1180, 1190,
	1200, 1210, 1220, 1230, 1240, 1250, 1260, 1270, 1280, 1290,
	1300, 1310, 1320, 1330, 1340, 1350, 1360, 1370, 1380, 1390,
	1400, 1410, 1420, 1430, 1440, 1450, 1460, 1470, 1480, 1490,
	1500, 1510, 1520, 1530, 1540, 1550, 1560, 1570, 1580, 1590,
	1600, 1610, 1620, 1630, 1640, 1650, 1660, 1670, 1680, 1690,
	1700, 1710, 1720, 1730, 1740, 1750, 1760, 1770,
};

static unsigned int num_voltages[] = {ARRAY_SIZE(VDCDC1_VSEL_table),
				0, 0, 0};

/* Regulator specific details */
struct tps_info {
	const char *name;
	unsigned min_uV;
	unsigned max_uV;
	bool fixed;
	u8 table_len;
	const u16 *table;
};

/* PMIC details */
struct tps_pmic {
	struct regulator_desc desc[TPS62360_NUM_REGULATOR];
	struct i2c_client *client;
	struct regulator_dev *rdev[TPS62360_NUM_REGULATOR];
	const struct tps_info *info[TPS62360_NUM_REGULATOR];
	struct mutex io_lock;
	bool en_internal_pulldn;
	bool en_discharge;
	int vset_id;
};

static inline int tps_62360_read(struct tps_pmic *tps, u8 reg)
{
	return i2c_smbus_read_byte_data(tps->client, reg);
}

static inline int tps_62360_write(struct tps_pmic *tps, u8 reg, u8 val)
{
	return i2c_smbus_write_byte_data(tps->client, reg, val);
}

static int tps_62360_update_bits(struct tps_pmic *tps, u8 reg, u8 mask, u8 value)
{
	int err, data;

	mutex_lock(&tps->io_lock);

	data = tps_62360_read(tps, reg);
	if (data < 0) {
		dev_err(&tps->client->dev, "Read from reg 0x%x failed\n", reg);
		err = data;
		goto out;
	}

	data &= ~mask;
	data |= value;
	err = tps_62360_write(tps, reg, data);
	if (err)
		dev_err(&tps->client->dev, "Write for reg 0x%x failed\n", reg);

out:
	mutex_unlock(&tps->io_lock);
	return err;
}

static int tps_62360_reg_read(struct tps_pmic *tps, u8 reg)
{
	int data;

	mutex_lock(&tps->io_lock);

	data = tps_62360_read(tps, reg);
	if (data < 0)
		dev_err(&tps->client->dev, "Read from reg 0x%x failed\n", reg);

	mutex_unlock(&tps->io_lock);
	return data;
}

static int tps_62360_reg_write(struct tps_pmic *tps, u8 reg, u8 val)
{
	int err;

	mutex_lock(&tps->io_lock);

	err = tps_62360_write(tps, reg, val);
	if (err < 0)
		dev_err(&tps->client->dev, "Write for reg 0x%x failed\n", reg);

	mutex_unlock(&tps->io_lock);
	return err;
}

static int tps62360_dcdc_get_voltage(struct regulator_dev *dev)
{
	struct tps_pmic *tps = rdev_get_drvdata(dev);
	int data, dcdc = rdev_get_id(dev);

	data = tps_62360_reg_read(tps, (REG_VSET0 + tps->vset_id));
	if (data < 0)
		return data;
	data &= (tps->info[dcdc]->table_len - 1);

	return tps->info[dcdc]->table[data] * 1000;
}

static int tps62360_dcdc_set_voltage(struct regulator_dev *dev,
				int min_uV, int max_uV)
{
	struct tps_pmic *tps = rdev_get_drvdata(dev);
	int dcdc = rdev_get_id(dev);
	int vsel;

	if (dcdc != TPS62360_DCDC_1)
		return -EINVAL;

	if (min_uV < tps->info[dcdc]->min_uV
			|| min_uV > tps->info[dcdc]->max_uV)
		return -EINVAL;
	if (max_uV < tps->info[dcdc]->min_uV
			|| max_uV > tps->info[dcdc]->max_uV)
		return -EINVAL;

	for (vsel = 0; vsel < tps->info[dcdc]->table_len; vsel++) {
		int mV = tps->info[dcdc]->table[vsel];
		int uV = mV * 1000;

		/* Break at the first in-range value */
		if (min_uV <= uV && uV <= max_uV)
			break;
	}

	/* write to the register in case we found a match */
	if (vsel == tps->info[dcdc]->table_len)
		return -EINVAL;
	else
		return tps_62360_update_bits(tps, (REG_VSET0 + tps->vset_id), REG_VSET_MASK, vsel);
}

static int tps62360_dcdc_list_voltage(struct regulator_dev *dev,
					unsigned selector)
{
	struct tps_pmic *tps = rdev_get_drvdata(dev);
	int dcdc = rdev_get_id(dev);

	if (dcdc < TPS62360_DCDC_1 || dcdc > TPS62360_DCDC_3)
		return -EINVAL;

	if (dcdc == TPS62360_DCDC_1) {
		if (selector >= tps->info[dcdc]->table_len)
			return -EINVAL;
		else
			return tps->info[dcdc]->table[selector] * 1000;
	} else
		return tps->info[dcdc]->min_uV;
}

static int tps62360_set_mode(struct regulator_dev *dev, unsigned int mode)
{
	struct tps_pmic *tps = rdev_get_drvdata(dev);
	int val;
	int ret;

	/* Enable force PWM mode in FAST mode only. */
	switch (mode) {
	case REGULATOR_MODE_FAST:
		val = FORCE_PWM_ENABLE;
		break;

	case REGULATOR_MODE_NORMAL:
		val = 0;
		break;

	default:
		return -EINVAL;
	}

	ret = tps_62360_update_bits(tps, (REG_VSET0 + tps->vset_id), FORCE_PWM_ENABLE, val);
	if (ret < 0) {
		dev_err(&tps->client->dev,
			"%s(): register %d update failed with err %d\n",
			__func__, (REG_VSET0 + tps->vset_id), ret);
	}

	return ret;
}

static unsigned int tps62360_get_mode(struct regulator_dev *dev)
{
	struct tps_pmic *tps = rdev_get_drvdata(dev);
	unsigned int data;
	int ret;

	data = tps_62360_reg_read(tps, (REG_VSET0 + tps->vset_id));
	if (data < 0) {
		dev_err(&tps->client->dev, "%s(): register %d read failed with err %d\n",
			__func__, (REG_VSET0 + tps->vset_id), ret);
		return ret;
	}

	return (data & FORCE_PWM_ENABLE) ?
				REGULATOR_MODE_FAST : REGULATOR_MODE_NORMAL;
}

/* Operations permitted on VDCDCx */
static struct regulator_ops tps62360_dcdc_ops = {
	.get_voltage 	= tps62360_dcdc_get_voltage,
	.set_voltage 	= tps62360_dcdc_set_voltage,
	.list_voltage 	= tps62360_dcdc_list_voltage,
	.set_mode		= tps62360_set_mode,
	.get_mode		= tps62360_get_mode,
};

static int tps62360_init_hw(struct regulator_dev *dev)
{
	struct tps_pmic *tps = rdev_get_drvdata(dev);
	int ret = 0;

	/* Initialize internal pull up/down control */
	if (tps->en_internal_pulldn)
		ret = tps_62360_reg_write(tps, REG_CONTROL, 0xE0);
	else
		ret = tps_62360_reg_write(tps, REG_CONTROL, 0x0);
	if (ret < 0) {
		dev_err(&tps->client->dev,
			"%s(): register %d write failed with err %d\n",
			__func__, REG_CONTROL, ret);

		return ret;
	}

	/* Reset output discharge path to reduce power consumption */
	/* Output voltage ramp timing: 8 mV / �gs */
	ret = tps_62360_reg_write(tps, REG_RAMPCTRL, 0x40);
	if (ret < 0) {
		dev_err(&tps->client->dev,
			"%s(): register %d update failed with err %d\n",
			__func__, REG_RAMPCTRL, ret);
		return ret;
	}

	return ret;
}

static ssize_t tps_62360_uV_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t size)
{
	unsigned int uV;
	struct regulator_dev *rdev = dev_get_drvdata(dev);

	/* Check constraints */
	if (!rdev->constraints) {
		dev_err(dev, "%s: no constraints defined\n", __func__);
		return -ENODEV;
	}

	if (!(rdev->constraints->valid_ops_mask & REGULATOR_CHANGE_VOLTAGE)) {
		dev_err(dev, "%s: operation not allowed\n", __func__);
		return -EPERM;
	}

	if (sscanf(buf, "%u", &uV) != 1)
		return -EINVAL;

	if (uV > rdev->constraints->max_uV)
		uV = rdev->constraints->max_uV;
	if (uV < rdev->constraints->min_uV)
		uV = rdev->constraints->min_uV;

	mutex_lock(&rdev->mutex);
	tps62360_dcdc_set_voltage(rdev, uV, uV);
	mutex_unlock(&rdev->mutex);

	return size;
}
static DEVICE_ATTR(microvolts_set, 0200, NULL, tps_62360_uV_store);

static int __devinit tps_62360_probe(struct i2c_client *client,
				     const struct i2c_device_id *id)
{
	static int desc_id;
	const struct tps_info *info = (void *)id->driver_data;
	struct regulator_init_data *init_data;
	struct regulator_dev *rdev;
	struct tps_pmic *tps;
	int i;
	int error = 0, ret;

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_SMBUS_BYTE_DATA))
		return -EIO;

	/* Init descriptor id */
	desc_id = 0;

	/**
	 * init_data points to array of regulator_init structures
	 * coming from the board-evm file.
	 */
	init_data = client->dev.platform_data;
	if (!init_data)
		return -EIO;

	tps = kzalloc(sizeof(*tps), GFP_KERNEL);
	if (!tps)
		return -ENOMEM;

	mutex_init(&tps->io_lock);

	/* common for all regulators */
	tps->client = client;

	for (i = 0; i < TPS62360_NUM_REGULATOR; i++, info++, init_data++) {
		/* Store regulator specific information */
		tps->info[i] = info;
		if (init_data->driver_data) {
			struct tps62360_reg_platform_data *data = init_data->driver_data;

			tps->en_internal_pulldn = data->en_internal_pulldn;
			tps->en_discharge = data->en_discharge;
			tps->vset_id = data->vset_id;
		}

		/* Use supply name */
		tps->desc[i].name = init_data->consumer_supplies->supply;
		tps->desc[i].id = desc_id++;
		tps->desc[i].n_voltages = num_voltages[i];
		tps->desc[i].ops = (&tps62360_dcdc_ops);
		tps->desc[i].type = REGULATOR_VOLTAGE;
		tps->desc[i].owner = THIS_MODULE;

		/* Register the regulators */
		rdev = regulator_register(&tps->desc[i], &client->dev,
					  init_data, tps);
		if (IS_ERR(rdev)) {
			dev_err(&client->dev, "failed to register %s\n",
				id->name);
			error = PTR_ERR(rdev);
			goto fail;
		}

		ret = device_create_file(&rdev->dev, &dev_attr_microvolts_set);
		if (ret < 0)
			dev_err(&rdev->dev, "failed to create sysfs entry\n");

		/* Save regulator for cleanup */
		tps->rdev[i] = rdev;
	}

	i2c_set_clientdata(client, tps);

	ret = tps62360_init_hw(rdev);
	if (ret < 0) {
		dev_err(&client->dev, "%s(): Init failed with err = %d\n",
				__func__, ret);
		goto fail;
	}

	return 0;

 fail:
	while (--i >= 0)
		regulator_unregister(tps->rdev[i]);

	kfree(tps);
	return error;
}

static void tps62360_shutdown(struct i2c_client *client)
{
	struct tps_pmic *tps = i2c_get_clientdata(client);
	int st;

	if (!tps->en_discharge)
		return;

	/* Configure the output discharge path */
	st = tps_62360_update_bits(tps, REG_RAMPCTRL, BIT(2), BIT(2));
	if (st < 0)
		dev_err(&tps->client->dev,
			"%s(): register %d update failed with err %d\n",
			__func__, REG_RAMPCTRL, st);
}

/**
 * tps_62360_remove - TPS62360 driver i2c remove handler
 * @client: i2c driver client device structure
 *
 * Unregister TPS driver as an i2c client device driver
 */
static int __devexit tps_62360_remove(struct i2c_client *client)
{
	struct tps_pmic *tps = i2c_get_clientdata(client);
	int i;

	for (i = 0; i < TPS62360_NUM_REGULATOR; i++)
		regulator_unregister(tps->rdev[i]);

	kfree(tps);

	return 0;
}

static const struct tps_info tps62361b_regs[] = {
	{
		.name = "VDCDC1",
		.min_uV =  500000,
		.max_uV = 1770000,
		.table_len = ARRAY_SIZE(VDCDC1_VSEL_table),
		.table = VDCDC1_VSEL_table,
	},
};

static const struct tps_info tps62363_regs[] = {
	{
		.name = "VDCDC1",
		.min_uV =  500000,
		.max_uV = 1770000,
		.table_len = ARRAY_SIZE(VDCDC1_VSEL_table),
		.table = VDCDC1_VSEL_table,
	},
};

static const struct i2c_device_id tps_62360_id[] = {
	{.name = "tps62361B",
	.driver_data = (unsigned long) tps62361b_regs,},
	{.name = "tps62363",
	.driver_data = (unsigned long) tps62363_regs,},
	{ },
};

MODULE_DEVICE_TABLE(i2c, tps_62360_id);

static struct i2c_driver tps_62360_i2c_driver = {
	.driver = {
		.name = "tps62360",
		.owner = THIS_MODULE,
	},
	.probe = tps_62360_probe,
	.shutdown = tps62360_shutdown,
	.remove = __devexit_p(tps_62360_remove),
	.id_table = tps_62360_id,
};

/**
 * tps_62360_init
 *
 * Module init function
 */
static int __init tps_62360_init(void)
{
	return i2c_add_driver(&tps_62360_i2c_driver);
}
subsys_initcall(tps_62360_init);

/**
 * tps_62360_cleanup
 *
 * Module exit function
 */
static void __exit tps_62360_cleanup(void)
{
	i2c_del_driver(&tps_62360_i2c_driver);
}
module_exit(tps_62360_cleanup);

MODULE_DESCRIPTION("TPS62360 voltage regulator driver");
MODULE_LICENSE("GPL v2");
