/*
 * linux/drivers/video/ls013b7dh01.c -- FB driver for ls013b7dh01 LCD controller
 * Layout is based on skeletonfb.c by James Simmons and Geert Uytterhoeven.
 *
 * Copyright (C) 2014, Travis Kuo
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/fb.h>
#include <linux/gpio.h>
#include <linux/spi/spi.h>
#include <linux/mutex.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/pwm.h>
#include <video/ls013b7dh01.h>

  // more than 13 data dummy clocks + 3 mode clocks to clear. Take 20 now
#define NUM_CLKS_CLEAR_ALL 20

static void ls013b7dh01_clear_display(struct ls013b7dh01_par *par);
static void ls013b7dh01_update_display(struct ls013b7dh01_par *par);

static struct fb_fix_screeninfo ls013b7dh01_fix __devinitdata = {
	.id =		"LS013B7DH01", 
	.type =		FB_TYPE_PACKED_PIXELS,
	.visual =	FB_VISUAL_MONO01,
	.xpanstep =	0,
	.ypanstep =	0,
	.ywrapstep =	0, 
	.line_length =	WIDTH*BPP/8,
	.accel =	FB_ACCEL_NONE,
};

static struct fb_var_screeninfo ls013b7dh01_var __devinitdata = {
	.xres =			WIDTH,
	.yres =			HEIGHT,
	.xres_virtual =		WIDTH,
	.yres_virtual =		HEIGHT,
	.bits_per_pixel =	BPP,
	.nonstd	=		1,
};

static u8 rotate_bits(u8 in)
{
	int ret = 0;
	int i;
	
	for(i=0;i<8;i++)
	{
		if(in&(1<<(i)))
			ret |=(1<<(7-i));
	}
	return ret;
}


static int ls013b7dh01_write_data_buf(struct ls013b7dh01_par *par,
					u8 *txbuf, int size)
{

	/* Write entire buffer */
	return spi_write(par->spi, txbuf, size);
}


static void ls013b7dh01_power(struct ls013b7dh01_par *par, int power, int restore_display)
{
	dev_dbg(&par->spi->dev, "%s power %d\n", __func__, power);
	
	if( par->on == power )
	{
		pr_warn("current status is already power %d \n",par->on);
		return;
	}
	
	if(power){
		/* GPIO power on */

		/* Display init works without these waits
		   Removed the delays to improve boot time */

		gpio_set_value(par->disp_5v_on_gpio, 1);
		udelay(500); /* increase T1 time, new charge pump needs >400us */
		ls013b7dh01_clear_display(par);
		gpio_set_value(par->disp_on_gpio, 1);
		// T3 30us more
		udelay(50);
		pwm_enable( par->pwm );
		// T34 30us more
		udelay(50);		
		//send framebuffer to LCD, usally after suspend/resume
		if(restore_display)
			ls013b7dh01_update_display(par);
	}else {
		/* GPIO power off */
		ls013b7dh01_clear_display(par);
		// T5 1V 16.6ms or more
		msleep(17);
		gpio_set_value(par->disp_on_gpio, 0);
		pwm_disable( par->pwm );
		// T6 1V 16.6ms or more
		msleep(17);
		gpio_set_value(par->disp_5v_on_gpio, 0);
		msleep(1);
	}		
	
	par->on = power;
}

static const unsigned char rot_bits_tab[256] = {
  0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0, 
  0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8, 
  0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4, 
  0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC, 
  0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2, 
  0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
  0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6, 
  0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
  0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
  0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9, 
  0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
  0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
  0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3, 
  0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
  0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7, 
  0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};

static void ls013b7dh01_prepare_spi_data(struct ls013b7dh01_par *par)
{
	int i,j;
	u8 *vmem = par->info->screen_base;

	switch(par->rotate){
		case 180:
  		for(i=0;i<HEIGHT;i++){
			unsigned char *lcm_line = &par->spi_x_data.lcm_line[HEIGHT-1-i].data[LCM_BYTES_PER_LINE-1];
  			for(j=0;j<LCM_BYTES_PER_LINE;j++)
				*(lcm_line--) = rot_bits_tab[*(vmem++)];
   		}
   		break;
   		
   	default:
   		pr_err("%s rotate %d not supported, use 0 \n",__FUNCTION__, par->rotate);
		case 0:
			for(i=0;i<HEIGHT;i++){
				memcpy(&par->spi_x_data.lcm_line[i].data[0], &vmem[i*LCM_BYTES_PER_LINE], LCM_BYTES_PER_LINE);
			}   		
   		break;   		
  }
}

static void ls013b7dh01_update_display(struct ls013b7dh01_par *par)
{
	int ret = 0;

	mutex_lock(&par->lock_mutex);
	
	ls013b7dh01_prepare_spi_data(par);
	
	ret = ls013b7dh01_write_data_buf(par, (u8 *)&par->spi_x_data, sizeof(SPI_Tansmit_data_t));

	if (ret < 0)
		pr_err("%s: spi_write failed to update display buffer\n",
			par->info->fix.id);
	
	par->spi_x_data.mode = (1 << 7);	
	
	mutex_unlock(&par->lock_mutex);
}

static void ls013b7dh01_clear_display(struct ls013b7dh01_par *par)
{
	int ret = 0;

	mutex_lock(&par->lock_mutex);
  
	par->spi_x_data.mode = (1 << 5);;  
  
	ret = ls013b7dh01_write_data_buf(par, (u8 *)&par->spi_x_data, NUM_CLKS_CLEAR_ALL);

	if (ret < 0)
		pr_err("%s: spi_write failed to update display buffer\n",
			par->info->fix.id);

	par->spi_x_data.mode = (1 << 7);

	mutex_unlock(&par->lock_mutex);
}

static void ls013b7dh01_deferred_io(struct fb_info *info,
				struct list_head *pagelist)
{
	ls013b7dh01_update_display(info->par);
}

static int ls013b7dh01_init_display(struct ls013b7dh01_par *par)
{
	int ret;
	u8 y,x;

  ret = gpio_request_one(par->disp_5v_on_gpio, GPIOF_OUT_INIT_LOW, "disp 5V on");
  
  if(ret){
  			pr_err("failed to request disp_5v_on_gpio\n");
  }
  
  gpio_direction_output(par->disp_5v_on_gpio, 0);
  
  ret = gpio_request_one(par->disp_on_gpio, GPIOF_OUT_INIT_LOW, "disp_on_gpio");

  if(ret){
  			pr_err("failed to request disp_on_gpio\n");
  }
  
  gpio_direction_output(par->disp_on_gpio, 0);
  
  for (y=0; y<HEIGHT; y++){
		par->spi_x_data.lcm_line[y].line_addr=rotate_bits(y+1);
		for(x=0;x<LCM_BYTES_PER_LINE;x++)
			par->spi_x_data.lcm_line[y].data[x] = 0xff;
	}
			
	par->on = 0;	

	ls013b7dh01_power( par, 1 , 0 );

	return 0;
}

void ls013b7dh01_fillrect(struct fb_info *info, const struct fb_fillrect *rect)
{
	struct ls013b7dh01_par *par = info->par;

	 sys_fillrect(info, rect);

	ls013b7dh01_update_display(par);
}

void ls013b7dh01_copyarea(struct fb_info *info, const struct fb_copyarea *area) 
{
	struct ls013b7dh01_par *par = info->par;

	 sys_copyarea(info, area);

	ls013b7dh01_update_display(par);
}

void ls013b7dh01_imageblit(struct fb_info *info, const struct fb_image *image) 
{
	struct ls013b7dh01_par *par = info->par;

  sys_imageblit(info, image);

	ls013b7dh01_update_display(par);
}

static ssize_t ls013b7dh01_write(struct fb_info *info, const char __user *buf,
		size_t count, loff_t *ppos)
{
	struct ls013b7dh01_par *par = info->par;
	unsigned long p = *ppos;
	void *dst;
	int err = 0;
	unsigned long total_size;
  
	if (info->state != FBINFO_STATE_RUNNING)
		return -EPERM;

	total_size = info->fix.smem_len;

	if (p > total_size)
		return -EFBIG;

	if (count > total_size) {
		err = -EFBIG;
		count = total_size;
	}

	if (count + p > total_size) {
		if (!err)
			err = -ENOSPC;

		count = total_size - p;
	}

	dst = (void __force *) (info->screen_base + p);

	if (copy_from_user(dst, buf, count))
		err = -EFAULT;

	if  (!err)
		*ppos += count;

	ls013b7dh01_update_display(par);

	return (err) ? err : count;		
}

static int ls013b7dh01_open(struct fb_info *info, int user)
{
	struct ls013b7dh01_par *par = (struct ls013b7dh01_par *) info->par;	
	
	atomic_inc(&par->ref_count);
	
	return 0;
}

static int ls013b7dh01_release(struct fb_info *info, int user)
{
	struct ls013b7dh01_par *par = (struct ls013b7dh01_par *) info->par;	
	int count = atomic_read(&par->ref_count);
	if (!count)
		return -EINVAL;
	atomic_dec(&par->ref_count);
	return 0;
}
/*
 *    (Un)Blank the display.
 */
static int ls013b7dh01_blank(int blank_mode, struct fb_info *info)
{
	struct ls013b7dh01_par *par = info->par;

	switch (blank_mode) {
	case FB_BLANK_POWERDOWN:	/* powerdown - both sync lines down */
		ls013b7dh01_power(par, 0, 0);
		break;
	case FB_BLANK_HSYNC_SUSPEND:		/* hsync off */
		break;
	case FB_BLANK_VSYNC_SUSPEND:		/* vsync off */
		break;
	case FB_BLANK_NORMAL:		/* just blank screen (backlight stays on) */
		break;
	case FB_BLANK_UNBLANK:		/* unblank */
		ls013b7dh01_power(par, 1, 1);	
		break;
	default:	/* Anything else we don't understand; return 1 to tell
			 * fb_blank we didn't aactually do anything */
		return 1;
	}

	return 0;
}

static struct fb_ops ls013b7dh01_ops = {
	.owner		= THIS_MODULE,
	.fb_open = ls013b7dh01_open,
	.fb_release = ls013b7dh01_release,
	.fb_read	= fb_sys_read,
	.fb_write	= ls013b7dh01_write,
	.fb_fillrect	= ls013b7dh01_fillrect,
	.fb_copyarea	= ls013b7dh01_copyarea,
	.fb_imageblit	= ls013b7dh01_imageblit,
	.fb_blank	= ls013b7dh01_blank,
};

static struct fb_deferred_io ls013b7dh01_defio = {
	.delay		= HZ,
	.deferred_io	= ls013b7dh01_deferred_io,
};

static int __devinit ls013b7dh01_probe (struct spi_device *spi)
{

	int chip = spi_get_device_id(spi)->driver_data;
	struct ls013b7dh01_platform_data *pdata = spi->dev.platform_data;
	int vmem_size = WIDTH*HEIGHT*BPP/8;
	u8 *vmem;
	struct fb_info *info;
	struct ls013b7dh01_par *par;
	int retval = -ENOMEM;

	dev_dbg(&spi->dev, "%s\n", __func__);
	
	if (chip != LS013B7DH01_DISPLAY_AF_TFT18) {
		pr_err("%s: only the %s device is supported\n", DRVNAME,
			to_spi_driver(spi->dev.driver)->id_table->name);
		return -EINVAL;
	}

	if (!pdata) {
		pr_err("%s: platform data required for rst and dc info\n",
			DRVNAME);
		return -EINVAL;
	}

	vmem = vzalloc(vmem_size);
	if (!vmem)
		return retval;

	info = framebuffer_alloc(sizeof(struct ls013b7dh01_par), &spi->dev);
	if (!info)
		goto fballoc_fail;

	info->screen_base = (u8 __force __iomem *)vmem;
	info->fbops = &ls013b7dh01_ops;
	info->fix = ls013b7dh01_fix;
	info->fix.smem_len = vmem_size;
	info->var = ls013b7dh01_var;

	info->flags = FBINFO_FLAG_DEFAULT | FBINFO_VIRTFB;
	info->fbdefio = &ls013b7dh01_defio;
	
	fb_deferred_io_init(info);

	par = info->par;
	par->info = info;
	par->spi = spi;
	mutex_init(&par->lock_mutex);
	
	par->disp_on_gpio = pdata->disp_on_gpio;
	par->disp_5v_on_gpio = pdata->disp_5v_on_gpio;
	par->rotate = pdata->rotate;
	par->lcd_on_in_suspend = pdata->lcd_on_in_suspend;
	
	par->pwm_period_ns = 1000000000 / pdata->pwm_freq;
	
	par->pwm = pwm_request( pdata->pwm_id, "ext_com");
	
	if (IS_ERR(par->pwm)) {
		pr_err("unable to request PWM for EXT_COM\n");
		retval = PTR_ERR(par->pwm);
		goto pwm_fail;
	}
	
	pwm_config(par->pwm, par->pwm_period_ns >> 1 , par->pwm_period_ns );
		
	retval = register_framebuffer(info);
	if (retval < 0)
		goto fbreg_fail;

	spi_set_drvdata(spi, info);

	retval = ls013b7dh01_init_display(par);
	if (retval < 0)
		goto init_fail;

	printk(KERN_DEBUG
		"fb%d: %s frame buffer device,\n\tusing %d KiB of video memory\n",
		info->node, info->fix.id, vmem_size);

	return 0;

	pwm_free(par->pwm);

init_fail:
	spi_set_drvdata(spi, NULL);

fbreg_fail:
	framebuffer_release(info);

fballoc_fail:
	vfree(vmem);

pwm_fail:
	return retval;
}

static int __devexit ls013b7dh01_remove(struct spi_device *spi)
{
	struct fb_info *info = spi_get_drvdata(spi);
	struct ls013b7dh01_par *par = info->par;
	
  dev_info(&spi->dev, "%s \n",__FUNCTION__);
	
	ls013b7dh01_power(par, 0, 0);
	pwm_free(par->pwm);	
	gpio_free(par->disp_on_gpio);
	gpio_free(par->disp_5v_on_gpio);
	
	spi_set_drvdata(spi, NULL);

	if (info) {
		unregister_framebuffer(info);
		vfree(info->screen_base);	
		framebuffer_release(info);
	}


	return 0;
}

#ifdef CONFIG_PM
static int ls013b7dh01_suspend(struct spi_device *spi, pm_message_t state)
{
	struct fb_info *info = spi_get_drvdata(spi);
	struct ls013b7dh01_par *par = info->par;
	
	dev_dbg(&spi->dev, "%s state:%d\n", __func__, state.event);
	
	if(par->lcd_on_in_suspend)
		return 0;
		
	ls013b7dh01_power(par, 0, 0);
	
	fb_set_suspend(info, 1);

	return 0;
}
static int ls013b7dh01_resume(struct spi_device *spi)
{
	struct fb_info *info = spi_get_drvdata(spi);
	struct ls013b7dh01_par *par = info->par;
  
	dev_dbg(&spi->dev, "%s\n", __func__);
  
	if(par->lcd_on_in_suspend)
		return 0;
		  
	fb_set_suspend(info, 0);
	
	ls013b7dh01_power(par, 1, 1);

	return 0;
}

static int ls013b7dh01_shutdown(struct spi_device *spi)
{
	struct fb_info *info = spi_get_drvdata(spi);
	struct ls013b7dh01_par *par = info->par;
				
	ls013b7dh01_power(par, 0, 0);
}

#else
#define fb_suspend NULL
#define fb_resume NULL
#endif

static const struct spi_device_id ls013b7dh01_ids[] = {
	{ "ls013b7dh01", LS013B7DH01_DISPLAY_AF_TFT18 },
	{ },
};

MODULE_DEVICE_TABLE(spi, ls013b7dh01_ids);

static struct spi_driver ls013b7dh01_driver = {
	.driver = {
		.name   = "ls013b7dh01",
		.owner  = THIS_MODULE,
	},
	.id_table = ls013b7dh01_ids,
	.probe  = ls013b7dh01_probe,
	.remove = __devexit_p(ls013b7dh01_remove),
	.suspend = ls013b7dh01_suspend,
	.resume = ls013b7dh01_resume,	
	.shutdown = ls013b7dh01_shutdown
};

static int __init ls013b7dh01_init(void)
{
	return spi_register_driver(&ls013b7dh01_driver);
}

static void __exit ls013b7dh01_exit(void)
{
	spi_unregister_driver(&ls013b7dh01_driver);
}

/* ------------------------------------------------------------------------- */

module_init(ls013b7dh01_init);
module_exit(ls013b7dh01_exit);
