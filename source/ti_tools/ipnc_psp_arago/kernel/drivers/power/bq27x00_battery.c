/*
 * BQ27x00 battery driver
 *
 * Copyright (C) 2008 Rodolfo Giometti <giometti@linux.it>
 * Copyright (C) 2008 Eurotech S.p.A. <info@eurotech.it>
 * Copyright (C) 2010-2011 Lars-Peter Clausen <lars@metafoo.de>
 * Copyright (C) 2011 Pali Rohár <pali.rohar@gmail.com>
 *
 * Based on a previous work by Copyright (C) 2008 Texas Instruments, Inc.
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * THIS PACKAGE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * Datasheets:
 * http://focus.ti.com/docs/prod/folders/print/bq27000.html
 * http://focus.ti.com/docs/prod/folders/print/bq27500.html
 */

#include <linux/module.h>
#include <linux/param.h>
#include <linux/jiffies.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/idr.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <asm/unaligned.h>

#include <linux/power/bq27x00_battery.h>

#define DRIVER_VERSION			"1.2.0"
#define G3_FW_VERSION			0x0324
#define L1_FW_VERSION			0x0600

#define CONTROL_CMD			0x00
/* Subcommands of Control() */
#define DEV_TYPE_SUBCMD			0x0001
#define FW_VER_SUBCMD			0x0002
#define DF_VER_SUBCMD			0x001F
#define RESET_SUBCMD			0x0041

#define INVALID_REG_ADDR		0xFF

bool kill_sent = 0;

enum bq27x00_reg_index {
	BQ27x00_REG_TEMP = 0,
	BQ27x00_REG_INT_TEMP,
	BQ27x00_REG_VOLT,
	BQ27x00_REG_AI,    /* Current now */
	BQ27x00_REG_FLAGS,
	BQ27x00_REG_TTE,   /* Time to empty */
	BQ27x00_REG_TTF,   /* Time to full */
	BQ27x00_REG_TTES,  /* not used */
	BQ27x00_REG_TTECP, /* time to empty average */
	BQ27x00_REG_NAC,   /* Nominal available capacity */
	BQ27x00_REG_LMD,   /* Last measured discharge */
	BQ27x00_REG_CYCT,  /* Cycle count total */
	BQ27x00_REG_AE,    /* Average energy */
	BQ27000_REG_RSOC,  /* Relative State-of-Charge */
	BQ27000_REG_ILMD,  /* Initial last measured discharge */
	BQ27500_REG_SOC,   /* State-of-Charge */
	BQ27500_REG_DCAP,  /* Designed capacity */
	BQ27421_REG_SOHP,  /* State-of-Health (percentage) */
	BQ27421_REG_SOHS,  /* State-of-Health (status) */
	BQ27500_REG_CTRL
};

/* bq27421 registers */
static u8 bq27421_regs[] = {
	0x02, /* TEMP */
	0x1e, /* INT_TEMP */
	0x04, /* VOLT */
	0x10, /* AI */
	0x06, /* FLAGS */
	0xFF, /* TTE - not supported */
	0xFF, /* TTF - not supported */
	0xFF, /* TTES - not supported */
	0xFF, /* TTECP - not supported */
	0x08, /* NAC */
	0xFF, /* LMD - not supported */
	0xFF, /* CYCT - not supported */
	0x18, /* AE */
	0xFF, /* RSOC (1 byte) - not supported. */
	0xFF, /* ILMD */
	0x1C, /* SOC (2 bytes) */
	0x3C, /* DCAP - extended data command */
	0x20, /* SOH percentage */
	0x21, /* SOH status */
	0x00  /* CTRL */
};

/* TI G3 Firmware (v3.24) */
static u8 bq27x00_fw_g3_regs[] = {
	0x06,
	0x36,
	0x08,
	0x14,
	0x0A,
	0x16,
	0x18,
	0x1c,
	0x26,
	0x0C,
	0x12,
	0x2A,
	0x22,
	0x0B,
	0x76,
	0x2C,
	0x3C,
	0xFF,
	0xFF,
	0x00
};

/*
 * TI L1 firmware (v6.00)
 * Some of the commented registers are missing in this fw.
 * Mark them as 0xFF for being invalid
 */
static u8 bq27x00_fw_l1_regs[] = {
	0x06,
	0x28,
	0x08,
	0x14,
	0x0A,
	0x16,
	0xFF, /* TTF */
	0x1A,
	0xFF, /* TTECP */
	0x0C,
	0xFF, /* LMD */
	0x1E,
	0xFF, /* AE */
	0xFF, /* RSOC */
	0xFF, /* ILMD */
	0x20,
	0x2E,
	0xFF,
	0xFF,
	0x00
};

#define BQ27000_FLAG_CHGS		BIT(7)
#define BQ27000_FLAG_FC			BIT(5)

#define BQ27421_FLAG_DSC		BIT(0)
#define BQ27421_FLAG_SOCF		BIT(1)
#define BQ27421_FLAG_SOC1		BIT(2)
#define BQ27421_FLAG_FC			BIT(9)
#define BQ27421_FLAG_UT			BIT(14)
#define BQ27421_FLAG_OT			BIT(15)

#define BQ27500_FLAG_DSC		BIT(0)
#define BQ27500_FLAG_FC			BIT(9)

#define BQ27000_RS			20 /* Resistor sense */

#define BQ27421_MAX_PREVIOUS_VOLTAGE_SAMPLES 2

/**
 * Note 1:
 *
 * When reading battery voltage value only glitches once, we don't not want to shutdown directly.
 * When reading battery voltage keeps failing, we want to shutdown.
 * Device will shutdown when the running battery voltage over 30 seconds becomes below BQ27421_SHUTDOWN_VOLTAGE
 * By inserting the BQ27421_GLITCH_VOLTAGE voltage as sample when reading the battery voltage failed,
 * the running average will only drop below BQ27421_SHUTDOWN_VOLTAGE when reading the voltage keeps
 * failing or when the average battery voltage is already near BQ27421_SHUTDOWN_VOLTAGE.
 * This assumes BQ27421_GLITCH_VOLTAGE is lower than the BQ27421_SHUTDOWN_VOLTAGE.
 */


struct bq27x00_device_info;
struct bq27x00_access_methods {
	int (*read)(struct bq27x00_device_info *di, u8 reg, bool single);
	int (*write)(struct bq27x00_device_info *di, u8 reg, int value,
			bool single);
};

enum bq27x00_chip { BQ27000, BQ27500, BQ27421 };

struct bq27x00_reg_cache {
	int temperature;
	int time_to_empty;
	int time_to_empty_avg;
	int time_to_full;
	int charge_full;
	int cycle_count;
	int capacity;
	int flags;

	int current_now;
	int voltage_avg;
};

struct bq27x00_device_info {
	struct device 		*dev;
	struct bq27000_platform_data   *pdata;
	int			id;
	enum bq27x00_chip	chip;

	struct bq27x00_reg_cache cache;
	int charge_design_full;

	unsigned long last_update;
	struct delayed_work work;

	struct power_supply	bat;

	struct bq27x00_access_methods bus;

	struct mutex lock;
	struct mutex lock_control;

	int fake_capacity;
	int fake_capacity_level;

	u8 *regs;
	int fw_ver;
	int df_ver;

	int previous_voltage_samples[BQ27421_MAX_PREVIOUS_VOLTAGE_SAMPLES];
	int n_previous_voltage_samples;

	u8 ps_registered;
	u8 invalid_cache;
};

static enum power_supply_property bq27x00_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_PRESENT,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CURRENT_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
	POWER_SUPPLY_PROP_TEMP,
	POWER_SUPPLY_PROP_TIME_TO_EMPTY_NOW,
	POWER_SUPPLY_PROP_TIME_TO_EMPTY_AVG,
	POWER_SUPPLY_PROP_TIME_TO_FULL_NOW,
	POWER_SUPPLY_PROP_TECHNOLOGY,
	POWER_SUPPLY_PROP_CHARGE_FULL,
	POWER_SUPPLY_PROP_CHARGE_NOW,
	POWER_SUPPLY_PROP_CHARGE_FULL_DESIGN,
	POWER_SUPPLY_PROP_CYCLE_COUNT,
	POWER_SUPPLY_PROP_ENERGY_NOW
};

/**
 * Properties commented out are not supported by the bq27421 chip.
 */
static enum power_supply_property bq27421_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_PRESENT,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_VOLTAGE_AVG,
	POWER_SUPPLY_PROP_CURRENT_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
	POWER_SUPPLY_PROP_CAPACITY_LEVEL, /* Only implemented for bq27421 */
	POWER_SUPPLY_PROP_TEMP,
/*	POWER_SUPPLY_PROP_TIME_TO_EMPTY_NOW,*/
/*	POWER_SUPPLY_PROP_TIME_TO_EMPTY_AVG, */
/*	POWER_SUPPLY_PROP_TIME_TO_FULL_NOW, */
	POWER_SUPPLY_PROP_TECHNOLOGY,
/*	POWER_SUPPLY_PROP_CHARGE_FULL, */
	POWER_SUPPLY_PROP_CHARGE_NOW,
	POWER_SUPPLY_PROP_CHARGE_FULL_DESIGN,
/*	POWER_SUPPLY_PROP_CYCLE_COUNT, */
	POWER_SUPPLY_PROP_ENERGY_NOW
};

static unsigned int poll_interval = 10;
module_param(poll_interval, uint, 0644);
MODULE_PARM_DESC(poll_interval, "battery poll interval in seconds - " \
				"0 disables polling");

static int bq27x00_battery_voltage(struct bq27x00_device_info *di,
	union power_supply_propval *val);
static int bq27x00_check_battery_power_failure(struct bq27x00_device_info *di);
static int bq27x00_battery_update_voltage_avg(struct bq27x00_device_info *di, int shift);

/*
 * Common code for BQ27x00 devices
 */

static inline int bq27x00_read(struct bq27x00_device_info *di, int reg_index,
		bool single)
{
	int val;
	u8 reg;

	/* Reports 0 for invalid/missing registers */
	if (!di || !di->regs)
		return 0;

	reg = di->regs[reg_index];
	if (reg == INVALID_REG_ADDR)
		return 0;

	mutex_lock(&di->lock_control);

	val = di->bus.read(di, reg, single);

	mutex_unlock(&di->lock_control);

	if (val < 0)
		dev_err(di->dev, "Reading reg 0x%02x failed. Error=%d\n", reg, val);

	return val;
}

static inline int bq27x00_write(struct bq27x00_device_info *di, int reg_index,
		int value, bool single)
{
	int ret;
	u8 reg;

	if (!di || !di->regs)
		return 0;

	reg = di->regs[reg_index];
	if (reg == INVALID_REG_ADDR)
		return 0;

	mutex_lock(&di->lock_control);

	ret = di->bus.write(di, reg, value, single);

	mutex_unlock(&di->lock_control);

	if (ret < 0)
		dev_err(di->dev, "Writing reg 0x%02x failed. Error=%d\n", reg, ret);

	return ret;
}

/*
 * Return the battery Relative State-of-Charge
 * Or < 0 if something fails.
 */
static int bq27x00_battery_read_rsoc(struct bq27x00_device_info *di)
{
	int rsoc;

	if (di->chip == BQ27500 || di->chip == BQ27421)
		rsoc = bq27x00_read(di, BQ27500_REG_SOC, false);
	else
		rsoc = bq27x00_read(di, BQ27000_REG_RSOC, true);

	if (rsoc < 0)
		dev_err(di->dev, "error reading relative State-of-Charge\n");

	return rsoc;
}

/*
 * Return a battery charge value in µAh
 * Or < 0 if something fails.
 */
static int bq27x00_battery_read_charge(struct bq27x00_device_info *di, u8 reg)
{
	int charge;

	charge = bq27x00_read(di, reg, false);
	if (charge < 0) {
		dev_err(di->dev, "error reading nominal available capacity\n");
		return charge;
	}

	if (di->chip == BQ27500 || di->chip == BQ27421)
		charge *= 1000;
	else
		charge = charge * 3570 / BQ27000_RS;

	return charge;
}

/*
 * Return the battery Nominal available capaciy in µAh
 * Or < 0 if something fails.
 */
static inline int bq27x00_battery_read_nac(struct bq27x00_device_info *di)
{
	return bq27x00_battery_read_charge(di, BQ27x00_REG_NAC);
}

/*
 * Return the battery Last measured discharge in µAh
 * Or < 0 if something fails.
 */
static inline int bq27x00_battery_read_lmd(struct bq27x00_device_info *di)
{
	return bq27x00_battery_read_charge(di, BQ27x00_REG_LMD);
}

/*
 * Return the battery Initial last measured discharge in µAh
 * Or < 0 if something fails.
 */
static int bq27x00_battery_read_ilmd(struct bq27x00_device_info *di)
{
	int ilmd;

	if (di->chip == BQ27500 || di->chip == BQ27421)
		ilmd = bq27x00_read(di, BQ27500_REG_DCAP, false);
	else
		ilmd = bq27x00_read(di, BQ27000_REG_ILMD, true);

	if (ilmd < 0) {
		dev_err(di->dev, "error reading initial last measured discharge\n");
		return ilmd;
	}

	if (di->chip == BQ27500 || di->chip == BQ27421)
		ilmd *= 1000;
	else
		ilmd = ilmd * 256 * 3570 / BQ27000_RS;

	return ilmd;
}

/*
 * Return the battery Cycle count total
 * Or < 0 if something fails.
 */
static int bq27x00_battery_read_cyct(struct bq27x00_device_info *di)
{
	int cyct;

	cyct = bq27x00_read(di, BQ27x00_REG_CYCT, false);
	if (cyct < 0)
		dev_err(di->dev, "error reading cycle count total\n");

	return cyct;
}

/*
 * Read a time register.
 * Return < 0 if something fails.
 */
static int bq27x00_battery_read_time(struct bq27x00_device_info *di, u8 reg)
{
	int tval;

	tval = bq27x00_read(di, reg, false);
	if (tval < 0) {
		dev_err(di->dev, "error reading register %02x: %d\n", reg, tval);
		return tval;
	}

	if (tval == 65535)
		return -ENODATA;

	return tval * 60;
}

/*
 * Return the battery StateOfHealth Percentage
 * Or < 0 if something fails.
 */
static int bq27x00_battery_read_soh_percentage(struct bq27x00_device_info *di)
{
	return bq27x00_read(di, BQ27421_REG_SOHP, true);
}

/*
 * Return the battery StateOfHealth Status
 * Or < 0 if something fails.
 */
static int bq27x00_battery_read_soh_status(struct bq27x00_device_info *di)
{
	return bq27x00_read(di, BQ27421_REG_SOHS, true);
}

static int bq27x00_update(struct bq27x00_device_info *di)
{
	int ret = 0;
	struct bq27x00_reg_cache cache = {0, };
	bool is_bq27500 = di->chip == BQ27500;
	bool is_bq27421 = di->chip == BQ27421;

	/* flags is two bytes for bq27500 and bq27421. bq27000 only one. */
	cache.flags = bq27x00_read(di, BQ27x00_REG_FLAGS, (!is_bq27500) && (!is_bq27421));
	if (cache.flags >= 0) {
		cache.capacity = bq27x00_battery_read_rsoc(di);
		cache.temperature = bq27x00_read(di, BQ27x00_REG_TEMP, false);
		cache.time_to_empty = bq27x00_battery_read_time(di, BQ27x00_REG_TTE);
		cache.time_to_empty_avg = bq27x00_battery_read_time(di, BQ27x00_REG_TTECP);
		cache.time_to_full = bq27x00_battery_read_time(di, BQ27x00_REG_TTF);
		cache.charge_full = bq27x00_battery_read_lmd(di);
		cache.cycle_count = bq27x00_battery_read_cyct(di);
		cache.voltage_avg = bq27x00_battery_update_voltage_avg(di, 1);

		/* bq27500 and bq27421 return average current. So no need to cache it. */
		if (!is_bq27500 && !is_bq27421)
			cache.current_now = bq27x00_read(di, BQ27x00_REG_AI, false);

		/* We only have to read charge design full once */
		if (di->charge_design_full <= 0)
			di->charge_design_full = bq27x00_battery_read_ilmd(di);

		if ((ret = bq27x00_check_battery_power_failure(di)) < 0)
			return ret;
	}

	/* Ignore current_now which is a snapshot of the current battery state
	 * and is likely to be different even between two consecutive reads */
	if (memcmp(&di->cache, &cache, sizeof(cache) - sizeof(int)) != 0) {
		di->cache = cache;

		/* Inform power supply core about the change.
		 *
		 * Only do this after being fully registered otherwise
		 * power_supply_changed() will crash while queuing work.
		 * The call power_supply_register() can result in ending
		 * up here via the get_property() callback.
		 */
		if(di->ps_registered)
			power_supply_changed(&di->bat);
	}

	di->last_update = jiffies;
	di->invalid_cache = 0;

	return ret;
}

static void bq27x00_battery_poll(struct work_struct *work)
{
	struct bq27x00_device_info *di =
		container_of(work, struct bq27x00_device_info, work.work);

	bq27x00_update(di);

	if (poll_interval > 0) {
		/* The timer does not have to be accurate. */
		set_timer_slack(&di->work.timer, poll_interval * HZ / 4);
		schedule_delayed_work(&di->work, poll_interval * HZ);
	}
}


/*
 * Return the battery temperature in tenths of degree Celsius
 * Or < 0 if something fails.
 */
static int bq27x00_battery_temperature(struct bq27x00_device_info *di,
	union power_supply_propval *val)
{
	if (di->cache.temperature < 0)
		return di->cache.temperature;

	if (di->chip == BQ27500 || di->chip == BQ27421)
		val->intval = di->cache.temperature - 2731;
	else
		val->intval = ((di->cache.temperature * 5) - 5463) / 2;

	return 0;
}

/*
 * Return the battery average current in µA
 * Note that current can be negative signed as well
 * Or 0 if something fails.
 */
static int bq27x00_battery_current(struct bq27x00_device_info *di,
	union power_supply_propval *val)
{
	int curr;
	bool cached = (di->chip != BQ27500) && (di->chip != BQ27421);

	if (!cached)
	    curr = bq27x00_read(di, BQ27x00_REG_AI, false);
	else
	    curr = di->cache.current_now;

	if (curr < 0)
		return curr;

	if (!cached) {
		/* bq27500 returns signed value */
		val->intval = (int)((s16)curr) * 1000;
	} else {
		if (di->cache.flags & BQ27000_FLAG_CHGS) {
			dev_dbg(di->dev, "negative current!\n");
			curr = -curr;
		}

		val->intval = curr * 3570 / BQ27000_RS;
	}

	return 0;
}

static int bq27x00_battery_status(struct bq27x00_device_info *di,
	union power_supply_propval *val)
{
	int status;

	if (di->chip == BQ27421) {
		if (di->cache.flags & (BQ27421_FLAG_UT | BQ27421_FLAG_OT))
			status =  POWER_SUPPLY_STATUS_NOT_CHARGING;
		else if (di->cache.flags & BQ27421_FLAG_FC)
			status = POWER_SUPPLY_STATUS_FULL;
		else if (di->cache.flags & BQ27421_FLAG_DSC)
			status = POWER_SUPPLY_STATUS_DISCHARGING;
		else if (power_supply_am_i_supplied(&di->bat))
			status = POWER_SUPPLY_STATUS_CHARGING;
		else
			status = POWER_SUPPLY_STATUS_NOT_CHARGING;
	} else if (di->chip == BQ27500) {
		if (di->cache.flags & BQ27500_FLAG_FC)
			status = POWER_SUPPLY_STATUS_FULL;
		else if (di->cache.flags & BQ27500_FLAG_DSC)
			status = POWER_SUPPLY_STATUS_DISCHARGING;
		else
			status = POWER_SUPPLY_STATUS_CHARGING;
	} else {
		if (di->cache.flags & BQ27000_FLAG_FC)
			status = POWER_SUPPLY_STATUS_FULL;
		else if (di->cache.flags & BQ27000_FLAG_CHGS)
			status = POWER_SUPPLY_STATUS_CHARGING;
		else if (power_supply_am_i_supplied(&di->bat))
			status = POWER_SUPPLY_STATUS_NOT_CHARGING;
		else
			status = POWER_SUPPLY_STATUS_DISCHARGING;
	}

	val->intval = status;

	return 0;
}

static int bq27x00_capacity_level(struct bq27x00_device_info *di,
	union power_supply_propval *val)
{
	int level;
	int voltage_avg;

	if (di->fake_capacity_level != -1)
		level = di->fake_capacity_level;
	else if (di->chip == BQ27421) {

		/**
		 * LBP-1362, LBP-1478. Use battery voltage average to determine whether batter capacity is critical.
		 *
		 * When we did not yet read enough previous voltage samples, we just powered on.
		 * During probe, we already sampled at least one. So read another one and average over these samples.
		 **/
		if (di->n_previous_voltage_samples < BQ27421_MAX_PREVIOUS_VOLTAGE_SAMPLES)
			voltage_avg = bq27x00_battery_update_voltage_avg(di, 0);
		else
			voltage_avg = di->cache.voltage_avg;
		/**
		 * If there is no platform data, don't take try to compare the average voltage to threshold values.
		 * Otherwise, compare it with the values defined in the board file.
		 **/
		if ((di->pdata != NULL) && ((((di->n_previous_voltage_samples < BQ27421_MAX_PREVIOUS_VOLTAGE_SAMPLES) &&
			(voltage_avg < di->pdata->bq27421_boot_voltage)) || (voltage_avg < di->pdata->bq27421_shutdown_voltage)))) {

			level = POWER_SUPPLY_CAPACITY_LEVEL_CRITICAL;
		} else {
#ifdef CONFIG_BATTERY_BQ27421_SOC1_SOCF_SWAPPED

			if (di->cache.flags & BQ27421_FLAG_SOC1) {
				level = POWER_SUPPLY_CAPACITY_LEVEL_CRITICAL;
			} else if (di->cache.flags & BQ27421_FLAG_SOCF) {
				level = POWER_SUPPLY_CAPACITY_LEVEL_LOW;
			} else if (di->cache.flags & BQ27421_FLAG_FC) {
				level = POWER_SUPPLY_CAPACITY_LEVEL_FULL;
			} else {
				level = POWER_SUPPLY_CAPACITY_LEVEL_NORMAL;
			}

#else /* CONFIG_BATTERY_BQ27421_SOC1_SOCF_SWAPPED */

			if (di->cache.flags & BQ27421_FLAG_SOCF) {
				level = POWER_SUPPLY_CAPACITY_LEVEL_CRITICAL;
			} else if (di->cache.flags & BQ27421_FLAG_SOC1) {
				level = POWER_SUPPLY_CAPACITY_LEVEL_LOW;
			} else if (di->cache.flags & BQ27421_FLAG_FC) {
				level = POWER_SUPPLY_CAPACITY_LEVEL_FULL;
			} else {
				level = POWER_SUPPLY_CAPACITY_LEVEL_NORMAL;
			}
#endif /* CONFIG_BATTERY_BQ27421_SOC1_SOCF_SWAPPED */

			/**
			 * LBP-1362 FG indicates a critical low battery too early. We already check battery voltage
			 * so just change level to low when FG indicates a critical level.
			 * This prevents powering-off too early.
			 */
			if (level == POWER_SUPPLY_CAPACITY_LEVEL_CRITICAL) {
				level = POWER_SUPPLY_CAPACITY_LEVEL_LOW;
			}
		}

	} else {
		level = POWER_SUPPLY_CAPACITY_LEVEL_UNKNOWN;
	}

	val->intval = level;

	return 0;
}

/*
 * Return the battery Voltage in microvolts
 * Or < 0 if something fails.
 */
static int bq27x00_battery_voltage(struct bq27x00_device_info *di,
	union power_supply_propval *val)
{
	int volt;

	volt = bq27x00_read(di, BQ27x00_REG_VOLT, false);
	if (volt < 0)
		return volt;

	val->intval = volt * 1000;

	return 0;
}

static int bq27x00_battery_update_voltage_avg(struct bq27x00_device_info *di, int shift)
{
	int ret;
	int i;
	union power_supply_propval voltage;
	int sum;
	int avg;
	int n;

	/* Read current voltage */
	ret = bq27x00_battery_voltage(di, &voltage);
	if (ret < 0) {
		/* If it fails, use 'glitch' voltage as sample value. See 'Note 1' for details. If pdata is null, use 0 as sample, since we don't have platform data*/
		if (di->pdata != NULL)
			voltage.intval = di->pdata->bq27421_glitch_voltage;
		else
			voltage.intval = 0;
	}
	/* Average */
	sum = voltage.intval;
	for (i = 0; i < di->n_previous_voltage_samples; i++) {
		sum += di->previous_voltage_samples[i];
	}

	avg = sum / (di->n_previous_voltage_samples + 1);

	/* Shift previous samples */
	if (shift) {
		if (di->n_previous_voltage_samples < BQ27421_MAX_PREVIOUS_VOLTAGE_SAMPLES) {
			n = di->n_previous_voltage_samples;
			di->n_previous_voltage_samples++;
		} else {
			n = BQ27421_MAX_PREVIOUS_VOLTAGE_SAMPLES - 1;
		}
		for (i = n; i > 0; i--) {
			di->previous_voltage_samples[i] = di->previous_voltage_samples[i-1];
		}
		di->previous_voltage_samples[0] = voltage.intval;
	}

	return avg;
}

/*
 * Return the battery Available energy in µWh
 * Or < 0 if something fails.
 */
static int bq27x00_battery_energy(struct bq27x00_device_info *di,
	union power_supply_propval *val)
{
	int ae;

	ae = bq27x00_read(di, BQ27x00_REG_AE, false);
	if (ae < 0) {
		dev_err(di->dev, "error reading available energy\n");
		return ae;
	}

	if (di->chip == BQ27500 || di->chip == BQ27421)
		ae *= 1000;
	else
		ae = ae * 29200 / BQ27000_RS;

	val->intval = ae;

	return 0;
}


static int bq27x00_simple_value(int value,
	union power_supply_propval *val)
{
	if (value < 0)
		return value;

	val->intval = value;

	return 0;
}

#define to_bq27x00_device_info(x) container_of((x), \
				struct bq27x00_device_info, bat);

static int bq27x00_battery_get_property(struct power_supply *psy,
					enum power_supply_property psp,
					union power_supply_propval *val)
{
	int ret = 0;
	struct bq27x00_device_info *di = to_bq27x00_device_info(psy);

	/*
	 * Update change if user space told us to do so or when last
	 * update is more than 5 seconds ago.
	 */
	mutex_lock(&di->lock);
	if (di->invalid_cache || time_is_before_jiffies(di->last_update + 5 * HZ)) {
		cancel_delayed_work_sync(&di->work);
		bq27x00_battery_poll(&di->work.work);
	}
	mutex_unlock(&di->lock);

	if (psp != POWER_SUPPLY_PROP_PRESENT && di->cache.flags < 0)
		return -ENODEV;

	switch (psp) {
	case POWER_SUPPLY_PROP_STATUS:
		ret = bq27x00_battery_status(di, val);
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		ret = bq27x00_battery_voltage(di, val);
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_AVG:
		ret = bq27x00_simple_value(di->cache.voltage_avg, val);
		break;
	case POWER_SUPPLY_PROP_PRESENT:
		val->intval = di->cache.flags < 0 ? 0 : 1;
		break;
	case POWER_SUPPLY_PROP_CURRENT_NOW:
		ret = bq27x00_battery_current(di, val);
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		if (di->fake_capacity == -1)
			ret = bq27x00_simple_value(di->cache.capacity, val);
		else
			ret = bq27x00_simple_value(di->fake_capacity, val);
		break;
	case POWER_SUPPLY_PROP_CAPACITY_LEVEL:
		ret = bq27x00_capacity_level(di, val);
		break;
	case POWER_SUPPLY_PROP_TEMP:
		ret = bq27x00_battery_temperature(di, val);
		break;
	case POWER_SUPPLY_PROP_TIME_TO_EMPTY_NOW:
		ret = bq27x00_simple_value(di->cache.time_to_empty, val);
		break;
	case POWER_SUPPLY_PROP_TIME_TO_EMPTY_AVG:
		ret = bq27x00_simple_value(di->cache.time_to_empty_avg, val);
		break;
	case POWER_SUPPLY_PROP_TIME_TO_FULL_NOW:
		ret = bq27x00_simple_value(di->cache.time_to_full, val);
		break;
	case POWER_SUPPLY_PROP_TECHNOLOGY:
		val->intval = POWER_SUPPLY_TECHNOLOGY_LION;
		break;
	case POWER_SUPPLY_PROP_CHARGE_NOW:
		ret = bq27x00_simple_value(bq27x00_battery_read_nac(di), val);
		break;
	case POWER_SUPPLY_PROP_CHARGE_FULL:
		ret = bq27x00_simple_value(di->cache.charge_full, val);
		break;
	case POWER_SUPPLY_PROP_CHARGE_FULL_DESIGN:
		ret = bq27x00_simple_value(di->charge_design_full, val);
		break;
	case POWER_SUPPLY_PROP_CYCLE_COUNT:
		ret = bq27x00_simple_value(di->cache.cycle_count, val);
		break;
	case POWER_SUPPLY_PROP_ENERGY_NOW:
		ret = bq27x00_battery_energy(di, val);
		break;
	default:
		return -EINVAL;
	}

	return ret;
}

#ifdef CONFIG_BATTERY_BQ27X00_SIGPWR

extern void normal_power_off(void);

static int bq27x00_check_battery_power_failure(struct bq27x00_device_info *di)
{
	int ret;
	union power_supply_propval level;
	union power_supply_propval status;
	int external_power;

	ret = bq27x00_capacity_level(di, &level);
	if (ret != 0)
		return ret;

	external_power = power_supply_am_i_supplied(&di->bat);
	if (external_power < 0)
		return external_power;

	ret = bq27x00_battery_status(di, &status);
	if (ret != 0)
		return ret;

	dev_dbg(di->dev, "checking battery power failure. level=%d, enternal_power=%d, status=%d\n", level.intval, external_power, status.intval);

	/**
	 * Send SIGPWR when battery is critical low and external power is not connected.
	 * When battery is critical low and we have external power, we only send SIGPWR when
	 * battery is not charging. This might happen when device needs more current than
	 * can be provided by the external power supply or is too hot to charge.
	 * We make sure we send the SIGPWR only after systemd service has started, by checking
	 * if we have measured at least 'BQ27421_MAX_PREVIOUS_VOLTAGE_SAMPLES' voltage samples
	 * (which happens after system boot).
	 * kill_sent is used to make sure SIGPWR is sent only once during the low battery condition.
	 */
	if (level.intval == POWER_SUPPLY_CAPACITY_LEVEL_CRITICAL &&
			(!external_power || status.intval != POWER_SUPPLY_STATUS_CHARGING) && !kill_sent) {

		dev_info(di->dev, "Battery critical low and discharging! Sending SIGPWR.\n");

		if (system_state == SYSTEM_RUNNING) {
			/* Send SIGPWR to PID 1. */
			ret = kill_cad_pid(SIGPWR, 0);
			if (ret == 0)
				kill_sent = 1;
		}
		else {
			/* The userspace is not started yet. In this case we power off directly */
			normal_power_off();
		}

		return -1;
	}

	return 0;
}
#else /* CONFIG_BATTERY_BQ27X00_SIGPWR */

static int bq27x00_check_battery_power_failure(struct bq27x00_device_info *di) {}

#endif

static void bq27x00_external_power_changed(struct power_supply *psy)
{
	struct bq27x00_device_info *di = to_bq27x00_device_info(psy);

	dev_dbg(di->dev, "bq27x00_external_power_changed\n");

	cancel_delayed_work_sync(&di->work);
	schedule_delayed_work(&di->work, 0);
}

static int bq27x00_powersupply_init(struct bq27x00_device_info *di)
{
	int ret;

	di->bat.type = POWER_SUPPLY_TYPE_BATTERY;
	if (di->chip == BQ27421) {
		di->bat.properties = bq27421_battery_props;
		di->bat.num_properties = ARRAY_SIZE(bq27421_battery_props);
	} else {
		di->bat.properties = bq27x00_battery_props;
		di->bat.num_properties = ARRAY_SIZE(bq27x00_battery_props);
	}
	di->bat.get_property = bq27x00_battery_get_property;
	di->bat.external_power_changed = bq27x00_external_power_changed;

	INIT_DELAYED_WORK(&di->work, bq27x00_battery_poll);
	mutex_init(&di->lock);

	di->invalid_cache = 1;

	ret = power_supply_register(di->dev, &di->bat);
	if (ret) {
		dev_err(di->dev, "failed to register battery: %d\n", ret);
		return ret;
	}
	di->ps_registered = 1;

	dev_info(di->dev, "support ver. %s enabled\n", DRIVER_VERSION);

	bq27x00_update(di);

	return 0;
}

static void bq27x00_powersupply_unregister(struct bq27x00_device_info *di)
{
	cancel_delayed_work_sync(&di->work);

	di->ps_registered = 0;
	power_supply_unregister(&di->bat);

	mutex_destroy(&di->lock);
}

static ssize_t show_fake_capacity(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);

	return sprintf(buf, "%d\n", di->fake_capacity);
}

static ssize_t store_fake_capacity(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	long value;
	struct bq27x00_device_info *di = dev_get_drvdata(dev);

	if (strict_strtol(buf, 0, &value))
		return -EINVAL;

	di->fake_capacity = value;

	return count;
}

static ssize_t show_fake_capacity_level(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);

	return sprintf(buf, "%d\n", di->fake_capacity_level);
}

static ssize_t store_fake_capacity_level(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	long value;
	struct bq27x00_device_info *di = dev_get_drvdata(dev);

	if (strict_strtol(buf, 0, &value))
		return -EINVAL;

	di->fake_capacity_level = value;

	return count;
}

static ssize_t show_real_capacity(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);

	return sprintf(buf, "%d\n", di->cache.capacity);
}

static ssize_t store_invalidate_cache(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	long value;
	struct bq27x00_device_info *di = dev_get_drvdata(dev);

	if (strict_strtol(buf, 0, &value))
		return -EINVAL;

	if (value == 1)
		di->invalid_cache = 1;

	return count;
}

static DEVICE_ATTR(fake_capacity, S_IRUGO | S_IWUGO, show_fake_capacity, store_fake_capacity);
static DEVICE_ATTR(fake_capacity_level, S_IRUGO | S_IWUGO, show_fake_capacity_level, store_fake_capacity_level);
static DEVICE_ATTR(real_capacity, S_IRUGO, show_real_capacity, NULL);
static DEVICE_ATTR(invalidate_cache, S_IWUGO, NULL, store_invalidate_cache);

/* i2c specific code */
#ifdef CONFIG_BATTERY_BQ27X00_I2C

/* If the system has several batteries we need a different name for each
 * of them...
 */
static DEFINE_IDR(battery_id);
static DEFINE_MUTEX(battery_mutex);

static int bq27x00_read_i2c(struct bq27x00_device_info *di, u8 reg, bool single)
{
	struct i2c_client *client = to_i2c_client(di->dev);
	struct i2c_msg msg[2];
	unsigned char data[2];
	int ret;

	if (!client->adapter)
		return -ENODEV;

	msg[0].addr = client->addr;
	msg[0].flags = 0;
	msg[0].buf = &reg;
	msg[0].len = sizeof(reg);
	msg[1].addr = client->addr;
	msg[1].flags = I2C_M_RD;
	msg[1].buf = data;
	if (single)
		msg[1].len = 1;
	else
		msg[1].len = 2;

	ret = i2c_transfer(client->adapter, msg, ARRAY_SIZE(msg));
	if (ret < 0)
		return ret;

	if (!single)
		ret = get_unaligned_le16(data);
	else
		ret = data[0];

	return ret;
}

static int bq27x00_write_i2c(struct bq27x00_device_info *di, u8 reg, int value, bool single)
{
	struct i2c_client *client = to_i2c_client(di->dev);
	struct i2c_msg msg[2];
	unsigned char data[2];
	int ret;

	if (!client->adapter)
		return -ENODEV;

	if (!single)
		put_unaligned_le16(value, data);
	else
		data[0] = value;

	msg[0].addr = client->addr;
	msg[0].flags = 0;
	msg[0].buf = &reg;
	msg[0].len = sizeof(reg);
	msg[1].addr = client->addr;
	msg[1].flags = 0;
	msg[1].buf = data;
	if (single)
		msg[1].len = 1;
	else
		msg[1].len = 2;

	ret = i2c_transfer(client->adapter, msg, ARRAY_SIZE(msg));
	if (ret < 0)
		return ret;

	return 0;
}

static int bq27x00_control_cmd(struct bq27x00_device_info *di, int sub_cmd)
{
	int ret;

	mutex_lock(&di->lock_control);

	ret = bq27x00_write_i2c(di, CONTROL_CMD, sub_cmd, false);
	if (ret < 0) {
		dev_err(di->dev, "Writing control command failed. Error: %d\n", ret);
		goto out;
	}

	usleep_range(66, 70);

	ret = bq27x00_read_i2c(di, CONTROL_CMD, false);
	if (ret < 0)
		dev_err(di->dev, "Reading control command result failed. Error: %d\n", ret);

out:
	mutex_unlock(&di->lock_control);
	return ret;
}

static int bq27x00_battery_read_fw_version(struct bq27x00_device_info *di)
{
	return bq27x00_control_cmd(di, FW_VER_SUBCMD);
}

static int bq27x00_battery_read_device_type(struct bq27x00_device_info *di)
{
	return bq27x00_control_cmd(di, DEV_TYPE_SUBCMD);
}

static int bq27x00_battery_read_dataflash_version(struct bq27x00_device_info *di)
{
	if (di->chip == BQ27421) {
		return 0x0;
	}

	return bq27x00_control_cmd(di, DF_VER_SUBCMD);
}

static ssize_t show_firmware_version(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);
	int ver;

	ver = bq27x00_battery_read_fw_version(di);
	if (ver < 0)
		return ver;

	return scnprintf(buf, PAGE_SIZE, "%d\n", ver);
}

static ssize_t show_dataflash_version(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);
	int ver;

	ver = bq27x00_battery_read_dataflash_version(di);
	if (ver < 0)
		return ver;

	return scnprintf(buf, PAGE_SIZE, "%d\n", ver);
}

static ssize_t show_device_type(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);
	int dev_type;

	dev_type = bq27x00_battery_read_device_type(di);
	if (dev_type < 0)
		return dev_type;

	return scnprintf(buf, PAGE_SIZE, "%d\n", dev_type);
}

static ssize_t show_reset(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int ret;
	struct bq27x00_device_info *di = dev_get_drvdata(dev);

	dev_info(di->dev, "Gas Gauge Reset\n");

	ret = bq27x00_control_cmd(di, RESET_SUBCMD);
	if (ret < 0)
		return ret;

	return scnprintf(buf, PAGE_SIZE, "okay\n");
}

static ssize_t show_health_percentage(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);
	int percentage;

	percentage = bq27x00_battery_read_soh_percentage(di);
	if (percentage < 0)
		return percentage;

	return scnprintf(buf, PAGE_SIZE, "%d\n", percentage);
}

static ssize_t show_health_status(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct bq27x00_device_info *di = dev_get_drvdata(dev);
	int status;

	status = bq27x00_battery_read_soh_status(di);
	if (status < 0)
		return status;

	return scnprintf(buf, PAGE_SIZE, "%d\n", status);
}

static DEVICE_ATTR(fw_version, S_IRUGO, show_firmware_version, NULL);
static DEVICE_ATTR(df_version, S_IRUGO, show_dataflash_version, NULL);
static DEVICE_ATTR(device_type, S_IRUGO, show_device_type, NULL);
static DEVICE_ATTR(reset, S_IRUGO, show_reset, NULL);
static DEVICE_ATTR(health_percentage, S_IRUGO, show_health_percentage, NULL);
static DEVICE_ATTR(health_status, S_IRUGO, show_health_status, NULL);

static struct attribute *bq27x00_attributes[] = {
	&dev_attr_fw_version.attr,
	&dev_attr_df_version.attr,
	&dev_attr_device_type.attr,
	&dev_attr_reset.attr,
	&dev_attr_health_percentage.attr,
	&dev_attr_health_status.attr,
	&dev_attr_fake_capacity.attr,
	&dev_attr_fake_capacity_level.attr,
	&dev_attr_real_capacity.attr,
	&dev_attr_invalidate_cache.attr,
	NULL
};

static const struct attribute_group bq27x00_attr_group = {
	.attrs = bq27x00_attributes,
};

static int bq27x00_battery_probe(struct i2c_client *client,
				 const struct i2c_device_id *id)
{
	char *name;
	struct bq27x00_device_info *di;
	int num;
	int retval = 0;

	/* Get new ID for the new battery device */
	retval = idr_pre_get(&battery_id, GFP_KERNEL);
	if (retval == 0)
		return -ENOMEM;
	mutex_lock(&battery_mutex);
	retval = idr_get_new(&battery_id, client, &num);
	mutex_unlock(&battery_mutex);
	if (retval < 0)
		return retval;

	name = kasprintf(GFP_KERNEL, "%s-%d", id->name, num);
	if (!name) {
		dev_err(&client->dev, "failed to allocate device name\n");
		retval = -ENOMEM;
		goto batt_failed_1;
	}

	di = kzalloc(sizeof(*di), GFP_KERNEL);
	if (!di) {
		dev_err(&client->dev, "failed to allocate device info data\n");
		retval = -ENOMEM;
		goto batt_failed_2;
	}

	di->id = num;
	di->dev = &client->dev;
	di->chip = id->driver_data;
	di->bat.name = name;
	di->bus.read = &bq27x00_read_i2c;
	di->bus.write = &bq27x00_write_i2c;
	di->fake_capacity = -1; /* Disabled */
	di->fake_capacity_level = -1;  /* Disabled */
	if ((di->pdata = client->dev.platform_data) == NULL)
		dev_info(&client->dev,
		"WARNING: No platform data is supplied!");

	mutex_init(&di->lock_control);

	/* Get the fw version to determine the register mapping */
	di->fw_ver = bq27x00_battery_read_fw_version(di);
	di->df_ver = bq27x00_battery_read_dataflash_version(di);
	dev_info(&client->dev,
		"Gas Gauge fw version 0x%04x; df version 0x%04x\n",
		di->fw_ver, di->df_ver);

	if (di->chip == BQ27421)
		di->regs = bq27421_regs;
	else if (di->fw_ver == L1_FW_VERSION)
		di->regs = bq27x00_fw_l1_regs;
	else if (di->fw_ver == G3_FW_VERSION)
		di->regs = bq27x00_fw_g3_regs;
	else {
		dev_err(&client->dev,
			"Unkown Gas Gauge fw version: 0x%04x\n", di->fw_ver);
		di->regs = bq27x00_fw_g3_regs;
	}

	if (bq27x00_powersupply_init(di))
		goto batt_failed_3;

	i2c_set_clientdata(client, di);
	retval = sysfs_create_group(&client->dev.kobj, &bq27x00_attr_group);
	if (retval)
		dev_err(&client->dev, "could not create sysfs files\n");

	return 0;

batt_failed_3:
	mutex_destroy(&di->lock_control);
	kfree(di);
batt_failed_2:
	kfree(name);
batt_failed_1:
	mutex_lock(&battery_mutex);
	idr_remove(&battery_id, num);
	mutex_unlock(&battery_mutex);

	return retval;
}

static int bq27x00_battery_remove(struct i2c_client *client)
{
	struct bq27x00_device_info *di = i2c_get_clientdata(client);

	bq27x00_powersupply_unregister(di);

	mutex_destroy(&di->lock_control);
	kfree(di->bat.name);

	mutex_lock(&battery_mutex);
	idr_remove(&battery_id, di->id);
	mutex_unlock(&battery_mutex);

	kfree(di);

	return 0;
}

static int bq27x00_battery_suspend(struct i2c_client *client)
{
	struct bq27x00_device_info *di = i2c_get_clientdata(client);

	return bq27x00_update(di);
}

static int bq27x00_battery_resume(struct i2c_client *client)
{
	struct bq27x00_device_info *di = i2c_get_clientdata(client);

	di->invalid_cache = 1;

	bq27x00_update(di);

	return 0;
}

static const struct i2c_device_id bq27x00_id[] = {
	{ "bq27200", BQ27000 },	/* bq27200 is same as bq27000, but with i2c */
	{ "bq27500", BQ27500 },
	{ "bq27421", BQ27421 },
	{},
};
MODULE_DEVICE_TABLE(i2c, bq27x00_id);

static struct i2c_driver bq27x00_battery_driver = {
	.driver = {
		.name = "bq27x00-battery",
	},
	.probe = bq27x00_battery_probe,
	.remove = bq27x00_battery_remove,
	.suspend = bq27x00_battery_suspend,
	.resume = bq27x00_battery_resume,
	.id_table = bq27x00_id,
};

static inline int bq27x00_battery_i2c_init(void)
{
	int ret = i2c_add_driver(&bq27x00_battery_driver);
	if (ret)
		printk(KERN_ERR "Unable to register BQ27x00 i2c driver\n");

	return ret;
}

static inline void bq27x00_battery_i2c_exit(void)
{
	i2c_del_driver(&bq27x00_battery_driver);
}

#else

static inline int bq27x00_battery_i2c_init(void) { return 0; }
static inline void bq27x00_battery_i2c_exit(void) {};

#endif

/* platform specific code */
#ifdef CONFIG_BATTERY_BQ27X00_PLATFORM

static int bq27000_read_platform(struct bq27x00_device_info *di, u8 reg,
			bool single)
{
	struct device *dev = di->dev;
	struct bq27000_platform_data *pdata = dev->platform_data;
	unsigned int timeout = 3;
	int upper, lower;
	int temp;

	if (!single) {
		/* Make sure the value has not changed in between reading the
		 * lower and the upper part */
		upper = pdata->read(dev, reg + 1);
		do {
			temp = upper;
			if (upper < 0)
				return upper;

			lower = pdata->read(dev, reg);
			if (lower < 0)
				return lower;

			upper = pdata->read(dev, reg + 1);
		} while (temp != upper && --timeout);

		if (timeout == 0)
			return -EIO;

		return (upper << 8) | lower;
	}

	return pdata->read(dev, reg);
}

static int __devinit bq27000_battery_probe(struct platform_device *pdev)
{
	struct bq27x00_device_info *di;
	struct bq27000_platform_data *pdata = pdev->dev.platform_data;
	int ret;

	if (!pdata) {
		dev_err(&pdev->dev, "no platform_data supplied\n");
		return -EINVAL;
	}

	if (!pdata->read) {
		dev_err(&pdev->dev, "no hdq read callback supplied\n");
		return -EINVAL;
	}

	di = kzalloc(sizeof(*di), GFP_KERNEL);
	if (!di) {
		dev_err(&pdev->dev, "failed to allocate device info data\n");
		return -ENOMEM;
	}

	platform_set_drvdata(pdev, di);

	di->dev = &pdev->dev;
	di->chip = BQ27000;

	di->bat.name = pdata->name ?: dev_name(&pdev->dev);
	di->bus.read = &bq27000_read_platform;
	di->fake_capacity = -1; /* Disabled */
	di->fake_capacity_level = -1; /* Disabled */

	ret = bq27x00_powersupply_init(di);
	if (ret)
		goto err_free;

	return 0;

err_free:
	platform_set_drvdata(pdev, NULL);
	kfree(di);

	return ret;
}

static int __devexit bq27000_battery_remove(struct platform_device *pdev)
{
	struct bq27x00_device_info *di = platform_get_drvdata(pdev);

	bq27x00_powersupply_unregister(di);

	platform_set_drvdata(pdev, NULL);
	kfree(di);

	return 0;
}

static int bq27000_battery_resume(struct platform_device *pdev)
{
	struct bq27x00_device_info *di = platform_get_drvdata(pdev);

	di->invalid_cache = 1;

	return 0;
}

static struct platform_driver bq27000_battery_driver = {
	.probe	= bq27000_battery_probe,
	.remove = __devexit_p(bq27000_battery_remove),
	.resume = bq27000_battery_resume,
	.driver = {
		.name = "bq27000-battery",
		.owner = THIS_MODULE,
	},
};

static inline int bq27x00_battery_platform_init(void)
{
	int ret = platform_driver_register(&bq27000_battery_driver);
	if (ret)
		printk(KERN_ERR "Unable to register BQ27000 platform driver\n");

	return ret;
}

static inline void bq27x00_battery_platform_exit(void)
{
	platform_driver_unregister(&bq27000_battery_driver);
}

#else

static inline int bq27x00_battery_platform_init(void) { return 0; }
static inline void bq27x00_battery_platform_exit(void) {};

#endif

/*
 * Module stuff
 */

static int __init bq27x00_battery_init(void)
{
	int ret;

	ret = bq27x00_battery_i2c_init();
	if (ret)
		return ret;

	ret = bq27x00_battery_platform_init();
	if (ret)
		bq27x00_battery_i2c_exit();

	return ret;
}
module_init(bq27x00_battery_init);

static void __exit bq27x00_battery_exit(void)
{
	bq27x00_battery_platform_exit();
	bq27x00_battery_i2c_exit();
}
module_exit(bq27x00_battery_exit);

MODULE_AUTHOR("Rodolfo Giometti <giometti@linux.it>");
MODULE_DESCRIPTION("BQ27x00 battery monitor driver");
MODULE_LICENSE("GPL");
