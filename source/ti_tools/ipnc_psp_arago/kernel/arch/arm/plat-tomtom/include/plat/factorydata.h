
#ifndef __TOMTOM_FACTORYDATA_H__
#define __TOMTOM_FACTORYDATA_H__

struct factorydata_buffer_info_t {
	u32 address;
	u32 size;
};

#endif /* __TOMTOM_FACTORYDATA_H__ */

