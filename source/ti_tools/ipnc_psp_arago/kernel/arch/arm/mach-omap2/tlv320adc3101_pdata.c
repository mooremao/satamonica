/*
 * Copyright (C) 2009 TomTom BV <http://www.tomtom.com/>
 * Authors: Niels Langendorff <niels.langendorff@tomtom.com>
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <plat/tlv320adc3101_pdata.h>
#include <linux/io.h>
#include <linux/delay.h>
#include <mach/gpio.h>
//#include <linux/vgpio.h>
//#include <plat/mendoza.h>
//#include <plat/gpio-cfg.h>

#define AUDIO_RESET_GPIO (3*32+0) // for Longbeach

static int tlv320adc3101_gpio_request(void)
{
	int err = 0;

	if ((err = gpio_request(AUDIO_RESET_GPIO, "AUDIO_RESET_GPIO"))) {
		printk("%s: Can't request TT_VGPIO_MIC_RESET GPIO\n", __func__);
	}

	return err;
}

static void tlv320adc3101_gpio_free(void)
{
	gpio_free(AUDIO_RESET_GPIO);
}

static void tlv320adc3101_config_gpio(void)
{
	gpio_direction_output(AUDIO_RESET_GPIO, 0);
}

static void mic_reset(int activate)
{
	if(activate)
		gpio_set_value(AUDIO_RESET_GPIO, 0);
	else
		gpio_set_value(AUDIO_RESET_GPIO, 1);
}

static void tlv320adc3101_suspend (void)
{
	gpio_direction_output(AUDIO_RESET_GPIO, 0);
}

static void tlv320adc3101_resume (void)
{
	gpio_direction_output(AUDIO_RESET_GPIO, 1);
}

tlv320adc3101_platform_t tlv320adc3101_pdata = {
	.mic_reset	= mic_reset,

	.suspend		= tlv320adc3101_suspend,
	.resume			= tlv320adc3101_resume,

	.config_gpio	= tlv320adc3101_config_gpio,
	.request_gpio	= tlv320adc3101_gpio_request,
	.free_gpio		= tlv320adc3101_gpio_free,
};

