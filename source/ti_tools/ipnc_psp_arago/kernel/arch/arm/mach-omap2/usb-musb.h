#ifndef __ARCH_USB_MUSB_H
#define __ARCH_USB_MUSB_H

#if defined(CONFIG_USB_MUSB_OMAP2PLUS) || defined(CONFIG_USB_MUSB_AM35X) \
                || defined(CONFIG_USB_MUSB_TI81XX)

extern void ti81xx_musb_phy_power(u8 id, u8 on);
#define MUSB_PHY_POWER(id, on) ti81xx_musb_phy_power(id, on);

#else

#define MUSB_PHY_POWER(id, on)

#endif

#endif /* __ARCH_USB_MUSB_H */
