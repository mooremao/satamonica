/*
 * Code for DM385 EVM.
 *
 * Copyright (C) 2010 Texas Instruments, Inc. - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/platform_device.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/gpio.h>
#include <linux/gpio_keys.h>
#include <linux/leds_pwm.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <linux/phy.h>
#include <linux/i2c/at24.h>
#include <linux/i2c/mpu3050.h>
#include <linux/input/tca8418_keypad.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/tps62360.h>
#include <linux/mfd/tps65910.h>
#include <linux/wl12xx.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/reboot.h>
#include <linux/power/bq27x00_battery.h>
#include <linux/ti_wilink_st.h>

#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/system.h>

#include <plat/mcspi.h>
#include <plat/irqs.h>
#include <plat/board.h>
#include <plat/common.h>
#include <plat/asp.h>
#include <plat/usb.h>
#include <plat/mmc.h>
#include <plat/gpmc.h>
#include <plat/nand.h>
#include <plat/hdmi_lib.h>
#include <plat/dmtimer.h>
#include <mach/board-ti814x.h>
#include <plat/board_id.h>
#include <video/ls013b7dh01.h>
#include <linux/tomtom/flipflop.h>
#include <linux/tomtom/mcu.h>
#include <linux/tomtom/reboot_counter.h>
#ifdef CONFIG_SPI_CSRGSD5XP_MODULE
#define CONFIG_SPI_CSRGSD5XP
#endif
#ifdef CONFIG_SPI_CSRGSD5XP
#include <linux/spi/spi_csrgsd5xp.h>
#endif
#ifdef CONFIG_HAVE_PWM
#include <plat/pwm.h>
#endif

#include "board-flash.h"
#include "clock.h"
#include "mux.h"
#include "hsmmc.h"
#include "control.h"
#include "timer-gp.h"
#include "longbeach_usb.h"

#ifdef CONFIG_SND_SOC_TLV320ADC3101
#include <plat/tlv320adc3101_pdata.h>
#endif

#include <plat/factorydata.h>

#include <linux/clocksource.h>

#define GPS_WAKE_GPIO (2*32+29)
#define GPS_RESET_GPIO (3*32+3)
#define GPS_ON_OFF_GPIO (2*32+30)
#define GPS_DATA_READY_GPIO (1*32+11)
#define BT_PWEN_GPIO (3*32+2)

#define AUDIO_RESET_GPIO (3*32+0)
#define NAND_WP_GPIO (3*32+13)
#define KILL_POWER_GPIO_WS1 (1*32+24)
#define KILL_POWER_GPIO_WS2 (3*32+23)

#define DISP_ON_GPIO 54
#define DISP_5V_ON_GPIO 35

#define FLIPFLOP_BOOT_CLK_GPIO	((2 * 32) + 2)	/* BOOT_CLK is GP2[2] */
#define FLIPFLOP_BOOT_Q_GPIO	((3 * 32) + 21)	/* BOOT_Q is GP3[21] */

#define GPIO_HOST_INT	((0 * 32) + 10)	/* HOST_INT is GP0[10] */
#define GPIO_VBUS_ON	((0 * 32) + 11)	/* VBUS_ON is GP0[11] */

#define GPIO_DEVOSC_WAKE		((1 * 32) + 7)	/* DEVOSC_WAKE is GP1[7] */

#define GPIO_WLAN_EN	((3 * 32) + 1) /* WLAN_EN is GP3[1] */
#define GPIO_WLAN_IRQ	((2 * 32) + 16) /* WLAN_IRQ is GP2[16] */

#define GPIO_IMAGE_PWR_EN ((1 * 32) + 16) /* IMAGE_PWR_EN is GP1[16] */

#define GP_TIMER_ID__CLOCKEVENT 1
#define GP_TIMER_ID__LED1 2
#define GP_TIMER_ID__PWR_TIMER 3 //Used for measuring the time from reset
#define GP_TIMER_ID__CLOCKSOURCE 4 //Not used when OMAP_32K_TIMER
#define GP_TIMER_ID__BACKLIGHT 5
#define GP_TIMER_ID__DISP_EXTCOM 6
#define GP_TIMER_ID__BUZZER 7
#define GP_TIMER_ID__PERSISTENT 8 /* Used for measuring time during suspend */

//On W1: DISP_EXTCOM and Buzzer timers are swapped.
#define GP_TIMER_ID__WS1_DISP_EXTCOM 6
#define GP_TIMER_ID__WS1_BUZZER 7

#define PWM_ID__LED1 0
#define PWM_ID__BACKLIGHT 1
#define PWM_ID__DISP_EXTCOM 2
#define PWM_ID__BUZZER 3

#define GPIO_BUTTONS_ID 0
#define GPIO_BATLOCK_ID 1

#define GPIO_BUT_RECORD         (2*32+7)
#define GPIO_BUT_OFF (2*32+10)
#define GPIO_BUT_UP     (2*32+8)
#define GPIO_BUT_DOWN (2*32+9)

#define GPIO_BACK_CAP_DET (0*32+17)

#define GPIO_DEBOUNCE_TIME 8 /* debounce time in msecs */

#define FLIPFLOP_RECOVERY_BOOT_SCRATCH_REG_NUM 0 /* Shared with reboot counter. See 'Note 1'. */
#define FLIPFLOP_RECOVERY_BOOT_MAGIC 0x73647570
#define FLIPFLOP_RECOVERY_BOOT_UPDATE_MAGIC (~FLIPFLOP_RECOVERY_BOOT_MAGIC)

#define FLIPFLOP_FASTBOOT_SCRATCH_REG_NUM 1
#define FLIPFLOP_FASTBOOT_MAGIC 0xFA51B001

#define FUEL_GAUGE "bq27421"

#define MMC_CONTROLLER_WLAN      1
#define MMC_CONTROLLER_BOOT_CARD 2
#define MMC_CONTROLLER_SD_CARD   3

#define BQ27421_BOOT_VOLTAGE 3300000
#define BQ27421_SHUTDOWN_VOLTAGE 2900000
#define BQ27421_GLITCH_VOLTAGE (BQ27421_SHUTDOWN_VOLTAGE-50000)

#if defined(CONFIG_KEYBOARD_GPIO) || defined(CONFIG_KEYBOARD_GPIO_MODULE)
static struct omap_dm_timer *dm_timer;
static bool booted = false;
#define ON_TIME	1000
#endif

static bool coldboot = false;

static bool usb_poweron = false;
static bool too_short_poweron = false;
static bool usb_poweron_check_done = false;
extern char* saved_command_line;

#define USB_POWERON_BOOTREASON "boot_reason=usb"
#define SYSTEMD_USB_TARGET "systemd.unit=usb.target"

/**
 * Note 1. The reboot counter is used to reboot into recovery after a number of reboots
 * This reboot counter is stored in the RTC scratch register number 1. We use a RTC
 * scratch register to keep the value over reboots without storing it persistently.
 * This register is shared with the fastboot flip flop. This is the only one that is
 * not changed during boot by u-boot and not used by the recovery kernel. Next, when
 * you want to enter fastboot, you don't care about the reboot counter.
 *
 * Note 2. RTC scratch register number 1 is also used to store a nibble to indicate if next
 * boot is a 'USB Power On' boot.
 */

#define REBOOT_COUNTER_LIMIT 5
#define REBOOT_COUNTER_MASK  0xF000FFFF /* Lower 16 bits are use counter */
#define REBOOT_COUNTER_MAGIC 0x0EAD0000 /* Lower 16 bits are use counter */

#define USB_POWER_ON_MASK 0xF0000000 /* Upper 4 bits are used for USB Power on */
#define USB_POWER_ON_MAGIC 0xC0000000 /* Upper 4 bits are used for USB Power on */

#ifdef CONFIG_OMAP_MUX
static struct omap_board_mux board_mux[] __initdata = {
	{ .reg_offset = OMAP_MUX_TERMINATOR },
};
#else
#define board_mux     NULL
#endif

static struct omap2_hsmmc_info mmc[] = {
       {
		.mmc		= MMC_CONTROLLER_WLAN,
		.caps		= MMC_CAP_4_BIT_DATA | MMC_CAP_POWER_OFF_CARD,
		.gpio_cd	= -EINVAL,
		.gpio_wp	= -EINVAL,
		.ocr_mask	= MMC_VDD_165_195,
		.nonremovable	= true,
	},
#ifdef SD_BOOT_ENABLE_MMC1
	{
		.mmc            = MMC_CONTROLLER_BOOT_CARD,
		.caps           = MMC_CAP_4_BIT_DATA | MMC_CAP_NEEDS_POLL,
		.gpio_cd        = -EINVAL, /* Dedicated pins for CD and WP */
		.gpio_wp        = -EINVAL,
		.ocr_mask       = MMC_VDD_33_34,
	},
#endif
	{
		.mmc            = MMC_CONTROLLER_SD_CARD,
		.caps		= MMC_CAP_4_BIT_DATA | MMC_CAP_NEEDS_POLL,
		.gpio_cd	= -EINVAL, /* Dedicated pins for CD and WP */
		.gpio_wp	= -EINVAL,
		.ocr_mask	= MMC_VDD_33_34,
		.nonremovable	= true,
	},
	{}	/* Terminator */
};

static struct omap2_mcspi_device_config spidev2_config = {
	.turbo_mode = 0, 	/* (de)select Turbo mode */
	.single_channel = 1, 	/* 0: slave, 1: master */
};

static struct omap2_mcspi_device_config spidev3_config = {
	.turbo_mode = 0, 	/* (de)select Turbo mode */
	.single_channel = 1, 	/* 0: slave, 1: master */
};

static struct ls013b7dh01_platform_data ls013b7dh01_pdata = {
	.disp_on_gpio = DISP_ON_GPIO,
	.disp_5v_on_gpio = DISP_5V_ON_GPIO,
	.pwm_id = PWM_ID__DISP_EXTCOM,
	.pwm_freq = 30,
	.rotate = 180,
	.lcd_on_in_suspend = 1
};

#ifdef CONFIG_SPI_CSRGSD5XP
static struct csrgsd5xp_platform_data csrgsd5xp_pdata = {
	.gps_reset_gpio = GPS_RESET_GPIO,
	.gps_reset_active_high = 1, /* will be changed to 0 for WS1 board */
	.gps_on_off_gpio = GPS_ON_OFF_GPIO,
	.gps_wakeup_gpio = GPS_WAKE_GPIO,
	.gps_data_ready_gpio = GPS_DATA_READY_GPIO,
};
#endif

static struct spi_board_info __initdata dm385_spi_info[] = {
#ifdef CONFIG_SPI_CSRGSD5XP
	{
		.modalias	 = "spi_csrgsd5xp",
		.irq		 = -1,
		.max_speed_hz	 = 1000000,
		.bus_num	 = 2,
		.chip_select	 = 0,
		.controller_data = &spidev2_config,
		.mode = SPI_MODE_1,
		.platform_data = &csrgsd5xp_pdata
	},
#endif
#ifdef CONFIG_FB_SHARP_LS013B7DH01
	{
		.modalias	 = "ls013b7dh01",
		.irq		 = -1,
		.max_speed_hz	 = 2000000,
		.bus_num	 = 3,
		.chip_select	 = 0,
		.controller_data = &spidev3_config,
		.mode = (SPI_MODE_0|SPI_CS_HIGH),
		.platform_data = &ls013b7dh01_pdata
	},
#else
	{
		.modalias	 = "spidev",
		.irq		 = -1,
		.max_speed_hz	 = 1000000,
		.bus_num	 = 3,
		.chip_select	 = 0,
		.controller_data = &spidev3_config,
		.mode = (SPI_MODE_0|SPI_CS_HIGH),
	},
#endif //CONFIG_FB_SHARP_LS013B7DH01
};

static void __init dm385_spi_init(void)
{
	spi_register_board_info(dm385_spi_info,
				ARRAY_SIZE(dm385_spi_info));
}

/* BQ27421 platform voltages definitions */
struct bq27000_platform_data bq27421_platform_data = {
	.bq27421_boot_voltage = BQ27421_BOOT_VOLTAGE,
	.bq27421_shutdown_voltage = BQ27421_SHUTDOWN_VOLTAGE,
	.bq27421_glitch_voltage = BQ27421_GLITCH_VOLTAGE,
};

static struct i2c_board_info __initdata ti814x_i2c_boardinfo4[] = {
#if 0
	{
		I2C_BOARD_INFO("lsm303e_acc", 0x19),
	},
	{
		I2C_BOARD_INFO("lsm303c_acc", 0x1d),
	},
	{
		I2C_BOARD_INFO("lsm303e_mag", 0x1E),
	},
	{
		I2C_BOARD_INFO("bmp280", 0x77),
	},
#endif
};

static char *usbpower_supplicants[] = { FUEL_GAUGE "-0" };

static struct tomtom_mcu_info* mcu_info = NULL;

static void mcu_reg_callback(struct tomtom_mcu_info *m) {
	mcu_info = m;
}

static void mcu_unreg_callback(struct tomtom_mcu_info *m) {
	if (mcu_info == m)
		mcu_info = NULL;
}

int lbusb_is_dcp_charger_connected() {
	if (mcu_info == NULL || mcu_info->is_ac_online == NULL)
		return -1;

	return mcu_info->is_ac_online(mcu_info);
}

static struct tomtom_mcu_pdata tomtom_mcu_platform_data = {
	.host_int_gpio  = GPIO_HOST_INT,
	.vbus_on_gpio = GPIO_VBUS_ON,
	.usbpower_supplicants = usbpower_supplicants,
	.usbpower_num_supplicants = ARRAY_SIZE(usbpower_supplicants),
	.notify_vbus = lbusb_notify_vbus,
	.register_callback = mcu_reg_callback,
	.unregister_callback = mcu_unreg_callback
};

static struct i2c_board_info __initdata ti814x_i2c_boardinfo2[] = {
	{
		I2C_BOARD_INFO("sh1103", 0x3C),
	},
};

static void __init ti814x_evm_i2c2_init(void)
{	omap_register_i2c_bus(2, 400, ti814x_i2c_boardinfo2,
				ARRAY_SIZE(ti814x_i2c_boardinfo2));
}

static struct i2c_board_info __initdata ti814x_i2c_boardinfo3[] = {
	{
		I2C_BOARD_INFO("tmp103", 0x71),
	},
	{
		I2C_BOARD_INFO("tmp103", 0x72),
	},
};

static void __init ti814x_evm_i2c3_init(void)
{
	/* Enable IMAGE_PWR_EN to enable pull up on i2c bus 3 */
	/* Work-aroudn: Used for LB image sensor board only */
	gpio_request_one(GPIO_IMAGE_PWR_EN, GPIOF_OUT_INIT_HIGH, "image_pwr_en");

	/* There are 4 instances of I2C in DM385 but currently only two
	 * instances are being used on the DM385 LB
	 */
	omap_register_i2c_bus(3, 400, ti814x_i2c_boardinfo3,
				ARRAY_SIZE(ti814x_i2c_boardinfo3));
}

static void __init ti814x_evm_i2c4_init(void)
{
	/* There are 4 instances of I2C in DM385 but currently only two
	 * instances are being used on the DM385 LB
	 */
	omap_register_i2c_bus(4, 400, ti814x_i2c_boardinfo4,
				ARRAY_SIZE(ti814x_i2c_boardinfo4));
}
deferred_initcall(ti814x_evm_i2c4_init);

static u8 dm385_iis_serializer_direction_LB[] = {
	RX_MODE,	TX_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
};

static struct snd_platform_data dm385_evm_snd_data = {
	.tx_dma_offset	= 0x46000000,
	.rx_dma_offset	= 0x46000000,
	.op_mode	= DAVINCI_MCASP_IIS_MODE,
	.num_serializer = ARRAY_SIZE(dm385_iis_serializer_direction_LB),
	.tdm_slots	= 2,
	.serial_dir = dm385_iis_serializer_direction_LB,
	.asp_chan_q	= EVENTQ_0,
	.version	= MCASP_VERSION_2,
	.txnumevt	= 1,
	.rxnumevt	= 32,
};

/* NAND flash information */
static struct mtd_partition dm385lb_nand_partitions[] = {
/* All the partition sizes are listed in terms of NAND block size */
	{
		.name           = "uboot",
		.offset         = 0,    /* Offset = 0x0 */
		.size           = SZ_128K,
	},
	{
		.name           = "uboot2",
		.offset         = MTDPART_OFS_APPEND,/* Offset = 0x0 + 128K */
		.size           = 18 * SZ_128K,
	},
	{
		.name           = "environment",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x260000 */
		.size           = 5 * SZ_128K,
	},
	{
		.name           = "kernel",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x300000 */
		.size           = 12 * SZ_1M,
	},
	{
		.name           = "system",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0xF00000 */
		.size           = 80 * SZ_1M,
	},
	{
		.name           = "userdata",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x5F00000 */
		.size           = SZ_16M,
	},
	{
		.name           = "recovery",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x6F00000 */
		.size           = SZ_16M,
	},
	{
		.name           = "fdt",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x7F00000 */
		.size           = 2 * SZ_128K,
	},
	{
		.name           = "reserved",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x7F40000 */
		.size           = MTDPART_SIZ_FULL,
	},
};

static struct omap_musb_board_data musb_board_data = {
	.interface_type		= MUSB_INTERFACE_ULPI,
#ifdef CONFIG_USB_MUSB_OTG
	.mode           = MUSB_OTG,
#elif defined(CONFIG_USB_MUSB_HDRC_HCD)
	.mode           = MUSB_HOST,
#elif defined(CONFIG_USB_GADGET_MUSB_HDRC)
	.mode           = MUSB_PERIPHERAL,
#endif
	.power		= 500,
	.instances	= 1,
	.notify_connect = lbusb_notify_connect,
	.notify_config_change = lbusb_notify_config_change
};

#ifdef CONFIG_HAVE_PWM
//Update PWM_ID__* values when modifying order of pwm_cfg entries.
struct omap2_pwm_platform_config dm385_ipnc_pwm_cfg[] = {
	{
		.timer_id = GP_TIMER_ID__LED1, //Controlled by leds_pwm
		.polarity = 1,
	},
	{
		.timer_id = GP_TIMER_ID__BACKLIGHT, //Controlled by leds_pwm
		.polarity = 1,
	},
	{
		.timer_id = GP_TIMER_ID__DISP_EXTCOM, //Controlled by LCM module
		.polarity = 1,
	},
	{
		.timer_id = GP_TIMER_ID__BUZZER,
		.polarity = 1,
	}
};
#endif

static void __init dm385_evm_init_irq(void)
{
	omap2_init_common_infrastructure();
	omap2_init_common_devices(NULL, NULL);
	omap_init_irq();
	gpmc_init();
	omap2_gp_clockevent_set_gptimer(GP_TIMER_ID__CLOCKEVENT);
	omap2_gp_clocksource_set_gpttimer(GP_TIMER_ID__CLOCKSOURCE);
}

#ifdef CONFIG_WL12XX_PLATFORM_DATA

static int wlan_power_ctl = 0;

static struct wl12xx_platform_data wlan_data __initdata = {
	.irq = OMAP_GPIO_IRQ(GPIO_WLAN_IRQ),
	/* COM6 (127x) uses FREF */
	.board_ref_clock = WL12XX_REFCLOCK_26,
	/* COM7 (128x) uses TCXO */
	.board_tcxo_clock = WL12XX_TCXOCLOCK_26,
};

static ssize_t show_wlan_power_ctl(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", wlan_power_ctl);
}

static ssize_t set_wlan_power_ctl(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	if (count == 0 || *buf == '0')
		wlan_power_ctl = 0;
	else
		wlan_power_ctl = 1;

	return count;
}

static DEVICE_ATTR(wlan_power_ctl, 0644, show_wlan_power_ctl, set_wlan_power_ctl);

static int wl12xx_set_power(struct device *dev, int slot, int power_on,
			    int vdd)
{
	static bool power_state;

	/* Enable/disable of wlan power is allowed only when the user explicitly sets wlan_power_ctl via sysfs */
	/* This prevents the driver to power-up and probe the WiFi module at boot, ie before the GPS clock reaches a stable state */
	if (wlan_power_ctl) {
		pr_debug("Powering %s wl12xx", power_on ? "on" : "off");

		if (power_on == power_state)
			return 0;
		power_state = power_on;

		if (power_on) {
			gpio_set_value(GPIO_WLAN_EN, 1);
			msleep(70);
		} else {
			gpio_set_value(GPIO_WLAN_EN, 0);
		}
	}

	return 0;
}

static void __init ti814x_wl12xx_wlan_init(void)
{
	struct device *dev = NULL;
	struct omap_mmc_platform_data *pdata;
	struct omap2_hsmmc_info* c;
	int ret;

	/* Set up mmc0 muxes */
	omap_mux_init_signal("mmc0_clk", TI814X_PULL_UP);
	omap_mux_init_signal("mmc0_cmd", TI814X_PULL_UP);
	omap_mux_init_signal("mmc0_dat0", TI814X_PULL_UP);
	omap_mux_init_signal("mmc0_dat1", TI814X_PULL_UP);
	omap_mux_init_signal("mmc0_dat2", TI814X_PULL_UP);
	omap_mux_init_signal("mmc0_dat3", TI814X_PULL_UP);

        /* Set up the WLAN_EN and WLAN_IRQ muxes */
        //TomTom Longbeach Usage
        omap_mux_init_signal("gpio3_1", TI814X_PULL_DIS);
        omap_mux_init_signal("gpio2_16", TI814X_PULL_DIS);
	/* Pass the wl12xx platform data information to the wl12xx driver */
	if (wl12xx_set_platform_data(&wlan_data)) {
		pr_err("Error setting wl12xx data\n");
		return;
	}

	/*
	 * The WLAN_EN gpio has to be toggled without using a fixed regulator,
	 * as the omap_hsmmc does not enable/disable regulators on the TI814X.
	 */
	ret = gpio_request_one(GPIO_WLAN_EN, GPIOF_OUT_INIT_LOW, "wlan_en");
	if (ret) {
		pr_err("Error requesting wlan enable gpio: %d\n", ret);
		return;
	}

	/*
	 * Set our set_power callback function which will be called from
	 * set_ios. This is requireq since, unlike other omap2+ platforms, a
	 * no-op set_power function is registered. Thus, we cannot use a fixed
	 * regulator, as it will never be toggled.
	 * Moreover, even if this was not the case, we're on mmc0, for which
	 * omap_hsmmc' set_power functions do not toggle any regulators.
	 * TODO: Consider modifying omap_hsmmc so it would enable/disable a
	 * regulator for ti814x/mmc0.
	 */
	for (c = mmc; c->mmc; c++) {
		if (c->mmc == MMC_CONTROLLER_WLAN) {
			dev = c->dev;
			break;
		}
	}
	if (dev == NULL) {
		pr_err("wl12xx mmc device initialization failed\n");
		return;
	}

	pdata = dev->platform_data;
	if (!pdata) {
		pr_err("Platform data of wl12xx device not set\n");
		return;
	}

	ret = device_create_file(dev, &dev_attr_wlan_power_ctl);
	if (ret) {
		pr_err("Unable to create sysfs entry for wlan power control\n");
		return;
	}

	pdata->slots[0].set_power = wl12xx_set_power;
}

static void __init ti814x_wl12xx_bt_init(void)
{
	int ret;
	/* Set up uart1 muxes */
	omap_mux_init_signal("uart0_dtrn.uart1_txd_mux0", TI814X_PULL_DIS);
	omap_mux_init_signal("uart0_rin.uart1_rxd_mux0", TI814X_PULL_UP);

	/* uart flow control pins */
	omap_mux_init_signal("gmii0_txen.uart1_rtsn", TI814X_PULL_DIS);
	omap_mux_init_signal("gmii0_txd7.uart1_ctsn", TI814X_PULL_UP);

	/* Set up the BT_PWEN muxes */
	omap_mux_init_signal("vout1_b_cb_c5.gpio3_2", TI814X_PULL_DIS);
	ret = gpio_request_one(BT_PWEN_GPIO, GPIOF_OUT_INIT_LOW, "bt_pwen");
	if (ret) {
		pr_err("Error requesting bt_pwen enable gpio: %d\n", ret);
  }

	gpio_export(BT_PWEN_GPIO, false);
}

static void __init ti814x_wl12xx_init(void)
{
	ti814x_wl12xx_wlan_init();
        ti814x_wl12xx_bt_init();
}

#else /* CONFIG_WL12XX_PLATFORM_DATA */

static void __init ti814x_wl12xx_init(void) { }

#endif

static void __init dm385_gps_init(void)
{
#ifdef CONFIG_SPI_CSRGSD5XP
	if(system_rev == LONG_BEACH_WS1_BOARD ) {
		csrgsd5xp_pdata.gps_reset_active_high = 0;
	}
#endif
}

void normal_power_off(void)
{
	void __iomem *address;

	/* Clear recovery flip flop. To ensure we boot normally next boot. */
	address = ioremap(TI81XX_RTC_BASE + RTC_SCRATCH0_REG, sizeof(int));
	__raw_writel(0, address);

	machine_power_off();
}
EXPORT_SYMBOL(normal_power_off);

static void check_poweron_ok(void)
{
	/* Power off if boot reason was not usb charger connected
	   and power button press was too short */
	if ((usb_poweron_check_done) && (!usb_poweron && too_short_poweron)) {
		/* Using KERN_ERR to ensure it is shown on console even when quiet is enabled */
		printk(KERN_ERR "Too short power-on press. Powering off\n");

		normal_power_off();
	}
}

static bool get_usbpoweron(void) {
	void __iomem * usb_poweron_address;
	unsigned int value;

	/* Read upper 4 bits of RTC_SCRATCH1_REG to see if this boot is a
	 * 'USB power on' boot */

	usb_poweron_address =
		ioremap(TI81XX_RTC_BASE + RTC_SCRATCH1_REG, sizeof(int));

	value = __raw_readl(usb_poweron_address);

	iounmap(usb_poweron_address);

	value &= USB_POWER_ON_MASK;

	return value == USB_POWER_ON_MAGIC ? 1 : 0;
}

static void store_usbpoweron(unsigned int value) {
	void __iomem * usb_poweron_address;
	unsigned int regval;

	/* Shared with fastboot flipflop. See 'Note 1' and 'Note 2' */
	usb_poweron_address =
		ioremap(TI81XX_RTC_BASE + RTC_SCRATCH1_REG, sizeof(int));

	regval = __raw_readl(usb_poweron_address);
	regval &= ~(USB_POWER_ON_MASK);
	regval |= (value ? USB_POWER_ON_MAGIC : 0);
	__raw_writel(regval, usb_poweron_address);

	iounmap(usb_poweron_address);
}

void update_boot_reason(bool usb_power)
{
	char *boot_reason_start, *boot_reason_end;
	char *systemdtarget_start, *systemdtarget_end;
	bool is_usbpoweron_boot = get_usbpoweron();
	int boot_reason_len = 0;
	int systemd_unit_len = 0;

	/* Clear 'USB Power On boot' reg */
	if (is_usbpoweron_boot)
		store_usbpoweron(0);

	if (usb_power || is_usbpoweron_boot) {
		usb_poweron = true;

		/* Change boot reason to 'usb' */
		boot_reason_start = strstr(saved_command_line, "boot_reason");
		boot_reason_end = strchr(boot_reason_start, ' ');
		boot_reason_len = (int)strlen(boot_reason_start) - (boot_reason_end?(int)strlen(boot_reason_end):0);
		if(boot_reason_len < strlen(USB_POWERON_BOOTREASON))
		{
			printk("ERROR: The length of inserted boot_reason is larger than the existing boot_reason string. Old length %d, new length %d!\n",
				boot_reason_len, strlen(USB_POWERON_BOOTREASON));
			return;
		}

		strncpy(boot_reason_start, USB_POWERON_BOOTREASON, strlen(USB_POWERON_BOOTREASON));

		if(boot_reason_end)
			strlcpy(boot_reason_start + strlen(USB_POWERON_BOOTREASON), boot_reason_end,
					strlen(boot_reason_start) - strlen(USB_POWERON_BOOTREASON) + 1);
		else
			*(boot_reason_start + strlen(USB_POWERON_BOOTREASON)) = '\0';

		systemdtarget_start = strstr(saved_command_line, "systemd.unit");
		systemdtarget_end = strchr(systemdtarget_start, ' ');
		systemd_unit_len = (int)strlen(systemdtarget_start) - (systemdtarget_end?(int)strlen(systemdtarget_end):0);
		if(systemd_unit_len < strlen(SYSTEMD_USB_TARGET))
		{
			printk("ERROR: The length of inserted systemd.unit is larger than the existing systemd.unit string. Old length %d, new length %d!\n",
				systemd_unit_len, strlen(SYSTEMD_USB_TARGET));
			return;
		}

		strncpy(systemdtarget_start, SYSTEMD_USB_TARGET, strlen(SYSTEMD_USB_TARGET));

		if(systemdtarget_end)
			strlcpy(systemdtarget_start + strlen(SYSTEMD_USB_TARGET), systemdtarget_end,
					strlen(systemdtarget_start) - strlen(SYSTEMD_USB_TARGET) + 1);
		else
			*(systemdtarget_start + strlen(SYSTEMD_USB_TARGET)) = '\0';
		printk("CommandLine=%s\n", boot_reason_start);
		return;

	}

	usb_poweron_check_done = true;

	check_poweron_ok();
}
EXPORT_SYMBOL(update_boot_reason);

/*
 * GPIO Buttons
 */
#if defined(CONFIG_KEYBOARD_GPIO) || defined(CONFIG_KEYBOARD_GPIO_MODULE)

static unsigned long int pwr_gettime(void)
{
	int time;

	time = omap_dm_timer_read_counter(dm_timer);
	time = time / (20000);		/* (OSC_FREQ/CONFIG_SYS_HZ) */
	return time;
}

static void power_button_check(struct gpio_keys_button *button)
{
	if (!gpio_get_value(button->gpio) && coldboot && pwr_gettime() < ON_TIME) {
		too_short_poweron = true;
		check_poweron_ok();
	}
}

static void power_button_isr_callback(void)
{
	if (!booted) {
		if (coldboot && pwr_gettime() < ON_TIME) {
			too_short_poweron = true;
			check_poweron_ok();
		}

		booted = true;
	}
}

static struct gpio_keys_button dm385lb_buttons[] = {
	{
		.gpio = GPIO_BUT_RECORD,
		.code = KEY_RECORD,
		.desc = "Record",
		.active_low = 1,
		.wakeup = 1,
		.type = EV_KEY,
		.debounce_interval = GPIO_DEBOUNCE_TIME,
		.isr_callback = power_button_isr_callback,
		.gpio_callback = power_button_check,
	},
	{
		.gpio = GPIO_BUT_OFF,
		.code = KEY_STOP,
		.desc = "Stop",
		.active_low = 1,
		.wakeup = 1,
		.type = EV_KEY,
		.debounce_interval = GPIO_DEBOUNCE_TIME,
	},
	{
		.gpio = GPIO_BUT_UP,
		.code = KEY_UP,
		.desc = "Up",
		.active_low = 1,
		.wakeup = 1,
		.type = EV_KEY,
		.debounce_interval = GPIO_DEBOUNCE_TIME,
	},
	{
		.gpio = GPIO_BUT_DOWN,
		.code = KEY_DOWN,
		.desc = "Down",
		.active_low = 1,
		.wakeup = 1,
		.type = EV_KEY,
		.debounce_interval = GPIO_DEBOUNCE_TIME,
	},
};

static struct gpio_keys_platform_data dm385lb_button_data = {
	.buttons = dm385lb_buttons,
	.nbuttons = ARRAY_SIZE(dm385lb_buttons),
	.rep = 1, /* enable auto repeat */
};

static struct platform_device dm385lb_button_device = {
	.name = "gpio-keys",
	.id	= GPIO_BUTTONS_ID,
	.num_resources = 0,
	.dev = {
		.platform_data	= &dm385lb_button_data,
	}
};

#ifdef CONFIG_ST_KIM_DEVICE
struct ti_st_plat_data st_wilink_pdata = {
	// we cannot enable this because it's now requested by ti814x_wl12xx_bt_init().Otherwise the gpio_request
	// in st_kim.c will fail.
	// for SM WS1 bringup we have to use "echo 1 > /sys/class/gpio/gpio98/value" before testing
	//.nshutdown_gpio = BT_PWEN_GPIO,
	.dev_name = "/dev/ttyO1",
	.flow_cntrl = 1,
	.baud_rate = 3000000,
};

static struct platform_device st_kim_device = {
	.name		= "kim",
	.id		= -1,
	.dev.platform_data = &st_wilink_pdata,
};
#endif

static struct gpio_keys_button dm385lb_batlock[] = {
	{
		.gpio = GPIO_BACK_CAP_DET,
		.code = KEY_BATTERY,
		.desc = "Batterylock",
		.active_low = 0,
		.wakeup = 0,
		.type = EV_KEY,
		.debounce_interval = GPIO_DEBOUNCE_TIME,
	},
};

static struct gpio_keys_platform_data dm385lb_batlock_data = {
	.buttons = dm385lb_batlock,
	.nbuttons = ARRAY_SIZE(dm385lb_batlock),
	.rep = 1, /* enable auto repeat */
};

static struct platform_device dm385lb_batlock_device = {
	.name = "gpio-keys",
	.id	= GPIO_BATLOCK_ID,
	.num_resources = 0,
	.dev = {
		.platform_data	= &dm385lb_batlock_data,
	}
};

static void __init dm385lb_add_device_buttons(void)
{
	dm_timer = omap_dm_timer_request_specific(GP_TIMER_ID__PWR_TIMER);
	platform_device_register(&dm385lb_button_device);
	platform_device_register(&dm385lb_batlock_device);
}

#ifdef CONFIG_ST_KIM_DEVICE
static void __init dm385lb_add_st_kim_devices(void)
{
	platform_device_register(&st_kim_device);
}
#endif
#else
static void __init dm385lb_add_device_buttons(void)
{
}
#endif

#if defined(CONFIG_LEDS_PWM) || defined(CONFIG_LEDS_PWM_MODULE)
static struct led_pwm dm385lb_led_pwm[] = {
	{
		.name = "LED1",
		.pwm_id = PWM_ID__LED1,
		.active_low = 0,
		.pwm_period_ns = 1000000000 / 100, //100 Hz.
	},
	{
		.name = "Backlight",
		.pwm_id = PWM_ID__BACKLIGHT,
		.active_low = 0,
		.pwm_period_ns = 1000000000 / 100, //100 Hz.
	}
};

static struct led_pwm_platform_data dm385lb_led_pwm_pdata = {
	.num_leds = ARRAY_SIZE(dm385lb_led_pwm),
	.leds = dm385lb_led_pwm,
};

static struct platform_device dm385lb_leds_device = {
	.name = "leds_pwm",
	.id = -1,
	.dev = {
		.platform_data = &dm385lb_led_pwm_pdata,
	}
};

static void __init dm385lb_add_device_leds(void) {
	platform_device_register(&dm385lb_leds_device);
}
#else
static void __init dm385lb_add_device_leds(void) {
}
#endif

#if defined(CONFIG_INPUT_PWM_BEEPER) || defined(CONFIG_INPUT_PWM_BEEPER_MODULE)

static struct platform_device dm385lb_buzzer_device = {
	.name = "pwm-beeper",
	.id = -1,
	.dev = {
		.platform_data = (void*) PWM_ID__BUZZER,
	}
};

static void __init dm385lb_add_buzzer(void) {
	platform_device_register(&dm385lb_buzzer_device);
}

#else
static void __init dm385lb_add_buzzer(void) {
}
#endif

static void long_beach_poweroff(void)
{
	int r;
	unsigned kill_pwr_gpio;
	void __iomem *address;

	local_irq_disable();
	local_fiq_disable();

	if(system_rev == LONG_BEACH_WS1_BOARD )
		kill_pwr_gpio = KILL_POWER_GPIO_WS1;
	else
		kill_pwr_gpio = KILL_POWER_GPIO_WS2;

	r = gpio_request_one(kill_pwr_gpio, GPIOF_OUT_INIT_HIGH, "kill_pwr");
	if (r < 0) {
		printk(KERN_ERR "Unable to get kill power GPIO\n");
		return;
	}

	/* ensure next boot is not a warm boot */
	address = ioremap(TI81XX_RTC_BASE + RTC_SCRATCH2_REG, sizeof(int));
	__raw_writel(0x0, address);

	gpio_direction_output(kill_pwr_gpio, 1);

	while(1);
}

static void __init dm385_lb_pin_mux_init_ws1(void)
{
	/* Do omap_mux_init_signal */
	/* PWR_DOWN */
	omap_mux_init_signal("gpmc_cs1.gpio1_24", TI814X_PULL_DIS);

}

static void __init dm385_lb_pin_mux_init_ws2(void)
{
	/* Do omap_mux_init_signal */

	/* NAND */
	omap_mux_init_signal("vout1_g_y_yc9.gpio3_13",TI814X_PIN_OUTPUT_PULL_DIS);
	/* PWR_DOWN */
	omap_mux_init_signal("gmii0_txclk.gpio3_23", TI814X_PULL_DIS);
	/* EXT_COM */
	omap_mux_init_signal("mmc2_dat5.gpio1_21", TI814X_PULL_DIS);
	/* SW_PS */
	omap_mux_init_signal("mcasp2_axr1.gpio0_13_mux1", TI814X_PULL_DIS);
}

static void __init dm385_lb_mux_init(void)
{
	if (system_rev == LONG_BEACH_WS1_BOARD)
		dm385_lb_pin_mux_init_ws1();
	else
		dm385_lb_pin_mux_init_ws2();
}

static void __init dm385_longbeach_display_fixup(void)
{
#ifdef CONFIG_HAVE_PWM
	if(system_rev == LONG_BEACH_WS1_BOARD) {
		dm385_ipnc_pwm_cfg[PWM_ID__DISP_EXTCOM].timer_id = GP_TIMER_ID__WS1_DISP_EXTCOM;
		dm385_ipnc_pwm_cfg[PWM_ID__BUZZER].timer_id = GP_TIMER_ID__WS1_BUZZER;
	}
#endif
}

#ifdef CONFIG_TOMTOM_FLIPFLOP_HW
static struct flipflop_pdata flipflop_recovery_platform_data = {
	.boot_clk_gpio  = FLIPFLOP_BOOT_CLK_GPIO,
	.boot_q_gpio    = FLIPFLOP_BOOT_Q_GPIO,
};
#else

/*
 * The recovery flip flop is now a tri-state flipflop.
 * 0 = do not boot into recovery
 * 1 = recovery (0x73647570)
 * 2 = update (~0x73647570)
 */
static struct flipflop_pdata flipflop_recovery_platform_data = {
	.scratch_reg_number = FLIPFLOP_RECOVERY_BOOT_SCRATCH_REG_NUM,
	.magic1 = FLIPFLOP_RECOVERY_BOOT_MAGIC,
	.magic2 = FLIPFLOP_RECOVERY_BOOT_UPDATE_MAGIC,
};
#endif

static struct platform_device dm385_flipflop_recovery = {
	.name   = "flipflop",
	.id     = FLIPFLOP_RECOVERY_BOOT_SCRATCH_REG_NUM,
	.dev = {
		.platform_data = &flipflop_recovery_platform_data,
	}
};

#ifndef CONFIG_TOMTOM_FLIPFLOP_HW

static struct flipflop_pdata flipflop_fastboot_platform_data = {
	.scratch_reg_number = FLIPFLOP_FASTBOOT_SCRATCH_REG_NUM,
	.magic1 = FLIPFLOP_FASTBOOT_MAGIC
};

static struct platform_device dm385_flipflop_fastboot = {
	.name   = "flipflop",
	.id     = FLIPFLOP_FASTBOOT_SCRATCH_REG_NUM, /* Share with reboot counter. See 'Note 1'. */
	.dev = {
		.platform_data = &flipflop_fastboot_platform_data,
	}
};

#endif

static struct platform_device *dm385_flipflop_devices[] __initdata = {
	&dm385_flipflop_recovery,
#ifndef CONFIG_TOMTOM_FLIPFLOP_HW
	&dm385_flipflop_fastboot
#endif
 };

static void __init dm385_flipflop_init(void)
{
	platform_add_devices(dm385_flipflop_devices, ARRAY_SIZE(dm385_flipflop_devices));
}

#ifdef CONFIG_TOMTOM_FDT
extern struct factorydata_buffer_info_t fdt_buffer_info;

static struct resource tomtom_fdtexport_resource[] ={
	{
		.name = "dtb-buffer",
		.flags = IORESOURCE_MEM,
		.start = 0,
		.end = 0,
	},
};

struct platform_device tomtom_device_fdtexport = {
	.name		= "fdtexport",
	.id		= -1,
	.num_resources	= ARRAY_SIZE(tomtom_fdtexport_resource),
	.resource	= tomtom_fdtexport_resource,
};

static int __init tomtom_fdtexport_init(void)
{
	tomtom_device_fdtexport.resource[0].start = fdt_buffer_info.address;
	tomtom_device_fdtexport.resource[0].end = fdt_buffer_info.address + fdt_buffer_info.size;

	platform_device_register(&tomtom_device_fdtexport);
	return 0;
}
#endif /* CONFIG_TOMTOM_FDT */

static int __init dm385lb_keypad_init_gpio(void)
{
	int error;

	error = gpio_request_one(GPIO_DEVOSC_WAKE, GPIOF_IN, "devosc_wake");
	if (error) {
		pr_err("Error requesting DEVOSC_WAKE pin GPIO: %d\n", error);
	}
	return 0;
}

static int __init save_boot_reason(char *p)
{
	if (!p)
		return 0;

	if (strcmp(p, "cold") == 0)
		coldboot = 1;
	return 0;
}
early_param("boot_reason", save_boot_reason);

static unsigned int load_reboot_counter(void) {
	void __iomem * reboot_counter_address;
	unsigned int value;

	/* Shared with fastboot flipflop. See 'Note 1'. */
	reboot_counter_address = ioremap(TI81XX_RTC_BASE + RTC_SCRATCH1_REG, sizeof(int));

	value = __raw_readl(reboot_counter_address);

	if ((value & (~REBOOT_COUNTER_MASK)) == REBOOT_COUNTER_MAGIC)
		value &= REBOOT_COUNTER_MASK;
	else
		value = 0;

	iounmap(reboot_counter_address);

	return value;
}

static void store_reboot_counter(unsigned int value) {
	void __iomem * reboot_counter_address;

	/* Shared with fastboot flipflop. See 'Note 1'. */
	reboot_counter_address = ioremap(TI81XX_RTC_BASE + RTC_SCRATCH1_REG, sizeof(int));

	value &= REBOOT_COUNTER_MASK;
	value |= REBOOT_COUNTER_MAGIC;

	__raw_writel(value, reboot_counter_address);

	iounmap(reboot_counter_address);
}

#ifdef CONFIG_TOMTOM_REBOOT_COUNTER

static struct reboot_counter_pdata reboot_counter_platform_data = {
	.load_reboot_counter = load_reboot_counter,
	.store_reboot_counter = store_reboot_counter
};

static struct platform_device dm385_reboot_counter = {
	.name   = "reboot-counter",
	.id     = -1,
	.dev = {
		.platform_data = &reboot_counter_platform_data,
	}
};

static int __init tomtom_reboot_counter_init(void)
{
	printk("tomtom_reboot_counter_init\n");
	platform_device_register(&dm385_reboot_counter);
	return 0;
}

#else

static int __init tomtom_reboot_counter_init(void) {}

#endif

static void long_beach_restart(char mode, const char *cmd)
{
	void __iomem * ff_recovery_address;
	void __iomem * ff_fastboot_address;
	u32 ff_recovery_value = 0;
	u32 ff_fastboot_value = 0;
	u32 reboot_counter = 0;

	/* Map flip flop addresses */
	ff_recovery_address = ioremap(TI81XX_RTC_BASE + RTC_SCRATCH0_REG, sizeof(int));
	ff_fastboot_address = ioremap(TI81XX_RTC_BASE + RTC_SCRATCH1_REG, sizeof(int));

	/*
	 * Determine values of the flip flops based on given argument/cmd
	 * Non or unknown commands result in normal reboot (default)
	 */
	if (cmd != NULL) {
		if (strcmp(cmd, "usbpoweron") == 0) {
			ff_recovery_value = 0;
			ff_fastboot_value = USB_POWER_ON_MAGIC;
		} else if (strcmp(cmd, "update") == 0) {
			ff_recovery_value = FLIPFLOP_RECOVERY_BOOT_UPDATE_MAGIC;
			ff_fastboot_value = 0;
		} else if (strcmp(cmd, "recovery") == 0) {
			ff_recovery_value = FLIPFLOP_RECOVERY_BOOT_MAGIC;
			ff_fastboot_value = 0;
		} else if (strcmp(cmd, "fastboot") == 0) {
			ff_recovery_value = 0;
			ff_fastboot_value = FLIPFLOP_FASTBOOT_MAGIC;
		} else if (strcmp(cmd, "count") == 0) {
			/* Increase reset counter. When it reach a limit, reboot into recovery */
			reboot_counter = load_reboot_counter();
			reboot_counter++;
			if(reboot_counter >= REBOOT_COUNTER_LIMIT) {
				reboot_counter = 0;
				ff_recovery_value = FLIPFLOP_RECOVERY_BOOT_MAGIC;
				ff_fastboot_value = 0;
			}
		}
	}

	/* set recovery flipflop */
	__raw_writel(ff_recovery_value, ff_recovery_address);

	/* set fastboot flipflop or reboot counter. See 'Note 1'. */
	if (ff_fastboot_value)
		__raw_writel(ff_fastboot_value, ff_fastboot_address);
	else
		store_reboot_counter(reboot_counter);

	/* Proceed with generic restart code */
	arm_machine_restart(mode, cmd);
}

static struct omap_dm_timer *persistent_timer;

#define PERSISTENT_CLOCK_SHIFT 10

/**
 * read_persistent_clock -  Return time from a persistent clock.
 *
 * Reads the time from a source which isn't disabled during PM, which
 * is TIMER 8 on this board.  Convert the cycles elapsed since last read
 * into nsecs and adds to a monotonically increasing timespec.
 */
static struct timespec persistent_ts;
static cycles_t cycles, last_cycles;
void read_persistent_clock(struct timespec *ts)
{
	unsigned long long nsecs;
	unsigned int mult, shift;
	cycles_t delta, mask;
	struct timespec *tsp = &persistent_ts;

	mask = CLOCKSOURCE_MASK(32);

	last_cycles = cycles;
	if (persistent_timer)
		cycles = omap_dm_timer_read_counter(persistent_timer);
	else
		cycles = 0;
	delta = (cycles - last_cycles) & mask;

	/* Convert cycles to nsec */
	shift = PERSISTENT_CLOCK_SHIFT;
	mult =  clocksource_hz2mult(32768, shift);
	nsecs = clocksource_cyc2ns(delta, mult, shift);

	timespec_add_ns(tsp, nsecs);
	*ts = *tsp;
}

static void long_beach_persistent_timer_init(void)
{
	struct timespec now;

	/* initialize TIMER 8 as persistent timer */
	persistent_timer =
		omap_dm_timer_request_specific(GP_TIMER_ID__PERSISTENT);

	/* set clock source for persistent timer to sysclk18_ck
	 * Note that at reset the prescalar for the timer is disabled
	 * We leave it disabled so that prescalar clock divisor is 1
	 * and the timer rate = input clock frequency
	 */
	omap_dm_timer_set_source(persistent_timer, OMAP_TIMER_SRC_32_KHZ);

	/* start timer with autoreload = 1, reload value = 0 */
	omap_dm_timer_set_load_start(persistent_timer, 1, 0);

	/* read persistent timer to initialize persistent_ts structure */
	read_persistent_clock(&now);
}

static void dm385lb_sd_card_clock_init(void)
{
	struct omap2_hsmmc_info* c;

	/* The MMC clock can be forced by the kernel config */
#ifdef CONFIG_DM385_LB_SD_CLOCK
	for (c = mmc; c->mmc; c++) {
		if (c->mmc == MMC_CONTROLLER_SD_CARD) {
			c->max_freq = CONFIG_DM385_LB_SD_CLOCK;
			break;
		}
	}
#else
	/* On PR2 maximum SD card MMC clock speed is 38.4 MHz as higher results
	 * in CMD setup time to be too short due to added notch filter
	 * Note: Maximum of HW might be a little bit higher,
	 * but current fck is running at 192MHz. So next
	 * SD clock is 48MHz which is too high. From MFB the notch
	 * filter has been changed */
	if (system_rev < LONG_BEACH_MFB1_BOARD) {
		for (c = mmc; c->mmc; c++) {
			if (c->mmc == MMC_CONTROLLER_SD_CARD) {
				c->max_freq = 38400000;
				break;
			}
		}
	}
#endif
}

static struct platform_device dm385_mems_mic_codec_device = {
       .name   = "mems-dummy-codec",
       .id     = -1,
};

static struct platform_device *dm385_mems_devices[] __initdata = {
       &dm385_mems_mic_codec_device,
};

static void __init dm385_evm_init(void)
{
	int ret, bw; /* bus-width */
	u32 *control_status;

	ti814x_mux_init(board_mux);
	dm385_lb_mux_init();
	omap_serial_init();

	/* for OLED */
	ti814x_evm_i2c2_init();
	omap_mux_init_signal("vin0_d20.gpio0_14_mux0", TI814X_PULL_DIS);

	ti814x_evm_i2c3_init();
	dm385_longbeach_display_fixup();
	dm385_spi_init();
	ti81xx_register_mcasp(0, &dm385_evm_snd_data);

	omap_mux_init_signal("mmc2_dat0", TI814X_PULL_UP);
	omap_mux_init_signal("mmc2_dat1", TI814X_PULL_UP);
	omap_mux_init_signal("mmc2_dat2", TI814X_PULL_UP);
	omap_mux_init_signal("mmc2_dat3", TI814X_PULL_UP);
	omap_mux_init_signal("mmc2_clk", TI814X_PULL_UP);
	omap_mux_init_signal("gpmc_cs4.mmc2_cmd", TI814X_PULL_UP);

	dm385lb_sd_card_clock_init();
	omap2_hsmmc_init(mmc);

	pm_power_off = long_beach_poweroff;
	arm_pm_restart = long_beach_restart;

	long_beach_persistent_timer_init();

	/* nand initialisation */
	if(system_rev != LONG_BEACH_WS1_BOARD)
	{
		/* NAND write protect off */
		ret = gpio_request_one(NAND_WP_GPIO, GPIOF_OUT_INIT_HIGH, "nand_wp");
		if (ret) {
			pr_err("Error requesting nand_wp enable gpio: %d\n", ret);
			//return;
		}

		gpio_export(NAND_WP_GPIO, false);
	}

	control_status = TI81XX_CTRL_REGADDR(0x40);
	if (*control_status & (1 << 16)) {
		bw = NAND_OMAP_BUS_16; /*16-bit nand if BTMODE BW pin on board is ON*/
		printk("board_init: 16-bit nand selected\n");
	} else {
		bw = NAND_OMAP_BUS_8; /*8-bit nand if BTMODE BW pin on board is OFF*/
		printk("board_init: 8-bit nand selected\n");
	}
	board_nand_init(dm385lb_nand_partitions,
		ARRAY_SIZE(dm385lb_nand_partitions), 0, bw);

	/* initialize usb */
#ifdef CONFIG_DM385_LB_OPTIMIZED
	if(system_rev == LONG_BEACH_WS2_BOARD) {
		/*
		 * Disabling USB support for release builds on WS2 boards as workaround
		 * for battery charge issue on MCU of Batstick (LBP-210).
		 */
		musb_board_data.mode = 0;
	}
#endif
	usb_musb_init(&musb_board_data);
	dm385_flipflop_init();
	platform_add_devices(dm385_mems_devices, ARRAY_SIZE(dm385_mems_devices));
	tomtom_reboot_counter_init();
        dm385_gps_init();
	ti814x_wl12xx_init();
	dm385lb_add_device_buttons();
	dm385lb_add_device_leds();
	dm385lb_add_buzzer();
	regulator_use_dummy_regulator();
#ifdef CONFIG_HAVE_PWM
	omap_register_pwm_config(dm385_ipnc_pwm_cfg, ARRAY_SIZE(dm385_ipnc_pwm_cfg));
#endif

#ifdef CONFIG_TOMTOM_FDT
	tomtom_fdtexport_init();
#endif /* CONFIG_TOMTOM_FDT */

	/* Init TCA8418 keypad */
	dm385lb_keypad_init_gpio();

#ifdef CONFIG_ST_KIM_DEVICE
	dm385lb_add_st_kim_devices();
#endif
}

static void __init dm385_evm_map_io(void)
{
	omap2_set_globals_ti816x();
	ti81xx_map_common_io();
}

MACHINE_START(DM385LB, "dm385longbeach")
	/* Maintainer: TomTom */
	.boot_params	= 0x80000100,
	.map_io		= dm385_evm_map_io,
	.reserve         = ti81xx_reserve,
	.init_irq	= dm385_evm_init_irq,
	.init_machine	= dm385_evm_init,
	.timer		= &omap_timer,
MACHINE_END
