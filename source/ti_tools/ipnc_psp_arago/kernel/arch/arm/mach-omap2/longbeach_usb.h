#ifndef __ARCH_LONGBEACH_USB_H
#define __ARCH_LONGBEACH_USB_H

/*
 * Supply external events to USB host detection state machine
 * See longbeach_usb.c for more info
 */

#ifdef CONFIG_MACH_DM385LB

/* Notify USB host detection state machine that host was detected */
extern void lbusb_notify_connect(void);

/* Notify USB host detection state machine that vbus state changed */
extern void lbusb_notify_vbus(int state /* { VBUS_PRESENT, VBUS_REMOVED } */);

/*
 * Notify USB host detection state machine that USB config changed, e.g. new
 * gadget driver was added
 */
extern void lbusb_notify_config_change(void);

/*
 * This function should be implemented by the board file and should return
 * - 0 when no DCP changer is connected,
 * - 1 when a DCP changer is connected
 * - or a negative error code.
 */
extern int lbusb_is_dcp_charger_connected(void);

#else

static void lbusb_notify_connect(void) {}
static void lbusb_notify_vbus(int state) {}
static void lbusb_notify_config_change(void) {}

#endif

#endif /* __ARCH_LONGBEACH_USB_H */
