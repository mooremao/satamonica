/*
 * TI81XX Power Management
 *
 * This module implements TI81XX specific Power management Routines
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include <linux/opp.h>

#include <plat/cpu.h>
#include <plat/omap_device.h>

#include "omap_opp_data.h"

static struct omap_opp_def __initdata ti814x_opp_def_list[] = {
       /* MPU OPPs
        * Supported frequencies 100 MHz to 1000 MHz
	* Voltage in Single OPP Mode: 1.32V at all frequencies
	* Voltages in Cross OPP Mode:
        * 100 MHz @ 1.1V
        * 101 MHz to 720 MHz @ 1.2V
        * 721 MHz to 1000 MHz @ 1.32V
        */

	/* Disabling OPP50/120 and keep system default/reset voltage as
	 * enough testing is not done
	 * and may cause system instability on some boards
	 */
	OPP_INITIALIZER("mpu", true, 100000000, 1100000),
	/* MPU OPP2 - OPP 100 */
	OPP_INITIALIZER("mpu", true, 720000000, 1200000),
	/* MPU OPP3 - OPP 120 */
	OPP_INITIALIZER("mpu", true, 1000000000, 1320000),

       /* IVA OPPs
        * Supported frequencies 266 MHz to 410 MHz
	* Voltage in Single OPP Mode: 1.32V at all frequencies
	* Voltages in Cross OPP Mode:
        * 266 MHz @ 1.2V
        * 267 MHz to 410 MHz @ 1.32V
        */
	OPP_INITIALIZER("iva", true, 266000000, 1200000),
	OPP_INITIALIZER("iva", true, 410000000, 1320000),

	/* ISS OPPs
         * Supported frequencies 100 MHz to 560 MHz
	 */
	OPP_INITIALIZER("iss", true, 100000000, -1),
	OPP_INITIALIZER("iss", true, 560000000, -1),

	/* L3 OPPs
         * Supported frequencies 50 MHz to 240 MHz
	 */
	OPP_INITIALIZER("l3_slow", true, 50000000, -1),
	OPP_INITIALIZER("l3_slow", true, 240000000, -1),
};

/**
 * ti814x_opp_init() - initialize ti814x opp table
 */
static int __init ti814x_opp_init(void)
{
	int r = -ENODEV;

	if (!cpu_is_ti814x() || !(omap_rev() > TI8148_REV_ES1_0))
		return r;

	r = omap_init_opp_table(ti814x_opp_def_list,
			ARRAY_SIZE(ti814x_opp_def_list));

	return r;
}
device_initcall(ti814x_opp_init);
