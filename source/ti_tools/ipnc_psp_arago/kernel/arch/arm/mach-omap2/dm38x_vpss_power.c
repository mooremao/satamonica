/*
 * Code for DM38x VPSS-M3 Power Management
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/sysfs.h>

#ifdef CONFIG_TI81XX_VPSS_SYSNLINK_NOTIFY
#include <ti/syslink/Std.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/Notify.h>
#else
#include <syslink/multiproc.h>
#include <syslink/notify.h>
#endif

/* Define the information used by VPSS-M3 */
#define VPSS_SUSPEND_RESUME_EVENTID	31
#define VPSS_SUSPEND_PAYLOAD		0x0
#define VPSS_RESUME_PAYLOAD		0x1
#define VPSS_LINEID			0

static bool suspend = false;

struct dm38x_vpss_power_dev {
	struct device *dev;
	unsigned int rmprocid;
};

static ssize_t dm38x_vpss_suspend_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return scnprintf(buf, PAGE_SIZE, "%d\n", suspend);
}

static ssize_t dm38x_vpss_suspend_store(struct device *dev,
			struct device_attribute *attr,
			const char *buf, size_t count)
{
	unsigned long value;
	int status = strict_strtoul(buf, 10, &value);

	if (0 != status)
		return status;

	suspend = value;

	return count;
}

static struct device_attribute dm38x_vpss_attr =
	__ATTR(suspend, 0664, dm38x_vpss_suspend_show, dm38x_vpss_suspend_store);

static int dm38x_vpss_power_probe(struct platform_device *pdev)
{
	struct dm38x_vpss_power_dev *dev;

	dev = kzalloc(sizeof(struct dm38x_vpss_power_dev), GFP_KERNEL);
	if (!dev)
		return -ENOMEM;

	dev->dev = get_device(&pdev->dev);
	platform_set_drvdata(pdev, dev);
	if (device_create_file(&pdev->dev, &dm38x_vpss_attr) < 0) {
		dev_err(&pdev->dev, "%s:Unable to create interface\n", __func__);
		platform_set_drvdata(pdev, NULL);
		kfree(dev);
		return -1;
	}

	return 0;
}

static int dm38x_vpss_power_remove(struct platform_device *pdev)
{
	struct dm38x_vpss_power_dev *dev = platform_get_drvdata(pdev);

	device_remove_file(&pdev->dev, &dm38x_vpss_attr);
	platform_set_drvdata(pdev, NULL);
	kfree(dev);

	return 0;
}

#ifdef CONFIG_SUSPEND
static int dm38x_vpss_power_prepare(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dm38x_vpss_power_dev *vpss_dev = platform_get_drvdata(pdev);
	int ret;

	if (!suspend)
		return 0;

	/* Do pre suspend operations */

#ifdef CONFIG_TI81XX_VPSS_SYSNLINK_NOTIFY
	vpss_dev->rmprocid = MultiProc_getId("VPSS-M3");
#else
	vpss_dev->rmprocid = multiproc_get_id("VPSS-M3");
#endif
	if (MULTIPROC_INVALIDID == vpss_dev->rmprocid) {
		printk(KERN_ERR "failed to get the M3VPSS processor ID.\n");
		return -EINVAL;
	}


	/* If this event ID is not available then fail */
#ifdef CONFIG_TI81XX_VPSS_SYSNLINK_NOTIFY
	ret = Notify_eventAvailable(vpss_dev->rmprocid,
				    VPSS_LINEID,
				    VPSS_SUSPEND_RESUME_EVENTID);
#else
	ret = notify_event_available(vpss_dev->rmprocid,
				     VPSS_LINEID,
				     VPSS_SUSPEND_RESUME_EVENTID);
#endif
	if (!ret) {
		printk(KERN_ERR "vpss_suspend notify event unavailable\n");
		return -EINVAL;
	}

	/* Send notify to remote processor to suspend */
#ifdef CONFIG_TI81XX_VPSS_SYSNLINK_NOTIFY
	ret = Notify_sendEvent(vpss_dev->rmprocid,
			VPSS_LINEID,
			VPSS_SUSPEND_RESUME_EVENTID,
			VPSS_SUSPEND_PAYLOAD,
			0);
#else
	ret = notify_send_event(vpss_dev->rmprocid,
			VPSS_LINEID,
			VPSS_SUSPEND_RESUME_EVENTID,
			VPSS_SUSPEND_PAYLOAD,
			0);
#endif

	if (ret < 0) {
		printk(KERN_ERR "vpss_suspend notify send failed %d\n", ret);
		return ret;
	}

	return 0;
}

static void dm38x_vpss_power_complete(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dm38x_vpss_power_dev *vpss_dev = platform_get_drvdata(pdev);
	int ret;

	if (!suspend)
		return;

	/* Do post resume operations */

	/* Send notify to remote processor to resume */

	udelay(1000);

#ifdef CONFIG_TI81XX_VPSS_SYSNLINK_NOTIFY
	ret = Notify_sendEvent(vpss_dev->rmprocid,
			VPSS_LINEID,
			VPSS_SUSPEND_RESUME_EVENTID,
			VPSS_RESUME_PAYLOAD,
			0);
#else
	ret = notify_send_event(vpss_dev->rmprocid,
			VPSS_LINEID,
			VPSS_SUSPEND_RESUME_EVENTID,
			VPSS_RESUME_PAYLOAD,
			0);
#endif

	if (ret < 0)
		printk(KERN_ERR "vpss_resume notify send failed %d\n", ret);

	return;
}

static int dm38x_vpss_power_suspend(struct device *dev)
{
	return 0;
}

static int dm38x_vpss_power_resume(struct device *dev)
{
	return 0;
}

static const struct dev_pm_ops dm38x_vpss_power_pm = {
	.prepare	= dm38x_vpss_power_prepare,
	.complete	= dm38x_vpss_power_complete,
	.suspend        = dm38x_vpss_power_suspend,
	.resume         = dm38x_vpss_power_resume,
};

#define dm38x_vpss_power_pm_ops (&dm38x_vpss_power_pm)
#else
#define dm38x_vpss_power_pm_ops NULL
#endif

static struct platform_driver dm38x_vpss_power_driver = {
	.probe		= dm38x_vpss_power_probe,
	.remove		= dm38x_vpss_power_remove,
	.driver		= {
		.name	= "dm38x_vpss_power",
		.owner	= THIS_MODULE,
		.pm	= dm38x_vpss_power_pm_ops,
	},
};

static int dm38x_vpss_power_init_driver(void)
{
	return platform_driver_probe(&dm38x_vpss_power_driver,
					dm38x_vpss_power_probe);
}
module_init(dm38x_vpss_power_init_driver);

static void __exit dm38x_vpss_power_exit_driver(void)
{
	platform_driver_unregister(&dm38x_vpss_power_driver);
}
module_exit(dm38x_vpss_power_exit_driver);

MODULE_AUTHOR("TomTom International B.V.");
MODULE_DESCRIPTION("TI DM38x VPSS Power Manager");
MODULE_LICENSE("GPL");
