/*
 * LBP-478 This implements a state machine to detect a USB host or charger.
 *
 * When the USB PHY on longbeach is enabled, it interferes with detection of a
 * DCP high-current charger. Therefore, a state machine has been implemented to
 * first try to detect a USB host when vbus is detected, and then if the host
 * does not enumerate the device, the PHY is disabled and the DCP can be
 * detected.
 *
 * LBP-1768: When DCP charger is detected before powering off USB PHY, leave
 * USB PHY enabled. When the USB-PHY is enabled, it counter-acts the diode
 * leakage and the detection chip can correctly identify when there is a charger
 * attached. But that is only for 1 type of charger; we still need the existing
 * case of USB_PHY disabled to detect the other type of charger.
 */

//#define DEBUG

#include <linux/jiffies.h>
#include <linux/printk.h>
#include <linux/spinlock.h>
#include <linux/timer.h>
#include <linux/tomtom/mcu.h>
#include <linux/workqueue.h>

#include "longbeach_usb.h"
#include "usb-musb.h"

#define TIMEOUT_MS 10000

#define DBG(fmt, ...) pr_debug("LB-USB: " fmt, ##__VA_ARGS__);

enum event_e {
	EV_CONFIG_CHANGE,   /* changed USB gadget config e.g. new gadget loaded */
	EV_VBUS_ON,         /* vbus detected */
	EV_VBUS_OFF,        /* vbus removed */
	EV_CONNECT,         /* USB Host connected to device controller */
	EV_TIMEOUT,	    /* Timer timed out waiting for USB Host to connect. */
	EV_DCP_DETECTED,    /* DCP charger detected */
	EV_NO_DCP_DETECTED, /* No DCP charger detected */
};

static DEFINE_SPINLOCK(state_lock);

/* Must be protected by state_lock */
static enum state_e {
	STATE_NOPOWER,       /* No power on vbus, USB PHY enabled */
	STATE_PROBING,       /* Timer armed, waiting for USB host to connect (or not), USB PHY enabled */
	STATE_HOST,          /* Host connected, USB PHY enabled */
	STATE_GET_DCP_STATE, /* Timer wired. Schedule work to retrieve whether a DCP charger is detected. */
	STATE_CHARGER1,	     /* No USB host, we have a DCP charger, USB PHY enabled */
	STATE_WAIT4CHARGER2, /* No USB host, allow for 'resistor-bridge' type chargers to be detected, USB PHY disabled */
}
state = STATE_NOPOWER;

/* Timer to wait for USB host connection, assume it's a charger if this times out */
static struct timer_list timer;
static void timeout(unsigned long data);
static DEFINE_TIMER(timer, timeout, 0, 0);

static void lbusb_work_func(struct work_struct *work);
static DECLARE_WORK(lsusb_work, lbusb_work_func);

#ifdef CONFIG_USB_MUSB_HDRC
const int never_enable_phy = 0;
#else
const int never_enable_phy = 1;
#endif

static void reset_timeout(void)
{
	DBG("Setting host detection timeout: %d ms\n", TIMEOUT_MS);
	mod_timer(&timer, jiffies + msecs_to_jiffies(TIMEOUT_MS));
}

/*
 * Change the USB host detection state and take the appropriate actions - call
 * with lock held! Can be executed from ISR context
 */
static void set_state(enum state_e new_state)
{
	assert_spin_locked(&state_lock);

	if (state != new_state) {
		/* From ... transitions */
		switch (state) {
		case STATE_PROBING:
			del_timer(&timer); /* Disable timer on all transitions
						from STATE_PROBING */
			break;

		case STATE_WAIT4CHARGER2:
			DBG("%sEnabling usb 0 PHY\n", never_enable_phy ? "Not " : "");
			MUSB_PHY_POWER(0, !never_enable_phy);
			break;

		default:
			/* Do nothing */
			break;
		}

		/* To ... transitions */
		switch (new_state) {
		case STATE_PROBING:
			reset_timeout();
			break;

		case STATE_HOST:
			DBG("Host detected\n");
			break;

		case STATE_GET_DCP_STATE:
			DBG("No host detected, retrieving DCP detected state\n");
			schedule_work(&lsusb_work);
			break;

		case STATE_CHARGER1:
			DBG("No host detected, DCP charger detected: Leaving PHY enabled\n");
			break;

		case STATE_WAIT4CHARGER2:
			DBG("No host detected or DCP charger detected disabling usb 0 PHY\n");
			MUSB_PHY_POWER(0, 0);
			break;

		case STATE_NOPOWER:
		default:
			break;
		}

		/* Update the state */
		state = new_state;
	}
}

/**
 * React to events
 *
 * Can be executed from ISR context
 */
static void post_event(enum event_e ev)
{
	unsigned long flags;
	enum state_e new_state;

	spin_lock_irqsave(&state_lock, flags);

	new_state = state;

	/* State change rules */
	switch (ev) {
	case EV_CONFIG_CHANGE:
		switch (state) {
		case STATE_PROBING:
			reset_timeout(); /* Reset the timeout if configuration
					change occurred in the middle of probing */
			break;

		case STATE_CHARGER1:
		case STATE_WAIT4CHARGER2:
			new_state = STATE_PROBING;
			break;

		default:
			/* Do nothing */
			break;
		}
		break;

	case EV_VBUS_ON:
		if (state == STATE_NOPOWER)
			new_state = STATE_PROBING;
		break;

	case EV_VBUS_OFF:
		/* All states return to STATE_NOPOWER if vbus removed */
		new_state = STATE_NOPOWER;
		break;

	case EV_TIMEOUT:
		if (state == STATE_PROBING)
			new_state = STATE_GET_DCP_STATE;
		break;

	case EV_DCP_DETECTED:
		if (state == STATE_GET_DCP_STATE)
			new_state = STATE_CHARGER1;
		break;

	case EV_NO_DCP_DETECTED:
		if (state == STATE_GET_DCP_STATE)
			new_state = STATE_WAIT4CHARGER2;
		break;

	case EV_CONNECT:
		if (state == STATE_PROBING)
			new_state = STATE_HOST;
		break;
	}

	set_state(new_state);

	spin_unlock_irqrestore(&state_lock, flags);
}

static void timeout(unsigned long data)
{
	DBG("USB host detection timed out\n");
	post_event(EV_TIMEOUT);
}

static void lbusb_work_func(struct work_struct *work)
{
	enum event_e event;

	/* We need to check for the DCP charger here. State machine uses
	 * spin_locks as it needs to be called from IRQ context.
	 * Checking for DCP can not be done from IRQ context.
	 */
	if (lbusb_is_dcp_charger_connected() == 1)
		event = EV_DCP_DETECTED;
	else
		event = EV_NO_DCP_DETECTED;
	post_event(event);
}

/* USB host connected, don't disable the PHY */
void lbusb_notify_connect(void)
{
	post_event(EV_CONNECT);
}

/* State of vbus changed */
void lbusb_notify_vbus(int state)
{
	if (state == VBUS_PRESENT) {
		DBG("vbus detected\n");
		post_event(EV_VBUS_ON);
	} else { /* VBUS_REMOVED */
		DBG("vbus removed\n");
		post_event(EV_VBUS_OFF);
	}
}

void lbusb_notify_config_change(void)
{
	DBG("USB config change\n");
	post_event(EV_CONFIG_CHANGE);
}
