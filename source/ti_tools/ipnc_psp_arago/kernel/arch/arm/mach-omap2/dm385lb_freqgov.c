/*
 * Frequency governors for DM385 LongBeach
 * Based on drivers/cpufreq/cpufreq_powersave.c
 * and drivers/cpufreq/cpufreq_conservative.c
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/cpufreq.h>
#include <linux/init.h>
#include <linux/sysdev.h>
#include <linux/cpu.h>
#include <linux/jiffies.h>
#include <linux/kernel_stat.h>
#include <linux/mutex.h>
#include <linux/hrtimer.h>
#include <linux/tick.h>
#include <linux/ktime.h>
#include <linux/sched.h>
#include <mach/dm385lb_freqgov.h>

#define governor_to_powermode(governor) (struct dm385lb_powermode*) governor

#ifdef CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG
#define kobj_to_powermode(kobj) container_of(kobj, struct dm385lb_powermode, kobj)
#endif

/* CPU frequency control functions - Based on cpufreq_conservative.c */

/*
 * dbs is used in this file as a shortform for demandbased switching
 * It helps to keep variable names smaller, simpler
 */

#define DEF_FREQUENCY_UP_THRESHOLD		(60)
#define DEF_FREQUENCY_DOWN_THRESHOLD		(40)

/*
 * The polling frequency of this governor depends on the capability of
 * the processor. Default polling frequency is 1000 times the transition
 * latency of the processor. The governor will work on any processor with
 * transition latency <= 10mS, using appropriate sampling
 * rate.
 * For CPUs with transition latency > 10mS (mostly drivers with CPUFREQ_ETERNAL)
 * this governor will not work.
 * All times here are in uS.
 */
#define MIN_SAMPLING_RATE_RATIO			(2)

/* GNSS band frequencies */
#define GNSS_BAND1_START_MHZ 1571
#define GNSS_BAND1_END_MHZ 1580

#define GNSS_BAND2_START_MHZ 1597
#define GNSS_BAND2_END_MHZ 1606

static unsigned int min_sampling_rate;

#define LATENCY_MULTIPLIER			(1000)
#define MIN_LATENCY_MULTIPLIER			(100)

static void do_dbs_timer(struct work_struct *work);

struct cpu_dbs_info_s {
	cputime64_t prev_cpu_idle;
	cputime64_t prev_cpu_wall;
	cputime64_t prev_cpu_nice;
	struct cpufreq_policy *cur_policy;
	struct delayed_work work;
	unsigned int requested_freq;
	/*
	 * mutex that serializes governor limit change with
	 * do_dbs_timer invocation. We do not want do_dbs_timer to run
	 * when user is changing the governor or limits.
	 */
	struct mutex timer_mutex;
} dm385lb_cpu_dbs_info;

static struct workqueue_struct	*cpufreq_gov_lb_wq;

static struct dbs_tuners {
	unsigned int sampling_rate;
	unsigned int up_threshold;
	unsigned int down_threshold;
	unsigned int freq_step;
} dbs_tuners_ins = {
	.up_threshold = DEF_FREQUENCY_UP_THRESHOLD,
	.down_threshold = DEF_FREQUENCY_DOWN_THRESHOLD,
	.freq_step = 5,
};

static bool is_freq_in_gnss_band(unsigned int freq)
{
	unsigned int orig_freq = freq;

	while (freq < GNSS_BAND2_END_MHZ) {
		if (((freq >= GNSS_BAND1_START_MHZ)
					&& (freq <= GNSS_BAND1_END_MHZ)) ||
				((freq >= GNSS_BAND2_START_MHZ)
					&& (freq <= GNSS_BAND2_END_MHZ)))
			return true;

		freq += orig_freq;
	}

	return false;
}

static unsigned int get_gnss_safe_freq(unsigned int freq)
{
	while (is_freq_in_gnss_band(freq))
		freq -= 1;

	/* Some times PLL fails to lock for freq values between
	 * 125 to 129 MHz. So round it up to 130 MHz
	 */
	if ((freq >= 125) && (freq <= 129))
		freq = 130;

	return freq;
}

#define MAX_LOAD_VALUES 5
static int load[MAX_LOAD_VALUES];
static int num_load_samples;

static void dbs_check_cpu(struct cpu_dbs_info_s *this_dbs_info)
{
	unsigned int cur_load = 0;
	unsigned int freq_target;
	static int load_array_index;
	int i, avg_load, sum = 0;

	struct cpufreq_policy *policy;

	cputime64_t cur_wall_time, cur_idle_time;
	unsigned int idle_time, wall_time;

	policy = this_dbs_info->cur_policy;

	/*
	 * Every sampling_rate, we check, if current cpu load is more
	 * than DEF_FREQUENCY_UP_THRESHOLD, then we try to increase frequency
	 * Every sampling_rate, we check, if current cpu load is less
	 * than DEF_FREQUENCY_DOWN_THRESHOLD, then we try to decrease frequency
	 *
	 * Any frequency increase takes it to the maximum frequency.
	 * Frequency reduction happens at minimum steps of
	 * 5% (default) of maximum frequency
	 */

	/* Get Absolute Load */

	cur_idle_time = get_cpu_idle_time_us(0, &cur_wall_time);

	wall_time = (unsigned int) cputime64_sub(cur_wall_time,
			dm385lb_cpu_dbs_info.prev_cpu_wall);
	dm385lb_cpu_dbs_info.prev_cpu_wall = cur_wall_time;

	idle_time = (unsigned int) cputime64_sub(cur_idle_time,
			dm385lb_cpu_dbs_info.prev_cpu_idle);
	dm385lb_cpu_dbs_info.prev_cpu_idle = cur_idle_time;

	if (unlikely(!wall_time || wall_time < idle_time))
		return;

	cur_load = 100 * (wall_time - idle_time) / wall_time;

	load[load_array_index] = cur_load;
	load_array_index = (load_array_index + 1) % MAX_LOAD_VALUES;
	if (num_load_samples < MAX_LOAD_VALUES)
		num_load_samples++;

	for (i = 0; i < num_load_samples; i++)
		sum += load[i];

	avg_load = sum / num_load_samples;

	/* Check for frequency increase */
	if (cur_load > dbs_tuners_ins.up_threshold) {

		/* if we are already at full speed then break out early */
		if (this_dbs_info->requested_freq == policy->max)
			return;

		this_dbs_info->requested_freq = policy->max;

		this_dbs_info->requested_freq =
			(get_gnss_safe_freq(this_dbs_info->requested_freq / 1000))
			* 1000;

		__cpufreq_driver_target(policy, this_dbs_info->requested_freq,
			CPUFREQ_RELATION_H);
		return;
	}

	/*
	 * The optimal frequency is the frequency that is the lowest that
	 * can support the current CPU usage without triggering the up
	 * policy. To be safe, we focus 10 points under the threshold.
	 */
	if (avg_load < (dbs_tuners_ins.down_threshold - 10) && (policy->cur > policy->min)) {
		freq_target = (dbs_tuners_ins.freq_step * 1000000) / 100;

		this_dbs_info->requested_freq -= freq_target;
		if (this_dbs_info->requested_freq < policy->min)
			this_dbs_info->requested_freq = policy->min;

		this_dbs_info->requested_freq =
			(get_gnss_safe_freq(this_dbs_info->requested_freq / 1000))
			* 1000;

		__cpufreq_driver_target(policy, this_dbs_info->requested_freq,
				CPUFREQ_RELATION_H);
		return;
	}
}

static void do_dbs_timer(struct work_struct *work)
{
	struct cpu_dbs_info_s *dbs_info =
		container_of(work, struct cpu_dbs_info_s, work.work);

	/* We want all CPUs to do sampling nearly on same jiffy */
	int delay = usecs_to_jiffies(dbs_tuners_ins.sampling_rate);

	delay -= jiffies % delay;

	mutex_lock(&dbs_info->timer_mutex);

	dbs_check_cpu(dbs_info);

	queue_delayed_work(cpufreq_gov_lb_wq, &dbs_info->work, delay);
	mutex_unlock(&dbs_info->timer_mutex);
}

static inline void dbs_timer_init(struct cpu_dbs_info_s *dbs_info)
{
	/* We want all CPUs to do sampling nearly on same jiffy */
	int delay = usecs_to_jiffies(dbs_tuners_ins.sampling_rate);
	delay -= jiffies % delay;

	INIT_DELAYED_WORK_DEFERRABLE(&dbs_info->work, do_dbs_timer);
	queue_delayed_work(cpufreq_gov_lb_wq, &dbs_info->work, delay);
}

static inline void dbs_timer_exit(struct cpu_dbs_info_s *dbs_info)
{
	cancel_delayed_work_sync(&dbs_info->work);
}

/* sysfs entries */
/* Governor Tunables */
#define show_one(file_name, object)					\
static ssize_t show_##file_name						\
(struct cpufreq_policy *unused, char *buf)				\
{									\
	return sprintf(buf, "%u\n", dbs_tuners_ins.object);		\
}
show_one(sampling_rate, sampling_rate);
show_one(up_threshold, up_threshold);
show_one(down_threshold, down_threshold);
show_one(freq_step, freq_step);

static ssize_t store_sampling_rate(struct cpufreq_policy *unused,
		const char *buf, size_t count)
{
	unsigned int input;
	int ret;
	ret = sscanf(buf, "%u", &input);

	if (ret != 1)
		return -EINVAL;

	dbs_tuners_ins.sampling_rate = max(input, min_sampling_rate);

	return count;
}

static ssize_t store_up_threshold(struct cpufreq_policy *unused,
		const char *buf, size_t count)
{
	unsigned int input;
	int ret;
	ret = sscanf(buf, "%u", &input);

	if (ret != 1 || input > 100 ||
			input <= dbs_tuners_ins.down_threshold) {
		return -EINVAL;
	}

	dbs_tuners_ins.up_threshold = input;

	return count;
}

static ssize_t store_down_threshold(struct cpufreq_policy *unused,
		const char *buf, size_t count)
{
	unsigned int input;
	int ret;
	ret = sscanf(buf, "%u", &input);

	/* cannot be lower than 11 otherwise freq will not fall */
	if (ret != 1 || input < 11 || input > 100 ||
			input >= dbs_tuners_ins.up_threshold) {
		return -EINVAL;
	}

	dbs_tuners_ins.down_threshold = input;

	return count;
}

static ssize_t store_freq_step(struct cpufreq_policy *unused,
		const char *buf, size_t count)
{
	unsigned int input;
	int ret;
	ret = sscanf(buf, "%u", &input);

	if (ret != 1)
		return -EINVAL;

	if (input > 100)
		input = 100;

	/* no need to test here if freq_step is zero as the user might actually
	 * want this, they would be crazy though :) */
	dbs_tuners_ins.freq_step = input;

	return count;
}

cpufreq_freq_attr_rw(sampling_rate);
cpufreq_freq_attr_rw(up_threshold);
cpufreq_freq_attr_rw(down_threshold);
cpufreq_freq_attr_rw(freq_step);

static struct attribute *dbs_attributes[] = {
	&sampling_rate.attr,
	&up_threshold.attr,
	&down_threshold.attr,
	&freq_step.attr,
	NULL
};

static struct attribute_group dbs_attr_group = {
	.attrs = dbs_attributes,
	.name = "dm385lb",
};

/* DM385 LB Frequency Governor */
static int cpufreq_governor_lb(struct cpufreq_policy *policy,
					unsigned int event)
{
	struct dm385lb_powermode* powermode = governor_to_powermode(policy->governor);

	unsigned int cpu = policy->cpu;
	struct cpu_dbs_info_s *this_dbs_info;
	unsigned int latency;

	this_dbs_info = &dm385lb_cpu_dbs_info;

	switch (event) {
	case CPUFREQ_GOV_START:
		if ((!cpu_online(cpu)) || (!policy->cur))
			return -EINVAL;

		if (sysfs_create_group(&policy->kobj, &dbs_attr_group) < 0)
			pr_err("%s : Unable to create governor sysfs entries\n", __func__);

		policy->min = powermode->cpu_freq_min * 1000;
		policy->max = powermode->cpu_freq_max * 1000;

		dm385lb_cpu_dbs_info.cur_policy = policy;

		dm385lb_cpu_dbs_info.prev_cpu_idle = get_cpu_idle_time_us(0,
						&dm385lb_cpu_dbs_info.prev_cpu_wall);

		this_dbs_info->requested_freq = policy->cur;

		mutex_init(&this_dbs_info->timer_mutex);

		/*
		 * Start the timerschedule work, when this governor
		 * is used for first time
		 */
		/* policy latency is in nS. Convert it to uS first */
		latency = policy->cpuinfo.transition_latency / 1000;
		if (latency == 0)
			latency = 1;

		/*
		 * conservative does not implement micro like ondemand
		 * governor, thus we are bound to jiffes/HZ
		 */
		min_sampling_rate =
			MIN_SAMPLING_RATE_RATIO * jiffies_to_usecs(10);
		/* Bring kernel and HW constraints together */
		min_sampling_rate = max(min_sampling_rate,
				MIN_LATENCY_MULTIPLIER * latency);
		dbs_tuners_ins.sampling_rate =
			max(min_sampling_rate,
					latency * LATENCY_MULTIPLIER);

		dbs_timer_init(this_dbs_info);

		/* Set L3, ISS and IVA frequency */

		l3_setfreq(powermode->l3_freq * 1000);
		iss_setfreq(powermode->iss_freq * 1000);
		iva_setfreq(powermode->iva_freq * 1000);

		break;

	case CPUFREQ_GOV_STOP:
		sysfs_remove_group(&policy->kobj, &dbs_attr_group);
		dbs_timer_exit(this_dbs_info);

		mutex_destroy(&this_dbs_info->timer_mutex);

		break;

	case CPUFREQ_GOV_LIMITS:

		/* We do not allow policy min and max values to be different
		   from that for the corresponding powermode */

		policy->min = powermode->cpu_freq_min * 1000;
		policy->max = powermode->cpu_freq_max * 1000;

		mutex_lock(&this_dbs_info->timer_mutex);
		if (policy->max < this_dbs_info->cur_policy->cur)
			__cpufreq_driver_target(
					this_dbs_info->cur_policy,
					policy->max, CPUFREQ_RELATION_H);
		else if (policy->min > this_dbs_info->cur_policy->cur)
			__cpufreq_driver_target(
					this_dbs_info->cur_policy,
					policy->min, CPUFREQ_RELATION_L);
		mutex_unlock(&this_dbs_info->timer_mutex);

		l3_setfreq(powermode->l3_freq * 1000);
		iss_setfreq(powermode->iss_freq * 1000);
		iva_setfreq(powermode->iva_freq * 1000);

		break;

	default:
		break;
	}
	return 0;
}

/* DM385 LB Power Modes */

#ifdef CONFIG_DM385_LB_POWER_MODES

/* To add a new power mode add corresponding entry to this array */
/* TODO: The frequencies corresponding to each mode are not finalized */
static struct dm385lb_powermode powermodes[] = {
	/* Passive Power mode */
	{
		.governor = {
			.name = "lb_passive",
			.governor = cpufreq_governor_lb,
			.owner	= THIS_MODULE,
		},
		.cpu_freq_min = 110,
		.cpu_freq_max = 110,
		.cpu_freq = 110,
		.iss_freq = 390, /* Should be lower. Depends upon LBP-1361 */
		.l3_freq = 75,
		.iva_freq = 410, /* Should be lower. Depends upon LBP-1361 */
	},
	/* Active Power Mode */
	{
		.governor = {
			.name = "lb_active",
			.governor = cpufreq_governor_lb,
			.owner	= THIS_MODULE,
		},
		.cpu_freq_min = 110,
		.cpu_freq_max = 390,
		.cpu_freq = 390,
		.iss_freq = 390, /* Should be lower. Depends upon LBP-1361 */
		.l3_freq = 125,
		.iva_freq = 410, /* Should be lower. Depends upon LBP-1361 */
	},
	/* Viewfinder Mode */
	{
		.governor = {
			.name = "lb_high1",
			.governor = cpufreq_governor_lb,
			.owner	= THIS_MODULE,
		},
		.cpu_freq_min = 110,
		.cpu_freq_max = 390,
		.cpu_freq = 390,
		.iss_freq = 390,
		.l3_freq = 125,
		.iva_freq = 410,
	},

	/* Low Video Rate */
	{
		.governor = {
			.name = "lb_high2",
			.governor = cpufreq_governor_lb,
			.owner	= THIS_MODULE,
		},
		.cpu_freq_min = 110,
		.cpu_freq_max = 429,
		.cpu_freq = 429,
		.iss_freq = 390,
		.l3_freq = 125,
		.iva_freq = 410,
	},

	/* Full Video Mode */
	{
		.governor = {
			.name = "lb_high3",
			.governor = cpufreq_governor_lb,
			.owner	= THIS_MODULE,
		},
		.cpu_freq_min = 110,
		.cpu_freq_max = 429,
		.cpu_freq = 429,
		.iss_freq = 390,
		.l3_freq = 125,
		.iva_freq = 410,
	}
};

#else

static struct dm385lb_powermode powermodes[] = {
	/* Single power mode */
	{
		.governor = {
			.name = "lb_governor",
			.governor = cpufreq_governor_lb,
			.owner	= THIS_MODULE,
		},
		.cpu_freq_min = 110,
		.cpu_freq_max = 429,
		.cpu_freq = 429,
		.iss_freq = 390, /* Should be lower. Depends upon LBP-1361 */
		.l3_freq = 125,
		.iva_freq = 410, /* Should be lower. Depends upon LBP-1361 */
	}
};

#endif

#ifdef CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG

static ssize_t cpu_freq_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	struct dm385lb_powermode* powermode = kobj_to_powermode(kobj);
	return scnprintf(buf, PAGE_SIZE, "%u\n", powermode->cpu_freq);
}

static ssize_t cpu_freq_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t n)
{
	unsigned long value;
	struct dm385lb_powermode* powermode = kobj_to_powermode(kobj);

	if (strict_strtoul(buf, 0, &value))
		return -EINVAL;

	powermode->cpu_freq = value;

	return n;
}

static ssize_t iss_freq_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	struct dm385lb_powermode* powermode = kobj_to_powermode(kobj);
	return scnprintf(buf, PAGE_SIZE, "%u\n", powermode->iss_freq);
}

#ifdef CONFIG_DM385_LB_POWER_MODES
static ssize_t iss_freq_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t n)
{
	unsigned long value;
	struct dm385lb_powermode* powermode = kobj_to_powermode(kobj);

	if (strict_strtoul(buf, 0, &value))
		return -EINVAL;

	powermode->iss_freq = value;

	return n;
}
#endif

static ssize_t l3_freq_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	struct dm385lb_powermode* powermode = kobj_to_powermode(kobj);
	return scnprintf(buf, PAGE_SIZE, "%u\n", powermode->l3_freq);
}

static ssize_t l3_freq_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t n)
{
	unsigned long value;
	struct dm385lb_powermode* powermode = kobj_to_powermode(kobj);

	if (strict_strtoul(buf, 0, &value))
		return -EINVAL;

	powermode->l3_freq = value;

	return n;
}

static ssize_t iva_freq_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	struct dm385lb_powermode* powermode = container_of(kobj, struct dm385lb_powermode, kobj);
	return scnprintf(buf, PAGE_SIZE, "%u\n", powermode->iva_freq);
}

#ifdef CONFIG_DM385_LB_POWER_MODES
static ssize_t iva_freq_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t n)
{
	unsigned long value;
	struct dm385lb_powermode* powermode = kobj_to_powermode(kobj);

	if (strict_strtoul(buf, 0, &value))
		return -EINVAL;

	powermode->iva_freq = value;

	return n;
}
#endif

static struct kobj_attribute cpu_freq_attr =
__ATTR(cpu_freq, 0644, cpu_freq_show, cpu_freq_store);

#ifdef CONFIG_DM385_LB_POWER_MODES

static struct kobj_attribute iss_freq_attr =
__ATTR(iss_freq, 0644, iss_freq_show, iss_freq_store);

static struct kobj_attribute iva_freq_attr =
__ATTR(iva_freq, 0644, iva_freq_show, iva_freq_store);

#else

static struct kobj_attribute iss_freq_attr =
__ATTR_RO(iss_freq);

static struct kobj_attribute iva_freq_attr =
__ATTR_RO(iva_freq);

#endif

static struct kobj_attribute l3_freq_attr =
__ATTR(l3_freq, 0644, l3_freq_show, l3_freq_store);

struct kobject * governors;

static struct attribute * def_attrs[] = {
	&cpu_freq_attr.attr,
	&iss_freq_attr.attr,
	&l3_freq_attr.attr,
	&iva_freq_attr.attr,
	NULL,
};

static struct kobj_type powermode_type = {
	.sysfs_ops = &kobj_sysfs_ops,
	.default_attrs = def_attrs
};

static struct sysdev_class lbgov_debug_class = {
	.name		= "lbgov_debug",

};

static struct sys_device lbgov_debug_device = {
	.id		= 0,
	.cls		= &lbgov_debug_class,
};

static int cpufreq_gov_lb_debug_init(void)
{
	int ret;
	int i;
	struct dm385lb_powermode *powermode;

	ret = sysdev_class_register(&lbgov_debug_class);
	if(ret < 0) {
		return ret;
	}

	ret = sysdev_register(&lbgov_debug_device);
	if(ret < 0) {
		goto err_sysdev_register;
	}

	for (i = 0; i < ARRAY_SIZE(powermodes); i++) {
		powermode = &powermodes[i];
		ret = kobject_init_and_add(&powermode->kobj, &powermode_type, &lbgov_debug_device.kobj, powermode->governor.name);
		if(ret != 0) {
			pr_err("%s: Failed to kobject for governor \"%s\"\n",
					powermode->governor.name, __func__);
			goto err_kobject_register;
		}
	}

	return 0;

err_kobject_register:
	i -= 1;
	for (; i >= 0; i--)
		kobject_del(&powermode[i].kobj);

	sysdev_unregister(&lbgov_debug_device);

err_sysdev_register:
	sysdev_class_unregister(&lbgov_debug_class);

	return ret;
}

static void cpufreq_gov_lb_debug_exit(void)
{
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(powermodes); i++)
		kobject_del(&powermodes[i].kobj);

	sysdev_unregister(&lbgov_debug_device);

	sysdev_class_unregister(&lbgov_debug_class);
}

#else /* CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG */

static inline int cpufreq_gov_lb_debug_init(void) { return 0; }
static inline void cpufreq_gov_lb_debug_exit(void) {}

#endif /* CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG */

static int __init cpufreq_gov_lb_init(void)
{
	int ret = 0;
	int i;
	struct dm385lb_powermode *powermode;

	for (i = 0; i < ARRAY_SIZE(powermodes); i++) {
		powermode = &powermodes[i];
		ret = cpufreq_register_governor(&powermode->governor);
		if (ret < 0) {
			pr_err("%s: Failed to register cpufreq governor \"%s\"\n",
					powermode->governor.name, __func__);
			goto err_cpufreq_register;
		}
	}
	cpufreq_gov_lb_wq = create_workqueue("kcpufreqlb");

	ret = cpufreq_gov_lb_debug_init();
	if(ret < 0) {
		goto err_debug_init;
	}

	return 0;

err_debug_init:
	i = ARRAY_SIZE(powermodes);

err_cpufreq_register:
	i -= 1;
	for (; i >= 0; i--)
		cpufreq_unregister_governor(&powermodes[i].governor);

	return ret;
}


static void __exit cpufreq_gov_lb_exit(void)
{
	unsigned int i;

	cpufreq_gov_lb_debug_exit();

	for (i = 0; i < ARRAY_SIZE(powermodes); i++)
		cpufreq_unregister_governor(&powermodes[i].governor);
}


MODULE_AUTHOR("Komal Padia <komal.jpadia@pathpartnertech.com>");
MODULE_DESCRIPTION("Longbeach CPUfreq policy governor'");
MODULE_LICENSE("GPL");

module_init(cpufreq_gov_lb_init);
module_exit(cpufreq_gov_lb_exit);
