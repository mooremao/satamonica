/*
 * Frequency governor for DM385 LongBeach
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef __ARCH_ARM_MACH_OMAP2_DM385LB_FREQGOV_COMMON_H
#define __ARCH_ARM_MACH_OMAP2_DM385LB_FREQGOV_COMMON_H

#include <plat/iss-omap.h>
#include <plat/l3-omap.h>
#include <plat/iva-omap.h>
#include <linux/cpufreq.h>

struct dm385lb_powermode {
	struct cpufreq_governor governor;

	unsigned int cpu_freq_min;
	unsigned int cpu_freq_max;

	unsigned int cpu_freq;
	unsigned int iss_freq;
	unsigned int l3_freq;
	unsigned int iva_freq;
#ifdef CONFIG_DM385_LB_FREQ_GOVERNORS_DEBUG
	struct kobject kobj;
#endif
};
#endif
