/*
 * L3 Frequency scaling for OMAP
 * (using drivers/freqscaling/freqscaling.c)
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/opp.h>
#include <linux/regulator/consumer.h>
#include <plat/freqscaling.h>
#include <plat/l3-omap.h>
#include <plat/omap-pm.h>
#include <plat/common.h>
#include <plat/omap_device.h>
#include <plat/board_id.h>

#define L3_CLK	"l3_dpll_ck"
#define L3_VERY_HI_RATE 240000000

static struct clk *l3_clk;
struct device *l3_dev;

static unsigned int l3_getfreq(void);

struct freqscaling_ops l3_freqscaling_ops = {
	.get_freq = l3_getfreq,
	.set_freq = l3_setfreq,
};

struct freqscaling_info l3_freqscaling_info;

struct freqscaling_driver l3_freqscaling_driver = {
	.name = "l3",
	.ops = &l3_freqscaling_ops,
	.info = &l3_freqscaling_info,
};

int l3_setfreq(unsigned int target_freq)
{
	int ret = 0;
	unsigned int freq_old, freq_new;

	/* Ensure desired rate is within allowed range */
	if (target_freq < l3_freqscaling_driver.info->min_freq)
		target_freq = l3_freqscaling_driver.info->min_freq;
	if (target_freq > l3_freqscaling_driver.info->max_freq)
		target_freq = l3_freqscaling_driver.info->max_freq;

	freq_old = l3_getfreq();
	freq_new = clk_round_rate(l3_clk, target_freq * 1000) / 1000;

	if (freq_old == freq_new)
		return ret;

	ret = clk_set_rate(l3_clk, freq_new * 1000);

	return ret;
}
EXPORT_SYMBOL(l3_setfreq);

static unsigned int l3_getfreq(void)
{
	unsigned long rate;

	rate = clk_get_rate(l3_clk) / 1000;
	return rate;
}


__init int l3_freqscaling_init(void)
{
	unsigned long freq;
	struct opp *opp;

	l3_clk = clk_get(NULL, L3_CLK);
	if (IS_ERR(l3_clk))
		return PTR_ERR(l3_clk);

	l3_dev = omap2_get_l3_device();

	/* Get the minimum and maximum frequency
	 * from the OPP table
	 */
	freq = 0;
	opp = opp_find_freq_ceil(l3_dev, &freq);
	l3_freqscaling_info.min_freq = opp_get_freq(opp) / 1000;
	freq = L3_VERY_HI_RATE;
	opp = opp_find_freq_floor(l3_dev, &freq);
	l3_freqscaling_info.max_freq = opp_get_freq(opp) / 1000;

	freqscaling_register_driver(&l3_freqscaling_driver);

	return 0;
}

late_initcall(l3_freqscaling_init);
