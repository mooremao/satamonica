/*
 * Framework for sysfs based frequency control of SoC subsystems
 * (Based on drivers/freqscaling/freqscaling.c)
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <plat/freqscaling.h>
#include <plat/iva-omap.h>
#include <plat/cpu-omap.h>
#include <linux/sysfs.h>
#include <linux/err.h>
#include <linux/regulator/consumer.h>

#define SINGLE_OPP_VOLTAGE_mV 1320000
#define DEFAULT_RESOLUTION 12500

/* If set, device voltage will be changed when frequency is changed */
int voltage_scaling_enabled;
static struct regulator *mpu_reg;
static struct regulator *iva_reg;

#ifdef CONFIG_DM385_LB_CROSSOPP
extern struct kobject *power_kobj;
#endif

/*********************************************************************
 *                    SYSFS HELPER FUNCTIONS                         *
 *********************************************************************/
#define show_one(file_name, object)			\
	static ssize_t show_##file_name			\
(struct freqscaling_driver *driver, char *buf)		\
{							\
	return sprintf(buf, "%u\n", driver->info->object);	\
}
show_one(scaling_max_freq, max_freq);
show_one(scaling_min_freq, min_freq);

static ssize_t show_scaling_cur_freq(struct freqscaling_driver *driver,
		char *buf)
{
	driver->info->cur_freq = driver->ops->get_freq();
	return sprintf(buf, "%u\n", driver->info->cur_freq);
}

static ssize_t show_scaling_setspeed(struct freqscaling_driver *driver,
		char *buf)
{
	driver->info->cur_freq = driver->ops->get_freq();
	return sprintf(buf, "%u\n", driver->info->cur_freq);
}

static ssize_t store_scaling_setspeed(struct freqscaling_driver *driver,
		const char *buf, size_t count)
{
	unsigned int freq = 0;
	unsigned int ret;

	ret = sscanf(buf, "%u", &freq);
	if (ret != 1)
		return -EINVAL;

	driver->ops->set_freq(freq);

	return count;
}

struct freqscaling_attr {
	struct attribute attr;
	ssize_t (*show)(struct freqscaling_driver *, char *);
	ssize_t (*store)(struct freqscaling_driver *, const char *,
			size_t count);
};

#define freqscaling_freq_attr_ro(_name)		\
	static struct freqscaling_attr _name =	\
__ATTR(_name, 0444, show_##_name, NULL)

#define freqscaling_freq_attr_rw(_name)		\
	static struct freqscaling_attr _name =	\
__ATTR(_name, 0644, show_##_name, store_##_name)

freqscaling_freq_attr_ro(scaling_max_freq);
freqscaling_freq_attr_ro(scaling_min_freq);
freqscaling_freq_attr_ro(scaling_cur_freq);
freqscaling_freq_attr_rw(scaling_setspeed);

static struct attribute *default_attrs[] = {
	&scaling_min_freq.attr,
	&scaling_max_freq.attr,
	&scaling_cur_freq.attr,
	&scaling_setspeed.attr,
	NULL
};

#define to_driver(k) container_of(k, struct freqscaling_driver, kobj)
#define to_attr(a) container_of(a, struct freqscaling_attr, attr)

static ssize_t show(struct kobject *kobj, struct attribute *attr, char *buf)
{
	struct freqscaling_driver *driver = to_driver(kobj);
	struct freqscaling_attr *fattr = to_attr(attr);
	ssize_t ret = -EINVAL;

	if (fattr->show)
		ret = fattr->show(driver, buf);
	else
		ret = -EIO;

	return ret;
}

static ssize_t store(struct kobject *kobj, struct attribute *attr,
		const char *buf, size_t count)
{
	struct freqscaling_driver *driver = to_driver(kobj);
	struct freqscaling_attr *fattr = to_attr(attr);
	ssize_t ret = -EINVAL;

	if (fattr->store)
		ret = fattr->store(driver, buf, count);
	else
		ret = -EIO;

	return ret;
}

static void freqscaling_sysfs_release(struct kobject *kobj)
{
	struct freqscaling_driver *driver = to_driver(kobj);
	printk(KERN_DEBUG "last reference is dropped\n");
	complete(&driver->kobj_unregister);
}

static const struct sysfs_ops sysfs_ops = {
	.show	= show,
	.store	= store,
};

static struct kobj_type ktype_freqscaling = {
	.sysfs_ops	= &sysfs_ops,
	.default_attrs	= default_attrs,
	.release	= freqscaling_sysfs_release,
};

/*********************************************************************
 *           REGISTER / UNREGISTER FREQSCALING DRIVER                *
 *********************************************************************/
#define sysdev_to_driver(d) container_of(d, struct freqscaling_driver, sysdev)
static int add_sysdev(struct sys_device *sysdev)
{
	int ret;
	struct freqscaling_driver *driver;

	driver = sysdev_to_driver(sysdev);
	ret = kobject_init_and_add(&driver->kobj, &ktype_freqscaling,
			&driver->sysdev.kobj, "freqscaling");

	return ret;
}

static int remove_sysdev(struct sys_device *sysdev)
{
	struct freqscaling_driver *driver;

	driver = sysdev_to_driver(sysdev);
	kobject_del(&driver->kobj);

	return 0;
}

static int set_single_opp(void)
{
	int volt_old = 0, volt_new = 0;
	int ret;

	volt_new = SINGLE_OPP_VOLTAGE_mV;

	volt_old = regulator_get_voltage(mpu_reg);
	if (volt_old != volt_new) {
		ret = regulator_set_voltage(mpu_reg, volt_new,
				volt_new + DEFAULT_RESOLUTION - 1);
		if (ret) {
			pr_err("%s: unable to set mpu voltage to %d uV\n",
					__func__, volt_new);
			return ret;
		}
	}

	volt_old = regulator_get_voltage(iva_reg);
	if (volt_old != volt_new) {
		ret = regulator_set_voltage(iva_reg, volt_new,
				volt_new + DEFAULT_RESOLUTION - 1);
		if (ret) {
			pr_err("%s: unable to set mpu voltage to %d uV\n",
					__func__, volt_new);
			return ret;
		}
	}

	return 0;
}

#ifdef CONFIG_DM385_LB_CROSSOPP
/* NOTE: The way single OPP and cross OPP works right now is
 * in single OPP mode we set the voltages to 1.32V at init
 * and do not allow voltage scaling by the DVFS drivers.
 * Frequencies can still be changed.
 *
 * In cross OPP mode voltage scaling by DVFS drivers is
 * enabled. The voltages are scaled as per frequency according
 * to OPP table entries
 *
 * To change the way single and cross OPP modes are supported
 * change the functions crossopp_store below and set_single_opp
 * above
 */
static ssize_t crossopp_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", voltage_scaling_enabled);
}

static ssize_t crossopp_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t n)
{
	unsigned long val;

	if (strict_strtoul(buf, 10, &val))
		return -EINVAL;

	if (val > 1)
		return -EINVAL;

	voltage_scaling_enabled = val;
	if (voltage_scaling_enabled) {
		omap_setvoltage(0);
		iva_setvoltage();
	}
	else {
		set_single_opp();
	}

	return n;
}

static struct kobj_attribute crossopp_attr =
__ATTR(crossopp_enabled, 0644, crossopp_show,
		crossopp_store);
#endif

__init int freqscaling_init(void)
{
	voltage_scaling_enabled = 0;

#ifdef CONFIG_DM385_LB_CROSSOPP
	sysfs_add_file_to_group(power_kobj, &crossopp_attr.attr, NULL);
#endif

	mpu_reg = regulator_get(NULL, "mpu");
	if (IS_ERR(mpu_reg)) {
		pr_err("%s:Regulator get failed\n", __func__);
		return -EINVAL;
	}

	iva_reg = regulator_get(NULL, "iva");
	if (IS_ERR(iva_reg)) {
		pr_err("%s:Regulator get failed\n", __func__);
		return -EINVAL;
	}

	/* Set OPP to single OPP by default */
	set_single_opp();

	return 0;
}
deferred_initcall(freqscaling_init);

/*
 * freqscaling_register_driver - register a Frequency scaling driver
 * @driver_data: A struct freqscaling_driver containing the values
 * submitted by the subsystem driver.
 */
int freqscaling_register_driver(struct freqscaling_driver *driver)
{
	if (!driver)
		return -EINVAL;

	/* Register a system class for this subsystem */
	driver->syscls.name = driver->name;
	sysdev_class_register(&driver->syscls);

	/* Register a system driver for this subsystem */
	driver->sysdrv.add = add_sysdev;
	driver->sysdrv.remove = remove_sysdev;
	sysdev_driver_register(&driver->syscls, &driver->sysdrv);

	/* Register a system device for this subsystem */
	driver->sysdev.id = 0;
	driver->sysdev.cls = &driver->syscls;
	sysdev_register(&driver->sysdev);

	return 0;
}
EXPORT_SYMBOL(freqscaling_register_driver);

void freqscaling_unregister_driver(struct freqscaling_driver *driver)
{
	sysdev_unregister(&driver->sysdev);
	sysdev_driver_unregister(&driver->syscls, &driver->sysdrv);
	sysdev_class_unregister(&driver->syscls);
}
EXPORT_SYMBOL(freqscaling_unregister_driver);
