/*
 * ISS Frequency scaling for OMAP
 * (using drivers/freqscaling/freqscaling.c)
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/opp.h>
#include <linux/regulator/consumer.h>
#include <plat/freqscaling.h>
#include <plat/iss-omap.h>
#include <plat/omap-pm.h>
#include <plat/common.h>
#include <plat/omap_device.h>
#include <plat/board_id.h>
#include "../mach-omap2/powerdomain.h"
#include "../mach-omap2/clockdomain.h"

#define ISS_CLK	"iss_dpll_ck"
#define ISS_VERY_HI_RATE 560000000

static struct clk *iss_clk;
struct device *iss_dev;
struct powerdomain *iss_pwrdm;

static unsigned int iss_getfreq(void);

struct freqscaling_ops iss_freqscaling_ops = {
	.get_freq = iss_getfreq,
	.set_freq = iss_setfreq,
};

struct freqscaling_info iss_freqscaling_info;

struct freqscaling_driver iss_freqscaling_driver = {
	.name = "iss",
	.ops = &iss_freqscaling_ops,
	.info = &iss_freqscaling_info,
};

static int iss_clkdm_force_sleep(struct powerdomain *pwrdm, struct clockdomain *clkdm)
{
	return omap2_clkdm_sleep(clkdm);
}

static int iss_clkdm_force_wakeup(struct powerdomain *pwrdm, struct clockdomain *clkdm)
{
	return omap2_clkdm_wakeup(clkdm);
}

int iss_setfreq(unsigned int target_freq)
{
	int ret = 0;
	unsigned int freq_old, freq_new;
	unsigned int pwrdm_state;

	pwrdm_state = pwrdm_read_pwrst(iss_pwrdm);

	if (target_freq == 0) {
		/* Turning off ISS is not supported */
		return -EINVAL;
	}
	else if (pwrdm_state == PWRDM_POWER_OFF) {
		/* The power domain is in the OFF state
		 * We need to turn on the power domain before
		 * we attempt to change its frequency
		 */

		/* Request all clock domains in this power domain
		 * to go to forced wakeup state
		 */
		pwrdm_for_each_clkdm(iss_pwrdm, iss_clkdm_force_wakeup);

		/* Always setting the next power state for a power domain
		 * to OFF. The functional clock domain register decides the
		 * state of the power domain. Since we have forced wakeup
		 * on the clock domain, the power domain will be turned ON
		 */
		pwrdm_set_next_pwrst(iss_pwrdm, PWRDM_POWER_OFF);
		ret = pwrdm_wait_transition(iss_pwrdm);
		if (ret) {
			dev_err(iss_dev, "Error: Could not turn on power domain\n");
			return ret;
		}
	}

	/* Ensure desired rate is within allowed range */
	if (target_freq < iss_freqscaling_driver.info->min_freq)
		target_freq = iss_freqscaling_driver.info->min_freq;
	if (target_freq > iss_freqscaling_driver.info->max_freq)
		target_freq = iss_freqscaling_driver.info->max_freq;

	freq_old = iss_getfreq();
	freq_new = clk_round_rate(iss_clk, target_freq * 1000) / 1000;

	if (freq_old == freq_new)
		return ret;

	ret = clk_set_rate(iss_clk, freq_new * 1000);

	return ret;
}
EXPORT_SYMBOL(iss_setfreq);

static unsigned int iss_getfreq(void)
{
	unsigned long rate;

	rate = clk_get_rate(iss_clk) / 1000;
	return rate;
}


__init int iss_freqscaling_init(void)
{
	unsigned long freq;
	struct opp *opp;

	iss_clk = clk_get(NULL, ISS_CLK);
	if (IS_ERR(iss_clk))
		return PTR_ERR(iss_clk);

	iss_pwrdm = pwrdm_lookup("isp_pwrdm");
	if (IS_ERR(iss_pwrdm))
		pr_err("%s:Failed to get iss power domain\n", __func__);

	iss_dev = omap2_get_iss_device();

	/* Get the minimum and maximum frequency
	 * from the OPP table
	 */
	freq = 0;
	opp = opp_find_freq_ceil(iss_dev, &freq);
	iss_freqscaling_info.min_freq = opp_get_freq(opp) / 1000;
	freq = ISS_VERY_HI_RATE;
	opp = opp_find_freq_floor(iss_dev, &freq);
	iss_freqscaling_info.max_freq = opp_get_freq(opp) / 1000;

	freqscaling_register_driver(&iss_freqscaling_driver);

	return 0;
}

late_initcall(iss_freqscaling_init);
