/*
 * IVA Frequency scaling for OMAP
 * (using drivers/freqscaling/freqscaling.c)
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/opp.h>
#include <linux/io.h>
#include <linux/regulator/consumer.h>
#include <plat/freqscaling.h>
#include <plat/iva-omap.h>
#include <plat/omap-pm.h>
#include <plat/common.h>
#include <plat/omap_device.h>
#include <plat/board_id.h>
#include "../mach-omap2/powerdomain.h"
#include "../mach-omap2/clockdomain.h"
#include "../mach-omap2/prm2xxx_3xxx.h"

#define IVA_CLK	"hdvicp_dpll_ck"
#define DEFAULT_RESOLUTION 12500
#define IVA_VERY_HI_RATE 450000000
#define TI81XX_RST_ASSERT TI81XX_PRCM_BASE + TI814X_PRM_HDVICP_MOD + TI81XX_RM_RSTCTRL
#define RST_ASSERT_VAL 0x03

static struct clk *iva_clk;
static struct regulator *iva_reg;
struct device *iva_dev;
struct powerdomain *iva_pwrdm;
extern int voltage_scaling_enabled;
static void __iomem *rstaddr;

static unsigned int iva_getfreq(void);

struct freqscaling_ops iva_freqscaling_ops = {
	.get_freq = iva_getfreq,
	.set_freq = iva_setfreq,
};

struct freqscaling_info iva_freqscaling_info;

struct freqscaling_driver iva_freqscaling_driver = {
	.name = "iva",
	.ops = &iva_freqscaling_ops,
	.info = &iva_freqscaling_info,
};

static int iva_clkdm_force_sleep(struct powerdomain *pwrdm, struct clockdomain *clkdm)
{
	return omap2_clkdm_sleep(clkdm);
}

static int iva_clkdm_force_wakeup(struct powerdomain *pwrdm, struct clockdomain *clkdm)
{
	return omap2_clkdm_wakeup(clkdm);
}

int iva_setvoltage(void)
{
	struct opp *opp;
	int volt_old = 0, volt_new = 0;
	unsigned long freq;
	int ret;

	freq = iva_getfreq() * 1000;
	opp = opp_find_freq_ceil(iva_dev, &freq);
	if (IS_ERR(opp)) {
		dev_err(iva_dev, "%s: iva: no opp match for freq %lu\n",
				__func__, freq);
		return -EINVAL;
	}

	volt_new = opp_get_voltage(opp);
	if (!volt_new) {
		dev_err(iva_dev, "%s: iva: no opp voltage for freq %lu\n",
				__func__, freq);
		return -EINVAL;
	}

	volt_old = regulator_get_voltage(iva_reg);

	if (volt_old != volt_new) {
		ret = regulator_set_voltage(iva_reg, volt_new,
				volt_new + DEFAULT_RESOLUTION - 1);
		if (ret) {
			dev_err(iva_dev, "%s: unable to set voltage to"
					" %d uV (for %lu MHz)\n", __func__,
					volt_new, freq/1000);
			return ret;
		}
	}

	return 0;
}
EXPORT_SYMBOL(iva_setvoltage);

int iva_setfreq(unsigned int target_freq)
{
	int ret = 0;
	unsigned int freq_old, freq_new;
	unsigned int pwrdm_state;
	unsigned long freq;
	unsigned int volt_old, volt_new = 0;
	struct opp *opp;

	pwrdm_state = pwrdm_read_pwrst(iva_pwrdm);

	if (target_freq == 0) {
		/* Power domain is already in OFF state */
		if (pwrdm_state == PWRDM_POWER_OFF)
			return 0;

		/* Assert reset of IVA module put into standby */
		__raw_writel(__raw_readl(rstaddr) | RST_ASSERT_VAL, rstaddr);

		/* Request all clock domains in this power domain
		 * to go to forced sleep state
		 */
		pwrdm_for_each_clkdm(iva_pwrdm, iva_clkdm_force_sleep);
		pwrdm_set_next_pwrst(iva_pwrdm, PWRDM_POWER_OFF);
		ret = pwrdm_wait_transition(iva_pwrdm);
		return ret;
	}
	else if (pwrdm_state == PWRDM_POWER_OFF) {
		/* The power domain is in the OFF state
		 * We need to turn on the power domain before
		 * we attempt to change its frequency
		 */

		/* Request all clock domains in this power domain
		 * to go to forced wakeup state
		 */
		pwrdm_for_each_clkdm(iva_pwrdm, iva_clkdm_force_wakeup);

		/* Always setting the next power state for a power domain
		 * to OFF. The functional clock domain register decides the
		 * state of the power domain. Since we have forced wakeup
		 * on the clock domain, the power domain will be turned ON
		 */
		pwrdm_set_next_pwrst(iva_pwrdm, PWRDM_POWER_OFF);
		ret = pwrdm_wait_transition(iva_pwrdm);
		if (ret) {
			dev_err(iva_dev, "Error: Could not turn on power domain\n");
			return ret;
		}
		/* Clear reset of IVA module */
		__raw_writel(__raw_readl(rstaddr) & (~RST_ASSERT_VAL), rstaddr);

	}

	/* Ensure desired rate is within allowed range */
	if (target_freq < iva_freqscaling_driver.info->min_freq)
		target_freq = iva_freqscaling_driver.info->min_freq;
	if (target_freq > iva_freqscaling_driver.info->max_freq)
		target_freq = iva_freqscaling_driver.info->max_freq;

	freq_old = iva_getfreq();
	freq_new = clk_round_rate(iva_clk, target_freq * 1000) / 1000;

	if (freq_old == freq_new)
		return ret;

	if (voltage_scaling_enabled && (system_rev >= LONG_BEACH_PR1_BOARD)) {
		freq = freq_new * 1000;
		opp = opp_find_freq_ceil(iva_dev, &freq);
		if (IS_ERR(opp)) {
			dev_err(iva_dev, "%s: iva: no opp match for freq %d\n",
					__func__, target_freq);
			return -EINVAL;
		}

		volt_new = opp_get_voltage(opp);
		if (!volt_new) {
			dev_err(iva_dev, "%s: iva: no opp voltage for freq %d\n",
					__func__, target_freq);
			return -EINVAL;
		}

		volt_old = regulator_get_voltage(iva_reg);

		if (freq_new > freq_old) {
			ret = regulator_set_voltage(iva_reg, volt_new,
					volt_new + DEFAULT_RESOLUTION - 1);
			if (ret) {
				dev_err(iva_dev, "%s: unable to set voltage to"
						" %d uV (for %u MHz)\n", __func__,
						volt_new, freq_new/1000);
				return ret;
			}
		}
	}

	ret = clk_set_rate(iva_clk, freq_new * 1000);

	if (voltage_scaling_enabled && system_rev >= LONG_BEACH_PR1_BOARD) {
		freq_new = iva_getfreq();

		if (freq_new < freq_old) {
			ret = regulator_set_voltage(iva_reg, volt_new,
					volt_new + DEFAULT_RESOLUTION - 1);
			if (ret) {
				dev_err(iva_dev, "%s: unable to set voltage to"
						" %d uV (for %u MHz)\n", __func__,
						volt_new, freq_new/1000);

				if (clk_set_rate(iva_clk, freq_old * 1000)) {
					dev_err(iva_dev, "%s: failed restoring"
							" clock rate to %u MHz,"
							" clock rate is %u MHz",
							__func__, freq_old/1000,
							freq_new/1000);
					return ret;
				}
			}
		}
	}

	return ret;
}
EXPORT_SYMBOL(iva_setfreq);

static unsigned int iva_getfreq(void)
{
	unsigned long rate;

	rate = clk_get_rate(iva_clk) / 1000;
	return rate;
}


__init int iva_freqscaling_init(void)
{
	unsigned long freq;
	struct opp *opp;

	iva_clk = clk_get(NULL, IVA_CLK);
	if (IS_ERR(iva_clk))
		return PTR_ERR(iva_clk);

	iva_reg = regulator_get(NULL, "iva");
	if (IS_ERR(iva_reg)) {
		pr_err("%s:Regulator get failed\n", __func__);
		clk_put(iva_clk);
		return -EINVAL;
	}

	iva_pwrdm = pwrdm_lookup("ivahd_pwrdm");
	if (IS_ERR(iva_pwrdm))
		pr_err("%s:Failed to get iva power domain\n", __func__);

	iva_dev = omap2_get_iva_device();

	/* Get the minimum and maximum frequency
	 * from the OPP table
	 */
	freq = 0;
	opp = opp_find_freq_ceil(iva_dev, &freq);
	iva_freqscaling_info.min_freq = opp_get_freq(opp) / 1000;
	freq = IVA_VERY_HI_RATE;
	opp = opp_find_freq_floor(iva_dev, &freq);
	iva_freqscaling_info.max_freq = opp_get_freq(opp) / 1000;

	freqscaling_register_driver(&iva_freqscaling_driver);

	rstaddr = ioremap(TI81XX_RST_ASSERT, sizeof(int));

	return 0;
}

late_initcall(iva_freqscaling_init);
