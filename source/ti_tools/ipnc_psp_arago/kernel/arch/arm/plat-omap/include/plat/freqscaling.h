/*
 * Framework for sysfs based frequency control of SoC subsystems
 * (Based on drivers/cpufreq/cpufreq.c)
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __ARCH_ARM_PLAT_OMAP_INCLUDE_PLAT_FREQSCALING_H
#define __ARCH_ARM_PLAT_OMAP_INCLUDE_PLAT_FREQSCALING_H

#include <linux/device.h>
#include <linux/kobject.h>
#include <linux/completion.h>
#include <linux/sysdev.h>

/*********************************************************************
 *                    FREQUENCY SCALING INTERFACE                    *
 *********************************************************************/
#define FREQSCALING_DRV_NAME_LENGTH 10

/*
 * Frequency scaling information for a subsystem
 * We currently support minimum, maximum and current frequency
 * Can be extended to support other parameters
 */
struct freqscaling_info {
	unsigned int max_freq;
	unsigned int min_freq;
	unsigned int cur_freq;
};

/*
 * The frequency scaling driver for a subsystem must define
 * functions to set and get the frequency
 */
struct freqscaling_ops {
	int (*set_freq) (unsigned int freq);
	unsigned int (*get_freq) (void);
};

struct freqscaling_driver {
	char name[FREQSCALING_DRV_NAME_LENGTH];

	struct freqscaling_info *info;
	struct freqscaling_ops *ops;

	struct sysdev_class syscls;
	struct sys_device sysdev;
	struct sysdev_driver sysdrv;

	struct kobject kobj;
	struct completion kobj_unregister;
};

int freqscaling_register_driver(struct freqscaling_driver *driver);
void freqscaling_unregister_driver(struct freqscaling_driver *driver);

#endif
