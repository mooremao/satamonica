/*
 * CPU Frequency scaling for OMAP
 *
 * Copyright (C) 2014-2015 TomTom International B.V.
 * Written by Komal Padia <komal.jpadia@pathpartnertech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __ARCH_ARM_PLAT_OMAP_INCLUDE_PLAT_CPU_OMAP_H
#define __ARCH_ARM_PLAT_OMAP_INCLUDE_PLAT_CPU_OMAP_H

int omap_setvoltage(unsigned int cpu);
#endif
