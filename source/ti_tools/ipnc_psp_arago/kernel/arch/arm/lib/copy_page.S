/*
 *  linux/arch/arm/lib/copypage.S
 *
 *  Copyright (C) 1995-1999 Russell King
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 *  ASM optimised string functions
 */
#include <linux/linkage.h>
#include <asm/assembler.h>
#include <asm/asm-offsets.h>
#include <asm/cache.h>

#define COPY_COUNT (PAGE_SZ / L1_CACHE_BYTES)

		.text
		.align	5
/*
 * NEON and ARM optimised copy_page routine
 * profiling needs to be performed for accurate performance numbers
 */
ENTRY(copy_page)
#ifndef CONFIG_NEON
		stmfd	sp!, {r4-r8, lr}
#else
		stmfd	sp!, {r4, lr}
		stmfd	sp!, {r0-r1}
		bl	kernel_neon_begin
		ldmfd	sp!, {r0-r1}
#endif
	PLD(	pld	[r1, #0]		)
	PLD(	pld	[r1, #L1_CACHE_BYTES]		)
	PLD(	pld	[r1, #2 * L1_CACHE_BYTES])
	PLD(	pld	[r1, #3 * L1_CACHE_BYTES])

		/* completely unrolled loop */
#ifdef CONFIG_NEON
		.rept	((COPY_COUNT / 4) - 1)
		vldm	r1!, {d0-d15}
		vstm	r0!, {d0-d15}
	PLD(	pld	[r1, #2 * L1_CACHE_BYTES])
	PLD(	pld	[r1, #3 * L1_CACHE_BYTES]	)
		vldm	r1!, {d16-d31}
		vstm	r0!, {d16-d31}
	PLD(	pld	[r1, #2 * L1_CACHE_BYTES])
	PLD(	pld	[r1, #3 * L1_CACHE_BYTES]	)
	PLD(	pli	[pc, #L1_CACHE_BYTES]		)

		.endr
#else
		.rept	(COPY_COUNT - 1)
		ldmia	r1!, {r2-r8, lr}
		stmia	r0!, {r2-r8, lr}
		ldmia	r1!, {r2-r8, lr}
		stmia	r0!, {r2-r8, lr}
	PLD(	pld	[r1, #3 * L1_CACHE_BYTES]	)
	PLD(	pli	[pc, #L1_CACHE_BYTES]		)
		.endr
#endif

#ifdef CONFIG_NEON
		vldm	r1!, {d0-d15}
		vstm	r0!, {d0-d15}
		vldm	r1!, {d16-d31}
		vstm	r0!, {d16-d31}

		bl	kernel_neon_end
		ldmfd	sp!, {r4, pc}
#else
		ldmia	r1!, {r2-r8, lr}
		stmia	r0!, {r2-r8, lr}
		ldmia	r1!, {r2-r8, lr}
		stmia	r0!, {r2-r8, lr}
		ldmfd	sp!, {r4-r8, pc}
#endif

ENDPROC(copy_page)
