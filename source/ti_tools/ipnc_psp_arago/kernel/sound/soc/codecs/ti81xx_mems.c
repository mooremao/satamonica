/*
 * ALSA SoC TI81XX MEMS dummy driver
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <sound/soc.h>
#include <sound/pcm.h>
#include <sound/initval.h>

static struct snd_soc_codec_driver soc_codec_mems;

/* Dummy dai driver for MEMS */
static struct snd_soc_dai_driver ti81xx_dai = {
	.name = "MEMS-DAI-CODEC",
	.capture = {
		.stream_name = "Capture",
		.channels_min = 2,
		.channels_max = 2,
		.rates = SNDRV_PCM_RATE_48000
			| SNDRV_PCM_RATE_32000
			| SNDRV_PCM_RATE_44100
			| SNDRV_PCM_RATE_96000
			| SNDRV_PCM_RATE_192000 ,
		.formats = SNDRV_PCM_FMTBIT_S16_LE
			| SNDRV_PCM_FMTBIT_S32_LE
			| SNDRV_PCM_FMTBIT_S24_LE,
	},
};

static int mems_codec_probe(struct platform_device *pdev)
{
	int ret;

	ret = snd_soc_register_codec(&pdev->dev, &soc_codec_mems,
					&ti81xx_dai, 1);
	if (ret < 0)
		printk(KERN_INFO " MEMS Codec Register Failed\n");

	return ret;
}

static int mems_codec_remove(struct platform_device *pdev)
{
	snd_soc_unregister_codec(&pdev->dev);
	return 0;
}

static struct platform_driver mems_codec_driver = {
	.probe		= mems_codec_probe,
	.remove		= mems_codec_remove,
	.driver		= {
		.name	= "mems-dummy-codec",
		.owner	= THIS_MODULE,
	},
};

static int __init mems_modinit(void)
{
	return platform_driver_register(&mems_codec_driver);
}

static void __exit mems_exit(void)
{
	platform_driver_unregister(&mems_codec_driver);
}

module_init(mems_modinit);
module_exit(mems_exit);

MODULE_DESCRIPTION(" MEMS Dummy codec Interface");
MODULE_LICENSE("GPL");
