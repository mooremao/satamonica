/*
 * linux/include/video/ls013b7dh01.h -- FB driver for ls013b7dh01 LCD controller
 *
 * Copyright (C) 2014, Travis Kuo
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#define DRVNAME		"ls013b7dh01"
#define WIDTH		144
#define HEIGHT		168
#define BPP		1
#define LCM_PIXELS_PER_BYTE		8
#define LCM_BYTES_PER_LINE		(WIDTH / LCM_PIXELS_PER_BYTE)
/* Supported display modules */
#define LS013B7DH01_DISPLAY_AF_TFT18		0	/* Adafruit SPI TFT 1.8" */

typedef struct _lcm_line_t
{
	u8						line_addr;
	u8 						data[LCM_BYTES_PER_LINE];
	u8 						dummy_data;
} lcm_line_t;

typedef struct _SPI_Tansmit_data_t
{
	u8		mode;
	lcm_line_t	lcm_line[HEIGHT];
	u8		dummy_data;
} SPI_Tansmit_data_t;

struct ls013b7dh01_par {
	struct spi_device *spi;
	atomic_t ref_count;
	struct fb_info *info;
	u16 *ssbuf;
	int disp_on_gpio;
	int disp_5v_on_gpio;
	struct pwm_device *pwm;
	unsigned int pwm_period_ns;
	struct mutex lock_mutex; /* lock */
	SPI_Tansmit_data_t spi_x_data;
	int on;
	int rotate;
	int lcd_on_in_suspend;
};

struct ls013b7dh01_platform_data {
	int disp_on_gpio;
	int disp_5v_on_gpio;
	unsigned int pwm_id;
	unsigned int pwm_freq;
	int rotate;
	int lcd_on_in_suspend;
};
