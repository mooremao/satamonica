/*
 * TCA8418 keypad platform support
 *
 * Copyright (C) 2014 TomTom BV <http://www.tomtom.com/>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#ifndef __TCA8418_KEYPAD_H__
#define __TCA8418_KEYPAD_H__

#include <linux/types.h>

#define TCA8418_I2C_ADDR	0x34
#define	TCA8418_NAME		"tca8418_keypad"

enum tca8418_irq_type {
	TCA8418_IRQ_TYPE_SW_L		= 0x0001,
	TCA8418_IRQ_TYPE_SW_R		= 0x0002,
	TCA8418_IRQ_TYPE_SW_U		= 0x0004,
	TCA8418_IRQ_TYPE_SW_D		= 0x0008,
	TCA8418_IRQ_TYPE_SW_PS		= 0x0010,
	TCA8418_IRQ_TYPE_SW_B1		= 0x0100,
	TCA8418_IRQ_TYPE_BT_HCI		= 0x0200,
	TCA8418_IRQ_TYPE_SW_2		= 0x0400,
	TCA8418_IRQ_TYPE_HOST_INT	= 0x0800,
	TCA8418_IRQ_TYPE_BAT_LOCK	= 0x1000,
};

struct tca8418_button {
	/* Configuration parameters */
	int code;		/* input event code (KEY_*, SW_*) */
	int active_low;
	int type;		/* input event type (EV_KEY, EV_SW) */
};

struct tca8418_keypad_platform_data {
	struct tca8418_button *row_buttons;
	int nrow_buttons;
	uint16_t row_pinmask;
	struct tca8418_button *col_buttons;
	int ncol_buttons;
	uint16_t col_pinmask;
	int always_on;		/* 1: always active, 0: active before enter suspend mode */
	int rep;
	int interrupt_gpio;
	uint16_t row_debmask;	/* n-th bit = 1 -> disable debounce for n-th row */
	uint16_t col_debmask;	/* n-th bit = 1 -> disable debounce for n-th column */
};

#endif  /* __TCA8418_KEYPAD_H__ */
