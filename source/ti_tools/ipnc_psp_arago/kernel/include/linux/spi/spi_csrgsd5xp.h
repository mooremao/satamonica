/*
 * linux/include/spi/spi_csrgsd5xp.h -- GSD5xp from CSR connected via SPI
 *
 * Copyright (C) 2014, Jeroen Jensen
 *
 * General Public License version 2 (GPLv2)
 */

#ifndef SPI_CSRGSD5XP_H_
#define SPI_CSRGSD5XP_H_

struct csrgsd5xp_platform_data {
	int gps_reset_gpio;
	int gps_reset_active_high;
	int gps_on_off_gpio;
	int gps_wakeup_gpio;
	int gps_data_ready_gpio;
};

#endif /* SPI_CSRGSD5XP_H_ */
