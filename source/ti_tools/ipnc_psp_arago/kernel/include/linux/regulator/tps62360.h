/*
 * tps62360.h  --  Voltage regulation for the Texas Instruments TPS62360
 *
 * Copyright (C) 2010 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef __REGULATOR_TPS62360__
#define __REGULATOR_TPS62360__

struct tps62360_reg_platform_data {
	bool en_internal_pulldn;
	bool en_discharge;
	int vset_id;
};

#endif  /* __REGULATOR_TPS62360__ */
