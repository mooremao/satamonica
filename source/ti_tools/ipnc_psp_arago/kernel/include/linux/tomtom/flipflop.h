/*
 * Flipflop driver
 *
 * Copyright (C) 2014 TomTom BV <http://www.tomtom.com/>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#define RTC_SCRATCH0_REG	0x60
#define RTC_SCRATCH1_REG	0x64
#define RTC_SCRATCH2_REG	0x68

struct flipflop_pdata
{
#ifdef CONFIG_TOMTOM_FLIPFLOP_HW
	int boot_clk_gpio;
	int boot_q_gpio;
#else
	void __iomem *address; /* Set by driver */
	int scratch_reg_number;
	unsigned int magic1;
	unsigned int magic2;
#endif
};
