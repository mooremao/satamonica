/*
 * Tomtom MCU driver
 *
 * Copyright (C) 2014 TomTom BV <http://www.tomtom.com/>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#ifndef	__TOMTOM_MCU_H__
#define	__TOMTOM_MCU_H__

#include <linux/notifier.h>

#define TOMTOM_MCU_DEV_NAME			"tomtom_mcu"

enum {
	VBUS_PRESENT,
	VBUS_REMOVED,
};

struct tomtom_mcu_info
{
	int (*is_ac_online)(struct tomtom_mcu_info *mcu_info);
	void *priv;
};

struct tomtom_mcu_pdata
{
	int host_int_gpio;
	int vbus_on_gpio;
	int host_int_irq;
	char** usbpower_supplicants;
	int usbpower_num_supplicants;
	void (*notify_vbus)(int state);
	void (*register_callback)(struct tomtom_mcu_info *mcu_info);
	void (*unregister_callback)(struct tomtom_mcu_info *mcu_info);
};

enum host_int_type {
	HOST_INT_EXT_MIC_PRESENT = 0,
	HOST_INT_EXT_MIC_NOT_PRESENT,
	HOST_INT_WAKEUP_TIMER_TIMEOUT,
	HOST_INT_POWER_PRESENT,
	HOST_INT_POWER_NOT_PRESENT,
	HOST_INT_BATT_TEMP_IN_RANGE,
	HOST_INT_BATT_TEMP_NOT_IN_RANGE,
	HOST_INT_SW_BUTTON_PRESSED,
	HOST_INT_SOC_BATT_LOW_CHANGED,
	HOST_INT_SOC_BATT_TURN_OFF_CHANGED
};

int host_int_reg_notifier(struct notifier_block *nb);
void host_int_unreg_notifier(struct notifier_block *nb);

#endif	/* __TOMTOM_MCU_H__ */
