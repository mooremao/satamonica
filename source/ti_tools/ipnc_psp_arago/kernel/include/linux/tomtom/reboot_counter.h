/*
 * Reboot counter driver
 *
 * Copyright (C) 2015 TomTom BV <http://www.tomtom.com/>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

struct reboot_counter_pdata
{
	unsigned int (*load_reboot_counter)(void);
	void (*store_reboot_counter)(unsigned int value);
};
