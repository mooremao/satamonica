#!/bin/sh

# Script to (re-)build ALSA library
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

ALSA_LIB_VERSION=alsa-lib-1.0.24.1

# Untar the sources. We only do this when they do not yet exists.
cd alsa_lib/
if [ ! -d ${ALSA_LIB_VERSION} ]
then
	tar xjf ${ALSA_LIB_VERSION}.tar.bz2
	cd ${ALSA_LIB_VERSION}/
	patch -p1 < ../0001-fix-config-dirs.patch
	patch -p1 < ../0002-fix-tstamp-declaration.patch

else
	cd ${ALSA_LIB_VERSION}/
fi

# Configure
CFLAGS="-O2" CC=arm-arago-linux-gnueabi-gcc AR=arm-arago-linux-gnueabi-ar STRIP=arm-arago-linux-gnueabi-strip RANLIB=arm-arago-linux-gnueabi-ranlib ./configure --host=arm-arago-linux --datarootdir=/../${ALSA_LIB_VERSION}-data/usr --includedir=/../${ALSA_LIB_VERSION}-include/usr --disable-python --with-cards=pdaudiocf --with-oss=yes --disable-python --enable-shared --enable-static

# Make
make

# Make install
make install-strip DESTDIR=`pwd`/../${ALSA_LIB_VERSION}_arm-arago-linux-gnueabi/

