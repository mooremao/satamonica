#!/bin/sh

# Script to (re-)build dropbear
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

DROPBEAR_VERSION=dropbear-2014.65

# Untar the dropbear sources. We only do this when they do not yet exists.
cd dropbear/
if [ ! -d ${DROPBEAR_VERSION} ]
then
	tar xjf ${DROPBEAR_VERSION}.tar.bz2

	cd ${DROPBEAR_VERSION}/
	patch -p1 < ../0001-increase-maximum-number-of-ports.patch
	patch -p1 < ../0002-add-sbin-to-path.patch
else
	cd ${DROPBEAR_VERSION}/
fi

# Configure
CC=arm-arago-linux-gnueabi-gcc CXX=arm-arago-linux-gnueabi-g++ AR=arm-arago-linux-gnueabi-ar RANLIB=arm-arago-linux-gnueabi-ranlib ./configure --host=arm-arago-linux --prefix=`pwd`/../${DROPBEAR_VERSION}_arm-arago-linux-gnueabi/usr --mandir=`pwd`/../${DROPBEAR_VERSION}-man/usr

# Make
make PROGRAMS="dropbear dropbearkey scp"

# Make install
make PROGRAMS="dropbear dropbearkey scp" install

