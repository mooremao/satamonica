#!/bin/sh

# Script to (re-)build fatattr utilty
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

FATATTR_VERSION=fatattr-1.0.1

# Untar the sources. We only do this when they do not yet exists.
cd fatattr/
if [ ! -d ${FATATTR_VERSION} ]
then
	tar xzf ${FATATTR_VERSION}.tar.gz
	cd ${FATATTR_VERSION}/
else
	cd ${FATATTR_VERSION}/
fi

# Configure
CFLAGS="-O2" CC=arm-arago-linux-gnueabi-gcc AR=arm-arago-linux-gnueabi-ar STRIP=arm-arago-linux-gnueabi-strip RANLIB=arm-arago-linux-gnueabi-ranlib ./configure --host=arm-arago-linux

# Make
make

# Make install
make install INSTALLROOT=`pwd`/../${FATATTR_VERSION}_arm-arago-linux-gnueabi/

# Clean up unneeded files.
rm -rf `pwd`/../${FATATTR_VERSION}_arm-arago-linux-gnueabi/usr/share/
