#!/bin/sh

# Script to (re-)build systemd
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e

SYSTEMD_VERSION=systemd-216

cd systemd

DESTDIR=`pwd`/${SYSTEMD_VERSION}_arm-arago-linux-gnueabi
DESTDIR_DEV=${DESTDIR}-dev
DESTDIR_DOC=${DESTDIR}-doc
DESTDIR_EXTRA=${DESTDIR}-extra
DESTDIR_UNNEEDED=${DESTDIR}-unneeded

BASHCOMPLETIONDIR=/usr/share/bash-completion/completions
ZSHCOMPLETIONDIR=/usr/share/zsh/site-functions

KMOD_DIR=`pwd`/../kmod/kmod-18_arm-arago-linux-gnueabi
KMOD_DEV_DIR=${KMOD_DIR}-dev/

EXTRA_FILE_OR_DIRS=" \
	usr/bin/systemd-analyze \
	usr/bin/busctl \
	usr/bin/systemd-cgls \
	usr/bin/systemd-run \
	usr/bin/systemd-cgtop \
	usr/bin/systemd-delta \
	usr/bin/systemd-path \
	usr/bin/systemd-cat \
	lib/systemd/systemd-remount-fs \
	lib/systemd/systemd-bootchart"

#List of services that we don't need.
UNNEEDED_SYSTEM_SERVICES=" \
	systemd-update-utmp-runlevel.service \
	systemd-update-utmp.service \
	console-getty.service \
	getty@.service \
	autovt@.service \
	getty@tty1.service \
	serial-getty@.service \
	container-getty@.service \
	console-shell.service \
	debug-shell.service \
	quotaon.service \
	systemd-ask-password-console.service \
	systemd-ask-password-wall.service \
	systemd-journald.service \
	systemd-journal-catalog-update.service \
	systemd-modules-load.service \
	systemd-sysctl.service \
	systemd-update-done.service \
	systemd-remount-fs.service \
	systemd-shutdownd.service \
	systemd-remount-fs.service \
	systemd-udev-hwdb-update.service"

UNNEEDED_PATH_UNITS=" \
	systemd-ask-password-wall.path \
	systemd-ask-password-console.path"

UNNEEDED_MOUNT_UNITS=" \
	dev-hugepages.mount \
	sys-fs-fuse-connections.mount \
	sys-kernel-config.mount"

UNNEEDED_TARGET_UNITS=" \
	timers.target \
	paths.target \
	slices.target \
	swap.target"

UNNEEDED_SLICE_UNITS=" \
	system.slice \
	-.slice"

UNNEEDED_SOCKET_UNITS=" \
	systemd-shutdownd.socket"

UNNEEDED_FILES_OR_DIRS=" \
	bin/systemd-ask-password \
	bin/systemd-tty-ask-password-agent \
	bin/systemd-escape \
	bin/systemd-machine-id-setup \
	lib/udev/hwdb.d/ \
	lib/systemd/system-generators \
	lib/systemd/systemd-bus-proxyd \
	lib/systemd/systemd-update-utmp \
	lib/systemd/systemd-fsck \
	lib/systemd/systemd-socket-proxyd \
	lib/systemd/systemd-activate \
	lib/systemd/systemd-sysctl \
	lib/systemd/systemd-update-done \
	lib/systemd/systemd-reply-password \
	lib/systemd/systemd-ac-power \
	usr/bin/systemd-detect-virt \
	usr/lib/systemd/catalog/systemd.it.catalog \
	usr/lib/systemd/catalog/systemd.fr.catalog \
	usr/lib/systemd/catalog/systemd.ru.catalog \
	etc/systemd/bootchart.conf \
	var/log"

PATCHED_REPO_DIR="systemd-repo-patched"

if [ ! -d ${PATCHED_REPO_DIR} ]
then
	cp -a systemd-repo ${PATCHED_REPO_DIR}
	cd ${PATCHED_REPO_DIR}/

	# Apply patches
	patch -p1 < ../0001-syslog-socket-retry.patch
else
	cd ${PATCHED_REPO_DIR}/
fi


if [ "x${1}" = "xdistclean" ]
then
	make distclean
fi

#Run ./autogen.sh if ./configure does not exists yet.
if [ ! -f ./configure ]
then
	./autogen.sh
fi

#Configure when not already done.
if [ ! -f ./Makefile ]
then
	CC=arm-arago-linux-gnueabi-gcc CXX=arm-arago-linux-gnueabi-g++ AR=arm-arago-linux-gnueabi-ar RANLIB=arm-arago-linux-gnueabi-ranlib \
	PKG_CONFIG_PATH=${KMOD_DEV_DIR}/usr/lib/pkgconfig/ \
	LDFLAGS="-L${KMOD_DIR}/usr/lib/" \
	CFLAGS="-O2 -I${KMOD_DEV_DIR}/usr/include/" \
	./configure \
		--host=arm-arago-linux \
		--prefix=/usr \
		--libexecdir=/lib \
		--sysconfdir=/etc \
		--with-rootprefix= \
		--with-rootlibdir=/lib \
		--with-dbuspolicydir=/etc/dbus-1/system.d \
		--with-dbussessionservicedir=/usr/share/dbus-1/services \
		--with-dbussystemservicedir=/usr/share/dbus-1/system-services \
		--with-dbusinterfacedir=/usr/share/dbus-1/interfaces \
		--with-bashcompletiondir=${BASHCOMPLETIONDIR} \
		--with-zshcompletiondir=${ZSHCOMPLETIONDIR} \
		--with-sysvinit-path= \
		--with-sysvrcnd-path= \
		--with-firmware-path=/lib/firmware \
		--disable-nls \
		--disable-python-devel \
		--disable-dbus \
		--disable-blkid \
		--disable-seccomp \
		--disable-ima \
		--disable-selinux \
		--disable-chkconfig \
		--disable-apparmor \
		--disable-xz \
		--disable-lz4 \
		--disable-pam \
		--disable-acl \
		--disable-smack \
		--disable-gcrypt \
		--disable-audit \
		--disable-elfutils \
		--disable-libcryptsetup \
		--disable-qrencode \
		--disable-microhttpd \
		--disable-gnutls \
		--disable-libcurl \
		--disable-libidn \
		--disable-binfmt \
		--disable-vconsole \
		--disable-readahead \
		--enable-bootchart \
		--disable-quotacheck \
		--disable-tmpfiles \
		--disable-sysusers \
		--disable-firstboot \
		--disable-randomseed \
		--disable-backlight \
		--disable-rfkill \
		--disable-logind \
		--disable-machined \
		--disable-hostnamed \
		--disable-timedated \
		--disable-timesyncd \
		--disable-localed \
		--disable-coredump \
		--disable-polkit \
		--disable-networkd \
		--disable-resolved \
		--disable-efi \
		--disable-multi-seat-x \
		--disable-terminal \
		--disable-kdbus \
		--disable-myhostname \
		--disable-gudev \
		--disable-manpages \
		--disable-ldconfig \
		--enable-split-usr \
		--disable-tests \
		--enable-kmod
fi

#Compile it.
make

#Remove destdir and install systemd to destdir
rm -rf ${DESTDIR}
make install DESTDIR=${DESTDIR}

#Strip
find ${DESTDIR} -type f -executable -exec file  '{}' \; | grep 'ARM.*not stripped' | sed 's/:.*//g' | xargs arm-arago-linux-gnueabi-strip

#Remove dev destdir and (re)create it
rm -rf ${DESTDIR_DEV}
mkdir -p ${DESTDIR_DEV}/usr/include
mv ${DESTDIR}/usr/include/* ${DESTDIR_DEV}/usr/include

#Remove doc destdir and (re)create it
rm -rf ${DESTDIR_DOC}
mkdir -p ${DESTDIR_DOC}/usr/share/doc
mv ${DESTDIR}/usr/share/doc/* ${DESTDIR_DOC}/usr/share/doc

#Remove unneeded destdir and (re)create it
rm -rf ${DESTDIR_UNNEEDED}
mkdir -p ${DESTDIR_UNNEEDED}/${BASHCOMPLETIONDIR} ${DESTDIR_UNNEEDED}/${ZSHCOMPLETIONDIR}
mv ${DESTDIR}${BASHCOMPLETIONDIR}/* ${DESTDIR_UNNEEDED}${BASHCOMPLETIONDIR}
mv ${DESTDIR}${ZSHCOMPLETIONDIR}/* ${DESTDIR_UNNEEDED}/${ZSHCOMPLETIONDIR}

#Remove doc destdir and (re)create it
rm -rf ${DESTDIR_EXTRA}
mkdir -p ${DESTDIR_EXTRA}
for extrafile_or_dir in ${EXTRA_FILE_OR_DIRS}
do
	mkdir -p ${DESTDIR_EXTRA}/$(basename $extrafile_or_dir)
	mv ${DESTDIR}/$extrafile_or_dir ${DESTDIR_EXTRA}/$(basename $extrafile_or_dir)/
done

#Clean up services files we don't need
mkdir -p ${DESTDIR_UNNEEDED}/lib/systemd/system
for service in ${UNNEEDED_SYSTEM_SERVICES}
do
	mv ${DESTDIR}/lib/systemd/system/$service ${DESTDIR_UNNEEDED}/lib/systemd/system/ || true
	rm -rf ${DESTDIR}/lib/systemd/system/*.target.wants/$service
	rm -rf ${DESTDIR}/etc/systemd/system/*.target.wants/$service
done

#Clean up path unit files we don't need
for pathunit in ${UNNEEDED_PATH_UNITS}
do
	mv ${DESTDIR}/lib/systemd/system/$pathunit ${DESTDIR_UNNEEDED}/lib/systemd/system/
	rm -rf ${DESTDIR}/lib/systemd/system/*.target.wants/$pathunit
done

#Clean up mount unit files we don't need
for mountunit in ${UNNEEDED_MOUNT_UNITS}
do
	mv ${DESTDIR}/lib/systemd/system/$mountunit ${DESTDIR_UNNEEDED}/lib/systemd/system/
	rm -rf ${DESTDIR}/lib/systemd/system/*.target.wants/$mountunit
done

#Clean up target unit files we don't need
for targetunit in ${UNNEEDED_TARGET_UNITS}
do
	mv ${DESTDIR}/lib/systemd/system/$targetunit ${DESTDIR_UNNEEDED}/lib/systemd/system/
	rm -rf ${DESTDIR}/lib/systemd/system/*targetunit.wants
done

#Clean up slice unit file swe don't need
for sliceunit in ${UNNEEDED_SLICE_UNITS}
do
	mv ${DESTDIR}/lib/systemd/system/$sliceunit ${DESTDIR_UNNEEDED}/lib/systemd/system/
	rm -rf ${DESTDIR}/lib/systemd/system/*.target.wants/$sliceunit
done

for socketunit in ${UNNEEDED_SOCKET_UNITS}
do
	mv ${DESTDIR}/lib/systemd/system/$socketunit ${DESTDIR_UNNEEDED}/lib/systemd/system/
	rm -rf ${DESTDIR}/lib/systemd/system/*.target.wants/$socketunit
done

#Clean up other things
for file_or_dir in ${UNNEEDED_FILES_OR_DIRS}
do
	mkdir -p ${DESTDIR_UNNEEDED}/$(basename $file_or_dir)
	mv ${DESTDIR}/$file_or_dir ${DESTDIR_UNNEEDED}/$(basename $file_or_dir)/
done

