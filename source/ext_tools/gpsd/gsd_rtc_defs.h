/*
 * This file contains some RTC-related definitions for the GSD module
 * Copyright (c) 2014 TomTom B.V.
 */
#ifndef __GSD_RTC_DEFS__
#define __GSD_RTC_DEFS__

#define GSD_RTC_PIPE		"/tmp/gps_rtc"
#define GPS_EPOCH		315964800       /* 6 Jan 1980 00:00:00 UTC */
#define DEFAULT_RTC_DATE	1420070400      /* 1 Jan 2015 00:00:00 UTC */
#define DELTA_UTC		17		/* Difference between GPS and UTC time. NOTE: Valid till 31st December 2015 */

#endif
