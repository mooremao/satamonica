/**
 * \file gpsd_sgee.h
 * \brief Shared header to export SGEE data to external APIs to libttsystem
 */

#ifndef _GPSD_SGEE_H_
#define _GPSD_SGEE_H_

#define SGEE_INFO_FILE_GPS "/tmp/sgee_info_gps"
#define SGEE_INFO_FILE_GLO "/tmp/sgee_info_glo"

/** CLOCK_BOOTTIME was added in Linux 2.6.39 and backported to Longbeach project.
 *  If the glibc definition is missing we define it here to be able to access the kernel function.
 *  Use clock_gettime(CLOCK_BOOTTIME,..) to get monotonic time including time spent in suspend.
 */
#ifndef CLOCK_BOOTTIME
#define CLOCK_BOOTTIME 7
#endif

/**
 * \brief GSD SGEE status
 */
typedef enum {
	SGEE_NOQF,	/**< QuickFix data not loaded */
	SGEE_DOWNLOAD,	/**< QuickFix data dowloading */
	SGEE_ERR,	/**< QuickFix download failed */
	SGEE_DONE,	/**< QuickFix download complete */
	SGEE_NORTC,	/**< QuickFix download complete but RTC time invalid */
} sgee_status_t;

#endif /* _GPSD_SGEE_H_ */
