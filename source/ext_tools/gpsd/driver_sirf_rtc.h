/*
 * This is the header file for gpsd driver RTC for SiRF GPSes operating in binary mode.
 * Copyright (c) 2014 TomTom B.V.
 */
#ifndef __DRIVER_SIRF_RTC__
#define __DRIVER_SIRF_RTC__

int sirf_rtc_init(struct gps_device_t *session);
void sirf_rtc_deinit(struct gps_device_t *session);
void sirf_rtc_msg(struct gps_device_t * session, unsigned char *msg);
void sirf_rtc_poll(struct gps_device_t *session);
#endif //__DRIVER_SIRF_RTC__
