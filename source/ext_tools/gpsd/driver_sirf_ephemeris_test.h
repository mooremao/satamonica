/*
 * This is the header file for gpsd driver ephemeris tester for SiRF GPSes operating in binary mode.
 * Copyright (c) 2014 TomTom B.V.
 */
#ifndef __DRIVER_SIRF_EPHERMERIS_TEST__
#define __DRIVER_SIRF_EPHERMERIS_TEST__

int sirf_eph_test_init(struct gps_device_t *session);
void sirf_ephemeris_read_cmd_pipe(struct gps_device_t *session);
void sirf_eph_test_deinit(struct gps_device_t *session);
#endif //__DRIVER_SIRF_EPHERMERIS_TEST__
