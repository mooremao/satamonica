/*
 * This is the gpsd driver ephemeris helper for SiRF GPSes operating in binary mode.
 * Copyright (c) 2014 TomTom B.V.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "gpsd.h"
#include "gpsd_sgee.h"
#include "driver_sirf_ephemeris.h"
#include "driver_sirf_ephemeris_test.h"

#define MIN(a,b)  (((a) < (b)) ? (a) : (b))
#define MAX_FILENAME_LEN 256
#define NV_FILE_PREFIX "/mnt/userdata/gpshoststorage/"
#define MAX_READ_FAIL_COUNT 10
#define MAX_18S_RESTART_COUNT 3
#define BBRAM_HEADER_TX_PKT_SIZE 200
#define NVM_HEADER_SIZE (512)

#define SGEE_AGE_POLL_TIMEOUT 5

typedef enum {
	DW_IDLE,
	DW_DONE,
	DW_ERR,
} sgee_download_state_t;

static uint32_t seq[NUM_EPH_NAVSYS] = {1,1};
static eeph_state_t state[NUM_EPH_NAVSYS],next_state[NUM_EPH_NAVSYS];
static uint32_t sgee_age[NUM_EPH_NAVSYS] = {INVALID_SGEE_AGE, INVALID_SGEE_AGE};
static int32_t sgee_age_ts[NUM_EPH_NAVSYS] = {-1,-1};
static uint32_t sgee_prediction_interval[NUM_EPH_NAVSYS] = {0,0};
static uint32_t sgee_age_poll_ts[NUM_EPH_NAVSYS] = {0,0};
static sgee_download_state_t sgee_download_status[NUM_EPH_NAVSYS] = {DW_IDLE, DW_IDLE};
static int last_fix_status = STATUS_NO_FIX;

static char sgee_filename[NUM_EPH_NAVSYS][MAX_FILENAME_LEN];

static uint32_t is_sgee_updated[NUM_EPH_NAVSYS] = {0,0};
static uint32_t sgee_updating[NUM_EPH_NAVSYS] = {0,0};
static uint32_t file_size[NUM_EPH_NAVSYS] = {0,0};
static uint32_t	remain[NUM_EPH_NAVSYS] = {0,0};
static uint32_t	retry_count_18s[NUM_EPH_NAVSYS] = {0, 0};

static uint32_t time_second, last_ee_time_second = 0, retry_trigger = 0, retry_wait_time = 0;
static const uint32_t nvm_header_size_global = NVM_HEADER_SIZE;
static uint16_t sif_header_seq_no[NUM_EPH_NAVSYS] = {1,1};
static uint32_t sif_header_pending_read_size[NUM_EPH_NAVSYS] = {0,},sif_header_read_offset[NUM_EPH_NAVSYS] = {0,0};
static uint8_t sif_header_response_wait_ack[NUM_EPH_NAVSYS];

extern bool sirf_write(struct gps_device_t *session, unsigned char *msg);

#define SGEE_GPS_FILE_DOWNLOADED "/mnt/userdata/quickfix/sifgps.f2pxenc.ee.ok"
#define SGEE_GLO_FILE_DOWNLOADED "/mnt/userdata/quickfix/sifglo.f2pxenc.ee.ok"

#define SGEE_GPS_FILE_NG "/mnt/userdata/quickfix/sifgps.f2pxenc.ee.ng"
#define SGEE_GLO_FILE_NG "/mnt/userdata/quickfix/sifglo.f2pxenc.ee.ng"

static const char sgee_downloaded_done_file[NUM_EPH_NAVSYS][MAX_FILENAME_LEN] =
{
	SGEE_GPS_FILE_DOWNLOADED,
	SGEE_GLO_FILE_DOWNLOADED
};

static const char sgee_downloaded_ng_file[NUM_EPH_NAVSYS][MAX_FILENAME_LEN] =
{
	SGEE_GPS_FILE_NG,
	SGEE_GLO_FILE_NG
};

sgee_status_t sgee_state_map(eeph_state_t state, uint32_t age, sgee_download_state_t dw_stat)
{
	sgee_status_t status;

	switch(state) {
		/* Download finished or not yet started */
		case EPH_STATE_INIT:
		case EPH_STATE_SEND_SGEE_FILE_FINISH:
			/* The cause of the invalid age depends of the download status and the RTC vailidity */
			if (age == INVALID_SGEE_AGE)
				status = (dw_stat == DW_ERR) ? SGEE_ERR : (dw_stat == DW_DONE) ? SGEE_NORTC : SGEE_NOQF;
			else
				status = SGEE_DONE;
			break;

		/* Download in progress */
		case EPH_STATE_START_SGEE_DOWNLOAD_REQ:
		case EPH_STATE_START_SGEE_DOWNLOAD_ACK:
		case EPH_STATE_SET_SGEE_FILE_SIZE_REQ:
		case EPH_STATE_SET_SGEE_FILE_SIZE_ACK:
		case EPH_STATE_SEND_SGEE_FILE_REQ:
		case EPH_STATE_SEND_SGEE_FILE_ACK:
			status = SGEE_DOWNLOAD;
			break;

		/* Download failed */
		case EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY:
		case EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY:
		default:
			status = SGEE_ERR;
			break;
	}
	return status;
}

static void update_sgee_info_file(struct gps_device_t *session, eeph_navsys_t navsys)
{
	FILE *fp;
	int ret;
	char tmp_file[100];
	sgee_status_t status;

	/* Use a temporary file to avoid being read during writing */
	snprintf(tmp_file, sizeof(tmp_file), "%s.tmp", (navsys == EPH_NAVSYS_GPS) ? SGEE_INFO_FILE_GPS : SGEE_INFO_FILE_GLO);

	/* Create the file */
	fp = fopen(tmp_file,"wb");
	if (fp == NULL) {
		gpsd_report(session->context->debug, LOG_ERROR, "SIRF_EE: file %s open failed\n", tmp_file);
		return;
	}

	/* Map the internal status to an export value */
	status = sgee_state_map(state[navsys], sgee_age[navsys], sgee_download_status[navsys]);

	if (sgee_age[navsys] == INVALID_SGEE_AGE)
		ret = fprintf(fp, "state=%d;age=%d;ts=%d;pi=%u\n", status, -1, -1, 0);
	else
		ret = fprintf(fp, "state=%d;age=%d;ts=%d;pi=%u\n", status, (int32_t)sgee_age[navsys], (int32_t)sgee_age_ts[navsys], sgee_prediction_interval[navsys]);

	if (ret < 0) {
		gpsd_report(session->context->debug, LOG_ERROR, "SIRF_EE: file %s write failed\n", tmp_file);
		fclose(fp);
		remove(tmp_file);
		return;
	}
	fclose(fp);

	/* Rename the file with the final name */
	ret = rename(tmp_file, (navsys == EPH_NAVSYS_GPS) ? SGEE_INFO_FILE_GPS : SGEE_INFO_FILE_GLO);
	if (ret < 0) {
		gpsd_report(session->context->debug, LOG_ERROR, "SIRF_EE: failed renaming file %s: %s\n", tmp_file, strerror(errno));
		remove(tmp_file);
	}
};

bool send_osp (struct gps_device_t *session, uint8_t *data, uint32_t len)
{
	static uint8_t buffer[1024];

	buffer[0] = 0xA0;
	buffer[1] = 0xA2;
	buffer[2] = (len & 0xFF00) >> 8;
	buffer[3] = len & 0xFF;

	memcpy(&buffer[4], data, len);
	//checksum 2 byes filled by sirf_write();
	buffer[len+6] = 0xB0;
	buffer[len+7] = 0xB3;

	return sirf_write(session, buffer);
}

static eeph_nvm_file_type_t getNvmFileTypeByID(uint8_t nvm_id)
{
	eeph_nvm_file_type_t ret;

	switch(nvm_id) {
		case EPH_NVM_ID_GPS_SGEE:
		case EPH_NVM_ID_GLO_SGEE:
			ret = EPH_NVM_FILE_TYPE_SGEE;
			break;
		case EPH_NVM_ID_GPS_CGEE:
		case EPH_NVM_ID_GLO_CGEE:
			ret = EPH_NVM_FILE_TYPE_CGEE;
			break;
		case EPH_NVM_ID_GPS_BE:
		case EPH_NVM_ID_GLO_BE:
			ret = EPH_NVM_FILE_TYPE_BE;
			break;
		case EPH_NVM_ID_GPS_HEADER:
		case EPH_NVM_ID_GLO_HEADER:
			ret = EPH_NVM_FILE_TYPE_HEADER;
			break;
		default:
			ret = EPH_NVM_FILE_TYPE_INVALID;
			break;
		}
		return ret;
}

static uint32_t get_sgee_file_size(struct gps_device_t *session, eeph_navsys_t navsys)
{
	struct stat     sts;

	if ((stat( sgee_filename[navsys], &sts)) == -1)
	{
		gpsd_report(session->context->debug, LOG_INF,"SIRF_EE:file missing %s\n", sgee_filename[navsys]);
		return 0;
	}
	return sts.st_size;
}

static bool eclm_send_size (struct gps_device_t *session, eeph_navsys_t navsys, uint32_t f_size)
{
	uint8_t data[8] = {0xE8, 0x4a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	bool status;

	data[2] = navsys;
	data[3] = (f_size >> 24) & 0xFF;
	data[4] = (f_size >> 16) & 0xFF;
	data[5] = (f_size >>  8) & 0xFF;
	data[6] =  f_size & 0xFF;

	status = send_osp (session, data, sizeof (data));

	return status;
}

static void sgee_send_size(struct gps_device_t *session, eeph_navsys_t navsys)
{
	remain[navsys] = file_size[navsys];
	eclm_send_size (session, navsys, file_size[navsys]);
	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:navsys %d Size %d\n", navsys, file_size[navsys]);
}

static bool eclm_start (struct gps_device_t *session, eeph_navsys_t navsys)
{
	uint8_t data[5] = {0xE8, 0x49, 0x00, 0x00, 0x00};
	bool status;

	data[2] = navsys;
	status = send_osp (session, data, sizeof (data));
	if (status)
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM navsys %d start sent\n",navsys);
	else
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:ECLM navsys %d start FAILED\n",navsys);

	return status;
}

static bool set_ee_storage (struct gps_device_t *session, eeph_navsys_t navsys, uint8_t storage_type)
{
	uint8_t storage[] = {0xE8, 0x56, 0x00, 0x00, 0x00};
	bool status;

	storage[2]= navsys;
	storage[3] = storage_type;

	status = send_osp (session, storage, sizeof (storage));

	if(status)
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM storage sent navsys %d storage_type %d\n", navsys, storage_type);
	else
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:Can't send the command set_ee_storage \n");
	return status;
}

static int8_t eclm_send_packet (struct gps_device_t *session, eeph_navsys_t navsys, uint16_t seq, uint8_t *packet, uint16_t packet_len)
{
	uint8_t data[1024];
	int8_t status;

	data[0] = 0xE8;
	data[1] = 0x4b;
	/* Nav system */
	data[2] = navsys;
	/* msg sequence */
	data[3] = (seq >> 8) & 0xFF;
	data[4] =  seq & 0xFF;

	/* packet length */
	data[5] = (packet_len >> 8) & 0xFF;
	data[6] =  packet_len & 0xFF;

	/* SGEE data */
	memcpy (&data[7], packet, packet_len);
	data[packet_len + 7] = 0x00;

	/* Send to GSD5 */
	status = send_osp (session,data, packet_len + 8);

	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:PACKET SENT navsys %d seq %d packet_len %d \n",navsys,seq,packet_len);

	return status;
}

static int sgee_send_file(struct gps_device_t *session, eeph_navsys_t navsys, uint32_t seg_size, uint32_t offset, uint32_t sequence)
{
	uint8_t packet[1024];
	uint32_t read_bytes;
	FILE *fp = NULL;

	fp = fopen(sgee_filename[navsys],"rb");
	if(fp == NULL) {
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:file %s open failed\n",sgee_filename[navsys]);
		return 0;
	}
	fseek(fp,offset, SEEK_SET);
	read_bytes = fread (packet,1,seg_size,fp);
	fclose(fp);

	if(read_bytes != seg_size) {
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:Error reading SGEE file %s - read_bytes\n",sgee_filename[navsys]);
		return 0;
	}

	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:navsys=%d, file_size=%d, offset= %d, seq = %d\n",navsys, seg_size, offset, sequence);

	eclm_send_packet (session, navsys, sequence, packet, seg_size);

	return 1;
}

static int sgee_continue_send_file(struct gps_device_t *session, eeph_navsys_t navsys)
{
	uint32_t	read_num;

	if(remain[navsys] != 0 ) {
		read_num = MIN (400, remain[navsys] );
		sgee_send_file(session, navsys, read_num, file_size[navsys]-remain[navsys], seq[navsys]);
		seq[navsys]++;
		remain[navsys] -= read_num;
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:%s:navsys %d seq %d remain %d\n",__FUNCTION__,navsys,seq[navsys],remain[navsys] );
		return remain[navsys];
	} else {
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:navsys %d Send Sequence finishes\n",navsys);
		return 0;
	}
}

static int32_t send_EE_stored (struct gps_device_t *session, eeph_navsys_t navsys, uint8_t *osp_msg, uint8_t nvm_id, uint8_t blocks, uint32_t *offset, uint32_t *fileLength)
{
	uint8_t pData[400];
	uint8_t i;
	FILE *fp;
	uint16_t seqNum = osp_msg[4] << 8 | osp_msg[5];
	uint32_t accumilatedLen=0;
	uint32_t read_fail_count;
	bool returnStatus;
	uint32_t read_len=0;
	char nv_filename[256];
	eeph_nvm_file_type_t file_type;

	file_type = getNvmFileTypeByID(nvm_id);

	sprintf(nv_filename,"%snavsys_%d_nvm_id_%d.ee",NV_FILE_PREFIX,navsys,nvm_id);

	memset(pData, 0x00, sizeof(pData));

	pData[0] = 0xe8;//232
	pData[1] = 0x4e;//78
	pData[2] = navsys;
	pData[3] = (seqNum & 0xff00) >> 8;
	pData[4] = (seqNum & 0xff);
	pData[5] = nvm_id;
	pData[6] = blocks;

	fp =fopen(nv_filename,"rb");

	if(fp == NULL) {
		if(file_type == EPH_NVM_FILE_TYPE_BE)
			gpsd_report(session->context->debug, LOG_INF,"SIRF_EE: %s: can't open  file for read %s\n",nv_filename,strerror(errno));
		else
			gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE: %s: can't open  file for read %s\n",nv_filename,strerror(errno));
		return -1;
	}

	for(i=0;i<blocks;i++) {
		pData[7+i*6] = (fileLength[i] & 0x0000ff00) >> 8;
		pData[8+i*6] = (fileLength[i] & 0xff);

		pData[9+i*6] = (offset[i] & 0xff000000) >> 24;
		pData[10+i*6] = (offset[i] & 0x00ff0000)>> 16;
		pData[11+i*6] = (offset[i] & 0x0000ff00) >> 8;
		pData[12+i*6] = (offset[i] & 0x000000ff);
		fseek(fp,offset[i], SEEK_SET);

		read_len = 0;
		read_fail_count = 0;

		while(read_len<fileLength[i]&& read_fail_count < MAX_READ_FAIL_COUNT) {
			uint32_t len = fread ((uint8_t*)&pData[7+blocks*6+accumilatedLen+read_len],1,fileLength[i]-read_len,fp);
			if(len>0)
			{
				read_len += len;
				read_fail_count = 0;
			}
			else
			{
				read_fail_count++;
				usleep(200000);
			}
			if(len!= fileLength[i])
				gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:Error reading  file nvm_id %d - read_len %d len %d fileLength[i] %d offset %d\n",nvm_id,read_len,len,fileLength[i],7+blocks*6+accumilatedLen+read_len);
	  }

		if(read_fail_count >= MAX_READ_FAIL_COUNT) {
			gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:Error reading decoded SGEE file nvm_id %d- readlen %d fileLength[i] %d\n",nvm_id,read_len,fileLength[i]);
			fclose(fp);
			return -1;
		}

		accumilatedLen += fileLength[i];
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM Offset[%d]:%d Length[%d]:%d\n",i,offset[i],i,fileLength[i]);
	}

	fclose(fp);

	pData[accumilatedLen+blocks*6+7] = 0;
	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM Going to send %d bytes\n", accumilatedLen+blocks*6+8);
	usleep(100000);

	returnStatus = send_osp (session, pData, accumilatedLen+blocks*6+8);

	if (returnStatus) {
			gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM Answered Req\n");
		return 0;
	}
	else
		return -1;
}

static void sirf_ephemeris_handle_DL_end( struct gps_device_t *session, eeph_navsys_t navsys)
{
	file_size[navsys] = 0;
	seq[navsys] = 1;
	remain[navsys] = 0;
	is_sgee_updated[navsys] = 1;
	sgee_updating[navsys] = 0;

	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:%s: navsys %d state[navsys] %d \n", __FUNCTION__, navsys, state[navsys]);

	switch(state[navsys])
	{
		case EPH_STATE_SEND_SGEE_FILE_FINISH:
			retry_count_18s[navsys] = 0;
			unlink(sgee_downloaded_done_file[navsys]);
			if(rename(sgee_filename[navsys],sgee_downloaded_done_file[navsys]) != 0) {
				gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:file rename error %s %s error %s\n",sgee_filename[navsys],sgee_downloaded_done_file[navsys],strerror(errno));
			}
			break;
		case EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY:
			retry_count_18s[navsys] = 0;
			unlink(sgee_downloaded_ng_file[navsys]);
			if(rename(sgee_filename[navsys],sgee_downloaded_ng_file[navsys]) != 0) {
				gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:file rename error %s %s error %s\n",sgee_filename[navsys],sgee_downloaded_ng_file[navsys],strerror(errno));
			}
			break;
		case EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY:
			if(retry_count_18s[navsys] < MAX_18S_RESTART_COUNT ) {
				retry_count_18s[navsys]++;
			} else {
				next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY;
			}
			break;
		default:
			break;
	}
}

static uint32_t sirf_ephemeris_sgee_updating( eeph_navsys_t navsys)
{
	return sgee_updating[navsys];
}

static uint32_t sirf_ephemeris_is_sgee_updated( eeph_navsys_t navsys)
{
	return is_sgee_updated[navsys];
}

/* Force not update the GPS/GLO header to test SGEE update */
static void force_update_sgee (struct gps_device_t *session,uint8_t sid_ack, uint8_t ack, uint8_t nav_system)
{
	uint8_t data[] = {0xE8, 0x4F, 0x00, 0x38, 0x58, 0x01, 0x00, 0x00};

	data[2] = nav_system;
	data[4] = sid_ack;
	data[5] = ack;

	send_osp (session, data, sizeof(data));
}

static int handle_ee_pkt (struct gps_device_t *session, eeph_navsys_t navsys, uint8_t *buf, uint8_t nvm_id)
{
	uint16_t size,seq_id;
	uint32_t offset;
	uint8_t*zero_char = NULL;
	uint32_t write_bytes;
	struct stat sts;
	FILE *fp = NULL;
	char nv_filename[256];

	sprintf(nv_filename,"%snavsys_%d_nvm_id_%d.ee",NV_FILE_PREFIX,navsys,nvm_id);

	size = (buf[4] << 8) | buf[5];
	offset =((buf[6] << 24)|(buf[7] << 16) | (buf[8] << 8) | buf[9]);
	seq_id = (buf[10]<<8) | buf[11];

	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:Write navsys %d nvm_id %d offset %d size %d seq_id %d \n", navsys, nvm_id, offset, size, seq_id);


	fp = fopen(nv_filename,"r+b");
	if(fp == NULL)
		fp = fopen(nv_filename,"wb");

	if( fp == NULL ) {
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:file open error %s error %s\n",nv_filename,strerror(errno));
		return 0;
	}

	fstat(fileno(fp), &sts);

	if(sts.st_size>=offset) {
		fseek(fp,offset, SEEK_SET);
	}
	else{
		fseek(fp,0, SEEK_END);
		zero_char = calloc(offset-sts.st_size,sizeof(uint8_t));
		if(zero_char == NULL)
		{
			gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:%s:error allocate zero_char\n",__FUNCTION__);
			return 0;
		}
		fwrite ((uint8_t*)zero_char,1,offset-sts.st_size,fp);
		free(zero_char);
	}

	write_bytes = fwrite ((uint8_t*)&buf[12],1,size,fp);
	fclose(fp);

	if(write_bytes != size)
	{
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:Error write SGEE file - write_bytes %d\n",write_bytes);
		return 0;
	}
	return 1;
}

static void sirf_ephemeris_handle_sif_request_header_file_content_req(struct gps_device_t *session,eeph_navsys_t navsys,uint32_t first_seq)
{
	uint8_t data[BBRAM_HEADER_TX_PKT_SIZE+20] = {0};
	int32_t returnStatus = -1,read_len,read_fail_count;
	uint8_t count = 0;
	uint16_t readSize = 0;
	uint8_t nvm_id;
	FILE *fp = NULL;
	char nv_filename[256];

	/* map NVM file name */
	if(navsys == EPH_NAVSYS_GPS) {
		nvm_id = EPH_NVM_ID_GPS_HEADER;
	}
	else if(navsys == EPH_NAVSYS_GLO) {
		nvm_id = EPH_NVM_ID_GLO_HEADER;
	}
	else {
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:%s:GOT wrong navsys %d\n",__FUNCTION__,navsys);
		force_update_sgee (session, 88, 1, navsys);
		return;
	}

	sif_header_response_wait_ack[navsys] = 0;

	if(first_seq == 1) {
		sif_header_seq_no[navsys] = 1;
		sif_header_pending_read_size[navsys] = NVM_HEADER_SIZE;
		sif_header_read_offset[navsys] = 0;
	}

	sprintf(nv_filename,"%snavsys_%d_nvm_id_%d.ee",NV_FILE_PREFIX,navsys,nvm_id);

	fp =fopen(nv_filename,"rb");

	if(fp == NULL) { /* use LOG_WARN because header is requested after GPS power cycling /factory_reset. This is not error */
		gpsd_report(session->context->debug, LOG_WARN,"SIRF_EE: %s: can't open  file for read %s\n",nv_filename,strerror(errno));
		goto error;
	}

	data[0] = 0xe8; /*232 */
	data[1] = 0x4e; /* 78 */
	data[2] = navsys;

	data[3] = (uint8_t)((sif_header_seq_no[navsys] & 0xff00) >> 8);
	data[4] = (uint8_t)(sif_header_seq_no[navsys] & 0x00ff);
	data[5] = nvm_id;
	data[6] = 1;

	data[9] = (uint8_t)((sif_header_read_offset[navsys] & 0xff000000) >> 24);
	data[10] = (uint8_t)((sif_header_read_offset[navsys] & 0x00ff0000)>> 16);
	data[11] = (uint8_t)((sif_header_read_offset[navsys] & 0x0000ff00) >> 8);
	data[12] = (uint8_t)(sif_header_read_offset[navsys] & 0x000000ff);

	if(BBRAM_HEADER_TX_PKT_SIZE <= sif_header_pending_read_size[navsys]) {
		readSize = BBRAM_HEADER_TX_PKT_SIZE;
	} else {
		readSize = (uint16_t)sif_header_pending_read_size[navsys];
	}

	data[7] = (uint8_t)((readSize & 0x0000ff00) >> 8);
	data[8] = (uint8_t)(readSize & 0xff);

	fseek(fp,sif_header_read_offset[navsys], SEEK_SET);

	read_len = 0;
	read_fail_count = 0;

	if((readSize+14) > sizeof(data)) {
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE: %s:buf insufficient readSize %d, buffer size %d!!\n",__FUNCTION__,readSize,sizeof(data));
		goto error;
	}

	while(read_len<readSize&& read_fail_count < MAX_READ_FAIL_COUNT) {
		uint32_t len = fread ((uint8_t*)&data[13+read_len],1,readSize-read_len,fp);
		if(len>0) {
			read_len += len;
			read_fail_count = 0;
		} else {
			read_fail_count++;
			usleep(200000);
		}
	}

	if(read_fail_count)
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE: %s: navsys %d read_fail_count %d\n",__FUNCTION__,navsys,read_fail_count );

	data[13+readSize] = 0;

	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE: %s: navsys %d seq %d, offset %d, size %d pending %d\n",__FUNCTION__,navsys,sif_header_seq_no[navsys],sif_header_read_offset[navsys],readSize,sif_header_pending_read_size[navsys] );

	returnStatus = send_osp (session, data, readSize+14);

	if(returnStatus == 0) {
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE: %s:send osp failed \n",__FUNCTION__);
		// if happened, retry??
	} else {
		sif_header_seq_no[navsys]++;
		sif_header_pending_read_size[navsys] -= readSize;
		sif_header_read_offset[navsys] += readSize;
		sif_header_response_wait_ack[navsys] = 1;
	}
	fclose(fp);
	return;
error:
	force_update_sgee (session, 88, 1, navsys);
	if(fp!=NULL)
		fclose(fp);
}

static void sirf_ephemeris_handle_sif_ack_nack(struct gps_device_t *session,eeph_navsys_t navsys,uint8_t *buf)
{
	uint8_t ack_nack_reason;

	switch (buf[4]) //Ack Message ID
	{
		case 0x49://73, response for eclm_start
			if(buf[5] == 0x00) {
				next_state[navsys] = EPH_STATE_START_SGEE_DOWNLOAD_ACK;
			} else {
				retry_wait_time = SGEE_GENERAL_RETRY_TIME_SEC;
				next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY;
				gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:ECLM> send start failed ack= %d get ACK reason = %d\n",buf[5],buf[6]);
			}
			break;
		case 0x4a://74, response for eclm_send_size
			if(buf[5] == 0x00) {
				next_state[navsys] = EPH_STATE_SET_SGEE_FILE_SIZE_ACK;
			} else {
				retry_wait_time = SGEE_GENERAL_RETRY_TIME_SEC;
				next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY;
				sgee_download_status[navsys] = DW_ERR;
				gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:ECLM> send size failed to get ACK reason = %d\n",buf[6]);
			}
			break;
		case 0x4b://75, response for GNSS sif packet data
			if(sgee_updating[navsys]) {
				if(buf[5] == 0x00) {
					next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ACK;
					gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM> EPH_STATE_SEND_SGEE_FILE_ACK navsys = %d seq = %d\n",navsys,seq[navsys]);
				} else {
					ack_nack_reason = buf[6];
					gpsd_report(session->context->debug, LOG_WARN,"SIRF_EE:ECLM>Packet sent failed to get ACK - navsys = %d ,reason = %d\n",navsys, ack_nack_reason);

					switch(ack_nack_reason)
					{
						case EPH_ACK_NACK_REASON_SIF_AID_IN_PROGRESS:
							retry_wait_time = SGEE_GENERAL_RETRY_TIME_SEC;
							next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY;
							sgee_download_status[navsys] = DW_ERR;
							break;
						case EPH_ACK_NACK_REASON_SIF_AID_NOT_STARTED:
							retry_wait_time = RESCHEDULE_INTERVAL_RETRY_EXHAUST;
							next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY;
							sgee_download_status[navsys] = DW_ERR;
							break;
						case EPH_ACK_NACK_REASON_NONEWFILE:
							gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM>SGEE already there navsys %d\n",navsys);
							next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_FINISH;
							sgee_download_status[navsys] = DW_DONE;
							break;

						case EPH_ACK_NACK_REASON_INSUFF_SPACE:
						case EPH_ACK_NACK_REASON_INVALID_PACKET_LEN:
						case EPH_ACK_NACK_REASON_CURRUPT_DL_FILE:
						case EPH_ACK_NACK_REASON_NONEWFILE_BUT_MOREDATA:
							next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY;
							sgee_download_status[navsys] = DW_ERR;
							break;

						case EPH_ACK_NACK_REASON_RECV_OUT_SEQ:
						case EPH_ACK_NACK_REASON_GENERAL_DL_FAILURE:
							retry_wait_time = SGEE_GENERAL_RETRY_TIME_SEC;
							next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY;
							sgee_download_status[navsys] = DW_ERR;
							break;
						default:
							break;
					}
				}
			}
			break;
		case 0x4e://78, Host Storage File Content Response
			if(buf[5] == 0x00) { //ACK
				if(sif_header_response_wait_ack[navsys]) {
					if(sif_header_pending_read_size[navsys])
						sirf_ephemeris_handle_sif_request_header_file_content_req(session,navsys,0);
				} else {
					gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:GOT unexpaced ACK respose for sif_header_reponse,navsys %d\n",navsys);
				}
			} else { //NACK
				gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE:GOT NACK respose for sif_header_reponse,navsys %d reason %d\n",navsys,buf[6]);
			}
			break;
		case 0x4d://77, SGEE Age Request Response
			if(buf[5] == 0x01) { //NACK
				/* NACK is received when there is quickfix data in GSD but the fix has never been obtained (i.e. RTC invalid) */
				gpsd_report(session->context->debug, LOG_WARN, "SIRF_EE:GOT NACK respose for SGEE age request, navsys %d reason %d\n", navsys, buf[6]);

				/* Clear the poll request timestamp */
				sgee_age_poll_ts[navsys] = 0;

				/* Invalidate the info */
				sgee_age_ts[navsys] = -1;
				sgee_age[navsys] = INVALID_SGEE_AGE;
				sgee_prediction_interval[navsys] = 0;

				/* Update the info file */
				update_sgee_info_file(session, navsys);
			} else {
				/* ACK is not supposed to be received, as the expected positive response is the age info message (MID 56,82) */
				gpsd_report(session->context->debug, LOG_ERROR, "SIRF_EE:GOT unexpected ACK respose for SGEE age request, navsys %d reason %d\n", navsys, buf[6]);
			}
			break;
		default:
			gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:GOT unhandled osp_msg[8] %d\n",buf[4]);
			break;
	}

}

static void sirf_ephemeris_handle_sif_request_file_content_req(struct gps_device_t *session,eeph_navsys_t navsys,uint8_t *buf)
{
	/* send decoded GPS file back to GSD5 */
	static uint32_t size[32], offset[32];
	uint8_t block = buf[6];
	uint16_t seq_no = (buf[4] << 8) | buf[5];
	uint8_t i,nvm_id;

	nvm_id = buf[3];
	memset (size, 0, 32);
	memset (offset, 0, 32);

	for (i=0; i < block; i++) {
		size[i] = (buf[7+i*6] << 8)|buf[8+i*6];
		offset[i] = (buf[9+i*6]<< 24)|(buf[10+i*6]<< 16)|(buf[11+i*6]<< 8)|buf[12+i*6];
	}

	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:ECLM navsys %d NvmID:%d, blocks:%d, seq = %d\n",navsys, nvm_id, block, seq_no);

	if (( nvm_id > EPH_NVM_ID_NULL && nvm_id < NUM_EPH_NVM_ID ) && (seq_no == 1)) {
		if(send_EE_stored (session, navsys, buf, nvm_id, block, offset, size)<0) {
			force_update_sgee (session, 86, 1, navsys);
		}
	} else {
		force_update_sgee (session, 86, 1, navsys);
	}
}

bool sirf_ephemeris_init(struct gps_device_t *session, eeph_navsys_t navsys)
{
	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:sirf_ephemeris_init navsys %d\n",navsys);

	state[navsys] = EPH_STATE_INIT;
	next_state[navsys] = EPH_STATE_INIT;

	/* Request the age of the current quickfix data */
	poll_sgee_age(session, navsys);

	return set_ee_storage(session, navsys,EPH_EE_STORAGE_INTERNAL|EPH_EE_STORAGE_TYPE_HOST);
}

bool sirf_ephemeris_start(struct gps_device_t *session, eeph_navsys_t navsys, const char *filename)
{
	bool ret = false;

	if(sgee_updating[navsys])
		return false;

	strncpy(sgee_filename[navsys], filename, MAX_FILENAME_LEN-1);

	file_size[navsys] = get_sgee_file_size(session,navsys);

	if(file_size[navsys]>0) {
		if(eclm_start(session,navsys)) {
			is_sgee_updated[navsys] = 0;
			sgee_updating[navsys] = 1;
			next_state[navsys] = EPH_STATE_START_SGEE_DOWNLOAD_REQ;
			sgee_download_status[navsys] = DW_IDLE;
			ret = true;
		}
	} else {
		sgee_filename[navsys][0] = 0;
		file_size[navsys] = 0;
	}

	return ret;
}

void sirf_ephemeris_handle_eclm(struct gps_device_t *session, uint8_t *buf, uint32_t len)
{
	uint8_t sid;
	uint8_t navsys;
	uint32_t wait_time, start_sgee_download;
	uint8_t nvm_id;
	uint8_t activity;
	uint16_t indication,sifState;

	if(len<2)
		return;

	sid = buf[1];
	navsys = buf[2];

	switch(sid)
	{
		case 80: // GNSS SIF Ack/Nack from GSD5
		{
			if(len<7)
				return;
			sirf_ephemeris_handle_sif_ack_nack(session,navsys,buf);
		}
		break;

		case 82: //response for polling ee age
		{
			struct timespec tp;

			/* Clear the poll request timestamp */
			sgee_age_poll_ts[navsys] = 0;

			if(len<11)
				return;
			sgee_age[navsys] = (buf[3] << 24) | (buf[4] << 16) | (buf[5] << 8) | buf[6];
			sgee_prediction_interval[navsys] = (buf[7] << 24) | (buf[8] << 16) | (buf[9] << 8) | buf[10];
			if(sgee_age[navsys] != INVALID_SGEE_AGE) {
				gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:navsys %d SGEE age %d interval %d\n", navsys, sgee_age[navsys], sgee_prediction_interval[navsys]);
			} else {
				gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:navsys %d SGEE not configured, age null\n", navsys);
			}

			/* Get the timestap of the received age */
			if (clock_gettime(CLOCK_BOOTTIME, &tp) < 0) {
				gpsd_report(session->context->debug, LOG_WARN,"SIRF_EE: unable to determine CLOCK_BOOTTIME: %s\n", strerror(errno));
				/* Without the timestamp, age is useless -> invalidate it */
				sgee_age_ts[navsys] = -1;
				sgee_age[navsys] = INVALID_SGEE_AGE;
				sgee_prediction_interval[navsys] = 0;
			} else {
				sgee_age_ts[navsys] = (int32_t)tp.tv_sec;
			}

			/* Update the info file with the new age data */
			update_sgee_info_file(session, navsys);
		}
		break;

		case 83: //GSD5 initiate ee download
		{
				if(len<8)
					return;
				start_sgee_download = buf[3];
				wait_time = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7];
				gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:GPS init download nav_sys %d,start %d, wait_time %d\n",navsys,start_sgee_download,wait_time);
		}
		break;

		case 85: //GSD5 requests host to save NVM files
		{
			if(len<13)
				return;
			nvm_id = buf[3];
			if( nvm_id > EPH_NVM_ID_NULL && nvm_id < NUM_EPH_NVM_ID )
				handle_ee_pkt (session, navsys,buf,nvm_id);
			else
				gpsd_report(session->context->debug, LOG_ERROR,"SIRF_EE: Invalid nvm_id: %d\n", nvm_id);

			/**
			  * LBP-1499. CSR issue 56932.
			  *
			  * "NOT to send ACK/NACK message in response to MID 0x38,SID 0x55 when NVM ID is set as 5 or 10 (GPS header or GLO Header)"
			  */
			if (nvm_id != EPH_NVM_ID_GPS_HEADER && nvm_id != EPH_NVM_ID_GLO_HEADER)
				force_update_sgee(session, 85, 0, navsys);
		}
		break;

		case 86: //GSD5 requests host to read NVM files and send it back
			if(len<13)
				return;
			sirf_ephemeris_handle_sif_request_file_content_req(session,navsys,buf);
		break;

		case 87: //obsolete now according to https://www.csrsupport.com/issue.php?iid=37211. Use 56,85 for all NVMs including header.
		{
			gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:GNSS store  EE Header Request.\n");
			force_update_sgee (session, 87, 1, navsys);//NACK
		}
		break;

		case 88: //get host header request
		{
			gpsd_report(session->context->debug, LOG_PROG, "SIRF_EE:GNSS Fetch EE Header Request.\n");
			if(sif_header_response_wait_ack[navsys])
				gpsd_report(session->context->debug, LOG_ERROR, "SIRF_EE:GNSS Fetch EE Header Request already in progress\n");
			sirf_ephemeris_handle_sif_request_header_file_content_req(session,navsys,1);
		}
		break;

		case 90://SIF Status
			gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:SIF Status nav_sys %d enabled %d\n",navsys,buf[3]);
		break;

		case 95://SIF activity report
			{
				if(len<8)
					return;
				activity = buf[3];
				indication = (buf[4]<<8)|buf[5];
				sifState = (buf[6]<<8)|buf[7];
				gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:SIF activity %d indication %d sifState %d\n",activity,indication,sifState);

				/* If a download completion is reported, request the age of the downloaded data */
				if ((activity == 1) && (indication == 1)) {
					sgee_download_status[navsys] = DW_DONE;
					poll_sgee_age(session, navsys);
				}
			}
			break;
		default:
			gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:GOT unhandled SID %d\n",sid);
		break;
	}
}

static void sirf_ephemeris_handle_state_change( struct gps_device_t *session,eeph_navsys_t navsys)
{

	switch(state[navsys])
	{
		case EPH_STATE_INIT:
			break;

		case EPH_STATE_START_SGEE_DOWNLOAD_REQ:
			break;

		case EPH_STATE_START_SGEE_DOWNLOAD_ACK:
			sgee_send_size(session, navsys);
			next_state[navsys] = EPH_STATE_SET_SGEE_FILE_SIZE_REQ;
			break;

		case EPH_STATE_SET_SGEE_FILE_SIZE_REQ:
			break;

		case EPH_STATE_SET_SGEE_FILE_SIZE_ACK:
			seq[navsys] = 1;
			sgee_continue_send_file(session, navsys);
			next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_REQ;
			break;

		case EPH_STATE_SEND_SGEE_FILE_REQ:
			break;

		case EPH_STATE_SEND_SGEE_FILE_ACK:
			if( remain[navsys] == 0 ) {
				next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_FINISH;
				sirf_ephemeris_handle_DL_end(session, navsys);
			} else {
				sgee_continue_send_file(session, navsys);
				next_state[navsys] = EPH_STATE_SEND_SGEE_FILE_REQ;
			}
			break;
		case EPH_STATE_SEND_SGEE_FILE_FINISH:
			sirf_ephemeris_handle_DL_end(session, navsys);
			next_state[navsys] = EPH_STATE_INIT;
			break;
		case EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY:
			sirf_ephemeris_handle_DL_end(session, navsys);
			next_state[navsys] = EPH_STATE_INIT;
			break;
		case EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY:
			{
				sirf_ephemeris_handle_DL_end(session, navsys);
				if (next_state[navsys] != EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY) {
					next_state[navsys] = EPH_STATE_INIT;
					/* wait for next trigger from sirf_ephemeris_state_machine */
					retry_trigger = time_second + retry_wait_time;
					gpsd_report(session->context->debug, LOG_WARN,"SIRF_EE:%s:init %d second retry\n",__FUNCTION__,retry_wait_time);
				} else {
					gpsd_report(session->context->debug, LOG_WARN,"SIRF_EE:%s:giving up, no more retries\n",__FUNCTION__);
				}
			}
			break;
		default:
			break;
		}

	if(state[navsys] != next_state[navsys]) {
		gpsd_report(session->context->debug, LOG_WARN,"SIRF_EE:%s:navsys %d state change %d-->%d\n",__FUNCTION__,navsys,state[navsys],next_state[navsys]);
		state[navsys] = next_state[navsys];

		/* Update the info file with the new state */
		update_sgee_info_file(session, navsys);
	}
}

void sirf_ephemeris_state_machine(struct gps_device_t *session)
{
	uint32_t tick_change = 0;
	uint32_t fix_change = 0;
	uint32_t do_start = 0;

	time_second = (uint32_t) timestamp();

	/* detect SGEE every 18s because CSR specifies 18s retry for failure  */
	if( time_second != last_ee_time_second ) {
	    	last_ee_time_second = time_second;
	    	tick_change = 1;
	}

	/* Detect a fix status change */
	if(session->gpsdata.status != last_fix_status) {
	    	last_fix_status = session->gpsdata.status;
	    	fix_change = 1;
	}

	if( retry_trigger !=0 && time_second == retry_trigger  ) {
		do_start = 1;
		retry_trigger = 0;
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:%s:starting 18 retry\n",__FUNCTION__);
	}

	if(tick_change == 1 && (time_second %SGEE_DETECT_TIME_SEC) == 0 ) {
		do_start = 1;
	}

	/* try to start update for every 18s */
	if(do_start ) {
		if(sirf_ephemeris_sgee_updating(EPH_NAVSYS_GPS) == 0 ) {
				if(sirf_ephemeris_start(session,EPH_NAVSYS_GPS,SGEE_GPS_FILE)== false )
					gpsd_report(session->context->debug, LOG_PROG, "SIRF_EE:%6.1f SGEE GPS doesn't start\n",timestamp());
		}
#ifdef SIRF_EE_GLO
		if(sirf_ephemeris_sgee_updating(EPH_NAVSYS_GLO) == 0 ) {
			if(sirf_ephemeris_start(session,EPH_NAVSYS_GLO,SGEE_GLO_FILE)== false )
				gpsd_report(session->context->debug, LOG_PROG, "SIRF_EE:%6.1f SGEE GLO doesn't start\n",timestamp());
		}
#endif
	}

	sirf_ephemeris_handle_state_change(session,EPH_NAVSYS_GPS);
#ifdef SIRF_EE_GLO
	sirf_ephemeris_handle_state_change(session,EPH_NAVSYS_GLO);
#endif

	/* Request an update of the SGEE age each time the fix is obtained */
	if (fix_change && (session->gpsdata.status != STATUS_NO_FIX)) {
		gpsd_report(session->context->debug, LOG_PROG, "SIRF_EE: detected fix, status %d\n", session->gpsdata.status);
		poll_sgee_age(session, EPH_NAVSYS_GPS);
#ifdef SIRF_EE_GLO
		poll_sgee_age(session, EPH_NAVSYS_GLO);
#endif
	} else {
		/* Workaround in case the GSD module doesn't respond to an age poll request */
		if ((sgee_age_poll_ts[EPH_NAVSYS_GPS] != 0) && ((time_second-sgee_age_poll_ts[EPH_NAVSYS_GPS]) > SGEE_AGE_POLL_TIMEOUT)) {
			gpsd_report(session->context->debug, LOG_ERROR, "SIRF_EE:timeout receiving sgee age reply for navsys %d, retrying..\n", EPH_NAVSYS_GPS);
			poll_sgee_age(session, EPH_NAVSYS_GPS);
		}
#ifdef SIRF_EE_GLO
		if ((sgee_age_poll_ts[EPH_NAVSYS_GLO] != 0) && ((time_second-sgee_age_poll_ts[EPH_NAVSYS_GLO]) > SGEE_AGE_POLL_TIMEOUT)) {
			gpsd_report(session->context->debug, LOG_ERROR, "SIRF_EE:timeout receiving sgee age reply for navsys %d, retrying..\n", EPH_NAVSYS_GLO);
			poll_sgee_age(session, EPH_NAVSYS_GLO);
		}
#endif
	}
	sirf_ephemeris_read_cmd_pipe(session);
}

/* Poll SGEE age */
bool poll_sgee_age(struct gps_device_t *session, uint8_t navsys)
{
	uint8_t data[5] = {0xE8, 0x4d, 0x00, 0x00, 0x00};	/* {MID232, SID77, navsys, SatId(GPS)/ChanId(GLO), RESERVED} */
	bool status;

	/* Record the timestamp of the poll request */
	sgee_age_poll_ts[navsys] = (uint32_t)timestamp();

	data[2] = navsys;
	data[3] = (navsys ==  EPH_NAVSYS_GPS) ? 0x01 : 0x46;
	status = send_osp (session, data, sizeof (data));

	if (status)
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:Poll sgee age sent %d\n", navsys);

	return status;
}
