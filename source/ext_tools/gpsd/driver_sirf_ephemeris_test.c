/*
 * This is the gpsd driver ephemeris helper for SiRF GPSes operating in binary mode.
 * Copyright (c) 2014 TomTom B.V.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "gpsd.h"

#define GPS_TEST_CMD_PIPE "/tmp/gps_cmd"

static int pipe_cmd_fd =-1;

extern bool send_osp (struct gps_device_t *session, uint8_t *data, uint32_t len);

/* Cold start */
static bool  cold_start (struct gps_device_t *session)
{
	uint8_t buf[] = {0x80,0x00,0x00,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 \
					,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x0C ,0x04};
	bool status = send_osp (session, buf, sizeof(buf));

	if (status )
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:COLD START SENT\n");
	return status;
}

/* Warm start only, no initialized info */
static bool warm_start (struct gps_device_t *session, bool navlib_en)
{
	uint8_t buf[25];
	bool status;

	memset(buf, 0x00, sizeof(buf));
	buf[0] = 0x80;
	buf[23] = 12;

	if(navlib_en)
		buf[24] = 0x12;
	else
		buf[24] = 0x02;

	status = send_osp (session, buf, sizeof(buf));

	if (status)
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:WARM START NO INIT\n");

	return status;
}

/* Hot start */
static bool hot_start (struct gps_device_t *session)
{
	uint8_t buf[25];
	bool status;

	memset(buf, 0x00, sizeof(buf));
	buf[0] = 0x80;

	buf[23] = 12;
	buf[24] = 0x00;

	status = send_osp (session, buf, sizeof(buf));

	if (status)
		gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:HOT START NO INIT\n");

	return status;
}

void sirf_ephemeris_read_cmd_pipe(struct gps_device_t *session)
{
	char cmd;

	if(pipe_cmd_fd <0)
		return;

	if(read( pipe_cmd_fd, &cmd,sizeof(cmd))>0) {
		if(cmd=='w') {
			warm_start(session,false);
		} else if(cmd == 'W' ) {
			warm_start(session,true);
		} else if(cmd == 'c' ) {
			cold_start(session);
		} else if(cmd == 'a') {
			poll_sgee_age(session,0);
		} else if(cmd == 'A') {
			poll_sgee_age(session,1);
		} else if(cmd == 'r') {
			hot_start(session);
		}
	}
}

int sirf_eph_test_init(struct gps_device_t *session)
{
	unlink(GPS_TEST_CMD_PIPE);
	mkfifo(GPS_TEST_CMD_PIPE,S_IRUSR| S_IWUSR);

	if (pipe_cmd_fd < 0) {
		pipe_cmd_fd = open(GPS_TEST_CMD_PIPE, O_RDONLY | O_NDELAY | O_NONBLOCK);

    if (pipe_cmd_fd < 0) {
    	gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:PIPE: read open cmd device fail (%d)\n", pipe_cmd_fd);
      return -1;
     }
     gpsd_report(session->context->debug, LOG_PROG,"SIRF_EE:PIPE created\n");
  }
  return 0;
}

void sirf_eph_test_deinit(struct gps_device_t *session)
{
	if(pipe_cmd_fd>0)
		close(pipe_cmd_fd);
	pipe_cmd_fd = -1;
	unlink(GPS_TEST_CMD_PIPE);
}
