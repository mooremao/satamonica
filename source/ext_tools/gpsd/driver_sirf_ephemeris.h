/*
 * This is the header file for gpsd driver ephemeris helper for SiRF GPSes operating in binary mode.
 * Copyright (c) 2014 TomTom B.V.
 */
#ifndef __DRIVER_SIRF_EPHERMERIS__
#define __DRIVER_SIRF_EPHERMERIS__

// enable SIRF_EE for GLONASS
#define SIRF_EE_GLO

typedef enum {
	EPH_STATE_INIT,
	EPH_STATE_START_SGEE_DOWNLOAD_REQ,
	EPH_STATE_START_SGEE_DOWNLOAD_ACK,
	EPH_STATE_SET_SGEE_FILE_SIZE_REQ,
	EPH_STATE_SET_SGEE_FILE_SIZE_ACK,
	EPH_STATE_SEND_SGEE_FILE_REQ,
	EPH_STATE_SEND_SGEE_FILE_ACK,
	EPH_STATE_SEND_SGEE_FILE_ABORT_NO_RETRY,
	EPH_STATE_SEND_SGEE_FILE_ABORT_TO_RETRY,
	EPH_STATE_SEND_SGEE_FILE_FINISH
} eeph_state_t;

typedef enum
{
	EPH_NAVSYS_GPS,
	EPH_NAVSYS_GLO,
	NUM_EPH_NAVSYS
} eeph_navsys_t;

typedef enum
{
	EPH_NVM_ID_NULL = 0,
	EPH_NVM_ID_GPS_SGEE = 1,
	EPH_NVM_ID_GPS_CGEE = 2,
	EPH_NVM_ID_GPS_BE = 3,
	EPH_NVM_ID_GPS_HEADER = 5,
	EPH_NVM_ID_GLO_SGEE = 6,
	EPH_NVM_ID_GLO_CGEE = 7,
	EPH_NVM_ID_GLO_BE = 8,
	EPH_NVM_ID_GLO_HEADER = 10,
	NUM_EPH_NVM_ID
} eeph_nvm_id_t;

typedef enum
{
	EPH_NVM_FILE_TYPE_INVALID = -1,
	EPH_NVM_FILE_TYPE_SGEE = 0,
	EPH_NVM_FILE_TYPE_CGEE = 1,
	EPH_NVM_FILE_TYPE_BE = 2,
	EPH_NVM_FILE_TYPE_HEADER = 3
} eeph_nvm_file_type_t;

typedef enum
{
	EPH_ACK_NACK_REASON_SUCCESS = 0,
	EPH_ACK_NACK_REASON_INSUFF_SPACE = 1,
	EPH_ACK_NACK_REASON_INVALID_PACKET_LEN = 2,
	EPH_ACK_NACK_REASON_RECV_OUT_SEQ = 3,
	EPH_ACK_NACK_REASON_NONEWFILE = 4,
	EPH_ACK_NACK_REASON_CURRUPT_DL_FILE = 5,
	EPH_ACK_NACK_REASON_GENERAL_DL_FAILURE = 6,
	EPH_ACK_NACK_REASON_GENERAL_API_FAILURE = 7,
	EPH_ACK_NACK_REASON_SIF_AID_IN_PROGRESS = 8,
	EPH_ACK_NACK_REASON_SIF_AID_NOT_STARTED = 9,
	EPH_ACK_NACK_REASON_NONEWFILE_BUT_MOREDATA = 10,
	NUM_EPH_ACK_NACK_REASON
} eeph_ack_nack_reason_t;

/* MID 232, SID 86 */
#define EPH_EE_STORAGE_TYPE_HOST 0
#define EPH_EE_STORAGE_TYPE_I2C_EEPROM 1
#define EPH_EE_STORAGE_TYPE_FLASH 2
#define EPH_EE_STORAGE_TYPE_NO_STORAGE 3

#define EPH_EE_STORAGE_INTERNAL (0<<4)
#define EPH_EE_STORAGE_EXTERNAL (1<<4)

#define SGEE_GPS_FILE "/mnt/userdata/quickfix/sifgps.f2pxenc.ee"
#define SGEE_GLO_FILE "/mnt/userdata/quickfix/sifglo.f2pxenc.ee"

#define SGEE_DETECT_TIME_SEC 18
#define SGEE_GENERAL_RETRY_TIME_SEC 18
#define RESCHEDULE_INTERVAL_RETRY_EXHAUST (1*3600) /* in seconds; Reschedule interval when all reties failed. */

#define INVALID_SGEE_AGE 0x7FFFFFFF

bool sirf_ephemeris_init(struct gps_device_t *session, eeph_navsys_t navsys);
bool sirf_ephemeris_start(struct gps_device_t *session, eeph_navsys_t navsys, const char *filename);
void sirf_ephemeris_handle_eclm(struct gps_device_t *session, uint8_t *buf, uint32_t len);
void sirf_ephemeris_state_machine(struct gps_device_t *session);
bool poll_sgee_age(struct gps_device_t *session, uint8_t navsys);
#endif //__DRIVER_SIRF_EPHERMERIS__
