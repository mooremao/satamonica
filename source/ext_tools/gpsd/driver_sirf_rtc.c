/*
 * This is the gpsd driver RTC helper for SiRF GPSes operating in binary mode.
 * Copyright (c) 2014 TomTom B.V.
 *
 * Implements the message flow suggested by CSR to set the GSD internal RTC:
 *
 *  1. Issue cold start – MID 128
 *  2. Wait for HW config request, MID 71
 *  3. Send out HW config response, MID 214 with coarse time available PLUS frequency aiding
 *  4. Send out Session Close, MID 213,2
 *  5. Wait for an approx. pos request, MID 73,1
 *  6. Send out reject, MID 216,2 with reason Not Available
 *  7. Wait for a Coarse time request, MID 73,2
 *  8. Send out Time response, MID 215,2
 *  9. Wait for an frequency transfer, MID 73,3
 * 10. Send out frequency response, MID 215,3:
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "gpsd.h"
#include "driver_sirf_ephemeris.h"
#include "driver_sirf_rtc.h"
#include "gsd_rtc_defs.h"

#define RTC_SET_TIMEOUT	10

static void set_time_res_msg(struct gps_device_t * session, int time);

static int pipe_rtc_fd = -1;
static struct timespec start_time;

/* Message flow state machine */
enum rtc_stat_t {
	RTC_IDLE,		/* idle state */
	RTC_RST,		/* MID128 (reset) sent */
	RTC_SESSION_CLOSE,	/* Session close sent */
	RTC_POS_REJ,		/* Position reject sent */
	RTC_TIME_RES,		/* Time response sent */
	RTC_FINISH,		/* RTC setup finished */
};

static enum rtc_stat_t rtc_state = RTC_IDLE;

/* Reset (MID128) */
static unsigned char cold_start[] = {
    0x80,			/* MID 128 */
    0x00, 0x00, 0x00, 0x00,	/* EXEF X */
    0x00, 0x00, 0x00, 0x00,	/* ECEF Y */
    0x00, 0x00, 0x00, 0x00,	/* ECEF Z */
    0x00, 0x00, 0x00, 0x00,	/* clock drift */
    0x00, 0x00, 0x00, 0x00,	/* time of week */
    0x00, 0x00,			/* week number */
    0x0C,			/* Chans 1-12 */
    0x04,			/* reset bit map (0x4 = cold start) */
};

/* HW config response (MID 214) with coarse time available PLUS frequency aiding */
static unsigned char hw_cfg_response[] = {
    0xd6,				/* MID 214 */
    0xce,				/* HW CONFIG (0xce = coarse time + frequency) */
    0x00, 0x00, 0x00, 0x00, 0x00,	/* NOMINAL FREQUENCY */
    0x00,				/* NW ENHANCE TYPE */
};

/* Session close (MID 213,2) */
static unsigned char session_close[] = {
    0xd5,	/* MID 213 */
    0x02,	/* SID 2 */
    0x00,	/* REQ INFO (00 = close request) */
};

/* Position reject (MID 216,2) with reason "not available"*/
static unsigned char pos_rej[] = {
    0xd8,	/* MID 216 */
    0x02,	/* SID 2 */
    0x49,	/* REJ MID (0x49 = reject MID 73 */
    0x01,	/* REJ SID (0x01 = reject SID 1 */
    0x04,	/* REJ REASON (0x04 = "not available") */
};

/* Time response (MID 215,2) */
static unsigned char time_response[] = {
    0xd7,				/* MID 215 */
    0x02,				/* SID 2 */
    0x00,				/* TT TYPE (00 = coarse time) */
    0x00, 0x00,				/* GPS WEEK (set later) */
    0x00, 0x00, 0x00, 0x00, 0x00,	/* GPS TIME (set later) */
    0x00, 0x00, 0x00,			/* DELTA UTC (set later)*/
    0xed,				/* TIME ACCURACY (0xed = 29696 ms : anything more prevents the time to survive the reset) */
};

/* Frequency response (MID 215,3) */
static unsigned char freq_response[] = {
    0xd7,			/* MID 215 */
    0x03,			/* SID 3 */
    0x00, 0x00,			/* FREQ OFFSET (0 = no offset) */
    0xb8,			/* REL FREQ ACC (b8 = 12 ppm)*/
    0xff, 0xff, 0xff, 0xfe,	/* TIME TAG (fffffffe = msg valid) */
    0x81, 			/* REF CLOCK INFO */
};

/* Initialize the RTC pipe */
int sirf_rtc_init(struct gps_device_t *session)
{
	unlink(GSD_RTC_PIPE);
	mkfifo(GSD_RTC_PIPE,S_IRUSR| S_IWUSR);

	pipe_rtc_fd = open(GSD_RTC_PIPE, O_RDONLY | O_NDELAY | O_NONBLOCK);

	if (pipe_rtc_fd < 0) {
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_RTC: RTC pipe creation fail (%s)\n", strerror(errno));
		return -1;
	}
	gpsd_report(session->context->debug, LOG_PROG,"SIRF_RTC: RTC pipe created\n");

	return 0;
}

/* Remove the RTC pipe */
void sirf_rtc_deinit(struct gps_device_t *session)
{
	gpsd_report(session->context->debug, LOG_PROG, "SIRF_RTC: removing pipe\n");

	if(pipe_rtc_fd > 0)
		close(pipe_rtc_fd);

	pipe_rtc_fd = -1;
	unlink(GSD_RTC_PIPE);
}

/* Start the RTC setup message flow */
static void sirf_rtc_start(struct gps_device_t *session)
{
	gpsd_report(session->context->debug, LOG_PROG,"SIRF_RTC: start RTC time set procedure\n");

	/* Get the timestamp of the start */
	if (clock_gettime(CLOCK_MONOTONIC, &start_time) < 0) {
		gpsd_report(session->context->debug, LOG_ERROR,"SIRF_RTC: unable to determine CLOCK_MONOTONIC: %s\n", strerror(errno));
	} else {
		/* Send the 1st message and update the state */
		rtc_state = RTC_RST;
		(void)send_osp(session, cold_start, sizeof(cold_start));
	}
}

/* Polling function called periodically by the main SiRF driver */
void sirf_rtc_poll(struct gps_device_t *session)
{
	char data[100];
	struct timespec curr_time;
	int timediff;
	int user_time;

	/* Don't do anything if the pipe is not created */
	if(pipe_rtc_fd < 0)
		return;

	switch (rtc_state) {

		case RTC_FINISH:
			/* When the RTC is updated, request the new SGEE age */
			poll_sgee_age(session, EPH_NAVSYS_GPS);
#ifdef SIRF_EE_GLO
			poll_sgee_age(session, EPH_NAVSYS_GLO);
#endif
			rtc_state = RTC_IDLE;
			system("systemctl start ntpd");
			break;

		case RTC_IDLE:
			/* Read the pipe only in idle state */
			if(read(pipe_rtc_fd, &data, sizeof(data)) > 0) {
				user_time = (int)strtol(data, NULL, 10);
				gpsd_report(session->context->debug, LOG_PROG,"SIRF_RTC: got time from user: %d\n", user_time);

				/* If a fix is already present, don't set the RTC to avoid loosing it and overwrite a valid time */
				if (session->gpsdata.status != STATUS_NO_FIX) {
					gpsd_report(session->context->debug, LOG_PROG,"SIRF_RTC: fix present, skip RTC setting\n");
					break;
				}

				/* Sanity check on the value */
				if (user_time < DEFAULT_RTC_DATE) {
					gpsd_report(session->context->debug, LOG_ERROR,"SIRF_RTC: invalid time: %d\n", user_time);
				} else {
					/* stop ntpd, ntpd doesn't like big jumps in time */
					system("systemctl stop ntpd");
					curr_time.tv_sec = user_time;
					curr_time.tv_nsec = 0;
					if (clock_settime(CLOCK_REALTIME, &curr_time) < 0)
					{
						gpsd_report(session->context->debug, LOG_ERROR,"SIRF_RTC: failed to set system's time: %s\n", strerror(errno));
					}
					/* Time is valid: set the message fields and start the flow */
					set_time_res_msg(session, user_time);
					sirf_rtc_start(session);
				}
			}
			break;

		default:
			/* In all other states check for timeout */
			if (clock_gettime(CLOCK_MONOTONIC, &curr_time) == 0) {
				timediff = (int)difftime(curr_time.tv_sec, start_time.tv_sec);
				if (timediff > RTC_SET_TIMEOUT) {
					gpsd_report(session->context->debug, LOG_ERROR, "SIRF_RTC: timeout setting RTC time\n");
					rtc_state = RTC_IDLE;
					system("systemctl start ntpd");
				}
			}
	}
}

/* Populate the response message with the provided time */
static void set_time_res_msg(struct gps_device_t * session, int time)
{
	int diff;
	int weeks;
	int delta;
	long long tow;

	/* Calculate the gap from GPS epoch and add leap seconds correction */
	diff = time - GPS_EPOCH + DELTA_UTC;

	/* Calculate the number of weeks from GPS epoch */
	weeks = diff/604800;

	/* Calculate the number of microseconds from the current week */
	tow = ((long long)(diff%604800))*1000000;

	/* Leap correction should be in milliseconds */
	delta = DELTA_UTC*1000;

	gpsd_report(session->context->debug, LOG_PROG,"SIRF_RTC: set time response fields: weeks:%d tow:%lli delta:%d\n", weeks, tow, delta);

	/* Set GPS week */
	time_response[3] = (unsigned char) ((weeks >> 8) & 0xFF);
	time_response[4] = (unsigned char) (weeks & 0xFF);

	/* Set GPS time of week */
	time_response[5] = (unsigned char) ((tow >> 32) & 0xFF);
	time_response[6] = (unsigned char) ((tow >> 24) & 0xFF);
	time_response[7] = (unsigned char) ((tow >> 16) & 0xFF);
	time_response[8] = (unsigned char) ((tow >> 8) & 0xFF);
	time_response[9] = (unsigned char) (tow & 0xFF);

	/* Set UTC delta */
	time_response[10] = (unsigned char) ((delta >> 16) & 0xFF);
	time_response[11] = (unsigned char) ((delta >> 8) & 0xFF);
	time_response[12] = (unsigned char) (delta & 0xFF);
}

/* Parse messages for RTC setup */
void sirf_rtc_msg(struct gps_device_t * session, unsigned char *msg)
{
	unsigned char mid;

	/* Don't care in idle state */
	if (rtc_state == RTC_IDLE)
		return;

	mid = msg[0];
	gpsd_report(session->context->debug, LOG_PROG, "SIRF_RTC: rtc mid:%d state:%d\n", mid, rtc_state);

	switch(mid) {
		case 71:
			if (rtc_state == RTC_RST) {
				/* MID 71: hw cfg request -> send hw cfg response and session close */
				gpsd_report(session->context->debug, LOG_PROG, "SIRF_RTC: received hw cfg req -> send hw cfg res + session close\n");
				rtc_state = RTC_SESSION_CLOSE;
				(void)send_osp(session, hw_cfg_response, sizeof(hw_cfg_response));
				(void)send_osp(session, session_close, sizeof(session_close));
			} else {
				gpsd_report(session->context->debug, LOG_ERROR, "SIRF_RTC: skipping received message mid:%d at state:%d\n", mid, rtc_state);
			}
			break;

		case 73:
			if ((msg[1] == 1) && (rtc_state == RTC_SESSION_CLOSE)) {
				/* MID 73,1: position request -> send position reject */
				gpsd_report(session->context->debug, LOG_PROG, "SIRF_RTC: received pos req -> send pos rej\n");
				rtc_state = RTC_POS_REJ;
				(void)send_osp(session, pos_rej, sizeof(pos_rej));
			} else if ((msg[1] == 2) && (rtc_state == RTC_POS_REJ)) {
				/* MID 73,2: time request -> send time response */
				gpsd_report(session->context->debug, LOG_PROG, "SIRF_RTC: received time req -> send time res\n");
				rtc_state = RTC_TIME_RES;
				(void)send_osp(session, time_response, sizeof(time_response));
			} else if ((msg[1] == 3) && (rtc_state == RTC_TIME_RES)) {
				/* MID 73,3: frequency transfer -> send frequency response */
				gpsd_report(session->context->debug, LOG_PROG, "SIRF_RTC: received freq transfer -> send freq res\n");
				rtc_state = RTC_FINISH;
				(void)send_osp(session, freq_response, sizeof(freq_response));
			} else {
				gpsd_report(session->context->debug, LOG_ERROR, "SIRF_RTC: skipping message mid:%d sid:%d at state:%d\n", mid, msg[1], rtc_state);
			}
			break;

		default:
			gpsd_report(session->context->debug, LOG_ERROR, "SIRF_RTC: received unexpected message mid:%d at state:%d\n", mid, rtc_state);
			break;
	}
}
