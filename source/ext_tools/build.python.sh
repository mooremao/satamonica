#!/bin/sh

# Script based on:
#
# http://randomsplat.com/id5-cross-compiling-python-for-embedded-linux.html
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

PYTHON_VERSION=Python-2.7.3

# Untar the python sources. We only do this when they do not yet exists.
cd python/
if [ ! -d ${PYTHON_VERSION} ]
then
	tar xzf ${PYTHON_VERSION}.tgz
fi

cd ${PYTHON_VERSION}/

# Build native (if needed), we need python and hostpgen to do some cross compilation.
# Building python here seems odd since we might just use an already installed python instance, but we do this since:
# - hostpgen is not part of any normal distibution so we have to build some here anyway
# - ensures we have the correct version of python.
#
# We try to apply the patch for cross compilation only once, after building the hostpython and hostpgen
if [ ! -f hostpython -o ! -f Parser/hostpgen ]
then
	./configure
	make python Parser/pgen
	mv python hostpython
	mv Parser/pgen Parser/hostpgen
	make distclean
	
	patch -p1 < ../${PYTHON_VERSION}-xcompile.patch
fi

# Configure
CC=arm-arago-linux-gnueabi-gcc CXX=arm-arago-linux-gnueabi-g++ AR=arm-arago-linux-gnueabi-ar RANLIB=arm-arago-linux-gnueabi-ranlib ./configure --host=arm-arago-linux-gnueabi --build=x86_64-linux-gnu --prefix=`pwd`/../${PYTHON_VERSION}_arm-arago-linux-gnueabi/usr

# Make
make HOSTPYTHON=./hostpython HOSTPGEN=./Parser/hostpgen BLDSHARED="arm-arago-linux-gnueabi-gcc -shared" CROSS_COMPILE=arm-arago-linux-gnueabi- CROSS_COMPILE_TARGET=yes HOSTARCH=arm-arago-linux-gnueabi BUILDARCH=x86_64-linux-gnu

# Make install
make install HOSTPYTHON=./hostpython BLDSHARED="arm-arago-linux-gnueabi-gcc -shared" CROSS_COMPILE=arm-arago-linux-gnueabi- CROSS_COMPILE_TARGET=yes
