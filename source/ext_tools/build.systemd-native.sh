#!/bin/sh

# Script to (re-)build systemd nativly (for journalctl)
#
#

set -e
set -x

SYSTEMD_VERSION=systemd-216

cd systemd

BUILDDIR=`pwd`/${SYSTEMD_VERSION}_build_native
DESTDIR=`pwd`/${SYSTEMD_VERSION}_native

BASHCOMPLETIONDIR=/usr/share/bash-completion/completions
ZSHCOMPLETIONDIR=/usr/share/zsh/site-functions

cd systemd-repo

if [ "x${1}" = "xdistclean" ]
then
	make distclean
fi

#Run ./autogen.sh if ./configure does not exists yet.
if [ ! -f ./configure ]
then
	./autogen.sh
fi

cd ..
mkdir -p ${BUILDDIR}
cd ${BUILDDIR}

#Configure when not already done.
if [ ! -f ./Makefile ]
then
	LDFLAGS=-m32 CPPFLAGS=-m32 \
	../systemd-repo/configure \
		--prefix=/usr \
		--libexecdir=/lib \
		--sysconfdir=/etc \
		--with-rootprefix= \
		--with-rootlibdir=/lib \
		--with-dbuspolicydir=/etc/dbus-1/system.d \
		--with-dbussessionservicedir=/usr/share/dbus-1/services \
		--with-dbussystemservicedir=/usr/share/dbus-1/system-services \
		--with-dbusinterfacedir=/usr/share/dbus-1/interfaces \
		--with-sysvinit-path= \
		--with-sysvrcnd-path= \
		--with-firmware-path=/lib/firmware \
		--disable-nls \
		--disable-python-devel \
		--disable-dbus \
		--disable-blkid \
		--disable-seccomp \
		--disable-ima \
		--disable-selinux \
		--disable-chkconfig \
		--disable-apparmor \
		--disable-xz \
		--disable-lz4 \
		--disable-pam \
		--disable-acl \
		--disable-smack \
		--disable-gcrypt \
		--disable-audit \
		--disable-elfutils \
		--disable-libcryptsetup \
		--disable-qrencode \
		--disable-microhttpd \
		--disable-gnutls \
		--disable-libcurl \
		--disable-libidn \
		--disable-binfmt \
		--disable-vconsole \
		--disable-readahead \
		--disable-bootchart \
		--disable-quotacheck \
		--disable-tmpfiles \
		--disable-sysusers \
		--disable-firstboot \
		--disable-randomseed \
		--disable-backlight \
		--disable-rfkill \
		--disable-logind \
		--disable-machined \
		--disable-hostnamed \
		--disable-timedated \
		--disable-timesyncd \
		--disable-localed \
		--disable-coredump \
		--disable-polkit \
		--disable-networkd \
		--disable-resolved \
		--disable-efi \
		--disable-multi-seat-x \
		--disable-terminal \
		--disable-kdbus \
		--disable-myhostname \
		--disable-gudev \
		--disable-manpages \
		--disable-ldconfig \
		--enable-split-usr \
		--disable-tests \
		--disable-kmod
fi

#Compile it.
make

#Remove destdir and install systemd to destdir
rm -rf ${DESTDIR}
make install DESTDIR=${DESTDIR}
