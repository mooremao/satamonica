#!/bin/sh
#
# Script to (re-)build/install python junitxml package
# Based on build.uinput.sh
#

set -e

JUNITXML_VERSION=junitxml-0.7
LINUX_DEVKIT_PATH=../../../ti_tools/linux_devkit/bin/
CROSS_COMPILE=arm-arago-linux-gnueabi-
INSTALL_DIR=../../python/Python-2.7.3_arm-arago-linux-gnueabi/usr/lib/python2.7/site-packages/

# Untar the python junitxml sources. We only do this when they do not yet exist.
cd python-junitxml/
if [ ! -d ${JUNITXML_VERSION} ]
then
	tar xvf ${JUNITXML_VERSION}.tar.gz
fi
cd ${JUNITXML_VERSION}/

PATH=$PATH:${LINUX_DEVKIT_PATH} \
CC="${CROSS_COMPILE}gcc -pthread" \
python setup.py install_lib --install-dir ${INSTALL_DIR}
