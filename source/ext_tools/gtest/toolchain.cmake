#
# arm-arago-linux-gnueabi toolchain definitions for cmake
#

#
# cmake is used to build gtest, which doesn't get rebuilt as part of a Longbeach
# full platform build - so cmake is *not* a dependency for most developers. If
# you desire to rebuild gtest, you need cmake >= 2.6.2 (for gtest 1.7.0)
#

set(CMAKE_SYSTEM_NAME Linux)

set(CMAKE_C_COMPILER arm-arago-linux-gnueabi-gcc)
set(CMAKE_CXX_COMPILER arm-arago-linux-gnueabi-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
