#!/bin/sh

# Script to (re-)build metalog
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

METALOG_VERSION=metalog-3

# Untar the metalog sources. We only do this when they do not yet exists.
cd metalog/
if [ ! -d ${METALOG_VERSION} ]
then
	tar xJf ${METALOG_VERSION}.tar.xz

	cd ${METALOG_VERSION}/
	patch -p1 < ../0001-journald-support.patch
	patch -p1 < ../0002-sequence-feature.patch
	patch -p1 < ../0003-flushed-feature.patch
	patch -p1 < ../0004-signal-exit-value.patch
	patch -p1 < ../0005-systemd-notify-ready.patch
	patch -p1 < ../0006-termination-improvements.patch
else
	cd ${METALOG_VERSION}/
fi

# Configure
CC=arm-arago-linux-gnueabi-gcc CXX=arm-arago-linux-gnueabi-g++ STRIP=arm-arago-linux-gnueabi-strip \
    ./configure \
        --host=arm-arago-linux \
        --prefix=`pwd`/../${METALOG_VERSION}_arm-arago-linux-gnueabi \
        --datarootdir=`pwd`/../${METALOG_VERSION}_arm-arago-linux-gnueabi-share \
        CFLAGS="-I../../../systemd/systemd-216_arm-arago-linux-gnueabi-dev/usr/include/ -g -O2 -DHAVE_JOURNALD -DHAVE_FEATURE_SEQUENCE -DHAVE_FEATURE_FLUSHED_INDICATOR -DHAVE_FEATURE_SYSTEMD_NOTIFY_READY" \

# Make and install it
make LDFLAGS="-L../../../systemd/systemd-216_arm-arago-linux-gnueabi/usr/lib/ -lsystemd"
make install

# Install our own config file
cd ..
cp metalog.conf ${METALOG_VERSION}_arm-arago-linux-gnueabi/etc/
