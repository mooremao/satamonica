#!/bin/sh
#
# Script to (re-)build python uinput modules
# Based on build.dropbear.sh
#

set -e

PYTHON_UINPUT_VERSION=python-uinput-0.10.2
LINUX_DEVKIT_PATH=../../../ti_tools/linux_devkit/bin/
CROSS_COMPILE=arm-arago-linux-gnueabi-
INSTALL_DIR=../../python/Python-2.7.3_arm-arago-linux-gnueabi/usr/lib/python2.7/site-packages/
LIB_UDEV_DIR=../../systemd/systemd-216_arm-arago-linux-gnueabi/usr/lib/

# Untar the python uinput sources. We only do this when they do not yet exist.
cd python-uinput/
if [ ! -d ${PYTHON_UINPUT_VERSION} ]
then
	tar xvf ${PYTHON_UINPUT_VERSION}.tar.gz
fi
cd ${PYTHON_UINPUT_VERSION}/

PATH=$PATH:${LINUX_DEVKIT_PATH} \
CC="${CROSS_COMPILE}gcc -pthread" \
LDSHARED="${CC} -shared -L ${LIB_UDEV_DIR}" \
python setup.py install_lib --install-dir ${INSTALL_DIR}
