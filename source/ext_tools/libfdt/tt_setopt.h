
#ifndef __TT_SETOPT_H__
#define __TT_SETOPT_H__

#include "dtc.h"

#define TTSO_TYPE_STRING	0
#define TTSO_TYPE_INTEGER	1

int add_setopt(int type, char *cmdlineopt);
int do_setopt(struct boot_info *bi);

#endif
