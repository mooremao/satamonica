/*
 * (C) Copyright David Gibson <dwg@au1.ibm.com>, IBM Corporation.  2005.
 * (C) Copyright 2009 TomTom BV <http://www.tomtom.com/>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *                                                                   USA
 */

#include "tt_setopt.h"
#include "fdt.h"

typedef struct setopt {
	const char	*fullpath;	/* Node path */
	const char	*propname;	/* Property name */
	char		*value;		/* Property value */
	long		ivalue;		/* Property value stored as long */
	int		len;		/* Length of property */
	int		set_count;	/* Number of time seen in source */
	struct setopt	*next;		/* Linked-list next element */
} setopt_t;

static setopt_t *pso_root = NULL;

#define for_each_setopt(n, p) \
	for ((p) = (n); (p); (p) = (p)->next)

/*
 * Add a new argument of type -i / -s encountered on the command-line.
 * type: TTSO_TYPE_INTEGER (-i)
 *       TTSO_TYPE_STRING  (-s)
 * cmdlineopt: command-line value, such as "/path/to/node/property=value"
 * Returns -1 if parsing error, 0 if OK.
 */
int add_setopt(int type, char *cmdlineopt)
{
	setopt_t *pso;
	char *p;
	char *err;
	pso = (setopt_t*)malloc(sizeof(setopt_t));
	if (pso == NULL) {
		fprintf(stderr, "malloc failed.\n");
		return -1;
	}
	/* Split the cmdlineopt in 3 pieces:
	 * - /path/to/node
	 * - property name
	 * - property value
	 * We do it in place by replacing 2 command-line argument char by \0.
	 */
	pso->fullpath = cmdlineopt;
	/* /path/to/node MUST start with a "/", even for a root property. */
	if (cmdlineopt[0] != '/') {
		fprintf(stderr, "/path/to/node MUST start with a '/': %s\n",
			cmdlineopt);
		return -1;
	}
	/* Node path + property name does not contain '='. */
	p = strstr(pso->fullpath, "=");
	if (p == NULL) {
		fprintf(stderr, "Invalid set option: %s\n", cmdlineopt);
		return -1;
	}
	*p = '\0';
	switch (type) {
	case TTSO_TYPE_INTEGER:
		/* Using 0 as the base, we rely on strtol for autodetection */ 
		pso->ivalue = strtol(p + 1, &err, 0);
		if (*err != '\0') {
			fprintf(stderr, "Invalid long int: '%s'\n", err);
			return -1;
		}
		pso->ivalue = cpu_to_fdt32(pso->ivalue);
		pso->value = (char*)&(pso->ivalue);
		/* Length is 4 and not sizeof(long)! See dtc specs. */
		pso->len = 4;
		break;
	case TTSO_TYPE_STRING:
		pso->value = p + 1;
		/* We have to include the final \0 for strings. */
		pso->len = strlen(p + 1) + 1;
		break;
	default:
		fprintf(stderr, "Invalid type: %d\n", type);
		return -1;
	}
	/* Go backward to the last '/' to split /path/to/node
	*  from property name:
	 * Before: "/path/to/node/property"
	 * After:  "/path/to/node\0property"
	 */
	while (p >= cmdlineopt && *p != '/')
		p--;
	if (*p != '/') {
		fprintf(stderr, "Missing / in node path: %s\n", cmdlineopt);
		return -1;
	}
	*p = '\0';
	/* Special case for root property such as "/property" :
	 * if the fullpath is empty, set it to "/". This is caused by
	 * the non-orthogonal way fullpath are stored into the tree by DTC:
	 * root is "/", other path are "/path" without '/' at the end. */
	if (pso->fullpath[0] == '\0')
		pso->fullpath = "/";
	pso->propname = p + 1;
	pso->set_count = 0;
	pso->next = pso_root;
	pso_root = pso;
	return 0;
}

static void setprop(const char *fullpath, struct property *prop)
{
	setopt_t *pso;

	for_each_setopt(pso_root, pso)
	if (strcmp(fullpath, pso->fullpath) == 0 &&
	    strcmp(prop->name, pso->propname) == 0) {
		prop->val.val = pso->value;
		prop->val.len = pso->len;
		pso->set_count++;
	}
}

static void setnode(struct node *tree)
{
	struct property *prop;
	struct node *child;
	
	for_each_property(tree, prop) {
		setprop(tree->fullpath, prop);
	}
	for_each_child(tree, child) {
		setnode(child);
	}
}

/*
 * Do the transformation: scan the whole tree and changes all values
 * on the fly.
 * bi: device-tree in-memory representation.
 */
int do_setopt(struct boot_info *bi)
{
	setopt_t *pso;

	/* Do the transform */
	setnode(bi->dt);

	/* Print warning for each property not encountered in the input. */
	for_each_setopt(pso_root, pso)
	if (pso->set_count == 0 && quiet < 2) {
		fprintf(stderr, "Warning: Property %s%s%s was never seen!\n",
			pso->fullpath,
			strcmp(pso->fullpath, "/") == 0 ? "" : "/",
			pso->propname);
	}
	return 0;
}
