#!/bin/sh

#
# Script to (re-)build gtest
#

set -e

make -C gtest mostlyclean && make -C gtest
