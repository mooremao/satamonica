#!/bin/sh
set -e

# Script to (re-)build kmod
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

KMOD_VERSION=kmod-18

DESTDIR=`pwd`/kmod/${KMOD_VERSION}_arm-arago-linux-gnueabi
DESTDIR_DEV=${DESTDIR}-dev
DESTDIR_DOC=${DESTDIR}-doc
DESTDIR_UNNEEDED=${DESTDIR}-unneeded

CREATE_SYMLINKS=" \
	/bin/lsmod \
	/sbin/depmod \
	/sbin/insmod \
	/sbin/lsmod \
	/sbin/modinfo \
	/sbin/modprobe \
	/sbin/rmmod"

# Untar the kmod sources. We only do this when they do not yet exists.
cd kmod/
if [ ! -d ${KMOD_VERSION} ]
then
	tar xJf ${KMOD_VERSION}.tar.xz

	cd ${KMOD_VERSION}/
else
	cd ${KMOD_VERSION}/
fi

if [ "x${1}" = "xdistclean" ]
then
	make distclean
fi

#Configure when not already done.
if [ ! -f ./Makefile ]
then
	CC=arm-arago-linux-gnueabi-gcc CXX=arm-arago-linux-gnueabi-g++ AR=arm-arago-linux-gnueabi-ar RANLIB=arm-arago-linux-gnueabi-ranlib ./configure \
		CFLAGS='-O2' --prefix=/ --sysconfdir=/etc --libdir=/usr/lib --includedir=/usr/include \
		--host=arm-arago-linux \
		--disable-maintainer-mode \
		--disable-manpages \
		--enable-tools
fi

#Compile it.
make

#Remove destdir and install kmod to destdir
rm -rf ${DESTDIR}
make install DESTDIR=${DESTDIR}

#Remove dev destdir and (re)create it
rm -rf ${DESTDIR_DEV}
mkdir -p ${DESTDIR_DEV}/usr/lib
mv ${DESTDIR}/usr/include ${DESTDIR_DEV}/usr/
mv ${DESTDIR}/usr/lib/pkgconfig ${DESTDIR_DEV}/usr/lib/
mv ${DESTDIR}/usr/lib/libkmod.la ${DESTDIR_DEV}/usr/lib/

#Remove unneeded destdir and (re)create it
rm -rf ${DESTDIR_UNNEEDED}
mkdir -p ${DESTDIR_UNNEEDED}/usr
mv ${DESTDIR}/usr/share ${DESTDIR_UNNEEDED}/usr

for symlink in ${CREATE_SYMLINKS}
do
	mkdir -p ${DESTDIR}/$(dirname $symlink)
	ln -s ../bin/kmod ${DESTDIR}/$symlink
done

