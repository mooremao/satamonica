#!/bin/sh

# Script to (re-)build ALSA library
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

ALSA_UTILS_VERSION=alsa-utils-1.0.20
ALSA_LIB_VERSION=alsa-lib-1.0.24.1

# Untar the sources. We only do this when they do not yet exists.
cd alsa_utils/
if [ ! -d ${ALSA_UTILS_VERSION} ]
then
	tar xjf ${ALSA_UTILS_VERSION}.tar.bz2
	cd ${ALSA_UTILS_VERSION}/
else
	cd ${ALSA_UTILS_VERSION}/
fi

# Configure
CC=arm-arago-linux-gnueabi-gcc CXX=arm-arago-linux-gnueabi-g++ STRIP=arm-arago-linux-gnueabi-strip ./configure --host=arm-arago-linux  --prefix=`pwd`/../${ALSA_UTILS_VERSION}_arm-arago-linux-gnueabi/usr --disable-xmlto --with-alsa-prefix=`pwd`/../../alsa_lib/${ALSA_LIB_VERSION}_arm-arago-linux-gnueabi/usr/lib --with-alsa-inc-prefix=`pwd`/../../alsa_lib/${ALSA_LIB_VERSION}-include/usr/include --mandir=`pwd`/../${ALSA_UTILS_VERSION}-man/usr --datarootdir=`pwd`/../${ALSA_UTILS_VERSION}-data/usr

# Make
make

# Make install
make install-strip

