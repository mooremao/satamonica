#!/bin/sh
#
# Building chrpath requires ggc-multilib to be installed.
# We need it to generate 32bits binaries which will work on 32bit and 64bits systems
#
# sudo apt-get install gcc-multilib
#
if [ ! -d chrpath-0.16 ]
then
	tar xzf chrpath-0.16.tar.gz
fi

cd chrpath-0.16
LDFLAGS=-m32 CPPFLAGS=-m32 ./configure --target=arm-arago-linux-gnueabi --prefix=`pwd`/../ext_devkit/
make
make install

