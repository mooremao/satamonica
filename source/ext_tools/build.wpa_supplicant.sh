#!/bin/sh

# Script to (re-)build wpa_supplicant
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

WPA_SUPPLICANT_VERSION=wpa_supplicant-2.3

# Untar the wpa_supplicant sources. We only do this when they do not yet exists.
cd wpa_supplicant/
if [ ! -d ${WPA_SUPPLICANT_VERSION} ]
then
	tar xzf ${WPA_SUPPLICANT_VERSION}.tar.gz

	cd ${WPA_SUPPLICANT_VERSION}/
	patch -p1 < ../0001-add-config.patch
	patch -p1 < ../0002-use-urandom.patch
else
	cd ${WPA_SUPPLICANT_VERSION}/
fi

# Make
cd wpa_supplicant; make CC=arm-arago-linux-gnueabi-gcc BINALL="wpa_supplicant wpa_cli wpa_passphrase"

# Create config file
cat > `pwd`/../../${WPA_SUPPLICANT_VERSION}_arm-arago-linux-gnueabi/etc/p2p_supplicant.conf <<"p2p_sup_defconfig"
ctrl_interface=/run/wpa_supplicant
update_config=1
device_name=test
device_type=1-0050F204-1
config_methods=physical_display
p2p_go_intent=0
driver_param=use_multi_chan_concurrent=1 use_p2p_group_interface=1
p2p_go_ht40=1
disassoc_low_ack=0
max_num_sta=1
p2p_go_max_inactivity=30

network={
	ssid=""
	bssid=00:00:00:00:00:00
	psk=""
	proto=RSN
	key_mgmt=WPA-PSK
	pairwise=CCMP
	auth_alg=OPEN
	mode=3
	disabled=2
}

p2p_sup_defconfig

# Make install
make BINALL="wpa_supplicant wpa_cli wpa_passphrase" BINDIR=`pwd`/../../${WPA_SUPPLICANT_VERSION}_arm-arago-linux-gnueabi/usr/sbin install
arm-arago-linux-gnueabi-strip ../../${WPA_SUPPLICANT_VERSION}_arm-arago-linux-gnueabi/usr/sbin/*
