#!/bin/sh

# Script to (re-)build dosfstools
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

DOSFSTOOLS_VERSION=dosfstools-3.0.26

# Untar the ntpd sources. We only do this when they do not yet exists.
cd dosfstools/
if [ ! -d ${DOSFSTOOLS_VERSION} ]
then
	tar xzf ${DOSFSTOOLS_VERSION}.tar.gz

	cd ${DOSFSTOOLS_VERSION}/
	patch -p1 < ../0001-fix-label-read.patch
else
	cd ${DOSFSTOOLS_VERSION}/
fi

# Make
make CC=arm-arago-linux-gnueabi-gcc CFLAGS="-O2 -fomit-frame-pointer -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"

# Make install
make PREFIX=`pwd`/../${DOSFSTOOLS_VERSION}_arm-arago-linux-gnueabi/usr install-bin
