#!/bin/sh

# Script to (re-)build blkid library (part of util-linux package)
#
# Note: Add the toolchain (linux_devkit/bin) to your path before running this script
#

set -e
set -x

UTIL_LINUX_VERSION=util-linux-2.26.1

# Untar the sources. We only do this when they do not yet exists.
cd util_linux/
if [ ! -d ${UTIL_LINUX_VERSION} ]
then
	tar xzf ${UTIL_LINUX_VERSION}.tar.gz
	cd ${UTIL_LINUX_VERSION}/
else
	cd ${UTIL_LINUX_VERSION}/
fi

# Configure
CC=arm-arago-linux-gnueabi-gcc AR=arm-arago-linux-gnueabi-ar STRIP=arm-arago-linux-gnueabi-strip RANLIB=arm-arago-linux-gnueabi-ranlib ./configure --host=arm-arago-linux --datarootdir=/../${UTIL_LINUX_VERSION}-data/usr --includedir=/../${UTIL_LINUX_VERSION}-include/usr --disable-all-programs --enable-libblkid

# Make
make

# Make install
make install-strip DESTDIR=`pwd`/../${UTIL_LINUX_VERSION}_arm-arago-linux-gnueabi/

